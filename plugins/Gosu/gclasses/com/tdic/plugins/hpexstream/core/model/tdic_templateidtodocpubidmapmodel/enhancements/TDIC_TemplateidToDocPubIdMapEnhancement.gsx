package com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_TemplateidToDocPubIdMapEnhancement : com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.TDIC_TemplateidToDocPubIdMap {
  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap) : com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.TDIC_TemplateidToDocPubIdMap {
    return new com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.TDIC_TemplateidToDocPubIdMap(object)
  }

  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap, options : gw.api.gx.GXOptions) : com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.TDIC_TemplateidToDocPubIdMap {
    return new com.tdic.plugins.hpexstream.core.model.tdic_templateidtodocpubidmapmodel.TDIC_TemplateidToDocPubIdMap(object, options)
  }

}