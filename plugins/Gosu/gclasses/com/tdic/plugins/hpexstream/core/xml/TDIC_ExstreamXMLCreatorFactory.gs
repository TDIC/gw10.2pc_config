/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.xml

uses gw.api.system.PLLoggerCategory
uses gw.document.SimpleSymbol
uses gw.lang.reflect.ReflectUtil
uses java.lang.Class
uses java.lang.ClassNotFoundException
uses java.lang.RuntimeException
uses java.util.Map
uses org.slf4j.LoggerFactory

class TDIC_ExstreamXMLCreatorFactory {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
  }

  /**
   * US656
   * 08/15/2014 shanem
   *
   * Returns product specific xml creator object
   *
   * Based on the contents of the param map, we determine which creator to return.
   * The Creator is constructed using reflection to keep this class generic across all XCenters.
   * Policy can include the account in the param map, we check if the BC Creator class exists, and if it does
   * we use the BC Creator, otherwise we return the PC Creator
   */
  @Param("aParamMap", "Parameter map containing GW entity to generate transaction data XML for.")
  @Returns("Product specific XMLCreator object")
  static function getExstreamXMLCreator(aParamMap: Map<Object, Object>): TDIC_ExstreamXMLCreator {
    _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - Entering")

    final var PC_CREATOR = "tdic.pc.integ.plugins.hpexstream.xml.TDIC_PCExstreamXMLCreator"
    final var BC_CREATOR = "tdic.pc.integ.plugins.hpexstream.xml.TDIC_BCExstreamXMLCreator"
    final var CC_CREATOR = "tdic.pc.integ.plugins.hpexstream.xml.TDIC_CCExstreamXMLCreator"

    var xmlCreator: TDIC_ExstreamXMLCreator
    var polPeriod = (aParamMap.get("policyPeriod") typeis SimpleSymbol)?(aParamMap.get("policyPeriod") as SimpleSymbol)?.Value as PolicyPeriod : aParamMap.get("policyPeriod") as PolicyPeriod
    var anAccount = (aParamMap.get("account") typeis SimpleSymbol)?(aParamMap.get("account") as SimpleSymbol)?.Value as Account : aParamMap.get("account") as Account
    var claim = (aParamMap.get(("Claim")) typeis SimpleSymbol)?(aParamMap.get("Claim") as SimpleSymbol)?.Value as Claim : aParamMap.get("Claim") as Claim

    if (polPeriod != null) {
      _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - PolicyCenter")
      xmlCreator = ReflectUtil.construct(PC_CREATOR, new Object[]{polPeriod})
    } else if (anAccount != null) {
      try {
        // 08/27/2014, shanem, US656: Check if class exists, if it does, we are in billingcenter
        Class.forName(BC_CREATOR)
        _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - BillingCenter")
        xmlCreator = ReflectUtil.construct(BC_CREATOR, new Object[]{anAccount})
      } catch (var e: ClassNotFoundException) {
        // 08/27/2014, shanem, US656: else, we are in policycenter
        _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - PolicyCenter")
        xmlCreator = ReflectUtil.construct(PC_CREATOR, new Object[]{anAccount})
      }
    } else if (claim != null) {
      _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - ClaimCenter")
      xmlCreator = ReflectUtil.construct(CC_CREATOR, new Object[]{claim})
    } else {
      throw new RuntimeException("Incorrect entity supplied for XML generation...")
    }

    _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - Exiting")
    return xmlCreator
  }
}

