package com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_FinalizationRequestEnhancement : com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest {
  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_FinalizationRequest) : com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest {
    return new com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest(object)
  }

  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_FinalizationRequest, options : gw.api.gx.GXOptions) : com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest {
    return new com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest(object, options)
  }

}