/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.messaging

uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses com.tdic.util.properties.PropertyUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentMetadataSource
uses gw.plugin.document.IDocumentTemplateDescriptor
uses gw.plugin.document.IDocumentTemplateSource
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.util.TDIC_DocumentEventSender
uses java.lang.Exception
uses java.util.Map
uses tdic.pc.integ.plugins.message.TDIC_MessagePlugin
uses gw.xml.XmlElement
uses tdic.pc.common.batch.unistat.util.TDIC_WCstatUtil
uses com.tdic.util.misc.EmailUtil
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * US555
 * 02/16/2015 Shane Murphy
 *
 * Message Transport class responsible for automated document production.
 * Uses <b>HP Exstream Command Center</b> on-demand and batch document creation.
 *
 * In the case of Batch creation, a call to Command Center API is not made directly, the composition payload data is
 * generated and stored in an intermediary staging area in the integration database ExstreamPayload table
 * The actual call to the command Center API is performed once an independent batch process is run and
 * doWork() method is triggered ( see {@link "tdic.plugin.hpexstream.pc.batch.TDIC_DocumentCreationBatch#doWork()"}),
 * it extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to
 * start a composition job.
 */
class TDIC_ExstreamTransport extends TDIC_MessagePlugin implements InitializablePlugin {
  private static var _payloadFilePath: String as PayloadFilePath
  private static final var _ccJobDefName:String as CCJobDefName = PropertyUtil.getInstance().getProperty("ccJobDefName")+"-"+gw.api.system.server.ServerUtil.getEnv().toUpperCase()
  private static final var _finalizationJobDefName:String as FinalizationJobDefName = PropertyUtil.getInstance().getProperty("finalizationJobDefName")+"-"+gw.api.system.server.ServerUtil.getEnv().toUpperCase()
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private var _docInfoAsString: String
  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Send message to HP Exstream containing the output XML which includes
   * all data required and list of documents to create for the event.
   */
  override function send(aMessage: Message, aPayload: String): void {
    _docInfoAsString = "AccountNumber: ${aMessage.Account.AccountNumber}; PolicyNumber: ${aMessage.PolicyPeriod.PolicyNumber}; Term: ${aMessage.PolicyPeriod.TermNumber}; Event: ${aMessage.EventName}"
    _logger.debug("TDIC_ExstreamTransport#send(Message, String) - aMessage.Payload: " + aMessage.Payload)
    _logger.info("TDIC_ExstreamTransport#send(Message, String)- ${_docInfoAsString}-Send message started ")

    /*
     To get the TemplateId, we can parse the XML payload and get the first templateId.
     To avoid using XCenter specific GX Model references here, we use XMLElement

     We can take the first template as all templates in the list will have the same meta data for the entry we're interested in.
    */
    try{
      var templateId: String
      if(aMessage.EventName == "FinalizeExstreamDocument") {
        templateId = (aMessage.MessageRoot as Document).TemplateId_TDIC
      } else {
        var parsedPayload = XmlElement.parse(aMessage.Payload)
        templateId = parsedPayload.$Children.where( \ elt -> elt.$QName.LocalPart == "TemplateIds").first()?.$Children?.first()?.$Children?.first()?.$SimpleValue?.StringValue
      }

      var templateDescriptor = getTemplateDescriptor(templateId)
      var _editable : Boolean = false
      if (templateDescriptor != null) {
        _editable = Coercions.makeBooleanFrom(templateDescriptor.getMetadataPropertyValue("Editable_TDIC"))
      }
      _logger.info("TDIC_ExstreamTransport#send(Message, String) - Editable document: " + _editable)
      if (_editable == true) {
        handleEditableDocument(aMessage)
        if (aMessage.MessageRoot != null && aMessage.MessageRoot typeis Document) {
            updateDocumentMetadata(aMessage.MessageRoot as Document, true)
          }
      } else {
        var successFlag = sendBatchDocument(aMessage)
        if(successFlag && aMessage.MessageRoot != null && aMessage.MessageRoot typeis Document) {
          updateDocumentMetadata(aMessage.MessageRoot as Document, successFlag)
        }
      }
      _logger.info("TDIC_ExstreamTransport#send(Message, String)-${_docInfoAsString}-Send message completed.")
    }catch(e: Exception){
      _logger.error("TDIC_ExstreamTransport#send(Message, String)-${_docInfoAsString}-Error while sending message to HP Exstream. Payload: \n${aMessage.Payload}", e)
      aMessage.ErrorDescription = e.Message
      aMessage.reportError()
    }

  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function resume(): void {
    _logger.info("TDIC_ExstreamTransport#resume() - Entering.")
    // Do Nothing
    _logger.info("TDIC_ExstreamTransport#resume() - Exiting.")
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function shutdown(): void {
    _logger.info("TDIC_ExstreamTransport#shutdown() - Entering.")
    // Do Nothing
    _logger.info("TDIC_ExstreamTransport#shutdown() - Exiting.")
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Reading the Parameter from the Plugin Directory
   */

  override property set Parameters(aParamMap: Map<Object, Object>): void {
  }

  /**
   * US555
   * 01/26/2015 Shane Murphy
   *
   * Writes payload data to the integration database.
   */
  @Param("aPayload", "Payload to store in the database")
  protected function sendBatchDocument(aMessage: Message): boolean {
    var successFlag = true
    _logger.trace("TDIC_ExstreamTransport#sendBatchDocument(Message) - Entering.")
    try{
      if (TDIC_DocumentEventSender.sendMessage(aMessage.Payload) < 1) {
        _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) - ${_docInfoAsString} - Could not send Payload to Exstream.")
        successFlag = false
        reportError(aMessage, "Could not send Payload to HPExstream, there is some issue with payload, Please verify in HPExstream log, fix the payload, by editing the payload and retry." +
                    "If the issue can't be resolved by editing the payload, Please skip the message from destination and create an IT Ticket to resolve the issue. ")

      } else {
        aMessage.reportAck()
      }
    }catch(e: Exception){
      _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) - ${_docInfoAsString} - Doc Production service is unable to process document request",e)
      reportError(aMessage, e.Message)
    }
    _logger.trace("TDIC_ExstreamTransport#sendBatchDocument(Message) - Exiting.")
    return successFlag
  }



  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * If the document is editable we send the request to HP Exstream to generate the document immediately.
   */
  @Param("anEventName", "The data type is: String")
  @Param("aPayload", "The data type is: String")
  protected function handleEditableDocument(aMessage: Message) {
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Entering.")
    var requestToCmd = getRequestObject(aMessage.Payload, aMessage.EventName)
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - aMessage.Payload: " + aMessage.Payload)
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Request: " + requestToCmd.getXMLModelAttributes())
    try {
      var actionInterface = TDIC_ExstreamHelper.getDocProdService()
      _logger.debug("Request Payload: ")
      requestToCmd.getXMLModelAttributes().each(\elt -> _logger.debug(elt.asUTFString()))
      var responseFromCMD = actionInterface.action("CREATE_JOB", requestToCmd.getXMLModelAttributes())
      _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Response from Command Center" + responseFromCMD.asUTFString())
      if (responseFromCMD != null and responseFromCMD.Attribute.where(\attrib -> (attrib.Name == "status_code" and attrib.Value_Attribute == "1001")).Empty == false) {
        aMessage.reportAck()
        _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Successfully consumed")
      } else {
        aMessage.reportError()
      }
    } catch (var responseException: Exception) {
      _logger.error("TDIC_DocumentCreationBatch#handleEditableDocument(String, String) - Exception in Response from Command Center " + responseException.toString(),responseException)
      aMessage.reportError()
    }
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Exiting")
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Depending on the event, Create a finalization or batch request object for HP's Command Center
   */
  @Param("aPayload", "XML Payload")
  @Param("anEventName", "The Event Name")
  @Returns("request object for the event")
  function getRequestObject(aPayload: String, anEventName: String): TDIC_CommandCenterActionRequest {
    _logger.trace("TDIC_ExstreamTransport#getRequestObject(String, String) - Entering")
    var reqXMLinBase64 = gw.util.Base64Util.encode(aPayload.Bytes)
    _logger.debug("TDIC_ExstreamTransport#getRequestObject(String, String) - Payload for ${anEventName} request: " + aPayload)
    var requestToCmd: TDIC_CommandCenterActionRequest
    if (anEventName == "FinalizeExstreamDocument") {
      requestToCmd = new TDIC_CommandCenterActionRequest(null /*aJobDefId*/, FinalizationJobDefName /*aJobDefName*/, "DRIVERFILE" /*aDriverName*/, reqXMLinBase64 /*anEncodedDriverContent*/)
    } else {
      requestToCmd = new TDIC_CommandCenterActionRequest(null /*aJobDefId*/, CCJobDefName /*aJobDefName*/, "DRIVERFILE" /*aDriverName*/, reqXMLinBase64 /*anEncodedDriverContent*/)
    }
    _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Request to Command Center")
    requestToCmd.getXMLModelAttributes().each(\elt -> _logger.debug(elt.asUTFString()))
    _logger.trace("TDIC_ExstreamTransport#getRequestObject(String, String) - Exiting")
    return requestToCmd
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Update the document's metadata after sending to HP Exstream
   */
  @Param("aDocument", "The document to update")
  function updateDocumentMetadata(aDocument: Document, successBooleanvalue:boolean) {
    _logger.trace("TDIC_ExstreamTransport#updateDocumentMetadata(Document) - Entering")
    try{
      aDocument.DeliveryStatus_TDIC = successBooleanvalue?DocDlvryStatusType_TDIC.TC_SENT: DocDlvryStatusType_TDIC.TC_ERROR
      var dms = Plugins.get(IDocumentMetadataSource)
      dms.saveDocument(aDocument)
    }catch(e: Exception){
      _logger.warn("TDIC_ExstreamTransport#updateDocumentMetadata(Document)-${_docInfoAsString} - ${e.Message}")
    }
    _logger.trace("TDIC_ExstreamTransport#updateDocumentMetadata(Document) - Exiting")
  }

  function getTemplateDescriptor(aDocTemplateId: String): IDocumentTemplateDescriptor {
    _logger.trace("TDIC_ExstreamTransport#getTemplateDescriptor(String) - Entering")
    if (aDocTemplateId == null) {
      return null
    } else {
      var dts = Plugins.get(IDocumentTemplateSource)
      var templateDescriptor = dts.getDocumentTemplate(aDocTemplateId, null)
      _logger.debug("TDIC_ExsreamTransport#getTemplateDescriptor(String) - Template Descriptor " + templateDescriptor.TemplateId
          + " retrieved for document with Template Id: " + aDocTemplateId)
      _logger.trace("TDIC_ExstreamTransport#getTemplateDescriptor(String) - Exiting")
      return templateDescriptor
    }
  }

  /**
   * US556
   * 03/27/2015 Shane Murphy
   *
   * This method changes the wait time while retry
   * For each retry time varies.
   */
  function reportError(aMessage: Message, errorMessage: String) {
    _logger.trace("unable to send the payload to exstream. There is something wrong with payload. Admin needs to look into the payload and retry")
    try{
      aMessage.ErrorDescription = errorMessage
    }catch(e: Exception){
      aMessage.ErrorDescription = "Unable to Update ErrorMessage"
      _logger.error("Unable to update ErrorMessage: ${aMessage}",e)
    }
    if (aMessage.MessageRoot != null && aMessage.MessageRoot typeis Document){
        var document = (aMessage.MessageRoot as Document)
        updateDocumentMetadata(document, false)
        EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, document.Event_TDIC + " payload sending to HP Exstream got falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" +
            document.PolicyPeriod.PolicyNumber, "Below is the detail of the exception: \n\n" + errorMessage)
      }
    aMessage.reportError()
  }
}