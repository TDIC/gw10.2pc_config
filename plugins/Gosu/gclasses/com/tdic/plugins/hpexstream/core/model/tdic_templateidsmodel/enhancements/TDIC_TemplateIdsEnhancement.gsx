package com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_TemplateIdsEnhancement : com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.TDIC_TemplateIds {
  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds) : com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.TDIC_TemplateIds {
    return new com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.TDIC_TemplateIds(object)
  }

  public static function create(object : com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds, options : gw.api.gx.GXOptions) : com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.TDIC_TemplateIds {
    return new com.tdic.plugins.hpexstream.core.model.tdic_templateidsmodel.TDIC_TemplateIds(object, options)
  }

}