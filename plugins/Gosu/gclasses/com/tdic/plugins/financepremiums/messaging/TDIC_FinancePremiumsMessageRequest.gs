package com.tdic.plugins.financepremiums.messaging

uses gw.plugin.messaging.MessageRequest

class TDIC_FinancePremiumsMessageRequest implements MessageRequest {

  construct() {
  }

  override function beforeSend(message : Message) : String {
    return message.Payload
  }

  override function shutdown() {
  }

  override function resume() {
  }

  override function suspend() {
  }

  override property set DestinationID(p0 : int) {
  }


  override function afterSend(p0 : Message) {
  }
}