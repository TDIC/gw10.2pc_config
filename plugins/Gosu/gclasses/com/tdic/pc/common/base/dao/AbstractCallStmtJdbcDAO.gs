package com.tdic.pc.common.base.dao

uses org.apache.commons.dbutils.DbUtils
uses org.slf4j.Logger

uses java.sql.CallableStatement
uses java.sql.Connection
uses java.sql.ResultSet

/**
 * JDBC DAO Framework class. This class responsible to provide function to execute sql queries. If you have any
 * any integration which needs to execute store procedure, that integration DAO must extend this class.
 */
public abstract class AbstractCallStmtJdbcDAO extends AbstractJdbcDAO implements IJdbcCallStmtDAO {

  override function  executeStoredProc(aQueryString: String, aLogger: Logger): Object {
    var myMethodName = "executeStoredProc"
    var myConnection: Connection = null
    var myStatement: CallableStatement = null
    var myResultSet: ResultSet = null
    var myResults: Object
    var myClassName = AbstractCallStmtJdbcDAO.Type.Name
    try {
      aLogger.info(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQueryString)
      if (aLogger.DebugEnabled)
        aLogger.debug(myClassName + ": " + myMethodName + ": ", "Getting Database Connection.")
      myConnection = getConnection(aLogger)
      myStatement = myConnection.prepareCall(aQueryString)
      if (aLogger.DebugEnabled)
        aLogger.debug(myClassName + ": " + myMethodName + ": ", "Setting Stored Procedure parameters.")
      setStoredProcParameters(myStatement, aLogger)
      if (aLogger.DebugEnabled)
        aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing Stored Procedure.")
      myResultSet = myStatement.executeQuery()
      if (aLogger.DebugEnabled)
        aLogger.debug(myClassName + ": " + myMethodName + ": ", "Retrieving Result Set Values.")
      myResults = retrieveResultSetValues(myResultSet, aLogger)
    }
    finally {
      DbUtils.closeQuietly(myResultSet)
      DbUtils.closeQuietly(myStatement)
      DbUtils.rollbackAndCloseQuietly(myConnection)
    }
    return myResults
  }
}

