package com.tdic.pc.common.base.dao


/**
 * JDBC DAO Framework interface for all JDBC CRUD operations.
 */
interface IJdbcPrepStmtDAO<T> extends IJdbcDAO<T> {

  function persist(aPersistentObject: T): int

  function persistAll(final persistentObjects: List<T>): int[]

  function update(aTransientObject: T): int

  function markAsProcessed(persistentObjects: List<T>): void

  function delete(aTransientObject: T): int

  function deleteAll(persistentObjects: List<T>): int[]

}

