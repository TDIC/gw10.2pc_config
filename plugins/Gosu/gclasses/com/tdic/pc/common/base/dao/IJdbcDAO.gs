package com.tdic.pc.common.base.dao

uses com.tdic.util.properties.exception.LoadingPropertyFileException
uses com.tdic.util.properties.exception.PropertyNotFoundException

uses javax.security.auth.login.CredentialNotFoundException
uses java.sql.SQLException

/**
 * JDBC DAO Framework interface for retrieving records from database.
 */
interface IJdbcDAO<T> {
  @Throws(SQLException, "SQL Exception occurred")
  @Throws(LoadingPropertyFileException, "SQL Exception occurred")
  @Throws(PropertyNotFoundException, "SQL Exception occurred")
  @Throws(CredentialNotFoundException, "SQL Exception occurred")
  function retrieveAll(): List<T>

  /**
   * Load all of the objects.
   *
   * @return List< T >
   * @throws SQLException the sQL exception
   * @throws LoadingPropertyFileException the loading property file exception
   * @throws PropertyNotFoundException the property not found exception
   * @throws CredentialNotFoundException the credential not found exception
   */
  @Returns("List<T>")
  @Throws(SQLException, "SQL Exception occurred")
  function retrieveAllUnprocessed(): List<T>
}
