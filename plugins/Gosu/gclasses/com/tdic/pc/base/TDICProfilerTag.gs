package com.tdic.pc.base

uses gw.api.profiler.ProfilerTag

class TDICProfilerTag extends ProfilerTag {

  construct(tagName : String){
    super(tagName)
  }

}