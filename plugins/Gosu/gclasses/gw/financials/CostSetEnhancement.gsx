package gw.financials
uses java.util.Set

enhancement CostSetEnhancement<T extends Cost> : Set<T>
{
  reified property get Premiums() : Set<T>
  {
    return this.where( \ c -> not c.IsTaxOrSurcharge).toSet()
  }
  
  reified property get TaxSurcharges() : Set<T>
  {
    return this.where( \ c -> c.IsTaxOrSurcharge).toSet()
  }

  reified property get StandardPremium() : Set<T>
  {
    return this.where( \ c -> c.RateAmountType == TC_STDPREMIUM ).toSet()
  }

  reified property get NonStandardPremium() : Set<T>
  {
    return this.where( \ c -> c.RateAmountType == TC_NONSTDPREMIUM ).toSet()
  }

  reified property get All() : Set<T>
  {
    return this.toSet()
  }
}
