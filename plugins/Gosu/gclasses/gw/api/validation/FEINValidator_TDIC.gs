package gw.api.validation

uses java.util.Map
uses gw.api.locale.DisplayKey

@Export
class FEINValidator_TDIC extends FieldValidatorBase {

  construct() {
  }

  private static property get isNonMigrationMode() : boolean {
    return ScriptParameters.TDIC_DataSource == "policycenter";
  }

  public static property get InputMask() : String {
    var retval : String;
    if (isNonMigrationMode) {
      retval = "##-#######";
    }
    return retval;
  }

  public static property get Regex() : String {
    var retval : String;
    if (isNonMigrationMode) {
      retval = "[0-9]{2}-[0-9]{7}|[0-9]{9}";
    }
    return retval;
  }

  override function validate(fein : String, p1 : Object, parameters : Map<Object, Object>) : IFieldValidationResult {
    var result = new FieldValidationResult();
    if (isNonMigrationMode and fein.matches("[0-9]{2}-[0-9]{7}") == false) {
      result.addError(DisplayKey.get("Validator.FEIN", ""));
    }
    return result;
  }

}
