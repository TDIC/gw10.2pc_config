package gw.api.ui

uses entity.BOPCost
uses gw.pl.currency.MonetaryAmount

/**
 * A wrapper class to help with UI presentation of a {@link BOPCost}
 *
 * @see BOPCost
 * @see CostWrapper
 */
class BOPCostWrapper_TDIC extends CostWrapper {

  construct(order : double, cost : BOPCost, description : String, aTotal : MonetaryAmount, bold : boolean) {
    super(order, description, aTotal, bold)
    Cost = cost
  }

  override property get Mode() : String {
    return Cost == null ? "total" : "BOP"
  }

  override property get Visible() : boolean {
    //do not display equipment breakdown
    if(Cost typeis BOPBuildingCost_TDIC) {
      if(Cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBPPLIM
          or Cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBUILDINGLIM) {
        return false
      }
    }
    //return super.Visible
    return true
  }

}