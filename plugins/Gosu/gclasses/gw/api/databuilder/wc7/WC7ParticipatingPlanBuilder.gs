package gw.api.databuilder.wc7

uses gw.api.databuilder.DataBuilder
uses java.math.BigDecimal

@Export
class WC7ParticipatingPlanBuilder extends DataBuilder<WC7ParticipatingPlan, WC7ParticipatingPlanBuilder> {

  construct() {
    super(WC7ParticipatingPlan)
    withPlanID(TC_1YSTD)
    withRetentionAmount(1)
    withLossConversionFactor(2)
  }
  
  final function withPlanID(id : WC7ParticipatingPlanID) : WC7ParticipatingPlanBuilder {
    set(WC7ParticipatingPlan#PlanID.getPropertyInfo(), id)
    return this
  }

  final function withRetentionAmount(amount : BigDecimal) : WC7ParticipatingPlanBuilder {
    set(WC7ParticipatingPlan#Retention.getPropertyInfo(), amount)
    return this
  }    
  
  final function withLossConversionFactor(factor : BigDecimal) : WC7ParticipatingPlanBuilder {
    set(WC7ParticipatingPlan#LossConversionFactor.getPropertyInfo(), factor)
    return this
  }  
}
