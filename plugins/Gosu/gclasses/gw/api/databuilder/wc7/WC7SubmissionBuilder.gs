package gw.api.databuilder.wc7

uses gw.api.builder.PolicyLocationBuilder
uses java.util.Date
uses gw.api.builder.AccountBuilder
uses gw.api.builder.AccountLocationBuilder
uses gw.api.builder.CoverageBuilder
uses gw.api.builder.SubmissionBuilderBase
uses gw.api.databuilder.wc7.contact.WC7PolicyOwnerOfficerBuilder
uses gw.api.databuilder.wc7.contact.WC7PolicyLaborClientBuilder
uses gw.api.databuilder.wc7.contact.WC7PolicyLaborContractorBuilder
uses gw.api.productmodel.ConditionPattern
uses gw.api.builder.PolicyConditionBuilder
uses gw.api.builder.OwnerOfficerBuilder
uses gw.api.productmodel.ExclusionPattern
uses gw.api.builder.ExclusionBuilder
uses gw.api.databuilder.wc7.contact.WC7LaborContactDetailBuilder
uses gw.api.builder.LaborClientBuilder
uses gw.api.builder.LaborContractorBuilder
uses gw.api.productmodel.CoveragePattern
uses gw.api.util.StateJurisdictionMappingUtil
uses gw.api.builder.AccountContactBuilder
uses gw.api.builder.PersonBuilder
uses gw.api.builder.AddressBuilder
uses java.math.BigDecimal
uses java.lang.IllegalStateException
uses gw.api.databuilder.wc7.contact.WC7BasicClientBuilder
uses gw.api.builder.SecondaryContactBuilder
uses gw.api.databuilder.wc7.contact.WC7BasicClientDetailBuilder
uses java.util.Calendar
uses gw.api.upgrade.PCCoercions

/**
 * A test builder that creates a submission with a workers comp line, a base jurisdiction,
 * and base class code, that is guaranteed to pass validation.
 */
@Export
class WC7SubmissionBuilder extends SubmissionBuilderBase<WC7SubmissionBuilder> {

  var actLocationBuilder : AccountLocationBuilder
  var accountBuilder : AccountBuilder
  var defaultPayroll = 100000
  var _withBenefitsDeductible = true
  var _includeCoveredEmployees = true
  var _coveredEmployeeBuilder : WC7CoveredEmployeeBuilder
  protected var lineBuilder : WC7WorkersCompLineBuilder


  construct() {
    this(true, true, null)
  }
  
  construct(validating : boolean) {
    this(validating, true, null)
  }
  
  construct(validating : boolean, includeCoveredEmployees : boolean) {
    this(validating, includeCoveredEmployees, null)
  }
  
  construct(jurisdiction : Jurisdiction, withBenefitsDeductible : boolean) {
    _withBenefitsDeductible = withBenefitsDeductible
    lineBuilder = new WC7WorkersCompLineBuilder(_withBenefitsDeductible)
    withValidation()
    createSubmission(jurisdiction)
  }

  construct(jurisdictionBuilder : WC7JurisdictionBuilder, includeCoveredEmployees : boolean) {
    lineBuilder = new WC7WorkersCompLineBuilder(_withBenefitsDeductible)
    _includeCoveredEmployees = includeCoveredEmployees
    withValidation()
    createSubmission(jurisdictionBuilder)
  }

  construct(jurisdictionBuilder : WC7JurisdictionBuilder) {
    this(jurisdictionBuilder, true)
  }

  construct(validating : boolean, includeCoveredEmployees : boolean, jurisdiction : Jurisdiction) {
    lineBuilder = new WC7WorkersCompLineBuilder(_withBenefitsDeductible)
    if (validating)
      withValidation()
    _includeCoveredEmployees = includeCoveredEmployees
    createSubmission(jurisdiction)
  }
  
  private function createSubmission(jurisdiction : Jurisdiction) {
    var jur = jurisdiction ?: new WC7SubmissionBuilderHelper().Jurisdiction
    createSubmission(new WC7JurisdictionBuilder(jur, _withBenefitsDeductible))
  }

  private function createSubmission(jurisdictionBuilder : WC7JurisdictionBuilder) {
    var jurisdiction = jurisdictionBuilder.Jurisdiction
    var state = State.get(jurisdiction.Code)
    actLocationBuilder = new AccountLocationBuilder().withState(state)
    _coveredEmployeeBuilder = new WC7CoveredEmployeeBuilder()
        .withClassCode(new WC7SubmissionBuilderHelper().ClassCode)
        .withPayroll(defaultPayroll)
    accountBuilder = new AccountBuilder().withPrimaryLocation(actLocationBuilder)
    withAccount(accountBuilder)
    withProduct( "WC7WorkersComp" )
    withPolicyLine(lineBuilder)
    withBaseState(state)
    withJurisdiction(jurisdictionBuilder)
    withWC7EmpLiabLimitWithStopGap("100,000/100,000", "500,000", StopGap.TC_ALLMONOPOLISTICSTATES)
    withWC7TerrorismRiskInsProgReauthEndt()
    if (_includeCoveredEmployees)
      lineBuilder.withWC7CoveredEmployee(_coveredEmployeeBuilder)
  }

  function withDefaultClassCode(classCode : String) : WC7SubmissionBuilder {
    _coveredEmployeeBuilder.withClassCode(classCode)
    return this
  }

  function withDefaultClassCodeAndBasisAmount(classCode : String, amount : int) : WC7SubmissionBuilder {
    _coveredEmployeeBuilder.withClassCode(classCode)
    _coveredEmployeeBuilder.withBasisAmount(amount)
    return this
  }

  function withDefaultClassCodeAndBasisAmount(classCode : WC7ClassCode, amount : int) : WC7SubmissionBuilder {
    _coveredEmployeeBuilder.withClassCode(classCode)
    _coveredEmployeeBuilder.withBasisAmount(amount)
    return this
  }

  function withDefaultClassCodeAndIfAnyBasis(classCode : String) : WC7SubmissionBuilder {
    _coveredEmployeeBuilder.withClassCode(classCode)
    _coveredEmployeeBuilder.asIfAny()
    return this
  }

  function withEmployeeLeasingPlan(plan : WC7EmployeeLeasingPlanBuilder) : WC7SubmissionBuilder {
    lineBuilder.withEmployeeLeasingPlan(plan)
    return this
  }
  
  function withEmployeeLeasingPlanWith(
      professionalEmployeeType : WC7ProfessionalEmployeeType,
      policyType : WC7EmployeeLeasingPolicyType = null) : WC7SubmissionBuilder {
    return withEmployeeLeasingPlan(new WC7EmployeeLeasingPlanBuilder()
        .withProfessionalEmployeeType(professionalEmployeeType)
        .withPolicyType(policyType))
  }

  function withLineCov(lineCovPattern : CoveragePattern) : WC7SubmissionBuilder {
    lineBuilder.withCoverage(new CoverageBuilder(entity.WC7WorkersCompCov).withPattern(lineCovPattern))
    return this 
  }
  
  /**
   * Set's the 'WC7WorkersCompEmpLiabInsurancePolicyACov' on the line with the given cov-term values.
   * @param limitPerAccidentPerEmpDisease e.g. "100,000/100,000"
   * @param diseaseLimit e.g. 5,000,000
   * @param stopGapVal the stop gap value
   */
  function withWorkersCompCov(limitPerAccidentPerEmpDisease : String, diseaseLimit : String, stopGapVal : typekey.StopGap) : WC7SubmissionBuilder {
    var covPattern : CoveragePattern = PCCoercions.makeProductModel<CoveragePattern>("WC7WorkersCompEmpLiabInsurancePolicyACov")
    lineBuilder.withCoverage(
      new CoverageBuilder(entity.WC7WorkersCompCov)
      .withPattern(covPattern)
                           
      .withPackageCovTerm("WC7EmpLiabLimit", limitPerAccidentPerEmpDisease)
      .withOptionCovTerm("WC7EmpLiabPolicyLimit", diseaseLimit)
      .withTypekeyCovTerm("WC7StopGap", stopGapVal))
    return this 
  }
  
  function withFELACoverage(limitPerAccident : String, diseaseLimit : String, programType : WC7CovProgramType) : WC7SubmissionBuilder {
    var covPattern : CoveragePattern = PCCoercions.makeProductModel<CoveragePattern>("WC7FederalEmployersLiabilityActACov")
    lineBuilder.withCoverage(
      new CoverageBuilder(entity.WC7WorkersCompCov)
      .withPattern(covPattern)
      .withOptionCovTerm("WC7FedEmpLiabLimit", limitPerAccident)
      .withOptionCovTerm("WC7FedEmpLiabAggLimit", diseaseLimit)
      .withTypekeyCovTerm("WC7FedEmpLiabProgram", programType))
    return this 
  }

  function withFELACoverage(limitPerAccident : String, diseaseLimit : String, programType : WC7CovProgramType, lawType : WC7GoverningLaw) : WC7SubmissionBuilder {
    var covPattern : CoveragePattern = PCCoercions.makeProductModel<CoveragePattern>("WC7FederalEmployersLiabilityActACov")
    lineBuilder.withCoverage(
        new CoverageBuilder(entity.WC7WorkersCompCov)
            .withPattern(covPattern)
            .withOptionCovTerm("WC7FedEmpLiabLimit", limitPerAccident)
            .withOptionCovTerm("WC7FedEmpLiabAggLimit", diseaseLimit)
            .withTypekeyCovTerm("WC7FedEmpLiabProgram", programType)
            .withTypekeyCovTerm("WC7FedEmpLiabLaw", lawType))
    return this
  }

  function withMaritimeCoverageAndManualPremium(limitPerAccident : String, diseaseLimit : String, programType : WC7CovProgramType, maritimeManualPremium : BigDecimal) : WC7SubmissionBuilder {
    var covPattern : CoveragePattern = PCCoercions.makeProductModel<CoveragePattern>("WC7MaritimeACov")
    lineBuilder.withCoverage(
      new CoverageBuilder(entity.WC7WorkersCompCov)
      .withPattern(covPattern)
      .withOptionCovTerm("WC7MaritimeLimit", limitPerAccident)
      .withOptionCovTerm("WC7MaritimeAggLimit", diseaseLimit)
      .withTypekeyCovTerm("WC7MaritimeProgram", programType)
      .withDirectTerm("WC7MaritimeManualPremium", maritimeManualPremium))
    return this 
  }
  
  function withMaritimeCoverage(limitPerAccident : String, diseaseLimit : String, programType : WC7CovProgramType) : WC7SubmissionBuilder {
    var covPattern : CoveragePattern = PCCoercions.makeProductModel<CoveragePattern>("WC7MaritimeACov")
    lineBuilder.withCoverage(
      new CoverageBuilder(entity.WC7WorkersCompCov)
      .withPattern(covPattern)
      .withOptionCovTerm("WC7MaritimeLimit", limitPerAccident)
      .withOptionCovTerm("WC7MaritimeAggLimit", diseaseLimit)
      .withTypekeyCovTerm("WC7MaritimeProgram", programType))
    return this 
  }

  function withLineCond(lineCondPattern : ConditionPattern) : WC7SubmissionBuilder {
    lineBuilder.withCondition(new PolicyConditionBuilder(entity.WC7WorkersCompCond).withPattern(lineCondPattern))
    return this 
  }

  function withLineCond(lineCond : PolicyConditionBuilder) : WC7SubmissionBuilder {
    lineBuilder.withCondition(lineCond)
    return this
  }
  
  function withLineExcl(lineExclPattern : ExclusionPattern) : WC7SubmissionBuilder {
    lineBuilder.withExclusion(new ExclusionBuilder(entity.WC7WorkersCompExcl).withPattern(lineExclPattern))
    return this 
  }

  function withLineExcl(lineExcl : ExclusionBuilder) : WC7SubmissionBuilder {
    lineBuilder.withExclusion(lineExcl)
    return this
  }

  function withWC7CoveredEmployee(coveredEmployeeBuilder : WC7CoveredEmployeeBuilder) : WC7SubmissionBuilder{
    lineBuilder.withWC7CoveredEmployee(coveredEmployeeBuilder)
    return this
  }

  function withWC7FedCoveredEmployee(fedCoveredEmployeeBuilder : WC7FedCoveredEmployeeBuilder) : WC7SubmissionBuilder{
    lineBuilder.withWC7FedCoveredEmployee(fedCoveredEmployeeBuilder)
    return this
  }

  function withWC7MaritimeCoveredEmployee(marCoveredEmployeeBuilder : WC7MaritimeCoveredEmployeeBuilder) : WC7SubmissionBuilder{
    lineBuilder.withWC7MaritimeCoveredEmployee(marCoveredEmployeeBuilder)
    return this
  }
  
  function withWC7RetrospectiveRatingPlan(retrospectiveRatingPlanBuilder : WC7RetrospectiveRatingPlanBuilder) : WC7SubmissionBuilder {
    lineBuilder.withRRP(retrospectiveRatingPlanBuilder)
    return this
  }
  
  function withoutWC7RetrospectiveRatingPlan() : WC7SubmissionBuilder {
    return withWC7RetrospectiveRatingPlan(null)
  }

  function withWC7RetrospectiveRatingPlan() : WC7SubmissionBuilder {
    if (effectiveDate == null) 
      throw new IllegalStateException("Must set policy effective date before adding default retrospective rating plan")
    var firstDate = firstDayOfNextMonthAfter(effectiveDate.addYears(1))
    var lastDate = firstDate.addMonths(1)
    var letterBuilder = new WC7RetroRatingLetterOfCreditBuilder()
      .withValidFrom(firstDate.addDays(-1))
      .withValidTo(lastDate.addDays(1))      
    return withWC7RetrospectiveRatingPlan(new WC7RetrospectiveRatingPlanBuilder()
      .withFirstComputationDate(firstDate)
      .withLastComputationDate(lastDate)
      .withComputationInterval(1)
      .withLetterOfCredit(letterBuilder)
      .withDefaultLetterOfCreditAndStateMultiplier())
  }

  private function firstDayOfNextMonthAfter(date : Date) : Date {
    var result = date.addMonths(1).FirstDayOfMonth
    return result
  }

  function withWC7ManuscriptOption(manuscriptOptionBuider : WC7ManuscriptOptionBuilder) : WC7SubmissionBuilder {
    lineBuilder.withWC7ManuscriptOption(manuscriptOptionBuider)
    return this
  }
  
  function withAircraftSeat(seatBuilder : WC7AircraftSeatBuilder) : WC7SubmissionBuilder {
    lineBuilder.withAircraftSeat(seatBuilder)
    return this
  }

  function withWC7WaiverOfSubro(waiverBuilder : WC7WaiverOfSubroBuilder) : WC7SubmissionBuilder {
    lineBuilder.withWaiverOfSubro(waiverBuilder)
    return this
  }

  function withAdditionalWCCoveredEmployee(additionalEmployeeBuilder : WC7CoveredEmployeeBuilder) : WC7SubmissionBuilder{
    lineBuilder.withWC7CoveredEmployee(additionalEmployeeBuilder)
    return this
  }

  function withLocation(accountLocation: AccountLocation): WC7SubmissionBuilder {
    var policyLocBuilder = new PolicyLocationBuilder().withAccountLocation(accountLocation)
    withPolicyLocation(policyLocBuilder)
    return this
  }

  function withLocation(state: State): WC7SubmissionBuilder {
    var alb = new AccountLocationBuilder().withState( state )
    accountBuilder.withAccountLocation( alb )
    var policyLocBuilder = new PolicyLocationBuilder().withAccountLocation(alb)
    withPolicyLocation(policyLocBuilder)
    return this
  }

  final function withWCFedEmpLiabCov() : WC7SubmissionBuilder {
    lineBuilder.withCoverage( new CoverageBuilder(WCFedEmpLiabCov).withPatternCode("WCFedEmpLiabCov") )
    return this
  }

  final function withWC7EmpLiabLimit(packageDescription : String, optionValue : String) : WC7SubmissionBuilder {
    lineBuilder.withCoverage(new CoverageBuilder(WC7WorkersCompCov)
                                         .withPatternCode("WC7WorkersCompEmpLiabInsurancePolicyACov")
                                         .withPackageCovTerm("WC7EmpLiabLimit", packageDescription)
                                         .withOptionCovTerm("WC7EmpLiabPolicyLimit", optionValue))
    return this
  } 

  final function withWC7EmpLiabLimitWithStopGap(packageDescription : String, optionValue : String, stopGapValue : StopGap) : WC7SubmissionBuilder {
    lineBuilder.withWC7EmpLiabLimitWithStopGap(packageDescription, optionValue, stopGapValue)
    return this
  }

  final function withWC7TerrorismRiskInsProgReauthEndt() : WC7SubmissionBuilder {
    lineBuilder.withWC7TerrorismRiskInsProgReauthEndt()
    return this
  }

  function withPayroll( payroll : int) : WC7SubmissionBuilder{
    defaultPayroll = payroll
    return this
  }
  
  /**
   * Set the account to have a primary location at Jurisdiction {@link Jurisdiction}
   */
  function withPrimaryLocationJurisdiction(juris : Jurisdiction) : WC7SubmissionBuilder {
    var aState = StateJurisdictionMappingUtil.getStateMappingForJurisdiction(juris)
    return withAccount(new AccountBuilder().withPrimaryLocation(new AccountLocationBuilder().withState(aState))).withBaseState(aState)
  } 
  
  final function withJurisdiction(jurisdictionBuilder : WC7JurisdictionBuilder) : WC7SubmissionBuilder {
    lineBuilder.withJurisdiction( jurisdictionBuilder )
    return this
  }

  function withAnniversaryDate(state: Jurisdiction, anniversaryDate: Date): WC7SubmissionBuilder {
    lineBuilder.withJurisdiction(new WC7JurisdictionBuilder(state).withAnniversaryDate(anniversaryDate))
    return this
  }

  function withAnniversaryDate(state: Jurisdiction, anniversaryDate: Date, withBenefitsDeductible : boolean): WC7SubmissionBuilder {
    lineBuilder.withJurisdiction(new WC7JurisdictionBuilder(state, withBenefitsDeductible).withAnniversaryDate(anniversaryDate))
    return this
  }

  function withParticipatingPlan(participatingPlanBuilder : WC7ParticipatingPlanBuilder) : WC7SubmissionBuilder {
    lineBuilder.withParticipatingPlan(participatingPlanBuilder)
    return this
  }
  
  //------------------------------------------------------------------ Aircraft Premium Endorsement
  
  function withAircraftPremiumEndorsementCondition()  : WC7SubmissionBuilder {
    var aircraftConditionPattern : ConditionPattern = PCCoercions.makeProductModel<ConditionPattern>("WC7AircraftPremiumEndorsementCond")
    withLineCond(aircraftConditionPattern)
    return this
  }
  
  //----------------------------------------------------------------- Owner Officer

  function withOwnerOfficer(ownerOfficerBuilder : WC7PolicyOwnerOfficerBuilder) : WC7SubmissionBuilder {
    return withOwnerOfficerHelper(ownerOfficerBuilder)
  }


  function withOwnerOfficerAlsoOnAccount(wc7OwnerOfficerBuilder : WC7PolicyOwnerOfficerBuilder, accountContactRole : AccountContactRole) : WC7SubmissionBuilder {
    lineBuilder.withWC7PolicyOwnerOfficer(wc7OwnerOfficerBuilder)
    this.withPolicyContactRole(wc7OwnerOfficerBuilder.withAccountContactRole(accountContactRole))
    return this
  }

  /**
   * Add an owner officer and also include the owner officer condition (WC7SoleProprietorsPartnersOfficersAndOthersCovCond)
   * {@link withOwnerOfficer(WC7PolicyOwnerOfficerBuilder)}
   */
  function withIncludedOwnerOfficer( ownerOfficerBuilder : WC7PolicyOwnerOfficerBuilder) : WC7SubmissionBuilder {
    var ownerOfficerConditionPattern : ConditionPattern = PCCoercions.makeProductModel<ConditionPattern>("WC7SoleProprietorsPartnersOfficersAndOthersCovCond")
    withLineCond(ownerOfficerConditionPattern)
    return withOwnerOfficerHelper(ownerOfficerBuilder.isIncluded())
  }

  /**
   * Add an owner officer and also excludes the owner officer exc;isopm (WC7PartnersOfficersAndOthersExclEndorsementExcl)
   */
  function withExcludedOwnerOfficer( ownerOfficerBuilder : WC7PolicyOwnerOfficerBuilder) : WC7SubmissionBuilder {
    var ownerOfficerExclusionPattern : ExclusionPattern = PCCoercions.makeProductModel<ExclusionPattern>("WC7PartnersOfficersAndOthersExclEndorsementExcl")
    var exclBuilder = new ExclusionBuilder(WC7WorkersCompExcl).withPattern(ownerOfficerExclusionPattern)
    lineBuilder.withExclusion(exclBuilder)
    withLineExcl(ownerOfficerExclusionPattern)
    return withOwnerOfficerHelper(ownerOfficerBuilder.isExcluded())
  }

  function withBasicClientDetailFor(conditionPattern : ConditionPattern) : WC7SubmissionBuilder {
    withLineCond(conditionPattern)
    var accountContactRoleBuilder = new SecondaryContactBuilder()
    accountBuilder.withAccountContactForRole(accountContactRoleBuilder)
    var clientBuilder = new WC7BasicClientBuilder()
        .withAccountContactRole(accountContactRoleBuilder)
        .withDetail(new WC7BasicClientDetailBuilder().forCondition(conditionPattern))
    lineBuilder.withWC7BasicClient(clientBuilder)
    this.withPolicyContactRole(clientBuilder)
    return this
  }


  //----------------------------------------------------------------- Labor Client

  function withLaborClient(laborClientBuilder : WC7PolicyLaborClientBuilder) : WC7SubmissionBuilder {
    lineBuilder.withWC7PolicyLaborClient(laborClientBuilder)
    this.withPolicyContactRole(laborClientBuilder)
    return this
  }

  /**
   * Add a new labor client with a single 'included' detail and add the condition (WC7EmployeeLeasingClientEndorsementCond)
   */
  function withIncludedLaborClientDetail(laborClientBuilder : WC7PolicyLaborClientBuilder) : WC7SubmissionBuilder {
    withEmployeeLeasingPlanWith(TC_PEO)
    return withIncludedLaborClientDetail(laborClientBuilder, PCCoercions.makeProductModel<ConditionPattern>("WC7EmployeeLeasingClientEndorsementCond"))
  }

  function withIncludedLaborClientDetail(
      laborClientBuilder : WC7PolicyLaborClientBuilder,
      lineCondition : ConditionPattern) : WC7SubmissionBuilder {
    withLineCond(lineCondition)
    var acctLaborClientBuilder = new LaborClientBuilder()
    accountBuilder.withAccountContactForRole(acctLaborClientBuilder)
    withLaborClient(laborClientBuilder
      .withAccountContactRole(acctLaborClientBuilder)
      .withDetail(new WC7LaborContactDetailBuilder().forCondition(lineCondition)))
    return this
  }

  function withIncludedLaborClientDetailFor(
      laborClientBuilder : WC7PolicyLaborClientBuilder,
      lineCondition : ConditionPattern,
      jurisidction : Jurisdiction ) : WC7SubmissionBuilder {
    withLineCond(lineCondition)
    var acctLaborClientBuilder = new LaborClientBuilder()
    accountBuilder.withAccountContactForRole(acctLaborClientBuilder)
    withLaborClient(laborClientBuilder
        .withAccountContactRole(acctLaborClientBuilder)
        .withDetail(new WC7LaborContactDetailBuilder().forCondition(lineCondition).withJurisdiction(jurisidction)))
    return this
  }

  function withIncludedLaborClientDetailFor(jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    return this.withIncludedLaborClientDetail(new WC7PolicyLaborClientBuilder()
      .withDetail(new WC7LaborContactDetailBuilder().withJurisdiction(jurisdiction)))
  }
  
  function withIncludedLaborClientDetailForCondition(lineCondition : ConditionPattern) : WC7SubmissionBuilder {
    return this.withIncludedLaborClientDetail(new WC7PolicyLaborClientBuilder(), lineCondition)
  }

  /**
   * Add a new labor client with a single 'excluded' detail and add the exclusion (WC7EmployeeLeasingClientExclEndorsementExcl)
   */
  function withExcludedLaborClientDetail(
      laborClientBuilder : WC7PolicyLaborClientBuilder,
      leasedClientExclusionPattern : ExclusionPattern,
      jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    lineBuilder.withExclusion(new ExclusionBuilder(WC7WorkersCompExcl).withPattern(leasedClientExclusionPattern))
    
    var acctLaborClientBuilder = new LaborClientBuilder()
    accountBuilder.withAccountContactForRole(acctLaborClientBuilder)
    withLaborClient(laborClientBuilder
      .withAccountContactRole(acctLaborClientBuilder)
      .withDetail(new WC7LaborContactDetailBuilder()
          .withJurisdiction(jurisdiction)
          .withInclusion(TC_EXCL)
          .forExclusion(leasedClientExclusionPattern)))
    return this    
  }

  function withExcludedLaborClientDetail(laborClientBuilder : WC7PolicyLaborClientBuilder) : WC7SubmissionBuilder {
    return withExcludedLaborClientDetail(laborClientBuilder, PCCoercions.makeProductModel<ExclusionPattern>("WC7EmployeeLeasingClientExclEndorsementExcl"), null)
  }

  /**
   * Add a new labor client with a single 'excluded' detail and add the exclusion (WC7EmployeeLeasingClientExclEndorsementExcl)
   */
  function withExcludedLaborClientDetailForState(laborClientBuilder : WC7PolicyLaborClientBuilder, juris : typekey.Jurisdiction) : WC7SubmissionBuilder {
    var leasedClientExclusionPattern : ExclusionPattern = PCCoercions.makeProductModel<ExclusionPattern>("WC7EmployeeLeasingClientExclEndorsementExcl")
    lineBuilder.withExclusion(new ExclusionBuilder(WC7WorkersCompExcl).withPattern(leasedClientExclusionPattern))
      .withEmployeeLeasingPlan(new WC7EmployeeLeasingPlanBuilder()
        .withProfessionalEmployeeType(WC7ProfessionalEmployeeType.TC_PEO))
    var acctLaborClientBuilder = new LaborClientBuilder()
    accountBuilder.withAccountContactForRole(acctLaborClientBuilder)
    withLaborClient(laborClientBuilder
      .withAccountContactRole(acctLaborClientBuilder)
      .withDetail(new WC7LaborContactDetailBuilder().withInclusion(Inclusion.TC_EXCL).withJurisdiction(juris)))
    return this    
  }
  
  function withExcludedLaborClientDetailForState(jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    return withExcludedLaborClientDetailForState(new WC7PolicyLaborClientBuilder(), jurisdiction)
  }

  function withExcludedLaborClientDetailForExclusion(lineExclusion : ExclusionPattern, jurisdiction : Jurisdiction)
      : WC7SubmissionBuilder {
    return this.withExcludedLaborClientDetail(new WC7PolicyLaborClientBuilder(), lineExclusion, jurisdiction)
  }

  function withExcludedLaborClientDetailForExclusion(lineExclusion : ExclusionPattern) : WC7SubmissionBuilder {
    return this.withExcludedLaborClientDetail(new WC7PolicyLaborClientBuilder(), lineExclusion, null)
  }

  //----------------------------------------------------------------- Labor Contractor
  
  function withMultipleCoordinatedPolicy(coordinatedPolicyBuilder : WC7CoordinatedPolicyBuilder) : WC7SubmissionBuilder {
    return withMultipleCoordinatedPolicy(coordinatedPolicyBuilder, PCCoercions.makeProductModel<ConditionPattern>("WC7MultipleCoordinatedPolicyEndorsementCond") )
  }

  function withMultipleCoordinatedPolicy(coordinatedPolicyBuilder : WC7CoordinatedPolicyBuilder,  conditionPattern : ConditionPattern) : WC7SubmissionBuilder {
    withIncludedLaborContractorDetail(new WC7PolicyLaborContractorBuilder())
    lineBuilder.withWC7CoordinatedPolicy(coordinatedPolicyBuilder)
    lineBuilder.withCondition(new PolicyConditionBuilder(WC7WorkersCompCond).withPattern(conditionPattern))
      .withEmployeeLeasingPlan(new WC7EmployeeLeasingPlanBuilder()
        .withProfessionalEmployeeType(WC7ProfessionalEmployeeType.TC_CLIENT)
        .withPolicyType(WC7EmployeeLeasingPolicyType.TC_MCP))
    return this
  }

  function withMultipleCoordinatedPolicyForState(jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    return withMultipleCoordinatedPolicy(new WC7CoordinatedPolicyBuilder().withState(jurisdiction))
  }

  
  function withLaborContractor(laborContractorBuilder : WC7PolicyLaborContractorBuilder) : WC7SubmissionBuilder {
    lineBuilder.withWC7PolicyLaborContractor(laborContractorBuilder)
    this.withPolicyContactRole(laborContractorBuilder)
    return this
  }
  
  /**
   * Add a new labor contractor with a single 'included' detail and add the condition (WC7LaborContractorEndorsementACond)
   */
  function withIncludedLaborContractorDetail(laborContractorBuilder : WC7PolicyLaborContractorBuilder) : WC7SubmissionBuilder {
    withEmployeeLeasingPlanWith(TC_CLIENT)
    return withIncludedLaborContractorDetail(laborContractorBuilder, PCCoercions.makeProductModel<ConditionPattern>("WC7LaborContractorEndorsementACond"))
  }

  function withIncludedLaborContractorDetail(
      laborContractorBuilder : WC7PolicyLaborContractorBuilder,
      lineCondition : ConditionPattern,
      jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
        
    withLineCond(lineCondition)
    var acctLaborContractorBuilder = new LaborContractorBuilder()
    accountBuilder.withAccountContactForRole(acctLaborContractorBuilder)
    withLaborContractor(laborContractorBuilder
      .withAccountContactRole(acctLaborContractorBuilder)
      .withDetail(new WC7LaborContactDetailBuilder()
          .withJurisdiction(jurisdiction)
          .forCondition(lineCondition)))
    return this
  }

  function withIncludedLaborContractorDetail(
      laborContractorBuilder : WC7PolicyLaborContractorBuilder,
      lineCondition : ConditionPattern) : WC7SubmissionBuilder {
    return withIncludedLaborContractorDetail(laborContractorBuilder, lineCondition, null)
  }

  function withIncludedLaborContractorDetailFor(jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    return this.withIncludedLaborContractorDetail(new WC7PolicyLaborContractorBuilder()
      .withDetail(new WC7LaborContactDetailBuilder().withJurisdiction(jurisdiction)))
  }

  function withIncludedLaborContractorDetailForCondition(lineCondition : ConditionPattern, jurisdiction : Jurisdiction)
      : WC7SubmissionBuilder {
    return this.withIncludedLaborContractorDetail(new WC7PolicyLaborContractorBuilder(), lineCondition, jurisdiction)
  }
  
  /**
   * Add a new labor contractor with a single 'included' detail and add the condition (WC7LaborContractorEndorsementACond)
   */
  function withIncludedLaborContractorDetail(laborContractorBuilder : WC7PolicyLaborContractorBuilder, laborContractor : LaborContractorBuilder) : WC7SubmissionBuilder {
    var leasedContractorConditionPattern : ConditionPattern = PCCoercions.makeProductModel<ConditionPattern>("WC7LaborContractorEndorsementACond")
    lineBuilder.withCondition(new PolicyConditionBuilder(WC7WorkersCompCond).withPattern(leasedContractorConditionPattern))
    
    withLaborContractor(laborContractorBuilder
      .withAccountContactRole(laborContractor)
      .withDetail(new WC7LaborContactDetailBuilder().withInclusion(Inclusion.TC_INCL)))
    return this
  }

  /**
   * Add a new labor contractor with a single 'excluded' detail and add the exclusion (WC7LaborContractorExclEndorsementExcl)
   */
  function withExcludedLaborContractorDetail(
      laborContractorBuilder : WC7PolicyLaborContractorBuilder,
      leasedContractorExclusionPattern : ExclusionPattern,
      jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    lineBuilder.withExclusion(new ExclusionBuilder(WC7WorkersCompExcl).withPattern(leasedContractorExclusionPattern))
    
    var acctLaborContractorBuilder = new LaborContractorBuilder()
    accountBuilder.withAccountContactForRole(acctLaborContractorBuilder)
    withLaborContractor(laborContractorBuilder
      .withAccountContactRole(acctLaborContractorBuilder)
      .withDetail(new WC7LaborContactDetailBuilder()
          .withJurisdiction(jurisdiction)
          .withInclusion(TC_EXCL)
          .forExclusion(leasedContractorExclusionPattern)))
    return this    
  }

  function withExcludedLaborContractorDetail(
      laborContractorBuilder : WC7PolicyLaborContractorBuilder,
      leasedContractorExclusionPattern : ExclusionPattern) : WC7SubmissionBuilder {
    withEmployeeLeasingPlanWith(TC_CLIENT)
    return withExcludedLaborContractorDetail(laborContractorBuilder, leasedContractorExclusionPattern, null)
  }

  function withExcludedLaborContractorDetail(laborContractorBuilder : WC7PolicyLaborContractorBuilder) : WC7SubmissionBuilder {
    withEmployeeLeasingPlanWith(TC_CLIENT)
    return withExcludedLaborContractorDetail(laborContractorBuilder, PCCoercions.makeProductModel<ExclusionPattern>("WC7LaborContractorExclEndorsementExcl"))
  }

    /**
   * Add a new labor contractor with a single 'excluded' detail and add the exclusion (WC7LaborContractorExclEndorsementExcl)
   */
  function withExcludedLaborContractorDetailForState(laborContractorBuilder : WC7PolicyLaborContractorBuilder, juris : typekey.Jurisdiction) : WC7SubmissionBuilder {
    var leasedContractorExclusionPattern : ExclusionPattern = PCCoercions.makeProductModel<ExclusionPattern>("WC7LaborContractorExclEndorsementExcl")
    lineBuilder.withExclusion(new ExclusionBuilder(WC7WorkersCompExcl).withPattern(leasedContractorExclusionPattern))
    lineBuilder.withEmployeeLeasingPlan(new WC7EmployeeLeasingPlanBuilder()
      .withProfessionalEmployeeType(WC7ProfessionalEmployeeType.TC_CLIENT))
    var acctLaborContractorBuilder = new LaborContractorBuilder()
    accountBuilder.withAccountContact(new AccountContactBuilder().withRole(acctLaborContractorBuilder)
      .withContact(new PersonBuilder().withAddress(new AddressBuilder().withState(StateJurisdictionMappingUtil.getStateMappingForJurisdiction(juris)))))
    withLaborContractor(laborContractorBuilder
      .withAccountContactRole(acctLaborContractorBuilder)
      .withDetail(new WC7LaborContactDetailBuilder().withInclusion(Inclusion.TC_EXCL).withJurisdiction(juris)))
    return this
  }

  function withExcludedLaborContractorDetailForState(jurisdiction : Jurisdiction) : WC7SubmissionBuilder {
    return withExcludedLaborContractorDetailForState(new WC7PolicyLaborContractorBuilder(), jurisdiction)
  }
  
  function withExcludedLaborContractorDetailForExclusion(
      employeeType : typekey.WC7ProfessionalEmployeeType,
      lineExclusion : ExclusionPattern,
      jurisdiction : Jurisdiction = null) : WC7SubmissionBuilder {
      withEmployeeLeasingPlanWith(employeeType)
    return this.withExcludedLaborContractorDetail(new WC7PolicyLaborContractorBuilder(), lineExclusion, jurisdiction)
  }  
  
  //----------------------------------------------------------------- WaiverOfSubro

  /**
   * Add a waiver of subrogation to the line.  This will also include the waiver condition (WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond)
   */
  function withWaiver(waiverBuilder : WC7WaiverOfSubroBuilder) : WC7SubmissionBuilder {    
    var waiverConditionPattern : ConditionPattern = PCCoercions.makeProductModel<ConditionPattern>("WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond")
    lineBuilder.withCondition(new PolicyConditionBuilder(WC7WorkersCompCond).withPattern(waiverConditionPattern))     
    lineBuilder.withWaiverOfSubro(waiverBuilder)
    return this
  }
  
  //----------------------------------------------------------------- Excluded Workplace

  /**
   * Add a waiver of subrogation to the line.  This will also include the waiver condition (WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond)
   */
  function withExcludedWorkplace(excludedWorkplace : WC7ExcludedWorkplaceBuilder) : WC7SubmissionBuilder {    
    var excludedWorkplaceExclusion : ExclusionPattern = PCCoercions.makeProductModel<ExclusionPattern>("WC7DesignatedWorkplacesExclEndorsementExcl")
    lineBuilder.withExclusion(new ExclusionBuilder(WC7WorkersCompExcl).withPattern(excludedWorkplaceExclusion))
    lineBuilder.withExcludedWorkplace(excludedWorkplace)
    return this
  }

  //----------------------------------------------------------------- Supplement Disease

  function withSupplementaryDiseaseExposure(suppDiseaseExposureBuilder : WC7SupplDiseaseExposureBuilder) : WC7SubmissionBuilder {    
    lineBuilder.withWC7SupplDiseaseExposure(suppDiseaseExposureBuilder)
    return this
  }

   //----------------------------------------------------------------- Atomic Energy

  function withAtomicEnergyExposure(atomicEnergyExposureBuilder : WC7AtomicEnergyExposureBuilder) : WC7SubmissionBuilder {    
    lineBuilder.withWC7AtomicEnergyExposure(atomicEnergyExposureBuilder)
    return this
  }

  //--------------------------------- Validation
  
  function withoutValidation() : WC7SubmissionBuilder {
    skipValidation = true
    return this
  }

  final function withVoluntaryCompCond() : WC7SubmissionBuilder {
    return withLineCond(PCCoercions.makeProductModel<ConditionPattern>("WC7VoluntaryCompensationAndEmployersLiabilityCovCond"))
  }
    
  //----------------------------------------------------------------- private helper methods
  
  private function withOwnerOfficerHelper(ownerOfficerBuilder : WC7PolicyOwnerOfficerBuilder) : WC7SubmissionBuilder {
    var ooBuilder = new OwnerOfficerBuilder()
    accountBuilder.withAccountContactForRole(ooBuilder)
    lineBuilder.withWC7PolicyOwnerOfficer(ownerOfficerBuilder)
    this.withPolicyContactRole(ownerOfficerBuilder.withAccountContactRole(ooBuilder))
    return this
  }

  public function getLineBuilder() :  WC7WorkersCompLineBuilder {
    return lineBuilder
  }
}
