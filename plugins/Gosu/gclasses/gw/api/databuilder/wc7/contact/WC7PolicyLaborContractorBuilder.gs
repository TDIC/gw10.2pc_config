package gw.api.databuilder.wc7.contact

@Export
class WC7PolicyLaborContractorBuilder extends WC7PolicyContactRoleBuilder <WC7PolicyLaborContractor, WC7PolicyLaborContractorBuilder> {

  construct() {
    super(WC7PolicyLaborContractor)
  }
  
}
