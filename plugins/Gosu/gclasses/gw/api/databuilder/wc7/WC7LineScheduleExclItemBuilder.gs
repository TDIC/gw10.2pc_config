package gw.api.databuilder.wc7
uses gw.api.databuilder.DataBuilder
uses gw.api.databuilder.BuilderContext

class WC7LineScheduleExclItemBuilder extends DataBuilder<WC7LineScheduleExclItem, WC7LineScheduleExclItemBuilder> {

  construct() {
    super(WC7LineScheduleExclItem)
  }
  
  protected override function createBean(context : BuilderContext) : WC7LineScheduleExclItem{
    var cov = context.ParentBean as WC7LineScheduleExcl
    var scheduledItem = new WC7LineScheduleExclItem(cov.Branch)
    cov.addToWC7LineScheduleExclItems(scheduledItem)
    return scheduledItem
  }

  function withString1(string1: String) : WC7LineScheduleExclItemBuilder {
    set(WC7LineScheduleExclItem#StringCol1.getPropertyInfo(), string1)
    return this
  }

  function withString2(string2 : String) : WC7LineScheduleExclItemBuilder {
    set(WC7LineScheduleExclItem#StringCol2, string2)
    return this
  }

  function withString3(string3 : String) : WC7LineScheduleExclItemBuilder {
    set(WC7LineScheduleExclItem#StringCol3, string3)
    return this
  }

  function withInt1(int1 : int) : WC7LineScheduleExclItemBuilder {
    set(WC7LineScheduleExclItem#IntCol1, int1)
    return this
  }
  
  function withTypeKey1(typeKeyCol1: String) : WC7LineScheduleExclItemBuilder {
    set(WC7LineScheduleExclItem#TypeKeyCol1.getPropertyInfo(), typeKeyCol1)
    return this
  }
}
