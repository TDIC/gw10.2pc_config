package gw.api.databuilder.wc7

uses gw.api.databuilder.DataBuilder
uses gw.api.databuilder.BuilderContext
uses gw.api.productmodel.ConditionPattern
uses gw.api.upgrade.PCCoercions

class WC7CoordinatedPolicyBuilder extends DataBuilder<WC7CoordinatedPolicy, WC7CoordinatedPolicyBuilder> {
  var _conditionPattern: ConditionPattern

  construct() {
    this(PCCoercions.makeProductModel<ConditionPattern>("WC7MultipleCoordinatedPolicyEndorsementCond"))
  }
  
  construct(conditionPattern: ConditionPattern) {
    super(WC7CoordinatedPolicy)
    _conditionPattern = conditionPattern ?: PCCoercions.makeProductModel<ConditionPattern>("WC7MultipleCoordinatedPolicyEndorsementCond")
  }

  protected override function createBean(context : BuilderContext) : WC7CoordinatedPolicy {
    var line = context.getParentBean() as entity.WC7WorkersCompLine

    line.setConditionExists(_conditionPattern, true)
    var coordinatedPolicy = new WC7CoordinatedPolicy(line.Branch)
    line.addToMultipleCoordinatedPolicies(coordinatedPolicy)
    coordinatedPolicy.setFieldValue("MultipleCoordindatedPolicyCond", line.getCondition(_conditionPattern))
    coordinatedPolicy.LaborContractor = line.WC7PolicyLaborContractors.first()?.AccountContactRole as LaborContractor
    return coordinatedPolicy
  }

  final function withJurisdiction(aJurisdiction : Jurisdiction) : WC7CoordinatedPolicyBuilder {
    return withStatePerformed(aJurisdiction)
  }

  final function withState(aJurisdiction : Jurisdiction) : WC7CoordinatedPolicyBuilder {
    return withStatePerformed(aJurisdiction)
  }

  final function withStatePerformed(aJurisdiction : Jurisdiction) : WC7CoordinatedPolicyBuilder {
    set(WC7CoordinatedPolicy#StatePerformed.getPropertyInfo(), aJurisdiction)
    return this
  }

  final function withContractProject(str : String) : WC7CoordinatedPolicyBuilder {
    set(WC7CoordinatedPolicy#ContractProject.getPropertyInfo(), str)
    return this
  }

  final function withNumber(str : String) : WC7CoordinatedPolicyBuilder {
    return withLaborContractorPolicyNumber(str)
  }

  final function withLaborContractorPolicyNumber(str : String) : WC7CoordinatedPolicyBuilder {
    set(WC7CoordinatedPolicy#LaborContractorPolicyNumber.getPropertyInfo(), str)
    return this
  }

  final function withAccountLaborContractor(aLaborContractor : entity.LaborContractor) : WC7CoordinatedPolicyBuilder {
     set(WC7CoordinatedPolicy#LaborContractor.getPropertyInfo(), aLaborContractor)
     return this
  }
}
