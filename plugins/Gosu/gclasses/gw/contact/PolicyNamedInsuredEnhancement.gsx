package gw.contact
uses java.util.Collection
uses java.util.ArrayList
uses gw.api.util.StateJurisdictionMappingUtil

@Export
enhancement PolicyNamedInsuredEnhancement : PolicyNamedInsured {

  function getOfficialIDsForState(st : State) : Collection<OfficialID> {
    var isPrimaryNamedInsured = this typeis PolicyPriNamedInsured
    var hasLocationNamedInsuredInState = false
    if (st != null) {
      hasLocationNamedInsuredInState = this.LocationNamedInsureds*.Location.hasMatch(\ polLoc -> polLoc.State == st)
    }

    var officialIdsForState = new ArrayList<OfficialID>()
    for (officialId in this.AccountContactRole.AccountContact.Contact.OfficialIDs) {
      var isInsuredAndState = officialId typeis PCOfficialID and officialId.Pattern.Scope == TC_INSUREDANDSTATE
      if (st == StateJurisdictionMappingUtil.getStateMappingForJurisdiction(officialId.State)
          and (isPrimaryNamedInsured or hasLocationNamedInsuredInState or not isInsuredAndState)) {
        officialIdsForState.add(officialId)
      }
    }
    return officialIdsForState
  }
  
  property get IndustryCode() : IndustryCode {
    return (this.AccountContactRole as NamedInsured).IndustryCode
  }

  property set IndustryCode(arg : IndustryCode) {
    (this.AccountContactRole as NamedInsured).IndustryCode = arg
  }

  /**
   * US669
   * 03/20/2015 Shane Murphy
   *
   * HP Exxstream requires the array of Policy Named insureds to contain the same amount of tags for all entries.
   * We also need to include the PlcyNonPriNamedInsured Relationship. 'Self' is returned for the Primary Named Insured.
   */
  @Returns("The Relationship to the Policy Named Insured")
  property get RelationshipToPriNamedInsured_TDIC(): String {
    if (this typeis PlcyNonPriNamedInsured) {
      return this.Relationship
    } else {
      return "Self"
    }
  }
}
