package gw.question

uses entity.PolicyLine
uses gw.api.locale.DisplayKey
uses gw.api.productmodel.Question
uses gw.api.util.DateUtil
uses gw.job.uw.UWIssueValueType
uses gw.policy.PolicyEvalContext

enhancement QuestionEnhancement : Question {

  /**
   * Returns mode for QuestionInputSet.pcf that determines UI for this question.
   *
   * @param container     the AnswerContainer that holds answers to this question
   * @param onChangeBlock an executable block to run when the answer to this question is changed
   * @return mode for QuestionInputSet.pcf
   */
  function getInputSetMode(container : AnswerContainer, onChangeBlock : block(), openForEdit : Boolean) : String {
    var currencyFormatQuestions = {"BOPBuildingReplacementCost", "LRPBuildingReplacementCost", "ImageEquipmentCost", "DentalOperatories",
        "DentalSupportEqupmentCost", "SmallDentalEquipmentCost", "TEchnologyCost", "DentalSuppliesCost", "TenantImprovementCost", "OfficeEquipmentCost", "ReceptionRoomCost",
        "ElectronicDataProcessingCost", "OtherCost", "AmountPaid", "AmountOfReservePLCMOC_TDIC", "AmountOfTotalSettlemetnJudgmentPLCMOC_TDIC", "AmountPaidOnYourBehalfPLCMOC_TDIC"}
    var booleanOnChangeQuestions = {"HoldDentalLicenseInOtherStates_TDIC", "HoldDentalLicensesInOtherStatesPLCMOC_TDIC", "OfficeContentsInsuredWithTDICCYB_TDIC",
        "DoYouPerformProvideBelowServices_TDIC", "TreatPatientsUnderBelowAnestheticModalities_TDIC"}

    if (currencyFormatQuestions.contains(this.CodeIdentifier) && openForEdit) {
      return (this.Format as String).concat("_Currency_TDIC")
    }
    if (currencyFormatQuestions.contains(this.CodeIdentifier) && !openForEdit) {
      return (this.Format as String).concat("_NonEditCurrency_TDIC")

    } else if (this.CodeIdentifier == "TotalBPPLimits") {
      return (this.Format as String).concat("_TotalBPPLimit_TDIC")

    } else if (getBoldLabelVisibility_TDIC()) {
      return (this.Format as String).concat("_BoldLabel_TDIC")

    } else if (booleanOnChangeQuestions.contains(this.CodeIdentifier)) {
      return (this.Format as String).concat("_BooleanOnChange_TDIC")

    } else if (this.CodeIdentifier == "NewGradDiscEffDate_TDIC") {
      return (this.Format as String).concat("_NewGradDiscEffDate_TDIC")

    } else if (this.CodeIdentifier == "NewGradDiscExpDate_TDIC") {
      return (this.Format as String).concat("_NewGradDiscExpDate_TDIC")

    } else if (this.CodeIdentifier == "DentalScool_TDIC" or this.CodeIdentifier == "DentalSchoolPLCMOC_TDIC") {
      return (this.Format as String).concat("_DentalSchool_TDIC")

    } else if (this.QuestionPostOnChange == TC_ALWAYS or
        onChangeBlock != null or
        not container.getQuestionDependencies(this).Empty) {
      return this.Format as String
    } else {
      return this.Format + "_NoPost"
    }
  }

  /**
   * Computes associated risk points based on visibility of the question and its current answer.
   * If the question is visible and the answer is incorrect, it returns the number of risk points that was configured
   * to be added for this Question when it is answered incorrectly. Otherwise, it returns zero.
   *
   * @param container the AnswerContainer that holds answers to this question
   * @return effective risk points
   */
  function computeEffectiveRiskPoints(container : AnswerContainer) : int {
    if (!this.isQuestionAvailable(container) || !this.isQuestionVisible(container)) {
      return 0
    }

    var answer = container.getAnswer(this)

    return (this.isIncorrect(answer) and (this.RiskPoints != null))
        ? this.RiskPoints : 0
  }

  function addUWIssueIfAnswerIsIncorrect(context : PolicyEvalContext, container : AnswerContainer) {
    if (!this.isQuestionAvailable(container) || !this.isQuestionVisible(container)) {
      return
    }

    var answer = container.getAnswer(this)

    if (this.isIncorrect(answer) and this.UWIssueType != null) {
      var issue = context.addIssue(this.UWIssueType.Code, issueKey(container),
          \-> this.FailureMessage, \-> issueLongDescription(container, answer))
      if (answer.AnswerValue != null and
          this.UWIssueType.ComparatorWrapper.ValueType == UWIssueValueType.BIG_DECIMAL and
          this.QuestionType == TC_INTEGER) {
        issue.Value = answer.AnswerValue.toString()
      }
    }
  }

  function getTextForBooleanSelect(option : boolean) : String {
    return option ? DisplayKey.get("Java.NameValueView.Boolean.True") : DisplayKey.get("Java.NameValueView.Boolean.False")
  }

  private function issueKey(container : AnswerContainer) : String {
    var containerReference : String
    if (container typeis PolicyLocation)
      containerReference = "PolicyLocation"
    else if (container typeis PolicyLine)
      containerReference = "PolicyLine"
    else if (container typeis PolicyPeriod)
      containerReference = "PolicyPeriod"
    else
      containerReference = (typeof container).DisplayName

    if (container typeis EffDated)
      containerReference = containerReference + ":" + container.FixedId.Value

    return this.QuestionSet.PublicID + "::" + this.PublicID + "::" + containerReference
  }

  private function issueLongDescription(container : AnswerContainer, answer : PCAnswerDelegate) : String {
    var containerName = ""
    var booleanAnswer : String
    if (container typeis PolicyLine)
      containerName = container.Pattern.Name
    else if (container typeis PolicyPeriod)
      containerName = DisplayKey.get("UWIssue.Question.PolicyPeriodName", container.Job.JobNumber, container.BranchName)
    else
      containerName = container.DisplayName

    var longDescription = this.QuestionSet.Name + "\n" +
        containerName + "\n" +
        this.Text + (this.RiskPoints == null ? "" : " (${this.RiskPoints})") + "\n"

    if (answer.AnswerValue == null) {
      return longDescription + DisplayKey.get("UWIssue.Question.NoAnswerSupplied")
    } else if (answer.getQuestion().getQuestionType().equals(QuestionType.TC_BOOLEAN)) {
      // get localized boolean answer
      booleanAnswer = answer.AnswerValue.toString().equals("true") ? DisplayKey.get("Java.QuestionSet.Answer.True") : DisplayKey.get("Java.QuestionSet.Answer.False")
      return longDescription + booleanAnswer
    }

    return longDescription + answer.AnswerValue
  }

  /*
   * create by: SureshB
   * @description: method to validate the Building UW question's answer lengths
   * @create time: 7:34 PM 9/10/2019
    * @param length of the answer
   * @return: Validation message
   */
  function getLengthForQuestion_TDIC(length : int) : String {
    var char256Questions = {"WhatRenovationsWilllBeDone", "OngoingProblemWithPropertyDesc", "DoYouShareOfficeWithOwnerEntityDesc", "OccupanciesOtherThanMedicalDental",
        "CarrierRescinedCancelledRefRenDesc", "PropertyLossInLastFiveYearsDesc", "WhatHasBeenDoneToPreventTheLoss", "IfNoListTypeOfOccupancies", "AdditionalComments_TDIC", "IfYesPleaseProvideDetails_TDC",
        "PracticedWithoutPLInsuranceDesc_TDIC", "ActionAgainstYourPracticeOfDentistryDesc_TDIC", "ConvictedOfCrimesOtherThanMinorTrafficDesc_TDIC", "HealthProblemsDesc_TDIC", "ClaimOrAllegationOfMalPracticeDesc_TDIC",
        "AdditionalCommentsPLCMOC_TDIC", "IfYesPleaseProvideDetailsPLCMOC_TDIC", "ListCourseDetailsPLCMOC_TDIC", "YesGiveDetailsDatesBTWYouUninsuredPLCMOC_TDIC", "IfYesTDICDeclinedCoverageDetailsPLCMOC_TDIC",
        "InvestigatedYouDetailsPLCMOC_TDIC", "ConvictedOtherThanMinorTrafficViolDetailsPLCMOC_TDIC", "ChargedWithFraudulantBillingDetailsPLCMOC_TDIC", "NarrativeDescOfTheClaimPLCMOC_TDIC",
        "BelieveCouldGiveRiseToClaimDetailsPLCMOC_TDIC", "TerminateIndependentContractorDescPLCMOC_TDIC", "EmplymentAccusationsInFiveYearsDetailsPLCMOC_TDIC", "LawsuitInFiveYearsDetailsPLCMOC_TDIC",
        "OtherEmplIncidentsClaimDetailsPLCMOC_TDIC", "DeclinedLiabilityPracticesDetailsPLCMOC_TDIC", "PLInsuranceIfYesPleaseExpl_TDIC", "PracticeOfDentistryDetails_TDIC", "ClaimOfMalpracticeDetails_TDIC", "PersonalHealthProblemDetails_TDIC"}
    var char80Questions = {"RenovationsPlannedOnText", "CurrentCarrier", "DentalScool_TDIC", "YearGraduated_TDIC", "YearFristPracticingInUS_TDIC", "NameOfHospital_TDIC", "YearCompleted_TDIC",
        "LocalDentalSociety_TDIC", "WhoAdministers_TDIC", "IfYesNameOfPartnership_TDIC", "IfYesNameOfCorporation_TDIC", "DentalSchoolPLCMOC_TDIC", "YearFirstBeganPracticingINUSPLCMOC_TDIC",
        "NameOfHospitalPLCMOC_TDIC", "YearCompletedPLCMOC_TDIC", "SpecialityShoolAttendedPLCMOC_TDIC", "YearSpecialityProgramCompletedPLCMOC_TDIC", "OtherPleaseDescribePLCMOC_TDIC", "WhoAdministersPLCMOC_TDIC",
        "IfYesNameOfPartnershipPLCMOC_TDIC", "IfYesNameOfCorporationPLCMOC_TDIC", "NamOfPatientClaimantPLCMOC_TDIC", "CityStateIncidentOccuredPLCMOC_TDIC", "AllegationPLCMOC_TDIC",
        "NameOfInsurerPLCMOC_TDIC", "DatesOfAllegedOccurrencePLCMOC_TDIC", "DateIncidentClaimSuitReportedPLCMOC_TDIC", "DateClosedPLCMOC_TDIC"}
    var char3Questions = {"PercentageBuilldingSqrFootNotOccupied", "PercentageBuilldingSqrFootNotOccupiedLRP"}
    var char12Questoins = {"PolicyNumber"}
    var char20Questions = {"DentalLicenseNumber_TDIC", "DentalLicenseNumberPLCMOC_TDIC", "ADANumber_TDIC"}
    var char4Questions = {"HygienistsFullTimePLCMOC_TDIC", "HygienistsParTimePLCMOC_TDIC", "DetnalAssistantsFullTimePLCMOC_TDIC", "DentalAssistantsPartTimePLCMOC_TDIC",
        "PartnersShareHoldersFullTimePLCMOC_TDIC", "PartnersShareHoldersPartTimePLCMOC_TDIC", "OtherOfficeStaffFullTimePLCMOC_TDIC", "OtherOfficeStaffPartTimePLCMOC_TDIC",
        "OtherIndependentDentistsFullTimePLCMOC_TDIC", "OtherIndependentDentistsPartTimePLCMOC_TDIC"}
    var char2Questions = {"AverageHoursPerWeekToPractiveDentistryOverNextYear_TDIC", "NumberOfLocationsYouPractice_TDIC"}
    if (char80Questions.contains(this.CodeIdentifier) and length > 80) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 80)
    } else if (char256Questions.contains(this.CodeIdentifier) and length > 256) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 256)
    } else if (char3Questions.contains(this.CodeIdentifier) and length > 3) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 3)
    } else if (char12Questoins.contains(this.CodeIdentifier) and length > 12) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 12)
    } else if (char20Questions.contains(this.CodeIdentifier) and length > 20) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 20)
    } else if (char4Questions.contains(this.CodeIdentifier) and length > 4) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 4)
    } else if (char2Questions.contains(this.CodeIdentifier) and length > 2) {
      return DisplayKey.get("TDIC.Web.Policy.BOP.Buildings.QuestionLengthValidation", this.DisplayName, 2)
    }
    return null
  }
  /*
   * create by: SureshB
   * @description: method to determine the visibility of Bold Label Questions
   * @create time: 6:50 PM 10/17/2019
    * @param null
   * @return: boolean
   */

  function getBoldLabelVisibility_TDIC() : boolean {
    var boldLabelQuestions = {"IndicateTheCostToReplaceWithNewEquipment", "IfYesYouMustAttachALetterFromSchool_TDIC", "IfYesYouMustAttachALetterFromSchoolStudentStatus_TDIC",
        "AttachLetterVerifySchoolDiscoutPLCMOC_TDIC", "AttachLetterVerifySchoolPostGradDiscountPLCMOC_TDIC", "AttachCopyOfMostRecentInsuranceDeclarationPLCMOC_TDIC", "AttachStatemetFromPhysicianPLCMOC_TDIC",
        "IncludeCopyOfCurrentEmploymentPLCMOC_TDIC", "NoOfEmployeesAtAllLocationsExcludingFamilyPLCMOC_TDIC", "HygienistsPLCMOC_TDIC", "DentalAssistantsPLCMOC_TDIC", "PartnersOrShareHoldersPLCMOC_TDIC",
        "OtherOfficeStaffPLCMOC_TDIC", "OtherIndependentDentistsPLCMOC_TDIC", "DoYouHaveWrittenProceduresPLCMOC_TDIC", "HaveWrittenProceduresInOfficeQQ_TDIC"}
    if (boldLabelQuestions.contains(this.CodeIdentifier)) {
      return true
    }
    return false
  }
/*
 * create by: SureshB
 * @description: method to determine the editability of the PLUW Questions.
 * @create time: 4:52 PM 10/21/2019
  * @param null
 * @return: AnswerContainer
 */

  function isQuestionEditable_TDIC(answerContainer : AnswerContainer) : boolean {
    var isEditable = true

    var NWStates = {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}
    var isNWState = NWStates.contains(answerContainer.AssociatedPolicyPeriod?.BaseState.getCode())


    if (answerContainer.AssociatedPolicyPeriod?.isDIMSPolicy_TDIC  or isNWState)
    {
      return true
    }
    if (answerContainer.AssociatedPolicyPeriod.GLLineExists and answerContainer typeis PolicyLine) {
      var editableSubRewriteNotPCRen = answerContainer.AssociatedPolicyPeriod?.Policy?.Product?.getQuestionSetByCodeIdentifier("GLUnderwriting")?.OrderedQuestions*.CodeIdentifier.toList()
      var editableRenewalSubRewriteNotPC = {"WishToApplyForEmpoymentPracticesPLCMOC_TDIC", "WishtToApplyForEmployementPracticesDESCPLCMOC_TDIC", "IncludeCopyOfCurrentEmploymentPLCMOC_TDIC", "NoOfEmployeesAtAllLocationsExcludingFamilyPLCMOC_TDIC",
          "HygienistsPLCMOC_TDIC", "HygienistsFullTimePLCMOC_TDIC", "HygienistsParTimePLCMOC_TDIC", "DentalAssistantsPLCMOC_TDIC", "DentalAssistantsPartTimePLCMOC_TDIC", "DetnalAssistantsFullTimePLCMOC_TDIC",
          "PartnersOrShareHoldersPLCMOC_TDIC", "PartnersShareHoldersPartTimePLCMOC_TDIC", "PartnersShareHoldersFullTimePLCMOC_TDIC", "OtherOfficeStaffPLCMOC_TDIC", "OtherOfficeStaffFullTimePLCMOC_TDIC",
          "OtherOfficeStaffPartTimePLCMOC_TDIC", "OtherIndependentDentistsPLCMOC_TDIC", "OtherIndependentDentistsPartTimePLCMOC_TDIC", "OtherIndependentDentistsFullTimePLCMOC_TDIC",
          "GivesRightToTakeOverThePracticePLCMOC_TDIC", "TerminatedOrIndependentContractorWithin5YearsPLCMOC_TDIC", "TerminateIndependentContractorDescPLCMOC_TDIC", "EmploymentAccusationsInFiveYearsPLCMOC_TDIC",
          "EmplymentAccusationsInFiveYearsDetailsPLCMOC_TDIC", "LawsuitsInFiveYearsPLCMOC_TDIC", "LawsuitInFiveYearsDetailsPLCMOC_TDIC", "OtherEmployementIncidentsClaimPLCMOC_TDIC", "OtherEmplIncidentsClaimDetailsPLCMOC_TDIC",
          "DeclinedLiabilityInsurancePracticesPLCMOC_TDIC", "DeclinedLiabilityPracticesDetailsPLCMOC_TDIC", "DoYouHaveWrittenProceduresPLCMOC_TDIC", "TerminationPLCMOC_TDIC", "HiringPLCMOC_TDIC",
          "DisciplinePLCMOC_TDIC", "HaveStandardEmplApplicationForAllPLCMOC_TDIC", "HaveEmpoyeeHandbookPLCMOC_TDIC", "HaveAtWillProvisionInEmploymentPLCMOC_TDIC", "HaveWrittenPolicyWithRespectToSexualHarassmentPLCMOC_TDIC",
          "HaveWritternPolicyWithRespectToDiscriminationPLCMOC_TDIC", "HaveWritternAnnualPerformanceEvaluationPLCMOC_TDIC", "HaveWritternProceduresForHandlingComplaintsPLCMOC_TDIC",
          "PostRequiredFederalStatePostersPLCMOC_TDIC","DentalScool_TDIC"}
      editableSubRewriteNotPCRen?.removeAll(editableRenewalSubRewriteNotPC)
      if (editableSubRewriteNotPCRen.contains(this.CodeIdentifier)) {
        if (!((answerContainer as PolicyLine).JobType == typekey.Job.TC_SUBMISSION or (answerContainer as PolicyLine).JobType == typekey.Job.TC_REWRITE)) {
          isEditable = false
        }
      }
      if (editableRenewalSubRewriteNotPC.contains(this.CodeIdentifier)) {
        if (!((answerContainer as PolicyLine).JobType == typekey.Job.TC_SUBMISSION or (answerContainer as PolicyLine).JobType == typekey.Job.TC_RENEWAL or (answerContainer as PolicyLine).JobType == typekey.Job.TC_REWRITE)) {
          isEditable = false
        }
      }
    }
    if(!isEditable){
      isEditable = isGlobalQuestionsEditableForGL(answerContainer)
    }
    return isEditable
  }

  private function isGlobalQuestionsEditableForGL(answerContainer : AnswerContainer) : boolean {
    var glQuestions = {"OfficeContentsInsuredWithTDICCYB_TDIC","BreachOfPersonalInoIn12MonthsCYB_TDIC","ConductBackgroundScreensForEmployeesCYB_TDIC", "HavePostedDocumentRetensionPolicyCYB_TDIC",
        "RegularlyUpdateComputerSecurityMeasuresCYB_TDIC","CustomerPhysicalRecordsMaintainedInSecuredPlaceCYB_TDIC"}
    if (glQuestions.contains(this.CodeIdentifier) and (answerContainer as PolicyLine) typeis GeneralLiabilityLine){
      var line = answerContainer as GeneralLiabilityLine
      if({typekey.Job.TC_POLICYCHANGE ,typekey.Job.TC_RENEWAL}.contains(line.JobType) and line.GLCyberLiabCov_TDICExists){
        return line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value != null and
            line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value > line.BasedOn.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value
      }
    }
    return Boolean.FALSE
  }

  /**
   *
   * @param effDate
   */
  function validateNewGradDiscountEffDate(effDate : Date) : String {
    if(effDate != null and effDate.YearOfDate <= 1992) {
      return DisplayKey.get("TDIC.Web.GL.NewGradEffDate.Error.EffectiveDateTooEarly", effDate.ShortFormat, DateUtil.createDateInstance(1, 1, 1993).ShortFormat)
    }
    return null
  }

  /**
   * create by: OsmanI
   * @description: method to determine if the policy is a DIMS policy from a NW state
   * @param answerContainer
   */
  function isQuestionMandatory(answerContainer : AnswerContainer) : boolean {
    var isMandatory = true
    var NWStates = {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}
    var isNWState = NWStates.contains(answerContainer.AssociatedPolicyPeriod?.BaseState.getCode())


    if (answerContainer.AssociatedPolicyPeriod?.isDIMSPolicy_TDIC  or isNWState) {

      if (answerContainer.AssociatedPolicyPeriod.GLLineExists and answerContainer typeis PolicyLine) {
        var nonMandatoryQuestions = answerContainer.AssociatedPolicyPeriod?.Policy?.Product?.getQuestionSetByCodeIdentifier("GLUnderwriting")?.OrderedQuestions*.CodeIdentifier.toList()
        var MandatoryQuestions = {"DentalLicenseNumber_TDIC", "DentalLicenseState_TDIC"}
        nonMandatoryQuestions?.removeAll(MandatoryQuestions)

        if (nonMandatoryQuestions.contains(this.CodeIdentifier) and this.CodeIdentifier != "WishToApplyForEmpoymentPracticesPLCMOC_TDIC" and this.Filters.first().FilterQuestionID != "zrbge4gk4cnco0a0mqqbe7bltdb") {
          isMandatory = false
          return isMandatory
        }
        if (MandatoryQuestions.contains(this.CodeIdentifier)){
          isMandatory = true
          return isMandatory
        }
      }
      if (answerContainer.AssociatedPolicyPeriod.BOPLineExists and (answerContainer typeis PolicyLocation or answerContainer typeis PolicyPeriod)) {

        var LRPQuestions = answerContainer.AssociatedPolicyPeriod?.Policy?.Product?.getQuestionSetByCodeIdentifier("BOPBuildingSupp")?.OrderedQuestions*.CodeIdentifier.toList()

        var BOPQuestions = answerContainer.AssociatedPolicyPeriod?.Policy?.Product?.getQuestionSetByCodeIdentifier("BOPOffering")?.OrderedQuestions*.CodeIdentifier.toList()

        if (LRPQuestions.contains(this.CodeIdentifier) or BOPQuestions.contains(this.CodeIdentifier)) {

          isMandatory = false
          return isMandatory

        }
      }
    }



    return this.Required
  }

  function getInteger(answerContainer : AnswerContainer ) : java.lang.Integer
  {
    if (answerContainer.getAnswer(this).IntegerAnswer == null)
    { return  Integer.valueOf(0) }
    else
    { return answerContainer.getAnswer(this).IntegerAnswer }
  }


}
