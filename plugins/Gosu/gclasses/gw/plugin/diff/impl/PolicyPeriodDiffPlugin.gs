package gw.plugin.diff.impl

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffItem
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffUtils
uses gw.api.diff.node.IDiffTreeNode
uses gw.api.logicalmatch.EffDatedLogicalMatcher
uses gw.api.tree.RowTreeRootNode
uses gw.api.web.util.DateRangeUtil
uses gw.diff.tree.DiffTree
uses gw.plugin.diff.IPolicyPeriodDiffPlugin

uses java.util.ArrayList
uses java.util.List

/**
  * All these methods are invoked from Java.
  */

@Export
class PolicyPeriodDiffPlugin implements IPolicyPeriodDiffPlugin {
  
  construct() 
  {
  }
  
  /**
   * Compare two policy periods, returning a list of DiffItems representing the
   * differences between them. This method is called in 3 places: <ol>
   * <li>Multi-quote
   * <li>Comparing PolicyPeriods of a Policy
   * <li>Comparing Jobs of a Policy
   * </ol><p>
   * An implementation of this method would walk the
   * PolicyPeriods graphs comparing fields on related beans within the graphs.
   * Fields don't necessarily need to be different, you could highlight the fact
   * that two fields are the same across periods.
   * <p>
   * To add a new item create a new instance of DiffProperty, DiffAdd, or
   * DiffRemove. When creating a new DiffProperty use the bean from p1 as the
   * original bean, and the bean from p2 as the new bean. A DiffAdd
   * designated that the bean exists in p2 but not in p1. A DiffRemove
   * designates that the bean exists in p1 but not in p2. You can use utilities
   * found on DiffUtils to help you compare beans or bean graphs.
   *
   * @param reason Reason for calling Diff code
   * @param p1 "source" PolicyPeriod 
   * @param p2 PolicyPeriod to compare against
   * @return A list of DiffItems that represents the differences between the
   */
  public override function compareBranches(reason : DiffReason, p1 : PolicyPeriod, p2 : PolicyPeriod) : List<DiffItem> {
    var diffItems = new ArrayList<DiffItem>()
    var diffHelper : DiffHelper

    p1.AllAccountSyncables.each(\ a -> a.prepareForDiff())
    p2.AllAccountSyncables.each(\ a -> a.prepareForDiff())
    
    // Add diffs for PolicyPeriod attributes
    if (p2.Renewal == null and reason != null) {
      diffItems = addPolicyPeriodDiffItems(p2, p1, reason, diffItems) as ArrayList<DiffItem>
    }
    
    // Add diffs by line of business
    for (line1 in p1.Lines){
      var line2 = p2.Lines.firstWhere( \ l -> l.Subtype == line1.Subtype )
      if (line2 != null) {
        diffHelper = line1.createPolicyLineDiffHelper(reason, line2)
        if (diffHelper != null) {
          diffItems = diffHelper.addDiffItems(diffItems) as ArrayList<DiffItem>
          diffItems = diffHelper.filterDiffItems(diffItems) as ArrayList<DiffItem>
        }
      }
    }
    return new ArrayList<DiffItem>(diffItems.order())
  } 
  
  /**
   * Filters a list of DiffItems that originates from the database. This method
   * is called in 4 places:<ol>
   * <li>Displaying differences between a PolicyPeriod and its based on
   * PolicyPeriods during a policy change
   * <li>To determine out-of-sequence transaction conflicts on a PolicyPeriod
   * <li>To determine preemption conflicts on a PolicyPeriod
   * <li>To determine the changes to communicate to
   * an integration point when a new PolicyPeriod is bound
   * </ol><p>
   * The input set of DiffItems is derived from the database and therefore
   * represents a database POV of these changes. By filtering the items you
   * can provide a more intuitive summary of the differences. See the Difference
   * class for how these DiffItems are displayed in the UI.
   *
   * @param reason Reason for calling Diff code
   * @currentPeriod PolicyPeriod associated with the DiffItems
   * @param items Original set of DiffItems
   * @return A filtered, ordered list of DiffItems.
   */   
  public override function filterDiffItems(reason : DiffReason, currentPeriod : PolicyPeriod, diffItems : List<DiffItem>) : List<DiffItem> {
    var diffHelper : DiffHelper

    // Make sure there is a BasedOn (may be called from submissions)
    if (currentPeriod.BasedOn == null) {
      return diffItems
    }

    if (reason == DiffReason.TC_FINDDUPLICATES) {
      diffItems.removeWhere(\ d -> not (d typeis DiffAdd and 
            (d.EffDatedBean typeis EffDatedLogicalMatcher and not d.EffDatedBean.findMatchesInPeriodUntyped(currentPeriod, false).Empty)))
    } else if (reason == DiffReason.TC_EXPIRATIONDATECHECK) {
      diffItems = diffItems.where(\ d -> d typeis DiffAdd or d typeis DiffProperty)
      // only need 1 entry per changed bean
      var tmpMap = diffItems.partition(\ d -> d.EffDatedBean)
      var consolidatedDiffItems = new ArrayList<DiffItem>()
      tmpMap.eachValue(\ e -> consolidatedDiffItems.add(e.first()))
      diffItems = consolidatedDiffItems
    } else if (reason != DiffReason.TC_INTEGRATION and 
        reason != DiffReason.TC_APPLYCHANGES) {
          
      // Add diffs for PolicyPeriod attributes
      // always check estimated premium, even on renewals
      if (currentPeriod.Renewal == null and reason != null) {
        diffItems = addPolicyPeriodDiffItems(currentPeriod, currentPeriod.BasedOn, reason, diffItems)
      } else if (currentPeriod.EstimatedPremium != currentPeriod.BasedOn.EstimatedPremium) {
        diffItems.add(new DiffProperty(currentPeriod, currentPeriod.BasedOn, PolicyPeriod.Type.TypeInfo.getProperty( "EstimatedPremium" )))
      }

      // Filter diffs by LOB if this is not for integration or OOS
      for (line1 in currentPeriod.BasedOn.Lines){
        var line2 = currentPeriod.Lines.firstWhere( \ p -> p.Subtype == line1.Subtype )
        if (line2 != null) {
          diffHelper = line1.createPolicyLineDiffHelper(reason, line2)
          if (diffHelper != null) {
            diffItems = diffHelper.filterDiffItems(diffItems)
          }
        }
      }
      
      // Remove PolicyPeriod attribute diffs if this is a rewrite job
      if (currentPeriod.Rewrite != null) {
        diffItems.removeWhere( \ d -> d.Bean typeis PolicyPeriod and d.asProperty().PropertyInfo.Name != "EstimatedPremium"  )
      }
    } 
    
    // Filter specific diffs for OOS
    if (reason == DiffReason.TC_APPLYCHANGES) {
      var productAbbrev = currentPeriod.Policy.Product.Abbreviation
      if(productAbbrev == "WC7") {
        diffHelper = new WC7DiffHelper(reason, null, null)
      } else {
        diffHelper = new DiffHelper(reason, null, null)
      }
      diffItems = diffHelper.filterDiffItems(diffItems)
    }
    return new ArrayList<DiffItem>(diffItems.order())
  }

  /**
   * Entities that hang directly off of PolicyPeriod are not diffed, add these manually here
   * @param currentPeriod - the current policy period
   * @param basedOnPeriod - the current policy period's based on period
   * @param diffReason - the reason used to generate the diffs. 
   * @param diffItems - list of diff items to add to
   * @return List<DiffItem> - returns the list of diff items that we've modified
   */
  function addPolicyPeriodDiffItems(currentPeriod : PolicyPeriod, basedOnPeriod : PolicyPeriod, reason : DiffReason, diffItems : List<DiffItem>) : List<DiffItem>{
    // Ensure diffs are against the correct slice for policy reviews
    if (reason == DiffReason.TC_POLICYREVIEW) {
      var periodRange = DateRangeUtil.allDatesBetween(basedOnPeriod.PeriodStart, basedOnPeriod.PeriodEnd)
      if (periodRange.includes(currentPeriod.EditEffectiveDate)) {
        basedOnPeriod = basedOnPeriod.getSlice(currentPeriod.EditEffectiveDate)  
      } else {
        basedOnPeriod = basedOnPeriod.LatestPeriod    
      }
    }

    if (currentPeriod.EstimatedPremium != basedOnPeriod.EstimatedPremium) {
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod.Type.TypeInfo.getProperty( "EstimatedPremium" )))
    }

    if (currentPeriod.ProducerCodeOfRecord != basedOnPeriod.ProducerCodeOfRecord) {
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod.Type.TypeInfo.getProperty( "ProducerCodeOfRecord" )))
    }   
      
    if (currentPeriod.Offering != basedOnPeriod.Offering) {
      var newItems = new DiffUtils(new PCBeanMatcher()).compareField(basedOnPeriod, currentPeriod, PolicyPeriod.Type.TypeInfo.getProperty("EffectiveDatedFields"), 1)
      diffItems.addAll(newItems)
    } 
    
    // Add Term Type, PeriodStart/PeriodEnd diff items
    if (currentPeriod.PeriodStart != basedOnPeriod.PeriodStart) {
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod.Type.TypeInfo.getProperty("PeriodStart")))
    }  
    if (currentPeriod.PeriodEnd != basedOnPeriod.PeriodEnd) {
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod.Type.TypeInfo.getProperty("PeriodEnd")))
    }

    // Add WrittenDate to diffs
    if (currentPeriod.WrittenDate != basedOnPeriod.WrittenDate){
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod#WrittenDate.PropertyInfo))
    }

    // Add RateAsOfDate to diffs
    if (currentPeriod.RateAsOfDate != basedOnPeriod.RateAsOfDate){
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod#RateAsOfDate.PropertyInfo))
    }

    // Add Base State to diffs
    if (currentPeriod.BaseState != basedOnPeriod.BaseState) {
      diffItems.add(new DiffProperty(currentPeriod, basedOnPeriod, PolicyPeriod.Type.TypeInfo.getProperty("BaseState")))
    }

    // Add Product Modifiers to diffs
    diffItems.addAll(new DiffUtils(new PCBeanMatcher()).compareField(basedOnPeriod.EffectiveDatedFields, currentPeriod.EffectiveDatedFields, EffectiveDatedFields.Type.TypeInfo.getProperty("ProductModifiers"), 2))
    
    return diffItems
  }

  // Step 1: PolicyPeriodDiffPlugin_locateQualifyingChangesOnThisJob_TDIC
  //         Examine current job for qualifying nodes and fields
  //         Input: PolicyPeriod, Qualifying nodes and fields
  //         Output: HashMap of Eligible nodes and fields that changed on the current policy

  // Step 2: PolicyPeriodDiffPlugin_locateImpactedPoliciesWithEligibleNodesAndFields_TDIC
  //         Locate list of all affected policies
  //         Input: PolicyPeriod, HashMap of Eligible nodes and fields that changed on the current policy (from Step 1)
  //         Output: HashSet<PolicyPeriod>
  // Step 3:

  // Step 4: PolicyPeriodDiffPlugin_generateEndorsementReminderActivity_TDIC
  //         Generate activity on each affected policy
  //         Input: PolicyPeriod, HashSet<PolicyPeriod> (from Step 2)
  //         Output:

  property get QualifyingAddressFields_TDIC () : ArrayList<String> {
    var tmpQualifyingAddressFields : ArrayList<String> = new ArrayList<String>()
    tmpQualifyingAddressFields.add("Address Line 1")
    tmpQualifyingAddressFields.add("Address Line 2")
    tmpQualifyingAddressFields.add("Address Line 3")
    tmpQualifyingAddressFields.add("City")
    tmpQualifyingAddressFields.add("State")
    tmpQualifyingAddressFields.add("Postal Code")
    return tmpQualifyingAddressFields
  }

  property get QualifyingContactFields_TDIC () : ArrayList<String> {
    var tmpQualifyingContactFields : ArrayList<String> = new ArrayList<String>()
    tmpQualifyingContactFields.add("First Name")
    tmpQualifyingContactFields.add("Last Name")
    tmpQualifyingContactFields.add("Company Name")
    return tmpQualifyingContactFields
  }

  property get QualifyingPolicyAddressNodesAndFields_TDIC() : HashMap{
    var tmpMap = new HashMap()
    tmpMap.put("Policy Address", QualifyingAddressFields_TDIC)
    return tmpMap
  }

  property get QualifyingPolicyLocationNodesAndFields_TDIC() : HashMap{
    var tmpMap = new HashMap()
    tmpMap.put("Policy Location", QualifyingAddressFields_TDIC)
    return tmpMap
  }

  //    var tmpQualifyingAccountLocationNodesAndFields : HashMap = new HashMap()
  //    tmpQualifyingAddressNodesAndFields.put("Account Location", QualifyingAddressFields_TDIC)
  property get QualifyingPNIContactNodesAndFields_TDIC() : HashMap {
    var tmpMap = new HashMap()
    tmpMap.put("Primary Named Insured", QualifyingContactFields_TDIC)
    return tmpMap
  }

  property get QualifyingANIContactNodesAndFields_TDIC() : HashMap {
    var tmpMap = new HashMap()
    tmpMap.put("Additional Named Insured", QualifyingContactFields_TDIC)
    return tmpMap
  }

  property get QualifyingOOContactNodesAndFields_TDIC() : HashMap {
    var tmpMap = new HashMap()
    tmpMap.put("Owner/Officer", QualifyingContactFields_TDIC)
    return tmpMap
  }

  function getImpactedPoliciesFromPolicyPeriods_TDIC(passedInEligibleImpactedPolicies: HashSet<PolicyPeriod>): String {
    var tmpActualImpactedPolicies: String = ""
    for (each in passedInEligibleImpactedPolicies) {
      var tmpLatestBoundPeriod = each.Policy.LatestBoundPeriod
      //Defect GW-407- Filter the List of impacted policy, which is In Force Policies.
      if (!tmpLatestBoundPeriod.Canceled and !(gw.api.util.DateUtil.currentDate() >= tmpLatestBoundPeriod.PeriodEnd)){
        tmpActualImpactedPolicies += (each.PolicyNumber + " , ")
        tmpActualImpactedPolicies.chomp(",")
      }
    }
    return tmpActualImpactedPolicies
  }

  function PolicyPeriodDiffPlugin_locateQualifyingChangesOnThisJob_TDIC(passedInPolicyPeriod : PolicyPeriod) : HashMap {
    var tmpRootTree : gw.diff.tree.DiffTree


    // discover qualifying changes on this commited Period (name and address only)
    tmpRootTree = PolicyPeriodDiffPlugin_RecalculateRootTree_TDIC(passedInPolicyPeriod, passedInPolicyPeriod.BasedOn, typekey.DiffReason.TC_POLICYREVIEW)
    var tmpDiffItems = new HashMap()
    var tmpEligibleNodesAndFields : HashMap = PolicyPeriodDiffPlugin_printBranch_TDIC(tmpRootTree.RootNode, tmpDiffItems)
    var tmpNodesToRemove : HashSet = new HashSet<String>()
    for (eachNode in tmpEligibleNodesAndFields.Keys){
      if (
           !(
             (eachNode.toString().startsWithIgnoreCase(QualifyingPolicyAddressNodesAndFields_TDIC.keySet().first() as String))
             || (eachNode.toString().startsWithIgnoreCase(QualifyingPolicyLocationNodesAndFields_TDIC.keySet().first() as String))
             || (eachNode.toString().startsWithIgnoreCase(QualifyingPNIContactNodesAndFields_TDIC.keySet().first() as String))
             || (eachNode.toString().startsWithIgnoreCase(QualifyingANIContactNodesAndFields_TDIC.keySet().first() as String))
             || (eachNode.toString().startsWithIgnoreCase(QualifyingOOContactNodesAndFields_TDIC.keySet().first() as String))
           )
         ){
            tmpNodesToRemove.add(eachNode)
      }
    }
    for (eachNodeToRemove in tmpNodesToRemove ){
      tmpEligibleNodesAndFields.remove(eachNodeToRemove as String)
    }

    return tmpEligibleNodesAndFields
  }

  function PolicyPeriodDiffPlugin_locateImpactedPoliciesWithEligibleNodesAndFields_TDIC(passedInPolicyPeriod : PolicyPeriod, passedInEligibleNodesAndFields : HashMap) : HashSet<PolicyPeriod> {

    var tmpEligibleImpactedPolicies : HashSet<PolicyPeriod> = new HashSet<PolicyPeriod>()

//      if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(tmpQualifyingAccountLocationNodesAndFields, tmpEligibleNodesAndFields)){
//        tmpEligibleImpactedPolicies.addAll(passedInPolicyPeriod.PolicyAddress.Address.getImpactedPoliciesForAccountLocationChange_TDIC())
//      }

    if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(QualifyingPolicyAddressNodesAndFields_TDIC, passedInEligibleNodesAndFields)){
      tmpEligibleImpactedPolicies.addAll(passedInPolicyPeriod.PolicyAddress.Address.getImpactedPoliciesForAddressChange_TDIC().where( \ elt -> elt != passedInPolicyPeriod))
    }

    if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(QualifyingPolicyLocationNodesAndFields_TDIC, passedInEligibleNodesAndFields)){
      for (eachPolicyLocation in passedInPolicyPeriod.PolicyLocations){
        tmpEligibleImpactedPolicies.addAll(eachPolicyLocation.getImpactedPoliciesForPolicyLocationChange_TDIC().where( \ elt -> elt != passedInPolicyPeriod))
      }
    }
    if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(QualifyingPNIContactNodesAndFields_TDIC, passedInEligibleNodesAndFields)){
      tmpEligibleImpactedPolicies.addAll(passedInPolicyPeriod.PrimaryNamedInsured.ContactDenorm.ImpactedPoliciesForContactChange_TDIC.where(\elt -> elt != passedInPolicyPeriod))
    }

    if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(QualifyingANIContactNodesAndFields_TDIC, passedInEligibleNodesAndFields)){
      for (eachANI in passedInPolicyPeriod.addAllExistingAdditionalNamedInsureds()){
        tmpEligibleImpactedPolicies.addAll(passedInPolicyPeriod.PrimaryNamedInsured.ContactDenorm.ImpactedPoliciesForContactChange_TDIC.where(\elt -> elt != passedInPolicyPeriod))
      }
    }

    if (PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(QualifyingOOContactNodesAndFields_TDIC, passedInEligibleNodesAndFields)){
      for (eachOwnerOfficer in passedInPolicyPeriod.WC7Line.WC7PolicyOwnerOfficers){
        tmpEligibleImpactedPolicies.addAll(passedInPolicyPeriod.PrimaryNamedInsured.ContactDenorm.ImpactedPoliciesForContactChange_TDIC.where(\elt -> elt != passedInPolicyPeriod))
      }
    }

    var periodIter = tmpEligibleImpactedPolicies.iterator()
    while( periodIter.hasNext() ) {
      var period = periodIter.next()
      if (period.Policy == passedInPolicyPeriod.Policy) {
        periodIter.remove()
      }
    }

    return tmpEligibleImpactedPolicies
  }

  //20150813 TJ Talluto
  // These three functions were taken from a GW Accelerator:
  // "How To Get The PolicyPeriod Diff Output In a Useable Format"
  function PolicyPeriodDiffPlugin_RecalculateRootTree_TDIC(p1 : PolicyPeriod, p2 : PolicyPeriod, diffReason : DiffReason) : gw.diff.tree.DiffTree {
    var diffTreeConfig = p2.Policy.Product.DiffTreeConfig
    if (diffTreeConfig != null) {
      var items : List<DiffItem>
      if (p1 != null and p2 != null and diffReason != TC_POLICYREVIEW) {
        items = p1.compareTo(diffReason, p2).toList()
      } else if (p1 != null) {
        items = p1.getDiffItems(diffReason)
      } else {
        items = Collections.emptyList<DiffItem>()
      }
//      print ("diffItems found: " + items.Count)
      var diffTree = new gw.diff.tree.DiffTree(items, diffTreeConfig, diffReason)
      return diffTree
    }
    return null
  }

  private function PolicyPeriodDiffPlugin_printBranch_TDIC(node : IDiffTreeNode, passedInDiffItems : HashMap) : HashMap {
    var tmpDiffString : String
    var tmpNodeName : String
    //if (!(node.IsTitle||(node.Value1?.trim().length < 1 && node.Value2?.trim().length < 1))) {
    if (!(node.Value1?.trim().length < 1 && node.Value2?.trim().length < 1)) {
      tmpDiffString = node.Label + "|" + node.Value1 + "|" + node.Value2
      if (!passedInDiffItems.containsKey(node.Parent.Label)){
        passedInDiffItems.put(node.Parent.Label, new ArrayList<String>() {tmpDiffString}) //passedInNodeName)
        //print ("NODE: " + node.Parent.Label + ", ITEM: " + tmpDiffString)
      } else {
        var tmpArrayList : ArrayList<String> = passedInDiffItems.get(node.Parent.Label) as ArrayList<String>
        tmpArrayList.add(tmpDiffString)
        passedInDiffItems.put(node.Parent.Label, tmpArrayList)
        //print ("NODE: " + node.Parent.Label + ", ITEM: " + tmpDiffString)
      }
    }
//print ("KEYS: " + passedInDiffItems.Keys.toString() )
//print ("VALS: " + passedInDiffItems.Values.toString() )
    if(node.Children.Count > 0) {
      for (each in node.Children){
        passedInDiffItems.putAll(PolicyPeriodDiffPlugin_printBranch_TDIC(each, passedInDiffItems))
      }
    }
    return passedInDiffItems
  }

  private function PolicyPeriodDiffPlugin_findQualifyingChange_TDIC(passedInQualifyingNodesAndFields : HashMap, passedInEligibleNodesAndFields : HashMap) : boolean {
    var blnQualifyingChangesFound : boolean = false
    for (eachEligibleNode in passedInEligibleNodesAndFields.keySet()){
      for (eachQualifyingNode in passedInQualifyingNodesAndFields.keySet()){
//        print ("comparing " + eachEligibleNode.toString() + " with " + eachQualifyingNode.toString())
        if (eachEligibleNode.toString().startsWith(eachQualifyingNode as String)){
          for (eachEligibleField in passedInEligibleNodesAndFields.get(eachEligibleNode) as ArrayList<String>){
            for (eachQualifyingField in passedInQualifyingNodesAndFields.get(eachQualifyingNode) as ArrayList<String>){
//              print ("    comparing " + eachEligibleField + " with " + eachQualifyingField)
              if (eachEligibleField.startsWith(eachQualifyingField)){
                blnQualifyingChangesFound = true
//                print ("        Qualifying field match: " + eachQualifyingField)
              }
            }
          }
        }
      }
    }
    return blnQualifyingChangesFound
  }

  public function PolicyPeriodDiffPlugin_generateEndorsementReminderActivity_TDIC(passedInPolicyPeriod: PolicyPeriod, passedInEligibleImpactedPolicies: HashSet<PolicyPeriod>, passedInEligibleNodesAndFields: HashMap) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      for (eachEligibleImpactedPolicyPeriod in passedInEligibleImpactedPolicies) {
        var tmpLatestBoundPeriod = eachEligibleImpactedPolicyPeriod.Policy.LatestBoundPeriod
        if ((!tmpLatestBoundPeriod.Canceled and !(gw.api.util.DateUtil.currentDate() >=tmpLatestBoundPeriod.PeriodEnd))){
          if ((eachEligibleImpactedPolicyPeriod as PolicyPeriod).Policy != passedInPolicyPeriod.Policy){
            eachEligibleImpactedPolicyPeriod = bundle.add(eachEligibleImpactedPolicyPeriod as PolicyPeriod)
            var processDate = passedInPolicyPeriod.EditEffectiveDate
            if (passedInPolicyPeriod.EditEffectiveDate.compareIgnoreTime(gw.api.util.DateUtil.currentDate()) < 0){
              processDate = gw.api.util.DateUtil.currentDate()
            }
            var activityPattern = ActivityPattern.finder.getActivityPatternByCode("general_reminder")
            //var activitySubject = "Changes on Policy " + passedInPolicyPeriod.PolicyNumber + " impact Policy " + eachEligibleImpactedPolicyPeriod.PolicyNumber + " on " + gw.api.util.StringUtil.formatDate(passedInPolicyPeriod.EditEffectiveDate, "short")
            var activitySubject = "Endorsement request"
            //var activityDescription = "Please endorse this policy on " + gw.api.util.StringUtil.formatDate(processDate, "short") + ". If the endorsement(s) are not completed on that date, the changes will be picked up by a downstream policy transaction. See notes for all changes made."
            var activityDescription = "Changes were made on Policy" + " " + passedInPolicyPeriod.PolicyNumber + " " + "that may impact this Policy(see Note for details)."+" "+"Please endorse this Policy immediately on"+" "+ gw.api.util.StringUtil.formatDate(processDate, "short")+". If the endorsement is not completed on this date, this Policy will pick up the changes on the next policy transaction."
            var activity = activityPattern.createPolicyActivity(bundle, (eachEligibleImpactedPolicyPeriod as PolicyPeriod).Policy,
                activitySubject, activityDescription,
                null, Priority.TC_URGENT, true, processDate, processDate.addBusinessDays(1))
            //Note
            var note = activity.newNote()
            //print ("Note's Activity: " + note.Activity)
            note.Topic = typekey.NoteTopicType.TC_END_REQ_DETAILS_TDIC
            note.Subject = "This Policy was impacted by changes made on Policy: " + passedInPolicyPeriod.PolicyNumber + " effective on " + gw.api.util.StringUtil.formatDate(passedInPolicyPeriod.EditEffectiveDate, "short")
            //note.Subject = "Changes made to Policy" + " " + passedInPolicyPeriod.PolicyNumber + " " + "may be linked to this policy."
            var tmpNoteBody ="Details of changes that may need to be endorsed on this policy:\n\n"
            for (eachKey in passedInEligibleNodesAndFields.Keys) {
              tmpNoteBody += (eachKey + ":\n")
              for (eachValue in (passedInEligibleNodesAndFields.get(eachKey) as ArrayList<String>)) {
                var fieldName = eachValue.substring(0, eachValue.indexOf("|"))
                eachValue = eachValue.substring(eachValue.indexOf("|") + 1, eachValue.length)
                var valueBefore = eachValue.substring(0, eachValue.indexOf("|"))
                valueBefore = valueBefore == "" ? '<empty>' : "\"" + valueBefore + "\""
                eachValue = eachValue.substring(eachValue.indexOf("|") + 1, eachValue.length)
                var valueAfter = eachValue
                valueAfter = valueAfter == "" ? '<empty>' : "\"" + valueAfter + "\""
                tmpNoteBody += ( "\t    " + fieldName + " changed from " + valueBefore + " to " + valueAfter + "\n")
              }
            }
            tmpNoteBody += ("\nAll Policies impacted by the changes to policy " + passedInPolicyPeriod.PolicyNumber + ":\n\t")
            for (each in passedInEligibleImpactedPolicies) {
              var latestBoundPeriod = each.Policy.LatestBoundPeriod
              if (!latestBoundPeriod.Canceled and !(gw.api.util.DateUtil.currentDate() >= latestBoundPeriod.PeriodEnd)){
                tmpNoteBody += (" " + each.PolicyNumber + ",")
              }
            }
           /* passedInEligibleImpactedPolicies.each(\elt -> {
              tmpNoteBody += (" " + elt.PolicyNumber + ",")
            })*/
            tmpNoteBody = tmpNoteBody.chop()
            note.Body = tmpNoteBody

            //print ("\nActivitySubject: " + activitySubject)
            //print ("\nActivityDesc: " + activityDescription)
            //print ("\nNote Topic: " + note.Topic.Value)
            //print ("\nNote Subject: " + note.Subject)
            //print ("\nNote Body:    " + note.Body)
            var success = activity.assignUserAndDefaultGroup(User.util.CurrentUser)
          }
        }
      }
    })
    // end of bundle transaction
  }
}
