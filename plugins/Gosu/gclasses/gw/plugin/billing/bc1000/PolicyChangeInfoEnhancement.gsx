package gw.plugin.billing.bc1000

uses acc.onbase.util.LoggerFactory
uses wsi.remote.gw.webservice.bc.bc1000.entity.types.complex.PCContactInfo
uses wsi.remote.gw.webservice.bc.bc1000.entity.types.complex.PolicyChangeInfo
uses wsi.remote.gw.webservice.bc.bc1000.entity.anonymous.elements.PolicyChangeInfo_AdditionalNamedInsureds_TDIC
uses java.util.ArrayList

@Export
enhancement PolicyChangeInfoEnhancement : PolicyChangeInfo
{
  function syncPolicyChange(period : PolicyPeriod){
    commonSync(period)
    var contactInfo = new PCContactInfo()
    contactInfo.sync( period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact )
    this.PrimaryNamedInsuredContact.$TypeInstance = contactInfo
    /**
     * US368: PC to BC - Billing Instructions
     * 17/09/2014 Vicente
     *
     * Modification of the OOT functionality. All the additional contacts are included in PolicyChangeInfo in order to be
     * included in BC
     */
    var additionalNamedInsureds = new ArrayList<PCContactInfo>()
    var policyAdditionalNamedInsureds = period.PolicyContactRoles.whereTypeIs(PolicyAddlNamedInsured)
    for(p in policyAdditionalNamedInsureds){
      var policyContact = new PCContactInfo()
      policyContact.sync( p.ContactDenorm )
      additionalNamedInsureds.add( policyContact )
    }
    additionalNamedInsureds.each(\ p -> {
      var element = new PolicyChangeInfo_AdditionalNamedInsureds_TDIC()
      element.$TypeInstance = p
      this.AdditionalNamedInsureds_TDIC.add(element)
    })
  }

  function syncPolicyChangeForPreview(period : PolicyPeriod){
    commonSync(period)  
  }
  
  private function commonSync(period : PolicyPeriod){
    this.syncBasicPolicyInfo( period )
    this.Description = period.PolicyChange.Description
    this.JurisdictionCode = period.BaseState.Code
    this.PeriodStart = period.PeriodStart.XmlDateTime
    this.PeriodEnd = period.PeriodEnd.XmlDateTime
    if(period.GLLineExists and period.GLLine.GLServiceMembersCRADiscount_TDICExists) {
      this.ServiceMembCivilReliefAct_TDIC = true
    }
    /*GWPS-680 - RiskManagement For Polcy Change - Start*/
    if(period.GLLineExists and period?.GLLine?.GLRiskMgmtDiscount_TDICExists){
      this.RiskManagementDiscount_TDIC = true
      this.RiskManagementExpires_TDIC = period?.GLLine?.GLRiskMgmtDiscount_TDIC?.ExpirationDate?.XmlDateTime
    }
    //GWPS-2483 (To handle scenario where during NB if MultiDisc is False and during midterm
    // if it is added then on BC table the value for MultilineDisc is not getting set to True so added below mapping update the value to true)
    this.MultilineWC_TDIC = period.MultilineDiscount
    if (period.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_A) {
      this.MultilineAll_TDIC = true
    }
    if (period.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P) {
      this.MultilinePLCP_TDIC = true
    }
    if (period.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_W) {
      this.MultilineWCPL_TDIC = true
    }
    LoggerFactory.getLogger("Application.BillingAPI").info("PolicyChangeInfoEnhancment --------------- this.RiskManagementDiscount_TDIC:" + this.RiskManagementDiscount_TDIC)
    /*GWPS-680 - RiskManagement For Polcy Change - End*/
  }
}
