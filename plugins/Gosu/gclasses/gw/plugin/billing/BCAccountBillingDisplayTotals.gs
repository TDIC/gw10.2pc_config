package gw.plugin.billing

uses gw.pl.currency.MonetaryAmount
uses typekey.Currency
uses java.util.ArrayList
uses java.util.List

//20170807 TJT: GW-2848 (created from JAR file because this wasn't exposed for configuration
/**
 * Class calculating billing totals from different currencies using the default currency FX rate.
 *
 * Used for display purposes only.
 */
class BCAccountBillingDisplayTotals {
  var _infos: BillingAccountInfo[]
  var _currency: Currency

  /**
   * @param infos an array with summaries (in different currencies)
   * @param currency the currency to display amounts in
   */
  construct(infos: BillingAccountInfo[], currency: Currency) {
    _infos = infos
    _currency = currency
  }

  property get BilledOutstandingTotal(): AggregatedTotal {
    return new AggregatedTotal(_infos*.BilledOutstandingTotal)
  }

  /**
   * The total billed amount current
   */
  property get BilledOutstandingCurrent(): AggregatedTotal {
    return new AggregatedTotal(_infos*.BilledOutstandingCurrent)
  }

  /**
   * The total billed amount pastdue
   */
  property get BilledOutstandingPastDue(): AggregatedTotal {
    return new AggregatedTotal(_infos*.BilledOutstandingPastDue)
  }

  /**
   * The total unbilled amount
   */
  property get UnbilledTotal(): AggregatedTotal {
    return new AggregatedTotal(_infos*.UnbilledTotal)
  }

  /**
   * The total amount of fund unapplied
   */
  property get UnappliedFundsTotal(): AggregatedTotal {
    return new AggregatedTotal(_infos*.UnappliedFundsTotal)
  }

  /**
   * The total collateral requirement
   */
  property get CollateralRequirement(): AggregatedTotal {
    return new AggregatedTotal(_infos*.CollateralRequirement)
  }

  /**
   * The amount of collateral held
   */
  property get CollateralHeld(): AggregatedTotal {
    return new AggregatedTotal(_infos*.CollateralHeld)
  }

  /**
   * The amount of collateral charge billed
   */
  property get CollateralChargesBilled(): AggregatedTotal {
    return new AggregatedTotal(_infos*.CollateralChargesBilled)
  }

  /**
   * The amount of collateral charge past due
   */
  property get CollateralChargesPastDue(): AggregatedTotal {
    return new AggregatedTotal(_infos*.CollateralChargesPastDue)
  }

  /**
   * The amount of collateral charge unbilled
   */
  property get CollateralChargesUnbilled(): AggregatedTotal {
    return new AggregatedTotal(_infos*.BilledOutstandingCurrent)
  }

  property get Paid(): AggregatedTotal {
    return new AggregatedTotal(_infos*.Paid)
  }

  property get WrittenOff(): AggregatedTotal {
    return new AggregatedTotal(_infos*.WrittenOff)
  }

  /**
   * The account name
   */
  property get AccountName(): String {
    return _infos.IsEmpty ? null : _infos[0].AccountName
  }

  /**
   * Account numbers of PC accounts whose BC data was used to calculate the total amounts.
   */
  property get AccountNumbers(): String[] {
    return _infos*.AccountCurrencyGroupOwner.toSet().toTypedArray()
  }

  /**
   * True if the account is delinquent
   */
  property get Delinquent(): boolean {
    return _infos.where(\info -> info.Delinquent).HasElements
  }

  /**
   * Number of delinquency processes started recently or a negative number when the data is not available
   * The time period is configured in BC and by default it's last 12 months.
   */
  property get RecentDelinquencies(): int {
    return _infos.sum(\info -> info.DelinquenciesLast12Months)
  }

  /**
   * The primary payer contact
   */
  property get PrimaryPayer(): BillingContactInfo {
    return _infos.IsEmpty ? null : _infos[0].PrimaryPayer
  }

  /**
   * A flag indicating that "Account Balances" column totals were calculated using currency conversion
   */
  property get AccountBalancesConverted(): boolean {
    return BilledOutstandingTotal.Converted or
        BilledOutstandingCurrent.Converted or
        BilledOutstandingPastDue.Converted or
        UnbilledTotal.Converted or
        UnappliedFundsTotal.Converted
  }

  /**
   * A flag indicating that "Collateral" column totals were calculated using currency conversion
   */
  property get CollateralConverted(): boolean {
    return CollateralRequirement.Converted or
        CollateralHeld.Converted
  }
  //20170818 TJT: GW-2848
  property get WrittenOff_TDIC(): AggregatedTotal {
    var myList : List<MonetaryAmount> = new ArrayList<MonetaryAmount>()
    for (eachInfo in _infos){
      if (eachInfo typeis gw.plugin.billing.bc1000.BCAccountBillingSummaryWrapper){
        if (eachInfo.WrittenOff_TDIC != null){ //TJT: I tested this successfully without null safe code, but added this null check anyway
          myList.add(eachInfo.WrittenOff_TDIC)
        }
      }
    }
    return new AggregatedTotal(myList?.toTypedArray()) // TJT: I tested this with zero objects in the List successfully
  }
  /**
   * Information of Last Payment
   */
  property get LastPayment() : BillingAccountInfo.MoneyReceived {
    return _infos*.LastPaymentReceived.sortBy(\payment -> payment.Date).last()
  }

  /**
   * Next Invoice with date
   */
  property get NextInvoice(): NextInvoiceWrapper {
    var info = _infos.sortBy(\info -> info.NextInvoicesDueDate).first()
    return new NextInvoiceWrapper(info.NextInvoicesDue*.Amount, info.NextInvoicesDueDate)
  }

  /**
   * Inner class packing the total amount with a flag indicating whether currency conversion
   * is used while calculating it.
   */
  class AggregatedTotal {
    var _amounts: MonetaryAmount[]

    private construct(amounts: MonetaryAmount[]) {
      _amounts = amounts
    }

    /**
     * The total/aggregated amount
     */
    property get Total(): MonetaryAmount {
      return _amounts.toList().sumDifferentCurriences(_currency)
    }

    /**
     * Determines whether currency conversion would be used when aggregating
     */
    property get Converted(): boolean {
      return _amounts.hasMatch(\amount -> amount.Currency != _currency and amount.IsNotZero)
    }

    /**
     * Determines if the aggregated total consists of at least one valid (non-null) amount.
     */
    property get Exists(): boolean {
      return _amounts.hasMatch(\amount -> amount != null)
    }
  }

  class NextInvoiceWrapper {
    var _invoices: MonetaryAmount[] as Invoices
    var _invoiceDate: Date as NextInvoiceDate

    private construct(invoices: MonetaryAmount[], invoiceDate: Date) {
      _invoices = invoices
      _invoiceDate = invoiceDate
    }
  }
}