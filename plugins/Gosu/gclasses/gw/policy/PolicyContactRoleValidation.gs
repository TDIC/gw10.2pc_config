package gw.policy

uses gw.api.locale.DisplayKey
uses gw.util.GosuStringUtil
uses gw.validation.PCValidationBase
uses gw.validation.PCValidationContext
uses tdic.pc.config.rating.RatingConstants
uses typekey.Contact
uses entity.PolicyContactRole
uses typekey.Job


@Export
class PolicyContactRoleValidation extends PCValidationBase {

  var _policyRole : PolicyContactRole as PolicyRole

  construct(valContext : PCValidationContext, role : PolicyContactRole) {
    super(valContext)
    _policyRole = role
  }

  override protected function validateImpl() {
    Context.addToVisited( this, "validateImpl" )

    allAccountContactsAreActive()
    ValidatePersonSSN_TDIC()
    ValidateCompanyFEIN_TDIC()
  }

  function allAccountContactsAreActive() {
    Context.addToVisited(this, "allAccounContactsAreActive")
    if (Context.isAtLeast(TC_QUOTABLE)) {
      if (!PolicyRole.AccountContactRole.AccountContact.Active) {
        Result.addError(PolicyRole.Branch, TC_QUOTABLE, DisplayKey.get("Web.Policy.PolicyContact.Validation.NotActive", PolicyRole.DisplayName))
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to validte if the SSN of PNI person contact is blank
   * @create time: 6:49 PM 10/11/2019
    * @param null
   * @return:
   */

  function ValidatePersonSSN_TDIC() {
    Context.addToVisited(this, "ValidatePersonSSN_TDIC")

    var period = PolicyRole.Branch

    // flag is to remove warning and error from ssn for exsiting NW DIMS policies
    var flag = true
    if(period.isDIMSPolicy_TDIC || {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}.contains(period.BaseState.Code) ){
      if(period.Job.Subtype== Job.TC_SUBMISSION || period.Job.Subtype== Job.TC_RENEWAL || period.Job.Subtype== Job.TC_POLICYCHANGE )
        flag=false
    }

    if(PolicyRole.ContactDenorm.Subtype == Contact.TC_PERSON and PolicyRole.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED
        and (period?.GLLineExists and !(period?.isQuickQuote_TDIC or period?.Offering.CodeIdentifier == RatingConstants.plCyberOffering))){
      if (Level == ValidationLevel.TC_QUOTABLE) {
        if (GosuStringUtil.isBlank(PolicyRole.ContactDenorm?.SSNOfficialID) and flag){
          Result.addWarning(PolicyRole.Branch, TC_QUOTABLE, DisplayKey.get("TDIC.Web.Policy.PolicyContact.Validation.SSN"))
        }
      }
      if (Level == ValidationLevel.TC_BINDABLE or Level == ValidationLevel.TC_READYFORISSUE) {
        if (GosuStringUtil.isBlank(PolicyRole.ContactDenorm?.SSNOfficialID) and flag){
          Result.addError(PolicyRole.Branch, Level, DisplayKey.get("TDIC.Web.Policy.PolicyContact.Validation.SSN"))
        }
      }
    }
  }
    /*
   * create by: SureshB
   * @description: method to validte if the FEIN of PNI company contact is blank
   * @create time: 6:49 PM 10/11/2019
    * @param null
   * @return:
   */

  function ValidateCompanyFEIN_TDIC() {
    Context.addToVisited(this, "ValidateCompanyFEIN_TDIC")
    if(PolicyRole.ContactDenorm.Subtype == Contact.TC_COMPANY and PolicyRole.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED and PolicyRole.Branch?.BOPLineExists){
      if (Context.isAtLeast(TC_QUOTABLE)) {
        if (GosuStringUtil.isBlank(PolicyRole.ContactDenorm?.TaxID)){
          Result.addWarning(PolicyRole.Branch, TC_QUOTABLE, DisplayKey.get("TDIC.Web.Policy.PolicyContact.Validation.FEIN"))
        }
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to validate the PNI changes
   * @create time: 1:11 PM 10/14/2019
    * @param null
   * @return:
   */

  public static function validatePNIChanges_TDIC(policyContactRole : entity.PolicyContactRole, contactChanged : boolean) {
    if (contactChanged) {
      gw.validation.PCValidationContext.doPageLevelValidation(\context -> new PolicyContactRoleValidation(context, policyContactRole).validatePNIChange_TDIC())
    }
  }
  /*
   * create by: SureshB
   * @description: method to validate PNI changes
   * @create time: 1:11 PM 10/14/2019
    * @param null
   * @return:
   */

  function validatePNIChange_TDIC() {
    var policyPeriod = PolicyRole.Branch
    if (PolicyRole.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED
        and (policyPeriod.Job typeis PolicyChange
        or policyPeriod.Job typeis Renewal)) {
      var account = policyPeriod.Policy.Account
      if (policyPeriod.BOPLineExists) {
        var hasCyber = account.IssuedPolicies.hasMatch(\elt -> elt.fetchPolicyPeriod()?.Offering?.CodeIdentifier == "PLCyberLiab_TDIC")
        if(hasCyber) {
          Result.addWarning(PolicyRole, Context.Level, DisplayKey.get("TDIC.Web.Policy.PolicyContact.Validation.CPPNI"))
        }
      } else if(policyPeriod.GLLineExists and policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        var hasBOP = account.IssuedPolicies.hasMatch(\elt -> elt.fetchPolicyPeriod().BOPLineExists)
        if(hasBOP) {
          Result.addWarning(PolicyRole, Context.Level, DisplayKey.get("TDIC.Web.Policy.PolicyContact.Validation.PLPNI"))
        }
      }
    }
  }
}
