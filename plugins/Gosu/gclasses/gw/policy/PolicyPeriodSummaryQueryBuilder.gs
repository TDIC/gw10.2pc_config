package gw.policy

uses gw.api.database.ISelectQueryBuilder
uses gw.search.StringColumnRestrictor

class PolicyPeriodSummaryQueryBuilder extends PolicyPeriodQueryBuilderBase<PolicyPeriodSummary, PolicyPeriodSummaryQueryBuilder> {

  var _legacyPolicyNumber_TDIC : String
  var _legacyPolicyNumberRestrictor_TDIC : StringColumnRestrictor
  var _userTDIC : User as User_TDIC

  override protected property get SelectQueryBuilderType() : Type {
    return PolicyPeriod
  }

  function withUser_TDIC(_user : User) : PolicyPeriodQueryBuilderBase {
    _userTDIC = _user
    return this
  }


  function withLegacyPolicyNumberRestricted_TDIC(value : String, restrictor : StringColumnRestrictor) : PolicyPeriodSummaryQueryBuilder {
    _legacyPolicyNumber_TDIC = value
    _legacyPolicyNumberRestrictor_TDIC = restrictor
    return this
  }

  function withLegacyPolicyNumber_TDIC(value : String) : PolicyPeriodSummaryQueryBuilder {
    // Most string compares with our query builders will use EqualsIgnoringCase, but since the PolicyNumber column
    // doesn't support linguistic search, there's a performance degradation when ignoring case. Furthermore
    // PolicyNumber isn't really a place whereusers expect to ignore case as much as they might if they were searching
    // on names. Thus, we use Equals here.
    return withLegacyPolicyNumberRestricted_TDIC(value, ContainsIgnoringCase)
  }

  protected override function doRestrictQuery(selectQueryBuilder : ISelectQueryBuilder) {
    super.doRestrictQuery(selectQueryBuilder)

    if (_legacyPolicyNumber_TDIC.NotBlank) {
      _legacyPolicyNumberRestrictor_TDIC.restrict(selectQueryBuilder, PolicyPeriod#LegacyPolicyNumber_TDIC, _legacyPolicyNumber_TDIC)
    }

    if (User_TDIC != null) {
      //implement me
      /*if (this.User_TDIC != null and this.SearchObjectType == typekey.SearchObjectType.TC_POLICY){
        if (this.UserRole_TDIC == null){
          this.UserRole_TDIC = typekey.UserRole.TC_UNDERWRITER
        }
        var subQuery = new Query<PolicyPeriodSummary>(PolicyPeriodSummary)
        var t1 : Table<PolicyPeriodSummary> = subQuery.join("Job")
        var t2 : Table<PolicyPeriodSummary> = t1.join("Policy")
        var t3 : Table<PolicyPeriodSummary> = t2.subselect("ID", CompareIn, PolicyUserRoleAssignment,"Policy")
        t3.compare("AssignedUser", Equals, this.User_TDIC)
        t3.compare("Role", Equals, this.UserRole_TDIC)
        tmpResult = query.intersect(subQuery).select()
      } else if (this.User_TDIC != null and this.SearchObjectType != typekey.SearchObjectType.TC_POLICY){
        if (this.UserRole_TDIC == null){
          this.UserRole_TDIC = typekey.UserRole.TC_UNDERWRITER
        }
        var subQuery = new Query<PolicyPeriodSummary>(PolicyPeriodSummary)
        var tmpJoinedTable : Table<PolicyPeriodSummary> = subQuery.subselect("Job", CompareIn, JobUserRoleAssignment, "Job")
        tmpJoinedTable.compare("AssignedUser", Equals, this.User_TDIC)
        tmpJoinedTable.compare("Role", Equals, this.UserRole_TDIC)
        tmpResult = query.intersect(subQuery).select()
      } else {
        tmpResult = query.select()
      }*/
    }
  }
}