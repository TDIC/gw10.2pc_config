package gw.admin

uses gw.api.database.Query
uses gw.api.database.IQueryBeanResult
uses gw.api.locale.DisplayKey

enhancement GroupSearchCriteriaEnhancement : GroupSearchCriteria
{

  /** This is the minimally acceptable information for a query
  */
  property get MinimumCriteriaForSearch() : boolean {  
    return this.Organization != null
      or this.GroupName != null
      or this.GroupNameKanji != null
      or this.GroupType != null 
      or this.Organization != null 
      or this.ParentGroup != null
      or this.ProducerCode != null
      or this.BranchSearch
  }

  /* This will check that the minimally acceptable information has been supplied
  * and perform the query
  * otherwize it will throw an exception
  */
  function validateAndSearch() : IQueryBeanResult<Group> {
    if (this.MinimumCriteriaForSearch) {
      // explicitly filtering out root groups from search results if user sets GroupType == "Any" in UI
      if (this.GroupType == null) {
        this.ExcludeRootGroup = true
      }
     //return this.performSearch() as IQueryBeanResult<Group>
     return performExtendedGrpSearch_TDIC()
   }
   throw new gw.api.util.DisplayableException(DisplayKey.get("Web.GroupSearch.NotEnoughInfo"))
  }

  static function createBranchSearchCriteria() : GroupSearchCriteria {
    return createCriteria().asBranchSearch()
  }

  static function createCriteria() : GroupSearchCriteria {
    return new GroupSearchCriteria() { :Organization = User.util.CurrentUser.Producer, :ExcludeRootGroup = true}
  }
  //20180619 TJTalluto GW:3213, Guidewire Incident 00267048
  function performExtendedGrpSearch_TDIC() : IQueryBeanResult<Group> {
    var query = Query.make(Group)
    var orgQuery = Query.make(Organization)
    var carrierOrg = orgQuery.compare(Organization#Carrier, Equals, true).select().getAtMostOneRow()
    query.compare(Group#Organization, Equals, carrierOrg)
    return query.select()
  }
}
