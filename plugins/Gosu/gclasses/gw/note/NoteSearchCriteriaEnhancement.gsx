package gw.note

uses gw.api.database.IQueryBeanResult

enhancement NoteSearchCriteriaEnhancement : NoteSearchCriteria {
  function setSearchCriteria(activity: Activity) {
    if (this.RelatedTo == null or this.RelatedTo == activity) {
      this.RelatedTo = activity
      this.Activity = activity
      this.Policy = activity?.Policy
      this.Job = activity?.Job
    } else {
      this.Activity = null
    }
  }

  function getRelatedToSearchCriteria(policyPeriod: PolicyPeriod): Object[] {
    var related =
        (policyPeriod != null) ?
            this.getRelatedToOptionsForPolicyFile(policyPeriod) :
            this.getRelatedToOptionsForAccount()
    return getRelatedToSearchCriteria(related)
  }

  private function getRelatedToSearchCriteria(nullableRelated: Object[]): Object[] {
    var relatedToOptions = Optional
        .ofNullable(nullableRelated)
        .map(\arr -> arr.toList())
        .orElse({})
    if (this.Activity != null) {
      relatedToOptions.add(this.Activity)
    }
    return relatedToOptions.toTypedArray()
  }

  function initialize_TDIC(passedInAccount : entity.Account, passedInPolicyPeriod : entity.PolicyPeriod) : NoteSearchCriteria {
    this.Account = (passedInPolicyPeriod == null ? passedInAccount : null)
    this.Policy = passedInPolicyPeriod.Policy
   // this.RelatedTo = (passedInAccount != null ? passedInAccount : null) // Commented this line as part of GW-329
    return this
  }
   //Defect GW-329- Filter the Notes at Account Level and Policy level from OOTB performSearch() operation.
  function performFinalNoteSearch(): IQueryBeanResult<Note> {

    //The result to get calling perform OOTB method
    var results = this.performSearch()
    //find the Notes matching Account and Policy
    if(this.RelatedTo!=null){
      for(notes in results){
        if(this.RelatedTo==notes.Account){
          results=results.where( \ elt -> elt.Account==this.Account) as gw.api.database.IQueryBeanResult<entity.Note>
        } else if(this.RelatedTo==notes.Policy){
          results=results.where( \ elt -> elt.Policy==this.Policy) as gw.api.database.IQueryBeanResult<entity.Note>
        }
      }
    }

    return results
  }
}