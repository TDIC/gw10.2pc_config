package gw.job

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.job.EffectiveDateCalculator
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Key
uses gw.plugin.Plugins
uses gw.plugin.notification.INotificationPlugin

enhancement CancellationEnhancement : Cancellation {
  /**
   * Returns the last date of the underwriting period for "policyPeriod". The
   * underwriting period starts from the period effective date. Its length
   * is the smallest value of UWPeriod among all NotificationConfigs that match the
   * period's states and lines.
   */
  static function findUWPeriodEnd(inForcePeriod : PolicyPeriod) : Date {

    // We need to find the period to use for the start of the UW period.
    // We do this by starting with the inForcePeriod and walking through the
    // BasedOn periods until we find a Submission or Rewrite.  To avoid pulling alot
    // of PolicyPeriods into memory, we just query for the few values we actually need.
    var query = Query.make(PolicyPeriod)
    query.compare("Policy", Relop.Equals, inForcePeriod.Policy)

    // NOTE pdalbora 6-May-2011 -- The following line ensures that inForcePeriod is in the
    // results. It won't be in the results if inForcePeriod has not yet been committed as
    // a non-temporary branch (i.e., the first commit after it's initially created), because the
    // query layer now excludes temporary branches by default.
    query.withFindTemporaryBranches(true)

    var periodInfos = query.select()

    // Map PolicyPeriod ids to the values returned by the query.
    var periodInfoMap = periodInfos
        .partitionUniquely(\periodInfo -> periodInfo.ID as Key)

    // find the Submission or Renewal PolicyPeriod
    var periodId = inForcePeriod.ID
    var startDate : Date = null
    while (startDate == null) {
      var periodInfo = periodInfoMap.get(periodId)
      if (periodInfo == null) {
        throw DisplayKey.get("Java.PolicyPeriod.Error.MissingId", periodId)
      }
      var jobType = periodInfo.Job.Subtype as typekey.Job

      if (jobType == typekey.Job.TC_SUBMISSION or
          jobType == typekey.Job.TC_REWRITENEWACCOUNT or
          jobType == typekey.Job.TC_REWRITE) {
        startDate = periodInfo.PeriodStart as Date
      } else {
        periodId = periodInfo.BasedOn.ID as Key
        if (periodId == null or periodInfoMap.get(periodId).Job.Subtype == null) {
          //conversion on renewal has a "BaseOn" period with jobType == null.
          if (jobType == typekey.Job.TC_RENEWAL) {
            startDate = periodInfo.PeriodStart as Date
          } else {
            throw DisplayKey.get("Java.PolicyPeriod.Error.MissingId", periodId)
          }
        }
      }
    }

    //  Find the length of the UW period.  Do the lookup using inForcePeriod.
    var notificationPlugin = Plugins.get(INotificationPlugin)
    var lineToJurisdictions = inForcePeriod.AllPolicyLinePatternsAndJurisdictions
    var uwPeriodLength = notificationPlugin.getMinimumLeadTime(
        inForcePeriod.PeriodStart, lineToJurisdictions, NotificationActionType.TC_UWPERIOD)

    return startDate.addDays(uwPeriodLength)
  }

  /**
   * Calculates the CalculationMethod for the refund based on the CancelReasonCode.
   * If the CancelReasonCode is null, the CalculationMethod will also be null.
   * Otherwise, it will be the first valid refund calculation method found by
   * findValidRefundMethods().
   */
  function calculateRefundCalcMethod(inForcePeriod : PolicyPeriod) : CalculationMethod {
    if (this.CancelReasonCode != null) {
      // 20150228 TJ Talluto US473 - this should be sorted by priority, but the typelist can't be modified.  Sorting by name instead.
      //return findValidRefundMethods(inForcePeriod.PeriodStart).sortByDescending(\elt -> elt.Description). first()
      //02/01/2020 Britto S- included Other LoBs
      return findValidRefundMethods_TDIC(inForcePeriod).sortBy(\elt -> elt.Description).first()
    } else {
      return null
    }
  }

  function canEnterInitialEffectiveDate(inForcePeriod : PolicyPeriod, defaultEffectiveDate : Date) : boolean {
    if (inForcePeriod.Locked or inForcePeriod.Job.IntrinsicType != Cancellation) {
      var refundCalcMethod = calculateRefundCalcMethod(inForcePeriod)
      return (perm.System.cancelovereffdate)
          and refundCalcMethod != null
          and refundCalcMethod != TC_FLAT
          and defaultEffectiveDate != null
    }
    return inForcePeriod.CancellationProcess.canEditCancellationDate().Okay
  }

  function canEnterInitialEffectiveDate(inForcePeriod : PolicyPeriod, defaultEffectiveDate : Date, passedInRefundCalcMethod : CalculationMethod) : boolean {
    // 20150115 TJ Talluto: US473 - changed the signature to include passed in RefundCalcMethod (otherwise it will always pick the first from a query)
    if (inForcePeriod.Locked or inForcePeriod.Job.IntrinsicType != Cancellation) {
      var refundCalcMethod = passedInRefundCalcMethod == null ? calculateRefundCalcMethod(inForcePeriod) : passedInRefundCalcMethod
      return (perm.System.cancelovereffdate)
          and refundCalcMethod != null
          and refundCalcMethod != TC_FLAT
          //and defaultEffectiveDate != null
          and ((this.Source == CancellationSource.TC_INSURED) or (this.Source != CancellationSource.TC_INSURED and defaultEffectiveDate != null))
    }
    return inForcePeriod.CancellationProcess.canEditCancellationDate().Okay
  }
  //BrittoS 03/09/2020 - enabling for Insured & Carrier
  function canEnterInitialEffectiveDate_TDIC(inForcePeriod : PolicyPeriod, defaultEffectiveDate : Date, passedInRefundCalcMethod : CalculationMethod) : boolean {
    if (inForcePeriod.Locked or inForcePeriod.Job.IntrinsicType != Cancellation) {
      var refundCalcMethod = passedInRefundCalcMethod == null ? calculateRefundCalcMethod(inForcePeriod) : passedInRefundCalcMethod
      return (perm.System.cancelovereffdate)
          and refundCalcMethod != null
          and refundCalcMethod != TC_FLAT
    }
    return inForcePeriod.CancellationProcess.canEditCancellationDate().Okay
  }

  function canEditEffectiveDate(inForcePeriod : PolicyPeriod) : boolean {
    if (inForcePeriod.CancellationProcess == null) {
      return false;
    }
    return inForcePeriod.CancellationProcess.canEditCancellationDate().Okay
  }

  function findValidRefundMethods(date : Date) : CalculationMethod[] {
    var resultQuery = Query.make(CancelRefund)
    resultQuery.compare(CancelRefund#EffectiveDate.PropertyInfo.Name, LessThan, date)
    resultQuery.compare(CancelRefund#ReasonCode.PropertyInfo.Name, Equals, this.CancelReasonCode)
    resultQuery.or(\restriction -> {
      restriction.compare(CancelRefund#ExpirationDate.PropertyInfo.Name, Equals, null)
      restriction.compare(CancelRefund#ExpirationDate.PropertyInfo.Name, GreaterThan, date)
    })
    return resultQuery.select().map(\c -> c.CalculationMethod).toSet().toTypedArray()
  }

  /**
   * 01/02/2020 Britto S
   *
   * @param inForcePeriod
   * @return
   */
  function findValidRefundMethods_TDIC(inForcePeriod : PolicyPeriod) : CalculationMethod[] {
    var date = inForcePeriod.PeriodStart
    var resultQuery = Query.make(CancelRefund)
    resultQuery.compare(CancelRefund#EffectiveDate.PropertyInfo.Name, LessThan, date)
    resultQuery.compare(CancelRefund#ReasonCode.PropertyInfo.Name, Equals, this.CancelReasonCode)

    // Jeff Lin, 1/17/2020, 3:14 pm, Defect GPC-2591, RefunMethod not correct
    resultQuery.or(\restriction -> {
      restriction.compare(CancelRefund#LineOfBusiness_TDIC.PropertyInfo.Name, Equals, null)
      if (inForcePeriod.BOPLineExists) {
        restriction.compare(CancelRefund#LineOfBusiness_TDIC.PropertyInfo.Name, Equals, inForcePeriod.BOPLine.PatternCode)
      } else if (inForcePeriod.GLLineExists) {
        restriction.compare(CancelRefund#LineOfBusiness_TDIC.PropertyInfo.Name, Equals, inForcePeriod.GLLine.PatternCode)
      } else if (inForcePeriod.WC7LineExists) {
        restriction.compare(CancelRefund#LineOfBusiness_TDIC.PropertyInfo.Name, Equals, inForcePeriod.WC7Line.PatternCode)
      }
    })
    resultQuery.or(\restriction -> {
      restriction.compare(CancelRefund#ExpirationDate.PropertyInfo.Name, Equals, null)
      restriction.compare(CancelRefund#ExpirationDate.PropertyInfo.Name, GreaterThan, date)
    })
    var methods = resultQuery.select().map(\c -> c.CalculationMethod).toSet().toTypedArray()
    return filterRefundMethods_TDIC(inForcePeriod, methods)
  }

  /**
   * Returns the default effective date for a new cancellation, including a time
   * component as determined by the effective time plugin. If the policy is canceled
   * flat, returns the PeriodStart; otherwise calls the private helper method
   * {@link calcDefaultEffectiveDate} to determine the day, then sets the time
   * component.
   */
  function getDefaultEffectiveDate(inForcePeriod : PolicyPeriod, refundCalcMethod : CalculationMethod) : Date {
    //Britto S 03/09/2020 - If the source is Carrier
    if (this.Source == CancellationSource.TC_CARRIER and refundCalcMethod != typekey.CalculationMethod.TC_FLAT) {
      if (this.CancelReasonCode == ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC) {
        var periodStart = inForcePeriod.PeriodStart
        return DateUtil.createDateInstance(periodStart.MonthOfYear, periodStart.DayOfMonth, periodStart.YearOfDate).addDays(59)
      } else {
        return null
      }
    }
    //20150128 TJ Talluto - US473 if the source is Insured, don't return a date.  Otherwise, let OOTB recommend a value.
    if (this.Source == typekey.CancellationSource.TC_INSURED and refundCalcMethod != typekey.CalculationMethod.TC_FLAT) {
      return null
    }
    return EffectiveDateCalculator.instance().getCancellationEffectiveDate(calcDefaultEffectiveDate(inForcePeriod, refundCalcMethod), inForcePeriod, this, refundCalcMethod)
  }

  /**
   * Returns the earliest effective date for a new cancellation, including a time
   * component as determined by the effective time plugin. If the policy is canceled
   * flat, returns the PeriodStart; otherwise calls the private helper method
   * {@link calcDefaultEffectiveDate} to determine the day, then sets the time
   * component.
   */
  function getEarliestEffectiveDate(inForcePeriod : PolicyPeriod, refundCalcMethod : CalculationMethod) : Date {
    return EffectiveDateCalculator.instance().getCancellationEffectiveDate(calcEarliestEffectiveDate(inForcePeriod, refundCalcMethod), inForcePeriod, this, refundCalcMethod)
  }

  function validateEffectiveDate(inForcePeriod : PolicyPeriod, newEffectiveDate : Date, refundCalcMethod : CalculationMethod) : String {
    var checkConditionFromPlugin = inForcePeriod.Policy.canStartCancellation(newEffectiveDate)
    if (checkConditionFromPlugin != null) {
      return checkConditionFromPlugin
    } else {
      var formatString = DisplayKey.get("Web.CancellationSetup.Error.DateFormatString")
      if (this.CancelReasonCode != null) {
        var earliestDate = getEarliestEffectiveDate(inForcePeriod, refundCalcMethod)
        //20160503 TJT: GW-1692 - the black box doesn't allow ShortRate to be backdated when Manually Migrating.  It doesn't allow the
        // user to cancel a policy that is already expired/cancelled.  I'm circumventing the earliest start date calc, so that the manual
        // migration user can backdate a ShortRate cancellation to any historical term.  Fix is in two places: here and CancellationProcess.gs
        if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_MANUALMIGRATION as String) {
          if (refundCalcMethod == typekey.CalculationMethod.TC_SHORTRATE) {
            earliestDate = inForcePeriod.Policy.findEarliestPeriodStart()
          }
        }
        if (newEffectiveDate < earliestDate) {
          var effDateStr = newEffectiveDate.format(formatString)
          var earliestDateStr = earliestDate.format(formatString)
          return DisplayKey.get("Web.CancellationSetup.Error.EffectiveDateTooEarly", effDateStr, earliestDateStr)
        } else {
          return null
        }
      }
      return null
    }
  }

  /**
   * Returns the default effective date for a new cancellation. The cancellation is
   * represented by a CancellationSetup (which includes the PolicyPeriod to be
   * canceled and the cancellation source) and a ReasonCode. This implementation
   * first computes the earliest and latest possible effective dates. The default
   * will be today's date if it falls within the range of valid dates; otherwise
   * the default will be the earliest possible effective date.
   */
  private function calcDefaultEffectiveDate(inForcePeriod : PolicyPeriod, refundCalcMethod : CalculationMethod) : Date {
    // Find the range of valid dates for the cancellation
    var earliestEffDate = calcEarliestEffectiveDate(inForcePeriod, refundCalcMethod)
    var latestEffDate = getLatestEffectiveDate(inForcePeriod, refundCalcMethod)

    // Return today's date if it's within the valid date range
    var todaysDate = Date.CurrentDate
    if (earliestEffDate < todaysDate and todaysDate < latestEffDate) {
      return todaysDate
    }

    // Otherwise return the earliest possible effective date
    return earliestEffDate
  }

  /**
   * Return the earliest possible effective date for a cancellation.  If the cancellation can
   * be started at the period start, return the period start.  Otherwise, calculate the maximum
   * lead time and add that to the processing date.
   */
  private function calcEarliestEffectiveDate(inForcePeriod : PolicyPeriod, refundCalcMethod : CalculationMethod) : Date {
    if (canCancelFromPeriodStart(inForcePeriod, refundCalcMethod)) {
      return inForcePeriod.PeriodStart
    }
    var initialProcessingDate = this.InitialNotificationDate != null ? this.InitialNotificationDate : Date.CurrentDate
    var leadTimeCalculator = new CancellationLeadTimeCalculator(this.CancelReasonCode,
        inForcePeriod.AllPolicyLinePatternsAndJurisdictions,
        initialProcessingDate,
        //initialProcessingDate <= findUWPeriodEnd(inForcePeriod))
        initialProcessingDate < findUWPeriodEnd(inForcePeriod).addDays(-1)) //GW-584 : The policy, which is effective ON or BEFORE
    //initialProcessingDate, is not considered as a policy
    //with in UW Period
    var leadTime = leadTimeCalculator.calculateMaximumLeadTime()
    if (leadTime == null) {
      // the correct behavior would be to set leadTime to 0, but to maintain backward compatibility,
      // return the period start
      return inForcePeriod.PeriodStart
    }
    // The earliest effective date is initial processing date plus the lead time, but not before the period start
    var earliestEffDate = initialProcessingDate.addDays(leadTime)
    return earliestEffDate < inForcePeriod.PeriodStart ? inForcePeriod.PeriodStart : earliestEffDate
  }

  /**
   * Flat refunds, rewrites, insured cancellations and bound non-issued policies can cancel from the period start.
   */
  protected function canCancelFromPeriodStart(period : PolicyPeriod, refundCalcMethod : CalculationMethod) : boolean {
    return refundCalcMethod == TC_FLAT
        or this.CancelReasonCode == TC_FLATREWRITE
        or this.CancelReasonCode == TC_MIDTERMREWRITE
        or this.CancelReasonCode == typekey.ReasonCode.TC_REWRITTEN_TDIC
        or this.CancelReasonCode == typekey.ReasonCode.TC_OTHER_TDIC //20150521 TJ Talluto - US1376
        or this.Source == TC_INSURED
        or period.Policy.IssueDate == null
  }

  /**
   * Returns the latest possible effective date for a cancellation. This is computed
   * from the PolicyPeriod to be canceled and the reason for cancellation. Rewrites
   * and flat-rate cancellations cannot be performed after the period's period
   * effective date, but all other cancellation can be done until the end of the period.
   */
  function getLatestEffectiveDate(inForcePeriod : PolicyPeriod, refundCalcMethod : CalculationMethod) : Date {
    if (this.CancelReasonCode == TC_FLATREWRITE or refundCalcMethod == TC_FLAT) {
      return inForcePeriod.PeriodStart
    } else {
      return inForcePeriod.PeriodEnd
    }
  }

  /**
   * GW235 9/28/2015
   * Hermia kho add 'TC_NOTTAKEN' to the if condition
   */

  function TDIC_filterReasonCodesForCancellation(passedInReasonCode : typekey.ReasonCode) : boolean {
    //20150122 TJ Talluto US473
    // I wasn't able to remove the CancellationReason categories from the two typecodes below, so they are being
    // filtered out of the UI using this function with hardcoded comparison values
    //20160429 TJ Talluto GW-1692 the nonmayment reason must be exposed on the UI during manual migration mode only
    var tmpReasonCode = true
    if (this.Source == null) {
      tmpReasonCode = false
    } else {
      if (passedInReasonCode == typekey.ReasonCode.TC_MIDTERMREWRITE or
          passedInReasonCode == typekey.ReasonCode.TC_FLATREWRITE or
          (passedInReasonCode == typekey.ReasonCode.TC_NONPAYMENT and this.DataSource_TDIC == typekey.DataSource_TDIC.TC_POLICYCENTER) or
          passedInReasonCode == typekey.ReasonCode.TC_NOTTAKEN) {

        tmpReasonCode = false
      }
    }
    return tmpReasonCode
  }

  private function filterRefundMethods_TDIC(inForcePeriod : PolicyPeriod, methods : CalculationMethod[]) : CalculationMethod[] {
    if(inForcePeriod.GLLineExists or inForcePeriod.BOPLineExists) {
      if(this.Source == CancellationSource.TC_CARRIER) {
        switch(this.CancelReasonCode) {
          case ReasonCode.TC_REWRITTEN_TDIC :
            return methods.where(\elt -> elt != CalculationMethod.TC_PRORATA)
        }
      }
    }
    return methods
  }
}
