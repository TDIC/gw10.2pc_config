package gw.pcf.contacts

/**
 * Helper class for AdditionalInsuredsDV.pcf
 */
@Export
class AdditionalInsuredsDVUIHelper {
  static function onAdditionalInsuredTypeChange(additionalInsuredDetail: PolicyAddlInsuredDetail) {
    // Clear the additional information text cell if not required
    if (not additionalInformationRequired(additionalInsuredDetail)) {
      additionalInsuredDetail.AdditionalInformation = null
    }
  }

  static function additionalInformationRequired(additionalInsuredDetail: PolicyAddlInsuredDetail): boolean {
    return additionalInsuredDetail.AdditionalInformationType != null
  }
  /*
   * create by: SureshB
   * @description: method to filter the Additional Insured Type dropdown values
   * @create time: 6:34 PM 10/10/2019
    * @param PolicyPeriod
   * @return: List<AdditionalInsuredType>
   */

  static function filterAdditionalInsuredTypes_TDIC(policyPeriod:PolicyPeriod) : List<AdditionalInsuredType>{
    var filteredAddionalInsuredTypes = new ArrayList<AdditionalInsuredType>()
    if(policyPeriod.BOPLineExists){
      if(policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
        filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_LESSEQUIP)
        filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_MGRPREM)
      }
      if(policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" || policyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
        filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_OTHER)
      }
    }
    if(policyPeriod.GLLineExists){
      filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_LESSEQUIP)
      filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_MGRPREM)
      filteredAddionalInsuredTypes.add(AdditionalInsuredType.TC_OTHER)
    }
    return filteredAddionalInsuredTypes
  }
}