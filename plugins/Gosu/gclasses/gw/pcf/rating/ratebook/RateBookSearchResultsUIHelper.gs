package gw.pcf.rating.ratebook

uses gw.api.util.DisplayableException
uses pcf.RateBookMergePopup
uses gw.api.locale.DisplayKey

@Export
class RateBookSearchResultsUIHelper {


  function beginMerge(books : RateBook[]) {
    validateMerge(books)
    RateBookMergePopup.push(books[0], books[1])
  }

  function delete(books : RateBook[]) {
   validateDelete(books)
    books.each(\cv ->cv.removeRateBook())
    gw.transaction.Transaction.getCurrent().commit()
  }

  function validateMerge(books : RateBook[]) {
    if (books.Count != 2) {
      throw new DisplayableException(DisplayKey.get("Web.Rating.RateBooks.Errors.MergeValidation.MustSelectTwoRateBooks"))
    }

    //If it gets to this line there should be only two RateBooks in the list
    if (books[0].PolicyLine != books[1].PolicyLine) {
      throw new DisplayableException(DisplayKey.get("Web.Rating.RateBooks.Errors.MergeValidation.MustBeSamePolicyLine"))
    }
  }

  function validateDelete(books: RateBook[]) {
    if (books.where( \ elt -> elt.Status != TC_DRAFT).HasElements) {
      throw new DisplayableException(DisplayKey.get("Web.Rating.RateBooks.Errors.DeleteValidation.CanOnlyDeleteDraftBooks"))
    }
    if (books.where(\book -> book.ExportLock).HasElements) {
      throw new DisplayableException(DisplayKey.get("Web.Rating.RateBooks.Errors.DeleteValidation.CannotDeleteBooksLockedForExport"))
    }
  }

  function changeStatusToDraft_TDIC(books : RateBook[]) {
    validateStatusForDraftRB_TDIC(books)
    books.each(\cv -> {
      var ratebookHelper = new gw.pcf.rating.ratebook.RateBookDetailsScreenUIHelper(cv)
      ratebookHelper.processInTx(\rbook -> rbook.changeStatusTo(RateBookStatus.TC_DRAFT, true))
    })
    gw.transaction.Transaction.getCurrent().commit()
  }

  function changeStatusToStage_TDIC(books : RateBook[]) {
    validateStatusForStagedRB_TDIC(books)
    books.each(\cv -> {
      var ratebookHelper = new gw.pcf.rating.ratebook.RateBookDetailsScreenUIHelper(cv)
      ratebookHelper.processInTx(\rbook -> rbook.changeStatusTo(RateBookStatus.TC_STAGE, true))
    })
    gw.transaction.Transaction.getCurrent().commit()
  }

  function validateStatusForDraftRB_TDIC(books: RateBook[]) {
    if (books.where( \ elt -> elt.Status != RateBookStatus.TC_STAGE).HasElements) {
      throw new DisplayableException("Cannot change staus to Draft as ratebook is already in drafts status")
    }
    if (books.where(\book -> book.ExportLock).HasElements) {
      throw new DisplayableException("Cannot Change Status")
    }
  }

  function validateStatusForStagedRB_TDIC(books: RateBook[]) {
    if (books.where( \ elt -> elt.Status != RateBookStatus.TC_DRAFT).HasElements) {
      throw new DisplayableException("Cannot change staus to Stage as ratebook is already in Stage status")
    }
    if (books.where(\book -> book.ExportLock).HasElements) {
      throw new DisplayableException("Cannot Change Status")
    }
  }

  function exportToXML_TDIC(books : RateBook[]) {
    if (books.where(\book -> book.ExportLock).HasElements
        and books.where(\elt -> elt.Status==RateBookStatus.TC_DRAFT ||  elt.Status==RateBookStatus.TC_ACTIVE).HasElements) {
      throw new DisplayableException("Cannot Export non Staged ratebooks")
    }
    books.each(\rb -> {
      var exporter = new gw.rating.rtm.RateBookBatchExport()
      exporter.start(rb, RateBookExportType.TC_XML)
    })
  }

 /*
  * create by: AnkitaG
  * @description: function to export selected rateboook to excel
  * @create time:  9/19/2019
   * @param books: list of ratbooks
  * @return: null
  */

  function exportToSpreadsheet_TDIC(books : RateBook[]) {
    if (books.where(\book -> book.ExportLock).HasElements
        and books.where(\elt -> elt.Status == RateBookStatus.TC_DRAFT || elt.Status == RateBookStatus.TC_ACTIVE).HasElements) {
      throw new DisplayableException("Cannot Export non Staged ratebooks")
    }
    books.each(\rb -> {
      var exporter = new gw.rating.rtm.RateBookBatchExport()
      exporter.start(rb, RateBookExportType.TC_EXCEL)
    })
  }
}