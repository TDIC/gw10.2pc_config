package gw.pcf.coverage

uses gw.api.domain.covterm.DirectCovTerm
uses gw.api.locale.DisplayKey

uses java.lang.Double
uses java.text.DecimalFormat

uses gw.api.util.PCNumberFormatUtil
uses gw.api.upgrade.Coercions

@Export
class CovTermDirectInputSetHelper {

  public static function validate(covTerm : DirectCovTerm): String {
    if (covTerm == null) {
      return DisplayKey.get("Java.Validation.NonNullable", new Object[]{"Term"})
    } else {
      return covTerm.validateValueInRange(covTerm.Value);
    }
  }

  public static function convertFromString(value: String): Object {
    return PCNumberFormatUtil.parse(value)
  }

  public static function convertToString(value: Object): String {
    return PCNumberFormatUtil.render(Coercions.makeDoubleFrom(value))
  }
  /*
   * create by: SureshB
   * @description: method to get the display value of Currency CovTerms
   * @create time: 4:52 PM 12/2/2019
    * @param val : Object, directCovTerm : gw.api.domain.covterm.DirectCovTerm
   * @return: String
   */

  static function getOutputConversion_TDIC(val : Object, directCovTerm : gw.api.domain.covterm.DirectCovTerm) : String {
    var covTermArray = {"BOPEQZipCode_TDIC", "BOPEQSLZipCode_TDIC" }
    if (not covTermArray.contains(directCovTerm.Pattern.CodeIdentifier)) {
      var finalValue =  convertToString(val)
      return finalValue
    }
    var decimalFormat = new DecimalFormat("00000")
    return (val != null ? decimalFormat.format(val) : null)
  }
}
