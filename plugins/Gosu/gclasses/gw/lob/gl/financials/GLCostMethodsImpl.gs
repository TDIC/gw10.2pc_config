package gw.lob.gl.financials

@Export
class GLCostMethodsImpl extends GenericGLCostMethodsImpl<GLCost>
{
  construct( owner : GLCost)
  {
    super( owner )
  }
  
  override property get OwningCoverable() : Coverable {
    return Cost.GeneralLiabilityLine
  }

  override property get State() : Jurisdiction {
    return Cost.GeneralLiabilityLine.BaseState
  }
}
