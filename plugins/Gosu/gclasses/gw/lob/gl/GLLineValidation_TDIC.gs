package gw.lob.gl

uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.validation.PCValidationContext
uses tdic.pc.config.gl.historicaldata.GLHistoryHelper
uses typekey.Job

uses java.math.BigDecimal

/**
 * TDIC implementation for GL Validations
 */
class GLLineValidation_TDIC extends GLLineValidation {

  construct(valContext : PCValidationContext, polLine : entity.GeneralLiabilityLine) {
    super(valContext, polLine)
  }

  /**
   * Screen level validation before save
   */
  static function validateDiscounts(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation_TDIC(context, line).checkDiscountSelections())
  }

  static function validateOffering(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation_TDIC(context, line).checkOffering())
  }

  static function validateHistoricalParams(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation_TDIC(context, line).checkHistoricalParams())
  }

  public static function validateGLScreenBeforeSave(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> {
      validateCYBLimitUWQuestions_TDIC(line)
      validateAddIns_TDIC(line)

      var val = new GLLineValidation_TDIC(context, line)
      val.checkDiscountSelections()
      val.validateAdditionalCoverages()
      val.validateRetroDateforDEPL()

    })
  }
  /**
   * create by: ChitraK
   *
   * @description: Validate and remove Additional Insured
   * @create time: 5:24 PM 10/21/2019
   * @return:
   */

  static function validateAddIns_TDIC(line : GLLine) {
    var i : int = 0
    if (!line.GLAdditionalInsuredCov_TDICExists && line.GLAdditionInsdSched_TDIC.Count > 0) {
      while (i < line.GLAdditionInsdSched_TDIC.Count) {
        for (addtlInsured in line.AdditionalInsureds*.PolicyAdditionalInsuredDetails) {
          addtlInsured.PolicyAddlInsured.removeDetail(line.GLAdditionInsdSched_TDIC[i].AdditionalInsured)
        }
        i = i + 1;
      }
    }


    var k : int = 0
    if (!line.GLSchoolServicesCov_TDICExists && line.GLSchoolServSched_TDIC.Count > 0) {
      while (k < line.GLSchoolServSched_TDIC.Count) {
        for (addtlInsured in line.AdditionalInsureds*.PolicyAdditionalInsuredDetails) {
          addtlInsured.PolicyAddlInsured.removeDetail(line.GLSchoolServSched_TDIC[k].AddlInsured)
        }
        k = k + 1;
      }
    }

    var m : int = 0
    if (!line.GLBLAICov_TDICExists && line.GLDentalBLAISched_TDIC.Count > 0) {
      while (m < line.GLDentalBLAISched_TDIC.Count) {
        for (addtlInsured in line.AdditionalInsureds*.PolicyAdditionalInsuredDetails) {
          addtlInsured.PolicyAddlInsured.removeDetail(line.GLDentalBLAISched_TDIC[m].AdditionalInsured)
        }
        m = m + 1;
      }
    }
  }

  override function doValidate() {
    Context.addToVisited(this, "doValidate")
    super.doValidate()
    checkOffering()
    checkDiscountSelections()
    checkHistoricalParams()
    discountValidationsOnQuote()
    validateClassCode()
    validateCyberAlreadyExists()
    validateBaseStateAndPrimaryNamedIsuredState()
    validateCyberPolicyWithBOP()
    validateCYBLimitsWithUWQuestions()
    validateIdentityTheftRecovery()
    validateRetroDateforDEPL()
    validateGLManuscriptTDIC()
  }

  function validateRetroDateforDEPL() {
       var period = this.Line.Branch

    if (period.GLLineExists && period.GLLine.GLDentalEmpPracLiabCov_TDIC.HasGLDEPLRetroDate_TDICTerm) {
      var retroDate = this.Line?.GLDentalEmpPracLiabCov_TDIC?.CovTerms?.firstWhere(\elt -> elt.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC")?.DisplayValue.toDate()

      var TransEffDate = period.EditEffectiveDate
      if (retroDate != null) {
        var _01_01_1998 = DateUtil.createDateInstance(1, 1, 1998)
        var _07_01_1998 = DateUtil.createDateInstance(7, 1, 1998)
        var _07_01_1998_States = {Jurisdiction.TC_CA, Jurisdiction.TC_AZ, Jurisdiction.TC_NV}

        if( Level==ValidationLevel.TC_DEFAULT || Level==ValidationLevel.TC_QUOTABLE) {
          if (_07_01_1998_States.contains(period.BaseState) and retroDate.before(_07_01_1998)) {
            Result.addWarning(glLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _07_01_1998, period.BaseState))
          } else if (retroDate.before(_01_01_1998)) {
            Result.addWarning(glLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _01_01_1998, period.BaseState))
          }
          if (retroDate.after(TransEffDate)) {
            Result.addError(glLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.AfterDate"))

          }
        }
      }
    }
  }
  /**
   * GPC-891 PL Risk Analysis - UW Issues, Prior Policies, Claims and Prior Losses
   */
  function checkHistoricalParams() {
    if (GLHistoryHelper.canApplyHistory(glLine.Branch)) {
      //var historicalParams = glLine.Branch.GLHistoricalParameters_TDIC.where(\elt -> elt.ExpirationDate.beforeOrEqual(glLine.Branch.PeriodStart))
      var historicalParams = tdic.pc.config.gl.historicaldata.GLHistoryHelper.getHistoryFor(glLine.Branch)
      if (historicalParams.HasElements) {
        var sortedParams = historicalParams.sortByDescending(\p -> p.EffectiveDate)
        for (hp in sortedParams index i) {
          if (i == 0) {  //latest items expiration date must be equal to current policy periods effective date
            if (hp.ExpirationDate.compareIgnoreTime(glLine.Branch.PeriodStart) != 0) {
              //error
              Result.addError(hp, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.RatingParam.MissingSegmentInPriorYearsAndBranch", hp.ExpirationDate.ShortFormat, glLine.Branch.PeriodStart.ShortFormat))
            }
          } else if (i > 0) {
            if (hp.ExpirationDate.compareIgnoreTime(sortedParams[i - 1].EffectiveDate) != 0) {
              Result.addError(hp, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.RatingParam.MissingSegmentInPriorYearsBetweenParameters", hp.ExpirationDate.ShortFormat, sortedParams[i - 1].EffectiveDate.ShortFormat))
            }
          }
        }
      }
    }
  }

  function checkOffering() {
    //BR-001:  "Offering selection for Professional Liability is mandatory.", per GPC-387
    if (glLine.Branch.Offering == null) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.Offering.Required.TDIC"))
    }
  }

  function checkDiscountSelections() {
    //BR-003
    if (numberOfStudentDiscounts() > 1 and this.glLine.JobType == TC_SUBMISSION) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.Multiple.Student.Discount.TDIC"))
    }
    //BR-004 // Removed as part of GPC-494
/*    if (glLine.GLNewDentistDiscount_TDICExists and
        (glLine.GLRiskMgmtDiscount_TDICExists or glLine.GLNewToCompanyDiscount_TDICExists or glLine.GLIRPMDiscount_TDICExists or numberOfStudentDiscounts() > 0)) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.Not.Only.New.Dentist.Discount.TDIC"))
    }*/
    //BR-005: The New Dentist discount is only available for policies with a Coverage A limit of 1,000,000/3,000,000
    if (glLine.GLNewDentistDiscount_TDICExists and glLine.GLDentistProfLiabCov_TDICExists) {
      if (not(glLine.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 1000000 or glLine.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 1000000)) {
        Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.COVA.Only.One.Three.Million.Dentist.Discount.TDIC"))
        Result.addError(glLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("Web.Policy.GL.Validation.COVA.Only.One.Three.Million.Dentist.Discount.TDIC"))
      }
    }
    //BR-006 - This BR is disabled as per the updated requiements in GPC-3258
    /*if (glLine.GLNewDentistDiscount_TDICExists and glLine.Branch.TermType == TermType.TC_OTHER) {
      Result.addError(glLine, TC_QUOTABLE, DisplayKey.get("Web.Policy.GL.Validation.No.Other.TermType.New.Dentist.Discount.TDIC"))
    }*/

    //BR-008
    if (glLine.GLNewDentistDiscount_TDICExists and glLine.GLPriorActsCov_TDICExists) {
      var retroDate = glLine.GLPriorActsCov_TDIC?.GLPriorActsRetroDate_TDICTerm?.Value
      var effectiveDate = glLine.Branch.PeriodStart
      if (retroDate?.differenceInDays(effectiveDate) != 0) {
        Result.addError(glLine, TC_QUOTABLE, DisplayKey.get("Web.Policy.GL.Validation.Same.Retro.Date.As.EffDate.New.Dentist.Discount.TDIC"))
      }
    }

    //BR-009 IRPM Modifier validation per GPC-870
    if (glLine.GLIRPMDiscount_TDICExists) {
      var irpmModifier = glLine.GLModifiers.firstWhere(\mod -> mod.Pattern.CodeIdentifier == "GLIRPMModifier_TDIC")
      if (irpmModifier != null) {
        var modifierMax = irpmModifier.Maximum
        var modifierMin = irpmModifier.Minimum
        var modifierSum : BigDecimal = 0
        for (rf in irpmModifier.RateFactors) {
          modifierSum += (rf.AssessmentWithinLimits != null ? rf.AssessmentWithinLimits : 0BD)
        }
        if (modifierSum > modifierMax or modifierSum < modifierMin) {
          Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.IRPM.Total.Equal.Other.Sum.TDIC"))
        }
      }
    }

    //BR-007: Set the Discount Expiry Date for New to Company Credit to 3 years from the Discount Effective Date
    //BR-010: We are not using the OOTB modifiers. We retiring the 4 OOTB GL Modifiers, and adding the new discounts as additionals with a new category
    //BR-011: Use the DISCOUNTS tab for adding the additional coverages to the new category

    /**
     * GPC-1287 PC - PL Product Model - Modifiers - BR-054
     */
    if (glLine.GLPriorActsCov_TDICExists and glLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm != null and
        glLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm.Value.trimToMidnight()
            .before((glLine.Branch.EditEffectiveDate.trimToMidnight().addYears(-3))) and
        glLine.GLNewGradDiscount_TDICExists) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GL.Validation.NewGraduateDiscountValidation"))
    }

    /**
     * GPC-1287 PC - PL Product Model - Modifiers - BR-055
     */
    if (numberOfStudentDiscountsForIssuanceChange() > 1 and (glLine.JobType == TC_RENEWAL or
        (glLine.Branch.BasedOn.Job.Subtype == TC_SUBMISSION and glLine.JobType == TC_POLICYCHANGE))) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GL.Validation.Multiple.Student.Discount.IssuanceChange"))
    }

    /**
     * GPC-1287 PC - PL Product Model - Modifiers - BR-056
     */
    if (numberOfStudentDiscountsForRenewalChange() > 1 and
        (glLine.Branch.BasedOn.Job.Subtype == TC_RENEWAL and
            glLine.Branch.BasedOn.Status == PolicyPeriodStatus.TC_BOUND and
            glLine.JobType == TC_POLICYCHANGE)) {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GL.Validation.Multiple.Student.Discount.RenewalChange"))
    }
  }


  /*
   * create by: SureshB
   * @description: method to validate discounts on quote
   * @create time: 6:00 PM 1/22/2020
    * @param null
   * @return:
   */

  function discountValidationsOnQuote() {
    Context.addToVisited(this, "discountValidationsOnQuote_TDIC")
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REWRITE}
    var offering = glLine.Branch.Offering.CodeIdentifier

    if ((offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC")
        and validJobTypes.contains(glLine.JobType)) {

      if (glLine.GLDentalEmpPracLiabCov_TDICExists and ((glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value > 21) or
          (glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value >= 11 and glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value <= 20 and
              glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLClaimslasttwoyears_TDICTerm.Value) or
          (glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value >= 11 and glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value <= 20 and
              !glLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLRishMgtReq_TDICTerm.Value))) {

        if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.IneligibleForCovD"))
        }
      }
    }

  }

  /**
   * ClassCode & Coverage validations
   */
  function validateClassCode() {
    var jobTypesForValidation = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REWRITE}
    var offering = glLine.Branch.Offering.CodeIdentifier

    if (jobTypesForValidation.contains(glLine.JobType) and (offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC")) {
      var classCode = glLine.GLExposuresWM.first().ClassCode_TDIC

      if (classCode == ClassCode_TDIC.TC_01 or classCode == ClassCode_TDIC.TC_02)
      {
      validatePriorActsForClassCode_01(classCode)                  //validate Prior Acts
      validateLiabilityForClassCode_01(classCode)                  //validate Liability limits
      validatePremiumDiscountsForClassCode_01(classCode)           //validate Discounts
      validateAdditionalInsuredsForClassCode_01(classCode)
    }
    }
  }


  function validateBaseStateAndPrimaryNamedIsuredState() {
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REWRITE}
    if (validJobTypes.contains(glLine.JobType)) {
      if (not(glLine.Branch.PrimaryNamedInsured.ContactDenorm?.AllAddresses?.hasMatch(\address ->
          address?.State?.Code == glLine.Branch.BaseState.Code))) {
        if (Level == ValidationLevel.TC_QUOTABLE) {
          Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.PNIStateAndBaseStateDifferent"))
        }
      }
    }
  }

  function validateCyberAlreadyExists() {
    //check if coverage already exists
    if (glLine.JobType == TC_SUBMISSION
        and glLine.Branch.Policy.Account.IssuedPolicies?.hasMatch(\issuedPolicy -> issuedPolicy?.Product?.CodeIdentifier == "GeneralLiability"
        and issuedPolicy?.Job?.LatestPeriod?.Offering == glLine.Branch.Offering
        and issuedPolicy.CancellationDate == null
        and issuedPolicy?.Job?.LatestPeriod?.PrimaryNamedInsured.ContactDenorm == glLine.Branch.PrimaryNamedInsured.ContactDenorm
        and (issuedPolicy?.PeriodEnd?.trimToMidnight() < glLine.Branch.PeriodEnd.trimToMidnight()
        or issuedPolicy?.PeriodStart?.trimToMidnight() > glLine.Branch.PeriodStart.trimToMidnight()))) {
      if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
        Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CovExistsForPeriodOnAccount"))
      }
    }
  }

  /**
   * Cyber must hvaave a valid BOP on the same Account or one of related Accounts
   */
  function validateCyberPolicyWithBOP() {
    var isValidJobType = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REINSTATEMENT, typekey.Job.TC_RENEWAL, typekey.Job.TC_REWRITE}.contains(glLine.JobType)
    var currentPeriod = glLine.Branch

    if (currentPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC" and isValidJobType) {
      var hasValidBOPPolicy = false
      var accounts = currentPeriod.Policy.Account.RelatedAccountsBySharedNamedInsuredContacts_TDIC.toSet()
      accounts = accounts.union({currentPeriod.Policy.Account})
      var quotedPeriods = accounts.QuotedPolicyPeriods
      if (quotedPeriods.HasElements) {
        var bopPeriods = quotedPeriods.where(\p -> p.getSlice(p.EditEffectiveDate).Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC")
        for (bopPeriod in bopPeriods) {
          //var hasSamePNI = bopPeriod.Job.LatestPeriod.PrimaryNamedInsured.ContactDenorm == glLine.Branch.PrimaryNamedInsured.ContactDenorm
          var isCancelled = bopPeriod.Canceled
          var isCyberInBOPDateRange = bopPeriod.PeriodStart.compareIgnoreTime(glLine.Branch.PeriodStart) <= 0 and glLine.Branch.PeriodEnd.compareIgnoreTime(bopPeriod.PeriodStart) > 0
          if (/*hasSamePNI and*/
              not isCancelled
                  and isCyberInBOPDateRange) {
            hasValidBOPPolicy = true
            break
          }
        }
      }
      if (not hasValidBOPPolicy) {
        if (Level == ValidationLevel.TC_QUOTABLE) {
          Result.addWarning(glLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotWriteCybWithoutBOPPolicy"))
        }
        if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotWriteCybWithoutBOPPolicy"))
        }
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to validate CYB UW Questions related to Cyber limit values
   * @create time: 6:05 PM 1/23/2020
    * @param null
   * @return:
   */

  function validateCYBLimitsWithUWQuestions() {
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REWRITE}

    if (glLine.Branch.Offering.CodeIdentifier == "PLCyberLiab_TDIC"
        and !glLine.Branch.isQuickQuote_TDIC
        and validJobTypes.contains(glLine.JobType)) {
      if (glLine.GLCyberLiabCov_TDICExists) {
        var cyberLimit = glLine.GLCyberLiabCov_TDIC?.GLCybLimit_TDICTerm?.Value
        var question1 = glLine.getAnswerForGLUWQuestion_TDIC("OfficeContentsInsuredWithTDICCYB_TDIC")
        var question2 = glLine.getAnswerForGLUWQuestion_TDIC("BreachOfPersonalInoIn12MonthsCYB_TDIC")
        var question3 = glLine.getAnswerForGLUWQuestion_TDIC("ConductBackgroundScreensForEmployeesCYB_TDIC")
        var question4 = glLine.getAnswerForGLUWQuestion_TDIC("HavePostedDocumentRetensionPolicyCYB_TDIC")
        var question5 = glLine.getAnswerForGLUWQuestion_TDIC("RegularlyUpdateComputerSecurityMeasuresCYB_TDIC")
        var question6 = glLine.getAnswerForGLUWQuestion_TDIC("CustomerPhysicalRecordsMaintainedInSecuredPlaceCYB_TDIC")
        var count = 0
        if (question3) count = count + 1
        if (question4) count = count + 1
        if (question5) count = count + 1
        if (question6) count = count + 1
        if ((glLine.BaseState != Jurisdiction.TC_ND and question1 and cyberLimit == 100000 and !(!question2 and (question3 or question4))) or
            (question1 and cyberLimit == 250000 and !(!question2 and count >= 2)) or
            (glLine.BaseState == Jurisdiction.TC_ND and cyberLimit == 50000)) {
          if (Level == ValidationLevel.TC_QUOTABLE) {
            Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotWriteCybWithSelectedLimits"))
          }
          if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotWriteCybWithSelectedLimits"))
          }
        }
      }
    }
  }


  /**
   *
   */
  function validateAdditionalCoverages() {
    //Professional Liability Additional Insured Endorsement
    if(glLine.GLAdditionalInsuredCov_TDICExists) {
      for(schedule in glLine.GLAdditionInsdSched_TDIC) {
        if(schedule.AdditionalInsured == null) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.MissingAdditionalInsured", glLine.GLAdditionalInsuredCov_TDIC.Pattern.DisplayName, schedule.LTEffectiveDate.ShortFormat))
        }
      }
    }
    //School Services Endorsement
    if(glLine.GLSchoolServicesCov_TDICExists) {
      for(schedule in glLine.GLSchoolServSched_TDIC) {
        if(schedule.AddlInsured == null) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.MissingAdditionalInsured", glLine.GLSchoolServicesCov_TDIC.Pattern.DisplayName, schedule.EventName))
        }
      }
    }
    //Dental Business Liability Additional Insured Endorsement
    if(glLine.GLBLAICov_TDICExists) {
      for (schedule in glLine.GLDentalBLAISched_TDIC) {
        if(schedule.AdditionalInsured == null) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.MissingAdditionalInsured", glLine.GLAdditionalInsuredCov_TDIC.Pattern.DisplayName, schedule.LTEffectiveDate.ShortFormat))
        }
      }
    }
  }

  function validateIdentityTheftRecovery() {
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_POLICYCHANGE}
    if(validJobTypes.contains(glLine.JobType)
        and glLine.GLIDTheftREcoveryCov_TDICExists
        and glLine.GLNewDentistDiscount_TDICExists) {
      if (Level == ValidationLevel.TC_QUOTABLE) {
        Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.IdentityTheftRecoveryCannotBeApplied"))
      }
      if (Level == ValidationLevel.TC_READYFORISSUE or Level == ValidationLevel.TC_BINDABLE) {
        Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.IdentityTheftRecoveryCannotBeApplied"))
      }
    }
  }

  /*********************************************************private functions****************************************************************/
  /**
   * Prior Acts eligibility
   */
  private function validatePriorActsForClassCode_01(classcode : ClassCode_TDIC) {
    Context.addToVisited(this, "validatePriorActsForClassCode01")
    var validJobTypes = {typekey.Job.TC_SUBMISSION}

    if (validJobTypes.contains(glLine.JobType)) {
      if (glLine.GLPriorActsCov_TDICExists
          and glLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm != null
          and glLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm.Value.trimToMidnight() != glLine.Branch.PeriodStart.trimToMidnight()) {
        if (Level == ValidationLevel.TC_QUOTABLE) {
          if(classcode == ClassCode_TDIC.TC_01) {
            Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode1NotEligibleForPriorActsCov"))
          }
          else if(classcode == ClassCode_TDIC.TC_02) {
            Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode2NotEligibleForPriorActsCov"))
          }
        }
        if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
          if(classcode == ClassCode_TDIC.TC_01) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode1NotEligibleForPriorActsCov"))
          }
          else if(classcode == ClassCode_TDIC.TC_02) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode2NotEligibleForPriorActsCov"))
          }
        }
      }

    }
  }

  /**
   * Liability Limits  eligibility
   */
  private function validateLiabilityForClassCode_01(classCode : ClassCode_TDIC) {
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE, typekey.Job.TC_REWRITE}
    var offering = glLine.Branch.Offering.CodeIdentifier

    if (validJobTypes.contains(glLine.JobType) and glLine.GLDentistProfLiabCov_TDICExists) {
      var isLimitValidFor_01 = false
      if (offering == "PLClaimsMade_TDIC") {
        isLimitValidFor_01 = glLine.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 1000000
      } else if (offering == "PLOccurence_TDIC") {
        isLimitValidFor_01 = glLine.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 1000000
      }

      if (not isLimitValidFor_01) {
        if (Level == ValidationLevel.TC_QUOTABLE) {
          if(classCode == ClassCode_TDIC.TC_01) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.LiabilityLimitsNotValidForClassCode1"))
          }
          else if(classCode == ClassCode_TDIC.TC_02) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.LiabilityLimitsNotValidForClassCode2"))
          }
        }
        if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
          if(classCode == ClassCode_TDIC.TC_01) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.LiabilityLimitsNotValidForClassCode1"))
          }
          else if(classCode == ClassCode_TDIC.TC_02) {
            Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.LiabilityLimitsNotValidForClassCode2"))
          }
        }
      }
    }
  }

  /**
   *
   */
  private function validatePremiumDiscountsForClassCode_01(classCode : ClassCode_TDIC) {
    if (numberOfStudentDiscounts() >= 1) {
      if (Level == ValidationLevel.TC_QUOTABLE) {
        if(classCode == ClassCode_TDIC.TC_01) {
          Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode1NotEligibleForDiscounts"))
        }
        else if(classCode == ClassCode_TDIC.TC_02) {
          Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode2NotEligibleForDiscounts"))
        }
      }
      if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
        if(classCode == ClassCode_TDIC.TC_01) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode1NotEligibleForDiscounts"))
        }
        else if(classCode == ClassCode_TDIC.TC_02) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ClassCode2NotEligibleForDiscounts"))
        }
      }
    }
  }

  private function validateAdditionalInsuredsForClassCode_01(classCode : ClassCode_TDIC) {
    if (glLine.AdditionalInsureds.Count > 0 || !glLine.AdditionalInsureds.IsEmpty) {
      if (Level == ValidationLevel.TC_QUOTABLE) {
        if(classCode == ClassCode_TDIC.TC_01) {
          Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotAddAIForClassCode1"))
        }
        else if(classCode == ClassCode_TDIC.TC_02) {
          Result.addWarning(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotAddAIForClassCode2"))
        }
      }
      if (Level == ValidationLevel.TC_READYFORISSUE || Level == ValidationLevel.TC_BINDABLE) {
        if(classCode == ClassCode_TDIC.TC_01) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotAddAIForClassCode1"))
        }
        else if(classCode == ClassCode_TDIC.TC_02) {
          Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.CannotAddAIForClassCode2"))
        }
      }
    }
  }

  /**
   * @return
   */
  private function numberOfStudentDiscounts() : int {

    var numberOfDiscounts : int = 0

    if (glLine.GLFullTimePostGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLFullTimeFacultyDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLNewGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLPartTimeDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLNewDentistDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    return numberOfDiscounts
  }

  /**
   * create by: SanjanaS
   *
   * @description: Validate the number of student discounts
   * @create time: 2:52 PM 12/13/2019
   * @return: Integer
   */
  private function numberOfStudentDiscountsForIssuanceChange() : int {
    var numberOfDiscounts : int = 0

    if (glLine.GLFullTimePostGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLFullTimeFacultyDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLNewGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLPartTimeDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLTempDisDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    return numberOfDiscounts
  }

  /**
   * create by: SanjanaS
   *
   * @description: Validate the number of student discounts
   * @create time: 2:52 PM 12/13/2019
   * @return: Integer
   */
  private function numberOfStudentDiscountsForRenewalChange() : int {
    var numberOfDiscounts : int = 0

    if (glLine.GLFullTimePostGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLFullTimeFacultyDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLNewGradDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLPartTimeDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLTempDisDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    if (glLine.GLCovExtDiscount_TDICExists) {
      numberOfDiscounts += 1
    }
    return numberOfDiscounts
  }

  private function validateGLManuscriptTDIC() {

    var hardstop : boolean = false

    if (this.glLine?.GLManuscriptEndor_TDICExists && Level == ValidationLevel.TC_QUOTABLE && (this.glLine?.JobType == Job.TC_SUBMISSION
         || this.glLine?.JobType == Job.TC_POLICYCHANGE || this.glLine?.JobType == Job.TC_RENEWAL  )) {
     //GWPS-2201 : Updating the validation rule by not considering the Manuscript’s which are expired and having Eff date is equal to Exp date then we wont invoke the validation rule.
     var glManuEnd = this.glLine?.GLManuscript_TDIC.where(\elt -> elt.ManuscriptEffectiveDate != elt.ManuscriptExpirationDate)
      for (elt in glManuEnd) {
        if (elt.ManuscriptType == GLManuscriptType_TDIC.TC_ADDITIONALINSUREDCONTINUED && elt.PolicyAddInsured_TDIC == null) {
          hardstop = true
          break
        }
        if (elt.ManuscriptType == GLManuscriptType_TDIC.TC_CERTIFICATEHOLDERNAMEAMENDEDTOREAD && elt.CertificateHolder == null) {
          hardstop = true
          break
        }
      }
    }

    if (hardstop){
     Result.addError(glLine, Level, DisplayKey.get("TDIC.Web.Policy.GL.Validation.ManuscriptEndor"))

    }

  }


}

