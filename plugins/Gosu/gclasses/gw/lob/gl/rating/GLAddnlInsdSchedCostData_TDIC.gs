package gw.lob.gl.rating

uses entity.windowed.GLAdditionInsdSched_TDICVersionList
uses entity.windowed.GLAddnlInsdSchedCost_TDICVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.effdate.EffDatedVersionList

class GLAddnlInsdSchedCostData_TDIC extends GLCostData<GLAddnlInsdSchedCost_TDIC> {
  var _addlInsured : GLAdditionInsdSched_TDIC
  var _costType : GLAddnlInsuredCostType_TDIC as CostType

  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction, addlInsured : GLAdditionInsdSched_TDIC, costType : GLAddnlInsuredCostType_TDIC) {
    super(effDate, expDate, stateArg, null, null)
    assertKeyType(addlInsured.FixedId, GLAdditionInsdSched_TDIC)
    _addlInsured = addlInsured
    _costType = costType
  }

  construct(cost : GLAddnlInsdSchedCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    _addlInsured = cost.GLAdditionInsdScheds
    _costType = cost.GLAddnlInsuredCostType_TDIC
  }
  construct(cost : GLAddnlInsdSchedCost_TDIC) {
    super(cost)
    _addlInsured = cost.GLAdditionInsdScheds
    _costType = cost.GLAddnlInsuredCostType_TDIC

  }
  override function getVersionedCosts(line : GeneralLiabilityLine) : List<EffDatedVersionList> {
    var coverageVL = EffDatedUtil.createVersionList(line.Branch, _addlInsured.FixedId) as GLAdditionInsdSched_TDICVersionList
    return coverageVL.AddnlInsdSchedCosts.where(\elt -> isMyCost(elt))
  }

  override function setSpecificFieldsOnCost(line : GeneralLiabilityLine, cost: GLAddnlInsdSchedCost_TDIC ) : void {
    super.setSpecificFieldsOnCost(line, cost)
    //cost.GLAdditionInsdScheds.AdditionalInsured = _addlInsured.AdditionalInsured
    cost.GLAdditionInsdScheds = _addlInsured
    cost.GLAddnlInsuredCostType_TDIC = _costType
  }

  private function isMyCost(costVL : GLAddnlInsdSchedCost_TDICVersionList) : boolean {
    var firstVersion = costVL.AllVersions.first()
    return firstVersion typeis GLAddnlInsdSchedCost_TDIC and firstVersion.GLAddnlInsuredCostType_TDIC == _costType
  }

  override property get GLKeyValues() : List<Object> {
    return {_addlInsured?.FixedId, _costType}
  }
}