package gw.lob.common

uses gw.api.locale.DisplayKey
uses gw.lob.bop.BOPLineValidation
uses gw.validation.PCValidationBase
uses gw.validation.PCValidationContext
uses gw.validation.ValidationUtil
uses tdic.pc.integ.plugins.policyperiod.TDIC_PolicyPeriodIsMultiline

class PolicyInfoValidation extends PCValidationBase {
  final static var LegacyPolicyNumberRegExp : String = "[A-Z]{2}\\s{1}[0-9]{6}-[0-9]{1}-[A-Z0-9]{1}[0-9]{1}"
  final static var LegacyPolicyNumberLength : int = 15
  final static var LegacyPolicyNumberSuffixLength : int = 1
  var _policyPeriod : PolicyPeriod;

  construct(valContext : PCValidationContext, policyPeriod : PolicyPeriod) {
    super(valContext)
    _policyPeriod = policyPeriod;
  }

  static function validateFields(policyPeriod : PolicyPeriod) {
    var context = ValidationUtil.createContext(ValidationLevel.TC_DEFAULT);
    new PolicyInfoValidation(context, policyPeriod).validate();
    context.raiseExceptionIfProblemsFound();
  }

  override protected function validateImpl() {
    var lineValidation : AbstractPolicyInfoValidation;
    Context.addToVisited(this, "validateImpl")
    checkYearBusinessStartedMakesSense()
    checkLegacyPolicyNumberFormat_TDIC()  // GPC-274: Legacy Policy Number and suffix formay validation
    checkAdditionalInsuredPresent_TDIC()
    checkADANumbers_TDIC()
    for (line in _policyPeriod.RepresentativePolicyLines) {
      lineValidation = line.createPolicyInfoValidation_TDIC(Context);
      if (lineValidation != null) {
        lineValidation.validate();
      }
    }
  }

  function checkYearBusinessStartedMakesSense() {
    Context.addToVisited(this, "checkYearBusinessStartedMakesSense")
    new gw.policy.PolicyYearBusinessStartedValidator(Context, _policyPeriod.Lines.first()).validate()
  }

  /*
   * US-159, US387, Jeff Lin
   * The legacy policy number and suffix will have the following format:
   * Legacy Policy Number: CA 024419-4-C1 (The C can either be a letter or a number)
   * Legacy Policy Suffix: 1 (This suffix can be 0-9)
   * RegExp Pattern: [A-Z]{2}\s{1}[0-9]{6}-[0-9]{1}-[A-Z0-9]{1}[0-9]{1}
   */
  private function checkLegacyPolicyNumberFormat_TDIC() {
    Context.addToVisited(this, "checkLegacyPolicyNumberFormat_TDIC")
    if (_policyPeriod.Job.Subtype == TC_RENEWAL and _policyPeriod.Job.MigrationJobInd_TDIC and _policyPeriod.LegacyPolicyNumber_TDIC != null) {
      if (_policyPeriod.LegacyPolicyNumber_TDIC.length != LegacyPolicyNumberLength or not _policyPeriod.LegacyPolicyNumber_TDIC.match(LegacyPolicyNumberRegExp).Matcher.matches()) {
        Result.addError(_policyPeriod, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.Common.Validation.LegacyPolicyNumber.Not.Formatted", _policyPeriod.LegacyPolicyNumber_TDIC))
      }
      if (_policyPeriod.LegacyPolicySuffix_TDIC != null and (_policyPeriod.LegacyPolicySuffix_TDIC.length != LegacyPolicyNumberSuffixLength or not _policyPeriod.LegacyPolicySuffix_TDIC.Numeric)) {
        Result.addError(_policyPeriod, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.Common.Validation.LegacyPolicyNumber.Suffix.Not.Vqalid", _policyPeriod.LegacyPolicySuffix_TDIC))
      }
    }
  }

  /**
   * US890, robk
   */
  private function checkAdditionalInsuredPresent_TDIC() {
    Context.addToVisited(this, "checkAdditionalInsuredPresent_TDIC")
    if (_policyPeriod.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE and _policyPeriod.WC7LineExists) {
      var additionalNamedInsureds = _policyPeriod.PolicyContactRoles.whereTypeIs(PolicyAddlNamedInsured)
      if (additionalNamedInsureds == null or additionalNamedInsureds.length < 2) {
        Result.addError(_policyPeriod, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.Common.Validation.MissingAdditionalInsured", 2, _policyPeriod.PolicyOrgType_TDIC))
      }
    }
  }

  protected function checkADANumbers_TDIC() : void {
    Context.addToVisited(this, "checkADANumbers_TDIC");

    // Do not validate quick quotes
    if (_policyPeriod.Job typeis Submission and _policyPeriod.Job.QuickMode) {
      return
    }
    //do not validate for cyber liability
    if (_policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
      return
    }

    // Check if there is at least one applicable ADA Number
    var multiLineDiscount = new TDIC_PolicyPeriodIsMultiline(_policyPeriod);
    if (multiLineDiscount.ADANumbers.Empty) {
      Result.addWarning(_policyPeriod, ValidationLevel.TC_DEFAULT,
          DisplayKey.get("TDIC.Web.Policy.Common.Validation.MissingADANumber"), "PolicyInfo");
    }
  }

  private function checkFEINOfficialID_TDIC() {
    Context.addToVisited(this, "checkFEINOfficialID_TDIC")
    if(_policyPeriod.WC7LineExists or (_policyPeriod.BOPLineExists and _policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC")) {
      var fein = _policyPeriod.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.FEINOfficialID
      if(fein == null or fein.matches("[0-9]{2}-[0-9]{7}") == false) {
        Result.addError(_policyPeriod, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.common.Validation.PleaseReviewFEIN"))
      }
    }
  }
}