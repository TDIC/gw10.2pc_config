package gw.lob.common.schedules.impl

uses gw.api.domain.Clause
uses gw.api.domain.Schedule
uses gw.api.productmodel.ScheduleNamedInsuredPropertyInfo
uses gw.api.productmodel.SchedulePropertyInfo

class ProxyScheduleNamedInsuredPropertyInfo extends ProxyScheduleForeignKeyPropertyInfo {
  construct(colName: String, colLabel: String,
            valRangeGetterClassName: String, isRequired: boolean, isIdentityColumn : boolean, priority : int) {
    super(colName, colLabel, valRangeGetterClassName, isRequired, isIdentityColumn, priority)
  }

  override reified function toSchedulePropertyInfo<T extends Schedule & Clause>(owner : T) : SchedulePropertyInfo {
    var valRangeGetter = newValueRangeGetterInstance(owner)    
    return new ScheduleNamedInsuredPropertyInfo(ColumnName, 
      ColumnLabel, valRangeGetter, Required, IdentityColumn, Priority)
  }
}
