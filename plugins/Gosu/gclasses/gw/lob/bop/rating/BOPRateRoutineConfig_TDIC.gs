package gw.lob.bop.rating

uses gw.pc.rating.flow.util.InScopeUsageBase
uses gw.plugin.rateflow.IRateRoutineConfig
uses java.math.RoundingMode
uses java.lang.Integer
uses gw.plugin.rateflow.CostDataBase
uses gw.lang.reflect.IType
uses gw.plugin.rateflow.ICostDataWrapper
uses gw.rating.CostDataWithOverrideSupport
uses gw.rating.flow.domain.CalcRoutineCostDataWithAmountOverride
uses gw.rating.flow.domain.CalcRoutineCostDataWithTermOverride
uses gw.lang.reflect.IPropertyInfo
uses gw.pc.rating.flow.AvailableCoverageWrapper
uses gw.rating.flow.util.ItemFilter

@Export
class BOPRateRoutineConfig_TDIC implements IRateRoutineConfig {

  construct() {
  }

  override function getCostDataWrapperType(paramSet : CalcRoutineParameterSet) : IType {
    if(paramSet.Parameters.hasMatch(\elt1 -> elt1.Code==CalcRoutineParamName.TC_PREVIOUSTERMAMOUNT)){
      return  CalcRoutineCostDataWithTermOverride
    }else {
      return BOPCalcRoutineCostData_TDIC
    }
  }

  override function makeCostDataWrapper(paramSet : CalcRoutineParameterSet, 
                                        costData : CostDataBase, 
                                        defaultRoundingLevel : Integer, 
                                        defaultRoundingMode : RoundingMode) : ICostDataWrapper {
    if(paramSet.Parameters.hasMatch(\elt1 -> elt1.Code==CalcRoutineParamName.TC_PREVIOUSTERMAMOUNT)){
    return new CalcRoutineCostDataWithTermOverride(costData as CostDataWithOverrideSupport,
                                                    CalcRoutineCostDataWithAmountOverride.OverrideMode.APPROXIMATE_STANDARD_RATES,
                                                    defaultRoundingLevel,
                                                    defaultRoundingMode)
  }else {
    return new BOPCalcRoutineCostData_TDIC(costData as BOPCostData,defaultRoundingLevel,defaultRoundingMode)
    }
  }


  override function worksheetsEnabledForLine(linePatternCode: String): boolean {
    return true
  }

  override function includeProperty(policyLinePatternCode: String, prop: IPropertyInfo): Boolean {
    return null
  }

  override function getCoverageWrappersForLine(linePatternCode: String): AvailableCoverageWrapper[] {
    return {}
  }

  override function filterIrrelevantItems(input: List<InScopeUsageBase>, policyLinePatternCode: String): List<InScopeUsageBase> {
    return ItemFilter.applyDefaultFilters(input)
  }
}
