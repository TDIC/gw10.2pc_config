package gw.lob.bop.rating

uses java.util.Date

uses entity.windowed.BOPBuildingModifier_TDICVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache

uses java.util.List

uses gw.pl.persistence.core.Key

class BOPBuildingModifierCostData_TDIC extends BOPCostData<BOPModifierCost_TDIC> {

  protected var _modifierID : Key

  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction, modifierID : Key) {
    super(effDate, expDate, stateArg)
    assertKeyType(modifierID, entity.BOPBuildingModifier_TDIC)
    init(modifierID)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, modifierID : Key) {
    super(effDate, expDate, c, rateCache, stateArg)
    assertKeyType(modifierID, entity.BOPBuildingModifier_TDIC)
    init(modifierID)
  }

  construct(cost : BOPModifierCost_TDIC) {
    super(cost)
    init(cost.Modifier_TDIC.FixedId)
  }

  construct(cost : BOPModifierCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    init(cost.Modifier_TDIC.FixedId)
    }

  private function init(modifierID : Key) {
    _modifierID = modifierID
  }

  override function setSpecificFieldsOnCost(line : BOPLine, cost : BOPModifierCost_TDIC) {
    super.setSpecificFieldsOnCost(line, cost)
    cost.setFieldValue("BOPBuildingModifier_TDIC", _modifierID)
  }

  override function getVersionedCosts(line : BOPLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    var covVL = EffDatedUtil.createVersionList(line.Branch, _modifierID) as BOPBuildingModifier_TDICVersionList
    var costs = covVL.BOPModifierCosts.allVersions<BOPModifierCost_TDIC>(true) // warm up the bundle and global cache
    return costs.Keys.where(\VL -> costs[VL].first().Modifier_TDIC.FixedId == _modifierID)
  }

  protected override property get KeyValues() : List<Object> {
    return {_modifierID}
  }

}