package gw.lob.bop.rating
uses java.util.Date
uses gw.api.effdate.EffDatedUtil
uses entity.windowed.BOPBuildingCovVersionList
uses gw.financials.PolicyPeriodFXRateCache
uses java.util.List
uses gw.pl.persistence.core.Key

@Export
class BOPBuildingCovCostData extends BOPCostData<BOPBuildingCovCost> {
  
  protected var _covID : Key
  var _costType : BOPCostType_TDIC
  
  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction, covID : Key) {
    super(effDate, expDate, stateArg)
    assertKeyType(covID, entity.BOPBuildingCov)
    init(covID)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, covID : Key) {
    super(effDate, expDate, c, rateCache, stateArg)
    assertKeyType(covID, entity.BOPBuildingCov)
    init(covID)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, covID : Key, costType : BOPCostType_TDIC) {
    super(effDate, expDate, c, rateCache, stateArg)
    assertKeyType(covID, entity.BOPBuildingCov)
    init(covID)
    _costType = costType
  }

  construct(cost : BOPBuildingCovCost) {
    super(cost)
    init(cost.BOPBuildingCov.FixedId)
    _costType = cost.BOPCostType_TDIC
  }

  construct(cost : BOPBuildingCovCost, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    init(cost.BOPBuildingCov.FixedId)
    _costType = cost.BOPCostType_TDIC
  }

  private function init(covID : Key) {
    _covID = covID
  }

  override function setSpecificFieldsOnCost(line : BOPLine, cost : BOPBuildingCovCost) : void {
    super.setSpecificFieldsOnCost( line, cost )
    cost.setFieldValue("BOPBuildingCov", _covID)
    cost.BOPCostType_TDIC = _costType
    if(cost.BOPBuildingCov typeis BOPILMineSubCov_TDIC) {
      cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
      cost.ChargePattern = ChargePattern.TC_ILMINESUBSIDENCE_TDIC
    }
    /*if(cost.BOPBuildingCov?.BOPBuilding?.BOPILMineSubCov_TDICExists){
      cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
      cost.ChargePattern = ChargePattern.TC_ILMINESUBSIDENCE_TDIC
    }*/
  }

  override function getVersionedCosts(line : BOPLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    var covVL = EffDatedUtil.createVersionList( line.Branch, _covID ) as BOPBuildingCovVersionList
    var costs = covVL.Costs.allVersions<BOPBuildingCovCost>(true) // warm up the bundle and global cache
    return costs.Keys.where(\VL -> costs[VL].first().Coverage.FixedId == _covID and costs[VL].first().BOPCostType_TDIC == _costType)
  }
  
  protected override property get KeyValues() : List<Object> {
    return {_covID.toString() + _costType?.toString()}
  }
  
}
