package gw.lob.bop

uses gw.api.domain.ModifierAdapter

class BOPBuildingModifierAdapter_TDIC implements ModifierAdapter
{
  var _owner : BOPBuildingModifier_TDIC

  construct(modifier : BOPBuildingModifier_TDIC) {
    _owner = modifier
  }

  override property get OwningModifiable() : Modifiable {
    return _owner.BOPBuilding
  }

  override property get RateFactors() : RateFactor[] {
    return _owner.BOPRateFactors
  }

  override function addToRateFactors(element : RateFactor) {
    _owner.addToBOPRateFactors(element as BOPBuildRateFactor_TDIC)
  }

  override function removeFromRateFactors(element : RateFactor) {
    _owner.removeFromBOPRateFactors(element as BOPBuildRateFactor_TDIC)
  }

  override function createRawRateFactor() : RateFactor {
    return new BOPBuildRateFactor_TDIC(_owner.BOPBuilding.Branch)
  }
}
