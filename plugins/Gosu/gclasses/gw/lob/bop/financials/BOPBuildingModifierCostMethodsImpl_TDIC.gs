package gw.lob.bop.financials

uses entity.BOPBuildingModifier_TDIC
uses gw.api.util.JurisdictionMappingUtil

@Export
class BOPBuildingModifierCostMethodsImpl_TDIC extends GenericBOPCostMethodsImpl<BOPModifierCost_TDIC> {
  construct(owner : BOPModifierCost_TDIC) {
    super(owner)
  }

  override property get Coverage() : Coverage {
    return null
  }

  override property get OwningCoverable() : Coverable {
    return Cost.BusinessOwnersLine
  }

  override property get State() : Jurisdiction {
    return JurisdictionMappingUtil.getJurisdiction(Cost.BOPBuildingModifier_TDIC.BOPBuilding.BOPLocation.Location)
  }

  override property get Location() : BOPLocation {
    return Cost.BOPBuildingModifier_TDIC.BOPBuilding.BOPLocation
  }

  override property get Building() : BOPBuilding {
    return Cost.BOPBuildingModifier_TDIC.BOPBuilding
  }

  override property get Modifier_TDIC() : BOPBuildingModifier_TDIC {
    return Cost.BOPBuildingModifier_TDIC
  }

}
