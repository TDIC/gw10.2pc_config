package gw.lob.bop.financials

uses gw.api.util.JurisdictionMappingUtil

class BOPBuildingCostMethodsImpl_TDIC extends GenericBOPCostMethodsImpl<BOPBuildingCost_TDIC>
{
  construct( owner : BOPBuildingCost_TDIC )
  {
    super( owner )
  }

  override property get Coverage() : Coverage
  {
    return null
  }

  override property get OwningCoverable() : Coverable
  {
    return Cost.BusinessOwnersLine
  }

  override property get State() : Jurisdiction
  {
    return JurisdictionMappingUtil.getJurisdiction(Cost.BOPBuilding_TDIC.BOPLocation.Location)
  }

  override property get Location() : BOPLocation
  {
    return Cost.BOPBuilding_TDIC.BOPLocation
  }

  override property get Building() : BOPBuilding
  {
    return Cost.BOPBuilding_TDIC
  }

}