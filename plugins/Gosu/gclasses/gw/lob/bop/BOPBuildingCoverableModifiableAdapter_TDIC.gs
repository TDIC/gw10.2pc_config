package gw.lob.bop

uses gw.api.contact.AdditionalInterestContainer
uses gw.api.locale.DisplayKey

class BOPBuildingCoverableModifiableAdapter_TDIC implements gw.api.domain.CoverableAdapter, gw.api.domain.ModifiableAdapter, AdditionalInterestContainer {

   /**
   * This class exists to aggregate the implementations of gw.api.domain.CoverableAdapter and
   * gw.api.domain.ModifiableAdapter for BOPBuilding. These interfaces have overlapping methods, so they must be
   * implemented via a single class.
   */

    delegate _coverableAdapter represents gw.api.domain.CoverableAdapter
    delegate _modifiableAdapter represents gw.api.domain.ModifiableAdapter

    private var _owner : BOPBuilding

    construct(owner: BOPBuilding) {
      _coverableAdapter = new gw.lob.bop.BOPBuildingCoverableAdapter (owner)
      _modifiableAdapter = new gw.lob.bop.BOPBuildingModifiableAdapter_TDIC (owner)
      _owner = owner
    }

    override property get PolicyLine() : PolicyLine {
      return _coverableAdapter.PolicyLine
    }

    override property  get State() : Jurisdiction {
      return _coverableAdapter.State
    }

    override property get ReferenceDateInternal() : Date {
      return _owner.ReferenceDateInternal
    }

    override property set ReferenceDateInternal(d : Date)  {
      _owner.ReferenceDateInternal = d
    }
  override function createAndAddAdditionalInterestDetail(contactType : ContactType) : AddlInterestDetail  {
    return new BOPBldgAddlInterest(this.PolicyPeriod)
  }

  override function removeFromAdditionalInterestDetails( interestDetail: AddlInterestDetail ) : void {
  }

  override property get OwnerDisplayName() : String {
    return _owner.DisplayName + " (" + _owner.DisplayName + ")"
  }

  override function addToAdditionalInterestDetails( interestDetail: AddlInterestDetail ) : void {
  }

  override function addAdditionalInterestDetail(policyAdditionalInterest : PolicyAddlInterest) : AddlInterestDetail{
    return null
  }

  override function addAdditionalInterestDetail(contact : Contact) : AddlInterestDetail {
    return null
  }

  override property get AdditionalInterestOtherCandidates() : AccountContact[] {
    return null
  }

  override property get AdditionalInterestCandidates() : AccountContact[] {
    return null
  }

  override property get TypeLabel() : String {
    return DisplayKey.get("BusinessOwners.Building.AdditionalInterest.LVLabel")
  }

  override property get AdditionalInterestDetails() : AddlInterestDetail[] {
    return _owner.AdditionalInterests
  }

  override function getAdditionalInterestDetailsForPolicyAddlInterest(policyAdditionalInterest : PolicyAddlInterest) : AddlInterestDetail[] {
    return null
  }

 }


