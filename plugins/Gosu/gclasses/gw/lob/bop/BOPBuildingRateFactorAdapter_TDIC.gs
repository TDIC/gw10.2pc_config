package gw.lob.bop

uses gw.api.domain.RateFactorAdapter

class BOPBuildingRateFactorAdapter_TDIC implements RateFactorAdapter
{
  var _owner : BOPBuildRateFactor_TDIC

  construct(rateFactor : BOPBuildRateFactor_TDIC) {
    _owner = rateFactor
  }

  override property get Modifier() : Modifier {
    return _owner.BOPBuildingModifier_TDIC
  }
}
