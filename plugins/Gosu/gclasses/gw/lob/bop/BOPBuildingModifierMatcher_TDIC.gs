package gw.lob.bop

uses gw.lob.common.AbstractModifierMatcher

class BOPBuildingModifierMatcher_TDIC extends AbstractModifierMatcher<BOPBuildingModifier_TDIC> {
  construct(owner : BOPBuildingModifier_TDIC) {
    super(owner)
  }
}