package gw.lob.wc7

uses gw.lob.common.AbstractConditionMatcher
uses gw.entity.ILinkPropertyInfo

/**
 * Matches {@link WC7WorkersCompCond}s based on the FK to the {@link WCLine} as well as the
 * properties defined in {@link AbstractConditionMatcher}.
 */
@Export
class WC7JurisdictionCondMatcher extends AbstractConditionMatcher<WC7JurisdictionCond>{

  construct(owner : WC7JurisdictionCond) {
    super(owner)
  }
  override property get Parent() : ILinkPropertyInfo {
    return WC7JurisdictionCond#WC7Jurisdiction.PropertyInfo as ILinkPropertyInfo
  }

}
