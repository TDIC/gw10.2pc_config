package gw.lob.wc7.availability

uses gw.api.productmodel.ConditionPattern

enhancement WC7CoverageCategoryEnhancement : gw.api.productmodel.CoverageCategory {

  function availableConditionPatternsForCoverable(coveredObject : Coverable, editable : boolean) : ConditionPattern[] {
    return (editable
        ? this.ConditionPatterns.where(\ c -> coveredObject.isConditionSelectedOrAvailable(c))
        : this.ConditionPatterns.where(\ c -> coveredObject.hasCondition(c))).toTypedArray()
  }
}
