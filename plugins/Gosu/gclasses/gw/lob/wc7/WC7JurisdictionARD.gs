package gw.lob.wc7

uses gw.api.database.Query
uses java.util.Date
uses gw.api.database.IQueryBeanResult

/**
 * Class to determine if a Jurisdiction is ARD splittable and follow 90-days rule
 */
class WC7JurisdictionARD {

  /**
   *  Determines if a jurisdiction can be splittable based on a period start date
   */
  public function hasSplittableARD(jurisdiction : Jurisdiction, periodStartDate : Date) : boolean {
    var hasSplittableARD = true
    var queryWithJurisdictionResult = queryARDForJurisdictionAndDates(jurisdiction, periodStartDate)

    if (queryWithJurisdictionResult.Empty) {
      //if no result found for a specific jurisdiction then we'll check if there is a countrywide entry that matches
      // the date
      var queryWithoutJurisdictionResult = queryARDForCWAndDates(periodStartDate)
      if(not queryWithoutJurisdictionResult.Empty) { //The has elements is a really slow call so using not Empty instead
        hasSplittableARD = queryWithoutJurisdictionResult.first().SplittableARD
      }
    } else {
      hasSplittableARD = queryWithJurisdictionResult.first().SplittableARD
    }
    return hasSplittableARD
  }

  private function queryARDForJurisdictionAndDates(jurisdiction : Jurisdiction, periodStartDate : Date) : IQueryBeanResult<WC7JurisdictionSplittableARD> {
    var queryJuris = Query.make(WC7JurisdictionSplittableARD)
    return queryJuris.compare("Jurisdiction", Equals, jurisdiction)
        .compare("EffectiveDate",LessThanOrEquals, periodStartDate)
        .or(\ cond -> {
          cond.compare("ExpirationDate",GreaterThan, periodStartDate)
          cond.compare("ExpirationDate",Equals, null)
        })
        .select()
  }

  private function queryARDForCWAndDates(periodStartDate : Date) : IQueryBeanResult<WC7JurisdictionSplittableARD> {
    var queryJuris = Query.make(WC7JurisdictionSplittableARD)
    return queryJuris.compare("Jurisdiction", Equals, null)
        .compare("EffectiveDate",LessThanOrEquals, periodStartDate)
        .or(\ cond -> {
          cond.compare("ExpirationDate",GreaterThan, periodStartDate)
          cond.compare("ExpirationDate",Equals, null)
        })
        .select()
  }

  protected function isCandidateFor90DaysARDRule(jurisdiction: Jurisdiction, periodStartDate: Date) : boolean {
    var queryJuris = Query.make(WC7JurisdictionSplittableARD)
    var queryRes = queryJuris.compare("Jurisdiction", Equals, jurisdiction)
        .compare("EffectiveDate",LessThanOrEquals , periodStartDate)
        .or(\ cond -> {
          cond.compare("ExpirationDate",GreaterThan, periodStartDate)
          cond.compare("ExpirationDate",Equals, null)
        })
        .select()

    if (queryRes.Empty) {
      return false
    } else {
      var wc7JurisdictionSplittableARD = queryRes.first()
      return( wc7JurisdictionSplittableARD.SplittableARD and wc7JurisdictionSplittableARD.Apply90DaysARDRule)
    }
  }

}