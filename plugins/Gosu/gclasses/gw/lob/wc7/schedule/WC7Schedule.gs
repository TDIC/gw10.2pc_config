package gw.lob.wc7.schedule

uses java.util.List

interface WC7Schedule<T> {
  property get Items() : List<T>
  property get ExportedProperties() : WC7PropertyList<T>
  function itemsFor(jurisdiction : Jurisdiction) : List<T>
  property get IsMandatory() : boolean
  property get Title() : String
}
