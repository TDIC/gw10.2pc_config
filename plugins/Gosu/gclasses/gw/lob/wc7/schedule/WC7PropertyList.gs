package gw.lob.wc7.schedule

uses gw.lang.reflect.features.PropertyReference
uses gw.lang.reflect.IPropertyInfo
uses java.util.List

class WC7PropertyList<T> {
  var _properties : List<PropertyReference<T, Object>>

  construct(properties : List<PropertyReference<T, Object>>) {
    _properties = properties
  }

  function prepend(newProperty: PropertyReference<T, Object>) : WC7PropertyList<T> {
    var properties = _properties.copy()
    properties.add(0, newProperty)
    return new WC7PropertyList<T>(properties)
  }

  function append(newProperty : PropertyReference<T, Object>) : WC7PropertyList<T> {
    return appendAll({newProperty})
  }

  function appendAll(newProperties : List<PropertyReference<T, Object>>) : WC7PropertyList<T> {
    var properties = _properties.copy()
    properties.addAll(newProperties)
    return new WC7PropertyList<T>(properties)
  }

  function each(callback(prop : PropertyReference<T, Object>)) {
    _properties.each(callback)
  }

  property get PropertyInfos() : List<IPropertyInfo> {
    return _properties*.PropertyInfo.toList()
  }
}
