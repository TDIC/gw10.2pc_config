package gw.lob.wc7.schedule

uses gw.lob.wc7.services.WC7ServiceLocator
uses gw.xml.XMLNode
uses gw.api.domain.Clause
uses java.util.Set
uses java.util.List

class WC7ScheduleXMLSerializer {
  function addContent(clause : Clause, parentNode : XMLNode, availableJurisdictions : Set<Jurisdiction>) {
    if (clause == null) return
    var scheduleFinder = WC7ServiceLocator.Instance.scheduleFinderFor(clause.PolicyLine as WC7Line)
    var schedule = scheduleFinder.findBy(clause.Pattern)
    if (schedule != null)
      parentNode.addChild(serialize(schedule, availableJurisdictions))
  }

  function serialize(schedule : WC7Schedule, availableJurisdictions : Set<Jurisdiction>) : XMLNode {
    var result = new XMLNode("Schedule")
    itemsForAvailableJurisdictions(schedule, availableJurisdictions)
        .each(\ item -> result.addChild(childNodeForItem(schedule, item)))
    return result
  }

  private function itemsForAvailableJurisdictions(schedule : WC7Schedule, availableJurisdictions : Set<Jurisdiction>)
      : List {
    // This implementation is verbose and inefficient, but it preserves the original order
    // (possibly unnecessary, but convenient).
    return schedule.Items.where(\ item -> availableJurisdictions.hasMatch(\ j -> schedule.itemsFor(j).contains(item)))
  }
  
  private function childNodeForItem(schedule : WC7Schedule, item : Object) : XMLNode {
    var result = new XMLNode("ScheduledItem")
    var properties = schedule.ExportedProperties
    properties.each(\ prop -> {
      var propertyNode = new XMLNode(prop.PropertyInfo.Name)
      propertyNode.Text = prop.get(item)?.toString()
      result.addChild(propertyNode)
    })
    return result
  }
}
