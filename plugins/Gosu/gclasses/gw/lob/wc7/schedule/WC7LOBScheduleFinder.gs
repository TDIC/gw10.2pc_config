package gw.lob.wc7.schedule

uses gw.api.productmodel.ClausePattern
uses gw.api.productmodel.ConditionPattern
uses gw.api.domain.Schedule
uses gw.lang.reflect.features.PropertyReference
uses gw.lob.wc7.WC7ClauseClassifier
uses gw.lob.wc7.WC7LOBClauseClassifier
uses java.util.List
uses gw.api.locale.DisplayKey
uses gw.api.upgrade.PCCoercions

class WC7LOBScheduleFinder implements WC7ScheduleFinder {
  var _line : WC7Line
  var _clauseClassifier : WC7ClauseClassifier

  construct(line : WC7Line, clauseClassifier : WC7ClauseClassifier = null) {
    _line = line
    _clauseClassifier = clauseClassifier ?: new WC7LOBClauseClassifier()
  }

  override function findBy(clausePattern : ClausePattern) : WC7Schedule {
    for (finder in finderBlocks()) {
      var schedule = finder(clausePattern)
      if (schedule != null)
        return schedule
    }
    return null
  }

  protected property get Line() : WC7Line {
    return _line
  }

  protected function finderBlocks() : List<block(pattern : ClausePattern) : WC7Schedule> {
    return {
        \ pattern -> genericScheduleFor(pattern),
        \ pattern -> customScheduleFor(pattern)
    }
  }

  private function genericScheduleFor(clausePattern : ClausePattern) : WC7Schedule {
    var clause = _line.getCoverageConditionOrExclusion(clausePattern)
    if (clause typeis Schedule)
      return scheduleFor(clause.ScheduledItems,
          propertiesWith(
              clause.PropertyInfos.toList().map(\ prop ->
                  new PropertyReference<ScheduledItem, Object>(prop.PropertyInfo.OwnersType, prop.PropertyInfo.Name))),
          jurisdictionFilterForGenericSchedule(clause))
    else
      return null
  }

  private function jurisdictionFilterForGenericSchedule(schedule: Schedule)
      : block(item : ScheduledItem, jurisdiction : Jurisdiction) : boolean {
    var prop = schedule.PropertyInfos.firstWhere(\ prop -> prop typeis WC7ScheduleJurisdictionPropertyInfo)
    if (prop == null)
      return noJurisdictionFilter()
    else
      return jurisdictionFilterOn(\ item -> prop.PropertyInfo.Accessor.getValue(item) as Jurisdiction)
  }

  private function customScheduleFor(pattern : ClausePattern) : WC7Schedule {
    switch (pattern) {
      case PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientEndorsementCond"):
        return contactDetailScheduleFor(_line.getIncludedLaborClientDetails(), employeeLeasingClientProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientExclEndorsementExcl"):
        return contactDetailScheduleFor(
            _line.getExcludedLaborClientDetails(PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientExclEndorsementExcl")),
            baseEmployeeLeasingProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7LaborContractorEndorsementACond"):
        return contactDetailScheduleFor(
            _line.getIncludedLaborContractorDetails(),
            employeeLeasingContractorProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7LaborContractorExclEndorsementExcl"):
        return contactDetailScheduleFor(
            _line.getExcludedLaborContractorDetails(PCCoercions.makeProductModel<ClausePattern>("WC7LaborContractorExclEndorsementExcl")),
            baseEmployeeLeasingProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7SoleProprietorsPartnersOfficersAndOthersCovCond"):
        return ownerOfficerScheduleFor(_line.IncludedOwnerOfficers, ownerOfficerCoverageProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7PartnersOfficersAndOthersExclEndorsementExcl"):
        return ownerOfficerScheduleFor(_line.ExcludedOwnerOfficers, ownerOfficerExclusionProperties())
      case PCCoercions.makeProductModel<ClausePattern>("WC7AircraftPremiumEndorsementCond"):
        return aircraftSeatSchedule()
      case PCCoercions.makeProductModel<ClausePattern>("WC7DesignatedWorkplacesExclEndorsementExcl"):
        return excludedWorkplaceSchedule()
      case PCCoercions.makeProductModel<ClausePattern>("WC7FederalEmployersLiabilityActACov"):
        return coveredEmployeesScheduleFor(_line.AllWC7FedCoveredEmployeesWM,
            {WC7FedCoveredEmployee#ClassCodeShortDescription})
      case PCCoercions.makeProductModel<ClausePattern>("WC7MaritimeACov"):
        return coveredEmployeesScheduleFor(_line.AllWC7MaritimeCoveredEmployeesWM,
            {WC7MaritimeCoveredEmployee#ClassCodeShortDescription, WC7MaritimeCoveredEmployee#Vessel})
      case PCCoercions.makeProductModel<ClausePattern>("WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond"):
        return waiversOfSubrogationSchedule()
      case PCCoercions.makeProductModel<ClausePattern>("WC7MultipleCoordinatedPolicyEndorsementCond"):
        return multipleCoordinatedPoliciesSchedule()
      default:
        return _clauseClassifier.isSimpleContactSchedule(pattern)
            ? simpleContactScheduleFor(pattern)
            : null
      }
  }

  protected function simpleContactScheduleFor(pattern : ClausePattern,
      isMandatory : boolean = true, title : String = null) : WC7Schedule<WC7ContactDetail> {
    var clause = _line.getCondition(pattern as ConditionPattern)
    return scheduleFor<WC7ContactDetail>(_line.basicContactDetailsFor(clause).toTypedArray(),
        effDatedPropertiesWith({
            WC7ContactDetail#ContactName,
            WC7ContactDetail#Address
        }),
        noJurisdictionFilter(),
        :isMandatory = isMandatory,
        :title = title)
  }

  private function ownerOfficerScheduleFor(
      contactRoles : WC7PolicyOwnerOfficer[],
      exportedPropertyList : WC7PropertyList<WC7PolicyOwnerOfficer>) : WC7Schedule<WC7PolicyOwnerOfficer> {
    return scheduleFor<WC7PolicyOwnerOfficer>(contactRoles,
        exportedPropertyList,
        jurisdictionFilterOn<WC7PolicyOwnerOfficer>(\ item -> item.Jurisdiction))
  }

  private function ownerOfficerCoverageProperties() : WC7PropertyList<WC7PolicyOwnerOfficer> {
    return baseOwnerOfficerProperties().appendAll({
        WC7PolicyOwnerOfficer#Jurisdiction
    })
  }

  private function ownerOfficerExclusionProperties() : WC7PropertyList<WC7PolicyOwnerOfficer> {
    return baseOwnerOfficerProperties().append(WC7PolicyOwnerOfficer#WC7OwnershipPct)
  }

  private function baseOwnerOfficerProperties() : WC7PropertyList<WC7PolicyOwnerOfficer> {
    return effDatedPropertiesWith({
        WC7PolicyOwnerOfficer#DisplayName,
        WC7PolicyOwnerOfficer#RelationshipTitle
    })
  }

  reified protected function scheduleFor<T>(
      items : T[],
      exportedPropertyList : WC7PropertyList<T>,
      jurisdictionFilter(item : T, jurisdiction : Jurisdiction) : boolean,
      isMandatory : boolean = true,
      title : String = null) : WC7Schedule<T> {

    return new WC7Schedule<T>() {
      override property get Items() : List<T> {
        return items.toList()
      }

      override property get ExportedProperties() : WC7PropertyList<T> {
        return exportedPropertyList
      }

      override function itemsFor(jurisdiction : Jurisdiction) : List<T> {
        return Items.where(\ item -> jurisdictionFilter(item, jurisdiction))
      }

      override property get IsMandatory() : boolean {
        return isMandatory
      }

      override property get Title() : String {
        return title ?: DisplayKey.get("Web.Policy.Schedule.ScheduledItems")
      }
    }
  }

  protected function jurisdictionFilterOn<T>(getJurisdiction(item: T): Jurisdiction)
      : block(item : T, jurisdiction : Jurisdiction) : boolean {
    return \ item, jurisdiction -> getJurisdiction(item) == jurisdiction
  }

  protected function noJurisdictionFilter<T>() : block(item : T, jurisdiction : Jurisdiction) : boolean {
    return \ item, jurisdiction -> true
  }

  protected function contactDetailScheduleFor(
      contactDetails : WC7LaborContactDetail[],
      exportedPropertyList : WC7PropertyList<WC7LaborContactDetail>) : WC7Schedule<WC7LaborContactDetail> {
    return scheduleFor(contactDetails,
        exportedPropertyList,
        jurisdictionFilterOn<WC7LaborContactDetail>(\ item -> item.Jurisdiction))
  }

  private function employeeLeasingContractorProperties() : WC7PropertyList<WC7LaborContactDetail> {
    return baseEmployeeLeasingProperties().append(WC7LaborContactDetail#ContractProject)
  }

  protected function employeeLeasingClientProperties() : WC7PropertyList<WC7LaborContactDetail> {
    return baseEmployeeLeasingProperties().appendAll({
        WC7LaborContactDetail#NumberOfEmployees,
        WC7LaborContactDetail#DescriptionOfDuties,
        WC7LaborContactDetail#WorkLocation
    })
  }

  private function baseEmployeeLeasingProperties() : WC7PropertyList<WC7LaborContactDetail> {
    return effDatedPropertiesWith({
        WC7LaborContactDetail#ContactName,
        WC7LaborContactDetail#Jurisdiction,
        WC7LaborContactDetail#ContractEffectiveDate,
        WC7LaborContactDetail#ContractExpirationDate
    })
  }

  private function aircraftSeatSchedule() : WC7Schedule<WC7AircraftSeat> {
    return scheduleFor(_line.WC7AircraftSeats,
        effDatedPropertiesWith({
            WC7AircraftSeat#Jurisdiction,
            WC7AircraftSeat#Description,
            WC7AircraftSeat#AircraftNumber,
            WC7AircraftSeat#PassengerSeats
       }),
       jurisdictionFilterOn<WC7AircraftSeat>(\ item -> item.Jurisdiction))
  }

  private function excludedWorkplaceSchedule() : WC7Schedule<WC7ExcludedWorkplace> {
    return scheduleFor(_line.WC7ExcludedWorkplaces,
        effDatedPropertiesWith({
            WC7ExcludedWorkplace#ExcludedItem,
            WC7ExcludedWorkplace#AddressLine1,
            WC7ExcludedWorkplace#AddressLine2,
            WC7ExcludedWorkplace#City,
            WC7ExcludedWorkplace#Jurisdiction
        }),
        jurisdictionFilterOn<WC7ExcludedWorkplace>(\ item -> item.Jurisdiction))
  }

  private function coveredEmployeesScheduleFor(
      employees : List<WC7CoveredEmployeeBase>,
      addedProperties : List<PropertyReference<WC7CoveredEmployeeBase, Object>>)
      : WC7Schedule<WC7CoveredEmployeeBase> {
    return scheduleFor(employees.toTypedArray(),
        effDatedPropertiesWith<WC7CoveredEmployeeBase>({
            WC7CoveredEmployeeBase#Location,
            WC7CoveredEmployeeBase#ClassCode,
            WC7CoveredEmployeeBase#NumEmployees,
            WC7CoveredEmployeeBase#IfAnyExposureAndClearBasisAmount,
            WC7CoveredEmployeeBase#BasisAmount,
            WC7CoveredEmployeeBase#Rate,
            WC7CoveredEmployeeBase#SpecificDiseaseLoaded,
            WC7CoveredEmployeeBase#SupplementalDiseaseLoaded,
            WC7CoveredEmployeeBase#SupplementalLoadingRate
        }).appendAll(addedProperties),
        jurisdictionFilterOn<WC7CoveredEmployeeBase>(\ item -> item.Jurisdiction.Jurisdiction))
  }

  private function waiversOfSubrogationSchedule() : WC7Schedule<WC7WaiverOfSubro> {
    return scheduleFor(_line.AllWC7WaiverOfSubrosWM.toTypedArray(),
        effDatedPropertiesWith<WC7WaiverOfSubro>({}),
        jurisdictionFilterOn<WC7WaiverOfSubro>(\ item -> item.Jurisdiction))
  }

  private function multipleCoordinatedPoliciesSchedule() : WC7Schedule<WC7CoordinatedPolicy> {
    return scheduleFor<WC7CoordinatedPolicy>(_line.MultipleCoordinatedPolicies,
        effDatedPropertiesWith({
            WC7CoordinatedPolicy#StatePerformed,
            WC7CoordinatedPolicy#LaborContractor,
            WC7CoordinatedPolicy#ContractProject,
            WC7CoordinatedPolicy#LaborContractorPolicyNumber
        }),
        jurisdictionFilterOn<WC7CoordinatedPolicy>(\ item -> item.StatePerformed))
  }

  protected reified function effDatedPropertiesWith<T extends EffDated>(properties : List<PropertyReference <T, Object>>)
      : WC7PropertyList<T> {
    return propertiesWith(properties).prepend(T#FixedId)
  }

  private reified function propertiesWith<T>(properties : List<PropertyReference <T, Object>>) : WC7PropertyList<T> {
    return new WC7PropertyList<T>(properties)
  }
}