package gw.lob.wc7.services

uses gw.api.productmodel.ClausePattern
uses gw.lob.wc7.schedule.WC7ScheduleFinder
uses gw.lob.wc7.schedule.WC7LOBScheduleFinder

class WC7LOBServiceLocator extends WC7ServiceLocator {
  override property get CoverageInputSetModeFinder(): block(pattern : ClausePattern) : String {
    return \ pattern -> pattern.CodeIdentifier
  }

  override function scheduleFinderFor(line : WC7Line) : WC7ScheduleFinder {
    return new WC7LOBScheduleFinder(line)
  }
}
