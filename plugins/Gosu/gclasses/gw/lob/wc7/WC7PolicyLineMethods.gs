package gw.lob.wc7

uses java.util.ArrayList

uses gw.api.locale.DisplayKey
uses gw.api.policy.AbstractPolicyLineMethodsImpl
uses java.util.Set

uses gw.api.tree.RowTreeRootNode
uses gw.api.util.DateUtil
uses gw.lang.reflect.IType
uses gw.lob.common.AbstractPolicyInfoValidation
uses gw.lob.common.UnderwriterEvaluator
uses gw.policy.PolicyEvalContext
uses gw.policy.PolicyLineValidation
uses gw.rating.worksheet.treenode.WorksheetTreeNodeContainer
uses gw.rating.worksheet.treenode.WorksheetTreeNodeUtil
uses gw.validation.PCValidationContext
uses gw.plugin.diff.impl.WC7DiffHelper
uses java.util.Date
uses gw.api.util.StateJurisdictionMappingUtil
uses java.util.HashSet
uses java.lang.Iterable
uses entity.windowed.WC7CostVersionList
uses gw.api.util.JurisdictionMappingUtil
uses java.math.BigDecimal
uses gw.api.domain.Schedule
uses gw.lob.wc7.schedule.WC7ScheduleHelper
uses gw.rating.AbstractRatingEngine
uses java.util.Map
uses gw.lob.wc7.rating.WC7SysTableRatingEngine
uses gw.pl.persistence.core.Key
uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses tdic.pc.config.wc7.WC7_UnderwriterEvaluator

@Export
class WC7PolicyLineMethods extends AbstractPolicyLineMethodsImpl {
  public var _line : entity.WC7WorkersCompLine

  construct(line : entity.WC7WorkersCompLine) {
    super(line)
    _line = line
  }

  override function initialize() {
    var state = JurisdictionMappingUtil.getJurisdiction(_line.Branch.Policy.Account.PrimaryLocation)
    _line.addJurisdiction(state )
  }

  override function syncComputedValues() {
    _line.recalculateGoverningClasscode()
    _line.updateWCExposuresAndModifiers()
    _line.syncWC7CoveredEmployeeBasesVersions()
    _line.syncSpecificWaiversOfSubroVersions()
  }

  override property get CoveredStates() : Jurisdiction[] {
    var jurisdictions = _line.WC7CoveredEmployees.where(\ emp -> emp.Location != null and not WC7GoverningLaw.TC_STOPGAP.equals(emp.GoverningLaw))
        .map(\ emp -> JurisdictionMappingUtil.getJurisdiction(emp.Location)).sort().toSet().toTypedArray()
    return jurisdictions
  }

  override property get AllCoverages() : Coverage[] {
    var covs = new ArrayList<Coverage>()
    covs.addAll(_line.WC7LineCoverages.toList())
    covs.addAll(_line.WC7Jurisdictions*.Coverages.toList())
    return covs?.toTypedArray()
  }

  override property get AllExclusions() : Exclusion[] {
    return _line.WC7LineExclusions
  }

  override property get AllConditions() : PolicyCondition[] {
    return _line.WC7LineConditions
  }

  override property get AllCoverables() : Coverable[] {
    var list : ArrayList<Coverable> = {_line}
    _line.WC7Jurisdictions.each(\ jur -> list.add(jur))
    return list.toTypedArray()
  }

  override property get AllModifiables() : Modifiable[] {
    return _line.WC7Jurisdictions
  }

  override property get AllSchedules() : Schedule[] {
    return _line.CoveragesConditionsAndExclusionsFromCoverable.whereTypeIs(Schedule)
  }

  override property get SupportsRatingOverrides() : boolean {
    return true
  }

  override property get OfficialIDStates() : State[] {
    var jurisdictions = _line.WC7Jurisdictions.map(\ juris -> juris.Jurisdiction)
    var states = new HashSet<State>()
    for(jurisdiction in jurisdictions){
      states.add(StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction))
    }
    return states?.toTypedArray()
  }

  override function onIssuanceBeginEditing() {
    initializeAnniversaryDates()
  }

  override function onSubmissionBeginEditing() {
    initializeAnniversaryDates()
    var state = JurisdictionMappingUtil.getJurisdiction(_line.Branch.PrimaryLocation)
    if (_line.getJurisdiction( state ) == null) {
      _line.addJurisdiction( state )
    }
  }

  override property get Auditable() : boolean {
    return true
  }

  override property get AllowsPremiumAudit() : boolean {
    return true
  }

  private function initializeAnniversaryDates() {
    for (jurisdiction in _line.WC7Jurisdictions) {
      if (jurisdiction.AnniversaryDate == null) {
        jurisdiction.AnniversaryDate = _line.Branch.PeriodStart
      }
    }
  }

  /**
   * Indicates whether ChangeEditEffectiveDate (CEED) can be performed on this policy line.  If this returns
   * null or an empty string, that means the line thinks it's OK.   Otherwise, this method should returnan
   * error message indicating why ChangeEditEffectiveDate cannot be performed.
   * @return an error message if Edit Effective Date can't be changed, null or the empty String if it can
   */
  override function canSafelyCEED() : boolean {
    for (jurisdiction in _line.WC7Jurisdictions) {
      if (jurisdiction.BasedOn != null and not jurisdiction.SplitDates.toSet().equals(jurisdiction.BasedOn.SplitDates.toSet())) {
        return false
      }
    }
    return true
  }

  /**
   * Indicates whether or not this location can be safely deleted.  If this returns
   * a null or empty string, that means that the line thinks it's okay to delete the location.  Otherwise, this method
   * should return an error message indicating why the location can't be deleted.
   */
  override function canSafelyDeleteLocation(location : PolicyLocation) : String {
    // check whether this location is the last one for given jurisdiction
    var juris = StateJurisdictionMappingUtil.getJurisdictionMappingForState(location.State)
    var matchingLocs = _line.PolicyLocations.where(\ p -> p.State.equals(location.State) and p != location)
    var scheduleHelper = new WC7ScheduleHelper()
    //Check for Generic and Specific Schedule References if this is the last Location associated with a Jurisdiction
    if (matchingLocs.IsEmpty){
      var wc7Line = _line as productmodel.WC7Line
      if (scheduleHelper.isJurisdictionUsedByCurrentAndFutureScheduleItems(juris, wc7Line) or
          scheduleHelper.isJurisdictionUsedByCurrentAndFutureSpecificSchedules(juris, wc7Line)){
        return DisplayKey.get("Web.Policy.WC7.Jurisdiction.UsedBySchedule", "Location " + location.LocationNum)
      }
    }
    return super.canSafelyDeleteLocation(location)
  }



  override property get AllCurrentAndFutureScheduledItems() : List<ScheduledItem> {
    return new WC7ScheduleHelper().getCurrentAndFutureScheduledItemsForPolicyLine(_line as productmodel.WC7Line)
  }

  /**
   * All WC7Costs across the term, in window mode.
   */
  override property get CostVLs() : Iterable<WC7CostVersionList> {
    return _line.VersionList.WC7Costs
  }

  override property get Transactions() : Set<Transaction> {
    return _line.WC7Transactions.toSet()
  }

  override function prorateBasesFromCancellation() {
    for (jurisdiction in _line.WC7Jurisdictions) {
      var cancellationDate = _line.Branch.CancellationDate
      if(jurisdiction.canAddRPSD(cancellationDate) == null){
        jurisdiction.addWC7RatingPeriodStartDate(cancellationDate, TC_AUDIT)
      }
    }
    _line.updateWCExposuresAndModifiers()
  }

  override function createPolicyLineValidation(validationContext : PCValidationContext) : PolicyLineValidation {
    return WC7ValidationFactory.getPolicyLineValidation(validationContext, _line as productmodel.WC7Line)
  }

  override function preLocationDelete(location : PolicyLocation) {
    endDateClausesAndExposuresRelatedToLocation(location)
  }

  override property get LocationsInUseOnOrAfterSliceDate() : Set<Key> {
    // only time WC location should be removed automatically is when jurisdiction is removed - PC-13922
    // therefore we'll return the full set of locations for all slices of branch as being "in use"
    return _line.Branch.OOSSlices*.PolicyLocations*.FixedId.toSet()
  }

  private function endDateClausesAndExposuresRelatedToLocation(location : PolicyLocation) {
    for (exposure in _line.getAllExistingOrFutureCoveredEmployeesRelatedToLocation(location)){
      exposure.endDateWM(location.SliceDate)
    }

    for (supplDisExposure in _line.getExistingOrFutureSupplementaryDiseaseExposuresRelatedToLocation(location)){
      supplDisExposure.endDateWM(location.SliceDate)
    }
  }

  override function onBeginIssueJob() {
    super.onBeginIssueJob()
    // because we don't allow locations to be removed before quoting, clean them up before bind:
    _line.Branch.removeUnusedLocations()
  }

  override function createPolicyLineDiffHelper(reason : DiffReason, policyLine : PolicyLine) : WC7DiffHelper {
    return new WC7DiffHelper(reason, this._line, policyLine as WC7WorkersCompLine)
  }


  override function postApplyChangesFromBranch(sourcePeriod : PolicyPeriod) {
    for(employee in this._line.WC7CoveredEmployees){
      if(employee.Location == null){ // location has been deleted
        employee.removeWM()
      }
    }
    for(jurisdiction in _line.WC7Jurisdictions){
      jurisdiction.updateRPSDsAfterPeriodChange(sourcePeriod.PeriodStart)
    }
    _line.updateWCExposuresAndModifiers()
  }

  override function postSetPeriodWindow( oldEffDate: Date, oldExpDate: Date) {
    for (jurisdiction in _line.WC7Jurisdictions) {
      jurisdiction.adjustAnniversaryDate()
      jurisdiction.updateRPSDsAfterPeriodChange(oldEffDate)
    }
  }

  override function postCreateDraftBranchInNewPeriod() {
    var jurisdictions = this._line.WC7Jurisdictions
    for (jurisdiction in jurisdictions) {
      jurisdiction.updateRPSDsInNewPeriod()
    }
    // BrianS - LineAnswers are only used on the submission.  Remove answer to avoid history messages (GW-2069).
    for (answer in _line.LineAnswers) {
      _line.removeFromLineAnswers(answer);
    }
  }

  override function doGetTIVForCoverage(cov : Coverage) : BigDecimal {
    switch (cov.FixedId.Type) {
      //Workers Comp Line
      case entity.WC7WorkersCompCov.Type:
        return getWC7WorkersCompCovLimit(cov as WC7WorkersCompCov)

      case entity.WC7JurisdictionCov.Type:
        break
    }
    return BigDecimal.ZERO
  }

  private function getWC7WorkersCompCovLimit(cov : WC7WorkersCompCov) : BigDecimal {
    var insuredValue = BigDecimal.ZERO
    return insuredValue
  }

  override function createRatingEngine(method : RateMethod, parameters : Map<RateEngineParameter, Object>) : AbstractRatingEngine<WC7Line> {
    if (RateMethod.TC_SYSTABLE == method) {
      return new WC7SysTableRatingEngine(_line as WC7Line)
    }
    return new tdic.pc.config.rating.wc7.WC7RatingEngine(_line as WC7Line, parameters[RateEngineParameter.TC_RATEBOOKSTATUS] as RateBookStatus)
  }

  override property get SupportsNonSpecificLocations() : boolean {
    return true
  }

  override property get PolicyLocationCanBeRemovedWithoutRemovingAssociatedRisks() : boolean {
    return true
  }

  override function canDateSliceOnPropertyChange(bean : KeyableBean) : boolean {
    if (bean typeis WC7CoveredEmployee or bean typeis WC7FedCoveredEmployee) {
      // Apply property changes to the above objects in window mode
      return false
    }
    if (bean typeis WC7Modifier) {
      // Modifiers on Jurisdiction and (theoretically) RPSD should also be applied in window mode
      return not ((bean.OwningModifiable typeis WC7Jurisdiction) or (bean.OwningModifiable typeis RatingPeriodStartDate))
    }
    return super.canDateSliceOnPropertyChange(bean)
  }

  override property get BaseStateRequired() : boolean {
    return false
  }

  override function usesAllOOSSliceDates() : boolean {
    return false
  }

  function validateLocationAfterReturnFromPopUp(location : PolicyLocation) {
    //Do nothing by default
  }
  
  function validateAfterRemovingLocation(location : PolicyLocation) {
    //Do nothing by default
  }
  /**
   * US969, robk
   */
  override function onRenewalInitialize() {
    try {
      this._line.setCDAMembershipStatusOnAllOwnerOfficers_TDIC()

      //20161021 TJT GW-2371
      _line.initializeAndTransformWC7ClassCodes_TDIC()

      //20161023 TJT GW-948 GW-1762
      for (each in _line.WC7Jurisdictions){
        each.syncModifiers()
        _line.initializeAndTransformSafetyGroupSchedRatingFactorToModifier_TDIC()
      }
    } catch (e : java.lang.Exception) {

    }
  }

  override function createPolicyInfoValidation_TDIC (context : PCValidationContext) : AbstractPolicyInfoValidation {
    return new WC7PolicyInfoValidation(context, _line);
  }

  /**
   * Get the multi-line discount
   */
  override function getMultiLineDiscount_TDIC() : Map<String,boolean> {
    var retval = new HashMap<String,boolean>();
    var modifier : WC7Modifier;

    for (jurisdiction in _line.WC7Jurisdictions) {
      modifier = jurisdiction.WC7Modifiers.firstWhere(\m -> m.PatternCode == "WC7TDICMultiLineDiscount");
      retval.put(jurisdiction.DisplayName, modifier.BooleanModifier);
    }

    return retval;
  }

  /**
   * Set the multi-line discount
   */
  override function setMultiLineDiscount_TDIC (value : boolean) : void {
    var modifier : WC7Modifier;

    for (jurisdiction in _line.WC7Jurisdictions) {
      modifier = jurisdiction.WC7Modifiers.firstWhere(\m -> m.PatternCode == "WC7TDICMultiLineDiscount");
      if(null!=modifier) {
        modifier.BooleanModifier = value;
      }
    }
  }

  //20161129 TJT GW-TBD
  override function createUnderwriterEvaluator(context : PolicyEvalContext) : UnderwriterEvaluator {
    return new WC7_UnderwriterEvaluator(context)
  }

  /**
   * TDIC rating worksheet implementation
   * @param showConditionals
   * @return
   */
  override function getWorksheetRootNode(showConditionals: boolean): RowTreeRootNode {
    if(_line.Costs.Count > 0 and _line.Costs.first().UpdateTime >= WC7RatingUtil.getBasicTemplateRatingStartDate()) {
      var treeNodes : List<WorksheetTreeNodeContainer> = {}
      var jurCostMap = _line.Costs.cast(WC7Cost).partition(\c -> c.JurisdictionState).toAutoMap(\st -> java.util.Collections.emptySet<WC7Cost>())
      //var jurisdictions = _line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)

      var lineContainer = new WorksheetTreeNodeContainer(_line.DisplayName)
      lineContainer.ExpandByDefault = true

      //Jurisdiction level worksheets
      var jMainContainer = new WorksheetTreeNodeContainer(DisplayKey.get("Web.Admin.WC7AffinityGroupDetail.Jurisdictions"))
      jMainContainer.ExpandByDefault = true

      for (jurisdiction in _line.RepresentativeJurisdictions.orderBy(\juri -> juri.Jurisdiction)) {
        var j = jurisdiction.Jurisdiction
        var jContainer = new WorksheetTreeNodeContainer(j.DisplayName)
        jContainer.ExpandByDefault = true

        //jurisdiction costs
        var jurisdictionCosts = jurCostMap.get(j)
        if (jurisdictionCosts != null and jurisdictionCosts.Count > 0) {
          for (period in jurisdiction.RatingPeriods) {
            var pContainer : WorksheetTreeNodeContainer = null
            if (jurisdiction.RatingPeriods.Count > 1) {
              var startDate = gw.api.util.StringUtil.formatDate(period.RatingStart, "short")
              var endDate = gw.api.util.StringUtil.formatDate(period.RatingEnd, "short")
              var pContainerDesc = DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Worksheet.RatingPeriod", period.Number, startDate, endDate)
              pContainer = new WorksheetTreeNodeContainer(pContainerDesc)
              pContainer.ExpandByDefault = true
            }
            var periodCosts = jurisdictionCosts.byRatingPeriod(period).where(\c -> c.CalcOrder <= 400)
            for (cost in periodCosts) {
              var costContainer = new WorksheetTreeNodeContainer(cost.DisplayName)
              costContainer.addChildren(WorksheetTreeNodeUtil.buildTreeNodes(cost, showConditionals))
              if (pContainer != null) {
                pContainer.addChild(costContainer)
              } else {
                jContainer.addChild(costContainer)
              }
            }
            if (pContainer != null) {
              jContainer.addChild(pContainer)
            }
          }
          //other costs
          for (cost in jurisdictionCosts.where(\c -> c.CalcOrder > 400)) {
            var costContainer = new WorksheetTreeNodeContainer(cost.DisplayName)
            costContainer.addChildren(WorksheetTreeNodeUtil.buildTreeNodes(cost, showConditionals))
            jContainer.addChild(costContainer)
          }
        }
        jMainContainer.addChild(jContainer)
      }
      //add all Jurisdiction worksheets
      lineContainer.addChild(jMainContainer)

      treeNodes.add(lineContainer)
      return WorksheetTreeNodeUtil.buildRootNode(treeNodes)
    } else {
      getLegacyWorksheetRootNode_TDIC(showConditionals)
    }
    return super.getWorksheetRootNode(showConditionals)
  }

  /**
   * BT list of rating work sheets
   * @param showConditionals
   * @return RowTreeRootNode
   */
  private function getLegacyWorksheetRootNode_TDIC(showConditionals : boolean) : RowTreeRootNode {
    var treeNodes : List<WorksheetTreeNodeContainer> = {}

    var allWC7CoveredEmployees = this._line.VersionList.WC7CoveredEmployees.flatMap( \ empVL -> empVL.AllVersions )

    var jurisdictionContainer = new WorksheetTreeNodeContainer()
    jurisdictionContainer.ExpandByDefault = true
    if(allWC7CoveredEmployees.Count > 0) {
      for ( empLocation in allWC7CoveredEmployees )
      {
        var locationContainer = new WorksheetTreeNodeContainer(empLocation.DisplayName)
        locationContainer.ExpandByDefault = true
        jurisdictionContainer.addChild(locationContainer)
        var locationCosts : List<WC7Cost> = {}
        locationCosts.addAll(empLocation.VersionList.WC7Costs.flatMap( \ elt -> elt.AllVersions).toList())
// TODO JL need to finish out this adding of costs to the worksheet


      }
      treeNodes.add(jurisdictionContainer)
    }


    var taxes = _line.Branch.AllCosts.TaxSurcharges.orderBy(\ c -> c.DisplayName).toList()
    taxes.each(\ tax -> {
      var taxContainer = new WorksheetTreeNodeContainer(tax.DisplayName)
      taxContainer.addChildren(WorksheetTreeNodeUtil.buildTreeNodes(tax, showConditionals))
      treeNodes.add(taxContainer)
    })

    var otherCosts = _line.Branch.AllCosts.where(\ p ->  p.RateAmountType != TC_TAXSURCHARGE).whereTypeIs(WC7Cost)
    var allOtherCosts = otherCosts.flatMap(\oc -> oc.VersionList.AllVersions).toList()
    allOtherCosts
        .orderBy(\ otherCost -> otherCost.DisplayName)
        .each(\ otherCost -> {
          var otherContainer = new WorksheetTreeNodeContainer(otherCost.DisplayName)
          otherContainer.addChildren(WorksheetTreeNodeUtil.buildTreeNodes(otherCost, showConditionals))
          treeNodes.add(otherContainer)
        })

    // non-cost worksheets.
    // Note that this is only worksheets created by the current branch.
    var nonCost = _line.Branch.AllBeansWithWorksheets.entrySet().where(\ e -> not (e.Key typeis WC7Cost))
    nonCost.each(\ nc -> {
      var container = new WorksheetTreeNodeContainer(nc.Key.DisplayName)
      nc.Value.each(\ ws -> container.addChildren(WorksheetTreeNodeUtil.buildTreeNodes(ws, showConditionals)))
      treeNodes.add(container)
    })

    return WorksheetTreeNodeUtil.buildRootNode(treeNodes)
  }
}