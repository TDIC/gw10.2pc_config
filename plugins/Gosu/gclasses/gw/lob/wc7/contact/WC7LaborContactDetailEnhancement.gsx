package gw.lob.wc7.contact

@Export
enhancement WC7LaborContactDetailEnhancement : entity.WC7LaborContactDetail {
  
  property get WC7LaborContact() : WC7LaborContact {
    return this.WC7Contact as WC7LaborContact
  }

  property set WC7LaborContact(contact : WC7LaborContact) {
    this.WC7Contact = contact
  }
}
