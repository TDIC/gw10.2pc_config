package gw.lob.wc7.contact

uses gw.api.domain.Clause
uses java.lang.IllegalArgumentException

@Export
enhancement WC7LaborContactEnhancement : entity.WC7LaborContact {

  property get LaborContactDetails() : WC7LaborContactDetail[] {
    return this.WC7Details.whereTypeIs(WC7LaborContactDetail)
  }

  property get IncludedLaborContactDetails() : WC7IncludedLaborContactDetail[] {
    return LaborContactDetails.whereTypeIs(WC7IncludedLaborContactDetail)
  }

  property get ExcludedLaborContactDetails() : WC7ExcludedLaborContactDetail[] {
    return LaborContactDetails.whereTypeIs(WC7ExcludedLaborContactDetail)
  }

  /**
   * Adds a new {@link WC7IncludedLaborContactDetail} for this {@link WC7LaborContact}.
   * The detail's contract effective period is initialized to the branch's policy period.
   */
  function addNewIncludedLaborContactDetail(laborContractorCond : PolicyCondition) : WC7IncludedLaborContactDetail {
    var detail = new WC7IncludedLaborContactDetail(this.Branch)
    detail.setFieldValue("LaborContactCondition", laborContractorCond)
    detail.ContractEffectiveDate  = this.Branch.PeriodStart
    detail.ContractExpirationDate = this.Branch.PeriodEnd
    this.addToWC7Details(detail)
    return detail
  }

  /**
   * Adds a new {@link WC7ExcludedLaborContactDetail} for this {@link WC7LaborContact}.
   * The detail's contract effective period is initialized to the branch's policy period.
   */
  function addNewExcludedLaborContactDetail(laborContractorExcl : Exclusion) : WC7ExcludedLaborContactDetail {
    var detail = new WC7ExcludedLaborContactDetail(this.Branch)
    detail.setFieldValue("LaborContactExclusion", laborContractorExcl)
    detail.ContractEffectiveDate  = this.Branch.PeriodStart
    detail.ContractExpirationDate = this.Branch.PeriodEnd
    this.addToWC7Details(detail)
    return detail
  }

  function addNewLaborContactDetail(clause : Clause) : WC7LaborContactDetail {
    if (clause typeis PolicyCondition)
      return addNewIncludedLaborContactDetail(clause)
    else if (clause typeis Exclusion)
      return addNewExcludedLaborContactDetail(clause)
    else
      throw new IllegalArgumentException("Invalid clause type for a contact detail: " + typeof clause)
  }
}
