package gw.lob.wc7.contact

uses gw.api.domain.Clause
uses gw.api.util.DisplayableException
uses gw.lob.wc7.availability.WC7ContactAvailability
uses gw.api.productmodel.ClausePattern
uses gw.api.upgrade.PCCoercions

@Export
enhancement WC7PolicyContactRoleEnhancement : entity.WC7PolicyContactRole {

  /**
   * Remove the detail from this WC7LaborContact, and furthermore remove this WC7LaborContact
   * if it is the very last detail.  NOTE: use this method instead of removeFromDetails.
   */
  function removeDetail(toRemove : WC7ContactDetail) {
    this.removeFromWC7Details(toRemove)
    if (this.WC7Details.Count == 0) {
      this.Branch.removeFromPolicyContactRoles(this)
    }
  }

  function addNewBasicContactDetail(scheduleClause : Clause) : WC7ContactDetail {
    var detail = new WC7ContactDetail(this.Branch)
    detail.setParentClause(scheduleClause)
    this.addToWC7Details(detail)
    return detail
  }

  /**
   * Remove the detail from this WC7LaborContact, and furthermore remove this WC7LaborContact
   * if it is the very last detail.  NOTE: use this method instead of removeFromDetails.
   */
  function removeDetailFromCurrentAndChild(toRemove : WC7ContactDetail) {
    if(toRemove typeis WC7IncludedLaborContactDetail and toRemove.LaborContactCondition.Pattern == PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientEndorsementCond")) {
      var infoMsg = WC7ContactAvailability.removeRelatedContactDetails(toRemove)
      this.removeDetail(toRemove)

      if(not infoMsg.Empty) {
        throw new DisplayableException(infoMsg)
      }
    }
  }
}
