package gw.lob.wc7

uses gw.validation.PCValidationContext

class WC7ScreenLineValidation {
  protected var wc7Line : WC7Line

  construct(theLine: WC7Line) {
    wc7Line = theLine
  }

  function validateWCLineCoveragesStep() {
    PCValidationContext.doPageLevelValidation(\ context -> {
      var val = new WC7LineValidation(context, wc7Line)
      val.validateWCLineCoveragesStep()
    })
  }
}
