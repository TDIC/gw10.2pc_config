package gw.lob.wc7.rating

uses gw.api.database.Query
uses gw.api.database.Relop
uses java.lang.Integer
uses java.util.Date
uses gw.api.database.IQueryBeanResult
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

/**
 * Search criteria that can find rating steps in the <code>WC_Rating_Steps</code> system table.
 * See <code>matchAllInRange</code> methods for full details.
 */
@Export
class WC7RatingStepsSearchCriteria
{
  var _findDate  : Date
  var _findState : Jurisdiction
  
  construct( findDate : Date, findState : Jurisdiction )
  {
    if ( findDate == null )
    {
      // Throw because, while the match code will work (typically returning empty result set) passing
      // in a null findDate is probably indicative of a bug.  A null findState merely means we're searching
      // for the default (non-state) rating steps.
      throw "WC7RatingStepsSearchCriteria requires a findDate " + this
    }
    _findDate  = findDate
    _findState = findState
  }

  /**
   * Return a query that finds all steps from the WC_Rating_Steps system table matching the criteria such that
   * if there are *any* state-specific steps for the findState,
   * <ul>
   * <li><code>findState</code> matches
   * <li><code>findDate</code> falls within the [<code>effDate</code>, <code>expDate</code>) range
   * <li><code>calcOrder</code> falls within the [<code>fromStep</code>, <code>toStep</code>] range 
   *     (n.b. inclusive range)
   * </ul>
   * If there are no rating steps at all specified for the findState, then return the factors for all
   * states where the findDate and calcOrder matches (as described above).
   * The steps are ordered ascending by their calcOrder.
   */
  function matchAllInRange( fromStep : int, toStep : int ) : IQueryBeanResult<WC7RatingStepExt>
  {
    return findInRange( HasAnyStateSpecificSteps ? _findState : null, fromStep, toStep )
  }
  
  /**
   * Find all steps from the WC_Rating_Steps system table matching the criteria such that
   * <ul>
   * <li><code>findState</code> matches
   * <li><code>findDate</code> falls within the [<code>effDate</code>, <code>expDate</code>) range.
   *     If either dates is null, then that side of the range is considered unbound.
   * <li><code>calcOrder</code> falls within the [<code>fromStep</code>, <code>toStep</code>] range 
   *     (n.b. inclusive range)
   * </ul>
   * The steps are ordered ascending by their calcOrder.
   */
  private function findInRange( findState : Jurisdiction, fromStep : int, toStep : int ) : IQueryBeanResult<WC7RatingStepExt>
  {
    var stepQuery = Query.make(entity.WC7RatingStepExt).and(\r -> {
      r.compare("rateState", Relop.Equals, findState.Code)
      r.or(\r1 -> {
        r1.compare("effDate", Relop.Equals, null)
        r1.compare("effDate", Relop.LessThanOrEquals, _findDate)
      })
      r.or(\r2 -> {
        r2.compare("expDate", Relop.Equals, null)
        r2.compare("expDate", Relop.GreaterThan, _findDate)
      })
      r.compare("calcOrder", Relop.GreaterThanOrEquals, fromStep as Integer)
      r.compare("calcOrder", Relop.LessThanOrEquals, toStep as Integer)
    }).select()
    stepQuery.orderBy(
      QuerySelectColumns.path(Paths.make(entity.WC7RatingStepExt#calcOrder))
    )
    return stepQuery
  }
  
  /**
   * Return true if there are any steps from the WC_Rating_Steps system table for the findState
   * that are effective during the findDate.
   */
  private property get HasAnyStateSpecificSteps() : boolean
  {
    var stepQuery = Query.make(entity.WC7RatingStepExt).and(\r -> {
      r.compare("rateState", Relop.Equals, _findState.Code)
      r.or(\r1 -> {
        r1.compare("effDate", Relop.Equals, null)
        r1.compare("effDate", Relop.LessThanOrEquals, _findDate)
      })
      r.or(\r2 -> {
        r2.compare("expDate", Relop.Equals, null)
        r2.compare("expDate", Relop.GreaterThan, _findDate)
      })
    }).select()
    return stepQuery.getCount() > 0
  }

  override function toString() : String
  {
    return "{findDate=" + _findDate  + "}"
  }
  
}
