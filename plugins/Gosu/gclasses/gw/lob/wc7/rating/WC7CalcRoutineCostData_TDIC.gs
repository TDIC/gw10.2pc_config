package gw.lob.wc7.rating

uses gw.rating.CostDataWithOverrideSupport
uses gw.rating.flow.domain.CalcRoutineCostData
uses gw.rating.flow.domain.CalcRoutineCostDataWithTermOverride

uses java.lang.Integer
uses java.math.RoundingMode

class WC7CalcRoutineCostData_TDIC extends CalcRoutineCostDataWithTermOverride {

  construct(costData : CostDataWithOverrideSupport, mode : OverrideMode, defaultRoundingLevel : Integer, defaultRoundingMode : RoundingMode) {
    super(costData, mode , defaultRoundingLevel , defaultRoundingMode)
  }

  property set StatCode(stCode : String) {
    (_costData as WC7CostData).StatCode = stCode
  }
  
  property get StatCode() : String {
    return (_costData as WC7CostData).StatCode
  }

}