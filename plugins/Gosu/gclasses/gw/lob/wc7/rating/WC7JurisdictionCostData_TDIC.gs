package gw.lob.wc7.rating

uses java.util.Date
uses gw.api.effdate.EffDatedUtil
uses entity.windowed.WC7JurisdictionVersionList
uses java.util.List
uses gw.pl.persistence.core.Key
uses gw.pl.persistence.core.effdate.EffDatedVersionList
uses tdic.pc.config.rating.wc7.WC7RatingUtil

class WC7JurisdictionCostData_TDIC extends WC7CostData<WC7JurisdictionCost> {
  private var _jurisdictionID : Key
  private var _state : Jurisdiction as readonly State
  private var _costType : WC7JurisdictionCostType as readonly CostType
  private var _wc7Jurisdiction : WC7Jurisdiction

  construct(effDate : Date, expDate : Date, aWC7Jurisdiction : WC7Jurisdiction, costTypeArg : WC7JurisdictionCostType, order : int) {
    super(effDate, expDate)
    _jurisdictionID = aWC7Jurisdiction.FixedId
    _costType = costTypeArg
    _wc7Jurisdiction = aWC7Jurisdiction
    CalcOrder = order
  }

  construct(c : WC7JurisdictionCost) {
    super(c)
    _jurisdictionID = c.WC7Jurisdiction.FixedId
    _costType = c.JurisdictionCostType
    _wc7Jurisdiction = c.WC7Jurisdiction
  }

  override function setSpecificFieldsOnCost(line : WC7WorkersCompLine, cost : WC7JurisdictionCost) {
    super.setSpecificFieldsOnCost( line, cost )
    cost = cost.Unsliced
    cost.setFieldValue("WC7Jurisdiction", _jurisdictionID)
    cost.JurisdictionCostType = CostType
    cost.StatCode = _statCode
    if(WC7RatingUtil.isJurisdictionCostTaxesAndSurcharges(cost)){
      cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
      cost.ChargePattern = ChargePattern.TC_TAXES
    }
  }

  override property get Jurisdiction() : Jurisdiction {
    return _wc7Jurisdiction.Jurisdiction
  }



  private function matchesStep(cost : WC7JurisdictionCost) : boolean {
    return cost.JurisdictionCostType == CostType
  }

  override property get KeyValues() : List<Object> {
    return {CostType, _jurisdictionID}
  }

  /**
   * Subtypes must implement this function in order to find the candidate set of costs to potentially reuse (or clone)
   * when persisting this CostData.  This function must return a List of size 0 or 1.  For the simple case where a particular
   * thing (like a vehicle-level coverage) only ever has a single cost, this method can generally simply return something
   * like coverageVersionList.Costs.
   * <p>
   * For cases where there could potentially be multiple Costs that live off of an entity
   * (for example, a line-level coverage that has one Cost per vehicle), this method should find the
   * appropriate cost version list.  Thus, the matching code might look more like
   * <code>
   * coverageVersionList.Costs.where(\c -> c.AllVersions.First.Vehicle.FixedID == _vehicleID)
   * </code>
   * i.e. finding the VersionList for each possible cost and then extracting out the ones (there should be 0 or 1) that match.
   * <p>
   * In other words, <em>the implementation of this function must be consistent with the implementation of KeyValues(),
   * so that the values returned from getVersionedCosts() have Key values which match those of this CostData.</em>
   *
   * @param line the PolicyLine that we're creating costs for, which is generally used to access the associated PolicyPeriod in order
   *             to traverse the tree
   */
  override function getVersionedCosts(line : WC7WorkersCompLine) : List<EffDatedVersionList> {
    var jurisdictionVL = EffDatedUtil.createVersionList( line.Branch, _jurisdictionID ) as WC7JurisdictionVersionList
    return jurisdictionVL.Costs.where( \ costVL -> matchesStep(costVL.AllVersions.first())).toList()
  }
}
