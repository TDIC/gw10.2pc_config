package gw.lob.wc7.rating

uses java.util.Date
uses gw.pl.persistence.core.Key
uses gw.api.effdate.EffDatedUtil
uses entity.windowed.WC7AtomicEnergyExposureVersionList
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.effdate.EffDatedVersionList
uses java.util.List

/**
 * Atomic Energy Cost Data
 */
@Export
class WC7AtomicEnergyCostData extends WC7CostData<WC7AtomicEnergyCost> {

  private var _exposure : Key as readonly ExposureID
  private var _jurisdiction : Jurisdiction as readonly State
  
  construct(effDate : Date, expDate : Date, theJurisdiction : Jurisdiction, exposure : Key, c : Currency, rateCache : PolicyPeriodFXRateCache = null) {
    super(effDate, expDate, c, rateCache)
    _exposure = exposure
    _jurisdiction = theJurisdiction
  }
  
  override property get Jurisdiction() : Jurisdiction {
    return State 
  }
  
  override function setSpecificFieldsOnCost(line : WC7WorkersCompLine, cost : WC7AtomicEnergyCost) {
    super.setSpecificFieldsOnCost( line, cost )
    cost.setFieldValue("WC7AtomicEnergyExposure", ExposureID)
    cost.StatCode = StatCode
  }
  
  override function getVersionedCosts(line : WC7WorkersCompLine) : List<EffDatedVersionList> {
    var expVL = EffDatedUtil.createVersionList(line.Branch, ExposureID) as WC7AtomicEnergyExposureVersionList
    return expVL.WC7Costs
  }

  override property get KeyValues() : List<Object> {
    return {ExposureID}  
  }

  property get StatCode() : String {
    return _statCode
  }
}
