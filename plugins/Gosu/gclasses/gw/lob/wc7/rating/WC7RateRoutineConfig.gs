package gw.lob.wc7.rating

uses gw.pc.rating.flow.util.InScopeUsageBase
uses gw.plugin.rateflow.IRateRoutineConfig
uses java.math.RoundingMode
uses java.lang.Integer
uses gw.plugin.rateflow.CostDataBase
uses gw.lang.reflect.IType
uses gw.plugin.rateflow.ICostDataWrapper
uses gw.rating.CostData
uses gw.rating.CostDataWithOverrideSupport
uses gw.rating.flow.domain.CalcRoutineCostDataWithAmountOverride
uses gw.rating.flow.domain.CalcRoutineCostDataWithPremiumCap
uses gw.rating.flow.domain.CalcRoutineCostDataWithTermOverride
uses gw.lang.reflect.IPropertyInfo
uses gw.pc.rating.flow.AvailableCoverageWrapper
uses gw.rating.flow.util.ItemFilter

@Export
class WC7RateRoutineConfig implements IRateRoutineConfig {

  construct() {
  }
  var noCostDataParamSets : Set<String> = {}

  private function paramSetAssociatedWithCostData(paramSet : CalcRoutineParameterSet) : boolean {
    if (noCostDataParamSets?.contains(paramSet.Code)) {
      return false
    } else {
      return true
    }
  }

  override function getCostDataWrapperType(paramSet : CalcRoutineParameterSet) : IType {

      return WC7CalcRoutineCostData_TDIC

  }

  override function makeCostDataWrapper(paramSet : CalcRoutineParameterSet, 
                                        costData : CostDataBase, 
                                        defaultRoundingLevel : Integer, 
                                        defaultRoundingMode : RoundingMode) : ICostDataWrapper {
    if(costData typeis WC7CostData and (costData.Overridable)) {
      new CalcRoutineCostDataWithTermOverride(costData as CostDataWithOverrideSupport,
          CalcRoutineCostDataWithAmountOverride.OverrideMode.APPROXIMATE_STANDARD_RATES,
          defaultRoundingLevel,
          defaultRoundingMode)
    }


        return new WC7CalcRoutineCostData_TDIC(costData as CostDataWithOverrideSupport,CalcRoutineCostDataWithAmountOverride.OverrideMode.APPROXIMATE_STANDARD_RATES ,defaultRoundingLevel, defaultRoundingMode)

  }

  override function worksheetsEnabledForLine(linePatternCode: String): boolean {
    return true
  }

  override function includeProperty(policyLinePatternCode: String, prop: IPropertyInfo): Boolean {
    return null
  }


  override function getCoverageWrappersForLine(linePatternCode: String): AvailableCoverageWrapper[] {
    return {}
  }

  override function filterIrrelevantItems(input: List<InScopeUsageBase>, policyLinePatternCode: String): List<InScopeUsageBase> {
    return ItemFilter.applyDefaultFilters(input)
  }
}
