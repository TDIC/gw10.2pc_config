package gw.lob.wc7.rating

uses java.util.Date
uses gw.pl.persistence.core.Key
uses gw.financials.PolicyPeriodFXRateCache
uses java.util.List

/**
 * Abstract methods for a {@link WC7CostData} for a cost data 
 * associated with a {@link entity.WC7CoveredEmployeeBase}
 */
@Export
abstract class WC7BaseCovEmpCostData<T extends WC7Cost> extends WC7CostData<T> {
  private var _empID : Key as readonly EmpID
  private var _covID : Key as readonly CovID
  private var _state : Jurisdiction as readonly State

  construct(effDate : Date, expDate : Date, empIDArg : Key, covIDArg : Key, stateArg : Jurisdiction, c : Currency, rateCache : PolicyPeriodFXRateCache) {
    super(effDate, expDate, c, rateCache)
    _empID = empIDArg
    _covID = covIDArg
    _state = stateArg 
  }

  override property get Jurisdiction() : Jurisdiction {
    return State 
  }

  override function setSpecificFieldsOnCost(line : WC7WorkersCompLine, cost : T) {
    super.setSpecificFieldsOnCost( line, cost )
    setCoveredEmployee(_empID, cost)
    setCoverage(_covID, cost)
    cost.StatCode = StatCode
  }
  
  override property get KeyValues() : List<Object> {
    return {CovID, EmpID}  
  }

  /**
   * Returns WC7ClassCode of the Covered Employee (including FELA and Maritime) related to this cost
   */
  function getWC7ClassCode(workersCompLine : WC7WorkersCompLine) : WC7ClassCode {
    return workersCompLine.AllWC7CoveredEmployeeBaseWM.firstWhere(\ w -> w.FixedId == this.EmpID).ClassCode
  }

  //----------------------------------------------------------------- abstract functions
  
  /**
   * Set the Covered Employee property for the associated cost with the provided key.
   * @param coveredEmployeeFixedID the FixedID of a WC7CoveredEmployeeBase
   */
  abstract function setCoveredEmployee(coveredEmployeeFixedID : Key, cost : T)
  
  /**
   * Set the Coverage property for the associated cost with the provided key.
   * @param coverageFixedID the FixedID of a WC7WorkersCompCov
   */
  abstract function setCoverage(coverageFixedID : Key, cost : T)
}
