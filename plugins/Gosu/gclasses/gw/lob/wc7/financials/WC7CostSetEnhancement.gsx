package gw.lob.wc7.financials

uses java.util.Set

uses gw.api.util.DateUtil
uses gw.lob.wc7.rating.WC7RatingPeriod
uses java.util.ArrayList
uses gw.api.ui.WC7CostWrapper
uses gw.api.locale.DisplayKey
uses tdic.pc.config.rating.wc7.WC7RatingUtil

enhancement WC7CostSetEnhancement<T extends WC7Cost> : Set<T>{
  /**
   * Returns the costs in this set that have an effective date in the rating period's [RatingStart, RatingEnd).
   */
  reified function byRatingPeriod( ratingPeriod : WC7RatingPeriod ) : Set<T> {
    var start = ratingPeriod.RatingStart.trimToMidnight()
    var end   = ratingPeriod.RatingEnd.trimToMidnight()
    return this.where( \ c -> c.EffDate >= start and c.EffDate < end ).toSet()
  }
  
  /**
   * Returns the costs in this set that have a calcOrder in [calcOrder, endOrder].
   */
  reified function byCalcOrder( startOrder : int, endOrder : int ) : Set<T> {
    return this.where( \ c -> c.CalcOrder >= startOrder and c.CalcOrder <= endOrder ).toSet()
  }

  /**
   *
   * TDIC Cusotm function for handling v8 quotes
   */
  reified function byCalcOrder_TDIC( startOrder : int, endOrder : int, premiumLevel : typekey.WC7PremiumLevelType ) : Set<T> {

    if(canUseBT()) {
      return this.byCalcOrder(startOrder, endOrder)
    }
    var startOrd = 0
    var endOrd = 0
    switch(premiumLevel) {
      case WC7PremiumLevelType.TC_TOTALMANUALPREMIUM:
        endOrd = 100
        break
      case WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM:
        startOrd = startOrder != 0 ? 101 : startOrder
        endOrd = 300
        break
      case WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM:
        startOrd = startOrder != 0 ? 301 : startOrder
        endOrd = 400
        break
      case WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM:
        startOrd = startOrder != 0 ? 401 : startOrder
        endOrd = 500
        break
      case WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM:
        startOrd = startOrder != 0 ? 501 : startOrder
        endOrd = 1000000
        break

        default:

    }
    return this.where( \ c -> c.CalcOrder >= startOrd and c.CalcOrder <= endOrd ).toSet()
  }

  reified function getOtherPremiumAndSurcharges(currency : Currency) : WC7CostWrapper[]{
    var ordered = new ArrayList<WC7CostWrapper>()

    var state = this.first().JurisdictionState
    var header1 = DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.EstimatedAnnualPremium")

    if(canUseBT()) {
      ordered.addWC7Costs(this.where(\t -> t.CalcOrder >= 400))
      ordered.add(new WC7CostWrapper(500.5, header1, this.byCalcOrder(0, 500).AmountSum(currency), true))
    } else {
      ordered.addWC7Costs(this.where(\t -> t.CalcOrder >= 500))
      ordered.add(new WC7CostWrapper(590.5, header1, this.byCalcOrder(0, 590).AmountSum(currency), true))
    }
    var header2 = DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalAmountDue")
    ordered.add( new WC7CostWrapper(10000000, header2, this.AmountSum(currency), true) )

    return ordered.toTypedArray()
  }
  
  reified function getStandardPremiums(currency : Currency) : WC7CostWrapper[]{
    var ordered = new ArrayList<WC7CostWrapper>()
    //ordered.addCosts( this.where( \ t -> t.CalcOrder < 400 ))
    //var costcollection = this as Collection<WC7Cost>
    if(canUseBT()) {
      ordered.addAll(this.where(\ c -> c.CalcOrder < 400 and c.CalcOrder != 110) .map(\ c -> new WC7CostWrapper(c)))
      ordered.addWC7SpecificWaiverCosts(this.where(\c -> c.CalcOrder == 110 ))  //Specific Waiver costs
      ordered.add(new WC7CostWrapper(100.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalManualPremium"),
          this.byCalcOrder(0, 100).AmountSum(currency), true) )
      ordered.add( new WC7CostWrapper(200.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalSubjectPremium"),
          this.byCalcOrder( 0, 200 ).AmountSum(currency), true) )
      ordered.add( new WC7CostWrapper(300.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalModifiedPremium"),
          this.byCalcOrder( 0, 300 ).AmountSum(currency), true))
      ordered.add( new WC7CostWrapper(400.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalStandardPremium"),
          this.byCalcOrder( 0, 400 ).AmountSum(currency), true))
    } else {
      ordered.addAll(this.where(\ c -> c.CalcOrder == 0).orderBy( \ elt -> elt.ClassCode) .map(\ c -> new WC7CostWrapper(c)).orderBy( \ elt -> elt.LocNumber))
      ordered.addWC7SpecificWaiverCosts(this.where(\c -> c.CalcOrder == 210 ))  //Specific Waiver costs
      ordered.addAll(this.where(\ c -> c.CalcOrder > 0 and c.CalcOrder < 500 and c.CalcOrder != 210) .map(\ c -> new WC7CostWrapper(c)))
      ordered.add(new WC7CostWrapper(100.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalManualPremium"),
          this.byCalcOrder(0, 100).AmountSum(currency), true) )
      ordered.add( new WC7CostWrapper(300.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalSubjectPremium"),
          this.byCalcOrder( 0, 300 ).AmountSum(currency), true) )
      ordered.add( new WC7CostWrapper(400.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalModifiedPremium"),
          this.byCalcOrder( 0, 400 ).AmountSum(currency), true))
      ordered.add( new WC7CostWrapper(500.5,
          DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalStandardPremium"),
          this.byCalcOrder( 0, 500 ).AmountSum(currency), true))
    }


    return ordered.toTypedArray()
  }
  
  reified property get StandardPremiumCosts() : Set<WC7Cost> {
    //return this.where(\ cost -> cost.PremiumLevelType <= WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).toSet()
    return this.where(\ cost -> cost.PremiumLevelType <= WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).toSet()
  }  
  
  reified property get OtherPremiumAndSurchargeCosts() : Set<WC7Cost> {
    //return this.where(\ cost -> cost.PremiumLevelType > WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).toSet()
    return this.where(\ cost -> cost.PremiumLevelType > WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).toSet()
  }

  reified private function canUseBT() : Boolean {
    return WC7RatingUtil.getBasicTemplateRatingStartDate() < this.first().UpdateTime
  }
}
