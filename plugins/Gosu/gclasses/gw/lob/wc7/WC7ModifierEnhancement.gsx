package gw.lob.wc7

uses java.math.BigDecimal

enhancement WC7ModifierEnhancement : entity.WC7Modifier {

  function isValueWithinRange( targetAsBD : BigDecimal  ) : boolean {
    return this.Minimum <= targetAsBD and targetAsBD <= this.Maximum
  }
}
