package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement BOPBuildingCoverageEnhancement : entity.BOPBuilding {
  property get BOPACVEndCov_TDIC () : productmodel.BOPACVEndCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zisg8k4geedjde8uflhhve6bih9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPACVEndCov_TDIC
  }
  
  property get BOPACVEndCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zisg8k4geedjde8uflhhve6bih9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPAccReceivablesCov_TDIC () : productmodel.BOPAccReceivablesCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zbig8l1e3trj9cv80r2vbiir219") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPAccReceivablesCov_TDIC
  }
  
  property get BOPAccReceivablesCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zbig8l1e3trj9cv80r2vbiir219") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPAddDebrisRemoval_TDIC () : productmodel.BOPAddDebrisRemoval_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zu4h41behod6ccv5f079cggah08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPAddDebrisRemoval_TDIC
  }
  
  property get BOPAddDebrisRemoval_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zu4h41behod6ccv5f079cggah08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPArsonReward_TDIC () : productmodel.BOPArsonReward_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z7oi2n0qc1q1t0961ob1f4hnbhb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPArsonReward_TDIC
  }
  
  property get BOPArsonReward_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z7oi2n0qc1q1t0961ob1f4hnbhb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBPPDebrisRemoval_TDIC () : productmodel.BOPBPPDebrisRemoval_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zjsjoo0sbrf6i4voifalmah3eeb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBPPDebrisRemoval_TDIC
  }
  
  property get BOPBPPDebrisRemoval_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zjsjoo0sbrf6i4voifalmah3eeb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBPPOffPremises_TDIC () : productmodel.BOPBPPOffPremises_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1gg0l3t8ulbn62413d3jhso5n9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBPPOffPremises_TDIC
  }
  
  property get BOPBPPOffPremises_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1gg0l3t8ulbn62413d3jhso5n9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBuilDebrisRemoval_TDIC () : productmodel.BOPBuilDebrisRemoval_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zjrg6da4bopngdrmma4rqmf72t9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBuilDebrisRemoval_TDIC
  }
  
  property get BOPBuilDebrisRemoval_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zjrg6da4bopngdrmma4rqmf72t9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBuildingCov () : productmodel.BOPBuildingCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBuildingCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBuildingCov
  }
  
  property get BOPBuildingCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBuildingCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBuildingOwnersLiabCov_TDIC () : productmodel.BOPBuildingOwnersLiabCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zosiit8dmp9r07tbqa1dis38308") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBuildingOwnersLiabCov_TDIC
  }
  
  property get BOPBuildingOwnersLiabCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zosiit8dmp9r07tbqa1dis38308") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBusIncDepPrpCov () : productmodel.BOPBusIncDepPrpCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncDepPrpCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBusIncDepPrpCov
  }
  
  property get BOPBusIncDepPrpCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncDepPrpCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBusIncExtCov () : productmodel.BOPBusIncExtCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncExtCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBusIncExtCov
  }
  
  property get BOPBusIncExtCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncExtCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPBusIncPayrollCov () : productmodel.BOPBusIncPayrollCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncPayrollCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPBusIncPayrollCov
  }
  
  property get BOPBusIncPayrollCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPBusIncPayrollCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPCAEqBldgRecCov () : productmodel.BOPCAEqBldgRecCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCAEqBldgRecCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPCAEqBldgRecCov
  }
  
  property get BOPCAEqBldgRecCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCAEqBldgRecCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPCAEqBldgSubCov () : productmodel.BOPCAEqBldgSubCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCAEqBldgSubCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPCAEqBldgSubCov
  }
  
  property get BOPCAEqBldgSubCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCAEqBldgSubCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPCondoUnitOwnCov () : productmodel.BOPCondoUnitOwnCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCondoUnitOwnCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPCondoUnitOwnCov
  }
  
  property get BOPCondoUnitOwnCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPCondoUnitOwnCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPDEBLCov_TDIC () : productmodel.BOPDEBLCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1lgmajmeen788t3gfvrsk7gg1a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPDEBLCov_TDIC
  }
  
  property get BOPDEBLCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1lgmajmeen788t3gfvrsk7gg1a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPDMWLDCov_TDIC () : productmodel.BOPDMWLDCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsaggrmo36h523k2m9nd4a11sgb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPDMWLDCov_TDIC
  }
  
  property get BOPDMWLDCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsaggrmo36h523k2m9nd4a11sgb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPDeductibleCov_TDIC () : productmodel.BOPDeductibleCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zr9jom897p36564oq9fc312g949") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPDeductibleCov_TDIC
  }
  
  property get BOPDeductibleCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zr9jom897p36564oq9fc312g949") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPDentalGenLiabilityCov_TDIC () : productmodel.BOPDentalGenLiabilityCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z9jjc41jfhf7b9qb00s5j4kemm8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPDentalGenLiabilityCov_TDIC
  }
  
  property get BOPDentalGenLiabilityCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z9jjc41jfhf7b9qb00s5j4kemm8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPDigitalXRayCov_TDIC () : productmodel.BOPDigitalXRayCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zcnjk835qoul4bi73dn4nhqhg39") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPDigitalXRayCov_TDIC
  }
  
  property get BOPDigitalXRayCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zcnjk835qoul4bi73dn4nhqhg39") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEDPCov_TDIC () : productmodel.BOPEDPCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zp5iakp6u4hab5nmkl7da19tke8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEDPCov_TDIC
  }
  
  property get BOPEDPCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zp5iakp6u4hab5nmkl7da19tke8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPElectricalSchedCov () : productmodel.BOPElectricalSchedCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPElectricalSchedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPElectricalSchedCov
  }
  
  property get BOPElectricalSchedCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPElectricalSchedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEmpDisCov_TDIC () : productmodel.BOPEmpDisCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zmch4nmekd73h7qjqn0d1drrdo9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEmpDisCov_TDIC
  }
  
  property get BOPEmpDisCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zmch4nmekd73h7qjqn0d1drrdo9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEncCovEndtCond_TDIC () : productmodel.BOPEncCovEndtCond_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zg5hs3ump21fvbtnqtuctciel8a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEncCovEndtCond_TDIC
  }
  
  property get BOPEncCovEndtCond_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zg5hs3ump21fvbtnqtuctciel8a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEncCovFineCond_TDIC () : productmodel.BOPEncCovFineCond_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zv7ioth236fmv9u4v5v781j1988") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEncCovFineCond_TDIC
  }
  
  property get BOPEncCovFineCond_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zv7ioth236fmv9u4v5v781j1988") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEncCovMoneySCond_TDIC () : productmodel.BOPEncCovMoneySCond_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8pi8c2r7n7u15cqgbfttvn4k69") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEncCovMoneySCond_TDIC
  }
  
  property get BOPEncCovMoneySCond_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8pi8c2r7n7u15cqgbfttvn4k69") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEqBldgCov () : productmodel.BOPEqBldgCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPEqBldgCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEqBldgCov
  }
  
  property get BOPEqBldgCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPEqBldgCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEqSpBldgCov () : productmodel.BOPEqSpBldgCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPEqSpBldgCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEqSpBldgCov
  }
  
  property get BOPEqSpBldgCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPEqSpBldgCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPEquipBreakCov_TDIC () : productmodel.BOPEquipBreakCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zkshsmut05c1t666g086548hkj8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPEquipBreakCov_TDIC
  }
  
  property get BOPEquipBreakCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zkshsmut05c1t666g086548hkj8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPExtraExpenseCov_TDIC () : productmodel.BOPExtraExpenseCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zg2jcbpr6nptd5la3eaddc7umf8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPExtraExpenseCov_TDIC
  }
  
  property get BOPExtraExpenseCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zg2jcbpr6nptd5la3eaddc7umf8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPFineArtsCov_TDIC () : productmodel.BOPFineArtsCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("znvhc0702m7o1aflj04t9dd2nb9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPFineArtsCov_TDIC
  }
  
  property get BOPFineArtsCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("znvhc0702m7o1aflj04t9dd2nb9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPFireDepSerCharge_TDIC () : productmodel.BOPFireDepSerCharge_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsejs7vavqetb94d3c18onhvtm9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPFireDepSerCharge_TDIC
  }
  
  property get BOPFireDepSerCharge_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsejs7vavqetb94d3c18onhvtm9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPFireExtRecharge_TDIC () : productmodel.BOPFireExtRecharge_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zemik698oc76l3rrqiibjlt75hb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPFireExtRecharge_TDIC
  }
  
  property get BOPFireExtRecharge_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zemik698oc76l3rrqiibjlt75hb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPFuncPerPropCov () : productmodel.BOPFuncPerPropCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPFuncPerPropCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPFuncPerPropCov
  }
  
  property get BOPFuncPerPropCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPFuncPerPropCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPFungiCov_TDIC () : productmodel.BOPFungiCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z6gi8jki4mi9q2pd4pv18hoddlb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPFungiCov_TDIC
  }
  
  property get BOPFungiCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z6gi8jki4mi9q2pd4pv18hoddlb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPGoldPreMetals_TDIC () : productmodel.BOPGoldPreMetals_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8siefhq6fdef8jc2a6obcq6c7b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPGoldPreMetals_TDIC
  }
  
  property get BOPGoldPreMetals_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8siefhq6fdef8jc2a6obcq6c7b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPILMineSubCov_TDIC () : productmodel.BOPILMineSubCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zu0h6ucq7djqs8vntfod4orobeb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPILMineSubCov_TDIC
  }
  
  property get BOPILMineSubCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zu0h6ucq7djqs8vntfod4orobeb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPLockandKeyCov_TDIC () : productmodel.BOPLockandKeyCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zmlikbq1ajjinb0q7flemtabls9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPLockandKeyCov_TDIC
  }
  
  property get BOPLockandKeyCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zmlikbq1ajjinb0q7flemtabls9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPLossOutdoorTSP_TDIC () : productmodel.BOPLossOutdoorTSP_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z5iho5bmkdr2o8c8b9u5aqjojha") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPLossOutdoorTSP_TDIC
  }
  
  property get BOPLossOutdoorTSP_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z5iho5bmkdr2o8c8b9u5aqjojha") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPLossofIncomeTDIC () : productmodel.BOPLossofIncomeTDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zheg44l0d2gq1drv5erva6feqh9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPLossofIncomeTDIC
  }
  
  property get BOPLossofIncomeTDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zheg44l0d2gq1drv5erva6feqh9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPMALeadPoisonCov () : productmodel.BOPMALeadPoisonCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMALeadPoisonCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPMALeadPoisonCov
  }
  
  property get BOPMALeadPoisonCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMALeadPoisonCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPMATenantReloCov () : productmodel.BOPMATenantReloCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMATenantReloCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPMATenantReloCov
  }
  
  property get BOPMATenantReloCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMATenantReloCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPManuscriptEndorsement_TDIC () : productmodel.BOPManuscriptEndorsement_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zk7iqltjgu5ama8il02gu686a3a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPManuscriptEndorsement_TDIC
  }
  
  property get BOPManuscriptEndorsement_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zk7iqltjgu5ama8il02gu686a3a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPMechBreakdownCov () : productmodel.BOPMechBreakdownCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMechBreakdownCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPMechBreakdownCov
  }
  
  property get BOPMechBreakdownCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMechBreakdownCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPMineSubCov () : productmodel.BOPMineSubCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMineSubCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPMineSubCov
  }
  
  property get BOPMineSubCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPMineSubCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPMoneySecCov_TDIC () : productmodel.BOPMoneySecCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlbjk96o7rnok3p000ihkifegja") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPMoneySecCov_TDIC
  }
  
  property get BOPMoneySecCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlbjk96o7rnok3p000ihkifegja") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPNewBPP_TDIC () : productmodel.BOPNewBPP_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zt4gk8olcv0h64ohrmljjd4isbb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPNewBPP_TDIC
  }
  
  property get BOPNewBPP_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zt4gk8olcv0h64ohrmljjd4isbb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPNewBuildingCov_TDIC () : productmodel.BOPNewBuildingCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zuijgt6hjolb08s620vid4v6no9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPNewBuildingCov_TDIC
  }
  
  property get BOPNewBuildingCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zuijgt6hjolb08s620vid4v6no9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPOrdLaw_TDIC () : productmodel.BOPOrdLaw_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zv1gsmrq59r7sc2dkpt44qhvrob") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPOrdLaw_TDIC
  }
  
  property get BOPOrdLaw_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zv1gsmrq59r7sc2dkpt44qhvrob") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPOrdinanceCov () : productmodel.BOPOrdinanceCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPOrdinanceCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPOrdinanceCov
  }
  
  property get BOPOrdinanceCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPOrdinanceCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPOutdoorProperty_TDIC () : productmodel.BOPOutdoorProperty_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zudh0u246h4lu2gkcvprd4qpv38") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPOutdoorProperty_TDIC
  }
  
  property get BOPOutdoorProperty_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zudh0u246h4lu2gkcvprd4qpv38") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPPersonalEffects_TDIC () : productmodel.BOPPersonalEffects_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ztrg8j55mbrmldbv3c4n3t8l6j8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPPersonalEffects_TDIC
  }
  
  property get BOPPersonalEffects_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ztrg8j55mbrmldbv3c4n3t8l6j8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPPersonalPropCov () : productmodel.BOPPersonalPropCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPPersonalPropCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPPersonalPropCov
  }
  
  property get BOPPersonalPropCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPPersonalPropCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPPollCleanRemoval_TDIC () : productmodel.BOPPollCleanRemoval_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zodj03cmtlrv8133pq3eed65c08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPPollCleanRemoval_TDIC
  }
  
  property get BOPPollCleanRemoval_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zodj03cmtlrv8133pq3eed65c08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPReceivablesCov () : productmodel.BOPReceivablesCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPReceivablesCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPReceivablesCov
  }
  
  property get BOPReceivablesCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPReceivablesCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPRentalIncome_TDIC () : productmodel.BOPRentalIncome_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zpahgpno0f10321hr99hdamsqu9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPRentalIncome_TDIC
  }
  
  property get BOPRentalIncome_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zpahgpno0f10321hr99hdamsqu9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPSigns_TDIC () : productmodel.BOPSigns_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z01io2oelb0igd5u752bak2t9s8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPSigns_TDIC
  }
  
  property get BOPSigns_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z01io2oelb0igd5u752bak2t9s8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPTenantsLiabilityCov () : productmodel.BOPTenantsLiabilityCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPTenantsLiabilityCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPTenantsLiabilityCov
  }
  
  property get BOPTenantsLiabilityCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPTenantsLiabilityCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPUtilDirectCov () : productmodel.BOPUtilDirectCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPUtilDirectCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPUtilDirectCov
  }
  
  property get BOPUtilDirectCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPUtilDirectCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPUtilTimeCov () : productmodel.BOPUtilTimeCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPUtilTimeCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPUtilTimeCov
  }
  
  property get BOPUtilTimeCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPUtilTimeCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPVacancyChangeCov () : productmodel.BOPVacancyChangeCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPVacancyChangeCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPVacancyChangeCov
  }
  
  property get BOPVacancyChangeCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPVacancyChangeCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPVacancyCov () : productmodel.BOPVacancyCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPVacancyCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPVacancyCov
  }
  
  property get BOPVacancyCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPVacancyCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPValuablePapersCov () : productmodel.BOPValuablePapersCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPValuablePapersCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPValuablePapersCov
  }
  
  property get BOPValuablePapersCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("BOPValuablePapersCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPValuablePapersCov_TDIC () : productmodel.BOPValuablePapersCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z9nh0fg56dmqc8e72uj26rffd59") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPValuablePapersCov_TDIC
  }
  
  property get BOPValuablePapersCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z9nh0fg56dmqc8e72uj26rffd59") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get BOPWASTOPGAP_TDIC () : productmodel.BOPWASTOPGAP_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zrugiv2odb03g2pv52obqbnrg3b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.BOPWASTOPGAP_TDIC
  }
  
  property get BOPWASTOPGAP_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zrugiv2odb03g2pv52obqbnrg3b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}