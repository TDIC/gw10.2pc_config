package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement WC7WorkersCompLineExclusionEnhancement : entity.WC7WorkersCompLine {
  property get WC7CustomizedExcl_TDIC () : productmodel.WC7CustomizedExcl_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CustomizedExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7CustomizedExcl_TDIC
  }
  
  property get WC7CustomizedExcl_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CustomizedExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7DesignatedLocationsExcl_TDIC () : productmodel.WC7DesignatedLocationsExcl_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedLocationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7DesignatedLocationsExcl_TDIC
  }
  
  property get WC7DesignatedLocationsExcl_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedLocationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7DesignatedOperationsDesignatedLocationsExcl_TDIC () : productmodel.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedOperationsDesignatedLocationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC
  }
  
  property get WC7DesignatedOperationsDesignatedLocationsExcl_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedOperationsDesignatedLocationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7DesignatedOperationsExcl_TDIC () : productmodel.WC7DesignatedOperationsExcl_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedOperationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7DesignatedOperationsExcl_TDIC
  }
  
  property get WC7DesignatedOperationsExcl_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedOperationsExcl_TDIC") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7DesignatedWorkplacesExclEndorsementExcl () : productmodel.WC7DesignatedWorkplacesExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedWorkplacesExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7DesignatedWorkplacesExclEndorsementExcl
  }
  
  property get WC7DesignatedWorkplacesExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DesignatedWorkplacesExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7DomesticAndAgriculturalWorkersExclEndorsemeExcl () : productmodel.WC7DomesticAndAgriculturalWorkersExclEndorsemeExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DomesticAndAgriculturalWorkersExclEndorsemeExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7DomesticAndAgriculturalWorkersExclEndorsemeExcl
  }
  
  property get WC7DomesticAndAgriculturalWorkersExclEndorsemeExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7DomesticAndAgriculturalWorkersExclEndorsemeExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7EmployeeLeasingClientEndorsementExcl () : productmodel.WC7EmployeeLeasingClientEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7EmployeeLeasingClientEndorsementExcl
  }
  
  property get WC7EmployeeLeasingClientEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7EmployeeLeasingClientExclEndorsementExcl () : productmodel.WC7EmployeeLeasingClientExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7EmployeeLeasingClientExclEndorsementExcl
  }
  
  property get WC7EmployeeLeasingClientExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7LaborContractorEndorsementExcl () : productmodel.WC7LaborContractorEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7LaborContractorEndorsementExcl
  }
  
  property get WC7LaborContractorEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7LaborContractorExclEndorsementExcl () : productmodel.WC7LaborContractorExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7LaborContractorExclEndorsementExcl
  }
  
  property get WC7LaborContractorExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7MedicalBenefitsExclEndorsementExcl () : productmodel.WC7MedicalBenefitsExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MedicalBenefitsExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7MedicalBenefitsExclEndorsementExcl
  }
  
  property get WC7MedicalBenefitsExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MedicalBenefitsExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7NevadaEmployeeLeasingClientExclEndorsementExcl () : productmodel.WC7NevadaEmployeeLeasingClientExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingClientExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7NevadaEmployeeLeasingClientExclEndorsementExcl
  }
  
  property get WC7NevadaEmployeeLeasingClientExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingClientExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7NevadaEmployeeLeasingPEOExclEndorsementExcl () : productmodel.WC7NevadaEmployeeLeasingPEOExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingPEOExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7NevadaEmployeeLeasingPEOExclEndorsementExcl
  }
  
  property get WC7NevadaEmployeeLeasingPEOExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingPEOExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7PartnersOfficersAndOthersExclEndorsementExcl () : productmodel.WC7PartnersOfficersAndOthersExclEndorsementExcl {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PartnersOfficersAndOthersExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7PartnersOfficersAndOthersExclEndorsementExcl
  }
  
  property get WC7PartnersOfficersAndOthersExclEndorsementExclExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PartnersOfficersAndOthersExclEndorsementExcl") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelatives () : productmodel.WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelatives {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelatives") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelatives
  }
  
  property get WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelativesExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICEndorsementAgreementLimitingRestrictingThisInsRelatives") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICEndorsementAgreementLimitingThisInsuranceJV () : productmodel.WC7TDICEndorsementAgreementLimitingThisInsuranceJV {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICEndorsementAgreementLimitingThisInsuranceJV") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICEndorsementAgreementLimitingThisInsuranceJV
  }
  
  property get WC7TDICEndorsementAgreementLimitingThisInsuranceJVExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICEndorsementAgreementLimitingThisInsuranceJV") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}