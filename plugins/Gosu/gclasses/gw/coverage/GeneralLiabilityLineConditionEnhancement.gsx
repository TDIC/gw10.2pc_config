package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement GeneralLiabilityLineConditionEnhancement : entity.GeneralLiabilityLine {
  property get AggLimitsLocationProject () : productmodel.AggLimitsLocationProject {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AggLimitsLocationProject") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.AggLimitsLocationProject
  }
  
  property get AggLimitsLocationProjectExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AggLimitsLocationProject") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get AmendExtRepPerdSpecAccidSchedule () : productmodel.AmendExtRepPerdSpecAccidSchedule {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecAccidSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.AmendExtRepPerdSpecAccidSchedule
  }
  
  property get AmendExtRepPerdSpecAccidScheduleExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecAccidSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get AmendExtRepPerdSpecLocSchedule () : productmodel.AmendExtRepPerdSpecLocSchedule {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecLocSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.AmendExtRepPerdSpecLocSchedule
  }
  
  property get AmendExtRepPerdSpecLocScheduleExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecLocSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get AmendExtRepPerdSpecProdWorkSchedule () : productmodel.AmendExtRepPerdSpecProdWorkSchedule {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecProdWorkSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.AmendExtRepPerdSpecProdWorkSchedule
  }
  
  property get AmendExtRepPerdSpecProdWorkScheduleExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("AmendExtRepPerdSpecProdWorkSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get ClaimsMadeExtendedSpecificRpting () : productmodel.ClaimsMadeExtendedSpecificRpting {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ClaimsMadeExtendedSpecificRpting") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.ClaimsMadeExtendedSpecificRpting
  }
  
  property get ClaimsMadeExtendedSpecificRptingExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ClaimsMadeExtendedSpecificRpting") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get ClaimsMadeSupplementalRptng () : productmodel.ClaimsMadeSupplementalRptng {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ClaimsMadeSupplementalRptng") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.ClaimsMadeSupplementalRptng
  }
  
  property get ClaimsMadeSupplementalRptngExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ClaimsMadeSupplementalRptng") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAmendLiquorLiability () : productmodel.GLAmendLiquorLiability {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAmendLiquorLiability") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAmendLiquorLiability
  }
  
  property get GLAmendLiquorLiabilityExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAmendLiquorLiability") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLArbitrationCond () : productmodel.GLArbitrationCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLArbitrationCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLArbitrationCond
  }
  
  property get GLArbitrationCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLArbitrationCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCancellationEarlierNotice () : productmodel.GLCancellationEarlierNotice {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLCancellationEarlierNotice") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCancellationEarlierNotice
  }
  
  property get GLCancellationEarlierNoticeExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLCancellationEarlierNotice") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLContractLiabilityLimitation () : productmodel.GLContractLiabilityLimitation {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLContractLiabilityLimitation") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLContractLiabilityLimitation
  }
  
  property get GLContractLiabilityLimitationExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLContractLiabilityLimitation") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCovExtDiscount_TDIC () : productmodel.GLCovExtDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zm2ju0qc9kbv08s4kl8f08bq2ib") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCovExtDiscount_TDIC
  }
  
  property get GLCovExtDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zm2ju0qc9kbv08s4kl8f08bq2ib") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLFullTimeFacultyDiscount_TDIC () : productmodel.GLFullTimeFacultyDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zltia76bohcd7bqisoquqbn1h59") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLFullTimeFacultyDiscount_TDIC
  }
  
  property get GLFullTimeFacultyDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zltia76bohcd7bqisoquqbn1h59") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLFullTimePostGradDiscount_TDIC () : productmodel.GLFullTimePostGradDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqnh0h71rrrqs7biu7qah6u6ipa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLFullTimePostGradDiscount_TDIC
  }
  
  property get GLFullTimePostGradDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqnh0h71rrrqs7biu7qah6u6ipa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLIRPMDiscount_TDIC () : productmodel.GLIRPMDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zuoh8d4ln8jus2ndbp8o16hj7p9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLIRPMDiscount_TDIC
  }
  
  property get GLIRPMDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zuoh8d4ln8jus2ndbp8o16hj7p9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLInsuredContractDefinition () : productmodel.GLInsuredContractDefinition {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLInsuredContractDefinition") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLInsuredContractDefinition
  }
  
  property get GLInsuredContractDefinitionExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLInsuredContractDefinition") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNJNonMemberDiscount_TDIC () : productmodel.GLNJNonMemberDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zogh2t9f3j4lh32mc2tsrehc428") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNJNonMemberDiscount_TDIC
  }
  
  property get GLNJNonMemberDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zogh2t9f3j4lh32mc2tsrehc428") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNewDentistDiscount_TDIC () : productmodel.GLNewDentistDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zkuiu8qmpd3v19ihi9baoh08k08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNewDentistDiscount_TDIC
  }
  
  property get GLNewDentistDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zkuiu8qmpd3v19ihi9baoh08k08") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNewGradDiscount_TDIC () : productmodel.GLNewGradDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z64i2gamk9bpoaibarqd03o5s7b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNewGradDiscount_TDIC
  }
  
  property get GLNewGradDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z64i2gamk9bpoaibarqd03o5s7b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNewToCompanyDiscount_TDIC () : productmodel.GLNewToCompanyDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("za5jqtscu5als3tb2nrjcj9vrga") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNewToCompanyDiscount_TDIC
  }
  
  property get GLNewToCompanyDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("za5jqtscu5als3tb2nrjcj9vrga") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLPartTimeDiscount_TDIC () : productmodel.GLPartTimeDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zb5hculdaifq85ea500t08dbem9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLPartTimeDiscount_TDIC
  }
  
  property get GLPartTimeDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zb5hculdaifq85ea500t08dbem9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLRiskMgmtDiscount_TDIC () : productmodel.GLRiskMgmtDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z18hgiqocnvs3ffd23tmi0rnj5b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLRiskMgmtDiscount_TDIC
  }
  
  property get GLRiskMgmtDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z18hgiqocnvs3ffd23tmi0rnj5b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLServiceMembersCRADiscount_TDIC () : productmodel.GLServiceMembersCRADiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z4jh8rjmnl1fp0obqminbvgjrda") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLServiceMembersCRADiscount_TDIC
  }
  
  property get GLServiceMembersCRADiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z4jh8rjmnl1fp0obqminbvgjrda") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLTempDisDiscount_TDIC () : productmodel.GLTempDisDiscount_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zedgm6rfrefpq70l0madbv4cel9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLTempDisDiscount_TDIC
  }
  
  property get GLTempDisDiscount_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zedgm6rfrefpq70l0madbv4cel9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLTerritory () : productmodel.GLTerritory {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLTerritory") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLTerritory
  }
  
  property get GLTerritoryExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLTerritory") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLVendorsExcess () : productmodel.GLVendorsExcess {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLVendorsExcess") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLVendorsExcess
  }
  
  property get GLVendorsExcessExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLVendorsExcess") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLWaiveImmunity () : productmodel.GLWaiveImmunity {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLWaiveImmunity") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLWaiveImmunity
  }
  
  property get GLWaiveImmunityExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLWaiveImmunity") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLWaiveSubrogation () : productmodel.GLWaiveSubrogation {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLWaiveSubrogation") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLWaiveSubrogation
  }
  
  property get GLWaiveSubrogationExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLWaiveSubrogation") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get LimitCovPremiseProject () : productmodel.LimitCovPremiseProject {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("LimitCovPremiseProject") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.LimitCovPremiseProject
  }
  
  property get LimitCovPremiseProjectExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("LimitCovPremiseProject") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get XCUHazardsAllowed () : productmodel.XCUHazardsAllowed {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("XCUHazardsAllowed") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.XCUHazardsAllowed
  }
  
  property get XCUHazardsAllowedExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("XCUHazardsAllowed") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}