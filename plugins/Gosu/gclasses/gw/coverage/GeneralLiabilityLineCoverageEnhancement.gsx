package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement GeneralLiabilityLineCoverageEnhancement : entity.GeneralLiabilityLine {
  property get GLAddCondoCov () : productmodel.GLAddCondoCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAddCondoCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAddCondoCov
  }
  
  property get GLAddCondoCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAddCondoCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAddInjuryLeasedWorkers () : productmodel.GLAddInjuryLeasedWorkers {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAddInjuryLeasedWorkers") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAddInjuryLeasedWorkers
  }
  
  property get GLAddInjuryLeasedWorkersExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLAddInjuryLeasedWorkers") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAdditionalInsuredCov_TDIC () : productmodel.GLAdditionalInsuredCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z7djmlqvnl5ct6ud5st50bsfdu9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAdditionalInsuredCov_TDIC
  }
  
  property get GLAdditionalInsuredCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z7djmlqvnl5ct6ud5st50bsfdu9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAggLimit_A_TDIC () : productmodel.GLAggLimit_A_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zalgs4n9p2ro01tnks9nb34m1ga") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAggLimit_A_TDIC
  }
  
  property get GLAggLimit_A_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zalgs4n9p2ro01tnks9nb34m1ga") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAggLimit_B_TDIC () : productmodel.GLAggLimit_B_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zf0ham095l38t4ee713uja2vaf8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAggLimit_B_TDIC
  }
  
  property get GLAggLimit_B_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zf0ham095l38t4ee713uja2vaf8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLAggLimit_TDIC () : productmodel.GLAggLimit_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlnig7a009gkf8rla9spc0kg6d9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLAggLimit_TDIC
  }
  
  property get GLAggLimit_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlnig7a009gkf8rla9spc0kg6d9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLBLAICov_TDIC () : productmodel.GLBLAICov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8kig4gg8ki5j7eck1mc84a6qg8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLBLAICov_TDIC
  }
  
  property get GLBLAICov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z8kig4gg8ki5j7eck1mc84a6qg8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCGLCov () : productmodel.GLCGLCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLCGLCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCGLCov
  }
  
  property get GLCGLCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLCGLCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCOICov_TDIC () : productmodel.GLCOICov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zhfhik1h2o3bc8pj2hbn76hn1qb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCOICov_TDIC
  }
  
  property get GLCOICov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zhfhik1h2o3bc8pj2hbn76hn1qb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLContractualLiabRR () : productmodel.GLContractualLiabRR {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLContractualLiabRR") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLContractualLiabRR
  }
  
  property get GLContractualLiabRRExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLContractualLiabRR") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCybCov1_TDIC () : productmodel.GLCybCov1_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zpmjm6ojp3onj7v34kpvmohffd8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCybCov1_TDIC
  }
  
  property get GLCybCov1_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zpmjm6ojp3onj7v34kpvmohffd8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCybCov2_TDIC () : productmodel.GLCybCov2_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsohkk7am6eqo5e883hrjckqp4a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCybCov2_TDIC
  }
  
  property get GLCybCov2_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsohkk7am6eqo5e883hrjckqp4a") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCybCov4_TDIC () : productmodel.GLCybCov4_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zh5i8plhql7f4fvvfjhj6ufhhpa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCybCov4_TDIC
  }
  
  property get GLCybCov4_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zh5i8plhql7f4fvvfjhj6ufhhpa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCybCov5_TDIC () : productmodel.GLCybCov5_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z3nhg22h2ap0hd8ugs1gko84ov9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCybCov5_TDIC
  }
  
  property get GLCybCov5_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z3nhg22h2ap0hd8ugs1gko84ov9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCybCov6_TDIC () : productmodel.GLCybCov6_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("znqi0n33rddp3e8eicmeo6k8m18") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCybCov6_TDIC
  }
  
  property get GLCybCov6_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("znqi0n33rddp3e8eicmeo6k8m18") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCyberLiabCov_TDIC () : productmodel.GLCyberLiabCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqgje9sp99nsa5hjuel570v4kv9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCyberLiabCov_TDIC
  }
  
  property get GLCyberLiabCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqgje9sp99nsa5hjuel570v4kv9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCyberLiabDedCov_TDIC () : productmodel.GLCyberLiabDedCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ztcgatksqcpugcbselian1q1rpa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCyberLiabDedCov_TDIC
  }
  
  property get GLCyberLiabDedCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ztcgatksqcpugcbselian1q1rpa") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLCyvSupplementalERECov_TDIC () : productmodel.GLCyvSupplementalERECov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z83j0gi7gvfbs4oo52b9enprk29") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLCyvSupplementalERECov_TDIC
  }
  
  property get GLCyvSupplementalERECov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z83j0gi7gvfbs4oo52b9enprk29") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDeductible () : productmodel.GLDeductible {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLDeductible") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDeductible
  }
  
  property get GLDeductibleExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLDeductible") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDentalBusinessLiabCov_TDIC () : productmodel.GLDentalBusinessLiabCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zd8iscrh4ck6adkv01dn8j8cf6b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDentalBusinessLiabCov_TDIC
  }
  
  property get GLDentalBusinessLiabCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zd8iscrh4ck6adkv01dn8j8cf6b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDentalEmpBenLiab_TDIC () : productmodel.GLDentalEmpBenLiab_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zvrgqpupku5j47kkmugmquvbi3b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDentalEmpBenLiab_TDIC
  }
  
  property get GLDentalEmpBenLiab_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zvrgqpupku5j47kkmugmquvbi3b") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDentalEmpPracLiabCov_TDIC () : productmodel.GLDentalEmpPracLiabCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ze4go54r15oki0sttjdi1v4c0ta") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDentalEmpPracLiabCov_TDIC
  }
  
  property get GLDentalEmpPracLiabCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ze4go54r15oki0sttjdi1v4c0ta") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDentalMedWasteCov_TDIC () : productmodel.GLDentalMedWasteCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z03h23ksbo3nvdvtb1acm29rkc8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDentalMedWasteCov_TDIC
  }
  
  property get GLDentalMedWasteCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z03h23ksbo3nvdvtb1acm29rkc8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLDentistProfLiabCov_TDIC () : productmodel.GLDentistProfLiabCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1fi2ei7vg6hi2tsva2m5k017t8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLDentistProfLiabCov_TDIC
  }
  
  property get GLDentistProfLiabCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1fi2ei7vg6hi2tsva2m5k017t8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLElectronicDataLiability () : productmodel.GLElectronicDataLiability {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLElectronicDataLiability") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLElectronicDataLiability
  }
  
  property get GLElectronicDataLiabilityExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLElectronicDataLiability") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLEmpBenefitsLiabilityCov () : productmodel.GLEmpBenefitsLiabilityCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLEmpBenefitsLiabilityCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLEmpBenefitsLiabilityCov
  }
  
  property get GLEmpBenefitsLiabilityCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLEmpBenefitsLiabilityCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLExcludedServiceCond_TDIC () : productmodel.GLExcludedServiceCond_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zoji2mvtr8tj45jbvav6jadg6a9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLExcludedServiceCond_TDIC
  }
  
  property get GLExcludedServiceCond_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zoji2mvtr8tj45jbvav6jadg6a9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLExtendedReportPeriodCov_TDIC () : productmodel.GLExtendedReportPeriodCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlohk0oieji0mfa33s8eakf9cla") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLExtendedReportPeriodCov_TDIC
  }
  
  property get GLExtendedReportPeriodCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zlohk0oieji0mfa33s8eakf9cla") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLExtendedReportPeriodDPLCov_TDIC () : productmodel.GLExtendedReportPeriodDPLCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zdqhg3dg7n67d0voj3s53b8cota") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLExtendedReportPeriodDPLCov_TDIC
  }
  
  property get GLExtendedReportPeriodDPLCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zdqhg3dg7n67d0voj3s53b8cota") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLGovSubdivisions () : productmodel.GLGovSubdivisions {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLGovSubdivisions") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLGovSubdivisions
  }
  
  property get GLGovSubdivisionsExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLGovSubdivisions") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLIDTheftREcoveryCov_TDIC () : productmodel.GLIDTheftREcoveryCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zq6ishpvj1pmq1hha66rqb7kia8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLIDTheftREcoveryCov_TDIC
  }
  
  property get GLIDTheftREcoveryCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zq6ishpvj1pmq1hha66rqb7kia8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLLawnCare () : productmodel.GLLawnCare {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLawnCare") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLLawnCare
  }
  
  property get GLLawnCareExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLawnCare") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLLimitedPAandInjuryCov () : productmodel.GLLimitedPAandInjuryCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLimitedPAandInjuryCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLLimitedPAandInjuryCov
  }
  
  property get GLLimitedPAandInjuryCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLimitedPAandInjuryCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLLiquorEndorsement () : productmodel.GLLiquorEndorsement {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLiquorEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLLiquorEndorsement
  }
  
  property get GLLiquorEndorsementExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLiquorEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLLocumTenensCov_TDIC () : productmodel.GLLocumTenensCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zogh8sf2madve1f37q96qg6lsq8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLLocumTenensCov_TDIC
  }
  
  property get GLLocumTenensCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zogh8sf2madve1f37q96qg6lsq8") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLLtdFungiBacteriaCov () : productmodel.GLLtdFungiBacteriaCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLtdFungiBacteriaCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLLtdFungiBacteriaCov
  }
  
  property get GLLtdFungiBacteriaCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLLtdFungiBacteriaCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLManuscriptEndor_TDIC () : productmodel.GLManuscriptEndor_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zgcgqc34k85fg4an76i3fac2ikb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLManuscriptEndor_TDIC
  }
  
  property get GLManuscriptEndor_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zgcgqc34k85fg4an76i3fac2ikb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLMedPayCov_TDIC () : productmodel.GLMedPayCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqtga9td0gg96fpucnomhm039d9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLMedPayCov_TDIC
  }
  
  property get GLMedPayCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zqtga9td0gg96fpucnomhm039d9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLMobileDentalClinicCov_TDIC () : productmodel.GLMobileDentalClinicCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsjg20o5qmc5i1mm1pov1j8ibd9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLMobileDentalClinicCov_TDIC
  }
  
  property get GLMobileDentalClinicCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zsjg20o5qmc5i1mm1pov1j8ibd9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLMultiOwnerDPECov_TDIC () : productmodel.GLMultiOwnerDPECov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1fiu51tgq6l17cfgd2o62pcts9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLMultiOwnerDPECov_TDIC
  }
  
  property get GLMultiOwnerDPECov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z1fiu51tgq6l17cfgd2o62pcts9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNJDeductibleCov_TDIC () : productmodel.GLNJDeductibleCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zb7hom21c0rmjc4gd9ajlbb5vi9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNJDeductibleCov_TDIC
  }
  
  property get GLNJDeductibleCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zb7hom21c0rmjc4gd9ajlbb5vi9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNJWaiverCov_TDIC () : productmodel.GLNJWaiverCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zvhg2837kk04f4l13btmtg2h0l9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNJWaiverCov_TDIC
  }
  
  property get GLNJWaiverCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zvhg2837kk04f4l13btmtg2h0l9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLNameOTDCov_TDIC () : productmodel.GLNameOTDCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zdjh8gm0t4rvc93uu5nf49qe1rb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLNameOTDCov_TDIC
  }
  
  property get GLNameOTDCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zdjh8gm0t4rvc93uu5nf49qe1rb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLPestHerbicideApplicatorSchedule () : productmodel.GLPestHerbicideApplicatorSchedule {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPestHerbicideApplicatorSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLPestHerbicideApplicatorSchedule
  }
  
  property get GLPestHerbicideApplicatorScheduleExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPestHerbicideApplicatorSchedule") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLPollutionDesignatedCov () : productmodel.GLPollutionDesignatedCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPollutionDesignatedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLPollutionDesignatedCov
  }
  
  property get GLPollutionDesignatedCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPollutionDesignatedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLPollutionShortTermCov () : productmodel.GLPollutionShortTermCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPollutionShortTermCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLPollutionShortTermCov
  }
  
  property get GLPollutionShortTermCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLPollutionShortTermCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLPriorActsCov_TDIC () : productmodel.GLPriorActsCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z21h4nettbg0gavskojcktm0q58") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLPriorActsCov_TDIC
  }
  
  property get GLPriorActsCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z21h4nettbg0gavskojcktm0q58") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLRegulatoryAuthCov_TDIC () : productmodel.GLRegulatoryAuthCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zbch2givgcshjf66nmgl6uph608") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLRegulatoryAuthCov_TDIC
  }
  
  property get GLRegulatoryAuthCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zbch2givgcshjf66nmgl6uph608") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLSchoolServicesCov_TDIC () : productmodel.GLSchoolServicesCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zc5g0p0eebumqc6qf12ktjgcts9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLSchoolServicesCov_TDIC
  }
  
  property get GLSchoolServicesCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zc5g0p0eebumqc6qf12ktjgcts9") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLSpecialEventCov_TDIC () : productmodel.GLSpecialEventCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zhhi0u9fsg777a8ar49vj7m2b88") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLSpecialEventCov_TDIC
  }
  
  property get GLSpecialEventCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("zhhi0u9fsg777a8ar49vj7m2b88") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLStateExclCov_TDIC () : productmodel.GLStateExclCov_TDIC {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z4eggi3e8n9q9bjmmkqiiiirdcb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLStateExclCov_TDIC
  }
  
  property get GLStateExclCov_TDICExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("z4eggi3e8n9q9bjmmkqiiiirdcb") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get GLUndergroundResourceCov () : productmodel.GLUndergroundResourceCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLUndergroundResourceCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.GLUndergroundResourceCov
  }
  
  property get GLUndergroundResourceCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("GLUndergroundResourceCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get PollutionBroadLimited () : productmodel.PollutionBroadLimited {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("PollutionBroadLimited") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.PollutionBroadLimited
  }
  
  property get PollutionBroadLimitedExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("PollutionBroadLimited") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get ProductWithdrawalLtd () : productmodel.ProductWithdrawalLtd {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ProductWithdrawalLtd") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.ProductWithdrawalLtd
  }
  
  property get ProductWithdrawalLtdExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("ProductWithdrawalLtd") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get XCUSpecified () : productmodel.XCUSpecified {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("XCUSpecified") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.XCUSpecified
  }
  
  property get XCUSpecifiedExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("XCUSpecified") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get Y2KLimitedCov () : productmodel.Y2KLimitedCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("Y2KLimitedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.Y2KLimitedCov
  }
  
  property get Y2KLimitedCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("Y2KLimitedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}