package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement WC7JurisdictionCoverageEnhancement : entity.WC7Jurisdiction {
  property get WC7BenefitsDedCov () : productmodel.WC7BenefitsDedCov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7BenefitsDedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7BenefitsDedCov
  }
  
  property get WC7BenefitsDedCovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7BenefitsDedCov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}