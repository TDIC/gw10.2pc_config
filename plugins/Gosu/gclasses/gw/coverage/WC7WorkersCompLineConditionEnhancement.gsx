package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement WC7WorkersCompLineConditionEnhancement : entity.WC7WorkersCompLine {
  property get WC7AircraftPremiumEndorsementCond () : productmodel.WC7AircraftPremiumEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7AircraftPremiumEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7AircraftPremiumEndorsementCond
  }
  
  property get WC7AircraftPremiumEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7AircraftPremiumEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7AlternateEmployerEndorsementACond () : productmodel.WC7AlternateEmployerEndorsementACond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7AlternateEmployerEndorsementACond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7AlternateEmployerEndorsementACond
  }
  
  property get WC7AlternateEmployerEndorsementACondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7AlternateEmployerEndorsementACond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7CatastropheOtherThanCertifiedActsOfTerrorisCond () : productmodel.WC7CatastropheOtherThanCertifiedActsOfTerrorisCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CatastropheOtherThanCertifiedActsOfTerrorisCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7CatastropheOtherThanCertifiedActsOfTerrorisCond
  }
  
  property get WC7CatastropheOtherThanCertifiedActsOfTerrorisCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CatastropheOtherThanCertifiedActsOfTerrorisCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7ContractingClassificationPremiumAdjustmentECon2 () : productmodel.WC7ContractingClassificationPremiumAdjustmentECon2 {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7ContractingClassificationPremiumAdjustmentECon2") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7ContractingClassificationPremiumAdjustmentECon2
  }
  
  property get WC7ContractingClassificationPremiumAdjustmentECon2Exists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7ContractingClassificationPremiumAdjustmentECon2") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7EmployeeLeasingClientEndorsementCond () : productmodel.WC7EmployeeLeasingClientEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7EmployeeLeasingClientEndorsementCond
  }
  
  property get WC7EmployeeLeasingClientEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7EmployeeLeasingClientEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7ExperienceRatingModFactEndorsementCond () : productmodel.WC7ExperienceRatingModFactEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7ExperienceRatingModFactEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7ExperienceRatingModFactEndorsementCond
  }
  
  property get WC7ExperienceRatingModFactEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7ExperienceRatingModFactEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7InsuranceCompanyAsInsuredEndorsementCond () : productmodel.WC7InsuranceCompanyAsInsuredEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7InsuranceCompanyAsInsuredEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7InsuranceCompanyAsInsuredEndorsementCond
  }
  
  property get WC7InsuranceCompanyAsInsuredEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7InsuranceCompanyAsInsuredEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7JointVentureAsInsuredEndorsementCond () : productmodel.WC7JointVentureAsInsuredEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7JointVentureAsInsuredEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7JointVentureAsInsuredEndorsementCond
  }
  
  property get WC7JointVentureAsInsuredEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7JointVentureAsInsuredEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7LaborContractorEndorsementACond () : productmodel.WC7LaborContractorEndorsementACond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorEndorsementACond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7LaborContractorEndorsementACond
  }
  
  property get WC7LaborContractorEndorsementACondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LaborContractorEndorsementACond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7LongshoreAndHarborWorkersCompensationActRatCond () : productmodel.WC7LongshoreAndHarborWorkersCompensationActRatCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LongshoreAndHarborWorkersCompensationActRatCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7LongshoreAndHarborWorkersCompensationActRatCond
  }
  
  property get WC7LongshoreAndHarborWorkersCompensationActRatCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7LongshoreAndHarborWorkersCompensationActRatCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7MedicalBenefitsReimbursementEndorsementCond () : productmodel.WC7MedicalBenefitsReimbursementEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MedicalBenefitsReimbursementEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7MedicalBenefitsReimbursementEndorsementCond
  }
  
  property get WC7MedicalBenefitsReimbursementEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MedicalBenefitsReimbursementEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7MultipleCoordinatedPolicyEndorsementCond () : productmodel.WC7MultipleCoordinatedPolicyEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MultipleCoordinatedPolicyEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7MultipleCoordinatedPolicyEndorsementCond
  }
  
  property get WC7MultipleCoordinatedPolicyEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MultipleCoordinatedPolicyEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7NevadaEmployeeLeasingMasterPolicyCond () : productmodel.WC7NevadaEmployeeLeasingMasterPolicyCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingMasterPolicyCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7NevadaEmployeeLeasingMasterPolicyCond
  }
  
  property get WC7NevadaEmployeeLeasingMasterPolicyCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaEmployeeLeasingMasterPolicyCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7NevadaMultipleCoordinatedPolicyEndorsementCond () : productmodel.WC7NevadaMultipleCoordinatedPolicyEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaMultipleCoordinatedPolicyEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7NevadaMultipleCoordinatedPolicyEndorsementCond
  }
  
  property get WC7NevadaMultipleCoordinatedPolicyEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NevadaMultipleCoordinatedPolicyEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7NotificationOfChangeInOwnershipEndorsementCond () : productmodel.WC7NotificationOfChangeInOwnershipEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NotificationOfChangeInOwnershipEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7NotificationOfChangeInOwnershipEndorsementCond
  }
  
  property get WC7NotificationOfChangeInOwnershipEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7NotificationOfChangeInOwnershipEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7PendingRateChangeEndorsementCond () : productmodel.WC7PendingRateChangeEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PendingRateChangeEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7PendingRateChangeEndorsementCond
  }
  
  property get WC7PendingRateChangeEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PendingRateChangeEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7PolicyPeriodEndorsementCond () : productmodel.WC7PolicyPeriodEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PolicyPeriodEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7PolicyPeriodEndorsementCond
  }
  
  property get WC7PolicyPeriodEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PolicyPeriodEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7RateChangeEndorsementCond () : productmodel.WC7RateChangeEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7RateChangeEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7RateChangeEndorsementCond
  }
  
  property get WC7RateChangeEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7RateChangeEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7RuralElectrificationAdministrationEndorsemeCond () : productmodel.WC7RuralElectrificationAdministrationEndorsemeCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7RuralElectrificationAdministrationEndorsemeCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7RuralElectrificationAdministrationEndorsemeCond
  }
  
  property get WC7RuralElectrificationAdministrationEndorsemeCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7RuralElectrificationAdministrationEndorsemeCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7SoleProprietorsPartnersOfficersAndOthersCovCond () : productmodel.WC7SoleProprietorsPartnersOfficersAndOthersCovCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7SoleProprietorsPartnersOfficersAndOthersCovCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7SoleProprietorsPartnersOfficersAndOthersCovCond
  }
  
  property get WC7SoleProprietorsPartnersOfficersAndOthersCovCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7SoleProprietorsPartnersOfficersAndOthersCovCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICAnniversaryRatingDateEndorsement () : productmodel.WC7TDICAnniversaryRatingDateEndorsement {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICAnniversaryRatingDateEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICAnniversaryRatingDateEndorsement
  }
  
  property get WC7TDICAnniversaryRatingDateEndorsementExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICAnniversaryRatingDateEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICCaliforniaCodeOfRegulations10 () : productmodel.WC7TDICCaliforniaCodeOfRegulations10 {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCaliforniaCodeOfRegulations10") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICCaliforniaCodeOfRegulations10
  }
  
  property get WC7TDICCaliforniaCodeOfRegulations10Exists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCaliforniaCodeOfRegulations10") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICCaliforniaCodeOfRegulations11 () : productmodel.WC7TDICCaliforniaCodeOfRegulations11 {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCaliforniaCodeOfRegulations11") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICCaliforniaCodeOfRegulations11
  }
  
  property get WC7TDICCaliforniaCodeOfRegulations11Exists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCaliforniaCodeOfRegulations11") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICNoticeRequiredByLaw () : productmodel.WC7TDICNoticeRequiredByLaw {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICNoticeRequiredByLaw") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICNoticeRequiredByLaw
  }
  
  property get WC7TDICNoticeRequiredByLawExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICNoticeRequiredByLaw") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICSoleProprietorCoverageEndorsement () : productmodel.WC7TDICSoleProprietorCoverageEndorsement {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICSoleProprietorCoverageEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICSoleProprietorCoverageEndorsement
  }
  
  property get WC7TDICSoleProprietorCoverageEndorsementExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICSoleProprietorCoverageEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7VoluntaryCompensationAndEmployersLiabilityCovCond () : productmodel.WC7VoluntaryCompensationAndEmployersLiabilityCovCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7VoluntaryCompensationAndEmployersLiabilityCovCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7VoluntaryCompensationAndEmployersLiabilityCovCond
  }
  
  property get WC7VoluntaryCompensationAndEmployersLiabilityCovCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7VoluntaryCompensationAndEmployersLiabilityCovCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7VolunteerWorkerEndorsementCond () : productmodel.WC7VolunteerWorkerEndorsementCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7VolunteerWorkerEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7VolunteerWorkerEndorsementCond
  }
  
  property get WC7VolunteerWorkerEndorsementCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7VolunteerWorkerEndorsementCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond () : productmodel.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond
  }
  
  property get WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}