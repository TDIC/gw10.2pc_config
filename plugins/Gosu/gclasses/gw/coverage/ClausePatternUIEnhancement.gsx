package gw.coverage

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.productmodel.ClausePattern

enhancement ClausePatternUIEnhancement : ClausePattern {

  function allowToggle(coverable : Coverable) : boolean {
    if (not this.isRequiredCov(coverable)) {
      return true
    } else {
      //required coverage
      if (coverable.getCoverageConditionOrExclusion(this) == null) {
        //if doesn't exist, allow toggle so user can add it if need be
        return true
      } else {
        //if exists, but unavailable, allow toggle so user can remove
        if (not coverable.isCoverageConditionOrExclusionAvailable(this)) {
          return true
        }
      }
      return false
    }
  }

  /**
   * create by: SureshB
   *
   * @param coverable and toggle boolean value
   * @description: this method returns the boolean vlaue of ERE DEPL coverage availability based on Coverage D
   * @create time: 9:26 PM 8/21/2019
   * @return: boolean value to decide the availability of ERE DEPL coverage
   */
  function getEREDEPLAvailability_TDIC(coverable : Coverable, exist : boolean) {
    if (this.CodeIdentifier == "GLDentalEmpPracLiabCov_TDIC") {
      coverable.setCoverageExists(coverable.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodDPLCov_TDIC"), exist)
    }
  }
  /*
   * create by: SureshB
   * @description: method to decide the editable property for GLNewToCompanyDiscount_TDIC
   * @create time: 9:24 PM 9/25/2019
    * @param Coverable
   * @return: boolean
   */

  function isGLNewToCompanyEditable_TDIC(coverable : Coverable) : boolean {
    if (coverable.PolicyLine?.JobType == typekey.Job.TC_RENEWAL) {
      return false
    }
    if (coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE) {
      if ((coverable.PolicyLine as GLLine)?.BasedOn?.GLNewToCompanyDiscount_TDICExists) {
        var effectiveDate = coverable.PolicyLine?.Branch?.EditEffectiveDate
        var expirationDate = (coverable.PolicyLine as GLLine)?.GLNewToCompanyDiscount_TDIC?.GLNTCExpDate_TDICTerm?.Value
        return expirationDate?.differenceInDays(effectiveDate) == 0
      }
      return true
    }
    return true
  }

  /*
 * create by: SureshB
 * @description: method to set the GLNTCExpDate_TDIC cov term value
 * @create time: 9:24 PM 9/25/2019
  * @param Coverable
 * @return:
 */
  function setGLNTCExpDate_TDIC(coverable : Coverable) {
    if (!(coverable.PolicyLine as GLLine)?.BasedOn?.GLNewToCompanyDiscount_TDICExists) {
      (coverable.PolicyLine as GLLine)?.GLNewToCompanyDiscount_TDIC?.GLNTCExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.EditEffectiveDate.addYears(3))
    }
  }

  /*
 * create by: SureshB
 * @description: method to decide the mode of the PCF
 * @create time: 9:24 PM 9/25/2019
  * @param CovTerm
 * @return: String
 */
  function getMode_TDIC(term : gw.api.domain.covterm.CovTerm) : String {
    if (term.Pattern.CodeIdentifier == "GLFTPGGSchoolCode_TDIC" || term.Pattern.CodeIdentifier == "GLFTFSchoolCode_TDIC") {
      return "GLSchoolCode_TDIC"
    } else {
      return term.ValueTypeName
    }
  }

  /*
 * create by: SureshB
 * @description: method to decide the editable property for GLIRPMDDiscount_TDIC
 * @create time: 9:24 PM 9/25/2019
  * @param Coverable
 * @return: boolean
 */
  function isGLIRPMDEditable_TDIC(coverable : Coverable) : boolean {
    if (coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE ){
      return false
    }
    return true
  }
  /*
   * create by: SureshB
   * @description: method to get the additional insureds of type Other
   * @create time: 9:56 PM 10/10/2019
    * @param productmodel.GLLine
   * @return: PolicyAddlInsuredDetail[]
   */

  function getAdditionalInsuredOfTypeOther_TDIC(line : productmodel.GLLine) : PolicyAddlInsuredDetail[] {
    return line.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.where(\addlInsuredDetail ->
        addlInsuredDetail.AdditionalInsuredType == AdditionalInsuredType.TC_OTHER)
  }
/*
 * create by: SureshB
 * @description: method to set the effective and expiration dates for coverages
  * "GLExtendedReportPeriodCov_TDIC", "GLExtendedReportPeriodDPLCov_TDIC", "GLCyvSupplementalERECov_TDIC"
 * @create time: 2:45 PM 11/9/2019
  * @param Coverable
 * @return:
 */

  function setEREEREDEPLCYBERECovEffExpDates_TDIC(coverable : Coverable) {
    var additionalEREEREDEPLCYBERECoverages = {"GLExtendedReportPeriodCov_TDIC", "GLExtendedReportPeriodDPLCov_TDIC", "GLCyvSupplementalERECov_TDIC"}
    if (additionalEREEREDEPLCYBERECoverages.contains(this.CodeIdentifier)) {
      if((this.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC") and ((coverable.PolicyLine as GLLine).CoverableState == Jurisdiction.TC_AK)
          and (coverable.PolicyLine?.Branch?.Canceled or coverable.PolicyLine?.Branch?.Status == PolicyPeriodStatus.TC_NONRENEWED)){
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart)
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart.addYears(5))
      }
      else
      if (coverable.PolicyLine?.Branch?.BasedOn?.Canceled) {
        if ((coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodCov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodCov_TDIC?.GLERPEEffectiveDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodStart)
        }
        if ((coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDIC?.GLCybEREEffectiveDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodStart)
          (coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDIC?.GLCybEREExpirationDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodStart.addYears(1))
        }
        if ((coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodStart)
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodStart.addYears(1))
        }
      } else if (coverable.PolicyLine?.Branch?.BasedOn?.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code) {
        if ((coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodCov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodCov_TDIC?.GLERPEEffectiveDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodEnd)
        }
        if ((coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDIC?.GLCybEREEffectiveDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodEnd)
          (coverable.PolicyLine as GLLine)?.GLCyvSupplementalERECov_TDIC?.GLCybEREExpirationDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodEnd.addYears(1))
        }
        if ((coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDICExists) {
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodEnd)
          (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.BasedOn?.PeriodEnd.addYears(1))
        }
      } else if (this.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC" and coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE) {
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart)
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart.addYears(1))
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to set the term values for coverage GLExtendedReportPeriodDPLCov_TDIC on selecting the coverage GLDentalEmpPracLiabCov_TDIC
   * @create time: 2:10 PM 11/11/2019
    * @param Coverable
   * @return:
   */

  function setGLExtendedReportPeriodDPLCov_TDIC(coverable : Coverable) {
    if (this.CodeIdentifier == "GLDentalEmpPracLiabCov_TDIC" and (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDICExists) {
      if((coverable.CoverableState == Jurisdiction.TC_AK) and ((coverable.PolicyLine as GLLine).Branch.Canceled or ((coverable.PolicyLine as GLLine).Branch.Status
          == PolicyPeriodStatus.TC_NONRENEWED))){
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart)
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart.addYears(5))
      }else {
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart)
        (coverable.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(coverable.PolicyLine?.Branch?.PeriodStart.addYears(1))
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to create "nj_newtocompany_discount" activity
   * @create time: 5:58 PM 11/15/2019
    * @param Coverable
   * @return:
   */

  function createNewToCompanyActivity_TDIC(coverable : Coverable) {
    if (coverable.PolicyLine.JobType == typekey.Job.TC_SUBMISSION and
        !coverable.PolicyLine?.Branch?.Job?.AllOpenActivities.hasMatch(\act -> act.ActivityPattern.Code == "nj_newtocompany_discount")) {
      var actPattern = ActivityPattern.finder.getActivityPatternByCode("nj_newtocompany_discount")
      var act = coverable.PolicyLine?.Branch?.Job?.createGroupActivity_TDIC(actPattern, actPattern.Subject, actPattern.Description)
      var newToCompanyDiscExpDate = (coverable.PolicyLine as GLLine).GLNewToCompanyDiscount_TDIC?.GLNTCExpDate_TDICTerm?.Value
      var policyExpDate = coverable.PolicyLine.AssociatedPolicyPeriod.PeriodEnd
      if (newToCompanyDiscExpDate > policyExpDate and newToCompanyDiscExpDate.DayOfMonth == policyExpDate.DayOfMonth
          and newToCompanyDiscExpDate.MonthOfYear == policyExpDate.MonthOfYear) {
        // Renewal GROUP = tdic-sb:203
        //Renewal QUEUE = tdic-sb:102
        var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:203").select().AtMostOneRow
        var queue = group.AssignableQueues.firstWhere(\q -> q.PublicID == "tdic-sb:102")
        act.assignActivityToQueue(queue, group)
      } else {
        // POLICY CHANGE GROUP = tdic-sb:206
        // POLICY CHANGE QUEUE = tdic-sb:105
        var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:206").select().AtMostOneRow
        var queue = group.AssignableQueues.firstWhere(\q -> q.PublicID == "tdic-sb:105")
        act.assignActivityToQueue(queue, group)
      }
    }
  }
}
