package gw.coverage

@javax.annotation.Generated("", "", "com.guidewire.pc.productmodel.codegen.ProductModelCodegen")
enhancement WC7WorkersCompLineCoverageEnhancement : entity.WC7WorkersCompLine {
  property get WC7CaliforniaCancelationEndorsement () : productmodel.WC7CaliforniaCancelationEndorsement {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CaliforniaCancelationEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7CaliforniaCancelationEndorsement
  }
  
  property get WC7CaliforniaCancelationEndorsementExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CaliforniaCancelationEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7CaliforniaShortRateCancelationEndorsement () : productmodel.WC7CaliforniaShortRateCancelationEndorsement {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CaliforniaShortRateCancelationEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7CaliforniaShortRateCancelationEndorsement
  }
  
  property get WC7CaliforniaShortRateCancelationEndorsementExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7CaliforniaShortRateCancelationEndorsement") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7FederalEmployersLiabilityActACov () : productmodel.WC7FederalEmployersLiabilityActACov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7FederalEmployersLiabilityActACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7FederalEmployersLiabilityActACov
  }
  
  property get WC7FederalEmployersLiabilityActACovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7FederalEmployersLiabilityActACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7MaritimeACov () : productmodel.WC7MaritimeACov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MaritimeACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7MaritimeACov
  }
  
  property get WC7MaritimeACovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7MaritimeACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7OtherStatesInsurance () : productmodel.WC7OtherStatesInsurance {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7OtherStatesInsurance") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7OtherStatesInsurance
  }
  
  property get WC7OtherStatesInsuranceExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7OtherStatesInsurance") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7PolicyAmendatoryEndorsementCA () : productmodel.WC7PolicyAmendatoryEndorsementCA {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PolicyAmendatoryEndorsementCA") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7PolicyAmendatoryEndorsementCA
  }
  
  property get WC7PolicyAmendatoryEndorsementCAExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7PolicyAmendatoryEndorsementCA") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TDICCatastrophe () : productmodel.WC7TDICCatastrophe {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCatastrophe") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TDICCatastrophe
  }
  
  property get WC7TDICCatastropheExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TDICCatastrophe") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TerrorismPremiumEndorsementCA () : productmodel.WC7TerrorismPremiumEndorsementCA {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TerrorismPremiumEndorsementCA") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TerrorismPremiumEndorsementCA
  }
  
  property get WC7TerrorismPremiumEndorsementCAExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TerrorismPremiumEndorsementCA") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7TerrsmRiskInsProgReauthActDisclsrCov1 () : productmodel.WC7TerrsmRiskInsProgReauthActDisclsrCov1 {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TerrsmRiskInsProgReauthActDisclsrCov1") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7TerrsmRiskInsProgReauthActDisclsrCov1
  }
  
  property get WC7TerrsmRiskInsProgReauthActDisclsrCov1Exists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7TerrsmRiskInsProgReauthActDisclsrCov1") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  property get WC7WorkersCompEmpLiabInsurancePolicyACov () : productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7WorkersCompEmpLiabInsurancePolicyACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).getClause(this) as productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov
  }
  
  property get WC7WorkersCompEmpLiabInsurancePolicyACovExists () : boolean {
    return (gw.api.productmodel.ClausePatternLookup.getByPublicID("WC7WorkersCompEmpLiabInsurancePolicyACov") as com.guidewire.pc.api.productmodel.ClausePatternInternal).hasClause(this)
  }
  
  
}