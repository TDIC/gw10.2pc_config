package gw.rating.rtm.util

uses gw.api.database.Query
uses gw.api.productmodel.Offering
uses gw.api.productmodel.PolicyLinePattern
uses gw.api.productmodel.PolicyLinePatternLookup
uses gw.api.productmodel.Product
uses gw.api.system.PCDependenciesGateway

uses java.util.ArrayList
uses java.util.Collection
uses gw.api.productmodel.ProductLookup
uses gw.rating.GenericRateBookFieldSearch
uses gw.util.Pair
uses gw.api.locale.DisplayKey

class ProductModelUtils {

  static var availablePatterns = {"WC7Line", "GLLine", "BOPLine"}

  static function getAllPolicyLines() : List<String> {
    return getAllPolicyLinePatterns().map(\ p -> p.PublicID)
  }

  static function getAllPolicyLinesForSearch() : List<String> {
    return getAllPolicyLines()
              .concat({GenericRateBookFieldSearch.GENERIC_POLICY_LINE_CODE})
              .toList()
  }

  static function getAllPolicyLinePatterns() : Collection<gw.api.productmodel.PolicyLinePattern> {
    return PolicyLinePatternLookup.All.where(\elt -> availablePatterns.contains(elt.CodeIdentifier))
  }

  static function getPolicyLinesWithTables() : List<String> {
    var lines = new ArrayList<String>()
    var lobPatterns = getAllPolicyLinePatterns()
    for (lobPattern in lobPatterns) {
      var lobTables = Query.make(RateTableDefinition)
            .compare(RateTableDefinition#PolicyLine, Equals, lobPattern.PublicID).select()
      if (lobTables.HasElements) {
        lines.add(lobPattern.PublicID)
      }
    }
    return lines
  }

  static function getAllUWCompanies() : List<UWCompany> {
    return PCDependenciesGateway.getUWCompanyFinder().findUWCompanies().toList()
  }

  static function getAllUWCompaniesForSearch() : List<Pair<String, UWCompany>> {
    return getAllUWCompanies()
      .map(\ uwCompany -> new Pair<String, UWCompany>(uwCompany.DisplayName, uwCompany))
      .concat({new Pair<String, UWCompany>(DisplayKey.get("Web.Rating.Filter.Generic"), null)})
      .toList()
  }

  static function getAllJurisdictionsForSearch() : List<Pair<String, Jurisdiction>> {
    return Jurisdiction
      .getTypeKeys(false)
      .map(\ jurisdiction -> new Pair<String, Jurisdiction>(jurisdiction.DisplayName, jurisdiction))
      .concat({new Pair<String, Jurisdiction>(DisplayKey.get("Web.Rating.Filter.Generic"), null)})
      .toList()
  }
  
  static function getOfferingDisplayName(code : String) : String {
    if (code == null) return null
    var o = PCDependenciesGateway.getProductModel().getAllInstances(Offering)
            .firstWhere(\o -> o.PublicID.equals(code))
    return o.Product.Name + " - " + o.Name
  }
  
  static function getOfferingsFor(line : String) : List<String> {
    var products = productsFor(line)
    return buildOfferingList(products)
  }

  static function getAllSearchOfferingsFor(line : String) : List<String> {
    var offerings = line == null
      ? buildOfferingList(allProducts())
      : getOfferingsFor(line)
    return {GenericRateBookFieldSearch.GENERIC_OFFERING_CODE}
      .concat(offerings)
      .toList()
  }

  static function getAllDisplayOfferingsFor(line : String) : List<String> {
    var offerings = getOfferingsFor(line)
    return {GenericRateBookFieldSearch.GENERIC_OFFERING_CODE}
      .concat(offerings)
      .toList()
  }

  // Jeff, GPC-350, Account and Policy search
  public static function getSpecificProducts_TDIC(linePatternCodes: ArrayList<String>) : List<Product> {
    var allProds = allProducts()
    var specificProds = new ArrayList<Product>()
      for (var lpc in linePatternCodes) {
      var prod = allProds.firstWhere(\ prod -> prod.LinePatterns.contains(PolicyLinePatternLookup.getByCodeIdentifier(lpc)))
      if (prod !== null) {
        specificProds.add(prod)
      }
    }
    return specificProds
  }

  private static function allProducts() : List<Product> {
    return PCDependenciesGateway.getProductModel().getAllInstances(Product).toList()
  }
   
  private static function productsFor(line : String) : List<Product> {
    return allProducts().where(\ prod -> prod.LinePatterns.contains(PolicyLinePatternLookup.getByPublicID(line)))
  }
  
  private static function buildOfferingList(products : List<Product>) : List<String> {
    var offerings = new ArrayList<String>()
    products.each(\ p -> {
      // need to create a copy because p.Offerings is of type LocableList that can't be sorted
      var copy = p.Offerings.copy()
      copy.sortBy(\ o -> o.Priority).each(\ o -> offerings.add(o.PublicID))
    })    
    return offerings
  }
  
  /**
   * If more than one product has the same displayname, display with the abbreviation appended.
   * 
   * Use case:  if the ISO GL7 line is applied, General Liability shows up twice, once for base GL and once for GL7. 
   * Since the rating screen is outside the context of a policy, there's no good way to call the availability logic and discover that the base GL is unavailable.
   * Therefore, the products in a pulldown will be displayed like this:
   * 
   * ...
   * Commercial Property
   * General Liability (GL)
   * General Liability (GL7)
   * Inland Marine
   * ...
   */
  static function lineStyleProductDisplay(p : Product) : String {
    var productsKeyedByDisplayname = ProductLookup.getAll().partition(\ prod -> prod.DisplayName)
    var isDuplicated = productsKeyedByDisplayname.get(p.DisplayName).Count > 1
    return isDuplicated ? p + " (" + p.Abbreviation + ")" : p.DisplayName
  }

  static function getPolicyLinePattern(linePatternCode : String) : PolicyLinePattern {
    return linePatternCode == GenericRateBookFieldSearch.GENERIC_POLICY_LINE_CODE ? null : PolicyLinePatternLookup.All.singleWhere(\p -> p.PublicID == linePatternCode)
  }
}
