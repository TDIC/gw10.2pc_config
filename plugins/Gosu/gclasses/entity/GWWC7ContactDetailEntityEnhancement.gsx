package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7ContactDetail.eti;WC7ContactDetail.eix;WC7ContactDetail.etx")
enhancement GWWC7ContactDetailEntityEnhancement : entity.WC7ContactDetail {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}