package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7Cost.eti;WC7Cost.eix;WC7Cost.etx")
enhancement GWWC7CostEntityEnhancement : entity.WC7Cost {
  property get AuditableRatingEffDated () : gw.lob.wc7.WC7RatingEffDatedExposure {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).AuditableRatingEffDated
  }
  
  property get ClassCode () : java.lang.String {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).ClassCode
  }
  
  property get Description () : java.lang.String {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).Description
  }
  
  property get JurisdictionState () : typekey.Jurisdiction {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).JurisdictionState
  }
  
  property get LocationNum () : java.lang.Integer {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).LocationNum
  }
  
  property get RatesOverridable () : boolean {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.financials.WC7CostMethods") as gw.lob.wc7.financials.WC7CostMethods).RatesOverridable
  }
  
  
}