package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7WorkersCompCov.eti;WC7WorkersCompCov.eix;WC7WorkersCompCov.etx")
enhancement GWWC7WorkersCompCovEntityEnhancement : entity.WC7WorkersCompCov {
  function getEffDateRangeForRatingPeriod (ratingPeriod :  gw.lob.wc7.rating.WC7RatingPeriod) : com.guidewire.pl.system.util.DateRange {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).getEffDateRangeForRatingPeriod(ratingPeriod)
  }
  
  function getNumDaysEffectiveForDateRange (dateRange :  com.guidewire.pl.system.util.DateRange) : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).getNumDaysEffectiveForDateRange(dateRange)
  }
  
  property get EffectiveDateForRating () : java.util.Date {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).EffectiveDateForRating
  }
  
  property get ExpirationDateForRating () : java.util.Date {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).ExpirationDateForRating
  }
  
  property get NumDaysEffective () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).NumDaysEffective
  }
  
  property get NumDaysEffectiveForRating () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).NumDaysEffectiveForRating
  }
  
  
}