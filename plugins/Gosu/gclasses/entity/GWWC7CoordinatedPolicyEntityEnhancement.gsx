package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7CoordinatedPolicy.eti;WC7CoordinatedPolicy.eix;WC7CoordinatedPolicy.etx")
enhancement GWWC7CoordinatedPolicyEntityEnhancement : entity.WC7CoordinatedPolicy {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}