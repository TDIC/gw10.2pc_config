package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7CoveredEmployeeBase.eti;WC7CoveredEmployeeBase.eix;WC7CoveredEmployeeBase.etx")
enhancement GWWC7CoveredEmployeeBaseEntityEnhancement : entity.WC7CoveredEmployeeBase {
  function getEffDateRangeForRatingPeriod (ratingPeriod :  gw.lob.wc7.rating.WC7RatingPeriod) : com.guidewire.pl.system.util.DateRange {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).getEffDateRangeForRatingPeriod(ratingPeriod)
  }
  
  function getNumDaysEffectiveForDateRange (dateRange :  com.guidewire.pl.system.util.DateRange) : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).getNumDaysEffectiveForDateRange(dateRange)
  }
  
  property get BasedOnBasisForRating () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDatedExposure") as gw.lob.wc7.WC7RatingEffDatedExposure).BasedOnBasisForRating
  }
  
  property get BasisForRating () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDatedExposure") as gw.lob.wc7.WC7RatingEffDatedExposure).BasisForRating
  }
  
  property get EffectiveDateForRating () : java.util.Date {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).EffectiveDateForRating
  }
  
  property get ExpirationDateForRating () : java.util.Date {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).ExpirationDateForRating
  }
  
  property get NumDaysEffective () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).NumDaysEffective
  }
  
  property get NumDaysEffectiveForRating () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDated") as gw.lob.wc7.WC7RatingEffDated).NumDaysEffectiveForRating
  }
  
  property get UnproratedBasisForRating () : int {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.WC7RatingEffDatedExposure") as gw.lob.wc7.WC7RatingEffDatedExposure).UnproratedBasisForRating
  }
  
  
}