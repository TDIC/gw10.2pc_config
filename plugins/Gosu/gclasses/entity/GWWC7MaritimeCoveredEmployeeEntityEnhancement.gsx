package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7MaritimeCoveredEmployee.eti;WC7MaritimeCoveredEmployee.eix;WC7MaritimeCoveredEmployee.etx")
enhancement GWWC7MaritimeCoveredEmployeeEntityEnhancement : entity.WC7MaritimeCoveredEmployee {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}