package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7ExcludedOwnerOfficer.eti;WC7ExcludedOwnerOfficer.eix;WC7ExcludedOwnerOfficer.etx")
enhancement GWWC7ExcludedOwnerOfficerEntityEnhancement : entity.WC7ExcludedOwnerOfficer {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}