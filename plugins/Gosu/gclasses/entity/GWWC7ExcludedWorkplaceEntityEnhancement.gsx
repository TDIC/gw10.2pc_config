package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7ExcludedWorkplace.eti;WC7ExcludedWorkplace.eix;WC7ExcludedWorkplace.etx")
enhancement GWWC7ExcludedWorkplaceEntityEnhancement : entity.WC7ExcludedWorkplace {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}