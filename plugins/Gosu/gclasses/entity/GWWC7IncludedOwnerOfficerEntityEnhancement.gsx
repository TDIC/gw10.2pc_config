package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7IncludedOwnerOfficer.eti;WC7IncludedOwnerOfficer.eix;WC7IncludedOwnerOfficer.etx")
enhancement GWWC7IncludedOwnerOfficerEntityEnhancement : entity.WC7IncludedOwnerOfficer {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}