package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "WC7FedCoveredEmployee.eti;WC7FedCoveredEmployee.eix;WC7FedCoveredEmployee.etx")
enhancement GWWC7FedCoveredEmployeeEntityEnhancement : entity.WC7FedCoveredEmployee {
  property get ParentClause () : entity.Clause {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.lob.wc7.schedule.WC7SpecificScheduledItem") as gw.lob.wc7.schedule.WC7SpecificScheduledItem).ParentClause
  }
  
  
}