package tdic.web.admin.shared

uses entity.Contact
uses entity.PolicyContactRole
uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.api.util.LocationUtil
uses gw.api.util.StateJurisdictionMappingUtil
uses tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria
uses tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult
uses tdic.pc.integ.plugins.membership.TDIC_MembershipCheck
uses typekey.Job

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 9/22/14
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
class SharedUIHelper extends gw.web.admin.shared.SharedUIHelper {

  public static function getAvailableSubtypes(availableSubtypes : typekey.Contact[]
      , contactTypes : typekey.ContactType[]
      , product : gw.api.productmodel.Product
      , accountContactRole : typekey.AccountContactRole) : typekey.Contact[] {
    if (availableSubtypes != null) {
      return availableSubtypes
    }
    var subTypes = contactTypes.map(\c -> c == TC_COMPANY ?
        typekey.Contact.TC_COMPANY : typekey.Contact.TC_PERSON)
        .sortBy(\c -> c.DisplayName)  // make the order deterministic
    if (product != null and accountContactRole == TC_NAMEDINSURED) {
      subTypes = subTypes.where(\c -> product.isContactTypeSuitableForProductAccountType(c == TC_COMPANY ? Company : Person))
    }
    return subTypes
  }

  public static function createSearchCriteria(initialSearchCriteria : entity.ContactSearchCriteria
      , availableSubtypes : typekey.Contact[]) : ContactSearchCriteria {
    if (initialSearchCriteria != null) {
      return initialSearchCriteria
    }
    var c = new ContactSearchCriteria()
    c.ContactSubtype = availableSubtypes.first()
    c.FirstNameExact = false;
    c.KeywordExact = false;
    c.PermissiveSearch = false;
    //20150401 TJ Talluto US304 - default Country to US.  Widget will have to be un-hidden to change this in the future
    c.Address.Country = typekey.Country.TC_US
    return c
  }

  public static function doSearch(criteria : ContactSearchCriteria) : gw.plugin.contact.impl.ContactResultWrapper {
    var result = criteria.overridePerformSearch()
    if (result.contactResults == null or result.contactResults.IsEmpty) {
      gw.api.util.LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Java.Search.NoResults"))
    }
    return result
  }

  /**
   * DE222, robk
   * Checks the membership status for the specified Contact.
   * <p>
   * If the membership integration throws an exception and the specified boolean is true then an error message will be displayed on the UI.
   * If the membership integration throws an exception and the specified boolean is false then the membership status is set to "Inactive"
   */
  @Returns("true if the membership check was performed successfully; false otherwise")
  public static function performMembershipCheck_TDIC(aContact : Contact, displayMembershipCheckError : boolean, baseState : Jurisdiction) : boolean {
    if (aContact typeis Person) {
      try {
        aContact.CDAMembershipStatus_TDIC = new TDIC_MembershipCheck().checkMembership(aContact.ADANumber_TDICOfficialID, StateJurisdictionMappingUtil.getStateMappingForJurisdiction(baseState))
        return true
      } catch (e : java.lang.Exception) {
        if (displayMembershipCheckError) {
          LocationUtil.addRequestScopedErrorMessage(e.Message)
        } else {
          aContact.CDAMembershipStatus_TDIC = typekey.GlobalStatus_TDIC.TC_INACTIVE
        }
      }
    }
    return false
  }
  /*
   * create by: SureshB
   * @description: method to validate and display an error message if any changes are made to the contacts at the ACCOUNT LEVEL contact screen which exist in already bound policices of that account
   * @create time: 6:44 PM 10/9/2019
    * @param impactedPolicies:java.util.List<PolicyPeriod>
   * @return: void
   */

  public static function validateAccountContactInBoundPolicy_TDIC(impactedPolicies : java.util.List<PolicyPeriod>) {
    if (impactedPolicies.Count > 0) {
      LocationUtil.addRequestScopedErrorMessage(DisplayKey.get("TDIC.ContactInIssuedPolicyError"))
    }
  }

  /**
   * US1377
   * ShaneS 02/20/2015
   */
  public static function doReinsuranceSearch(criteria : TDIC_ReinsuranceSearchCriteria) : TDIC_ReinsuranceSearchResult[] {
    var results = criteria.overridePerformReinsuranceSearch()
    // Order for UI display
    var resultsOrdered = results.sortBy(\aResult -> aResult.RelatedAddressLine1)
    return resultsOrdered
  }
/*
 * create by: SureshB
 * @description: method to determine the visibility of contact fields at policy level for Additional Insureds, Mortgagee, Loss Paypees in PL/CP (Person/Company)
 * - Primary Phone
 - Home Phone
 - Work Phone
 - Mobile Phone
 - Fax Phone
 - Primary Email
 - FEIN
 * @create time: 2:32 PM 10/14/2019
  * @param PolicyContactRole
 * @return:
 */

  public static function getVisibilityOfPolicContactFields_TDIC(policyContactRole : PolicyContactRole) : boolean {
    var roles = {typekey.PolicyContactRole.TC_POLICYADDLINSURED, typekey.PolicyContactRole.TC_POLICYMORTGAGEE_TDIC,
        typekey.PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC, typekey.PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC}
    if ((policyContactRole.Branch?.GLLineExists or policyContactRole.Branch?.BOPLineExists) and roles.contains(policyContactRole.Subtype)) {
      return false
    } else {
      return true
    }
  }

  /*
 * create by: IndramaniP
 * @description: method to validate and display an error message if any changes are made to the contacts at the ACCOUNT LEVEL contact screen which exist in already bound policices of that account
 * @create time: 6:44 PM 12/15/2019
  * @param accountContact:AccountContact
 * @return: void
 */
  public static function checkForChangedFields_TDIC(contact : Contact) : boolean {
    var changedFields = contact.ChangedFields
    // CDAMembershipStatus_TDIC field will be changed in all account contact editing. So we have increased the changed fields count by 1.
    if (changedFields.Count == 2) {
      return !(contact.isFieldChanged("DoNotSoliciteDirectMail_TDIC")
          or contact.isFieldChanged("DoNotSoliciteEMail_TDIC"))
    } else if (changedFields.Count == 3) {
      return !(contact.isFieldChanged("DoNotSoliciteDirectMail_TDIC")
          and contact.isFieldChanged("DoNotSoliciteEMail_TDIC"))
    } else {
      return true
    }
  }

  /**
   * flag is to remove warning and error from ssn for exsiting NW DIMS policies
   */
  public static function isSSNRequiredDIMS(policyContactRole : PolicyContactRole) : Boolean {
    if(policyContactRole.Branch.isDIMSPolicy_TDIC || {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}.contains(policyContactRole.Branch.BaseState.Code) ){
      if(policyContactRole.Branch.Job.Subtype== Job.TC_SUBMISSION || policyContactRole.Branch.Job.Subtype== Job.TC_RENEWAL || policyContactRole.Branch.Job.Subtype== Job.TC_POLICYCHANGE )
        return false
    }
    return true
    /*else if()
    var flag = !(policyContactRole.Branch.isDIMSPolicy_TDIC
        and policyContactRole.Branch.Job.Subtype.getCode() != typekey.Job.TC_SUBMISSION.getCode()
        and {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}.contains(policyContactRole.Branch.BaseState.Code))
    return flag*/
  }

  /**
   *
   */
  public static function isSSNRequired(policyContactRole : PolicyContactRole) : Boolean {
    var branch = policyContactRole.Branch
    if (not branch.isQuickQuote_TDIC) {
      if (branch.GLLineExists and policyContactRole.AccountContactRole.AccountContact.ContactType == ContactType.TC_PERSON) {
        return (branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or branch.Offering.CodeIdentifier == "PLOccurence_TDIC")
      }
    }
    return false
  }

  public static function canSSNVisible(period: PolicyPeriod) : Boolean {
    if(period.GLLineExists){
      return (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or period.Offering.CodeIdentifier == "PLOccurence_TDIC")
    }
    return false
  }

  public static function isFEINRequired(policyContactRole : PolicyContactRole) : Boolean {
    var branch = policyContactRole.Branch

    if (not branch.isQuickQuote_TDIC) {
      if(branch.WC7LineExists) {
        return true
      }
    }
    return false
  }
}