package tdic.web.pcf.helper

uses java.text.DecimalFormat
uses gw.api.productmodel.Question
uses gw.api.locale.DisplayKey

class GLCPUWQuestionAnswersCurrencyHelper {

  // Jeff Lin, 11/22/2019, 5:37 PM, GPC-2520: The amounts in PL/CP UW question answers should not contain decimal point.
  // called for output conversion property to remove decimal point and tailing zero for those dollar input of PL/CP UW questions
  public static function getUWQuestionCurrencyAnswerNoDecimal(question: Question, val : Integer) : String {
    var formatPattern = "000,000,000,000,000"
    var currencyFormatUWQuestions_PLCP = {"BOPBuildingReplacementCost","LRPBuildingReplacementCost","ImageEquipmentCost","DentalOperatories", "DentalSupportEqupmentCost",
        "SmallDentalEquipmentCost","TEchnologyCost","DentalSuppliesCost","TenantImprovementCost","OfficeEquipmentCost","ReceptionRoomCost", "TotalBPPLimits",
        "ElectronicDataProcessingCost"}
    if (val == null)
    {
      val = Integer.valueOf(0)
    }
    if (val != null and currencyFormatUWQuestions_PLCP.contains(question.CodeIdentifier)) {
      try {
        var decimalFormat = new DecimalFormat(formatPattern)
        var decimalStr = decimalFormat.format(val)?.toString()
        var valStr = removeLeadingZeroes(decimalStr)
        return valStr
      } catch (e : NumberFormatException){
        throw new com.guidewire.pl.web.controller.UserDisplayableException(DisplayKey.get("Java.Validation.Number.NotAnInteger",
            DisplayKey.get("Web.Profiler.DbmsInstrumentationCaptureThresholdMs")), e)
      }
    }
    return val?.toString()
  }

  private static function removeLeadingZeroes(valStr: String): String {
    while (valStr.length() > 1 and (valStr.indexOf("0") == 0 or valStr.indexOf(",") == 0) ) {
      valStr = valStr.substring(1)
    }
    return valStr
  }
}