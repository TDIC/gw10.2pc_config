package tdic.web.pcf.helper

uses gw.api.database.Query
uses gw.api.database.Relop

class GLExposureUnitsHelper {

  public static function getStateComponents(stateCode : String) : GLTerritory_TDIC[] {
    if (stateCode != null) {
      var query = Query.make(GLTerritory_TDIC)
      query.compare("StateCode", Relop.Equals, stateCode)
      var componentTerritoryPairs = query.select()
      if (componentTerritoryPairs != null) {
        return componentTerritoryPairs.toTypedArray().sortBy(\p->p.Component)
      }
    }
    return null
  }

  public static function setTerritoryCodeByComponent(stateCode : String, componentCode : String, exposure : GLExposure) {
    if (stateCode != null and componentCode != null) {
      var query = Query.make(GLTerritory_TDIC)
      query.compare("StateCode", Relop.Equals, stateCode)
      query.compare("Component", Relop.Equals, componentCode)
      var componentTerritoryPair = query.select().FirstResult
      if (componentTerritoryPair != null) {
        exposure.GLTerritory_TDIC = componentTerritoryPair
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to filter the ClssCode_TDIC typecodes based on the state.
   * @create time: 2:57 PM 10/7/2019
    * @param PolicyPeriod and ClassCode_TDIC[]
   * @return: List<ClassCode_TDIC>
   */

  public static function filterClassCodes_TDIC(policyPeriod : PolicyPeriod, exposure : GLExposure) : List<ClassCode_TDIC> {
    var classCodes = typekey.ClassCode_TDIC.getTypeKeys(false)
    var fileteredClassCodes = new ArrayList<ClassCode_TDIC>()
    for (classCode in classCodes) {
      fileteredClassCodes.add(classCode)
      if (!classCode.Categories.whereTypeIs(Jurisdiction)?.IsEmpty) {
        if (!classCode.hasCategory(policyPeriod.BaseState)) {
          fileteredClassCodes.remove(classCode)
        }
      }
      if (!classCode.Categories.whereTypeIs(SpecialityCode_TDIC)?.IsEmpty and exposure.SpecialityCode_TDIC != null) {
        if (!classCode.hasCategory(exposure.SpecialityCode_TDIC)) {
          fileteredClassCodes.remove(classCode)
        }
      }
      if (classCode == ClassCode_TDIC.TC_02 and {typekey.Job.TC_SUBMISSION, typekey.Job.TC_REWRITE}.contains(policyPeriod.Job.Subtype)) {
        fileteredClassCodes.remove(classCode)
      }
      if((exposure.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_60ORALPATHOLOGY_TDIC)
          && ((policyPeriod.BaseState == Jurisdiction.TC_WA) ||(policyPeriod.BaseState == Jurisdiction.TC_ID) || (policyPeriod.BaseState == Jurisdiction.TC_MT) || (policyPeriod.BaseState == Jurisdiction.TC_TN) || (policyPeriod.BaseState == Jurisdiction.TC_OR))
          && classCode == ClassCode_TDIC.TC_10) {
        fileteredClassCodes.remove(classCode)
      }

      if (classCode == ClassCode_TDIC.TC_50 and policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC" and
          (exposure.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC || exposure.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_10ORALSURGERY_TDIC)) {
        fileteredClassCodes.remove(classCode)
      }

      if (classCode == ClassCode_TDIC.TC_01 and policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC" and
          (exposure.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_10ORALSURGERY_TDIC)) {
        fileteredClassCodes.remove(classCode)
      }

      if (classCode == ClassCode_TDIC.TC_10 and policyPeriod.BaseState == Jurisdiction.TC_PA and exposure.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_60ORALPATHOLOGY_TDIC) {
        fileteredClassCodes.remove(classCode)
      }
      if (classCode == ClassCode_TDIC.TC_02 and policyPeriod.BaseState == Jurisdiction.TC_PA and (policyPeriod.Offering.CodeIdentifier.containsIgnoreCase("PLOccurence_TDIC") || policyPeriod.Offering.CodeIdentifier.containsIgnoreCase("PLClaimsMade_TDIC"))) {
        fileteredClassCodes.add(classCode)
      }
    }
    if (fileteredClassCodes.Count == 1) {
      exposure.ClassCode_TDIC = fileteredClassCodes.first()
    }
    return fileteredClassCodes
  }
}