package tdic.web.pcf.helper
uses gw.api.productmodel.ClausePattern

class GLCoverageConditionsHelper {

  private static function isTransactionNonEditable(coverable: Coverable): boolean {
    return  ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and   coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
              coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or       coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
              coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE)
  }

  private static function  isTransactionEditable(coverable: Coverable): boolean {
    return ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
             coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE)
  }

  public static function isInputSetDefaultDividerVisible(coveragePattern: ClausePattern, coverable: Coverable, openForEdit: boolean) : boolean {
    switch(coveragePattern.CodeIdentifier){
      case "GLNewDentistDiscount_TDIC":
          if (isTransactionNonEditable(coverable)) {
            return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLNewDentistDiscount_TDICExists
          }
          if (isTransactionEditable(coverable)) {
            return true
          }
        break
      case "GLNewGradDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLNewGradDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLFullTimeFacultyDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLFullTimeFacultyDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLFullTimePostGradDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLFullTimePostGradDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLPartTimeDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLPartTimeDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLServiceMembersCRADiscount_TDIC":
        if (isTransactionEditable(coverable) || coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL) {
          return true
        }
        break
      case "GLTempDisDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLTempDisDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLRiskMgmtDiscount_TDIC":
        return true //editable all the time BR-048 and BR-053
        /*if (isTransactionNonEditable(coverable) or isTransactionEditable(coverable)) {
          return false
        }
        break*/
      case "GLCovExtDiscount_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLCovExtDiscount_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLDentalEmpPracLiabCov_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLDentalEmpPracLiabCov_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLIDTheftREcoveryCov_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLIDTheftREcoveryCov_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      case "GLManuscriptEndor_TDIC":
        if (isTransactionNonEditable(coverable)) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLManuscriptEndor_TDICExists
        }
        if (isTransactionEditable(coverable)) {
          return true
        }
        break
      default:
        break
    }
    // as before for other coverages in .default PCF file
    return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
  }

}