package tdic.web.pcf.helper

uses gw.api.locale.DisplayKey
uses gw.api.ui.BOPCostWrapper_TDIC
uses gw.pl.currency.MonetaryAmount

/**
 * Utility class for BOP Quote screen
 */
class BOPQuoteScreenHelper {

  private static var covPatternsFilter = {"BOPBuildingCov", "BOPPersonalPropCov", "BOPDentalGenLiabilityCov_TDIC"}

  private static var propertyCovTermsFilter = {"BOPTotalIncLimit_TDIC", "BOPTotalInLimitSigns_TDIC", "BOPExtraExpTotalIncLimit_TDIC",
      "BOPValTotIncLimit_TDIC", "BOPACcTotalIncLimit_TDIC", "BOPMoneySecTotIncLimit_TDIC", "BOPEDTotInclLimit_TDIC",
      "BOPGoldTotalIncLimit_TDIC", "BOPFineArtsTotIncLimit_TDIC"}

  private static var propertyCovsForQuoteGrid = {"BOPMoneySecCov_TDIC", "BOPExtraExpenseCov_TDIC", "BOPEmpDisCov_TDIC", "BOPAccReceivablesCov_TDIC",
      "BOPValuablePapersCov_TDIC", "BOPSigns_TDIC", "BOPGoldPreMetals_TDIC", "BOPFineArtsCov_TDIC", "BOPFungiCov_TDIC"}

  private static var liabilityCovsForQuoteGrid = {"BOPDentalGenLiabilityCov_TDIC", "BOPBuildingOwnersLiabCov_TDIC", "BOPDEBLCov_TDIC", "BOPDMWLDCov_TDIC","BOPWASTOPGAP_TDIC"}

  private static var eqTermFilter = {"BOPEQDeductible_TDIC", "BOPEQSLDeductible_TDIC"}

  private static var covNameMap = {"BOPExtraExpenseCov_TDIC" -> DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.BOP.ExtraExpense")}

  private static var covTermNameMap = {
      "BOPBldgValuation" -> DisplayKey.get("entity.BOPBuilding.Building"),
      "BOPBPPValuation" -> DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.BOP.BusinessPersonalProperty")}

  // Order in "double" primitive, can be incremented by decimals.
  private static var costOrderMap = {
      //Property
      "BOPBuildingCov" -> 1.0,                           //Building Premium
      "BOPPersonalPropCov" -> 2.0,                       //Business Personal Property Premium
      "BOPMoneySecCov_TDIC" -> 3.0,                      //Money and Securities Premium
      "BOPLossofIncomeTDIC" -> 4.0,                      //Loss of Income Premium
      "BOPExtraExpenseCov_TDIC" -> 5.0,                  //Extra Expense Premium
      "BOPEmpDisCov_TDIC" -> 6.0,                        //Employee Dishonesty Premium
      "BOPAccReceivablesCov_TDIC" -> 7.0,                //Accounts Receivable Premium
      "BOPValuablePapersCov_TDIC" -> 8.0,                //Valuable Papers and Records Premium
      "BOPSigns_TDIC" -> 9.0,                            //Signs Premium
      "BOPGoldPreMetals_TDIC" -> 10.0,                   //Gold and Other Precious Metals Premium
      "BOPFineArtsCov_TDIC" -> 11.0,                     //Fine Arts Premium
      "BOPFungiCov_TDIC" -> 12.0,                        //Fungi Premium
      "BOPEqBldgCov" -> 13.0,                            //Earthquake Premium
      "BOPEqSpBldgCov" -> 14.0,                          //Earthquake Sprinkler Leakage Premium
      //"BOPEquipBreakCov_TDIC" -> 15.0,                  //Equipment Breakdown
      "EquipmentBreakBuildingLim" -> 15.0,               //Equipment Breakdown - Building
      "EquipmentBreakBPPLim" -> 16.0,                    //Equipment Breakdown - BPP
      "BOPIRPMProperty_TDIC" -> 17.0,                    //IRPM - Property
      "BOPClosedEndCredit_TDIC" -> 18.0,                 //Closed End Water Credit
      "MultiLinePropertyDiscount" -> 19.0,               //Multi Line Discount
      "BOPEncCovEndtCond_TDIC" -> 20.0,                  //Enhanced Coverage Endorsement


      //Liability
      "BOPDentalGenLiabilityCov_TDIC"-> 21.0,            //Dental General Liability Premium
      "BOPBuildingOwnersLiabCov_TDIC" -> 22.0,           //Building Owners Liability Premium
      "BOPDEBLCov_TDIC" -> 23.0,                         //Dental Employee Benefits Liability Premium
      "BOPDMWLDCov_TDIC" -> 24.0,                        //Dental Medical Waste Legal Defense Premium
      "BOPWASTOPGAP_TDIC" -> 25.0,                        //Washington Stop Gap Employers Liability Coverage
      "BOPIRPMLiab_TDIC" -> 26.0,                        //IRPM - Liability
      "MultiLineLiabilityDiscount" -> 27.0,              //Multi Line Discount

      //Taxes & Surcharges
      "BOPILMineSubCov_TDIC"-> 28.0,                     //Mine Subsidence Premium
      "FireFighterReliefSurcharge" -> 29.0,              //Fire Fighter Relief Surcharge (S01) Premium
      "IGASurcharge" -> 30.0,                            //IGA Surcharge Premium
      "FireSafetySurcharge" -> 31.0                      //Fire Safety Surcharge (S02) Premium
  }

  /**
   *
   * filter certain coverage terms from quote screen
   */
  public static function hideBuildingCovTermsToDisplay(cov : entity.Coverage) : gw.api.domain.covterm.CovTerm[] {
    switch(typeof cov) {
      case BOPBuildingCov :
      case BOPPersonalPropCov:
        return cov.CovTerms.where(\elt -> elt.Pattern.DisplayName != "Inflation Guard")

      case BOPEqBldgCov:
        return cov.CovTerms.where(\elt -> elt.Pattern.DisplayName == "EQ Deductible")

      case BOPEqSpBldgCov:
        return cov.CovTerms.where(\elt -> elt.Pattern.DisplayName == "EQSL Deductible")

      default:
        return cov.CovTerms
    }
  }

  /**
   * Customized coverage name
   */
  public static function getCoverageDisplayName(cov : entity.Coverage) : String {
    if(cov != null) {
      var covDisplayName = covNameMap.get(cov.Pattern.CodeIdentifier)
      covDisplayName = covDisplayName == null ? cov.Pattern.DisplayName : covDisplayName
      return covDisplayName.concat(" ").concat(DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.BOP.Premium"))
    }
    return null
  }

  /**
   * customixed coverage term name
   */
  public static function getCoverageTermDisplayName(cov : entity.Coverage, covTerm : gw.api.domain.covterm.CovTerm) : String {
    var displayName = covTermNameMap.get(covTerm.Pattern.CodeIdentifier)
    displayName = displayName != null ? displayName.concat(" ").concat(covTerm.DisplayName) : covTerm.DisplayName

    if (covPatternsFilter.contains(cov.Pattern.CodeIdentifier)) {
      return displayName
    }

    var covDisplayName = covNameMap.get(cov.Pattern.CodeIdentifier)
    covDisplayName = covDisplayName == null ? cov.Pattern.DisplayName : covDisplayName
    if (propertyCovTermsFilter.contains(covTerm.Pattern.CodeIdentifier)) {
      return covDisplayName.concat(" : ").concat(DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.BOP.TotalLimit"))
    } else if(eqTermFilter.contains(covTerm.Pattern.CodeIdentifier)) {
      return covDisplayName.concat(" : ").concat(DisplayKey.get("Web.Rating.RateTable.Column.BOPPDeductible.Label"))
    }
    return covDisplayName.concat(" : ").concat(displayName)
  }

  /**
   *
   * @param costs
   * @return
   */
  public static function wrapPropertyCosts(costs : Set<BOPCost>) : BOPCostWrapper_TDIC[] {

    if(costs.HasElements) {
      var costWrappers : List<BOPCostWrapper_TDIC> = {}
      var firstCost = costs.first()
      var covPatterns = costs.where(\elt -> elt.Coverage != null and propertyCovsForQuoteGrid.contains(elt.Coverage.Pattern.CodeIdentifier)).map(\elt -> elt.Coverage.Pattern.CodeIdentifier)

      for(covCode in propertyCovsForQuoteGrid) {
        if(covPatterns != null
            and not covPatterns.contains(covCode)
            and getCoverage(firstCost, covCode) != null) {
          costWrappers.add(new BOPCostWrapper_TDIC(costOrderMap.get(covCode), null, covCode, null, true))
        }
      }
      return costWrappers.union(wrapCosts(costs).toSet()).toTypedArray()
    }
    return {}
  }

  /**
   *
   * @param costs
   * @return
   */
  public static function wrapLiabilityCosts(costs : Set<BOPCost>) : BOPCostWrapper_TDIC[] {

    if(costs.HasElements) {
      var costWrappers : List<BOPCostWrapper_TDIC> = {}
      var firstCost = costs.first()
      var covPatterns = costs.where(\elt -> elt.Coverage != null and liabilityCovsForQuoteGrid.contains(elt.Coverage.Pattern.CodeIdentifier)).map(\elt -> elt.Coverage.Pattern.CodeIdentifier)

      for(covCode in liabilityCovsForQuoteGrid) {
        if(covPatterns != null
            and not covPatterns.contains(covCode)
            and getCoverage(firstCost, covCode) != null) {
          costWrappers.add(new BOPCostWrapper_TDIC(costOrderMap.get(covCode), null, covCode, null, true))
        }
      }
      return costWrappers.union(wrapCosts(costs).toSet()).toTypedArray()
    }
    return {}
  }

  /**
   *
   * @param costs
   * @return
   */
  public static function wrapCosts(costs : Set<BOPCost>) : BOPCostWrapper_TDIC[] {
    var costWrappers : List<BOPCostWrapper_TDIC> = {}
    for(c in costs) {
      var wrapper = new BOPCostWrapper_TDIC(getCostOrder(c), c, getCoverageDisplayName(c.Coverage), getPremium(c, costs), true)
      if(not isEquipmentBreakdown(c)){
        costWrappers.add(wrapper)
      }
    }
    return costWrappers.toTypedArray()
  }

  /**
   * retrieve coverage
   */
  public static function getCoverage(coverable : Coverable, patternCode : String) : Coverage {
    if(coverable != null and patternCode != null) {
      var pattern = coverable.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier(patternCode)
      if(pattern != null) {
        return coverable.getCoverage(pattern)
      }
    }
    return null
  }

  public static function canDisaplyTerm(cov : entity.Coverage, covTerm : gw.api.domain.covterm.CovTerm) : Boolean {
    if(propertyCovsForQuoteGrid.contains(cov.Pattern.CodeIdentifier) ) {
      var incrTerm = cov.CovTerms.firstWhere(\elt -> propertyCovTermsFilter.contains(elt.Pattern.CodeIdentifier))
      if(incrTerm != null
          and incrTerm.DisplayValue != null
          and incrTerm.DisplayValue != ""
          and covTerm.Pattern.CodeIdentifier != incrTerm.Pattern.CodeIdentifier) {
        return false
      } else if(covTerm.DisplayValue == null or covTerm.DisplayValue == "") {
        return false
      }
    }

    return true
  }

  public static function canDisplayPremium(cov : entity.Coverage) : Boolean {
    if(propertyCovsForQuoteGrid.contains(cov.Pattern.CodeIdentifier) ) {
      var incrTerm = cov.CovTerms.firstWhere(\elt -> propertyCovTermsFilter.contains(elt.Pattern.CodeIdentifier))
      if(incrTerm == null or incrTerm.DisplayValue == null or incrTerm.DisplayValue == "") {
        return false
      }
    }

    return true
  }

  public static function getActualTermAmount(cost : BOPCost, costs : Set<BOPCost>) : MonetaryAmount {
    if(cost.Coverage != null) {
      var eqCost : BOPCost
      if(cost.Coverage.Pattern.CodeIdentifier == "BOPBuildingCov") {
        eqCost = costs.firstWhere(\elt -> elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_EQUIPMENTBREAKBUILDINGLIM)
      } else if(cost.Coverage.Pattern.CodeIdentifier == "BOPPersonalPropCov") {
        eqCost = costs.firstWhere(\elt -> elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_EQUIPMENTBREAKBPPLIM)
      }
      if(eqCost != null) {
        return cost.ActualTermAmountBilling + eqCost.ActualTermAmountBilling
      }
    }
    return cost.ActualTermAmountBilling
  }

  /******************************************************* Private functions **********************************************************************************/
  private static function getPremium(cost : BOPCost, costs : Set<BOPCost>) : MonetaryAmount {
    if(cost.Coverage != null) {
      var eqCost : BOPCost
      if(cost.Coverage.Pattern.CodeIdentifier == "BOPBuildingCov") {
        eqCost = costs.firstWhere(\elt -> elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_EQUIPMENTBREAKBUILDINGLIM)
      } else if(cost.Coverage.Pattern.CodeIdentifier == "BOPPersonalPropCov") {
        eqCost = costs.firstWhere(\elt -> elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_EQUIPMENTBREAKBPPLIM)
      }
      if(eqCost != null) {
        return cost.ActualAmountBilling + eqCost.ActualAmountBilling
      }
    }
    return cost.ActualAmountBilling
  }

  private static function getCostOrder(cost : BOPCost) : double {
    if(cost != null) {
      var key : String = null
      if(cost.Coverage != null) {
        key = cost.Coverage.Pattern.CodeIdentifier
      } else if(cost typeis BOPModifierCost_TDIC) {
        key = cost.Modifier_TDIC.Pattern.CodeIdentifier
      } else if(cost typeis BOPBuildingCost_TDIC) {
        key = cost.BOPCostType_TDIC.Code
      }
      if(key != null) {
        return costOrderMap.get(key)
      }
    }
    return 0
  }

  private static function getCoverage(cost: BOPCost, codeIdentifier : String) : Coverage {
    var coverable = cost.Coverable
    var pattern = coverable.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier(codeIdentifier)
    if(pattern != null) {
      return coverable.getCoverage(pattern)
    }
    return null
  }

  private static function isEquipmentBreakdown(cost : BOPCost) : Boolean {
    if(cost != null) {
      return( cost typeis BOPBuildingCost_TDIC
          and (cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBUILDINGLIM or cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBPPLIM))
    }
    return false
  }

  static function getCostOrderFoQuoteScreen(covCode : Coverage) : double {
    print(" rterterterterfdgdf - " + covCode?.PatternCode + " - ertterte - " + covCode)
      if(covCode != null and costOrderMap.get(covCode)!= null) {
        return costOrderMap.get(covCode)
      }
    return 0
  }

}