package tdic.web.pcf.helper

uses entity.GLCost
uses gw.api.locale.DisplayKey
uses gw.api.ui.GL_CostWrapper
uses gw.api.web.job.JobWizardHelper
uses gw.pl.currency.MonetaryAmount

/**
 * BrittoS 03/28/2020
 * Utility class for GL Quote screen
 */
class GLQuoteScreenHelper {

  // Order in "double" primitive, can be incremented by decimals.
  private static var standardPremiumOrderMap = {
      "GLAddnlInsdSchedCost_TDIC" -> 1.0,
      "RiskManagement_TDIC" -> 2.0,
      "ERE_RiskManagement_TDIC" -> 2.0,    //ERE Risk Management
      "MultiLineDiscount_TDIC" -> 3.0,
      "IRPMDiscount_TDIC" -> 4.0,
      "ERE_IRPMDiscount_TDIC" -> 4.0,      //ERE IRPM Discount
      "NewToCompany_TDIC" -> 5.0,
      "NJDeductible_TDIC" -> 6.0,
      "ERE_NJDeductible_TDIC" -> 6.0,      //ERE NJ Deductible
      "NJWaiverOfConsent_TDIC" -> 7.0,
      "ERE_NJWaiverOfConsent_TDIC" -> 7.0, //ERE NJ Waiver Of Consent
      "NJNonMemberSurcharge_TDIC" -> 8.0,
      "NewDentist_TDIC" -> 9.0,
      "ERE_NewDentist_TDIC" -> 9.0,        //ERE New Dentist
      "ERE_PLClaimsMade_TDIC" -> 10.0,
      "SpecialEvent_TDIC" -> 11.0,
      "SchoolServices_TDIC" -> 12.0,
      "IdentityTheftRec_TDIC" -> 13.0,
      "EPLI_TDIC" -> 14.0,
      "ServiceMemberDisc_TDIC" -> 15.0,
      "GLMinPremCost_TDIC" -> 16.0,

      "TotalEREPremium" -> 20.0         //make it at the end, for ERE total
  }

  private static var ereCostTypeMap : Map<Object, List<String>> = {
      GLCovExposureCost -> {GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.Code},
      GLHistoricalCost_TDIC -> {GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.Code},
      GLStateCost -> {GLStateCostType.TC_ERE_NJDEDUCTIBLE_TDIC.Code,
          GLStateCostType.TC_ERE_NJWAIVEROFCONSENT_TDIC.Code,
          GLStateCostType.TC_ERE_NJIGASURCHARGE_TDIC.Code,
          GLStateCostType.TC_ERE_RISKMANAGEMENT_TDIC.Code,
          GLStateCostType.TC_ERE_IRPMDISCOUNT_TDIC.Code,
          GLStateCostType.TC_ERE_PLCLAIMSMADE_TDIC.Code
          /*GLStateCostType.TC_ERE_NEWDENTIST_TDIC.Code*/
      },
      GLAddnlInsdSchedCost_TDIC -> {GLAddnlInsuredCostType_TDIC.TC_ERE_ADDITIONALINSURED.Code}
  }

  /**
   * @param cost
   * @return
   */
  public static function getLiabilityLimit(cost : GLCost) : String {
    var period = cost.Branch
    var coverage = cost.Coverage

    if ((cost typeis GLCovExposureCost or cost typeis GLHistoricalCost_TDIC)
        and coverage != null
        and coverage typeis GLDentistProfLiabCov_TDIC) {
      if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        if (cost typeis GLCovExposureCost and cost.GLCostType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY) {
          var matchingLine = period.Policy.Periods*.GLLine.firstWhere(\elt -> elt.GLDentistProfLiabCov_TDIC.SliceDate == cost.ERESliceDate_TDIC)
          if(matchingLine != null) {
            return matchingLine.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.DisplayValue
          }
        }
        return coverage.GLDPLPerClaimLimit_TDICTerm.DisplayValue
      } else if (period.Offering.CodeIdentifier == "PLOccurence_TDIC") {
        return coverage.GLDPLPerOccLimit_TDICTerm.DisplayValue
      }
    }
    return null
  }


  public static function getERETermExpirationDate(cost : GLCost) : Date {
    var period = cost.Branch
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (cost typeis GLCovExposureCost and cost.GLCostType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY) {
        var expDate = period.AllEffectiveDates.firstWhere(\d -> d > cost.ERESliceDate_TDIC)
        if(expDate != null) {
          if(period.EditEffectiveDate == expDate) {
            return period.isCanceled() ? period.CancellationDate : period.PeriodEnd
          }
          return expDate
        }
      }
    }
    return cost.ExpirationDate
  }

  /**
   * @param cost
   * @return
   */
  public static function getSpecialtyCode(cost : GLCost) : String {
    if (cost typeis GLHistoricalCost_TDIC) {
      var code = cost.GLSplitParameters.SpecialityCode
      if (code != null) {
        return SpecialityCode_TDIC.get(code).DisplayName
      }
    }
    return null
  }

  /**
   * @param glCosts
   * @return
   */
  public static function getNonERETermPremiums(glCosts : Set<GLCost>) : Set<GLCost> {
    return glCosts.getTermPremiums_TDIC()
        .where(\elt -> not((elt typeis GLCovExposureCost and ereCostTypeMap.get(typeof(elt)).contains(elt.GLCostType.Code))
            or (elt typeis GLHistoricalCost_TDIC and ereCostTypeMap.get(typeof(elt)).contains(elt.CostType.Code))))
        .toSet()
  }

  /**
   * @param glCosts
   * @return
   */
  public static function getERETermPremiums(glCosts : Set<GLCost>) : Set<GLCost> {
    return glCosts.getTermPremiums_TDIC()
        .where(\elt -> (elt typeis GLCovExposureCost and ereCostTypeMap.get(typeof(elt)).contains(elt.GLCostType.Code))
            or (elt typeis GLHistoricalCost_TDIC and ereCostTypeMap.get(typeof(elt)).contains(elt.CostType.Code)))
        .toSet()
  }

  public static function getEREStandardPremiums(glCosts : Set<GLCost>) : Set<GLCost> {
    return glCosts.getStandardPremiums_TDIC().where(\elt ->
        (elt typeis GLStateCost and ereCostTypeMap.get(typeof(elt)).contains(elt.StateCostType.Code))
            or (elt typeis GLAddnlInsdSchedCost_TDIC and ereCostTypeMap.get(typeof(elt)).contains(elt.GLAddnlInsuredCostType_TDIC.Code)))
        .toSet()
  }

  /**
   *
   */
  public static function getNonERETaxesAndSurcharges(glCosts : Set<GLCost>) : Set<GLCost> {
    return glCosts.getTaxSurchargePremiums_TDIC()
        .where(\elt -> elt typeis GLStateCost and not ereCostTypeMap.get(typeof(elt)).contains(elt.StateCostType.Code))
        .toSet()
  }

  /**
   *
   */
  public static function getERETaxesAndSurcharges(glCosts : Set<GLCost>) : Set<GLCost> {
    return glCosts.getTaxSurchargePremiums_TDIC()
        .where(\elt -> elt typeis GLStateCost and ereCostTypeMap.get(typeof(elt)).contains(elt.StateCostType.Code))
        .toSet()
  }

  public static function getERETaxesAndSurchargeWrappers(glCosts : Set<GLCost>) : GL_CostWrapper[] {
    var costWrappers : List<GL_CostWrapper> = {}
    for (cost in getERETaxesAndSurcharges(glCosts)) {
      costWrappers.add(new GL_CostWrapper(0, cost, getStandardPremiumDisplayName(cost), cost.ActualAmountBilling, true))
    }
    return costWrappers.toTypedArray()
  }

  /**
   * @param period
   * @return
   */
  public static function getNonEREStandardPremiumWrappers(period : PolicyPeriod) : GL_CostWrapper[] {
    var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
    var costWrappers : List<GL_CostWrapper> = {}
    var erePremiums : Set<GLCost> = {}

    //Liability Pemium Total
    if (period.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      var termsTotal = getNonERETermPremiums(glCosts).toSet().AmountSum(period.PreferredSettlementCurrency)
      costWrappers.add(new GL_CostWrapper(0, null, DisplayKey.get("TDIC.Web.Policy.GL.Header.TotalforAllTerms"), termsTotal, false))
    }
    //Standard Premiums
    var standardPremiums = glCosts.getStandardPremiums_TDIC().toSet()

    //Special Handling for ERE
    if (period.isERERating_TDIC) {
      erePremiums = getEREStandardPremiums(standardPremiums)
      if (erePremiums.HasElements) {
        standardPremiums = standardPremiums.where(\elt -> not erePremiums.contains(elt)).toSet()
      }

      erePremiums = getERETermPremiums(glCosts).union(erePremiums).union(getERETaxesAndSurcharges(glCosts))
      if (erePremiums.HasElements) {
        costWrappers.add(new GL_CostWrapper(getPremiumOrder("TotalEREPremium"), null, DisplayKey.get("TDIC.Web.Policy.GL.Header.TotalEREPremium"), erePremiums.toList().AmountSum(period.PreferredSettlementCurrency), true))
      }
    }

    if (standardPremiums.HasElements) {
      var addnlInsdCosts = standardPremiums.where(\elt -> elt typeis GLAddnlInsdSchedCost_TDIC).toSet()
      if (addnlInsdCosts.HasElements) {
        var firstCost = addnlInsdCosts.first()
        costWrappers.add(new GL_CostWrapper(getPremiumOrder(firstCost), null, getStandardPremiumDisplayName(firstCost), addnlInsdCosts.AmountSum(period.PreferredSettlementCurrency), true))
      }
      for (cost in standardPremiums.where(\elt -> not addnlInsdCosts.contains(elt))) {
        costWrappers.add(new GL_CostWrapper(getPremiumOrder(cost), cost, getStandardPremiumDisplayName(cost), cost.ActualAmountBilling, true))
      }
    }
    return costWrappers.toTypedArray()
  }

  /**
   * @param period
   * @return
   */
  public static function getEREStandardPremiumWrappers(period : PolicyPeriod) : GL_CostWrapper[] {
    var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
    var costWrappers : List<GL_CostWrapper> = {}

    //Liability Pemium Total
    if (period.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      var termsTotal = getERETermPremiums(glCosts).toSet().AmountSum(period.PreferredSettlementCurrency)
      costWrappers.add(new GL_CostWrapper(0, null, DisplayKey.get("TDIC.Web.Policy.GL.Header.TotalforAllTerms"), termsTotal, false))
    }
    //Standard Premiums
    var standardPremiums = getEREStandardPremiums(glCosts.getStandardPremiums_TDIC().toSet())

    if (standardPremiums.HasElements) {
      var addnlInsdCosts = standardPremiums.where(\elt -> elt typeis GLAddnlInsdSchedCost_TDIC).toSet()
      if (addnlInsdCosts.HasElements) {
        var firstCost = addnlInsdCosts.first()
        costWrappers.add(new GL_CostWrapper(getPremiumOrder(firstCost), firstCost, getStandardPremiumDisplayName(firstCost), addnlInsdCosts.AmountSum(period.PreferredSettlementCurrency), true))
      }
      for (cost in standardPremiums.where(\elt -> not addnlInsdCosts.contains(elt))) {
        costWrappers.add(new GL_CostWrapper(getPremiumOrder(cost), cost, getStandardPremiumDisplayName(cost), cost.ActualAmountBilling, true))
      }
    }
    return costWrappers.toTypedArray()
  }

  /**
   * Total premium with tax for ERE screen
   */
  public static function getTotalCost(period : PolicyPeriod, isEREJob : Boolean) : MonetaryAmount {
    if(period.validateERECyberChangeReason){
      return period.EREStandardPrem
    }
    if (isEREJob) {
      var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
      var erePremiums = getERETermPremiums(glCosts).union(getEREStandardPremiums(glCosts)).union(getERETaxesAndSurcharges(glCosts))
      return erePremiums.AmountSum(period.PreferredSettlementCurrency)
    }
    return period.TotalCostRPT
  }

  public static function getTotalCostPolicyFile(period : PolicyPeriod) : MonetaryAmount {
    if(period.validateERECyberChangeReason){
      return period.EREStandardPrem
    }
    return period.TotalCostRPT
  }

  public static function EREStandardPremVal(period : PolicyPeriod) : GL_CostWrapper[] {
    var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
    var costWrappers : List<GL_CostWrapper> = {}

      var termsTot = glCosts.getStandardPremiums_TDIC().where(\elt -> elt.DisplayName == "Cyber Suite Liability Extended Reporting Period Endorsement").toSet().AmountSum(period.PreferredSettlementCurrency)
      costWrappers.add(new GL_CostWrapper(0, null, "Cyber Suite Liability Extended Reporting Period Endorsement", termsTot, false))

    return costWrappers.toTypedArray()
  }

  public static function validateERECyberChangeReason(period : PolicyPeriod) : Boolean{
    if(period.GLLineExists and (period.Job typeis PolicyChange) and (period.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).Count==1)){
      return true
    }
    return false
  }
  /**
   * Taxes & Surcharges total for ERE screen
   */
  public static function getTotalTaxSurcharges(period : PolicyPeriod, isEREJob : Boolean) : MonetaryAmount {
    if (isEREJob) {
      var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
      return getERETaxesAndSurcharges(glCosts).AmountSum(period.PreferredSettlementCurrency)
    }
    return period.TaxAndSurchargesRPT
  }

  public static function getTotalNonERETaxSurcharges(period : PolicyPeriod) : MonetaryAmount {
    if (period.isERERating_TDIC) {
      var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
      return glCosts.getTaxSurchargePremiums_TDIC()
          .where(\elt -> elt typeis GLStateCost and not ereCostTypeMap.get(typeof(elt)).contains(elt.StateCostType.Code))
          .toSet()
          .AmountSum(period.PreferredSettlementCurrency)
    }
    return period.TaxAndSurchargesRPT
  }

  /**
   * Premium that includes ERE Surcharges
   */
  public static function getPolicyFileTotalPremium(period : PolicyPeriod) : MonetaryAmount {
    if(period.validateERECyberChangeReason){
      return period.EREStandardPrem
    }
    if (period.isERERating_TDIC) {
      var ereSurcharges = getERETaxesAndSurcharges(period.GLLine.Costs.cast(GLCost).toSet())
      var ereTotal = ereSurcharges.AmountSum(period.PreferredSettlementCurrency)
      return period.TotalPremiumRPT + ereTotal
    }
    return period.TotalPremiumRPT
  }

  /**
   * Total premium prior tax for ERE screen
   */
  public static function getTotalPremium(period : PolicyPeriod, isEREJob : Boolean) : MonetaryAmount {
    if(period.validateERECyberChangeReason){
      return period.EREStandardPrem
    }
    if (isEREJob) {
      var glCosts = period.GLLine.Costs.cast(GLCost).toSet()
      var erePremiums = getERETermPremiums(glCosts).union(getEREStandardPremiums(glCosts))
      return erePremiums.AmountSum(period.PreferredSettlementCurrency)
    }
    return period.TotalPremiumRPT
  }

  /**
   * Method to get attached Exposure from cost during policy change
   */
  public static function getTermExposure(cost : GLCost) : GLExposure {
    var period = cost.Branch
    if (cost typeis GLCovExposureCost) {
      if (period.Job typeis PolicyChange) {
        var exposures = cost.Branch.Policy.BoundPeriods*.GLLine*.Exposures.orderBy(\elt -> elt.SliceDate).toList()
        if (cost.GLCostType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY and cost.ERESliceDate_TDIC != null) {
          for (exp in exposures) {
            if (exp.SliceDate != null and cost.ERESliceDate_TDIC == exp.SliceDate) {
              return exp
            }
          }
        } else {
          for (exp in exposures) {
            if (exp.SliceDate != null and cost.EffectiveDate == exp.SliceDate) {
              //GWPS-2035 : Territory code and Exposures are not correctly displayed on screen.
              if(cost.GLCostType == GLCostType_TDIC.TC_PROFESSIONALLIABILITY and cost.ExpDate == cost.Branch.PeriodEnd) {
                return cost.GLExposure
              }
              return exp
            }
          }
        }
      }
      return cost.GLExposure
    }
    return null
  }

  /**
   *
   */
  public static function getStandardPremiumDisplayName(cost : GLCost) : String {
    if (cost typeis GLStateCost) {
      var line = cost.Branch.GLLine
      switch (cost.StateCostType) {
        case GLStateCostType.TC_ERE_PLCLAIMSMADE_TDIC:
          if (line.GLExtendedReportPeriodCov_TDICExists and not line.GLExtendedReportPeriodCov_TDIC?.GLERPRated_TDICTerm?.Value) {
            return DisplayKey.get("TDIC.Web.Policy.GL.Header.ERECredit")
          }
          break
        default:
          return cost.StateCostType.DisplayName
      }
    } else if (cost typeis GLAddnlInsdSchedCost_TDIC) {
      if (cost.GLAddnlInsuredCostType_TDIC != null) {
        return cost.GLAddnlInsuredCostType_TDIC.DisplayName
      }
      return GLAddnlInsuredCostType_TDIC.TC_ADDITIONALINSURED.DisplayName
    } else if (cost typeis GLMinPremCost_TDIC) {
      return DisplayKey.get("TDIC.Web.Policy.GL.MinimumPremium_TDIC")
    }
    return cost.DisplayName
  }

  /**
   *
   */
  public static function isEREJob(period : PolicyPeriod, jobWizardHelper : JobWizardHelper) : Boolean {
    return (jobWizardHelper != null and period.isERERating_TDIC)
  }

  /******************************************************* Private functions **************************************************************/
  private static function getPremiumOrder(cost : GLCost) : double {
    var key : String = null
    if (cost != null) {
      if (cost typeis GLAddnlInsdSchedCost_TDIC) {
        key = "GLAddnlInsdSchedCost_TDIC"
      } else if (cost typeis GLStateCost) {
        key = cost.StateCostType.Code
      } else if (cost typeis GLMinPremCost_TDIC) {
        key = "GLMinPremCost_TDIC"
      }
    }
    return getPremiumOrder(key)
  }

  /**
   * @param key
   * @return
   */
  private static function getPremiumOrder(key : String) : double {
    if (key != null) {
      return standardPremiumOrderMap.get(key)
    }
    return 100.0
  }

}

