package tdic.web.pcf.helper

uses gw.plugin.billing.InstallmentPlanData
uses gw.billing.PolicyPeriodBillingInstructionsManager

class PolicyPaymentPlanScreenHelper {


  /**
   * Jeff Lin, 12/02/2019, 3:19 PM, GPC-2575: 12P payment plan is available for non migrated policy (Should not)
   * @param bcInstallPlans
   * @param policyPeriodBillingInstructionsManager
   * @return
   */
  public static function getFilteredIntallmentPlans_TDIC(bcInstallPlans: InstallmentPlanData[], policyPeriodBillingInstructionsManager: PolicyPeriodBillingInstructionsManager) : InstallmentPlanData[] {

    var period = policyPeriodBillingInstructionsManager.PeriodInternal
    var currentPlan = bcInstallPlans.firstWhere(\elt -> elt.BillingId == policyPeriodBillingInstructionsManager.PaymentPlanChoice.BillingId)

    //do not drop 12 pay plan for policies, those are already in 12 pay in BC) & no filter for Legacy policies
    if(currentPlan != null and (currentPlan.NumberOfInstallments == 12 or period.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION)) {  // For Migrated Policies
      return bcInstallPlans
    } else if(period.Job typeis Submission                                                      //New Business
        and period.GLLineExists and period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"       //PL Claims Made
        and period.GLLine.GLNewDentistDiscount_TDICExists                                       //New Dentist discount exists
        and User.util.CurrentUser.ExternalUser) {                                               //External brocker
      return bcInstallPlans.where(\elt -> elt.NumberOfInstallments == 0)                        //only 1-Pay plan is allowed
    }

    return bcInstallPlans.where(\elt -> elt.NumberOfInstallments != 12) // For Non-Migrated Policies
  }

  /**
   * Jeff Lin, 12/02/2019, 3:19 PM, GPC-633: set default installment payment plan to 1 Pay for GL-CM policies if New Dentist Program is selected.
   * @param bcInstallPlans
   * @param period
   * @param periodBillingInstructionsManager
   * @return
   */
  public static function getDefaultIntallmentPlanBillingID(bcInstallPlans: InstallmentPlanData[], period: PolicyPeriod, periodBillingInstructionsManager: PolicyPeriodBillingInstructionsManager) : String {
    if (period.GLLineExists and period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and period.GLLine.GLNewDentistDiscount_TDICExists) {
      return bcInstallPlans.firstWhere(\elt -> elt.NumberOfInstallments == 0)?.BillingId
    }
    return periodBillingInstructionsManager.PaymentPlanChoice?.BillingId
  }

}