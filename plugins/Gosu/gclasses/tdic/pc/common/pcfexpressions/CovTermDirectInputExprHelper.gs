package tdic.pc.common.pcfexpressions

uses gw.lob.gl.TDIC_CyberCoverageSubLimits

class CovTermDirectInputExprHelper {

  private static var instance : CovTermDirectInputExprHelper
  private construct(){
  }

  static property get Instance(): CovTermDirectInputExprHelper {
    instance = instance == null ? new CovTermDirectInputExprHelper()  : instance
    return instance
  }

  function setCovTermLimits(bldg : BOPBuilding) {
    bldg.setOrdinanceTermLimits_TDIC();
    bldg.syncCoverages();
    bldg.setBPPDebrisLimit_TDIC();
    bldg.setBuildingDebrisLimit_TDIC();
    bldg.setEnhancedCoverageLimits();
    bldg.remEnhancedCoverageLimits()
  }

  function plCovRules(line : GLLine) {
    line.setCybDeductibleLimit_TDIC()
    line.setNJEndorsementLimits_TDIC()
    line.checkCovALimits_TDIC()
    line.setCovBLimits_TDIC()
    line.checkAggLimitCovA_TDIC()
    line.checkAggLimitCovB_TDIC()
    var cybLimits = new TDIC_CyberCoverageSubLimits()
    cybLimits.getCyberSubLimits(line)
  }

  function bopCovRules(cov : Coverage, bldg : BOPBuilding){
    if(cov.Pattern.CodeIdentifier=="BOPEqBldgCov"){
      bldg.setEarthquakeLimits_TDIC();
      bldg.setZipcodeTerritoryCodeForEQEQSLCov_TDIC()
	  bldg.setEarthquakeDeductibles_TDIC()

    }
    if(cov.Pattern.CodeIdentifier == "BOPILMineSubCov_TDIC"){
      bldg.setILSubLimit_TDIC()
    }
    if(cov.Pattern.CodeIdentifier == "BOPEncCovEndtCond_TDIC") {
      bldg.setEnhancedCoverageLimits();
    }
    if(cov.Pattern.CodeIdentifier != "BOPEncCovEndtCond_TDIC") {
      bldg.remEnhancedCoverageLimits();
    }
    if(cov.Pattern.CodeIdentifier=="BOPEqSpBldgCov") {
      bldg.setEQSPExistence_TDIC()
      bldg.setZipcodeTerritoryCodeForEQSLCov_TDIC()
      bldg.setEarthquakeSLDeductibles_TDIC()
      bldg.setEQSLZone_TDIC()
    }
  }

}