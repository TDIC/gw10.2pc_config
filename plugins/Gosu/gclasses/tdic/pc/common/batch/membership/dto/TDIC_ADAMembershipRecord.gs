package tdic.pc.common.batch.membership.dto

uses java.util.Date
/**
 * Created with IntelliJ IDEA.
 * User: KesavaT
 * Date: 4/13/15
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_ADAMembershipRecord {

  var _adaMembershipNumber : String as ADANumber
  var _memberType : String as MemberType
  var _dateUpdated : Date as DateUpdated

  /**
   * US1316
   * 04/13/2015 Kesava Tavva
   *
   * Override function to return field values in a String object.
   */
  override function toString() : String{
    return "ADANumber: ${ADANumber} | Status: ${MemberType} | DateUpdated: ${DateUpdated}"
  }
}