package tdic.pc.common.batch.finance.reports

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.upgrade.Coercions
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinancePremiumsInterface
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportTypeEnum
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportUtil

uses java.io.File
uses java.io.PrintWriter

abstract class TDIC_FinancePremiumsReportBatchBase extends BatchProcessBase {
  private static final var CLASS_NAME = TDIC_FinancePremiumsReportBatchBase.Type.RelativeName

  private var _batchRunPeriodStartDate : Date as readonly PeriodStartDate
  private var _batchRunPeriodEndDate : Date as readonly PeriodEndDate
  private var _emailTo : String as readonly EmailTo
  private var _emailRecipient : String as readonly EmailRecipient
  public static final var TERMINATE_SUB : String = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-PC Finance Premiums Batch Job Terminated"
  protected var _filePath : String

  construct(args : Object[]){
    super(BatchProcessType.TC_FINANCEPREMIUMSREPORTBATCH_TDIC)
    initialize(args)
  }

  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }

  protected function getPremiumPoliciesProcessor(reportType : TDIC_FinanceReportTypeEnum) : TDIC_FinancePremiumsInterface {
    return TDIC_FinancePremiumsReportFactory.getPremiumPoliciesProcessor(reportType, PeriodStartDate, PeriodEndDate)
  }

  protected function writeToFile(reportType : TDIC_FinanceReportTypeEnum, premLines : List<String>) {
    var logPrefix = "${CLASS_NAME}#writeToFile(reportType, premLines)"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Entering")
    if(!new File(_filePath).exists()){
      EmailUtil.sendEmail(EmailRecipient, "Unable to access path on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to access path:" + _filePath + ": The Directory May Not Exist or There is an Error in network Connection.")
      throw new Exception("Cannot Access ${_filePath}. The Directory May Not Exist or There is an Error in network Connection")
    } else {
      TDIC_FinanceReportConstants.LOGGER.info("$${logPrefix}- Access to ${_filePath} is Successful.")
    }
    var flatFile:PrintWriter
    try{
      TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Writing premium details to file started.")
      flatFile = new PrintWriter(getFileName(reportType))     //Open flat file for writing
      premLines?.each( \ elt -> {
        incrementOperationsCompleted()
        flatFile.println(elt)
      })
      TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Writing premium details to file completed.")
    }catch(var e:Exception){
      OperationsFailed = OperationsFailed + premLines?.size()
      EmailUtil.sendEmail(EmailRecipient, "Unable to write premium lines to file on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to write premium lines to a file ${_filePath}" + e.LocalizedMessage)
      throw new Exception("Exception occurred while writing Premium lines to a file ${_filePath}",e)
    } finally{
      if(flatFile!=null) flatFile.close()
    }
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Exiting")
  }

  private function getFileName(reportType : TDIC_FinanceReportTypeEnum) : String {
    switch(reportType){
      case TDIC_FinanceReportTypeEnum.WPRM :
        return "${_filePath}${TDIC_FinanceReportConstants.WPM_FILE_NAME}-${TDIC_FinanceReportUtil.getFileNameSuffix()}.GLT"
      case TDIC_FinanceReportTypeEnum.EPRM :
        return "${_filePath}${TDIC_FinanceReportConstants.EPM_FILE_NAME}-${TDIC_FinanceReportUtil.getFileNameSuffix()}.GLT"
      default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }

  private function initialize(args : Object[]){
    _filePath = TDIC_FinanceReportUtil.getFilePath()
    if(args == null || args?.Count == 0){
      _batchRunPeriodStartDate = TDIC_FinanceReportUtil.getBatchRunPeriodStartDate(gw.api.util.DateUtil.currentDate().MonthOfYear-1, gw.api.util.DateUtil.currentDate().YearOfDate.intValue())
    } else {
      validateArguments(args)
      _batchRunPeriodStartDate = TDIC_FinanceReportUtil.getBatchRunPeriodStartDate(Coercions.makePIntFrom(args[0]), Coercions.makePIntFrom(args[1]))
    }
    _batchRunPeriodEndDate = TDIC_FinanceReportUtil.getBatchRunPeriodEndDate(_batchRunPeriodStartDate)
  }

  private function validateArguments(args : Object[]) {
    var logPrefix = "${CLASS_NAME}#validateArguments(reportType, premLines)"
    if(args.Count == 2){
      try{
        var month: int = args[0].toString().toInt()
        var year: int = args[1].toString().toInt()
        TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- Arguments received are ${month}, ${year}")
        return
      }catch(e : Exception){
        throw new Exception("Invalid arguments (${args[0]}, ${args[1]}) received for batch run. Expected batch command arguments are <mm>(2 digit month. Valid values 01,02,..12) and <yyyy>(4 digit year).")
      }
    } else {
      throw new Exception("Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year).")
    }
  }

  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(TDIC_FinanceReportConstants.EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_FinanceReportConstants.EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(TDIC_FinanceReportConstants.EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_FinanceReportConstants.EMAIL_RECIPIENT}' from integration database.")
    }
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  protected function getEmailSubject(flag : Boolean) : String {
    var subject : StringBuffer = new StringBuffer()
    if(flag){
      subject.append("SUCCESS::${gw.api.system.server.ServerUtil.ServerId}-PC GLInterface Batch Job Completed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }else{
      subject.append("FAILURE::${gw.api.system.server.ServerUtil.ServerId}-PC GLInterface Batch Job Failed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }
    return subject.toString()
  }

}