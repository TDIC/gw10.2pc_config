package tdic.pc.common.batch.finance

uses java.math.BigDecimal

class TDIC_FinanceEarnedPremiumObject {
  var _reportingMonth: int as ReportingMonth
  var _reportingYear: int as ReportingYear
  var _premiumDays: int as PremiumDays
  var _amount: BigDecimal as Amount
}