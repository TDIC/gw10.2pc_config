package tdic.pc.common.batch.finance.reports.util

uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.text.SimpleDateFormat

class TDIC_FinanceReportConstants {
  public static final var LOGGER: Logger = LoggerFactory.getLogger("TDIC_FINANCE_PREMIUMS")
  protected static final var FILE_PATH_PROP_FIELD : String = "gl.pc.outputdir"
  public static final var DATE_FORMAT : SimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
  public static final var FILE_SUFFIX_DATE_FORMAT : SimpleDateFormat = new SimpleDateFormat("MM-dd-yyyy-HHmmss")
  public static final var WPM_FILE_NAME : String = "GW-LGL-WPM"
  public static final var EPM_FILE_NAME : String = "GW-LGL-EPM"

  public static final var EMAIL_TO_PROP_FIELD : String = "PCGLNotificationEmail"
  public static final var EMAIL_RECIPIENT : String = "PCInfraIssueNotificationEmail"

  public static final var WC7WORKERSCOMP : String = "Workers' Compensation"
  public static final var COMMERCIAL_PROPERTY : String = "Commercial Property"
  public static final var PROFESSIONAL_LIABILITY : String = "Professional Liability"


}