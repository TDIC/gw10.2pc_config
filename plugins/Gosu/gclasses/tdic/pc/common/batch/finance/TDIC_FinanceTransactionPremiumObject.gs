package tdic.pc.common.batch.finance

uses org.slf4j.LoggerFactory

uses java.math.BigDecimal
uses java.text.DecimalFormat

class TDIC_FinanceTransactionPremiumObject {
  private  var CLASS_NAME = TDIC_FinanceTransactionPremiumObject.Type.RelativeName
  private  var DECIMAL_FORMAT: DecimalFormat = new DecimalFormat("0.00")
  private  var logger = LoggerFactory.getLogger("TDIC_FINANCE_PREMIUMS")

  private var _chargePattern: String as ChargePattern
  private var _costType: String as CostType
  private var _stateCostType: String as StateCostType
  private var _cmpType: String as CMPType
  private var _transactionID : Long as TransactionID
  private var _transCreateTime: Date as TransactionCreateTime
  private var _transEffDate: Date as TransactionEffDate
  private var _transExpDate: Date as TransactionExpDate
  private var _accountDate: Date as AccountDate
  private var _perDayPremium: BigDecimal
  private var _isWrittenPremium:Boolean as IsWrittenPremium
  private var _reportingMonth: int as ReportingMonth
  private var _reportingYear: int as ReportingYear
  private var _premiumDays: int as PremiumDays
  private var _amount: BigDecimal as Amount

  private var _reportPeriodPremiumsList:List<TDIC_FinanceEarnedPremiumObject> as ReportPeriodPremiumsList

  construct(pp: PolicyPeriod, transaction: Transaction){
    buildCostTypes(transaction)
    _transactionID = transaction.ID.Value
    _transCreateTime = transaction.CreateTime
    _transEffDate = transaction.EffDate
    _transExpDate = transaction.ExpDate
    _accountDate = ((pp.Job.CloseDate < pp.EditEffectiveDate) ? pp.EditEffectiveDate : pp.Job.CloseDate).trimToMidnight()
    _isWrittenPremium = Boolean.TRUE
    _premiumDays = transaction.EffDate.differenceInDays(transaction.ExpDate)
    _reportingMonth = AccountDate.MonthOfYear
    _reportingYear = AccountDate.YearOfDate
    _amount = transaction.Amount
    if(transaction.Cost.ChargePattern == typekey.ChargePattern.TC_PREMIUM or transaction.Cost.ChargePattern == TC_EREPREMIUM_TDIC)
      buildReportPeriodEarnedPremiums(pp,transaction)
  }

  function buildCostTypes(t: Transaction) {
    _chargePattern = t.Cost.ChargePattern.Code

    if(t typeis WC7Transaction)  {
      _costType = t.WC7Cost.Subtype.Code
      _stateCostType = t.WC7Cost typeis WC7JurisdictionCost ? t.WC7Cost.JurisdictionCostType.Code : null
    }else if(t typeis BOPTransaction) {
      _costType = t.BOPCost.Subtype.Code
      switch (typeof(t.BOPCost)) {
        case BOPBuildingCost_TDIC:
          _stateCostType = t.BOPCost.BOPCostType_TDIC.DisplayName
          if(t.BOPCost.BOPCostType_TDIC == BOPCostType_TDIC.TC_MULTILINELIABILITYDISCOUNT){
            _cmpType = "BOPBuildingLiabCov_TDIC"
          }else{
            _cmpType = "BOPBuildingPropCov_TDIC"
          }
          break
        //GWPS-2068 - populating statecosttype and CMPtype in financepremiums_tdic for BOPModifier cost
        case BOPModifierCost_TDIC:
          _stateCostType = t.BOPCost.Modifier_TDIC.Pattern.DisplayName
          if(t.BOPCost.Modifier_TDIC.Pattern.CodeIdentifier == "BOPClosedEndCredit_TDIC") {
            _cmpType = "BOPBuildingPropCov_TDIC"
          } else if(t.BOPCost.Modifier_TDIC.Pattern.CodeIdentifier == "BOPIRPMLiab_TDIC"){
            _cmpType = "BOPBuildingLiabCov_TDIC"
          } else if(t.BOPCost.Modifier_TDIC.Pattern.CodeIdentifier == "BOPIRPMProperty_TDIC"){
            _cmpType = "BOPBuildingPropCov_TDIC"
          }
          break
        default:
          _stateCostType = t.BOPCost.Coverage.DisplayName
          _cmpType = t.BOPCost.Coverage.CoverageCategory.CodeIdentifier
          break
      }
    }else if(t typeis GLTransaction) {
      _costType = t.GLCost.Subtype.Code

      switch(typeof(t.GLCost)){

        case GLAddlInsuredCost:
        case GLAddnlInsdSchedCost_TDIC:
        case GLLineCovCost:
        case GLCovCost_TDIC:        {
          _stateCostType = t.GLCost.Coverage.PatternCode
          break
        }
        case GLCovExposureCost:{
          _stateCostType = t.GLCost.GLCostType.Code
          break
        }
        case GLHistoricalCost_TDIC:{
          _stateCostType = tdic.pc.config.rating.RatingConstants.glHistoricalCostType
          break
        }
        case GLStateCost:{
          _stateCostType = t.GLCost.StateCostType.Code
          break
        }
        default: {
          _stateCostType = t.GLCost.Coverage.PatternCode
          break
        }
      }
    }

  }

  private function buildReportPeriodEarnedPremiums(pp: PolicyPeriod, transaction: Transaction){
    var reportPeriodStartDate = pp.Job.CloseDate.trimToMidnight().FirstDayOfMonth
    var reportPeriodEndDate:Date = null
    var tempTransEffDate:Date = null
    var tempTransExpDate:Date = null

    var writtenExposureDays = transaction.EffDate.differenceInDays(transaction.ExpDate)
    _perDayPremium = transaction.Amount.Amount / writtenExposureDays
    logger.info("${CLASS_NAME}#send(): ${pp.PolicyNumber}-${pp.TermNumber}; ${pp.Job.JobNumber}; ${pp.Policy.ProductCode}; ${_costType} - ${_stateCostType};transaction.Amount ${transaction.Amount} ;  writtenExposureDays: ${writtenExposureDays}; PerdayPremium: ${_perDayPremium}")

    var reportPeriodPremiumObject: TDIC_FinanceEarnedPremiumObject = null

    _reportPeriodPremiumsList = new ArrayList<TDIC_FinanceEarnedPremiumObject>()
    //Calculate EarnedPremiums
    do{

      reportPeriodPremiumObject = new TDIC_FinanceEarnedPremiumObject()

      reportPeriodPremiumObject.PremiumDays = 0

      reportPeriodEndDate = reportPeriodStartDate.addMonths(1).addDays(-1)

      if((_accountDate >= reportPeriodStartDate and _accountDate <= reportPeriodEndDate)
          and ("Audit".equalsIgnoreCase(pp.Job.Subtype.DisplayName) or transaction.ExpDate < reportPeriodStartDate)){
        logger.info("Setting the PremiumDays to difference :" + transaction.EffDate.differenceInDays(transaction.ExpDate))
        reportPeriodPremiumObject.PremiumDays = transaction.EffDate.differenceInDays(transaction.ExpDate)
      }else if ("Audit".equalsIgnoreCase(pp.Job.Subtype.DisplayName)
          or (_accountDate > reportPeriodEndDate) or (transaction.ExpDate < reportPeriodStartDate)) {
        logger.info(" Setting the PremiumDays to 0")
        reportPeriodPremiumObject.PremiumDays = 0
      }else {

        tempTransEffDate = null
        tempTransExpDate = null

        if((_accountDate >= reportPeriodStartDate and _accountDate <= reportPeriodEndDate)
            or (transaction.EffDate > reportPeriodStartDate)){
          tempTransEffDate =  transaction.EffDate
        }else{
          tempTransEffDate = reportPeriodStartDate
        }

        if(transaction.ExpDate <= reportPeriodEndDate){
          tempTransExpDate = transaction.ExpDate.addDays(-1)
        }else {
          tempTransExpDate = reportPeriodEndDate
        }
        logger.info("In else setting the PremiumDays to " + (tempTransEffDate.differenceInDays(tempTransExpDate) + 1))
        reportPeriodPremiumObject.PremiumDays = tempTransEffDate.differenceInDays(tempTransExpDate) + 1
      }
      logger.info("${CLASS_NAME}#send(): reportPeriodEndDate - "+ reportPeriodEndDate + " transaction.ExpDate - " + transaction.ExpDate)
      if(reportPeriodEndDate >= transaction.ExpDate){
        logger.info("${CLASS_NAME}#send(): reportPeriodEndDate is greater than transaction.ExpDate")

        reportPeriodPremiumObject.PremiumDays = transaction.EffDate.differenceInDays(transaction.ExpDate)
        logger.info("${CLASS_NAME}#send(): setting reportPeriodPremiumObject.PremiumDays : " + reportPeriodPremiumObject.PremiumDays)
        reportPeriodPremiumObject.Amount = new BigDecimal(DECIMAL_FORMAT.format(reportPeriodPremiumObject.PremiumDays*_perDayPremium))
        if(reportPeriodPremiumObject.PremiumDays == 0){ //Expiry date is start of the reportperiod month
          if(_reportPeriodPremiumsList.HasElements and _reportPeriodPremiumsList.last() != null)
            _reportPeriodPremiumsList.last()?.Amount = _reportPeriodPremiumsList.last()?.Amount + reportPeriodPremiumObject.Amount
        }

      }else{
        reportPeriodPremiumObject.Amount = new BigDecimal(DECIMAL_FORMAT.format(reportPeriodPremiumObject.PremiumDays*_perDayPremium))
      }

      reportPeriodPremiumObject.ReportingMonth = reportPeriodStartDate.MonthOfYear
      reportPeriodPremiumObject.ReportingYear = reportPeriodStartDate.YearOfDate
      logger.info("${CLASS_NAME}#send(): ${pp.PolicyNumber}-${pp.TermNumber}; ${pp.Job.JobNumber}; ${pp.Policy.ProductCode}; ${_costType} - ${_stateCostType}; ${reportPeriodPremiumObject.PremiumDays}; ${reportPeriodPremiumObject.ReportingMonth}-${reportPeriodPremiumObject.ReportingYear}-->${reportPeriodPremiumObject.Amount}")

      if(reportPeriodPremiumObject.PremiumDays > 0)
        _reportPeriodPremiumsList.add(reportPeriodPremiumObject)

      reportPeriodStartDate = reportPeriodEndDate.addDays(1)

    }while(reportPeriodEndDate < transaction.ExpDate)
  }
}