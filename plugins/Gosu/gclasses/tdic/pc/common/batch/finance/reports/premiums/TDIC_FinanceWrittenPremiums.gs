package tdic.pc.common.batch.finance.reports.premiums

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLineTypeEnum
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLinesInterface
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants
uses typekey.Job
uses typekey.WC7Cost

uses java.math.BigDecimal

class TDIC_FinanceWrittenPremiums extends TDIC_FinancePremiumsBase {


  private static final var CLASS_NAME = TDIC_FinanceWrittenPremiums.Type.RelativeName
  protected static final var _logger: Logger = LoggerFactory.getLogger("TDIC_FINANCE_PREMIUMS")

  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_FinancePremiumLinesInterface){
    super(periodStartDate, periodEndDate, prmLines)
  }

  override function getPremiumAmounts(productCodes : List<String>) : List<TDIC_FinanceReportPremiumObject> {
    var reportPremiums = new ArrayList<TDIC_FinanceReportPremiumObject>()
    for(productCode in productCodes) {
      switch (productCode) {
        case TDIC_FinanceReportConstants.WC7WORKERSCOMP: {
          reportPremiums.addAll(buildWC7PremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        case TDIC_FinanceReportConstants.COMMERCIAL_PROPERTY: {
          reportPremiums.addAll(buildCPPremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        case TDIC_FinanceReportConstants.PROFESSIONAL_LIABILITY: {
          reportPremiums.addAll(buildPLPremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        default:
          return null
      }
    }
    return reportPremiums
  }

  protected function retrievePremiums(productCode: String, reportingMonth: int, reportingYear: int): IQueryBeanResult<FinancePremiums_TDIC> {
    var prmsQuery = Query.make(FinancePremiums_TDIC)
        .compare(FinancePremiums_TDIC#ProductCode,Equals,productCode)
        .compare(FinancePremiums_TDIC#ReportingYear,Equals,reportingYear)
        .compare(FinancePremiums_TDIC#ReportingMonth,Equals,reportingMonth)
        .select()
    return prmsQuery
  }

  private function buildWC7PremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()

    var jurisdictions : Set<String> = null

    var premiums = retrievePremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)
    //Non-Audits
    var nonAuditPremiums = premiums.where(\elt -> elt.JobType != Job.TC_AUDIT.Code)
    jurisdictions = nonAuditPremiums*.Jurisdiction?.toSet()

    for (j in jurisdictions) {
      premiumAmounts.addAll(buildPremiumAmounts(productCode, j, nonAuditPremiums, Boolean.FALSE))
    }

    //Audits
    var auditPremiums = premiums.where(\elt -> elt.JobType == Job.TC_AUDIT.Code)
    jurisdictions = auditPremiums*.Jurisdiction?.toSet()

    for (j in jurisdictions) {
      premiumAmounts.addAll(buildPremiumAmounts(productCode, j, auditPremiums, Boolean.TRUE))
    }

    //State assessments
    var linePremiumAmount : BigDecimal = null
    jurisdictions = premiums*.Jurisdiction?.toSet()
    for (j in jurisdictions) {
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.ChargePattern != ChargePattern.TC_PREMIUM.Code
                            and elt.ChargePattern != ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.WC_WP_SURCHARGES, linePremiumAmount))
    }
    return premiumAmounts
  }

  private function buildPremiumAmounts(productCode: String, j: String, premiums: List<FinancePremiums_TDIC>, isAudit: Boolean): List<TDIC_FinanceReportPremiumObject>{
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()
    var linePremiumAmount : BigDecimal = null
    //Terrorism Premium
    linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j and elt.StateCostType == WC7JurisdictionCostType.TC_TERRORPREM.Code)?.sum(\elt -> elt.Amount)
    premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,
        (isAudit) ? TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM_AUDIT : TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM, linePremiumAmount))

    //ExpenseConstant Premium
    linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j and elt.StateCostType == WC7JurisdictionCostType.TC_EXPENSECONST.Code)?.sum(\elt -> elt.Amount)
    premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,
        (isAudit) ? TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT_AUDIT : TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT, linePremiumAmount))

    //Premium+Discounts
    linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                          and (elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
                                or elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          and elt.StateCostType != WC7JurisdictionCostType.TC_EXPENSECONST.Code
                          and elt.StateCostType != WC7JurisdictionCostType.TC_TERRORPREM.Code)
                        ?.sum(\elt -> elt.Amount)
    premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,
        (isAudit) ? TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM_AUDIT : TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM, linePremiumAmount))

    return premiumAmounts
  }

  private function buildCPPremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()
    var jurisdictions : Set<String> = null
    var premiums = retrievePremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)

    jurisdictions = premiums*.Jurisdiction?.toSet()
    var linePremiumAmount : BigDecimal = null
    for(j in jurisdictions){
      //Property
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j and elt.CMP_Type == "BOPBuildingPropCov_TDIC"
          and (elt.ChargePattern == ChargePattern.TC_PREMIUM.Code or elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
          and !(elt.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM.DisplayName or elt.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM.DisplayName))
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_WP_PROPERTY, linePremiumAmount))

      //EQUIPMENT BREAKDOWN
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j and elt.CMP_Type == "BOPBuildingPropCov_TDIC"
          and (elt.ChargePattern == ChargePattern.TC_PREMIUM.Code or elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
          and (elt.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM.DisplayName or elt.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM.DisplayName))
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_WP_EQUIP_BREAKDOWN, linePremiumAmount))

      //General Liability
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
          and (elt.ChargePattern == ChargePattern.TC_PREMIUM.Code or elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
          and elt.CMP_Type == "BOPBuildingLiabCov_TDIC")?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_WP_GENERAL_LIAB, linePremiumAmount))

      //State Assessments
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.ChargePattern != ChargePattern.TC_PREMIUM.Code
                            and elt.ChargePattern != ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_WP_SURCHARGES, linePremiumAmount))
    }

    return premiumAmounts
  }

  private function buildPLPremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()
    var jurisdictions : Set<String> = null
    var premiums = retrievePremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)

    jurisdictions = premiums*.Jurisdiction?.toSet()
    var linePremiumAmount : BigDecimal = null
    for(j in jurisdictions){
      //REP. ENDORSEMENTS
      //GWPS-2068 - excluding cyber liab offering
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                          and elt.Offering != "PLCyberLiab_TDIC"
                          and elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.PL_WP_REP_ENDORSEMENTS, linePremiumAmount))

      //Cyber Liability
      //GWPS-2068 - including ERE premium charge pattern
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
          and elt.Offering == "PLCyberLiab_TDIC"
          and (elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
          or elt.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
          and elt.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_WP_CYBER_LIAB, linePremiumAmount))

      //IDENTITY THEFT
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.StateCostType == GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_WP_IDENITY_THEFT, linePremiumAmount))

      //PROFESSIONAL LIABILITY OCCURRENCE
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.Offering == "PLOccurence_TDIC"
                            and elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB_OCCUR, linePremiumAmount))

      //EMPLOYMENT PRACTICES
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.Offering == "PLClaimsMade_TDIC"
                            and elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.StateCostType == GLStateCostType.TC_EPLI_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.PL_WP_EMPL_PRACTICES, linePremiumAmount))

      //PROF. LIAB
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.Offering == "PLClaimsMade_TDIC"
                            and elt.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code
                            and elt.StateCostType != GLStateCostType.TC_EPLI_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB, linePremiumAmount))

      //Surcharges
      linePremiumAmount = premiums.where(\elt -> elt.Jurisdiction == j
                            and elt.ChargePattern != ChargePattern.TC_PREMIUM.Code
                            and elt.ChargePattern != ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_WP_SURCHARGES, linePremiumAmount))
    }
    return premiumAmounts
  }
}