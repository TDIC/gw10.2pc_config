package tdic.pc.common.batch.finance.reports

uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinanceEarnedPremiums
uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinancePremiumsInterface
uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinanceWrittenPremiums
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinanceEarnedPremiumLines
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLinesInterface
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinanceWrittenPremiumLines
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportTypeEnum

class TDIC_FinancePremiumsReportFactory {


  @Param("reportType","represents Written or Earned premium type")
  @Returns("Returns TDIC_PremiumPoliciesInterface")
  protected static function getPremiumPoliciesProcessor(reportType : TDIC_FinanceReportTypeEnum, psd: Date, ped: Date) : TDIC_FinancePremiumsInterface {
    var prmPoliciesProcessor : TDIC_FinancePremiumLinesInterface
    switch(reportType){
      case TDIC_FinanceReportTypeEnum.WPRM :
        return new TDIC_FinanceWrittenPremiums(psd, ped, getPremiumLinesProcessor(reportType, ped))
      case TDIC_FinanceReportTypeEnum.EPRM :
        return new TDIC_FinanceEarnedPremiums(psd, ped, getPremiumLinesProcessor(reportType, ped))
      default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }


  @Param("reportType","represents Written or Earned premium type")
  @Returns("Returns TDIC_PremiumLinesInterface")
  private static function getPremiumLinesProcessor(reportType : TDIC_FinanceReportTypeEnum, ped : Date) : TDIC_FinancePremiumLinesInterface {
    switch(reportType){
      case TDIC_FinanceReportTypeEnum.WPRM :
        return new TDIC_FinanceWrittenPremiumLines(ped)
      case TDIC_FinanceReportTypeEnum.EPRM :
        return new TDIC_FinanceEarnedPremiumLines(ped)
      default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }

}