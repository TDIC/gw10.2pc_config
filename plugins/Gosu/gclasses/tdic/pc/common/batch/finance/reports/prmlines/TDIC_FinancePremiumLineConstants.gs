package tdic.pc.common.batch.finance.reports.prmlines

uses java.text.DecimalFormat
uses java.text.SimpleDateFormat

class TDIC_FinancePremiumLineConstants {
  //Line constants
  protected static final var BLANK_STRING : String = ""
  protected static final var DOUBLE_QUOTE : String  = "\""
  protected static final var COMMA_SEPARATOR: String = ","
  protected static final var DOT_SEPARATOR: String = "."
  protected static final var ZERO_AMOUNT_DOUBLE : double = 0.0
  protected static final var ZERO_AMOUNT_INT : int = 0
  protected static final var NEG_ADJ_Y : String = "Y"

  protected static final var GTR_COMPANY : int = 5
  protected static final var GTR_SOURCE_CODE : String = "IG"
  protected static final var ACCT_UNIT_PREFIX : String = "0005"
  protected static final var BAL_SHEET_ACCT_UNIT : String = "000"
  protected static final var SALES_ACCT_UNIT : String = "800"
  private static final var MMDDYY : String = "MM/dd/yy"
  private static final var MMDDYYYY : String = "MM/dd/yyyy"
  protected static final var DATE_FORMAT_MMDDYY : SimpleDateFormat = new SimpleDateFormat(MMDDYY)
  protected static final var DATE_FORMAT_MMDDYYYY : SimpleDateFormat = new SimpleDateFormat(MMDDYYYY)
  protected static final var INT_PATTERN_2 : String = "%02d"
  protected static final var INT_PATTERN_6 : String = "%06d"
  protected static final var DECIMAL_FORMAT : DecimalFormat = new DecimalFormat("0.00")

  /*
  private static final var ACC_NBR_TERRORISM : String = "0040502201"
  private static final var ACC_NBR_EXP_CONSTANT : String = "0040502202"
  private static final var ACC_NBR_PREMIUM : String = "0040502203"
  private static final var ACC_NBR_TERRORISM_AUDIT : String = "0040502204"
  private static final var ACC_NBR_EXP_CONSTANT_AUDIT : String = "0040502205"
  private static final var ACC_NBR_PREMIUM_AUDIT : String = "0040502206"
  private static final var ACC_NBR_STATE_ASSMNTS : String = "0026600000"
  private static final var ACC_NBR_PRM_RCV_ACCOUNT : String = "0012652230"
  //private static final var ACC_NBR_UNEARNED_PREMIUM : String = "0020200000"
  //private static final var ACC_NBR_CHG_UNEARNED_PREMIUM : String = "0041700000"
*/
  //R2
  protected static final var ACC_NBR_UNEARNED_PREMIUM: String = "0020201200"
  protected static final var ACC_NBR_CHG_UNEARNED_PREMIUM: String = "0041701200"


  private static final var LOB_CODE_WC7WORKERSCOMPLINE : String = "40"
  private static final var LOB_CODE_CP_PROPERTY : String = "30"
  private static final var LOB_CODE_CP_EQUIP_BREAKDOWN : String = "33"
  private static final var LOB_CODE_CP_GENERAL_LIABILITY : String = "36"
  private static final var LOB_CODE_PL_CYBER_LIABILITY : String = "38"
  private static final var LOB_CODE_PL_REP_ENDORSEMENTS : String = "12"
  private static final var LOB_CODE_PL_IDENTITY_THEFT : String = "14"
  private static final var LOB_CODE_PL_EMP_PRACTICES: String = "20"
  private static final var LOB_CODE_PL_PROF_LIAB: String ="10"
  private static final var LOB_CODE_PL_PROF_LIAB_OCCUR =	"17"

  private static final var LOB_LABEL_WC7WORKERSCOMPLINE : String = "WC"
  private static final var LOB_LABEL_CP_PROPERTY : String = "PR"
  private static final var LOB_LABEL_CP_EQUIP_BREAKDOWN : String = "EB"
  private static final var LOB_LABEL_CP_GENERAL_LIABILITY : String = "GL"
  private static final var LOB_LABEL_PL_CYBER_LIABILITY : String = "CL"
  private static final var LOB_LABEL_PL_REP_ENDORSEMENTS : String = "RE"
  private static final var LOB_LABEL_PL_IDENTITY_THEFT : String = "ID"
  private static final var LOB_LABEL_PL_EMP_PRACTICES: String = "EP"
  private static final var LOB_LABEL_PL_PROF_LIAB: String ="PL"
  private static final var LOB_LABEL_PL_PROF_LIAB_OCCUR =	"PL-OCC"

  private static final var STATE_AK : String = "02"
  private static final var STATE_AZ : String = "04"
  private static final var STATE_CA : String = "06"
  private static final var STATE_GA : String = "13"
  private static final var STATE_HI : String = "15"
  private static final var STATE_ID : String = "16"
  private static final var STATE_IL : String = "17"
  private static final var STATE_MN : String = "28"
  private static final var STATE_MT : String = "31"
  private static final var STATE_NV : String = "33"
  private static final var STATE_NJ : String = "35"
  private static final var STATE_NM : String = "36"
  private static final var STATE_ND : String = "39"
  private static final var STATE_OR : String = "43"
  private static final var STATE_PA : String = "45"
  private static final var STATE_TN : String = "50"
  private static final var STATE_WA : String = "56"

  protected static final var LOB_CODE_MAP : Map<TDIC_FinancePremiumLineTypeEnum, String> = buildLobCodeMap()
  protected static final var LOB_LABEL_MAP : Map<TDIC_FinancePremiumLineTypeEnum, String> = buildLobLabelMap()
  protected static final var STATE_UNIT_MAP : Map<String, String> =  buildStateAcctUnitMap()


  @Returns("Returns Map object for Policy Line (lob) and its Account Unit combinations")
  private static function buildLobCodeMap() : Map<TDIC_FinancePremiumLineTypeEnum, String> {
    return {
        TDIC_FinancePremiumLineTypeEnum.WC_EP_PREMIUM -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM_AUDIT -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM_AUDIT -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT_AUDIT -> LOB_CODE_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_SURCHARGES -> LOB_CODE_WC7WORKERSCOMPLINE,

        TDIC_FinancePremiumLineTypeEnum.CP_EP_PROPERTY -> LOB_CODE_CP_PROPERTY,
        TDIC_FinancePremiumLineTypeEnum.CP_EP_EQUIP_BREAKDOWN -> LOB_CODE_CP_EQUIP_BREAKDOWN,
        TDIC_FinancePremiumLineTypeEnum.CP_EP_GENERAL_LIAB -> LOB_CODE_CP_GENERAL_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_PROPERTY -> LOB_CODE_CP_PROPERTY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_EQUIP_BREAKDOWN -> LOB_CODE_CP_EQUIP_BREAKDOWN,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_GENERAL_LIAB -> LOB_CODE_CP_GENERAL_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_SURCHARGES -> LOB_CODE_CP_PROPERTY,


        TDIC_FinancePremiumLineTypeEnum.PL_EP_CYBER_LIAB -> LOB_CODE_PL_CYBER_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_EMPL_PRACTICES -> LOB_CODE_PL_EMP_PRACTICES,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_IDENITY_THEFT -> LOB_CODE_PL_IDENTITY_THEFT,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB -> LOB_CODE_PL_PROF_LIAB,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB_OCCUR -> LOB_CODE_PL_PROF_LIAB_OCCUR,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_REP_ENDORSEMENTS -> LOB_CODE_PL_REP_ENDORSEMENTS,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_CYBER_LIAB -> LOB_CODE_PL_CYBER_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_EMPL_PRACTICES -> LOB_CODE_PL_EMP_PRACTICES,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_IDENITY_THEFT -> LOB_CODE_PL_IDENTITY_THEFT,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB -> LOB_CODE_PL_PROF_LIAB,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB_OCCUR -> LOB_CODE_PL_PROF_LIAB_OCCUR,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_REP_ENDORSEMENTS -> LOB_CODE_PL_REP_ENDORSEMENTS,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_SURCHARGES -> LOB_CODE_PL_PROF_LIAB
    }
  }

  private static function buildLobLabelMap() : Map<TDIC_FinancePremiumLineTypeEnum, String> {
    return {
        TDIC_FinancePremiumLineTypeEnum.WC_EP_PREMIUM -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM_AUDIT -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM_AUDIT -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT_AUDIT -> LOB_LABEL_WC7WORKERSCOMPLINE,
        TDIC_FinancePremiumLineTypeEnum.WC_WP_SURCHARGES -> LOB_LABEL_WC7WORKERSCOMPLINE,

        TDIC_FinancePremiumLineTypeEnum.CP_EP_PROPERTY -> LOB_LABEL_CP_PROPERTY,
        TDIC_FinancePremiumLineTypeEnum.CP_EP_EQUIP_BREAKDOWN -> LOB_LABEL_CP_EQUIP_BREAKDOWN,
        TDIC_FinancePremiumLineTypeEnum.CP_EP_GENERAL_LIAB -> LOB_LABEL_CP_GENERAL_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_PROPERTY -> LOB_LABEL_CP_PROPERTY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_EQUIP_BREAKDOWN -> LOB_LABEL_CP_EQUIP_BREAKDOWN,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_GENERAL_LIAB -> LOB_LABEL_CP_GENERAL_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.CP_WP_SURCHARGES -> LOB_LABEL_CP_PROPERTY,


        TDIC_FinancePremiumLineTypeEnum.PL_EP_CYBER_LIAB -> LOB_LABEL_PL_CYBER_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_EMPL_PRACTICES -> LOB_LABEL_PL_EMP_PRACTICES,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_IDENITY_THEFT -> LOB_LABEL_PL_IDENTITY_THEFT,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB -> LOB_LABEL_PL_PROF_LIAB,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB_OCCUR -> LOB_LABEL_PL_PROF_LIAB_OCCUR,
        TDIC_FinancePremiumLineTypeEnum.PL_EP_REP_ENDORSEMENTS -> LOB_LABEL_PL_REP_ENDORSEMENTS,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_CYBER_LIAB -> LOB_LABEL_PL_CYBER_LIABILITY,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_EMPL_PRACTICES -> LOB_LABEL_PL_EMP_PRACTICES,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_IDENITY_THEFT -> LOB_LABEL_PL_IDENTITY_THEFT,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB -> LOB_LABEL_PL_PROF_LIAB,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB_OCCUR -> LOB_LABEL_PL_PROF_LIAB_OCCUR,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_REP_ENDORSEMENTS -> LOB_LABEL_PL_REP_ENDORSEMENTS,
        TDIC_FinancePremiumLineTypeEnum.PL_WP_SURCHARGES -> LOB_LABEL_PL_PROF_LIAB
    }
  }

  @Returns("Returns Map object of Jurisdiction state and its Account Unit combinations")
  private static function buildStateAcctUnitMap() : Map<String, String> {
    return   {
        typekey.Jurisdiction.TC_AK.Code -> STATE_AK,
        typekey.Jurisdiction.TC_AZ.Code -> STATE_AZ,
        typekey.Jurisdiction.TC_CA.Code -> STATE_CA,
        typekey.Jurisdiction.TC_GA.Code -> STATE_GA,
        typekey.Jurisdiction.TC_HI.Code -> STATE_HI,
        typekey.Jurisdiction.TC_ID.Code -> STATE_ID,
        typekey.Jurisdiction.TC_IL.Code -> STATE_IL,
        typekey.Jurisdiction.TC_MN.Code -> STATE_MN,
        typekey.Jurisdiction.TC_MT.Code -> STATE_MT,
        typekey.Jurisdiction.TC_NV.Code -> STATE_NV,
        typekey.Jurisdiction.TC_NJ.Code -> STATE_NJ,
        typekey.Jurisdiction.TC_NM.Code -> STATE_NM,
        typekey.Jurisdiction.TC_ND.Code -> STATE_ND,
        typekey.Jurisdiction.TC_OR.Code -> STATE_OR,
        typekey.Jurisdiction.TC_PA.Code -> STATE_PA,
        typekey.Jurisdiction.TC_TN.Code -> STATE_TN,
        typekey.Jurisdiction.TC_WA.Code -> STATE_WA
    }
  }

}