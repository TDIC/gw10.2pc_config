package tdic.pc.common.batch.finance.reports.util

enum TDIC_FinanceReportTypeEnum {
  WPRM, EPRM
}