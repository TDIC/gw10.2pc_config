package tdic.pc.common.batch.officialids

uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses gw.api.database.Query

uses java.lang.Exception
uses org.slf4j.LoggerFactory

class UnencryptOfficialIDsBatch extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${UnencryptOfficialIDsBatch.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UNENCRYPTOFFICIALIDS_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var contact : Contact;
    var msg : String;

    // Get OfficialIDs where the unencrypted value may not match the encrypted value.
    // Returns all rows with the exception of rows where both values are null.
    var query = Query.make(OfficialID);
    query.or (\ orCriteria -> {
      orCriteria.compare("OfficialIDValue",           NotEquals, null);
      orCriteria.compare("OfficialIDValueUnenc_TDIC", NotEquals, null);
    });

    _logger.info (_LOG_TAG + "Querying for OfficialIDs.");
    var results = query.select();
    _logger.debug (_LOG_TAG + "Found " + results.Count + " potential records.");

    // Don't set Operations Expected, because we don't know yet how many records need to be updated.
    // OperationsExpected = results.Count;

    for (officialID in results) {
      // Check the entity values to see if they are different.  Values are automatically decrypted by the entity.
      if (officialID.OfficialIDValue != officialID.OfficialIDValueUnenc_TDIC) {
        if (_logger.isTraceEnabled()) {
          contact = officialID.Contact;
          msg = "Contact " + contact.DisplayName + " (ID = " + contact.ID + ") - "
              + officialID.OfficialIDType.DisplayName + " OfficialID (ID = " + officialID.ID
              + ") - Unencrypted value does not equal the encrypted value.";
          _logger.trace (_LOG_TAG + msg);
        }

        try {
          // Copy encrypted value to unencrypted value
          Transaction.runWithNewBundle (\bundle -> {
            officialID = bundle.add(officialID);
            officialID.OfficialIDValueUnenc_TDIC = officialID.OfficialIDValue;
          }, "iu");
        } catch (e : Exception) {
          _logger.error (_LOG_TAG + "Exception occurred while updating OfficialID (ID = " + officialID.ID + "): "
                            + e.toString());
          incrementOperationsFailed();
        }
        incrementOperationsCompleted();
      }
    }

    _logger.info (_LOG_TAG + "Updated " + (OperationsCompleted - OperationsFailed) + " OfficialIDs.  "
                    + OperationsFailed + " updates failed.");
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return false;
  }
}