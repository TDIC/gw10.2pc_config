package tdic.pc.common.batch.as400

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.DateUtil
uses gw.processes.BatchProcessBase
uses gw.api.database.Query
uses tdic.util.csv.CSVFile

uses java.util.ArrayList
uses java.io.IOException
uses com.tdic.util.misc.EmailUtil
uses java.lang.Exception
uses gw.api.database.IQueryBeanResult
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: PraneethK
 * Date: 1/10/17
 * Time: 5:28 AM
 *
 * AS400 batch process implementation class.
 * This process is designed for read all the policies and write the maximum policy period data to the following csv format texts files
 *  - Policy file
 *  - Policy Owner file
 */
class TDIC_AS400BatchProcess extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("TDIC_AS400")

  /**
   * Property with the policy file name generated to AS400 property
   */
  private static final var AS400FEED_POLICYFILE_NAME_PROPERTY = "as400feed.policyfile.name"

  /**
   *  Property with the policy Owner file name generated to AS400 property
   */
  private static final var AS400FEED_POLICYOWNERFILE_NAME_PROPERTY = "as400feed.policyownerfile.name"

  /**
   *  Property with the relative route to place the files generated to AS400 property
   */
  private static final var AS400FEED_FILESDIRECTORY_NAME_PROPERTY = "as400feed.filesdirectory.name"

  /**
   * Policy file name generated to AS400 property
   */
  private var pFileName : String

  /**
   * Policy Owner file name generated to AS400 property
   */
  private var pFileOwnerName : String

  /**
   * Relative route to place the files generated to AS400 property
   */
  private var pFilesDirectory : String

  /**
   * Policy file generated to AS400 property
   */
  private var policyFile : CSVFile

  /**
   * Policy Owner file generated to AS400 property
   */
  private var policyOwnerFile : CSVFile

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: InfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "PCInfraIssueNotificationEmail"

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for General Ledger batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "AS400 Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  construct() {
    super(BatchProcessType.TC_AS400BATCH_TDIC)
  }

  override function doWork(): void {
    _logger.debug("TDIC_AS400BatchProcess#doWork() - Entering")
    _logger.debug("TDIC_AS400BatchProcess#doWork() - Opening files")

    try{
      policyFile = new CSVFile(pFilesDirectory + pFileName)
      policyOwnerFile = new CSVFile(pFilesDirectory + pFileOwnerName)

      //Get all the policies In force or cancelled
      var policies = getAllPoliciesToWrite()
      OperationsExpected = policies.Count
      if(policies.Count > 0){
        _logger.debug("TDIC_AS400BatchProcess#doWork() - Writing files")
        //All the policies in force are selected
        for(policy in policies){
          if (TerminateRequested) {
            _logger.debug("TDIC_AS400BatchProcess#doWork() - TerminateRequested received...")

            _logger.debug("TDIC_AS400BatchProcess#requestTermination() - Deleting files")
            policyFile.delete()
            policyOwnerFile.delete()

            break
          }

          _logger.debug("TDIC_AS400BatchProcess#doWork() - writePolicyFile started for Policy: ${policy}")
          var isWriteSuccess = writePolicyFile(policy)
          if(isWriteSuccess) {
            writePolicyOwnerFile(policy)
          }
          _logger.debug("TDIC_AS400BatchProcess#doWork() - writePolicyFile completed for Policy: ${policy}")

          incrementOperationsCompleted()
        }
      } else {
        _logger.debug("TDIC_AS400BatchProcess#doWork() - There are no policies in force or cancelled to be written.")
      }
    } catch(ioe : IOException) {
      incrementOperationsFailed()
      _logger.warn("IOException caught in batch process: " + ioe.LocalizedMessage, ioe)
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "Error writting files to AS400: " + ioe.Message)
      throw new IOException("Error writting files to AS400: " + ioe.Message)
    } catch(e: Exception) {
      incrementOperationsFailed()
      _logger.warn("Exception caught in batch process: " + e.LocalizedMessage, e)
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "AS400 batch failed due to an unexpected error:" + e.Message)
      throw new Exception("Error writting files to AS400: " + e.Message)
    }finally{
      _logger.debug("TDIC_AS400BatchProcess#doWork() - Closing files")
      if(policyFile!= null){
        policyFile.close()
      }

      if(policyOwnerFile!= null){
        policyOwnerFile.close()
      }
    }
    _logger.debug("TDIC_AS400BatchProcess#doWork() - Exiting")
  }

  /**
   *  Method that writes the policy information required to Policy File:
   *  - Policy#
   *  - Policy term
   *  - Policy start date
   *  - Policy Status
   *  - Policy effective date
   *  - Policy expiration date
   *  - Organization type
   *  - Premium
   *  - Cancel date
   *  - Cancel reason
   */
  @Param("policy", "Policy period where the information is taken")
  @Param("policyFile", "CSV file to write")
  private function writePolicyFile(pPeriod: PolicyPeriod):boolean {
    _logger.debug("TDIC_AS400BatchProcess#writePolicyFile() - Entering")
    var account = pPeriod.Policy.Account

    var recordPolicyFile = new ArrayList<String>()
    //Policy#
    recordPolicyFile.add(pPeriod.PolicyNumber)
    //Policy term
    recordPolicyFile.add(pPeriod.TermNumber as String)
    //Policy start date
    recordPolicyFile.add(pPeriod.Policy.OriginalEffectiveDate as String)
    //Policy Status
    recordPolicyFile.add(pPeriod.PeriodDisplayStatus)
    //Policy effective date
    recordPolicyFile.add(pPeriod.PeriodStart as String)
    //Policy expiration date
    recordPolicyFile.add(pPeriod.PeriodEnd as String)
    //Organization type
    recordPolicyFile.add(pPeriod.getSlice(pPeriod.EditEffectiveDate).PolicyOrgType_TDIC.DisplayName)
    //Premium
    recordPolicyFile.add(pPeriod.TotalPremiumRPT.Amount as String)
    //Cancel date
    recordPolicyFile.add(pPeriod.CancellationDate as String)
    //Cancel reason
    recordPolicyFile.add(pPeriod.Cancellation.CancelReasonCode?.Description)

    policyFile.writeRow(recordPolicyFile)
    _logger.debug("TDIC_AS400BatchProcess#writePolicyFile() - Exiting")
    return true
  }

  /**
   *  Method that writes the policy information required to Policy Owner File for each policy owner and policy owner officer:
   *  - Policy#
   *  - Policy term
   *  - Policy owner ADA#
   *  - Policy owner Name
   *  - Role
   *  - FEIN#
   */
  @Param("policy", "Policy period where the information is taken")
  @Param("policyOwnerFile", "CSV file to write")
  private function writePolicyOwnerFile(pPeriod: PolicyPeriod) {
    _logger.debug("TDIC_AS400BatchProcess#writePolicyOwnerFile() - Entering")
    var account = pPeriod.Policy.Account
    pPeriod = pPeriod.getSlice(pPeriod.EditEffectiveDate)

    //First the is witten the policy owner
    writePolicyOwnerInFile(pPeriod.PrimaryNamedInsured.ContactDenorm, pPeriod)

    //Then all the policy owner officers with ownership greater than 0 are witten
    if(pPeriod.WC7Line!= null){
      for(policyOwner in pPeriod.WC7Line.WC7PolicyOwnerOfficers.where( \ policyOwnerOfficer -> policyOwnerOfficer.WC7OwnershipPct > 0)) {
        if(policyOwner.ContactDenorm != pPeriod.PrimaryNamedInsured.ContactDenorm) {
          writePolicyOwnerInFile(policyOwner.ContactDenorm, pPeriod)
        }
      }
    }

    _logger.debug("TDIC_AS400BatchProcess#writePolicyOwnerFile() - Exiting")
  }

  /**
   *  Auxiliar method that writes the policy information following information about one contact:
   *  - Policy#
   *  - Policy term
   *  - Policy owner ADA#
   *  - Policy owner Name
   *  - Role
   *  - FEIN#
   */
  @Param("contact", "contact where the information is taken")
  @Param("pPeriod", "policy period where the information is taken")
  @Param("policyOwnerFile", "CSV file to write")
  private function writePolicyOwnerInFile(contact: Contact, pPeriod: PolicyPeriod) {
    _logger.debug("TDIC_FeedPivotalAS400BatchProcess#writePolicyOwneInFile() - Entering")

    var recordPolicyOwnerFile = new ArrayList<String>()
    //Policy#
    recordPolicyOwnerFile.add(pPeriod.PolicyNumber)
    //Policy term
    recordPolicyOwnerFile.add(pPeriod.TermNumber as String)
    //Policy owner ADA#
    recordPolicyOwnerFile.add(contact typeis Person ? contact.ADANumberOfficialID_TDIC : null)
    //Policy owner Name
    recordPolicyOwnerFile.add(contact.DisplayName)
    //Roles
    recordPolicyOwnerFile.add(contact.getPolicyPeriodRoles_TDIC(pPeriod))
    //FEIN
    recordPolicyOwnerFile.add(contact.FEINOfficialID)

    policyOwnerFile.writeRow(recordPolicyOwnerFile)

    _logger.debug("TDIC_FeedPivotalAS400BatchProcess#writePolicyOwneInFile() - Exiting")
  }
  /**
   *  Method that returns all the WC7 Workers Comp policies in force or cancelled or expired
   */
  @Returns("Returns all the policies in force")
  private function getAllPoliciesToWrite(): IQueryBeanResult<PolicyPeriod> {
    _logger.debug("TDIC_AS400BatchProcess#getAllPoliciesToWrite() - Entering")
    var currentDate = DateUtil.currentDate()
    var resultPolicies = Query.make(PolicyPeriod)
        .compare("Status", Equals, typekey.PolicyPeriodStatus.TC_BOUND)
        .compare("MostRecentModel", Equals, true)
        .join("PolicyTerm")
        .compare("MostRecentTerm", Equals, true)
        .join("Policy")
        .compare("ProductCode", Equals, "WC7WorkersComp").select()

    _logger.debug("TDIC_AS400BatchProcess#getAllPoliciesToWrite() - Exiting")
    return resultPolicies
  }

  /**
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  override function checkInitialConditions() : boolean {
    _logger.debug("TDIC_AS400BatchProcess#checkInitialConditions() - Entering")

    pFileName = PropertyUtil.getInstance().getProperty(AS400FEED_POLICYFILE_NAME_PROPERTY)
    pFileOwnerName = PropertyUtil.getInstance().getProperty(AS400FEED_POLICYOWNERFILE_NAME_PROPERTY)
    pFilesDirectory = PropertyUtil.getInstance().getProperty(AS400FEED_FILESDIRECTORY_NAME_PROPERTY)
    _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)

    if(pFileName != null and pFileOwnerName != null and pFilesDirectory!= null and _failureNotificationEmail!= null){
      _logger.debug("TDIC_AS400BatchProcess#checkInitialConditions() - Exiting")
      return Boolean.TRUE
    } else {
      incrementOperationsFailed()
      _logger.debug("TDIC_AS400BatchProcess#checkInitialConditions() - Properties with the files names of folder name not defined in GWINT")

      _logger.debug("TDIC_AS400BatchProcess#checkInitialConditions() - Exiting")
      return Boolean.FALSE
    }
  }

  /**
   * This is override function to handle termination request during batch process. The partially written files are
   * removed.
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    _logger.debug("TDIC_AS400BatchProcess#requestTermination() - Entering")
    super.requestTermination()
    _logger.debug("TDIC_AS400BatchProcess#requestTermination() - Exiting")
    return Boolean.TRUE
  }

}