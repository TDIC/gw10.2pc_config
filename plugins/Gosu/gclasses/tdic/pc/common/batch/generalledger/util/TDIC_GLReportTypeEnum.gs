package tdic.pc.common.batch.generalledger.util

/**
 * US627
 * 11/24/2014 Kesava Tavva
 *
 * Defines enumeration values for GL report types i.e written premium, earned premium
 *
 */
enum TDIC_GLReportTypeEnum {
  WPRM, EPRM
}