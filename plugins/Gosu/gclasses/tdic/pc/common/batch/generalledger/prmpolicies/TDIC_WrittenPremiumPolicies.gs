package tdic.pc.common.batch.generalledger.prmpolicies

uses org.slf4j.Logger
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLineTypeEnum
uses java.util.Map
uses java.util.HashMap
uses gw.api.database.Query
uses java.util.Date
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLinesInterface
uses gw.api.database.IQueryBeanResult
uses org.slf4j.LoggerFactory

/**
 * US627
 * 11/26/2014 Kesava Tavva
 *
 * This class is to calculate aggregated cost values for all premium line types
 * i.e Premium, Expense constant, Terrorism premium and state surcharges from the qualified
 * policy periods for a given batch run period.
 */
class TDIC_WrittenPremiumPolicies extends TDIC_PremiumPoliciesBase {
  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  private static final var CLASS_NAME : String = "TDIC_WrittenPremiumPolicies"

  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_PremiumLinesInterface){
    super(periodStartDate, periodEndDate, prmLines)
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns aggregated cost amounts corresponding to each Jurisdiction from the qualified
   * policy periods for a given batch run period.
   */
  @Returns("Map object of aggregate amounts for each Jurisdiction")
  override function getPremiumAmounts(): Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    return buildPremiums()
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function retrieves all qualified policy periods for the batch period and aggregate
   * costs based on cost type.
   */
  @Returns("Map object of aggregate amounts for each Jurisdiction")
  private function buildPremiums() : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    var logPrefix = "${CLASS_NAME}#buildPremiums()"
    _logger.debug("${logPrefix} - Entering")
    var premiums = new HashMap<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>()
    //BatchPeriods
    var batchPSD = getBatchPeriodStartDate()
    var batchPED = getBatchPeriodEndDate(batchPSD)
    //Retrieve PolicyPeriods
    var policyPeriods = retrievePolicyPeriods(batchPSD, batchPED)
    //Retrieve PolicyPeriods
    var auditPolicyPeriods = retrieveAuditPolicyPeriods(batchPSD, batchPED)
    var periods = policyPeriods.concat(auditPolicyPeriods?.toCollection())
    //Calculate Premiums from Policy Periods
    var prmLineTypeMap : Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>
    _logger.info("${logPrefix} - PolicyPeriod; ModelDate; EditEffectiveDate; PeriodEnd; CancellationDate")
    for(pp in periods) {
      _logger.info("${logPrefix} - ${pp}; ${pp.ModelDate}; ${pp.EditEffectiveDate}; ${pp.PeriodEnd}; ${pp.CancellationDate}")
      for(pl in pp.RepresentativePolicyLines) {
        if (pl typeis productmodel.WC7Line) {
          for(jur in pl.RepresentativeJurisdictions){
            var wc7Transactions = pl.WC7Transactions?.where(\wc7Trans -> (wc7Trans.WC7Cost.JurisdictionState == jur.Jurisdiction) == true)
            //By state
            prmLineTypeMap = premiums.get(jur.Jurisdiction)
            if (prmLineTypeMap == null) {
              prmLineTypeMap = new HashMap<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>()
              premiums.put(jur.Jurisdiction, prmLineTypeMap)
            }
            for(wc7Trans in wc7Transactions){
              addAmountToPremiumLineType(pp.Job, wc7Trans, prmLineTypeMap)
            }
          }
        }
      }
    }
    _logger.debug("${logPrefix} - Exiting")
    return premiums
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function adds cost amounts to corresponding premium line type aggregated amounts.
   */
  @Param("job","Job that represents transaction type")
  @Param("wcTrans", "WC7Transaction lines for each Policy period's policy line type")
  @Param("prmLineTypeMap","Map object that represents aggregated amounts and units")
  private function addAmountToPremiumLineType(job : Job, wc7Trans : WC7Transaction, prmLineTypeMap : Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>) {
    var logPrefix = "${CLASS_NAME}#addAmountToPremiumLineType(job, wc7Trans, prmLineTypeMap)"
    _logger.debug("${logPrefix} - Entering")
    if(wc7Trans == null || prmLineTypeMap == null)
      return
    var prmLineType =  getPremiumLineType(wc7Trans.WC7Cost.CalcOrder, job)
    _logger.debug("${logPrefix} - ${wc7Trans.WC7Cost.CalcOrder} - ${prmLineType}")
    if(prmLineType != null)
    {
      var prmObject = prmLineTypeMap.get(prmLineType)
      if(prmObject == null) {
        prmObject = new TDIC_PremiumObject(prmLineType)
        prmLineTypeMap.put(prmLineType, prmObject)
      }
      prmObject.addToAmount(job,wc7Trans.Amount.Amount)
    }
    _logger.debug("${logPrefix} - Exiting")
  }

  /**
   * US627
   * 12/03/2014 Kesava Tavva
   *
   * This function finds Premium line type for each cost type associated with WC7Transaction lines
   */
  @Param("jurCostType","WC7JurisdictionCostType")
  @Returns("TDIC_PremiumLineTypeEnum object")
  private function getPremiumLineType(jurCostType : WC7JurisdictionCostType) : TDIC_PremiumLineTypeEnum {
    var logPrefix = "${CLASS_NAME}#getPremiumLineType(jurCostType)"
    _logger.debug("${logPrefix} - Entering")
    var premLineType : TDIC_PremiumLineTypeEnum
    switch(jurCostType) {
      case typekey.WC7JurisdictionCostType.TC_TERRORPREM :
            premLineType = TDIC_PremiumLineTypeEnum.TERRORISM
            break
      case typekey.WC7JurisdictionCostType.TC_EXPENSECONST :
            premLineType = TDIC_PremiumLineTypeEnum.EXP_CONSTANT
            break
      case typekey.WC7JurisdictionCostType.TC_TAX :
            premLineType = TDIC_PremiumLineTypeEnum.STATE_ASSMNTS
            break
      default :
            premLineType = TDIC_PremiumLineTypeEnum.PREMIUM
            break
    }
    _logger.debug("${logPrefix} - Exiting")
    return premLineType
  }

  /**
   * US627
   * 12/10/2014 Kesava Tavva
   *
   * This function finds Premium line type based on calcOrder associated with each WC7Transaction line
   */
  @Param("calcOrder","int value of CalcOrder")
  @Returns("TDIC_PremiumLineTypeEnum object")
  private function getPremiumLineType(calcOrder : int, job: Job) : TDIC_PremiumLineTypeEnum {
    var premLineType : TDIC_PremiumLineTypeEnum
    if(calcOrder <=CALC_ORDER_PRM)
      premLineType = (job.Subtype == typekey.Job.TC_AUDIT) ? TDIC_PremiumLineTypeEnum.PREMIUM_AUDIT : TDIC_PremiumLineTypeEnum.PREMIUM
    else if(calcOrder == CALC_ORDER_EXP_CONST)
      premLineType = (job.Subtype == typekey.Job.TC_AUDIT) ? TDIC_PremiumLineTypeEnum.EXP_CONSTANT_AUDIT : TDIC_PremiumLineTypeEnum.EXP_CONSTANT
    else if(calcOrder == CALC_ORDER_TERRORISM)
      premLineType = (job.Subtype == typekey.Job.TC_AUDIT) ? TDIC_PremiumLineTypeEnum.TERRORISM_AUDIT : TDIC_PremiumLineTypeEnum.TERRORISM
    else if(calcOrder >= CALC_ORDER_SURCHARGES)
      premLineType = TDIC_PremiumLineTypeEnum.STATE_ASSMNTS

    return premLineType
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function builds query to retrieve all qualified policy periods for a given batch period
   * start and end dates and returns list of those policy periods.
   */
  @Param("batchPSD","Batch period start date")
  @Param("batchPED","Batch period end date")
  @Returns("Returns list of qualified policy periods")
  private function retrievePolicyPeriods(batchPSD : Date, batchPED : Date): IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrievePolicyPeriods(batchPSD, batchPED)"
    _logger.debug("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.compare("ModelDate",NotEquals,null)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE).compare(PolicyLine#EffectiveDate,Equals,null)

    ppQuery.or(\ or1 -> {
      or1.between("ModelDate",batchPSD,batchPED)
      or1.and(\ andE ->{
        andE.between("EditEffectiveDate",batchPSD,batchPED)
        andE.compare("ModelDate",LessThanOrEquals,batchPED)
      })
      or1.and(\ andC ->{
        andC.compare("CancellationDate",NotEquals,null)
        andC.between("CancellationDate",batchPSD,batchPED)
      })
    })
    ppQuery.and(\andE ->{
      andE.or(\or2 ->{
        or2.compare("EditEffectiveDate",LessThanOrEquals,batchPED)
        or2.compare("PeriodEnd",LessThanOrEquals, batchPSD)
      })
    })
    _logger.debug("${logPrefix}- Retrieving Policy Periods complete for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    _logger.debug("${logPrefix} - Exiting")
    return ppQuery.select()
  }

  private function retrieveAuditPolicyPeriods(batchPSD : Date, batchPED : Date): IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrieveAuditPolicyPeriods(batchPSD, batchPED)"
    _logger.debug("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_AUDITCOMPLETE)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE)
        .compare(PolicyLine#EffectiveDate,Equals,null)
    ppQuery.join(PolicyPeriod#Job).between(Job#CloseDate,batchPSD,batchPED)
    _logger.debug("${logPrefix}- Retrieving Policy Periods complete for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    _logger.debug("${logPrefix} - Exiting")
    return ppQuery.select()
  }
}