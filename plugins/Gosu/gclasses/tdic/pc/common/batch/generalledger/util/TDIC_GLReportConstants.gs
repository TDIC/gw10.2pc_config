package tdic.pc.common.batch.generalledger.util

uses java.text.SimpleDateFormat

/**
 * US627
 * 12/01/2014 Kesava Tavva
 *
 * This class is to define constants related to GL report batch process
 */
class TDIC_GLReportConstants {
  protected static final var FILE_PATH_PROP_FIELD : String = "gl.pc.outputdir"
  public static final var DATE_FORMAT : SimpleDateFormat  = new SimpleDateFormat("MM/dd/yyyy");
  public static final var FILE_SUFFIX_DATE_FORMAT : SimpleDateFormat = new SimpleDateFormat("MM-dd-yyyy-HHmmss")
  public static final var WPM_FILE_NAME : String = "GW-LGL-WPM"
  public static final var EPM_FILE_NAME : String = "GW-LGL-EPM"

  public static final var EMAIL_TO_PROP_FIELD : String = "PCGLNotificationEmail"
  public static final var EMAIL_RECIPIENT : String = "PCInfraIssueNotificationEmail"
}