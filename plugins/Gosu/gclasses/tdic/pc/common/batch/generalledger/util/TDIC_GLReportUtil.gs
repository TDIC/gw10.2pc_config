package tdic.pc.common.batch.generalledger.util

uses com.tdic.util.properties.PropertyUtil

uses java.util.Calendar
uses java.util.Date

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This class defines all utility functions required for processing GL reports.
 *
 */
class TDIC_GLReportUtil {

  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * Function to get external path details from int database properties by using cache manager.
   *
   */
  @Returns("String object, external file path location for writing GL report files")
  public static function getFilePath() : String {
    return PropertyUtil.getInstance().getProperty(TDIC_GLReportConstants.FILE_PATH_PROP_FIELD)
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function returns batch period end date for generating GL reports
   * based on batch period start date.
   *
   */
  @Returns("Date object that represents Batch period end date")
  public static function getBatchRunPeriodEndDate(startDate : Date) : Date {
    return startDate.addMonths(1).addDays(-1)
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function returns formatted current date and time to be used as suffix in file name
   * as per GL batch file name structure requirements expected by Lawson.
   *
   */
  @Returns("String object that represents formatted current date and time as per Lawson file name requirements.")
  public static function getFileNameSuffix() : String {
    return TDIC_GLReportConstants.FILE_SUFFIX_DATE_FORMAT.format(Calendar.getInstance().getTime())
  }

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * This function returns Batch period start date for generating GL reports
   * based on arguments received in batch command.
   *
   */
  @Param("month", "Month of Batch run period")
  @Param("year", "Year of Batch run period")
  @Returns("Date object that represents Batch period start date")
  public static function getBatchRunPeriodStartDate(month : int, year : int) : Date {
    var calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month-1)
    calendar.set(Calendar.DATE, 1)
    return calendar.getTime().trimToMidnight()
  }
}

