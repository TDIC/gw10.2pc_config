package tdic.pc.common.batch.generalledger

uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportTypeEnum
uses com.tdic.util.misc.EmailUtil
uses java.lang.Exception
uses java.lang.System

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This class overrides dowork() method of batch process and initiates batch process
 * for Written and Earned premium General Ledger reporting.
 */
class TDIC_GLReportBatchProcess extends TDIC_GLReportBatchProcessBase {
  private static final var CLASS_NAME : String = "TDIC_GLReportBatchProcess"

  construct(args : Object[]){
    super(args)
  }

  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * This function overrides dowork() method to generate General Ledger reports
   * for both Written and Earned premium files.
   */
  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.debug("${logPrefix} - Entering")
    try{

      _logger.info("${logPrefix} started for the Period: ${PeriodStartDate} to ${PeriodEndDate}")
      var batchStartTime = System.currentTimeMillis()
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before generateGeneralLedgerReport(WPRM)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      generateGeneralLedgerReport(TDIC_GLReportTypeEnum.WPRM)
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before generateGeneralLedgerReport(EPRM)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      generateGeneralLedgerReport(TDIC_GLReportTypeEnum.EPRM)
      EmailUtil.sendEmail(EmailTo, getEmailSubject(Boolean.TRUE), "PC GLInterface batch completed successfully for the Period: ${PeriodStartDate} to ${PeriodEndDate}.")
      _logger.info("${logPrefix}- Total time taken for completing this batch process: ${System.currentTimeMillis()-batchStartTime}(ms)")
      _logger.info("${logPrefix}- completed for the Period: : ${PeriodStartDate} to ${PeriodEndDate}")
    }catch(e: Exception){
      EmailUtil.sendEmail(EmailRecipient, getEmailSubject(Boolean.FALSE), "PC GLInterface batch completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}.\n${e.StackTraceAsString}")
      throw new Exception("${logPrefix}- completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}",e)
    }
    _logger.debug("${logPrefix}- Exiting")
  }

  /**
   * US627
   * 11/22/2014 Kesava Tavva
   *
   * This function delegates following steps to generate Generate Ledger reports.
   *        -Aggregate premiums from qualified policy periods for the given batch period
   *        -Generate respective premium lines for each of batch reprot type
   *        -Write premium lines to external file
   */
  @Param("reportType", "Type of report i.e Written or Earned premium report")
  private function generateGeneralLedgerReport(reportType : TDIC_GLReportTypeEnum) {
    var logPrefix = "${CLASS_NAME}#generateGeneralLedgerReport(reportType)"
    _logger.debug("${logPrefix}- Entering")
    _logger.info("${logPrefix}- ${reportType} started for the Period: ${PeriodStartDate} to ${PeriodEndDate}")

    var batchStartTime = System.currentTimeMillis()

    var prmPolicyProcessor = getPremiumPoliciesProcessor(reportType)
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before getPremiumAmounts(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }

    var premiums = prmPolicyProcessor.getPremiumAmounts()
    _logger.info("${logPrefix}- ${reportType} premiums extraction completed. Count:${premiums?.size()}")
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before generatePremiumLines(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }
    //prepare list of lines to be written to external file
    var prmLines = prmPolicyProcessor.getPremiumLines().generatePremiumLines(premiums)
    OperationsExpected = OperationsExpected+prmLines?.size()
    _logger.info("${logPrefix}- ${reportType} premiums lines construction completed. Count:${prmLines?.size()}")
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before writeToFile(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }

    writeToFile(reportType, prmLines)
    _logger.info("${logPrefix}- ${reportType} writing premium lines to a file is completed.")

    _logger.info("${logPrefix}- Total time taken for completing ${reportType}: ${System.currentTimeMillis()-batchStartTime}(ms)")
    _logger.info("${logPrefix}- ${reportType} completed for the Period: ${PeriodStartDate} to ${PeriodEndDate}")
    _logger.debug("${logPrefix}- Exiting")
  }

}