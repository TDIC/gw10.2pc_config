package tdic.pc.common.batch.generalledger.prmlines

/**
 * US627
 * 11/24/2014 Kesava Tavva
 *
 * Defines enumeration values for Premium line types reported in premium batch files
 *
 */
enum TDIC_PremiumLineTypeEnum {
  TERRORISM, EXP_CONSTANT, PREMIUM,TERRORISM_AUDIT, EXP_CONSTANT_AUDIT, PREMIUM_AUDIT,
  STATE_ASSMNTS, PRM_RCV_ACCOUNT, UNEARNED_PREMIUM, CHG_UNEARNED_PREMIUM
}