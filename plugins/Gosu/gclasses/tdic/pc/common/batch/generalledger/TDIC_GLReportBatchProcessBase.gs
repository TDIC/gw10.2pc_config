package tdic.pc.common.batch.generalledger

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.upgrade.Coercions
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumPoliciesInterface
uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportConstants
uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportTypeEnum
uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportUtil

uses java.io.File
uses java.io.PrintWriter

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This abstract class is a batch class for GL report batch which provides common
 * functions such as building file name, writing premium lines to external file etc.
 */
abstract class TDIC_GLReportBatchProcessBase extends BatchProcessBase {

  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  private var _batchRunPeriodStartDate : Date as readonly PeriodStartDate
  private var _batchRunPeriodEndDate : Date as readonly PeriodEndDate
  private var _emailTo : String as readonly EmailTo
  private var _emailRecipient : String as readonly EmailRecipient
  public static final var TERMINATE_SUB : String = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-PC GLInterface Batch Job Terminated"
  protected var _filePath : String

  private static var CLASS_NAME : String = "TDIC_GLReportBatchProcessBase"

  construct(args : Object[]){
    super(BatchProcessType.TC_GLINTERFACE_TDIC)
    initialize(args)
  }

  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * This is override function to handle termination request during batch process
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns premium policies processor based on General Ledger Report type.
   */
  @Param("reportType", "Type of report i.e Written or Earned premium report")
  @Returns("Returns TDIC_PremiumPoliciesInterface")
  protected function getPremiumPoliciesProcessor(reportType : TDIC_GLReportTypeEnum) : TDIC_PremiumPoliciesInterface {
    return TDIC_GLReportFactory.getPremiumPoliciesProcessor(reportType, PeriodStartDate, PeriodEndDate)
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function writes all premium lines to external file in a directory path
   * defined in int database.
   *
   */
  @Param("reportType","represents Written or Earned premium type")
  @Param("premLines","List of premium line to be written to external file")
  @Throws(Exception,"Throws exception when unable create a file or write to a file")
  protected function writeToFile(reportType : TDIC_GLReportTypeEnum, premLines : List<String>) {
    var logPrefix = "${CLASS_NAME}#writeToFile(reportType, premLines)"
    _logger.debug("${logPrefix}- Entering")
    if(!new File(_filePath).exists()){
      EmailUtil.sendEmail(EmailRecipient, "Unable to access path on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to access path:" + _filePath + ": The Directory May Not Exist or There is an Error in network Connection.")
      throw new Exception("Cannot Access ${_filePath}. The Directory May Not Exist or There is an Error in network Connection")
    } else {
      _logger.info("$${logPrefix}- Access to ${_filePath} is Successful.")
    }
    var flatFile:PrintWriter
    try{
      _logger.debug("${logPrefix}- Writing premium details to file started.")
      flatFile = new PrintWriter(getFileName(reportType))     //Open flat file for writing
      premLines?.each( \ elt -> {
        incrementOperationsCompleted()
        flatFile.println(elt)
      })
      _logger.debug("${logPrefix}- Writing premium details to file completed.")
    }catch(var e:Exception){
      OperationsFailed = OperationsFailed + premLines?.size()
      EmailUtil.sendEmail(EmailRecipient, "Unable to write premium lines to file on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to write premium lines to a file ${_filePath}" + e.LocalizedMessage)
      throw new Exception("Exception occurred while writing Premium lines to a file ${_filePath}",e)
    } finally{
      if(flatFile!=null) flatFile.close()
    }
    _logger.debug("${logPrefix}- Exiting")
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function builds file name with the format specified by Lawson.
   *
   */
  @Param("reportType","represents Written or Earned premium type")
  @Returns("Returns General Ledger reporting file name as String Object")
  @Throws(Exception,"Throws exception when unknown report type received.")
  private function getFileName(reportType : TDIC_GLReportTypeEnum) : String {
    switch(reportType){
      case TDIC_GLReportTypeEnum.WPRM :
        return "${_filePath}${TDIC_GLReportConstants.WPM_FILE_NAME}-${TDIC_GLReportUtil.getFileNameSuffix()}.GLT"
      case TDIC_GLReportTypeEnum.EPRM :
          return "${_filePath}${TDIC_GLReportConstants.EPM_FILE_NAME}-${TDIC_GLReportUtil.getFileNameSuffix()}.GLT"
        default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * This function initializes FileName, start and end periods to generate GL report for premiums
   */
  @Param("args","Argugments received from batch command")
  private function initialize(args : Object[]){
    _filePath = TDIC_GLReportUtil.getFilePath()
    if(args == null || args?.Count == 0){
      _batchRunPeriodStartDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(gw.api.util.DateUtil.currentDate().MonthOfYear-1, gw.api.util.DateUtil.currentDate().YearOfDate.intValue())
    } else {
      validateArguments(args)
      _batchRunPeriodStartDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(Coercions.makePIntFrom(args[0]), Coercions.makePIntFrom(args[1]))
    }
    _batchRunPeriodEndDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(_batchRunPeriodStartDate)
  }

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * This function validates arguments received as part of batch command. Expected values are
   *  args[0] - format: <mm>; valid values: 01,02...12
   *  args[1] - format: <yyyy>; 4 digit year
   */
  @Param("args","Argugments received from batch command")
  @Throws(Exception,"Exception, details about invalid arguments received")
  private function validateArguments(args : Object[]) {
    var logPrefix = "${CLASS_NAME}#validateArguments(reportType, premLines)"
    if(args.Count == 2){
      try{
        var month: int = args[0].toString().toInt()
        var year: int = args[1].toString().toInt()
        _logger.info("${logPrefix}- Arguments received are ${month}, ${year}")
        return
      }catch(e : Exception){
        throw new Exception("Invalid arguments (${args[0]}, ${args[1]}) received for batch run. Expected batch command arguments are <mm>(2 digit month. Valid values 01,02,..12) and <yyyy>(4 digit year).")
      }
    } else {
      throw new Exception("Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year).")
    }
  }

  /**
   * US627
   * 01/09/2015 Kesava Tavva
   *
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(TDIC_GLReportConstants.EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_GLReportConstants.EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(TDIC_GLReportConstants.EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_GLReportConstants.EMAIL_RECIPIENT}' from integration database.")
    }
    _logger.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  /**
   * US627
   * 01/09/2015 Kesava Tavva
   *
   * Builds Email Subject for either batch failure or success
   */
  @Param("flag", "Success or Failure flag")
  @Returns("String, Email subject message")
  protected function getEmailSubject(flag : Boolean) : String {
    var subject : StringBuffer = new StringBuffer()
    if(flag){
      subject.append("SUCCESS::${gw.api.system.server.ServerUtil.ServerId}-PC GLInterface Batch Job Completed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }else{
      subject.append("FAILURE::${gw.api.system.server.ServerUtil.ServerId}-PC GLInterface Batch Job Failed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }
    return subject.toString()
  }
}