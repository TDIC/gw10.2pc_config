package tdic.pc.common.batch.generalledger.prmlines

uses org.slf4j.Logger
uses java.lang.Integer
uses java.lang.StringBuilder
uses java.math.BigDecimal
uses java.text.SimpleDateFormat
uses java.util.Calendar
uses java.util.Date
uses java.lang.RuntimeException
uses org.slf4j.LoggerFactory

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This abstract class is to define common functions that are needed to generate premium lines
 * for both Written and Earned premium batch files.
 *
 */
abstract class TDIC_PremiumLinesBase implements TDIC_PremiumLinesInterface {
  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  private static var CLASS_NAME : String = "TDIC_PremiumLinesBase"
  private static final var ZERO_DOUBLE = formatBigDecimal(TDIC_PremiumLineConstants.ZERO_AMOUNT_DOUBLE)
  private static final var ZERO_INT = formatInteger(TDIC_PremiumLineConstants.ZERO_AMOUNT_INT)

  protected static final var BLANK_STRING : String = formatString(TDIC_PremiumLineConstants.BLANK_STRING)
  protected static final var COMPANY : String = formatInteger(TDIC_PremiumLineConstants.GTR_COMPANY)
  protected static final var SOURCE_CODE : String = formatString(TDIC_PremiumLineConstants.GTR_SOURCE_CODE)

  private var PRM_SUBLINE_1 : String
  private var PRM_SUBLINE_2 : String
  private var PRM_SUBLINE_3 : String

  private var systemDate = formatString(getDate())
  private var batchRunPeriodEndDate : Date
  private var monthEndDateRef : String
  private var seqNumber : int = 0

  protected var monthEndDate : String
  protected var reportPeriodEndDate : String

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * Constructor delegates initialization of sublines and fields that need period end date
   *
   */
  @Param("periodEndDate","Batch period end date")
  construct(periodEndDate : Date){
    initialize(periodEndDate)
  }
  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * This function generates unique sequence number for each premium line reported in batch file
   */
  @Returns("Retrun int value of Sequence number")
override function getSeqNumber(): int {
  seqNumber = seqNumber + 1
  return seqNumber
}

  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * This function returns old account number for the corresponding premium line type received as input
   */
  @Param("prmLineType", "Type of TDIC_PremiumLineTypeEnum")
  @Returns("Returns Old Account number as string object")
  override function getOldAcctNumber(prmLineType : TDIC_PremiumLineTypeEnum): String {
    return formatString(TDIC_PremiumLineConstants.oldAcctNumber.get(prmLineType))
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns current system date to report in each premium line reported in premium files
   */
  @Returns("Returns current system date in String object")
  override function getSystemDate(): String {
    return systemDate
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns reference value to report in each premium line reported in premium files
   */
  @Param("state", "Type of typekey.Jurisdiction value")
  @Param("seqNum","int value of Sequence number for each premium line reported for each jurisdiction state")
  @Returns("Returns reference value in String object")
  override function getReference(state : typekey.Jurisdiction, seqNum : int): String {
    return formatString("${state}${monthEndDateRef}"+String.format(TDIC_PremiumLineConstants.INT_PATTERN_2,{seqNum}))
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns posting date which is batch run period end date to be reported in each in premium line
   * reported in batch file
   */
  @Returns("Returns batch run period end date value in String object")
  override function getPostingDate(): String {
    return formatString(reportPeriodEndDate)
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns static value of Premium sub line1 reported in each premium line reported in batch file
   */
  @Returns("Returns String object of Premium sub line1 value")
  protected property get PrmSubLine1() : String {
      return PRM_SUBLINE_1
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns static value of Premium sub line2 reported in each premium line reported in batch file
   */
  @Returns("Returns String object of Premium sub line2 value")
  protected property get PrmSubLine2() : String {
    return PRM_SUBLINE_2
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns static value of Premium sub line3 reported in each premium line reported in batch file
   */
  @Returns("Returns String object of Premium sub line3 value")
  protected property get PrmSubLine3() : String {
    return PRM_SUBLINE_3
  }

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function builds static premium sub line 1 to be reported in each of premium line in each premium file.
   * It includes
   *    GTR-BASE-AMOUNT, GTR-BASERATE, GTR-SYSTEM, GTR-PROGRAM-CODE, GTR-AUTO-REV
   */
  private function buildPrmSubLine1() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(ZERO_DOUBLE)//GTR-BASE-AMOUNT
    prmSubLine.append(ZERO_DOUBLE)//GTR-BASERATE
    prmSubLine.append(BLANK_STRING)//GTR-SYSTEM
    prmSubLine.append(BLANK_STRING)//GTR-PROGRAM-CODE
    prmSubLine.append(BLANK_STRING)//GTR-AUTO-REV
    PRM_SUBLINE_1 = prmSubLine.toString()
  }

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function builds static premium sub line 2 to be reported in each of premium line in each premium file.
   * It includes
   *   GTR-ACTIVITY, GTR-ACCT-CATEGORY, GTR-DOCUMENT-NBR, GTR-TO-BASE-AMT, GTR-EFFECT-DATE, GTR-JRNL-BOOK-NBR
   *   GTR-MX-VALUE1, GTR-MX-VALUE2, GTR-MX-VALUE3, GTR-JBK-SEQ-NBR
   */
  private function buildPrmSubLine2() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(BLANK_STRING)//GTR-ACTIVITY
    prmSubLine.append(BLANK_STRING)//GTR-ACCT-CATEGORY
    prmSubLine.append(BLANK_STRING)//GTR-DOCUMENT-NBR
    prmSubLine.append(ZERO_DOUBLE)//GTR-TO-BASE-AMT
    prmSubLine.append(formatString(reportPeriodEndDate))//GTR-EFFECT-DATE
    prmSubLine.append(BLANK_STRING)//GTR-JRNL-BOOK-NBR
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE1
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE2
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE3
    prmSubLine.append(ZERO_INT)//GTR-JBK-SEQ-NBR
    PRM_SUBLINE_2 = prmSubLine.toString()
  }

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function builds static premium sub line 3 to be reported in each of premium line in each premium file.
   * It includes
   *     GTR-SEGMENT-BLOCK, GTR-RPT-AMOUNT-1, GTR-RPT-RATE-1, GTR-RPT-ND-1, GTR-RPT-AMOUNT-2, GTR-RPT-RATE-2
   *     GTR-RPT-ND-2
   */
  private function buildPrmSubLine3() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(BLANK_STRING)//GTR-SEGMENT-BLOCK
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-AMOUNT-1
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-RATE-1
    prmSubLine.append(ZERO_INT)//GTR-RPT-ND-1
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-AMOUNT-2
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-RATE-2
    prmSubLine.append(TDIC_PremiumLineConstants.ZERO_AMOUNT_INT)//GTR-RPT-ND-2
    PRM_SUBLINE_3 = prmSubLine.toString()
  }

  /**
   * US627
   * 11/27/2014 Kesava Tavva
   *
   * This function formats the string object values as per the premium batch file data structure requirements.
   */
  @Param("value", "String Object value to be formatted")
  @Returns("Returns formatted values in String object")
  protected static function formatString(value: String) : String {
    return TDIC_PremiumLineConstants.DOUBLE_QUOTE+value+TDIC_PremiumLineConstants.DOUBLE_QUOTE+TDIC_PremiumLineConstants.COMMA_SEPARATOR
  }

  /**
   * US627
   * 11/27/2014 Kesava Tavva
   *
   * This function formats the BigDecimal object values as per the premium batch file data structure requirements.
   */
  @Param("value", "BigDecimal Object value to be formatted")
  @Returns("Returns formatted values in String object")
  protected static function formatBigDecimalWithDecimals(value : BigDecimal) : String {
    return TDIC_PremiumLineConstants.DECIMAL_FORMAT.format(value)+TDIC_PremiumLineConstants.COMMA_SEPARATOR
  }

  /**
   * US627
   * 11/27/2014 Kesava Tavva
   *
   * This function formats the BigDecimal object values as per the premium batch file data structure requirements.
   */
  @Param("value", "BigDecimal Object value to be formatted")
  @Returns("Returns formatted values in String object")
  protected static function formatBigDecimal(value : BigDecimal) : String {
    return value+TDIC_PremiumLineConstants.COMMA_SEPARATOR
  }

  /**
   * US627
   * 11/27/2014 Kesava Tavva
   *
   * This function formats the Integer object values as per the premium batch file data structure requirements.
   */
  @Param("value", "Integer Object value to be formatted")
  @Param("pattern", "Pattern to be applied in formating integer values")
  @Returns("Returns formatted values in String object")
  protected static function formatInteger(value : Integer, pattern : String) : String {
    return String.format(pattern,{value})+TDIC_PremiumLineConstants.COMMA_SEPARATOR
  }

  /**
   * US627
   * 11/27/2014 Kesava Tavva
   *
   * This function formats the Integer object values as per the premium batch file data structure requirements.
   */
  @Param("value", "Integer Object value to be formatted")
  @Returns("Returns formatted values in String object")
  protected static function formatInteger(value : Integer) : String {
    return value+TDIC_PremiumLineConstants.COMMA_SEPARATOR
  }

  /**
   * US627
   * 11/28/2014 Kesava Tavva
   *
   * This function builds formatted current system date as per the premium batch file data structure requirements.
   */
  @Returns("Returns current system date in string object")
  private function getDate() : String {
    var sysDate = Calendar.getInstance().getTime()
    return TDIC_PremiumLineConstants.DATE_FORMAT_MMDDYYYY.format(sysDate)
  }

  /**
   * US627
   * 11/28/2014 Kesava Tavva
   *
   * This function builds formatted batch period end date as per the premium batch file data structure requirements.
   */
  @Param("sdf", "Simple date format object for formatting period end date.")
  @Returns("Returns batch period end date in string object")
  private static function getBatchRunPeriodEndDate(sdf : SimpleDateFormat) : String {
    var calendar = Calendar.getInstance()
    calendar.set(Calendar.MONTH, calendar.CalendarMonth-1)
    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
    var date = calendar.getTime()
    return sdf.format(date)
  }

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * This function initialize premium line values that contains period end date
   *
   */
  @Param("ped", "Period end date for batch run")
  private function initialize(ped : Date) {
    batchRunPeriodEndDate = ped
    monthEndDate = TDIC_PremiumLineConstants.DATE_FORMAT_MMDDYY.format(batchRunPeriodEndDate)
    monthEndDateRef = TDIC_PremiumLineConstants.DATE_FORMAT_MMDDYY.format(batchRunPeriodEndDate).replaceAll("/","")
    reportPeriodEndDate = TDIC_PremiumLineConstants.DATE_FORMAT_MMDDYYYY.format(batchRunPeriodEndDate)
    initializeSubLines()
  }

  /**
   * US627
   * 12/15/2014 Kesava Tavva
   *
   * This function initialize static sublines that are needed to report in each premium line in GL report
   *
   */
  private function initializeSubLines() {
    buildPrmSubLine1()
    buildPrmSubLine2()
    buildPrmSubLine3()
  }

  /**
   * US627
   * 01/02/2015 Kesava Tavva
   *
   * This function is to get account code defined for a given Product line (lob)
   */
  @Param("lobCode", "Produuct line type code value")
  @Returns("Returns account code mapped for a given product line")
  protected function getMappedLOBAccountCode(lobCode : String) : String {
    var logPrefix = "${CLASS_NAME}#getMappedLOBAccountCode(lobCode)"
    if (lobCode == null) {
      return "00"
    }
    else {
      var mappedCode = LOBAccountCode_TDIC.get(lobCode)
      if (mappedCode != null) {
        _logger.debug("${logPrefix} - Found account mapping for LOB: " + mappedCode)
        return mappedCode.DisplayName
      }
      else {
        _logger.error("TDIC_GLIntegrationHelper#getMappedLOBAccountCode - Incorrect setup:  Cannot find mapping for LOB code '" + lobCode + "'")
        throw new RuntimeException("Incorrect setup:  Cannot find mapping for LOBCode '" + lobCode + "'")
      }
    }
  }

  /**
   * US627
   * 01/02/2015 Kesava Tavva
   *
   * This function is to get account code defined for a given Jurisdiction state
   */
  @Param("jurisdictionCode", "Jurisdiction type code value")
  @Returns("Returns account code mapped for a given Jurisdiction state code")
  protected function getMappedJurisdictionAccountCode(jurisdictionCode : String) : String {
    var logPrefix = "${CLASS_NAME}#getMappedJurisdictionAccountCode(jurisdictionCode)"
    if (jurisdictionCode == null) {
      return "00"
    }
    else {
      var mappedCode = JurisdictionAcctCode_TDIC.get(jurisdictionCode)
      if (mappedCode != null) {
        _logger.debug("TDIC_GLIntegrationHelper#getMappedJurisdictionAccountCode - Found account mapping for Jurisdiction: " + mappedCode)
        return mappedCode.DisplayName
      }
      else {
        _logger.error("TDIC_GLIntegrationHelper#getMappedJurisdictionAccountCode - Incorrect setup:  Cannot find mapping for Jurisdiction '" + jurisdictionCode + "'")
        throw new RuntimeException("Incorrect setup:  Cannot find mapping for Jurisdiction '" + jurisdictionCode + "'")
      }
    }
  }
}