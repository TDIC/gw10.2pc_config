package tdic.pc.common.batch.generalledger.prmlines

uses java.util.Map
uses java.text.SimpleDateFormat
uses java.text.DecimalFormat

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This class is to define constants for Premium lines reported in Written and Earned premium batch files
 */
class TDIC_PremiumLineConstants {
  //Line constants
  protected static final var BLANK_STRING : String = ""
  protected static final var DOUBLE_QUOTE : String  = "\""
  protected static final var COMMA_SEPARATOR: String = ","
  protected static final var DOT_SEPARATOR: String = "."
  protected static final var ZERO_AMOUNT_DOUBLE : double = 0.0
  protected static final var ZERO_AMOUNT_INT : int = 0
  protected static final var NEG_ADJ_Y : String = "Y"

  protected static final var GTR_COMPANY : int = 5
  protected static final var GTR_SOURCE_CODE : String = "IG"
  protected static final var ACCT_UNIT_PREFIX : String = "0005"
  protected static final var BAL_SHEET_ACCT_UNIT : String = "000"
  protected static final var SALES_ACCT_UNIT : String = "800"
  private static final var MMDDYY : String = "MM/dd/yy"
  private static final var MMDDYYYY : String = "MM/dd/yyyy"

  private static final var ACC_NBR_TERRORISM : String = "0040502201"
  private static final var ACC_NBR_EXP_CONSTANT : String = "0040502202"
  private static final var ACC_NBR_PREMIUM : String = "0040502203"
  private static final var ACC_NBR_TERRORISM_AUDIT : String = "0040502204"
  private static final var ACC_NBR_EXP_CONSTANT_AUDIT : String = "0040502205"
  private static final var ACC_NBR_PREMIUM_AUDIT : String = "0040502206"
  private static final var ACC_NBR_STATE_ASSMNTS : String = "0026600000"
  private static final var ACC_NBR_PRM_RCV_ACCOUNT : String = "0012652230"
  private static final var ACC_NBR_UNEARNED_PREMIUM : String = "0020200000"
  private static final var ACC_NBR_CHG_UNEARNED_PREMIUM : String = "0041700000"

  private static final var WC7WORKERSCOMPLINE : String = "40"

  private static final var STATE_AK : String = "02"
  private static final var STATE_AZ : String = "04"
  private static final var STATE_CA : String = "06"
  private static final var STATE_GA : String = "13"
  private static final var STATE_HI : String = "15"
  private static final var STATE_IL : String = "17"
  private static final var STATE_MN : String = "28"
  private static final var STATE_NV : String = "33"
  private static final var STATE_NJ : String = "35"
  private static final var STATE_NM : String = "36"
  private static final var STATE_ND : String = "39"
  private static final var STATE_PA : String = "45"

  protected static final var stateAcctUnitMap : Map<typekey.Jurisdiction, String> =  buildStateAcctUnitMap()
  protected static final var lobAcctUnitMap : Map<typekey.PolicyLine, String> = buildLobAcctUnitMap()
  protected static final var oldAcctNumber : Map< TDIC_PremiumLineTypeEnum, String> = buildOldAcctNumber()
  protected static final var DATE_FORMAT_MMDDYY : SimpleDateFormat = new SimpleDateFormat(MMDDYY)
  protected static final var DATE_FORMAT_MMDDYYYY : SimpleDateFormat = new SimpleDateFormat(MMDDYYYY)
  protected static final var INT_PATTERN_2 : String = "%02d"
  protected static final var INT_PATTERN_6 : String = "%06d"
  protected static final var DECIMAL_FORMAT : DecimalFormat = new DecimalFormat("0.00")

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function is to define mappings for Jurisdiction State to include in
   * Account Unit for reporting in Premium lines
   */
  @Returns("Returns Map object of Jurisdiction state and its Account Unit combinations")
  private static function buildStateAcctUnitMap() : Map<typekey.Jurisdiction, String> {
    return   {
                typekey.Jurisdiction.TC_AK -> STATE_AK,
                typekey.Jurisdiction.TC_AZ -> STATE_AZ,
                typekey.Jurisdiction.TC_CA -> STATE_CA,
                typekey.Jurisdiction.TC_GA -> STATE_GA,
                typekey.Jurisdiction.TC_HI -> STATE_HI,
                typekey.Jurisdiction.TC_IL -> STATE_IL,
                typekey.Jurisdiction.TC_MN -> STATE_MN,
                typekey.Jurisdiction.TC_NV -> STATE_NV,
                typekey.Jurisdiction.TC_NJ -> STATE_NJ,
                typekey.Jurisdiction.TC_NM -> STATE_NM,
                typekey.Jurisdiction.TC_ND -> STATE_ND,
                typekey.Jurisdiction.TC_PA -> STATE_PA
            }
  }

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function is to define mappings for LOB to include in
   * Account Unit for reporting in Premium lines
   */
  @Returns("Returns Map object for Policy Line (lob) and its Account Unit combinations")
  private static function buildLobAcctUnitMap() : Map<typekey.PolicyLine, String> {
    return {typekey.PolicyLine.TC_WC7WORKERSCOMPLINE -> WC7WORKERSCOMPLINE}
  }

  /**
   * US627
   * 11/25/2014 Kesava Tavva
   *
   * This function is to build mappings for Premium line type and its related Old Account Number
   * to use in Premium lines reported in Written and Earned Premium batch files
   */
  @Returns("Returns Map Object of Premium Line Type Enumeration and its Old Account Number combinations")
  private static function buildOldAcctNumber() : Map< TDIC_PremiumLineTypeEnum, String> {
    return {
            TDIC_PremiumLineTypeEnum.TERRORISM -> ACC_NBR_TERRORISM,
            TDIC_PremiumLineTypeEnum.EXP_CONSTANT -> ACC_NBR_EXP_CONSTANT,
            TDIC_PremiumLineTypeEnum.PREMIUM -> ACC_NBR_PREMIUM,
            TDIC_PremiumLineTypeEnum.TERRORISM_AUDIT -> ACC_NBR_TERRORISM_AUDIT,
            TDIC_PremiumLineTypeEnum.EXP_CONSTANT_AUDIT -> ACC_NBR_EXP_CONSTANT_AUDIT,
            TDIC_PremiumLineTypeEnum.PREMIUM_AUDIT -> ACC_NBR_PREMIUM_AUDIT,
            TDIC_PremiumLineTypeEnum.STATE_ASSMNTS -> ACC_NBR_STATE_ASSMNTS,
            TDIC_PremiumLineTypeEnum.PRM_RCV_ACCOUNT -> ACC_NBR_PRM_RCV_ACCOUNT,
            TDIC_PremiumLineTypeEnum.UNEARNED_PREMIUM -> ACC_NBR_UNEARNED_PREMIUM,
            TDIC_PremiumLineTypeEnum.CHG_UNEARNED_PREMIUM -> ACC_NBR_CHG_UNEARNED_PREMIUM
           }
  }
}