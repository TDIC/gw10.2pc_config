package tdic.pc.common.batch.enrole

uses com.tdic.util.properties.PropertyUtil
uses entity.Contact
uses gw.api.gx.GXOptions
uses org.apache.commons.io.FileUtils
uses org.apache.commons.lang.StringUtils
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.enrole.dao.EnroleDAOImpl
uses tdic.pc.common.batch.enrole.dao.IEnroleDAO
uses tdic.pc.common.batch.enrole.exception.EnroleException
uses tdic.pc.common.batch.enrole.vo.EnroleCourseVO
uses tdic.pc.common.batch.enrole.vo.EnrolePolicyVO

uses java.io.File
uses java.io.FileNotFoundException
uses java.io.IOException
uses java.nio.file.CopyOption
uses java.nio.file.Files
uses java.nio.file.Paths
uses java.nio.file.StandardCopyOption
uses java.text.SimpleDateFormat

/**
 * EnroleHelper is responsible to
 * Insert enrole policies to table: EnrolePolicyDetails of GWINT DB.
 * Fetch unprocessed records from EnrolePolicyDetails and write to enrole policy outbound file.
 * Read enrole course details from inbound file and load into table: EnroleCourseDetails.
 * Fetch enrole course details from table: EnroleCourseDetails based on Policy Number and ADA number and
 * Update enrole course details on policy transaction.
 * Created by RambabuN on 10/15/2019.
 */
class EnroleHelper {

  /** Invalid componet string */
  static final var INVALID_COMPONENT : String = "*INVALID COMPONENT*"
  /** New Dentist (subtype of PH policyholder receiving New Dentist discount) */
  static final var PERSON_TYPE_NDEN : String = "NDEN"
  /** Policy holder code*/
  static final var PERSON_TYPE_PH : String = "PH"
  /** Policy status 'In Force'*/
  static final var POLICY_IN_FORCE : String = "In Force"

  static final var DATE_FORMAT_YYYY_MM_dd : String = "yyyyMMdd"
  static final var FYLE_TYPE_TXT : String = ".txt"
  static final var CAD_DATEFORMAT = "MM/dd/yyyy"
  static final var COUNTRY_USA =  "USA"
  static final var ERROR = "Error"
  static final var ACTIVE_CODE = "A"
  static final var INACTIVE_CODE = "I"
  static final var YES_CODE = "Y"
  static final var CHAR_UNDER_SCORE = "_"
  static final var CHAR_COMMA = ","

  static final var _logger = LoggerFactory.getLogger("TDIC_Enrole")

  static final var propUtil = PropertyUtil.getInstance()
  /**
   * Property with the policy file name generated to enrole property
   */
  static final var ENROLE_OUTBOUND_POLICYFILE_NAME_PROPERTY = "enrole.outbound.policyfile.name"
  /**
   * Property with the relative route to place the files generated to AS400 property
   */
  static final var ENROLE_OUTBOUND_FILESDIRECTORY_NAME_PROPERTY = "enrole.outbound.filesdirectory.name"
  /**
   * Property with the relative route to read the files from enrole property
   */
  static final var ENROLE_INBOUND_FILESDIRECTORY_NAME_PROPERTY = "enrole.inbound.filesdirectory.name"
  /**
   * Property with the relative route to read the files from enrole property
   */
  static final var ENROLE_INBOUND_DONE_FILESDIRECTORY_NAME_PROPERTY = "enrole.inbound.done.filesdirectory.name"
  /**
   * Property with the relative route to read the files from enrole property
   */
  static final var ENROLE_INBOUND_ERROR_FILESDIRECTORY_NAME_PROPERTY = "enrole.inbound.error.filesdirectory.name"

  /**
   * Function to load enrole inbound file Semtek_xxxxxxxx.txt to table EnroleCourseDetails of GWINT DB.
   */
  static function loadEnroleInboundFile() {
    _logger.debug("Enter EnroleHelper.loadEnroleInboundFile()")
    var enroleDAO : IEnroleDAO
    var currentFile : File
    try {
      enroleDAO = new EnroleDAOImpl()
      enroleDAO.initialize()

      var folder = new File(propUtil.getProperty(ENROLE_INBOUND_FILESDIRECTORY_NAME_PROPERTY))
      /** Check for the erole inbound directory existance */
      if (!folder.exists()) {
        throw new FileNotFoundException()
      }
      var enroleInboundFiles = folder.listFiles().where(\elt -> elt.isFile() && elt.getName().endsWith(FYLE_TYPE_TXT))
      for(file in enroleInboundFiles) {

        currentFile = file
        try{
          //Read Erole course details from inbound file.
          var courseVOs = readEnroleInboundFile(file)
          if (!courseVOs.isEmpty()) {
            enroleDAO.insertEnroleCourseDetails(courseVOs)
            _logger.debug("EnroleCourseDetails inbound file ${currentFile.Name} loaded successfully.)" )
          } else {
            _logger.debug("No EnroleCourseDetails found in inbound file : ${currentFile.Name}")
          }
        } catch (ex : Exception) {
          _logger.error("Failed load inbound file: ${currentFile.Name}")
          moveProcessedEnroleInboundFile(currentFile,Boolean.FALSE,ex.StackTraceAsString)
          continue
        }
        moveProcessedEnroleInboundFile(currentFile,Boolean.TRUE,null)
      }
    } catch (ex : Exception) {
      _logger.error("Exception while reading course details from  enrole inbound directory.", ex)
      throw new EnroleException(ex.Message, ex)
    } finally {
      enroleDAO.cleanConnections()
    }
    _logger.debug("Exit EnroleHelper.loadEnroleInboundFile()")
  }

  /**
   * Function to read Enrole course details and as list of EnroleCourseVO
   * @param inboundFile
   * @return courseVOs
   */
  private static function readEnroleInboundFile(inboundFile : File): List<EnroleCourseVO> {
    /**Read enrole course records as List from inbound file.*/
    var courses = FileUtils.readLines(inboundFile, java.nio.charset.StandardCharsets.UTF_8)
    _logger.debug("${courses.Count} enrole course records red from inbound file ${inboundFile} completed.")

    var courseVOs = new ArrayList<EnroleCourseVO>()
    /** Populate Enrole inbound course details as List of EnroleCourseVO*/
    courses.each(\ courseDetails -> {
      if(courseDetails?.trim().NotBlank)
        courseVOs.add(getEnroleCourseVO(Arrays.asList(courseDetails.split(CHAR_COMMA))))
    })

    return courseVOs
  }

  /**
   * Functionn to map list of enrole course records to List of EnroleCourseVO
   *
   * @param courseData
   * @return
   */
  private static function getEnroleCourseVO(courseData : List<String>) : EnroleCourseVO {
    _logger.debug("Enter EnroleHelper.getEnroleCourseVO()")
    var courseVO : EnroleCourseVO
    if (!courseData?.isEmpty()) {
      courseVO = new EnroleCourseVO()

      courseVO.PersonID = courseData.get(0)
      courseVO.PolicyNumber = courseData.get(1)
      courseVO.ADA = courseData.get(2)
      courseVO.SessionNumber = courseData.get(3)
      courseVO.SessionName = courseData.get(4)
      courseVO.CourseNumber = Integer.valueOf(courseData.get(5))
      courseVO.CourseName = courseData.get(6)
      courseVO.CourseAttendedDate = new SimpleDateFormat(CAD_DATEFORMAT).parse(courseData.get(7))

    }
    _logger.debug("Exit EnroleHelper.getEnroleCourseVO()")
    return courseVO
  }

  /**
   * Function to apply discounts to on policy renewal from enrole course details.
   *
   * @param policyPeriod
   */
  static function updateRiskManagementDiscount(policyPeriod : PolicyPeriod) {
    _logger.debug("Enter EnroleHelper.updateRiskManagementDiscount()")
    var enroleDAO : IEnroleDAO
    try {
      enroleDAO = new EnroleDAOImpl()
      enroleDAO.initialize()
      /**Fetch enrole course details based Policy Number and ADA.*/
      var priInsured = policyPeriod.PrimaryNamedInsured.ContactDenorm
      var courseVOs = enroleDAO.fetchEnrolePolicyDetails(policyPeriod.PolicyNumber,
          getHistoricalPolicyNumber(policyPeriod), priInsured.ADANumber_TDICOfficialID)

      var maxEndDate = policyPeriod.GLLine.RiskManagementDisc_TDIC?.maxBy(\elt -> elt.EndDate)?.EndDate

      /**
       * StartDate is calculated based on existing discount maximum enddate and policy renewal effective date.
       * Case1: maxEndDate is null or both dates are equal --> start date is renewal period effective date.
       * Case2: maxEndate is lower than renewal period effective date --> start date is renewal period effective date.
       * Case3: maxEndate is higher than renewal period effective date --> start date is maxEndDate.
       * EndDate is two years later from start date.
       */
      var startDate = maxEndDate?.compareTo(policyPeriod.PeriodStart) < 0 ? policyPeriod.PeriodStart :
          (maxEndDate?.compareTo(policyPeriod.PeriodStart) > 0 ? maxEndDate : policyPeriod.PeriodStart )

      for (courseVO in courseVOs) {
        /**Check for the existing RiskManagementDisc before applying discout on policy transaction.*/
        if (!policyPeriod.GLLine?.RiskManagementDisc_TDIC?.hasMatch(\elt1 -> elt1.Name == courseVO.SessionNumber &&
            elt1.LocationCode == String.valueOf(courseVO.CourseNumber) and
            elt1.LPSDate.toSQLDate().compareTo(courseVO.CourseAttendedDate) == 0)) {
          var riskManagementDisc = new RiskManagementDisc_TDIC(policyPeriod)
          riskManagementDisc.Name = courseVO.SessionNumber
          riskManagementDisc.Attended = YES_CODE
          riskManagementDisc.LPSDate = courseVO.CourseAttendedDate
          riskManagementDisc.LocationCode = String.valueOf(courseVO.CourseNumber)
          riskManagementDisc.LocationName = courseVO.CourseName
          riskManagementDisc.StartDate = policyPeriod.GLLine.RiskManagementDisc_TDIC.hasMatch(\elt1 ->
              elt1.LocationCode == String.valueOf(courseVO.CourseNumber))?
              policyPeriod.GLLine.RiskManagementDisc_TDIC?.maxBy(\elt -> elt.StartDate)?.StartDate : startDate
          riskManagementDisc.EndDate = riskManagementDisc.StartDate?.addYears(2)
          policyPeriod.GLLine.addToRiskManagementDisc_TDIC(riskManagementDisc)
          _logger.debug(courseVO.SessionNumber +
              " RiskManagementDisc applied for ${policyPeriod.PolicyNumber},${priInsured.ADANumber_TDICOfficialID}")
        }
      }
    } catch (ex : EnroleException) {
      _logger.error("Failed to apply discounts on policy# ${policyPeriod.PolicyNumber}")
      throw ex
    } catch (e : Exception) {
      _logger.error("Exception found while apply discounts on policy# ${policyPeriod.PolicyNumber}")
      throw new EnroleException(e.Message, e)
    } finally {
      /**Realease connections*/
      enroleDAO?.cleanConnections()
    }
    _logger.debug("Exit EnroleHelper.updateRiskManagementDiscount()")
  }

  /**
   * Function to insert enrole policy record into table EnrolePolicyDetails of GWINT DB.
   *
   * @param enrolePolVO
   */
  static function insertEnrolePolicy(enrolePolVO : EnrolePolicyVO) {
    _logger.debug("Enter EnroleHelper.insertEnrolePolicy()")
    var enroleDAO : IEnroleDAO
    try {
      enroleDAO = new EnroleDAOImpl()
      enroleDAO.initialize()

      enroleDAO.insertEnrolePolicyDetails(enrolePolVO)
    } catch (ex : EnroleException) {
      _logger.error("Failed to insert EnrolePolicyDetails:: ${enrolePolVO}")
      throw ex
    } catch (e : Exception) {
      _logger.error("Exception found while inserting EnrolePolicyDetails:: ${enrolePolVO}")
      throw new EnroleException(e.Message, e)
    } finally {
      /**Release connections.*/
      enroleDAO.cleanConnections()
    }
    _logger.debug("Exit EnroleHelper.insertEnrolePolicy()")
  }


  /**
   * Function to generate enrole policy record payload string from policy period.
   *
   * @param period
   * @return payload string
   */
  static function generateEnrolePolicyPayload(period : PolicyPeriod) : String {

    /**GX options are added to optimise parsing*/
    var options = new GXOptions()
    options.Incremental = false
    options.Verbose = false

    /**Map policy info to EnrolePolicyVO.*/
      var _policyVO = populateEnrolePolicyVO(period)
    var _voModel = new tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO().create(_policyVO, options)

    return _voModel.asUTFString()
  }

  /**
   * Map policy info to EnrolePolicyVO
   *
   * @param period
   * @return EnrolePolicyVO
   */
  private static function populateEnrolePolicyVO(period : PolicyPeriod) : EnrolePolicyVO {
    _logger.debug("Enter EnroleHelper.populateEnrolePolicyVO()")
    var policyVO = new EnrolePolicyVO()

    policyVO.PolicyNBR = period.PolicyNumber
    /**Populate policy primary insured person contact information.*/
    var primaryInsured = period.PrimaryNamedInsured.ContactDenorm
    populatePrimaryInsuredInfo(policyVO, primaryInsured)

    /**Populate component state,ID and Description.*/
    var component = period.GLLine.GLExposuresWM.first().GLTerritory_TDIC
    policyVO.ComponentID = component?.Component
    /** No Componet exists, set CompDesc to '*INVALID COMPONENT*' */
    policyVO.CompDesc = component != null ? component.Description?.toUpperCase() : INVALID_COMPONENT

    policyVO.ComponentState = period.BaseState.Code

    /** Policy has GL NewDentist Discount exists set person type as New dentist value 'NDEN' otherwise
     Policy holder value as 'PH' */
    policyVO.PersonType = period.GLLine?.GLNewDentistDiscount_TDICExists ? PERSON_TYPE_NDEN : PERSON_TYPE_PH
    /** Policy is in force set PolicyIndicator as active value 'A' otherwise inactive value 'I' */
    policyVO.PolicyIndicator = period.PeriodDisplayStatus == POLICY_IN_FORCE ? ACTIVE_CODE : INACTIVE_CODE

    /**Set status as Policy Holder value 'P'*/
    policyVO.Status = 'P'

    var specialityCode = period.GLLine?.GLExposuresWM?.first()?.SpecialityCode_TDIC.Code
    /** Speciality calue starts with numaric value set first two characters integer value otherwise null */
    policyVO.SpecType = Character.isDigit(specialityCode?.charAt(0)) ? specialityCode?.substring(0, 2) : null

    policyVO.RenewDT = period.Job typeis Renewal ? period.PeriodStart : null
    /**Max EndDate from risk management discounts */
    policyVO.DscExpDT = period.GLLine.RiskManagementDisc_TDIC?.maxBy(\elt -> elt.EndDate)?.EndDate

    //Policy is cancelled and the Policy Cancellation Reason is Deceased then mark the SEMTEK STATUS as "I"
    policyVO.SemtekStatus =
        period.CancellationReasonCode_TDIC == ReasonCode.TC_NOLONGERINBUSINESSDEC_TDIC ? INACTIVE_CODE : null
    _logger.debug("Exit EnroleHelper.populateEnrolePolicyVO()")
    return policyVO
  }

  /**
   * Function to populate primary insured information to EnrolePolicyVO.
   * @param policyVO
   * @param primaryInsured
   */
  private static function populatePrimaryInsuredInfo(policyVO : EnrolePolicyVO, primaryInsured : Contact) {
    if (primaryInsured typeis Person) {
      policyVO.FirstName = primaryInsured.FirstName
      policyVO.LastName = primaryInsured.LastName
      policyVO.Salutation = primaryInsured.Prefix.DisplayName
      /**Capture Middle name initial char*/
      policyVO.MiddleInitial = primaryInsured.MiddleName?.charAt(0)
      policyVO.Gender = primaryInsured.Gender.DisplayName

      var address = primaryInsured.PrimaryAddress
      if (address != null) {
        policyVO.Address1 = address.AddressLine1
        policyVO.Address2 = address.AddressLine2
        policyVO.City = address.City
        policyVO.State = address.State.DisplayName
        policyVO.Zip = address.PostalCode
        policyVO.Country = address.Country == Country.TC_US ? COUNTRY_USA : null
      }
      policyVO.Email = primaryInsured.EmailAddress1

      policyVO.Phone = primaryInsured.PrimaryPhoneValue
      policyVO.Fax = primaryInsured.FaxPhone

      policyVO.Title = primaryInsured.Credential_TDIC.DisplayName
      policyVO.ADA = primaryInsured.ADANumber_TDICOfficialID
    }
  }

  /**
   * Function to return outbound enrole policy file name.
   *
   * @return file name.
   */
  static function writeEnrolePolicyDetails() {
    _logger.debug("Enter EnroleHelper.writeEnrolePolicyDetails()")
    var enroleDAO : IEnroleDAO

    try {
      enroleDAO = new EnroleDAOImpl()
      enroleDAO.initialize()

      /**Fetch formatted text policy records to be enrolled from table EnrolePolicyDetails of GWINT DB.*/
      var policyRecords = enroleDAO.getEnrolePolicyDetails()

      if (policyRecords.size() > 1) {
        var enroleOutboundFile = getEnroleFile(ENROLE_OUTBOUND_FILESDIRECTORY_NAME_PROPERTY,
            ENROLE_OUTBOUND_POLICYFILE_NAME_PROPERTY)

        /**Write resulted policy records to enrole outbound polinfo_proc_xxxxxxxx.txt file.*/
        FileUtils.writeLines(new File(enroleOutboundFile), policyRecords)
        _logger.debug("Write policies to Enrole outbound PolicyFile : ${enroleOutboundFile} completed.")

        /**On sucessfull writing policy records to file, Update processed flag to true in table EnrolePolicyDetails.*/
        enroleDAO.updateEnrolePolicyDetails()
        _logger.debug("Updated EnrolePolicy processed record status to true.")
      } else {
        _logger.debug("There are no enrole policies to be written.")
      }
    } catch (ioe : IOException) {
      _logger.error("IOException caught in batch process: ", ioe)
      throw new EnroleException(ioe.Message, ioe)
    } catch (ex : Exception) {
      _logger.error("Exception while writing policies to enrole policy file", ex)
      throw new EnroleException(ex.Message, ex)
    } finally {
      /**Release connections*/
      enroleDAO?.cleanConnections()
    }
    _logger.debug("Exit EnroleHelper.writeEnrolePolicyDetails()")
  }

  /**
   * Function to generate enrole inbound or outbound file based on file directry and name properties.
   *
   * @param fileDirProp
   * @param fileNameProp
   * @return
   */
  private static function getEnroleFile(fileDirProp : String, fileNameProp : String) : String {

    var fileDir = propUtil.getProperty(fileDirProp)
    var fileName = propUtil.getInstance().getProperty(fileNameProp)
    return new StringBuilder().append(fileDir).append(fileName).append(CHAR_UNDER_SCORE)
        .append(new SimpleDateFormat(DATE_FORMAT_YYYY_MM_dd).format(Date.CurrentDate))
        .append(FYLE_TYPE_TXT).toString()
  }

  /**
   * Function to move processed enrole inbound file to done or error directries.
   *
   * @param file
   * @param loadSuccess
   * @param errorMessage
   * @return
   */
  private static function moveProcessedEnroleInboundFile(file : File, loadSuccess : boolean, errorMessage: String) {

    /** Generate target file based on inbound file success or failure */
    var targetFile = new StringBuilder()
        .append(propUtil.getProperty(loadSuccess ? ENROLE_INBOUND_DONE_FILESDIRECTORY_NAME_PROPERTY :
            ENROLE_INBOUND_ERROR_FILESDIRECTORY_NAME_PROPERTY)).append(file.Name).toString()

    /** Move processed inbound file to done or error directory. */
    Files.move(Paths.get(file.Path),
        Paths.get(targetFile), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING})

    /** Create Error file with error description in error directory for unprocessed inbound file. */
    if(!loadSuccess) {
      var errorFileDir = new StringBuilder()
          .append(propUtil.getProperty(ENROLE_INBOUND_ERROR_FILESDIRECTORY_NAME_PROPERTY))
          .append(ERROR).append(CHAR_UNDER_SCORE).append(file.Name).toString()
      Files.write(Paths.get(errorFileDir),errorMessage.toString().getBytes())
    }
  }

  private static function getHistoricalPolicyNumber(policyPeriod :  PolicyPeriod): String {
    // legacyPolcyNum = "CA 061939-5-C3"  datamartPolicyNum = "0619395"
    var datamartPolicyNum : String = ""
    datamartPolicyNum = (policyPeriod.LegacyPolicySource_TDIC == "DIMS") ? policyPeriod.LegacyPolicyNumber_TDIC?.replace("-", "")
                           : policyPeriod.LegacyPolicyNumber_TDIC?.substring(3,11)?.replace("-","")
    return datamartPolicyNum
  }

}