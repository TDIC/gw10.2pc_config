package tdic.pc.common.batch.unistat.util

uses com.tdic.util.properties.PropertyUtil
uses tdic.pc.common.batch.unistat.helper.TDIC_WCstatRecordTypes
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatPolicyHeaderFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatNameFields
uses java.text.SimpleDateFormat
uses java.text.DateFormat
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatAddressFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatExposureFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatLossFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatUnitTotalFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatReportHeaderFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatReportTrailerFields
uses java.util.ArrayList
uses java.util.List
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory
uses wsi.remote.gw.webservice.cc.cc1000.pcclaimsearchintegrationapi.anonymous.elements.PCClaimSearchCriteria_PolicyNumbers
uses wsi.remote.gw.webservice.cc.cc1000.pcclaimsearchintegrationapi.types.complex.PCClaimSearchCriteria

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/19/14
 * Time: 12:50 PM
 * This class implements functionality for assigning values to fields for all the record types.
 */
class TDIC_WCstatUtil {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")
  final static var label = '$!+WORKCOMP+!$'
  final static var dataTypeCode = 'WCS'
  final static var dataReceiverCode = 00004
  final static var versionIdentifier = 'V01'
  final static var dataProviderCode = 74419
  final static var dataProviderTypeCode = 'C'
  final static var blank = ' '
  final static var exposureStateCode = 04
  final static var correctionSequenceNumber = '0'
  final static var polRecTypeCode = 1
  final static var nameRecTypeCode = 2
  final static var addressRecTypeCode = 3
  final static var exposureRecTypeCode = 4
  final static var lossRecTypeCode = 5
  final static var unitStatRecTypeCode = 6
  final static var typeOfCovIdCode = 01
  final static var typeOfPlanIdCode = 01
  final static var typeOfNonStandardIdCode = 01
  final static var lossesSubjectToDeductibleCode = 00
  final static var basisOfDedCalcCode = 00
  final static var dedPercentage = 00
  final static var dedAmountPerClaimOrAccident = 000000000
  final static var deductibleAmountAgg = 000000000
  final static var unitFormatSubmissionCode = 'E'
  final static var splitPeriodCode = '0'
  final static var updateTypeCode = 'R'
  final static var claimCount = '0001'
  final static var lossCovActCode = 01
  final static var jurStateCode = 00
  final static var filler = '9999999999999999999999999999999999999999'
  final static var trailerRecTypeCode = 9
  final static var polNumPrefix = 'CAWC'
//GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
  final static var submissionTypeCode = PropertyUtil.getInstance().getProperty("typeCodeOfSubmission")
  final static var dataProviderEmail = PropertyUtil.getInstance().getProperty("dataProviderEmailId")
  final static var nameOfDataProvider = PropertyUtil.getInstance().getProperty("dataProviderName")
  final static var phoneNumber = PropertyUtil.getInstance().getProperty("dataProviderPhoneNumber")
  final static var faxNumber = PropertyUtil.getInstance().getProperty("dataProviderFaxNumber")
  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail")

  /**
   * Creates payload with values for different fields of various record types.
   **/
  public static function createPayload (pp : PolicyPeriod): TDIC_WCstatRecordTypes {

    var _wcstatRecord = new TDIC_WCstatRecordTypes()
    var policyPeriod  = pp.getSlice(pp.EditEffectiveDate)

    _wcstatRecord.PolicyHeaderRecord = createPolicyReportHeaderFields(policyPeriod)
    _wcstatRecord.NameRecord = createNameFields(policyPeriod)
    _wcstatRecord.AddressRecord = createAddressFields(policyPeriod)
    if(policyPeriod.PolicyTerm.StatReportCnt_TDIC == 0){
      _wcstatRecord.ExposureRecord = createExposureRecords(policyPeriod)
    }
    _wcstatRecord.LossRecord = createLossFields(policyPeriod)
    _wcstatRecord.UnitTotalsRecord = createUnitTotalFields(policyPeriod, _wcstatRecord)

    return _wcstatRecord
  }
  /**
   * Assigns and returns values to fields for Report header record.
   **/
  static function createReportheaderFields(): TDIC_WCstatReportHeaderFields {

    var _wcstatReportHeaderFields = new TDIC_WCstatReportHeaderFields()

    _wcstatReportHeaderFields.Label = label
    _wcstatReportHeaderFields.DataProviderEmail = dataProviderEmail
    _wcstatReportHeaderFields.RecordTypeCode = blank
    _wcstatReportHeaderFields.DataTypeCode = dataTypeCode
    _wcstatReportHeaderFields.DataReceiverCode = dataReceiverCode
    _wcstatReportHeaderFields.DateOfDataTransmission = getReportTransmissionDate()
    _wcstatReportHeaderFields.VersionIdentifier = versionIdentifier
    _wcstatReportHeaderFields.SubmissionTypeCode = submissionTypeCode
    _wcstatReportHeaderFields.DataProviderCode = dataProviderCode
    _wcstatReportHeaderFields.NamOfDataProvider = nameOfDataProvider
    _wcstatReportHeaderFields.PhoneNumber = Coercions.makePLongFrom(phoneNumber)
    _wcstatReportHeaderFields.PhoneNumberExtension = blank
    _wcstatReportHeaderFields.FaxNumber = Coercions.makePLongFrom(faxNumber)
    _wcstatReportHeaderFields.ProcessedDate = getDataProcessedDate()
    _wcstatReportHeaderFields.DataProviderStreet = blank
    _wcstatReportHeaderFields.DataProviderCity = blank
    _wcstatReportHeaderFields.DataProviderState = blank
    _wcstatReportHeaderFields.DataProviderZipCode = blank
    _wcstatReportHeaderFields.DataProviderTypeCode = dataProviderTypeCode
    _wcstatReportHeaderFields.Blank = ' '

    return _wcstatReportHeaderFields
  }

  /**
   * Returns Report Transmission Date
   **/
  static function  getReportTransmissionDate(): String {

    var sdf: DateFormat = new SimpleDateFormat("yyDDD")
    return sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")
  }

  /**
   * Returns data processed date.
   **/
  static function getDataProcessedDate() : String{

    var sdf: DateFormat = new SimpleDateFormat("yyyyMMdd")
    return sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")
  }

  /**
   * Returns Term number of a policy.
   **/

  static function getTermNumber(pp : PolicyPeriod) : String{

    if(pp.TermNumber < 10)
    {
      return '0'+ pp.TermNumber  as String
    }
    else {
      return pp.TermNumber as String
    }
  }

  /**
   * Assigns and returns values to fields for policy header record.
   **/
  static function createPolicyReportHeaderFields(pp:PolicyPeriod): TDIC_WCstatPolicyHeaderFields {

    var _wcstatPolicyHeaderFields = new TDIC_WCstatPolicyHeaderFields()

    _wcstatPolicyHeaderFields.CarrierCode = dataProviderCode
    _wcstatPolicyHeaderFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatPolicyHeaderFields.FutureReserved = blank
    _wcstatPolicyHeaderFields.ExposureStateCode = exposureStateCode
    _wcstatPolicyHeaderFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatPolicyHeaderFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatPolicyHeaderFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatPolicyHeaderFields.RecordTypeCode = polRecTypeCode
    _wcstatPolicyHeaderFields.FutureReserved1 = blank
    _wcstatPolicyHeaderFields.PolExpOrCancDate = getPolicyExpDate(pp)
    _wcstatPolicyHeaderFields.RiskIdNum = blank
    _wcstatPolicyHeaderFields.FutureReserved2 = blank
    _wcstatPolicyHeaderFields.OriginalAdminNumIdentifier = blank
    _wcstatPolicyHeaderFields.FutureReserved3 = blank
    _wcstatPolicyHeaderFields.EmpLeasingCode = blank
    _wcstatPolicyHeaderFields.FutureReserved4 = blank
    _wcstatPolicyHeaderFields.ReplacementReportCode = blank
    _wcstatPolicyHeaderFields.BusinessSegmentIdentifier = blank
    _wcstatPolicyHeaderFields.ThirdPartyEntityFeinNum = blank
    _wcstatPolicyHeaderFields.CorrectionTypeCode = blank
    _wcstatPolicyHeaderFields.StateEffectiveDate = blank
    _wcstatPolicyHeaderFields.FeinNumber = getFedEmpIdNumer(pp)
    _wcstatPolicyHeaderFields.FutureReserved5 = blank
    _wcstatPolicyHeaderFields.ThreeYearFixedRatePolInd = blank
    _wcstatPolicyHeaderFields.MultiStatePolInd = blank
    _wcstatPolicyHeaderFields.InterStateRatedPolInd = blank
    _wcstatPolicyHeaderFields.EstimatedAuditCode = getEstimatedAuditCode(pp.Audit.AuditInformation.ActualAuditMethod as String)
    _wcstatPolicyHeaderFields.RetrospectivePolInd = blank
    _wcstatPolicyHeaderFields.CancelledMidTermPolInd = blank
    _wcstatPolicyHeaderFields.ManagedCareOrgPolInd = blank
    _wcstatPolicyHeaderFields.CertifiedHealthCareNetworkPolInd = blank
    _wcstatPolicyHeaderFields.FutureReserved6 = blank
    _wcstatPolicyHeaderFields.TypeOfCovIdCode = typeOfCovIdCode
    _wcstatPolicyHeaderFields.TypeOfPlanIdCode = typeOfPlanIdCode
    _wcstatPolicyHeaderFields.TypeOfNonStandardIdCode = typeOfNonStandardIdCode
    _wcstatPolicyHeaderFields.FutureReserved7 = blank
    _wcstatPolicyHeaderFields.LossesSubjectToDeductibleCode = lossesSubjectToDeductibleCode
    _wcstatPolicyHeaderFields.BasisOfDeductibleCalcCode = basisOfDedCalcCode
    _wcstatPolicyHeaderFields.DeductiblePercentage = dedPercentage
    _wcstatPolicyHeaderFields.DeductibleAmountPerClaimOrAccident = dedAmountPerClaimOrAccident
    _wcstatPolicyHeaderFields.DeductibleAmountAggregate = deductibleAmountAgg
    _wcstatPolicyHeaderFields.PreviousReportLevelCode = getPreviousReportLevelCode(pp)
    _wcstatPolicyHeaderFields.FutureReserved8 = blank
    _wcstatPolicyHeaderFields.PreviousCorrectionSeqNum = blank
    _wcstatPolicyHeaderFields.PreviousCarrierCode = blank
    _wcstatPolicyHeaderFields.PreviousPolicyNumber = blank
    _wcstatPolicyHeaderFields.PreviousPolicyEffectiveDate = blank
    _wcstatPolicyHeaderFields.PreviousExposureStateCode = blank
    _wcstatPolicyHeaderFields.FutureReserved9 = blank
    _wcstatPolicyHeaderFields.ReservedForInsurerUse = blank
    _wcstatPolicyHeaderFields.ReservedForDCOUse = blank
    _wcstatPolicyHeaderFields.BeepEditByPassCode = blank
    _wcstatPolicyHeaderFields.UnitFormatSubmissionCode = unitFormatSubmissionCode

    return _wcstatPolicyHeaderFields
  }

  /**
   * Returns Report level code
   **/
  static function getReportLevelCode(pp : PolicyPeriod) : int{
    var reportLevelCode = pp.PolicyTerm.StatReportCnt_TDIC + 1
    return reportLevelCode
  }

  /**
   * Returns Previous Report level code
   **/
  static function getPreviousReportLevelCode(pp : PolicyPeriod) : int{
    var prevReportLevelCode = pp.PolicyTerm.StatReportCnt_TDIC
    return prevReportLevelCode
  }

  /**
   * Returns estimated audit code.
   **/
  static function getEstimatedAuditCode(str : String) : String{
    switch(str){
      case "Estimated":
          return "U"
      case "Physical":
          return "N"
      case "Voluntary":
          return "N"
        default:
        return " "
    }
  }
  /**
   * Returns policy effective date.
   **/
  static function getPolicyEffDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.PeriodStart).toString().replaceAll("[^0-9]", "")
  }
  /**
   * Returns policy expiration date
   **/
  static function getPolicyExpDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.PeriodEnd).toString().replaceAll("[^0-9]", "")
  }
  /**
   * Returns Fein number
   **/
  // Amended TaxID -- SSN/FEIN filed mapping and amended PersonFEIN_TDIC usage as part of GW-1623 & GW-753
  static function getFedEmpIdNumer(pp:PolicyPeriod): String {
//      if(pp?.PrimaryNamedInsured?.AccountContactRole?.AccountContact?.Contact != null and pp.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.FEIN == null and pp.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.SSN == null)
//        return  ""
//      else {
//        if(pp?.PrimaryNamedInsured?.AccountContactRole?.AccountContact?.Contact != null and pp.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.FEIN != null)
//          return (pp.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.FEIN).replaceAll("[^0-9]", "")
//        else
//          return pp.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.SSN.replaceAll("[^0-9]", "")
//      }
      //20160630 TJT
    return pp.PrimaryNamedInsured.TaxID == null ? "" : pp.PrimaryNamedInsured.TaxID.replaceAll("[^0-9]", "")
    }
  /**
   * Assigns and returns values to fields for name record.
   **/
  static function createNameFields(pp:PolicyPeriod): TDIC_WCstatNameFields {

    var _wcstatNameFields = new TDIC_WCstatNameFields()

    _wcstatNameFields.CarrierCode = dataProviderCode
    _wcstatNameFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatNameFields.FutureReserved = blank
    _wcstatNameFields.ExposureStateCode = exposureStateCode
    _wcstatNameFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatNameFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatNameFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatNameFields.RecordTypeCode = nameRecTypeCode
    _wcstatNameFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcstatNameFields.FutureReserved1 = blank

    return _wcstatNameFields
  }
  /**
   * Assigns and returns values to fields for address record.
   **/
  static function createAddressFields(pp:PolicyPeriod) : TDIC_WCstatAddressFields {

    var _wcstatAddressFields = new TDIC_WCstatAddressFields()

    _wcstatAddressFields.CarrierCode = dataProviderCode
    _wcstatAddressFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatAddressFields.FutureReserved = blank
    _wcstatAddressFields.ExposureStateCode = exposureStateCode
    _wcstatAddressFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatAddressFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatAddressFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatAddressFields.RecordTypeCode = addressRecTypeCode
    _wcstatAddressFields.AddressOfInsured = pp.Policy.Account.AccountHolderContact.PrimaryAddress as String
    _wcstatAddressFields.FutureReserved1 = blank

    return _wcstatAddressFields
  }
  /**
   * Creates multiple exposure records based on count of locations specified and classification codes.
   **/
  static function createExposureRecords(pp:PolicyPeriod):ArrayList<TDIC_WCstatExposureFields>{

    var policyLine = pp.WC7Line
    var splitPeriodCount : int = 0
    var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)
    var exposureRecords = new ArrayList<TDIC_WCstatExposureFields>()
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var stateCosts = partitionCosts.get(elt.Jurisdiction)
      var ratingPeriods = elt.RatingPeriods
      var allModifierVersions = elt.AllModifierVersions
      var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
      allModifierVersions.each( \ p -> p.Pattern.CodeIdentifier)
      var modifiers:List<WC7Modifier>
      if (ratingPeriods.Count > 1) {
        modifiers= allModifierVersions.where( \ mod -> not ((mod.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)
      } else {
        modifiers =  allModifierVersions
      }
      ratingPeriods.each( \ rp ->{
          var periodCosts = stateCosts.byRatingPeriod(rp)
          while(!periodCosts.Empty) {
            var expRecord = createIndividualExposureRecord(pp)
            if(periodCosts.first().ClassCode != null) {
              exposureRecords.add(expRecord)
            }
            var firstCost = periodCosts.first()

            expRecord.ClassificationCode = firstCost.ClassCode
            var premAmount = firstCost.ActualAmount.toNumber().toBigInteger()
            expRecord.PremiumAmount = premAmount.toString().replaceAll("[^0-9]", "")
            expRecord.ManualChargedRate= firstCost?.ActualAdjRate?.toString()?.replaceAll("[^0-9]", "")

            var rpModifiers =  allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary
                and gw.api.util.DateUtil.compareIgnoreTime(m.EffectiveDate, rp.Start) == 0
                and gw.api.util.DateUtil.compareIgnoreTime(m.ExpirationDate, rp.End) == 0 )

            var rpModifiersCount = (allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)).Count
            var expMod= rpModifiers.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ExpMod")
            expRecord.RateEffDate= sdf.format(elt.AnniversaryDate).toString().replaceAll("[^0-9]", "")
            expRecord.ExperienceModEffDate = sdf.format(expMod.EffectiveDate).toString().replaceAll("[^0-9]", "")

            if(firstCost.LocationNum != null) {
              expRecord.ExposureAmount = firstCost.Basis as long
              expRecord.ExposureActorCovCode = 01
              expRecord.ExperienceModFactor= expMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", "")
              if(splitPeriodCount < rpModifiersCount){
                expRecord.SplitPeriodCode = splitPeriodCount
                splitPeriodCount++
              }
            }
            else {
              expRecord.ExposureAmount = 0000000000
              expRecord.ExposureActorCovCode = 00
              expRecord.ExperienceModFactor= '0000'
            }
            periodCosts.remove(firstCost)
          }
      })
     })
    return exposureRecords
  }

  /**
   * Assigns and returns values to fields for exposure records.
   **/
  static function createIndividualExposureRecord(pp:PolicyPeriod): TDIC_WCstatExposureFields {

    var _wcstatExposureFields = new TDIC_WCstatExposureFields()

    _wcstatExposureFields.CarrierCode = dataProviderCode
    _wcstatExposureFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatExposureFields.FutureReserved = blank
    _wcstatExposureFields.ExposureStateCode = exposureStateCode
    _wcstatExposureFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatExposureFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatExposureFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatExposureFields.RecordTypeCode = exposureRecTypeCode
    _wcstatExposureFields.FutureReserved1 = blank
    _wcstatExposureFields.FutureReserved2 = blank
    _wcstatExposureFields.FutureReserved3 = blank
    _wcstatExposureFields.RatingTierIdCode = blank
    _wcstatExposureFields.FutureReserved4 = blank
    _wcstatExposureFields.UpdateTypeCode = updateTypeCode
    _wcstatExposureFields.FutureReserved5 = blank
    _wcstatExposureFields.FutureReserved6 = blank

    return _wcstatExposureFields
  }
  /**
   * retrieves data from Claimcenter through webservice call, assigns and returns values to fields for exposure records.
   **/
  static function createLossFields(pp:PolicyPeriod) : ArrayList<TDIC_WCstatLossFields> {

    var endp = new wsi.remote.gw.webservice.cc.cc1000.pcclaimsearchintegrationapi.PCClaimSearchIntegrationAPI()

    var sdFormat: DateFormat = new SimpleDateFormat("yyMMdd")
    var pcClaimSearchCriteria = new PCClaimSearchCriteria()
    pcClaimSearchCriteria.BeginDate = pp.PeriodStart
    pcClaimSearchCriteria.EndDate = pp.PeriodEnd
    pcClaimSearchCriteria.PolicyNumbers = new PCClaimSearchCriteria_PolicyNumbers()
    pcClaimSearchCriteria.PolicyNumbers.Entry.add(pp.PolicyNumber)

    var claims = endp.searchForClaims(pcClaimSearchCriteria)

    var records = new ArrayList<TDIC_WCstatLossFields>()

    claims.each( \ elt -> {
    var _wcstatLossFields = new TDIC_WCstatLossFields()
      records.add(_wcstatLossFields )
    _wcstatLossFields.CarrierCode = dataProviderCode
    _wcstatLossFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatLossFields.FutureReserved = blank
    _wcstatLossFields.ExposureStateCode = exposureStateCode
    _wcstatLossFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatLossFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatLossFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatLossFields.RecordTypeCode = lossRecTypeCode
    _wcstatLossFields.FutureReserved1 = blank
    _wcstatLossFields.ClassificationCode = elt.ClassificationCode
    _wcstatLossFields.FutureReserved2 = blank
    _wcstatLossFields.ClaimCount = claimCount
    _wcstatLossFields.AccidentDate = sdFormat.format(elt.LossDate).toString().replaceAll("[^0-9]", "")
    _wcstatLossFields.ClaimNumber = elt.ClaimNumber.toString().replaceAll("[^0-9]", "")
    _wcstatLossFields.ClaimOrStatusCode = getClaimStatus(elt.Status)
    _wcstatLossFields.WeeklyWageAmount = elt.WeeklyWageAmount
    _wcstatLossFields.InjuryCode = getInjuryCode(elt.InjuryCode, elt.ScheduledIndemnityPercentageOfDisability as int)
    _wcstatLossFields.CatastropheNumber = elt.CatastropheNumber as int
    _wcstatLossFields.IncurredIndemnityAmount = elt.IncurredIndemnityAmount
    _wcstatLossFields.IncurredMedicalAmount = elt.IncurredMedicalAmount
    _wcstatLossFields.SSN = blank
    _wcstatLossFields.FutureReserved3 = blank
    _wcstatLossFields.CasesNumberAssignedByState = blank
    _wcstatLossFields.UpdateTypeCode = updateTypeCode
    _wcstatLossFields.FutureReserved4 = blank
    _wcstatLossFields.LossCovActCode = lossCovActCode
    //_wcstatLossFields.TypeOfLossCode = getLossCode(elt.TypeOfLossCode)
    _wcstatLossFields.TypeOfRecoveryCode = 01
    _wcstatLossFields.TypeofClaimCode = 01
    //_wcstatLossFields.TypeOfSettlementCode = getSettlementCode(elt.TypeOfSettlementCode)
    //_wcstatLossFields.TotalIncurredVocRehabAmnt = elt.TotalIncurredVocRehabAmnt
    _wcstatLossFields.JurStateCode = jurStateCode
    _wcstatLossFields.ManagedCareOrgTypeCode = blank
    //_wcstatLossFields.PartofBodyCode = elt.PartOfBodyCode
    //_wcstatLossFields.NatureOfInjuryCode = elt.NatureOfInjuryCode
    //_wcstatLossFields.CauseOfInjuryCode = elt.CauseOfInjuryCode
    _wcstatLossFields.OccupationDescription = blank
    //_wcstatLossFields.VocationalRehabInd = getVocRehabInd(elt.VocationalRehabInd)
    _wcstatLossFields.LumpSumIndicator = blank
    //_wcstatLossFields.FradulentClaimCode = getFraudulentClaimCode(elt.FraudulentClaimCode)
    _wcstatLossFields.FutureReserved5 = blank
    //_wcstatLossFields.PaidIndemnityAmount = elt.PaidIndemnityAmount
    //_wcstatLossFields.PaidMedicalAmount = elt.PaidMedicalAmount
    _wcstatLossFields.TypeOfLossCode = getLossCode(elt.TypeOfLossCode)
    _wcstatLossFields.TypeOfRecoveryCode = 01
    _wcstatLossFields.TypeofClaimCode = 01
    _wcstatLossFields.TypeOfSettlementCode = getSettlementCode(elt.TypeOfSettlementCode)
    _wcstatLossFields.TotalIncurredVocRehabAmnt = elt.TotalIncurredVocRehabAmnt
    _wcstatLossFields.JurStateCode = jurStateCode
    _wcstatLossFields.ManagedCareOrgTypeCode = blank
    _wcstatLossFields.PartofBodyCode = elt.PartOfBodyCode
    _wcstatLossFields.NatureOfInjuryCode = elt.NatureOfInjuryCode
    _wcstatLossFields.CauseOfInjuryCode = elt.CauseOfInjuryCode
    _wcstatLossFields.OccupationDescription = blank
    _wcstatLossFields.VocationalRehabInd = getVocRehabInd(elt.VocationalRehabInd)
    _wcstatLossFields.LumpSumIndicator = blank
    _wcstatLossFields.FradulentClaimCode = getFraudulentClaimCode(elt.FraudulentClaimCode)
    _wcstatLossFields.FutureReserved5 = blank
    _wcstatLossFields.PaidIndemnityAmount = elt.PaidIndemnityAmount
    _wcstatLossFields.PaidMedicalAmount = elt.PaidMedicalAmount
    _wcstatLossFields.ClaimantsAttorneyFeesIncurredAmnt = blank
    _wcstatLossFields.EmployersAttorneyFeesIncurredAmnt = blank
    _wcstatLossFields.DeductibleReimbursementAmount = blank
    _wcstatLossFields.TotalGrossIncurredAmount = 000000000
    _wcstatLossFields.FutureReserved6 = blank
    //_wcstatLossFields.PaidAllocatedLossAdjExpenseAmnt = elt.PaidAllocatedLossAdjExpenseAmnt
    _wcstatLossFields.IncurredAllocatedLossAdjExpenseAmnt = blank
    //_wcstatLossFields.ScheduledIndemnityPercentageOfDisability = elt.ScheduledIndemnityPercentageOfDisability as int
    _wcstatLossFields.PaidAllocatedLossAdjExpenseAmnt = elt.PaidAllocatedLossAdjExpenseAmnt
    _wcstatLossFields.IncurredAllocatedLossAdjExpenseAmnt = blank
    _wcstatLossFields.ScheduledIndemnityPercentageOfDisability = elt.ScheduledIndemnityPercentageOfDisability as int
    })

    return records
  }
  /**
   * Returns claim status
   **/
  static function getClaimStatus(str : String) : String {
    switch(str){
      case "Open":
          return "0"
      case "Closed":
          return "1"
      case "resolved":
          return "3"
        default:
        return " "
    }
  }
  /**
   * Returns injury codes.
   **/
  static function getInjuryCode(str : String, impRate : int) : String {
    switch(str){
      case "fatal":
          return "01"
      case "perm-total":
          return "02"
      case "major-gen":
          return "03"
      case "minor":
          return "04"
      case "temporary":
          return "05"
      case "medical_only":
          return "06"
      case "contract-medical":
          return "07"
      default:
          return "00"
    }
  }
  /**
   * Returns loss code.
   **/
  static function getLossCode(str : String) : String {
    switch(str){
      case "specific":
          return "01"
      case "occupational":
          return "02"
      case "multiple":
          return "03"
        default:
        return "00"
    }
  }
  /**
   * Returns settlement code.
   **/
  static function getSettlementCode(str : String) : String {
    switch(str){
      case "stipaward":
          return "03"
      case "dismissal":
          return "05"
      case "cacompromise":
          return "06"
      case "other":
          return "09"
        default:
        return "00"
    }
  }
  /**
   * Returns vocational rehabilitation indicator.
   **/
  static function getVocRehabInd(str : String) : String {
    switch(str){
      case "true":
          return "Y"
      case "false":
          return "N"
        default:
        return " "
    }
  }
  /**
   * Returns fraudulent claim code.
   **/
  static function getFraudulentClaimCode(str : String) : String {
    switch(str){
      case "Yes":
          return "01"
      case "No":
          return "00"
        default:
        return " "
    }
  }

  /**
   * Assigns and returns values to fields for unit totals record.
   **/
  static function createUnitTotalFields(pp: PolicyPeriod, lossRecord:TDIC_WCstatRecordTypes) : TDIC_WCstatUnitTotalFields {

    var _wcstatUnitTotalFields = new TDIC_WCstatUnitTotalFields()

    _wcstatUnitTotalFields.CarrierCode = dataProviderCode
    _wcstatUnitTotalFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcstatUnitTotalFields.FutureReserved = blank
    _wcstatUnitTotalFields.ExposureStateCode = exposureStateCode
    _wcstatUnitTotalFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcstatUnitTotalFields.ReportLevelCode = getReportLevelCode(pp)
    _wcstatUnitTotalFields.CorrectionSeqNum = correctionSequenceNumber
    _wcstatUnitTotalFields.RecordTypeCode = unitStatRecTypeCode
    _wcstatUnitTotalFields.ExposurePayrolltotal = getExposureTotal(pp) as long//pp.WC7Line.WC7Costs*.Basis.sum()
    _wcstatUnitTotalFields.FutureReserved1 = blank
    _wcstatUnitTotalFields.SubjectPremiumTotal = getSubjectPremium(pp) as long
    _wcstatUnitTotalFields.StandardPremiumTotal = getStandardPremium(pp) as long
    _wcstatUnitTotalFields.ClaimCountTotal = lossRecord.LossRecord.Count
    _wcstatUnitTotalFields.IncurredIndemnityAmountTotal = getIncurredIndemnityAmountTotal(lossRecord) as long
    _wcstatUnitTotalFields.IncurredMedicalAmountTotal = getIncurredMedicalAmountTotal(lossRecord) as long
    _wcstatUnitTotalFields.RecordsInUnitReportTotal = (getRecordsInUnitReportTotal(lossRecord)  as long) as int
    _wcstatUnitTotalFields.PreviouslyReportedCode = blank
    _wcstatUnitTotalFields.FutureReserved2 = blank
    _wcstatUnitTotalFields.PaidIndemnityAmountTotal = blank
    _wcstatUnitTotalFields.PaidMedicalAmountTotal = blank
    _wcstatUnitTotalFields.ClaimantsAttorneyFeesIncAmntTotal = blank
    _wcstatUnitTotalFields.EmployersAttorneyFeesIncAmntTotal = blank
    _wcstatUnitTotalFields.PaidAllocatedLossAdjExpenseAmntTotal = blank
    _wcstatUnitTotalFields.IncurredAllocatedLossAdjExpenseAmntTotal = blank
    _wcstatUnitTotalFields.FutureReserved3 = blank

    return _wcstatUnitTotalFields
  }

  static function getExposureTotal(pp : PolicyPeriod): double {
    var sum = 0.0
    if(pp.PolicyTerm.StatReportCnt_TDIC == 0){
      sum = pp.Audit.AuditInformation.AuditedBasisAmount
      return sum
    }
    else {
      return sum
    }
  }
  /**
   * Returns Incurred indemnity amount total.
   **/
  static function getIncurredIndemnityAmountTotal(lossRec : TDIC_WCstatRecordTypes): double {
    var sum = 0.0
    lossRec.LossRecord.each( \ elt -> {sum = sum + elt.IncurredIndemnityAmount})
    return sum
  }
  /**
   * Returns Incurred medical amount total.
   **/
  static function getIncurredMedicalAmountTotal(lossRec : TDIC_WCstatRecordTypes): double {
    var sum = 0.0
    lossRec.LossRecord.each( \ elt -> {sum = sum + elt.IncurredMedicalAmount})
    return sum
  }
  /**
   * Returns records in unit report total.
   **/
  static function getRecordsInUnitReportTotal(lossRec : TDIC_WCstatRecordTypes): long {
    var sum = 1
    sum += lossRec.getCount()
    return sum
  }
  /**
   * Returns subject premium total.
   **/
  static function getSubjectPremium(pp:PolicyPeriod): double {

    var total = 0.0
    if(pp.PolicyTerm.StatReportCnt_TDIC == 0){
      var policyLine = pp.WC7Line
      var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)

      var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
      var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

      jurisdictions.each( \ elt -> {
        var stateCosts = partitionCosts.get(elt.Jurisdiction)
        var ratingPeriods = elt.RatingPeriods

        ratingPeriods.each( \ rp ->{
            var periodCosts = stateCosts.byRatingPeriod(rp)
            var stdPremiums = periodCosts.getStandardPremiums(pp.PreferredSettlementCurrency)

            stdPremiums.each( \ ee -> {
              if(ee.Description.endsWithIgnoreCase("Subject Premium") ){
                total += (ee.Total.Amount) as double
              }
            })
        })
      })
      return total
    }
    else {
      return total
    }
  }
  /**
   * Returns standard premium total.
   **/
  static function getStandardPremium(pp:PolicyPeriod): double {

    var total = 0.0
    if(pp.PolicyTerm.StatReportCnt_TDIC == 0){
      var policyLine = pp.WC7Line
      var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)

      var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
      var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

      jurisdictions.each( \ elt -> {
        var stateCosts = partitionCosts.get(elt.Jurisdiction)
        var ratingPeriods = elt.RatingPeriods

        ratingPeriods.each( \ rp ->{
          var periodCosts = stateCosts.byRatingPeriod(rp)
          var stdPremiums = periodCosts.getStandardPremiums(pp.PreferredSettlementCurrency)

          stdPremiums.each( \ ee -> {
            if(ee.Description.endsWithIgnoreCase("Standard Premium") ){
              total += (ee.Total.Amount) as double
            }
          })
        })
      })
      return total
    }
    else {
      return total
    }
  }
  /**
   * Assigns and returns values to fields for report trailer record.
   **/
  static function createReportTrailerrecord(recordCount:int,unitTotalsRecordCount:int): TDIC_WCstatReportTrailerFields {

    var _wcstatReportTrailerFields = new TDIC_WCstatReportTrailerFields()

    _wcstatReportTrailerFields.Filler = filler
    _wcstatReportTrailerFields.RecordTypeCode = trailerRecTypeCode
    _wcstatReportTrailerFields.DetailRecordCountTotal = recordCount
    _wcstatReportTrailerFields.UnitReportsSubmittedTotal = unitTotalsRecordCount
    _wcstatReportTrailerFields.PrimaryEffectiveYear = getPrimaryEffectiveYear()
    _wcstatReportTrailerFields.PrimaryEffectiveMonth = getPrimaryEffectiveMonth()
    _wcstatReportTrailerFields.ICRTotal = blank
    _wcstatReportTrailerFields.FutureReserved = blank

    return _wcstatReportTrailerFields
  }
  /**
   * Returns primary effective year.
   **/
  static function getPrimaryEffectiveYear() : String{

    var sdf: DateFormat = new SimpleDateFormat("yyyy")
    return sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")
  }
  /**
   * Returns primary effective month.
   **/
  static function getPrimaryEffectiveMonth() : String{

    var sdf: DateFormat = new SimpleDateFormat("yyyyMMdd")
    var effMonth = sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")
    return effMonth.substring(4,7)
  }
}