package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/18/14
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatLossFields extends TDIC_FlatFileLineGenerator{

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _exposureStateCode : int as ExposureStateCode
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _reportLevelCode : int as ReportLevelCode
  var _correctionSeqNum : String as CorrectionSeqNum
  var _recordTypeCode : int as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _classificationCode : String as ClassificationCode
  var _futureReserved2 : String as FutureReserved2
  var _claimCount : String as ClaimCount
  var _accidentDate : String as AccidentDate
  var _claimNumber : String as ClaimNumber
  var _ClaimOrStatusCode : String as ClaimOrStatusCode
  var _weeklyWageAmount : long as WeeklyWageAmount
  var _injuryCode : String as InjuryCode
  var _catastropheNumber : int as CatastropheNumber
  var _incurredIndemnityAmount : long as IncurredIndemnityAmount
  var _incurredMedicalAmount : long as IncurredMedicalAmount
  var _ssn : String as SSN
  var _futureReserved3 : String as FutureReserved3
  var _caseNumberAssignedByState : String as CasesNumberAssignedByState
  var _updateTypeCode : String as UpdateTypeCode
  var _futureReserved4 : String as FutureReserved4
  var _lossCovActCode : int as LossCovActCode
  var _typeOfLossCode : String as TypeOfLossCode
  var _typeOfRecoveryCode : int as TypeOfRecoveryCode
  var _typeOfClaimCode : int as TypeofClaimCode
  var _typeOfSettlementCode : String as TypeOfSettlementCode
  var _totalIncurredVocRehabAmnt : long as TotalIncurredVocRehabAmnt
  var _jurStateCode : int as JurStateCode
  var _managedCareOrgTypeCode : String as ManagedCareOrgTypeCode
  var _partOfBodyCode : String as PartofBodyCode
  var _natureOfInjuryCode : String as NatureOfInjuryCode
  var _causeOfInjuryCode : String as CauseOfInjuryCode
  var _occupationDescription : String as OccupationDescription
  var _vocationalrehabInd : String as VocationalRehabInd
  var _lumpSumInd : String as LumpSumIndicator
  var _fradulentClaimCode : String as FradulentClaimCode
  var _futureReserved5 : String as FutureReserved5
  var _paidIndemnityAmount : long as PaidIndemnityAmount
  var _paidMedicalAmount : long as PaidMedicalAmount
  var _claimantsAttorneyFeesIncurredAmnt : String as ClaimantsAttorneyFeesIncurredAmnt
  var _employersAttorneyFeesIncurredAmnt : String as EmployersAttorneyFeesIncurredAmnt
  var _deductibleReimbursementAmount : String as DeductibleReimbursementAmount
  var _totalGrossIncurredAmount : long as TotalGrossIncurredAmount
  var _futureReserved6 : String as FutureReserved6
  var _paidAllocatedLossAdjExpenseAmnt : long as PaidAllocatedLossAdjExpenseAmnt
  var _incurredAllocatedLossAdjExpenseAmnt : String as IncurredAllocatedLossAdjExpenseAmnt
  var _scheduledindemnityPercentageOfDisability : int as ScheduledIndemnityPercentageOfDisability

}