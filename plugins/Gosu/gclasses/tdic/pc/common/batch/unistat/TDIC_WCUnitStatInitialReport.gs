package tdic.pc.common.batch.unistat

uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses tdic.pc.common.batch.unistat.util.TDIC_WCstatBatchUtil
uses tdic.pc.common.batch.unistat.helper.TDIC_WCstatRecordTypes
uses java.util.ArrayList
uses gw.api.database.Query
uses tdic.pc.common.batch.unistat.util.TDIC_WCstatUtil
uses tdic.pc.common.batch.unistat.helper.TDIC_WCStatCompleteRecord
uses java.lang.Exception
uses java.sql.SQLException
uses com.tdic.util.misc.EmailUtil
uses java.io.IOException
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/30/15
 * Time: 4:38 PM
 * Batch for Workers Compensation unit statistical reporting, Creates flat file and reports Initial report for all the policies after 18 months of inception date.
 */
class TDIC_WCUnitStatInitialReport extends BatchProcessBase{

  /**
   *Logger for this Batch
   */
  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")
  private static final var BATCH_USER = "iu"
  //GINTEG-213: Reading properties from PropertyUtil rather from CacheManager
  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail")
  public static final var EMAIL_SUBJECT : String = "Policy not reported on Unit Stat Report on Server " + gw.api.system.server.ServerUtil.ServerId

  construct() {
    super(BatchProcessType.TC_WCSTATINITIALREPORTBATCH)
  }

  override function doWork() {

    doWorkFor18MonthsReport()
  }

  /**
   * Specifies the policy effective date time frame, Retrieves policies that meets time frame criteria, generates the content to be reported to WCSTAT initial report.
   */
  private function doWorkFor18MonthsReport(){

    try
    {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {

        var wcstatRecords:ArrayList<TDIC_WCstatRecordTypes> = new ArrayList<TDIC_WCstatRecordTypes>()

        var dat = gw.api.util.DateUtil.currentDate()
        //GW3035 - Adding additional 5 months to include audits that completed beyond 18 months
        var pStart = dat.addMonths(-1*24).FirstDayOfMonth.trimToMidnight()
        _logger.info("Policy effective start date :" + pStart)
        var pEnd = pStart.addMonths(6).trimToMidnight()
        _logger.info("Policy effective end date :" + pEnd)

        var policyQuery = Query.make(PolicyPeriod)
        var auditQuery = policyQuery.join("Job")
        auditQuery.compare("Subtype",Equals,typekey.Job.TC_AUDIT)
        var ppJoinQuery = auditQuery.join("Policy")
        var ptJoinQuery = policyQuery.join("PolicyTerm")
        ptJoinQuery.compare("StatReportComp_TDIC",Equals,false)
        ptJoinQuery.compare("StatReportCnt_TDIC",Equals,0)
        ppJoinQuery.compare("IssueDate",NotEquals,null)
        policyQuery.compare("PeriodStart",GreaterThanOrEquals,pStart)
        policyQuery.compare("PeriodStart",LessThan,pEnd)

        var res = ppJoinQuery.select().toSet()
        res.each( \ elt -> {
          elt = bundle.add(elt)
          if(elt == elt.CompletedNotReversedFinalAudits.firstWhere( \ faud -> faud.DisplayStatus.equalsIgnoreCase("Completed")).Audit.PolicyPeriod) {
            var record = TDIC_WCstatUtil.createPayload(elt)
            wcstatRecords.add(record)

            if(record.LossRecord.where( \ lr -> lr.ClaimOrStatusCode.equalsIgnoreCase("0")).Count>0) {
              elt.PolicyTerm.StatReportComp_TDIC=false
              elt.PolicyTerm.StatReportCnt_TDIC +=1
            }
            else {
              elt.PolicyTerm.StatReportComp_TDIC=true
              elt.PolicyTerm.StatReportCnt_TDIC +=1
            }
          }
          else {
            EmailUtil.sendEmail(EMAIL_RECIPIENT, EMAIL_SUBJECT, "Policy " + elt.PolicyNumber + " is not reported on Unit stat report created on " + gw.api.util.DateUtil.currentDate() + " as final Audit is not completed for this policy.")
          }
        })

        var completeRecord = new TDIC_WCStatCompleteRecord()
        completeRecord.ReportHeaderRecord = TDIC_WCstatUtil.createReportheaderFields()
        completeRecord.WCStatRecords = wcstatRecords
        var recordCount=1
        wcstatRecords.each( \ elt -> {
          recordCount +=elt.getCount()
        })

        completeRecord.ReportTrailerRecord = TDIC_WCstatUtil.createReportTrailerrecord(recordCount,wcstatRecords.Count)
        TDIC_WCstatBatchUtil.writeFlatFile(completeRecord, "InitialReport")
      },BATCH_USER)
      EmailUtil.sendEmail(TDIC_WCstatBatchUtil.WC_STAT_POL_NOTIFICATION_EMAIL, "WCSTAT Initial Report batch job completed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Initial Report batch job has been completed successfully.")
    }
    catch (io:IOException) {
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - IOException= " + io)
      if(EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else{
        EmailUtil.sendEmail(EMAIL_RECIPIENT, "WCSTAT Initial Report batch job failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Initial Report batch job failed due to error:" + io.LocalizedMessage)
      }
      throw io
    }catch (sql:SQLException) {
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - SQLException= " + sql)
      if(EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(EMAIL_RECIPIENT, "WCSTAT Initial Report batch job failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Initial Report batch job failed due to error:" + sql.LocalizedMessage)
      }
      throw sql
    }catch(ex:Exception){
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - Exception= " + ex)
      if(EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(EMAIL_RECIPIENT, "WCSTAT Initial Report batch job failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Initial Report batch job failed due to error" + ex.LocalizedMessage)
      }
      throw ex
    }
  }
}