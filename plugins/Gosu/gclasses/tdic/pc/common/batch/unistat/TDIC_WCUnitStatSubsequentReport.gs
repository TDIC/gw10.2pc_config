package tdic.pc.common.batch.unistat

uses gw.processes.BatchProcessBase
uses tdic.pc.common.batch.unistat.util.TDIC_WCstatBatchUtil
uses com.tdic.util.misc.EmailUtil
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/23/15
 * Time: 6:08 PM
 * Batch for Workers Compensation unit statistical reporting subsequent reports.
 */
class TDIC_WCUnitStatSubsequentReport extends BatchProcessBase{

 private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")
  construct() {
   super(BatchProcessType.TC_WCSTATSUBSEQUENTREPORTBATCH)
  }

  override function doWork() {
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(31, "SubsequentReport2")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(43,"SubsequentReport3")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(55,"SubsequentReport4")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(67,"SubsequentReport5")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(79,"SubsequentReport6")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(91,"SubsequentReport7")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(103,"SubsequentReport8")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(115,"SubsequentReport9")
    TDIC_WCstatBatchUtil.doWorkForMonthsReport(127,"SubsequentReport10")
    EmailUtil.sendEmail(TDIC_WCstatBatchUtil.WC_STAT_POL_NOTIFICATION_EMAIL, "WCSTAT Subsequent Report batch job completed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Subsequent Report batch job has been completed successfully.")
  }
}