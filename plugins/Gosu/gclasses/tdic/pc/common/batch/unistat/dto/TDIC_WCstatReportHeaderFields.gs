package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/21/14
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatReportHeaderFields extends TDIC_FlatFileLineGenerator {
  var _label : String as Label
  var _dataProviderEmail : String as DataProviderEmail
  var _recordTypeCode : String as RecordTypeCode
  var _dataTypeCode : String as DataTypeCode
  var _dataReceiverCode : int as DataReceiverCode
  var _dateOfDataTransmission : String as DateOfDataTransmission
  var _versionIdentifier : String as VersionIdentifier
  var _submissionTypeCode : String as SubmissionTypeCode
  var _dataProviderCode : long as DataProviderCode
  var _nameOfDataProviderContact : String as NamOfDataProvider
  var _phoneNumber : long as PhoneNumber
  var _phoneNumberExtension : String as PhoneNumberExtension
  var _faxNumber : long as FaxNumber
  var _processedDate : String as ProcessedDate
  var _dataProviderStreet : String as DataProviderStreet
  var _dataProviderCity : String as DataProviderCity
  var _dataProviderState : String as DataProviderState
  var _dataProviderZipCode : String as DataProviderZipCode
  var _datProviderTypeCode : String as DataProviderTypeCode
  var _blank : String as Blank
}