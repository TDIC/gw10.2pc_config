package tdic.pc.common.batch.multilinediscount

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses com.tdic.util.properties.PropertyUtil
uses tdic.pc.common.batch.multilinediscount.dto.MultiLinePolicyBatchDTO
uses com.tdic.util.database.DatabaseManager

uses java.sql.Connection
uses java.sql.SQLException
uses java.sql.PreparedStatement

class TDIC_MultiLinePolicyBatch extends BatchProcessBase {

  /**
   * The logger for this Batch.
   */
  private static final var LOGGER = LoggerFactory.getLogger("REVIEW_MULITLINE_DISCOUNT")
  /**
   * Query to insert contents from 'ACTAS400CA.csv'file into table 'AS400PolicyDetails'
   */
  private static final var _prepSqlStmt = "INSERT INTO MultiLinePolicy(ADA,PolNumbr,LastName,FirstName,Lob,BaseState) VALUES(?,?,?,?,?,?)"
  /**
   * Query to cleanup data in 'AS400PolicyDetails' table
   */
  private static final var _cleanupdata = "DELETE FROM MultiLinePolicy"
  private static final var _intdburl = PropertyUtil.getInstance().getProperty("IntDBURL")

  /**
   * The logger tag for this Batch.
   */
  private static final var LOG_TAG = TDIC_MultiLinePolicyBatch

  construct() {
    super(BatchProcessType.TC_MULTILINEPOLICYBATCH_EXT)
  }

  /**
   * Returns true, since this batch process can be terminated.
   */
  override function requestTermination() : boolean {
    super.requestTermination()
    return true
  }

  override function doWork() {
    var _con : Connection = null
    var _prepStatement : PreparedStatement = null
    var logTag = LOG_TAG + "doWork() - "
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Entering")
    }
    try {
      _con = DatabaseManager.getConnection(_intdburl)
      _prepStatement = _con.prepareStatement(_cleanupdata)
      _prepStatement.execute()
      _con.commit()
      var boundPolicies = Query.make(PolicyPeriod).compare(PolicyPeriod#Status, Relop.Equals, PolicyPeriodStatus.TC_BOUND).select()
      foreach (boundPolicy in boundPolicies) {
        boundPolicy = boundPolicy.getSlice(boundPolicy.EditEffectiveDate)
        var _adaList = new ArrayList<String>()
        _adaList.add(boundPolicy.PrimaryNamedInsured.AccountContactRole.AccountContact?.Contact?.ADANumber_TDICOfficialID)
        var ownerOfficerADANumber : String;
        if (boundPolicy.WC7LineExists) {
          for (anOwnerOfficer in boundPolicy.WC7Line.WC7PolicyOwnerOfficers) {
            if (shouldIncludeADANumber(anOwnerOfficer)) {
              ownerOfficerADANumber = anOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
              if (ownerOfficerADANumber != null) {
                _adaList.add(ownerOfficerADANumber)
              }
            }
          }
        }

        foreach (ada in _adaList) {
          if (ada != null) {
            var _multiLinePolicyBatchDTO = new MultiLinePolicyBatchDTO()
            _multiLinePolicyBatchDTO.ADA = ada
            _multiLinePolicyBatchDTO.PolNumbr = boundPolicy.PolicyNumber
            _multiLinePolicyBatchDTO.LastName = boundPolicy.PrimaryNamedInsured.LastName
            _multiLinePolicyBatchDTO.FirstName = boundPolicy.PrimaryNamedInsured.FirstName
            if (boundPolicy.BOPLineExists) {
              _multiLinePolicyBatchDTO.Lob = "CP"
            } else if (boundPolicy.GLLineExists) {
              _multiLinePolicyBatchDTO.Lob = "PL"
            } else if (boundPolicy.WC7LineExists) {
              _multiLinePolicyBatchDTO.Lob = "WC"
            }
            _multiLinePolicyBatchDTO.BaseState = boundPolicy.BaseState.Code
            _prepStatement = _con.prepareStatement(_prepSqlStmt)
            insertRecords(_multiLinePolicyBatchDTO, _prepStatement)
            _con.commit()
          }
        }

      }
    } catch (sql : SQLException) {
      LOGGER.debug("TDIC_MultiLinePolicyBatch - SQLException While Closing Connection !!!" + sql.toString())
      throw sql
    } catch (e : Exception) {
      LOGGER.debug("TDIC_MultiLinePolicyBatch - Exception While Closing Connection !!!" + e.toString())
      throw e
    } finally {
      try {
        if (_prepStatement != null) _prepStatement.close()
        if (_con != null) _con.close()
      } catch (fe : Exception) {
        LOGGER.debug("TDIC_MultiLinePolicyBatch - Error While Closing Connection !!!")
        throw fe
      }
    }
  }

  /**
   * Returns true if the ADA Number of the specified OwnerOfficer should be included in the ADA Numbers for this multiline discount business specification; false otherwise.
   */
  private function shouldIncludeADANumber(anOwnerOfficer : WC7PolicyOwnerOfficer) : boolean {
    return (anOwnerOfficer.IntrinsicType == PolicyEntityOwner_TDIC
        or (anOwnerOfficer.WC7OwnershipPct != null and anOwnerOfficer.WC7OwnershipPct.intValue() > 0))
  }

  /**
   * Below function will insert the details in 'ACTAS400CA.csv' file into table 'AS400PolicyDetails'
   */
  private function insertRecords(as400FieldsMap : MultiLinePolicyBatchDTO, prepardStmnt : PreparedStatement) {
    prepardStmnt.setString(1, as400FieldsMap.ADA)
    prepardStmnt.setString(2, as400FieldsMap.PolNumbr)
    prepardStmnt.setString(3, as400FieldsMap.LastName)
    prepardStmnt.setString(4, as400FieldsMap.FirstName)
    prepardStmnt.setString(5, as400FieldsMap.Lob)
    prepardStmnt.setString(6, as400FieldsMap.BaseState)
    prepardStmnt.executeUpdate()
  }
}