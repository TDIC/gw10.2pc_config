package tdic.pc.common.admindata

uses gw.api.database.Relop
uses gw.api.system.DatabaseDependenciesGateway
uses gw.api.system.PCLoggerCategory
uses gw.api.system.server.ServerModeUtil
uses gw.api.system.server.ServerUtil
uses gw.api.util.DateUtil
uses gw.api.webservice.importTools.ImportToolsImpl
uses gw.pl.util.FileUtil
uses gw.pl.persistence.core.Bundle
uses java.io.BufferedReader
uses java.io.File
uses java.io.FilenameFilter
uses com.guidewire.modules.file.VirtualDirectory
uses com.guidewire.bizrules.config.BizRulesConfigResourceKeys
uses com.guidewire.bizrules.system.dependency.BizRulesDependencies
uses com.guidewire.bizrules.management.exim.file.RuleFileFormatSupport
uses com.guidewire.bizrules.management.BizRulesVersionControllerInternal
uses com.guidewire.bizrules.management.exim.RuleMetadata
uses com.guidewire.pl.system.bundle.EntityBundleImpl
uses com.guidewire.pl.system.dependency.PLDependencies
uses gw.bizrules.system.BizRulesConfigParameters
uses gw.bizrules.BizRulesLoggerCategory

/**
 * BrittoS - AdminData/UW rules loader for Dev & Test Environments.
 */
class AdminDataLoader {

  private static var _dir = "/config/datafiles/AdminData-v10"
  private static var _configurationDir = gw.api.util.ConfigAccess.getModuleRoot("configuration")
  private static var _prodAdminData = "pc_AdminData_20190712.xml"
  private static var _prodBizRules = "UWRule-20190712-v8-PROD.gwrules"
  private static var _adminDataParameter = "tdic_admin_data_loader"
  private static var _bizrulesParameter = "tdic_bizrules_loader"
  private static var _adminImportFileList = "importfiles.txt"
  private static var _fileSeparator = "\\"
  private static var _commentSeparator = "#"
  private static var _uwRuleExt = "gwrules"
  private static var _comma = ","

  public static function Start() {
    //PCLoggerCategory.IMPORT.info("AdminDataLoader : Start")
    if(ServerModeUtil.isDev() or ServerModeUtil.isTest()) {
      //importAdminData()
      importUWRules()
    } else {
      PCLoggerCategory.IMPORT.info("AdminDataLoader : Admin data import is disabled for ${ServerUtil.Env}")
    }
    //PCLoggerCategory.IMPORT.info("AdminDataLoader : End")
  }

  private static function importUWRules() {
    PCLoggerCategory.IMPORT.info("importUWRules : Start")
    if(BizRulesConfigParameters.BizRulesImportBootstrapRules.Value
        and not DatabaseDependenciesGateway.getDatabase().isStartedFromEmptyDB()
        and DatabaseDependenciesGateway.getDatabase().wasUpgraded()) {
      var bizRulesDir : VirtualDirectory = BizRulesConfigResourceKeys.BUSINESS_RULES_IMPORT_DIR.Dir
      if (bizRulesDir.exists()) {
        var sampleRulesDir : File = bizRulesDir.resolveSingleDir()
        var files : File[] = getRuleImportFilesIn(sampleRulesDir)
        if (files != null) {
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            processRulesImportFiles(bundle, sampleRulesDir, files)
          }, User.util.UnrestrictedUser)
        }
      }
    }
    PCLoggerCategory.IMPORT.info("importUWRules : End")
  }

  private static function importAdminData() {
    if (DatabaseDependenciesGateway.getDatabase().isStartedFromEmptyDB()
        or DatabaseDependenciesGateway.getDatabase().wasUpgraded()) {
      var importDir = new File(_configurationDir.getAbsolutePath().concat(_dir))
      var importFilesList = new File(importDir.getAbsolutePath().concat(_fileSeparator).concat(_adminImportFileList))
      var adminDataParam = getParameter(_adminDataParameter)
      var finishedFiles = adminDataParam != null ? adminDataParam.split(_comma).toSet() : {}
      if(not DatabaseDependenciesGateway.getDatabase().isStartedFromEmptyDB()) {
        finishedFiles.add(_prodAdminData)  //no need to import existing prod admin data
      }
      if(importFilesList.exists()) {
        try {
          var importTool = new ImportToolsImpl()
          var br = new BufferedReader(FileUtil.getFileReader(importFilesList))
          var fileName = br.readLine()
          while(fileName != null) {
            fileName = fileName.trim()
            if (fileName.length() != 0 and !fileName.startsWith(_commentSeparator) and not finishedFiles.contains(fileName)) {
              var file = new File(importDir.getAbsolutePath().concat(_fileSeparator).concat(fileName))
              if(file.exists()) {
                PCLoggerCategory.IMPORT.info("Importing file ${file.getAbsoluteFile()}")
                gw.transaction.Transaction.runWithNewBundle(\bundle -> {
                  importTool.importXmlData(file.read())
                }, User.util.UnrestrictedUser)
              }
              finishedFiles.add(fileName)
            }
            fileName = br.readLine()
          }
          updateParameter(_adminDataParameter, finishedFiles.join(_comma))
          br.close()
        } catch (e) {
          PCLoggerCategory.IMPORT.error(e.Message)
        }
      }
    }
  }

  private static function getRuleImportFilesIn(directory : File) : File[] {
    return directory.listFiles(new FilenameFilter() {
      override function accept(dir : File, name : String) : boolean {
        return name.endsWith(_uwRuleExt)
      }
    })
  }
  private static function processRulesImportFiles(bundle: Bundle, sampleRulesDir : File, ruleImportFiles : File[] ) {
    var fileComparator : Comparator<File> = Comparator<File>.comparingLong(\f -> f.lastModified())
    var versionController = BizRulesDependencies.getBizRulesVersionController()
    var bizrulesParam = getParameter(_bizrulesParameter)
    var finishedFiles = bizrulesParam != null ? bizrulesParam.split(_comma).toSet() : {}
    if(not DatabaseDependenciesGateway.getDatabase().isStartedFromEmptyDB()) {
      finishedFiles.add(_prodBizRules)  //no need to import existing prod bizrules
    }
    Arrays.sort(ruleImportFiles, fileComparator)
    for (ruleImportFile in ruleImportFiles) {
      if(not finishedFiles.contains(ruleImportFile.Name)) {
        try {
          var fileFormatSupport = RuleFileFormatSupport.extractFrom(ruleImportFile)
          for (ruleMetadata in fileFormatSupport.readMetadata()) {
            importRuleVersions(versionController, bundle, fileFormatSupport, ruleMetadata)
          }
          finishedFiles.add(ruleImportFile.Name)
        } catch ( rnEx : RuntimeException) {
          BizRulesLoggerCategory.BIZRULES_IMPORT.error(rnEx.Message)
          finishedFiles.add(ruleImportFile.Name)
        } catch (ex : Exception) {
          BizRulesLoggerCategory.BIZRULES_IMPORT.error("Unable to open rules import file [" + ruleImportFile.getName() + "]. Please check the rules directory [" + sampleRulesDir
              .getPath() + "] for invalid rules import files", ex)
        }
      }
    }
    updateParameter(_bizrulesParameter, finishedFiles.join(_comma))
  }

  private static function importRuleVersions(versionController : BizRulesVersionControllerInternal, bundle : Bundle, fileFormatSupport : RuleFileFormatSupport, ruleMetadata : RuleMetadata) {
    var ruleId = ruleMetadata.getRuleID()
    BizRulesLoggerCategory.BIZRULES_IMPORT.info("Processing rule {}.", ruleId)
    var uwRule = gw.api.database.Query.make(UWRule).compare("PublicID", Relop.Equals, ruleId).select().FirstResult
    fileFormatSupport.importBootstrapRuleVersions(versionController.getRuleLinkInfo(), bundle, ruleId)
    createRuleHead(versionController, bundle, ruleMetadata.getHeadVersionID())
  }

  private static function createRuleHead(versionController : BizRulesVersionControllerInternal, bundle : Bundle, headVersionID : String) {
      var headVersion = findRuleVersionInBundle(bundle, headVersionID)
      var head = versionController.createNewEmptyHead(bundle, headVersion.getGlobalRuleID())
      versionController.changeHeadVersionRaw(head, headVersion)
  }

  private static function findRuleVersionInBundle(bundle : Bundle, publicId : String) : RuleVersion {
    for (bean in bundle.getBeansByRootType(RuleVersion.TYPE.get())) {
      var version : RuleVersion = bean as RuleVersion
      if (version.getGlobalVersionID().equals(publicId)) {
        return version
      }
    }
    throw new IllegalArgumentException("Rule-version not found: " + publicId)
  }
  /**
   * 11/05/2019
   * do not use in PROD
   */
  private static function updateParameter(param : String, newValue : String) {
    try {
      var entityBundleImpl = new EntityBundleImpl()
      PLDependencies.getParameterAccess().setParameterString(entityBundleImpl, param, newValue);
      entityBundleImpl.commit()
    } catch (e) {
      //suppress exception for now
    }
  }
  private static function getParameter(param : String) : String {
    return PLDependencies.getParameterAccess().getParameterString(param, null)
  }
}