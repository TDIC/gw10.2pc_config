package tdic.pc.common.util

uses java.util.regex.Pattern

class StringUtil {
  //static final var specialCharacters = {"•","#","&"}


  /**
   * Code merged from ProdSupport
   *
   *
   * GWPS-181 return true if String has Special Chars.
   *
   */
  public static function hasSpecialCharacters(text : String, specialChars : String) : Boolean {
    var regex = Pattern.compile(specialChars)
    return (text != null) ? regex.matcher(text)?.find() : false
  }

  /**
   *
   * @param text
   * @param specialChars
   * @return
   */
  public static function removeSpecialCharacters(text : String, specialChars : String) : String {
    return (text != null) ? text.replaceAll("[${specialChars}}]*","") : text
  }

  /**
   * GWPS-43 replace special characters from the the String with Special Chars.
   *
   */
/*  public static function replaceSpecialCharacters(strWithspecialChars:String) : String {
    var sb = new StringBuilder("[")
    for(currentChar in specialCharacters){
      sb.append(currentChar)
      sb.append("|")
    }
    sb.append("]")
    return strWithspecialChars.replaceAll(sb.toString(),"")
  }*/

}