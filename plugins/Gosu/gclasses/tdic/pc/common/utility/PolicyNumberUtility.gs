package tdic.pc.common.utility

uses tdic.pc.conversion.constants.ConversionConstants

class PolicyNumberUtility {

  static final var TDIC_AS400 = "8"
  static final var TDIC_DIMS = "9"
  static final var AS400 = "AS400"
  static final var DIMS = "DIMS"


  static function getLegacyIdentifier(source : String) : String {
    if (AS400.equalsIgnoreCase(source)) {
      return TDIC_AS400
    } else if (DIMS.equalsIgnoreCase(source)) {
      return TDIC_DIMS
    }else{
      return null
    }
  }

  /**
   * This function will convert the legacyPolicyNumber_TDIC in policycenter
   * to match the corresponding legacypolicynumber format in datamart legacy table.
   */
  static function getHistoricalPolicyNumber(legacyPolicyNumber :  String): String {
    // legacyPolcyNum = "CA 061939-5-C3"  datamartPolicyNum = "0619395"
    var datamartPolicyNum : String = ""
    datamartPolicyNum = legacyPolicyNumber?.substring(3,11)?.replace("-","")
    return datamartPolicyNum
  }
}