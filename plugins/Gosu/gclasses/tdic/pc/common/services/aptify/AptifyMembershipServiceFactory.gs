package tdic.pc.common.services.aptify

uses gw.api.util.DateUtil
uses tdic.pc.integ.services.membership.aptifymembershipservice.AptifyMembershipService

abstract class AptifyMembershipServiceFactory {

  private static var _service : AptifyMembershipService
  private static var _createTime : Date

  static property get Instance() : AptifyMembershipService {
    if(_service == null or _createTime == null or (DateUtil.currentDate().Time-_createTime.Time >7200000)){
      _service = new AptifyMembershipService()
      _service.Config.setCallTimeout(60000)
      _createTime = DateUtil.currentDate()
    }
    return _service
  }

}