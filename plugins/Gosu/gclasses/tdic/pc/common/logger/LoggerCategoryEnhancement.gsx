package tdic.pc.common.logger

uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses gw.pl.logging.LoggerCategory

/**
 * User: skiriaki
 * Date: 8/26/14
 *
 * Logger Enhancement, developers should add their own Logger Categories in this Enhancement.
 */
enhancement LoggerCategoryEnhancement: LoggerCategory {
  /**
   * Logger for TDIC Integration.
   */
  static property get TDIC_INTEGRATION(): Logger {
    return LoggerFactory.getLogger("TDIC_INTEGRATION")
  }

  /**
   * US656
   * 08/25/2014 shanem
   *
   * Logger for Exstream integration.
   */
  static property get EXSTREAM_DOCUMENT_PRODUCTION(): Logger {
    return LoggerFactory.getLogger("Plugin.ExstreamDocumentProduction")
  }

  /**
   * US688
   * 09/03/2014 shanem
   *
   * Logger for Flat File Utility.
   */
  static property get FLAT_FILE_UTILITY(): Logger {
    return LoggerFactory.getLogger("Utility.FlatFileUtility")
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * Logger for Excel Reader Utility.
   */
  static property get EXCEL_VENDOR_DEFINITION_READER(): Logger {
    return LoggerFactory.getLogger("Utility.ExcelDefinitionReader")
  }

  /**
   * US552
   * 10/30/2014 Vicente
   *
   * Logger for Encryption Plugin.
   */
  static property get ENCRYPTION_PLUGIN(): Logger {
    return LoggerFactory.getLogger("Utility.EncryptionPlugin")
  }

  /**
   * DE43 - related to US463
   * Shane Sheridan 28/11/2014
   *
   * Logger for Process Final Audit Estimation Batch process.
   */
  static property get PROCESS_FINAL_AUDIT_ESTIMATION_BATCH(): Logger {
    return LoggerFactory.getLogger("Server.BatchProcess.TDIC_ProcessFinalAuditEstimationBatch")
  }

  /**
   * US561: Policy Feed to Pivotal
   * 11/19/2014 Vicente
   * 12/05/2017 Praneethk
   * Logger for Pivotal Data creation batch
   */
  static property get TDIC_PIVOTAL(): Logger {
    return LoggerFactory.getLogger("BatchProcess.FeedPivotal")
  }

  /**
   * US627: PC GL Batch reporting
   * 11/17/2014 Kesava Tavva
   *
   * Logger for PC GL Report Batch.
   */
  static property get TDIC_BATCH_GLREPORT(): Logger {
    return LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  }

  /**
   * US469
   * Shane Sheridan 12/15/2014
   *
   * Logger for use with Renewal Auto Update Rule for WC7 line.
   */
  /*static property get TDIC_RULES_TDIC_WC7_RAU100000(): Logger {
    return LoggerFactory.getLogger("TDIC_Rules.TDIC_WC7_RAU100000")
  }*/

  /**
   * US1377
   * ShaneS 04/28/2015
   */
  static property get TDIC_REINSURANCE_SEARCH() : Logger{
    return LoggerFactory.getLogger("TDIC_Reinsurance.ReinsuranceSearch")
  }

  /**
   * US966
   * 12/04/2014 robk
   *
   * Logger for Review Multiline Discount Batch.
   */
  static property get REVIEW_MULITLINE_DISCOUNT(): Logger {
    return LoggerFactory.getLogger("Server.BatchProcess.ReviewMultilineDiscount")
  }

  /**
   * US684
   * 01/09/2015 robk
   *
   * Logger for Authentication Plugins.
   */
  static property get AUTHENTICATION(): Logger {
    return LoggerFactory.getLogger("Plugin.Authentication")
  }

  /**
   * US562
   * Sunnihith B 02/11/2015
   *
   * Logger for Statistical Reporting Batches
   */
  static property get TDIC_BATCH_WCPOLSREPORT(): Logger {
    return LoggerFactory.getLogger("TDIC_Batch_WCPOLSReport")
  }

  static property get TDIC_BATCH_WCSTATREPORT(): Logger {
    return LoggerFactory.getLogger("TDIC_Batch_WCSTATReport")
  }

  /**
   * US1316: ADA Membership sync
   * 04/13/2015 Kesava Tavva
   *
   * Logger for ADA Membership sync
   */
  static property get TDIC_BATCH_ADAMEMBERSHIPSYNC(): Logger {
    return LoggerFactory.getLogger("TDIC_Batch_ADAMembershipSync")
  }

  /**
   * US: PC Earned premium reporting
   * 12/28/2015 Kesava Tavva
   *
   * Logger for PC EarnedPremium Report Batch.
   */
  static property get TDIC_BATCH_EPREPORT(): Logger {
    return LoggerFactory.getLogger("TDIC_BATCH_EPREPORT")
  }

  /**
   *
   * Logger for PC TDIC_CreateCMContacts Batch.
   */
  static property get TDIC_BATCH_CREATECMCONTACTS(): Logger {
    return LoggerFactory.getLogger("TDIC_BATCH_CREATECMCONTACTS")
  }

  /**
   * 01/10/2017 praneethk
   *
   * Logger for AS400 Data creation batch
   */
  static property get TDIC_AS400(): Logger {
    return LoggerFactory.getLogger("TDIC_AS400")
  }

  static property get TDIC_BATCH_LOSSRATIORECLACULATE(): Logger {
    return LoggerFactory.getLogger("TDIC_BATCH_LOSSRATIORECLACULATE")
  }

  static property get Document_ExportRatebooks(): Logger {
    return acc.onbase.util.LoggerFactory.getLogger("Document.ExportRatebooks")
  }

  /**
   * 15/10/2019 RambabuN
   *
   * Logger for enrole inbound and outbound batch
   */
  static property get TDIC_Enrole(): Logger {
    return LoggerFactory.getLogger("TDIC_Enrole")
  }

  /**
   * Logger for Retro rating integration.
   */
  static property get TDIC_RetroRating() : Logger {
    return LoggerFactory.getLogger("TDIC_RetroRating")
  }

  /**
   * Logger for Onbase integration.
   */
  static property get Document_OnBaseDMS() : Logger {
    return acc.onbase.util.LoggerFactory.getLogger("Document.OnBaseDMS")
  }

  /**
   * Logger for Conversion integration.
   */
  static property get Conversion() : Logger {
    return LoggerFactory.getLogger("TDIC_Conversion")
  }

  /**
   * Logger for ConversionError integration.
   */
  static property get ConversionError() : Logger {
    return LoggerFactory.getLogger("TDIC_ConversionError")
  }

  /**
   * Logger for ConversionErrorEmail integration.
   */
  static property get ConversionErrorEmail() : Logger {
    return LoggerFactory.getLogger("TDIC_ConversionErrorEmail")
  }

  /**
   * Logger for ConversionErrorAddress integration.
   */
  static property get ConversionErrorAddress() : Logger {
    return LoggerFactory.getLogger("ConversionErrorAddress")
  }

}
