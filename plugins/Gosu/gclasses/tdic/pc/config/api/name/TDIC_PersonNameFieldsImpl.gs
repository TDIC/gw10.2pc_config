package tdic.pc.config.api.name

uses gw.api.name.PersonNameFieldsImpl

/**
 * Shane Sheridan 08/22/2014
 * Extension of the OOTB PersonNameFieldsImpl for TDIC.
 */
class TDIC_PersonNameFieldsImpl extends PersonNameFieldsImpl {

  var _credential_TDIC : Credential_TDIC as Credential_TDIC
}