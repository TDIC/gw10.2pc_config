package tdic.pc.config.api.name

uses java.util.Set
uses gw.api.name.NameLocaleSettings
uses gw.api.name.NameOwnerFieldId

/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 22/08/14
 * Time: 10:41
 * This class is an extension of NameLocaleSettings for TDIC.
 */
@Export
class TDIC_NameLocaleSettings extends NameLocaleSettings{

  /* fields shown in display names.
   *  */
  public static final var TDIC_DEFAULT_DISPLAY_NAME_FIELDS : Set<NameOwnerFieldId> = {
      TDIC_NameOwnerFieldId.FIRSTNAME, TDIC_NameOwnerFieldId.MIDDLENAME, TDIC_NameOwnerFieldId.LASTNAME, TDIC_NameOwnerFieldId.SUFFIX, TDIC_NameOwnerFieldId.CREDENTIALS
  }.freeze().toSet()
}