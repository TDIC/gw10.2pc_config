package tdic.pc.config.wc7

uses gw.api.webservice.exception.SOAPServerException
uses gw.losshistory.ClaimSearchCriteria
uses gw.plugin.billing.BillingPeriodInfo
uses gw.policy.PolicyEvalContext
uses gw.plugin.Plugins
uses gw.plugin.claimsearch.IClaimSearchPlugin
uses gw.plugin.claimsearch.NoResultsClaimSearchException
uses gw.plugin.claimsearch.ResultsCappedClaimSearchException
uses gw.question.QuestionIssueAutoRaiser
uses java.math.BigDecimal
uses gw.pl.currency.MonetaryAmount
uses gw.api.util.CurrencyUtil
uses gw.api.util.DateUtil
uses gw.lob.common.AbstractUnderwriterEvaluator
uses gw.api.system.PCLoggerCategory
uses gw.api.domain.financials.PCFinancialsLogger
uses gw.api.locale.DisplayKey
uses gw.web.policy.PolicyBillingUIHelper
uses org.slf4j.LoggerFactory

@Export
class WC7_UnderwriterEvaluator extends AbstractUnderwriterEvaluator {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");



  // 20170215 TJT - UW issues should be created only if they are above the company threshold (else everyone will get them and be able to approve them)
  private final var COMPANYREQUIREMENT_WC7NumberOfYearsForLossRatioCalculation : BigDecimal = 4.00
  //Issuance Issues
  private final var COMPANYLIMIT_WC7ExModGreaterThanOne : BigDecimal = 1.00
  private final var COMPANYLIMIT_WC7ExModLessThanOne : BigDecimal = 1.00
  private final var COMPANYLIMIT_BackdatedBind : int = 30
  private final var COMPANYLIMIT_WC7ExceedClaimsMax : int = 2
  private final var COMPANYLIMIT_WC7TotalIncurredMax : MonetaryAmount = new MonetaryAmount(5000.00, _policyEvalContext.Period.PreferredSettlementCurrency)
  private final var COMPANYLIMIT_WC7ScheduleRatingFactorCredit : BigDecimal = -0.06
  private final var COMPANYLIMIT_WC7ScheduleRatingFactorDebit : BigDecimal = 0
  //Quote issues
  private final var COMPANYLIMIT_WC7RiskAnalysisPriorPolicies : int = 0
  private final var COMPANYLIMIT_WC7MissingLossHistory : int = 0
  private final var COMPANYLIMIT_WC7EmploymentPriorToEffectiveDate : int = 0
  private final var COMPANYLIMIT_WC7AccountUnhealthy : int = 0
  private final var COMPANYLIMIT_WC7UnpaidBalance : MonetaryAmount = new MonetaryAmount(0.00, _policyEvalContext.Period.PreferredSettlementCurrency)
  private final var COMPANYLIMIT_WC7OwnershipPercentage : int = 1 //if the value less than or equal to this, no approval required
  //Quote Release issues
  private final var COMPANYLIMIT_WC7SchedRatingManPremExceeded : MonetaryAmount = new MonetaryAmount(7500.00, _policyEvalContext.Period.PreferredSettlementCurrency)
  //Renewal issues
  private final var COMPANYLIMIT_WC7LossRatioAboveThreshold : BigDecimal = 150.0
  private final var COMPANYLIMIT_WC7OverMaxNumberClaimsOnExpiringPolicy : int = 0
  private final var COMPANYLIMIT_TDIC_MoneyOverThresholdStillOwedOnPolicy : MonetaryAmount = new MonetaryAmount(10.00, _policyEvalContext.Period.PreferredSettlementCurrency)
  private final var COMPANYLIMIT_WC7ScheduledRatingFactorOutsideRange_CREDIT : BigDecimal = -0.00
  private final var COMPANYLIMIT_WC7ScheduledRatingFactorOutsideRange_DEBIT : BigDecimal = 0.00

  construct(policyEvalContext : PolicyEvalContext) {
    super(policyEvalContext)
  }

  override function onPreBind() {
    quoteHasManualOverrides()
    policyHold()
  }

  override function onPreissuance(){
    issuanceClassCodeOutsideOfRange_TDIC()
    issuanceConditionEndorsementElected_TDIC()
    issuanceExModGreaterThanOne_TDIC()
    issuanceExModLessThanOrEqualOne_TDIC()
    issuanceHasCostsOverridden_TDIC()
    issuanceOpenClaims_TDIC()
    issuancePolicyIssuedAfterEffectiveDate_TDIC()
    issuancePriorLossesNumberOfClaimsExceeded_TDIC()
    issuancePriorLossesTotalIncurredExceeded_TDIC()
    issuanceScheduleRatingFactorCredit_TDIC()
    issuanceScheduleRatingFactorDebit_TDIC()
    issuanceWaiverOfSubroExists_TDIC()


    //20160322 TJT : GW-1045  Simulate the Sales User clicking the "Referral" button
    /*var blnFoundBlockingUWIssue : boolean = false
    for (eachUWIssue in _policyEvalContext.Period.UWIssuesActiveOnly) {
      if (eachUWIssue.isBlocking(typekey.UWIssueBlockingPoint.TC_BLOCKSISSUANCE)) {
        blnFoundBlockingUWIssue = true
        break;
      }
    }
    if (!perm.System.editlockoverride and blnFoundBlockingUWIssue){
      var approvalOption = "UWRequest"
      var activityPattern = ActivityPattern.finder.getActivityPatternByCode("approve_general")
      var tmpActivity = activityPattern.createJobActivity( _policyEvalContext.Period.Job.Bundle, _policyEvalContext.Period.Job, null, null, null, null, null, null, null )
      gw.api.web.activity.ActivityUtil.setPreviousUserOnNewActivity(tmpActivity, User.util.CurrentUser)
      var assigneePicker = gw.pcf.UWActivityPopupHelper.getDefaultAssignee(approvalOption, _policyEvalContext.Period)
      gw.pcf.UWActivityPopupHelper.updatePolicyPeriodAndActivity(null, assigneePicker, approvalOption, _policyEvalContext.Period, tmpActivity)
    }*/
  }

  override function onPrequote() {
    periodStartAndEndDates()
    underwritingCompanySegmentNotValid()
    sumOfPreQuoteRiskFactor()
    producerChanged()

   /* quoteRiskAnalysisPriorPolicies_TDIC()                   //FIXME TJT - this should be in the Question override
    quoteRiskAnalysisPriorPoliciesGapsOrOverlaps_TDIC()
    quoteRiskAnalysisNoLossHistory_TDIC()                   //FIXME TJT - this should be in the Question override
//    quoteRiskAnalysisLossHistoryMissing_TDIC()*/
    quoteFirstEmployeeStartBeforeEffectiveDate_TDIC()       //FIXME TJT - this should be in the Question override (but needs a date comparator created)
    quoteAccountUnhealthy_TDIC()
    quoteAccountUnpaidBalance_TDIC()
    quoteOwnershipPercentage_TDIC()
    accountHasZeroPayrollAuditedBasis_TDIC()  //Audit
    accountHasDetainedZeroPayrollBasis_TDIC() //Renewal
    //accountHasZeroPayrollBasisPreRenewalDirection_TDIC() //Renewal & RenewalPreDirection

  }

  override function onPrequoteRelease() {
    //GW - 2257 - Generate an UW-issue when the schedule rating is applicable and manual premium is greater  than or equal to $7500
      quotereleaseSchedRatingManPremExceeded_TDIC()
  }

  override function onQuestion() {
    // DE49 - related to US462 Shane Sheridan 12/01/2014
    // Do not want to raise issues for questions in Quick Quote Submissions, if the line is WC7.
    if(_policyEvalContext.Period.Job typeis Submission){
      if (_policyEvalContext.Period.Job.QuoteType == typekey.QuoteType.TC_QUICK) {
        return
      }
    }
    QuestionIssueAutoRaiser.autoRaiseIssuesForQuestions(_policyEvalContext)
  }

  override function onReferral() {
    var allReferralReasons = _policyEvalContext.Period.Policy.UWReferralReasons
    var openReferralReasons = allReferralReasons.where(\ referral -> referral.Open)
    for (referralReason in openReferralReasons) {
      var issue = _policyEvalContext.addIssue(
        referralReason.IssueType.Code,
        referralReason.IssueKey,
        \ -> referralReason.ShortDescription,
        \ -> referralReason.LongDescription
      )
      issue.Value = referralReason.Value
    }
  }

  override function onReinsurance()  {
    var period = _policyEvalContext.Period
    var nextOOSSliceDate = period.Policy.BoundEditEffectiveDates.firstWhere(\ d -> d > period.SliceDate)
    // There may be more slices of RIRisk than OOSSlice (because of auto splitting on
    // program end for example. So we need to find those slices of RIRisk and
    // evaluate them.
    var allRIRiskVersions = period.AllReinsurables*.RIVersionList*.AllVersions.where( \ ririsk -> ririsk.EffectiveDate >= period.SliceDate
        and (nextOOSSliceDate == null or ririsk.EffectiveDate < nextOOSSliceDate))

    allRIRiskVersions.each( \ ririsk -> {
      checkTargetNetRetention(ririsk)
      checkFacNotCededTo(ririsk)
      checkAttachmentNotCededToCapacity(ririsk)
    })
  }

  override function onRenewal() {
    // Shane Sheridan 01/07/2015
    // Replaced renewalLossClaim() with a WC7 version
    //renewalLossClaim()  //OOTB
    //renewalLossClaimWC7_TDIC()    //TODO - temporarily disable these in production (too much work for underwriting at the moment)
    renewalPreviousEstimatedAudits_TDIC()
    renewalScheduledRatingFactorOutsideRange_TDIC()
    renewalExperienceRatingModifierExists_TDIC()
    renewalMoneyStillOwedOnPolicy_TDIC()
  }

  private function checkTargetNetRetention(ririsk : RIRisk) {
    var risk = ririsk.Reinsurable
    var facRINeeded = ririsk.FacRINeeded
    if (facRINeeded.IsPositive) {
      var shortDescription = DisplayKey.get("UWIssue.Reinsurance.NetRetentionGreaterThanTarget.ShortDesc")
      var longDescription = DisplayKey.get("UWIssue.Reinsurance.NetRetentionGreaterThanTarget.LongDesc", risk, CurrencyUtil.renderAsCurrency(facRINeeded))
      _policyEvalContext.addIssue("RINetRetention", risk.UWIssueKey, \-> shortDescription, \->longDescription, ririsk.NetRetention)
    }
  }

  private function checkFacNotCededTo(ririsk : RIRisk) {
    var nonCededFacs = ririsk.Attachments.FacAgreements.where( \ fac -> fac.CededRisk.IsZero)
    if (nonCededFacs.HasElements) {
      var agreementsDisplay = nonCededFacs*.Agreement*.Name.toList()
//      nonCededFacs.each( \ fac -> {
//        agreementsDisplay += displaykey.Web.Reinsurance.RIRisk.Validation.Agreements(fac.Agreement.Name)
//      })

      var shortDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsHaveNoCededRiskShort")
      var longDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsHaveNoCededRiskLong", ririsk.Reinsurable, agreementsDisplay)
      _policyEvalContext.addIssue("AgreementsHaveNoCededRisk", ririsk.UWIssueKey, \-> shortDescription, \-> longDescription)
    }
  }

  private function checkAttachmentNotCededToCapacity(ririsk : RIRisk) {
    var attachmentsNotCededToMax = ririsk.Attachments.where( \ att -> att.MaxCeding != null and att.MaxCeding > att.CededRisk)
    if (attachmentsNotCededToMax.HasElements) {
      var agreementsDisplay = attachmentsNotCededToMax*.Agreement*.Name.toList()
//      attachmentsNotCededToMax.each( \ att -> {
//        agreementsDisplay += displaykey.Web.Reinsurance.RIRisk.Validation.Agreements(att.Agreement.Name)
//      })

      var shortDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsDoNotCedeToCapacityShort")
      var longDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsDoNotCedeToCapacityLong", ririsk.Reinsurable, agreementsDisplay)
      _policyEvalContext.addIssue("AgreementsDoNotCedeToCapacity", ririsk.UWIssueKey, \-> shortDescription, \-> longDescription)
    }
  }

  /*----------Private Helper Functions----------*/

  private function createClaimTotalIncurred(result : ClaimSet, basedOn : PolicyPeriod) {
    var claimWithMaxCost = result.Claims.maxBy(\ c -> c.TotalIncurred)
    var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ClaimTotalIncurred.ShortDesc", claimWithMaxCost.ClaimNumber)
    var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ClaimTotalIncurred.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), claimWithMaxCost.ClaimNumber, claimWithMaxCost.TotalIncurred)
    _policyEvalContext.addIssue("ClaimTotalIncurred", "ClaimTotalIncurred", shortDescription, longDescription, claimWithMaxCost.TotalIncurred)
  }

  private function createIncidenceOfClaim(totalIncurred : MonetaryAmount, writtenPremium : BigDecimal, claimCount : int, basedOn : PolicyPeriod) {
    var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.IncidenceOfClaims.ShortDesc")
    var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.IncidenceOfClaims.LongDesc", claimCount, basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), writtenPremium)
    _policyEvalContext.addIssue("IncidenceOfClaims", "IncidenceOfClaims", shortDescription, longDescription, claimCount)

    if (not totalIncurred.IsZero) {
      // Create RatioOfClaimsTotalIncurredToWrittenPremium
      var value = totalIncurred.divide(writtenPremium, 5, HALF_UP)
      shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.RatioOfClaimsTotalIncurredToWrittenPremium.ShortDesc")
      longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.RatioOfClaimsTotalIncurredToWrittenPremium.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), totalIncurred, writtenPremium, value.multiply(100))
      _policyEvalContext.addIssue("RatioOfClaimsTotalIncurredToWrittenPremium", "RatioOfClaimsTotalIncurredToWrittenPremium", shortDescription, longDescription, value)
    }
  }

  private function periodStartAndEndDates() {
    if(_policyEvalContext.Period.Job typeis Rewrite and _policyEvalContext.Period.PeriodEnd != _policyEvalContext.Period.BasedOn.PeriodEnd) {
      var period = _policyEvalContext.Period
      var shortDescription = \ -> DisplayKey.get("UWIssue.Rewrite.PeriodMismatch.ShortDesc")
      var longDescription =
        \ -> DisplayKey.get("UWIssue.Rewrite.PeriodMismatch.LongDesc", period.PeriodStart,
            period.PeriodEnd, period.BasedOn.PeriodStart, period.BasedOn.PeriodEnd)
      _policyEvalContext.addIssue("RewritePeriodDates", "RewritePeriodDates",
        shortDescription, longDescription)
    }
  }

  private function policyHold(){
    if(_policyEvalContext.CheckingSet == UWIssueCheckingSet.TC_UWHOLD or
        _policyEvalContext.CheckingSet == UWIssueCheckingSet.TC_REGULATORYHOLD){
          var policyHolds = PolicyHold.finder.findPolicyHolds()
      for(hold in policyHolds) {
        if (hold.IssueType.CheckingSet == _policyEvalContext.CheckingSet) {
          if (hold.compareWithPolicyPeriod(_policyEvalContext.Period)) {
            var bundle = _policyEvalContext.Period.Bundle
            hold = bundle.add(hold)
            hold.updateLastEvalTime(_policyEvalContext.Period.Job, java.util.Date.CurrentDate, _policyEvalContext.Period)
            _policyEvalContext.addIssue(hold.IssueType.Code, hold.PolicyHoldCode, \->hold.Description, \->hold.UWIssueLongDesc)
          }
        }
      }
    }
  }

  private function quoteHasManualOverrides() {
    if(_policyEvalContext.CheckingSet == UWIssueCheckingSet.TC_PREQUOTERELEASE and _policyEvalContext.Period.hasAtLeastOneCostOverride()) {
      var shortDescription = \ -> DisplayKey.get("UWIssue.PersonalAuto.QuoteHasManualOverrides.ShortDesc")
      var longDescription = \ -> DisplayKey.get("UWIssue.PersonalAuto.QuoteHasManualOverrides.LongDesc")
      _policyEvalContext.addIssue("QuoteHasManualOverrides", "QuoteHasManualOverrides",
          shortDescription, longDescription)
    }
  }

  private function renewalLossClaim(){
    if (not (_policyEvalContext.Period.Job typeis Renewal)) {
      return
    }

    var renewalPeriod = _policyEvalContext.Period
    var basedOn = renewalPeriod.BasedOn

    try {
      var result = searchForClaims(basedOn)
      var totalIncurred = result.Claims.where(\ c -> c.TotalIncurred != null).sum(_policyEvalContext.Period.PreferredSettlementCurrency, \ c -> c.TotalIncurred)
      var claimCount = result.Claims.Count

      if (not totalIncurred.IsZero) {
        createClaimTotalIncurred(result, basedOn)
      }

      var writtenPremium = basedOn.TotalPremiumRPT
      if (writtenPremium != null and not writtenPremium.IsZero and claimCount != 0 ) {
        createIncidenceOfClaim(totalIncurred, writtenPremium, claimCount, basedOn)
      }
    } catch (ex : NoResultsClaimSearchException) {
      // No actions necessary
    } catch (ex : ResultsCappedClaimSearchException) {
      // Create ManualClaimReviewNeeded
      var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.ShortDesc")
      var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
      _policyEvalContext.addIssue("ManualClaimReviewNeeded", "ManualClaimReviewNeeded",
          shortDescription, longDescription)
    } catch(ex : SOAPServerException) {
      //Create UnableRetrieveClaimInfo
      var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.ShortDesc")
      var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
      _policyEvalContext.addIssue("UnableRetrieveClaimInfo", "UnableRetrieveClaimInfo",
          shortDescription, longDescription)
    }
  }

  private function searchForClaims(basedOn : PolicyPeriod) : ClaimSet {
    var result : ClaimSet
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var searchCriteria = new ClaimSearchCriteria()
      searchCriteria.Policy = basedOn.Policy
      searchCriteria.DateCriteria.StartDate = basedOn.PeriodStart
      searchCriteria.DateCriteria.EndDate = basedOn.EndOfCoverageDate
      searchCriteria.DateCriteria.DateSearchType = TC_ENTEREDRANGE
      result = Plugins.get(IClaimSearchPlugin).searchForClaims(searchCriteria)
    })
    return result
  }

  private function sumOfPreQuoteRiskFactor() {
    if(_policyEvalContext.Period.Job?.createsNewPolicy() and
        _policyEvalContext.CheckingSet == UWIssueCheckingSet.TC_PREQUOTE and
        _policyEvalContext.Period.PreQualRiskPointSum >= 100){

      var sum = _policyEvalContext.Period.PreQualRiskPointSum
      _policyEvalContext.addIssue("PreQualQuestionRiskPointSum",
        "PreQualQuestionRiskPointSum",
        \ -> DisplayKey.get("UWIssue.Question.PreQualRiskPointSumDescription", sum),
        \ -> DisplayKey.get("UWIssue.Question.PreQualRiskPointSumDescription", sum),
        sum)
    }
  }

  private function underwritingCompanySegmentNotValid() {
    if (_policyEvalContext.Period.Job typeis Submission or
          _policyEvalContext.Period.Job typeis Rewrite or
          _policyEvalContext.Period.Job typeis RewriteNewAccount ) {
      var uwcompany = _policyEvalContext.Period.UWCompany
      var polperiod = _policyEvalContext.Period
      var segmentOkay = true
      var uwcompanysegment : typekey.Segment
      if (uwcompany.LicensedStates.Count > 0) {
        uwcompanysegment = uwcompany.LicensedStates
            .firstWhere(\ l -> l.State == polperiod.BaseState ).Segment
        //  if the polperiod segment is low, then any uw company is okay
        if (polperiod.Segment != TC_LOW) {
          // policy segment is not lows so if the uwcompanysegmemnt is low, then problem
          if (uwcompanysegment == TC_LOW) {
            segmentOkay = false
          } else {
            // if uw company is high then no problem
            // so only problem is if uw is medium and policy is high
            if (uwcompanysegment == TC_MED and polperiod.Segment == TC_HIGH) {
              segmentOkay = false
            }
          }
        }
      }
      if (!segmentOkay) {
        var shortDescription =
            \ -> DisplayKey.get("UWIssue.PolicySegment.PolicyRiskSegmentInvalidForUWCompany")
        var longDescription =
            \ -> DisplayKey.get("UWIssue.PolicySegment.PolicyRiskSegmentValueNotAllowedForUWCompany", polperiod.Segment.DisplayName, uwcompany.DisplayName)
        _policyEvalContext.addIssue("UWCompanySegmentValid", "UWCompanySegmentValid",
            shortDescription, longDescription)
      }
    }
  }


   // T D I C   C O N F I G U R A T I O N

  /**
   * US469
   * Shane Sheridan 12/17/2014
   *
   * This function creates a "TDICClassCode" Underwriting Issue if any WC7ClassCode code value of any WC7CoveredEmployee object
   * is outside of a class code range required by TDIC.
   *
   * Range: 8839, 8810.
   */
  private function issuanceClassCodeOutsideOfRange_TDIC(){
    final var classCodeRange = {"8839", "8810", "8810(1)"}  //20161021 TJT GW-2438 realigning class codes to WCIRB with suffixes
    var isClassCodeOutOfRange = false
    for(covEmp in _policyEvalContext.Period.WC7Line.WC7CoveredEmployees){
      if(not classCodeRange.contains(covEmp.ClassCode.Code) and not isClassCodeOutOfRange){
        isClassCodeOutOfRange = true
        break
      }
    }

    if(isClassCodeOutOfRange){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICClassCode.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICClassCode.LongDesc")
      _policyEvalContext.addIssue("TDICClassCode", "TDICClassCode", shortDesc, longDesc)
    }
  }

  /**
   * US878
   * Shane Sheridan 01/12/2015
   *
   * This function checks if any of a specified range of Condition Endorsements are elected on this policy.
   *
   * Range:
   *  a. Voluntary Compensation And Employers Liability Cov Cond
   *  b. Waiver Of Our Right To Recover From Others Endorsement Cond
   *  c. Sole Proprietor Coverage Endorsement
   *  d. Notice Required By Law
   *  e. California Code Of Regulations 10
   *  f. California Code Of Regulations 11
   */
  //20170214 TJT refactored (combined a helper function into this, since it was only used here)
  private function issuanceConditionEndorsementElected_TDIC(){
    if ( _policyEvalContext.Period.WC7Line.WC7VoluntaryCompensationAndEmployersLiabilityCovCondExists
        or _policyEvalContext.Period.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists
        or _policyEvalContext.Period.WC7Line.WC7TDICSoleProprietorCoverageEndorsementExists
        or _policyEvalContext.Period.WC7Line.WC7TDICNoticeRequiredByLawExists
        or _policyEvalContext.Period.WC7Line.WC7TDICCaliforniaCodeOfRegulations10Exists
        or _policyEvalContext.Period.WC7Line.WC7TDICCaliforniaCodeOfRegulations11Exists ){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICConditionEndorsement.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICConditionEndorsement.LongDesc")
      _policyEvalContext.addIssue("TDICConditionEndorsement", "TDICConditionEndorsement", shortDesc, longDesc)
    }
  }


  /**
   * US878
   * Shane Sheridan 01/13/2015
   *
   * If the Experience Rating (Ex-Mod) is eligible, this checks if the Experience Rating Factor value is
   * greater than 1; creating the "TDIC_WC7ExModGreaterThanOne" uw issue if it is.
   */
  //20170214 TJT refactored
  private function issuanceExModGreaterThanOne_TDIC(){
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions){
      for(aModifier in jurisdiction.WC7Modifiers){
        if(aModifier.Pattern.CodeIdentifier == "WC7ExpMod") {
          if(aModifier.RateWithinLimits != null and aModifier.Eligible and aModifier.RateWithinLimits > COMPANYLIMIT_WC7ExModGreaterThanOne){
            var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExModGreaterThanOne.ShortDesc")
            var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExModGreaterThanOne.LongDesc")
            _policyEvalContext.addIssue("TDIC_WC7ExModGreaterThanOne", "TDIC_WC7ExModGreaterThanOne", shortDesc, longDesc, aModifier.RateWithinLimits)
          }
        }
      }
    }
  }


  /**
   * US878
   * Shane Sheridan 01/22/2015
   *
   * If the Experience Rating (Ex-Mod) is eligible, this checks if the Experience Rating Factor value is
   * less than or equal to 1; creating the "TDIC_WC7ExModLessThanOrEqualOne" uw issue if it is.
   */
  //20170214 TJT refactored
  // will loop thru splits to examine the modifier on each split (if either is out of bounds, a single issue is raised)
  private function issuanceExModLessThanOrEqualOne_TDIC(){
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions){
      for (eachRatingPeriod in jurisdiction.RatingPeriods){
        for(aModifier in eachRatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(eachRatingPeriod.RatingStart).where( \ elt -> elt.PatternCode == "WC7ExpMod")){
          if(aModifier.RateWithinLimits != null and aModifier.Eligible and aModifier.RateWithinLimits < COMPANYLIMIT_WC7ExModLessThanOne){
            var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExModLessThanOrEqualOne.ShortDesc")
            var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExModLessThanOrEqualOne.LongDesc")
            _policyEvalContext.addIssue("TDIC_WC7ExModLessThanOrEqualOne", "TDIC_WC7ExModLessThanOrEqualOne", shortDesc, longDesc, aModifier.RateModifier)
          }
        }
      }
    }
  }


  /**
   * GW-1296. Raise UW issue if policyperiod has costs overriden.
   * Fire only on Renewal and block quote.
   */
  private function issuanceHasCostsOverridden_TDIC() {
    if (_policyEvalContext.Period.hasAtLeastOneCostOverride()){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.TDIC_CostsOverridden.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.TDIC_CostsOverridden.LongDesc")
      _policyEvalContext.addIssue("TDIC_CostsOverridden", "TDIC_CostsOverridden", shortDesc, longDesc)
    }
  }


  /**
   * US976
   * Rob Kelly 02/03/2015
   *
   * If there is at least one open claim, then create a TDIC_WC7OpenClaims UW issue.
   */
  //20170112 TJT - Refactored this for accuracy when comparing totalOpenClaims to an Authority Grant
  private function issuanceOpenClaims_TDIC() {
    var totalPriorCarrierOpenClaims : int = 0
    var totalTDICOpenClaims : int = 0

    totalPriorCarrierOpenClaims = _policyEvalContext.Period.Policy.PriorLosses.where( \ elt -> elt.Status_TDIC == "Open").Count

    for(eachPreviousPeriod in getPreviousPeriods()){
      totalTDICOpenClaims += searchForOpenClaims_TDIC(eachPreviousPeriod).Count
    }

    if (totalPriorCarrierOpenClaims + totalTDICOpenClaims > 0){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7OpenClaims.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7OpenClaims.LongDesc")
      _policyEvalContext.addIssue("TDIC_WC7OpenClaims", "TDIC_WC7OpenClaims", shortDesc, longDesc)
    }
  }


  /**
   * US878
   * Shane Sheridan 01/27/2015
   */
  //20170214 TJT refactored (simplified, no functional change)
  private function issuancePolicyIssuedAfterEffectiveDate_TDIC() {
    // trim dates for no time difference
    var dateOfAttemptedIssuance = java.util.Date.CurrentDate.trimToMidnight()
    //20170221 TJT policy change should compare txn eff date, not period start
    //var effectiveDate = _policyEvalContext.Period.PeriodStart.trimToMidnight()
    var effectiveDate = _policyEvalContext.Period.EditEffectiveDate.trimToMidnight()
    if (dateOfAttemptedIssuance.after(effectiveDate) and dateOfAttemptedIssuance.daysBetween(effectiveDate) > COMPANYLIMIT_BackdatedBind){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICBackdatedBind.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICBackdatedBind.LongDesc")
      var issueKey = "TDICBackdatedBind_" + _policyEvalContext.Period.Job.JobNumber
      _policyEvalContext.addIssue("TDICBackdatedBind", issueKey, shortDesc, longDesc, dateOfAttemptedIssuance.daysBetween(effectiveDate))
    }
  }


  /**
   * Shane Sheridan 02/11/2015
   *
   * Create Underwriter Issue if there are greater than 2 number of claims reported in prior losses.
   */
  private function issuancePriorLossesNumberOfClaimsExceeded_TDIC() {
    var numberOfClaims : int = 0
    for(eachPreviousPeriod in getPreviousPeriods()){
      var claims = searchForClaims_TDIC(eachPreviousPeriod)
      if(claims != null and claims.Count > 0) {
        numberOfClaims = numberOfClaims + claims.Count
      }
    }
    var priorClaims = _policyEvalContext.Period.Policy.PriorLosses.Count
    var totalNumberOfClaims : int =  numberOfClaims + priorClaims
    if (totalNumberOfClaims > COMPANYLIMIT_WC7ExceedClaimsMax){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExceedClaimsMax.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExceedClaimsMax.LongDesc")
      _policyEvalContext.addIssue("TDIC_WC7ExceedClaimsMax", "TDIC_WC7ExceedClaimsMax", shortDesc, longDesc, totalNumberOfClaims)
    }
  }

  /**
   *  Shane Sheridan 02/11/2015
   */
  private function issuancePriorLossesTotalIncurredExceeded_TDIC() {
    var period = _policyEvalContext.Period
    var totalIncurred : MonetaryAmount = new MonetaryAmount(0.00, _policyEvalContext.Period.PreferredSettlementCurrency)
    if (period.Policy.LossHistoryType == typekey.LossHistoryType.TC_MAN){
      for (aPriorLoss in period.Policy.PriorLosses){
        if(aPriorLoss.TotalIncurred != null){
          totalIncurred = totalIncurred + aPriorLoss.TotalIncurred
        }
      }
    }
    if (totalIncurred >= COMPANYLIMIT_WC7TotalIncurredMax){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7TotalIncurredMax.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7TotalIncurredMax.LongDesc")
      _policyEvalContext.addIssue("TDIC_WC7TotalIncurredMax", "TDIC_WC7TotalIncurredMax", shortDesc, longDesc, totalIncurred)
    }
  }


  /**
   * US878
   * Shane Sheridan 01/22/2015
   */
  private function issuanceScheduleRatingFactorCredit_TDIC(){
    //FIXME TJT - need to cast script param (string) to double?
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions){
      var modifiers = jurisdiction.WC7Modifiers
      //for(aModifier in modifiers){
      for (eachRatingPeriod in jurisdiction.RatingPeriods){
        for (aModifier in eachRatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(eachRatingPeriod.RatingStart)){
          if(aModifier.PatternCode == "WC7ScheduleMod" and aModifier.Eligible) {
            var rateValueAsDouble = aModifier.RateWithinLimits
            if(rateValueAsDouble < COMPANYLIMIT_WC7ScheduleRatingFactorCredit){
              var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduleRatingFactorCredit.ShortDesc")
              var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduleRatingFactorCredit.LongDesc")
              _policyEvalContext.addIssue("TDIC_WC7ScheduleRatingFactorCredit", "TDIC_WC7ScheduleRatingFactorCredit", shortDesc, longDesc, rateValueAsDouble)
            }
          }
        }
      }
    }
  }

  /**
   * US878
   * Shane Sheridan 01/22/2015
   *
   * This function creates a "TDIC_WC7ScheduleRatingFactorDebit" Underwriting Issue if there is a Scheduled Rating Factor debit (+) on the policy.
   */
  private function issuanceScheduleRatingFactorDebit_TDIC(){
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions){
      //for(aModifier in jurisdiction.WC7Modifiers){
      for (eachRatingPeriod in jurisdiction.RatingPeriods){
        for (aModifier in eachRatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(eachRatingPeriod.RatingStart)){
          if(aModifier.PatternCode == "WC7ScheduleMod" and aModifier.Eligible) {
            var rateValueAsDouble = aModifier.RateWithinLimits
            if(rateValueAsDouble > COMPANYLIMIT_WC7ScheduleRatingFactorDebit){
              var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduleRatingFactorDebit.ShortDesc")
              var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduleRatingFactorDebit.LongDesc")
              _policyEvalContext.addIssue("TDIC_WC7ScheduleRatingFactorDebit", "TDIC_WC7ScheduleRatingFactorDebit", shortDesc, longDesc, rateValueAsDouble)
            }
          }
        }
      }
    }
  }


  /**
   * GW 838
   * Raise UW Issue if there is Waiver Of Subrogation on the policy.
   */
  private function issuanceWaiverOfSubroExists_TDIC() {
    if (_policyEvalContext.Period.WC7Line.HasWC7WaiverOfSubro){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WaiverOfSubroExists.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WaiverOfSubroExists.LongDesc")
      _policyEvalContext.addIssue("TDIC_WaiverOfSubroExists", "TDIC_WaiverOfSubroExists", shortDesc, longDesc)
    }
  }

  /**
   * US878
   * Shane Sheridan 01/30/2015
   *
   * If the insured has been in business with WC coverage for grater than 0 years (answered through the TDIC_YearsInBusiness supplemental question),
   * then the Risk Analysis - Prior Policies section must not be empty, otherwise a TDIC_WC7RiskAnalysisPriorPoliciesEmpty uw issues is generated.
   */
  private function quoteRiskAnalysisPriorPolicies_TDIC() {
    var tmpAnswerDelegate : entity.PCAnswerDelegate = getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC(_policyEvalContext.Period, "TDIC_YearsInBusiness")
    if(tmpAnswerDelegate != null and tmpAnswerDelegate.hasAnswer()){
      var numberOfYearsInBusiness = tmpAnswerDelegate.IntegerAnswer
      var numberOfPriorPolicies : Integer = _policyEvalContext?.Period?.Policy?.PriorPolicies?.Count
      numberOfPriorPolicies = (numberOfPriorPolicies == null) ? 0 : numberOfPriorPolicies
      var numberOfYearsMissing = 0
      // if answer >= 4, then prior policies must have at least 4 entries, OR if answer < 4, then prior policy entries must match number of years in answer
      numberOfYearsMissing = (numberOfYearsInBusiness > 4 ? 4 : numberOfYearsInBusiness) - numberOfPriorPolicies
      if (numberOfYearsMissing > COMPANYLIMIT_WC7RiskAnalysisPriorPolicies){
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7RiskAnalysisPriorPolicies.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7RiskAnalysisPriorPolicies.LongDesc")
        _policyEvalContext.addIssue("TDIC_WC7RiskAnalysisPriorPolicies", "TDIC_WC7RiskAnalysisPriorPolicies", shortDesc, longDesc, numberOfYearsInBusiness - numberOfPriorPolicies)
      }
    }
  }


  /*
  Create Underwriter Issue if there is overlap in coverage for prior policy
   */
  private function quoteRiskAnalysisPriorPoliciesGapsOrOverlaps_TDIC() {
    var overlapsOrGapsFoundMsg : String = _policyEvalContext.Period.RiskAnalysisPriorPolicyWarningMessage
    if (overlapsOrGapsFoundMsg != null){
      var longDescription = \ -> overlapsOrGapsFoundMsg
      var shortDescription = \ -> DisplayKey.get("TDIC.GapInPriorCoverage")
      _policyEvalContext.addIssue("TDIC_WC7GapOrOverLapInCov", "TDIC_WC7GapOrOverLapInCov", shortDescription, longDescription)
    }
  }

  /**
   * US977
   * Rob Kelly 02/06/2015
   *
   * If "No Loss History" is selected as Loss History Type on Prior Losses screen but this is not a new venture,
   * then create a TDIC_WC7NoLossHistory UW issue.
   */
  private function quoteRiskAnalysisNoLossHistory_TDIC() {
    var tmpYearsInBusiness : Integer = 0
    var newVenture : boolean = false
    var tmpAnswerDelegate : entity.PCAnswerDelegate
    //20160314 TJT GW-299: the Question Set may not be present on txns owned by Migrated Accounts
    tmpAnswerDelegate = getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC(_policyEvalContext.Period, "TDIC_YearsInBusiness")
    if (tmpAnswerDelegate != null and tmpAnswerDelegate.hasAnswer()) {
      tmpYearsInBusiness = tmpAnswerDelegate.IntegerAnswer

      if (tmpYearsInBusiness != null and tmpYearsInBusiness == 0) {
        tmpAnswerDelegate = getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC(_policyEvalContext.Period, "TDIC_NewVenture")
        if (tmpAnswerDelegate != null and tmpAnswerDelegate.hasAnswer()) {
          newVenture = tmpAnswerDelegate.BooleanAnswer.booleanValue()
        }
      }
      if (tmpYearsInBusiness != null and newVenture != true and _policyEvalContext.Period.Policy.LossHistoryType == typekey.LossHistoryType.TC_NOL) {
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7NoLossHistory.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7NoLossHistory.LongDesc")
        _policyEvalContext.addIssue("TDIC_WC7NoLossHistory", "TDIC_WC7NoLossHistory", shortDesc, longDesc)
      }
    }
  }

  /**
   * US977
   * Rob Kelly 02/06/2015
   *
   * If "Attached" is selected as Loss History Type on Prior Losses screen but the # of years of loss history attached is less the number of years in business with WC coverage,
   * then create a TDIC_WC7MissingLossHistory UW issue.
   */
  private function quoteRiskAnalysisLossHistoryMissing_TDIC() {
    //20160314 TJT GW-299: the Question Set may not be present on txns owned by Migrated Accounts
    var yearsInBusiness : Integer = 0
    var tmpAnswerDelegate : entity.PCAnswerDelegate = getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC(_policyEvalContext.Period, "TDIC_YearsInBusiness")
    if (tmpAnswerDelegate != null and tmpAnswerDelegate.hasAnswer()){
      yearsInBusiness = tmpAnswerDelegate.IntegerAnswer

      if (yearsInBusiness != null and _policyEvalContext.Period.Policy.LossHistoryType == typekey.LossHistoryType.TC_ATT) {
        var maxYearsRequired = yearsInBusiness > 4 ? 4 : yearsInBusiness
        var numYearsRequired = maxYearsRequired - _policyEvalContext.Period.Policy.NumYearsOfPriorLosses_TDIC
        if (numYearsRequired > COMPANYLIMIT_WC7MissingLossHistory){
          var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7MissingLossHistory.ShortDesc")
          var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7MissingLossHistory.LongDesc", _policyEvalContext.Period.Policy.NumYearsOfPriorLosses_TDIC, numYearsRequired)
          _policyEvalContext.addIssue("TDIC_WC7MissingLossHistory", "TDIC_WC7MissingLossHistory", shortDesc, longDesc)
        }
      }
    }
  }


  /**
   * US878
   * Shane Sheridan 01/26/2015; 20170204 TJT refactored
   */
  private function quoteFirstEmployeeStartBeforeEffectiveDate_TDIC(){
    var tmpAnswerDelegate : entity.PCAnswerDelegate = getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC(_policyEvalContext.Period, "TDIC_FirstEmployeeStarted")
    if(tmpAnswerDelegate != null and tmpAnswerDelegate.hasAnswer()){
      // trim dates for no time difference
      var firstEmploymentStartDate = tmpAnswerDelegate.DateAnswer.trimToMidnight()
      var trimmedPeriodStart = _policyEvalContext.Period.PeriodStart.trimToMidnight()
      var daysDifference = firstEmploymentStartDate.daysBetween(trimmedPeriodStart)
      if (firstEmploymentStartDate.compareIgnoreTime(trimmedPeriodStart) < 0 and daysDifference > COMPANYLIMIT_WC7EmploymentPriorToEffectiveDate){
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_EmploymentPriorToEffectiveDate.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_EmploymentPriorToEffectiveDate.LongDesc")
        _policyEvalContext.addIssue("TDIC_WC7EmploymentPriorToEffectiveDate", "TDIC_WC7EmploymentPriorToEffectiveDate", shortDesc, longDesc, daysDifference)
      }
    }
  }

  //20170215 TJT - GW-2380
  //TODO this needs to be refactored if TDIC implements Owner Officer accurately from the WC7 Extension Pack (subtypes reflect inclusion/exclusion)
  //Ignore txns on the current policy.  Use case: no cancels/non-renews on prior history at Submission, but
  // system will evalauate this for the first time on a Reinstatement or Renewal and accidentally discover
  // a cancellation on the current policy.
  private function quoteAccountUnhealthy_TDIC(){

    var contactScope : Contact = _policyEvalContext.Period.getSlice(_policyEvalContext.Period.EditEffectiveDate).PrimaryNamedInsured.ContactDenorm
    var caf = new gw.contact.ContactAssociationFinder(contactScope)

    // PNI Cancels
    var numPNIPriorCancellationsOpen = caf.findWorkOrders(false, typekey.Job.TC_CANCELLATION, null, null)
        .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy)
    var numPNIPriorCancellationsClosed = caf.findWorkOrders(true, typekey.Job.TC_CANCELLATION, null, null)
        .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy)
    var numPNIPriorCancellations = numPNIPriorCancellationsClosed + numPNIPriorCancellationsOpen

    // PNI Non Renewals
    var numPNIPriorNonRenewalsClosed = (caf.findWorkOrders(true, typekey.Job.TC_RENEWAL, null, null))
      .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy
                            and
                            (elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWING
                              or
                            elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWED)
                  )
    var numPNIPriorNonRenewalsOpen = (caf.findWorkOrders(false, typekey.Job.TC_RENEWAL, null, null))
        .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy
            and
            (elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWING
                or
                elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWED)
                  )
    var numPNIPriorNonRenewals = numPNIPriorNonRenewalsClosed + numPNIPriorNonRenewalsOpen

    // OO Non Renewals
    var numOOPriorNonRenewals : int = 0
    for (eachWC7PolicyOwnerOfficer in _policyEvalContext.Period.getSlice(_policyEvalContext.Period.EditEffectiveDate).WC7Line.WC7PolicyOwnerOfficers){
      var caf3 = new gw.contact.ContactAssociationFinder(eachWC7PolicyOwnerOfficer.ContactDenorm)
      var numOOPriorNonRenewalsClosed = caf3.findWorkOrders(true, typekey.Job.TC_RENEWAL, null, null)
          .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy
                                and
                                (elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWING
                                  or
                                elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWED)
                     )
      var numOOPriorNonRenewalsOpen = caf3.findWorkOrders(false, typekey.Job.TC_RENEWAL, null, null)
          .countWhere( \ elt -> elt.LatestPeriod.Policy != _policyEvalContext.Period.Policy
              and
              (elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWING
                  or
                  elt.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_NONRENEWED)
                      )
      numOOPriorNonRenewals = numOOPriorNonRenewalsClosed + numOOPriorNonRenewalsOpen
    }

    if (numPNIPriorCancellations + numPNIPriorNonRenewals + numOOPriorNonRenewals > COMPANYLIMIT_WC7AccountUnhealthy){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7AccountUnhealthy.ShortDesc", numPNIPriorCancellations + numPNIPriorNonRenewals + numOOPriorNonRenewals)
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7AccountUnhealthy.LongDesc", numPNIPriorCancellations + numPNIPriorNonRenewals + numOOPriorNonRenewals)
      _policyEvalContext.addIssue("TDIC_WC7AccountUnhealthy", "TDIC_WC7AccountUnhealthy", shortDesc, longDesc, numPNIPriorCancellations + numPNIPriorNonRenewals + numOOPriorNonRenewals)
    }
  }

  //20170215 TJT - GW-2380
  private function quoteAccountUnpaidBalance_TDIC(){
    // Look for an issued policy on this account, then the account exists in BC (or will when messaging queue is cleared)
    // If there isn't an Issued policy, then there's no point looking for unpaid charges on prior activity
    if (_policyEvalContext.Period.Policy.Account.IssuedPolicies.Count > 0){
      //var billingPlugin = gw.plugin.Plugins.get(gw.plugin.billing.IBillingSummaryPlugin)
      var billingTotals : gw.plugin.billing.BCAccountBillingDisplayTotals
      var pastDueBalance : MonetaryAmount = new MonetaryAmount(0.00, _policyEvalContext.Period.PreferredSettlementCurrency)
      var writeoffBalance : MonetaryAmount = new MonetaryAmount(0.00, _policyEvalContext.Period.PreferredSettlementCurrency)
      var unpaidBalance : MonetaryAmount = new MonetaryAmount(0.00, _policyEvalContext.Period.PreferredSettlementCurrency)

      var billingSummary = gw.web.account.AccountBillingUIHelper.retrieveBillingSummary({_policyEvalContext.Period.Policy.Account.AccountNumber}, _policyEvalContext.Period.Policy.Account.PreferredSettlementCurrency)
      if (billingSummary != null){
        billingTotals = billingSummary as gw.plugin.billing.BCAccountBillingDisplayTotals
        if (billingTotals != null){
          //20170807 TJT: GW-2848 added WrittenOff to this computation`
          pastDueBalance = billingTotals.BilledOutstandingPastDue.Total
          writeoffBalance = billingTotals.WrittenOff_TDIC.Total
          unpaidBalance = pastDueBalance + writeoffBalance
        }
      }
      if (unpaidBalance > COMPANYLIMIT_WC7UnpaidBalance){
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7AccountUnpaidBalance.ShortDesc", unpaidBalance)
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7AccountUnpaidBalance.LongDesc", unpaidBalance, pastDueBalance, writeoffBalance)
        _policyEvalContext.addIssue("TDIC_WC7AccountUnpaidBalance", "TDIC_WC7AccountUnpaidBalance", shortDesc, longDesc, unpaidBalance)
      }
    }
  }
  //20171204 TJT GW-
  function quoteOwnershipPercentage_TDIC() {
    // TJT: copying this from WC7PolicyInfoValidation, and simplifying...
    var policyOwnerOfficers : WC7PolicyOwnerOfficer[] = _policyEvalContext.Period.WC7Line.WC7PolicyOwnerOfficers.where( \ oo -> oo.IntrinsicType != PolicyEntityOwner_TDIC)
    if (policyOwnerOfficers == null or policyOwnerOfficers.length == 0){
      return // have none
    }
    var totalOwnership = 0
    for (anOwnerOfficer in policyOwnerOfficers) {
      // BrianS - Adding nulls will get a null pointer exception.  Treat as 0%
      if (anOwnerOfficer.WC7OwnershipPct != null) {
        totalOwnership += anOwnerOfficer.WC7OwnershipPct.intValue()
      }
    }
    // TJT: end copiied snippet

    if (totalOwnership > COMPANYLIMIT_WC7OwnershipPercentage and totalOwnership < 100){
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICOwnershipPercentage.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDICOwnershipPercentage.LongDesc")
      var issueKey = "TDIC_OwnershipPercentage" + _policyEvalContext.Period.Job.JobNumber
      _policyEvalContext.addIssue("TDIC_OwnershipPercentage", issueKey, shortDesc, longDesc, totalOwnership)
    }
  }

  /**
   * JIRA -3049
   * This function creates a "TDIC_WC7PreviousEstimatedAudits" Underwriting Issue if the expiring policy has estimated audit for previous term.
   */
  private function renewalPreviousEstimatedAudits_TDIC(){
    var renewalTerm = _policyEvalContext.Period.TermNumber
    var previousTerm = renewalTerm - 2
    var policyNumber = _policyEvalContext.Period.PolicyNumber.toString()
    _logger.info("#### renewalPreviousEstimatedAudits_TDIC- Start -- "+policyNumber+"-renewalTerm----"+renewalTerm+"-previousTerm---"+previousTerm)
    var previousPolicyPeriod = gw.api.database.Query.make(PolicyPeriod).select().where( \ elt -> elt.PolicyNumber == policyNumber and elt.TermNumber == previousTerm).first()
    if(previousPolicyPeriod != null){
      previousPolicyPeriod?.DisplayableAuditInfoList.each( \ elt -> {
        var audit = elt.Audit
        if(elt.Audit != null){
          var addAuditInfo = audit?.AuditInformation
          _logger.info("#### Results - "+addAuditInfo. ActualAuditMethod+"--"+  addAuditInfo.DisplayStatus+"--"+previousPolicyPeriod.TermNumber+"--"+previousPolicyPeriod.EditEffectiveDate+"--"+addAuditInfo.AuditPeriodStartDate+"--"+addAuditInfo.AuditPeriodEndDate)
          var isEligibleDisplayStatus = addAuditInfo.DisplayStatus == "Completed" or addAuditInfo.DisplayStatus == "In Progress"

          if(addAuditInfo.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED
              and isEligibleDisplayStatus
              and addAuditInfo.AuditPeriodStartDate.equals(previousPolicyPeriod.EditEffectiveDate)
              and addAuditInfo.AuditPeriodEndDate.equals((previousPolicyPeriod.PeriodEnd)) ) {
            var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7PreviousEstimatedAudits.ShortDesc")
            var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7PreviousEstimatedAudits.LongDesc")
            _policyEvalContext.addIssue("TDIC_WC7TwoPreviousEstimatedAudits", "TDIC_WC7TwoPreviousEstimatedAudits", shortDesc, longDesc)
            _logger.info("####  UW Activity TDIC_WC7PreviousEstimatedAudit Created -- "+_policyEvalContext.Period.PolicyNumber+"--"+_policyEvalContext.Period.TermNumber)

          }
        }
      })
    }
  }


  /**
   * US469
   * Shane Sheridan 01/06/2014
   *
   * This function creates a "TDIC_WC7ScheduledRatingFactorOutsideRange" Underwriting Issue if  Scheduled Rating Factor is outside of range on expiring policy.
   *
   */

  //TODO TJT - candidate to remove when limits are in the ratebook/product model (this was used to constrain wider company limits that are outside of the product model during migration
  // 20170301 TJT Because this issue is consolidated (limits in both positive and negative directions), we can't pass a value to the grant for comparision. It must be none/any.
  private function renewalScheduledRatingFactorOutsideRange_TDIC(){
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions) {
      //var modifiers = jurisdiction.WC7Modifiers
      //for(aModifier in modifiers) {
      for (eachRatingPeriod in jurisdiction.RatingPeriods){
        for (aModifier in eachRatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(eachRatingPeriod.RatingStart)){
          if(aModifier.Pattern.CodeIdentifier == "WC7ScheduleMod") {
            var rateValueAsDouble = aModifier.RateWithinLimits
            if(rateValueAsDouble < COMPANYLIMIT_WC7ScheduledRatingFactorOutsideRange_CREDIT or rateValueAsDouble > COMPANYLIMIT_WC7ScheduledRatingFactorOutsideRange_DEBIT){
              var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduledRatingFactorOutsideRange.ShortDesc")
              var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ScheduledRatingFactorOutsideRange.LongDesc")
              _policyEvalContext.addIssue("TDIC_WC7ScheduledRatingFactorOutsideRange", "TDIC_WC7ScheduledRatingFactorOutsideRange", shortDesc, longDesc)
            }
          }
        }
      }
    }
  }

  /**
   * US469
   * Shane Sheridan 01/06/2014
   *
   * This function creates a "TDIC_WC7ExperienceRatingModifierExists" Underwriting Issue if expiring policy has an experience rating modifier.
   */
  private function renewalExperienceRatingModifierExists_TDIC(){
    for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions){
      //for(aModifier in jurisdiction.WC7Modifiers){
      for (eachRatingPeriod in jurisdiction.RatingPeriods){
        for (aModifier in eachRatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(eachRatingPeriod.RatingStart)){
          if(aModifier.Pattern.CodeIdentifier == "WC7ExpMod") {
            if(aModifier.RateWithinLimits != null and aModifier.Eligible and aModifier.RateWithinLimits != 0){
              var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExperienceRatingModifierExists.ShortDesc")
              var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ExperienceRatingModifierExists.LongDesc")
              _policyEvalContext.addIssue("TDIC_WC7ExperienceRatingModifierExists", "TDIC_WC7ExperienceRatingModifierExists", shortDesc, longDesc)
            }
          }
        }
      }
    }
  }

  private function renewalMoneyStillOwedOnPolicy_TDIC() {
    //TODO TJT - fix the fetch for term number (who did this and why?)
    var period = _policyEvalContext.Period
    var billingPlugin = gw.plugin.Plugins.get(gw.plugin.billing.IBillingSummaryPlugin)
    //var sortedTermNumbers = gw.web.policy.PolicyBillingUIHelper.getSortedTermNumbers(period.Policy)
    //var termNumberIndex = gw.web.policy.PolicyBillingUIHelper.getTermNumberIndex(period.BasedOn, sortedTermNumbers)
    var billingSummary = billingPlugin.retrievePolicyBillingSummary(period.BasedOn.PolicyNumber, period.BasedOn.TermNumber)
    var previousTermPaid = false
    var totalUnpaid : MonetaryAmount = new MonetaryAmount(0, period.PreferredSettlementCurrency)
    for (anInvoice in billingSummary.Invoices) {
      totalUnpaid = totalUnpaid.add(anInvoice.Amount)
      totalUnpaid = totalUnpaid.subtract(anInvoice.Paid)
    }
    if (totalUnpaid > COMPANYLIMIT_TDIC_MoneyOverThresholdStillOwedOnPolicy ) {
      var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_MoneyOverThresholdStillOwedOnPolicy.ShortDesc")
      var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_MoneyOverThresholdStillOwedOnPolicy.LongDesc")
      _policyEvalContext.addIssue("TDIC_MoneyOverThresholdStillOwedOnPolicy", "TDIC_MoneyOverThresholdStillOwedOnPolicy", shortDesc, longDesc, totalUnpaid.Amount.setScale(2))
    }
  }

  /**
   * GW - 2257 - Generate an UW-issue when the schedule rating is applicable and manual premium is less than the authority profile
   * Rajesh N 10/18/2016
   */
  // GW-2745 TJT - This is part of a filing that takes affect on Jan 1, 2017.  Suppress on txns that are effective prior to this date
  private function quotereleaseSchedRatingManPremExceeded_TDIC() {
    //TODO TJT - use the OOTB Man Prem?
    if (gw.api.util.DateUtil.compareIgnoreTime(_policyEvalContext.Period.PeriodStart, gw.api.util.DateUtil.createDateInstance(1,1,2017)) >= 0){
      var manualPremium = _policyEvalContext.Period.ManualPremium_TDIC
      for(jurisdiction in _policyEvalContext.Period.WC7Line.WC7Jurisdictions) {
        var modifiers = jurisdiction.WC7Modifiers
        for(aModifier in modifiers) {
          if(aModifier.PatternCode == "WC7ScheduleMod" and aModifier.Eligible and aModifier.RateWithinLimits != 0 and manualPremium <= COMPANYLIMIT_WC7SchedRatingManPremExceeded) {
            var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ManualPremiumLimit.ShortDesc", manualPremium)
            var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_WC7ManualPremiumLimit.LongDesc", manualPremium)
            _policyEvalContext.addIssue("TDIC_WC7SchedRatingManPremExceeded", "TDIC_WC7SchedRatingManPremExceeded", shortDesc, longDesc, manualPremium)
          }
        }
      }
    }
  }

  /**
   * Account has Zero Payroll in Audited Basis
   */
  private function accountHasZeroPayrollAuditedBasis_TDIC() {
    var period = _policyEvalContext.Period
    if(period.Job typeis Audit and period.ValidQuote == false) {
      var hasZeroPayroll = false
      for (jurisdiction in period.WC7Line.WC7Jurisdictions) {
        for (covEmp in period.WC7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
          if (covEmp.AuditedAmount == null or covEmp.AuditedAmount == 0) {
            hasZeroPayroll = true
            break
          }
        }
      }
      if(hasZeroPayroll) {
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollAuditedBasis.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollAuditedBasis.LongDesc")
        _policyEvalContext.addIssue("TDIC_AccountHasZeroPayrollAuditedBasis", "TDIC_AccountHasZeroPayrollAuditedBasis", shortDesc, longDesc)
      }
    }
  }

  private function accountHasDetainedZeroPayrollBasis_TDIC() {
    var period = _policyEvalContext.Period
    if(period.Job typeis Renewal) {
      if(hasZeroPayrollBasis(period)) {
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollBasis.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollBasis.LongDesc")
        _policyEvalContext.addIssue("TDIC_AccountHasDetainedZeroPayrollBasis", "TDIC_AccountHasDetainedZeroPayrollBasis", shortDesc, longDesc)
      }
    }
  }

/*  private function accountHasZeroPayrollBasisPreRenewalDirection_TDIC() {
    var period = _policyEvalContext.Period
    var hasPreRenewalDirection = period.PolicyTerm.PreRenewalDirection != null
    if(period.Job typeis Renewal) {
      if(hasZeroPayrollBasis(period) and hasPreRenewalDirection) {
        var shortDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollBasis.ShortDesc")
        var longDesc = \ -> DisplayKey.get("TDIC.UWIssue.WC7.TDIC_AccountHasZeroPayrollBasis.LongDesc")
        _policyEvalContext.addIssue("TDIC_AccountHasZeroPayrollBasis", "TDIC_AccountHasZeroPayrollBasis", shortDesc, longDesc)
      }
    }
  }*/

 // H E L P E R  F U N C T I O N S
  private function getAnswerFromSupplementalQuestionIfQuestionVisible_TDIC (passedInPeriod : PolicyPeriod, passedInQuestionCode : String) : entity.PCAnswerDelegate{
    var tmpAnswer : entity.PCAnswerDelegate = null
    var tmpQuestion : gw.api.productmodel.Question
    var tmpQuestionSet : gw.api.productmodel.QuestionSet = passedInPeriod.Policy.Product.getQuestionSetByCodeIdentifier("WC7Supplemental")

    if (passedInPeriod.WC7Line.hasSupplementalQuestions()) {
      tmpQuestion = tmpQuestionSet.getQuestionByCodeIdentifier(passedInQuestionCode)
      if (tmpQuestion != null){
        if (tmpQuestion.isQuestionVisible(_policyEvalContext.Period.WC7Line)){
          tmpAnswer = _policyEvalContext.Period.WC7Line.getAnswer(tmpQuestion)
        }
      }else if (tmpQuestion == null){
        //FIXME - question not found, log a warning?  May not be an error?
      }
    }
    return tmpAnswer
  }

  /**
   * US469
   * Shane Sheridan 01/07/2015
   *
   * This function retrieves all bound periods associated with this policy, over a specified number of years.
   *
   */
  @Returns("all bound periods associated with this policy, over a specified number of years.")
  private function getPreviousPeriods() : PolicyPeriod[] {
    var renewalPeriod = _policyEvalContext.Period

    // get periods that:
    // have have an expiration date within the last [number] of years,
    // are not future scheduled policies, i.e. have an effective date before today,
    var currentDate = DateUtil.currentDate()
    var numberOfYears = COMPANYREQUIREMENT_WC7NumberOfYearsForLossRatioCalculation
    var pastDate = currentDate.addYears((-numberOfYears) as int)
    var boundPeriods = renewalPeriod.Policy.BoundPeriods.where( \ period -> period.PeriodEnd.afterOrEqual(pastDate)
                                                                            and period.PeriodStart.before(currentDate)
                                                                            and period.CancellationDate == null)
    // get the latest period per policy term (based on ModelDate); filter out the others
    var latestPerTermPeriods = boundPeriods.where( \ period -> period.IsPolicyTermLatestPeriodByModelDate )

    return latestPerTermPeriods
  }

  /**
   * US469
   * Shane Sheridan 01/11/2015
   *
   * This function searches for and retrieves the Claims on the given PolicyPeriod and returns a ClaimSet.
   */
  private function searchForClaims_TDIC(basedOn : PolicyPeriod) : Claim[] {
    var result : ClaimSet
    var matchingClaims : Claim[]
    try{
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var searchCriteria = new ClaimSearchCriteria()
        searchCriteria.Policy = basedOn.Policy
        searchCriteria.DateCriteria.StartDate = basedOn.PeriodStart
        searchCriteria.DateCriteria.EndDate = basedOn.EndOfCoverageDate
        searchCriteria.DateCriteria.DateSearchType = TC_ENTEREDRANGE
        result = Plugins.get(IClaimSearchPlugin).searchForClaims(searchCriteria)
      })
    }
    catch (ex : NoResultsClaimSearchException) {
      // no claims found on this particular policy
    }
    catch (ex : ResultsCappedClaimSearchException) {// Create ManualClaimReviewNeeded
          var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.ShortDesc")
          var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
          _policyEvalContext.addIssue("ManualClaimReviewNeeded", "ManualClaimReviewNeeded",
              shortDescription, longDescription)
    }
    catch(ex : SOAPServerException) {   //FIXME TJT - remove this
      //Create UnableRetrieveClaimInfo
      var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.ShortDesc")
      var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
      _policyEvalContext.addIssue("UnableRetrieveClaimInfo", "UnableRetrieveClaimInfo", shortDescription, longDescription)
    }
    if( result != null ) {
      matchingClaims = result.Claims.where( \ clm -> clm.PolicyPeriod == basedOn )
    }
    return matchingClaims
  }


  //GW-1773 - Retrieving Open Claims for Open Claim UWIssue
  //FIXME TJT - if CC is down, there should be a hardstop (UI) or automatic retry (automated renewal processing)
  //FIXME TJT - Too many claims in CC shouldn't throw an UW Issue - what is the CC problem?
  //FIXME TJT - CC down doesn't need to raise an UW Issue or catch this error, it should throw the error - what is the CC problem?
  private function searchForOpenClaims_TDIC(basedOn : PolicyPeriod) : Claim[] {
    var result : ClaimSet
    var matchingClaims : Claim[]

    try{
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var searchCriteria = new ClaimSearchCriteria()
        searchCriteria.Policy = basedOn.Policy
        searchCriteria.DateCriteria.StartDate = basedOn.PeriodStart
        searchCriteria.DateCriteria.EndDate = basedOn.EndOfCoverageDate
        searchCriteria.DateCriteria.DateSearchType = TC_ENTEREDRANGE
        result = Plugins.get(IClaimSearchPlugin).searchForClaims(searchCriteria)
      })
    }
        catch (ex : NoResultsClaimSearchException) {
          // no claims found on this particular policy
        }
        catch (ex : ResultsCappedClaimSearchException) {// Create ManualClaimReviewNeeded
          var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.ShortDesc")
          var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
          _policyEvalContext.addIssue("ManualClaimReviewNeeded", "ManualClaimReviewNeeded",
              shortDescription, longDescription)
        }
        catch(ex : SOAPServerException) { //FIXME TJT - remove this entire catch outright
          //Create UnableRetrieveClaimInfo
          var shortDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.ShortDesc")
          var longDescription = \ -> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
          _policyEvalContext.addIssue("UnableRetrieveClaimInfo", "UnableRetrieveClaimInfo", shortDescription, longDescription)
        }
    if( result != null ) {
      matchingClaims = result.Claims.where(\ clm -> clm.Status.equalsIgnoreCase("Open"))
    }

    return matchingClaims
  }

  /**
   * check if payroll has zero value
   */
  private function hasZeroPayrollBasis(period : PolicyPeriod) : Boolean {
    for (jurisdiction in period.WC7Line.WC7Jurisdictions) {
      for (covEmp in period.WC7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (covEmp.BasisAmount == null or covEmp.BasisAmount == 0) {
          return true
        }
      }
    }
    return false
  }
}