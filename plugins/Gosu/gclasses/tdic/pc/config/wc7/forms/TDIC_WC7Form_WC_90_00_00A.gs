package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set
uses java.util.List

/**
 * Created with IntelliJ IDEA.
 * User: TimT
 * Date: 5/3/16
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WC7Form_WC_90_00_00A extends WC7FormData {

  var _period : PolicyPeriod as Period
  var _additionalNamedInsureds : List<PolicyAddlNamedInsured> as AdditionalNamedInsureds

  //See FormData.gs for abstract description of the use for this function.
  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _period = context.Period
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    var blnWC7CertifiedSafetyExists : boolean = false
    for (eachWC7Jurisdiction in _period.WC7Line.WC7Jurisdictions){
      for (eachWC7Modifier in eachWC7Jurisdiction.WC7Modifiers.where( \ elt -> elt.PatternCode == "WC7CertifiedSafety")){
        if (eachWC7Modifier.BooleanModifier == true){
          blnWC7CertifiedSafetyExists = true
          break
        }
      }
    }
    return blnWC7CertifiedSafetyExists
  }

  override function addDataForComparisonOrExport(contentNode: XMLNode) {
  }
}