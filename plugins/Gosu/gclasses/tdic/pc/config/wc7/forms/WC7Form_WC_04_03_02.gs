package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set
uses java.util.Calendar
uses java.util.Date

class WC7Form_WC_04_03_02 extends WC7FormData {
  var _wc7Line : WC7WorkersCompLine;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Line = context.Period.WC7Line;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return _wc7Line.Branch.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP
       and _wc7Line.WC7PartnersOfficersAndOthersExclEndorsementExclExists;
  }

  /**
   * Schedule of Exclusion Name and Title
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var exclusionNode : XMLNode;
    var exclusionsNode = new XMLNode("Exclusions");
    contentNode.addChild(exclusionsNode);

    for (exclusion in _wc7Line.WC7PolicyOwnerOfficers.where(\oo -> oo.isExcluded_TDIC).orderBy(\oo -> oo.FixedId)) {
      exclusionNode = new XMLNode("Exclusion");
      exclusionNode.addChild(createTextNode("Exclusion", exclusion as String));
      exclusionNode.addChild(createTextNode("Title", exclusion.RelationshipTitle.Code));
      exclusionsNode.addChild(exclusionNode);
    }
  }

  /**
  * Use job effective date unless job effective is on or after 1/01/2017.
   */
  override function getLookupDate (context : FormInferenceContext, state : Jurisdiction) : Date {
    var calendar = Calendar.getInstance();
    calendar.set (2017, Calendar.JANUARY, 1);
    var ab2883Date = calendar.Time;

    var lookupDate = context.Period.EditEffectiveDate;
    if (lookupDate.compareIgnoreTime(ab2883Date) < 0) {
      lookupDate = super.getLookupDate (context, state);
    }

    return lookupDate;
  }
}