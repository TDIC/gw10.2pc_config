package tdic.pc.config.pcf.contacts.name

uses gw.search.BasicNameOwner
uses java.util.Set
uses gw.api.name.ContactNameFields
uses gw.api.name.NameOwnerFieldId

@Export
/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 18/08/14
 * Time: 18:11
 * This class is used to determine the properties of UI fields in GlobalPersonNameInputSet.default.pcf.
 */
class TDIC_RequiredBasicNameOwner extends BasicNameOwner {

  /* Set of gw.api.name.NameOwnerFieldId objects to specify visible fields. */
  var _visible : Set<NameOwnerFieldId> = {NameOwnerFieldId.PREFIX,
                                          NameOwnerFieldId.FIRSTNAME,
                                          NameOwnerFieldId.MIDDLENAME,
                                          NameOwnerFieldId.LASTNAME,
                                          NameOwnerFieldId.SUFFIX}


  /* Set of gw.api.name.NameOwnerFieldId objects to specify required fields. */
  //GW-548: First name and Last name should be mandatory
  var _required : Set<NameOwnerFieldId> = {NameOwnerFieldId.PREFIX, NameOwnerFieldId.FIRSTNAME, NameOwnerFieldId.LASTNAME}


  construct(fields : ContactNameFields) {
    super(fields)
  }

  /**
   * This method is used to determine the visibilty of UI fields in GlobalPersonNameInputSet.default.pcf.
  */
  @Param("fieldId", "representation of a UI field")
  @Returns("Boolean based on the required visibilty of the field.")
  override function isVisible(fieldId : NameOwnerFieldId) : boolean {
    // if field is specified as visible - return true
    if(_visible.contains(fieldId)){
      return true
    }
    else{
      return false
    }
  }

  /**
   * This method is used to determine the required UI fields in GlobalPersonNameInputSet.default.pcf.
   */
  override  function isRequired(fieldId : NameOwnerFieldId) : boolean {
    // if field is specified as required - return true
    if(_required.contains(fieldId)){
      return true
    }
    else{
      return false
    }
  }
}