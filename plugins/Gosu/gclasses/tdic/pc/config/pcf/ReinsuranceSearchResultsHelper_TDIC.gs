package tdic.pc.config.pcf

uses java.math.BigDecimal
uses java.math.RoundingMode

/* Jeff Lin, Added for GPC-350 of Property Location Inauiry under top Search Menu of PC.
 * The functions are to gather the values for diaply on the Search Result List View screen.
 */
class ReinsuranceSearchResultsHelper_TDIC {

  final static var PERCENT10 : BigDecimal = 0.10
  final static var PERCENT25 : BigDecimal = 0.25

  public static function getBuildingYearBuilt(bopLine : BOPLine, locNum : Integer) : Integer {
    if (bopLine != null) {
      for (var loc in bopLine.BOPLocations) {
        if (loc.Location.LocationNum == locNum) {
          for (var bldg in loc.Location.Buildings) {
            if (bldg.YearBuilt != null and bldg.YearBuilt > 0) {
              return bldg.YearBuilt
            }
          }
        }
      }
    }
    return null
  }

  public static function getConstructionType(bopLine : BOPLine, locNum : Integer) : String {
    if (bopLine != null) {
      for (var loc in bopLine.BOPLocations) {
        if (loc.Location.LocationNum == locNum) {
          for (var bldg in loc.Buildings) {
            if (bldg.ConstructionType != null and bldg.ConstructionType.DisplayName != null) {
              return bldg.ConstructionType.DisplayName
            }
          }
        }
      }
    }
    return null
  }

  public static function getTotalBOPLinePropertyValue(searchResultsGroup : ArrayList<entity.PolicyLocation>) : BigDecimal {
    var totalPropValue : BigDecimal = 0
    searchResultsGroup.each(\polLoc -> {
      var propValue = getBOPLinePropertyValue(polLoc)
      totalPropValue = totalPropValue + (propValue == null ? 0 : propValue)
    })
    return totalPropValue.setScale(0, RoundingMode.HALF_UP)
  }

  public static function getBOPLinePropertyValue(polLoc : entity.PolicyLocation) : BigDecimal {
    if (polLoc.AssociatedPolicyPeriod.BOPLineExists) {
      var bopLocs = polLoc.Branch.BOPLine.BOPLocations.where(\loc -> loc.Location.LocationNum == polLoc.LocationNum)
      if (polLoc.AssociatedPolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
        return getBOPOfferingPropertyValue(bopLocs).setScale(0, RoundingMode.HALF_UP)
      }
      if (polLoc.AssociatedPolicyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
        return getLRPOfferingPropertyValue(bopLocs).setScale(0, RoundingMode.HALF_UP)
      }
    }
    return null
  }

  private static function getBOPOfferingPropertyValue(bopLocs : BOPLocation[]) : BigDecimal {
    var propValue : BigDecimal = 0

    for (bopLoc in bopLocs) {
      for (var building in bopLoc.Buildings) {
        //buildng
        if (building.BOPBuildingCovExists and building.BOPBuildingCov?.BOPBldgLimTerm?.Value > 0) {
          var bldgValue = building.BOPBuildingCov?.BOPBldgLimTerm?.Value
          propValue += bldgValue
        }
        //personal property
        if (building.BOPPersonalPropCovExists and building.BOPPersonalPropCov?.BOPBPPBldgLimTerm?.Value > 0) {
          var ppValue = building.BOPPersonalPropCov?.BOPBPPBldgLimTerm?.Value
          propValue += ppValue
        }
        // Jeff Lin, 11:04 am, 1/7/2020, GPC-2697 & GPC-2698, Use Total Include Limit if exists, otherwise use basic limit
        //money and security
        if (building.BOPMoneySecCov_TDICExists and building.BOPMoneySecCov_TDIC?.BOPMoneySecInclBasicLimit_TDICTerm?.Value > 0) {
          var moneySecurityValue = building.BOPMoneySecCov_TDIC?.BOPMoneySecTotIncLimit_TDICTerm?.Value > 0 ?
              building.BOPMoneySecCov_TDIC?.BOPMoneySecTotIncLimit_TDICTerm?.Value : building.BOPMoneySecCov_TDIC?.BOPMoneySecInclBasicLimit_TDICTerm?.Value
          propValue += moneySecurityValue
        }
        //extra expense
        if (building.BOPExtraExpenseCov_TDICExists and building.BOPExtraExpenseCov_TDIC?.BOPExtraExpIncBasicLimit_TDICTerm?.Value > 0) {
          var extraExpense = building.BOPExtraExpenseCov_TDIC?.BOPExtraExpTotalIncLimit_TDICTerm?.Value > 0 ?
              building.BOPExtraExpenseCov_TDIC?.BOPExtraExpTotalIncLimit_TDICTerm?.Value : building.BOPExtraExpenseCov_TDIC?.BOPExtraExpIncBasicLimit_TDICTerm?.Value
          propValue += extraExpense
        }
        // gold , metals
        if (building.BOPGoldPreMetals_TDICExists and building.BOPGoldPreMetals_TDIC?.BOPGoldIncBasicLimit_TDICTerm?.Value > 0) {
          var goldMetal = building.BOPGoldPreMetals_TDIC?.BOPGoldTotalIncLimit_TDICTerm?.Value > 0 ?
              building.BOPGoldPreMetals_TDIC?.BOPGoldTotalIncLimit_TDICTerm?.Value : building.BOPGoldPreMetals_TDIC?.BOPGoldIncBasicLimit_TDICTerm?.Value
          propValue += goldMetal
        }
        //valuable papers
        if (building.BOPValuablePapersCov_TDICExists and building.BOPValuablePapersCov_TDIC?.BOPValIncBasicLimit_TDICTerm?.Value > 0) {
          var  valuablePaper = building.BOPValuablePapersCov_TDIC?.BOPValTotIncLimit_TDICTerm?.Value > 0 ?
              building.BOPValuablePapersCov_TDIC?.BOPValTotIncLimit_TDICTerm?.Value : building.BOPValuablePapersCov_TDIC?.BOPValIncBasicLimit_TDICTerm?.Value
          propValue += valuablePaper
        }
        //fine arts
        if (building.BOPFineArtsCov_TDICExists and building.BOPFineArtsCov_TDIC?.BOPFineArtsIncBasicLimit_TDICTerm?.Value > 0) {
          var fineArts = building.BOPFineArtsCov_TDIC?.BOPFineArtsTotIncLimit_TDICTerm?.Value > 0 ?
              building.BOPFineArtsCov_TDIC?.BOPFineArtsTotIncLimit_TDICTerm?.Value : building.BOPFineArtsCov_TDIC?.BOPFineArtsIncBasicLimit_TDICTerm?.Value
          propValue += fineArts
        }
        //account recievble
        if (building.BOPAccReceivablesCov_TDICExists and building.BOPAccReceivablesCov_TDIC?.BOPAccIncBasicLimit_TDICTerm?.Value > 0) {
          var accRecievable = building.BOPAccReceivablesCov_TDIC?.BOPACcTotalIncLimit_TDICTerm?.Value > 0 ?
              building.BOPAccReceivablesCov_TDIC?.BOPACcTotalIncLimit_TDICTerm?.Value : building.BOPAccReceivablesCov_TDIC?.BOPAccIncBasicLimit_TDICTerm?.Value
          propValue += accRecievable
        }
        //signs
        if (building.BOPSigns_TDICExists and building.BOPSigns_TDIC?.BOPIncBasicLimitSigns_TDICTerm?.Value > 0) {
          var signs = building.BOPSigns_TDIC?.BOPTotalInLimitSigns_TDICTerm?.Value > 0 ?
              building.BOPSigns_TDIC?.BOPTotalInLimitSigns_TDICTerm?.Value : building.BOPSigns_TDIC?.BOPIncBasicLimitSigns_TDICTerm?.Value
          propValue += signs
        }
        //Loss of income
        if (building.BOPLossofIncomeTDICExists and building.BOPLossofIncomeTDIC?.BOPTotalIncLimit_TDICTerm?.Value > 0) {
          var lossOfIncome = building.BOPLossofIncomeTDIC?.BOPTotalIncLimit_TDICTerm?.Value * 10
          propValue += lossOfIncome
        }
        //persoanl effects
        if (building.BOPPersonalEffects_TDICExists and building.BOPPersonalEffects_TDIC?.BOPPersonalEffectsIncLimit_TDICTerm?.Value > 0) {
          var pEffects = building.BOPPersonalEffects_TDIC?.BOPPersonalEffectsIncLimit_TDICTerm?.Value
          propValue += pEffects
        }
        //Ordinance Law
        if (building.BOPOrdLaw_TDICExists and building.BOPBuildingCov?.BOPBldgLimTerm?.Value > 0) {
          // Jeff Lin, 11:04 am, 1/7/2020, GPC-2697 & GPC-2698, Year built < 1990 -> 25%, or 10% otherwise
          var ordinanceLaw = building.Building.YearBuilt < 1990 ? (building.BOPBuildingCov?.BOPBldgLimTerm?.Value * PERCENT25) : (building.BOPBuildingCov?.BOPBldgLimTerm?.Value * PERCENT10).setScale(0, RoundingMode.HALF_UP)
          propValue += ordinanceLaw
        }
        // BPP Debris cov term
        if (building.BOPBPPDebrisRemoval_TDICExists and building.BOPBPPDebrisRemoval_TDIC?.BOPBPPDebrisRemoInclLimit_TDICTerm?.Value > 0) {
          var bppDebri = building.BOPBPPDebrisRemoval_TDIC?.BOPBPPDebrisRemoInclLimit_TDICTerm?.Value
          propValue += bppDebri
        }
        // Bldg Debris cov term
        if (building.BOPBuilDebrisRemoval_TDICExists and building.BOPBuilDebrisRemoval_TDIC?.BOPDebrisRemovInclLimit_TDICTerm?.Value > 0) {
          var bldgDebri = building.BOPBuilDebrisRemoval_TDIC?.BOPDebrisRemovInclLimit_TDICTerm?.Value
          propValue += bldgDebri
        }
      }
    }
    return propValue
  }

  private static function getLRPOfferingPropertyValue(bopLocs : BOPLocation[]) : BigDecimal {
    var propValue : BigDecimal = 0

    for (bopLoc in bopLocs) {
      for (var building in bopLoc.Buildings) {
        if (building.BOPBuildingCovExists and building.BOPBuildingCov?.BOPBldgLimTerm?.Value > 0) {
          propValue += building.BOPBuildingCov?.BOPBldgLimTerm?.Value
        }
        // Jeff Lin, 11:04 am, 1/7/2020, GPC-2697 & GPC-2698, Year built < 1990 -> 25%, or 10% otherwise, Remove BPP Debris cov term limit as well
        if (building.BOPOrdLaw_TDICExists and building.BOPBuildingCov?.BOPBldgLimTerm?.Value > 0) {
          propValue += (building.Building.YearBuilt < 1990 ? building.BOPBuildingCov?.BOPBldgLimTerm?.Value * PERCENT25 : building.BOPBuildingCov?.BOPBldgLimTerm?.Value * PERCENT10).setScale(0, RoundingMode.HALF_UP)
        }
        // Bldg Debris cov term
        if (building.BOPBuilDebrisRemoval_TDICExists and building.BOPBuilDebrisRemoval_TDIC?.BOPDebrisRemovInclLimit_TDICTerm?.Value > 0) {
          propValue += building.BOPBuilDebrisRemoval_TDIC?.BOPDebrisRemovInclLimit_TDICTerm?.Value
        }
      }
    }
    return propValue
  }
}