package tdic.pc.config.globalization

uses gw.globalization.PolicyContactRoleNameAdapter
uses java.lang.Exception
uses java.lang.NullPointerException

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 10/15/14
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_PolicyContactRoleNameAdapter extends PolicyContactRoleNameAdapter {
  var _policyContactRole : PolicyContactRole

  // Constructor
  construct(policyContactRole : PolicyContactRole) {
    super(policyContactRole)
    _policyContactRole = policyContactRole
  }

  /**
   * US891, robk
   * TDIC require MiddleName synched and revisioned.
   */
  override property get MiddleName(): String {
    return _policyContactRole.MiddleName
  }

  override property set MiddleName(value: String) {
    _policyContactRole.MiddleName = value
  }

  // Prefix
  override property get Prefix(): typekey.NamePrefix {
    return (_policyContactRole.AccountContactRole.AccountContact.Contact as Person).Prefix
  }

  override property set Prefix(value: typekey.NamePrefix) {
    (_policyContactRole.AccountContactRole.AccountContact.Contact as Person).Prefix = value
  }

  /**
   * US891, robk
   * TDIC require Suffix synched and revisioned.
   */
  override property get Suffix(): typekey.NameSuffix {
    return _policyContactRole.Suffix
  }

  override property set Suffix(value : typekey.NameSuffix) {
    _policyContactRole.Suffix = value
  }

}