package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses typekey.Job

class CPForm_BOP2510WA_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    var bopLocations = context.Period.BOPLine?.BOPLocations
    var locations : ArrayList<BOPLocation> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and bopLocations.HasElements) {
      var bopLocationsOnPolicyChange = bopLocations.where(\loc -> loc.BasedOn == null)
      if ((context.Period.Job typeis Submission || context.Period.Job.Subtype== Job.TC_RENEWAL)
          and bopLocations*.Buildings.HasElements ) {
        locations.add(bopLocations.first())
      } else if ((context.Period.Job typeis PolicyChange) and bopLocationsOnPolicyChange.HasElements) {
        if (bopLocationsOnPolicyChange.hasMatch(\loc -> loc.BasedOn == null)) {
          locations.add(bopLocationsOnPolicyChange.first())
        }
      }
    }
    return locations.HasElements ? locations.toList() : {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("OriginalEffectiveDate", _entity.Location.OriginalEffDate_TDIC.toString()))
    contentNode.addChild(createTextNode("AddressLine1", _entity.Location.AddressLine1.toString()))
    contentNode.addChild(createTextNode("City", _entity.Location.City.toString()))
    contentNode.addChild(createTextNode("PostalCode", _entity.Location.PostalCode.toString()))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}