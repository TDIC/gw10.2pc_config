package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */

class CPForm_NW0809LRP_TDIC extends AbstractSimpleAvailabilityForm{

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var bopLocations = context.Period.BOPLine?.BOPLocations
    if (context.Period.Job typeis Renewal and context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC" and bopLocations.HasElements and context.Period.isDIMSPolicy_TDIC) {
      return true
    }
    return false
  }
}