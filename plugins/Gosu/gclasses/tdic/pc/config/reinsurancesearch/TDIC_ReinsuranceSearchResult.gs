package tdic.pc.config.reinsurancesearch

uses java.util.ArrayList
/**
 * US1377
 * ShaneS 04/21/2015
 *
 * This class is used to group PolicyLocations together based on related (i.e. common) address information
 * or an associated control address.
 */
class TDIC_ReinsuranceSearchResult {

  /* Fields used in UI display. */
  var _relatedAddressLine1 : String as RelatedAddressLine1
  var _relatedCity : String as RelatedCity
  var _relatedState : typekey.State as RelatedState
  var _relatedPostalCode : String as RelatedPostalCode
  var _controlAddress : String as ControlAddress

  /* Fields used in Reinsurance Search. */
  var _reinsuranceSearchTag : String as ReinsuranceSearchTag
  var _reinsuranceGroup : ArrayList<PolicyLocation> as ReinsuranceGroup

  construct(){
    _reinsuranceGroup = new ArrayList<PolicyLocation>()
  }
}