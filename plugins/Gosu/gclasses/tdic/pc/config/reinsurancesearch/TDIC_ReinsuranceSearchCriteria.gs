package tdic.pc.config.reinsurancesearch

uses java.io.Serializable
uses gw.api.database.IQueryBeanResult
uses gw.api.util.DisplayableException
uses java.util.ArrayList
uses java.util.List
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

/**
 * US1377
 * ShaneS 04/20/2015
 *
 * A search criteria used for Reinsurance Search.
 */
class TDIC_ReinsuranceSearchCriteria implements Serializable{

  var _logger = LoggerFactory.getLogger("TDIC_REINSURANCE_SEARCH")

  // Currently only AddressLine1 and Product are used in search query.
  var _product : gw.api.productmodel.Product as Product
  var _addressLine1 : String as AddressLine1
  var _city : String as City
  var _state : typekey.State as State
  var _postalCode : String as PostalCode

  /**
   * US1377
   * ShaneS 04/20/2015
   */
  function overridePerformReinsuranceSearch() : TDIC_ReinsuranceSearchResult[] {
    _logger.debug("TDIC_ReinsuranceSearchCriteria#overridePerformReinsuranceSearch() - entered.")
    var policyLocations = doPolicyLocationSearch()
    var results = createReinsuranceSearchResults(policyLocations)
    _logger.debug("TDIC_ReinsuranceSearchCriteria#overridePerformReinsuranceSearch() - returning results.")
    return results
  }

  /**
   * US1377
   * ShaneS 04/20/2015
   */
  private function doPolicyLocationSearch() : PolicyLocation[] {
    validateSearchData()
    _logger.debug("TDIC_ReinsuranceSearchCriteria#doPolicyLocationSearch() - Starting PolicyLocation query.")
    var policyLocationQueryBuilder = new TDIC_PolicyLocationQueryBuilder()
    var convertedAddressLine1 = TDIC_ReinsuranceSearchHelper.convertAddress(AddressLine1, false)

    policyLocationQueryBuilder.withConvertedAddressLine1(convertedAddressLine1)
    policyLocationQueryBuilder.withProductCode(Product.CodeIdentifier)
    var result = policyLocationQueryBuilder.build().select() as IQueryBeanResult<PolicyLocation>

    if(hasTooManyResults(result)){
      throw new DisplayableException(DisplayKey.get("TDIC.Web.ReinsuranceSearch.TooManySearchResults"))
    }

    return result.toTypedArray()
  }

  /**
   * US1377
   * ShaneS 04/21/2015
   */
  private function hasTooManyResults(result : IQueryBeanResult<PolicyLocation>) : boolean {
    var upperBound = ScriptParameters.TDIC_ReinsuranceAdminSearchResultLimit
    upperBound++
    if(result.getCountLimitedBy(upperBound) < upperBound){
      return false
    }
    return true
  }

  /**
   * US1377
   * ShaneS 04/21/2015
   */
  private function validateSearchData(){
    if(AddressLine1 == null){
      throw new DisplayableException(DisplayKey.get("TDIC.Web.ReinsuranceSearch.AddressLine1Required"))
    }
  }

  /**
   * US1377
   * ShaneS 04/20/2015
   *
   * This groups PolicyLocations together based on their reinsurance search tags and stores each group within a
   * TDIC_ReinsuranceSearchResult object.
   * Also, if a group of PolicyLocations has an associated Control Address, then this Control Address is added to the group.
   */
  private function createReinsuranceSearchResults(policyLocations : PolicyLocation[]) : TDIC_ReinsuranceSearchResult[]{
    _logger.debug("TDIC_ReinsuranceSearchCriteria#createReinsuranceSearchResults() - Starting location matching logic.")
    var controlLocations = new ArrayList<PolicyLocation>()
    var results = processLocations(policyLocations, controlLocations)
    processControlLocations(controlLocations, results)

    return results.toTypedArray()
  }

  /**
   * US1378
   * ShaneS 05/11/2015
   */
  private function processLocations(policyLocations : PolicyLocation[], controlLocations : List<PolicyLocation>) : ArrayList<TDIC_ReinsuranceSearchResult>{
    var results = new ArrayList<TDIC_ReinsuranceSearchResult>()
    var aResult : TDIC_ReinsuranceSearchResult

    for(aPolicyLocation in policyLocations){
      aResult = getSearchResult(aPolicyLocation.ReinsuranceSearchTag_TDIC ,results)
      if(aResult == null){
        aResult = createNewResult(aPolicyLocation.ReinsuranceSearchTag_TDIC, results)
      }
      aResult.ReinsuranceGroup.add(aPolicyLocation)

      if(not aResult.HasRelatedAddress){
        // Set related address
        var convertedAddress = TDIC_ReinsuranceSearchHelper.convertAddress(aPolicyLocation.AddressLine1, true)
        aResult.RelatedAddressLine1 = convertedAddress
        aResult.RelatedCity = aPolicyLocation.City
        aResult.RelatedState = aPolicyLocation.State
        aResult.RelatedPostalCode = aPolicyLocation.PostalCode

        //GW-158: Reinsurance Search results doesn't have the ControlAddress data
        if(aPolicyLocation.HasControlAddress_TDIC && aResult.ControlAddress == null)   {
          var convertedControlAddress = TDIC_ReinsuranceSearchHelper.convertAddress(aPolicyLocation.ControlAddressLine1_TDIC, true)
          aResult.ControlAddress = convertedControlAddress
        }
      }

     if(aPolicyLocation.ControlAddressRISearchTag_TDIC != null){
        controlLocations.add(aPolicyLocation)
      }
    }

    return results
  }

  /**
   * US1378
   * ShaneS 05/11/2015
   */
  private function processControlLocations(controlLocations : List<PolicyLocation>, results : List<TDIC_ReinsuranceSearchResult>){
    var aResult : TDIC_ReinsuranceSearchResult

    for(aControlLocation in controlLocations){
      aResult = getSearchResult(aControlLocation.ControlAddressRISearchTag_TDIC ,results)
      if(aResult != null){
        aResult.ReinsuranceGroup.add(aControlLocation)

        if(aResult.ControlAddress == null){
          var convertedAddress = TDIC_ReinsuranceSearchHelper.convertAddress(aControlLocation.AddressLine1, true)
          aResult.ControlAddress = convertedAddress
        }
      }
    }
  }

  /**
   * US1378
   * ShaneS 05/11/2015
   */
  private function getSearchResult(searchTag : String, results : List<TDIC_ReinsuranceSearchResult>) : TDIC_ReinsuranceSearchResult{
    for(aResult in results){
      if(aResult.ReinsuranceSearchTag == searchTag){
        return aResult
      }
    }
    return null
  }

  /**
   * US1378
   * ShaneS 05/11/2015
   */
  private function createNewResult(searchTag : String, results : List<TDIC_ReinsuranceSearchResult>) : TDIC_ReinsuranceSearchResult{
    var newResult = new TDIC_ReinsuranceSearchResult()
    newResult.ReinsuranceSearchTag = searchTag
    results.add(newResult)

    return newResult
  }
}