package tdic.pc.config.reinsurancesearch

uses gw.search.EntityQueryBuilder
uses gw.api.database.ISelectQueryBuilder
uses gw.api.util.DateUtil
uses gw.api.database.DBFunction
uses gw.pl.logging.LoggerCategory
uses org.slf4j.LoggerFactory

/**
 * US1377
 * ShaneS 04/20/2015
 *
 * A query builder used in Reinsurance Search.
 */
class TDIC_PolicyLocationQueryBuilder extends EntityQueryBuilder<PolicyLocation> {
  var _logger = LoggerFactory.getLogger("TDIC_REINSURANCE_SEARCH")

  var _convertedAddressLine1 : String
  var _productCode: String

  /**
   * US1377
   * ShaneS 04/21/2015
   */
  function withConvertedAddressLine1(value : String) {
    _convertedAddressLine1 = value
  }

  /**
   * US1378
   * ShaneS 05/12/2015
   */
  function withProductCode(value : String) {
    _productCode = value
  }

  /**
   * US1377
   * ShaneS 04/21/2015
   *
   * Restrict the results returned from the query.
  */
  override function doRestrictQuery(selectQueryBuilder: ISelectQueryBuilder<gw.pl.persistence.core.Bean>) {
    _logger.debug("TDIC_PolicyLocationQueryBuilder.doRestrictQuery() - entered.")

    // also consider control address in query
    selectQueryBuilder.or(\ orCriteria -> {
      orCriteria.startsWith(PolicyLocation#ReinsuranceSearchTag_TDIC, _convertedAddressLine1, true)
      orCriteria.startsWith(PolicyLocation#ControlAddressRISearchTag_TDIC, _convertedAddressLine1, true)
    })

    var currentDate = DateUtil.currentDate()
    restrictToMostRecentBoundInForcePolicyPeriods(selectQueryBuilder, currentDate)
    restrictToInForcePolicyLocation(selectQueryBuilder, currentDate)

    if(_productCode != null){
      restrictToSelectedProduct(selectQueryBuilder)
    }

    _logger.debug("TDIC_PolicyLocationQueryBuilder.doRestrictQuery() - exiting.")
  }

  /**
   * US1377
   * ShaneS 04/21/2015
   */
  private function restrictToMostRecentBoundInForcePolicyPeriods(selectQueryBuilder:ISelectQueryBuilder<gw.pl.persistence.core.Bean>,
                                                                 currentDate:java.util.Date){

    // BranchValue represents the BranchID database column, which is a foreign key to PolicyPeriod.
    var periodTable = selectQueryBuilder.join(PolicyLocation#BranchValue)
    periodTable.and(\ andCriteria ->{
      andCriteria.compare(PolicyPeriod#Status, Equals, typekey.PolicyPeriodStatus.TC_BOUND)
      andCriteria.compare(PolicyPeriod#MostRecentModel, Equals, true)
      andCriteria.compare(DBFunction.DateFromTimestamp(andCriteria.getColumnRef("PeriodStart")), LessThanOrEquals, currentDate)
      andCriteria.compare(DBFunction.DateFromTimestamp(andCriteria.getColumnRef("PeriodEnd")), GreaterThanOrEquals, currentDate)
      andCriteria.or(\ orCriteria ->{
        orCriteria.compare(PolicyPeriod#CancellationDate, Equals, null)
        orCriteria.compare(PolicyPeriod#CancellationDate, GreaterThan, currentDate)
      })
    })

    var policyTermTable = selectQueryBuilder.join(PolicyLocation#BranchValue).join(PolicyPeriod#PolicyTerm)
    policyTermTable.compare(PolicyTerm#MostRecentTerm, Equals, true)
  }

  /**
   * US1377
   * ShaneS 04/23/2015
  */
  private function restrictToInForcePolicyLocation(selectQueryBuilder: ISelectQueryBuilder<gw.pl.persistence.core.Bean>,
                                                                 currentDate:java.util.Date){

    // If the period is a PolicyChange that was scheduled and modified a PolicyLocation, then both in-force and
    // scheduled/expired versions of the PolicyLocation are maintained in the system. This query ensures that only the
    // in-force version is retrieved.
    // Otherwise, if the period is not a PolicyChange, then both Eff and Exp dates will be null and this query will retrieve them.
    selectQueryBuilder.and(\ andCriteria ->{
      andCriteria.or(\ orCriteria -> {
        orCriteria.compare(DBFunction.DateFromTimestamp(orCriteria.getColumnRef("ExpirationDate")), GreaterThan, currentDate )
        orCriteria.compare(DBFunction.DateFromTimestamp(orCriteria.getColumnRef("ExpirationDate")), Equals, null )
      })
      andCriteria.or(\ orCriteria -> {
        orCriteria.compare(DBFunction.DateFromTimestamp(orCriteria.getColumnRef("EffectiveDate")), LessThanOrEquals, currentDate )
        orCriteria.compare(DBFunction.DateFromTimestamp(orCriteria.getColumnRef("EffectiveDate")), Equals, null )
      })
    })
  }

  /**
   * US1378
   * ShaneS 05/12/2015
   */
  private function restrictToSelectedProduct(selectQueryBuilder:ISelectQueryBuilder<gw.pl.persistence.core.Bean>){
    var policyTable = selectQueryBuilder.join(PolicyLocation#BranchValue).join(PolicyPeriod#Policy)
    policyTable.compare(Policy#ProductCode, Equals, _productCode)
  }
}