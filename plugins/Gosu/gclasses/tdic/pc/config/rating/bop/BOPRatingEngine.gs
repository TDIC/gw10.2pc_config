package tdic.pc.config.rating.bop

uses entity.BOPBuildingCov
uses entity.BOPBuildingModifier_TDIC
uses gw.api.domain.financials.PCFinancialsLogger
uses gw.api.util.JurisdictionMappingUtil
uses gw.financials.Prorater
uses gw.job.RenewalProcess
uses gw.lob.bop.rating.BOPBuildingCostData_TDIC
uses gw.lob.bop.rating.BOPBuildingCovCostData
uses gw.lob.bop.rating.BOPBuildingModifierCostData_TDIC
uses gw.lob.bop.rating.BOPCostData
uses gw.lob.bop.rating.BOPCovBuildingCostData
uses gw.lob.bop.rating.BOPCovCostData
uses gw.lob.bop.rating.BOPMinPremiumCostData
uses gw.lob.bop.rating.BOPMoneySecCovCostData
uses gw.rating.AbstractRatingEngine
uses gw.rating.CostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.bop.params.BOPBuildingCovRatingParams
uses tdic.pc.config.rating.bop.params.BOPBuildingModifierRatingParams
uses tdic.pc.config.rating.bop.params.BOPBuildingRatingParams

uses java.math.BigDecimal
uses java.math.RoundingMode

/**
 * Rating engine, this is the starting point for BOP rating.
 * package (tdic.pc.config.rating.bop)
 *
 * @author- Ankita Gupta
 * 06/06/2019
 * .
 */
class BOPRatingEngine extends AbstractRatingEngine<BOPLine> {
  var _baseRatingDate : Date
  var _rateBook : RateBook as RateBook
  var _ratingLevel : RateBookStatus as RatingLevel
  var _costCache : Map<BOPPremiumKey, BOPCostData>as readonly CostCache = {}
  var isOfferingBOP : boolean
  var bopRoutineMapper = {"BOPBuildingCov" -> "BOPBuildingCov_TDIC",
      "BOPPersonalPropCov" -> "BOPPersonalPropCov_TDIC",
      "BOPMoneySecCov_TDIC" -> "BOPMoneyAndSecurities_TDIC",
      "BOPLossofIncomeTDIC" -> "BOPLossOfIncome_TDIC",
      "BOPEmpDisCov_TDIC" -> "BOPEmployeeDishonesty_TDIC",
      "BOPGoldPreMetals_TDIC" -> "BOPGoldPrecMetal_TDIC",
      "BOPValuablePapersCov_TDIC" -> "BOPValuablePapers_TDIC",
      "BOPExtraExpenseCov_TDIC" -> "BOPExtraExpense_TDIC",
      "BOPSigns_TDIC" -> "BOPSigns_TDIC",
      "BOPAccReceivablesCov_TDIC" -> "BOPAccountsReceivable_TDIC",
      "BOPFineArtsCov_TDIC" -> "BOPFineArts_TDIC",
      "BOPEqBldgCov" -> "BOPEarthquake_TDIC",
      "BOPEqSpBldgCov" -> "BOPEQSL_TDIC",
      "BOPFungiCov_TDIC" -> "BOPFungi_TDIC",
      "BOPILMineSubCov_TDIC" -> "BOPILMineSubsidence_TDIC",
      "BOPDentalGenLiabilityCov_TDIC" -> "BOPDentalGeneralLiability_TDIC",
      "BOPEncCovEndtCond_TDIC" -> "BOPEnhancedCoverage_TDIC",
      "BOPWASTOPGAP_TDIC" -> "BOPWAStopGapEmpsLiabCov_TDIC"
  }
  var lrpRoutineMapper = {"BOPBuildingCov" -> "LRPBuildingCov_TDIC",
      "BOPEqBldgCov" -> "LRPEarthquake_TDIC",

      "BOPEqSpBldgCov" -> "LRPEQSL_TDIC",
      "BOPFungiCov_TDIC" -> "LRPFungi_TDIC",
      "BOPILMineSubCov_TDIC" -> "LRPILMineSubsidence_TDIC",
      "BOPBuildingOwnersLiabCov_TDIC" -> "LRPBuildingOwnersLiab_TDIC"
  }
  var bopModifierRoutineMapper = {
      RatingConstants.bopClosedEndCredit -> RatingConstants.bopClosedEndWaterCreditRoutine,
      RatingConstants.bopIRPMLiability -> RatingConstants.bopIRPMLiabilityRoutine,
      RatingConstants.bopIRPMProperty -> RatingConstants.bopIRPMPropertyRoutine
  }
  var lrpModifierRoutineMapper = {
      RatingConstants.bopIRPMLiability -> RatingConstants.lrpIRPMLiabilityRoutine,
      RatingConstants.bopIRPMProperty -> RatingConstants.lrpIRPMPropertyRoutine
  }
  var covListBOPMultiLineProp =
      {RatingConstants.bopMoneyAndSecurityCoverage,
          RatingConstants.bopLossOfIncomeCoverage,
          RatingConstants.bopGoldAndPreciousMetalsCoverage,
          RatingConstants.bopValuablePapersCoverage,
          RatingConstants.bopAccRecievablesCoverage,
          RatingConstants.bopFineArtsCoverage,
          RatingConstants.bopBuildingCoverage,
          RatingConstants.bopPersonalPropertyCoverage,
          RatingConstants.bopExtraExpensecoverage,
          RatingConstants.bopFungiCoverage,
          RatingConstants.bopEmpDishonestyCoverage,
          RatingConstants.bopSignsCoverage,
          RatingConstants.bopEnhancedCoverage,
          RatingConstants.bcpWAStopGapEmployersLiabilityCoverage
      }
  var covListBOPMultiLineLiab =
      {
          RatingConstants.bopDentalGeneralLiability,
          RatingConstants.bopBuildingOwnersLiability
      }

  construct(bopLineArg : BOPLine) {
    super(bopLineArg)
    // set the base Rating using the first policyperiod in the term.
    // this will be used for U/W lookup and other basic items
    // rating date by object will be set separately
    _baseRatingDate = bopLineArg.Branch.FirstPeriodInTerm.getReferenceDateForCurrentJob(bopLineArg.BaseState)
  }

  construct(line : BOPLine, minimumRatingLevel : RateBookStatus) {
    super(line)
    _ratingLevel = minimumRatingLevel
    _rateBook = getRateBook(line.Branch.PeriodStart)
  }

  // Business Owner's Property is rated on a 365-day term
  override protected property get NumDaysInCoverageRatedTerm() : int {
    return Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(PolicyLine.EffectiveDate, PolicyLine.EffectiveDate.addYears(1))
  }

  override protected function existingSliceModeCosts() : Iterable<Cost> {
    return PolicyLine.Costs.where(\c -> c typeis BOPBuildingCovCost or
        c typeis BOPCovBuildingCost or
        c typeis BOPCovCost or
        c typeis BOPMoneySecCovCost or
        c typeis BOPMinPremiumCost or
        c typeis BOPModifierCost_TDIC or
        c typeis BOPBuildingCost_TDIC)
  }

  // Used by the extractCostDatasFromExistingCosts method.  Must be implemented if that method is going to be called
  override protected function createCostDataForCost(c : Cost) : CostData {
    switch (typeof c) {
      case BOPBuildingCovCost:
        return new BOPBuildingCovCostData(c, RateCache)
      case BOPCovBuildingCost:
        return new BOPCovBuildingCostData(c, RateCache)
      case BOPCovCost:
        return new BOPCovCostData(c, RateCache)
      case BOPMoneySecCovCost:
        return new BOPMoneySecCovCostData(c, RateCache)
      case BOPMinPremiumCost:
        return new BOPMinPremiumCostData(c, RateCache)
      case BOPModifierCost_TDIC:
        return new BOPBuildingModifierCostData_TDIC(c, RateCache)
      case BOPBuildingCost_TDIC:
        return new BOPBuildingCostData_TDIC(c, RateCache)
      default:
        throw "Unepxected cost type ${c.DisplayName}"
    }
  }


  protected override function rateSlice(lineVersion : BOPLine) {
    if (lineVersion.Branch.isCanceledSlice()) {
      return
    }
    assertSliceMode(lineVersion)
    isOfferingBOP = (lineVersion.Branch.Offering.CodeIdentifier == RatingConstants.businessOwnersPolicy) ? true : false
    var logMsg = "Rating ${lineVersion} ${lineVersion.SliceDate} version..."
    var totalLocPrem : BigDecimal = 0
    PCFinancialsLogger.logInfo(logMsg)
    _costCache.clear()
    var buildingParam : BOPBuildingRatingParams
    for (location in lineVersion.BOPLocations) {
      for (building in location.Buildings) {
        _costCache.clear()
        buildingParam = new BOPBuildingRatingParams(building)
        _rateBook = RateBook.selectRateBook(building.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode, JurisdictionMappingUtil.getJurisdiction(building.BOPLocation.Location), _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess, PolicyLine.Branch.Offering.PublicID)
        for (cov in building.Coverages) {
           if (isCoverageEligibleForRating(cov)) {
            rateBuildingCoverage(cov, building, buildingParam)
          }
        }
        rateModifiers(building)
        rateMultiLineDiscount(building)
        totalLocPrem = _costCache.Values*.ActualTermAmount.sum()
        rateSurcharges(building, location, totalLocPrem)
      }
    }
    PCFinancialsLogger.logInfo(logMsg + "done")
  }

  protected override function rateWindow(lineVersion : BOPLine) {
  }

  /**
   * Method to execute rate routine to get premium for a given coverage
   *
   *
   * @param routineName             : String - param to hold routine name
   * @param rateRoutineParameterMap : Map - map to hold params to be used routine
   * @param cost                    - CostData for holding premiums
   */
  public function execute(routineName : String, rateRoutineParameterMap : Map<CalcRoutineParamName, Object>, cost : BOPCostData) {
    cost.NumDaysInRatedTerm = this.NumDaysInCoverageRatedTerm
    _rateBook.executeCalcRoutine(routineName, :costData = cost,
        :worksheetContainer = cost, :paramSet = rateRoutineParameterMap)
    cost.copyStandardColumnsToActualColumns()
    addCost(cost)
  }

  /**
   * Method to get apt RateBoks
   *
   * @param refDate
   * @return RateBook
   */

  private function getRateBook(refDate : Date) : RateBook {
    return RateBook.selectRateBook(refDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
        PolicyLine.BaseState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
        PolicyLine.Branch.Offering.PublicID)
  }

  /**
   * Method to rate Building level coverages
   *
   * @param cov
   * @param builingCovParam
   */
  private function rateBuildingCoverage(cov : BOPBuildingCov, building : BOPBuilding, buildingParam : BOPBuildingRatingParams) {
    var builingCoverageParam = new BOPBuildingCovRatingParams(cov)
    var map : Map<CalcRoutineParamName, Object> = {
        TC_POLICYLINE -> PolicyLine,
        TC_BOPBUILDINGPARAM_TDIC -> buildingParam,
        TC_BOPBUILDINGCOVPARAM_TDIC -> builingCoverageParam
    }
    var routine : String
    if (cov.Pattern.CodeIdentifier == RatingConstants.bopEquipBreakDownCoverage) {
      rateEquipmentBreakdownCov(cov, building, builingCoverageParam, map)
      return
    }

    if (isOfferingBOP) {

       routine = bopRoutineMapper.get(cov.Pattern.CodeIdentifier)

         } else {

      routine = lrpRoutineMapper.get(cov.Pattern.CodeIdentifier)
    }
    var cost = new BOPBuildingCovCostData(cov.SliceDate, getNextSliceDateAfter(cov.SliceDate),
        cov.Currency, RateCache, JurisdictionMappingUtil.getJurisdiction(cov.BOPBuilding.BOPLocation.Location), cov.FixedId, null)
    if (routine != null and routine != "") {
      _rateBook = RateBook.selectRateBook(cov.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          building.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)

      execute(routine, map, cost)
    }
    _costCache.put((new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier)), cost)

  }

  private property get RoundingLevel() : int {
    return Branch.Policy.Product.QuoteRoundingLevel
  }

  private property get RoundingMode() : RoundingMode {
    return Branch.Policy.Product.QuoteRoundingMode
  }

  private function getRepresentativeAdditionalInsureds(lineVersion : BOPLine) : Set<PolicyAddlInsured> {
    return lineVersion.Branch.VersionList.PolicyContactRoles.map(\contactVL -> contactVL.AllVersions.first())
        .whereTypeIs(PolicyAddlInsured).where(\addIns -> addIns.PolicyLine == lineVersion).toSet()
  }

  /**
   * Function to check eligibility to run a routine
   *
   * @param cov
   * @return boolean
   */
  private function isCoverageEligibleForRating(cov : Coverage) : boolean {
    var eligible : boolean = false
    eligible = allMandatoryCoverageEligibility(cov)
    if (!eligible) {
      eligible = isCoverageEligibleForRatingPassOne(cov)
    }
    if (!eligible) {
      eligible = isCoverageEligibleForRatingPassTwo(cov)
    }
    if (!eligible) {
      eligible = isCoverageEligibleForRatingPassThree(cov)
    }
    if (!eligible) {
      eligible = isCoverageEligibleForRatingPassFour(cov)
    }
    return eligible
  }


  private function allMandatoryCoverageEligibility(cov : Coverage) : boolean {
    var eligible = false
    switch (typeof cov) {
      case productmodel.BOPBuildingCov:
      case productmodel.BOPPersonalPropCov:
      case BOPDentalGenLiabilityCov_TDIC:
      case BOPEquipBreakCov_TDIC:
      case BOPBuildingOwnersLiabCov_TDIC:
      case BOPLossofIncomeTDIC:
      case BOPEncCovEndtCond_TDIC:
      case BOPWASTOPGAP_TDIC:
        eligible = true
        break
    }
    return eligible
  }

  private function isCoverageEligibleForRatingPassOne(cov : Coverage) : boolean {
    var eligible = false
    switch (typeof cov) {
      case BOPMoneySecCov_TDIC:
        eligible = checkCovConditions(cov.BOPMoneySecTotIncLimit_TDICTerm.Value, 10000)
        break
      case BOPEmpDisCov_TDIC:
        eligible = checkCovConditions(cov.BOPEDTotInclLimit_TDICTerm.Value, 25)
        break
      case BOPGoldPreMetals_TDIC:
        eligible = checkCovConditions(cov.BOPGoldTotalIncLimit_TDICTerm.Value, 5000)
        break
    }
    return eligible
  }

  public function isCoverageEligibleForRatingPassTwo(cov : Coverage) : boolean {
    var eligible : boolean
    switch (typeof cov) {
      case BOPValuablePapersCov_TDIC:
        eligible = checkCovConditions(cov.BOPValTotIncLimit_TDICTerm.Value, 50000)
        break
      case BOPExtraExpenseCov_TDIC:
        eligible = checkCovConditions(cov.BOPExtraExpTotalIncLimit_TDICTerm.Value, 100000)
        break
      case BOPSigns_TDIC:
        eligible = checkCovConditions(cov.BOPTotalInLimitSigns_TDICTerm.Value, 10000)
        break
      case BOPAccReceivablesCov_TDIC:
        eligible = checkCovConditions(cov.BOPACcTotalIncLimit_TDICTerm.Value, 100000)
        break
    }
    return eligible
  }

  private function isCoverageEligibleForRatingPassThree(cov : Coverage) : boolean {
    var eligible = false
    switch (typeof cov) {
      case BOPFungiCov_TDIC:
        if (cov.BOPBuilding.BOPLocation.Location.State == State.TC_NJ
            and cov.BOPFungiTotIncLimit_TDICTerm.Value != null
            and (cov.BOPFungiTotIncLimit_TDICTerm.Value == 25000 or cov.BOPFungiTotIncLimit_TDICTerm.Value == 50000)) {
          eligible = true
        }
        break
      case BOPEqBldgCov:
        if (cov.BOPBuilding.BOPLocation.Location.State == State.TC_HI) {
          eligible = true
        }
        break
    }
    return eligible
  }

  private function isCoverageEligibleForRatingPassFour(cov : Coverage) : boolean {
    var eligible = false
    switch (typeof cov) {
      case BOPFineArtsCov_TDIC:
        eligible = checkCovConditions(cov.BOPFineArtsTotIncLimit_TDICTerm.Value, 5000)
        break
      case BOPILMineSubCov_TDIC:
        if (cov.BOPBuilding.BOPLocation.Location.State == State.TC_IL
            and cov.BOPBuilding.BOPBuildingCovExists
            and cov.BOPILMineSubInclLimit_TDICTerm.Value <= 750000) {
          eligible = true
        }
        break
      case BOPEqSpBldgCov:
        if (cov.BOPBuilding.BOPLocation.Location.State == State.TC_HI) {
          eligible = true
        }
        break
    }
    return eligible
  }


  function checkCovConditions(termValue : BigDecimal, eligibleVal : BigDecimal) : boolean {
    return (termValue != null and termValue > eligibleVal) ? true : false
  }

  /**
   * rates the fire fighter relief surcharge
   *
   * @param building
   */
  function rateFireFighterReliefSurcharges(building : BOPBuilding) {
    var cost : BOPCostData
    var routine : String
    var buildingId = building.FixedId
    var closedEndCreditPortion : BigDecimal = 0
    var irpmPropertyPortion : BigDecimal = 0
    var totalPortion : BigDecimal = 0
    var totalCovCost : BigDecimal = 0
    var totalDGLCost : BigDecimal = 0
    var irpmLiabModifierExists : BOPBuildingModifier_TDIC
    var irpmLiabCost : BigDecimal = 0
    var buildingOwnerLiabCost : BigDecimal = 0
    var dentalLiabCost : BigDecimal = 0
    var multilineLiabCost : BigDecimal = 0
    var multilinePropertyPortion : BigDecimal = 0
    var irpmPropertyModifier : BOPBuildingModifier_TDIC
    var irpmPropertyExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopIRPMProperty and elt1.hasRateFactors() == true)
    var closedEndCreditExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopClosedEndCredit and elt1.BooleanModifier == true)

    var policyRatingParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    for (cov in building.Coverages) {
      if (cov.Pattern.CodeIdentifier == RatingConstants.bopPersonalPropertyCoverage
          or cov.Pattern.CodeIdentifier == RatingConstants.bopBuildingCoverage) {
        cost = _costCache.get(new BOPPremiumKey(buildingId, cov.Pattern.CodeIdentifier))
        totalCovCost = totalCovCost + cost.ActualTermAmount
      }
    }
    policyRatingParam.BuildingAndBPPPremium = totalCovCost
    if(building.PolicyPeriod.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P){
      policyRatingParam.HasMultiLineDiscount = true
    }
    if (closedEndCreditExists) {
      closedEndCreditPortion = ((-0.1) * totalCovCost)
    }
    if (irpmPropertyExists) {
      irpmPropertyModifier = building.BOPBuildingModifiers_TDIC.firstWhere(\elt -> elt.Pattern.CodeIdentifier == RatingConstants.bopIRPMProperty)
      irpmPropertyPortion = ((policyRatingParam.getTotalIRPMCredit(irpmPropertyModifier)) * totalCovCost)
    }
    totalCovCost += (closedEndCreditPortion + irpmPropertyPortion)
    policyRatingParam.FireFighterReliefSurchargeBasis = totalCovCost
    if (building.PolicyPeriod.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P) {
      multilineLiabCost = _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopMultiLineLiablityCoverage)).ActualTermAmount
    }
    if (building.BOPDentalGenLiabilityCov_TDICExists) {
      dentalLiabCost = _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopDentalGeneralLiability)).ActualTermAmount
    }
    if (building.BOPBuildingOwnersLiabCov_TDICExists) {
      buildingOwnerLiabCost = _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopBuildingOwnersLiability)).ActualTermAmount
    }
    if (building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopIRPMLiability and elt1.hasRateFactors() == true)) {
      var irpmLiablityModifier = building.BOPBuildingModifiers_TDIC.firstWhere(\elt -> elt.Pattern.CodeIdentifier == RatingConstants.bopIRPMLiability)
      if (building.BOPDentalGenLiabilityCov_TDICExists) {
        irpmLiabCost = (policyRatingParam.getTotalIRPMCredit(irpmLiablityModifier)) * dentalLiabCost
      }
      if (building.BOPBuildingOwnersLiabCov_TDICExists) {
        irpmLiabCost = (policyRatingParam.getTotalIRPMCredit(irpmLiablityModifier)) * buildingOwnerLiabCost
      }
    }
    totalDGLCost += (multilineLiabCost + dentalLiabCost + buildingOwnerLiabCost + irpmLiabCost)
    policyRatingParam.DGLPremium = totalDGLCost
    var map = new HashMap<CalcRoutineParamName, Object>(){
        TC_POLICYLINE -> PolicyLine,
        TC_BOPBUILDINGMODIFIERPARAM_TDIC -> policyRatingParam
    }
    routine = (isOfferingBOP) ? RatingConstants.bopFireFighterReliefSurchargeRoutine : RatingConstants.lrpFireFighterReliefSurchargeRoutine
    cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_FIREFIGHTERRELIEFSURCHARGE)
    if (routine != null and routine != "") {
      execute(routine, map, cost)
    }
    cost.ChargePattern = ChargePattern.TC_FIREFIGHTERRELIEFSURCHARGE_TDIC
    cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
  }

  /**
   * rates the IGA surcharge
   *
   * @param line
   * @param totalLocPrem
   */
  function rateIGASurcharge(building : BOPBuilding, totalLocPrem : BigDecimal) {
    var cost : BOPCostData
    var routine : String
    var policyRatingParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    policyRatingParam.IGASurchargeBasis = totalLocPrem
    var map = new HashMap<CalcRoutineParamName, Object>(){
        TC_POLICYLINE -> PolicyLine,
        TC_BOPBUILDINGMODIFIERPARAM_TDIC -> policyRatingParam
    }
    routine = (isOfferingBOP) ? RatingConstants.bopIGASurchargeRoutine : RatingConstants.lrpIGASurchargeRoutine
    cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_IGASURCHARGE)
    cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
    if (routine != null and routine != "") {
      execute(routine, map, cost)
    }
    /*
     * 02/05/2020 Britto S
     * Per BillingCenter requirement and downstream system impacts, changing to state specific code,
     * may need to revist and aggregate to one in upcoming relases, as it gets included in other states
     */
    if (building.Branch.BaseState == Jurisdiction.TC_NJ) {
      cost.ChargePattern = ChargePattern.TC_NJIGASURCHARGE_TDIC
    } else {
      cost.ChargePattern = ChargePattern.TC_IGASURCHARGE_TDIC
    }
  }

  /**
   * rates the fire safety surcharge
   *
   * @param line
   * @param totalLocPrem
   */
  function rateFireSafetySurcharge(building : BOPBuilding, totalLocPrem : BigDecimal) {
    var cost : BOPCostData
    var routine : String
    var irpmLiabilityExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopIRPMLiability and elt1.hasRateFactors() == true)
    var liabilityCoverages = {
        RatingConstants.bopDentalGeneralLiability,
        RatingConstants.bopBuildingOwnersLiability
    }
    var policyRatingParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    policyRatingParam.FireSafetySurchargeBasis = totalLocPrem
    for (cov in building.Coverages) {
      if (liabilityCoverages.contains(cov.Pattern.CodeIdentifier)) {
        policyRatingParam.FireSafetySurchargeBasis -= _costCache.get(new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier)).ActualTermAmount
      }
    }
    if (irpmLiabilityExists) {
      policyRatingParam.FireSafetySurchargeBasis -= _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopIRPMLiability)).ActualTermAmount
    }
    if (building.PolicyPeriod.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P) {
      policyRatingParam.FireSafetySurchargeBasis -= _costCache.get(new BOPPremiumKey(building.FixedId, "MultiLineLiabilityDiscount")).ActualTermAmount
    }
    var map = new HashMap<CalcRoutineParamName, Object>(){
        TC_POLICYLINE -> PolicyLine,
        TC_BOPBUILDINGMODIFIERPARAM_TDIC -> policyRatingParam
    }
    routine = (isOfferingBOP) ? RatingConstants.bopFireSafetySurchargeRoutine : RatingConstants.lrpFireSafetySurchargeRoutine
    cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_FIRESAFETYSURCHARGE)
    cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
    if (routine != null and routine != "") {
      execute(routine, map, cost)
    }
    cost.ChargePattern = ChargePattern.TC_FIRESAFETYSURCHARGE_TDIC
  }

  /**
   * rates the modifiers
   *
   * @param modifier
   */
  private function rateModifier(modifier : BOPBuildingModifier_TDIC) {

    var buildingModifierParam = new BOPBuildingModifierRatingParams(modifier, _costCache)
    var routine : String
    routine = (isOfferingBOP) ?
        bopModifierRoutineMapper.get(modifier.Pattern.CodeIdentifier)
        : lrpModifierRoutineMapper.get(modifier.Pattern.CodeIdentifier)

    if (routine != null and routine != '') {
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> PolicyLine,
          TC_BOPBUILDINGMODIFIERPARAM_TDIC -> buildingModifierParam
      }
      _rateBook = RateBook.selectRateBook(modifier.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          modifier.BOPBuilding.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)
      var cost = new BOPBuildingModifierCostData_TDIC(modifier.SliceDate, getNextSliceDateAfter(modifier.SliceDate), modifier.BranchValue.getPreferredCoverageCurrency(), RateCache, JurisdictionMappingUtil.getJurisdiction(modifier.BOPBuilding.BOPLocation.Location), modifier.FixedId)
      execute(routine, map, cost)

      _costCache.put((new BOPPremiumKey(modifier.BOPBuilding.FixedId, modifier.Pattern.CodeIdentifier)), cost)
    }

  }

  /**
   * calls the method to rate the modifiers
   *
   * @param building
   */
  private function rateModifiers(building : BOPBuilding) {

    var modifiers = building.BOPBuildingModifiers_TDIC.
        where(\elt -> (elt.Pattern.CodeIdentifier == RatingConstants.bopClosedEndCredit and elt.BooleanModifier == true)
            or elt.Pattern.CodeIdentifier == RatingConstants.bopIRPMProperty
            or elt.Pattern.CodeIdentifier == RatingConstants.bopIRPMLiability)

    for (modifier in modifiers) {
      rateModifier(modifier)
    }
  }

  private function rateMultiLineDiscount(building : BOPBuilding) {
    if (building.Branch.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P) {
      rateMultiLinePropertyDiscount(building)
      rateMultiLineLiabilityDiscount(building)
    }
  }


  private function rateMultiLinePropertyDiscount(building : BOPBuilding) {
    var buildingModifierParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    var routine : String
    var totalCoverageCost : BigDecimal = 0
    var equipBreakCost : BigDecimal = 0
    var equipBrkBuildingCovCost : BigDecimal = 0
    var equipBrkBPPCovCost : BigDecimal = 0
    var irpmPropertyExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopIRPMProperty and elt1.hasRateFactors() == true)
    var closedEndCreditExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 ->
        elt1.Pattern.CodeIdentifier == RatingConstants.bopClosedEndCredit and elt1.BooleanModifier == true)

    routine = (isOfferingBOP) ?
        RatingConstants.bopMultiLineDiscountPropertyRoutine : RatingConstants.lrpMultiLineDiscountPropertyRoutine

    if (routine != null and routine != '') {
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> PolicyLine,
          TC_BOPBUILDINGMODIFIERPARAM_TDIC -> buildingModifierParam
      }

      for (cov in building.Coverages) {
        var cost = _costCache.get(new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier))
        if (cost != null and covListBOPMultiLineProp.contains(cov.Pattern.CodeIdentifier)) {
          totalCoverageCost += cost.ActualTermAmount
        }
      }
      if (building.BOPBuildingCovExists and building.BOPEquipBreakCov_TDICExists) {
        equipBrkBuildingCovCost = _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopEquipBreakDownCoverage + "building")).ActualTermAmount
      }
      if (building.BOPPersonalPropCovExists and building.BOPEquipBreakCov_TDICExists) {
        equipBrkBPPCovCost = _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopEquipBreakDownCoverage + "bpp")).ActualTermAmount
      }
      equipBreakCost = equipBrkBuildingCovCost + equipBrkBPPCovCost
      totalCoverageCost = totalCoverageCost + equipBreakCost
      if (irpmPropertyExists) {
        totalCoverageCost += _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopIRPMProperty)).ActualTermAmount
      }
      if (closedEndCreditExists) {
        totalCoverageCost += _costCache.get(new BOPPremiumKey(building.FixedId, RatingConstants.bopClosedEndCredit)).ActualTermAmount
      }
      buildingModifierParam.MultiLinePropertyDiscountBasis = totalCoverageCost

      _rateBook = RateBook.selectRateBook(building.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          building.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)
      var cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_MULTILINEPROPERTYDISCOUNT)
      execute(routine, map, cost)

      _costCache.put((new BOPPremiumKey(building.FixedId, "MultiLinePropertyDiscount")), cost)
    }
  }

  private function rateMultiLineLiabilityDiscount(building : BOPBuilding) {
    var buildingModifierParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    var routine : String
    var totalCoverageCost : BigDecimal = 0
    var irpmLiabilityExists = building.BOPBuildingModifiers_TDIC.hasMatch(\elt1 -> elt1.Pattern.CodeIdentifier == RatingConstants.bopIRPMLiability
        and elt1.hasRateFactors() == true)
    routine = (isOfferingBOP) ? RatingConstants.bopMultiLineDiscountLiabilityRoutine : RatingConstants.lrpMultiLineDiscountLiabilityRoutine
    if (routine != null and routine != '') {
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> PolicyLine,
          TC_BOPBUILDINGMODIFIERPARAM_TDIC -> buildingModifierParam
      }
      for (cov in building.Coverages) {
        var cost = _costCache.get(new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier))
        if (cost != null and covListBOPMultiLineLiab.contains(cov.Pattern.CodeIdentifier)) {
          totalCoverageCost += cost.ActualTermAmount
        }
      }
      buildingModifierParam.MultiLineLiabilityDiscountBasis = totalCoverageCost
      _rateBook = RateBook.selectRateBook(building.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          building.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)
      var cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_MULTILINELIABILITYDISCOUNT)
      execute(routine, map, cost)
      _costCache.put((new BOPPremiumKey(building.FixedId, "MultiLineLiabilityDiscount")), cost)
    }
  }


  /**
   * Calls all the surcharge rating methods
   *
   * @param line
   * @param totalLocPrem
   */
  private function rateSurcharges(building : BOPBuilding, location : BOPLocation, totalLocPrem : BigDecimal) {
    var baseState = building.Branch.BaseState

    if (totalLocPrem < 500) {
      rateMinPremiumAdjustment(building, totalLocPrem)
    }

    if (location.PolicyLocation.State.Code == typekey.Jurisdiction.TC_MN.Code) {
      if (RatingConstants.validFireFighterSurchargeCity.contains(building.BOPLocation.Location.City)) {
        rateFireFighterReliefSurcharges(building)
      }
    }
    //02/05/2020 Britto S, currently applicable only for NJ
    if (building.Branch.BaseState == Jurisdiction.TC_NJ) {
      rateIGASurcharge(building, totalLocPrem)
    }
    if (baseState == typekey.Jurisdiction.TC_MN) {
      rateFireSafetySurcharge(building, totalLocPrem)
    }

  }

  /**
   * rates Minimum Premium Adjustment
   *
   * @param line
   * @param totalLocPrem
   */
  function rateMinPremiumAdjustment(building : BOPBuilding, totalLocPrem : BigDecimal) {
    var cost : BOPCostData
    var routine : String
    var policyRatingParam = new BOPBuildingModifierRatingParams(building.Branch.BOPLine)
    policyRatingParam.MinimumPremiumAdjustmentBasis = totalLocPrem
    //TODO  include multiline discount
    for (cov in building.Coverages) {
      if (cov.Pattern.CodeIdentifier == RatingConstants.bopILMineSubsidenceCoverage) {
        policyRatingParam.MinimumPremiumAdjustmentBasis -= _costCache.get(new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier)).ActualTermAmount
      }
    }

    var map = new HashMap<CalcRoutineParamName, Object>(){
        TC_POLICYLINE -> PolicyLine,
        TC_BOPBUILDINGMODIFIERPARAM_TDIC -> policyRatingParam
    }
    routine = (isOfferingBOP) ? RatingConstants.bopMinimumPremiumAdjustmentRoutine : RatingConstants.lrpMinimumPremiumadjustmentRoutine
    cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate), building.PreferredCoverageCurrency, RateCache, building.Branch.BaseState, building.FixedId, BOPCostType_TDIC.TC_BOPMINPREMIUMADJ)
    if (routine != null and routine != "") {
      execute(routine, map, cost)
    }
    _costCache.put((new BOPPremiumKey(building.FixedId, "BOPMinimumPremiumAdjustment")), cost)
  }

  /**
   * rates Equoment Breakdown coverage
   *
   * @param cov
   * @param building
   * @param buildingCoverageParam
   * @param map
   */
  private function rateEquipmentBreakdownCov(cov : BOPBuildingCov, building : BOPBuilding, buildingCoverageParam : BOPBuildingCovRatingParams, map : Map<CalcRoutineParamName, Object>) {
    if (building.BOPBuildingCovExists and building.BOPEquipBreakCov_TDICExists and buildingCoverageParam.BuildingLimit != null) {
      rateEquipmentBreakdownBuildingLimit(cov, building, map)
    }
    if (building.BOPPersonalPropCovExists and building.BOPEquipBreakCov_TDICExists and buildingCoverageParam.PersonalPropLimit != null) {
      rateEquipmentBreakdownBPPLimit(cov, building, map)
    }

  }

  /**
   * rates EquipmentBreakdown coverage with building limits
   *
   * @param cov
   * @param building
   * @param map
   */
  private function rateEquipmentBreakdownBuildingLimit(cov : BOPBuildingCov, building : BOPBuilding, map : Map<CalcRoutineParamName, Object>) {
    var routine : String
    routine = isOfferingBOP ? RatingConstants.bopEquipBreakBuildingLimitRoutine : RatingConstants.lrpEquipBreakBuildingLimitRoutine
    var cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate),
        building.BranchValue.PreferredCoverageCurrency, RateCache, JurisdictionMappingUtil.getJurisdiction(building.BOPLocation.Location), building.FixedId, BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM)
    if (routine != null and routine != "") {
      _rateBook = RateBook.selectRateBook(cov.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          building.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)
      execute(routine, map, cost)
      _costCache.put((new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier + "building")), cost)
    }
  }

  /**
   * rate Equipment breakdown coverage with BPP limits
   *
   * @param cov
   * @param building
   * @param map
   */
  private function rateEquipmentBreakdownBPPLimit(cov : BOPBuildingCov, building : BOPBuilding, map : Map<CalcRoutineParamName, Object>) {
    var routine : String
    if (isOfferingBOP) {
      routine = RatingConstants.bopEquipBreakBPPLimitRoutine
    }
    var cost = new BOPBuildingCostData_TDIC(building.SliceDate, getNextSliceDateAfter(building.SliceDate),
        building.BranchValue.PreferredCoverageCurrency, RateCache, JurisdictionMappingUtil.getJurisdiction(building.BOPLocation.Location), building.FixedId, BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM)
    if (routine != null and routine != "") {
      _rateBook = RateBook.selectRateBook(cov.SliceDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PatternCode,
          building.BOPLocation.CoverableState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess,
          PolicyLine.Branch.Offering.PublicID)
      execute(routine, map, cost)
      _costCache.put((new BOPPremiumKey(building.FixedId, cov.Pattern.CodeIdentifier + "bpp")), cost)
    }
  }

}
