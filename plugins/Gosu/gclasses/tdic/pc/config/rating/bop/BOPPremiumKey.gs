package tdic.pc.config.rating.bop

uses gw.pl.persistence.core.Key

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 10/26/2019
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
class BOPPremiumKey implements Comparable<BOPPremiumKey> {

  var _coverageCode : String as readonly CoverageCode
  var _buildingID : Key as readonly BuildingID


  construct(buildingFixedID : Key, covCode : String) {
    _coverageCode = covCode
    _buildingID = buildingFixedID
    if (buildingFixedID == null) {
      throw new IllegalArgumentException("Cannot create key without a building and coverage")
    }
  }

  override function hashCode() : int {
    var hash = 7
    hash = 31 * hash + (BuildingID == null ? 0 : BuildingID.hashCode())
    hash = 31 * hash + (CoverageCode == null ? 0 : CoverageCode.hashCode())
    return hash
  }

  override function equals(obj : Object) : boolean {
    if (this === obj) {
      return true
    }
    if ((obj == null) || (typeof obj != BOPPremiumKey)) {
      return false
    }
    var typedObj = obj as BOPPremiumKey
    return typedObj.BuildingID == this.BuildingID
        and typedObj.CoverageCode == this.CoverageCode
  }

  override function compareTo(key : BOPPremiumKey) : int {
    var sort = 0

    if (sort == 0) {
      sort = this.BuildingID.compareTo(key.BuildingID)
    }
    if (sort == 0) {
      sort = this.CoverageCode.compareTo(key.CoverageCode)
    }
   /* if (sort == 0) {
      if (this.CoverageCode != null and key.CoverageCode != null) {
        sort = this.CoverageCode.compareTo(key.CoverageCode)
      } else if (this.CoverageCode == null and key.CoverageCode == null) {
        sort = 0
      } else {
        sort = this.CoverageCode == null ? 1 : -1
      }
    }*/
    return sort
  }

  override function toString() : String {
    var str = "${BuildingID}"
    var dateStr = CoverageCode == null ? "" : " " + CoverageCode
    return str + dateStr
  }
}