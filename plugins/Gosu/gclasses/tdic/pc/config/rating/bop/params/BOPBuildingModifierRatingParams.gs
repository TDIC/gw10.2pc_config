package tdic.pc.config.rating.bop.params
uses entity.BOPBuildingCov
uses gw.lob.bop.rating.BOPCostData
uses gw.pl.currency.MonetaryAmount
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.bop.BOPPremiumKey
uses typekey.BOPBuildingModifier_TDIC

uses java.math.BigDecimal

/**
 * Class to calculate the BOP Building Modifier Rating Parameters
 */
class BOPBuildingModifierRatingParams {

  var _closeEndCreditBasis : BigDecimal as CloseEndCreditBasis = 0
  var _irpmPropertyBasis : BigDecimal as IRPMPropertyBasis = 0
  var _irpmLiabilityBasis : BigDecimal as IRPMLiabilityBasis = 0
  var _irpmPropertyCreditBaseRate : BigDecimal as IRPMPropertyCreditBaseRate = 0
  var _irpmLiabilityCreditBaseRate : BigDecimal as IRPMLiabilityCreditBaseRate = 0
  var _multiLineDiscountBasis : BigDecimal as MultiLineDiscountBasis = 0
  var _closeEndWaterCredit : BigDecimal as CloseEndWaterCredit = 0
  var _irpmPropertyCredit : BigDecimal as IRPMPropertyCredit = 0
  var _irpmLiabilityCredit : BigDecimal as IRPMLiabilityCredit = 0
  var _multiLineDiscount : BigDecimal as MultiLineDiscount = 0
  var _fireFighterReliefSurchargeBasis : BigDecimal as FireFighterReliefSurchargeBasis = 0
  var _fireFighterReliefSurcharge : BigDecimal as FireFighterReliefSurcharge = 0
  var _IGASurchargeBasis : BigDecimal as IGASurchargeBasis = 0
  var _igaSurcharge : BigDecimal as IGASurcharge = 0
  var _fireSafetySurchargeBasis : BigDecimal as FireSafetySurchargeBasis = 0
  var _fireSafetySurcharge : BigDecimal as FireSafetySurcharge = 0
  var _minPremAdjustmentBasis : BigDecimal as MinimumPremiumAdjustmentBasis = 0
  var _minimumPremiumAdjustment : BigDecimal as MinimumPremiumAdjustment = 0
  var _multilinePropertyDiscountbasis : BigDecimal as MultiLinePropertyDiscountBasis = 0
  var _multilineLiabilityDiscountbasis : BigDecimal as MultiLineLiabilityDiscountBasis = 0
  var _dglPremium : BigDecimal as DGLPremium = 0
  var _buildingAndBPPPremium : BigDecimal as BuildingAndBPPPremium = 0
  var _hasMultileneDiscount : Boolean as HasMultiLineDiscount

  var covListClosedEndCredit =
      {   RatingConstants.bopMoneyAndSecurityCoverage,
          RatingConstants.bopLossOfIncomeCoverage,
          RatingConstants.bopGoldAndPreciousMetalsCoverage,
          RatingConstants.bopValuablePapersCoverage,
          RatingConstants.bopAccRecievablesCoverage,
          RatingConstants.bopFineArtsCoverage,
          RatingConstants.bopBuildingCoverage,
          RatingConstants.bopPersonalPropertyCoverage,
          RatingConstants.bopExtraExpensecoverage,
          RatingConstants.bopFungiCoverage
      }

  var covListBOPIRPMProp =
      {   RatingConstants.bopMoneyAndSecurityCoverage,
          RatingConstants.bopLossOfIncomeCoverage,
          RatingConstants.bopGoldAndPreciousMetalsCoverage,
          RatingConstants.bopValuablePapersCoverage,
          RatingConstants.bopAccRecievablesCoverage,
          RatingConstants.bopFineArtsCoverage,
          RatingConstants.bopBuildingCoverage,
          RatingConstants.bopPersonalPropertyCoverage,
          RatingConstants.bopExtraExpensecoverage,
          RatingConstants.bopFungiCoverage

      }

  var covListBOPIRPMLiab=
      {
          RatingConstants.bopDentalGeneralLiability,
          RatingConstants.bopBuildingOwnersLiability
      }

  /**
   * calculates the basis and base rate for respective modifier
   * @param modifier
   * @param costCache
   */
  construct(modifier : entity.BOPBuildingModifier_TDIC, costCache : Map<BOPPremiumKey, BOPCostData>) {
// Add all applicable coverages
    switch(modifier.Pattern.CodeIdentifier){
      case RatingConstants.bopClosedEndCredit:
        _closeEndCreditBasis = getTotalCoveragePrem(costCache,modifier,covListClosedEndCredit)
        break
      case RatingConstants.bopIRPMProperty:
        _irpmPropertyBasis = getTotalCoveragePrem(costCache,modifier,covListBOPIRPMProp)
        _irpmPropertyCreditBaseRate = getTotalIRPMCredit(modifier)///100
        break
      case RatingConstants.bopIRPMLiability:
        _irpmLiabilityBasis = getTotalCoveragePrem(costCache,modifier,covListBOPIRPMLiab)
        _irpmLiabilityCreditBaseRate = getTotalIRPMCredit(modifier)///100
        break

    }
  }


  /**
   * Calculates the total premium of all coverages in a building
   * @param mod
   * @return
   */
  private function getTotalCoveragePrem(costCache: Map<BOPPremiumKey, BOPCostData>,modifier : entity.BOPBuildingModifier_TDIC,irpmIncludedCovList : ArrayList<String>) : BigDecimal {
    var totalCoverageCost : BigDecimal = 0
    var cost : BOPCostData
    var buildingId = modifier.BOPBuilding.FixedId
    var BOPCoverages = modifier.BOPBuilding.Coverages
    for(cov in BOPCoverages) {
      cost = costCache.get(new BOPPremiumKey(buildingId,cov.Pattern.CodeIdentifier))
      if(cost != null and irpmIncludedCovList.contains(cov.Pattern.CodeIdentifier)){
        totalCoverageCost += cost.ActualTermAmount
      }
    }
    return totalCoverageCost
  }

  /**
   * Calculates the total credit of all Ratefactors in a Modifier
   * @param mod
   * @return
   */
  public function getTotalIRPMCredit(mod : entity.BOPBuildingModifier_TDIC) : BigDecimal {
    var totalCredit : BigDecimal = 0
    for (credit in mod.RateFactors){
     totalCredit += credit.AssessmentWithinLimits
    }
    return totalCredit
  }

  construct(line : BOPLine){

  }
}