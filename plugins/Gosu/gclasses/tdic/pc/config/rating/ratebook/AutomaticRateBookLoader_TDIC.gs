package tdic.pc.config.rating.ratebook


uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.pc.config.rating.ratebook.RateBookLoader

uses java.lang.invoke.MethodHandles

class AutomaticRateBookLoader_TDIC extends BatchProcessBase {

  static final var LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

  public construct() {
    super(BatchProcessType.TC_AUTOMATICRATEBOOKLOADER_TDIC)
  }

  protected override function doWork() {
    try {
      RateBookLoader.Start()
    } catch (ex : Exception) {
      LOGGER.error("Caught Exception while reloading ratebooks = {}", ex.StackTraceAsString)
    }
  }
}