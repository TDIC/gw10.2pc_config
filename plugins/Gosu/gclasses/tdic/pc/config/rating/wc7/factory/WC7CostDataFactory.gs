package tdic.pc.config.rating.wc7.factory

uses gw.api.util.JurisdictionMappingUtil
uses gw.financials.PolicyPeriodFXRateCache
uses gw.lob.wc7.rating.WC7JurisdictionCostData
uses gw.lob.wc7.rating.WC7JurisdictionCostData_TDIC
uses gw.lob.wc7.rating.WC7JurisdictionCovCostData
uses gw.lob.wc7.rating.WC7CostData
uses gw.lob.wc7.rating.WC7CovEmpCostData
uses gw.lob.wc7.rating.WC7JurisdictionModifierCostData_TDIC
uses gw.lob.wc7.rating.WC7WaiverOfSubroCostData
uses gw.rating.CostData
uses tdic.pc.config.rating.RatingException
uses gw.api.util.Logger
uses gw.lob.wc7.rating.WC7RatingPeriod

/**
 * Factory class to create necessary cost datas.
 *
 * @author- Ankita Gupta
 */
class WC7CostDataFactory {

  var _daysInTerm : int
  var _rateCache : PolicyPeriodFXRateCache as RateCache

  construct(daysInTerm : int) {
    _daysInTerm = daysInTerm
  }

  public function createCostData(period : WC7RatingPeriod, lineVersion : WC7Line, costEntityName : String, coverage : Coverage, focusBean : EffDated, costType : String, order : int) : CostData {

    var startDate = (period != null) ? period.RatingStart : focusBean.EffectiveDate
    var endDate = (period != null) ? period.RatingEnd : focusBean.ExpirationDate

    if (lineVersion.Branch.Job typeis Cancellation and endDate.beforeOrEqual(startDate)) {
      endDate = startDate.addSeconds(1)
    }

    switch (costEntityName) {

      case "WC7WaiverOfSubroCost":

        return createAndInit( lineVersion, \ ->
            new WC7WaiverOfSubroCostData( startDate, endDate, (focusBean as WC7WaiverOfSubro).WC7WaiverJurisdiction, focusBean as WC7WaiverOfSubro, Currency.TC_USD, _rateCache), order)

      case "WC7JurisdictionCovCost":
        return createAndInit(lineVersion, \->
            new WC7JurisdictionCovCostData(startDate, endDate, coverage.FixedId, focusBean as WC7Jurisdiction, WC7JurisdictionCostType.get(costType), Currency.TC_USD), order)

      case "WC7JurisdictionCost":
        return createAndInit( lineVersion, \ ->
            new WC7JurisdictionCostData_TDIC( startDate, endDate, focusBean as WC7Jurisdiction, WC7JurisdictionCostType.get(costType), order), order)

      case "WC7CovEmpCost":
        var covEmp = focusBean as WC7CoveredEmployeeBase
        var effDate = covEmp.EffectiveDateForRating
        var expDate = covEmp.ExpirationDateForRating
        if (lineVersion.Branch.Job typeis Cancellation and expDate.beforeOrEqual(effDate)) {
          expDate = effDate.addSeconds(1)
        }
        return createAndInit(lineVersion, \-> new WC7CovEmpCostData(effDate, expDate, covEmp.FixedId, coverage.FixedId,
            JurisdictionMappingUtil.getJurisdiction(covEmp.Location), WC7CovEmpCostType.get(costType), Currency.TC_USD), order)

      case "WC7JurisdictionModifierCost" :
        return createAndInit( lineVersion, \ ->
            new WC7JurisdictionModifierCostData_TDIC( startDate, endDate, focusBean as WC7Modifier, WC7JurisdictionCostType.get(costType), order), order)

    }
    throw new RatingException("Error creating cost data '" + costEntityName + "' for '" + focusBean + "'")
  }

  public function createCondCostData(lineVersion : WC7Line, costEntityName : String, condition : PolicyCondition, focusBean : EffDated, costType : WC7JurisdictionCostType) : CostData {
    return null
  }

  private function createAndInit(lineVersion : WC7Line, createCostDataFn : block() : WC7CostData, order : int) : CostData {

    var costData = createCostDataFn()
    costData.init(lineVersion)
    costData.NumDaysInRatedTerm = _daysInTerm
    costData.CalcOrder = order

    return costData
  }
}