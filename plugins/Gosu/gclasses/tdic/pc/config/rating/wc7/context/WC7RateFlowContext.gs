package tdic.pc.config.rating.wc7.context

uses gw.lob.wc7.rating.WC7CovEmpCostData
uses java.util.Map
uses tdic.pc.config.rating.wc7.WC7PremiumKey
uses gw.lob.wc7.rating.WC7CostData
uses tdic.pc.config.rating.wc7.WC7PremiumTotal
uses tdic.pc.config.rating.wc7.WC7RatingEngine
uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses java.math.BigDecimal
uses gw.financials.Prorater

/**
 * Contextual information and is a 1-to-1 at the RatingEngine level.
 *
 */
 
class WC7RateFlowContext extends RateFlowContext <WC7Line> {

  var _preferredCurrency: Currency as PreferredCurrency = Currency.TC_USD
  var _daysInTerm: int as readonly NumberOfDaysInTerm
  var _errorMessages: List <String> as readonly ErrorMessages = {}
  var _ratingLevel: typekey.RateBookStatus as readonly MinimumRatingLevel
  
  /** This costCache holds a custom set of cost data/objects which allows 
   *  us to share information during the rating iteration but that is independent 
   *  of the platform-defined cost objects that are actually used to develop premium. */
  var _costCache : Map<WC7PremiumKey, WC7PremiumTotal> as readonly CostCache = {}
  
  var _payrollMap :  Map<String, Map> as readonly PayrollMap
  var _line : WC7Line as readonly PolicyLine
  var _engine : WC7RatingEngine as readonly Engine

  
  construct(rEngine : WC7RatingEngine) {
    super(rEngine.CostDataMap)
    _engine = rEngine
    _line = rEngine.PolicyLine
    _daysInTerm = rEngine.NumDaysInCoverageRatedTerm
    _ratingLevel = rEngine.RatingLevel
    _payrollMap = WC7RatingUtil.getPayrollMap(this)
    initCostCache()
  }
  
  /** constructor adapted specifically for annualized-base premium calculator */
  construct(line: WC7Line, daysInTerm: int, ratingLevel: typekey.RateBookStatus) {
    super(null)
    _daysInTerm = daysInTerm
    _ratingLevel = ratingLevel
    _line = line
  }

  /**
   * Add error messages
   *
   * @param errorMessage error message to display
   */
  public function addErrorMessage(errorMessage: String) {
    _errorMessages.add(errorMessage)
  }

  public function updateCache(key : WC7PremiumKey, costData : WC7CostData) {
    updateCostCache(key, costData)
  }
  
  private function initCostCache() {
    var premiumLevels = WC7PremiumLevelType.getTypeKeys(false).orderBy(\ w -> w.Priority)
    for(j in PolicyLine.WC7Jurisdictions) {
      for(period in j.RatingPeriods) {
        var lastTotal : WC7PremiumTotal = null
        for (level in premiumLevels) {
          var premiumKey = new WC7PremiumKey(j.Jurisdiction, level, period.RatingStart)
          var currentTotal = new WC7PremiumTotal(premiumKey, lastTotal)
         _costCache.put(premiumKey, currentTotal)
          lastTotal = currentTotal
        }
      }
    }
  }
  

  private function updateCostCache(key : WC7PremiumKey, costData : WC7CostData) {
    var premiumTotal = _costCache.get(key)
    if(premiumTotal == null) {
      //finding substitute key
      var subKey = _costCache.Keys.where(\ w -> w.Jurisdiction == key.Jurisdiction and
                                                w.PremiumLevelType == key.PremiumLevelType and
                                                w.RatingPeriodStart.beforeOrEqual(key.RatingPeriodStart))
                                  .sortBy(\ w -> w.RatingPeriodStart.daysBetween(key.RatingPeriodStart)).first()
      
      if(subKey != null) {
        //If substitute key is found, use it to update cost cache
        premiumTotal = _costCache.get(subKey)
      } else {
        //log instance of key where substitute is not found
      }
    }
    
    if(premiumTotal != null) {
      if(costData.ActualTermAmount != null) {
        if(costData typeis WC7CovEmpCostData and costData.ActualTermAmount == 0bd) {
          return
        }
        premiumTotal.addCost(costData)
      }
    } else {
      //Add logging for costs that slip through the cracks. (should indicate problem)
    }
  }


  public function runningInPremiumReport() : boolean {
    return PolicyLine.Branch.Job typeis Audit && PolicyLine.Branch.Audit.AuditInformation.IsPremiumReport
  }
  
  public function runningInFinalAudit() : boolean {
    return PolicyLine.Branch.Job typeis Audit && PolicyLine.Branch.Audit.AuditInformation.IsFinalAudit
  }
  

  public function isPolicyCancelled() : boolean {
    return PolicyLine.Branch.Canceled
  }

  public function isPolicyCancelledProRata() : boolean {
    return isPolicyCancelled() and PolicyLine.Branch.RefundCalcMethod == CalculationMethod.TC_PRORATA
  }

  public function isPolicyCancelledShortRate() : boolean {
    return isPolicyCancelled() and PolicyLine.Branch.RefundCalcMethod == CalculationMethod.TC_SHORTRATE
  }

  public function getShortRateDays() : BigDecimal {
    if(isPolicyCancelledShortRate()) {
      return Prorater.forRounding(0, HALF_UP,ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(PolicyLine.EffectiveDate, PolicyLine.Branch.CancellationDate)
    }
    return 0
  }
}