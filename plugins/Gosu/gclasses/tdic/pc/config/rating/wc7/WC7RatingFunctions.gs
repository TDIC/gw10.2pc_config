package tdic.pc.config.rating.wc7

uses gw.lob.wc7.rating.WC7CovEmpCostData
uses gw.lob.wc7.rating.WC7JurisdictionCostData_TDIC
uses gw.lob.wc7.rating.WC7JurisdictionModifierCostData_TDIC
uses gw.lob.wc7.rating.WC7RateFactorSearch
uses gw.pl.persistence.core.Key
uses gw.rating.flow.RatingFunctionSource
uses gw.rating.rtm.query.QueryStrategyFactory
uses tdic.pc.config.rating.wc7.context.WC7RateRoutineContext
uses java.math.BigDecimal
uses java.lang.Integer
uses java.math.RoundingMode

uses gw.financials.Prorater
uses typekey.Job

/**
 * Custom functions for direct use in rate routines,
 *
 */
class WC7RatingFunctions extends RatingFunctionSource {
  private static var manualPremClassCodeList ={"7707","7722","8278","8631"}

  override protected function availableForLine(policyLineCode : String) : boolean {
    return policyLineCode.equals("WC7Line")
  }

  public static function hasSafetyProgramCredit(routineContext : WC7RateRoutineContext) : Boolean {
    return isModifierEligible(routineContext, "WC7CertifiedSafety")
  }

  /**
   *
   */
  public static function getExpModFactor(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getModifierFactor(routineContext, "WC7ExpMod")
  }


  public static function getScheduleModFactor(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getModifierFactor(routineContext, "WC7ScheduleMod")
  }

  /**
   *
   */
  public static function getTotalManualPremiumByState(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getTotalPremiumByStateAt(routineContext, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM)
  }

  /**
   *
   */
  public static function getTotalSubjectPremiumByState(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getTotalPremiumByStateAt(routineContext, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM)
  }

  /**
   * Getting Total Modified Premium By State
   */
  public static function getTotalModifiedPremiumByState(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getTotalPremiumByStateAt(routineContext, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM)
  }

  /**
   * Getting Total Standard Premium By State
   */
  public static function getTotalStandardPremiumByState(routineContext : WC7RateRoutineContext) : BigDecimal {
    return getTotalPremiumByStateAt(routineContext, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM)
  }

  /**
   *
   */
  public static function getManuscriptPremium(routineContext : WC7RateRoutineContext) : BigDecimal {
    return (routineContext.RatingStepData.FocusBean as WC7ManuscriptOption).Premium
  }

  /**
   *
   */
  public static function getShortRateDays(routineContext : WC7RateRoutineContext) : BigDecimal {
    return routineContext.RateFlowContext.getShortRateDays()
  }

  /**
   *
   */
  public static function prorate(routineContext : WC7RateRoutineContext, amount : BigDecimal) : BigDecimal {
    if(amount != null and amount != 0) {
      var branch = routineContext.PolicyLine.Branch
      var ratingPeriod = routineContext.RatingPeriod
      return Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).prorate(branch.PeriodStart, branch.PeriodStart.addYears(1), ratingPeriod.RatingStart, ratingPeriod.RatingEnd, amount)
    }
    return 0
  }

  /**
   * Total standard premium for premium discout
   */
  public static function getPremiumDiscountBasis(routineContext: WC7RateRoutineContext) : BigDecimal {
    return getTotalPremiumByStateForAllPeriods(routineContext, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM)
  }

  public static function getTerrorismBasis(routineContext : WC7RateRoutineContext) : Integer {
    return WC7RatingUtil.getTerrorismBasis(routineContext.RateFlowContext, routineContext.RatingPeriod.Jurisdiction.Jurisdiction.Code)
  }

  /**
   * Function to check if the policy is "Minimum Premium" policy?
   */
  public static function isMinimumPremiumPolicy(routineContext: WC7RateRoutineContext) : Boolean {
    var premiumTotal  = getTotalPremiumByStateAt(routineContext, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM)
    if (premiumTotal != null and premiumTotal < 320) {
      return true
    }
    return false
  }

  /**
   * Function to identify is multiline discount is Yes or No
   *
   * @param routineContext - WC7RateRoutineContext
   * @return boolean
   */
  public static function hasMultilineDiscount(routineContext : WC7RateRoutineContext) : boolean {
    return isModifierEligible(routineContext, "WC7TDICMultiLineDiscount")
  }

  /**
   * Function to calculate class code premium 1.e total manual premium per class code, location and governing law
   *
   * @param routineContext - WC7RateRoutineContext
   * @param classCode      - Covered Employee Class Code
   * @return classCodePrem manual premium per class code
   */
  public static function getTotalManualPremiumByClasscode(routineContext : WC7RateRoutineContext, classCode : String) : BigDecimal{
    var classCodePrem : BigDecimal = 0
    var location : PolicyLocation
    var costs : List<WC7CovEmpCostData>
    var focusBean = routineContext.RatingStepData.FocusBean
    var premiumTotal : WC7PremiumTotal
    var currentStatus = routineContext.WC7WorkersCompLine.AssociatedPolicyPeriod.Job.Subtype
    var line = routineContext.PolicyLine as WC7Line
    var isCostOverride = false
    var ratingPeriod = routineContext.RatingPeriod
    if (focusBean typeis WC7WaiverOfSubro) {
      location = focusBean.Location
      var covEmpWaivers = line.AllWC7CoveredEmployeesWM.where(\elt -> elt.ClassCode.Code == focusBean.ClassCode.Code
          and elt.GoverningLaw.Code == focusBean.GoverningLaw.Code and elt.Location.FixedId == focusBean.Location.FixedId)
      var covEmpBasis = covEmpWaivers*.BasisAmount.sum()
      for (covEmpWaiver in covEmpWaivers) {
        isCostOverride = WC7RatingUtil.isCostOveridden(covEmpWaiver.WC7CovEmpCost)
        if (isCostOverride) {
          break
        }
      }
      if (currentStatus == Job.TC_POLICYCHANGE and focusBean.BasisForRating != covEmpBasis and !isCostOverride) {
        classCodePrem = calculateWaiverForPolicyChange(focusBean as WC7WaiverOfSubro, routineContext)
      } else {
        premiumTotal = getPremiumTotal(routineContext, routineContext.RatingPeriod.Jurisdiction, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM)
        costs = premiumTotal.Costs.whereTypeIs(WC7CovEmpCostData).toList()
        for (cost in costs) {
          var covEmp = WC7RatingUtil.getCoveredEmployee(routineContext.RateFlowContext, cost.EmpID)
          if (cost != null and covEmp.ClassCode.Code == classCode and covEmp.Location.FixedId == location.FixedId and covEmp.GoverningLaw.Code == focusBean.GoverningLaw.Code
              and covEmp.EffectiveDateForRating >= ratingPeriod.RatingStart and covEmp.EffectiveDateForRating < ratingPeriod.RatingEnd
              and covEmp.ExpirationDateForRating > ratingPeriod.RatingStart and covEmp.ExpirationDateForRating <= ratingPeriod.RatingEnd)
            classCodePrem += cost.ActualAmount
        }
      }
    }
    return classCodePrem
  }
  /**
   * Function to calculate total premium till Terrorism i.e Standrad premium +Expense constant +Terrorism Premium
   *
   * @param routineContext - WC7RateRoutineContext
   * @return totalPrem
   */
  public static function getTotalPremiumTaxesAndSurcharges(routineContext : WC7RateRoutineContext) : BigDecimal {
    var totalPrem : BigDecimal = getTotalPremiumByStateForAllPeriods(routineContext, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM ) //total standard premium
    var jurisdiction = routineContext.RatingPeriod.Jurisdiction
    var key = new WC7PremiumKey(jurisdiction.Jurisdiction, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM, routineContext.RatingPeriod.RatingStart)
    var premiumTotal : WC7PremiumTotal = routineContext.RateFlowContext.CostCache.get(key)

    var terrorismCost = premiumTotal?.Costs?.whereTypeIs(WC7JurisdictionCostData_TDIC)?.firstWhere(\elt -> elt.CostType == WC7JurisdictionCostType.TC_TERRORPREM)
    if(terrorismCost != null) {
      totalPrem += terrorismCost.ActualAmount
    }

    var expenseConstCost = premiumTotal?.Costs?.whereTypeIs(WC7JurisdictionCostData_TDIC)?.firstWhere(\elt -> elt.CostType == WC7JurisdictionCostType.TC_EXPENSECONST)
    if(expenseConstCost != null) {
      totalPrem += expenseConstCost.ActualAmount
    }

    return totalPrem
  }

  /**
   * Function to return annual standard premium stored for a quoted policy
   * Total Standard Premium is sum of Manual, Waiver, Experience Mod, Schedule, Safety and Mulitline Discount
   * @param routineContext
   * @return Annual Standard Premium
   */
  public static function getTotalAnnualStandardPremiumByState(routineContext : WC7RateRoutineContext) : BigDecimal {
    var premium : BigDecimal = 0
    var ratingPeriod = routineContext.RatingPeriod
    if (routineContext.WC7WorkersCompLine.AssociatedPolicyPeriod.Job.Subtype == Job.TC_AUDIT) {
      var totalStdPrem = getTotalStandardPremiumByState(routineContext)
      var policyeffectiveDays = routineContext.RateFlowContext.NumberOfDaysInTerm
      var auditEffectiveDays = Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(ratingPeriod.RatingStart, ratingPeriod.RatingEnd)
      premium = (totalStdPrem * policyeffectiveDays / auditEffectiveDays).setScale(0, HALF_UP)
    } else {
      var costs = routineContext.WC7WorkersCompLine.Branch.BasedOn.AllCosts
      for (cost in costs) {
        if ((cost typeis WC7CovEmpCost) or (cost typeis WC7WaiverOfSubroCost)
            or (cost typeis WC7ModifierCost)) {
          premium += cost.ActualAmount.Amount
        }
      }
    }
    return premium
  }
  /**
   * Fuction to return Total Basis for Safety group discount
   */
  public static function getTotalSafetyDiscountBasis(routineContext: WC7RateRoutineContext) : BigDecimal {
    var totalPrem : BigDecimal =  getTotalModifiedPremiumByState(routineContext)

    var jurisdiction = routineContext.RatingPeriod.Jurisdiction
    var key = new WC7PremiumKey(jurisdiction.Jurisdiction, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM, routineContext.RatingPeriod.RatingStart)
    var premiumTotal : WC7PremiumTotal = routineContext.RateFlowContext.CostCache.get(key)

    var schedCreditCost = premiumTotal?.Costs?.whereTypeIs(WC7JurisdictionModifierCostData_TDIC)?.firstWhere(\elt -> elt.CostType == WC7JurisdictionCostType.TC_SCHEDCREDIT)
    if(schedCreditCost != null) {
      totalPrem += schedCreditCost.ActualAmount
    }

    return totalPrem
  }

/**********************************************************************************************************************
 ****************************************Private Functions Only - Start************************************************
 **********************************************************************************************************************/
  /**
   *
   */
  private static function getTotalPremiumByStateAt(routineContext : WC7RateRoutineContext, premiumLevel : WC7PremiumLevelType) : BigDecimal {
    var premiumTotal = getPremiumTotal(routineContext, routineContext.RatingPeriod.Jurisdiction, premiumLevel)
    if(premiumTotal != null) {
      return premiumTotal.totalCost()
    }
    return 0
  }
  /**
   *
   */
  private static function getModifierFactor(routineContext : WC7RateRoutineContext, modifierType : String) : BigDecimal {
    if(modifierType != null) {
      var modifier = routineContext.RatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(routineContext.RatingPeriod.RatingStart)
          .firstWhere(\ w -> w.Pattern.CodeIdentifier == modifierType)
      if (modifier.Pattern.ModifierDataType == TC_RATE and modifier.RateWithinLimits != null) {
        return modifier.RateWithinLimits
      }
    }
    return 0
  }

  /**
   *
   */
  private static function getModifierTypeCode(routineContext : WC7RateRoutineContext, modifierType : String) : String {
    if(modifierType != null) {
      var modifier = routineContext.RatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(routineContext.RatingPeriod.RatingStart)
          .firstWhere(\ w -> w.Pattern.CodeIdentifier == modifierType)
      if(modifier.DataType == TC_TYPEKEY) {
        return modifier.TypeKeyModifier
      }
    }
    return null
  }

  /**
   * Function to check if current cost is of modifier or not
   * @param routineContext
   * @param modifierType
   * @return boolean
   */
  private static function isModifierEligible(routineContext : WC7RateRoutineContext, modifierType : String) : Boolean {
    if(modifierType != null) {
      var modifier = routineContext.RatingPeriod.Jurisdiction.VersionList.WC7ModifiersAsOf(routineContext.RatingPeriod.RatingStart)
          .firstWhere(\ w -> w.Pattern.CodeIdentifier == modifierType)
      if(modifier.DataType == TC_BOOLEAN) {
        return modifier.BooleanModifier.booleanValue()
      }
    }
    return false
  }

  /**
   * Function to get total Premium per premium level
   * @param routineContext
   * @param jurisdiction
   * @param premiumLevel
   * @return total premium per premium level
   */
  private static function getPremiumTotal(routineContext : WC7RateRoutineContext, jurisdiction : WC7Jurisdiction, premiumLevel : WC7PremiumLevelType) : WC7PremiumTotal {
    var key = new WC7PremiumKey(jurisdiction.Jurisdiction, premiumLevel, routineContext.RatingPeriod.RatingStart)
    return routineContext.RateFlowContext.CostCache.get(key)
  }

  /**
   * Total Premium from all split periods on the policy, for a specific jurisdiction
   */
  private static function getTotalPremiumByStateForAllPeriods(routineContext: WC7RateRoutineContext, premiumLevel : WC7PremiumLevelType) : BigDecimal {
    var premium : BigDecimal = 0
    var juri = routineContext.RatingPeriod.Jurisdiction
    var periods = juri.RatingPeriods
    for(period in periods) {
      if (period.RatingStart.before(period.RatingEnd)) {
        var key = new WC7PremiumKey(juri.Jurisdiction, premiumLevel, period.RatingStart)
        var premiumTotal = routineContext.RateFlowContext.CostCache.get(key)
        if(premiumTotal != null) {
          premium += premiumTotal.totalCost()
        }
      }
    }
    return premium
  }

  /**
   * Function to calculate waiver in case of Policy Change
   */

  private static function calculateWaiverForPolicyChange(focusBean : WC7WaiverOfSubro , routineContext : WC7RateRoutineContext) : BigDecimal{
    var classCodePrem : BigDecimal = 0
    var waiverBasis = focusBean.BasisForRating
    var waiverClassCd = focusBean.ClassCode.Code
    var table = routineContext.RateBook.getTable("WC7ClassCodeRate_TDIC")
    var lossCostMultTable = routineContext.RateBook.getTable("WC7LossCostMultiplier_TDIC")
    var params = {waiverClassCd}.toTypedArray()
    var lossCostParam = {"Y"}.toTypedArray()
    var waiverRate = (new QueryStrategyFactory().getFactorQuery(table).convertParamsAndQuery(table, params, null)).Factor
    var lossCostMultipler = (new QueryStrategyFactory().getFactorQuery(lossCostMultTable).convertParamsAndQuery(lossCostMultTable, lossCostParam, null)).Factor
    if (manualPremClassCodeList.contains(waiverClassCd)) {
      classCodePrem = (waiverBasis * ((waiverRate as BigDecimal * lossCostMultipler as BigDecimal).setScale(2, RoundingMode.HALF_UP))).setScale(0, RoundingMode.HALF_UP)
    } else {
      classCodePrem = (waiverBasis * ((waiverRate as BigDecimal * lossCostMultipler as BigDecimal).setScale(2, RoundingMode.HALF_UP)) / 100).setScale(0, RoundingMode.HALF_UP)
    }
    return classCodePrem
  }
/**********************************************************************************************************************
 ******************************************Private Functions Only - End************************************************
 **********************************************************************************************************************/
}