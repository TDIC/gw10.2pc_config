package tdic.pc.config.rating.wc7

uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.api.util.Logger
uses gw.financials.Prorater
uses gw.job.RenewalProcess
uses gw.lob.wc7.rating.WC7RatingPeriod
uses gw.pl.persistence.core.Key
uses gw.rating.flow.util.QueryUtils
uses gw.rating.rtm.NoSuitableRateBookFoundException
uses gw.rating.rtm.query.QueryStrategyFactory
uses gw.rating.rtm.query.RateQueryParam
uses tdic.pc.config.rating.RatingException
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext

uses java.math.BigDecimal

uses entity.WC7Cost
uses entity.DefaultRateFactorRow
uses entity.WC7Modifier

/**
 * Util class with custom methods for rating Worker's comp
 *
 * @author - Ankita Gupta
 */

class WC7RatingUtil {
  static var log = Logger.forCategory("Application.Rating")
  private static final var RATE_ORDER_CALCULATIONS = "WC7RateOrderTable_TDIC"


  private static final var COVERED_OBJECT_LEVEL : java.util.Map<String, WC7RatingLevel_TDIC> = {
      "WC7WorkersCompLine" -> WC7RatingLevel_TDIC.TC_WC7LINE,
      "WC7Jurisdiction" -> WC7RatingLevel_TDIC.TC_WC7JURISDICTION,
      "WC7CoveredEmployee" -> WC7RatingLevel_TDIC.TC_WC7COVEMP
  }

  private static final var LEGACY_CALC_ORDER_MAPPING : Map<Integer, Integer> = {
      190 -> 10,
      210 -> 110,
      310 -> 210,
      430 -> 310,
      431 -> 310,
      440 -> 320,
      450 -> 330,
      460 -> 370,
      470 -> 380,
      505 -> 410,
      507 -> 420,
      520 -> 430,
      530 -> 440,
      540 -> 450,
      550 -> 460,
      560 -> 470,
      570 -> 480,
      510 -> 490

  }

  /**
   * This method gets rateorder table entries from selected ratebook
   */
  public static function readRateOrderEntries(book : RateBook) : List<WC7ROCEntry> {
    return book.getTable(RATE_ORDER_CALCULATIONS).Factors.map(\elt -> WC7ROCEntry.createFromRow(elt as DefaultRateFactorRow))
  }

  /**
   * Gets ratinlevel for given covered object
   */
  public static function getRatingLevelForCoveredObject(coveredObject : String) : WC7RatingLevel_TDIC {
    return COVERED_OBJECT_LEVEL.get(coveredObject)
  }

  /**
   * Util to pick rate book
   */
  public static function selectRateBook(
      line : WC7Line,
      ratingLevel : typekey.RateBookStatus,
      offeringCode : String,
      jurisdiction : WC7Jurisdiction = null) : RateBook {

    var refDate = getJurisdictionStartDate(line, jurisdiction)
    log.info("Selecting Ratebook: RefDate:" + refDate + " RatingLevel:" + ratingLevel + " OfferingCode:" + offeringCode + " Jurisdiction:" + jurisdiction)
    if (refDate == null) {
      throw new IllegalArgumentException("ref date cannot be null")
    }
    var baseState = jurisdiction != null ? jurisdiction.Jurisdiction : line.BaseState
    try {
      return RateBook.
          selectRateBook(
              refDate,
              line.Branch.RateAsOfDate,
              line.PatternCode,
              baseState,
              ratingLevel,
              line.Branch.JobProcess typeis RenewalProcess,
              offeringCode)
    } catch (e : NoSuitableRateBookFoundException) {
      if (QueryUtils.getRateBooksForLine("WC7Line").size() == 0) {
        throw new RatingException("No WC7Line rate books loaded")
      } else {
        throw new RatingException("No WC7Line rate book found for rating level : ${ratingLevel}")
      }
    }
  }

  /**
   * Method to get factors from rate order table
   */
  public static function getFactor(table : RateTable, params : Comparable<RateQueryParam>[], factorColum : String) : Object {
    return new QueryStrategyFactory().getFactorQuery(table).query(table, params, factorColum).Factor
  }


  public static function getMultiFactors(table : RateTable, params : Comparable<RateQueryParam>[], factorColumns : List<String>) : Map {
    return new QueryStrategyFactory().getFactorQuery(table).queryMultipleFactors(table, params, factorColumns)
  }

  public static function getPayrollMap(context : WC7RateFlowContext) : Map<String, Map> {
    var payrollMap = new HashMap<String, Map>()
    var engine = context.Engine
    var line = engine.PolicyLine
    if (line != null) {
      for (juri in line.WC7Jurisdictions) {
        var classCodeMap = new HashMap<String, Map<String, Object>>()
        var covEmps = line.VersionList.WC7CoveredEmployees.flatMap(\elt -> elt.AllVersions).where(\wcCovEmp -> wcCovEmp.Jurisdiction.FixedId == juri.FixedId)
        for (covEmp in covEmps) {
          if (covEmp typeis WC7CoveredEmployee) {
            var classCode = covEmp.ClassCode.Code
            var payroll = covEmp.BasisForRating
            var classCodeDataMap = classCodeMap.get(classCode)
            if (classCodeDataMap != null) {
              var totalPayroll = classCodeDataMap.get("TotalPayroll")
              if (totalPayroll != null) {
                payroll += (totalPayroll) as Integer
                classCodeDataMap.put("TotalPayroll", payroll)
              }
            } else {
              classCodeDataMap = new HashMap<String, Object>()
              try {
                var rateBook = selectRateBook(line, engine.RatingLevel, line.Branch.Offering as String, juri)
                classCodeDataMap.put("TotalPayroll", payroll)
              } catch (e : Exception) {
                log.error(e.StackTraceAsString)
              }
            }
            classCodeMap.put(classCode, classCodeDataMap)
          }
        }
        payrollMap.put(juri.Jurisdiction.Code, classCodeMap)
      }
    }
    return payrollMap
  }

  public static function getNumDaysInCoverageRatedTerm(context : WC7RateFlowContext) : BigDecimal {
    return Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(context.PolicyLine.EffectiveDate, context.PolicyLine.EffectiveDate.addYears(1))
  }

  public static function isModifierRatable(period : WC7RatingPeriod, entry : WC7ROCEntry) : boolean {
    if (getModifier(period, entry) != null) {
      return true
    }
    return false
  }

  public static function getModifier(period : WC7RatingPeriod, entry : WC7ROCEntry) : WC7Modifier {
    if (entry.ClausePatternCode != null) {
      var modifier = period.Jurisdiction.VersionList.WC7ModifiersAsOf(period.RatingStart)
          .firstWhere(\w -> w.PatternCode == entry.ClausePatternCode)
      if (modifier.Pattern.ModifierDataType == TC_RATE and modifier.RateWithinLimits != null and modifier.RateWithinLimits != 0) {
        return modifier
      }
    }
    return null
  }


  public static function booleanModifierExists(period : WC7RatingPeriod, entry : WC7ROCEntry) : boolean {
    if (entry.ClausePatternCode != null) {
      var modifier = period.Jurisdiction.VersionList.WC7ModifiersAsOf(period.RatingStart)
          .firstWhere(\w -> w.Pattern.toString() == entry.ClausePatternCode)
      if (modifier != null and modifier.Pattern.ModifierDataType == TC_BOOLEAN) {
        return modifier.BooleanModifier.booleanValue()
      }
    }
    return false
  }

/*  public static function isManuscriptOption(entry : WC7ROCEntry) : Boolean {
    if (entry.RoutineCode == "WC7ManuscriptEndorsement") {
      return true
    }
    return false
  }*/

  public static function isROCEntryEligible(entry : WC7ROCEntry, juri : Jurisdiction = null) : boolean {
    if (entry.Availability != null) {
      if (("CW".equalsIgnoreCase(entry.Availability))
          or (juri != null and entry.Availability.containsIgnoreCase(juri.Code))) {
        return true
      }
    }
    return false
  }

  public static function isROCPremiumReportEligible(context : WC7RateFlowContext, entry : WC7ROCEntry) : boolean {
    if (context.runningInPremiumReport() and (not entry.IsPremiumReporting)) {  //eliminate ineligible entries for premium reporting
      return false
    }
    return true
  }

  /**
   * returns rating periods within the date range
   */
  public static function getRatingPeriods(context : WC7RateFlowContext, juri : WC7Jurisdiction, entry : WC7ROCEntry) : List<WC7RatingPeriod> {
    if (not entry.SplitRatingPeriod) {
      return {new WC7RatingPeriod(juri, context.PolicyLine.Branch.PeriodStart, context.PolicyLine.Branch.PeriodEnd, 1)}
    } else if (context.runningInPremiumReport() or context.runningInFinalAudit()) {
      return juri.AuditRatingPeriods
    }
    return juri.RatingPeriods
  }

  /**
   * NCCI ClassCode rates
   */
/*  public static function getClassCodeRates(context : WC7RateFlowContext, juri : WC7Jurisdiction, classCodes : Set<String>) : Map<String, BigDecimal> {
    var engine = context.Engine
    var line = context.PolicyLine
    var rateMap : Map<String, BigDecimal> = {}

    var rateBook = selectRateBook(line, engine.RatingLevel, line.Branch.Offering as String, juri)
    var ncciRateTable = rateBook.getTable("WC7NCCILossCostsRates")
    for (classCode in classCodes) {
      var param = {new RateQueryParam("ClassCode", classCode) as Comparable<RateQueryParam>}.toTypedArray()
      var rate = getFactor(ncciRateTable, param, "Rate") as BigDecimal
      rateMap.put(classCode, rate)
    }
    return rateMap
  }*/
  public static function getJurisdictionStartDate(line : WC7Line, juri : WC7Jurisdiction) : Date {
    var jurStartDate = line.EffectiveDate
    if (juri == null) {
      return jurStartDate
    }
    var empDate = line.getWC7CoveredEmployeesWM(juri.Jurisdiction).min(\w -> w.EffectiveDate)
    if (empDate.after(jurStartDate)) {
      jurStartDate = empDate
    }
    return jurStartDate
  }

  /**
   * Created to grab the class code for related Classification information
   */

/*  public static function fetchStatCode(cost : WC7Cost) : WC7ClassCode {
    var codes = Query.make(WC7ClassCode).compare("Code", Equals, cost?.StatCode?.trim()).select().toList()
    var jurisdictionHit = codes.where(\c -> c.Jurisdiction == cost?.JurisdictionState)
    if (jurisdictionHit.size() > 0)
      return jurisdictionHit.first()
    if (codes.size() > 0)
      return codes.first()

    return null
  }*/
  public static function getTerrorismBasis(context : WC7RateFlowContext, jurisdictionCode : String) : Integer {
    var basis = 0
    var payrollMap = context.PayrollMap
    var jurisdiction = jurisdictionCode
    if (payrollMap != null) {
      var classCodeMap = payrollMap.get(jurisdiction)
      if (classCodeMap != null and classCodeMap.Values.Count > 0) {
        for (ccData in classCodeMap.Values) {
          if (ccData typeis Map<Object, Object>) {
            var payroll = ccData.get("TotalPayroll")
            if (payroll != null) {
              basis += Coercions.makePIntFrom(payroll)
            }
          }
        }
      }
    }
    return basis
  }

  /**
   * Function to get WC7CoveredEmployee from FixedID
   *
   * @param context       - WC7RateFlowContext
   * @param covEmpFixedId - Key - Fixed IF for covered employee
   * @return WC7CoveredEmployee
   */
  public static function getCoveredEmployee(context : WC7RateFlowContext, covEmpFixedId : Key) : WC7CoveredEmployee {
    var engine = context.Engine
    var line = engine.PolicyLine
    if (line != null) {
      var covEmps = line.WC7CoveredEmployees
      return covEmps.firstWhere(\elt -> elt.FixedId == covEmpFixedId)
    }
    return null
  }

  public static function isJurisdictionCostTaxesAndSurcharges(cost : WC7JurisdictionCost) : boolean {
    if (cost.JurisdictionCostType == TC_ADMINREVOLVINGFUND or cost.JurisdictionCostType == TC_CIGA or cost.JurisdictionCostType == TC_UNINSUREDEMPLOYERSBENEFITTRUSTFUND
        or cost.JurisdictionCostType == TC_SUBSEQUENTINJURIESTRUSTFUND or cost.JurisdictionCostType == TC_OCCUPATIONALSAFETYHEALTHFUND or cost.JurisdictionCostType == TC_LABORENFORCEMENTCOMPLIANCEFUND
        or cost.JurisdictionCostType == TC_FRAUDACCOUNT) {
      return true
    }
    return false
  }

  /**
   * Returns if it is TDIC Underwriting Supervisor
   */
  public static function isUnderwritingSupervisor() : boolean {
    var currUser = User.util.CurrentUser
    var userRoleUWSup = currUser.listAvailableRoles().firstWhere(\elt -> elt.DisplayName == "TDIC Underwriting Supervisor")
    if (userRoleUWSup != null) {
      return true
    }
    return false
  }

  /**
   * Function to check if cost is Overridden
   */

  public static function isCostOveridden(cost : WC7Cost) : boolean {
    var isCostOverride = false
    if (cost.OverrideAmount != null or cost.OverrideAdjRate != null or cost.OverrideBaseRate != null) {
      isCostOverride = true
    }
    return isCostOverride
  }

  /**
   * @param cost
   * @return
   */
  public static function getCalcOrder_TDIC(basedOnCost : WC7Cost) : Integer {
    var calcOrders : Integer = basedOnCost.CalcOrder
    if (basedOnCost.Branch.UpdateTime < getBasicTemplateRatingStartDate()) {
      var legacyCalcOrder = LEGACY_CALC_ORDER_MAPPING.get(basedOnCost.CalcOrder)
      if (legacyCalcOrder != null) {
        calcOrders = legacyCalcOrder
      }
    }
    return calcOrders
  }

  /**
   * new WorkComp rating started date
   */
  public static function getBasicTemplateRatingStartDate() : Date {
    return DateUtil.createDateInstance(1, 17, 2020)    //Fri Jan 17 00:00:00 PST 2020
  }

  /*********************************************************Private functions only*********************************************************************/

}