package tdic.pc.config.rating.wc7.flow

uses java.util.List

interface RateFlowFactory<LINE extends PolicyLine, ITEM extends RateFlowItem> {

  function createRateFlow(line: LINE) : List<ITEM>

}