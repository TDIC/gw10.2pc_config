package tdic.pc.config.rating.wc7.properties

uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext

uses java.math.BigDecimal

class WC7LineProperties {

  var _line : WC7WorkersCompLine
  var _context : WC7RateFlowContext

  var _shortRateFactor : BigDecimal as readonly ShortRateFactor
  var _shortRatePercent : BigDecimal as readonly ShortRatePercent

  var _isPolicyCancelled : Boolean as readonly IsPolicyCancelled
  var _isPolicyCancelledShortRate : Boolean as readonly IsPolicyCancelledShortRate
  var _isPolicyCancelledProRata : Boolean as readonly IsPolicyCancelledProrata
  var _runningInPremiumReport : Boolean as readonly RunningInPremiumReport
  var _runningInFinalAudit : Boolean as readonly RunningInFinalAudit
  var _numDaysInPeriod : BigDecimal as readonly NumDaysInPeriod
  var _numDaysInCoverageRatedTerm : BigDecimal as readonly NumDaysInCoverageRatedTerm
  var _prorataFactor : BigDecimal as readonly ProrationFactor
  var _routineEligibility : RatingEligibility_TDIC as RatingEligiblity
  var _shortRateDays : BigDecimal as readonly ShortRateDays

  construct(rateFlowContext : WC7RateFlowContext) {
    this(rateFlowContext.PolicyLine)
    _context = rateFlowContext
    _isPolicyCancelled = _context.isPolicyCancelled()
    _isPolicyCancelledShortRate = _context.isPolicyCancelledShortRate()
    _isPolicyCancelledProRata= _context.isPolicyCancelledProRata()
    _runningInPremiumReport = _context.runningInPremiumReport()
    _runningInFinalAudit = _context.runningInFinalAudit()
    _numDaysInCoverageRatedTerm = WC7RatingUtil.getNumDaysInCoverageRatedTerm(_context)
    _shortRateDays = _context.getShortRateDays()
  }

  construct(lineVersion : WC7WorkersCompLine) {
    _line = lineVersion
    _numDaysInPeriod = _line?.Branch?.NumDaysInPeriod
    _prorataFactor= _line?.Branch?.ProrationFactor
  }

}
