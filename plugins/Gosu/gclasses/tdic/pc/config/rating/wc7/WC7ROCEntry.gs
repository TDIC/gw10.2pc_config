package tdic.pc.config.rating.wc7

uses gw.api.productmodel.ClausePattern
uses gw.api.upgrade.PCCoercions

uses java.lang.Integer

/**
 * Class represents Rate Order Calculation (ROC) entry item.
 *
 * @author - Ankita Gupta
 */

class WC7ROCEntry {
  construct() {
  }

  var _premiumLevel : WC7PremiumLevelType as readonly PremiumLevel
  var _premiumReporting : boolean as readonly IsPremiumReporting
  var _clauseCode : String as readonly ClausePatternCode
  var _cost : String as readonly CostEntityName
  var _level : WC7RatingLevel_TDIC as readonly RatingLevelExt
  var _routineCode : String as readonly RoutineCode
  var _eligibilityRoutineCode : String as readonly EligibilityRoutineCode
  var _order : int as readonly Order
  var _costType : String as readonly CostType
  var _crossJurisdiction : Boolean as readonly IsCrossJurisdiction
  var _availability : String as readonly Availability
  var _splitRatingPeriod : Boolean as readonly SplitRatingPeriod

  construct(
      premLevel : WC7PremiumLevelType,
      premiumReporting : boolean,
      clauseCode : String,
      cost : String,
      level : WC7RatingLevel_TDIC,
      rtCode : String,
      elgRoutineCode : String,
      cType : String,
      ord : int,
      crossJur : Boolean,
      availableStates : String,
      splitRtPeriod : Integer) {
    _premiumLevel = premLevel
    _premiumReporting = premiumReporting
    _clauseCode = clauseCode
    _cost = cost
    _level = level
    _routineCode = rtCode
    _eligibilityRoutineCode = elgRoutineCode
    _costType = cType
    _order = ord
    _crossJurisdiction = crossJur
    _availability = availableStates
    _splitRatingPeriod = splitRtPeriod == 0 ? false : true
  }

  /**
   * Creates ROC entry POGO objects for rate flows.
   *
   * @param row table lookup value frow wc7_tdic_roc
   * @return POGO for roc entry row
   */
  public static function createFromRow(row : DefaultRateFactorRow) : WC7ROCEntry {

    var clauseCode = row.str4
    var premiumLevel = WC7PremiumLevelType.get(row.str3)
    var premiumReporting = row.bit2
    var cost = row.str6
    var routineCode = row.str1
    var eligibilityRoutineCode = row.str8
    var level = row.str5.HasContent ? typekey.WC7RatingLevel_TDIC.get(row.str5) : inferLevel(row.str5)
    var costType = row.str7
    var order = row.int2
    var crossJuri = row.bit1
    var avail = row.str2
    var splitPeriod = row.int3

    return new WC7ROCEntry(
        premiumLevel,
        premiumReporting,
        clauseCode,
        cost,
        level,
        routineCode,
        eligibilityRoutineCode,
        costType,
        order,
        crossJuri,
        avail,
        splitPeriod
    )
  }


  private static function inferLevel(cPattern : String) : WC7RatingLevel_TDIC {
    var pattern = PCCoercions.makeProductModel<ClausePattern>(cPattern)
    return WC7RatingUtil.getRatingLevelForCoveredObject(pattern.OwningEntityType)
  }


}