package tdic.pc.config.rating.wc7.context

uses gw.rating.CostData
uses gw.rating.worksheet.domain.WorksheetEntryContainer
uses tdic.pc.config.rating.wc7.flow.RateFlowItem
uses java.util.Collections
uses java.util.HashMap
uses java.util.Map
uses java.util.List

abstract class RateRoutineContext<LINE extends PolicyLine> {
  var _paramSet : Map<typekey.CalcRoutineParamName, Object> as readonly ParamSet
  var _costDatas : List<CostData> = {}
  var _policyLine : LINE as readonly PolicyLine
  var _costData : CostData as readonly CostData
  var _worksheetEntryContainer : WorksheetEntryContainer as readonly WorksheetEntryContainer

  construct( costDatas : List<CostData>,
             parmSet : Map<typekey.CalcRoutineParamName, Object>,
             pLine : LINE,
             cData : CostData,
             wsEntryContainer : WorksheetEntryContainer){

    init(costDatas, parmSet, pLine, cData, wsEntryContainer)

  }

  private function init(costDatas : List<CostData>,
                        parmSet : Map<typekey.CalcRoutineParamName, Object>,
                        pLine : LINE,
                        cData : CostData  /** nullable for no-cost routines */,
                        wsEntryContainer : WorksheetEntryContainer) {

    _costDatas.addAll(costDatas)
    _paramSet = new HashMap<typekey.CalcRoutineParamName, Object>(parmSet)
    _policyLine = pLine
    _costData = cData
    _worksheetEntryContainer = wsEntryContainer

  }

  public property get ThisSliceCostDatas() : List<CostData> {
    return _costDatas.where( \ data -> data.EffectiveDate == _policyLine.EffectiveDate )
  }

  public property get AllSliceCostDatas() : List<CostData> {
    return Collections.unmodifiableList(_costDatas)
  }
  public function addParam(name : typekey.CalcRoutineParamName, value : Object) {
    _paramSet.put(name, value)
  }

}