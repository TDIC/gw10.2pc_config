package tdic.pc.config.rating.wc7

uses com.google.common.base.Optional
uses gw.api.domain.Clause
uses gw.lob.wc7.rating.WC7RatingPeriod

/**
 * This class contains object needed for execution of rate routine
 *
 * @author Ankita Gupta
 * 05/21/2019
 */
class WC7CovStepData implements WC7StepData {

  protected var _book : RateBook as RateBook
  protected var _cov : entity.Coverage as Coverage
  protected var _focusBean : entity.EffDated as FocusBean
  protected var _ratingPeriod : WC7RatingPeriod as RatingPeriod

  construct(book : RateBook, cov : Coverage, fBean : EffDated, rPeriod : WC7RatingPeriod) {
    _book = book;
    _cov = cov
    _focusBean = fBean
    _ratingPeriod = rPeriod
  }

  override property get PolicyLineVersion() : WC7Line {
    if (_cov == null) {
      return _ratingPeriod.Jurisdiction.PolicyLine as WC7Line
    }
    return _cov.PolicyLine as WC7Line
  }

  override property get OptionalCoverage() : Optional<Clause> {
    return Optional.fromNullable(_cov)
  }

  property get CoveragePart() : Coverable {
    return _cov.OwningCoverable
  }

  public function toString() : String {
    return "Cov '" + Coverage.PatternCode + "' for bean '" + _focusBean.toString() + "'"
  }

}