package tdic.pc.config.gl

uses gw.api.database.IQueryBeanResult
uses gw.api.database.IQueryBuilder
uses gw.api.database.Query
uses gw.search.EntitySearchCriteria

/**
 * A search criteria used for searching GL School Codes via the UI.
 */

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 9/23/2019
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
@Export
class GLSchoolCodeSearchCriteria_TDIC extends EntitySearchCriteria<GLSchoolCode_TDIC> {
  var _schoolName : String as SchoolName
  var _schoolCode : String as SchoolCode

  override protected property get MinimumSearchCriteriaMessage() : String {
    return null
  }

  override protected property get InvalidSearchCriteriaMessage() : String {
    return null
  }

  override protected function doSearch() : IQueryBeanResult {
    return constructSearchQuery_TDIC().select()
  }

  /*
   * create by: SureshB
   * @description: method to create the search query based on the user input values
   * @create time: 2:19 PM 9/23/2019
    * @param null
   * @return:
   */

  function constructSearchQuery_TDIC() : Query {
    var query = Query.make(GLSchoolCode_TDIC)

    var codeRestrict = ((SchoolCode != null) || (SchoolName != null))
        ? applyCodeRestrictBlock_TDIC() : null
    if (codeRestrict != null) {
      codeRestrict(query)
    }
    return query
  }

/*
 * create by: SureshB
 * @description: method to add the user entered values to the search filters.
 * @create time: 2:18 PM 9/23/2019
  * @param IQueryBuilder
 * @return:
 */

  private function applyCodeRestrictBlock_TDIC() : block(qb : IQueryBuilder) {
    return (\qb : IQueryBuilder -> {
      if (SchoolCode != null) {
        addStartsWithSchoolCodeRestrict_TDIC(qb)
      }
      if (SchoolName != null) {
        addStartsWithSchoolNameRestrict_TDIC(qb)
      }
    })
  }
/*
 * create by: SureshB
 * @description: Method to Add StartsWith filter to the SchoolCode field
 * @create time: 2:16 PM 9/23/2019
  * @param IQueryBuilder
 * @return:
 */

  private function addStartsWithSchoolCodeRestrict_TDIC(qb : IQueryBuilder) {
    addStartsWithRestrict_TDIC("SchoolCode", SchoolCode, qb)
  }

  /*
   * create by: SureshB
   * @description: Method to Add StartsWith filter to the SchoolName field
   * @create time: 2:16 PM 9/23/2019
    * @param IQueryBuilder
   * @return:
   */

  private function addStartsWithSchoolNameRestrict_TDIC(qb : IQueryBuilder) {
    addStartsWithRestrict_TDIC("SchoolName", SchoolName, qb)
  }
  /*
   * create by: SureshB
   * @description: Adds a "starts With" restriction value to the query for the named String attribute.
   * @create time: 2:14 PM 9/23/2019
    * @param Field Name , Field Value and IQueryBuilder
   * @return:
   */

  protected final function addStartsWithRestrict_TDIC(attrName : String, value : String, qb : IQueryBuilder) {
    qb.startsWith(attrName, value, true)
  }
}

