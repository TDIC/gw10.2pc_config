package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 01/25/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_TDIC1160CX_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.Job typeis Cancellation and (context.Period.Job.CancelReasonCode == ReasonCode.TC_NONPAYMENT or
      context.Period.Job.CancelReasonCode == ReasonCode.TC_NOTTAKEN) and context.Period.BasedOn.Active) {
        return true
      }
    }
    return false
  }
}