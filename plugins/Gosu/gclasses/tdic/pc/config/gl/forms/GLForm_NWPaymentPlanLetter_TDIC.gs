package tdic.pc.config.gl.forms

/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_NWPaymentPlanLetter_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
      if ((context.Period.Job typeis Renewal) && context.Period.BaseState == Jurisdiction.TC_AZ) {
        return (context.Period?.isDIMSPolicy_TDIC)
      }
      if((context.Period.Job typeis Renewal )&& (context.Period.BaseState == Jurisdiction.TC_WA || context.Period.BaseState == Jurisdiction.TC_ID
          || context.Period.BaseState == Jurisdiction.TC_MT || context.Period.BaseState == Jurisdiction.TC_TN
          || context.Period.BaseState == Jurisdiction.TC_OR)) {
        return true
      }
    return false
  }
}