package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 04/09/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_TDIC1160AS_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
      var priorTermExists = (context.Period.TermNumber - 1) > 0
      if(context.Period.Job typeis Cancellation){
        return (context.Period.Cancellation.CancelReasonCode == ReasonCode.TC_NONPAYMENT or
            (context.Period.Cancellation.CancelReasonCode == ReasonCode.TC_NOTTAKEN and priorTermExists))
      }
    }
    return false
  }
}