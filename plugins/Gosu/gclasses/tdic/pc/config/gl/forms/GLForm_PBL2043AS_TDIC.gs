package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2043AS_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.GLLine?.BasedOn != null and !(context.Period.Job typeis Renewal)) {
        if (context.Period.GLLine?.GLIDTheftREcoveryCov_TDICExists and
            context.Period.GLLine?.GLIDTheftREcoveryCov_TDIC?.GLTRTypeTDICTerm?.Value
                == IdentityTheftRecType_TDIC.TC_FAMILY) {
          return context.Period.GLLine?.BasedOn?.GLIDTheftREcoveryCov_TDICExists ?
              context.Period.GLLine?.BasedOn?.GLIDTheftREcoveryCov_TDIC.GLTRTypeTDICTerm?.Value !=
                  context.Period.GLLine?.GLIDTheftREcoveryCov_TDIC?.GLTRTypeTDICTerm?.Value : true

        }
      } else if (context.Period.GLLine?.GLIDTheftREcoveryCov_TDICExists) {
        return context.Period.GLLine?.GLIDTheftREcoveryCov_TDIC?.GLTRTypeTDICTerm?.Value
            == IdentityTheftRecType_TDIC.TC_FAMILY
      }
    }
    return false
  }
}