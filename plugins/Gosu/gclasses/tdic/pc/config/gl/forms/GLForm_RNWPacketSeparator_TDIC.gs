package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/*Form is common for CP and GeneralLiability*/
class GLForm_RNWPacketSeparator_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if((context.Period.GLLineExists || context.Period.BOPLineExists) && !(context.Period.isDIMSPolicy_TDIC)){
      return true
    }
    return false
  }
}