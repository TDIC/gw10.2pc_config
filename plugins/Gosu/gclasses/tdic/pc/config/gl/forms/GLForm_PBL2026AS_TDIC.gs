package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2026AS_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.BaseState == Jurisdiction.TC_CA and
          (context.Period.Job typeis Submission or context.Period.Job typeis Renewal)) {
        return true
      }
      if (context.Period.BaseState != Jurisdiction.TC_CA) {
        if (context.Period.GLLine?.BasedOn != null and !(context.Period.Job typeis Renewal)) {
          return context.Period.GLLine?.GLMultiOwnerDPECov_TDICExists and
              !context.Period.GLLine?.BasedOn?.GLMultiOwnerDPECov_TDICExists
        } else {
          return context.Period.GLLine?.GLMultiOwnerDPECov_TDICExists
        }
      }
    }
    return false
  }
}