package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffRemove
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2514AS_TDIC extends AbstractMultipleCopiesForm<GLSpecialEventSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLSpecialEventSched_TDIC> {
    var period = context.Period
    var glSpecialEventSchedItems = period.GLLine?.GLSpecialEventSched_TDIC
    var specialEventSchedItems : ArrayList<GLSpecialEventSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and period.GLLine?.GLSpecialEventCov_TDICExists and
        glSpecialEventSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var specialEventItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLSpecialEventSched_TDIC)*.Bean
        var specialEventItemRemoved = changeList?.where(\changedItem -> (changedItem typeis DiffRemove) and
            changedItem.Bean typeis GLSpecialEventSched_TDIC)*.Bean
        if(glSpecialEventSchedItems.hasMatch(\item -> item.BasedOn == null)){
          specialEventSchedItems.add(glSpecialEventSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glSpecialEventSchedItems.hasMatch(\item -> specialEventItemsModified.contains(item))) {
          specialEventSchedItems.add(glSpecialEventSchedItems.firstWhere(\item -> specialEventItemsModified.contains(item)))
        }
        else if(specialEventItemRemoved.HasElements){
          specialEventSchedItems.add(glSpecialEventSchedItems.first())
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          specialEventSchedItems.add(glSpecialEventSchedItems.first())
        }
      }
      else {
        specialEventSchedItems.add(glSpecialEventSchedItems.first())
      }
      return specialEventSchedItems.HasElements? specialEventSchedItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLSpecialEventSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("EventName", _entity.EventName))
    contentNode.addChild(createTextNode("EventStartDate", _entity.EventStartDate as String))
    contentNode.addChild(createTextNode("EventEndDate", _entity.EventEndDate as String))
    contentNode.addChild(createTextNode("Address", _entity.Address))
    contentNode.addChild(createTextNode("Onsite", _entity.Onsite as String))
    contentNode.addChild(createTextNode("ManagerLessorString", _entity.ManagerLessorString))
    contentNode.addChild(createTextNode("EquipmentLessorString", _entity.EquipmentLessorString))
    contentNode.addChild(createTextNode("GovtAgencyString", _entity.GovtAgencyString))
    if (_entity.ManagerLessor.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.ManagerLessor.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.ManagerLessor.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.ManagerLessor.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.ManagerLessor.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.ManagerLessor.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.ManagerLessor.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.ManagerLessor.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.ManagerLessor.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.ManagerLessor.PolicyAddlInsured.TaxID))
    }
    if (_entity.EquipmentLessor.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.EquipmentLessor.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.EquipmentLessor.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.EquipmentLessor.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.EquipmentLessor.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.EquipmentLessor.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.EquipmentLessor.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.EquipmentLessor.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.EquipmentLessor.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.EquipmentLessor.PolicyAddlInsured.TaxID))
    }
    if (_entity.GovtAgency.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.GovtAgency.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.GovtAgency.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.GovtAgency.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.GovtAgency.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.GovtAgency.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.GovtAgency.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.GovtAgency.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.GovtAgency.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.GovtAgency.PolicyAddlInsured.TaxID))
    }

  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}