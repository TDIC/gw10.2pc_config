package tdic.pc.config.gl.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2020BAS_TDIC extends AbstractMultipleCopiesForm<GLDentalBLAISched_TDIC> {
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLDentalBLAISched_TDIC> {
    var period = context.Period
    var AddInsd : ArrayList<GLDentalBLAISched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var GLAddInsureds = context.Period.GLLine?.GLDentalBLAISched_TDIC
      if(period.GLLine?.GLBLAICov_TDICExists and GLAddInsureds.HasElements){
        var GLAddInsuredAddedOnPolicyChange = GLAddInsureds.where(\addInsured -> addInsured.BasedOn == null)
        if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
          var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
          var addlInsuredModifiedOnPolicyChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis GLDentalBLAISched_TDIC)*.Bean
          var policyAddlInsuredModifiedOnPolicyChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsured)*.Bean
          var mansuScriptItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis GLManuscript_TDIC)*.Bean
          var policyAddlInsuredDetaModifiedOnPolicyChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsuredDetail)*.Bean
          if(GLAddInsuredAddedOnPolicyChange.HasElements){
            AddInsd.add(GLAddInsuredAddedOnPolicyChange.first())
          }
          else if(GLAddInsureds.hasMatch(\ai -> addlInsuredModifiedOnPolicyChange.contains(ai))){
            AddInsd.add(GLAddInsureds?.firstWhere(\ai -> addlInsuredModifiedOnPolicyChange.contains(ai)))
          }
          /*GWPS-1766 Logic to Compare POlicy Additional insured modified on policy change and GLBAI Policy Additional insured.*/
          if(policyAddlInsuredModifiedOnPolicyChange.HasElements){
            policyAddlInsuredModifiedOnPolicyChange.each(\policyaddInsured -> {
              GLAddInsureds.each(\GLAddInsured -> {
                if(GLAddInsured.AdditionalInsured.PolicyAddlInsured.equals(policyaddInsured as PolicyAddlInsured ) && AddInsd.Count==0){
                  AddInsd.add(GLAddInsured)
                }
              })
            })
          }
          if(policyAddlInsuredDetaModifiedOnPolicyChange.HasElements){
            policyAddlInsuredDetaModifiedOnPolicyChange.each(\policyaddInsuredDetails -> {
              GLAddInsureds.each(\GLAddInsured -> {
                if(GLAddInsured.AdditionalInsured.equals(policyaddInsuredDetails as PolicyAddlInsuredDetail ) && AddInsd.Count==0){
                  AddInsd.add(GLAddInsured)
                }
              })
            })
          }
          if(mansuScriptItemsModified.HasElements){
            var GLBLAddlInsuredManuscriptModified=(mansuScriptItemsModified.firstWhere(\elt -> (elt as GLManuscript_TDIC).PolicyAddInsured_TDIC!=null)) as GLManuscript_TDIC
            if(GLBLAddlInsuredManuscriptModified!=null && AddInsd.Count==0){
              AddInsd.add(GLBLAddlInsuredManuscriptModified.GeneralLiabilityLine.GLDentalBLAISched_TDIC.first())
            }
          }
        }
        else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
          if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
            AddInsd.add(GLAddInsureds.first())
          }
        }
        else {
          AddInsd.add(GLAddInsureds.first())
        }
      }
      return AddInsd.HasElements? AddInsd.toList() : {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLDentalBLAISched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber as String))
    if (_entity.AdditionalInsured.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.AdditionalInsured.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.AdditionalInsured.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.AdditionalInsured.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.AdditionalInsured.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.AdditionalInsured.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.AdditionalInsured.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.AdditionalInsured.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.AdditionalInsured.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.AdditionalInsured.PolicyAddlInsured.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}