package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OfferingCheck_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var pLCMFormPatterns = {"PBL1111AS", "PBL2015AZ", "PBL2122AS", "PBL2122IL", "PBL2210AK", "PBL2210NV", "PBL2211AK",
        "PBL2214NJ", "PBL2215MN", "PBL2500AK", "PBL2500AZ", "PBL2500CA", "PBL2500HI", "PBL2500IL", "PBL2500MN",
        "PBL2500NJ", "PBL2500NV", "PBL2500PA", "PBL2501MN", "PBL2503MN", "PBL2504IL", "PBL2506IL", "PBL2508IL",
        "PBL2513AS", "PBL2522AK", "PBL2027(A)AS", "PBL2027(B)AS", "PBL2500ND","PBL2500OR","PBL2210OR", "PBL2500WA",
        "PBL2550AS", "PBL2551AS", "PBL2555AS", "PBL2500ID", "PBL2500MT", "PBL2500TN", "PBL2210MT", "PBL2045AK"}
    var pLOCFormPatterns = {"OCC1111AS", "OCC2122AS", "OCC2500PA", "PLFCLTY", "PLGRAD", "PLPT16", "PLPT20", "PLTEMPDI"}
    var pLCYBFormPattern = {"CYB1205AS", "CYB2210IL", "CYB2210MN", "CYB2210ND", "CYB2210PA", "CYB2215MN", "CYB2500AZ",
        "CYB2500IL", "CYB2500MN", "CYB2500ND",
        "CYB2500NJ", "CYB2500NV", "CYB2500PA", "CYB2506ND","CYB2500OR", "CYB2500WA", "CYB2500ID", "CYB2500TN",
        "CYB2210OR", "CYB2210WA", "CYB2505OR"}
    if (pLCMFormPatterns.contains(this.Pattern.Code)) {
      return context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
    }
    if (pLOCFormPatterns.contains(this.Pattern.Code)) {
      return context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC"
    }
    if (pLCYBFormPattern.contains(this.Pattern.Code)) {
      return context.Period.Offering.CodeIdentifier == "PLCyberLiab_TDIC"
    }
    return false
  }
}