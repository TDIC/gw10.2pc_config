package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_TRIARNAS_TDIC extends AbstractSimpleAvailabilityForm{
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var period = context.Period
    if ((period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" or period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC")
        &&!(period.isDIMSPolicy_TDIC))
    {
      return (period.BaseState == Jurisdiction.TC_AK)? false: true
    }
    else if((period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or period.Offering.CodeIdentifier =="PLOccurence_TDIC")
          &&!(period.isDIMSPolicy_TDIC)){
      return true
    }
    return false
  }
}