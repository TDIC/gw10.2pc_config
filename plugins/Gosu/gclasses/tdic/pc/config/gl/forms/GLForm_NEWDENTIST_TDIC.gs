package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses typekey.Job

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 4/8/2020
 * Create Time: 5:33 PM
 * To change this template use File | Settings | File Templates.
 */
 class GLForm_NEWDENTIST_TDIC extends AbstractSimpleAvailabilityForm {
    override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
    var newDentistDiscPrevTerm = context.Period.BasedOn?.GLLine.GLNewDentistDiscount_TDICExists && context.Period.BasedOn.TermNumber == 1
    if(newDentistDiscPrevTerm and context.Period.isNewDentistFormInferred_TDIC == false){
      context.Period.isNewDentistFormInferred_TDIC = true
      return true
    }
  }
  return false
  }
 }

