package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 01/25/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_EREREOFFER_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.Job typeis PolicyChange and context.Period.Job.ChangeReasons.
          hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)
          and (context.Period.CancellationDate != null and context.Period.RefundCalcMethod != CalculationMethod.TC_FLAT)) {
        return true
      }
    }
    return false
  }
}