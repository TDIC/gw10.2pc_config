package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OCC2150AS_TDIC extends AbstractMultipleCopiesForm<GLMobileDCSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLMobileDCSched_TDIC> {
    var period = context.Period
    var glMobileDCSchedItems = period.GLLine?.GLMobileDCSched_TDIC
    var mobileDCExclItems : ArrayList<GLMobileDCSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLOccurence_TDIC" and period.GLLine?.GLMobileDentalClinicCov_TDICExists and
        glMobileDCSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var mobileDCSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLMobileDCSched_TDIC)*.Bean
        if(glMobileDCSchedItems.hasMatch(\item -> item.BasedOn == null)){
          mobileDCExclItems.add(glMobileDCSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glMobileDCSchedItems.hasMatch(\item -> mobileDCSchedLineItemsModified.contains(item))) {
          mobileDCExclItems.add(glMobileDCSchedItems.firstWhere(\item -> mobileDCSchedLineItemsModified.contains(item)))
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          mobileDCExclItems.add(glMobileDCSchedItems.first())
        }
      }
      else {
        mobileDCExclItems.add(glMobileDCSchedItems.first())
      }
      return mobileDCExclItems.HasElements? mobileDCExclItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLMobileDCSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    if (_entity.AdditionalInsured.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.AdditionalInsured.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.AdditionalInsured.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.AdditionalInsured.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.AdditionalInsured.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.AdditionalInsured.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.AdditionalInsured.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.AdditionalInsured.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.AdditionalInsured.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.AdditionalInsured.PolicyAddlInsured.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}