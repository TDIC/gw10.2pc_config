package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2511AS_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.GLLine?.BasedOn != null and !(context.Period.Job typeis Renewal)) {
        return context.Period.GLLine?.GLExposuresWM?.hasMatch(\glExposure ->
            glExposure?.ClassCode_TDIC == ClassCode_TDIC.TC_01) and
            !context.Period.GLLine?.BasedOn?.GLExposuresWM?.hasMatch(\glExposure ->
                glExposure?.ClassCode_TDIC == ClassCode_TDIC.TC_01)
      } else {
        return context.Period.GLLine?.GLExposuresWM?.hasMatch(\glExposure ->
            glExposure?.ClassCode_TDIC == ClassCode_TDIC.TC_01)
      }
    }
    return false
  }
}