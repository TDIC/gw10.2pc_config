package tdic.pc.config.job.renewal

uses java.lang.Integer
uses java.util.HashMap
uses gw.util.Pair
uses java.util.ArrayList
uses org.slf4j.LoggerFactory

class CopyAuditToRenewal {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${CopyAuditToRenewal.Type.RelativeName} - "

  protected var _policyPeriod : PolicyPeriod;
  protected var _classCodeMap : HashMap<WC7ClassCode,WC7ClassCode>;

  public construct (policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod;
    _classCodeMap = new HashMap<WC7ClassCode,WC7ClassCode>();
  }

  /**
   *  Determine if a copy is required, and if so, copy the payroll and number of employees from the
   *  audit to the renewal.
   */
  public function copyAuditToRenewalIfNeeded() {
    var lastAudit = getLastAudit(_policyPeriod);
    var previousRenewal = getPreviousRenewal(_policyPeriod);

    if (previousRenewal == null) {
      _logger.debug (_LOG_TAG + "There is no previous renewal, so there is no previous final audit, so no copying is required.");
    } else if (didPayrollOrEmployeesChange(previousRenewal.LatestPeriod, _policyPeriod)) {
      _logger.debug (_LOG_TAG + "Data changed during previous term, so no copying is required.");
    } else if (lastAudit == null) {
      _logger.debug (_LOG_TAG + "Unable to find a final audit to copy, so no copying is required.");
    } else if (lastAudit.LatestPeriod.PeriodEnd != previousRenewal.LatestPeriod.PeriodStart) {
      _logger.debug (_LOG_TAG + "Last final audit was for an earlier term, no copying required.");
    } else if (locationsChanged(lastAudit, _policyPeriod) == true) {
      _logger.debug (_LOG_TAG + "Audit and renewal have different locations, skipping copy of audit to renewal.");
    } else {
      copyAuditToRenewal (lastAudit, _policyPeriod);
    }
  }

  /**
   * Find the previous renewal on the policy.
  */
  protected function getPreviousRenewal(policyPeriod : PolicyPeriod) : Renewal {
    var previousPeriod = policyPeriod.BasedOn;
    while (previousPeriod != null and previousPeriod.Renewal == null) {
      previousPeriod = previousPeriod.BasedOn;
    }
    return previousPeriod.Renewal;
  }

  /**
   *  Computes the total payroll and number of employees on a per location, per class code basis and compares
   *  the start period to the end period.
   *
   */
  protected function didPayrollOrEmployeesChange (startPeriod : PolicyPeriod, endPeriod : PolicyPeriod) : boolean {
    var retval = false;
    var startValue : Pair<Integer,Integer>;
    var endValue : Pair<Integer,Integer>;

    var startSummary = getPayrollEmployeeSummary(startPeriod);
    var endSummary   = getPayrollEmployeeSummary(endPeriod);

    if (_logger.TraceEnabled) {
      logSummaryData (startPeriod, startSummary);
      logSummaryData (endPeriod,   endSummary);
    }

    if (startSummary.Count != endSummary.Count) {
      retval = true;
    } else {
      for (key in startSummary.Keys) {
        startValue = startSummary.get(key);
        endValue   = endSummary.get(key);
        if (startValue != endValue) {
          retval = true;
          break;
        }
        endSummary.remove(key);
      }
      if (endSummary.Count != 0) {
        retval = true;
      }
    }

    _logger.debug (_LOG_TAG + "didPayrollOrEmployeeChange - retval = " + (retval ? "TRUE" : "FALSE"));

    return retval;
  }

  /**
   *  Logs the summary data.
   */
  protected function logSummaryData (policyPeriod : PolicyPeriod,
                                     summary : HashMap<Pair<AccountLocation,String>,Pair<Integer,Integer>>) : void {
    var value : Pair<Integer,Integer>;

    var message = "Summary data for Policy " + policyPeriod.PolicyNumber
                      + ", effective " + policyPeriod.PeriodStart.formatDate(SHORT)
                      + " - " + policyPeriod.PeriodEnd.formatDate(SHORT);

    for (key in summary.Keys.orderBy(\k -> k.First.ID).thenBy(\k -> k.Second)) {
      value = summary.get(key);
      message += "\n\t" + key.First + ", Class Code " + key.Second
                  + " - payroll = " + value.First + ", " + value.Second + " employee(s)";
    }
    _logger.trace (_LOG_TAG + message);
  }

  /**
   *  Computes the total payroll and number of employees on a per location, per class code basis.
   */
  protected function getPayrollEmployeeSummary (policyPeriod : PolicyPeriod) : HashMap<Pair<AccountLocation,String>,Pair<Integer,Integer>> {
    var summary = new HashMap<Pair<AccountLocation,String>,Pair<Integer,Integer>>();
    var key : Pair<AccountLocation,String>;
    var numEmployees : Integer;
    var value : Pair<Integer,Integer>;
    var wc7Line = policyPeriod.WC7Line;

    for (jurisdiction in wc7Line.WC7Jurisdictions) {
      for (covEmp in wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (deleteCovEmp(covEmp) == false) {
          numEmployees = covEmp.NumEmployees;
          if (numEmployees == null) {
            numEmployees = 0;
          }

          key = new Pair<AccountLocation,String> (covEmp.LocationWM.AccountLocation, covEmp.ClassCode.Code);
          value = summary.get(key);
          if (value == null) {
            value = new Pair<Integer,Integer> (covEmp.BasisAmount, numEmployees);
          } else {
            value = new Pair<Integer,Integer> (value.First + covEmp.BasisAmount, value.Second + numEmployees);
          }
          summary.put(key,value);
        }
      }
    }

    return summary;
  }

  /**
   *  Get the last audit on the policy.
   */
  protected function getLastAudit(policyPeriod : PolicyPeriod) : Audit {
    var auditInformations = policyPeriod.Policy.AllAuditInformations
                                .where(\ai -> ai.ActualAuditMethod != AuditMethod.TC_ESTIMATED
                                          and ai.IsComplete == true
                                          and ai.IsFinalAudit == true
                                          and ai.HasBeenReversed == false
                                          and ai.IsReversal == false
                                          and ai.RevisingAudit == null);

    return auditInformations.sortByDescending(\ai -> ai.AuditPeriodEndDate).first().Audit;
  }

  /**
   *  Check if the locations match between the audit and the renewal.
   */
  protected function locationsChanged (lastAudit : Audit, policyPeriod : PolicyPeriod) : boolean {
    var accountLocation : AccountLocation;
    var auditLocations = new ArrayList<AccountLocation>();
    var renewalLocations = new ArrayList<AccountLocation>();
    var wc7Line = lastAudit.LatestPeriod.WC7Line;

    // Get all the locations used on the audit.
    for (jurisdiction in wc7Line.WC7Jurisdictions) {
      for (covEmp in wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (deleteCovEmp(covEmp) == false) {
          accountLocation = covEmp.LocationWM.AccountLocation;
          if (auditLocations.contains(accountLocation) == false) {
            auditLocations.add(accountLocation);
          }
        }
      }
    }

    // Get all the locations available to the renewal.
    for (location in policyPeriod.PolicyLocations) {
      accountLocation = location.AccountLocation;
      if (renewalLocations.contains(accountLocation) == false) {
        renewalLocations.add(accountLocation);
      }
    }

    return (auditLocations.disjunction(renewalLocations).Count != 0);
  }

  /**
   *  Copy the audit data to the renewal.
   */
  protected function copyAuditToRenewal (lastAudit : Audit, policyPeriod : PolicyPeriod) : void {
    var srcWC7Line = lastAudit.LatestPeriod.WC7Line;
    var destWC7Line = policyPeriod.WC7Line;
    var unprocessedJurisdictions = new ArrayList<WC7Jurisdiction>();
    var destJurisdiction : WC7Jurisdiction;
    var unprocessedSrcCovEmps : ArrayList<WC7CoveredEmployee>;
    var unprocessedDestCovEmps : ArrayList<WC7CoveredEmployee>;
    var processedCovEmps : ArrayList<WC7CoveredEmployee>;
    var destCovEmp : WC7CoveredEmployee;
    var deleteCovEmp : boolean;

    _logger.info (_LOG_TAG + "Copying payroll and number of employees from Audit " + lastAudit.JobNumber
                                + " to Renewal " + policyPeriod.Job.JobNumber);

    unprocessedJurisdictions.addAll (destWC7Line.WC7Jurisdictions.toList());

    for (srcJurisdiction in srcWC7Line.WC7Jurisdictions) {
      _logger.info (_LOG_TAG + "Processing WC7Jurisdiction for " + srcJurisdiction.Jurisdiction);

      destJurisdiction = unprocessedJurisdictions.firstWhere(\j -> j.Jurisdiction == srcJurisdiction.Jurisdiction);
      if (destJurisdiction != null) {
        unprocessedJurisdictions.remove(destJurisdiction);
      } else {
        _logger.trace (_LOG_TAG + "Adding WC7Jurisdiction for " + srcJurisdiction.Jurisdiction);
        destJurisdiction = destWC7Line.addJurisdiction(srcJurisdiction.Jurisdiction);
      }

      unprocessedSrcCovEmps = new ArrayList<WC7CoveredEmployee>();
      unprocessedSrcCovEmps.addAll (srcWC7Line.getWC7CoveredEmployeesWM(destJurisdiction.Jurisdiction).toList());
      unprocessedDestCovEmps = new ArrayList<WC7CoveredEmployee>();
      unprocessedDestCovEmps.addAll (destWC7Line.getWC7CoveredEmployeesWM(destJurisdiction.Jurisdiction).toList());

      // Copy Covered Employees with a matching Fixed ID
      _logger.info (_LOG_TAG + "Copying WC7CoveredEmployees with matching FixedID's");
      processedCovEmps = new ArrayList<WC7CoveredEmployee>();
      for (srcCovEmp in unprocessedSrcCovEmps) {
        destCovEmp = unprocessedDestCovEmps.firstWhere(\ce -> ce.FixedId == srcCovEmp.FixedId);
        if (destCovEmp != null) {
          processedCovEmps.add(srcCovEmp);
          unprocessedDestCovEmps.remove (destCovEmp);
          copyCoveredEmployee (srcCovEmp, destCovEmp);
        }
      }
      unprocessedSrcCovEmps.removeAll(processedCovEmps);

      // Copy Covered Employees with matching Location/ClassCode
      _logger.info (_LOG_TAG + "Copying WC7CoveredEmployees with matching locations and class codes");
      processedCovEmps = new ArrayList<WC7CoveredEmployee>();
      for (srcCovEmp in unprocessedSrcCovEmps.orderBy(\ce -> ce.FixedId)) {
        destCovEmp = unprocessedDestCovEmps.firstWhere (\ce -> ce.LocationWM.AccountLocation == srcCovEmp.LocationWM.AccountLocation
                                                           and ce.ClassCode.Code == srcCovEmp.ClassCode.Code);
        if (destCovEmp != null) {
          processedCovEmps.add(srcCovEmp);
          unprocessedDestCovEmps.remove (destCovEmp);
          copyCoveredEmployee (srcCovEmp, destCovEmp);
        }
      }
      unprocessedSrcCovEmps.removeAll(processedCovEmps);

      // Create new Covered Employees
      for (srcCovEmp in unprocessedSrcCovEmps) {
        if (deleteCovEmp(srcCovEmp) == false) {
          _logger.trace (_LOG_TAG + "Creating new WC7CoveredEmployee");
          destCovEmp = destWC7Line.addCoveredEmployeeWM_TDIC(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(srcJurisdiction.Jurisdiction));
          copyCoveredEmployee (srcCovEmp, destCovEmp);
        } else {
          _logger.trace (_LOG_TAG + "WC7CoveredEmployee (fixedID = " + srcCovEmp.FixedId
                                  + ") has $0.00 payroll and no employees.  This covered employee will not be copied.");
        }
      }

      // Remove Covered Employees
      for (covEmp in unprocessedDestCovEmps) {
        _logger.trace (_LOG_TAG + "Removing WC7CoveredEmployee (fixedID = " + covEmp.FixedId + ")");
        covEmp.removeWM();
      }
    }

    for (jurisdiction in unprocessedJurisdictions) {
      _logger.trace (_LOG_TAG + "Removing WC7Jurisdiction for " + jurisdiction.Jurisdiction);
      destWC7Line.removeJurisdiction(jurisdiction);
    }
  }

  /**
   *  Copy one covered employee.
   */
  protected function copyCoveredEmployee (srcCovEmp : WC7CoveredEmployee, destCovEmp : WC7CoveredEmployee) : void {
    if (deleteCovEmp(srcCovEmp)) {
      _logger.trace (_LOG_TAG + "WC7CoveredEmployee (fixedID = " + srcCovEmp.FixedId
                                  + ") has $0.00 payroll and no employees.  Removing WC7CoveredEmployee (fixedID = "
                                  + destCovEmp.FixedId + ")");
      destCovEmp.removeWM();
    } else {
      _logger.trace (_LOG_TAG + "WC7CoveredEmployee (fixedID = " + destCovEmp.FixedId + ") - Replacing "
                                    + destCovEmp.BasisAmount + " payroll and " + destCovEmp.NumEmployees
                                    + " employee(s) with " + srcCovEmp.AuditedAmount + " payroll and "
                                    + srcCovEmp.AuditedNumEmployees_TDIC + " employees.");
      destCovEmp.LocationWM = destCovEmp.Branch.PolicyLocationsWM.firstWhere(\pl -> pl.AccountLocation == srcCovEmp.LocationWM.AccountLocation);
      destCovEmp.ClassCode = findClassCode (srcCovEmp.ClassCode);
      destCovEmp.BasisAmount = srcCovEmp.AuditedAmount;
      destCovEmp.NumEmployees = srcCovEmp.AuditedNumEmployees_TDIC;
    }
  }

  /**
   *  Check if covered employee represents a removed entity (all zero values).
   */
  protected function deleteCovEmp (covEmp : WC7CoveredEmployee) : boolean {
    var retval : boolean;
    var job = covEmp.Branch.Job;
    if (job typeis Audit) {
      retval = (covEmp.AuditedAmount == 0 and covEmp.AuditedNumEmployees_TDIC == 0);
    } else {
      retval = (covEmp.BasisAmount == 0 and covEmp.NumEmployees == 0);
    }
    return retval;
  }

  /**
   *  Find a matching class code.
   */
  protected function findClassCode(oldClassCode : WC7ClassCode) : WC7ClassCode {
    var newClassCode = _classCodeMap.get(oldClassCode);
    if (newClassCode == null) {
      newClassCode = _policyPeriod.WC7Line.transformWC7ClassCode(oldClassCode);
      _classCodeMap.put (oldClassCode,newClassCode);
    }
    return newClassCode;
  }
}