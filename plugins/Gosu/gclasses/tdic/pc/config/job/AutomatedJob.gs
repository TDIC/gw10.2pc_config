package tdic.pc.config.job

uses java.util.Date
uses gw.util.Pair
uses java.util.ArrayList
uses java.util.List
uses org.slf4j.LoggerFactory

abstract class AutomatedJob {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${AutomatedJob.Type.RelativeName} - "

  protected var _jobType : AutomatedJob_TDIC;
  protected var _policy : Policy;
  protected var _effectiveDate : Date;
  private var _errors : List<Pair<Date,String>> as readonly Errors;
  private var _warnings : List<Pair<Date,String>> as readonly Warnings;

  public construct (jobType : AutomatedJob_TDIC, policy : Policy, effectiveDate : Date) {
    _jobType = jobType;
    _policy = policy;
    _effectiveDate = effectiveDate;
  }

  final public property get HasErrors() : boolean {
    return _errors.HasElements;
  }

  final protected function logError (message : String) : void {
    _errors.add (new Pair<Date,String> (Date.Now, message));
    _logger.error (message);
  }

  final public property get HasWarnings() : boolean {
    return _warnings.HasElements;
  }

  final protected function logWarning (message : String) : void {
    _warnings.add (new Pair<Date,String> (Date.Now, message));
    _logger.warn (message);
  }

  abstract public property get Description() : String;

  public property get IsJobNeeded() : boolean {
    return true;
  }

  abstract public function processAutomatedJob() : Job;

  final protected function resetErrorsAndWarnings() : void {
    _errors = new ArrayList<Pair<Date,String>>();
    _warnings = new ArrayList<Pair<Date,String>>();
  }
}