package tdic.pc.config.job.policychange.automated

uses java.util.Date

uses tdic.pc.config.job.AutomatedJobTemplate
uses org.slf4j.LoggerFactory

/*
 * BrianS - Abstract class to automatically process a policy change.
 */
abstract class AutomatedPolicyChange extends AutomatedJobTemplate<PolicyChange> {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${AutomatedPolicyChange.Type.RelativeName} - "

  public construct (changeType : AutomatedJob_TDIC, policy : Policy, effectiveDate : Date) {
    super(changeType, policy, effectiveDate);
  }

  final override public property get IsJobNeeded() : boolean {
    var policyPeriod = Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(_policy, _effectiveDate);
    return isPolicyChangeNeededImpl(policyPeriod);
  }

  /**
   * Function to determine if a policy change is needed.
  */
  abstract protected function isPolicyChangeNeededImpl (policyPeriod : PolicyPeriod) : boolean;

  final override protected function canStartJob() : String {
    return _policy.canStartPolicyChange(_effectiveDate);
  }

  /**
   * Create the policy change entity.
   */
  override protected function createJob() : PolicyChange {
    var policyChange = new PolicyChange(_policy.Bundle);
    return policyChange;
  }

  final override protected function startJob () : void {
    Job.startJob (_policy, _effectiveDate);
  }
}