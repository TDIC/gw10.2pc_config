package tdic.pc.config.job.helper

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey

/**
 * BrittoS 04/03/2020
 * Helper Class for various JobProcess implementations
 */
class JobProcessHelper {

  public static function createActivityForReview(branch : PolicyPeriod) {
    if(branch.Job typeis Submission) {
      createUWActivityForNewBusinessReview(branch)    //New Submission
    } else if(branch.Job typeis Renewal and not branch.isLegacyConversion) {
      createUWActivityForRenewalReview(branch)        //Renewal
    } else if(branch.Job typeis PolicyChange) {
      createUWActivityForPolicyChangeReview(branch)   //Policy Change
    }
  }

  /**
   * Create an activity and assign to Underwriter's Queue - applicable for any of the external brocker's policy Submission
   * @param branch
   */
  public static function createUWActivityForNewBusinessReview(branch : PolicyPeriod) {
    var uwReviewActivityPattern = ActivityPattern.finder.getActivityPatternByCode("approve_submission")

    var uwActivity = uwReviewActivityPattern.createJobActivity(branch.Job.Bundle, branch.Job,
                  DisplayKey.get("Job.Review.Activity.Subject", branch.Job.Subtype.Description),
                  DisplayKey.get("Job.Review.Activity.Description"), null, null, null, null, null)
    // New Business Group = tdic-sb:202
    // New Business Queue = tdic-sb:101 <New Business>
    var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:202").select().AtMostOneRow
    if(group != null) {
      var queue = group.getQueue("New Business")
      if(queue != null) {
        uwActivity.assignActivityToQueue(queue, group)
      }
    }
  }

  /**
   *
   * @param branch
   */
  public static function createUWActivityForRenewalReview(branch : PolicyPeriod) {
    var uwReviewActivityPattern = ActivityPattern.finder.getActivityPatternByCode("review_renewal")
    var uwActivity = branch.Job.createRoleActivity(TC_UNDERWRITER, uwReviewActivityPattern,
        DisplayKey.get("Job.Review.Activity.Subject", branch.Job.Subtype.Description),
        DisplayKey.get("Job.Review.Activity.Description"))

    /*var uwActivity = uwReviewActivityPattern.createJobActivity(branch.Job.Bundle, branch.Job,
        DisplayKey.get("Job.Review.Activity.Subject", branch.Job.Subtype.Description),
        DisplayKey.get("Job.Review.Activity.Description"), null, null, null, null, null)*/

    // Renewal Group = tdic-sb:203
    // Renewal Queue = tdic-sb:102 <Renewal>
    var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:203").select().AtMostOneRow
    if(group != null) {
      var queue = group.getQueue("Renewal")
      if(queue != null) {
        uwActivity.assignActivityToQueue(queue, group)
      }
    }
  }

  /**
   *
   * @param branch
   */
  public static function createUWActivityForPolicyChangeReview(branch : PolicyPeriod) {

    var uwReviewActivityPattern = ActivityPattern.finder.getActivityPatternByCode("approve_policy_change")
    var uwActivity = branch.Job.createRoleActivity(TC_UNDERWRITER, uwReviewActivityPattern,
        DisplayKey.get("Job.Review.Activity.Subject", branch.Job.Subtype.Description),
        DisplayKey.get("Job.Review.Activity.Description"))

   /* var uwActivity = uwReviewActivityPattern.createJobActivity(branch.Job.Bundle, branch.Job,
        DisplayKey.get("Job.Review.Activity.Subject", branch.Job.Subtype.Description),
        DisplayKey.get("Job.Review.Activity.Description"), null, null, null, null, null)*/

    // PolicyChange Group = tdic-sb:206
    // PolicyChange Queue = tdic-sb:105 <Policy Change>
    var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:206").select().AtMostOneRow
    if(group != null) {
      var queue = group.getQueue("Policy Change")
      if(queue != null) {
        uwActivity.assignActivityToQueue(queue, group)
      }
    }
  }
}