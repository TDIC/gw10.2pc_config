package tdic.pc.config.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses org.slf4j.LoggerFactory

/**
 * BrittoS - Class that can be used to remove all data from a table.
 */
class ClearTable extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${ClearTable.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber
  private var _tableName : String as readonly TableName

  construct(versionNumber : int, tableName : String) {
    super(versionNumber)
    _versionNumber = versionNumber
    _tableName = tableName
  }

  override public property get Description() : String {
    return "Remove data from " + _tableName
  }

  override function hasVersionCheck() : boolean {
    return false
  }

  override function execute() : void {
    var table = getTable(_tableName)

    // Step 1:  Set column to null.
    _logger.info (_LOG_TAG + "Clearing data from " + _tableName)
    var builder = table.delete()
    builder.execute()
    _logger.info (_LOG_TAG + "Completed " + Description)
  }
}