package tdic.pc.config.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses org.slf4j.LoggerFactory

/**
 * BrittoS - Class that can be used to remove table columns.
 */
class RemoveObsoleteColumn extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${RemoveObsoleteColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber
  private var _tableName : String as readonly TableName
  private var _columnName : String as readonly ColumnName

  construct(versionNumber : int, tableName : String, columnName : String) {
    super(versionNumber)
    _versionNumber = versionNumber
    _tableName = tableName
    _columnName = columnName
  }

  override property get Description(): String {
    return "Remove Obsolete Column " + _tableName + "." + _columnName
  }

  override function hasVersionCheck(): boolean {
    return false
  }

  override function execute() {

    var table = getTable(_tableName)
    var column = table.getColumn(_columnName)

    // Step 1:  Delete column.
    _logger.info (_LOG_TAG + "Removing column " + _tableName + "." + _columnName)
    column.drop()

    _logger.info (_LOG_TAG + "Completed " + Description)
  }

}