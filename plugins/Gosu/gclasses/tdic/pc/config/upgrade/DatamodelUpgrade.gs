package tdic.pc.config.upgrade

uses gw.api.util.DateUtil
uses gw.plugin.upgrade.IDatamodelUpgrade
uses gw.api.datamodel.upgrade.IDatamodelChange
uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.database.upgrade.after.AfterUpgradeVersionTrigger

uses java.util.ArrayList

uses gw.api.database.upgrade.DatamodelChangeWithoutArchivedDocumentChange

uses java.util.List

uses org.slf4j.LoggerFactory

class DatamodelUpgrade implements IDatamodelUpgrade {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${DatamodelUpgrade.Type.RelativeName} - "

  override property get MajorVersion() : int {
    // BrianS - TDIC's major version is currently 0.  Not sure if this can be changed.
    return 0
  }

  override property get BeforeUpgradeDatamodelChanges() : List<IDatamodelChange<BeforeUpgradeVersionTrigger>> {
    _logger.info(_LOG_TAG + "BeforeUpgradeDataModelChanges")
    var list = new ArrayList<IDatamodelChange<BeforeUpgradeVersionTrigger>>()

    // BrianS - ChangedEntity_TDIC.IsChangedToRead was modified from a datatype of shorttext to longtext.
    //          PolicyCenter fails to auto upgrade the column.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new UpgradeColumnUsingTemp(83, "pcx_changedentity_tdic", "IsChangedToRead")))

    // BrianS - WC7WaiverOfSubro.Description was modified from a datatype of shorttext to longtext.
    //          PolicyCenter fails to auto upgrade the column.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new UpgradeColumnUsingTemp(83, "pcx_wc7waiverofsubro", "Description")))

    // Kesava T - pcx_earnedpremiumreport_tdic.Amount was modified from money(18,2) to money(18,6)
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new UpgradeColumnUsingTemp(88, "pcx_earnedpremiumreport_tdic", "Amount")))

    // BrianS - DBA was moved from PolicyLocation.LocationNameInternal_TDIC to DBA_TDIC.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RenameColumn(95, "pc_policylocation", "LocationNameInternal_TDIC", "DBA_TDIC")))
    // BrianS - Clear AccountLocation.LocationName since it is no longer account synced.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearColumn(95, "pc_address", "LocationName")))

    // BrianS - Move pc_job.AutomatedEndorsement_TDIC to AutomatedJob_TDIC
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ChangeTypekeyColumn(107, "pc_job", "AutomatedEndorsement_TDIC", "AutomatedEndorsement_TDIC", "AutomatedJob_TDIC", "AutomatedJob_TDIC")))

    // Kesava T - Renamed column name from Insured to Insurer.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RenameColumn(110, "pcx_wc7exclalternatecov_tdic", "Insured", "Insurer")))

    // Kesava T - Remove obsolete columns - JIRA GW-3012
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(114, "pcx_wc7desopexclop_tdic", "StandardClassificationNumber")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(114, "pcx_wc7desopexclop_tdic", "AbbreviatedPhraseology")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(114, "pcx_wc7desopdeslocexclop_tdic", "StandardClassificationNumber")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(114, "pcx_wc7desopdeslocexclop_tdic", "AbbreviatedPhraseology")))

    // Kesava T - Remove obsolete columns - JIRA GW-3261
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(115, "pcx_wc7exclalternatecov_tdic", "Insurer")))

    //WC v10 upgrade - removing SBT - 07/01/2019
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7diseasecode", "companionclasscode")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm10")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm10avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm11")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm11avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm12")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm12avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm13")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm13avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm14")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm14avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm15")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm15avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm16")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm16avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm17")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm17avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm18")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm18avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm19")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm19avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm20")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm20avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm21")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm21avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm7")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm7avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm8")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm8avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm9")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7jurisdictioncov", "choiceterm9avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7workerscompcov", "booleanterm1avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7workerscompcov", "booleanterm2avl")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7coveredemployee", "classcodesubclass")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7coveredemployee", "numberapparatus")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7suppldiseaseexp", "companionclasscode")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7suppldiseaseexp", "companioncoveredemployee")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7waiverofsubro", "numberofcontracts")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pc_auditinformation", "reasonforsurcharge")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "auditsurcharge")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "baseclasscode")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "ratedevcovempcosttype")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "ratedevfelacovempcosttype")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "ratedevwc7maricovempcosttype")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7aircraftseatratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7atomicenergyexposureratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7coveredemployeeratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7felacoveredemployeeratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7maritimecoveredemployeerd")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7nonratablecllimitcosttype")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7suppldiseaseexposureratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteColumn(118, "pcx_wc7cost", "wc7workerscompcovratedev")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_ratedeviatedcost")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7admiraltycov1classcode")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7athleticteamssubclass")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7auditsurcharge")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7felacov1classcode")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7felasplitlimit")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7maritimesplitlimit")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7ratingstateconfig")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7statutoryemployer")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7subcontractor")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7supplebenefitsfund")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RemoveObsoleteTable(118, "pcx_wc7terminationrecipient")))

    //Sanjana S - Renamed column name from Desciption to Description.
    //list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new RenameColumn(125, "pc_policyaddlinsureddetail", "Desciption", "Description")))

    /* 01/18/2020 Not required after upgrade - Britto 
    //WC v10 upgrade - removing ratebooks - 08/24/2019
    //Sequence - 1
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratebookcalcroutine")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_rtrefownership")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_defratefactors")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_rtownership")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratetable")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratebook")))
    //Sequence - 2
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratetablecolumn")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratetablematchop")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_ratetabledefinition")))
    //Sequence - 3
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcstepdefarg")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcstepdefoperand")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcstepdef")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcroutinedefinition")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcroutineparam")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcroutineparamset")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_defratefactors")))
    //Sequence - 4
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(118, "pc_calcroutinedefinition")))

    //temp fix for R2
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new ClearTable(123, "pc_customforminference")))
    */
    return list
  }

  override property get AfterUpgradeDatamodelChanges() : List<IDatamodelChange<AfterUpgradeVersionTrigger>> {
    _logger.info(_LOG_TAG + "AfterUpgradeDataModelChanges")
    var list = new ArrayList<IDatamodelChange<AfterUpgradeVersionTrigger>>()

    return list
  }
}