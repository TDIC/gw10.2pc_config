package tdic.pc.config.enhancements.lob.gl

enhancement TDIC_GLExtendedReportPeriodDPLCovEnhancement : GLExtendedReportPeriodDPLCov_TDIC {

  property get ExpiryDate_TDIC() : Date {
    if (this.HasGLERPDLExpDate_TDICTerm) {
      return this.GLERPDLExpDate_TDICTerm.Value
    }
    return this.Branch?.PeriodEnd
  }

}
