package tdic.pc.config.enhancements.lob.gl

enhancement TDIC_GLExcludedServiceCondEnhancement : GLExcludedServiceSched_TDIC {

  property get EffectiveDate_TDIC() : Date {
    if (this.LTEffecticeDate.afterOrEqual(this.Branch.PeriodStart)) {
      return this.LTEffecticeDate
    }
    return this.Branch?.PeriodStart
  }

}
