package tdic.pc.config.enhancements.lob.wc7

enhancement WC7WorkersCompExclEnhancement : entity.WC7WorkersCompExcl {
  /**
   * Creates and adds a new Alternate Coverage to this exclusion.
   *
   * @return the newly created Alternate Coverage
   */
  function createAndAddWC7AlternateCoverage () : WC7ExclAlternateCov_TDIC {
    var policy = new WC7ExclAlternateCov_TDIC(this.Branch);
    this.addToWC7AlternateCoverages_TDIC(policy);
    return policy;
  }
}
