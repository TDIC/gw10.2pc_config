package tdic.pc.config.enhancements.pcf
/**
 * US506
 * Shane Sheridan 11/21/2014
 *
 * This enhancement was created for configurations made to DiffPolicyPeriodsScreen.
 */
enhancement TDIC_PolicyPeriodDiffEnhancement : entity.PolicyPeriod {

  /**
   * US506
   * Shane Sheridan 11/21/2014
   *
   * If this is an Audit Job then this returns the AuditedBasis,
   * Otherwise it returns the estimated basis.
   */
  @Returns("Basis amount; either from policy or audit.")
  property get BasisAmountCheckJobType() : int{
    // if this is an audit
    if(this.Audit != null){
      return this.Audit.AuditInformation.AuditedBasisAmount
    }
    // else
    return this.BasisAmount
  }
}
