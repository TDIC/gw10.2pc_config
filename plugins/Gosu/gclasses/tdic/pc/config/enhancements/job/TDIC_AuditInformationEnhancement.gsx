package tdic.pc.config.enhancements.job

uses gw.api.database.Query
/**
 * US463
 * Shane Sheridan 11/7/2014
 *
 * Extension of the OOTB AuditInformationEnhancement for TDIC.
 */
enhancement TDIC_AuditInformationEnhancement : entity.AuditInformation {

  /**
   * US463
   * Shane Sheridan 11/10/2014
  */
  property get AvailableAuditMethods() : typekey.AuditMethod[] {
      return typekey.AuditMethod.TF_WC7FINALAUDIT_TDIC.TypeKeys.toTypedArray()
  }

  /**
   * US477
   * Shane Sheridan 11/18/2014
   */
  property get AuditedBasisAmount() : int{
    var basisAmount : int = 0;
    var wc7Line = this.Audit.PolicyPeriod.WC7Line;

    for (jurisdiction in wc7Line.WC7Jurisdictions) {
      for (covEmp in wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (covEmp.AuditedAmount != null) {
          basisAmount += covEmp.AuditedAmount;
        }
      }
    }

    return basisAmount;
  }

  /**
   * US959
   * Shane Sheridan 02/11/2015
   */
  function onReceivedDateFieldChange(){
    if(this.isFieldChanged("ReceivedDate") and this.AuditMethod == typekey.AuditMethod.TC_VOLUNTARY){

      if(this.getOriginalValue("ReceivedDate") != null){
        // cancel previously created Activity.
        this.Audit.cancelOpenActivities(ActivityPattern.finder.getActivityPatternByCode("new_audit_assigmnent"))
      }

      if(this.getFieldValue("ReceivedDate") != null){
        createActivityAndAssignUsingRules()
      }

    }
  }

  /**
   *   JIRA -3098
   *  Create activity "audit_awaiting_auditor_processing" and assign to Final Audit Queue when first time "Ready to Review Date Field changed in Final Audit Summery Screen
  */
  function onReadyToReviewDateFieldChange(){
    if(this.getOriginalValue("ReadyForReviewDate") == null and this.getFieldValue("ReadyForReviewDate")!=null){
      var k =this.getOriginalValue("ReadyForReviewDate")
      var c =this.getFieldValue("ReadyForReviewDate")
        var activityPattern = ActivityPattern.finder.getActivityPatternByCode("audit_awaiting_auditor_processing")
        var pp = this.Audit.PolicyPeriod
        if(activityPattern!= null){
          var activity = activityPattern.createJobActivity(pp.Job.Bundle,pp.Job,activityPattern.Subject,activityPattern.Description,null,null,null,null,null)
          var group = gw.api.database.Query.make(Group).compare("PublicID", Equals, "pc-tdic:3").select().AtMostOneRow
          var queue = group.AssignableQueues.firstWhere( \ q -> q.PublicID == "pc-tdic:2")
          activity.assignActivityToQueue(queue, group)
        }
      }
  }

  /**
   * US959
   * Shane Sheridan 02/10/2015
   *
   * Creates an Activity and assigns it using business rules (i.e. GlobalActivityAssignmentRules).
   */
  function createActivityAndAssignUsingRules(){
    if ( not (this.IsReversal or this.IsRevision) and this.Audit.PolicyPeriod.WC7LineExists){
      var patternCode = "new_audit_assigmnent"
      if(this.AuditMethod == typekey.AuditMethod.TC_PHYSICAL){
        patternCode = "audit_fee_service"
      }

      var pattern = ActivityPattern.finder.getActivityPatternByCode(patternCode)
      var subject = pattern.Subject
      var description = pattern.Description
      var activity = this.Audit.PolicyPeriod.Job.createGroupActivity_TDIC(pattern, subject, description)

      if(activity != null){
        activity.autoAssign()
      }
    }
  }
}
