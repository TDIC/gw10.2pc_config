package tdic.pc.config.enhancements.job
/**
 * US469
 * Shane Sheridan 01/19/2015
 *
 * Functionality for Job Activities for TDIC.
 */
enhancement TDIC_JobActivityEnhancement : Job {

  /**
   * US469
   * Shane Sheridan 01/19/2015
   *
   * Pass null for subject or description parameters to use values from the Activity Pattern.
  */
  function createGroupActivity_TDIC(pattern : ActivityPattern, subject : String, description : String) : Activity {
    // Most of these arguments are null since the ActivityPattern config will set their default values
    var activity = pattern.createJobActivity( this.Bundle, this, subject, description, null, null, null, null, null )
    return activity
  }
}
