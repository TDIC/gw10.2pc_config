package tdic.pc.config.enhancements.job.ere

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * BrittoS 05/15/2020 ERE QuickQoute
 */

enhancement GLEREQuickQuote_TDICEnhancement : GLEREQuickQuote_TDIC {

  function createAndAddQuickQuoteCost() : GLEREQuickQuoteCost_TDIC {
    var cost = new GLEREQuickQuoteCost_TDIC(this)
    cost.QuickQuote = this
    return cost
  }

  property get TotalEREPremium() : MonetaryAmount {
    return this.Costs.where(\elt -> elt.RateAmountType != RateAmountType.TC_TAXSURCHARGE).sum(\c -> c.ProratedPremium)
  }

  property get TaxesAndSurcharges() : MonetaryAmount {
    var taxesSurcharges = this.Costs.where(\elt -> elt.RateAmountType == RateAmountType.TC_TAXSURCHARGE)
    if (taxesSurcharges.HasElements) {
      return taxesSurcharges.sum(\elt -> elt.ProratedPremium)
    }
    return new MonetaryAmount(0, CurrencyUtil.DefaultCurrency)
  }

  property get TotalPremium() : MonetaryAmount {
    return TotalEREPremium + TaxesAndSurcharges
  }

  property get TotalTermPremium() : MonetaryAmount {
    return this.Costs.where(\elt -> elt.PremiumType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.DisplayName).sum(\c -> c.ProratedPremium)
  }
}
