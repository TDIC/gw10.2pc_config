package tdic.pc.config.enhancements.job

uses gw.api.assignment.Assignee
uses gw.assignment.PCAssigneePicker
uses java.util.HashSet
/**
 * US884
 * Shane Sheridan 03/20/2015
 */
enhancement TDIC_PolicyPeriodActivityEnhancement : entity.PolicyPeriod {

  /**
   * US884
   * Shane Sheridan 03/20/2015
   */
  property get SuggestedAssignees_TDIC() : Assignee[] {
    var assigneeSet = new HashSet<Assignee>()
    var underwriterOnPolicy = new gw.api.assignment.RoleAssignee(typekey.UserRole.TC_UNDERWRITER, this.Job.Underwriter )

    assigneeSet.add(underwriterOnPolicy)

    return assigneeSet.toTypedArray()
  }

  /**
   * US884
   * Shane Sheridan 03/20/2015
   */
  function getSuggestedAssigneesForUWActivity_TDIC(assigneePicker : PCAssigneePicker, assignees : Assignee[]) : Assignee[] {
    if(this.WC7LineExists){
      return SuggestedAssignees_TDIC
    }

    // OOTB method
    return assigneePicker.buildSuggestedAssignees(assignees)
  }
}
