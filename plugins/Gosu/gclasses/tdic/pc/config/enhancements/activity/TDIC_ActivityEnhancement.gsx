package tdic.pc.config.enhancements.activity

/**
 * US1320
 * 04/01/2015 Rob Kelly
 *
 * Activity Enhancement for TDIC.
 */
enhancement TDIC_ActivityEnhancement : entity.Activity {

  /**
   * Returns true if this activity can be assigned to the specified User; false otherwise.
   *
   * An activity can be assigned to the specified user if
   * 1. the activity is of status Open
   * 2. the activity is assigned to a queue
   * 3. the user is a member of the group with which the queue is associated
   */
  public function canAssignToUser_TDIC(aUser : User) : boolean {

    return this.Status == typekey.ActivityStatus.TC_OPEN
           and this.AssignedQueue != null
           and this.AssignedQueue.Group.Users.firstWhere( \ u -> u.User == aUser) != null
  }
}
