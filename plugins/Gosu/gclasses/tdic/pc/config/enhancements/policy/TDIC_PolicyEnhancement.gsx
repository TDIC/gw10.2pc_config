package tdic.pc.config.enhancements.policy
/**
 * DE128
 * Shane Sheridan 02/17/2015
 *
 * Extension of OOTB PolicyEnhancement for TDIC.
 */
enhancement TDIC_PolicyEnhancement : entity.Policy {

  /**
   * DE128
   * Shane Sheridan 02/17/2015
  */
  function createPreRenewalDirectionNote_TDIC() : Note {
    var newNote = this.newPrerenewalNote()
    newNote.SecurityType = NoteSecurityType.TC_UNRESTRICTED
    return newNote
  }

}
