package tdic.pc.config.enhancements.policy

uses gw.api.database.Query
/**
 * Shane Sheridan 12/08/2014
 */
enhancement TDIC_PolicyPeriodSubmissionEnhancement : entity.PolicyPeriod {

  /**
   * Shane Sheridan 12/08/2014
   *
   * This function is invoked when creating a new Submission Job.
  */
  function initializeNewSubmission(){
    setDateQuoteNeeded()
    setDefaultUWCompany()
    setDefaultIndustryCode()

    for(aPolicyLocation in this.PolicyLocations){
      aPolicyLocation.setReinsuranceSearchTag()
    }
  }

  /**
   * Shane Sheridan 12/08/2014
   */
  private function setDateQuoteNeeded(){
    if(this.Submission.DateQuoteNeeded == null){
      this.Submission.DateQuoteNeeded = java.util.Date.CurrentDate
    }
  }

  /**
   * Shane Sheridan 12/08/2014
   */
  private function setDefaultUWCompany(){
    // Default UW Company = The Dentist Insurance Company.
    final var DEFAULT_UW_COMPANY = typekey.UWCompanyCode.TC_1000_10000
    var queryObj = gw.api.database.Query.make(UWCompany).compare(UWCompany#Code, Equals, DEFAULT_UW_COMPANY).select()
    this.UWCompany = queryObj.AtMostOneRow
  }

  /**
   * Shane Sheridan 12/08/2014
   */
  private function setDefaultIndustryCode() {
    final var DEFAULT_INDUSTRY_CODE = "8021"
    // Industry code is associated with the PolicyPriNamedInsured contact.
    var primaryNamedInsured = this.PrimaryNamedInsured
    var queryObj = Query.make(IndustryCode).compare(IndustryCode#Code, Equals, DEFAULT_INDUSTRY_CODE).select()
    primaryNamedInsured.IndustryCode = queryObj.AtMostOneRow
  }
}
