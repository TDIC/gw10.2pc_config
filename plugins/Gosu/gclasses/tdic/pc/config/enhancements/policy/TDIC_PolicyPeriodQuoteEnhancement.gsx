package tdic.pc.config.enhancements.policy
/**
 * DE122
 * Shane Sheridan 02/19/2015
 *
 * This adds Quote related functionality to the PolicyPeriod entity.
 */
enhancement TDIC_PolicyPeriodQuoteEnhancement: entity.PolicyPeriod {

  /**
   * DE122
   * Shane Sheridan 02/19/2015
   *
   * This function may throw exceptions.
   */
  function regenerateExistingQuote_TDIC(){
    this.edit()

    // BrianS - Last parameter is false so that warnings do not stop renewal issuance
    //this.JobProcess.requestQuote();
    this.JobProcess.requestQuote(null, ValidationLevel.TC_QUOTABLE, RatingStyle.TC_DEFAULT, false);
  }
}
