package tdic.pc.config.enhancements.policy

uses java.util.Date
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.assignment.AssignmentUtil

/**
 * Shane Sheridan 08/22/2014
 * Extension of OOTB PolicyPeriodSummaryEnhancement for TDIC.
 */
enhancement PolicyPeriodSummaryEnhancement : entity.PolicyPeriodSummary {

  /**
   * Shane Sheridan 08/22/2014
   *
   * If policy is cancelled, return Cancellation Date, else return Expiration Date
   */
   property get ExpirationDate() : Date {
    if(this.CancellationDate != null){
      return this.CancellationDate
    }
    else{
      return this.PeriodEnd
    }
  }

  //20150503 TJ Talluto US1008 - fetching the Policy Participant UW (not the UW on the Job)
  property get Underwriter_TDIC() : User{
    return this.fetchPolicyPeriod()
      .Policy
      .RoleAssignments
      .firstWhere( \ elt -> elt.Role == typekey.UserRole.TC_UNDERWRITER)
      .AssignedUser
  }

}
