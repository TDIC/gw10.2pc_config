package tdic.pc.config.enhancements.policy
/**
 * Created with IntelliJ IDEA.
 * User: KesavaT
 * Date: 9/10/15
 * Time: 1:35 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement TDIC_WC7RatingPeriodEnhancement : gw.lob.wc7.rating.WC7RatingPeriod {
  /**
   * GW 141
   * Kesava Tavva 09/10/2015
   *
   * Get Costs for the Rating Period
   */
  @Returns("Array of FinalAuditOption typekeys.")
  property get BasedOnCosts_TDIC() : WC7Cost[] {
    return this.Jurisdiction.WCLine.BasedOn.Costs.cast( WC7Cost ).where( \ w -> w.JurisdictionState==this.Jurisdiction.Jurisdiction).toSet().byRatingPeriod( this )?.toTypedArray()
  }
}
