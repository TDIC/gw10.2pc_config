package tdic.pc.config.enhancements.policy
/**
 * US885
 * Shane Sheridan 02/16/2015
 *
 * Extension of OOTB PolicyTermEnhancement for TDIC.
 */
enhancement TDIC_PolicyTermEnhancement : entity.PolicyTerm {

  /**
   * US885
   * Shane Sheridan 02/16/2015
   */
  property get AvailablePreRenewalDirectionOptions() : typekey.PreRenewalDirection[] {
    var availableOptions = typekey.PreRenewalDirection.TF_UNDERWRITERONLY_TDIC.TypeKeys
    return availableOptions.toTypedArray()
  }

}
