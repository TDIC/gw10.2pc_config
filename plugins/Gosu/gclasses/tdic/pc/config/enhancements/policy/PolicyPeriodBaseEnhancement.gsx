package tdic.pc.config.enhancements.policy

uses entity.Activity
uses entity.Contact
uses entity.WC7Cost
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.Plugins
uses gw.plugin.billing.IBillingSummaryPlugin
uses gw.rating.rtm.query.QueryStrategyFactory
uses gw.rating.rtm.query.RateQueryParam
uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses tdic.pc.config.transientpolicy.TransientPriorPolicy_TDIC
uses tdic.web.pcf.helper.GLQuoteScreenHelper

uses java.math.BigDecimal

/**
 * Shane Sheridan 9/10/2014
 *
 * Extension of OOTB PolicyPeriodBaseEnhancement for TDIC.
 */
enhancement PolicyPeriodBaseEnhancement: entity.PolicyPeriod {
  /**
   * PolicyNumberLong_TDIC returns <<policy number>>-<<term number>>
   */
  property get PolicyNumberLong_TDIC() : String {
    var retval : String;
    if (this.PolicyNumberAssigned) {
      retval = this.PolicyNumber + "-" + this.TermNumber;
    }
    return retval;
  }

  /**
   * Returns the calculated Basis (i.e. Gross Annual Pay) for wc7 Line.
   */
  property get BasisAmount(): int {
    var basisAmount : int = 0;
    var wc7Line = this.WC7Line;

    for (jurisdiction in wc7Line.WC7Jurisdictions) {
      for (covEmp in wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (covEmp.BasisAmount != null) {
          basisAmount += covEmp.BasisAmount;
        }
      }
    }

    return basisAmount;
  }

  /**
   * Shane Sheridan 12/17/2014
   *
   * This returns a List of available TermTypes
   */
  property get getTermTypes(): java.util.List<typekey.TermType> {
    if (this.WC7LineExists){
      var termTypeList = typekey.TermType.getTypeKeys(false)
      var modifiedTermTypeList: java.util.List<typekey.TermType>
      if (this.Job.Subtype == typekey.Job.TC_SUBMISSION) {
        modifiedTermTypeList = termTypeList.where(\elt -> elt.Code == typekey.TermType.TC_ANNUAL as String)
      }
      else if (this.Job.Subtype == typekey.Job.TC_REWRITE){
        modifiedTermTypeList = termTypeList.where(\elt -> elt.Code == typekey.TermType.TC_ANNUAL as String
            or elt.Code == typekey.TermType.TC_OTHER as String)
      }
      return modifiedTermTypeList
    }
    // else if not WC7 line - use OOTB functionality
    return this.getAvailablePolicyTermsForCurrentOffering()
  }

  /**
   * Shane Sheridan 12/17/2014
   *
   * The Term Types field of the Policy Info screen was configured to be un-editable for WC7 line.
   * However, this is only required for the Submission wizard, and so other wizards need to be editable.
   */
  property get isTermTypeEditable(): boolean {
    if (this.WC7LineExists){
      if (this.Job.Subtype == typekey.Job.TC_SUBMISSION){
        return false
      }
    } else if (this.BOPLineExists) {
      if (this.Job typeis PolicyChange) {
        return false
      }
    } else if (this.GLLineExists) {
      if (this.Job typeis PolicyChange) {
        return false
      }
    }
    // else this is not a Submission for WC7 line - allow edit(or OOB).
    return this.CanUpdatePeriodDates
  }

  /**
   * Shane Sheridan 12/17/2014
   *
   * The expiration date should not be editable for Submissions.
   */
  property get isExpirationDateEditable(): boolean {
    if (this.WC7LineExists){
      if (this.Job.Subtype == typekey.Job.TC_SUBMISSION){
        return false
      }
    }
    // else this is not a Submission for WC7 line - allow edit.
    return true
  }

  /**
   * BrittoS 04/23/2020, Policy was originally issued in AS400 and renewed in PolicyCenter
   * @return
   */
  property get isLegacyPolicy_TDIC() : Boolean {
    if (this.AssociatedPolicyPeriod.BasedOn != null
        and ((this.AssociatedPolicyPeriod.LegacyPolicyNumber_TDIC != null
        and this.AssociatedPolicyPeriod.LegacyPolicyNumber_TDIC.trim() != "")
        or this.AssociatedPolicyPeriod.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION)) {
      return true
    }
    return false
  }

  /**
   * This function copies the OOTB addNewPolicyAddlNamedInsuredForContact function, but sets the TDIC default industry code - if WorkersComp7.
   * Add a new PolicyAddlNamedInsured to this period, linked to the NamedInsured role on the acctContact.
   * This method has a side-effect of creating the Account Contact and NamedInsured on the account if they don't
   * already exist.  These entities are all created in this period's bundle.
   * @Throws IllegalArgumentException If Contact is already a PolicyAddlNamedInsured on this period
   */
  @Param("contact", "The Contact that the PolicyAddlNamedInsured ultimately refers to")
  @Returns("The newly created PolicyAddlNamedInsured")
  function addNewPolicyAddlNamedInsuredForContact_TDIC(contact: Contact): PolicyAddlNamedInsured {
    var addlNamedInsured = this.addNewPolicyContactRoleForContact(contact, TC_POLICYADDLNAMEDINSURED) as PolicyAddlNamedInsured
    if (this.WC7LineExists){
      // Default TDIC IndustryCode.
      final var DEFAULT_INDUSTRY_CODE = "8021"
      // Query to get the Industry Code from Database
      var queryObj = gw.api.database.Query.make(IndustryCode).compare(IndustryCode#Code, Equals, DEFAULT_INDUSTRY_CODE).select()
      // Set the foreign key value for the Account.IndustryCode
      addlNamedInsured.IndustryCode = queryObj.AtMostOneRow
    }
    return addlNamedInsured
  }

  /**
   *  This function copies the OOTB addNewPolicyAddlNamedInsuredOfContactType function, but calls addNewPolicyAddlNamedInsuredForContact_TDIC
   *  instead of the OOTB addNewPolicyAddlNamedInsuredForContact.
   * Add a new contact as a PolicyAddlNamedInsured on this period.  This has the side-effect of creating
   * the Contact, AccountContact, and NamedInsured on the account.  These entities are all created
   * in this period's bundle.
   */
  @Param("contactType", "The type of contact to create")
  @Returns("The newly created PolicyAddlNamedInsured")
  function addNewPolicyAddlNamedInsuredOfContactType_TDIC(contactType: ContactType): PolicyAddlNamedInsured {
    var acctContact = this.Policy.Account.addNewAccountContactOfType(contactType)
    return this.addNewPolicyAddlNamedInsuredForContact_TDIC(acctContact.Contact)
  }

  /**
   * This function copies the OOTB addAllExistingAdditionalNamedInsureds function, but calls addNewPolicyAddlNamedInsuredForContact_TDIC
   *  instead of the OOTB addNewPolicyAddlNamedInsuredForContact.
   * For each AccountContact returned by the UnassignedAdditionalNamedInsureds property,
   * add that AccountContact as an additional named insured to the PolicyPeriod
   */
  @Returns("The newly created PolicyAddlNamedInsured.")
  function addAllExistingAdditionalNamedInsureds_TDIC(): PolicyAddlNamedInsured[] {
    var policyNamedInsureds = new ArrayList<PolicyAddlNamedInsured>()
    for (ani in this.UnassignedAdditionalNamedInsureds) {
      policyNamedInsureds.add(this.addNewPolicyAddlNamedInsuredForContact_TDIC(ani.Contact))
    }
    return policyNamedInsureds.toTypedArray()
  }

  /**
   * Author: John Ly
   * Created: 10/14/2014
   * Description: Virtual Property that will get prefix, first name, middle Initial, last name, suffix and credentials
   *              for a person contact for the person who is primary insured.
   */
  @Returns("Returns Primary Name Insured on the Policy")
  property get primaryNameInsured_TDIC(): String {
    // Check for the type of Contact
    if (this.PrimaryNamedInsured.ContactDenorm typeis Person) {
      var anPrimaryNameInsured = this.PrimaryNamedInsured.ContactDenorm as Person
      var primaryNameInsured: String

      if (anPrimaryNameInsured.Prefix != null) {
        primaryNameInsured = anPrimaryNameInsured.Prefix.Description + " "
      }

      primaryNameInsured += anPrimaryNameInsured.FirstName + " "

      if (anPrimaryNameInsured.MiddleName != null) {
        primaryNameInsured += anPrimaryNameInsured.MiddleName.substring(0, 1) + "." + " "
      }

      primaryNameInsured += anPrimaryNameInsured.LastName + " "

      if (anPrimaryNameInsured.Suffix != null) {
        primaryNameInsured += anPrimaryNameInsured.Suffix.Description
      }

      if (anPrimaryNameInsured.Credential_TDIC != null) {
        if (anPrimaryNameInsured.Suffix != null) {
          primaryNameInsured += ", "
        }
        primaryNameInsured += anPrimaryNameInsured.Credential_TDIC.Description
      }
      return primaryNameInsured
    } else {
      // Return Primary Name Insured for an Company
      return (this.PrimaryNamedInsured) as String
    }
  }

  /*
  * Add Claims to Prior Losses
  * */

  @Param("claims", "Array of Claim(s) Data")
  function AddToPriorHistory(claims: Claim[]) {
    // Check for duplicate records in the PriorLoss History and compare them with the Claims data.
    if (this.Policy.PriorLosses.where(\priorLoss -> priorLoss.ClaimNumber_TDIC == claims*.ClaimNumber.firstWhere(\claim -> claim == priorLoss.ClaimNumber_TDIC)).Count == 0)  {

      // Get all the claim(s) that was found and map the data to Prior Losses.
      for (claim in claims) {
        var claimsDetail = gw.api.web.claim.ClaimUtil.getClaimDetail(claim)
        var aLossHistory = new LossHistoryEntry()

        aLossHistory.Policy = claim.PolicyPeriod.Policy
        aLossHistory.PolicyNumber_TDIC = claim.PolicyPeriod.PolicyNumber
        //20150304 TJ Talluto - This should probably be done upstream.  FIXME
        //aLossHistory.Carrier_TDIC = claim.PriorCarrier_TDIC
        aLossHistory.Carrier_TDIC = Query.make(Organization).compare("PublicID", Relop.Equals, "systemTables:1").select().AtMostOneRow as String
        aLossHistory.PolicyPeriodEffDate_TDIC = claim.PolicyPeriod.PeriodStart
        aLossHistory.PolicyPeriodExpDate_TDIC = claim.PolicyPeriod.PeriodEnd
        aLossHistory.RunDate_TDIC = claim.RunDate_TDIC
        aLossHistory.ClaimNumber_TDIC = claim.ClaimNumber
        aLossHistory.Description = claimsDetail.Description
        aLossHistory.LossDate_TDIC = claim.LossDate
        aLossHistory.Status_TDIC = claim.Status
        aLossHistory.ClosedDate_TDIC = claim.ClosedDate_TDIC
        aLossHistory.TotalIncurred = claim.TotalIncurred
        aLossHistory.AmountPaid = claimsDetail.TotalPaid.Amount.ofDefaultCurrency()
        aLossHistory.AmountResv = claimsDetail.RemainingReserves

        // Add Prior Loss to the Policy Period.
        this.Policy.addToPriorLosses(aLossHistory)
      }

      // Set the Loss History Type to Manual so that the user can navigate to Prior Loss Card and automatically be directed to Manual
      this.Policy.LossHistoryType = LossHistoryType.TC_MAN;
    }
  }

  /*
   * Set the default value(s) when the user add a row to the Prior Loss List View.
   * */

  @Param("lossHistoryEntry", "Set the default vale for LossHistoryEntry")
  function defaultValuesLossHistory(lossHistoryEntry: LossHistoryEntry) {
    lossHistoryEntry.RunDate_TDIC = Date.Today
  }

  /**
   * US878
   * Shane Sheridan 02/02/2015
   *
   * Check if the Edit Policy Transaction option is available to the current user.
   */
  property get canUserApproveActivePreissuanceUWIssue(): boolean {
    var activeUWissues = this.UWIssuesActiveOnly

    for (ai in activeUWissues) {
      if (ai.isBlocking(typekey.UWIssueBlockingPoint.TC_BLOCKSISSUANCE)
          and ( ai.CheckingSet == typekey.UWIssueCheckingSet.TC_PREISSUANCE or ai.CheckingSet == typekey.UWIssueCheckingSet.TC_QUESTION)){
        var code = ai.IssueType.Code
        var value = ai.Value
        var isUserAuthorizedToApproveUWIssue = isUserAuthorizedToApproveUWIssue_TDIC(code, value)

        if (not isUserAuthorizedToApproveUWIssue){
          return false
        }
      }
    }
    // ELSE there are no UW issues, or user can approve all UW Issues; allow edit.
    return true
  }

  /**
   * US878
   * Shane Sheridan 02/02/2015
   *
   * This function checks if the current user has the authority to approve a given UW Issue.
   */
  private function isUserAuthorizedToApproveUWIssue_TDIC(uwIssueTypeCode: String, value: String): boolean {
    var result = false

    for (anUWAuthorityProfile in User.util.CurrentUser.UWAuthorityProfiles) {
      for (aGrant in anUWAuthorityProfile.Grants) {
        if (aGrant.IssueType.Code == uwIssueTypeCode){
          result = aGrant.authorizes(value)
        }
      }
    }
    return result
  }

  /**
   * US469
   * Shane Sheridan 01/13/2015
   */
  property get IsPolicyTermLatestPeriodByModelDate(): boolean {
    var latestBoundPeriod = this.Policy.BoundPeriods.where(\period -> period.TermNumber == this.TermNumber and period.CancellationDate == null).orderByDescending(\p -> p.ModelDate).first()

    return this == latestBoundPeriod
  }

  /**
   * US561: Policy Feed to Pivotal/AS400
   * 12/04/2014 Vicente
   *
   * Returns the Xmod rate
   */
  @Returns("Returns the XMod rate")
  property get XMod_TDIC(): BigDecimal {
    var modifier : Modifier;

    if (this.Lines.Count == 1) {
      var line = this.Lines.first();
      if (line typeis WC7WorkersCompLine) {
        if (line.WC7Jurisdictions.Count == 1) {
          var jurisdiction = line.WC7Jurisdictions.first();
          modifier = jurisdiction.Modifiers.firstWhere(\m -> m.PatternCode == "WC7ExpMod" and m.Eligible == true);
        } else {
          throw new UnsupportedOperationException("XMod_TDIC property not implemented for WC7WorkersCompLine with more than one jurisdiction.");
        }
      } else {
        throw new UnsupportedOperationException("XMod_TDIC property not implemented for " + typeof(line) + " line of business.");
      }
    } else {
      throw new UnsupportedOperationException("XMod_TDIC property not implemented for policy with more than one line of business.");
    }

    return modifier.RateModifier;
  }

  /**
   * US561: Policy Feed to Pivotal/AS400
   * 12/04/2014 Vicente
   *
   * Returns the First Payment Date
   */
  @Returns("Returns the First Payment Date")
  property get FirstPaymentDate_TDIC(): Date {
    var retval : Date;

    var billingSummaryPlugin = Plugins.get(IBillingSummaryPlugin);
    var billingPeriodInfo = billingSummaryPlugin.retrievePolicyBillingSummary(this.PolicyNumber, this.TermNumber)

    if (billingPeriodInfo typeis gw.plugin.billing.bc1000.BCPolicyBillingSummaryWrapper) {
      retval = billingPeriodInfo.FirstPaymentDate;
    } else {
      throw new UnsupportedOperationException ("PolicyPeriodBaseEnhancement.FirstPaymentDate_TDIC - IBillingSummaryPlugin was upgraded from bc800");
    }

    return retval;
  }

  /**
   * US561: Policy Feed to Pivotal/AS400
   * 12/04/2014 Vicente
   *
   * Returns the Policy Period has Multiline discount
   */
  @Returns("Returns the Policy Period has Multiline discount")
  property get MultilineDiscount(): boolean {
    var retval = false;
    var modifier : Modifier;

    if (this.WC7LineExists) {
      for (jurisdiction in this.WC7Line.WC7Jurisdictions) {
        modifier = jurisdiction.Modifiers.firstWhere(\m -> m.PatternCode == "WC7TDICMultiLineDiscount"
                                                       and m.Eligible == true);
        if (modifier != null) {
          break;
        }
      }
    }

    if (modifier != null) {
      retval = modifier.BooleanModifier;
    }

    return retval;
  }

  /*
   20150202 TJ Talluto - DE93,
   Return a list of warning messages that indicates that there are gaps or overlaps in coverage for prior policies and the current policy
   */
  //20180219 TJT GW-2981 - include prior TDIC policies that don't appear in the Prior Carrier Policies list on Risk Analysis

  @Returns("List of Strings that returns warning messages for Gap or Overlap in coverages for Prior Policies.")
    public property get RiskAnalysisPriorPolicyWarningMessage(): String {
      var tmpDaysDifference: int
      var tmpMsg: String = null
      var rtnMessage: String = null
      var aryTransientPriorPolicy_TDIC = new ArrayList<TransientPriorPolicy_TDIC>()

      if (!this.Policy.PriorPolicies.IsEmpty or this.Policy.Account.Policies.where(\elt -> elt != this.Policy).Count > 0) {
        if (this.Policy.PriorPolicies*.EffectiveDate.contains(null) || this.Policy.PriorPolicies*.ExpirationDate.contains(null) || this.Policy.PriorPolicies*.Carrier.contains(null) || this.Policy.PriorPolicies*.PolicyNumber.contains(null)) {
          rtnMessage = "Required values are missing (effective dates, expiration dates, carrier names, or policy numbers)"
        } else {
          for (eachPriorTDICPolicy in this.Policy.Account.Policies.where( \ elt -> elt != this.Policy and elt?.Product?.CodeIdentifier == this?.Policy?.Product?.CodeIdentifier)) {
            //20180308 TJT GW-3141: Must check to see if TDIC policies are bound before calling the constructor
            if (eachPriorTDICPolicy.LatestBoundPeriod != null){
              aryTransientPriorPolicy_TDIC.add(new TransientPriorPolicy_TDIC(eachPriorTDICPolicy))
            }
          }
          for (eachPriorCarrierPolicy in this.Policy.PriorPolicies){
            aryTransientPriorPolicy_TDIC.add(new TransientPriorPolicy_TDIC(eachPriorCarrierPolicy))
          }

          aryTransientPriorPolicy_TDIC = aryTransientPriorPolicy_TDIC.sortBy(\elt -> elt.PolicyStart) as ArrayList<TransientPriorPolicy_TDIC>

          //  Compare the expiration date of each Prior Policy term with the effective date of the next Prior Policy term (terms are sorted ascending)
          for (eachTransientPriorPolicy_TDIC in aryTransientPriorPolicy_TDIC index i) {
            if (i >= 1) {
              tmpDaysDifference = aryTransientPriorPolicy_TDIC[i - 1].PolicyEnd.differenceInDays(eachTransientPriorPolicy_TDIC.PolicyStart)// this function ignores the time component
              if (tmpDaysDifference > 0) {
                tmpMsg = "Gap in coverage on Prior Policies between " + aryTransientPriorPolicy_TDIC[i - 1].PolicyEnd.ShortFormat + " and " + eachTransientPriorPolicy_TDIC.PolicyStart.ShortFormat + " of " + tmpDaysDifference + " day(s)"
              } else if (tmpDaysDifference < 0) {
                tmpMsg = "Overlap in coverage on Prior Policies between " + aryTransientPriorPolicy_TDIC[i - 1].PolicyEnd.ShortFormat + " and " + eachTransientPriorPolicy_TDIC.PolicyStart.ShortFormat + " of " + tmpDaysDifference + " day(s)"
              } else {
                // dates are identical - no gap and no overlap
              }
              rtnMessage = (rtnMessage == null ? tmpMsg : rtnMessage.concat(tmpMsg))
            }
            // Compare the expiration date of the most recent Prior Policy term against the start of this Policy with TDIC (not to be confused with the term on the workorder)
            if (i == aryTransientPriorPolicy_TDIC.Count - 1){// test the last record only
              var tmpStartDateOfPolicy = this.Policy.findEarliestPeriodStart()
              tmpDaysDifference = aryTransientPriorPolicy_TDIC[i].PolicyEnd.differenceInDays(tmpStartDateOfPolicy)// this function ignores the time component
              if (tmpDaysDifference > 0) {
                tmpMsg = "Gap in coverage between the start of this Policy (" + tmpStartDateOfPolicy.ShortFormat + ") and the most recent Prior Policy expiration (" + aryTransientPriorPolicy_TDIC[i].PolicyEnd.ShortFormat + ") of " + tmpDaysDifference + " day(s)"
              } else if (tmpDaysDifference < 0) {
                tmpMsg = "Gap in coverage between the start of this Policy (" + tmpStartDateOfPolicy.ShortFormat + ") and the most recent Prior Policy expiration (" + aryTransientPriorPolicy_TDIC[i].PolicyEnd.ShortFormat + ") of " + tmpDaysDifference + " day(s)"
              } else {
                // dates are identical - no gap and no overlap
              }
              rtnMessage = (rtnMessage == null ? tmpMsg : rtnMessage.concat(tmpMsg))
            }
          }
        }
      }
      return (rtnMessage == null ? null : "On Risk Analysis - Prior Policies: " + rtnMessage)
    }

  /*
   Return note object with pre populated properties
   */

  @Param("activity", "Current activity that was generated")
  @Param("note", "Note object that was generated")
  @Returns("Return a note object")
  public function prePopulateNote(activity: Activity, note: Note): Note {
    note.Subject = activity.Subject
    return note
  }

  /**
   * US469
   * Shane Sheridan 01/08/2015
   *
   * calculate loss ratio given the claims on this policy.
   */
  function calculateLossRatio(claims: Claim[]): BigDecimal {
    var totalIncurredSum = claims.where(\c -> c.TotalIncurred != null).sum(this.PreferredSettlementCurrency, \c -> c.TotalIncurred)
    var estimatedPremium = this.TotalPremiumRPT.Amount
    // loss ratio [%] = incurred losses over premium [multiplied by 100]
    var lossRatio = totalIncurredSum.divide(estimatedPremium, FLOOR).multiply(100).Amount

    return lossRatio
  }

  /**
   * US669
   * 02/11/2015 Shane Murphy
   *
   * Standard Premium required for Document Production
   */
  @Returns("Standard Premium")
  property get StandardPremium_TDIC(): MonetaryAmount {
    //return this.AllCosts.StandardPremium.AmountSum(this.PreferredSettlementCurrency)
    var costs = this.WC7Line.WC7Costs?.toSet()
    var stdPremium = costs.getStandardPremiums(this.PreferredCoverageCurrency).where(\elt -> elt.Description == DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalStandardPremium")).first()
    var stdPrm = stdPremium.Total
    return stdPrm
  }

  /**
   * US669
   * 02/11/2015 Shane Murphy
   *
   * Manual Premium required for Document Production
   */
  @Returns("Manual Premium")
  property get ManualPremium_TDIC(): MonetaryAmount {
    var costs = this.WC7Line.WC7Costs?.toSet()
    var manualPremium = costs.getStandardPremiums(this.PreferredCoverageCurrency).where(\elt -> elt.Description == DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalManualPremium")).first()
    var manPrm = manualPremium.Total
    return manPrm
  }

  /**
   * US669
   * 02/11/2015 Shane Murphy
   *
   * Subject Premium required for Document Production
   */
  @Returns("Subject Premium")
  property get SubjectPremium_TDIC(): MonetaryAmount {
    var costs = this.WC7Line.WC7Costs?.toSet()
    var subjectPremium = costs.getStandardPremiums(this.PreferredCoverageCurrency).where(\elt -> elt.Description == DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalSubjectPremium")).first()
    var subPrm = subjectPremium.Total
    return subPrm
  }

  /**
   * US669
   * 02/11/2015 Shane Murphy
   *
   * Modified Premium required for Document Production
   */
  @Returns("Modified Premium")
  property get ModifiedPremium_TDIC(): MonetaryAmount {
    var costs = this.WC7Line.WC7Costs?.toSet()
    var modifiedPremium = costs.getStandardPremiums(this.PreferredCoverageCurrency).where(\elt -> elt.Description == DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalModifiedPremium")).first()
    var modPrm = modifiedPremium.Total
    return modPrm
  }

  /**
   * US669
   * 02/11/2015 Shane Murphy
   *
   * Total Fares and Taxes required for Document Production.
   * Obtained by subtracting the California Total Premium from the California Total Cost.
   */
  @Returns("Total Taxes and Fares")
  property get TaxesAndFares(): MonetaryAmount {
    return this.TotalCostRPT - this.TotalPremiumRPT
  }

  /**
   * US669
   * 03/10/2015 Shane Murphy
   *
   * Minimum Premium for Policy for DocumentProduction
   */
  @Returns("Total Taxes and Fares")
  property get PolicyMinimumPremium(): int {
    //return TDIC_WCpolsUtil.getPolicyMinimumPremiumAmount()
    try {
      var rateBook = WC7RatingUtil.selectRateBook(this.WC7Line, RateBookStatus.TC_APPROVED, this.Offering as String)
      var table = rateBook.getTable("WC7MinimumPremium_TDIC")
      var qry : List<RateQueryParam<String>> = {new RateQueryParam<String>("Constant", "Y")}
      var minPremium = new QueryStrategyFactory().getFactorQuery(table).query(table, qry, "Premium").Factor
      return (minPremium != null ? minPremium as int : 0)
    }catch(e: Exception){
      return 0
    }
  }

  /**
   * US890, robk
   */
  property get PolicyOrgType_TDIC() : typekey.PolicyOrgType_TDIC {
    return this.EffectiveDatedFields == null ? null : this.EffectiveDatedFields.PolicyOrgType_TDIC
  }

  /**
   * US890, robk
   */
  property set PolicyOrgType_TDIC(anOrgType : typekey.PolicyOrgType_TDIC) {
    if (this.EffectiveDatedFields != null) {
      this.EffectiveDatedFields.PolicyOrgType_TDIC = anOrgType
    }
  }

  /**
   * DE278
   * ShaneS 05/27/2015
   *
   * Create policy locations for all account locations in the list.
   *
   * This is an OOTB function taken from 'LocationPanelSet.pcf' to include TDIC modifications.
  */
  function addLocations_TDIC(locations: List<AccountLocation>) {
    for (loc in locations) {
      var policyLocation = this.newLocation(loc)
      policyLocation.setReinsuranceSearchTag()

      if (this.WC7LineExists and this.WC7Line.getJurisdiction(JurisdictionMappingUtil.getJurisdictionMappingForAccountLocation(loc)) == null) {
        this.WC7Line.addJurisdiction(JurisdictionMappingUtil.getJurisdictionMappingForAccountLocation(loc))
      }
    }
  }

  /**
   *  Return the cancellation reason code.
   */
  public property get CancellationReasonCode_TDIC() : ReasonCode {
    var cancellation = this.Policy.Jobs.where (\j -> j typeis Cancellation
                                                 and j.LatestPeriod.Status == PolicyPeriodStatus.TC_BOUND
                                                 and j.LatestPeriod.EditEffectiveDate == this.CancellationDate)
                                       .orderByDescending(\j -> j.LatestPeriod.ModelDate)
                                       .first() as Cancellation;
    return cancellation.CancelReasonCode;
  }

/**
 * US141
 * 04/11/2017 Kesava Tavva
 *
 * All WC7Costs associated with all Rating periods for Document Production
 */
    @Returns("List of WC7Cost objects")
    property get WC7Costs_TDIC(): List<WC7Cost> {
      return this.AllCosts.whereTypeIs(WC7Cost)

    }
  /**
   * create by: SureshB
   * @description: this method decides the Written Date field editable condition
   * @create time: 4:26 PM 8/19/2019
    * @param null
   * @return: boolean value based on the conditions
   */

    property get canEditWrittenDate_TDIC() : Boolean{
      if(this.BOPLineExists || this.GLLineExists){
        if(this.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
          return false
        }
      }
      return true
    }
  /**
   * create by: SureshB
   * @description: this method decides the Base State field editable condition
   * @create time: 4:28 PM 8/19/2019
    * @param null
   * @return: boolean value based on the conditions
   */

    property get canEditBaseState_TDIC() : Boolean{
      if(this.BOPLineExists || this.GLLineExists){
        // Jeff Lin, Added Renewal condition per requirement 10/30/2019, for defect, GPC-1925
        if(this.Job.Subtype == typekey.Job.TC_POLICYCHANGE or this.Job.Subtype == typekey.Job.TC_RENEWAL){
          return false
        }
      }
      return true
    }
  /**
   * create by: SureshB
   * @description: this method decides the Offering field editable condition
   * @create time: 4:28 PM 8/19/2019
   * @param null
   * @return: boolean value based on the conditions
   */

  property get canEditOffering_TDIC() : Boolean{
    if(this.BOPLineExists || this.GLLineExists){
      if(this.Job.Subtype == typekey.Job.TC_POLICYCHANGE || this.Job.Subtype == typekey.Job.TC_RENEWAL){
        return false
      }
    }
    return true
  }
  /*
     * create by: SureshB
     * @description: method to get the answer of the given BOPBuilding UW question
     * @create time: 5:09 PM 9/6/2019
      * @param question code
     * @return: PCAnswerDelegage[]
     */
  function getAnswerFromBOPBuildingUWQuestionIfQuestionVisible_TDIC (passedInQuestionCode : String) : entity.PCAnswerDelegate[] {
    var tmpAnswer : List<entity.PCAnswerDelegate> = new ArrayList<PCAnswerDelegate>()
    var tmpQuestion : gw.api.productmodel.Question
    var tmpQuestionSet : gw.api.productmodel.QuestionSet = this.Policy.Product.getQuestionSetByCodeIdentifier("BOPBuildingSupp")
    tmpQuestion = tmpQuestionSet.getQuestionByCodeIdentifier(passedInQuestionCode)
    for(policyLocation in this.PolicyLocations){
    if (policyLocation.hasUnderwritingQuestions_TDIC()) {
      if (tmpQuestion != null) {
        if (tmpQuestion.isQuestionVisible(policyLocation)) {
          tmpAnswer.add(policyLocation.getAnswer(tmpQuestion))
        }
      } else if (tmpQuestion == null) {
        //FIXME - question not found, log a warning?  May not be an error?
      }
    }
  }
    return tmpAnswer.toTypedArray()
  }
  /*
     * create by: SureshB
     * @description: method to check if the given BOPBuilding UW question is visible
     * @create time: 5:09 PM 9/6/2019
      * @param question code
     * @return: boolean
     */
  function isBOPBuildingUWQuestionVisible_TDIC (passedInQuestionCode : String) : boolean {
    var isVisible = false
    var tmpQuestion : gw.api.productmodel.Question
    var tmpQuestionSet : gw.api.productmodel.QuestionSet = this.Policy.Product.getQuestionSetByCodeIdentifier("BOPBuildingSupp")
    tmpQuestion = tmpQuestionSet.getQuestionByCodeIdentifier(passedInQuestionCode)
    for (policyLocation in this.PolicyLocations) {
      if (policyLocation.hasUnderwritingQuestions_TDIC()) {
        if (tmpQuestion != null) {
          if (tmpQuestion.isQuestionVisible(policyLocation)) {
            isVisible = true
          }
        }
      }
    }
    return isVisible
  }
  /*
   * create by: SureshB
   * @description: property to determine whether the quote type is quick quote
   * @create time: 7:30 PM 11/19/2019
    * @param null
   * @return: boolean
   */

  property get isQuickQuote_TDIC() : boolean{
    return this.Job typeis  Submission ? (this.Job as Submission).QuoteType == typekey.QuoteType.TC_QUICK : false
  }
  /*
   * create by: SureshB
   * @description: method to get the GL UW Question sets based on the quotetype.
   * @create time: 7:03 PM 11/20/2019
    * @param null
   * @return: QuestionSet[]
   */

  function getGLUWQuestionSets_TDIC() : gw.api.productmodel.QuestionSet[] {
    var uwQuestionSets = this.Policy.Product.getQuestionSetsByType(QuestionSetType.TC_UNDERWRITING)
    if (this.isQuickQuote_TDIC) {
      return uwQuestionSets?.where(\elt -> elt.CodeIdentifier == "GLQuickQuoteUnderwriting_TDIC")
    } else {
      return uwQuestionSets?.where(\elt -> elt.CodeIdentifier == "GLUnderwriting")
    }
  }
  function setRetroDate_TDIC() {
    if (this.GLLineExists) {
      var retroDate = this.GLLine?.GLPriorActsCov_TDIC?.GLPriorActsRetroDate_TDICTerm?.Value
      var retroDEPLDate = this.GLLine?.GLDentalEmpPracLiabCov_TDIC?.GLDEPLRetroDate_TDICTerm?.Value
      if (retroDate == null) {
        this.GLLine?.GLPriorActsCov_TDIC?.GLPriorActsRetroDate_TDICTerm?.setValue(this.EditEffectiveDate)
      }
      if (retroDEPLDate == null) {
        this.GLLine?.GLDentalEmpPracLiabCov_TDIC?.GLDEPLRetroDate_TDICTerm?.setValue(this.EditEffectiveDate)
      }
    }
  }
  /*
    * create by: IndramaniP
    * @description: method to return validation error if Discount eff date is before Submission Eff date
    * @create time: 9:32 PM 04/27/2020
     * @param effDate
    * @return: String
    */
  function isDiscountEffDateValid(effDate : Date) : String{
    var glLine = this.GLLine
    var offering = glLine.Branch.Offering.CodeIdentifier
    if(this.Job typeis Submission and this.Status == PolicyPeriodStatus.TC_DRAFT and offering == "PLOccurence_TDIC" and
        glLine.GLFullTimeFacultyDiscount_TDICExists and glLine.GLFullTimeFacultyDiscount_TDIC.GLFTFEffDate_TDICTerm != null and
        effDate.trimToMidnight().before(this.EditEffectiveDate.trimToMidnight())){
      return DisplayKey.get("TDIC.Web.Policy.GL.Validation.DiscountEffectiveDateCannotBePriorToEffDate")
    }
    if(this.Job typeis Submission and this.Status == PolicyPeriodStatus.TC_DRAFT and offering == "PLOccurence_TDIC" and
        glLine.GLPartTimeDiscount_TDICExists and glLine.GLPartTimeDiscount_TDIC.GLPTEffDate_TDICTerm != null and
        effDate.trimToMidnight().before(this.EditEffectiveDate.trimToMidnight())){
      return DisplayKey.get("TDIC.Web.Policy.GL.Validation.DiscountEffectiveDateCannotBePriorToEffDate")
    }
    return null
  }
  /*
    * create by: IndramaniP
    * @description: method to set Discount Eff date with Submission Eff date.
    * @create time: 9:32 PM 04/27/2020
     * @param null
    * @return: null
    */
  function setDiscountEffDate_TDIC() {
    if (this.GLLineExists and this.Job typeis Submission and this.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      var facultyDiscountEffDate = this.GLLine?.GLFullTimeFacultyDiscount_TDIC?.GLFTFEffDate_TDICTerm?.Value
      var partTimeDiscountEffDate = this.GLLine?.GLPartTimeDiscount_TDIC?.GLPTEffDate_TDICTerm?.Value
      if (facultyDiscountEffDate == null) {
        this.GLLine?.GLFullTimeFacultyDiscount_TDIC?.GLFTFEffDate_TDICTerm?.setValue(this.EditEffectiveDate)
      }
      if (partTimeDiscountEffDate == null) {
        this.GLLine?.GLPartTimeDiscount_TDIC?.GLPTEffDate_TDICTerm?.setValue(this.EditEffectiveDate)
      }
    }
  }
  property get profileChange_TDIC():Boolean{
      if(this.GLLineExists && this.Job typeis PolicyChange && (this.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE).Count==1)){
         return false
      }
    else if(this.BOPLineExists && this.Job typeis PolicyChange && (this.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE).Count==1)){
        return false
      }
   return true
  }
  /*
   * create by: SureshB
   * @description: Property to check if the policy change reason is only one and i.e, regenerateDelcaration.
   * @create time: 4:14 PM 1/28/2020
    * @param null
   * @return:
   */

  property get isChangeReasonOnlyRegenerateDeclaration_TDIC() : boolean {
    return this.Job typeis PolicyChange and this.Job.ChangeReasons.Count == 1 and this.Job.ChangeReasons.hasMatch(\changeReason ->
        changeReason.ChangeReason == ChangeReasonType_TDIC.TC_REGENERATEDECLARATION)
  }

  property get isLegacyConversion() : boolean {
    return this.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION
  }

  /**
   * BrittoS 05/11/2020
   * @return
   */
  property get isERERating_TDIC() : Boolean {
    if(this.GLLineExists
        and this.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
        and this.Job typeis PolicyChange
        and this.GLLine.GLExtendedReportPeriodCov_TDICExists
        and not this.GLLine.BasedOn.GLExtendedReportPeriodCov_TDICExists) {
      if((this.isCanceled() and this.EditEffectiveDate.compareIgnoreTime(this.CancellationDate.addDays(-1)) == 0)
          or (this.EditEffectiveDate.compareIgnoreTime(this.PeriodEnd.addDays(-1))) == 0) {
        return true
      }
    }
    return false
  }

  property get validateERECyberChangeReason() : Boolean{
    if(this.GLLineExists and (this.Job typeis PolicyChange) and (this.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).Count==1)){
      return true
    }
    return false
  }

  property get EREStandardPrem() : MonetaryAmount{
      var glCosts = this.GLLine.Costs.cast(GLCost).toSet()
      var erePremiums = glCosts.getStandardPremiums_TDIC().where(\elt -> elt.DisplayName == "Cyber Suite Liability Extended Reporting Period Endorsement")
      return erePremiums.sum(this.PreferredSettlementCurrency, \c -> c.ActualAmountBilling)
  }


  /**
   * AnudeepK 07/27/2020
   * @return
   */
  property get isDIMSPolicy_TDIC(): boolean{
    return this?.LegacyPolicySource_TDIC=="DIMS"
  }

  /**
   * GWPS-1654 removing location and adding new location, classification field value should be same
   */
  function removeLocAndAddClassfication(location : PolicyLocation){
    if(this.GLLineExists and this.PolicyLocations.Count > 1){
      var classification = this.GLLine.GLExposuresWM.firstWhere(\elt -> elt.Location == location)?.VersionList.AllVersions?.single()
      if(classification != null)
        classification.Location = this.PolicyLocations?.firstWhere(\elt -> elt != location)?.VersionList.AllVersions?.single()
    }
    this.removeLocation(location)
  }

  /*
   * create by: HarishP
   * @description: property to determine EREPremium based on the Cyber limit (GWPS-2054)
   * @param null
   * @return: MonetaryAmount
   */
  property get EREPremiumAmt(): MonetaryAmount {
    // EREPremium calculation for "PLCyberLiab_TDIC"
    if (this.Offering?.CodeIdentifier == "PLCyberLiab_TDIC") {
      if(this.Job typeis PolicyChange and isEREPremiumCalculationNeeded()) {
          var erePremium = getEREPremiumBasedOnCyberLimit(this.GLLine.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm?.Value)
          if(erePremium != null) {
            return new MonetaryAmount(erePremium, this.PreferredSettlementCurrency)
          }
      }
    } // EREPremium calculation for "PLClaimsMade_TDIC"
    else if (this.Offering?.CodeIdentifier == "PLClaimsMade_TDIC") {
      if(this.Job typeis PolicyChange) {
        // EREPremium logic when CHange REason is TC_EREPLOFFER.
          if ( this.Job.ChangeReasons?.hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)
              and this.Job.Policy.GLEREQuickQuotes_TDIC?.HasElements) {
            var glEREQuickQuote = this.Policy.getGLEREQuickQuotes_TDIC()?.orderByDescending(\date -> date.CreateTime).first()
            var prem = glEREQuickQuote?.TotalEREPremium
            var tax = glEREQuickQuote?.TaxesAndSurcharges
            if (prem != null or tax != null) {
              var totalAmt = prem + tax
              return new MonetaryAmount(totalAmt, this?.PreferredSettlementCurrency)
            }
          }else if(this.Job.ChangeReasons?.hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN)){
            // EREPremium logic when CHange REason is TC_EREPLDOCGEN.
            var erePLPolicyPeriod: PolicyPeriod = null
            if(this.BasedOn?.Job.ChangeReasons?.hasMatch(\ changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM)){
              erePLPolicyPeriod = this.BasedOn
            }else {
              var allBoundPeriods = this.Policy.BoundPeriods?.orderByDescending(\per -> per.ModelDate)
              erePLPolicyPeriod = allBoundPeriods?.firstWhere(\periodObj -> periodObj.Job.ChangeReasons
                  ?.hasMatch(\reason -> reason.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM))
            }
            return GLQuoteScreenHelper.getTotalCost(erePLPolicyPeriod, true)
          }
      }
    }
    return null
  }

  /*
   * Method isEREPremiumCalculationNeeded() method to check whether ere premium calculation needed based on the changereason.
   */
  private function isEREPremiumCalculationNeeded(): boolean {
    var allowedChangeReasonsForEREPremium = { ChangeReasonType_TDIC.TC_ERECYBEROFFER, ChangeReasonType_TDIC.TC_ERECYBDOCGEN }
    return this.Job.ChangeReasons?.hasMatch(\reason -> allowedChangeReasonsForEREPremium
        .hasMatch(\allowedChangeReason -> allowedChangeReason == reason.ChangeReason))
  }

  /*
   * create by: HarishP
   * @description: function to fetch EREPremium for corresponding cyberlimit.
   * @param BigDecimal
   * @return: BigDecimal
   */
  private function getEREPremiumBasedOnCyberLimit(cyberLimit: BigDecimal) : BigDecimal {
    var erePremium: BigDecimal = null
    if(cyberLimit == 50000) {
      erePremium = 100
    }else if(cyberLimit == 100000) {
      erePremium = 129
    }else if(cyberLimit == 250000) {
      erePremium = 195
    }
    return erePremium
  }

  function getEffectiveDateForPL(changeReason:ChangeReason_TDIC[]):Date{
    var changeReasons=changeReason*.ChangeReason
    var effectiveDate= this.PeriodStart
      if (this.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        ChangeReasonType_TDIC.TF_PLCMPOLICYCHANGEREASONS.getTypeKeys().each(\elt -> {
          if (changeReasons.contains(elt)) {
            effectiveDate = this.EditEffectiveDate
          }
        })
      }
      if ((this.Offering.CodeIdentifier == "PLOccurence_TDIC")) {
        ChangeReasonType_TDIC.TF_PLOCCPOLICYCHANGEREASONS.getTypeKeys().each(\elt -> {
          if (changeReasons.contains(elt)) {
            effectiveDate = this.EditEffectiveDate
          }
        })
      }

      if (this.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        ChangeReasonType_TDIC.TF_PLCYBERPOLICYCHANGEREASONS.getTypeKeys().each(\elt -> {
          if (changeReasons.contains(elt)) {
            effectiveDate = this.EditEffectiveDate
          }
        })
      }

    return effectiveDate
  }

  property get policyChangeEffectiveDate():java.util.Date{
    var changeReasons=this.Job.ChangeReasons*.ChangeReason
    var effectiveDate=this.PeriodStart
    if(this.Job typeis PolicyChange) {
        if (this.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
          ChangeReasonType_TDIC.TF_BOPPOLICYCHANGEREASONS.getTypeKeys().each(\elt -> {
            if (changeReasons.contains(elt)) {
              effectiveDate = this.EditEffectiveDate
            }
          })
        }
        else if (this.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
          ChangeReasonType_TDIC.TF_LRPPOLICYCHANGEREASONS.getTypeKeys().each(\elt -> {
            if (changeReasons.contains(elt)) {
              effectiveDate = this.EditEffectiveDate
            }
          })
        }
      else{
        effectiveDate=getEffectiveDateForPL(this.Job.ChangeReasons)
      }
    }
    return effectiveDate
  }

  property get skipUwIssuesOnEREChange() : boolean {
    return this.Job typeis PolicyChange and  this.Job.ChangeReasons.hasMatch(\changeReason ->
        changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNA ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNR ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNA ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNR ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN ||
            changeReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBDOCGEN )
  }
}
