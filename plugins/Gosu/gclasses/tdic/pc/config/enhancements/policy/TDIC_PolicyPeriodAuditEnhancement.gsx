package tdic.pc.config.enhancements.policy

uses tdic.pc.config.job.audit.TDIC_AuditScheduler
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.Plugins
uses gw.plugin.billing.IBillingSummaryPlugin

/**
 * US463
 * Shane Sheridan 11/11/2014
 *
 * This is an extension of the OOTB PolicyPeriodAuditEnhancement for TDIC.
 */
enhancement TDIC_PolicyPeriodAuditEnhancement : entity.PolicyPeriod {

  /**
   * US463
   * Shane Sheridan 11/11/2014
   *
   * Get the available planned audit methods when adding a new audit.
   */
  @Returns("Array of AuditMethod typekeys.")
  property get AvailablePlannedAuditMethods() : typekey.AuditMethod[] {
    var typekeys = typekey.AuditMethod.TF_WC7FINALAUDIT_TDIC.TypeKeys
    return typekeys.toTypedArray()
  }

  /**
   * US463
   * Shane Sheridan 11/11/2014
   *
   * Get the available Final Audit Options for Payment screen of Submission..
   */
  @Returns("Array of FinalAuditOption typekeys.")
  property get AvailableFinalAuditOptions() : typekey.FinalAuditOption[] {
    var typekeys = typekey.FinalAuditOption.TF_WC7OPTIONS_TDIC.TypeKeys
    return typekeys.toTypedArray()
  }

  /**
   * DE48
   * Shane Sheridan 11/21/2014
  */
  function scheduleCancellationFinalAudit_TDIC() {
    if (this.IsAuditable) {
      if(this.Job typeis Cancellation)
      TDIC_AuditScheduler.scheduleFinalAudit_TDIC(this, true)
      else
        TDIC_AuditScheduler.scheduleFinalAudit_TDIC(this, false)     // for Policychange
    }
  }

  /**
   * DE48
   * Shane Sheridan 11/24/2014
   *
   * IF WC7 line,
   * then use TDIC modifications for this function.
   */
  function scheduleExpirationFinalAudit_TDIC() {
    if (this.IsAuditable) {
      if(this.WC7LineExists){
        // use TDIC modifications
        TDIC_AuditScheduler.scheduleFinalAudit_TDIC(this, false)
      }
      else{
        // use OOTB
        this.scheduleExpirationFinalAudit()
      }

    }
  }

  /**
   * GW 141,244
   * Kesava Tavva 09/02/2015
   *
   * Get the premium difference amount
   */
  @Returns("MonetaryAmount, Returns premium difference amount")
  property get PremiumDifference_TDIC() : MonetaryAmount {
    return this.TotalCostRPT - this.BasedOn.TotalCostRPT
  }

  /**
   * GW 244
   * Kesava Tavva 10/15/2015
   *
   *
   */
  @Returns("MonetaryAmount, Returns Total paid amount from BillingCenter")
  property get TotalPaidAmount_TDIC() : MonetaryAmount {
    try{
      return Plugins.get(IBillingSummaryPlugin).retrievePolicyBillingSummary(this.PolicyNumber, this.TermNumber).Paid
    }catch(e){
      return new MonetaryAmount(0, this.PreferredSettlementCurrency)
    }
  }

}
