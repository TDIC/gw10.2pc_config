package tdic.pc.config.enhancements.account

uses gw.api.database.Query

/**
 * BrittoS 03/19/2020
 * Account[] emhancement
 */
enhancement AccountSetEnhancement : Set<Account> {

  property get AllPolicyPeriods() : PolicyPeriod[] {
    var periodQ = Query.make(PolicyPeriod)
    periodQ.join(PolicyPeriod#Policy)
        .compareIn(Policy#Account, this.toTypedArray())
    return periodQ.select().toTypedArray()
  }

  property get QuotedPolicyPeriods() : PolicyPeriod[] {
    return AllPolicyPeriods.where(\elt -> elt.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION and elt.Status.Priority >= PolicyPeriodStatus.TC_QUOTED.Priority)
  }
}
