package tdic.pc.config.enhancements.account
/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 10/17/14
 * Time: 9:56 AM
 * To change this template use File | Settings | File Templates.
 */
enhancement TDIC_AccountContactEnhancement : entity.AccountContact {

  /**
   * Author: John Ly
   * Created: 10/17/2014
   * Description: Virtual Property that will get prefix, first name, middle Initial, last name, suffix and credentials
   *              for a person contact.
   */
  @Returns("Returns Full Name with Professional Credentials on the Policy")
  property get fullNameWithProfessionalCredentials_TDIC() : String {
    // Check for the type of Contact
    if (this.isPerson()) {
      // Cast the Contact into a Person
      var anContact = this.Contact as Person
      var fullNameWithProfessionalCredentials = ""

      // Check for null in the prefix
      /*if (anContact.Prefix != null) {
        fullNameWithProfessionalCredentials = anContact.Prefix.Description + " "
      }*/
      if (anContact.FirstName != null) {
        fullNameWithProfessionalCredentials = anContact.FirstName + " "
      }

      // Check for null in the middle name and return just the first initial
      if (anContact.MiddleName != null) {
        fullNameWithProfessionalCredentials += anContact.MiddleName.substring(0,1) + "." + " "
      }

      fullNameWithProfessionalCredentials += anContact.LastName + " "

      // Check for null in the suffix
      if (anContact.Suffix != null) {
        fullNameWithProfessionalCredentials += anContact.Suffix.Description
      }

      // Check for null in the Credential
      if (anContact.Credential_TDIC != null) {
        if (anContact.Suffix != null) {
          fullNameWithProfessionalCredentials += ", "
        }
        fullNameWithProfessionalCredentials += anContact.Credential_TDIC.Description
      }

      return fullNameWithProfessionalCredentials
    } else {
      // Return Name for an Company
      return this.getDisplayName()
    }
  }
}
