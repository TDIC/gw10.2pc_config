package tdic.pc.config.enhancements.address
/**
 * US739
 * Shane Sheridan 02/25/2015
 *
 * Extension of OOTB AddressEnhancement for TDIC.
 */
enhancement TDIC_AddressEnhancement : entity.Address {

  /**
   * US739
   * Shane Sheridan 02/25/2015
   */
  property get AvailableAddressTypeOptions() : typekey.AddressType[] {
    var availableOptions = typekey.AddressType.TF_MAILINGANDBUSINESS_TDIC.TypeKeys
    return availableOptions.toTypedArray()
  }

  /**
   * US739
   * Shane Sheridan 02/25/2015
   *
   * This is called via PosyOnChange when an Address field is edited in the UI.
   *
   * The Standardization status must be reverted because the address verification service will
   * not be invoked for an address with a status of Standardized.
   */
  function revertStandardizedStatus(){
    if(this.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_STANDARDIZED){
      this.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_INVALID
    }
  }
}
