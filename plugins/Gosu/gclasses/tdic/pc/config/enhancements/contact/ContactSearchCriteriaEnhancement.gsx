package tdic.pc.config.enhancements.contact

uses java.util.ArrayList
uses java.util.HashSet
uses gw.plugin.contact.ContactResult
uses java.lang.Exception
uses gw.plugin.contact.impl.ContactResultWrapper
uses gw.plugin.contact.impl.ContactResultInternal
uses gw.api.database.IQueryBeanResult
uses gw.api.util.DisplayableException
uses tdic.pc.config.contact.ContactQueryBuilder
uses gw.api.locale.DisplayKey

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 9/22/14
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
enhancement ContactSearchCriteriaEnhancement : entity.ContactSearchCriteria {

  function overridePerformSearch() : ContactResultWrapper {

    var internalResults = this.internalContactResults()
    var iter = internalResults.iterator()
    var results = new ArrayList<ContactResult>()
    var uids = new HashSet()
    var waningMsg : String
    while (iter.hasNext()){
      var contact = iter.next()
        results.add(new ContactResultInternal(contact))
        if(contact.AddressBookUID != null){
          uids.add(contact.AddressBookUID)
        }
    }

    if (not User.util.CurrentUser.isUseProducerCodeSecurity()) {
      try {
        var remoteResults = this.searchExternalContacts()
        for(result in remoteResults){
          if(not (uids.contains(result.ContactAddressBookUID))){
            results.add(result)
          }
        }
      } catch (e : Exception) {
        if (results.Empty) {
          waningMsg = e.Message
        } else {
          // we need to explain that we have local results, but none from ContactCenter
          waningMsg = DisplayKey.get("Contact.Search.Warning.NoExternalResults", e.Message)
        }
      }
    }
    return new ContactResultWrapper(results.toTypedArray(), waningMsg)
  }

  // Custom function to retrieve contacts from Policy Center Contacts
  function internalContactResults() : IQueryBeanResult<Contact> {

    // Validate Data
    this.restrictorValidation()

    // Declare New Contact Query Builder
    var contactQueryBuilder : ContactQueryBuilder = new ContactQueryBuilder()

    // Determine the Contact Type Either Person or Company
    if (this.ContactType == ContactType.TC_PERSON) {

      contactQueryBuilder.contactType(ContactType.TC_PERSON)

      // Determine which type of search operator to use for First Name
      if (this.FirstNameExact) {
        contactQueryBuilder.withFirstName(this.FirstName)
      } else if (this.FirstNameContains_TDIC)  {
        contactQueryBuilder.withFirstNameContains(this.FirstName)
      } else {
        contactQueryBuilder.withFirstNameStarting(this.FirstName)
      }

      // Determine which type of search operator to use for Last Name
      if (this.KeywordExact) {
          contactQueryBuilder.withLastName(this.Keyword)
      } else if (this.KeywordContains_TDIC) {
          contactQueryBuilder.withLastNameContains(this.Keyword)
      } else {
        contactQueryBuilder.withLastNameStarting(this.Keyword)
      }

      // Determine if there is a official ID
      if (this.ADANumber_TDIC != null or this.ADANumber_TDIC != "") {
        contactQueryBuilder.withOfficialId(this.ADANumber_TDIC)
      }
    } else {
      contactQueryBuilder.contactType(ContactType.TC_COMPANY)

      // Determine which type of search operator to use for Company Name
      if (this.KeywordExact) {
        contactQueryBuilder.withCompanyName(this.Keyword)
      } else if (this.KeywordContains_TDIC) {
        contactQueryBuilder.withCompanyNameContains(this.Keyword)
      } else {
        contactQueryBuilder.withCompanyNameStarting(this.Keyword)
      }
    }


    contactQueryBuilder.withTaxId(this.TaxID)
                        .withWorkPhone(this.Phone)
                        .withCountryDenorm(this.Address.Country)
                        .withCityDenormStarting(this.Address.City)
                        .withStateDenorm(this.Address.State)
                        .withPostalCodeDenormStarting(this.Address.PostalCode)

    return contactQueryBuilder.build().select() as IQueryBeanResult<Contact>
  }

  function restrictorValidation() {
    // Determine the Contact Type Either Person or Company
    if (this.ContactType == ContactType.TC_PERSON) {
      // Check if at least one field is entered
      if (this.FirstName == null && this.OrganizationName == null && this.Keyword == null && (this.TaxID == null || this.TaxID == "") && (this.ADANumber_TDIC == "" ||
          this.ADANumber_TDIC == null) && this.Phone == null && this.Address.Country == null &&
          this.Address.City == null && this.Address.State == null && this.Address.PostalCode == null) {
        throw new DisplayableException("Person Search Must Have at Least One Field Enter")
      }
    } else {
      // Check if at least one field is entered
      if (this.Keyword == null && (this.TaxID == null || this.TaxID == "") && this.Phone == null && this.Address.Country == null &&
          this.Address.City == null && this.Address.State == null && this.Address.PostalCode == null) {
        throw new DisplayableException("Company Search Must Have at Least One Field Enter")
      }
    }

        //20150401 TJ Talluto US304 - suppressing this error because the Country is currently initialized to US while the Country widget is hidden on the UI
        //throw new DisplayableException("Please Select a State")
    // If the address information is entered, then have the user enter a least one piece of information about the contact.
    if (this.Address.Country != null && this.Address.State != null || this.Address.City != null || this.Address.PostalCode != null) {
      if (this.ContactType == ContactType.TC_PERSON) {
        if (this.FirstName == null && this.OrganizationName == null && this.Keyword == null && !this.TaxID.NotBlank && !this.ADANumber_TDIC.NotBlank && this.Phone == null) {
          throw new DisplayableException("Please Enter either First Name, Last Name, Tax ID (SSN), ADA Number, or Phone Number")
        }
      } else {
        if (this.Keyword == null && !this.TaxID.NotBlank && this.Phone == null) {
          throw new DisplayableException("Please Enter either Company Name, Tax ID (SSN), or Phone Number")
        }
      }
    }

  }



}


