package tdic.pc.config.enhancements.contact

uses java.lang.IllegalArgumentException

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 8/18/14
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement PersonEnhancement : entity.Person {
  /**
   *  Return the ADA Number from the Official ID table
   */
    property get ADANumberOfficialID_TDIC() : String {
    var anOfficialID : OfficialID

    anOfficialID = this.OfficialIDs.firstWhere( \ a -> a.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC)
    return anOfficialID.OfficialIDValue
  }

  /**
  * Set the ADA Number in the Official ID and Person entity
  * */
  property set ADANumberOfficialID_TDIC(value : String) {
    // Grab the number of official ID with the Official ID type of ADA Number
    var anOfficials = this.OfficialIDs.countWhere( \ a -> a.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC )

    // Check if there is more than one official IDs with the official ID Type of ADA Number
    if (anOfficials > 0) {
      if (anOfficials > 1) {
        throw new IllegalArgumentException("Can not have more than one ADA number in the Person Contact")
      }
      this.OfficialIDs.firstWhere( \ a -> a.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC).OfficialIDValue = value
    }
    else {
      // Create a new Official ID object
      var anOfficialID = new OfficialID()
      anOfficialID.OfficialIDType = OfficialIDType.TC_ADANUMBER_TDIC
      anOfficialID.OfficialIDValue = value

      // Add a new Official ID to the OfficialIDs Array
      this.addToOfficialIDs(anOfficialID)
    }
  }
}