package tdic.pc.config.addressverification
/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 8/25/14
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
class ErrorMessageProp {
    public var code : String
    public var displayMessage : String
    public var alternateMessage : String
    public var isStandardized: boolean
    public var doStreetSearch: boolean
    public var isError : boolean

  construct(_code:String, _displayMessage:String, _alternateMessage:String, _isError:boolean, _isStandardized:boolean, _doStreetSearch:boolean){
    code = _code
    displayMessage = _displayMessage
    alternateMessage = _alternateMessage
    isStandardized = _isStandardized
    doStreetSearch = _doStreetSearch
    isError = _isError
  }
}