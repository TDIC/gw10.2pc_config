package tdic.pc.config.addressverification

uses com.tdic.util.properties.PropertyUtil
uses tdic.pc.integ.services.addressverification.service_svc2.types.complex.RequestArray
uses java.util.ArrayList
uses tdic.pc.integ.services.addressverification.service_svc2.anonymous.elements.RequestArray_Record
uses tdic.pc.integ.services.streetsearch.service_svc2.types.complex.Request
uses tdic.pc.integ.services.addressverification.service_svc2.types.complex.ResponseArray
uses gw.xml.ws.WebServiceException
uses org.apache.commons.lang3.text.WordUtils
uses org.apache.commons.lang3.StringUtils
uses org.slf4j.LoggerFactory

/**
 * ID:US553
 * Name: MelissaDataHandler
 * Developer : Praneethk
 * Description: Standardize entered/modified addresses
 * MelissaHandler handles address verification requests from PC/BC/CC/CM.
 * Handler exposes two methods to search a address and get a verified address.
 */

class MelissaDataHandler {

 private var _logger = LoggerFactory.getLogger("Integration")
 private var _errorCodes="" //praneethk, US553 : Variable to hold Error Codes Returned by melissa Service
 private var _returnCode = "AS01"
 private var _malissaid = "MelissaID"

 /**
 * US553: Praneethk
 * Standardize the User Entered Address by Melissa Service
 */
 @Param("Address", "Address Object")
 @Returns("Standardized Address from the Melissa Service")

 public function standardizeAddress(address : Address ):Address{
   /*
    * Creating Address Verification Object
    */
   _logger.debug("MelissaDataHandler#standardizeAddress(Address ) - Entering Address Verification Service")
    var addressVerifyService = new tdic.pc.integ.services.addressverification.service.Service()
    var standardizedAddressList = new ArrayList<Address> ()

    var requestArray = new RequestArray()
   //GINTEG-263: Reading environment specific properties from property files instead of CacheManager
    requestArray.CustomerID = PropertyUtil.getInstance().getProperty(_malissaid)
    /*
     * Create a request array record and add address to it
     */
    var addressRecord = new RequestArray_Record()
    addressRecord.AddressLine1 = address.AddressLine1
    addressRecord.AddressLine2 = address.AddressLine2
    addressRecord.City = address.City
    addressRecord.State= address.State.Code
   if(address.PostalCode.contains("-")){
     addressRecord.Zip = address.PostalCode.split("-")[0]
     addressRecord.Plus4 = address.PostalCode.split("-")[1]
   }
   else{
     addressRecord.Zip = address.PostalCode
   }
    /*
     * Create an Arraylist and Add addressRecord to Arraylist to be consumed by Melissa Service
     */
    var addressList = new ArrayList<RequestArray_Record>()
    addressList.add(addressRecord)
    requestArray.Record = addressList
     /*
      *  Melissa Address Standardization.
      */
   var result:ResponseArray
   try{
     result = addressVerifyService.doAddressCheck(requestArray)
   }
       catch(var e:WebServiceException){
         _logger.error("Unable to connect to Address Verification System.Connection Timed Out. Override the address.")
         _errorCodes = "NIC"
         return new Address()
       }
     /*
      * Extarct the First Record from the result and convert it from  melissa address
      * object to GW address object
      */
    var standardizedAddressMelissaObject = result.Record.first().Address
    var standardizedAddress = new Address()
    standardizedAddress.AddressLine1 =standardizedAddressMelissaObject.Address1
    standardizedAddress.AddressLine2 = standardizedAddressMelissaObject.Suite.equals(" ")?standardizedAddressMelissaObject.PrivateMailBox:standardizedAddressMelissaObject.Suite
    standardizedAddress.City = standardizedAddressMelissaObject.City.Name
    standardizedAddress.State = State.get(standardizedAddressMelissaObject.State.Abbreviation)
    standardizedAddress.PostalCode = standardizedAddressMelissaObject.Zip+ "-" +standardizedAddressMelissaObject.Plus4
     /*
      * Error Codes Returned by Melissa Service
      */
    _errorCodes = result.Record.first().Results
   _logger.debug("MelissaDataHandler#standardizeAddress(Address ) - Exiting Address Verification Service")
   if(_errorCodes.contains(_returnCode) and address.AddressLine1==standardizedAddress.AddressLine1 and address.AddressLine2==standardizedAddress.AddressLine2 and
      address.City==standardizedAddress.City and address.State==standardizedAddress.State and address.PostalCode==standardizedAddress.PostalCode){
      _errorCodes = _returnCode
   }
    return standardizedAddress
 }
 /**
  * US553: Praneethk
  * Return Error Codes Sent from melissa service.
  */
 @Returns("String of Error Codes")
 public function getErrorCodes(): String {
   return _errorCodes
 }
 /**
  * US553: Praneethk
  * After Standardizing, if Street Search is Required Further to Standardizing the Address
  * doStreetSearch will be called and Suggested Addresses are populated.
  */
 @Param("Address", "Address Object")
 @Returns("An Array  List of Addresses Suggested by Melissa Street Search")
 public function doStreetSearch(address: Address): ArrayList<Address>{
   /*
    * Creating Street Search Object
    */
   _logger.debug("MelissaDataHandler#doStreetSearch(Address) - Entering Address Verification Service for Street Search")
    var streetSearchService = new tdic.pc.integ.services.streetsearch.service.Service()
    var suggestedAddressList = new ArrayList<Address> ()
    /*
     * Create Street Search Request Object
     */
    var streetSearchRequest = new Request()
    /*
     * Populate Street Search Request Object With the passed Address
     */

   //GINTEG-263: Reading environment specific properties from property files instead of CacheManager
    streetSearchRequest.CustomerID = PropertyUtil.getInstance().getProperty(_malissaid)
    streetSearchRequest.AddressLine = address.AddressLine1
    streetSearchRequest.City = address.City
    streetSearchRequest.State = (address.State) as String
    streetSearchRequest.Zip = address.PostalCode
    streetSearchRequest.OptInRangeOnly = "True"
    /*
     * Street Search Functionality
     */
    var streetSearchResult = streetSearchService.doStreetSearch(streetSearchRequest)
    /*
     * Convert all the addresses from  melissa address
     * object format to GW address object format
     */
    for(streetSearchRecord in streetSearchResult.StreetRecord) {
      var tempAddress = new Address()
      var streetLowRange:String = streetSearchRecord.PrimaryRange.Low
      var streetHighRange:String = streetSearchRecord.PrimaryRange.High
      var street:String = streetSearchRecord.Street.PreDirection+" "+streetSearchRecord.Street.Name+" "+streetSearchRecord.Street.Suffix +" "+streetSearchRecord.Street.PostDirection
      if (streetLowRange.equals(streetHighRange)){
        street = streetLowRange+" "+street
      }
      else{
        street = streetLowRange+"-"+streetHighRange+" "+street
      }

      var suiteLowRange:String = streetSearchRecord.Suite.Low
      var suiteHighRange:String = streetSearchRecord.Suite.High
      var suite:String = streetSearchRecord.Suite.Name
      if(suiteLowRange.equals(suiteHighRange)){
        suite += " "+suiteLowRange
      }
      else{
        suite += " "+suiteLowRange+"-"+suiteHighRange
      }

      tempAddress.AddressLine1 = WordUtils.capitalizeFully(street.replaceAll(" +"," "))
      
      var suiteSplit = suite.split(" ")
      if(suiteSplit.length>0){
        suiteSplit[0] = WordUtils.capitalizeFully(suiteSplit[0])
        tempAddress.AddressLine2 = StringUtils.join(suiteSplit," ")
      }
      
      tempAddress.City = WordUtils.capitalizeFully(address.City)
      tempAddress.State = address.State
      tempAddress.PostalCode = streetSearchRecord.Zip.Zip5 + "-" +streetSearchRecord.Zip.Plus4High
      suggestedAddressList.add(tempAddress)
    }
   _logger.debug("MelissaDataHandler#doStreetSearch(Address) - Exiting Address Verification Service for Street Search with suggested Address List")
   return suggestedAddressList
 }
}