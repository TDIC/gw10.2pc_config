package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/11/14
 * Time: 5:09 PM
 * This class includes variables for all fields in Report header record for policy specification report.
 */
class TDIC_WCpolsReportHeaderFields extends  TDIC_FlatFileLineGenerator{
  var _label : String as Label
  var _dataProviderEmail : String as DataProviderEmail
  var _recordTypeCode : String as RecordTypeCode
  var _dataTypeCode : String as DataTypeCode
  var _dataReceiverCode : int as DataReceiverCode
  var _dateOfDataTransmission : String as DateOfDataTransmission
  var _versionIdentifier : String as VersionIdentifier
  var _submissionTypeCode : String as SubmissionTypeCode
  var _submissionReplacementIdentifier : String as SubmissionReplacementIdentifier
  var _dataProviderCode : int as DataProviderCode
  var _nameOfDataProviderContact : String as NamOfDataProvider
  var _electronicOrPaperReceiptCode : String as ElectronicOrPaperReceiptCode
  var _phoneNumber : long as PhoneNumber
  var _phoneNumberExtension : String as PhoneNumberExtension
  var _faxNumber : long as FaxNumber
  var _processedDate : String as ProcessedDate
  var _dataProviderStreet : String as DataProviderStreet
  var _dataProviderCity : String as DataProviderCity
  var _dataProviderState : String as DataProviderState
  var _dataProviderZipCode : String as DataProviderZipCode
  var _datProviderTypeCode : String as DataProviderTypeCode
  var _thirdPartyEntityFEINNumber : String as ThirdPartyEntityFEINNumber
  var _futureReserved : String as FutureReserved
  var _futureReserved1 : String as FutureReserved1
}