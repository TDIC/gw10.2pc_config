package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsAddressFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.TDIC_WCpolsAddressFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.TDIC_WCpolsAddressFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.TDIC_WCpolsAddressFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.TDIC_WCpolsAddressFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddressfieldsmodel.TDIC_WCpolsAddressFields(object, options)
  }

}