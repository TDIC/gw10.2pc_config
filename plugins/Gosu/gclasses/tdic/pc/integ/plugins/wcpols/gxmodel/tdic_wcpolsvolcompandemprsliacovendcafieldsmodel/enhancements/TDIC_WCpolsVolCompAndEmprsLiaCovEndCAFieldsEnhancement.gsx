package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsvolcompandemprsliacovendcafieldsmodel.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields(object, options)
  }

}