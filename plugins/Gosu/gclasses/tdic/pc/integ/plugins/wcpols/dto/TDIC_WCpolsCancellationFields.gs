package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 1:50 PM
 * This class includes variables for all fields in cancellation record for policy specification report.
 */
class TDIC_WCpolsCancellationFields  extends  TDIC_FlatFileLineGenerator{
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _cancellationCode : int as CancellationCode
  var _cancellationTypeCode : int as CancellationTypeCode
  var _reasonForCancCode : int as ReasonForCancCode
  var _reinstatementTypeCode : int as ReinstatementTypeCode
  var _nameOfInsured : String as NameOfInsured
  var _addressOfInsured : String as AddressOfInsured
  var _natureOfInsured : String as NatureOfInsured
  var _cancMailedToInsuredDate : String as CancMailedToInsuredDate
  var _cancSeqNum : int as CancSeqNum
  var _reasonForReinstatementCode : String as ReasonForReinstatementCode
  var _futureReserved1 : String as FutureReserved1
  var _correspondingCancEffDate : String as CorrespondingCancEffDate
  var _cancEffDate : String as CancEffDate
  var _futureReserved2 : String as FutureReserved2
}