package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsClassAndOrRateChgAndOthEndRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsclassandorratechgandothendrecfieldsmodel.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields(object, options)
  }

}