package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsReportTrailerFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.TDIC_WCpolsReportTrailerFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportTrailerFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.TDIC_WCpolsReportTrailerFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.TDIC_WCpolsReportTrailerFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportTrailerFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.TDIC_WCpolsReportTrailerFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreporttrailerfieldsmodel.TDIC_WCpolsReportTrailerFields(object, options)
  }

}