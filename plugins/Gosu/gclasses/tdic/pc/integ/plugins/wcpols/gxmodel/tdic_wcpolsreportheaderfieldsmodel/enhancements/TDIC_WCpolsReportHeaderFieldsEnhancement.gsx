package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsReportHeaderFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.TDIC_WCpolsReportHeaderFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportHeaderFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.TDIC_WCpolsReportHeaderFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.TDIC_WCpolsReportHeaderFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportHeaderFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.TDIC_WCpolsReportHeaderFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreportheaderfieldsmodel.TDIC_WCpolsReportHeaderFields(object, options)
  }

}