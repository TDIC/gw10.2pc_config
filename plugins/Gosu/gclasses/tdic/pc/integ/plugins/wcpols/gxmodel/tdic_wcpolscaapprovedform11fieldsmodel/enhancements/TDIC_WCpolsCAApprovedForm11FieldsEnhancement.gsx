package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsCAApprovedForm11FieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.TDIC_WCpolsCAApprovedForm11Fields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm11Fields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.TDIC_WCpolsCAApprovedForm11Fields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.TDIC_WCpolsCAApprovedForm11Fields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm11Fields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.TDIC_WCpolsCAApprovedForm11Fields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform11fieldsmodel.TDIC_WCpolsCAApprovedForm11Fields(object, options)
  }

}