package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 8:39 AM
 * This class includes variables for all fields in policy header record for policy specification report.
 */
class TDIC_WCpolsPolicyHeaderFields  extends  TDIC_FlatFileLineGenerator {
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _futureReserved1 : String as FutureReserved1
  var _recordTypeCode : String as RecordTypeCode
  var _experienceRatingCode : String as ExperienceRatingCode
  var _interstateRiskIdNumber : String as InterstateRiskIdNumber
  var _policyExpirationDate : String as PolicyExpirationDate
  var _thirdPartyEntityFEIN : String as ThirdPartyEntityFEIN
  var _typeOfCoverageIdCode : int as TypeOfCoverageIdCode
  var _employeeLeasingPolicyTypeCode : int as EmployeeLeasingPolicyTypeCode
  var _policyTermCode : int as PolicyTermCode
  var _priorPolicyNumberIdentifier : String as PriorPolicyNumberIdentifier
  var _priorUnitCertificateIdentifier : String as PriorUnitCertificateIdentifier
  var _futureReserved2 : String as FutureReserved2
  var _legalNatureOfInsuredCode : int as LegalNatureOfInsuredCode
  var _typeOfPlanIdCode : String as TypeOfPlanIDCode
  var _wrapUpOCIPCode : int as WrapUpOCIPCode
  var _businessSegmentIdentifier : String as BusinessSegmentIdentifier
  var _policyMinimumPremiumAmount : int as PolicyMinimumPremiumAmount
  var _policyMinimumPremiumStateCode : int as PolicyMinimumPremiumStateCode
  var _policyEstimatedStandardPremiumTotal : long as PolicyEstimatedStandardPremiumTotal
  var _policyDepositPremiumAmount : int as PolicyDepositPremiumAmount
  var _auditFrequencyCode : int as AuditFrequencyCode
  var _billingFrequencyCode : int as BillingFrequencyCode
  var _retrospectiveRatingCode : String as RetrospectiveRatingCode
  var _emprLiabLimAmntBodInjByAcciEachAcciAmnt : long as EmprLiabLimAmntBodInjByAcciEachAccAmnt
  var _emprLiabLimAmntBodInjByDisePolLimAmnt : long as EmprLiabLimAmntBodInjByDisePolAmnt
  var _emprLiabLimAmntBodInjByDiseEachEmpeAmnt : long as EmprLiabLimAmntBodInjByDiseEachEmpeAmnt
  var _nameOfProducer : String as NameOfproducer
  var _assignedRiskBinderNumber : String as AssignedRiskBinderNumber
  var _groupCovStatusCode : int as GroupCovStatusCode
  var _futureReserved3 : String as FutureReserved3
  var _originalCarrierCode : int as OriginalCarrierCode
  var _originalPolicyNumberIdentifier : String as OriginalPolicyNumberIdentifier
  var _originalPolicyEffectiveDate : int as OriginalPolicyEffectiveDate
  var _textForOtherlegNatOfInsrd : String as TextForOtherLegNatOfInsrd
  var _assignmentDate : String as AssignmentDate
  var _assignedRiskBinderNum : String as AssignedRiskBinderNum
  var _futureReserved4 : String as FutureReserved4
  var _polChngEffDate : String as PolChngEffDate
  var _polChngExpDate : String as PolChngExpDate
}