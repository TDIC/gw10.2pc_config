package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsReinstatementFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.TDIC_WCpolsReinstatementFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReinstatementFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.TDIC_WCpolsReinstatementFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.TDIC_WCpolsReinstatementFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReinstatementFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.TDIC_WCpolsReinstatementFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsreinstatementfieldsmodel.TDIC_WCpolsReinstatementFields(object, options)
  }

}