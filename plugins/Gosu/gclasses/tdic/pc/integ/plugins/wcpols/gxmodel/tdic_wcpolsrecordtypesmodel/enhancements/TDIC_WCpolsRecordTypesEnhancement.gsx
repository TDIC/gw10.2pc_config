package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsRecordTypesEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsRecordTypes) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsRecordTypes, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes(object, options)
  }

}