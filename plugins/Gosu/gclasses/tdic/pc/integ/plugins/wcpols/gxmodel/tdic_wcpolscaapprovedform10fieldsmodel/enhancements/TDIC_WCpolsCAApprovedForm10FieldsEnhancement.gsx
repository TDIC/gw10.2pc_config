package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsCAApprovedForm10FieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.TDIC_WCpolsCAApprovedForm10Fields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm10Fields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.TDIC_WCpolsCAApprovedForm10Fields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.TDIC_WCpolsCAApprovedForm10Fields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm10Fields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.TDIC_WCpolsCAApprovedForm10Fields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscaapprovedform10fieldsmodel.TDIC_WCpolsCAApprovedForm10Fields(object, options)
  }

}