package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsDataElementsChgEndRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.TDIC_WCpolsDataElementsChgEndRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsDataElementsChgEndRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.TDIC_WCpolsDataElementsChgEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.TDIC_WCpolsDataElementsChgEndRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsDataElementsChgEndRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.TDIC_WCpolsDataElementsChgEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsdataelementschgendrecfieldsmodel.TDIC_WCpolsDataElementsChgEndRecFields(object, options)
  }

}