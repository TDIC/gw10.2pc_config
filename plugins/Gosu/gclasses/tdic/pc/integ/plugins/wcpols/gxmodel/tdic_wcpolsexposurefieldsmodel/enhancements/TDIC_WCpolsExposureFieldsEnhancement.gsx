package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsExposureFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.TDIC_WCpolsExposureFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExposureFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.TDIC_WCpolsExposureFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.TDIC_WCpolsExposureFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExposureFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.TDIC_WCpolsExposureFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexposurefieldsmodel.TDIC_WCpolsExposureFields(object, options)
  }

}