package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsStatePremiumChangeRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.TDIC_WCpolsStatePremiumChangeRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumChangeRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.TDIC_WCpolsStatePremiumChangeRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.TDIC_WCpolsStatePremiumChangeRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumChangeRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.TDIC_WCpolsStatePremiumChangeRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumchangerecfieldsmodel.TDIC_WCpolsStatePremiumChangeRecFields(object, options)
  }

}