package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsCancellationFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.TDIC_WCpolsCancellationFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCancellationFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.TDIC_WCpolsCancellationFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.TDIC_WCpolsCancellationFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCancellationFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.TDIC_WCpolsCancellationFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolscancellationfieldsmodel.TDIC_WCpolsCancellationFields(object, options)
  }

}