package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsStatePremiumFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.TDIC_WCpolsStatePremiumFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.TDIC_WCpolsStatePremiumFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.TDIC_WCpolsStatePremiumFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.TDIC_WCpolsStatePremiumFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsstatepremiumfieldsmodel.TDIC_WCpolsStatePremiumFields(object, options)
  }

}