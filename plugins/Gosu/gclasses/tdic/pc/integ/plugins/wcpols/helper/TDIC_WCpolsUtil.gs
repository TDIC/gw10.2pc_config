package tdic.pc.integ.plugins.wcpols.helper


uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.productmodel.ScheduleStringPropertyInfo
uses gw.api.util.DateUtil
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsRecordTypes
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_EndorsementNumberFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm10Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm11Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCancellationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsDataElementsChgEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsEndorsementIdentificationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExpRatingModChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExposureFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPolicyHeaderFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReinstatementFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportHeaderFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportTrailerFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumChangeRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields
uses java.lang.Math
uses java.math.BigDecimal
uses java.text.DateFormat
uses java.text.SimpleDateFormat
uses java.util.ArrayList
uses gw.lob.wc7.rating.WC7RatingPeriod
uses gw.api.diff.DiffItem
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffRemove
uses java.util.List
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/1/14
 * Time: 7:30 PM
 * This class implements functionality for assigning values to fields for all the record types.
 */
class TDIC_WCpolsUtil {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")
  final static var blank = ' '
  final static var carrierCode = 74419
  final static var label = '$!+WORKCOMP+!$'
  final static var dataTypeCode = 'WCP'
  final static var dataReceiverCode = 00004
  final static var versionIdentifier = 'V01'
  final static var dataProviderTypeCode = 'C'
  final static var policyRecordTypeCode = '01'
  final static var typeOfCoverageIdCode = 01
  final static var empLeasingPolicyTypeCode = 1
  final static var wrapUpOCIPCode = 2
  final static var polTermCode = 1
  final static var polMinPremStateCode = 04
  final static var polDepPremAmnt = 0000000000
  final static var auditFreqCode = 1
  final static var billingFreqCode = 1
  final static var groupCovStatusCode = 0
  final static var originalCarrierCode = 00000
  final static var originalPolEffDate = 000000
  final static var nameRecTypeCode = '02'
  final static var nameLinkIdentifier = 001
  final static var nameLinkCntrIdentifier = '00'
  final static var nameType = ' (Partner)'
  final static var nameTypeDBA = ' (dba)'
  final static var nameTypeCodeForDBA = 2
  final static var fedEmpIdValueForNoFEIN = '000000000'
  final static var addressRecTypeCode = '03'
  final static var foreignAddressInd = 'N'
  final static var addressStructureCode = 1
  final static var addressNameLinkCntrIdentifier = '00'
  final static var addressTypeCode = 2
  final static var addressNameLinkIdentifier = 001
  final static var stateCodeLink = 27
  final static var expRecLinkForLocCode = 00001
  final static var stateCode = 04
  final static var statePremRecTypeCode = '04'
  final static var stateCovCarrierCode = 00000
  final static var expModStatusCode = 1
  final static var expModStatusCodeWithOutXmod = 3
  final static var insurerPremDeviationFact = 1000
  final static var typeOfPremDevCode = 0
  final static var lossConstantAmnt = 0000000000
  final static var premAdjPeriodCode = 1
  final static var premDiscountAmnt = 0000000000
  final static var expRecordTypeCode = '05'
  final static var expPeriodEffDate = 000000
  final static var expPeriodCode = 1
  final static var expNameLinkIdentifier = 001
  final static var expStateCodeLink = 27
  final static var expRecLinkForExpCode = 00001
  final static var expNameLinkCounterIdentifier = '00'
  final static var endIdenRecTypeCode = '07'
  final static var reinsRecTypeCode  = '08'
  final static var reinsCode  = 2
  final static var reinsCancTypeCode  = 0
  final static var reasonForCancCode  = 0
  final static var reinstatementTypeCode  = 0
  final static var cancRecTypeCode  = '08'
  final static var cancCode  = 1
  final static var partnerRecTypeCode  = 'DB'
  final static var partnerEndNumber  = 'WC040302'
  final static var officerRecTypeCode  = 'DC'
  final static var officerEndNumber  = 'WC040303'
  final static var volRecTypeCode = 'DD'
  final static var volEndNumber = 'WC040305'
  final static var waiverRecTypeCode  = 'DE'
  final static var waiverEndNumber  = 'WC040306'
  final static var endSeqNum  = 01
  final static var recTypeCode  = '99'
  final static var caf10RecTypeCode  = 'DM'
  final static var caf10EndNumber  = 'WC0010CA'
  final static var caf11RecTypeCode = 'DN'
  final static var caf11EndNumber  = 'WC0011CA'
  final static var polNumPrefix = 'CAWC'
  final static var endorsementTransactionCode = 03
  final static var expModFactChgRecTypeCode = '10'
  final static var expModFactChgEndNum = 'WC890406'
  final static var statePremChgRecTypeCode = '84'
  final static var statePremSchRatChgEndNum = 'WC840402'
  final static var statepremEstStateStdPremTotChgEndNum = 'WC840405'
  final static var suppDataElemChgEndRec = '85'
  final static var classAndOrRateChgEndRecTypeCode = '86'
  final static var classAndOrRateChgEndNum = 'WC890415'
  final static var dataElementsChgEndRecType = '87'
  final static var datElementChgEndNumOrgType = 'WC890610'
  final static var dataElementChgEndNumFormNum = 'WC890614'
  final static var revisedCarrierCode = '00000'
  final static var revisedPolicyEffDate = '000000'
  final static var revisedPolicyExpDate = '000000'
  final static var endSeqNumDataElemRec = '01'
  final static var nameChangeRecTypeCode = '88'
  final static var nameChangeEndNum = 'WC890601'
  final static var revCodeForAdd = 'A'
  final static var revCodeForDelete = 'D'
  final static var addressChangeRecTypeCode = '89'
  final static var addressChangeEndNumMailing = 'WC890605'
  final static var addressChangeEndNumLocation = 'WC890608'
  final static var insuredMailAddChgAddTypeCode = 1
  final static var locAddChgAddTypeCode = 2
  final static var endBureauVerIdntfr = 'B'
  final static var endCarrierVerIdntfr = 'WC890600B'
  final static var endNumForVolComp = 'WC040305'
  final static var endNumForWaiverOfSubro = 'WC040306'
  final static var endNumForAnnivRatingDate = 'WC040401'
  final static var endNumForSolePropCov = 'WC040304'

//GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
  final static var submissionTypeCode = PropertyUtil.getInstance().getProperty("typeCodeOfSubmission")
  final static var dataProviderEmail = PropertyUtil.getInstance().getProperty("dataProviderEmailId")
  final static var nameOfDataProvider = PropertyUtil.getInstance().getProperty("dataProviderName")
  final static var phoneNumber = PropertyUtil.getInstance().getProperty("dataProviderPhoneNumber")
  final static var faxNumber = PropertyUtil.getInstance().getProperty("dataProviderFaxNumber")
  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail")
  public static final var WC_STAT_POL_NOTIFICATION_EMAIL : String = PropertyUtil.getInstance().getProperty("WCSTATPOLNotificationEmail")

  /**
   * Creates payload with values for different fields of various record types.
   **/
  public static function createPayload (pp : PolicyPeriod ) : String{
    var _payload : String
    var _wcpolsRecord = new TDIC_WCpolsRecordTypes()
    var policyPeriod  = pp.getSlice(pp.EditEffectiveDate)

    if(!(policyPeriod.Job typeis Cancellation) and !(policyPeriod.Job typeis Reinstatement) and !(policyPeriod.Job typeis PolicyChange)){

    _wcpolsRecord.PolicyHeaderRecord = createPolicyReportHeaderFields(policyPeriod)

    _wcpolsRecord.NameRecord = new ArrayList<TDIC_WCpolsNameFields>()
    policyPeriod.NamedInsureds.each( \ elt -> createNameRecords(_wcpolsRecord.NameRecord,elt,policyPeriod))

    if(policyPeriod.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP){
      policyPeriod.WC7Line.WC7PolicyOwnerOfficers.each( \ elt -> createNameRecords(_wcpolsRecord.NameRecord,elt,policyPeriod))
    }

    policyPeriod.PolicyLocations.each( \ elt -> {
      if(elt.DBA_TDIC != null){
        createNameRecords(_wcpolsRecord.NameRecord,elt,policyPeriod)
      }
    })

    _wcpolsRecord.AddressRecord = new ArrayList<TDIC_WCpolsAddressFields>()
    createAddressRecords(_wcpolsRecord.AddressRecord,policyPeriod)

    _wcpolsRecord.StatePremiumRecord = createStateRecords(policyPeriod)

    _wcpolsRecord.ExposureRecord = createExposureRecords(policyPeriod)

    _wcpolsRecord.EndorsementIdentificationRecord = createEndorsementIdentificationRecord(policyPeriod)

    if(policyPeriod.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP)_wcpolsRecord.PartnershipCovOrExclusionEndCARecord = createPartnerRecords(policyPeriod)

    if(policyPeriod.PolicyOrgType_TDIC != PolicyOrgType_TDIC.TC_PARTNERSHIP)_wcpolsRecord.OfficersAndDirectorsCovOrExcCARecord = createOfficersOrDirectorsRecords(policyPeriod)

    _wcpolsRecord.VolCompAndEmprsLiaCovEndCARecord = createVolCompAndEmprsLiaCovEndCAFieldsRecords(policyPeriod)

    _wcpolsRecord.WaiOfOurRightToRecFromOthsEndCARecord = createWaiverofRightRecords(policyPeriod)
    }

    else if(policyPeriod.Job typeis PolicyChange) {

     var changeList : List
     var primaryMailAddr = 0
     var primNamInsrd = 0
     var polLocation = 0
     changeList = policyPeriod.BasedOn.getDiffItemsForDiffReason(DiffReason.TC_COMPAREJOBS,policyPeriod)
     _wcpolsRecord.AddressChangeEndRecord = new ArrayList<TDIC_WCpolsAddressChangeEndRecFields>()
     _wcpolsRecord.NameChangeEndRecord = new ArrayList<TDIC_WCpolsNameChangeEndRecFields>()
     _wcpolsRecord.DataElementsChangeEndRecord = new ArrayList<TDIC_WCpolsDataElementsChgEndRecFields>()
     _wcpolsRecord.StatePremiumChangeRecord = new ArrayList<TDIC_WCpolsStatePremiumChangeRecFields>()
     _wcpolsRecord.ExpRatingModChangeEndRecord = new ArrayList<TDIC_WCpolsExpRatingModChangeEndRecFields>()
     _wcpolsRecord.ClassAndOrRateChgAndOthEndRec = new ArrayList<TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields>()

      changeList.each( \ elt -> {
        // For Primary insured mailing address change.
        if((elt as DiffItem).Bean typeis PolicyAddress){
          if(primaryMailAddr ==0){
            createAddressChangeEndRecords(_wcpolsRecord.AddressChangeEndRecord,policyPeriod)
            primaryMailAddr++
          }
        }
        //For policy location change.
        else if((elt as DiffItem).Bean typeis PolicyLocation){
          var changedLocValue = (elt as DiffItem).Bean as PolicyLocation
          if(elt as DiffItem typeis DiffAdd){
            createLocationAddressChangeEndRecords(_wcpolsRecord.AddressChangeEndRecord,changedLocValue,policyPeriod,revCodeForAdd)
          }
          else if(elt as DiffItem typeis DiffRemove){
            createLocationAddressChangeEndRecords(_wcpolsRecord.AddressChangeEndRecord,changedLocValue,policyPeriod,revCodeForDelete)
          }
          else if(elt as DiffItem typeis DiffProperty){
            // For dba change
            if((elt as DiffProperty).PropertyInfo.Name=="DBA_TDIC") {
              if(changedLocValue.BasedOn.DBA_TDIC == null){
                createDbaNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedLocValue,policyPeriod,revCodeForAdd)
              }
              else if(changedLocValue.DBA_TDIC == null){
                createDbaNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedLocValue.BasedOn,policyPeriod,revCodeForDelete)
              }
              else if(changedLocValue.DBA_TDIC != null and changedLocValue.BasedOn.DBA_TDIC != null){
                createDbaNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedLocValue.BasedOn,policyPeriod,revCodeForDelete)
                createDbaNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedLocValue,policyPeriod,revCodeForAdd)
              }
            }
            else if(((elt as DiffProperty).PropertyInfo.Name=="AddressLine1Internal") or ((elt as DiffProperty).PropertyInfo.Name=="AddressLine2Internal")
               or ((elt as DiffProperty).PropertyInfo.Name=="CityInternal") or ((elt as DiffProperty).PropertyInfo.Name=="PostalCodeInternal")) {
              if(polLocation == 0){
                createLocationAddressChangeEndRecords(_wcpolsRecord.AddressChangeEndRecord,changedLocValue.BasedOn,policyPeriod,revCodeForDelete)
                createLocationAddressChangeEndRecords(_wcpolsRecord.AddressChangeEndRecord,changedLocValue,policyPeriod,revCodeForAdd)
                polLocation++
              }
            }
          }
        }

        //For primary insured name change.
        else if((elt as DiffItem).Bean typeis PolicyPriNamedInsured){
          if(primNamInsrd == 0){
            createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,policyPeriod.BasedOn.PrimaryNamedInsured,policyPeriod,revCodeForDelete)
            createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,policyPeriod.PrimaryNamedInsured,policyPeriod,revCodeForAdd)
            primNamInsrd++
          }
        }

        //For Additional insured name change.
        else if((elt as DiffItem).Bean typeis PolicyAddlNamedInsured){
          var changedAddlInsured = (elt as DiffItem).Bean as PolicyAddlNamedInsured
          if(elt as DiffItem typeis DiffAdd){
            createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedAddlInsured,policyPeriod,revCodeForAdd)
          }
          else if(elt as DiffItem typeis DiffRemove){
            createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedAddlInsured,policyPeriod,revCodeForDelete)
          }
          else if(elt as DiffItem typeis DiffProperty){
            if(((elt as DiffProperty).PropertyInfo.Name=="FirstNameInternal") or ((elt as DiffProperty).PropertyInfo.Name=="LastNameInternal")
              or ((elt as DiffProperty).PropertyInfo.Name=="MiddleNameInternal_TDIC") or ((elt as DiffProperty).PropertyInfo.Name=="TaxIDInternal_TDIC")) {
              createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedAddlInsured.BasedOn,policyPeriod,revCodeForDelete)
              createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedAddlInsured,policyPeriod,revCodeForAdd)
            }
          }
        }

        // For partner/officer/director change.
        else if((elt as DiffItem).Bean typeis WC7PolicyOwnerOfficer){
          var changedPolOwnOffi = (elt as DiffItem).Bean as WC7PolicyOwnerOfficer
          if(policyPeriod.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP){
            if(elt as DiffItem typeis DiffAdd){
              createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedPolOwnOffi,policyPeriod,revCodeForAdd)
            }
            if(elt as DiffItem typeis DiffRemove){
              createNameChangeEndRecord(_wcpolsRecord.NameChangeEndRecord,changedPolOwnOffi,policyPeriod,revCodeForDelete)
            }
            if(elt as DiffItem typeis DiffProperty){
              if((elt as DiffProperty).PropertyInfo.Name == "isExcluded_TDIC") {
                if(policyPeriod.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP){
                  _wcpolsRecord.PartnershipCovOrExclusionEndCARecord = createPartnerRecords(policyPeriod)
                }
              }
            }
          }
          else if(policyPeriod.PolicyOrgType_TDIC != PolicyOrgType_TDIC.TC_PARTNERSHIP){
            if(elt as DiffItem typeis DiffProperty){
              if((elt as DiffProperty).PropertyInfo.Name == "isExcluded_TDIC") {
                _wcpolsRecord.OfficersAndDirectorsCovOrExcCARecord = createOfficersOrDirectorsRecords(policyPeriod)
              }
            }
          }
        }
        // For partner/officer/director exclusion or coverage.
        else if ((elt as DiffItem).Bean typeis WC7WorkersCompExcl){
          if(policyPeriod.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP){
            var endNumForPartnerExcl = 'WC040302'
            var bureauVerIdForPartner : String = blank
            if(elt as DiffItem typeis DiffAdd){
              var forms = policyPeriod.NewlyAddedForms
              forms.each( \ endForm ->{
                if(endForm.FormPatternCode.substring(0,8) == "WC040302"){
                  endNumForPartnerExcl = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
                  if (endNumForPartnerExcl.length > 8){
                    bureauVerIdForPartner =  endNumForPartnerExcl.charAt(8)
                  }
                  else {
                    bureauVerIdForPartner = blank
                  }
                }
              })
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForPartnerExcl,bureauVerIdForPartner,revCodeForAdd)
            }
            else if(elt as DiffItem typeis DiffRemove){
              var forms = policyPeriod.BasedOn.NewlyAddedForms
              forms.each( \ endForm ->{
                if(endForm.FormPatternCode.substring(0,8) == "WC040302"){
                  endNumForPartnerExcl = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
                  if (endNumForPartnerExcl.length > 8){
                    bureauVerIdForPartner =  endNumForPartnerExcl.charAt(8)
                  }
                  else {
                    bureauVerIdForPartner =  bureauVerIdForPartner.charAt(8)
                  }
                }
              })
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForPartnerExcl,bureauVerIdForPartner,revCodeForDelete)
            }
          }

          else if(policyPeriod.PolicyOrgType_TDIC != PolicyOrgType_TDIC.TC_PARTNERSHIP){
            var endNumForDirOrOffiExcl = 'WC040303'
            var bureauVerIdForOffiOrDir : String = blank
            if(elt as DiffItem typeis DiffAdd){
              var forms = policyPeriod.NewlyAddedForms
              forms.each( \ endForm ->{
                if(endForm.FormPatternCode.substring(0,8) == "WC040303"){
                  endNumForDirOrOffiExcl = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
                  if(endNumForDirOrOffiExcl.length > 8){
                    bureauVerIdForOffiOrDir =  endNumForDirOrOffiExcl.charAt(8)
                  }
                  else {
                    bureauVerIdForOffiOrDir = blank
                  }
                }
              })
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForDirOrOffiExcl,bureauVerIdForOffiOrDir,revCodeForAdd)
            }
            else if(elt as DiffItem typeis DiffRemove){
              var forms = policyPeriod.BasedOn.NewlyAddedForms
              forms.each( \ endForm ->{
                if(endForm.FormPatternCode.substring(0,8) == "WC040303"){
                  endNumForDirOrOffiExcl = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
                  if(endNumForDirOrOffiExcl.length > 8){
                    bureauVerIdForOffiOrDir =  endNumForDirOrOffiExcl.charAt(8)
                  }
                  else {
                    bureauVerIdForOffiOrDir = blank
                  }
                }
              })
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForDirOrOffiExcl,bureauVerIdForOffiOrDir,revCodeForDelete)
            }
          }
        }

        // For voluntary compensation employers liability inclusion or exclusion.
        else if((elt as DiffItem).Bean typeis WC7LineScheduleCond){
          if(elt as DiffItem typeis DiffAdd){
            createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForVolComp,blank,revCodeForAdd)
            _wcpolsRecord.VolCompAndEmprsLiaCovEndCARecord = createVolCompAndEmprsLiaCovEndCAFieldsRecords(policyPeriod)
          }
          else if(elt as DiffItem typeis DiffRemove){
            createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForVolComp,blank,revCodeForDelete)
            _wcpolsRecord.VolCompAndEmprsLiaCovEndCARecord = createVolCompAndEmprsLiaCovEndCAFieldsRecords(policyPeriod)
          }
        }

        // For voluntary compensation employers liability change.
        else if((elt as DiffItem).Bean typeis WC7LineScheduleCondItem){
          _wcpolsRecord.VolCompAndEmprsLiaCovEndCARecord = createVolCompAndEmprsLiaCovEndCAFieldsRecords(policyPeriod)
        }

        // For waiver of subrogation change.
        else if((elt as DiffItem).Bean typeis WC7WaiverOfSubro){
          _wcpolsRecord.WaiOfOurRightToRecFromOthsEndCARecord = createWaiverofRightRecords(policyPeriod)
        }

        // For waiver of subrogation inclusion or exclusion.
        else if((elt as DiffItem).Bean typeis WC7WorkersCompCond){
          if(elt as DiffItem typeis DiffAdd){
            if(policyPeriod.WC7Line.WC7TDICAnniversaryRatingDateEndorsementExists and !(policyPeriod.BasedOn.WC7Line.WC7TDICAnniversaryRatingDateEndorsementExists)){
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForAnnivRatingDate,blank,revCodeForAdd)
            }
            else if(policyPeriod.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists and !(policyPeriod.BasedOn.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists)){
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForWaiverOfSubro,blank,revCodeForAdd)
            }
            else if(policyPeriod.WC7Line.WC7TDICSoleProprietorCoverageEndorsementExists and !(policyPeriod.BasedOn.WC7Line.WC7TDICSoleProprietorCoverageEndorsementExists)) {
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForSolePropCov,blank,revCodeForAdd)
            }
          }
          else if(elt as DiffItem typeis DiffRemove){
            if(!(policyPeriod.WC7Line.WC7TDICAnniversaryRatingDateEndorsementExists) and policyPeriod.BasedOn.WC7Line.WC7TDICAnniversaryRatingDateEndorsementExists){
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForAnnivRatingDate,blank,revCodeForDelete)
            }
            else if(!(policyPeriod.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists) and policyPeriod.BasedOn.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExists){
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForWaiverOfSubro,blank,revCodeForDelete)
            }
            else if(!(policyPeriod.WC7Line.WC7TDICSoleProprietorCoverageEndorsementExists) and policyPeriod.BasedOn.WC7Line.WC7TDICSoleProprietorCoverageEndorsementExists) {
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,dataElementChgEndNumFormNum,policyPeriod,endNumForSolePropCov,blank,revCodeForDelete)
            }
          }
        }

        // For schedule rating factor change.
        else if((elt as DiffItem).Bean typeis WC7RateFactor){
          var changedRateFactor =  (elt as DiffItem).Bean as WC7RateFactor
          if(elt as DiffItem typeis DiffProperty){
            if((elt as DiffProperty).PropertyInfo.Name == "Assessment") {
              createStatePremiumChangeRecords(_wcpolsRecord.StatePremiumChangeRecord,policyPeriod,statePremSchRatChgEndNum,null,changedRateFactor)
            }
          }
        }

        // For inclusion or exclusion of WC7 modifiers( x-mod, Multiline discount).
        else if((elt as DiffItem).Bean typeis WC7Modifier){
          var changeModifier = (elt as DiffItem).Bean as WC7Modifier
          if(changeModifier.PatternCode == "WC7ExpMod") {
            if((elt as DiffItem typeis DiffAdd) or(elt as DiffItem typeis DiffRemove)){
              createExpModFactChangeRecords(_wcpolsRecord.ExpRatingModChangeEndRecord,policyPeriod,changeModifier,null)
              createStatePremiumChangeRecords(_wcpolsRecord.StatePremiumChangeRecord,policyPeriod,statepremEstStateStdPremTotChgEndNum,changeModifier,null)
            }
            else if(elt as DiffItem typeis DiffProperty){
              if((elt as DiffProperty).PropertyInfo.Name == "RateModifier") {
                createExpModFactChangeRecords(_wcpolsRecord.ExpRatingModChangeEndRecord,policyPeriod,changeModifier,null)
                createStatePremiumChangeRecords(_wcpolsRecord.StatePremiumChangeRecord,policyPeriod,statepremEstStateStdPremTotChgEndNum,changeModifier,null)
              }
            }
          }
          else if(changeModifier.PatternCode == "WC7TDICMultiLineDiscount"){
            createStatePremiumChangeRecords(_wcpolsRecord.StatePremiumChangeRecord,policyPeriod,statepremEstStateStdPremTotChgEndNum,changeModifier,null)
          }
        }

        // For class code and payroll changes.
        else if((elt as DiffItem).Bean typeis WC7CoveredEmployee){
          var changedClassCode = (elt as DiffItem).Bean as WC7CoveredEmployee
          if(elt as DiffItem typeis DiffAdd){
            createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod,revCodeForAdd,changedClassCode)
          }
          else if(elt as DiffItem typeis DiffRemove){
            createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod.BasedOn,revCodeForDelete,changedClassCode)
          }
          else if(elt as DiffItem typeis DiffProperty){
            if((elt as DiffProperty).PropertyInfo.Name=="ClassCode") {
              createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod,revCodeForAdd,changedClassCode)
              createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod.BasedOn,revCodeForDelete,changedClassCode.BasedOn)
            }
            else if((elt as DiffProperty).PropertyInfo.Name=="BasisAmount") {
              createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod,revCodeForAdd,changedClassCode)
              createClassAndOrRateChgEndRecords(_wcpolsRecord.ClassAndOrRateChgAndOthEndRec,policyPeriod.BasedOn,revCodeForDelete,changedClassCode.BasedOn)
            }
          }
        }

        // For organization type change.
        else if((elt as DiffItem).Bean typeis EffectiveDatedFields){
          if(elt as DiffItem typeis DiffProperty){
            if((elt as DiffProperty).PropertyInfo.Name=="PolicyOrgType_TDIC"){
              createDataElementsChangeEndRecord(_wcpolsRecord.DataElementsChangeEndRecord,datElementChgEndNumOrgType,policyPeriod,blank,blank,blank)
            }
          }
        }
      })
    }

    else if(policyPeriod.Job typeis Cancellation) {
      _wcpolsRecord.CancellationRecord = createCancellationRecord(policyPeriod)
    }

    else if(policyPeriod.Job typeis Reinstatement) {
      _wcpolsRecord.ReinstatementRecord = createReinstatementRecord(policyPeriod)
    }

    _payload = new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes(_wcpolsRecord).asUTFString()
    return _payload
  }

  /**
   * Returns NameTypeCode by Organization type
   **/
  static function getNameTypeCode(policyNamedInsured: PolicyContactRole):int {
    if(policyNamedInsured.AccountContactRole.AccountContact.Contact typeis Person){
      return 1
    }
    else {
      return 2
    }
  }

  /**
   * Returns CancellationTypeCode by Cancel Reason Code.
   **/
  static function getCancellationTypeCode(pp: PolicyPeriod):int {
    switch(pp.RefundCalcMethod){

      case typekey.CalculationMethod.TC_FLAT:
          return 1
      case typekey.CalculationMethod.TC_PRORATA:
          return 2
      case typekey.CalculationMethod.TC_SHORTRATE:
          return 3
      default:
        return 0
    }
  }

  /**
   * Returns ReinstatementTypeCode by Cancel Reason Code.
   **/
  static function getReinstatementTypeCode(pp: PolicyPeriod):int {
    switch(pp.BasedOn.RefundCalcMethod){

      case typekey.CalculationMethod.TC_FLAT:
          return 1
      case typekey.CalculationMethod.TC_PRORATA:
          return 2
      case typekey.CalculationMethod.TC_SHORTRATE:
          return 2
      default:
        return 0
    }
  }

  /**
   * Returns Policy effective date.
   **/
  static function getPolicyEffDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.PeriodStart).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Returns Transaction issue date.
   **/
  static function getTranIssueDate(pp : PolicyPeriod) : String{

    if(pp.ModelDate == null) return ""
    var sdf: DateFormat = new SimpleDateFormat("yyDDD")
    return sdf.format(pp.ModelDate).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Returns Policy expiration date.
   **/
  static function getPolicyExpDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.PeriodEnd).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Assigns values to fields for Reinstatement record.
   **/
  static function createReinstatementRecord(pp:PolicyPeriod):TDIC_WCpolsReinstatementFields {

    var _wcpolsReinstatementFields = new TDIC_WCpolsReinstatementFields()
    _wcpolsReinstatementFields.CarrierCode = carrierCode
    _wcpolsReinstatementFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsReinstatementFields.FutureReserved = blank
    _wcpolsReinstatementFields.UnitCertificateIdentifier = blank
    _wcpolsReinstatementFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsReinstatementFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsReinstatementFields.TransactionCode = getTransactionCode(pp)
    _wcpolsReinstatementFields.StateCode = stateCode
    _wcpolsReinstatementFields.RecordTypeCode = reinsRecTypeCode
    _wcpolsReinstatementFields.ReinstatementCode = reinsCode
    _wcpolsReinstatementFields.CancellationTypeCode = reinsCancTypeCode
    _wcpolsReinstatementFields.ReasonForCancCode = reasonForCancCode
    _wcpolsReinstatementFields.ReinstatementTypeCode = getReinstatementTypeCode(pp)
    _wcpolsReinstatementFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsReinstatementFields.AddressOfInsured = pp.PolicyAddress as String
    _wcpolsReinstatementFields.NatureOfInsured = blank
    _wcpolsReinstatementFields.CancMailedToInsuredDate = getCancDate(pp)
    _wcpolsReinstatementFields.ReinsSeqNum = getSeqNumber(pp)
    _wcpolsReinstatementFields.ReasonForReinstatementCode = blank
    _wcpolsReinstatementFields.FutureReserved1 = blank
    _wcpolsReinstatementFields.CorrespondingCancEffDate = getCancDate(pp)
    _wcpolsReinstatementFields.ReinsEffDate = getReinstatementEffDate(pp)
    _wcpolsReinstatementFields.FutureReserved2 = blank

    return _wcpolsReinstatementFields
  }


  /**
   * Returns transaction sequence number.
   **/
  static function getSeqNumber(pp : PolicyPeriod) : int{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    if(sdf.format(pp.ModelDate).toString().replaceAll("[^0-9]", "") == sdf.format(pp.BasedOn.ModelDate).toString().replaceAll("[^0-9]", "")){
      return 02
    }
    else {
      return 01
     }
  }

  /**
   * Returns Reinstatement date.
   **/
  static function getReinstatementEffDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.BasedOn.CancellationDate).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Returns Cancellation date of reinstated policy.
   **/

  static function getCancDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.BasedOn.CancellationDate).toString().replaceAll("[^0-9]", "")

  }

 /**
  * Returns Term number of a policy.
  **/

  static function getTermNumber(pp : PolicyPeriod) : String{

    if(pp.TermNumber < 10)
    {
     return '0' + pp.TermNumber as String
    }
    else {
      return pp.TermNumber as String
    }
  }

  /**
   * Assigns values to fields for Cancellation record.
   **/
  static function createCancellationRecord(pp:PolicyPeriod):TDIC_WCpolsCancellationFields {

    var _wcpolsCancellationFields = new TDIC_WCpolsCancellationFields()

    _wcpolsCancellationFields.CarrierCode = carrierCode
    _wcpolsCancellationFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsCancellationFields.FutureReserved = blank
    _wcpolsCancellationFields.UnitCertificateIdentifier = blank
    _wcpolsCancellationFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsCancellationFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsCancellationFields.TransactionCode = getTransactionCode(pp)
    _wcpolsCancellationFields.StateCode = stateCode
    _wcpolsCancellationFields.RecordTypeCode = cancRecTypeCode
    _wcpolsCancellationFields.CancellationCode = cancCode
    _wcpolsCancellationFields.CancellationTypeCode = getCancellationTypeCode(pp)
    _wcpolsCancellationFields.ReasonForCancCode = getCancellationReasonCode(pp)
    _wcpolsCancellationFields.ReinstatementTypeCode = reinstatementTypeCode
    _wcpolsCancellationFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsCancellationFields.AddressOfInsured = pp.PolicyAddress as String
    _wcpolsCancellationFields.NatureOfInsured =  blank
    _wcpolsCancellationFields.CancMailedToInsuredDate = getCancellationEffectiveDate(pp)
    _wcpolsCancellationFields.CancSeqNum = getSeqNumber(pp)
    _wcpolsCancellationFields.ReasonForReinstatementCode = blank
    _wcpolsCancellationFields.FutureReserved1 = blank
    _wcpolsCancellationFields.CorrespondingCancEffDate = blank
    _wcpolsCancellationFields.CancEffDate = getCancellationEffectiveDate(pp)
    _wcpolsCancellationFields.FutureReserved2 = blank

     return _wcpolsCancellationFields
  }

  /**
   * Returns cancellation date.
   **/
  static function getCancellationEffectiveDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.CancellationDate).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Returns CancellationReasonCode by Cancel Reason Code.
   **/
  static function getCancellationReasonCode(pp: PolicyPeriod):int {
    switch(pp.Cancellation.CancelReasonCode){

      case typekey.ReasonCode.TC_NOLONGERINBUSINESSRET_TDIC:
          return 01
      case typekey.ReasonCode.TC_NOEMPLOYEE:
          return 02
      case typekey.ReasonCode.TC_NONPAYMENT:
          return 05
      case typekey.ReasonCode.TC_FLATREWRITE:
          return 07
      case typekey.ReasonCode.TC_SOLD:
          return 08
      case typekey.ReasonCode.TC_TOOKCOVERELSE_TDIC:
          return 09
      case typekey.ReasonCode.TC_RISKCHANGE:
          return 15
      case typekey.ReasonCode.TC_FAILTERM:
          return 16
      case typekey.ReasonCode.TC_FRAUD:
          return 21
        default:
        return 99
    }
  }

  /**
   * Assigns values to fields for Name record.
   **/
  static function createNameRecords(nameRecords : ArrayList<TDIC_WCpolsNameFields>, policyNamedInsured : PolicyContactRole,pp:PolicyPeriod){
    var _wcpolsNameFields = new TDIC_WCpolsNameFields()

    _wcpolsNameFields.CarrierCode = carrierCode
    _wcpolsNameFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsNameFields.FutureReserved = blank
    _wcpolsNameFields.UnitCertificateIdentifier = blank
    _wcpolsNameFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsNameFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsNameFields.TransactionCode = getTransactionCode(pp)
    _wcpolsNameFields.FutureReserved1 = blank
    _wcpolsNameFields.RecordTypeCode = nameRecTypeCode
    _wcpolsNameFields.NameTypeCode = getNameTypeCode(policyNamedInsured)
    _wcpolsNameFields.NameLinkIdentifier = nameLinkIdentifier
    _wcpolsNameFields.ProfEmprOrgnOrClientCompCode = blank
    _wcpolsNameFields.NameOfInsured = getNameOfInsured(pp,policyNamedInsured)
    _wcpolsNameFields.FutureReserved2 = blank
    _wcpolsNameFields.FedEmpIdNum = getFedEmpIdNumer(pp,policyNamedInsured)
    _wcpolsNameFields.ConSeqNum =nameRecords.Count+1
    _wcpolsNameFields.LegNatOfEntityCode = getOrgCode(pp)
    _wcpolsNameFields.StateCodes = blank
    _wcpolsNameFields.StateUnemplNums = blank
    _wcpolsNameFields.StateCodes1= blank
    _wcpolsNameFields.StateUnemplNums1 = blank
    _wcpolsNameFields.StateCodes2 = blank
    _wcpolsNameFields.StateUnemplNums2 = blank
    _wcpolsNameFields.FutureReserved3 = blank
    _wcpolsNameFields.StateUnemplmntNumRecSeqNum = blank
    _wcpolsNameFields.TextForOtherLegNatOfEntity = blank
    _wcpolsNameFields.NameLinkCounterIdntfr = nameLinkCntrIdentifier
    _wcpolsNameFields.FutureReserved4 = blank
    _wcpolsNameFields.PolChngEffDate = blank
    _wcpolsNameFields.PolChngExpDate = blank

    nameRecords.add(_wcpolsNameFields)

  }

  /**
   * Assigns values to fields of Name record for DBA.
   **/
  static function createNameRecords(nameRecords : ArrayList<TDIC_WCpolsNameFields>, loc : PolicyLocation,pp:PolicyPeriod){
    var _wcpolsNameFields = new TDIC_WCpolsNameFields()

    _wcpolsNameFields.CarrierCode = carrierCode
    _wcpolsNameFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsNameFields.FutureReserved = blank
    _wcpolsNameFields.UnitCertificateIdentifier = blank
    _wcpolsNameFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsNameFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsNameFields.TransactionCode = getTransactionCode(pp)
    _wcpolsNameFields.FutureReserved1 = blank
    _wcpolsNameFields.RecordTypeCode = nameRecTypeCode
    _wcpolsNameFields.NameTypeCode = nameTypeCodeForDBA
    _wcpolsNameFields.NameLinkIdentifier = nameLinkIdentifier
    _wcpolsNameFields.ProfEmprOrgnOrClientCompCode = blank
    _wcpolsNameFields.NameOfInsured = loc.DBA_TDIC + nameTypeDBA
    _wcpolsNameFields.FutureReserved2 = blank
    _wcpolsNameFields.FedEmpIdNum = getFeinForDba(pp,pp.PrimaryNamedInsured)
    _wcpolsNameFields.ConSeqNum =nameRecords.Count+1
    _wcpolsNameFields.LegNatOfEntityCode = getOrgCode(pp)
    _wcpolsNameFields.StateCodes = blank
    _wcpolsNameFields.StateUnemplNums = blank
    _wcpolsNameFields.StateCodes1= blank
    _wcpolsNameFields.StateUnemplNums1 = blank
    _wcpolsNameFields.StateCodes2 = blank
    _wcpolsNameFields.StateUnemplNums2 = blank
    _wcpolsNameFields.FutureReserved3 = blank
    _wcpolsNameFields.StateUnemplmntNumRecSeqNum = blank
    _wcpolsNameFields.TextForOtherLegNatOfEntity = blank
    _wcpolsNameFields.NameLinkCounterIdntfr = nameLinkCntrIdentifier
    _wcpolsNameFields.FutureReserved4 = blank
    _wcpolsNameFields.PolChngEffDate = blank
    _wcpolsNameFields.PolChngExpDate = blank

    nameRecords.add(_wcpolsNameFields)

  }

  static function getNameOfInsured(pp:PolicyPeriod,policyNamedInsured : PolicyContactRole): String {
    if((policyNamedInsured.AccountContactRole.AccountContact.Contact typeis Person)){
      if(policyNamedInsured.MiddleName == null){
        if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC)) {
          return policyNamedInsured.LastName+','+policyNamedInsured.FirstName + nameType
        }
        return policyNamedInsured.LastName+','+policyNamedInsured.FirstName
      }
      else {
        if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC)) {
          return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured.MiddleName + nameType
        }
        //return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured?.AccountContactRole?.AccountContact?.Contact?.MiddleName
        return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured.MiddleName
      }
    }
    else {
      if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC )) {
        return policyNamedInsured.CompanyName + nameType
      }
      return policyNamedInsured.CompanyName
    }
  }

  /**
   * Returns Federal Employer Identification Number.
   **/
  // Amended TaxID -- SSN/FEIN filed mapping and amended PersonFEIN_TDIC usage as part of GW-1623 & GW-753
  static function getFedEmpIdNumer(pp:PolicyPeriod,policyNamedInsured : PolicyContactRole): String {
//      if(policyNamedInsured?.AccountContactRole?.AccountContact?.Contact != null and policyNamedInsured?.AccountContactRole?.AccountContact?.Contact.FEIN== null and policyNamedInsured.AccountContactRole.AccountContact.Contact.SSN == null)
//        return  ""
//      else {
//        if(policyNamedInsured?.AccountContactRole?.AccountContact?.Contact != null and policyNamedInsured?.AccountContactRole?.AccountContact?.Contact.FEIN != null)
//          return (policyNamedInsured.AccountContactRole.AccountContact.Contact.FEIN).replaceAll("[^0-9]", "")
//        else
//          return policyNamedInsured.AccountContactRole.AccountContact.Contact.SSN.replaceAll("[^0-9]", "")
//      }
    //20160630 TJT
    if(pp.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE and policyNamedInsured as String == pp.primaryNameInsured_TDIC){
      return fedEmpIdValueForNoFEIN
    }
    else
    return policyNamedInsured.TaxID == null ? fedEmpIdValueForNoFEIN : policyNamedInsured.TaxID.replaceAll("[^0-9]", "")
    }

  /**
   * Returns Federal Employer Identification Number for DBA.
   **/
  static function getFeinForDba(pp:PolicyPeriod,policyNamedInsured : PolicyContactRole): String {

    if(pp.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE){
      return fedEmpIdValueForNoFEIN
    }
    else
    return policyNamedInsured.TaxID == null ? fedEmpIdValueForNoFEIN : policyNamedInsured.TaxID.replaceAll("[^0-9]", "")

  }

  /**
   * Creates Address records for Mailing, Producer, Insurer and Locations.
   **/
  static function createAddressRecords(addressRecords : ArrayList<TDIC_WCpolsAddressFields>, pp : PolicyPeriod){

    //Mailing address record
    addressRecords.add(populateAddressRecord(pp.Policy.Account.AccountHolderContact.PrimaryAddress,pp,1,001,27,00001))

    //Insurer address record
    addressRecords.add(populateAddressRecord(getCarrierAddress(),pp,3,999,99,99999))

    //Producer address record
    addressRecords.add(populateAddressRecord(pp.ProducerCodeOfRecord.Address,pp,5,999,99,99999))

    //Locations address record
    pp.PolicyLocations.each( \ elt -> addressRecords.add(populateAddressRecord(elt,pp)))
  }

  /**
   * Assigns and returns values to fields for Address(Mailing, Producer, Insurer) records.
   **/
  static function populateAddressRecord(address:Address,pp:PolicyPeriod,addressType:int,nameIdentifier:int,stateLink:int, exposureLink:int):TDIC_WCpolsAddressFields{

    var addOwner = new gw.pcf.policysummary.PolicyInfoAddressOwner(new gw.api.address.AddressAutofillableDelegate(address), true)

    var _wcpolsAddressFields = new TDIC_WCpolsAddressFields()

    _wcpolsAddressFields.CarrierCode = carrierCode
    _wcpolsAddressFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsAddressFields.FutureReserved = blank
    _wcpolsAddressFields.UnitCertificateIdentifier = blank
    _wcpolsAddressFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsAddressFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsAddressFields.TransactionCode = getTransactionCode(pp)
    _wcpolsAddressFields.FutureReserved1 = blank
    _wcpolsAddressFields.RecordTypeCode = addressRecTypeCode
    _wcpolsAddressFields.AddressTypeCode = addressType
    _wcpolsAddressFields.ForeignAddressInd = foreignAddressInd
    _wcpolsAddressFields.AddressStructureCode = addressStructureCode
    _wcpolsAddressFields.Street = addOwner.AddressDelegate.AddressLine1 + blank + (addOwner.AddressDelegate.AddressLine2 != null ? addOwner.AddressDelegate.AddressLine2 : blank)
    _wcpolsAddressFields.City = addOwner.AddressDelegate.City
    _wcpolsAddressFields.State = addOwner.AddressDelegate.State as String
    _wcpolsAddressFields.ZipCode = addOwner.AddressDelegate.PostalCode.replaceAll("[^0-9]", "")
    _wcpolsAddressFields.NameLinkIdentifier = nameIdentifier
    _wcpolsAddressFields.StateCodeLink = stateLink
    _wcpolsAddressFields.ExposureRecLinkForLocCode = exposureLink
    _wcpolsAddressFields.FutureReserved2 = blank
    _wcpolsAddressFields.PhoneNumOfInsured = getPhoneNumOfInsured(pp, addressType)
    _wcpolsAddressFields.NumOfEmployees = blank
    _wcpolsAddressFields.IndustryCode = pp.PrimaryNamedInsured.IndustryCode as String
    _wcpolsAddressFields.GeographicArea = blank
    _wcpolsAddressFields.EmailAddress = getEmailAddress(pp, addressType)
    _wcpolsAddressFields.FutureReserved3 = blank
    _wcpolsAddressFields.CountryCode = blank
    _wcpolsAddressFields.NameLinkCounterIdntf = addressNameLinkCntrIdentifier
    _wcpolsAddressFields.FutureReserved4 = blank
    _wcpolsAddressFields.PolChngEffDate = blank
    _wcpolsAddressFields.PolChngExpDate = blank

    return _wcpolsAddressFields
  }

  static function getPhoneNumOfInsured(pp : PolicyPeriod, addressType : int) : String{
    if(addressType == 1 ) {
      var phNum = pp?.Policy?.Account?.AccountHolderContact?.PrimaryPhoneValue?.replaceAll("[^0-9]", "")
      return phNum
    }
    else {
      return ' '
    }
  }

  static function getEmailAddress(pp : PolicyPeriod, addressType : int) : String{
    if(addressType == 1 ) {
      var emailAddress = pp?.Policy?.Account?.AccountHolderContact?.EmailAddress1
      return emailAddress
    }
    else {
      return blank
    }
  }

  /**
   * Assigns and returns values to fields for Locations Address records.
   **/
  static function populateAddressRecord(loc:PolicyLocation,pp:PolicyPeriod):TDIC_WCpolsAddressFields{

    var _wcpolsAddressFields = new TDIC_WCpolsAddressFields()

    _wcpolsAddressFields.CarrierCode = carrierCode
    _wcpolsAddressFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsAddressFields.FutureReserved = blank
    _wcpolsAddressFields.UnitCertificateIdentifier = blank
    _wcpolsAddressFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsAddressFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsAddressFields.TransactionCode = getTransactionCode(pp)
    _wcpolsAddressFields.FutureReserved1 = blank
    _wcpolsAddressFields.RecordTypeCode = addressRecTypeCode
    _wcpolsAddressFields.AddressTypeCode = addressTypeCode
    _wcpolsAddressFields.ForeignAddressInd = foreignAddressInd
    _wcpolsAddressFields.AddressStructureCode = addressStructureCode
    _wcpolsAddressFields.Street = loc.AddressLine1 + blank + (loc.AddressLine2 != null ? loc.AddressLine2 : blank)
    _wcpolsAddressFields.City = loc.City
    _wcpolsAddressFields.State = loc.State as String
    _wcpolsAddressFields.ZipCode = loc.PostalCode.replaceAll("[^0-9]", "")
    _wcpolsAddressFields.NameLinkIdentifier = addressNameLinkIdentifier
    _wcpolsAddressFields.StateCodeLink = stateCodeLink
    _wcpolsAddressFields.ExposureRecLinkForLocCode = expRecLinkForLocCode
    _wcpolsAddressFields.FutureReserved2 = blank
    _wcpolsAddressFields.PhoneNumOfInsured = blank
    _wcpolsAddressFields.NumOfEmployees = blank
    _wcpolsAddressFields.IndustryCode = pp.PrimaryNamedInsured.IndustryCode as String
    _wcpolsAddressFields.GeographicArea = blank
    _wcpolsAddressFields.EmailAddress = blank
    _wcpolsAddressFields.FutureReserved3 = blank
    _wcpolsAddressFields.CountryCode = blank
    _wcpolsAddressFields.NameLinkCounterIdntf = addressNameLinkCntrIdentifier
    _wcpolsAddressFields.FutureReserved4 = blank
    _wcpolsAddressFields.PolChngEffDate = blank
    _wcpolsAddressFields.PolChngExpDate = blank

    return _wcpolsAddressFields
  }

  /**
   * Creates multiple exposure records based on count of locations specified.
   **/
  static function createExposureRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsExposureFields>{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    var policyLine = pp.WC7Line
    var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)
    var exposureRecords = new ArrayList<TDIC_WCpolsExposureFields>()

    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var stateCosts = partitionCosts.get(elt.Jurisdiction)
      var ratingPeriods = elt.RatingPeriods
      var ratingPeriodCount = 0
      ratingPeriods.each( \ rp ->{
        var periodCosts = stateCosts.byRatingPeriod(rp)

        while(!periodCosts.Empty) {
          var expRecord = createIndividualExposureRecord(pp)
          if(periodCosts.first().ClassCode != null) {
            exposureRecords.add(expRecord)
          }
          expRecord.ExposurePeriodEffDate = '000000'
          var firstCost = periodCosts.first()
            if(firstCost.LocationNum != null) {
              expRecord.EstimatedExposureAmount = firstCost.Basis as long
              if(ratingPeriods.Count > 1){
              expRecord.ExposurePeriodEffDate = sdf.format(rp.Start).toString().replaceAll("[^0-9]", "")
              }
            } else {
              expRecord.EstimatedExposureAmount = 000000000000

            }
            expRecord.ClassificationCode = (firstCost.ClassCode != null) ? firstCost.ClassCode : firstCost.StatCode
            expRecord.ClassificationWordingSuffix = getClassificationWordingSuffix(expRecord.ClassificationCode)

            var premiumAmount = firstCost?.ActualAmount?.toString().replaceAll("[^0-9]", "")
            expRecord.EstimatedPremiumAmount = premiumAmount.substring(0,premiumAmount.length() - 2)
            expRecord.ManualOrChargedRate = firstCost?.ActualAdjRate?.toString()?.replaceAll("[^0-9]", "")
            if(firstCost.LocationNum != null) {
              expRecord.ExposureActOrExposureCovCode = 01
            }
            else {
              expRecord.ExposureActOrExposureCovCode = 00
            }

          periodCosts.remove(firstCost)

        }
      } )
    })
    return exposureRecords
  }

  /**
   * Assigns and returns values to fields for exposure records.
   **/
  static function createIndividualExposureRecord(pp:PolicyPeriod):TDIC_WCpolsExposureFields{

    var _wcpolsExposureFields = new TDIC_WCpolsExposureFields()

    _wcpolsExposureFields.CarrierCode = carrierCode
    _wcpolsExposureFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsExposureFields.FutureReserved = blank
    _wcpolsExposureFields.UnitCertificateIdentifier = blank
    _wcpolsExposureFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsExposureFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsExposureFields.TransactionCode = getTransactionCode(pp)
    _wcpolsExposureFields.StateCode = stateCode
    _wcpolsExposureFields.RecordTypeCode = expRecordTypeCode
    _wcpolsExposureFields.FutureReserved1 = blank
    _wcpolsExposureFields.ClassificationUseCode = blank
    _wcpolsExposureFields.FutureReserved2 = blank
    _wcpolsExposureFields.FutureReserved3 = blank
    _wcpolsExposureFields.ExposurePeriodCode = expPeriodCode
    _wcpolsExposureFields.ClassificationWording = blank
    _wcpolsExposureFields.FutureReserved4 = blank
    _wcpolsExposureFields.NameLinkIdentifier = expNameLinkIdentifier
    _wcpolsExposureFields.StateCodeLink = expStateCodeLink
    _wcpolsExposureFields.ExposureRecordLinkForExposureCode = expRecLinkForExpCode
    _wcpolsExposureFields.NameLinkCounterIdentifier = expNameLinkCounterIdentifier
    _wcpolsExposureFields.FutureReserved5 = blank
    _wcpolsExposureFields.NumberOfPiecesOfApparatus = blank
    _wcpolsExposureFields.NumberOfVolunteers = blank
    _wcpolsExposureFields.PolicySurchargeFactor = blank
    _wcpolsExposureFields.PlanPremiumAdjustmentFactor = blank
    _wcpolsExposureFields.FutureReserved6 = blank
    _wcpolsExposureFields.PolChngEffDate = blank
    _wcpolsExposureFields.PolChngExpDate = blank

    return  _wcpolsExposureFields
  }

  static function getClassificationWordingSuffix(classCodeValue : String) : String{
    if(classCodeValue != null and classCodeValue.length() > 4){
      switch(classCodeValue.substring(4,7)) {
        case "(1)":
          return "01"
        case "(2)":
          return "02"
        case "(3)":
          return "03"
        case "(4)":
          return "04"
        default :
          return "00"
      }
    }
    else {
      return "00"
    }
  }

  /**
   * Creates and returns multiple state premium records
   **/
  static function createStateRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsStatePremiumFields>{

    var policyLine = pp.WC7Line
    var jurisdictions=policyLine.WC7Jurisdictions
    var stateRecords = new ArrayList<TDIC_WCpolsStatePremiumFields>()
    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var ratingPeriods = elt.RatingPeriods
      var allModifierVersions = elt.AllModifierVersions

      allModifierVersions.each( \ p -> p.Pattern.CodeIdentifier)
      var modifiers:List<WC7Modifier>
      if (ratingPeriods.Count > 1) {
        modifiers= allModifierVersions.where( \ mod -> not ((mod.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)
      } else {
        modifiers =  allModifierVersions
      }
      var ratingPeriodCount=0
      ratingPeriods.each( \ rp -> {
        ratingPeriodCount +=1
        var stateRecord = createIndividualStatePremiumRecord(pp,rp)
        stateRecords.add(stateRecord)

       var rpModifiers =  allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary
            and gw.api.util.DateUtil.compareIgnoreTime(m.EffectiveDate, rp.Start) == 0
            and gw.api.util.DateUtil.compareIgnoreTime(m.ExpirationDate, rp.End) == 0 )


        var expMod= rpModifiers.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ExpMod")
        stateRecord.ExpModFactOrMeritratingFact= expMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", "")

        if(expMod?.RateWithinLimits != null) {
          stateRecord.ExpModStatusCode = expModStatusCode
        }
        else {
          stateRecord.ExpModStatusCode = expModStatusCodeWithOutXmod
        }

        var schdMod= allModifierVersions.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ScheduleMod")
        stateRecord.OtherIndivdualRiskRatingFact= schdMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", "")

        var stateCosts = partitionCosts.get(elt.Jurisdiction)
        var stateCostsExp = stateCosts.toSet().getOtherPremiumAndSurcharges(pp.PreferredSettlementCurrency)
        var costForexpConstant = stateCostsExp.where( \ kk ->  kk.Cost typeis WC7Cost and  kk.Cost.ClassCode=="0900").first()
        var expConstant = (costForexpConstant.Cost as WC7Cost).ActualAmount

        if(ratingPeriods.Count > ratingPeriodCount){
          stateRecord.AnnivRatingDate= sdf.format(pp.PeriodStart).toString().replaceAll("[^0-9]", "")
          stateRecord.ExpenseConstantAmnt = 0000000000
        }
        else {
          stateRecord.AnnivRatingDate= sdf.format(schdMod.EffectiveDate).toString().replaceAll("[^0-9]", "")
          stateRecord.ExpenseConstantAmnt = expConstant as int
        }
        stateRecord.ExpModEffDate = sdf.format(expMod.EffectiveDate).toString().replaceAll("[^0-9]", "")

      })
    })
    return stateRecords

  }

  /**
   * Assigns and returns values to fields for state premium records.
   **/
  static function createIndividualStatePremiumRecord(pp:PolicyPeriod,rp:WC7RatingPeriod):TDIC_WCpolsStatePremiumFields{

    var _wcpolsStatePremiumFields = new TDIC_WCpolsStatePremiumFields()

    _wcpolsStatePremiumFields.CarrierCode = carrierCode
    _wcpolsStatePremiumFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsStatePremiumFields.FutureReserved = blank
    _wcpolsStatePremiumFields.UnitCertificateIdentifier = blank
    _wcpolsStatePremiumFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsStatePremiumFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsStatePremiumFields.TransactionCode = getTransactionCode(pp)
    _wcpolsStatePremiumFields.StateCode = stateCode
    _wcpolsStatePremiumFields.RecordTypeCode = statePremRecTypeCode
    _wcpolsStatePremiumFields.StateAddDeleteCode = blank
    _wcpolsStatePremiumFields.FutureReserved1 = blank
    _wcpolsStatePremiumFields.IndependentDCORiskIdNum = blank
    _wcpolsStatePremiumFields.FutureReserved2 = blank
    _wcpolsStatePremiumFields.StateCovCarrierCode = stateCovCarrierCode
    _wcpolsStatePremiumFields.ExpModPlanTypeCode = blank
    _wcpolsStatePremiumFields.InsurerPremiumDevFact = insurerPremDeviationFact
    _wcpolsStatePremiumFields.TypeOfPremDevCode = typeOfPremDevCode
    _wcpolsStatePremiumFields.EstimatedStateStanPremTot = getStandardPremium(pp,rp) as long
    _wcpolsStatePremiumFields.LossConstantAmnt = lossConstantAmnt
    _wcpolsStatePremiumFields.PremDiscountAmnt = premDiscountAmnt
    _wcpolsStatePremiumFields.ProratedExpenseConstAmntReasonCode = blank
    _wcpolsStatePremiumFields.ProratedMinPremAmntReasonCode = blank
    _wcpolsStatePremiumFields.ReasonStateWasAddedToPolicyCode = blank
    _wcpolsStatePremiumFields.FutureReserved3 = blank
    _wcpolsStatePremiumFields.AssignedRiskAdjProgFact = blank
    _wcpolsStatePremiumFields.FutureReserved4 = blank
    _wcpolsStatePremiumFields.PremAdjPeriodCode = premAdjPeriodCode
    _wcpolsStatePremiumFields.TypeOfNonStanIdCode = blank
    _wcpolsStatePremiumFields.FutureReserved5 = blank
    _wcpolsStatePremiumFields.PolChngEffDate = blank
    _wcpolsStatePremiumFields.PolChngExpDate = blank

    return  _wcpolsStatePremiumFields

  }

  static function getStandardPremium(pp:PolicyPeriod,rpp:WC7RatingPeriod): double {

    var policyLine = pp.WC7Line
    var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)

    var total = 0.0
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var stateCosts = partitionCosts.get(elt.Jurisdiction)
      var ratingPeriods = elt.RatingPeriods
      if(rpp != null)
        ratingPeriods =  ratingPeriods.where( \ crp -> crp==rpp)
      ratingPeriods.each( \ rp ->{
        var periodCosts = stateCosts.byRatingPeriod(rp)
        var stdPremiums = periodCosts.getStandardPremiums(pp.PreferredSettlementCurrency)

        stdPremiums.each( \ ee -> {
          if(ee.Description.endsWithIgnoreCase("Standard Premium") ){
            total += (ee.Total.Amount) as double
          }
        })
      })
    })
    return total
  }

  static function shouldCreatePayload(messageContext : entity.MessageContext):boolean{

    if(messageContext.Root typeis PolicyPeriod && messageContext.Root.getSlice(messageContext.Root.EditEffectiveDate).WC7LineExists){ // Only for Worker's Compensation
      var pp = messageContext.Root
      // BrianS - GW-2199 - In-progress migrated transactions should create a WCPOLS payload
      if(pp.isFieldChanged("Status")and pp.Status == PolicyPeriodStatus.TC_BOUND and pp.getOriginalValue("Status") != PolicyPeriodStatus.TC_BOUND)
        return true
    }
    return false
  }

  /**
   * Assigns and returns values to fields for Report header record.
   **/
  static function createReportHeaderRecordFields():TDIC_WCpolsReportHeaderFields {

    var _wcpolsReportHeaderFields = new TDIC_WCpolsReportHeaderFields()

    _wcpolsReportHeaderFields.Label = label
    _wcpolsReportHeaderFields.DataProviderEmail = dataProviderEmail
    _wcpolsReportHeaderFields.RecordTypeCode = blank
    _wcpolsReportHeaderFields.DataTypeCode = dataTypeCode
    _wcpolsReportHeaderFields.DataReceiverCode = dataReceiverCode
    _wcpolsReportHeaderFields.DateOfDataTransmission = getReportTransmissionDate()
    _wcpolsReportHeaderFields.VersionIdentifier = versionIdentifier
    _wcpolsReportHeaderFields.SubmissionTypeCode = submissionTypeCode
    _wcpolsReportHeaderFields.SubmissionReplacementIdentifier = blank
    _wcpolsReportHeaderFields.DataProviderCode = carrierCode
    _wcpolsReportHeaderFields.NamOfDataProvider = nameOfDataProvider
    _wcpolsReportHeaderFields.ElectronicOrPaperReceiptCode = blank
    _wcpolsReportHeaderFields.PhoneNumber = Coercions.makePLongFrom(phoneNumber)
    _wcpolsReportHeaderFields.PhoneNumberExtension = blank
    _wcpolsReportHeaderFields.FaxNumber = Coercions.makePLongFrom(faxNumber)
    _wcpolsReportHeaderFields.ProcessedDate = getDataProcessedDate()
    _wcpolsReportHeaderFields.DataProviderStreet = blank
    _wcpolsReportHeaderFields.DataProviderCity = blank
    _wcpolsReportHeaderFields.DataProviderState = blank
    _wcpolsReportHeaderFields.DataProviderZipCode = blank
    _wcpolsReportHeaderFields.DataProviderTypeCode = dataProviderTypeCode
    _wcpolsReportHeaderFields.ThirdPartyEntityFEINNumber = blank
    _wcpolsReportHeaderFields.FutureReserved = blank
    _wcpolsReportHeaderFields.FutureReserved1 = blank

    return _wcpolsReportHeaderFields
  }

  /**
   * Returns Report Transmission Date
   **/
  static function  getReportTransmissionDate(): String {

    var sdf: DateFormat = new SimpleDateFormat("yyDDD")
    return sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")

  }

  /**
  * Returns data processed date.
  **/
  static function getDataProcessedDate() : String{

    var sdf: DateFormat = new SimpleDateFormat("yyyyMMdd")
    return sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Assigns and returns values to fields for policy header record.
   **/
  static function createPolicyReportHeaderFields(pp:PolicyPeriod): TDIC_WCpolsPolicyHeaderFields {
    var _wcpolsPolicyHeaderFields = new TDIC_WCpolsPolicyHeaderFields()

    _wcpolsPolicyHeaderFields.CarrierCode = carrierCode
    _wcpolsPolicyHeaderFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsPolicyHeaderFields.FutureReserved = blank
    _wcpolsPolicyHeaderFields.UnitCertificateIdentifier = blank
    _wcpolsPolicyHeaderFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsPolicyHeaderFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsPolicyHeaderFields.TransactionCode = getTransactionCode(pp)
    _wcpolsPolicyHeaderFields.FutureReserved1 = blank
    _wcpolsPolicyHeaderFields.RecordTypeCode = policyRecordTypeCode
    _wcpolsPolicyHeaderFields.ExperienceRatingCode = blank
    _wcpolsPolicyHeaderFields.InterstateRiskIdNumber = blank
    _wcpolsPolicyHeaderFields.PolicyExpirationDate = getPolicyExpDate(pp)
    _wcpolsPolicyHeaderFields.ThirdPartyEntityFEIN = blank
    _wcpolsPolicyHeaderFields.TypeOfCoverageIdCode = typeOfCoverageIdCode
    _wcpolsPolicyHeaderFields.EmployeeLeasingPolicyTypeCode = empLeasingPolicyTypeCode
    _wcpolsPolicyHeaderFields.PolicyTermCode = polTermCode
    _wcpolsPolicyHeaderFields.PriorPolicyNumberIdentifier = getPriorPolicyNumber(pp)
    _wcpolsPolicyHeaderFields.PriorUnitCertificateIdentifier = blank
    _wcpolsPolicyHeaderFields.FutureReserved2 = blank
    _wcpolsPolicyHeaderFields.LegalNatureOfInsuredCode = getOrgCode(pp)
    _wcpolsPolicyHeaderFields.TypeOfPlanIDCode = blank
    _wcpolsPolicyHeaderFields.WrapUpOCIPCode = wrapUpOCIPCode
    _wcpolsPolicyHeaderFields.BusinessSegmentIdentifier = blank
    _wcpolsPolicyHeaderFields.PolicyMinimumPremiumAmount = getPolicyMinimumPremiumAmount()
    _wcpolsPolicyHeaderFields.PolicyMinimumPremiumStateCode = polMinPremStateCode
    _wcpolsPolicyHeaderFields.PolicyEstimatedStandardPremiumTotal = getStandardPremium(pp,null) as long
    _wcpolsPolicyHeaderFields.PolicyDepositPremiumAmount = polDepPremAmnt
    _wcpolsPolicyHeaderFields.AuditFrequencyCode = auditFreqCode
    _wcpolsPolicyHeaderFields.BillingFrequencyCode = billingFreqCode
    _wcpolsPolicyHeaderFields.RetrospectiveRatingCode = blank
    // Fixed value for this field corresponding to restored product model
    _wcpolsPolicyHeaderFields.EmprLiabLimAmntBodInjByAcciEachAccAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabLimitEachAccTDICTerm.Value  as long
    _wcpolsPolicyHeaderFields.EmprLiabLimAmntBodInjByDisePolAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabPolicyLimitTerm.Value as long
    // Fixed value for this field corresponding to restored product model
    _wcpolsPolicyHeaderFields.EmprLiabLimAmntBodInjByDiseEachEmpeAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabLimitEachEmpTDICTerm.Value as long
    _wcpolsPolicyHeaderFields.NameOfproducer = pp.ProducerCodeOfRecord.Organization as String
    _wcpolsPolicyHeaderFields.AssignedRiskBinderNumber = blank
    _wcpolsPolicyHeaderFields.GroupCovStatusCode = groupCovStatusCode
    _wcpolsPolicyHeaderFields.FutureReserved3 = blank
    _wcpolsPolicyHeaderFields.OriginalCarrierCode = originalCarrierCode
    _wcpolsPolicyHeaderFields.OriginalPolicyNumberIdentifier = blank
    _wcpolsPolicyHeaderFields.OriginalPolicyEffectiveDate = originalPolEffDate
    _wcpolsPolicyHeaderFields.TextForOtherLegNatOfInsrd = blank
    _wcpolsPolicyHeaderFields.AssignmentDate = blank
    _wcpolsPolicyHeaderFields.AssignedRiskBinderNum = blank
    _wcpolsPolicyHeaderFields.FutureReserved4 = blank
    _wcpolsPolicyHeaderFields.PolChngEffDate = blank
    _wcpolsPolicyHeaderFields.PolChngExpDate = blank

    return _wcpolsPolicyHeaderFields
  }

  /**
   * Returns Policy minimum premium amount.
   **/
  static function getPolicyMinimumPremiumAmount() : int {

    var qry = Query.make(DefaultRateFactorRow)
    var rt = qry.join("RateTable")
    var uu = rt.join("Definition")
    uu.compare("TableCode",Equals,"WorkersCompMinimumPremium")
    qry.compare("str1",Equals,"8839")
    var res = qry.select().first()
    return res.dec1 as int
  }

  /**
   * US669
   * 03/10/2015 Shane Murphy
   *
   * Get Rate associated with classcode
   *
   * We can use the current date here as this is used in the XML
   * generation and will be called on the date the event occurs.
   */
  @Param("aClassCode", "ClassCode to find Rate for")
  @Returns("Rate")
  static function getClassCodeRate(aClassCode: String): BigDecimal {
    var qry = Query.make(DefaultRateFactorRow)
    var rt = qry.join("RateTable")
    var uu = rt.join("Definition")
    var rb = rt.join("RateBook")
    rb.compare("EffectiveDate", LessThanOrEquals, DateUtil.currentDate())
    rb.or(\orCond -> {
      orCond.compare("ExpirationDate", GreaterThanOrEquals, DateUtil.currentDate())
      orCond.compare("ExpirationDate", Equals, null)
    })
    uu.compare("TableCode", Equals, "WC7_ClassCode_Rate")
    qry.compare("str1", Equals, aClassCode)
    var res = qry.select().first()
    return res.dec5
  }

  /**
   * Returns Transaction code based on Job subtype.
   **/
  static function getTransactionCode(pp: PolicyPeriod):int {
  switch(pp.Job.Subtype){
    case typekey.Job.TC_SUBMISSION:
        return 01
    case typekey.Job.TC_RENEWAL:
        return 02
    case typekey.Job.TC_POLICYCHANGE:
        return 03
    case typekey.Job.TC_CANCELLATION:
        return 05
    case typekey.Job.TC_REINSTATEMENT:
        return 05
      default:
      return 00
  }
}

  /**
   * Returns prior policy number.
   **/
  static function getPriorPolicyNumber(pp: PolicyPeriod):String{
    switch(pp.Job.Subtype){
      case typekey.Job.TC_RENEWAL:
          return polNumPrefix + pp.BasedOn.PolicyNumber  + getTermNumber(pp.BasedOn)
        default:
        return ''
    }
  }

  /**
   * Returns Organization code based on Organization type.
   **/
  static function getOrgCode(pp: PolicyPeriod):int {
    switch(pp.PolicyOrgType_TDIC){

      case typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP:
          return 01
      case typekey.PolicyOrgType_TDIC.TC_PARTNERSHIP:
          return 02
      case typekey.PolicyOrgType_TDIC.TC_PRIVATECORP:
          return 03
      case typekey.PolicyOrgType_TDIC.TC_NONPROFIT:
          return 04
      case typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE:
          return 06
      case typekey.PolicyOrgType_TDIC.TC_LLC:
          return 10
        default:
        return 99
    }
  }

  /**
   * Creates and returns endorsement identification records.
   **/
  static function createEndorsementIdentificationRecord(pp:PolicyPeriod):ArrayList<TDIC_WCpolsEndorsementIdentificationFields>{
     var forms = pp.NewlyAddedForms.toList()
     var records = new ArrayList<TDIC_WCpolsEndorsementIdentificationFields>()

    var splitForms = chopped(forms,11)
    splitForms.each( \ elt -> {
      var endRecord = createIndividualEndorsementIdentificationRecord(pp)
      records.add(endRecord)
      endRecord.EndorsementNumbers = new ArrayList<TDIC_EndorsementNumberFields>()
      elt.each( \ frm -> endRecord.EndorsementNumbers.add(createEndormentNumberRecord(frm)))

    })
    return records
  }

  /**
   * Assigns and returns values to fields for Endorsement identification records.
   **/
  static function createIndividualEndorsementIdentificationRecord(pp:PolicyPeriod):TDIC_WCpolsEndorsementIdentificationFields{

    var _wcpolsEndorsementIdentificationFields = new TDIC_WCpolsEndorsementIdentificationFields()

    _wcpolsEndorsementIdentificationFields.CarrierCode = carrierCode
    _wcpolsEndorsementIdentificationFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsEndorsementIdentificationFields.FutureReserved = blank
    _wcpolsEndorsementIdentificationFields.UnitCertificateIdentifier = blank
    _wcpolsEndorsementIdentificationFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsEndorsementIdentificationFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsEndorsementIdentificationFields.TransactionCode = getTransactionCode(pp)
    _wcpolsEndorsementIdentificationFields.StateCode = stateCode
    _wcpolsEndorsementIdentificationFields.RecordTypeCode = endIdenRecTypeCode
    _wcpolsEndorsementIdentificationFields.FutureReserved1 = blank
    _wcpolsEndorsementIdentificationFields.EndorsementNumbers = {}
    _wcpolsEndorsementIdentificationFields.FutureReserved2 = blank
    _wcpolsEndorsementIdentificationFields.PolChngEffDate = blank
    _wcpolsEndorsementIdentificationFields.PolChngExpDate = blank

    return _wcpolsEndorsementIdentificationFields
  }

  /**
   * Assigns and returns values to few repetitive fields for endorsement identification records.
   **/
  static function createEndormentNumberRecord(curForm:Form):TDIC_EndorsementNumberFields{
     var endNo = new TDIC_EndorsementNumberFields()
     endNo.BureauVersionIdentifier= getBureauVersionidentifier(curForm)
    endNo.CarrierVersionIdentifier = blank
    endNo.EndorsementNumber = curForm.FormNumber

    return endNo
  }

  /**
   * Returns Bureau version identifier.
   **/
  static function getBureauVersionidentifier(curForm:Form):String {

    var burVerid = curForm.FormNumber.replaceAll("[^0-9A-Z]", "")
    if (burVerid.length > 8){
      return burVerid.charAt(8)
    }
    else {
      return ' '
    }
  }

  static function chopped<E>(list:List<E> , L: int ):List<List<E>>  {
    var parts = new ArrayList<List<E>>()
    final var N = list.size()
    var i = 0
    while (i < N) {
      parts.add(new ArrayList<E>(
          list.subList(i, Math.min(N, i + L)))
      )
      i += L
    }
    return parts;
  }
  /**
   * Returns Carrier address.
   **/
  static function getCarrierAddress(): Address {

    var insuranceCompany = Query.make(Organization).compare("Type", Equals, BusinessType.TC_INSURER).select().AtMostOneRow
    if (insuranceCompany != null) {
      return insuranceCompany.Contact.PrimaryAddress
    }
    return null
  }

  /**
   * Returns Endorsement effective date.
   **/
  static function getEndorsementEffDate(pp : PolicyPeriod) : String{

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    return sdf.format(pp.EditEffectiveDate).toString().replaceAll("[^0-9]", "")

  }

  /**
   * Assigns and returns values for fields of partner exclusion record.
   **/
  static function createIndividualpartnerRecord(pp:PolicyPeriod): TDIC_WCpolsPartnershipCovOrExclusionEndCAFields {

    var _wcpolsPartnershipCovOrExclusionEndCAFields = new TDIC_WCpolsPartnershipCovOrExclusionEndCAFields()

    _wcpolsPartnershipCovOrExclusionEndCAFields.CarrierCode = carrierCode
    _wcpolsPartnershipCovOrExclusionEndCAFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.FutureReserved = blank
    _wcpolsPartnershipCovOrExclusionEndCAFields.UnitCertificateIdentifier = blank
    _wcpolsPartnershipCovOrExclusionEndCAFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.TransactionCode = getTransactionCode(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.StateCode = stateCode
    _wcpolsPartnershipCovOrExclusionEndCAFields.RecordTypeCode = partnerRecTypeCode
    _wcpolsPartnershipCovOrExclusionEndCAFields.FutureReserved1 = blank
    _wcpolsPartnershipCovOrExclusionEndCAFields.EndorsementNumber = partnerEndNumber
    _wcpolsPartnershipCovOrExclusionEndCAFields.BureauVersionIdentifier = getBureauVerIdForPartner(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.CarrierVersionIdentifier = blank
    _wcpolsPartnershipCovOrExclusionEndCAFields.NameOfGeneralPartnersExcluded = {}
    _wcpolsPartnershipCovOrExclusionEndCAFields.FutureReserved2 = blank
    _wcpolsPartnershipCovOrExclusionEndCAFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsPartnershipCovOrExclusionEndCAFields.EndorsementEffDate = getEndorsementEffDate(pp)
    _wcpolsPartnershipCovOrExclusionEndCAFields.FutureReserved3 = blank

    return _wcpolsPartnershipCovOrExclusionEndCAFields
  }

  /**
   * Returns bureau version identifier for partner record.
   **/
  static function getBureauVerIdForPartner(pp:PolicyPeriod): String{

    var forms = pp.NewlyAddedForms
    var burVerId:String = blank
    forms.each( \ endForm ->{
      if(endForm.FormPatternCode.substring(0,8) == "WC040302"){
        burVerId = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
        if(burVerId.length > 8){
          burVerId = burVerId.charAt(8)
        }
        else {
          burVerId = blank
        }
      }
    })
    return burVerId
  }

  /**
   * Creates and returns multiple partner exclusion records based on count of partners being excluded.
   **/
  static function createPartnerRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsPartnershipCovOrExclusionEndCAFields>{

    var records = new ArrayList<TDIC_WCpolsPartnershipCovOrExclusionEndCAFields>()
    var partners = pp.WC7Line.WC7PolicyOwnerOfficers.where( \ partner -> partner.isExcluded_TDIC ).toList()
    var splitForms = chopped(partners,3)
    splitForms.each( \ elt -> {
      var partnerRecord = createIndividualpartnerRecord(pp)
      records.add(partnerRecord)
      partnerRecord.NameOfGeneralPartnersExcluded = new ArrayList<String>()
      elt.each( \ frm -> partnerRecord.NameOfGeneralPartnersExcluded.add(frm.AccountContactRole.AccountContact.Contact as String))
    })
    return records
  }

  /**
   * Creates and returns multiple officers or directors exclusion records based on count of officers or directors being excluded.
   **/
  static function createOfficersOrDirectorsRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields>{

    var records = new ArrayList<TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields>()
    var owners = pp.WC7Line.WC7PolicyOwnerOfficers.where( \ owner -> owner.isExcluded_TDIC ).toList()
    var splitForms = chopped(owners,3)
    splitForms.each( \ elt -> {
      var officerRecord = createIndividualOfficersOrDirectorRecord(pp)
      records.add(officerRecord)
      officerRecord.NameAndTitleOfOffiOrDireExcluded = new ArrayList<String>()
      elt.each( \ frm -> officerRecord.NameAndTitleOfOffiOrDireExcluded.add(frm as String))

    })
    return records
  }

  /**
   * Creates and returns multiple voluntary compensation endorsement records based on count of number of persons for voluntary compensation.
   **/
  static function createVolCompAndEmprsLiaCovEndCAFieldsRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields>{

    var records = new ArrayList<TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields>()
    var wcLine = pp.WC7Line
    var sch = wcLine?.WC7VoluntaryCompensationAndEmployersLiabilityCovCond?.ScheduledItems?.toList()
    if(sch == null) {return null}
    var propInfo = wcLine?.WC7VoluntaryCompensationAndEmployersLiabilityCovCond?.PropertyInfos?.firstWhere( \ elt -> elt typeis ScheduleStringPropertyInfo )
    var splitPatterns = chopped(sch,3)
    splitPatterns.each( \ elt -> {
      var volRecord = createIndividualVolCompAndEmprsLiaCovEndCAFieldsRecord(pp)
      records.add(volRecord)
      volRecord.NameOfEmplAndNameOrGrpOrDescOfOperations = new ArrayList<String>()
      elt.each( \ frm -> {
        var valProvider = new gw.api.productmodel.SchedulePropertyValueProvider<String>(frm, propInfo.PropertyInfo)
        volRecord.NameOfEmplAndNameOrGrpOrDescOfOperations.add(valProvider.Value)
      })
    })
    return records
  }

  /**
   * Creates and returns multiple waiver of right to recover endorsement records based on count of number of persons to be waived.
   **/
  static function createWaiverofRightRecords(pp:PolicyPeriod):ArrayList<TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields>{

    var records = new ArrayList<TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields>()
    var wcLine = pp.WC7Line
    var splitPatterns = chopped(wcLine.WC7SpecificWaiversWM.toList(),2)
    splitPatterns.each( \ elt -> {
      var waiverRecord = createIndividualWaiverofRightRecord(pp)
      records.add(waiverRecord)
      waiverRecord.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR = new ArrayList<String>()
      elt.each( \ frm -> waiverRecord.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR.add(frm.Description.replaceAll("[\r\n]+", ", ")))

    })
    return records

  }

  /**
   * Assigns and returns values for fields of officers or directors exclusion record.
   **/
  static function createIndividualOfficersOrDirectorRecord(pp:PolicyPeriod):TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields{

    var _wcpolsOfficersAndDirectorsCovOrExcCAFields = new TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields()
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.CarrierCode = carrierCode
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.FutureReserved = blank
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.UnitCertificateIdentifier = blank
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.TransactionCode = getTransactionCode(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.StateCode = stateCode
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.RecordTypeCode = officerRecTypeCode
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.FutureReserved1 = blank
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.EndorsementNumber = officerEndNumber
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.BureauVersionIdentifier = getBurIdForOffiOrDire(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.CarrierVersionIdentifier = blank
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.NameAndTitleOfOffiOrDireExcluded = {}
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.FutureReserved2 = blank
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.EndorsementEffDate = getEndorsementEffDate(pp)
    _wcpolsOfficersAndDirectorsCovOrExcCAFields.FutureReserved3 = blank

    return  _wcpolsOfficersAndDirectorsCovOrExcCAFields
  }

  /**
   * Returns bureau version identifier for officer or director record.
   **/
  static function getBurIdForOffiOrDire(pp:PolicyPeriod): String{

    var forms = pp.NewlyAddedForms
    var burVerId:String = blank
    forms.each( \ endForm ->{
      if(endForm.FormPatternCode.substring(0,8) == "WC040303"){
        burVerId = endForm.FormNumber.replaceAll("[^0-9A-Z]", "")
        if(burVerId.length > 8){
          burVerId = burVerId.charAt(8)
        }
        else {
          burVerId = blank
        }
      }
    })
    return burVerId
  }

  /**
   * Assigns and returns values for fields of waiver of right to recover exclusion record.
   **/
  static function createIndividualWaiverofRightRecord(pp:PolicyPeriod):TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields{

    var _wcpolsWaiOfOurRightToRecFromOthsEndCAFields = new TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields()
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.CarrierCode = carrierCode
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.FutureReserved = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.UnitCertificateIdentifier = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.TransactionCode = getTransactionCode(pp)
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.StateCode = stateCode
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.RecordTypeCode = waiverRecTypeCode
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.FutureReserved1 = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.EndorsementNumber = waiverEndNumber
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.BureauVersionIdentifier = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.CarrierVersionIdentifier = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR = {}
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.PercentageOfPremium = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.EndSeqNumber = endSeqNum
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.FutureReserved2 = blank
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.EndorsementEffDate = getEndorsementEffDate(pp)
    _wcpolsWaiOfOurRightToRecFromOthsEndCAFields.FutureReserved3 = blank

    return  _wcpolsWaiOfOurRightToRecFromOthsEndCAFields
  }



  /**
   * Assigns and returns values for fields of voluntary compensation record.
   **/
  static function createIndividualVolCompAndEmprsLiaCovEndCAFieldsRecord(pp:PolicyPeriod):TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields{

    var _wcpolsVolCompAndEmprsLiaCovEndCAFields = new TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields()

    _wcpolsVolCompAndEmprsLiaCovEndCAFields.CarrierCode = carrierCode
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.FutureReserved = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.UnitCertificateIdentifier = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.TransactionCode = getTransactionCode(pp)
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.StateCode = stateCode
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.RecordTypeCode = volRecTypeCode
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.FutureReserved1 = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.EndorsementNumber = volEndNumber
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.BureauVersionIdentifier = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.CarrierVersionIdentifier = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.NameOfEmplAndNameOrGrpOrDescOfOperations = {}
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.FutureReserved2 = blank
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.EndorsementEffDate = getEndorsementEffDate(pp)
    _wcpolsVolCompAndEmprsLiaCovEndCAFields.FutureReserved3 = blank

    return _wcpolsVolCompAndEmprsLiaCovEndCAFields
  }

  /**
   * Assigns and returns values for fields of California approved form 10 record.
   **/

  static function createIndividualCaf10Record(pp:PolicyPeriod, loc:String, operation:String, operationTitle:String, classificationCode:int):TDIC_WCpolsCAApprovedForm10Fields {

    var _wcpolsCAApprovedForm10Fields = new TDIC_WCpolsCAApprovedForm10Fields()
    _wcpolsCAApprovedForm10Fields.CarrierCode = carrierCode
    _wcpolsCAApprovedForm10Fields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsCAApprovedForm10Fields.FutureReserved = blank
    _wcpolsCAApprovedForm10Fields.UnitCertificateIdentifier = blank
    _wcpolsCAApprovedForm10Fields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionCode = getTransactionCode(pp)
    _wcpolsCAApprovedForm10Fields.StateCode = stateCode
    _wcpolsCAApprovedForm10Fields.RecordTypeCode = caf10RecTypeCode
    _wcpolsCAApprovedForm10Fields.FutureReserved1 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementNumber = caf10EndNumber
    _wcpolsCAApprovedForm10Fields.BureauVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.CarrierVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSerialNumber = blank
    _wcpolsCAApprovedForm10Fields.Title10Wording = blank
    _wcpolsCAApprovedForm10Fields.NameOfEmployee = {}
    _wcpolsCAApprovedForm10Fields.NameOfOperation = operation
    _wcpolsCAApprovedForm10Fields.OperationTitle = operationTitle
    _wcpolsCAApprovedForm10Fields.AddressOfLocation = loc
    _wcpolsCAApprovedForm10Fields.ClassificationCode = classificationCode as String
    _wcpolsCAApprovedForm10Fields.ClassificationWordingSuffix = blank
    _wcpolsCAApprovedForm10Fields.ClassificationWording = blank
    _wcpolsCAApprovedForm10Fields.FutureReserved2 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSequenceNumber = endSeqNum
    _wcpolsCAApprovedForm10Fields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsCAApprovedForm10Fields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsCAApprovedForm10Fields.EndorsementExpirationDate = blank

    return _wcpolsCAApprovedForm10Fields
  }

  /**
   * Assigns and returns values for fields of California approved form 10 record.
   **/
  static function createRemainingCaf10Records(pp:PolicyPeriod):TDIC_WCpolsCAApprovedForm10Fields{

    var _wcpolsCAApprovedForm10Fields = new TDIC_WCpolsCAApprovedForm10Fields()
    _wcpolsCAApprovedForm10Fields.CarrierCode = carrierCode
    _wcpolsCAApprovedForm10Fields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsCAApprovedForm10Fields.FutureReserved = blank
    _wcpolsCAApprovedForm10Fields.UnitCertificateIdentifier = blank
    _wcpolsCAApprovedForm10Fields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionCode = getTransactionCode(pp)
    _wcpolsCAApprovedForm10Fields.StateCode = stateCode
    _wcpolsCAApprovedForm10Fields.RecordTypeCode = caf10RecTypeCode
    _wcpolsCAApprovedForm10Fields.FutureReserved1 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementNumber = caf10EndNumber
    _wcpolsCAApprovedForm10Fields.BureauVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.CarrierVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSerialNumber = blank
    _wcpolsCAApprovedForm10Fields.Title10Wording = blank
    _wcpolsCAApprovedForm10Fields.NameOfEmployee = {}
    _wcpolsCAApprovedForm10Fields.NameOfOperation = blank
    _wcpolsCAApprovedForm10Fields.OperationTitle = blank
    _wcpolsCAApprovedForm10Fields.AddressOfLocation = blank
    _wcpolsCAApprovedForm10Fields.ClassificationCode = 0 as String
    _wcpolsCAApprovedForm10Fields.ClassificationWordingSuffix = blank
    _wcpolsCAApprovedForm10Fields.ClassificationWording = blank
    _wcpolsCAApprovedForm10Fields.FutureReserved2 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSequenceNumber = endSeqNum
    _wcpolsCAApprovedForm10Fields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsCAApprovedForm10Fields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsCAApprovedForm10Fields.EndorsementExpirationDate = blank

    return _wcpolsCAApprovedForm10Fields
  }

  static function createOnlyCaf10Record(pp:PolicyPeriod, loc:String, operation:String, operationTitle:String, classificationCode:String):TDIC_WCpolsCAApprovedForm10Fields{

    var _wcpolsCAApprovedForm10Fields = new TDIC_WCpolsCAApprovedForm10Fields()
    _wcpolsCAApprovedForm10Fields.CarrierCode = carrierCode
    _wcpolsCAApprovedForm10Fields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsCAApprovedForm10Fields.FutureReserved = blank
    _wcpolsCAApprovedForm10Fields.UnitCertificateIdentifier = blank
    _wcpolsCAApprovedForm10Fields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsCAApprovedForm10Fields.TransactionCode = getTransactionCode(pp)
    _wcpolsCAApprovedForm10Fields.StateCode = stateCode
    _wcpolsCAApprovedForm10Fields.RecordTypeCode = caf10RecTypeCode
    _wcpolsCAApprovedForm10Fields.FutureReserved1 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementNumber = caf10EndNumber
    _wcpolsCAApprovedForm10Fields.BureauVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.CarrierVersionIdentifier = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSerialNumber = blank
    _wcpolsCAApprovedForm10Fields.Title10Wording = blank
    _wcpolsCAApprovedForm10Fields.NameOfEmployee = {}
    _wcpolsCAApprovedForm10Fields.NameOfOperation = operation
    _wcpolsCAApprovedForm10Fields.OperationTitle = operationTitle
    _wcpolsCAApprovedForm10Fields.AddressOfLocation = loc
    _wcpolsCAApprovedForm10Fields.ClassificationCode = classificationCode
    _wcpolsCAApprovedForm10Fields.ClassificationWordingSuffix = blank
    _wcpolsCAApprovedForm10Fields.ClassificationWording = blank
    _wcpolsCAApprovedForm10Fields.FutureReserved2 = blank
    _wcpolsCAApprovedForm10Fields.EndorsementSequenceNumber = endSeqNum
    _wcpolsCAApprovedForm10Fields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsCAApprovedForm10Fields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsCAApprovedForm10Fields.EndorsementExpirationDate = blank

    return _wcpolsCAApprovedForm10Fields
  }


  /**
   * Assigns and returns values for fields of California approved form 11 record.
   **/
  static function createIndividualcaf11Record(pp:PolicyPeriod):TDIC_WCpolsCAApprovedForm11Fields {

    var _wcpolsCAApprovedForm11Fields = new TDIC_WCpolsCAApprovedForm11Fields()
    _wcpolsCAApprovedForm11Fields.CarrierCode = carrierCode
    _wcpolsCAApprovedForm11Fields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsCAApprovedForm11Fields.FutureReserved = blank
    _wcpolsCAApprovedForm11Fields.UnitCertificateIdentifier = blank
    _wcpolsCAApprovedForm11Fields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsCAApprovedForm11Fields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsCAApprovedForm11Fields.TransactionCode = getTransactionCode(pp)
    _wcpolsCAApprovedForm11Fields.StateCode = stateCode
    _wcpolsCAApprovedForm11Fields.RecordTypeCode = caf11RecTypeCode
    _wcpolsCAApprovedForm11Fields.FutureReserved1 = blank
    _wcpolsCAApprovedForm11Fields.EndorsementNumber = caf11EndNumber
    _wcpolsCAApprovedForm11Fields.BureauVersionIdentifier = blank
    _wcpolsCAApprovedForm11Fields.CarrierVersionIdentifier = blank
    _wcpolsCAApprovedForm11Fields.EndorsementSerialNumber = blank
    _wcpolsCAApprovedForm11Fields.ExcludedOperationDescription = {}
    _wcpolsCAApprovedForm11Fields.FutureReserved2 = blank
    _wcpolsCAApprovedForm11Fields.EndorsementSequenceNumber = endSeqNum
    _wcpolsCAApprovedForm11Fields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsCAApprovedForm11Fields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsCAApprovedForm11Fields.EndorsementExpirationDate = getPolicyExpDate(pp)

    return _wcpolsCAApprovedForm11Fields
  }

  /**
   * Assigns and returns values for Trailer record.
   **/
   static function createTrailerRecords(recordCount:int,policyHeaderRecordCount:int):TDIC_WCpolsReportTrailerFields {
    var _wcpolsReportTrailerFields = new TDIC_WCpolsReportTrailerFields()

    _wcpolsReportTrailerFields.Blank = blank
    _wcpolsReportTrailerFields.RecordTypeCode = recTypeCode
    _wcpolsReportTrailerFields.RecordTotals = recordCount
    _wcpolsReportTrailerFields.HeaderRecordTotals = policyHeaderRecordCount
    _wcpolsReportTrailerFields.TransactionFromDate = getTransactionFromDate()
    _wcpolsReportTrailerFields.TransactionToDate = getDataProcessedDate()
    _wcpolsReportTrailerFields.EndBlank = blank

    return _wcpolsReportTrailerFields
  }

  /**
   * Returns Transaction from date value of report.
   **/
  static function getTransactionFromDate() : String{

    var sdf: DateFormat = new SimpleDateFormat("yyyyMMdd")
    return sdf.format(gw.api.util.DateUtil.currentDate().addDays(-7)).toString().replaceAll("[^0-9]", "")

  }

  // Creates and returns experience modification change records.
  static function createExpModFactChangeRecords(expRatingModFactChangeEndRec:ArrayList<TDIC_WCpolsExpRatingModChangeEndRecFields>,pp:PolicyPeriod,mod:WC7Modifier,rateFact:WC7RateFactor){

    var policyLine = pp.WC7Line
    var jurisdictions=policyLine.WC7Jurisdictions
    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var ratingPeriods : List<WC7RatingPeriod>
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var allRatingPeriods  = elt.RatingPeriods
      var allModifierVersions = elt.AllModifierVersions

      allModifierVersions.each( \ p -> p.Pattern.CodeIdentifier)
      var modifiers:List<WC7Modifier>
      if (allRatingPeriods.Count  > 1) {
        modifiers= allModifierVersions.where( \ mod1 -> not ((mod1.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)
      } else {
        modifiers =  allModifierVersions
      }
      //var ratingPeriodCount=0
      if(mod != null){
        ratingPeriods = allRatingPeriods.where( \ rp ->  gw.api.util.DateUtil.compareIgnoreTime(mod.EffectiveDate, rp.Start) == 0
            and gw.api.util.DateUtil.compareIgnoreTime(mod.ExpirationDate, rp.End) == 0)
      }
      if(rateFact!=null){
        ratingPeriods = allRatingPeriods.where( \ rp ->  gw.api.util.DateUtil.compareIgnoreTime(mod.EffectiveDate, rp.Start) == 0
            and gw.api.util.DateUtil.compareIgnoreTime(mod.ExpirationDate, rp.End) == 0)
      }
      ratingPeriods.each( \ rp -> {
        //ratingPeriodCount +=1
        var rpModifiers =  allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary
            and gw.api.util.DateUtil.compareIgnoreTime(m.EffectiveDate, rp.Start) == 0
            and gw.api.util.DateUtil.compareIgnoreTime(m.ExpirationDate, rp.End) == 0 )

        var expMod= rpModifiers.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ExpMod")
        var expModFactChgRecord = createIndividualExpModChangeRecord(pp)
        expRatingModFactChangeEndRec.add(expModFactChgRecord)
        expModFactChgRecord.ExpModFactor= (expMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", "")) != null ? expMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", ""): '0000'

        if(expMod?.RateWithinLimits != null) {
          expModFactChgRecord.ExpModStatusCode = expModStatusCode
        }
        else {
          expModFactChgRecord.ExpModStatusCode = expModStatusCodeWithOutXmod
        }

        expModFactChgRecord.ExpModEffectiveDate = sdf.format(expMod.EffectiveDate).toString().replaceAll("[^0-9]", "")

      })
    })
  }

  /**
   * Assigns values to fields for Experience rating modification change endorsement record.
   **/
  static function createIndividualExpModChangeRecord(pp:PolicyPeriod): TDIC_WCpolsExpRatingModChangeEndRecFields{

    var _wcpolsExpRatingModChgEndRec = new TDIC_WCpolsExpRatingModChangeEndRecFields()
    _wcpolsExpRatingModChgEndRec.CarrierCode = carrierCode
    _wcpolsExpRatingModChgEndRec.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsExpRatingModChgEndRec.FutureReserved = blank
    _wcpolsExpRatingModChgEndRec.UnitCertificateIdentifier = blank
    _wcpolsExpRatingModChgEndRec.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsExpRatingModChgEndRec.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsExpRatingModChgEndRec.TransactionCode = endorsementTransactionCode
    _wcpolsExpRatingModChgEndRec.StateCode = stateCode
    _wcpolsExpRatingModChgEndRec.RecordTypeCode = expModFactChgRecTypeCode
    _wcpolsExpRatingModChgEndRec.FutureReserved1 = blank
    _wcpolsExpRatingModChgEndRec.EndorsementNumber = expModFactChgEndNum
    _wcpolsExpRatingModChgEndRec.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsExpRatingModChgEndRec.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsExpRatingModChgEndRec.FutureReserved2 = blank
    _wcpolsExpRatingModChgEndRec.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsExpRatingModChgEndRec.EndorsementEffDate = getEndorsementEffDate(pp)
    _wcpolsExpRatingModChgEndRec.FutureReserved3 = blank
    return _wcpolsExpRatingModChgEndRec
  }

  // Creates and returns state premium change records based on rating periods.
  static function createStatePremiumChangeRecords(statePremiumChangeEndRec:ArrayList<TDIC_WCpolsStatePremiumChangeRecFields>,pp:PolicyPeriod,endorsementNo:String,mod:WC7Modifier,rateFact:WC7RateFactor){

    var policyLine = pp.WC7Line
    var jurisdictions=policyLine.WC7Jurisdictions
    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var ratingPeriods : List<WC7RatingPeriod>
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var allRatingPeriods = elt.RatingPeriods
      var allModifierVersions = elt.AllModifierVersions

      allModifierVersions.each( \ p -> p.Pattern.CodeIdentifier)
      var modifiers:List<WC7Modifier>
      if (allRatingPeriods.Count > 1) {
        modifiers= allModifierVersions.where( \ mod1 -> not ((mod1.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)
      } else {
        modifiers =  allModifierVersions
      }
      if(mod != null){
        ratingPeriods = allRatingPeriods.where( \ rp ->  gw.api.util.DateUtil.compareIgnoreTime(mod.EffectiveDate, rp.Start) == 0
          and gw.api.util.DateUtil.compareIgnoreTime(mod.ExpirationDate, rp.End) == 0)
      }
      if(rateFact!=null){
        ratingPeriods = allRatingPeriods.where( \ rp ->  gw.api.util.DateUtil.compareIgnoreTime(rateFact.EffectiveDate, rp.Start) == 0
          and gw.api.util.DateUtil.compareIgnoreTime(rateFact.ExpirationDate, rp.End) == 0)
      }
      ratingPeriods.each( \ rp -> {
       // ratingPeriodCount +=1
        var statePremChgRecord = createIndividualStatePremiumChangeRecord(pp,rp,endorsementNo)
        statePremiumChangeEndRec.add(statePremChgRecord)

        var rpModifiers =  allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary
            and gw.api.util.DateUtil.compareIgnoreTime(m.EffectiveDate, rp.Start) == 0
            and gw.api.util.DateUtil.compareIgnoreTime(m.ExpirationDate, rp.End) == 0 )

        var expMod= rpModifiers.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ExpMod")
        var schdMod= allModifierVersions.firstWhere( \ k -> k.Pattern.CodeIdentifier=="WC7ScheduleMod")
        statePremChgRecord.OtherIndividualRiskRatingFactor= schdMod?.RateWithinLimits?.toString()?.replaceAll("[^0-9]", "")

        var stateCosts = partitionCosts.get(elt.Jurisdiction)
        var stateCostsExp = stateCosts.toSet().getOtherPremiumAndSurcharges(pp.PreferredSettlementCurrency)
        var costForexpConstant = stateCostsExp.where( \ kk ->  kk.Cost typeis WC7Cost and  kk.Cost.ClassCode=="0900").first()
        var expConstant = (costForexpConstant.Cost as WC7Cost).ActualAmount

        if(allRatingPeriods.last() != rp){
          statePremChgRecord.PreviousReportedAnnivRatingDate= sdf.format(pp.PeriodStart).toString().replaceAll("[^0-9]", "")
          statePremChgRecord.ExpenseConstantAmnt = 0000000000
        }
        else {
          statePremChgRecord.PreviousReportedAnnivRatingDate= sdf.format(schdMod.BasedOn.EffectiveDate).toString().replaceAll("[^0-9]", "")
          statePremChgRecord.ExpenseConstantAmnt = expConstant as int
        }
        statePremChgRecord.PreviousReportedExpModEffDate = sdf.format(expMod.BasedOn.EffectiveDate).toString().replaceAll("[^0-9]", "")
      })
    })
  }

  /**
   * Assigns values to fields for State Premium Change record.
   **/
  static function createIndividualStatePremiumChangeRecord(pp:PolicyPeriod,rp:WC7RatingPeriod,endNum:String): TDIC_WCpolsStatePremiumChangeRecFields{

    var _wcpolsStatePremiumChangeRecordFields = new TDIC_WCpolsStatePremiumChangeRecFields()
    _wcpolsStatePremiumChangeRecordFields.CarrierCode = carrierCode
    _wcpolsStatePremiumChangeRecordFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsStatePremiumChangeRecordFields.FutureReserved = blank
    _wcpolsStatePremiumChangeRecordFields.UnitCertificateIdentifier = blank
    _wcpolsStatePremiumChangeRecordFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsStatePremiumChangeRecordFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsStatePremiumChangeRecordFields.TransactionCode = endorsementTransactionCode
    _wcpolsStatePremiumChangeRecordFields.StateCode = stateCode
    _wcpolsStatePremiumChangeRecordFields.RecordTypeCode = statePremChgRecTypeCode
    _wcpolsStatePremiumChangeRecordFields.FutureReserved1 = blank
    _wcpolsStatePremiumChangeRecordFields.DataElementChangeIdNum = endNum
    _wcpolsStatePremiumChangeRecordFields.FutureReserved2 = blank
    _wcpolsStatePremiumChangeRecordFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsStatePremiumChangeRecordFields.ExpModPlanTypeCode = blank
    _wcpolsStatePremiumChangeRecordFields.InsurerPremiumDeviationFactor = insurerPremDeviationFact
    _wcpolsStatePremiumChangeRecordFields.TypeOfPremDevCode = typeOfPremDevCode
    _wcpolsStatePremiumChangeRecordFields.EstimatedStateStanPremTot = getStandardPremium(pp,rp) as long
    _wcpolsStatePremiumChangeRecordFields.LossConstantAmnt = lossConstantAmnt
    _wcpolsStatePremiumChangeRecordFields.PremDiscountAmnt = premDiscountAmnt
    _wcpolsStatePremiumChangeRecordFields.ProratedExpenseConstAmntReasonCode = blank
    _wcpolsStatePremiumChangeRecordFields.ProratedMinPremAmntReasonCode = blank
    _wcpolsStatePremiumChangeRecordFields.ReasonStateWasAddedToPolicyCode = blank
    _wcpolsStatePremiumChangeRecordFields.AssignedRiskAdjProgFact = blank
    _wcpolsStatePremiumChangeRecordFields.TypeOfNonStanIdCode = blank
    _wcpolsStatePremiumChangeRecordFields.IndependentDCORiskIdNum = blank
    _wcpolsStatePremiumChangeRecordFields.FutureReserved3 = blank
    _wcpolsStatePremiumChangeRecordFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsStatePremiumChangeRecordFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsStatePremiumChangeRecordFields.FutureReserved4 = blank

    return _wcpolsStatePremiumChangeRecordFields
  }

  // Creates and returns class code change records.
  static function createClassAndOrRateChgEndRecords(classCodeOrRateChgEndRec:ArrayList<TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields>,pp:PolicyPeriod,revCode:String, changedClassCode:WC7CoveredEmployee){

    var sdf: DateFormat = new SimpleDateFormat("yyMMdd")
    var policyLine = pp.WC7Line
    var jurisdictions=pp.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)
    var lineCosts = pp.WC7Line.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())

    jurisdictions.each( \ elt -> {
      var stateCosts = partitionCosts.get(elt.Jurisdiction)
      var ratingPeriods = elt.RatingPeriods
      var ratingPeriodCount = 0
      ratingPeriods.each( \ rp ->{
        var periodCosts = stateCosts.byRatingPeriod(rp).where(\ eltt -> (eltt typeis WC7CovEmpCost and eltt.WC7CoveredEmployee == changedClassCode))

        while(!periodCosts.Empty) {
          var expChangeRecord = createIndividualClassAndOrRateChgAndOthEndRec(pp,revCode)
          if(periodCosts.first().ClassCode != null) {
            classCodeOrRateChgEndRec.add(expChangeRecord)
          }
          expChangeRecord.ExposurePeriodEffDate = '000000'
          var firstCost = periodCosts.first()
          if(firstCost.LocationNum != null) {
            expChangeRecord.EstimatedExposureAmount = firstCost.Basis as long
            if(ratingPeriods.Count > 1){
              expChangeRecord.ExposurePeriodEffDate = sdf.format(rp.Start).toString().replaceAll("[^0-9]", "")
            }
          } else {
            expChangeRecord.EstimatedExposureAmount = 000000000000
          }
          expChangeRecord.ClassificationCode = (firstCost.ClassCode != null) ? firstCost.ClassCode : firstCost.StatCode
          expChangeRecord.ClassificationWordingSuffix = getClassificationWordingSuffix(expChangeRecord.ClassificationCode)
          var premiumAmount = firstCost?.ActualAmount?.toString().replaceAll("[^0-9]", "")
          expChangeRecord.EstimatedPremiumAmount = premiumAmount.substring(0,premiumAmount.length() - 2)
          expChangeRecord.ManualOrChargedRate = firstCost?.ActualAdjRate?.toString()?.replaceAll("[^0-9]", "")
          if(firstCost.LocationNum != null) {
            expChangeRecord.ExposureActOrExposureCovCode = 01
          }
          else {
            expChangeRecord.ExposureActOrExposureCovCode = 00
          }

          periodCosts.remove(firstCost)

        }
      } )
    })

   }

  /**
   * Assigns values to fields for Class and or rate Change and Other endorsement record.
   **/
  static function createIndividualClassAndOrRateChgAndOthEndRec(pp:PolicyPeriod,classCodeRevCode:String) : TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields {

    var _wcpolsClassAndOrRateChgAndOthEndRecFields = new TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields()
    _wcpolsClassAndOrRateChgAndOthEndRecFields.CarrierCode = carrierCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsClassAndOrRateChgAndOthEndRecFields.FutureReserved = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.UnitCertificateIdentifier = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsClassAndOrRateChgAndOthEndRecFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsClassAndOrRateChgAndOthEndRecFields.TransactionCode = endorsementTransactionCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.StateCode = stateCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.RecordTypeCode = classAndOrRateChgEndRecTypeCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.FutureReserved1 = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.EndorsementNumber = classAndOrRateChgEndNum
    _wcpolsClassAndOrRateChgAndOthEndRecFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsClassAndOrRateChgAndOthEndRecFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsClassAndOrRateChgAndOthEndRecFields.ClassificationCodeRevisionCode = classCodeRevCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.ClassificationWording = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.NameLinkIdentifier = expNameLinkIdentifier
    _wcpolsClassAndOrRateChgAndOthEndRecFields.StateCodeLink = expStateCodeLink
    _wcpolsClassAndOrRateChgAndOthEndRecFields.ExposureRecordLinkForExposureCode = expRecLinkForExpCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.ClassificationUseCode = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.ExposurePeriodCode = expPeriodCode
    _wcpolsClassAndOrRateChgAndOthEndRecFields.NumberOfPiecesOfApparatus = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.NumberOfVolunteers = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.FutureReserved2 = blank
    _wcpolsClassAndOrRateChgAndOthEndRecFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsClassAndOrRateChgAndOthEndRecFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsClassAndOrRateChgAndOthEndRecFields.FutureReserved3 = blank

    return _wcpolsClassAndOrRateChgAndOthEndRecFields

  }

  /**
   * Assigns values to fields for Data elements change endorsement record.
   **/
  static function createDataElementsChangeEndRecord(dataElemChgRecord:ArrayList<TDIC_WCpolsDataElementsChgEndRecFields>,recordEndNumber:String,pp:PolicyPeriod,endorsementNum:String,burVerId:String,revCode:String) {

    var _wcpolsDataElementsChgEndRecFields = new TDIC_WCpolsDataElementsChgEndRecFields()
    _wcpolsDataElementsChgEndRecFields.CarrierCode = carrierCode
    _wcpolsDataElementsChgEndRecFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsDataElementsChgEndRecFields.FutureReserved = blank
    _wcpolsDataElementsChgEndRecFields.UnitCertificateIdentifier = blank
    _wcpolsDataElementsChgEndRecFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsDataElementsChgEndRecFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsDataElementsChgEndRecFields.TransactionCode = endorsementTransactionCode
    _wcpolsDataElementsChgEndRecFields.FutureReserved1 = blank
    _wcpolsDataElementsChgEndRecFields.RecordTypeCode = dataElementsChgEndRecType
    _wcpolsDataElementsChgEndRecFields.FutureReserved2 = blank
    _wcpolsDataElementsChgEndRecFields.EndorsementNumber = recordEndNumber
    _wcpolsDataElementsChgEndRecFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsDataElementsChgEndRecFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsDataElementsChgEndRecFields.CarrierCode1 = revisedCarrierCode
    _wcpolsDataElementsChgEndRecFields.PolicyNumberIdentifier = blank
    _wcpolsDataElementsChgEndRecFields.PolicyEffectiveDate1 = revisedPolicyEffDate
    _wcpolsDataElementsChgEndRecFields.PolicyExpirationDate = revisedPolicyExpDate
    _wcpolsDataElementsChgEndRecFields.LegalNatureOfInsuredCode = getOrgCode(pp,recordEndNumber)
    _wcpolsDataElementsChgEndRecFields.TextForOtherLegNatOfInsrd = blank
    _wcpolsDataElementsChgEndRecFields.Item3AOr3CCode = blank
    _wcpolsDataElementsChgEndRecFields.Item3CInclusionOrExclusionCode = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C1 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C2 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C3 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C4 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C5 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C6 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C7 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C8 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C9 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C10 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C11 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C12 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C13 = blank
    _wcpolsDataElementsChgEndRecFields.StateCodesForItem3AOr3C14 = blank
    _wcpolsDataElementsChgEndRecFields.EmprLiabLimAmntBodInjByAcciEachAccAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabLimitEachAccTDICTerm.Value  as long
    _wcpolsDataElementsChgEndRecFields.EmprLiabLimAmntBodInjByDisePolAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabPolicyLimitTerm.Value as long
    _wcpolsDataElementsChgEndRecFields.EmprLiabLimAmntBodInjByDiseEachEmpeAmnt = pp.WC7Line.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabLimitEachEmpTDICTerm.Value as long
    _wcpolsDataElementsChgEndRecFields.PremiumAdjustmentPeriodCode = blank
    _wcpolsDataElementsChgEndRecFields.EndorsementNumber1 = endorsementNum
    _wcpolsDataElementsChgEndRecFields.BureauVersionIdentifier1 = burVerId
    _wcpolsDataElementsChgEndRecFields.CarrierVersionIdentifier1 = blank
    _wcpolsDataElementsChgEndRecFields.NameOfProducer = blank
    _wcpolsDataElementsChgEndRecFields.InterstateRiskIdNumber = blank
    _wcpolsDataElementsChgEndRecFields.FutureReserved3 = blank
    _wcpolsDataElementsChgEndRecFields.EndorsementNumberRevisionCode = revCode
    _wcpolsDataElementsChgEndRecFields.FutureReserved4 = blank
    _wcpolsDataElementsChgEndRecFields.EndorsementSequenceNumber = endSeqNumDataElemRec
    _wcpolsDataElementsChgEndRecFields.NameOfInsured = pp.primaryNameInsured_TDIC
    _wcpolsDataElementsChgEndRecFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsDataElementsChgEndRecFields.FutureReserved5 = blank

    dataElemChgRecord.add(_wcpolsDataElementsChgEndRecFields)

  }

  /**
   * Returns Organization code based on Organization type for data element change endorsement.
   **/
  static function getOrgCode(pp: PolicyPeriod,recEndNum:String):String {

    if(recEndNum != datElementChgEndNumOrgType){
      return blank
    }
    switch(pp.PolicyOrgType_TDIC){

      case typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP:
          return "01"
      case typekey.PolicyOrgType_TDIC.TC_PARTNERSHIP:
          return "02"
      case typekey.PolicyOrgType_TDIC.TC_PRIVATECORP:
          return "03"
      case typekey.PolicyOrgType_TDIC.TC_NONPROFIT:
          return "04"
      case typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE:
          return "06"
      case typekey.PolicyOrgType_TDIC.TC_LLC:
          return "10"
        default:
        return "99"
    }
  }

  /**
   * Assigns values to fields for Name change endorsement record.
   **/
  static function createNameChangeEndRecord(nameChangeRecords:ArrayList<TDIC_WCpolsNameChangeEndRecFields>,namedInsured:PolicyContactRole,pp:PolicyPeriod, nameRevCode:String) {

    var _wcpolsNameChangeEndrecordFields = new TDIC_WCpolsNameChangeEndRecFields()
    _wcpolsNameChangeEndrecordFields.CarrierCode = carrierCode
    _wcpolsNameChangeEndrecordFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsNameChangeEndrecordFields.FutureReserved = blank
    _wcpolsNameChangeEndrecordFields.UnitCertificateIdentifier = blank
    _wcpolsNameChangeEndrecordFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsNameChangeEndrecordFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsNameChangeEndrecordFields.TransactionCode = endorsementTransactionCode
    _wcpolsNameChangeEndrecordFields.FutureReserved1 = blank
    _wcpolsNameChangeEndrecordFields.RecordTypeCode = nameChangeRecTypeCode
    _wcpolsNameChangeEndrecordFields.FutureReserved2 = blank
    _wcpolsNameChangeEndrecordFields.EndorsementNumber = nameChangeEndNum
    _wcpolsNameChangeEndrecordFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsNameChangeEndrecordFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsNameChangeEndrecordFields.NameTypeCode = getNameTypeCode(namedInsured)
    _wcpolsNameChangeEndrecordFields.NameLinkIdentifier = nameLinkIdentifier
    _wcpolsNameChangeEndrecordFields.NameOfInsured = getNameOfInsuredForEndorsements(pp,namedInsured)
    _wcpolsNameChangeEndrecordFields.FedEmpIdNum = getFedEmpIdNumer(pp,namedInsured)
    _wcpolsNameChangeEndrecordFields.ConSeqNum = nameChangeRecords.Count+1
    _wcpolsNameChangeEndrecordFields.LegNatOfEntityCode = blank
    _wcpolsNameChangeEndrecordFields.TextForOtherLegNatOfEntity = blank
    _wcpolsNameChangeEndrecordFields.StateCodes = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums = blank
    _wcpolsNameChangeEndrecordFields.StateCodes1 = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums1 = blank
    _wcpolsNameChangeEndrecordFields.StateCodes2 = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums2 = blank
    _wcpolsNameChangeEndrecordFields.FutureReserved3 = blank
    _wcpolsNameChangeEndrecordFields.NameRevisionCode = nameRevCode
    _wcpolsNameChangeEndrecordFields.FutureReserved4 = blank
    _wcpolsNameChangeEndrecordFields.ProfEmprOrgnOrClientCompCode = blank
    _wcpolsNameChangeEndrecordFields.NameOfInsured1 = pp.primaryNameInsured_TDIC
    _wcpolsNameChangeEndrecordFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsNameChangeEndrecordFields.NameLinkCounterIdntfr = nameLinkCntrIdentifier
    _wcpolsNameChangeEndrecordFields.FutureReserved5 = blank

    nameChangeRecords.add(_wcpolsNameChangeEndrecordFields)
  }

  static function getNameOfInsuredForEndorsements(pp:PolicyPeriod,policyNamedInsured : PolicyContactRole): String {
    if((policyNamedInsured.AccountContactRole.AccountContact.Contact typeis Person)){
      if(policyNamedInsured.MiddleName == null){
        if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC) and (policyNamedInsured as String != pp.BasedOn.primaryNameInsured_TDIC)) {
          return policyNamedInsured.LastName+','+policyNamedInsured.FirstName + nameType
        }
        return policyNamedInsured.LastName+','+policyNamedInsured.FirstName
      }
      else {
        if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC) and (policyNamedInsured as String != pp.BasedOn.primaryNameInsured_TDIC)) {
          return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured.MiddleName + nameType
        }
        //return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured?.AccountContactRole?.AccountContact?.Contact?.MiddleName
        return policyNamedInsured.LastName+','+policyNamedInsured.FirstName+','+policyNamedInsured.MiddleName
      }
    }
    else {
      if((pp.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) and (policyNamedInsured as String != pp.primaryNameInsured_TDIC ) and (policyNamedInsured as String != pp.BasedOn.primaryNameInsured_TDIC)) {
        return policyNamedInsured.CompanyName + nameType
      }
      return policyNamedInsured.CompanyName
    }
  }

  /**
   * Assigns values to fields for Name change(DBA) endorsement record.
   **/
  static function createDbaNameChangeEndRecord(nameChangeRecords:ArrayList<TDIC_WCpolsNameChangeEndRecFields>,loc:PolicyLocation,pp:PolicyPeriod, nameRevCode:String) {

    var _wcpolsNameChangeEndrecordFields = new TDIC_WCpolsNameChangeEndRecFields()
    _wcpolsNameChangeEndrecordFields.CarrierCode = carrierCode
    _wcpolsNameChangeEndrecordFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsNameChangeEndrecordFields.FutureReserved = blank
    _wcpolsNameChangeEndrecordFields.UnitCertificateIdentifier = blank
    _wcpolsNameChangeEndrecordFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsNameChangeEndrecordFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsNameChangeEndrecordFields.TransactionCode = endorsementTransactionCode
    _wcpolsNameChangeEndrecordFields.FutureReserved1 = blank
    _wcpolsNameChangeEndrecordFields.RecordTypeCode = nameChangeRecTypeCode
    _wcpolsNameChangeEndrecordFields.FutureReserved2 = blank
    _wcpolsNameChangeEndrecordFields.EndorsementNumber = nameChangeEndNum
    _wcpolsNameChangeEndrecordFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsNameChangeEndrecordFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsNameChangeEndrecordFields.NameTypeCode = nameTypeCodeForDBA
    _wcpolsNameChangeEndrecordFields.NameLinkIdentifier = nameLinkIdentifier
    _wcpolsNameChangeEndrecordFields.NameOfInsured = loc.DBA_TDIC + nameTypeDBA
    _wcpolsNameChangeEndrecordFields.FedEmpIdNum = getFeinForDba(pp,pp.PrimaryNamedInsured)
    _wcpolsNameChangeEndrecordFields.ConSeqNum = nameChangeRecords.Count+1
    _wcpolsNameChangeEndrecordFields.LegNatOfEntityCode = blank
    _wcpolsNameChangeEndrecordFields.TextForOtherLegNatOfEntity = blank
    _wcpolsNameChangeEndrecordFields.StateCodes = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums = blank
    _wcpolsNameChangeEndrecordFields.StateCodes1 = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums1 = blank
    _wcpolsNameChangeEndrecordFields.StateCodes2 = blank
    _wcpolsNameChangeEndrecordFields.StateUnemplNums2 = blank
    _wcpolsNameChangeEndrecordFields.FutureReserved3 = blank
    _wcpolsNameChangeEndrecordFields.NameRevisionCode = nameRevCode
    _wcpolsNameChangeEndrecordFields.FutureReserved4 = blank
    _wcpolsNameChangeEndrecordFields.ProfEmprOrgnOrClientCompCode = blank
    _wcpolsNameChangeEndrecordFields.NameOfInsured1 = pp.primaryNameInsured_TDIC
    _wcpolsNameChangeEndrecordFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsNameChangeEndrecordFields.NameLinkCounterIdntfr = nameLinkCntrIdentifier
    _wcpolsNameChangeEndrecordFields.FutureReserved5 = blank

    nameChangeRecords.add(_wcpolsNameChangeEndrecordFields)
  }

    /**
     * Assigns values to fields for Address change endorsement record.
     **/
  static function createAddressChangeEndRecords(addChangeRecords:ArrayList<TDIC_WCpolsAddressChangeEndRecFields>,pp:PolicyPeriod) {

    var addOwner = new gw.pcf.policysummary.PolicyInfoAddressOwner(new gw.api.address.AddressAutofillableDelegate(pp.Policy.Account.AccountHolderContact.PrimaryAddress), true)

    var _wcpolsAddressChangeEndrecordFields = new TDIC_WCpolsAddressChangeEndRecFields()
    _wcpolsAddressChangeEndrecordFields.CarrierCode = carrierCode
    _wcpolsAddressChangeEndrecordFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsAddressChangeEndrecordFields.FutureReserved = blank
    _wcpolsAddressChangeEndrecordFields.UnitCertificateIdentifier = blank
    _wcpolsAddressChangeEndrecordFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsAddressChangeEndrecordFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsAddressChangeEndrecordFields.TransactionCode = endorsementTransactionCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved1 = blank
    _wcpolsAddressChangeEndrecordFields.RecordTypeCode = addressChangeRecTypeCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved2 = blank
    _wcpolsAddressChangeEndrecordFields.EndorsementNumber = addressChangeEndNumMailing
    _wcpolsAddressChangeEndrecordFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsAddressChangeEndrecordFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsAddressChangeEndrecordFields.AddressTypeCode = insuredMailAddChgAddTypeCode
    _wcpolsAddressChangeEndrecordFields.AddressStructureCode = addressStructureCode
    _wcpolsAddressChangeEndrecordFields.Street = addOwner.AddressDelegate.AddressLine1 + blank + (addOwner.AddressDelegate.AddressLine2 != null ? addOwner.AddressDelegate.AddressLine2 : blank)
    _wcpolsAddressChangeEndrecordFields.City = addOwner.AddressDelegate.City
    _wcpolsAddressChangeEndrecordFields.State = addOwner.AddressDelegate.State as String
    _wcpolsAddressChangeEndrecordFields.ZipCode = addOwner.AddressDelegate.PostalCode.replaceAll("[^0-9]", "")
    _wcpolsAddressChangeEndrecordFields.NameLinkIdentifier = addressNameLinkIdentifier
    _wcpolsAddressChangeEndrecordFields.StateCodeLink = stateCodeLink
    _wcpolsAddressChangeEndrecordFields.ExposureRecLinkForLocCode = expRecLinkForLocCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved3 = blank
    _wcpolsAddressChangeEndrecordFields.EmailAddress = getEmailAddress(pp, insuredMailAddChgAddTypeCode)
    _wcpolsAddressChangeEndrecordFields.ForeignAddressInd = foreignAddressInd
    _wcpolsAddressChangeEndrecordFields.GeographicArea = blank
    _wcpolsAddressChangeEndrecordFields.CountryCode = blank
    _wcpolsAddressChangeEndrecordFields.EmailAddressContinued = blank
    _wcpolsAddressChangeEndrecordFields.AddressRevisionCode = blank
    _wcpolsAddressChangeEndrecordFields.NameOfInsured1 = pp.primaryNameInsured_TDIC
    _wcpolsAddressChangeEndrecordFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsAddressChangeEndrecordFields.NameLinkCounterIdntfr = addressNameLinkCntrIdentifier
    _wcpolsAddressChangeEndrecordFields.FutureReserved4 = blank


      addChangeRecords.add(_wcpolsAddressChangeEndrecordFields)

  }

  /**
   * Assigns values to fields for Location address change endorsement record.
   **/
  static function createLocationAddressChangeEndRecords(addChangeRecords:ArrayList<TDIC_WCpolsAddressChangeEndRecFields>,locAddress: PolicyLocation,pp:PolicyPeriod,addChgTypeCode:String){

    var _wcpolsAddressChangeEndrecordFields = new TDIC_WCpolsAddressChangeEndRecFields()
    _wcpolsAddressChangeEndrecordFields.CarrierCode = carrierCode
    _wcpolsAddressChangeEndrecordFields.PolicyNumber = polNumPrefix + pp.PolicyNumber + getTermNumber(pp)
    _wcpolsAddressChangeEndrecordFields.FutureReserved = blank
    _wcpolsAddressChangeEndrecordFields.UnitCertificateIdentifier = blank
    _wcpolsAddressChangeEndrecordFields.PolicyEffectiveDate = getPolicyEffDate(pp)
    _wcpolsAddressChangeEndrecordFields.TransactionIssueDate = getTranIssueDate(pp)
    _wcpolsAddressChangeEndrecordFields.TransactionCode = endorsementTransactionCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved1 = blank
    _wcpolsAddressChangeEndrecordFields.RecordTypeCode = addressChangeRecTypeCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved2 = blank
    _wcpolsAddressChangeEndrecordFields.EndorsementNumber = addressChangeEndNumLocation
    _wcpolsAddressChangeEndrecordFields.BureauVersionIdentifier = endBureauVerIdntfr
    _wcpolsAddressChangeEndrecordFields.CarrierVersionIdentifier = endCarrierVerIdntfr
    _wcpolsAddressChangeEndrecordFields.AddressTypeCode = locAddChgAddTypeCode
    _wcpolsAddressChangeEndrecordFields.AddressStructureCode = addressStructureCode
    _wcpolsAddressChangeEndrecordFields.Street = locAddress.AddressLine1 + blank + (locAddress.AddressLine2 != null ? locAddress.AddressLine2 : blank)
    _wcpolsAddressChangeEndrecordFields.City = locAddress.City
    _wcpolsAddressChangeEndrecordFields.State = locAddress.State as String
    _wcpolsAddressChangeEndrecordFields.ZipCode = locAddress.PostalCode.replaceAll("[^0-9]", "")
    _wcpolsAddressChangeEndrecordFields.NameLinkIdentifier = addressNameLinkIdentifier
    _wcpolsAddressChangeEndrecordFields.StateCodeLink = stateCodeLink
    _wcpolsAddressChangeEndrecordFields.ExposureRecLinkForLocCode = expRecLinkForLocCode
    _wcpolsAddressChangeEndrecordFields.FutureReserved3 = blank
    _wcpolsAddressChangeEndrecordFields.EmailAddress = getEmailAddress(pp, insuredMailAddChgAddTypeCode)
    _wcpolsAddressChangeEndrecordFields.ForeignAddressInd = foreignAddressInd
    _wcpolsAddressChangeEndrecordFields.GeographicArea = blank
    _wcpolsAddressChangeEndrecordFields.CountryCode = blank
    _wcpolsAddressChangeEndrecordFields.EmailAddressContinued = blank
    _wcpolsAddressChangeEndrecordFields.AddressRevisionCode = addChgTypeCode
    _wcpolsAddressChangeEndrecordFields.NameOfInsured1 = pp.primaryNameInsured_TDIC
    _wcpolsAddressChangeEndrecordFields.EndorsementEffectiveDate = getEndorsementEffDate(pp)
    _wcpolsAddressChangeEndrecordFields.NameLinkCounterIdntfr = addressNameLinkCntrIdentifier
    _wcpolsAddressChangeEndrecordFields.FutureReserved4 = blank

    addChangeRecords.add(_wcpolsAddressChangeEndrecordFields)
  }
 }