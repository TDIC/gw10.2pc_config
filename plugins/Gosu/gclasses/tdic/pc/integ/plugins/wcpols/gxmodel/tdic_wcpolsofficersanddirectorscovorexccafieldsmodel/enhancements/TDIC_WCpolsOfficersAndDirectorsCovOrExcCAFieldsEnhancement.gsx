package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsofficersanddirectorscovorexccafieldsmodel.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields(object, options)
  }

}