package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/13/14
 * Time: 2:11 PM
 * This class includes variables for all fields in state premium record for policy specification report.
 */
class TDIC_WCpolsStatePremiumFields  extends  TDIC_FlatFileLineGenerator {
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _stateAddDeleteCode : String as StateAddDeleteCode
  var _futureReserved1 : String as FutureReserved1
  var _independentDCORiskIdNum : String as IndependentDCORiskIdNum
  var _futureReserved2 : String as FutureReserved2
  var _stateCovCarrierCode : int as StateCovCarrierCode
  var _expModFactOrMeritRatingFact : String as ExpModFactOrMeritratingFact
  var _expModStatusCode : int as ExpModStatusCode
  var _expModPlanTypeCode : String as ExpModPlanTypeCode
  var _otherIndividualRiskRatingFact : String as OtherIndivdualRiskRatingFact
  var _insurerPremiumDevFact : int as InsurerPremiumDevFact
  var _typeOfPremDevCode : int as TypeOfPremDevCode
  var _estimatedStateStanPremTot : long as EstimatedStateStanPremTot
  var _expenseConstantAmnt : int as ExpenseConstantAmnt
  var _lossConstantAmnt : int as LossConstantAmnt
  var _premDiscountAmnt : int as PremDiscountAmnt
  var _proratedExpenseConstAmntReasonCode : String as ProratedExpenseConstAmntReasonCode
  var _proratedMinPremAmntReasonCode : String as ProratedMinPremAmntReasonCode
  var _reasonStateWasAddedToPolCode : String as ReasonStateWasAddedToPolicyCode
  var _futureReserved3 : String as FutureReserved3
  var _expModEffDate : String as ExpModEffDate
  var _annivRatingDate : String as AnnivRatingDate
  var _assignedRiskAdjProgFact : String as AssignedRiskAdjProgFact
  var _futureReserved4 : String as FutureReserved4
  var _premAdjPeriodCode : int as PremAdjPeriodCode
  var _typeofNonStanIdCode : String as TypeOfNonStanIdCode
  var _futureReserved5 : String as FutureReserved5
  var _polChngEffDate : String as PolChngEffDate
  var _polChngExpDate : String as PolChngExpDate
}