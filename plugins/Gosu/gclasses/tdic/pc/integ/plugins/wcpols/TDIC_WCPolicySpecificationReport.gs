package tdic.pc.integ.plugins.wcpols

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses gw.processes.BatchProcessBase
uses java.util.ArrayList
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_EndorsementNumberFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm10Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm11Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCancellationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsDataElementsChgEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsEndorsementIdentificationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExpRatingModChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExposureFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPolicyHeaderFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsRecordTypes
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReinstatementFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumChangeRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsTransaction
uses java.sql.Connection
uses com.tdic.util.database.DatabaseManager
uses java.io.IOException
uses java.sql.SQLException
uses java.lang.Exception
uses java.util.Set
uses tdic.util.flatfilegenerator.TDIC_FlatFileUtilityHelper
uses java.io.File
uses tdic.pc.integ.plugins.wcpols.helper.TDIC_WCpolsUtil

uses java.text.SimpleDateFormat
uses java.text.DateFormat
uses com.tdic.util.misc.EmailUtil
uses gw.pl.exception.GWConfigurationException
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/5/15
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCPolicySpecificationReport extends BatchProcessBase {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")
  var polHeaderRecCount = 0
  construct() {
    super(BatchProcessType.TC_WCPOLSREPORTBATCH)
  }

  /**
   * Extracts payload  from database and generates flat file content.
   **/
  override function doWork() {

    var _con: Connection
    var allrecords = retrieveAllWCPolsTransactionRecords()
    var wcpolRecords:ArrayList<TDIC_WCpolsRecordTypes> = new ArrayList<TDIC_WCpolsRecordTypes>()
    allrecords.each( \ elt -> {wcpolRecords.add(createRecordType(elt.Payload))})

    var wcPolsComplete = new TDIC_WCPolsCompleteRecord()
    wcPolsComplete.WCPolsRecords = wcpolRecords
    wcPolsComplete.ReportHeaderRecord = TDIC_WCpolsUtil.createReportHeaderRecordFields()
    var recordCount=1
    wcpolRecords.each( \ elt -> {
      recordCount +=elt.getCount()
    })
    wcPolsComplete.ReportTrailerRecord = TDIC_WCpolsUtil.createTrailerRecords(recordCount,polHeaderRecCount)

    try{
      writeFlatFile(wcPolsComplete)
      //GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
      var _envIntDbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      _con = DatabaseManager.getConnection(_envIntDbUrl)
      allrecords.each( \ elt -> {
        var _sqlStmt = "update WCPOLS set Processed = 1 where ID = '"+ elt.ID+"'"
        var _return = DatabaseManager.executeStmt(_con, _sqlStmt)
        _logger.debug(" Message Processed")
      })
      EmailUtil.sendEmail(TDIC_WCpolsUtil.WC_STAT_POL_NOTIFICATION_EMAIL, "WCPOLS Report batch completed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch has been completed successfully.")
    } catch (io:IOException) {
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - IOException= " + io)
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else{
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch job failed due to error:" + io.LocalizedMessage)
      }
      throw io
    }catch (sql:SQLException) {
      _logger.error("TDIC_WCPolicySpecificationReport#doWork() - SQLException= " + sql)
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCPolicySpecificationReport#doWork() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else{
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch job failed due to error:" + sql.LocalizedMessage)
      }
      throw sql
    } catch (ex:Exception) {
      _logger.error("TDIC_WCPolicySpecificationReport#doWork() - Exception= " + ex)
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCPolicySpecificationReport#doWork() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else{
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch job failed due to error:" + ex.LocalizedMessage)
      }
      throw ex
    }finally {
      try {
        if (_con != null) _con.close()
      } catch (fe:Exception) {
        _logger.error("TDIC_WCPolicySpecificationReport#doWork() - finally Exception= " + fe)
      }
    }
  }

  /**
   * Retrieves records from DB.
   **/
  function retrieveAllWCPolsTransactionRecords(): ArrayList<TDIC_WCpolsTransaction> {
    var wcPolsTrn = new TDIC_WCpolsTransaction()
    var allrecords = new ArrayList<TDIC_WCpolsTransaction>()
    var _con: Connection
    try{
     //GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
      var _envIntDbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      _con = DatabaseManager.getConnection(_envIntDbUrl)
      var _sqlStmt = "select * from WCPOLS where Processed=0"
      var results = DatabaseManager.executeQuery(_con, _sqlStmt)
      while (results.next()) {
        var tran = new TDIC_WCpolsTransaction()
        tran.CreatedDate = results.getDate("CreatedDate")
        tran.ID = results.getString("ID")
        tran.Payload = results.getString("Payload")
        allrecords.add(tran)
      }
      _logger.debug("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - Exiting")

    } catch (sql:SQLException) {
      _logger.error("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - SQLException= " + sql)
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch job failed due to error:" + sql.LocalizedMessage)
      }
      throw sql
    } catch (e:Exception) {
      _logger.error("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - Exception= " + e)
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "WCPOLS Report batch job failed due to error:" + e.LocalizedMessage)
      }
      throw e
    } finally {
      try {
        if (_con != null) _con.close()
      } catch (fe:Exception) {
        _logger.error("TDIC_WCPolicySpecificationReport#retrieveAllWCPolsTransactionRecords() - finally Exception= " + fe)
      }
    }
    return allrecords
  }

  function createRecordType(xml:String):TDIC_WCpolsRecordTypes{

    var pload = tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsrecordtypesmodel.TDIC_WCpolsRecordTypes.parse(xml)
    var record = new TDIC_WCpolsRecordTypes()

    if(pload.PolicyHeaderRecord != null){
      var policyHeaderRecord = new TDIC_WCpolsPolicyHeaderFields()
      loadFromModel(policyHeaderRecord,pload.PolicyHeaderRecord,policyHeaderRecord.objHashMap().keySet())
      record.PolicyHeaderRecord = policyHeaderRecord
      polHeaderRecCount++
    }

    record.NameRecord = new ArrayList<TDIC_WCpolsNameFields>()
    for(entry in pload.NameRecord.Entry){
      var nameRecord = new TDIC_WCpolsNameFields()
      loadFromModel(nameRecord,entry,nameRecord.objHashMap().keySet())
      record.NameRecord.add(nameRecord)
    }

    record.StatePremiumRecord = new ArrayList<TDIC_WCpolsStatePremiumFields>()
    for(entry in pload.StatePremiumRecord.Entry){
      var statePremiumRecord = new TDIC_WCpolsStatePremiumFields()
      loadFromModel(statePremiumRecord,entry,statePremiumRecord.objHashMap().keySet())
      record.StatePremiumRecord.add(statePremiumRecord)
    }

    record.ExposureRecord = new ArrayList<TDIC_WCpolsExposureFields>()
    for(entry in pload.ExposureRecord.Entry){
      var exposureRecord = new TDIC_WCpolsExposureFields()
      loadFromModel(exposureRecord,entry,exposureRecord.objHashMap().keySet())
      record.ExposureRecord.add(exposureRecord)
    }

    var partnershipCovOrExclusionEndCAReecords = pload.PartnershipCovOrExclusionEndCARecord.Entry
    record.PartnershipCovOrExclusionEndCARecord = new ArrayList<TDIC_WCpolsPartnershipCovOrExclusionEndCAFields>()
    for(entry in partnershipCovOrExclusionEndCAReecords){
      var partnershipCovOrExclusionEndCARecord = new TDIC_WCpolsPartnershipCovOrExclusionEndCAFields()
      partnershipCovOrExclusionEndCARecord.NameOfGeneralPartnersExcluded = new ArrayList<String>()
      for(i in entry.NameOfGeneralPartnersExcluded.Entry ){
        var endNo = i
        partnershipCovOrExclusionEndCARecord.NameOfGeneralPartnersExcluded.add(endNo)
      }
      var endFields = partnershipCovOrExclusionEndCARecord.objHashMap().keySet()
      endFields.remove("NameOfGeneralPartnersExcluded")
      loadFromModel(partnershipCovOrExclusionEndCARecord,entry,endFields)
      record.PartnershipCovOrExclusionEndCARecord.add(partnershipCovOrExclusionEndCARecord)
    }

    var officersAndDirectorsCovOrExcCARecords = pload.OfficersAndDirectorsCovOrExcCARecord.Entry
    record.OfficersAndDirectorsCovOrExcCARecord = new ArrayList<TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields>()
    for(entry in officersAndDirectorsCovOrExcCARecords){
      var officersAndDirectorsCovOrExcCARecord = new TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields()
      officersAndDirectorsCovOrExcCARecord.NameAndTitleOfOffiOrDireExcluded = new ArrayList<String>()
      for(i in entry.NameAndTitleOfOffiOrDireExcluded.Entry ){
        var endNo = i
        officersAndDirectorsCovOrExcCARecord.NameAndTitleOfOffiOrDireExcluded.add(endNo)
      }
      var endFields = officersAndDirectorsCovOrExcCARecord.objHashMap().keySet()
      endFields.remove("NameAndTitleOfOffiOrDireExcluded")
      loadFromModel(officersAndDirectorsCovOrExcCARecord,entry,endFields)
      record.OfficersAndDirectorsCovOrExcCARecord.add(officersAndDirectorsCovOrExcCARecord)
    }

     var volCompAndEmprsLiaCovEndCARecordRecords = pload.VolCompAndEmprsLiaCovEndCARecord.Entry
     record.VolCompAndEmprsLiaCovEndCARecord = new ArrayList<TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields>()
     for(entry in volCompAndEmprsLiaCovEndCARecordRecords){
       var volCompAndEmprsLiaCovEndCARecord = new TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields()
       volCompAndEmprsLiaCovEndCARecord.NameOfEmplAndNameOrGrpOrDescOfOperations = new ArrayList<String>()
       for(i in entry.NameOfEmplAndNameOrGrpOrDescOfOperations.Entry ){
         var endNo = i
         volCompAndEmprsLiaCovEndCARecord.NameOfEmplAndNameOrGrpOrDescOfOperations.add(endNo)
       }
       var endFields = volCompAndEmprsLiaCovEndCARecord.objHashMap().keySet()
       endFields.remove("NameOfEmplAndNameOrGrpOrDescOfOperations")
       loadFromModel(volCompAndEmprsLiaCovEndCARecord,entry,endFields)
       record.VolCompAndEmprsLiaCovEndCARecord.add(volCompAndEmprsLiaCovEndCARecord)
     }

    var waiOfOurRightToRecFromOthsEndCARecords = pload.WaiOfOurRightToRecFromOthsEndCARecord.Entry
    record.WaiOfOurRightToRecFromOthsEndCARecord = new ArrayList<TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields>()
    for(entry in waiOfOurRightToRecFromOthsEndCARecords){
      var waiOfOurRightToRecFromOthsEndCARecord = new TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields()
      waiOfOurRightToRecFromOthsEndCARecord.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR = new ArrayList<String>()
      for(i in entry.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR.Entry ){
        var endNo = i
        waiOfOurRightToRecFromOthsEndCARecord.NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR.add(endNo)
      }
      var endFields = waiOfOurRightToRecFromOthsEndCARecord.objHashMap().keySet()
      endFields.remove("NameOfPerOrOrgOfJobDescForWhomCarrWaivesROR")
      loadFromModel(waiOfOurRightToRecFromOthsEndCARecord,entry,endFields)
      record.WaiOfOurRightToRecFromOthsEndCARecord.add(waiOfOurRightToRecFromOthsEndCARecord)
    }

    var caApprovedForm10Records = pload.CAApprovedForm10CARecord.Entry
    record.CAApprovedForm10CARecord = new ArrayList<TDIC_WCpolsCAApprovedForm10Fields>()
    for(entry in caApprovedForm10Records){
      var caApprovedForm10Record = new TDIC_WCpolsCAApprovedForm10Fields()
      caApprovedForm10Record.NameOfEmployee = new ArrayList<String>()
      for(i in entry.NameOfEmployee.Entry ){
        var endNo = i
        caApprovedForm10Record.NameOfEmployee.add(endNo)
      }
      var endFields = caApprovedForm10Record.objHashMap().keySet()
      endFields.remove("NameOfEmployee")
      loadFromModel(caApprovedForm10Record,entry,endFields)
      record.CAApprovedForm10CARecord.add(caApprovedForm10Record)
    }

    var caApprovedForm11Records = pload.CAApprovedForm11CARecord.Entry
    record.CAApprovedForm11CARecord = new ArrayList<TDIC_WCpolsCAApprovedForm11Fields>()
    for(entry in caApprovedForm11Records){
      var caApprovedForm11Record = new TDIC_WCpolsCAApprovedForm11Fields()
      caApprovedForm11Record.ExcludedOperationDescription = new ArrayList<String>()
      for(i in entry.ExcludedOperationDescription.Entry ){
        var endNo = i
        caApprovedForm11Record.ExcludedOperationDescription.add(endNo)
      }
      var endFields = caApprovedForm11Record.objHashMap().keySet()
      endFields.remove("ExcludedOperationDescription")
      loadFromModel(caApprovedForm11Record,entry,endFields)
      record.CAApprovedForm11CARecord.add(caApprovedForm11Record)
    }

    if(pload.CancellationRecord != null){
      var cancellationRecord = new TDIC_WCpolsCancellationFields()
      loadFromModel(cancellationRecord,pload.CancellationRecord,cancellationRecord.objHashMap().keySet())
      record.CancellationRecord = cancellationRecord
    }

    if(pload.ReinstatementRecord != null){
      var reinstatementRecord = new TDIC_WCpolsReinstatementFields()
      loadFromModel(reinstatementRecord,pload.ReinstatementRecord,reinstatementRecord.objHashMap().keySet())
      record.ReinstatementRecord = reinstatementRecord
    }
    record.AddressRecord = new ArrayList<TDIC_WCpolsAddressFields>()
    for(entry in pload.AddressRecord.Entry){
      var adressRecord = new TDIC_WCpolsAddressFields()
      loadFromModel(adressRecord,entry,adressRecord.objHashMap().keySet())
      record.AddressRecord.add(adressRecord)

    }
    var endIdRecords = pload.EndorsementIdentificationRecord.Entry
    record.EndorsementIdentificationRecord = new ArrayList<TDIC_WCpolsEndorsementIdentificationFields>()
    for(entry in endIdRecords){
      var endorseRecord = new TDIC_WCpolsEndorsementIdentificationFields()
      endorseRecord.EndorsementNumbers = new ArrayList<TDIC_EndorsementNumberFields>()
      for(i in entry.EndorsementNumbers.Entry ){
        var endNo = new TDIC_EndorsementNumberFields()
        loadFromModel(endNo,i,endNo.objHashMap().keySet())
        endorseRecord.EndorsementNumbers.add(endNo)
      }
      var endFields = endorseRecord.objHashMap().keySet()
      endFields.remove("EndorsementNumbers")
      loadFromModel(endorseRecord,entry,endFields)
      record.EndorsementIdentificationRecord.add(endorseRecord)
    }

    record.ExpRatingModChangeEndRecord = new ArrayList<TDIC_WCpolsExpRatingModChangeEndRecFields>()
    for(entry in pload.ExpRatingModChangeEndRecord.Entry){
      var expModChgRec = new TDIC_WCpolsExpRatingModChangeEndRecFields()
      loadFromModel(expModChgRec,entry,expModChgRec.objHashMap().keySet())
      record.ExpRatingModChangeEndRecord.add(expModChgRec)
    }

    record.StatePremiumChangeRecord = new ArrayList<TDIC_WCpolsStatePremiumChangeRecFields>()
    for(entry in pload.StatePremiumChangeRecord.Entry){
      var statePremChgRec = new TDIC_WCpolsStatePremiumChangeRecFields()
      loadFromModel(statePremChgRec,entry,statePremChgRec.objHashMap().keySet())
      record.StatePremiumChangeRecord.add(statePremChgRec)
    }

    record.ClassAndOrRateChgAndOthEndRec = new ArrayList<TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields>()
    for(entry in pload.ClassAndOrRateChgAndOthEndRec.Entry){
      var classCodeOrRateChangeRecord = new TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields()
      loadFromModel(classCodeOrRateChangeRecord,entry,classCodeOrRateChangeRecord.objHashMap().keySet())
      record.ClassAndOrRateChgAndOthEndRec.add(classCodeOrRateChangeRecord)
    }

    record.DataElementsChangeEndRecord = new ArrayList<TDIC_WCpolsDataElementsChgEndRecFields>()
    for(entry in pload.DataElementsChangeEndRecord.Entry){
      var dataElemChgEndRec = new TDIC_WCpolsDataElementsChgEndRecFields()
      loadFromModel(dataElemChgEndRec,entry,dataElemChgEndRec.objHashMap().keySet())
      record.DataElementsChangeEndRecord.add(dataElemChgEndRec)
    }

    record.NameChangeEndRecord = new ArrayList<TDIC_WCpolsNameChangeEndRecFields>()
    for(entry in pload.NameChangeEndRecord.Entry){
      var nameChangeRecord = new TDIC_WCpolsNameChangeEndRecFields()
      loadFromModel(nameChangeRecord,entry,nameChangeRecord.objHashMap().keySet())
      record.NameChangeEndRecord.add(nameChangeRecord)
    }

    record.AddressChangeEndRecord = new ArrayList<TDIC_WCpolsAddressChangeEndRecFields>()
    for(entry in pload.AddressChangeEndRecord.Entry){
      var adressChangeRecord = new TDIC_WCpolsAddressChangeEndRecFields()
      loadFromModel(adressChangeRecord,entry,adressChangeRecord.objHashMap().keySet())
      record.AddressChangeEndRecord.add(adressChangeRecord)
    }

    return record

  }

  protected function loadFromModel(entity:Object, model:Object, fields:Set<String>) {
    for(entry in fields) {
      if (_logger.DebugEnabled) {
        _logger.debug("loading ${entry} from model")
      }
      // _logger.info("before value for "+entry.Key+" was [" + model[entry.Key] + "]")
      entity[entry] = model[entry]
      // _logger.info("value for "+entry.Key+" was [" + model[entry.Key] + "]")

      if (_logger.DebugEnabled) {
        _logger.debug("value for "+entry+" was [" + model[entry] + "]")
      }
    }
  }

  function writeFlatFile(completeRecord: TDIC_WCPolsCompleteRecord){

    var sdf: DateFormat = new SimpleDateFormat("yyyyMMdd")
    var effDate = sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")

    //GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
    var headerPath = ConfigAccess.getConfigFile(PropertyUtil.getInstance().getProperty("excelHeaderPath")).Path
    _logger.info("headerPath -> " + headerPath)
    if(headerPath == null){
      throw new GWConfigurationException("TDIC_WCPolicySpecificationReport#writeFlatFile() - Cannot retrieve header path with the key 'excelHeaderPath' from integration database.")
    }
    var excelPath = ConfigAccess.getConfigFile(PropertyUtil.getInstance().getProperty("srExcelPath")).Path
    _logger.info("excelPath -> " + excelPath)
    if(excelPath == null){
      throw new GWConfigurationException("TDIC_WCPolicySpecificationReport#writeFlatFile() - Cannot retrieve excel path with the key 'srExcelPath' from integration database.")
    }
    var destDir = PropertyUtil.getInstance().getProperty("destDirPath")
    _logger.info("destDir -> " + destDir)
    if(destDir == null){
      throw new GWConfigurationException("TDIC_WCPolicySpecificationReport#writeFlatFile() - Cannot retrieve destination directory path with the key 'destDirPath' from integration database.")
    }

    if(!new File(destDir).exists()) {
      _logger.error("TDIC_WCPolicySpecificationReport#writeFlatFile() - Cannot Access destDir. The Directory May Not Exist or There is an Error in network Connection")
      if(TDIC_WCpolsUtil.EMAIL_RECIPIENT == null) {
        _logger.error("TDIC_WCPolicySpecificationReport#writeFlatFile() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCpolsUtil.EMAIL_RECIPIENT, "WCPOLS Report batch failed on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to access path:" + destDir + ": The Directory May Not Exist or There is an Error in network Connection.")
      }
    }

    var flatFileName = "WCPOLSReport"+effDate+".txt"
    var destFile = destDir+flatFileName

    /*
    * Encode XML to FlatFile
    */
    var flatFileUtility = new TDIC_FlatFileUtilityHelper()
    var vendorStructure = flatFileUtility.encodeandReturnEncodedString(excelPath,headerPath,destFile)
    var fileContents = completeRecord.convertToFlatFileRecord(vendorStructure?.toTypedArray())
    var fileGenerateStatus = flatFileUtility.writeEncodedFlatfile(fileContents,destFile)
    if(!fileGenerateStatus){         //Error in flat file generation
      _logger.debug("File not generated")
    }
  }
}