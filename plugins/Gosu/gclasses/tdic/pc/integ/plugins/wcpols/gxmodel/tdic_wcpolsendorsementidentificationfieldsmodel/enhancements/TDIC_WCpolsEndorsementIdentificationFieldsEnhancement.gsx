package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsEndorsementIdentificationFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.TDIC_WCpolsEndorsementIdentificationFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsEndorsementIdentificationFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.TDIC_WCpolsEndorsementIdentificationFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.TDIC_WCpolsEndorsementIdentificationFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsEndorsementIdentificationFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.TDIC_WCpolsEndorsementIdentificationFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsendorsementidentificationfieldsmodel.TDIC_WCpolsEndorsementIdentificationFields(object, options)
  }

}