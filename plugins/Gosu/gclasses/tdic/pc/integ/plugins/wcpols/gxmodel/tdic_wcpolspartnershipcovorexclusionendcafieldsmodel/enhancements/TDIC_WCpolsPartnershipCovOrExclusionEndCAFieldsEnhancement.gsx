package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsPartnershipCovOrExclusionEndCAFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspartnershipcovorexclusionendcafieldsmodel.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields(object, options)
  }

}