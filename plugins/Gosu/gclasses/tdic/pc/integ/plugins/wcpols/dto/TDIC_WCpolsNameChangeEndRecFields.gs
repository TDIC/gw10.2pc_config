package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 9/15/16
 * Time: 1:33 PM
 * This class includes variables for all fields in name change record for policy specification report.
 */
class TDIC_WCpolsNameChangeEndRecFields extends  TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _futureReserved1 : String as FutureReserved1
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved2 : String as FutureReserved2
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _nameTypeCode : int as NameTypeCode
  var _nameLinkIdentifier : int as NameLinkIdentifier
  var _nameOfInsured : String as NameOfInsured
  var _fedEmpIdNum : String as FedEmpIdNum
  var _conSeqNum : int as ConSeqNum
  var _legNatOfEntityCode : int as LegNatOfEntityCode
  var _textForOtherLegNatOfEntity : String as TextForOtherLegNatOfEntity
  var _stateCodes : String as StateCodes
  var _stateUnemplmntNums : String as StateUnemplNums
  var _stateCodes1 : String as StateCodes1
  var _stateUnemplmntNums1 : String as StateUnemplNums1
  var _stateCodes2 : String as StateCodes2
  var _stateUnemplmntNums2 : String as StateUnemplNums2
  var _futureReserved3 : String as FutureReserved3
  var _nameRevisionCode : String as NameRevisionCode
  var _futureReserved4 : String as FutureReserved4
  var _profEmprOrgnOrClientCompCode : String as ProfEmprOrgnOrClientCompCode
  var _nameOfInsured1 : String as NameOfInsured1
  var _endorsementEffectiveDate : String as EndorsementEffectiveDate
  var _nameLinkCounterIdntfr : String as NameLinkCounterIdntfr
  var _futureReserved5 : String as FutureReserved5

}