package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_EndorsementNumberFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.TDIC_EndorsementNumberFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_EndorsementNumberFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.TDIC_EndorsementNumberFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.TDIC_EndorsementNumberFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_EndorsementNumberFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.TDIC_EndorsementNumberFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_endorsementnumberfieldsmodel.TDIC_EndorsementNumberFields(object, options)
  }

}