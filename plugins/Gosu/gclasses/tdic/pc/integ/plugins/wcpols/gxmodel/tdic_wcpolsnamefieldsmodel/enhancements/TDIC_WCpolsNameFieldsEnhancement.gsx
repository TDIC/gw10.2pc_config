package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsNameFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.TDIC_WCpolsNameFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.TDIC_WCpolsNameFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.TDIC_WCpolsNameFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.TDIC_WCpolsNameFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamefieldsmodel.TDIC_WCpolsNameFields(object, options)
  }

}