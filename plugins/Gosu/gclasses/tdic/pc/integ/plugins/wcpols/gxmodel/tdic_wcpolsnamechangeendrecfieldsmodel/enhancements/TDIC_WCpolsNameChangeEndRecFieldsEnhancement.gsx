package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsNameChangeEndRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.TDIC_WCpolsNameChangeEndRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameChangeEndRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.TDIC_WCpolsNameChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.TDIC_WCpolsNameChangeEndRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameChangeEndRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.TDIC_WCpolsNameChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsnamechangeendrecfieldsmodel.TDIC_WCpolsNameChangeEndRecFields(object, options)
  }

}