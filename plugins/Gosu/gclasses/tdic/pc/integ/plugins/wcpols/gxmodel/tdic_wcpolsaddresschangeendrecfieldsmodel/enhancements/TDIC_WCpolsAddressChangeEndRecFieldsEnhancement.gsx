package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsAddressChangeEndRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.TDIC_WCpolsAddressChangeEndRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressChangeEndRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.TDIC_WCpolsAddressChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.TDIC_WCpolsAddressChangeEndRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressChangeEndRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.TDIC_WCpolsAddressChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsaddresschangeendrecfieldsmodel.TDIC_WCpolsAddressChangeEndRecFields(object, options)
  }

}