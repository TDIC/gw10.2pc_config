package tdic.pc.integ.plugins.wcpols.dto

uses tdic.pc.integ.plugins.wcpols.TDIC_WCpolsFlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 9/14/16
 * Time: 12:53 PM
 * This class includes variables for all fields in state premium change record for policy specification report.
 */

 class TDIC_WCpolsStatePremiumChangeRecFields extends TDIC_WCpolsFlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _dataElementChangeIdNum : String as DataElementChangeIdNum
  var _futureReserved2 : String as FutureReserved2
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _expModPlanTypeCode : String as ExpModPlanTypeCode
  var _otherIndividualRiskRatingFactor : String as OtherIndividualRiskRatingFactor
  var _insurerPremiumDeviationFactor : int as InsurerPremiumDeviationFactor
  var _typeOfPremDevCode : int as TypeOfPremDevCode
  var _estimatedStateStanPremTot : long as EstimatedStateStanPremTot
  var _expenseConstantAmnt : int as ExpenseConstantAmnt
  var _lossConstantAmnt : int as LossConstantAmnt
  var _premDiscountAmnt : int as PremDiscountAmnt
  var _proratedExpenseConstAmntReasonCode : String as ProratedExpenseConstAmntReasonCode
  var _proratedMinPremAmntReasonCode : String as ProratedMinPremAmntReasonCode
  var _reasonStateWasAddedToPolCode : String as ReasonStateWasAddedToPolicyCode
  var _previousReportedExpModEffDate : String as PreviousReportedExpModEffDate
  var _previousReportedAnnivRatingDate : String as PreviousReportedAnnivRatingDate
  var _assignedRiskAdjProgFact : String as AssignedRiskAdjProgFact
  var _typeofNonStanIdCode : String as TypeOfNonStanIdCode
  var _independentDCORiskIdNum : String as IndependentDCORiskIdNum
  var _futureReserved3 : String as FutureReserved3
  var _nameOfInsured : String as NameOfInsured
  var _endorsementEffectivedate : String as EndorsementEffectiveDate
  var _futureReserved4 : String as FutureReserved4

 }