package tdic.pc.integ.plugins.wcpols

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.LoggerFactory
uses tdic.pc.integ.plugins.message.TDIC_MessagePlugin

uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/1/14
 * Time: 7:22 PM
 * Implementation of a Messaging Transport plugin to save the payload into database which will be retrieved later and written to flat file.
 */
class TDIC_WCpolsTransport extends TDIC_MessagePlugin {
  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")
  /*
   * send method writes payload into database.
   * Payload contains information about each policy transaction.
   */
  override function send(p0: entity.Message, p1: String) {
    _logger.debug("TDIC_WCpolsTransport#send() - Entering")
    var _con: Connection = null
    var _prepStatement : PreparedStatement = null
    try{
      //GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
      var _envIntDbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      _con = DatabaseManager.getConnection(_envIntDbUrl)
      var _prepSqlStmt = "INSERT INTO WCPOLS(CreatedDate,Processed,Payload) VALUES(?,?,?)"
      _prepStatement = _con.prepareStatement(_prepSqlStmt)
      _prepStatement.setDate(1,gw.api.util.DateUtil.currentDate().toSQLDate())
      _prepStatement.setInt(2,0)
      _prepStatement.setString(3,p0.Payload)
      var _return =_prepStatement.executeUpdate()
      _con.commit()
      p0.reportAck()
      _logger.debug("TDIC_WCpolsTransport#send() - Exiting")

    } catch (sql:SQLException) {
      reportError(p0)
      _logger.error("TDIC_WCpolsTransport#send() - SQLException= " + sql)
    } catch (e:Exception) {
      reportError(p0)
      _logger.error("TDIC_WCpolsTransport#send() - Exception= " + e)
    } finally {
      try {
        if (_con != null) _con.close()
      } catch (fe:Exception) {
        _logger.error("TDIC_WCpolsTransport#send() - finally Exception= " + fe)
      }
    }
  }

 /*
   * This method changes the wait time while retry
   * For each retry time varies.
   */
  function reportError(p0:entity.Message  )  {

    if(p0.RetryCount < 3){
      var backOffMultiplier =  p0.RetryCount + 1
      var waitTime = backOffMultiplier * 60
      var retryTime = gw.api.util.DateUtil.addSeconds(gw.api.util.DateUtil.currentDate(), waitTime)
      p0.reportError(retryTime)
    }
    else {
      p0.reportError()
    }
  }

  override function shutdown() {
  }

  override function resume() {
  }
}