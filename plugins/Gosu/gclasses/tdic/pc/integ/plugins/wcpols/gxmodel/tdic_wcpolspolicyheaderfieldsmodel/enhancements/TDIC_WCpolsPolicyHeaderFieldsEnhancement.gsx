package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsPolicyHeaderFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.TDIC_WCpolsPolicyHeaderFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPolicyHeaderFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.TDIC_WCpolsPolicyHeaderFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.TDIC_WCpolsPolicyHeaderFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPolicyHeaderFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.TDIC_WCpolsPolicyHeaderFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolspolicyheaderfieldsmodel.TDIC_WCpolsPolicyHeaderFields(object, options)
  }

}