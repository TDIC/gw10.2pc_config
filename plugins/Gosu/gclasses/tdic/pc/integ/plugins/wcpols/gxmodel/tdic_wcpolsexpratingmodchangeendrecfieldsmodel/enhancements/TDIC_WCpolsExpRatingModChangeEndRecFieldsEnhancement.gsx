package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsExpRatingModChangeEndRecFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.TDIC_WCpolsExpRatingModChangeEndRecFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExpRatingModChangeEndRecFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.TDIC_WCpolsExpRatingModChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.TDIC_WCpolsExpRatingModChangeEndRecFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExpRatingModChangeEndRecFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.TDIC_WCpolsExpRatingModChangeEndRecFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolsexpratingmodchangeendrecfieldsmodel.TDIC_WCpolsExpRatingModChangeEndRecFields(object, options)
  }

}