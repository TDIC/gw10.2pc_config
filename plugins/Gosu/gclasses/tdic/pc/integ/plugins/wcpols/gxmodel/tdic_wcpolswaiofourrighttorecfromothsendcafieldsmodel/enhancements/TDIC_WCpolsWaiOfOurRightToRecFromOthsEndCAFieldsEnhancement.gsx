package tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFieldsEnhancement : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields {
  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields(object)
  }

  public static function create(object : tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields {
    return new tdic.pc.integ.plugins.wcpols.gxmodel.tdic_wcpolswaiofourrighttorecfromothsendcafieldsmodel.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields(object, options)
  }

}