package tdic.pc.integ.plugins.membership

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.TypecodeMapperUtil
uses gw.pl.exception.GWConfigurationException
uses org.slf4j.LoggerFactory
uses org.xml.sax.InputSource
uses tdic.pc.common.services.aptify.AptifyMembershipServiceFactory
uses tdic.pc.integ.services.membership.aptifymembershipservice.AptifyMembershipService
uses javax.xml.parsers.DocumentBuilderFactory
uses java.io.StringReader
uses java.sql.Connection
uses java.sql.SQLException

/**
 * User: skiriaki
 * Date: 12/02/14
 *
 * Used to check membership status from CDA's membership database (Aptify) based on ADA Numbers
 */
class TDIC_MembershipCheck {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  private var MAPPER = TypecodeMapperUtil.getTypecodeMapper()
  private static final var GLOBAL_STATUS_TYPE = "GlobalStatus_TDIC"
  private static final var NAME_SPACE_APTIFY = "tdic:aptify"
  private static final var USER_NAME = "aptify.username"
  private static final var PASS_WORD = "aptify.password"
  private static final var MULTIPLE_REC_FOUND = "Multiple records found"
  private static final var NAME_SPACE_CDA = "tdic:cda"
  private static final var FILE_NAME = "State"
  private var APTIFY_USERNAME : String = null
  private var APTIFY_PASSWORD : String = null
  public static final var USERNAME_ERROR: String = "Cannot retrieve aptify username from properties file on server " + gw.api.system.server.ServerUtil.ServerId
  public static final var PASSWORD_ERROR: String = "Cannot retrieve aptify password from properties file on server " + gw.api.system.server.ServerUtil.ServerId


  /**
   * Key for looking up the DATAMART db url from properties file.
   */
  public static final var DATAMART_URL : String = "DataMartDBURL"
  /**
   * SQL query to pull out data from Legacy System(AS400) database.
   */
  public static final var MEMBERSHIP_SQL_QRY : String = "select COUNT(*) from AS400.MBDENT where FILE_NAME=? and SADANR=?"

  public function checkMembership(adaNumber: String, _state : State): typekey.GlobalStatus_TDIC {
    _logger.debug("MembershipCheck#checkMembership() - Entering")
    var _ret = typekey.GlobalStatus_TDIC.TC_INACTIVE
    if(_state?.equals(State.TC_CA)) {
      try {
        var membershipService = AptifyMembershipServiceFactory.Instance//new AptifyMembershipService()
        APTIFY_USERNAME = PropertyUtil.getInstance().getProperty(USER_NAME)
        if (APTIFY_USERNAME == null) {
          throw new GWConfigurationException(USERNAME_ERROR)
        }
        APTIFY_PASSWORD = PropertyUtil.getInstance().getProperty(PASS_WORD)
        if (APTIFY_PASSWORD == null) {
          throw new GWConfigurationException(PASSWORD_ERROR)
        }
        var gwCustResponse = membershipService.chkGWCust(APTIFY_USERNAME, APTIFY_PASSWORD, adaNumber)
        // parsing gwCustResponse XML  using DOM parser
        var factory = DocumentBuilderFactory.newInstance();
        var builder = factory.newDocumentBuilder();
        var document = builder.parse(new InputSource(new StringReader(gwCustResponse)));
        var membershipNodes = document.getElementsByTagName("MemberType");
        var membershipNode = membershipNodes.item(0);
        var memberType = membershipNode.getTextContent()
        _logger.info("MembershipCheck#checkMembership() - MemberType received for ADANumber: ${adaNumber} is ${memberType} - Exiting")
        if (memberType.HasContent and !memberType.equalsIgnoreCase(MULTIPLE_REC_FOUND)) {
          var status = MAPPER.getInternalCodeByAlias(GLOBAL_STATUS_TYPE, NAME_SPACE_APTIFY, memberType)

          if (status.HasContent)
            _ret = typekey.GlobalStatus_TDIC.get(status)
        }
        _logger.debug("MembershipCheck#checkMembership() - Exiting")
        return _ret
      } catch (e : Exception) {
        _logger.error("MembershipCheck#checkMembership() - Exception= " + e)
        throw (e)
      }
    }else{
      var _con : Connection = null
      try {
        var _fileName : String
        if (_state != null){
          _fileName = MAPPER.getAliasByInternalCode(FILE_NAME, NAME_SPACE_CDA, _state.Code)
        }
        if(_fileName != null){
          //  retrieve the Datamart DB URL from tdicintegrations.properties file
          var _datamartDBURL =  PropertyUtil.getInstance().getProperty(DATAMART_URL)
          // Get a connection from DatabaseManager using the Datamart DB URL
          _con = DatabaseManager.getConnection(_datamartDBURL)
          var _preStatement = _con.prepareStatement(MEMBERSHIP_SQL_QRY)
          _preStatement.setString(1, _fileName)
          _preStatement.setString(2, adaNumber)
          var _resultCount : int  = 0
          var _result = _preStatement.executeQuery()
          while (_result.next()) {
            _resultCount = _result.getInt(1)
          }
          if(_resultCount > 0){
            _ret  = GlobalStatus_TDIC.TC_ACTIVE
          }
        }
      } catch (sql: SQLException) {
        _logger.error("MembershipCheck#checkMembership() - SQLException : " + sql)
        throw(sql)
      } catch (ex : Exception) {
        _logger.error("MembershipCheck#checkMembership() - Exception  " + ex.StackTraceAsString)
        throw(ex)
      } finally {
        if(_con != null){
          try {
            _con.close()
          } catch (sql:SQLException) {
            _logger.error("MembershipCheck#checkMembership() - SQLException = " + sql)
            throw(sql)
          }
        }
      }
    }
    return _ret
  }

}