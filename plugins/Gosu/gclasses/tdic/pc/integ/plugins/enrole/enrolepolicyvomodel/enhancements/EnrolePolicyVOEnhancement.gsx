package tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement EnrolePolicyVOEnhancement : tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO {
  public static function create(object : tdic.pc.common.batch.enrole.vo.EnrolePolicyVO) : tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO {
    return new tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO(object)
  }

  public static function create(object : tdic.pc.common.batch.enrole.vo.EnrolePolicyVO, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO {
    return new tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO(object, options)
  }

}