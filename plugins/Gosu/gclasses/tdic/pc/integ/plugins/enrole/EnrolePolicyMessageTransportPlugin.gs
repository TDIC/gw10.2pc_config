package tdic.pc.integ.plugins.enrole

uses org.apache.commons.beanutils.BeanUtils
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.enrole.EnroleHelper
uses tdic.pc.common.batch.enrole.vo.EnrolePolicyVO
uses tdic.pc.common.batch.enrole.exception.EnroleException
uses tdic.pc.integ.plugins.message.TDIC_MessagePlugin

/**
 * The Transport plugin is responsible to read message from the EnrolePolicy Destination Queue
 * and store the policy info into table EnrolePolicyDetails of GWINT DB.
 * Created by RambabuN on 10/15/2019.
 */
class EnrolePolicyMessageTransportPlugin extends TDIC_MessagePlugin {

  static final var _logger = LoggerFactory.getLogger("TDIC_Enrole")

  public static final var DESTINATION_ID : int = 63

  override function send(message : Message, transformedPayload : String) {
    _logger.debug("Enter EnrolePolicyMessageTransportPlugin.send() ")

    try {

      //Parse the paylaod into model VO.
      var _voModel = tdic.pc.integ.plugins.enrole.enrolepolicyvomodel.EnrolePolicyVO.parse(transformedPayload)
      var enrolePolVO = new EnrolePolicyVO()

      //Copy all the GX VO model properties to EnrolePolicyVO
      BeanUtils.copyProperties(enrolePolVO, _voModel)

      EnroleHelper.insertEnrolePolicy(enrolePolVO)

      _logger.debug(enrolePolVO.PolicyNBR + " Policy record inserted successfully into table EnrolePolicyDetails.")

      //Acknowledge message
      message.reportAck()
    } catch (ex: EnroleException) {
      //Set Error message to message ErrorDescription for easy tracking.
      message.ErrorDescription = ex.Message
      _logger.error("Exception while inserting enrole Policy Feed: " + ex.Message, ex)
      //reportError() for the non recoverable errors
      message.reportError()
    }
    _logger.debug("Exit EnrolePolicyMessageTransportPlugin.send")

  }

  override function shutdown() {

  }

  override function resume() {

  }
}