package tdic.pc.integ.plugins.policyperiod

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.IQuery
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses org.slf4j.LoggerFactory
uses tdic.pc.integ.plugins.policyperiod.dto.TDIC_PolicyPeriodMultilineDTO

uses java.sql.PreparedStatement
uses java.sql.ResultSet
uses java.sql.SQLException
uses gw.api.database.Query
uses gw.api.database.Relop

/**
 * US560, US890
 * 03/11/2015 Rob Kelly
 * <p>
 * A business specification that determines whether a PolicyPeriod is eligible for the multiline discount or not.
 */
class TDIC_PolicyPeriodIsMultiline {

  /**
   * The logger for this business specification.
   */
  private static final var LOGGER = LoggerFactory.getLogger("TDIC_INTEGRATION")

  /**
   * The logger tag for this Credit Card Handler.
   */
  private static final var LOG_TAG = "TDIC_PolicyPeriodIsMultiline#"

  /**
   * The Data Mart connector for this business specification.
   */
  private var connector : DataMartConnector

  /**
   * Email property key.
   */
//GINTEG-219: Reading properties from PropertyUtil rather from CacheManager

  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail")
  private static final var WC_QUERY = "SELECT POLNBR FROM AS400.DLFPHMMS WHERE CORPFG<>'1' and STOVRD='CA' and PTYPE in(1,3,7) " +
      "and PSTAT in(3,25) and PROPAK in('Y','P','A') and CDANBR = ?"

  private static final var PL_QUERY = "SELECT POLNBR FROM AS400.DLFPHMMS WHERE CORPFG<>'1' and PTYPE in(1,3,7) and PSTAT in(3,25) " +
      "and CDANBR = ?"
  private static final var CP_QUERY = "SELECT POLNBR FROM AS400.DLFPHMMS WHERE PTYPE in(2,3,4,5,8,9) and PSTAT in(3,99,25) " +
      " and CDANBR = ?"


  protected var _policyPeriod : PolicyPeriod
  /**
   * The ADA Numbers used to determine eligibility for the multiline discount.
   */
  protected var _adaNumbers : List<String>

  construct(policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod
    this.connector = new DataMartConnector()
  }

  /**
   * Returns true if the specified PolicyPeriod is eligible for the multiline discount; false otherwise.
   */
  public function isSatisfied() : boolean {
    // adanumber
    var logTag = LOG_TAG + "isSatisfied() - "
    var dataMartPolicyNumbers : List<String>
    LOGGER.debug(logTag + "Checking multiline discount satisfied for PolicyPeriod " + _policyPeriod)
    if (ADANumbers != null and ADANumbers.size() > 0 and _policyPeriod.WC7LineExists) {
      var PolicyPeriodMultilineList = fetchMultiLineDiscountByADA(ADANumbers)
      if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "PL")
          and PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "CP")) {
        return true
      }
    }
    try {
      LOGGER.debug(logTag + "Getting Policy Numbers for ADA Numbers " + ADANumbers)
      dataMartPolicyNumbers = this.connector.getPolicyNumbers(ADANumbers, "WC")
    } catch (e : Exception) {
      throw new DisplayableException(e.getMessage())
    }
    if (this.connector.ConnectionFailedReason != null) {
      var msg = DisplayKey.get("TDIC.Multiline.DataMart.ConnectionFailed", this.connector.ConnectionFailedReason)
      LOGGER.warn(logTag + msg)
      throw new DisplayableException(msg)
    }
    LOGGER.debug(logTag + "Dat   aMart returned policy numbers " + dataMartPolicyNumbers + " for ADA Numbers " + ADANumbers)
    return (dataMartPolicyNumbers.Count > 0)
  }


  /**
   * Get the ADA Numbers for this multiline discount business specification.
   */
  public property get ADANumbers() : List<String> {
    if (_adaNumbers == null) {
      _adaNumbers = new ArrayList<String>()

      var primaryNamedInsuredADANumber = _policyPeriod.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
      if (primaryNamedInsuredADANumber != null) {
        _adaNumbers.add(primaryNamedInsuredADANumber)
      }
      var ownerOfficerADANumber : String
      if (_policyPeriod.WC7LineExists) {
        for (anOwnerOfficer in _policyPeriod.WC7Line.WC7PolicyOwnerOfficers) {
          if (shouldIncludeADANumber(anOwnerOfficer)) {
            ownerOfficerADANumber = anOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
            if (ownerOfficerADANumber != null) {
              _adaNumbers.add(ownerOfficerADANumber)
            }
          }
        }
      }
      if (_policyPeriod.BOPLineExists) {
        for (anOwnerOfficer in _policyPeriod.BOPLine.BOPPolicyOwnerOfficer_TDIC) {
          ownerOfficerADANumber = anOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
          if (ownerOfficerADANumber != null) {
            _adaNumbers.add(ownerOfficerADANumber)
          }
        }
      }
    }
    return _adaNumbers
  }

  /**
   * Returns true if the ADA Number of the specified OwnerOfficer should be included in the ADA Numbers for this multiline discount business specification; false otherwise.
   */
  private function shouldIncludeADANumber(anOwnerOfficer : WC7PolicyOwnerOfficer) : boolean {
    return (anOwnerOfficer.IntrinsicType == PolicyEntityOwner_TDIC
        or (anOwnerOfficer.WC7OwnershipPct != null and anOwnerOfficer.WC7OwnershipPct.intValue() > 0))
  }

  /**
   * A connector Data Mart.
   */
  private class DataMartConnector {

    private var connectionFailedReason : String as ConnectionFailedReason

    construct() {

    }


    /**
     * Gets the policy number on the IBM iSeries V5R4M000 database table TDICDTA.GWMLELIG; null if no such policy number is present in the table.
     */
    public function getPolicyNumbers(adaNumbersToSearch : List<String>, lob : String) : List<String> {
      //Sprint - 4 , GINTEG-237 : Refactored below method to read the ADA data from the new integration table 'AS400PolicyDetails'
      var _con = DatabaseManager.getConnection(PropertyUtil.getInstance().getProperty("DataMartDBURL"))
      var resultSet : ResultSet
      var statement : PreparedStatement
      var policyNumbers = new ArrayList<String>()
      try {
        if (adaNumbersToSearch != null and adaNumbersToSearch.size() > 0) {
          //GINTEG_237 : Reading ADA from the new integration table, corrected the select query
          var sqlBuilder = new StringBuilder(WC_QUERY)
          if (lob == "PL") {
            sqlBuilder = new StringBuilder(PL_QUERY)
          }
          if (lob == "CP") {
            sqlBuilder = new StringBuilder(CP_QUERY)
          }
          var i = 1
          while (i < adaNumbersToSearch.size()) {
            sqlBuilder.append(" or CDANBR = ?")
            i++
          }
          statement = _con.prepareStatement(sqlBuilder.toString())
          for (anADANumber in adaNumbersToSearch index j) {
            statement.setString(j + 1, anADANumber)
          }
          resultSet = statement.executeQuery()
          while (resultSet.next()) {
            policyNumbers.add(resultSet.getString("POLNBR"))
          }
        }
        return policyNumbers
      } catch (e : Exception) {
        if (gw.api.system.server.ServerUtil.getEnv().equalsIgnoreCase("PROD")) {
          EmailUtil.sendEmail(EMAIL_RECIPIENT, "Error retrieving policy number from Data Mart on server: " + gw.api.system.server.ServerUtil.ServerId, "ERROR retrieving policy number from Data Mart: " + e.LocalizedMessage)
          throw new DisplayableException("ERROR retrieving policy number from Data Mart: " + e.getMessage())
        } else {
          policyNumbers = new ArrayList<String>()
          return policyNumbers
        }
      } finally {
        try {
          if (resultSet != null) {
            resultSet.close()
          }
          if (statement != null) {
            statement.close()
          }
          if (_con != null) {
            _con.close()
          }
        } catch (e : SQLException) {
          throw e
        }
      }
    }
  }



  public function fetchMultiLineDiscountByADA(adaNumber : List<String>) : List<TDIC_PolicyPeriodMultilineDTO> {
    var policyPeriodMultiLineList = new ArrayList<TDIC_PolicyPeriodMultilineDTO>()
    //GWPS-1915 - Checking whether the ADA numbers count is zero or not if Yes, then return.

    if(adaNumber.Count == 0 ){
      return policyPeriodMultiLineList
    }
try {
    var resultSet = retrivePPBYADANumber(adaNumber).select()
      for (pp in resultSet) {
        var policyPeriodMultiLineDTO = new TDIC_PolicyPeriodMultilineDTO()
        pp = pp.getSlice(pp.EditEffectiveDate)
        policyPeriodMultiLineDTO.ADA = pp.PrimaryNamedInsured.AccountContactRole.AccountContact?.Contact?.ADANumber_TDICOfficialID
        policyPeriodMultiLineDTO.PolNumbr = pp.PolicyNumber
        if (pp.BOPLineExists) {
          policyPeriodMultiLineDTO.Lob = "CP"
        } else if (pp.GLLineExists) {
          policyPeriodMultiLineDTO.Lob = "PL"
        } else if (pp.WC7LineExists) {
          policyPeriodMultiLineDTO.Lob = "WC"
        }
        policyPeriodMultiLineDTO.BaseState = pp.BaseState.Code
        policyPeriodMultiLineList.add(policyPeriodMultiLineDTO)
      }
      return policyPeriodMultiLineList
    } catch (e : Exception){
      if (gw.api.system.server.ServerUtil.getEnv().equalsIgnoreCase("PROD")) {
        EmailUtil.sendEmail(EMAIL_RECIPIENT, "Error retrieving policy number from Integration DB server: " + gw.api.system.server.ServerUtil.ServerId, "ERROR retrieving policy number from Data Mart: " + e.LocalizedMessage)
        throw new DisplayableException("ERROR retrieving policy number from Integration DB: " + e.getMessage())
      } else {
        return policyPeriodMultiLineList
      }
    }
  }


  public function isSatisfiedForPL() : MultiLineDiscount_TDIC {
    var multiLineDiscount = new TDIC_PolicyPeriodIsMultiline(_policyPeriod)
    var datamartPolicy = this.connector.getPolicyNumbers(multiLineDiscount.ADANumbers, "PL").Count
    if (_policyPeriod.BaseState == Jurisdiction.TC_CA and _policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var PolicyPeriodMultilineList = multiLineDiscount.fetchMultiLineDiscountByADA(multiLineDiscount.ADANumbers)
      if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "WC")
          and PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "CP")) {
        return MultiLineDiscount_TDIC.TC_A
      } else if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "WC") and datamartPolicy > 0) {
        return MultiLineDiscount_TDIC.TC_A
      } else if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "WC")) {
        return MultiLineDiscount_TDIC.TC_W
      } else if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "CP")) {
        return MultiLineDiscount_TDIC.TC_P
      } else if (datamartPolicy > 0) {
        return MultiLineDiscount_TDIC.TC_P
      }

    } else if (_policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var PolicyPeriodMultilineList = multiLineDiscount.fetchMultiLineDiscountByADA(multiLineDiscount.ADANumbers)
      if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "CP")) {
        return MultiLineDiscount_TDIC.TC_P
      }
    }
    if (_policyPeriod.BaseState == Jurisdiction.TC_PA and _policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      var PolicyPeriodMultilineList = multiLineDiscount.fetchMultiLineDiscountByADA(multiLineDiscount.ADANumbers)
      if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "CP")) {
        return MultiLineDiscount_TDIC.TC_P
      }
    }
    return MultiLineDiscount_TDIC.TC_N
  }

  public function isSatisfiedForCP() : MultiLineDiscount_TDIC {
    var multiLineDiscount = new TDIC_PolicyPeriodIsMultiline(_policyPeriod)
    var PolicyPeriodMultilineList = multiLineDiscount.fetchMultiLineDiscountByADA(multiLineDiscount.ADANumbers)
    var datamartPolicy = this.connector.getPolicyNumbers(multiLineDiscount.ADANumbers, "CP").Count
    if (PolicyPeriodMultilineList.hasMatch(\elt -> elt.Lob == "PL") or datamartPolicy > 0) {
      return MultiLineDiscount_TDIC.TC_P
    }
    return MultiLineDiscount_TDIC.TC_N
  }

  private function retrivePPBYADANumber(adaNumber : List<String>) : Query<PolicyPeriod> {

    var retrivePolicyPeriod = Query.make(PolicyPeriod)
    retrivePolicyPeriod.join(PolicyContactRole, "BranchValue").compare(PolicyContactRole#Subtype, Relop.Equals, TC_POLICYPRINAMEDINSURED)
        .join("AccountContactRole")
        .join("AccountContact")
        .join("Contact")
        .join(OfficialID, "Contact").compare("OfficialIDType", Equals, OfficialIDType.TC_ADANUMBER_TDIC)
        .compareIn("OfficialIDValue", adaNumber?.toTypedArray())
    retrivePolicyPeriod.compare(PolicyPeriod#CancellationDate, Relop.Equals, null)
    retrivePolicyPeriod.compare(PolicyPeriod#MostRecentModel, Equals, true)
    retrivePolicyPeriod.join("PolicyTerm").compare(PolicyTerm#MostRecentTerm, Equals, true)
    // query.join(entity.PolicyLine, "BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE)

    return retrivePolicyPeriod
  }

}