package tdic.pc.integ.plugins.policyperiod.dto

class TDIC_PolicyPeriodMultilineDTO {

  var _ada : String as ADA
  var _polnumbr : String as PolNumbr
  var _lastname : String as LastName
  var _firstname : String as FirstName
  var _lob : String as Lob
  var _baseState : String as BaseState

}