package tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClauseEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.Clause {
  public static function create(object : gw.api.domain.Clause) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.Clause {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.Clause(object)
  }

  public static function create(object : gw.api.domain.Clause, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.Clause {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcclausemodel.Clause(object, options)
  }

}