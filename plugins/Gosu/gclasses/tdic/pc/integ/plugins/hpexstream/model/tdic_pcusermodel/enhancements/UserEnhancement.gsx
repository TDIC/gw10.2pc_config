package tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement UserEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.User {
  public static function create(object : entity.User) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.User {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.User(object)
  }

  public static function create(object : entity.User, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.User {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcusermodel.User(object, options)
  }

}