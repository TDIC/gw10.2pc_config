package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLLineEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.GLLine {
  public static function create(object : productmodel.GLLine) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.GLLine {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.GLLine(object)
  }

  public static function create(object : productmodel.GLLine, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.GLLine {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpllinemodel.GLLine(object, options)
  }

}