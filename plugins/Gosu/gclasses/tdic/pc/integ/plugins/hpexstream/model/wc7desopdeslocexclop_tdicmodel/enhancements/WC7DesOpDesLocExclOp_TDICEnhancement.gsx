package tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7DesOpDesLocExclOp_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.WC7DesOpDesLocExclOp_TDIC {
  public static function create(object : entity.WC7DesOpDesLocExclOp_TDIC) : tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.WC7DesOpDesLocExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.WC7DesOpDesLocExclOp_TDIC(object)
  }

  public static function create(object : entity.WC7DesOpDesLocExclOp_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.WC7DesOpDesLocExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7desopdeslocexclop_tdicmodel.WC7DesOpDesLocExclOp_TDIC(object, options)
  }

}