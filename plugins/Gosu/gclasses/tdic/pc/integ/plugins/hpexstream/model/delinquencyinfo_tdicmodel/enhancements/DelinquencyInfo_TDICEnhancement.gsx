package tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement DelinquencyInfo_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.DelinquencyInfo_TDIC {
  public static function create(object : entity.DelinquencyInfo_TDIC) : tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.DelinquencyInfo_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.DelinquencyInfo_TDIC(object)
  }

  public static function create(object : entity.DelinquencyInfo_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.DelinquencyInfo_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.delinquencyinfo_tdicmodel.DelinquencyInfo_TDIC(object, options)
  }

}