package tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement SchedulePropertyInfoEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.SchedulePropertyInfo {
  public static function create(object : gw.api.productmodel.SchedulePropertyInfo) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.SchedulePropertyInfo {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.SchedulePropertyInfo(object)
  }

  public static function create(object : gw.api.productmodel.SchedulePropertyInfo, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.SchedulePropertyInfo {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcschedulepropertyinfomodel.SchedulePropertyInfo(object, options)
  }

}