package tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.Account {
  public static function create(object : entity.Account) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.Account {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.Account {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountmodel.Account(object, options)
  }

}