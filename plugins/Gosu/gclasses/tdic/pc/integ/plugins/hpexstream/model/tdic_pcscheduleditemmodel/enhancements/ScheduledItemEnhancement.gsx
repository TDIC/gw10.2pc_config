package tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ScheduledItemEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.ScheduledItem {
  public static function create(object : entity.ScheduledItem) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.ScheduledItem {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.ScheduledItem(object)
  }

  public static function create(object : entity.ScheduledItem, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.ScheduledItem {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcscheduleditemmodel.ScheduledItem(object, options)
  }

}