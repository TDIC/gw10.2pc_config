package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7CostEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.WC7Cost {
  public static function create(object : entity.WC7Cost) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.WC7Cost {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.WC7Cost(object)
  }

  public static function create(object : entity.WC7Cost, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.WC7Cost {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7costmodel.WC7Cost(object, options)
  }

}