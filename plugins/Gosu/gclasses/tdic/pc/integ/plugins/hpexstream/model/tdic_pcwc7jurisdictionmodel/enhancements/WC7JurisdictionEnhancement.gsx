package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7JurisdictionEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.WC7Jurisdiction {
  public static function create(object : entity.WC7Jurisdiction) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.WC7Jurisdiction {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.WC7Jurisdiction(object)
  }

  public static function create(object : entity.WC7Jurisdiction, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.WC7Jurisdiction {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7jurisdictionmodel.WC7Jurisdiction(object, options)
  }

}