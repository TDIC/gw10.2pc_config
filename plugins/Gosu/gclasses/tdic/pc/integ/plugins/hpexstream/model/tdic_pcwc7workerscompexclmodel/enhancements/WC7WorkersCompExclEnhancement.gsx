package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7WorkersCompExclEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.WC7WorkersCompExcl {
  public static function create(object : entity.WC7WorkersCompExcl) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.WC7WorkersCompExcl {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.WC7WorkersCompExcl(object)
  }

  public static function create(object : entity.WC7WorkersCompExcl, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.WC7WorkersCompExcl {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompexclmodel.WC7WorkersCompExcl(object, options)
  }

}