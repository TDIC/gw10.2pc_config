package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyUserRoleAssignmentEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.PolicyUserRoleAssignment {
  public static function create(object : entity.PolicyUserRoleAssignment) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.PolicyUserRoleAssignment {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.PolicyUserRoleAssignment(object)
  }

  public static function create(object : entity.PolicyUserRoleAssignment, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.PolicyUserRoleAssignment {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyuserroleassignmentmodel.PolicyUserRoleAssignment(object, options)
  }

}