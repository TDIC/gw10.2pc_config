package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyNamedInsuredEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.PolicyNamedInsured {
  public static function create(object : entity.PolicyNamedInsured) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.PolicyNamedInsured {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.PolicyNamedInsured(object)
  }

  public static function create(object : entity.PolicyNamedInsured, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.PolicyNamedInsured {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicynamedinsuredmodel.PolicyNamedInsured(object, options)
  }

}