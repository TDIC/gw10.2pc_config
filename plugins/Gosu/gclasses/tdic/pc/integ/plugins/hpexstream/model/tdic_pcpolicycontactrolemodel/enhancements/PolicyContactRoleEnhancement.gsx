package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyContactRoleEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.PolicyContactRole {
  public static function create(object : entity.PolicyContactRole) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.PolicyContactRole {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.PolicyContactRole(object)
  }

  public static function create(object : entity.PolicyContactRole, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.PolicyContactRole {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicycontactrolemodel.PolicyContactRole(object, options)
  }

}