package tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AuditInformationEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.AuditInformation {
  public static function create(object : entity.AuditInformation) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.AuditInformation {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.AuditInformation(object)
  }

  public static function create(object : entity.AuditInformation, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.AuditInformation {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditinformationmodel.AuditInformation(object, options)
  }

}