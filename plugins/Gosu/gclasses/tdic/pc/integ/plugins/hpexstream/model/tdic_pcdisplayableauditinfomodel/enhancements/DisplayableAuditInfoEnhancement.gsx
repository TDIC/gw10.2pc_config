package tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement DisplayableAuditInfoEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.DisplayableAuditInfo {
  public static function create(object : gw.job.audit.DisplayableAuditInfo) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.DisplayableAuditInfo {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.DisplayableAuditInfo(object)
  }

  public static function create(object : gw.job.audit.DisplayableAuditInfo, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.DisplayableAuditInfo {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcdisplayableauditinfomodel.DisplayableAuditInfo(object, options)
  }

}