package tdic.pc.integ.plugins.hpexstream.model.schedulemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ScheduleEnhancement : tdic.pc.integ.plugins.hpexstream.model.schedulemodel.Schedule {
  public static function create(object : gw.api.domain.Schedule) : tdic.pc.integ.plugins.hpexstream.model.schedulemodel.Schedule {
    return new tdic.pc.integ.plugins.hpexstream.model.schedulemodel.Schedule(object)
  }

  public static function create(object : gw.api.domain.Schedule, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.schedulemodel.Schedule {
    return new tdic.pc.integ.plugins.hpexstream.model.schedulemodel.Schedule(object, options)
  }

}