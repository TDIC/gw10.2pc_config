package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyConditionEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.PolicyCondition {
  public static function create(object : entity.PolicyCondition) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.PolicyCondition {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.PolicyCondition(object)
  }

  public static function create(object : entity.PolicyCondition, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.PolicyCondition {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyconditionmodel.PolicyCondition(object, options)
  }

}