package tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AuditEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.Audit {
  public static function create(object : entity.Audit) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.Audit {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.Audit(object)
  }

  public static function create(object : entity.Audit, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.Audit {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcauditmodel.Audit(object, options)
  }

}