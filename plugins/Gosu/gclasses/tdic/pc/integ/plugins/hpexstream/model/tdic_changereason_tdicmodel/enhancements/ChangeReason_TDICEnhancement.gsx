package tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ChangeReason_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.ChangeReason_TDIC {
  public static function create(object : entity.ChangeReason_TDIC) : tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.ChangeReason_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.ChangeReason_TDIC(object)
  }

  public static function create(object : entity.ChangeReason_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.ChangeReason_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_changereason_tdicmodel.ChangeReason_TDIC(object, options)
  }

}