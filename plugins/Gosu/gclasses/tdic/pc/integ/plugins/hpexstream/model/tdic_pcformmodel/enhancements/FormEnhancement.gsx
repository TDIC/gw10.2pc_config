package tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement FormEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.Form {
  public static function create(object : entity.Form) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.Form {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.Form(object)
  }

  public static function create(object : entity.Form, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.Form {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcformmodel.Form(object, options)
  }

}