package tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.PolicyPeriod {
  public static function create(object : entity.PolicyPeriod) : tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.PolicyPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.PolicyPeriod(object)
  }

  public static function create(object : entity.PolicyPeriod, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.PolicyPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_basedonpolicyperiodmodel.PolicyPeriod(object, options)
  }

}