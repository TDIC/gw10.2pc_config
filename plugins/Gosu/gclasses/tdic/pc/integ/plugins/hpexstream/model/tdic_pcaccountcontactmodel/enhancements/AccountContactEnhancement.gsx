package tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountContactEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.AccountContact {
  public static function create(object : entity.AccountContact) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.AccountContact {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.AccountContact(object)
  }

  public static function create(object : entity.AccountContact, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.AccountContact {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaccountcontactmodel.AccountContact(object, options)
  }

}