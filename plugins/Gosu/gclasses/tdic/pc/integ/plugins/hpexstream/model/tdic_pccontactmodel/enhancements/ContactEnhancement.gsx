package tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ContactEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.Contact {
  public static function create(object : entity.Contact) : tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.Contact {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.Contact(object)
  }

  public static function create(object : entity.Contact, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.Contact {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pccontactmodel.Contact(object, options)
  }

}