package tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement UWCompanyEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.UWCompany {
  public static function create(object : entity.UWCompany) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.UWCompany {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.UWCompany(object)
  }

  public static function create(object : entity.UWCompany, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.UWCompany {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcuwcompanymodel.UWCompany(object, options)
  }

}