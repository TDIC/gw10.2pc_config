package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7PolicyOwnerOfficerEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.WC7PolicyOwnerOfficer {
  public static function create(object : entity.WC7PolicyOwnerOfficer) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.WC7PolicyOwnerOfficer {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.WC7PolicyOwnerOfficer(object)
  }

  public static function create(object : entity.WC7PolicyOwnerOfficer, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.WC7PolicyOwnerOfficer {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7policyownerofficermodel.WC7PolicyOwnerOfficer(object, options)
  }

}