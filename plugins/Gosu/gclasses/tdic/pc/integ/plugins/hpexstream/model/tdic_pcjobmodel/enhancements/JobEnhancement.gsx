package tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement JobEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.Job {
  public static function create(object : entity.Job) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.Job {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.Job(object)
  }

  public static function create(object : entity.Job, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.Job {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcjobmodel.Job(object, options)
  }

}