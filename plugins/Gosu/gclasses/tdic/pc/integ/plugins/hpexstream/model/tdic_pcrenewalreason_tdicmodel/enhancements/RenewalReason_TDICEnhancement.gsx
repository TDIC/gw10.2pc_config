package tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RenewalReason_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.RenewalReason_TDIC {
  public static function create(object : entity.RenewalReason_TDIC) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.RenewalReason_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.RenewalReason_TDIC(object)
  }

  public static function create(object : entity.RenewalReason_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.RenewalReason_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcrenewalreason_tdicmodel.RenewalReason_TDIC(object, options)
  }

}