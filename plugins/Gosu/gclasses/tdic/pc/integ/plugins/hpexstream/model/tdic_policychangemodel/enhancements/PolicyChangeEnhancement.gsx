package tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyChangeEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.PolicyChange {
  public static function create(object : entity.PolicyChange) : tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.PolicyChange {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.PolicyChange(object)
  }

  public static function create(object : entity.PolicyChange, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.PolicyChange {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_policychangemodel.PolicyChange(object, options)
  }

}