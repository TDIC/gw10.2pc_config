package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.Policy {
  public static function create(object : entity.Policy) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.Policy {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.Policy(object)
  }

  public static function create(object : entity.Policy, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.Policy {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicymodel.Policy(object, options)
  }

}