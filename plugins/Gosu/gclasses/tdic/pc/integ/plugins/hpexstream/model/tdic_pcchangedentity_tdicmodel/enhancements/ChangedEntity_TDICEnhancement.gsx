package tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ChangedEntity_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.ChangedEntity_TDIC {
  public static function create(object : entity.ChangedEntity_TDIC) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.ChangedEntity_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.ChangedEntity_TDIC(object)
  }

  public static function create(object : entity.ChangedEntity_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.ChangedEntity_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcchangedentity_tdicmodel.ChangedEntity_TDIC(object, options)
  }

}