package tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPLineEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.BOPLine {
  public static function create(object : productmodel.BOPLine) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.BOPLine {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.BOPLine(object)
  }

  public static function create(object : productmodel.BOPLine, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.BOPLine {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcboplinemodel.BOPLine(object, options)
  }

}