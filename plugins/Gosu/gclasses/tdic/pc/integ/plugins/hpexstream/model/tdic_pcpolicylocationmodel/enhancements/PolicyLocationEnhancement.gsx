package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyLocationEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.PolicyLocation {
  public static function create(object : entity.PolicyLocation) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.PolicyLocation {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.PolicyLocation(object)
  }

  public static function create(object : entity.PolicyLocation, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.PolicyLocation {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicylocationmodel.PolicyLocation(object, options)
  }

}