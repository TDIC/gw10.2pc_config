package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7ClassCodeEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.WC7ClassCode {
  public static function create(object : entity.WC7ClassCode) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.WC7ClassCode {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.WC7ClassCode(object)
  }

  public static function create(object : entity.WC7ClassCode, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.WC7ClassCode {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7classcodemodel.WC7ClassCode(object, options)
  }

}