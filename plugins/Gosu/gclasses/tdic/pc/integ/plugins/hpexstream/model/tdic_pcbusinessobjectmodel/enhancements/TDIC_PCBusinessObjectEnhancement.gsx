package tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_PCBusinessObjectEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.TDIC_PCBusinessObject {
  public static function create(object : tdic.pc.integ.plugins.hpexstream.bo.TDIC_PCBusinessObject) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.TDIC_PCBusinessObject {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.TDIC_PCBusinessObject(object)
  }

  public static function create(object : tdic.pc.integ.plugins.hpexstream.bo.TDIC_PCBusinessObject, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.TDIC_PCBusinessObject {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcbusinessobjectmodel.TDIC_PCBusinessObject(object, options)
  }

}