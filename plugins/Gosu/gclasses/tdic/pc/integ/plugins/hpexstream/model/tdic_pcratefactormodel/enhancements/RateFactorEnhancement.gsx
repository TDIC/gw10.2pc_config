package tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RateFactorEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.RateFactor {
  public static function create(object : entity.RateFactor) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.RateFactor {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.RateFactor(object)
  }

  public static function create(object : entity.RateFactor, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.RateFactor {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcratefactormodel.RateFactor(object, options)
  }

}