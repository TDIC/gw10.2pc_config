package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7CoveredEmployeeEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.WC7CoveredEmployee {
  public static function create(object : entity.WC7CoveredEmployee) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.WC7CoveredEmployee {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.WC7CoveredEmployee(object)
  }

  public static function create(object : entity.WC7CoveredEmployee, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.WC7CoveredEmployee {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7coveredemployeemodel.WC7CoveredEmployee(object, options)
  }

}