package tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AddressEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.Address {
  public static function create(object : entity.Address) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.Address {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.Address(object)
  }

  public static function create(object : entity.Address, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.Address {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcaddressmodel.Address(object, options)
  }

}