package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7RatingPeriodEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.WC7RatingPeriod {
  public static function create(object : gw.lob.wc7.rating.WC7RatingPeriod) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.WC7RatingPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.WC7RatingPeriod(object)
  }

  public static function create(object : gw.lob.wc7.rating.WC7RatingPeriod, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.WC7RatingPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7ratingperiodmodel.WC7RatingPeriod(object, options)
  }

}