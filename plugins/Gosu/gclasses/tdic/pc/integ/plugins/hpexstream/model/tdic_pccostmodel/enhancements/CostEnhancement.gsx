package tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CostEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.Cost {
  public static function create(object : entity.Cost) : tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.Cost {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.Cost(object)
  }

  public static function create(object : entity.Cost, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.Cost {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pccostmodel.Cost(object, options)
  }

}