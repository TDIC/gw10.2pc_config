package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7WaiverOfSubroEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.WC7WaiverOfSubro {
  public static function create(object : entity.WC7WaiverOfSubro) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.WC7WaiverOfSubro {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.WC7WaiverOfSubro(object)
  }

  public static function create(object : entity.WC7WaiverOfSubro, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.WC7WaiverOfSubro {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7waiverofsubromodel.WC7WaiverOfSubro(object, options)
  }

}