package tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7DesOpExclOp_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.WC7DesOpExclOp_TDIC {
  public static function create(object : entity.WC7DesOpExclOp_TDIC) : tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.WC7DesOpExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.WC7DesOpExclOp_TDIC(object)
  }

  public static function create(object : entity.WC7DesOpExclOp_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.WC7DesOpExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7desopexclop_tdicmodel.WC7DesOpExclOp_TDIC(object, options)
  }

}