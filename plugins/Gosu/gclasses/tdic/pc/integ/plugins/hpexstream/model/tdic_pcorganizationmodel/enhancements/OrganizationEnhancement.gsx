package tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement OrganizationEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.Organization {
  public static function create(object : entity.Organization) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.Organization {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.Organization(object)
  }

  public static function create(object : entity.Organization, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.Organization {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcorganizationmodel.Organization(object, options)
  }

}