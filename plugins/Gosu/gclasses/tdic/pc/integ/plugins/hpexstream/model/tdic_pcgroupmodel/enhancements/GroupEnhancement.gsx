package tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GroupEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.Group {
  public static function create(object : entity.Group) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.Group {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.Group(object)
  }

  public static function create(object : entity.Group, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.Group {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcgroupmodel.Group(object, options)
  }

}