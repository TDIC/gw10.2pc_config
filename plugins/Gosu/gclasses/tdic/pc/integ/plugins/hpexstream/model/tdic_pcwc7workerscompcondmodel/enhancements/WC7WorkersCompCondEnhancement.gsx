package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7WorkersCompCondEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.WC7WorkersCompCond {
  public static function create(object : entity.WC7WorkersCompCond) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.WC7WorkersCompCond {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.WC7WorkersCompCond(object)
  }

  public static function create(object : entity.WC7WorkersCompCond, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.WC7WorkersCompCond {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7workerscompcondmodel.WC7WorkersCompCond(object, options)
  }

}