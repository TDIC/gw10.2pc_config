package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7ModifierEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.WC7Modifier {
  public static function create(object : entity.WC7Modifier) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.WC7Modifier {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.WC7Modifier(object)
  }

  public static function create(object : entity.WC7Modifier, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.WC7Modifier {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7modifiermodel.WC7Modifier(object, options)
  }

}