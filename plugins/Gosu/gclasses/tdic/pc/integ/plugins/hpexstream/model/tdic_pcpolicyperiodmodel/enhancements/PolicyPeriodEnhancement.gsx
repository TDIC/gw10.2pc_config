package tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.PolicyPeriod {
  public static function create(object : entity.PolicyPeriod) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.PolicyPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.PolicyPeriod(object)
  }

  public static function create(object : entity.PolicyPeriod, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.PolicyPeriod {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcpolicyperiodmodel.PolicyPeriod(object, options)
  }

}