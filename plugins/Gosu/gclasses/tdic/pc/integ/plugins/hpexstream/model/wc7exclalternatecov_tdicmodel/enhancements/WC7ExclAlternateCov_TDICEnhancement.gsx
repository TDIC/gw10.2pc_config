package tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7ExclAlternateCov_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.WC7ExclAlternateCov_TDIC {
  public static function create(object : entity.WC7ExclAlternateCov_TDIC) : tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.WC7ExclAlternateCov_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.WC7ExclAlternateCov_TDIC(object)
  }

  public static function create(object : entity.WC7ExclAlternateCov_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.WC7ExclAlternateCov_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7exclalternatecov_tdicmodel.WC7ExclAlternateCov_TDIC(object, options)
  }

}