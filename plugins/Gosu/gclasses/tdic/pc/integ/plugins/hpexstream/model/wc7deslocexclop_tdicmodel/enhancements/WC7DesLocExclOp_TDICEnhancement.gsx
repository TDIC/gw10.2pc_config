package tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7DesLocExclOp_TDICEnhancement : tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.WC7DesLocExclOp_TDIC {
  public static function create(object : entity.WC7DesLocExclOp_TDIC) : tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.WC7DesLocExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.WC7DesLocExclOp_TDIC(object)
  }

  public static function create(object : entity.WC7DesLocExclOp_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.WC7DesLocExclOp_TDIC {
    return new tdic.pc.integ.plugins.hpexstream.model.wc7deslocexclop_tdicmodel.WC7DesLocExclOp_TDIC(object, options)
  }

}