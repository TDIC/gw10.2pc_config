package tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement WC7LineEnhancement : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.WC7Line {
  public static function create(object : productmodel.WC7Line) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.WC7Line {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.WC7Line(object)
  }

  public static function create(object : productmodel.WC7Line, options : gw.api.gx.GXOptions) : tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.WC7Line {
    return new tdic.pc.integ.plugins.hpexstream.model.tdic_pcwc7linemodel.WC7Line(object, options)
  }

}