package tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyLossPayee_TDICEnhancement : tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.PolicyLossPayee_TDIC {
  public static function create(object : entity.PolicyLossPayee_TDIC) : tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.PolicyLossPayee_TDIC {
    return new tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.PolicyLossPayee_TDIC(object)
  }

  public static function create(object : entity.PolicyLossPayee_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.PolicyLossPayee_TDIC {
    return new tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.PolicyLossPayee_TDIC(object, options)
  }

}