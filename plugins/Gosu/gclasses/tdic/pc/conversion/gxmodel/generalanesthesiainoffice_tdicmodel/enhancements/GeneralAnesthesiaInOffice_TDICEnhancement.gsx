package tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GeneralAnesthesiaInOffice_TDICEnhancement : tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.GeneralAnesthesiaInOffice_TDIC {
  public static function create(object : entity.GeneralAnesthesiaInOffice_TDIC) : tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.GeneralAnesthesiaInOffice_TDIC {
    return new tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.GeneralAnesthesiaInOffice_TDIC(object)
  }

  public static function create(object : entity.GeneralAnesthesiaInOffice_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.GeneralAnesthesiaInOffice_TDIC {
    return new tdic.pc.conversion.gxmodel.generalanesthesiainoffice_tdicmodel.GeneralAnesthesiaInOffice_TDIC(object, options)
  }

}