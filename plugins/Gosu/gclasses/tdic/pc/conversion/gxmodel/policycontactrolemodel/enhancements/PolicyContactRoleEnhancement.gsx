package tdic.pc.conversion.gxmodel.policycontactrolemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyContactRoleEnhancement : tdic.pc.conversion.gxmodel.policycontactrolemodel.PolicyContactRole {
  public static function create(object : entity.PolicyContactRole) : tdic.pc.conversion.gxmodel.policycontactrolemodel.PolicyContactRole {
    return new tdic.pc.conversion.gxmodel.policycontactrolemodel.PolicyContactRole(object)
  }

  public static function create(object : entity.PolicyContactRole, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policycontactrolemodel.PolicyContactRole {
    return new tdic.pc.conversion.gxmodel.policycontactrolemodel.PolicyContactRole(object, options)
  }

}