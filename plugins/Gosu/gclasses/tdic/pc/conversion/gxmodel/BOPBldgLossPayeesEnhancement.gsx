package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator


enhancement BOPBldgLossPayeesEnhancement : tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.types.complex.PolicyLossPayee_TDIC {

  function populateBOPLossPayee(bldg : BOPBuilding, lossPayee : PolicyLossPayee_TDIC) : PolicyLossPayee_TDIC {
    SimpleValuePopulator.populate(this, lossPayee)
    this.AccountContactRole.AccountContact.Contact.$TypeInstance.populateContact(lossPayee.AccountContactRole.AccountContact.Contact)
    return lossPayee
  }


}
