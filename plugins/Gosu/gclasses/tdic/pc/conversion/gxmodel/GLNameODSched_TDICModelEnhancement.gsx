package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.types.complex.GLNameODSched_TDIC

enhancement GLNameODSched_TDICModelEnhancement : GLNameODSched_TDIC {

  function populateGLNameODSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddNameODSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
    if(this.LTEffectiveDate != null) {
      sched.LTEffectiveDate = this.LTEffectiveDate
      sched.LTExpirationDate = this.LTEffectiveDate.addYears(1)
    }
    /*var policyAddlInsDetail =
        glLine.addNewAdditionalInsuredDetailOfContactType(ConversionUtility.
            getContactType(this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Subtype))
    this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.
        Contact.$TypeInstance.populateContact(policyAddlInsDetail.PolicyAddlInsured.AccountContactRole.AccountContact.Contact)
    this.AdditionalInsured.AdditionalInsuredType = this.AdditionalInsured.AdditionalInsuredType*/
    //sched.AdditionalInsured = policyAddlInsDetail
  }
}
