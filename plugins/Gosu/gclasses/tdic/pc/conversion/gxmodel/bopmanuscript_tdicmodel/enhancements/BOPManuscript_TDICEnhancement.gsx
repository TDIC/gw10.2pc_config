package tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPManuscript_TDICEnhancement : tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.BOPManuscript_TDIC {
  public static function create(object : entity.BOPManuscript_TDIC) : tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.BOPManuscript_TDIC {
    return new tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.BOPManuscript_TDIC(object)
  }

  public static function create(object : entity.BOPManuscript_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.BOPManuscript_TDIC {
    return new tdic.pc.conversion.gxmodel.bopmanuscript_tdicmodel.BOPManuscript_TDIC(object, options)
  }

}