package tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLStateExclSched_TDICEnhancement : tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.GLStateExclSched_TDIC {
  public static function create(object : entity.GLStateExclSched_TDIC) : tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.GLStateExclSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.GLStateExclSched_TDIC(object)
  }

  public static function create(object : entity.GLStateExclSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.GLStateExclSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.GLStateExclSched_TDIC(object, options)
  }

}