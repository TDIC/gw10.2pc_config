package tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RiskManagementDisc_TDICEnhancement : tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.RiskManagementDisc_TDIC {
  public static function create(object : entity.RiskManagementDisc_TDIC) : tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.RiskManagementDisc_TDIC {
    return new tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.RiskManagementDisc_TDIC(object)
  }

  public static function create(object : entity.RiskManagementDisc_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.RiskManagementDisc_TDIC {
    return new tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.RiskManagementDisc_TDIC(object, options)
  }

}