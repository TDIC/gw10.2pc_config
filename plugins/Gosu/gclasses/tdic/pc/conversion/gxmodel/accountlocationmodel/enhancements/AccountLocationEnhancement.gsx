package tdic.pc.conversion.gxmodel.accountlocationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountLocationEnhancement : tdic.pc.conversion.gxmodel.accountlocationmodel.AccountLocation {
  public static function create(object : entity.AccountLocation) : tdic.pc.conversion.gxmodel.accountlocationmodel.AccountLocation {
    return new tdic.pc.conversion.gxmodel.accountlocationmodel.AccountLocation(object)
  }

  public static function create(object : entity.AccountLocation, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.accountlocationmodel.AccountLocation {
    return new tdic.pc.conversion.gxmodel.accountlocationmodel.AccountLocation(object, options)
  }

}