package tdic.pc.conversion.gxmodel

uses entity.Building
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator


enhancement BuildingModelEnhancement : tdic.pc.conversion.gxmodel.buildingmodel.types.complex.Building {

  function populateBuilding(line : BOPLine, bopLocation : BOPLocation) : BOPBuilding {
    var bopBuilding = new BOPBuilding(bopLocation.Branch)
    var bldg = new Building(bopBuilding.Branch)
    var buildingSideTypes = BuildingSideType.getTypeKeys( false )
    for (var sideType in buildingSideTypes.iterator()) {
      var buildingSide = new BuildingSide(line.Branch)
      buildingSide.BuildingSideType = sideType
      bldg.addToBuildingSides(buildingSide)
    }
    var buildingImprTypes = BuildingImprType.getTypeKeys( false )
    for (var imprType in  buildingImprTypes.iterator()) {
      var buildingImprovement = new BuildingImprovement(line.Branch)
      buildingImprovement.BuildingImprType = imprType
      bldg.addToBuildingImprovements(buildingImprovement)
    }
    bopBuilding.Building = bldg
    this.YearBuilt = this.YearBuilt == null or this.YearBuilt < 1000 ? null : this.YearBuilt
    SimpleValuePopulator.populate(this, bldg)
    //bopBuilding.ConstructionType = bopBldgModel.ConstructionType
    bldg.PolicyLocation = bopLocation.PolicyLocation
    bldg.BuildingNum = bopLocation.PolicyLocation.LocationNum
    bopBuilding.BOPLocation = bopLocation
    bopLocation.addToBuildings(bopBuilding)
    return bopBuilding
  }

}
