package tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLExcludedServiceSched_TDICEnhancement : tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.GLExcludedServiceSched_TDIC {
  public static function create(object : entity.GLExcludedServiceSched_TDIC) : tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.GLExcludedServiceSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.GLExcludedServiceSched_TDIC(object)
  }

  public static function create(object : entity.GLExcludedServiceSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.GLExcludedServiceSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.GLExcludedServiceSched_TDIC(object, options)
  }

}