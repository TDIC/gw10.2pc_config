package tdic.pc.conversion.gxmodel.ratefactormodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RateFactorEnhancement : tdic.pc.conversion.gxmodel.ratefactormodel.RateFactor {
  public static function create(object : entity.RateFactor) : tdic.pc.conversion.gxmodel.ratefactormodel.RateFactor {
    return new tdic.pc.conversion.gxmodel.ratefactormodel.RateFactor(object)
  }

  public static function create(object : entity.RateFactor, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.ratefactormodel.RateFactor {
    return new tdic.pc.conversion.gxmodel.ratefactormodel.RateFactor(object, options)
  }

}