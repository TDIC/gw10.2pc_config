package tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountUserRoleAssignmentEnhancement : tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.AccountUserRoleAssignment {
  public static function create(object : entity.AccountUserRoleAssignment) : tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.AccountUserRoleAssignment {
    return new tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.AccountUserRoleAssignment(object)
  }

  public static function create(object : entity.AccountUserRoleAssignment, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.AccountUserRoleAssignment {
    return new tdic.pc.conversion.gxmodel.accountuserroleassignmentmodel.AccountUserRoleAssignment(object, options)
  }

}