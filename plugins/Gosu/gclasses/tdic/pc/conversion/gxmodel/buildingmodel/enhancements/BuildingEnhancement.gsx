package tdic.pc.conversion.gxmodel.buildingmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BuildingEnhancement : tdic.pc.conversion.gxmodel.buildingmodel.Building {
  public static function create(object : entity.Building) : tdic.pc.conversion.gxmodel.buildingmodel.Building {
    return new tdic.pc.conversion.gxmodel.buildingmodel.Building(object)
  }

  public static function create(object : entity.Building, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.buildingmodel.Building {
    return new tdic.pc.conversion.gxmodel.buildingmodel.Building(object, options)
  }

}