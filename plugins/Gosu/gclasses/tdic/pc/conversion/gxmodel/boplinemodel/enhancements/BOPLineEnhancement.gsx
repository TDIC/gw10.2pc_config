package tdic.pc.conversion.gxmodel.boplinemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPLineEnhancement : tdic.pc.conversion.gxmodel.boplinemodel.BOPLine {
  public static function create(object : productmodel.BOPLine) : tdic.pc.conversion.gxmodel.boplinemodel.BOPLine {
    return new tdic.pc.conversion.gxmodel.boplinemodel.BOPLine(object)
  }

  public static function create(object : productmodel.BOPLine, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.boplinemodel.BOPLine {
    return new tdic.pc.conversion.gxmodel.boplinemodel.BOPLine(object, options)
  }

}