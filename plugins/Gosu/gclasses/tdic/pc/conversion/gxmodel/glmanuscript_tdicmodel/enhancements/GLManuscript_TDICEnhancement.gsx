package tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLManuscript_TDICEnhancement : tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.GLManuscript_TDIC {
  public static function create(object : entity.GLManuscript_TDIC) : tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.GLManuscript_TDIC {
    return new tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.GLManuscript_TDIC(object)
  }

  public static function create(object : entity.GLManuscript_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.GLManuscript_TDIC {
    return new tdic.pc.conversion.gxmodel.glmanuscript_tdicmodel.GLManuscript_TDIC(object, options)
  }

}