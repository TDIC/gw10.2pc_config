package tdic.pc.conversion.gxmodel.policyperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodEnhancement : tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod {
  public static function create(object : entity.PolicyPeriod) : tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod {
    return new tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod(object)
  }

  public static function create(object : entity.PolicyPeriod, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod {
    return new tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod(object, options)
  }

}