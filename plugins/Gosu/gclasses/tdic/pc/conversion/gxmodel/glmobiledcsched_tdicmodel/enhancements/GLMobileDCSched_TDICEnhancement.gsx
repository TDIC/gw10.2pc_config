package tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLMobileDCSched_TDICEnhancement : tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.GLMobileDCSched_TDIC {
  public static function create(object : entity.GLMobileDCSched_TDIC) : tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.GLMobileDCSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.GLMobileDCSched_TDIC(object)
  }

  public static function create(object : entity.GLMobileDCSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.GLMobileDCSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.GLMobileDCSched_TDIC(object, options)
  }

}