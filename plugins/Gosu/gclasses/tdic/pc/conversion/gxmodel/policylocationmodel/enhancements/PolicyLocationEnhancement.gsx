package tdic.pc.conversion.gxmodel.policylocationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyLocationEnhancement : tdic.pc.conversion.gxmodel.policylocationmodel.PolicyLocation {
  public static function create(object : entity.PolicyLocation) : tdic.pc.conversion.gxmodel.policylocationmodel.PolicyLocation {
    return new tdic.pc.conversion.gxmodel.policylocationmodel.PolicyLocation(object)
  }

  public static function create(object : entity.PolicyLocation, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policylocationmodel.PolicyLocation {
    return new tdic.pc.conversion.gxmodel.policylocationmodel.PolicyLocation(object, options)
  }

}