package tdic.pc.conversion.gxmodel.generalliabilitycondmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GeneralLiabilityCondEnhancement : tdic.pc.conversion.gxmodel.generalliabilitycondmodel.GeneralLiabilityCond {
  public static function create(object : entity.GeneralLiabilityCond) : tdic.pc.conversion.gxmodel.generalliabilitycondmodel.GeneralLiabilityCond {
    return new tdic.pc.conversion.gxmodel.generalliabilitycondmodel.GeneralLiabilityCond(object)
  }

  public static function create(object : entity.GeneralLiabilityCond, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.generalliabilitycondmodel.GeneralLiabilityCond {
    return new tdic.pc.conversion.gxmodel.generalliabilitycondmodel.GeneralLiabilityCond(object, options)
  }

}