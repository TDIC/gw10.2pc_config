package tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLAdditionInsdSched_TDICEnhancement : tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.GLAdditionInsdSched_TDIC {
  public static function create(object : entity.GLAdditionInsdSched_TDIC) : tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.GLAdditionInsdSched_TDIC {
    return new tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.GLAdditionInsdSched_TDIC(object)
  }

  public static function create(object : entity.GLAdditionInsdSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.GLAdditionInsdSched_TDIC {
    return new tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.GLAdditionInsdSched_TDIC(object, options)
  }

}