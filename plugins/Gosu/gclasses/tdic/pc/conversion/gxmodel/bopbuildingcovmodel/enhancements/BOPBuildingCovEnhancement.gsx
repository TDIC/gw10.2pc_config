package tdic.pc.conversion.gxmodel.bopbuildingcovmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPBuildingCovEnhancement : tdic.pc.conversion.gxmodel.bopbuildingcovmodel.BOPBuildingCov {
  public static function create(object : entity.BOPBuildingCov) : tdic.pc.conversion.gxmodel.bopbuildingcovmodel.BOPBuildingCov {
    return new tdic.pc.conversion.gxmodel.bopbuildingcovmodel.BOPBuildingCov(object)
  }

  public static function create(object : entity.BOPBuildingCov, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.bopbuildingcovmodel.BOPBuildingCov {
    return new tdic.pc.conversion.gxmodel.bopbuildingcovmodel.BOPBuildingCov(object, options)
  }

}