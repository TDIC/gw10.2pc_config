package tdic.pc.conversion.gxmodel

uses entity.Contact
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.web.admin.shared.SharedUIHelper

enhancement PolicyContactRoleEnhancement : tdic.pc.conversion.gxmodel.policycontactrolemodel.types.complex.PolicyContactRole {

  function populateContactRole(period : entity.PolicyPeriod) {
    final var _logger = LoggerUtil.CONVERSION

    switch (this.Subtype) {
      case typekey.PolicyContactRole.TC_POLICYADDLNAMEDINSURED:
      case typekey.PolicyContactRole.TC_POLICYADDLINSURED:
        var contact = this.AccountContactRole.AccountContact.Contact.$TypeInstance.findOrCreateContact(period)
        if (!isDuplicate(period, contact, typekey.PolicyContactRole.TC_POLICYADDLNAMEDINSURED))
          period.addNewPolicyAddlNamedInsuredForContact(contact)
        break
      case typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED:
        var contact = this.AccountContactRole.AccountContact.Contact.$TypeInstance.findOrCreateContact(period)
        if (!isDuplicate(period, contact, this.Subtype)) {
          period.changePrimaryNamedInsuredTo(contact)
          period.changePolicyAddressTo(contact.PrimaryAddress)
        }
        if(contact typeis Person){
          if(contact.CDAMembershipStatus_TDIC == null or contact.CDAMembershipStatus_TDIC == typekey.GlobalStatus_TDIC.TC_INACTIVE){
            SharedUIHelper.performMembershipCheck_TDIC(contact, false, period.BaseState)
          }
        }
        break
      case typekey.PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC:
      case typekey.PolicyContactRole.TC_POLICYMORTGAGEE_TDIC:
      case typekey.PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC:
        break
      case typekey.PolicyContactRole.TC_POLICYENTITYOWNER_TDIC:
        //period.BOPLine.addNewBOPPolicyEntityOwnerOfContactType_TDIC(ConversionUtility.getContactType(this.AccountContactRole.AccountContact.Contact.Subtype))
        var contact = this.AccountContactRole.AccountContact.Contact.$TypeInstance.findOrCreateContact(period)
        var entityOwner = period.BOPLine.addBOPPolicyEntityOwner_TDIC(contact)
        entityOwner.Jurisdiction = typekey.Jurisdiction.get(entityOwner.AccountContactRole.AccountContact.Contact.State.Code)
        break
      default:
        var contact = this.AccountContactRole.AccountContact.Contact.$TypeInstance.findOrCreateContact(period)
        if (!isDuplicate(period, contact, this.Subtype))
          period.addNewPolicyContactRoleForContact(contact, this.Subtype)
        break
    }
  }

  private function isDuplicate(period : entity.PolicyPeriod, contact : Contact, subType : typekey.PolicyContactRole) : boolean {
    for (role in period.PolicyContactRoles.where(\elt -> elt.Subtype == subType)) {
      if (role.AccountContactRole.AccountContact.Contact.Person.LastName != null) {
        if (role.AccountContactRole.AccountContact.Contact.Person.LastName == contact.Person.LastName) {
          return true
        }
      }
      if (role.AccountContactRole.AccountContact.Contact.Name != null) {
        if (role.AccountContactRole.AccountContact.Contact.Name == contact.Name) {
          return true
        }
      }
    }
    return false
  }

}

