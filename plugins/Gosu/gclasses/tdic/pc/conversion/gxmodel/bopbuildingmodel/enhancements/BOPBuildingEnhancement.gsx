package tdic.pc.conversion.gxmodel.bopbuildingmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPBuildingEnhancement : tdic.pc.conversion.gxmodel.bopbuildingmodel.BOPBuilding {
  public static function create(object : entity.BOPBuilding) : tdic.pc.conversion.gxmodel.bopbuildingmodel.BOPBuilding {
    return new tdic.pc.conversion.gxmodel.bopbuildingmodel.BOPBuilding(object)
  }

  public static function create(object : entity.BOPBuilding, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.bopbuildingmodel.BOPBuilding {
    return new tdic.pc.conversion.gxmodel.bopbuildingmodel.BOPBuilding(object, options)
  }

}