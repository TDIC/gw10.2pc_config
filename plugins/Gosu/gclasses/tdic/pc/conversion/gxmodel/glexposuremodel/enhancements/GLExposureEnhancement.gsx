package tdic.pc.conversion.gxmodel.glexposuremodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLExposureEnhancement : tdic.pc.conversion.gxmodel.glexposuremodel.GLExposure {
  public static function create(object : entity.GLExposure) : tdic.pc.conversion.gxmodel.glexposuremodel.GLExposure {
    return new tdic.pc.conversion.gxmodel.glexposuremodel.GLExposure(object)
  }

  public static function create(object : entity.GLExposure, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glexposuremodel.GLExposure {
    return new tdic.pc.conversion.gxmodel.glexposuremodel.GLExposure(object, options)
  }

}