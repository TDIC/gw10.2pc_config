package tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PCAnswerDelegateEnhancement : tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.PCAnswerDelegate {
  public static function create(object : entity.PCAnswerDelegate) : tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.PCAnswerDelegate {
    return new tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.PCAnswerDelegate(object)
  }

  public static function create(object : entity.PCAnswerDelegate, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.PCAnswerDelegate {
    return new tdic.pc.conversion.gxmodel.pcanswerdelegatemodel.PCAnswerDelegate(object, options)
  }

}