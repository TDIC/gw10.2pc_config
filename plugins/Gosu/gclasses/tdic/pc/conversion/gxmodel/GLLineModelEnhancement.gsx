package tdic.pc.conversion.gxmodel

uses gw.api.productmodel.ClausePatternLookup
uses tdic.pc.common.pcfexpressions.CovTermDirectInputExprHelper
uses tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.util.ConversionUtility

enhancement GLLineModelEnhancement : tdic.pc.conversion.gxmodel.gllinemodel.types.complex.GLLine {

  function populateGLLineModel(period : PolicyPeriod) {
    this.GLLineCoverages.Entry.each(\cov -> {
      period.GLLine.createOrSyncCoverages()
      cov.$TypeInstance.populateCoverages(period.GLLine, this)
      CovTermDirectInputExprHelper.Instance.plCovRules(period.GLLine)
      period.GLLine.createOrSyncCoverages()
    })
    defaultProductModelCoverages(period)
    period.GLLine.createOrSyncCoverages()
    populateAdditionalEndorsements(period)
    period.GLLine.syncModifiers()
    for (mod in this.GLModifiers.Entry) {
      mod.$TypeInstance.populateModifier(period.GLLine)
    }
    for(exp in this.Exposures.Entry){
      exp.$TypeInstance.populateGLExposures(period.GLLine)
    }
    populateAnestheticModalities(period.GLLine)
    populateConditions(period.GLLine)
    populateRiskManagement(period.GLLine)
    populateLineQuestions(period.GLLine)
    // PolicyRenewalScreenHelper.carryOverCovForRenewalAndMigration(period)
  }

  private function populateLineQuestions(line : GLLine){
    var qs = line.Branch.Policy.Product.getQuestionSetByCodeIdentifier(ConversionConstants.GLUNDERWRITING_QS)
    for(ans in this.Answers.Entry){
      var qCode = ConversionConstants.GLUNDERWRITING_QS_CHANGES.get(ans.QuestionCode)
      var q = qs.getQuestionByCodeIdentifier(qCode == null ? ans.QuestionCode : qCode)
      if(q.PublicID != null) {
        var lineAnswer = new PolicyLineAnswer(line.Branch)
        switch (q.QuestionType){
          case TC_BOOLEAN :
            lineAnswer.BooleanAnswer = ans.BooleanAnswer
            break
          case TC_DATE :
            lineAnswer.DateAnswer = ans.DateAnswer
            break
          case TC_CHOICE :
            lineAnswer.ChoiceAnswer= ConversionUtility.getChoiceAnswer(ans.TextAnswer, q.CodeIdentifier)
            break
          default:
            switch(qCode){
              case ConversionConstants.DENTALSCHOOL_QUESTIONCODE :
                var criteria = new GLSchoolCodeSearchCriteria_TDIC() {:SchoolCode = ans.TextAnswer}
                var res = criteria.constructSearchQuery_TDIC().select().FirstResult as GLSchoolCode_TDIC
                lineAnswer.TextAnswer = res.DisplayName
                break
              default:
                lineAnswer.TextAnswer = ans.TextAnswer
            }
        }
        lineAnswer.QuestionCode = q.PublicID
        line.addToLineAnswers(lineAnswer)
      }
    }
  }

  private function populateAnestheticModalities(glLine : GLLine){
    var anesthetics = new ArrayList<AnestheticModilities_TDIC>()
    for(anesthetic in this.GLAnestheticModalities_TDIC.Entry) {
      anesthetics.add(anesthetic.AnestheticModality)
    }
    glLine.GLAnestheticModalitiesOfLine_TDIC = anesthetics.toTypedArray()
    for(ivim in this.GLIVIMSedationInHospital_TDIC.Entry) {
      var glIVIMSedationInHospital_TDIC = glLine.createAndAddGLIVIMSedationInHospital_TDIC()
      glIVIMSedationInHospital_TDIC.Name_TDIC = ivim.Name_TDIC
      glIVIMSedationInHospital_TDIC.PermitLicenseNumber_TDIC = ivim.PermitLicenseNumber_TDIC
    }
    for(inOffice in this.GLGeneralAnesthesiaInOffice_TDIC.Entry) {
      var glGeneralAnesthesiaInOffice_TDIC = glLine.createAndAddGLGeneralAnesthesiaInOffice_TDIC()
      glGeneralAnesthesiaInOffice_TDIC.Name_TDIC = inOffice.Name_TDIC
      glGeneralAnesthesiaInOffice_TDIC.PermitLicenseNumber_TDIC = inOffice.PermitLicenseNumber_TDIC
    }
  }

  private function populateRiskManagement(line : GLLine) {
    for (riskMgmt in this.RiskManagementDisc_TDIC.Entry) {
      riskMgmt.$TypeInstance.populateRiskManagementDiscount(line)
    }
  }

  private function populateConditions(glLine : GLLine) {
    if (this.GLLineConditions.Entry == null or this.GLLineConditions.Entry.Empty) {
      return
    }
    for (condition in this.GLLineConditions.Entry) {
      var pattern = ClausePatternLookup.getConditionPatternByCodeIdentifier(condition.Pattern.CodeIdentifier)
      try {
          var conditionEntity = glLine.getOrCreateCondition(pattern)
          glLine.setConditionExists(pattern, true)
          for (term in condition.CovTerms.Entry) {
            if (term.DisplayValue != null) {
              var conditionTerm = conditionEntity.getCovTermByCodeIdentifier(term.PatternCode)
              if (ConversionConstants.GLFULLTIMEFACULTY_DISCOUNT_TERMS.contains(conditionTerm.PatternCodeIdentifier)) {
                var criteria = new GLSchoolCodeSearchCriteria_TDIC() {:SchoolCode = term.DisplayValue}
                var res = criteria.constructSearchQuery_TDIC().select().FirstResult as GLSchoolCode_TDIC
                conditionTerm.setValueFromString(res.DisplayName)
              } else {
                var termOption = ConversionConstants.GL_CONDITION_TERM_OPTIONS_MAPPER.get(conditionTerm.PatternCodeIdentifier)
                var value = termOption == null ? term.DisplayValue : termOption.get(term.DisplayValue) == null ? term.DisplayValue : termOption.get(term.DisplayValue)
                conditionTerm.setValueFromString(value)
              }
            }
          }
      }catch (e : Exception) {
         LoggerUtil.CONVERSION_ERROR.info("Condition :" + condition.Pattern.CodeIdentifier+ " ;" + e.LocalizedMessage)
       }
    }
  }

  private function populateAdditionalEndorsements(period : PolicyPeriod) {
    for (entry in this.GLMobileDCSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLMobileDCSched_TDIC(period.GLLine)
    }
    for (entry in this.GLLocumTenensSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLLocumTenensSched_TDIC(period.GLLine)
    }
    for (entry in this.GLAdditionInsdSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLAdditionInsdSched_TDIC(period.GLLine)
    }
    for (entry in this.GLNameODSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLNameODSched_TDIC(period.GLLine)
    }
    for (entry in this.GLStateExclSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLStateExclSched_TDIC(period.GLLine)
    }

    for (entry in this.GLDentalBLAISched_TDIC.Entry) {
      entry.$TypeInstance.populateGLDentalBLAISched_TDIC(period.GLLine)
    }
    for (entry in this.GLCertofInsSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLCertofInsSched_TDIC(period.GLLine)
    }

    for (entry in this.GLExcludedServiceSched_TDIC.Entry) {
      entry.$TypeInstance.populateGLExcludedServiceSched_TDIC(period.GLLine)
    }
  }

  private function defaultProductModelCoverages(period : PolicyPeriod){
    period.GLLine.GLDentalEmpBenLiab_TDIC.CovTerms?.each(\elt -> {
      elt.setValueFromString("1000")
    })
    period.GLLine.GLDentalEmpBenLiab_TDIC.CovTerms?.each(\elt -> {
      if(elt.ValueAsString == null){
        elt.setValueFromString("100")
      }
    })
    period.GLLine.GLDentalMedWasteCov_TDIC.GLDMWCoPay_TDICTerm?.setValueFromString("20")
      period.GLLine.GLDentalMedWasteCov_TDIC.GLDMWLimit_TDICTerm?.setValueFromString("60")
    period.GLLine.GLRegulatoryAuthCov_TDIC.CovTerms?.each(\elt -> {
      elt.setValueFromString("100")
    })
  }

}
