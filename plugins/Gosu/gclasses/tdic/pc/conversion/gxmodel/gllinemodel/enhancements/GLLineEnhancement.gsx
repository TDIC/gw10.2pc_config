package tdic.pc.conversion.gxmodel.gllinemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLLineEnhancement : tdic.pc.conversion.gxmodel.gllinemodel.GLLine {
  public static function create(object : productmodel.GLLine) : tdic.pc.conversion.gxmodel.gllinemodel.GLLine {
    return new tdic.pc.conversion.gxmodel.gllinemodel.GLLine(object)
  }

  public static function create(object : productmodel.GLLine, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.gllinemodel.GLLine {
    return new tdic.pc.conversion.gxmodel.gllinemodel.GLLine(object, options)
  }

}