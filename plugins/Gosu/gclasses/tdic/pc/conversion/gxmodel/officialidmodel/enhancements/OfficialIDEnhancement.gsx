package tdic.pc.conversion.gxmodel.officialidmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement OfficialIDEnhancement : tdic.pc.conversion.gxmodel.officialidmodel.OfficialID {
  public static function create(object : entity.OfficialID) : tdic.pc.conversion.gxmodel.officialidmodel.OfficialID {
    return new tdic.pc.conversion.gxmodel.officialidmodel.OfficialID(object)
  }

  public static function create(object : entity.OfficialID, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.officialidmodel.OfficialID {
    return new tdic.pc.conversion.gxmodel.officialidmodel.OfficialID(object, options)
  }

}