package tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLLocumTenensSched_TDICEnhancement : tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.GLLocumTenensSched_TDIC {
  public static function create(object : entity.GLLocumTenensSched_TDIC) : tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.GLLocumTenensSched_TDIC {
    return new tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.GLLocumTenensSched_TDIC(object)
  }

  public static function create(object : entity.GLLocumTenensSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.GLLocumTenensSched_TDIC {
    return new tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.GLLocumTenensSched_TDIC(object, options)
  }

}