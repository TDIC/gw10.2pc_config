package tdic.pc.conversion.gxmodel.accountcontactrolemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountContactRoleEnhancement : tdic.pc.conversion.gxmodel.accountcontactrolemodel.AccountContactRole {
  public static function create(object : entity.AccountContactRole) : tdic.pc.conversion.gxmodel.accountcontactrolemodel.AccountContactRole {
    return new tdic.pc.conversion.gxmodel.accountcontactrolemodel.AccountContactRole(object)
  }

  public static function create(object : entity.AccountContactRole, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.accountcontactrolemodel.AccountContactRole {
    return new tdic.pc.conversion.gxmodel.accountcontactrolemodel.AccountContactRole(object, options)
  }

}