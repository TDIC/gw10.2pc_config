package tdic.pc.conversion.gxmodel

uses entity.Address
uses entity.Contact
uses entity.OfficialID
uses gw.api.database.Query
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.contact.ContactCreator
uses gw.plugin.contact.ContactSystemPlugin
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.exception.ConversionException
uses tdic.pc.conversion.helper.CommonConversionHelper

enhancement ContactModelEnhancement : tdic.pc.conversion.gxmodel.contactmodel.types.complex.Contact {
  function populateContact(contact : Contact) {
    this.EmailAddress1 = this.EmailAddress1 == null ? null : this.EmailAddress1.matches(".+@.+") ? this.EmailAddress1 : null
    SimpleValuePopulator.populate(this, contact)
    contact.AutoSync = AutoSync.TC_ALLOW
    if (contact typeis Person) {
      contact.FirstName = this.entity_Person.FirstName
      contact.LastName = this.entity_Person.LastName
      contact.MiddleName = this.entity_Person.MiddleName
      contact.MaritalStatus = this.entity_Person.MaritalStatus
      contact.Particle = this.entity_Person.Particle
      contact.DateOfBirth = (this.entity_Person.DateOfBirth != null) ? this.entity_Person.DateOfBirth : contact.DateOfBirth
      contact.CellPhone = (this.entity_Person.CellPhone != null) ? this.entity_Person.CellPhone : contact.CellPhone
      contact.LicenseNumber_TDIC = this.entity_Person.LicenseNumber_TDIC
      contact.LicenseState = this.entity_Person.LicenseState
      contact.Credential_TDIC = this.entity_Person.Credential_TDIC
      contact.Component_TDIC = this.entity_Person.Component_TDIC
    }
    if (!this.OfficialIDs.Entry.Empty) {
      for(official in this.OfficialIDs.Entry){
        switch(official.OfficialIDType){
          case OfficialIDType.TC_FEIN :
            contact.FEINOfficialID = official.OfficialIDValue
            break
          case OfficialIDType.TC_SSN :
            contact.SSNOfficialID = official.OfficialIDValue
            break
          case OfficialIDType.TC_ADANUMBER_TDIC :
            contact.ADANumber_TDICOfficialID = official.OfficialIDValue
            break
          default:
            throw new ConversionException("Invalid Official ID Type for Policy Contact Role :"+official.OfficialIDType)
        }
      }
    }

    if (contact.PrimaryAddress == null) {
      contact.PrimaryAddress = new Address(contact.Bundle)
    }

    populateAddress(this.PrimaryAddress.$TypeInstance, contact.PrimaryAddress)

    //SimpleValuePopulator.populate(this.PrimaryAddress.$TypeInstance, contact.PrimaryAddress)

    if (this.EmailAddress1 != null and !this.EmailAddress1.matches(".+@.+")) {
      LoggerUtil.CONVERSION_ERROR_EMAIL.error("Email :" + this.EmailAddress1 + "; " + contact.DisplayName + "; " + contact.PolicyPeriods?.first().LegacyPolicyNumber_TDIC)
    }
    if (this.PrimaryAddress.AddressLine1 != null and this.PrimaryAddress.AddressLine1.length() >= 45) {
      LoggerUtil.CONVERSION_ERROR_ADDRESS.error("AddressLine1 :" + this.PrimaryAddress.AddressLine1 + "; " + contact.DisplayName + "; " + contact.PolicyPeriods?.first().LegacyPolicyNumber_TDIC)
    }
    if (this.PrimaryAddress.AddressLine2 != null and this.PrimaryAddress.AddressLine2.length() >= 45) {
      LoggerUtil.CONVERSION_ERROR_ADDRESS.error("AddressLine2 :" + this.PrimaryAddress.AddressLine2 + "; " + contact.DisplayName + "; " + contact.PolicyPeriods?.first().LegacyPolicyNumber_TDIC)
    }
    if (this.PrimaryAddress.AddressLine3 != null and this.PrimaryAddress.AddressLine3.length() >= 45) {
      LoggerUtil.CONVERSION_ERROR_ADDRESS.error("AddressLine3 :" + this.PrimaryAddress.AddressLine3 + "; " + contact.DisplayName + "; " + contact.PolicyPeriods?.first().LegacyPolicyNumber_TDIC)
    }
  }

  function populateAddress(source : gw.webservice.pc.pc1000.gxmodel.addressmodel.types.complex.Address, target : Address) {

    if (target.AddressLine1 == null) {
      target.AddressLine1 = source.AddressLine1 == null ? null : source.AddressLine1.length() >= 45 ? source.AddressLine1.substring(0, 44) : source.AddressLine1
    }
    if (target.AddressLine2 == null) {
      target.AddressLine2 = source.AddressLine2 == null ? null : source.AddressLine2.length() >= 45 ? source.AddressLine2.substring(0, 44) : source.AddressLine2
    }
    if (target.AddressLine3 == null) {
      target.AddressLine3 = source.AddressLine3
    }
    if (target.PostalCode == null) {
      target.PostalCode = source.PostalCode
    }
    if (target.City == null) {
      target.City = source.City
    }
    if (target.State == null) {
      target.State = source.State
    }
    if (target.County == null) {
      target.County = source.County
    }
    if (target.Country == null) {
      target.Country = Country.TC_US
    }
    if (target.AddressType == null) {
      target.AddressType = source.AddressType
    }


  }

  function findOrCreateContact(account : Account) : Contact {
    var result : Contact
    if (this.AddressBookUID != null) {
      var contactSystemPlugin = Plugins.get(ContactSystemPlugin)
      result = contactSystemPlugin.retrieveContact(this.AddressBookUID, new ContactCreator(account.Bundle))
    } else if (this.PublicID != null) {
      result = Query.make(Contact).compare(Contact#PublicID, Equals, this.PublicID).select().AtMostOneRow
    }

    if (result == null) {
      if (this.entity_Person == null) {// company
        result = account.AccountContacts*.Contact.firstWhere(\c -> c typeis Company
            and c.Name == this.Name
            and c.NameKanji == this.NameKanji
        )
        if (result == null) {
          result = new Company(account.Bundle)
          this.populateContact(result)
        }
      } else {//person
        result = account.AccountContacts*.Contact.firstWhere(\c -> c typeis Person
            and c.FirstName == this.entity_Person.FirstName
            and c.LastName == this.entity_Person.LastName)
        if (result == null) {
          result = new Person(account.Bundle)
          this.populateContact(result)
        }
      }

    }
    return result
  }

  function findOrCreateContact(period : PolicyPeriod) : Contact {
    var result : Contact
    var account = period.Policy.Account
    result = new CommonConversionHelper().existingContact(this)
    findOrCreateOfficialIDs(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact)
    if (result != null) {
      result = period.Bundle.add(result)
      return result
      //this.populateContact(result)
      //account.Bundle.add(result)
    }
    if (result == null) {
      if (this.entity_Person == null) {
        // company
        result = new Company(account.Bundle)
        this.populateContact(result)

      } else {
        //person
        result = new Person(account.Bundle)
        this.populateContact(result)
      }
    }
    if (this.OfficialIDs != null)
      findOrCreateOfficialIDs(result)
    return result
  }

  function findOrCreateContact(bundle : Bundle) : Contact {
    var result : Contact
    result = new CommonConversionHelper().existingContact(this)
    if (result != null) {
      result = bundle.add(result)
      return result
      //this.populateContact(result)
      //account.Bundle.add(result)
    }
    if (result == null) {
      if (this.entity_Person == null) {
        // company
        result = new Company(bundle)
        this.populateContact(result)

      } else {
        //person
        result = new Person(bundle)
        this.populateContact(result)
      }
    }
    if (this.OfficialIDs != null)
      findOrCreateOfficialIDs(result)
    return result
  }

  function findOrCreateOfficialIDs(contact : Contact) {
    for (officialID in this.OfficialIDs.Entry) {
      var id = contact.OfficialIDs?.firstWhere(\elt -> elt.OfficialIDType == officialID.OfficialIDType)
      if (id == null) {
        id = new OfficialID(contact.Bundle)
        id.OfficialIDType = officialID.OfficialIDType
      }
      id.OfficialIDValue = officialID.OfficialIDValue
      if (id.OfficialIDType == OfficialIDType.TC_NCCIINTRASTATE || id.OfficialIDType == OfficialIDType.TC_DOLID) {
        id.State = Jurisdiction.TC_MO
      }
      contact.addToOfficialIDs(id)
    }
  }

  function createContact(bundle : Bundle) : Contact {
    var contact = isPersonContact() ? new Person(bundle) : new Company(bundle)
    populateContact(contact)
    return contact
  }

  function isContactExist() : boolean {
    if (not this.AddressBookUID_elem.$Nil) {
      if (Contact.findFromAddressBookUID(this.AddressBookUID) != null) {
        return true
      }
    }
    if (not this.PublicID_elem.$Nil) {
      if (Query.make(Contact).compare(Contact#PublicID, Equals, this.PublicID).select().HasElements) {
        return true
      }
    }
    return false
  }

  function isPersonContact() : boolean {
    // In PC at the time this code is written, there is only 2 subtypes of contact
    return this.Subtype == typekey.Contact.TC_PERSON
  }


}
