package tdic.pc.conversion.gxmodel.accountmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : tdic.pc.conversion.gxmodel.accountmodel.Account {
  public static function create(object : entity.Account) : tdic.pc.conversion.gxmodel.accountmodel.Account {
    return new tdic.pc.conversion.gxmodel.accountmodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.accountmodel.Account {
    return new tdic.pc.conversion.gxmodel.accountmodel.Account(object, options)
  }

}