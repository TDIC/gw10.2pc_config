package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.types.complex.GLCertofInsSched_TDIC
uses tdic.pc.conversion.util.ConversionUtility

enhancement GLCertofInsSched_TDICModelEnhancement : GLCertofInsSched_TDIC {

  function populateGLCertofInsSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddCertofInsSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
    var certHolder : PolicyCertificateHolder_TDIC
    if (glLine.GLCertificateHolders_TDIC?.hasMatch(\certificateHolder -> certificateHolder.AccountContactRole.AccountContact.Contact.Name == this.CertificateHolder.AccountContactRole.AccountContact.Contact.Name_elem.$Value)) {
      certHolder = glLine.GLCertificateHolders_TDIC.firstWhere(\certificateHolder -> certificateHolder.AccountContactRole.AccountContact.Contact.Name == this.CertificateHolder.AccountContactRole.AccountContact.Contact.Name_elem.$Value)
    } else {
      certHolder = glLine.addNewPolicyCertificateHolderOfContactType_TDIC(ConversionUtility.
          getContactType(this.CertificateHolder.AccountContactRole.AccountContact.Contact.Subtype))
      this.CertificateHolder.AccountContactRole.AccountContact.
          Contact.$TypeInstance.populateContact(certHolder.AccountContactRole.AccountContact.Contact)
      certHolder.Description = this.CertificateHolder.entity_PolicyCertificateHolder_TDIC.Description
    }
    sched.CertificateHolder = certHolder
  }
}
