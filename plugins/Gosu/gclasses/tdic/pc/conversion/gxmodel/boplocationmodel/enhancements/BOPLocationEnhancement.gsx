package tdic.pc.conversion.gxmodel.boplocationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BOPLocationEnhancement : tdic.pc.conversion.gxmodel.boplocationmodel.BOPLocation {
  public static function create(object : entity.BOPLocation) : tdic.pc.conversion.gxmodel.boplocationmodel.BOPLocation {
    return new tdic.pc.conversion.gxmodel.boplocationmodel.BOPLocation(object)
  }

  public static function create(object : entity.BOPLocation, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.boplocationmodel.BOPLocation {
    return new tdic.pc.conversion.gxmodel.boplocationmodel.BOPLocation(object, options)
  }

}