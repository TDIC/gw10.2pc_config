package tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLDentalBLAISched_TDICEnhancement : tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.GLDentalBLAISched_TDIC {
  public static function create(object : entity.GLDentalBLAISched_TDIC) : tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.GLDentalBLAISched_TDIC {
    return new tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.GLDentalBLAISched_TDIC(object)
  }

  public static function create(object : entity.GLDentalBLAISched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.GLDentalBLAISched_TDIC {
    return new tdic.pc.conversion.gxmodel.gldentalblaisched_tdicmodel.GLDentalBLAISched_TDIC(object, options)
  }

}