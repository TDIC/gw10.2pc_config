package tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement IVIMSedationInHospital_TDICEnhancement : tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.IVIMSedationInHospital_TDIC {
  public static function create(object : entity.IVIMSedationInHospital_TDIC) : tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.IVIMSedationInHospital_TDIC {
    return new tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.IVIMSedationInHospital_TDIC(object)
  }

  public static function create(object : entity.IVIMSedationInHospital_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.IVIMSedationInHospital_TDIC {
    return new tdic.pc.conversion.gxmodel.ivimsedationinhospital_tdicmodel.IVIMSedationInHospital_TDIC(object, options)
  }

}