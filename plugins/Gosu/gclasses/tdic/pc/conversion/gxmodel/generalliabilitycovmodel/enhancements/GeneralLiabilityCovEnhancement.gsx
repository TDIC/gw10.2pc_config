package tdic.pc.conversion.gxmodel.generalliabilitycovmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GeneralLiabilityCovEnhancement : tdic.pc.conversion.gxmodel.generalliabilitycovmodel.GeneralLiabilityCov {
  public static function create(object : entity.GeneralLiabilityCov) : tdic.pc.conversion.gxmodel.generalliabilitycovmodel.GeneralLiabilityCov {
    return new tdic.pc.conversion.gxmodel.generalliabilitycovmodel.GeneralLiabilityCov(object)
  }

  public static function create(object : entity.GeneralLiabilityCov, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.generalliabilitycovmodel.GeneralLiabilityCov {
    return new tdic.pc.conversion.gxmodel.generalliabilitycovmodel.GeneralLiabilityCov(object, options)
  }

}