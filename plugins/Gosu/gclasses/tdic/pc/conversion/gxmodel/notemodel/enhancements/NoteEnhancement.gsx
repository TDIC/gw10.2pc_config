package tdic.pc.conversion.gxmodel.notemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement NoteEnhancement : tdic.pc.conversion.gxmodel.notemodel.Note {
  public static function create(object : entity.Note) : tdic.pc.conversion.gxmodel.notemodel.Note {
    return new tdic.pc.conversion.gxmodel.notemodel.Note(object)
  }

  public static function create(object : entity.Note, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.notemodel.Note {
    return new tdic.pc.conversion.gxmodel.notemodel.Note(object, options)
  }

}