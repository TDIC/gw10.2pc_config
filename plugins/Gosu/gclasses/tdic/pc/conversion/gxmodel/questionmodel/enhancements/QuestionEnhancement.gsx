package tdic.pc.conversion.gxmodel.questionmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement QuestionEnhancement : tdic.pc.conversion.gxmodel.questionmodel.Question {
  public static function create(object : gw.api.productmodel.Question) : tdic.pc.conversion.gxmodel.questionmodel.Question {
    return new tdic.pc.conversion.gxmodel.questionmodel.Question(object)
  }

  public static function create(object : gw.api.productmodel.Question, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.questionmodel.Question {
    return new tdic.pc.conversion.gxmodel.questionmodel.Question(object, options)
  }

}