package tdic.pc.conversion.gxmodel.contactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ContactEnhancement : tdic.pc.conversion.gxmodel.contactmodel.Contact {
  public static function create(object : entity.Contact) : tdic.pc.conversion.gxmodel.contactmodel.Contact {
    return new tdic.pc.conversion.gxmodel.contactmodel.Contact(object)
  }

  public static function create(object : entity.Contact, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.contactmodel.Contact {
    return new tdic.pc.conversion.gxmodel.contactmodel.Contact(object, options)
  }

}