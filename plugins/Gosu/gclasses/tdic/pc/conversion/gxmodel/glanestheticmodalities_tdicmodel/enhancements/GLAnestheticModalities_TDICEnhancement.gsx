package tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLAnestheticModalities_TDICEnhancement : tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.GLAnestheticModalities_TDIC {
  public static function create(object : entity.GLAnestheticModalities_TDIC) : tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.GLAnestheticModalities_TDIC {
    return new tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.GLAnestheticModalities_TDIC(object)
  }

  public static function create(object : entity.GLAnestheticModalities_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.GLAnestheticModalities_TDIC {
    return new tdic.pc.conversion.gxmodel.glanestheticmodalities_tdicmodel.GLAnestheticModalities_TDIC(object, options)
  }

}