package tdic.pc.conversion.gxmodel.modifiermodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ModifierEnhancement : tdic.pc.conversion.gxmodel.modifiermodel.Modifier {
  public static function create(object : entity.Modifier) : tdic.pc.conversion.gxmodel.modifiermodel.Modifier {
    return new tdic.pc.conversion.gxmodel.modifiermodel.Modifier(object)
  }

  public static function create(object : entity.Modifier, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.modifiermodel.Modifier {
    return new tdic.pc.conversion.gxmodel.modifiermodel.Modifier(object, options)
  }

}