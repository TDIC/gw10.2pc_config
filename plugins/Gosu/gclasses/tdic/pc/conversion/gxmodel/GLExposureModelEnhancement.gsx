package tdic.pc.conversion.gxmodel

uses gw.api.database.Query
uses tdic.pc.conversion.gxmodel.glexposuremodel.types.complex.GLExposure

enhancement GLExposureModelEnhancement : GLExposure {

  function populateGLExposures(line : GLLine) {
    var exp = new entity.GLExposure(line.Branch)
    exp.SpecialityCode_TDIC = this.SpecialityCode_TDIC
    var loc = line.Branch.PolicyLocations.firstWhere(\elt -> elt.PrimaryLoc)
    exp.Location = loc
    exp.GLTerritory_TDIC = Query.make(GLTerritory_TDIC).
    compare(GLTerritory_TDIC#Component, Equals, this.GLTerritory_TDIC.Component).
    compare(GLTerritory_TDIC#StateCode, Equals, loc.State.Code).select().FirstResult
    exp.ClassCode_TDIC = this.ClassCode_TDIC
    exp.setGLExposureDescription_TDIC()
    line.addToExposures(exp)
  }

}
