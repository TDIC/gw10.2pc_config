package tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLNameODSched_TDICEnhancement : tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.GLNameODSched_TDIC {
  public static function create(object : entity.GLNameODSched_TDIC) : tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.GLNameODSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.GLNameODSched_TDIC(object)
  }

  public static function create(object : entity.GLNameODSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.GLNameODSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glnameodsched_tdicmodel.GLNameODSched_TDIC(object, options)
  }

}