package tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLCertofInsSched_TDICEnhancement : tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.GLCertofInsSched_TDIC {
  public static function create(object : entity.GLCertofInsSched_TDIC) : tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.GLCertofInsSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.GLCertofInsSched_TDIC(object)
  }

  public static function create(object : entity.GLCertofInsSched_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.GLCertofInsSched_TDIC {
    return new tdic.pc.conversion.gxmodel.glcertofinssched_tdicmodel.GLCertofInsSched_TDIC(object, options)
  }

}