package tdic.pc.conversion.gxmodel.covtermmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CovTermEnhancement : tdic.pc.conversion.gxmodel.covtermmodel.CovTerm {
  public static function create(object : gw.api.domain.covterm.CovTerm) : tdic.pc.conversion.gxmodel.covtermmodel.CovTerm {
    return new tdic.pc.conversion.gxmodel.covtermmodel.CovTerm(object)
  }

  public static function create(object : gw.api.domain.covterm.CovTerm, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.covtermmodel.CovTerm {
    return new tdic.pc.conversion.gxmodel.covtermmodel.CovTerm(object, options)
  }

}