package tdic.pc.conversion.gxmodel

uses gw.api.productmodel.ClausePatternLookup
uses gw.api.productmodel.CovTermPatternLookup
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.common.pcfexpressions.CovTermDirectInputExprHelper
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.gxmodel.bopbuildingcovmodel.anonymous.elements.BOPBuildingCov_CovTerms_Entry
uses tdic.pc.conversion.util.ConversionUtility


enhancement BOPBuildingCovModelEnhancement : tdic.pc.conversion.gxmodel.bopbuildingcovmodel.types.complex.BOPBuildingCov {

  function populateCoverages(bopBuilding : BOPBuilding, bopLine : tdic.pc.conversion.gxmodel.boplinemodel.types.complex.BOPLine) {
    var covPattern = ConversionConstants.COVERAGE_MAPPER.get(this.PatternCode)
    var pattern = ClausePatternLookup.getCoveragePatternByCodeIdentifier(covPattern == null ? this.PatternCode : covPattern)
    try {
      var cov = bopBuilding.getOrCreateCoverage(pattern)
      bopBuilding.setCoverageExists(pattern, true)
      for (covTerm in this.CovTerms.Entry) {
        var covTermPattern = CovTermPatternLookup.getByCodeIdentifier(covTerm.PatternCode)
        var term = cov.getCovTerm(covTermPattern)
        if (term == null) {
          LoggerUtil.CONVERSION_ERROR.info("Pol :" + bopBuilding.Branch.LegacyPolicyNumber_TDIC + " ;Building location : " + bopBuilding.BOPLocation.Location.State + " ;Coverage : " + this.PatternCode + ";Coverage Term : " + covTerm.PatternCode)
        } else {
          var value = ConversionConstants.BOP_COVERAGETERM_OPTIONS_MAPPER.get(covTerm.PatternCode)
          term.setValueFromString(value == null ? covTerm.DisplayValue : value.get(covTerm.DisplayValue))
        }
      }
      bopBuilding.setCoverageExists(pattern, true)
      if(cov.Pattern.CodeIdentifier == ConversionConstants.CP_MANUSCRIPT_PATTERN_CODE) {
        handleManuscriptSchedule(this.CovTerms.Entry, bopBuilding, bopLine)
      }
      CovTermDirectInputExprHelper.Instance.bopCovRules(cov, bopBuilding)
    } catch (e : Exception) {
      LoggerUtil.CONVERSION_ERROR.info(" Coverage :" + this.PatternCode + " ;" + e.LocalizedMessage)
    }
  }

  private function handleManuscriptSchedule(covTerms : List<BOPBuildingCov_CovTerms_Entry>, bopBuilding : BOPBuilding, bopLine : tdic.pc.conversion.gxmodel.boplinemodel.types.complex.BOPLine){
    for (manuscript in bopLine.BOPManuscript_TDIC.Entry) {
      var sched = bopBuilding.createAndAddBOPManuscript_TDIC()
      sched.ManuscriptEffectiveDate = bopBuilding.Branch.EditEffectiveDate.addYears(1)
      if (manuscript.policyAddInsured_TDIC != null) {
        var policyAddlInsDetail =
            bopBuilding.addNewAdditionalInsuredDetailOfContactType_TDIC(ConversionUtility.
                getContactType(manuscript.policyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Subtype))
        manuscript.policyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole.AccountContact.
            Contact.$TypeInstance.populateContact(policyAddlInsDetail.PolicyAddlInsured.AccountContactRole.AccountContact.Contact)
        policyAddlInsDetail.AdditionalInsuredType = manuscript.policyAddInsured_TDIC.AdditionalInsuredType
        policyAddlInsDetail.Desciption = manuscript.policyAddInsured_TDIC.Desciption
        sched.policyAddInsured_TDIC = policyAddlInsDetail
      }

      if (manuscript.PolicyMortgagee != null) {
        var contactType = ConversionUtility.getContactType(manuscript.PolicyMortgagee.AccountContactRole.AccountContact.Contact.Subtype)
        var policyMortgagee = bopBuilding.addNewPolicyMortgageeOfContactType_TDIC(contactType)
        manuscript.PolicyMortgagee.$TypeInstance.populateBOPBldgMortgagee(bopBuilding, policyMortgagee)
        sched.PolicyMortgagee = policyMortgagee
      }

      if (manuscript.PolicyLossPayee != null) {
        var contactType = ConversionUtility.getContactType(manuscript.PolicyLossPayee.AccountContactRole.AccountContact.Contact.Subtype)
        var policyLossPayee = bopBuilding.addNewPolicyLossOfPayeeOfContactType_TDIC(contactType)
        manuscript.PolicyLossPayee.$TypeInstance.populateBOPLossPayee(bopBuilding, policyLossPayee)
        sched.PolicyLossPayee  = policyLossPayee
      }

      sched.ManuscriptDescription = manuscript.ManuscriptDescription
      sched.ManuscriptType = manuscript.ManuscriptType
      sched.BOPBuilding = bopBuilding
    }
  }
}
