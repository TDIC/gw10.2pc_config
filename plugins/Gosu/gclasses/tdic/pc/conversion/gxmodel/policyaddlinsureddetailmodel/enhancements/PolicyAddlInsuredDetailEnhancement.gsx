package tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyAddlInsuredDetailEnhancement : tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.PolicyAddlInsuredDetail {
  public static function create(object : entity.PolicyAddlInsuredDetail) : tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.PolicyAddlInsuredDetail {
    return new tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.PolicyAddlInsuredDetail(object)
  }

  public static function create(object : entity.PolicyAddlInsuredDetail, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.PolicyAddlInsuredDetail {
    return new tdic.pc.conversion.gxmodel.policyaddlinsureddetailmodel.PolicyAddlInsuredDetail(object, options)
  }

}