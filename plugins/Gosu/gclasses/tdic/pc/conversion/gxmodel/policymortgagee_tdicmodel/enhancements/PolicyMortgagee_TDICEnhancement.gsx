package tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyMortgagee_TDICEnhancement : tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.PolicyMortgagee_TDIC {
  public static function create(object : entity.PolicyMortgagee_TDIC) : tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.PolicyMortgagee_TDIC {
    return new tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.PolicyMortgagee_TDIC(object)
  }

  public static function create(object : entity.PolicyMortgagee_TDIC, options : gw.api.gx.GXOptions) : tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.PolicyMortgagee_TDIC {
    return new tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.PolicyMortgagee_TDIC(object, options)
  }

}