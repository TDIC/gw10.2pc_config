package tdic.pc.conversion.account

uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.AccountContactDAO
uses tdic.pc.conversion.dao.AccountDAO
uses tdic.pc.conversion.dao.AddressDAO
uses tdic.pc.conversion.dao.ContactDAO
uses tdic.pc.conversion.dto.ContactDTO
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs_Entry
uses tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_Policy_Account
uses tdic.pc.conversion.helper.CommonConversionHelper

class AccountConversionHelper {

  var _accountID : String
  var _baseState : String
  var _accountModel : PolicyPeriod_Policy_Account

  construct(accountID : String, baseState : String) {
    _accountID = accountID
    _baseState = baseState
  }

  function generateAccountModel() : PolicyPeriod_Policy_Account {
    _accountModel = new PolicyPeriod_Policy_Account()
    var accountDAO = new AccountDAO({_accountID})
    var res = accountDAO.retrieveAllUnprocessed()
    var accountDTO = res.first()
    var accountContactDAO = new AccountContactDAO({_accountID})
    var accountContact = accountContactDAO.retrieveAllUnprocessed().first()

    prepareAccountHolderContact(accountContact.ContactID)
    _accountModel.YearBusinessStarted = accountDTO.YearBusinessStarted == null ?
        null : accountDTO.YearBusinessStarted.toInt() == 0 ? null : accountDTO.YearBusinessStarted.toInt()
    var orgType = _accountModel.AccountHolderContact.Subtype == typekey.Contact.TC_PERSON ? AccountOrgType.TC_SOLEPROPSHIP : AccountOrgType.TC_OTHER
    _accountModel.AccountOrgType = orgType
    var producerCode = ConversionConstants.PRODUCERCODE_BY_STATE.get(_baseState)
    _accountModel.ProducerCodes.Entry[0].ProducerCode.Code = producerCode == null ? ConversionConstants.DEFAULT_PRODUCERCODE : producerCode
    return _accountModel
  }

  private function prepareAccountHolderContact(contactID : String) {
    var contactDAO = new ContactDAO({contactID})
    var contactDTO = contactDAO.retrieveAllUnprocessed().first()

    if (contactDTO.SubType?.equalsIgnoreCase(typekey.ContactType.TC_COMPANY.Code)) {
      _accountModel.AccountHolderContact.Name = contactDTO.Name
      _accountModel.AccountHolderContact.Subtype = typekey.Contact.TC_COMPANY
    } else if (contactDTO.SubType?.equalsIgnoreCase(typekey.ContactType.TC_PERSON.Code)) {
      _accountModel.AccountHolderContact.entity_Person.LastName = contactDTO.LastName?.length() > 30 ? contactDTO.LastName?.substring(0, 29) : contactDTO.LastName?.trim()
      _accountModel.AccountHolderContact.entity_Person.FirstName = contactDTO.FirstName?.length() > 30 ? contactDTO.FirstName?.substring(0, 29) : contactDTO.FirstName?.trim()
      //_accountModel.AccountHolderContact.entity_Person.MiddleName = contactDTO.MiddleName?.length() > 30 ? contactDTO.MiddleName?.substring(0,29) : contactDTO.MiddleName?.trim()
      _accountModel.AccountHolderContact.entity_Person.DateOfBirth = CommonConversionHelper.parseDate(contactDTO.DateOfBirth)
      _accountModel.AccountHolderContact.entity_Person.CellPhone = contactDTO.CellPhone == null ? null : contactDTO.CellPhone.contains("000-") ? null : contactDTO.CellPhone
      _accountModel.AccountHolderContact.entity_Person.LicenseNumber_TDIC = contactDTO.LicenseNumber
      _accountModel.AccountHolderContact.entity_Person.LicenseState = contactDTO.LicenseState == null ? null : Jurisdiction.get(contactDTO.LicenseState)
      _accountModel.AccountHolderContact.entity_Person.Credential_TDIC = contactDTO.Credential_TDIC == null ? null : Credential_TDIC.get(contactDTO.Credential_TDIC)
      _accountModel.AccountHolderContact.entity_Person.Component_TDIC = contactDTO.Component_TDIC == null ? null : Component_TDIC.get(contactDTO.Component_TDIC)
      _accountModel.AccountHolderContact.Subtype = typekey.Contact.TC_PERSON
    }
    var addressDAO = new AddressDAO({contactDTO.PrimaryAddressID})
    var addressDTO = addressDAO.retrieveAllUnprocessed().first()

    _accountModel.AccountHolderContact.PrimaryAddress.AddressType = typekey.AddressType.get(addressDTO.AddressType)
    _accountModel.AccountHolderContact.PrimaryAddress.AddressLine1 = addressDTO.AddressLine1?.replaceAll("\\n", "")
    _accountModel.AccountHolderContact.PrimaryAddress.AddressLine2 = addressDTO.AddressLine2
    _accountModel.AccountHolderContact.PrimaryAddress.AddressLine3 = addressDTO.AddressLine3
    _accountModel.AccountHolderContact.PrimaryAddress.City = addressDTO.City
    _accountModel.AccountHolderContact.PrimaryAddress.Country = Country.TC_US//get(_accountDTO.Country)
    _accountModel.AccountHolderContact.PrimaryAddress.PostalCode = addressDTO.PostalCode
    _accountModel.AccountHolderContact.PrimaryAddress.State = typekey.State.get(addressDTO.State)
    _accountModel.AccountHolderContact.PrimaryAddress.County = addressDTO.County
    _accountModel.AccountHolderContact.EmailAddress1 = contactDTO.EmailAddress1
    _accountModel.IndustryCode.Code = ConversionConstants.DEFAULT_INDUSTRYCODE
    prepareOfficialID(contactDTO.SSNbr)
    if(contactDTO.SSNbr == null
        or (contactDTO.TaxID != null and contactDTO.TaxID.replaceAll("-","") != contactDTO.SSNbr.replaceAll("-","") )) {
      prepareOfficialID(contactDTO.TaxID)
    }
    prepareADANumber(contactDTO, typekey.State.get(addressDTO.State))

    if (contactDTO.PrimaryPhone?.equalsIgnoreCase(PrimaryPhoneType.TC_HOME.Code)) {
      _accountModel.AccountHolderContact.PrimaryPhone = PrimaryPhoneType.TC_HOME
    } else {
      _accountModel.AccountHolderContact.PrimaryPhone = PrimaryPhoneType.TC_WORK
    }
    _accountModel.AccountHolderContact.HomePhone = contactDTO.HomePhone == null ? null : contactDTO.HomePhone.contains("000-") ? null : contactDTO.HomePhone
    _accountModel.AccountHolderContact.WorkPhone = contactDTO.WorkPhone == null ? null : contactDTO.WorkPhone.contains("000-") ? null : contactDTO.WorkPhone
    _accountModel.AccountHolderContact.FaxPhone = contactDTO.FaxPhone == null ? null : contactDTO.FaxPhone.contains("000-") ? null : contactDTO.FaxPhone

  }

  private function prepareOfficialID(value : String) {
    if (value == null) {
      return
    }
    initializeOfficialIDEntry()
    var entry = new Contact_OfficialIDs_Entry()
    if (value.matches("\\d{3}-\\d{2}-\\d{4}")) {
      entry.OfficialIDType = OfficialIDType.TC_SSN
      entry.OfficialIDValue = value
    } else {
      entry.OfficialIDType = OfficialIDType.TC_FEIN
      entry.OfficialIDValue = feinFormat(value)
    }
    _accountModel.AccountHolderContact.OfficialIDs.Entry.add(entry)
  }

  private function initializeOfficialIDEntry(){
    if(_accountModel.AccountHolderContact.OfficialIDs == null){
      _accountModel.AccountHolderContact.OfficialIDs = new Contact_OfficialIDs()
      _accountModel.AccountHolderContact.OfficialIDs.Entry = new ArrayList<Contact_OfficialIDs_Entry>()
    }
  }

  private function prepareADANumber(contactDTO : ContactDTO, state : State) {
    if (contactDTO.getADANumberOfficialID_TDIC(state) == null) {
      return
    }
    initializeOfficialIDEntry()
    var adaEntry = new Contact_OfficialIDs_Entry()
    adaEntry.OfficialIDType = OfficialIDType.TC_ADANUMBER_TDIC
    adaEntry.OfficialIDValue = contactDTO.getADANumberOfficialID_TDIC(state)
    _accountModel.AccountHolderContact.OfficialIDs.Entry.add(adaEntry)
  }

  /*
    private static function ssnFormat(taxid : String) : String {
      var ssn = taxid?.replace("-", "");
      if (ssn.length > 8) {
        ssn = ssn?.substring(0, 3) + "-" + ssn?.substring(3, 5) + "-" + ssn?.substring(5, ssn.length)
      }

      return ssn
    }


  */
  private static function feinFormat(taxid : String) : String {
    var fein = taxid?.replace("-", "");
    if (fein.length > 8) {
      fein = fein?.substring(0, 2) + "-" + fein?.substring(2, fein.length)
    }

    return fein
  }
}