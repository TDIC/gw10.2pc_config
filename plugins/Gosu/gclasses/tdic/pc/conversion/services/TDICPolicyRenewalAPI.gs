package tdic.pc.conversion.services

uses gw.api.locale.DisplayKey
uses gw.api.productmodel.Product
uses gw.api.system.PCLoggerCategory
uses gw.api.upgrade.Coercions
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.DataConversionException
uses gw.api.webservice.exception.FieldFormatException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.api.webservice.exception.SOAPException
uses gw.webservice.SOAPUtil
uses gw.webservice.pc.pc900.job.PolicyRenewalAPI
uses gw.xml.ws.annotation.WsiPermissions
uses tdic.pc.conversion.account.AccountScenariosHandler
uses tdic.pc.conversion.exception.ConversionException
uses tdic.pc.conversion.util.ConversionUtility

class TDICPolicyRenewalAPI extends PolicyRenewalAPI {

  /**
   * Starts a renewal for a policy that does not already exist in PC. The policy data is passed
   * in as policyPeriodData string which will be parsed by Guidewire's PolicyPeriod GX model schema.
   *
   * @param accountNumber         account number
   * @param productCode           the code of the product (e.g., PersonalAuto, WorkersComp)
   * @param producerCodeId        public id of the producer code
   * @param policyNumber          The policy number for the policy periods associated with this Renewal. If null,
   *                              generate a new, unique policyNumber. Raises an underwriter issue if not unique.
   * @param policyPeriodData      the data used to populate the new policy period
   * @param changesToApply        things to change in the renewal period that differ from the legacy period. This
   *                              parameter is not used out of the box, but is present to facilitate custom implementations.
   * @param parseOptions          the options passed to the parser to parse policyPeriodData
   * @param basedOnEffectiveDate  the effective date for the basedOn period
   * @param basedOnExpirationDate the expiration date for the basedOn period. This is
   *                              also is the effectiveDate of the new renewal period.
   * @return the job number of the newly started renewal
   */
  @Throws(RequiredFieldException, "If any required field is null")
  @Throws(BadIdentifierException, "If the specified account, product or producer code does not exist")
  @Throws(DataConversionException, "If a policy period cannot be populated from policyPeriodData.")
  @Throws(SOAPException, "If communication fails")
  @Throws(FieldFormatException, "If either basedOnEffectiveDate or basedOnExpirationDate cannot be parsed")
  @Param("accountNumber", "account number")
  @Param("productCode", "the code of the product (e.g., PersonalAuto, WorkersComp)")
  @Param("producerCodeId", "public id of the producer code")
  @Param("policyNumber", "The policy number for the policy periods associated with this Renewal. If null, generate a new, unique policyNumber. Raises an underwriter issue if not unique.")
  @Param("policyPeriodData", "the data used to populate the new policy period")
  @Param("changesToApply", "things to change in the renewal period that differ from the legacy period. This parameter is not used out of the box, but is present to facilitate custom implementations.")
  @Param("parseOptions", "the options passed to the parser to parse policyPeriodData")
  @Param("basedOnEffectiveDate", "the effective date for the basedOn period")
  @Param("basedOnExpirationDate", "the expiration date for the basedOn period. This is also is the effectiveDate of the new renewal period.")
  @WsiPermissions({SystemPermissionType.TC_CREATERENEWAL})
  @Returns("the job number of the newly started renewal")
  function startConversionRenewal(accountNumber : String,
                                  producerCodeId : String,
                                  policyNumber : String,
                                  policyPeriodData : String,
                                  changesToApply : String,
                                  parseOptions : String,
                                  basedOnEffectiveDate : String,
                                  basedOnExpirationDate : String) : String {


    var basedOnEffectiveDateAsDate : Date
    var basedOnExpirationDateAsDate : Date
    var renewal : Renewal
    startRenewal(accountNumber, producerCodeId, policyPeriodData, \account, product, producerCode -> {
      SOAPUtil.require(basedOnEffectiveDate, "basedOnEffectiveDate")
      SOAPUtil.require(basedOnExpirationDate, "basedOnExpirationDate")
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        basedOnExpirationDateAsDate = ConversionUtility.parseDateFromString(basedOnExpirationDate, "basedOnExpirationDate")
        basedOnEffectiveDateAsDate = basedOnExpirationDateAsDate.addYears(-1)
        account = bundle.add(account)
        try {
          renewal = account.createConversionRenewalWithBasedOn(basedOnEffectiveDateAsDate, basedOnExpirationDateAsDate,
              product, producerCode, policyNumber, \period -> {
            try {
              if (policyPeriodData != null and policyPeriodData.NotBlank) {
                //period.PolicyNumber = null
                var model = tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod.parse(policyPeriodData)
                model.$TypeInstance.populatePolicyPeriod(period, basedOnEffectiveDateAsDate, basedOnExpirationDateAsDate)
                if (period.Policy.OriginalEffectiveDate == null) {
                  period.Policy.OriginalEffectiveDate = Coercions.makeDateFrom(basedOnEffectiveDate)
                }
                period.Policy.markIssued(model.Policy.IssueDate ?: period.Policy.OriginalEffectiveDate)
              }
            } catch (e : Exception) {
              PCLoggerCategory.API.error(e.getMessage(), e)
              throw new ConversionException(e)
            }
          })
          if (renewal == null) {
            throw new DataConversionException(DisplayKey.get("Job.Renewal.Error.ConversionOnRenewalFailure", policyNumber))
          }
          var policyPeriod = renewal.LatestPeriod

          //this makes the renewal period - not basedon - enter as draft and not new. assigns UWCompany.
          //different behavior from startNewRenewal.
          policyPeriod.RenewalProcess.beginEditing()

          for (territoryCode in policyPeriod.PrimaryLocation.TerritoryCodes) {
            if (territoryCode.Code == null) {
              territoryCode.fillWithFirst()
            }
          }
        } catch (e : Exception) {
          PCLoggerCategory.API.error(e.getMessage(), e)
          throw new ConversionException(e)
        }
      })
    })
    return renewal?.JobNumber
  }

  private function startRenewal(accountNumber : String,
                                producerCodeId : String,
                                policyPeriodData : String,
                                createConversionRenewal(account : Account,
                                                        product : Product,
                                                        producerCode : ProducerCode)) {
    // Check for NULL values
    SOAPUtil.require(accountNumber, "accountNumber")
    SOAPUtil.require(producerCodeId, "producerCodeId")
    // Check for invalid identifiers
    var policyPeriodModel = tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod.parse(policyPeriodData)
    var account = new AccountScenariosHandler(policyPeriodModel.Policy.Account, policyPeriodModel.PolicyNumber).findAccount()
    if (account == null) {
      throw new BadIdentifierException(DisplayKey.get("PolicyRenewalAPI.Error.AccountNotFound", accountNumber))
    }
    var product = gw.api.productmodel.ProductLookup.getByCodeIdentifier(policyPeriodModel.Policy.ProductCode)
    if (product == null) {
      throw new BadIdentifierException(DisplayKey.get("PolicyRenewalAPI.Error.ProductNotFound", policyPeriodModel.Policy.ProductCode))
    }
    var producerCode = gw.api.database.Query.make(ProducerCode).compare(ProducerCode#Code, Equals, producerCodeId).select().AtMostOneRow
    if (producerCode == null) {
      producerCode = gw.api.database.Query.make(ProducerCode).compare(ProducerCode#Code, Equals, producerCodeId).select().AtMostOneRow
    }
    if (producerCode == null) {
      throw new BadIdentifierException(DisplayKey.get("PolicyRenewalAPI.Error.ProducerCodeNotFound", producerCodeId))
    }
    createConversionRenewal(account, product, producerCode)
  }

}