package tdic.pc.conversion.services

uses gw.api.database.Query
uses gw.transaction.Transaction
uses gw.webservice.SOAPUtil
uses gw.webservice.pc.pc900.account.AccountAPI
uses gw.xml.ws.annotation.WsiWebMethod
uses gw.xml.ws.annotation.WsiWebService

/**
 * External MEM Account API for performing various operations on accounts within PolicyCenter.
 */
@Export
@WsiWebService("http://guidewire.com/pc/ws/com/mem/pc/common/services/account/AccountAPI")
class TDICAccountAPI extends AccountAPI {

  @WsiWebMethod(true)
  function addAccount(externalAccount : tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_Policy_Account) : String {
    SOAPUtil.require(externalAccount, "externalAccount")
    SOAPUtil.require(externalAccount.AccountHolderContact, "accountHolderContact")
    var account : Account
    Transaction.runWithNewBundle(\bundle -> {
      account = externalAccount.createAccount(bundle)
      account.AccountStatus = AccountStatus.TC_ACTIVE
      account.PreferredSettlementCurrency = Currency.TC_USD
      account.AccountOrgType = externalAccount.AccountOrgType
      //addContactToAccount(accountContacts, account)
      account.PrimaryLocation.LocationName = account.AccountHolderContact.DisplayName
      account.IndustryCode = Query.make(IndustryCode).compare(IndustryCode#Code, Equals, externalAccount.IndustryCode.Code).select().AtMostOneRow
    })
    return account.AccountNumber
  }


}