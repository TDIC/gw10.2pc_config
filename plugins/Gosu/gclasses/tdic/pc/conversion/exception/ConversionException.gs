package tdic.pc.conversion.exception

uses tdic.pc.conversion.util.ConversionUtility

class ConversionException extends Exception {


  construct(message : String) {
    super(message)
  }

  construct(message : String, args : Map) {
    super(ConversionUtility.prepareErrorMsg(message, args))
  }

  construct(exception : Exception) {
    super(exception)
  }

}