package tdic.pc.conversion.dto

class PolicyLocationDTO {

  var _locationNum : Integer as LocationNum
  var _territoryCode : String as TerritoryCode
  var _componentExt : String as Component_Ext
  var _offeringLOB : String as Offering_LOB
  var _addressID : String as AddressID
  var _policyID : String as PolicyID
  var _publicID : String as PublicID
  var _originalEffDate : String as OriginalEffectiveDate
  var _addrcntl : String as AddrCntl

}