package tdic.pc.conversion.dto

class PolicyContactDTO {

  var publicID : String as PublicID
  var offeringLOB : String as Offering_LOB
  var subtype : String as Subtype
  var frmTyp : String as FrmTyp
  var contactID : String as ContactID
  var policyID : String as PolicyID
  var loanNumber : String as LoanNumber
  var description : String as Description

}