package tdic.pc.conversion.dto

uses java.math.BigDecimal

class PolicyConversionDTO {

  private var _accountNumber : String as AccountNumber
  private var _policyNumber : String as PolicyNumber
  private var _offeringLOB : String as Offering_LOB
  private var _splitCounter : Integer as Counter
  private var _baseState : String as BaseState
  private var _termNumber : String as TermNumber
  private var _industryCode : String as IndustryCode
  private var _batchID : BigDecimal as BatchID
  private var _totalCostRpt : BigDecimal as TotalCostRpt

  construct() {

  }

  construct(accNumber : String, policyNumber : String, offeringLOB : String, counter : Integer, baseState : String,
            termNumber : String, industryCode : String, batchID : BigDecimal) {
    _accountNumber = accNumber
    _policyNumber = policyNumber
    _offeringLOB = offeringLOB
    _splitCounter = counter
    _baseState = baseState
    _termNumber = termNumber
    _industryCode = industryCode
    _batchID = batchID
  }

}