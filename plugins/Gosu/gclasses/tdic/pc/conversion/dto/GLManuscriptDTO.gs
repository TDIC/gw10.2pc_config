package tdic.pc.conversion.dto

class GLManuscriptDTO {

  var addInsured : String as AdditionalInsured
  var certificateHolder : String as CertificateHolder
  var manuscriptType : String as Manuscript_Type
  var manuscriptDes : String as Manuscript_Des

}