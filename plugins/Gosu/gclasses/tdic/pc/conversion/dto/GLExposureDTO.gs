package tdic.pc.conversion.dto

class GLExposureDTO {

  var specialityCode : String as SpecialityCode_TDIC
  var classCode : String as ClassCode_TDIC
  var glTerritory : String as GLTerritory_TDIC
  var policyID : String as PolicyID
  var publicID : String as PublicID

}