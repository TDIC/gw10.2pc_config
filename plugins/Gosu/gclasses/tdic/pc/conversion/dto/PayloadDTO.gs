package tdic.pc.conversion.dto

uses java.math.BigDecimal

class PayloadDTO {

  private var _policyNumber : String as PolicyNumber
  private var _gwPolicyNumber : String as GWPolicyNumber
  private var _acctKey : String as AccountKey
  private var _payloadXML : String as PayloadXML
  private var _payloadXMLAsString : String as PayloadXMLAsString
  private var _batchType : String as BatchType
  private var _batchID : BigDecimal as BatchID
  private var _offeringLOB : String as OfferingLOB
  private var _splitCounter : Integer as Counter

  construct() {
  }

  construct(batchID : long, accountKey : String, payloadXML : String, batchType : String) {
    _batchID = batchID
    _acctKey = accountKey
    _payloadXML = payloadXML
    _batchType = batchType
  }

  construct(batchID : long, accountKey : String, payloadXML : String, policyNumber : String,  gwPolicyNumber : String,
            batchType : String, offeringLOB : String, splitCounter : Integer) {
    _batchID = batchID
    _acctKey = accountKey
    _payloadXMLAsString = payloadXML
    _policyNumber = policyNumber
    _gwPolicyNumber = gwPolicyNumber
    _batchType = batchType
    _offeringLOB = offeringLOB
    _splitCounter = splitCounter
  }

}