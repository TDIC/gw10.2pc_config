package tdic.pc.conversion.dto

class GLAddlInsuredDTO {

  var ltEffDate : String as LTEffectiveDate
  var ltExpDate : String as LTExpirationDate
  var additionalInsuredID : String as AdditionalInsured

}