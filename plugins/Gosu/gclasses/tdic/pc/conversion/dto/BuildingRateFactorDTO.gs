package tdic.pc.conversion.dto

uses java.math.BigDecimal

class BuildingRateFactorDTO {

  var assessment : BigDecimal as Assessment
  var modifierID : String as ModifierID
  var patternCode : String as PatternCode
  var publicid : String as PublicID

}