package tdic.pc.conversion.dto

uses java.math.BigDecimal

class GLRateFactorDTO {

  var assessment : BigDecimal as Assessment
  var policyID : String as PolicyID
  var patternCode : String as PatternCode
  var publicid : String as PublicID

}