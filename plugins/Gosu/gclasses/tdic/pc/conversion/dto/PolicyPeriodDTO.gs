package tdic.pc.conversion.dto

uses tdic.pc.conversion.util.ConversionUtility

class PolicyPeriodDTO {

  var _policyNumber : String as PolicyNumber
  var _gwPolicyNumber : String as GWPolicyNumber
  var _accountID : String as AccountId
  var _periodStart : String as PeriodStart
  var _periodEnd : String as PeriodEnd
  var _baseState : String as BaseState
  var _pniContactDenorm : String as PNIContactDenorm
  var _pniName : String as PNIName
  var _as400Ptype : String as AS400_Ptype
  var _sourceSystemCd : String as SourceSystemCd
  var _offeringLOB : String as OFFERING_LOB
  var _producerCode : String as ProducerCode
  var _uwCompany : String as UWCompany
  var _paymentPlanName : String as PaymentPlanName
  var _termType : String as TermType
  var _policyPeriodID : Integer as PolicyPeriodID
  var _termNumber : Integer as TermNumber
  var _writtenDate : String as WrittenDate
  var _industryCode : String as IndustryCode
  var _multiLineDiscount_TDIC : String as MultiLineDiscount_TDIC
  var _legacyPolicySuffix_TDIC : String as LegacyPolicySuffix_TDIC
  var _legacyPolicyNumber_TDIC : String as LegacyPolicyNumber_TDIC
  var _legacyPolicyIdentifier_TDIC : String as LegacyPolicyIdentifier_TDIC
  var _splitCounter : Integer as Counter
  var _originalEffectiveDate : String as OriginalEffectiveDate
  var _publicID : String as PublicID
  var _policyOrgType : String as PolicyOrgType_TDIC
  var _paymentPlan : String as PaymentPlan
  var _abaRoutingNumber : String as ABARoutingNumber
  var _bankAccountNumber : String as BankAccountNumber

  property get PNIName() : String {
    return ConversionUtility.formatName(this._pniName)
  }

}