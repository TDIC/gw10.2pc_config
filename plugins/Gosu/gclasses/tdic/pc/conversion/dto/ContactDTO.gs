package tdic.pc.conversion.dto

uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.util.ConversionUtility

class ContactDTO {

  private static var participatingStates = {State.TC_CA, State.TC_IL, State.TC_NJ}
  var _name : String as Name
  var _firstName : String as FirstName
  var _lastName : String as LastName
  var _subType : String as SubType
  var _dob : String as DateOfBirth
  var _maritalStatus : String as MaritalStatus
  var _primaryPhone : String as PrimaryPhone
  var _homePhone : String as HomePhone
  var _workPhone : String as WorkPhone
  var _cellPhone : String as CellPhone
  var _faxPhone : String as FaxPhone
  var _emailAddr : String as EmailAddress1
  var _taxID : String as TaxID
  var _ssn : String as SSNbr
  var _primaryAddressID : String as PrimaryAddressID
  var _component : String as Component_TDIC
  var _licenseNumber : String as LicenseNumber
  var _licenseState : String as LicenseState
  var _credential : String as Credential_TDIC
  var _adaNumberOfficialID : String as ADANumberOfficialID_TDIC
  var _publicID : String as PublicID

  property get Name() : String {
    return ConversionUtility.formatName(this._name)
  }

  private property get ADANumberOfficialID_TDIC() : String {
    return this._adaNumberOfficialID == ConversionConstants.DEFAULT_ADA ? null : this._adaNumberOfficialID
  }

  property get LicenseNumber() : String {
    return this._licenseNumber == null ? null : this._licenseNumber.matches("[0]+") ? null : this._licenseNumber
  }

  function getADANumberOfficialID_TDIC(state : State) : String {
    if(participatingStates.contains(state)){
      if(null == this._adaNumberOfficialID){
        return ConversionConstants.DEFAULT_ADA
      }
    }
    return this._adaNumberOfficialID
  }

}