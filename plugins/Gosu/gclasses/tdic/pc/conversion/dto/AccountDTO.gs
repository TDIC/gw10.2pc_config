package tdic.pc.conversion.dto

class AccountDTO {

  var _accountOrgType : String as AccountOrgType
  var _publicID : String as PublicID
  var _yearBusinessStarted : String as YearBusinessStarted

}