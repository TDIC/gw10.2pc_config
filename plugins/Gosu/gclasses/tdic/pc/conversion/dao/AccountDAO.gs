package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.AccountDTO
uses tdic.pc.conversion.query.AccountConversionBatchQueries

uses java.sql.Connection

class AccountDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<AccountDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<AccountDTO> {
    _logger.info("Inside AccountDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(AccountDTO)
    var rows = runQuery(AccountConversionBatchQueries.ACCOUNT_DETAILS_SELECT_QUERY, _params, handler, _logger) as ArrayList<AccountDTO>

    return rows
  }

  override function update(aTransientObject : AccountDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : AccountDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<AccountDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : AccountDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<AccountDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<AccountDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<AccountDTO>) {
  }

}