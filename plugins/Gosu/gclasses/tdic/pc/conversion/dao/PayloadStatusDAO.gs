package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.PayloadDTO
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.query.PolicyPayloadBatchQueries

uses java.math.BigDecimal
uses java.sql.Connection

class PayloadStatusDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<PayloadDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"
  private static var CREATED_BY : String

  var _batchType : String

  var _params : Object[]

  construct(batchType : String, params : Object[]) {
    _batchType = batchType
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : List<PayloadDTO> {
   return null
  }

  override function update(aTransientObject : PayloadDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      //var params = new Object[2]
      var batchParams = new ArrayList<Object[]>()
      var params = new ArrayList<Object>()
      params.add(aTransientObject?.BatchType)
      params.add(aTransientObject?.BatchID)
      params.add(aTransientObject?.AccountKey)
      batchParams.add(params.toTypedArray())
      updatedRows = runUpdate(connection, PolicyPayloadBatchQueries.UPDATE_DSM_GW_LOAD_STATUS_QUERY, params.toTypedArray(), _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(payload : PayloadDTO) : int {
    var connection : Connection
    var insertedRow : BigDecimal = 0
    try {
      connection = getConnection(_logger)
      var batchParams = new ArrayList<Object[]>()
      var dateTime = CommonConversionHelper.getCurrentDateTime()
      var params = new ArrayList<Object>()
      var batchID : String = payload.BatchID as String
      params.add(payload.AccountKey)
      params.add(payload.BatchType)//params.add("AP")
      params.add(payload.PolicyNumber)
      params.add(batchID)
      params.add(payload.OfferingLOB)
      params.add(payload.Counter)
      batchParams.add(params.toTypedArray())
      insertedRow = runUpdate(connection, PolicyPayloadBatchQueries.INSERT_DSM_GW_LOAD_STATUS_QUERY, params.toTypedArray(), _logger)
      connection.commit()
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
    return insertedRow as int
  }

  override function persistAll(payloads : List<PayloadDTO>) : int[] {
    return null
  }

  property get CreatedBy() : String {
    if (CREATED_BY == null) {
      var connection = getConnection(_logger)
      var rs = connection.createStatement().executeQuery("select suser_sname()")
      if (rs.next()) {
        CREATED_BY = rs.getString(1)
      }
    }
    return CREATED_BY
  }

  override function delete(aTransientObject : PayloadDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<PayloadDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<PayloadDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<PayloadDTO>) {
  }

}