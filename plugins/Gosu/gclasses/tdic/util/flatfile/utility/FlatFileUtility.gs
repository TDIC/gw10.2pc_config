package tdic.util.flatfile.utility

uses gw.pl.logging.LoggerCategory
uses tdic.util.flatfile.excel.ExcelDefinitionReader
uses tdic.util.flatfile.model.gx.flatfilemodel.FlatFile
uses tdic.util.flatfile.utility.encoder.FileEncoder
uses tdic.util.flatfile.utility.encoder.FlatFileEncoder
uses tdic.util.flatfile.utility.parser.FileParser
uses tdic.util.flatfile.utility.parser.FlatFileParser
uses java.io.File
uses java.io.FileNotFoundException
uses java.lang.IllegalArgumentException
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * US688
 * 09/03/2014 shanem
 *
 * Handles interactions with Flat Files
 */
class FlatFileUtility {
  private var _logger = LoggerFactory.getLogger("FLAT_FILE_UTILITY")
  /**
   * US688
   * 09/03/2014 shanem
   *
   * Constructs a FileEncoder instance based on the Vendor Definition
   */
  @Param("aFile", "The gx model to encode")
  @Param("aVendorDefinition", "Vendor definition which will decide what type of file is to be parsed")
  @Returns("Instance of a FlatFileEncoder storing the encoded file as a String[]")
  function encode(anXMLFile: FlatFile, aVendorDefinitionPath: String): FileEncoder {
   _logger.trace("FlatFileUtility#encode(FlatFile, String) - Entering.")
    if (anXMLFile == null){
      _logger.debug("FlatFileUtility#Encode() - Argument is null")
      throw new IllegalArgumentException()
    }
    _logger.trace("FlatFileUtility#encode(FlatFile, String) - Exiting.")
    return new FlatFileEncoder(anXMLFile, readVendorSpec(aVendorDefinitionPath))
  }

  /**
   * US688
   * 09/03/2014 shanem
   *
   * Constructs a FileParser instance based on the Vendor Definition
   */
  @Param("aFile", "The gx model to encode")
  @Param("aVendorDefinition", "Vendor definition which will decide what type of file is to be parsed")
  @Returns("Instance of a FlatFileEncoder storing the encoded file as a String[]")
  function parse(aFilePath: String, aVendorDefinitionPath: String): FileParser {
    _logger.trace("FlatFileUtility#parse(String, String) - Entering.")
    if (aFilePath == null) {
      _logger.error("FlatFileUtility#parse(String, String) - Null File Path Argument received.")
      throw new IllegalArgumentException()
    } else {
      _logger.trace("FlatFileUtility#parse(String, String) - Exiting.")
      return new FlatFileParser(aFilePath, readVendorSpec(aVendorDefinitionPath))
    }
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * Reads the contents of a XLSX or XML file and constructs a FlatFile XML object
   */
  @Param("aVendorDefinitionPath", "The path to the vendor definition file")
  @Returns("A FlatFile Representation of the vendor definition xml")
  function readVendorSpec(aVendorDefinitionPath: String): FlatFile {
    _logger.trace("FlatFileUtility#readVendorSpec() - Entering.")
    if (aVendorDefinitionPath == null) {
      _logger.error("FlatFileUtility#readVendorSpec() - Null Vendor Definition Path Argument received.")
      throw new IllegalArgumentException()
    }
    var vendorDef = new File(aVendorDefinitionPath)
    if (!vendorDef.exists() || vendorDef.Directory){
      _logger.error("FlatFileUtility#readVendorSpec() - Vendor Definition file not found.")
      throw new FileNotFoundException()
    }
    var xml: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile
    if (vendorDef.Extension == "xlsx") {
      _logger.debug("FlatFileUtility#readVendorSpec() - Excel file.")
      var reader = new ExcelDefinitionReader(".\\modules\\configuration\\gsrc\\tdic\\util\\flatfile\\excelFileHeaders")
      var records = reader.read(vendorDef.Path)?.toTypedArray()
      var flatFile = new tdic.util.flatfile.model.classes.FlatFile(records)
      xml = new tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(flatFile)
    } else if (vendorDef.Extension == "xml") {
      _logger.debug("FlatFileUtility#readVendorSpec() - XML file.")
      var file = new File(aVendorDefinitionPath)
      return tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
    } else {
      _logger.error("FlatFileUtility#readVendorSpec() - Unsupported file.")
      throw new IllegalArgumentException()
    }
    _logger.trace("FlatFileUtility#readVendorSpec() - Exiting.")
    return xml
  }
}