package tdic.util.flatfile.model.gx.flatfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement FlatFileEnhancement : tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
  public static function create(object : tdic.util.flatfile.model.classes.FlatFile) : tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
    return new tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(object)
  }

  public static function create(object : tdic.util.flatfile.model.classes.FlatFile, options : gw.api.gx.GXOptions) : tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
    return new tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(object, options)
  }

}