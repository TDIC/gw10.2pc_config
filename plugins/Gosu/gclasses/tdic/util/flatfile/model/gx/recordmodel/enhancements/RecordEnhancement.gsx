package tdic.util.flatfile.model.gx.recordmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RecordEnhancement : tdic.util.flatfile.model.gx.recordmodel.Record {
  public static function create(object : tdic.util.flatfile.model.classes.Record) : tdic.util.flatfile.model.gx.recordmodel.Record {
    return new tdic.util.flatfile.model.gx.recordmodel.Record(object)
  }

  public static function create(object : tdic.util.flatfile.model.classes.Record, options : gw.api.gx.GXOptions) : tdic.util.flatfile.model.gx.recordmodel.Record {
    return new tdic.util.flatfile.model.gx.recordmodel.Record(object, options)
  }

}