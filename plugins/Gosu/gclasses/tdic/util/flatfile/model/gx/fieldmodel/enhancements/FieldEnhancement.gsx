package tdic.util.flatfile.model.gx.fieldmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement FieldEnhancement : tdic.util.flatfile.model.gx.fieldmodel.Field {
  public static function create(object : tdic.util.flatfile.model.classes.Field) : tdic.util.flatfile.model.gx.fieldmodel.Field {
    return new tdic.util.flatfile.model.gx.fieldmodel.Field(object)
  }

  public static function create(object : tdic.util.flatfile.model.classes.Field, options : gw.api.gx.GXOptions) : tdic.util.flatfile.model.gx.fieldmodel.Field {
    return new tdic.util.flatfile.model.gx.fieldmodel.Field(object, options)
  }

}