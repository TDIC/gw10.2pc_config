package tdic.util.gunit.test

uses gw.testharness.TestBase
uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses java.lang.Throwable

/**
 * When this test is run in the Guidewire Studio integrated GUnit test runner, only the "testNothing" runs.
 * The more interesting test is to run it with the TextTestRunner -- which will fail if it attempts to run
 * any of the other test methods in this class, as they all unconditionally (call) fail.
 */
@ServerTest
@RunLevel(Runlevel.NONE)
class TestMethodsThatShouldNotBeCalledTest extends TestBase {

  /**
   * This test method is required, due to the rule that all test classes must contain at least one runnable test method.
   */
  function testNothing() {
    // No test logic here.
  }



}
