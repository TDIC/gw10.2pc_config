package tdic.util.gunit.test.stubs

uses gw.testharness.TestBase
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

//@ServerTest
@RunLevel(Runlevel.NONE)
class DoesNotHaveServerTestAnnotationTest extends TestBase {

  function testNothing() {
    // GUnit runs this test in spite of the lack of '@ServerTest' annotation. See TestRunnerTest.
  }

}
