package tdic.util.gunit

uses gw.api.system.server.Runlevel

class RunlevelWrapper implements IRunlevelAccessor {

  override function getCurrentServerRunlevel() : Runlevel {
    return Runlevel.getCurrent()
  }

}
