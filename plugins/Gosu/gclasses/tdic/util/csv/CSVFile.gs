package tdic.util.csv

uses java.io.IOException
uses java.io.BufferedWriter
uses java.io.FileWriter
uses java.io.File
uses org.apache.commons.csv.CSVFormat
uses org.apache.commons.csv.CSVParser
uses org.apache.commons.csv.CSVPrinter
uses org.apache.commons.csv.CSVRecord
uses java.util.List

/**
 * US561: Policy Feed to Pivotal/AS400
 * 11/20/2014 Vicente
 *
 * Class to generate a CSV file.
 *
 */
class CSVFile {

  private var cvsFile : CSVPrinter
  private var file : File

  /**
   * Write the input data in the next column
   */
  @Param("data", "Data to write in the next column")
  public function writeRow(record: List<String>){
    try {
      if(record != null){
        cvsFile.printRecord(record)
      }
    } catch(e: IOException) {
      throw new IOException("Error writing CVS file with " + record + ": "+ e.Message)
    }
  }

  /**
   * CVS file constructor
   */
  @Param("fileName", "Name of the output CSV file")
  construct (fileName : String){
    try {
      file = new File(fileName)
      cvsFile = new CSVPrinter(new BufferedWriter(new FileWriter(file, false)), CSVFormat.DEFAULT.withDelimiter(','))
    } catch(e: IOException) {
      if(cvsFile != null){
        cvsFile.close()
      }
      throw new IOException("Error creating CVS file: " + e.Message)
    }
  }

  /**
   * Close the CVS file
   */
  public function close(){
    try {
      cvsFile.close()
    } catch(e: IOException) {
      throw new IOException("Error closing CVS file: " + e.Message)
    }
  }

  /**
   * Delete the CVS file
   */
  public function delete(){
    try {
      if(cvsFile != null){
        cvsFile.close()
      }

      if (file!= null and file.exists()) {
        file.delete();
      }

    } catch(e: IOException) {
      throw new IOException("Error removing CVS file: " + e.Message)
    }
  }
}