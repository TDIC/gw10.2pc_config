package acc.onbase.enhancement

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */
enhancement OnBaseJobEnhancement : Job {

  property get SubmissionNumber() : String {
    if (this.Subtype == typekey.Job.TC_SUBMISSION) {
      return this.JobNumber
    }
    return null
  }
}
