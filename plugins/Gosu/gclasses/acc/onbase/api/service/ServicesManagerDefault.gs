package acc.onbase.api.service

uses acc.onbase.api.service.interfaces.ArchiveDocumentAsyncInterface
uses acc.onbase.api.service.interfaces.ArchiveDocumentSyncInterface
uses acc.onbase.api.service.interfaces.DocumentContentInterface
uses acc.onbase.api.service.interfaces.ServicesManagerInterface
uses acc.onbase.api.service.interfaces.SubmitMessageInterface
uses acc.onbase.api.service.interfaces.SubmitMessageWithResponseInterface

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */
class ServicesManagerDefault implements ServicesManagerInterface {
  /** Async archive document service. */
  public property get ArchiveDocumentAsync(): ArchiveDocumentAsyncInterface {
    return new acc.onbase.api.service.implementations.wsp.ArchiveDocumentAsyncWSP()
  }
  /** Sync archive document service. */
  public property get ArchiveDocumentSync(): ArchiveDocumentSyncInterface {
    return new acc.onbase.api.service.implementations.wsp.ArchiveDocumentSyncWSP()
  }
  /** Get document content service. */
  public property get DocumentContent() : DocumentContentInterface {
    return new acc.onbase.api.service.implementations.direct.GetDocumentContentEIS()
  }
  /** Submit message service. */
  public property get SubmitMessage() : SubmitMessageInterface{
    return new acc.onbase.api.service.implementations.wsp.SubmitMessageWSP()
  }
  /** Submit message with response service. */
  public property get SubmitMessageWithResponse() : SubmitMessageWithResponseInterface{
    return new acc.onbase.api.service.implementations.wsp.SubmitMessageWithResponseWSP()
  }
}
