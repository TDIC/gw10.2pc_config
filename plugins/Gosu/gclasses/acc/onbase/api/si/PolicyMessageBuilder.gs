package acc.onbase.api.si

uses acc.onbase.api.si.model.accountmodel.Account
uses acc.onbase.api.si.model.policymodel.Policy
uses acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Utility functions to build the message with necessary data to send to OnBase
 */
public class PolicyMessageBuilder extends MessageBuilder {

  private var _sync : List<String>as Sync = new ArrayList<String>()
  private var _async : List<String>as Async = new ArrayList<String>()

  /**
   * Create a new builder for a specific message type
   *
   * @param messageType type of message, typically an event name
   */
  construct(messageType : String) {
    super(messageType)
  }

  /**
   * Add the corresponding model of the entity, the document is linked to, to the message builder.
   *
   * @param document The input document.
   */

  public function addEntityModelToMessage(document : Document) {
    if (document.PolicyPeriod != null) {
      var policyPeriodModel = new PolicyPeriod(document.PolicyPeriod)
      addMessagePart("policyperiod", policyPeriodModel)
    } else if (document.Policy != null) {
      var policyModel = new Policy(document.Policy)
      addMessagePart("policy", policyModel)
    } else if (document.Account != null) {
      var accountModel = new Account(document.Account)
      addMessagePart("account", accountModel)
    }
  }

  /**
   * Utility function to get DocUID and PendingDocUID for Sync and Async Documents.
   *
   * @param documents List of documents.
   */
  public function addDocumentIds(documents : Iterable<Document>) {
    for (d in documents) {
      if (d.PendingDocUID != null) {
        _async.add(d.PendingDocUID)
      } else if (d.DocUID != null) {
        _sync.add(d.DocUID)
      }
    }
  }


  /**
   * Build a Dcoument Message to be sent to Onbase during Document Archival
   *
   * @param document The document whose keywords have to be sent to OnBase
   * @return builder which contains the Document and Claim Model added to the Message
   */
  public static function buildArchiveMessage(document : Document) : MessageBuilder {
    var documentModel = new acc.onbase.api.si.model.documentmodel.Document(document)
    var builder = new PolicyMessageBuilder("ArchiveDocumentPC")
    builder.addMessagePart('document', documentModel)
    builder.addEntityModelToMessage(document)
    return builder
  }
}