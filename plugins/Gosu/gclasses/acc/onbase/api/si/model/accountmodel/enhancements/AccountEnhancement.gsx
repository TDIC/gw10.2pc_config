package acc.onbase.api.si.model.accountmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : acc.onbase.api.si.model.accountmodel.Account {
  public static function create(object : entity.Account) : acc.onbase.api.si.model.accountmodel.Account {
    return new acc.onbase.api.si.model.accountmodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.accountmodel.Account {
    return new acc.onbase.api.si.model.accountmodel.Account(object, options)
  }

}