package acc.onbase.api.si.model.policymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyEnhancement : acc.onbase.api.si.model.policymodel.Policy {
  public static function create(object : entity.Policy) : acc.onbase.api.si.model.policymodel.Policy {
    return new acc.onbase.api.si.model.policymodel.Policy(object)
  }

  public static function create(object : entity.Policy, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.policymodel.Policy {
    return new acc.onbase.api.si.model.policymodel.Policy(object, options)
  }

}