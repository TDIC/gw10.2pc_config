package acc.onbase.api.si.model.policyperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodEnhancement : acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod {
  public static function create(object : entity.PolicyPeriod) : acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod {
    return new acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod(object)
  }

  public static function create(object : entity.PolicyPeriod, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod {
    return new acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod(object, options)
  }

}