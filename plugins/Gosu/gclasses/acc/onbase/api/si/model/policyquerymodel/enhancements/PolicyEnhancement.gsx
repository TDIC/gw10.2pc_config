package acc.onbase.api.si.model.policyquerymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyEnhancement : acc.onbase.api.si.model.policyquerymodel.Policy {
  public static function create(object : entity.Policy) : acc.onbase.api.si.model.policyquerymodel.Policy {
    return new acc.onbase.api.si.model.policyquerymodel.Policy(object)
  }

  public static function create(object : entity.Policy, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.policyquerymodel.Policy {
    return new acc.onbase.api.si.model.policyquerymodel.Policy(object, options)
  }

}