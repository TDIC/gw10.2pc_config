package acc.onbase.api.security

uses acc.onbase.api.exception.InvalidContentException
uses acc.onbase.configuration.OnBaseConfigurationFactory

uses javax.crypto.Mac
uses javax.crypto.spec.SecretKeySpec
uses java.lang.*

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * 11/01/2016 - Daniel Q. Yu
 * * Refactored from Claim Center SecurityManager.gs
 * <p>
 * 12/01/2016 - Daniel Q. Yu
 * * Added security update for a document.
 * <p>
 * 02/08/2017 - Daniel Q. Yu
 * * Fixed null pointer exception when document is uploaded for a submission without producer code.
 * <p>
 * 02/17/2017 - Daniel Q. Yu
 * * Added Check Sum Method.
 * <p>
 * 03/17/2017 - Daniel Q. Yu
 * * Added check for no documents found for account or policy.
 */

/**
 * Policy Center 8 security manager.
 */
class SecurityManager {
  /**
   * The default max keyword value length
   */
  public static final var KEYWORD_MAX_LENGTH: int = 250

  /**
   * Get security producer code for a document.
   *
   * @param document The input document.
   * @return A list of strings of security producer codes with max length of KEYWORD_MAX_LENGTH
   */
  public static function getDocumentSecurityProducerCodes(document: Document): List<String> {
    return getDocumentSecurityProducerCodes(document, KEYWORD_MAX_LENGTH)
  }

  /**
   * Get security producer code for a document.
   *
   * @param document  The input document.
   * @param maxLength The max length of each string.
   * @return A list of strings of security producer codes with max length of maxLength
   */
  public static function getDocumentSecurityProducerCodes(document: Document, maxLength: int): List<String> {
    var roles = new ArrayList<String>()
    if (document.PolicyPeriod != null) {
      roles.add(document.PolicyPeriod.ProducerCodeOfRecord.toString())
      if (document.PolicyPeriod.EffectiveDatedFields.ProducerCode != document.PolicyPeriod.ProducerCodeOfRecord) {
        roles.add(document.PolicyPeriod.EffectiveDatedFields.ProducerCode.toString())
      }
    } else if (document.Policy != null) {
      var pp = document.Policy.MostRecentBoundPeriodOnMostRecentTerm
      if (pp != null && pp.ProducerCodeOfRecord != null) {
        roles.add(pp.ProducerCodeOfRecord.toString())
        if (pp.EffectiveDatedFields.ProducerCode != pp.ProducerCodeOfRecord) {
          roles.add(pp.EffectiveDatedFields.ProducerCode.toString())
        }
      }
    } else if (document.Account != null) {
      foreach(code in document.Account.ProducerCodes) {
        roles.add(code.ProducerCode.toString())
      }
    }

    var roleList = new ArrayList<String>()
    var buf = new StringBuilder()
    foreach(role in roles) {
      if (role.length() > maxLength) {
        throw new InvalidContentException("Keyword value exceeds maximum length" + maxLength + " - " + role)
      } else if (role.length() == maxLength) {
        roleList.add(role)
      } else if (buf.length() == 0) {
        buf.append(role)
      } else {
        if (buf.length() + role.length() > maxLength) {
          roleList.add(buf.toString())
          buf = new StringBuilder(role)
        } else {
          buf.append(",").append(role)
        }
      }
    }
    roleList.add(buf.toString())
    return roleList
  }

  /**
   * Compute DocID/webURL hash for docpop URL checksum.
   * @param checksumValue either the DocID or anything after the '?' in the WebURL
   *
   * @return The hash hex string value.
   */

  @java.lang.Deprecated
  public static function computeDocIdCheckSum(docId: String): String{
    return computeCheckSum(docId)
  }

  public static function computeCheckSum(checksumValue: String): String {
    // compute hashHex hashBytes.
    var key = new SecretKeySpec(OnBaseConfigurationFactory.Instance.PopChecksumKey.getBytes("ASCII"), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(key);
    var hashBytes = mac.doFinal(checksumValue.getBytes("ASCII"))

    // convert hashHex to hex string.
    var hashHex = new StringBuffer();
    foreach(b in hashBytes) {
      var hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hashHex.append('0');
      }
      hashHex.append(hex);
    }

    return hashHex.toString()
  }
}
