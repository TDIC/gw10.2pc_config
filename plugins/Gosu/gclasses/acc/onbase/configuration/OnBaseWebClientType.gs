package acc.onbase.configuration

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 *
 * Enum to select the OnBase web client type
 */
enum OnBaseWebClientType {
  HTML, ActiveX
}