package acc.onbase.configuration

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 *
 * Document link type enum
 */
enum DocumentLinkType {
  activityid, checkid, reserveid, ServiceRequest, claimid, exposureid
}