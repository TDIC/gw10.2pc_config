package acc.onbase.webservice

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */

/**
 * Guidewire web service returns account and policy metadata information.
 */
@WsiWebService("http://onbase/acc/policy/webservice/QueryPolicyAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class QueryPolicyAPI {
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)


  function getAccountQueryModel(accountNumber : String) : acc.onbase.api.si.model.accountquerymodel.Account {
    var account : Account = null
    if (accountNumber.NotBlank) {
      account = Account.finder.findAccountByAccountNumber(accountNumber)
      if (account == null) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
      }
    } else {
      //no account number entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccount"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccount"))
    }

    return new acc.onbase.api.si.model.accountquerymodel.Account(account)
  }

  function getPolicyQueryModel(policyNumber : String) : acc.onbase.api.si.model.policyquerymodel.Policy {
    var policy : Policy = null
    if (policyNumber.NotBlank) {
      policy = Policy.finder.findPolicyByPolicyNumber(policyNumber)
      if (policy == null) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", policyNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", policyNumber))
      }
    } else {
      //no policy number entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingPolicy"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingPolicy"))
    }

    return new acc.onbase.api.si.model.policyquerymodel.Policy(policy)
  }

}
