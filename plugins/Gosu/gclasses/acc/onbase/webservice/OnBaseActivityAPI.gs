package acc.onbase.webservice

uses acc.onbase.api.application.DocumentLinking
uses acc.onbase.configuration.DocumentLinkType
uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.assignment.AssignmentUtil
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService
uses entity.Activity

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */

@WsiWebService("http://onbase/acc/policy/webservice/OnBaseActivityAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class OnBaseActivityAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  function createNewActivity(activityPatternCode : String, accountNumber : String, policyNumber : String, documents : ArrayList<String>) : String {

    var activityPattern : ActivityPattern = null
    if (activityPatternCode.HasContent) {
      activityPattern = ActivityPattern.finder.getActivityPatternByCode(activityPatternCode)
      if (activityPattern == null) {
        //invalid activity pattern code entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activityPatternCode))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activityPatternCode))
      }
    } else {
      // no activity pattern code entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_NoActivityCode"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_NoActivityCode"))
    }

    var newActivity : Activity = null

    //Get Policy Number for the Activity
    var policy : Policy = null
    if (policyNumber.HasContent) {
      policy = Policy.finder.findPolicyByPolicyNumber(policyNumber)
      if (policy == null) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", policyNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", policyNumber))
      }
    }

    //Get Account Number for the Activity
    var account : Account = null
    if (accountNumber.HasContent) {
      account = Account.finder.findAccountByAccountNumber(accountNumber)
      if (account == null) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
      }
    }

    // Verify information
    if (account == null && policy == null) {
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccountOrPolicy"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccountOrPolicy"))
    } else if (account == null && policy != null) {
      //find the account linked to the policy
      account = policy.Account
    }
    //if both account and policy have content
    else if (account != null && policy != null) {
      if (policy.Account != account) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedAccountAndPolicy", accountNumber, policyNumber))
      }
    }

    // Get documents to be linked with this activity
    var docList = new ArrayList<Document>()
    //documents arraylist contain list of DocUIDs
    if (documents.Count > 0) {
      foreach (doc in documents) {
        var document = Query.make(Document).compare(Document#DocUID, Relop.Equals, doc).select().AtMostOneRow
        if (document != null) {
          if (document.Policy == policy && document.Account == account) {
            docList.add(document)
          } else {
            _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidDocUID", doc))
            throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidDocUID", doc))
          }
        } else {
          // docUID not found
          _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", doc))
          throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", doc))
        }
      }
    } else {
      // missing docUID
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }

    var activitySubject = DisplayKey.get("Accelerator.OnBase.WS.STR_GW_NewActivitySubject", documents)

    Transaction.runWithNewBundle(\bundle -> {
      // Create activity
      if (policy != null) {
        //create a policy activity which in turn would also create/link the activity under the account it is linked to
        newActivity = activityPattern.createPolicyActivity(bundle, policy, activitySubject, null, null, null, null, null, null)
      } else {
        //Create Account level Activity
        newActivity = activityPattern.createAccountActivity(bundle, activityPattern, account, activitySubject, null, null, null, null, null, null)
      }
      //Set default User Name and Group
      newActivity.assign(AssignmentUtil.DefaultUser.DefaultAssignmentGroup, AssignmentUtil.DefaultUser)
    } )

    //Link Documents to the newly created Activity
    new DocumentLinking().linkDocumentsToEntity(newActivity, docList.toArray(), DocumentLinkType.activityid)

    return newActivity.PublicID
  }
}