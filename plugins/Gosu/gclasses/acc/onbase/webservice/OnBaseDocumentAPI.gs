package acc.onbase.webservice

uses acc.onbase.api.si.PolicyMessageBuilder
uses acc.onbase.api.si.schema.onbasemessage.OnBaseMessage
uses acc.onbase.util.LoggerFactory
uses acc.onbase.webservice.util.DocumentModelParser
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * 2/23/2018 - Tori Brenneison
 * * Initial implementation
 */

/**
 * Guidewire web service
 */

@WsiWebService("http://onbase/acc/policy/webservice/OnBaseDocumentAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class OnBaseDocumentAPI {

  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  function newDocumentArchived(doc : acc.onbase.api.si.model.documentmodel.Document) : String {

    var parser = new DocumentModelParser(doc)
    var document : entity.Document = null
    parser.verifyDocumentIsLinked()
    Transaction.runWithNewBundle(\bundle -> {
      document = parser.getNewDocument()
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Imported from OnBase"
      document.Account = parser.Account
      document.Policy = parser.Policy
      document.Job = parser.Job
      document.Name = doc.Name
      document.Description = doc.Description
      document.Author = doc.Author
      document.Recipient = doc.Recipient
      document.Status = parser.Status
      document.Type = parser.Type
      document.Subtype = parser.Subtype
      document.MimeType = parser.MimeType
      document.DateCreated = Date.Now
      document.DateModified = Date.Now
      document.SecurityType = doc.SecurityType
      document.DMS = true

    } )

    return document.PublicID
  }

  function asyncDocumentArchived(doc : acc.onbase.api.si.model.documentmodel.Document) : OnBaseMessage {
    var parser = new DocumentModelParser(doc)
    var document = parser.getPendingDocument()

    // Because GW side should have most current metadata information,
    // all other keyword values will not be checked and should NOT be passed back.
    // If keyword values are modified in OnBase, use documentUpdated webservice to update GW side.

    Transaction.runWithNewBundle(\bundle -> {
      document = bundle.add(document)
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Async document archived"
      document.PendingDocUID = null
      document.DMS = true
      document.Status=DocumentStatusType.TC_FINAL

      if (doc.Type?.OnBaseName?.HasContent){
        document.Type= parser.Type
      }
      if (doc.Subtype?.OnBaseName?.HasContent){
        document.Subtype = parser.Subtype
      }

      if (doc.Name?.HasContent){
        document.Name = doc.Name
      }

    } )

    return PolicyMessageBuilder.buildArchiveMessage(document).Message
  }

  function documentUpdated(doc : acc.onbase.api.si.model.documentmodel.Document) : String {
    // All fields must be passed in (not only changed fields)

    var parser = new DocumentModelParser(doc)
    parser.verifyDocumentIsLinked()
    var document = parser.getExistingDocument()

    Transaction.runWithNewBundle(\bundle -> {
      document = bundle.add(document)
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Updated from OnBase"
      document.Account = parser.Account
      document.Policy = parser.Policy
      document.Job = parser.Job
      document.Name = doc.Name
      if (doc.Description?.HasContent){
        document.Description = doc.Description
      }
      document.Author = doc.Author
      document.Recipient = doc.Recipient
      document.Status = parser.Status
      document.Type = parser.Type
      document.Subtype = parser.Subtype
      if (doc.MimeType != "application/octet-stream") {
        document.MimeType = parser.MimeType
      }
      document.DateModified = Date.Now
      document.SecurityType = doc.SecurityType
      document.DMS = true

    } )

    return document.PublicID
  }

}