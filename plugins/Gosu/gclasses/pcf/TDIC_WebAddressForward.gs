package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_WebAddressForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_WebAddressForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (WebUrl :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_WebAddressForward, {WebUrl}, 0)
  }
  
  static function drilldown (WebUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_WebAddressForward, {WebUrl}, 0).drilldown()
  }
  
  static function printPage (WebUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_WebAddressForward, {WebUrl}, 0).printPage()
  }
  
  static function push (WebUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_WebAddressForward, {WebUrl}, 0).push()
  }
  
  
}