package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionSets_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class QuestionSets_TDICDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($questionSets :  gw.api.productmodel.QuestionSet[], $answerContainer :  AnswerContainer, $onChangeBlock :  block(), $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.QuestionSets_TDICDV, SECTION_WIDGET_CLASS).setVariables(false, {$questionSets, $answerContainer, $onChangeBlock, $openForEdit})
  }
  
  function refreshVariables ($questionSets :  gw.api.productmodel.QuestionSet[], $answerContainer :  AnswerContainer, $onChangeBlock :  block(), $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.QuestionSets_TDICDV, SECTION_WIDGET_CLASS).setVariables(true, {$questionSets, $answerContainer, $onChangeBlock, $openForEdit})
  }
  
  
}