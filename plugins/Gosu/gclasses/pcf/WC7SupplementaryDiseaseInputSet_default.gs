package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7SupplementaryDiseaseInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7SupplementaryDiseaseInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wcLine :  WC7WorkersCompLine, $selectedJurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7SupplementaryDiseaseInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$wcLine, $selectedJurisdiction})
  }
  
  function refreshVariables ($wcLine :  WC7WorkersCompLine, $selectedJurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7SupplementaryDiseaseInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$wcLine, $selectedJurisdiction})
  }
  
  
}