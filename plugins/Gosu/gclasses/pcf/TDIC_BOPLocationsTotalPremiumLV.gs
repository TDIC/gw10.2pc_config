package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationsTotalPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPLocationsTotalPremiumLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($totalLocationPremium :  gw.pl.currency.MonetaryAmount) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationsTotalPremiumLV, SECTION_WIDGET_CLASS).setVariables(false, {$totalLocationPremium})
  }
  
  function refreshVariables ($totalLocationPremium :  gw.pl.currency.MonetaryAmount) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationsTotalPremiumLV, SECTION_WIDGET_CLASS).setVariables(true, {$totalLocationPremium})
  }
  
  
}