package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GLSchoolCodeSearch_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class GLSchoolCodeSearch_TDICPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.GLSchoolCodeSearch_TDICPopup, {}, 1)
  }
  
  static function createDestination (term :  gw.api.domain.covterm.CovTerm) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.GLSchoolCodeSearch_TDICPopup, {term}, 0)
  }
  
  function pickValueAndCommit (value :  entity.GLSchoolCode_TDIC) : void {
    __widgetOf(this, pcf.GLSchoolCodeSearch_TDICPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.GLSchoolCodeSearch_TDICPopup, {}, 1).push()
  }
  
  static function push (term :  gw.api.domain.covterm.CovTerm) : pcf.api.Location {
    return __newDestinationForLocation(pcf.GLSchoolCodeSearch_TDICPopup, {term}, 0).push()
  }
  
  
}