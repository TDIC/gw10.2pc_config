package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7BlanketWaiverInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7BlanketWaiverInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wc7Line :  entity.WC7WorkersCompLine, $conditionPattern :  gw.api.productmodel.ClausePattern) : void {
    __widgetOf(this, pcf.WC7BlanketWaiverInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$wc7Line, $conditionPattern})
  }
  
  function refreshVariables ($wc7Line :  entity.WC7WorkersCompLine, $conditionPattern :  gw.api.productmodel.ClausePattern) : void {
    __widgetOf(this, pcf.WC7BlanketWaiverInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$wc7Line, $conditionPattern})
  }
  
  
}