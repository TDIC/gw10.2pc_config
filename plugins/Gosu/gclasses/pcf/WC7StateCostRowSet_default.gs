package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostRowSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7StateCostRowSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($costWrapper :  gw.api.ui.WC7CostWrapper) : void {
    __widgetOf(this, pcf.WC7StateCostRowSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$costWrapper})
  }
  
  function refreshVariables ($costWrapper :  gw.api.ui.WC7CostWrapper) : void {
    __widgetOf(this, pcf.WC7StateCostRowSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$costWrapper})
  }
  
  
}