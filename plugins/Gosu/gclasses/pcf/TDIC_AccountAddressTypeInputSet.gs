package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/TDIC_AccountAddressTypeInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_AccountAddressTypeInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($selectedAddress :  Address) : void {
    __widgetOf(this, pcf.TDIC_AccountAddressTypeInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$selectedAddress})
  }
  
  function refreshVariables ($selectedAddress :  Address) : void {
    __widgetOf(this, pcf.TDIC_AccountAddressTypeInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$selectedAddress})
  }
  
  
}