package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleCreditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ScheduleCreditPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (scheduleRate :  WC7Modifier, openForEdit :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7ScheduleCreditPopup, {scheduleRate, openForEdit}, 0)
  }
  
  static function push (scheduleRate :  WC7Modifier, openForEdit :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7ScheduleCreditPopup, {scheduleRate, openForEdit}, 0).push()
  }
  
  
}