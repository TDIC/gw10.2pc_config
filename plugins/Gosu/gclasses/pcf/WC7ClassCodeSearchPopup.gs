package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ClassCodeSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (polJurisdiction :  Jurisdiction, wcLine :  WC7WorkersCompLine) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {polJurisdiction, wcLine}, 3)
  }
  
  static function createDestination (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, classCodeType :  WC7ClassCodeType, programType :  WC7ClassCodeProgramType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, previousWC7ClassCode, classCodeType, programType}, 1)
  }
  
  static function createDestination (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, excludedClassCodeTypes :  java.util.List<WC7ClassCodeType>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, previousWC7ClassCode, excludedClassCodeTypes}, 0)
  }
  
  static function createDestination (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, classCodeType :  WC7ClassCodeType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, classCodeType}, 2)
  }
  
  function pickValueAndCommit (value :  WC7ClassCode) : void {
    __widgetOf(this, pcf.WC7ClassCodeSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (polJurisdiction :  Jurisdiction, wcLine :  WC7WorkersCompLine) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {polJurisdiction, wcLine}, 3).push()
  }
  
  static function push (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, classCodeType :  WC7ClassCodeType, programType :  WC7ClassCodeProgramType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, previousWC7ClassCode, classCodeType, programType}, 1).push()
  }
  
  static function push (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, excludedClassCodeTypes :  java.util.List<WC7ClassCodeType>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, previousWC7ClassCode, excludedClassCodeTypes}, 0).push()
  }
  
  static function push (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, classCodeType :  WC7ClassCodeType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7ClassCodeSearchPopup, {locationWM, wcLine, classCodeType}, 2).push()
  }
  
  
}