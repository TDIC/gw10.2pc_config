package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborContractorForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7NewLaborContractorForContactTypePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  WC7Line, contactType :  typekey.ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7NewLaborContractorForContactTypePopup, {line, contactType, clausePattern}, 0)
  }
  
  function pickValueAndCommit (value :  WC7LaborContactDetail) : void {
    __widgetOf(this, pcf.WC7NewLaborContractorForContactTypePopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  WC7Line, contactType :  typekey.ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7NewLaborContractorForContactTypePopup, {line, contactType, clausePattern}, 0).push()
  }
  
  
}