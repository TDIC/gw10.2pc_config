package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/forms/WC7FormClassCodeSelectionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7FormClassCodeSelectionLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($formPattern :  FormPattern) : void {
    __widgetOf(this, pcf.WC7FormClassCodeSelectionLV, SECTION_WIDGET_CLASS).setVariables(false, {$formPattern})
  }
  
  function refreshVariables ($formPattern :  FormPattern) : void {
    __widgetOf(this, pcf.WC7FormClassCodeSelectionLV, SECTION_WIDGET_CLASS).setVariables(true, {$formPattern})
  }
  
  
}