package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7PolicyLinePerStateConfigDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wcLine :  WC7WorkersCompLine, $jurisdiction :  WC7Jurisdiction, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7PolicyLinePerStateConfigDV, SECTION_WIDGET_CLASS).setVariables(false, {$wcLine, $jurisdiction, $openForEdit})
  }
  
  function refreshVariables ($wcLine :  WC7WorkersCompLine, $jurisdiction :  WC7Jurisdiction, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7PolicyLinePerStateConfigDV, SECTION_WIDGET_CLASS).setVariables(true, {$wcLine, $jurisdiction, $openForEdit})
  }
  
  
}