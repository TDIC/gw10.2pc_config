package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/assignment/AssignmentPopupScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignmentPopupScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $activities :  entity.Activity[]) : void {
    __widgetOf(this, pcf.AssignmentPopupScreen, SECTION_WIDGET_CLASS).setVariables(false, {$AssignmentPopup, $activities})
  }
  
  function refreshVariables ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $activities :  entity.Activity[]) : void {
    __widgetOf(this, pcf.AssignmentPopupScreen, SECTION_WIDGET_CLASS).setVariables(true, {$AssignmentPopup, $activities})
  }
  
  
}