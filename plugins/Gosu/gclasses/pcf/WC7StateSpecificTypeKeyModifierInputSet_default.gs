package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateSpecificTypeKeyModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7StateSpecificTypeKeyModifierInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($modifier :  entity.Modifier, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.WC7StateSpecificTypeKeyModifierInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$modifier, $policyPeriod})
  }
  
  function refreshVariables ($modifier :  entity.Modifier, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.WC7StateSpecificTypeKeyModifierInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$modifier, $policyPeriod})
  }
  
  
}