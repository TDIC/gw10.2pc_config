package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewOwnerOfficerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7NewOwnerOfficerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  WC7Line, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7NewOwnerOfficerPopup, {line, contactType}, 0)
  }
  
  static function createDestination (line :  WC7Line, contactType :  ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7NewOwnerOfficerPopup, {line, contactType, clausePattern}, 1)
  }
  
  function pickValueAndCommit (value :  WC7PolicyOwnerOfficer) : void {
    __widgetOf(this, pcf.WC7NewOwnerOfficerPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  WC7Line, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7NewOwnerOfficerPopup, {line, contactType}, 0).push()
  }
  
  static function push (line :  WC7Line, contactType :  ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7NewOwnerOfficerPopup, {line, contactType, clausePattern}, 1).push()
  }
  
  
}