package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ClassesInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($jurisdiction :  WC7Jurisdiction, $wc7Line :  WC7WorkersCompLine, $stateConfig :  gw.lob.wc7.stateconfigs.WC7StateConfig) : void {
    __widgetOf(this, pcf.WC7ClassesInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$jurisdiction, $wc7Line, $stateConfig})
  }
  
  function refreshVariables ($jurisdiction :  WC7Jurisdiction, $wc7Line :  WC7WorkersCompLine, $stateConfig :  gw.lob.wc7.stateconfigs.WC7StateConfig) : void {
    __widgetOf(this, pcf.WC7ClassesInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$jurisdiction, $wc7Line, $stateConfig})
  }
  
  
}