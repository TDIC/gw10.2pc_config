package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TaxLocationSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7TaxLocationSearchResultsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchResults :  TaxLocation[]) : void {
    __widgetOf(this, pcf.WC7TaxLocationSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(false, {$searchResults})
  }
  
  function refreshVariables ($searchResults :  TaxLocation[]) : void {
    __widgetOf(this, pcf.WC7TaxLocationSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(true, {$searchResults})
  }
  
  
}