package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPDeductibleDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPDeductibleDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bldg :  entity.BOPBuilding) : void {
    __widgetOf(this, pcf.TDIC_BOPDeductibleDV, SECTION_WIDGET_CLASS).setVariables(false, {$bldg})
  }
  
  function refreshVariables ($bldg :  entity.BOPBuilding) : void {
    __widgetOf(this, pcf.TDIC_BOPDeductibleDV, SECTION_WIDGET_CLASS).setVariables(true, {$bldg})
  }
  
  
}