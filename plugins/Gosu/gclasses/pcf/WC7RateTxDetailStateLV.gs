package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RateTxDetailStateLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($stateTxs :  java.util.Set<WC7Transaction>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7RateTxDetailStateLV, SECTION_WIDGET_CLASS).setVariables(false, {$stateTxs, $jurisdiction})
  }
  
  function refreshVariables ($stateTxs :  java.util.Set<WC7Transaction>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7RateTxDetailStateLV, SECTION_WIDGET_CLASS).setVariables(true, {$stateTxs, $jurisdiction})
  }
  
  
}