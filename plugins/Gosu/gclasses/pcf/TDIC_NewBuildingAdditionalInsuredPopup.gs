package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_NewBuildingAdditionalInsuredPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewBuildingAdditionalInsuredPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (bopBuilding :  BOPBuilding, contactType :  typekey.ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewBuildingAdditionalInsuredPopup, {bopBuilding, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyAddlInsuredDetail) : void {
    __widgetOf(this, pcf.TDIC_NewBuildingAdditionalInsuredPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (bopBuilding :  BOPBuilding, contactType :  typekey.ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewBuildingAdditionalInsuredPopup, {bopBuilding, contactType}, 0).push()
  }
  
  
}