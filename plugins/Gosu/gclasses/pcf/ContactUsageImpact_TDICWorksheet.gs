package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/ContactUsageImpact_TDICWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ContactUsageImpact_TDICWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (contact :  Contact, passedInImpactedPolicyPeriods :  java.util.List<PolicyPeriod>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ContactUsageImpact_TDICWorksheet, {contact, passedInImpactedPolicyPeriods}, 0)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (contact :  Contact, passedInImpactedPolicyPeriods :  java.util.List<PolicyPeriod>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ContactUsageImpact_TDICWorksheet, {contact, passedInImpactedPolicyPeriods}, 0).goInWorkspace()
  }
  
  static function push (contact :  Contact, passedInImpactedPolicyPeriods :  java.util.List<PolicyPeriod>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ContactUsageImpact_TDICWorksheet, {contact, passedInImpactedPolicyPeriods}, 0).push()
  }
  
  
}