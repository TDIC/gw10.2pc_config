package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_CarrierSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_CarrierSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_CarrierSearchPopup, {}, 0)
  }
  
  function pickValueAndCommit (value :  entity.GLCarrierNames_TDIC) : void {
    __widgetOf(this, pcf.TDIC_CarrierSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_CarrierSearchPopup, {}, 0).push()
  }
  
  
}