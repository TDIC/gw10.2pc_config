package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class LossPayeeDetails_TDICDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bopBuilding :  entity.BOPBuilding, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.LossPayeeDetails_TDICDV, SECTION_WIDGET_CLASS).setVariables(false, {$bopBuilding, $openForEdit})
  }
  
  function refreshVariables ($bopBuilding :  entity.BOPBuilding, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.LossPayeeDetails_TDICDV, SECTION_WIDGET_CLASS).setVariables(true, {$bopBuilding, $openForEdit})
  }
  
  
}