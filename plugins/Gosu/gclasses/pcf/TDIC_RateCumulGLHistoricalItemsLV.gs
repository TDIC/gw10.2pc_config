package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_RateCumulGLHistoricalItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_RateCumulGLHistoricalItemsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($glHistCosts :  java.util.Set<GLHistoricalCost_TDIC>) : void {
    __widgetOf(this, pcf.TDIC_RateCumulGLHistoricalItemsLV, SECTION_WIDGET_CLASS).setVariables(false, {$glHistCosts})
  }
  
  function refreshVariables ($glHistCosts :  java.util.Set<GLHistoricalCost_TDIC>) : void {
    __widgetOf(this, pcf.TDIC_RateCumulGLHistoricalItemsLV, SECTION_WIDGET_CLASS).setVariables(true, {$glHistCosts})
  }
  
  
}