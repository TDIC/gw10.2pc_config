package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7AuditRateCostDetailStateLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isPremiumReport :  boolean, $stateCosts :  java.util.Set<WC7Cost>, $basedOnStateCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailStateLV, SECTION_WIDGET_CLASS).setVariables(false, {$isPremiumReport, $stateCosts, $basedOnStateCosts, $jurisdiction})
  }
  
  function refreshVariables ($isPremiumReport :  boolean, $stateCosts :  java.util.Set<WC7Cost>, $basedOnStateCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailStateLV, SECTION_WIDGET_CLASS).setVariables(true, {$isPremiumReport, $stateCosts, $basedOnStateCosts, $jurisdiction})
  }
  
  
}