package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideStateCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RatingOverrideStateCostLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($stateCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7RatingOverrideStateCostLV, SECTION_WIDGET_CLASS).setVariables(false, {$stateCosts, $jurisdiction})
  }
  
  function refreshVariables ($stateCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction) : void {
    __widgetOf(this, pcf.WC7RatingOverrideStateCostLV, SECTION_WIDGET_CLASS).setVariables(true, {$stateCosts, $jurisdiction})
  }
  
  
}