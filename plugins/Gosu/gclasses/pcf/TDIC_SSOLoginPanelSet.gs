package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/login/TDIC_SSOLoginPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_SSOLoginPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($url :  String, $entryException :  java.lang.Exception) : void {
    __widgetOf(this, pcf.TDIC_SSOLoginPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$url, $entryException})
  }
  
  function refreshVariables ($url :  String, $entryException :  java.lang.Exception) : void {
    __widgetOf(this, pcf.TDIC_SSOLoginPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$url, $entryException})
  }
  
  
}