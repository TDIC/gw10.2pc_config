package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/forms/WC7MoreFormClassCodesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7MoreFormClassCodesPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (formPattern :  FormPattern, formClassCodes :  WC7ClassCode[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7MoreFormClassCodesPopup, {formPattern, formClassCodes}, 0)
  }
  
  function pickValueAndCommit (value :  WC7FormPatternClassCode[]) : void {
    __widgetOf(this, pcf.WC7MoreFormClassCodesPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (formPattern :  FormPattern, formClassCodes :  WC7ClassCode[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7MoreFormClassCodesPopup, {formPattern, formClassCodes}, 0).push()
  }
  
  
}