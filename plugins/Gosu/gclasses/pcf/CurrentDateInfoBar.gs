package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/CurrentDateInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CurrentDateInfoBar extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.CurrentDateInfoBar, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.CurrentDateInfoBar, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}