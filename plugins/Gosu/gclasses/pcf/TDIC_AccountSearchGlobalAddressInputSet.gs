package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/address/TDIC_AccountSearchGlobalAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_AccountSearchGlobalAddressInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($addressOwner :  gw.api.address.AddressOwner) : void {
    __widgetOf(this, pcf.TDIC_AccountSearchGlobalAddressInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$addressOwner})
  }
  
  function refreshVariables ($addressOwner :  gw.api.address.AddressOwner) : void {
    __widgetOf(this, pcf.TDIC_AccountSearchGlobalAddressInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$addressOwner})
  }
  
  
}