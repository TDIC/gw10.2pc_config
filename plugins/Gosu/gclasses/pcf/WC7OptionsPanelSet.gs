package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7OptionsPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($period :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionsPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$period, $openForEdit})
  }
  
  function refreshVariables ($period :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionsPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$period, $openForEdit})
  }
  
  
}