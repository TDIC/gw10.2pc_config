package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.Package.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CovTermInputSet_Package extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($term :  gw.api.domain.covterm.CovTerm, $openForEdit :  boolean, $coverable :  Coverable) : void {
    __widgetOf(this, pcf.CovTermInputSet_Package, SECTION_WIDGET_CLASS).setVariables(false, {$term, $openForEdit, $coverable})
  }
  
  function refreshVariables ($term :  gw.api.domain.covterm.CovTerm, $openForEdit :  boolean, $coverable :  Coverable) : void {
    __widgetOf(this, pcf.CovTermInputSet_Package, SECTION_WIDGET_CLASS).setVariables(true, {$term, $openForEdit, $coverable})
  }
  
  
}