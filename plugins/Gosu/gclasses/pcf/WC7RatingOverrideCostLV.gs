package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RatingOverrideCostLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $periodCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction, $periodStart :  java.util.Date, $periodEnd :  java.util.Date) : void {
    __widgetOf(this, pcf.WC7RatingOverrideCostLV, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $periodCosts, $jurisdiction, $periodStart, $periodEnd})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $periodCosts :  java.util.Set<WC7Cost>, $jurisdiction :  WC7Jurisdiction, $periodStart :  java.util.Date, $periodEnd :  java.util.Date) : void {
    __widgetOf(this, pcf.WC7RatingOverrideCostLV, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $periodCosts, $jurisdiction, $periodStart, $periodEnd})
  }
  
  
}