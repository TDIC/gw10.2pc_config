package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionSet_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class QuestionSet_TDICLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($questionSet :  gw.api.productmodel.QuestionSet, $answerContainer :  AnswerContainer, $onChangeBlock :  block(), $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.QuestionSet_TDICLV, SECTION_WIDGET_CLASS).setVariables(false, {$questionSet, $answerContainer, $onChangeBlock, $openForEdit})
  }
  
  function refreshVariables ($questionSet :  gw.api.productmodel.QuestionSet, $answerContainer :  AnswerContainer, $onChangeBlock :  block(), $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.QuestionSet_TDICLV, SECTION_WIDGET_CLASS).setVariables(true, {$questionSet, $answerContainer, $onChangeBlock, $openForEdit})
  }
  
  
}