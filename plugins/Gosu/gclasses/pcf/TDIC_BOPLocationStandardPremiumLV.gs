package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationStandardPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPLocationStandardPremiumLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($costWrapper :  gw.api.ui.BOPCostWrapper_TDIC[]) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationStandardPremiumLV, SECTION_WIDGET_CLASS).setVariables(false, {$costWrapper})
  }
  
  function refreshVariables ($costWrapper :  gw.api.ui.BOPCostWrapper_TDIC[]) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationStandardPremiumLV, SECTION_WIDGET_CLASS).setVariables(true, {$costWrapper})
  }
  
  
}