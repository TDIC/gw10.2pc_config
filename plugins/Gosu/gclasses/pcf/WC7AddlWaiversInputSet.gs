package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7AddlWaiversInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7AddlWaiversInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wc7Line :  entity.WC7WorkersCompLine, $conditionPattern :  gw.api.productmodel.ClausePattern) : void {
    __widgetOf(this, pcf.WC7AddlWaiversInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$wc7Line, $conditionPattern})
  }
  
  function refreshVariables ($wc7Line :  entity.WC7WorkersCompLine, $conditionPattern :  gw.api.productmodel.ClausePattern) : void {
    __widgetOf(this, pcf.WC7AddlWaiversInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$wc7Line, $conditionPattern})
  }
  
  
}