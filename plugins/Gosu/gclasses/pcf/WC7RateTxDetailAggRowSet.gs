package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RateTxDetailAggRowSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($aggTx :  WC7Transaction) : void {
    __widgetOf(this, pcf.WC7RateTxDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(false, {$aggTx})
  }
  
  function refreshVariables ($aggTx :  WC7Transaction) : void {
    __widgetOf(this, pcf.WC7RateTxDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(true, {$aggTx})
  }
  
  
}