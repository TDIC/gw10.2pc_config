package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_NewEntityOwnerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewEntityOwnerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  WC7Line, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewEntityOwnerPopup, {line, contactType}, 1)
  }
  
  static function createDestination (line :  WC7Line, contactType :  ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewEntityOwnerPopup, {line, contactType, clausePattern}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyEntityOwner_TDIC) : void {
    __widgetOf(this, pcf.TDIC_NewEntityOwnerPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  WC7Line, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewEntityOwnerPopup, {line, contactType}, 1).push()
  }
  
  static function push (line :  WC7Line, contactType :  ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewEntityOwnerPopup, {line, contactType, clausePattern}, 0).push()
  }
  
  
}