package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleRateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ScheduleRateLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($credit :  Modifier, $coverable :  Coverable) : void {
    __widgetOf(this, pcf.WC7ScheduleRateLV, SECTION_WIDGET_CLASS).setVariables(false, {$credit, $coverable})
  }
  
  function refreshVariables ($credit :  Modifier, $coverable :  Coverable) : void {
    __widgetOf(this, pcf.WC7ScheduleRateLV, SECTION_WIDGET_CLASS).setVariables(true, {$credit, $coverable})
  }
  
  
}