package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ExclAlternateCoverages_TDICInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ExclAlternateCoverages_TDICInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($exclusion :  WC7WorkersCompExcl) : void {
    __widgetOf(this, pcf.WC7ExclAlternateCoverages_TDICInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$exclusion})
  }
  
  function refreshVariables ($exclusion :  WC7WorkersCompExcl) : void {
    __widgetOf(this, pcf.WC7ExclAlternateCoverages_TDICInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$exclusion})
  }
  
  
}