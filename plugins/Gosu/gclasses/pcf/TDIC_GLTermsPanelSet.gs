package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLTermsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_GLTermsPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($period :  PolicyPeriod, $glCosts :  Set<entity.GLCost>, $isEREJob :  boolean) : void {
    __widgetOf(this, pcf.TDIC_GLTermsPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$period, $glCosts, $isEREJob})
  }
  
  function refreshVariables ($period :  PolicyPeriod, $glCosts :  Set<entity.GLCost>, $isEREJob :  boolean) : void {
    __widgetOf(this, pcf.TDIC_GLTermsPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$period, $glCosts, $isEREJob})
  }
  
  
}