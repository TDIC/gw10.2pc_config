package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyTransactionForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_PolicyTransactionForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (offerNumber :  String, jobIndex :  int) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0)
  }
  
  static function drilldown (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).goInWorkspace()
  }
  
  static function printPage (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).printPage()
  }
  
  static function push (offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PolicyTransactionForward, {offerNumber, jobIndex}, 0).push()
  }
  
  
}