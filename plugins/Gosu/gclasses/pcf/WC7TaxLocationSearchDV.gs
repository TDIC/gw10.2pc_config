package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TaxLocationSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7TaxLocationSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.lob.common.TaxLocationSearchCriteria) : void {
    __widgetOf(this, pcf.WC7TaxLocationSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.lob.common.TaxLocationSearchCriteria) : void {
    __widgetOf(this, pcf.WC7TaxLocationSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}