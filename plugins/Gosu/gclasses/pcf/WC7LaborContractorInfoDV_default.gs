package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7LaborContractorInfoDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7LaborContractorInfoDV_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($detail :  entity.WC7LaborContactDetail, $inclusionType :  Inclusion, $clause :  gw.api.domain.Clause) : void {
    __widgetOf(this, pcf.WC7LaborContractorInfoDV_default, SECTION_WIDGET_CLASS).setVariables(false, {$detail, $inclusionType, $clause})
  }
  
  function refreshVariables ($detail :  entity.WC7LaborContactDetail, $inclusionType :  Inclusion, $clause :  gw.api.domain.Clause) : void {
    __widgetOf(this, pcf.WC7LaborContractorInfoDV_default, SECTION_WIDGET_CLASS).setVariables(true, {$detail, $inclusionType, $clause})
  }
  
  
}