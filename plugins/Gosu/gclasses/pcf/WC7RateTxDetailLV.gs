package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RateTxDetailLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($periodTxs :  java.util.Set<WC7Transaction>) : void {
    __widgetOf(this, pcf.WC7RateTxDetailLV, SECTION_WIDGET_CLASS).setVariables(false, {$periodTxs})
  }
  
  function refreshVariables ($periodTxs :  java.util.Set<WC7Transaction>) : void {
    __widgetOf(this, pcf.WC7RateTxDetailLV, SECTION_WIDGET_CLASS).setVariables(true, {$periodTxs})
  }
  
  
}