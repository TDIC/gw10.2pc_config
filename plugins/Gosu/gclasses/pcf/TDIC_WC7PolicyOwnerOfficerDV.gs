package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_WC7PolicyOwnerOfficerDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($period :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_WC7PolicyOwnerOfficerDV, SECTION_WIDGET_CLASS).setVariables(false, {$period, $openForEdit})
  }
  
  function refreshVariables ($period :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_WC7PolicyOwnerOfficerDV, SECTION_WIDGET_CLASS).setVariables(true, {$period, $openForEdit})
  }
  
  
}