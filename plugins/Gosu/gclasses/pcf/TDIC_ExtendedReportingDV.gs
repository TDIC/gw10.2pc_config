package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_ExtendedReportingDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ExtendedReportingDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.TDIC_ExtendedReportingDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.TDIC_ExtendedReportingDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod})
  }
  
  
}