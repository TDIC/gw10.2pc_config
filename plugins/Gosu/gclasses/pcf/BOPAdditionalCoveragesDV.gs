package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BOPAdditionalCoveragesDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine, $coverable :  Coverable, $quoteType :  QuoteType, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPAdditionalCoveragesDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline, $coverable, $quoteType, $openForEdit})
  }
  
  function refreshVariables ($policyline :  PolicyLine, $coverable :  Coverable, $quoteType :  QuoteType, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPAdditionalCoveragesDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline, $coverable, $quoteType, $openForEdit})
  }
  
  
}