package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RatingOverrideCostDetailAggRowSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($aggCost :  WC7JurisdictionCost, $basedOnAggCost :  WC7JurisdictionCost) : void {
    __widgetOf(this, pcf.WC7RatingOverrideCostDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(false, {$aggCost, $basedOnAggCost})
  }
  
  function refreshVariables ($aggCost :  WC7JurisdictionCost, $basedOnAggCost :  WC7JurisdictionCost) : void {
    __widgetOf(this, pcf.WC7RatingOverrideCostDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(true, {$aggCost, $basedOnAggCost})
  }
  
  
}