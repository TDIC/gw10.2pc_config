package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($conditionPattern :  gw.api.productmodel.ClausePattern, $coverable :  Coverable, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond, SECTION_WIDGET_CLASS).setVariables(false, {$conditionPattern, $coverable, $openForEdit})
  }
  
  function refreshVariables ($conditionPattern :  gw.api.productmodel.ClausePattern, $coverable :  Coverable, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond, SECTION_WIDGET_CLASS).setVariables(true, {$conditionPattern, $coverable, $openForEdit})
  }
  
  
}