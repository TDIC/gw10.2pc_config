package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_IRPMDiscountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_IRPMDiscountDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine, $quoteType :  QuoteType) : void {
    __widgetOf(this, pcf.TDIC_IRPMDiscountDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline, $quoteType})
  }
  
  function refreshVariables ($policyline :  PolicyLine, $quoteType :  QuoteType) : void {
    __widgetOf(this, pcf.TDIC_IRPMDiscountDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline, $quoteType})
  }
  
  
}