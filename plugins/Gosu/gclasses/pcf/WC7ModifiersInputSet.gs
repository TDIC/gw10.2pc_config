package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7ModifiersInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($modifiers :  java.util.List<WC7Modifier>, $policyPeriod :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7ModifiersInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$modifiers, $policyPeriod, $openForEdit})
  }
  
  function refreshVariables ($modifiers :  java.util.List<WC7Modifier>, $policyPeriod :  PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7ModifiersInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$modifiers, $policyPeriod, $openForEdit})
  }
  
  
}