package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("FakePathForModalBase.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7SupplementaryDiseaseInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.WC7SupplementaryDiseaseInputSet, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.WC7SupplementaryDiseaseInputSet, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}