package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GLRiskManagementDiscount_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class GLRiskManagementDiscount_TDICDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine) : void {
    __widgetOf(this, pcf.GLRiskManagementDiscount_TDICDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline})
  }
  
  function refreshVariables ($policyline :  PolicyLine) : void {
    __widgetOf(this, pcf.GLRiskManagementDiscount_TDICDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline})
  }
  
  
}