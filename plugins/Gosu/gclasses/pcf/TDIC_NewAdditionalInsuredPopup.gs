package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_NewAdditionalInsuredPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewAdditionalInsuredPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  PolicyLine, contactType :  typekey.ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewAdditionalInsuredPopup, {line, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyAddlInsuredDetail) : void {
    __widgetOf(this, pcf.TDIC_NewAdditionalInsuredPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  PolicyLine, contactType :  typekey.ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewAdditionalInsuredPopup, {line, contactType}, 0).push()
  }
  
  
}