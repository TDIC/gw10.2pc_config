package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_EREQuoteDetailPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($latestQuote :  GLEREQuickQuote_TDIC) : void {
    __widgetOf(this, pcf.TDIC_EREQuoteDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$latestQuote})
  }
  
  function refreshVariables ($latestQuote :  GLEREQuickQuote_TDIC) : void {
    __widgetOf(this, pcf.TDIC_EREQuoteDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$latestQuote})
  }
  
  
}