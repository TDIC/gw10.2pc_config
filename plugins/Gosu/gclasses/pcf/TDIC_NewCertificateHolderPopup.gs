package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_NewCertificateHolderPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewCertificateHolderPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (glLine :  entity.GeneralLiabilityLine, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewCertificateHolderPopup, {glLine, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyCertificateHolder_TDIC) : void {
    __widgetOf(this, pcf.TDIC_NewCertificateHolderPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (glLine :  entity.GeneralLiabilityLine, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewCertificateHolderPopup, {glLine, contactType}, 0).push()
  }
  
  
}