package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_GLPolicyOwnerOfficerDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($period :  entity.PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_GLPolicyOwnerOfficerDV, SECTION_WIDGET_CLASS).setVariables(false, {$period, $openForEdit})
  }
  
  function refreshVariables ($period :  entity.PolicyPeriod, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_GLPolicyOwnerOfficerDV, SECTION_WIDGET_CLASS).setVariables(true, {$period, $openForEdit})
  }
  
  
}