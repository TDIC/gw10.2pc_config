package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7StateCostsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($stateCosts :  gw.api.ui.WC7CostWrapper[]) : void {
    __widgetOf(this, pcf.WC7StateCostsLV, SECTION_WIDGET_CLASS).setVariables(false, {$stateCosts})
  }
  
  function refreshVariables ($stateCosts :  gw.api.ui.WC7CostWrapper[]) : void {
    __widgetOf(this, pcf.WC7StateCostsLV, SECTION_WIDGET_CLASS).setVariables(true, {$stateCosts})
  }
  
  
}