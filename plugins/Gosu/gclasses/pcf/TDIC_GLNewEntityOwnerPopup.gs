package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/gl/TDIC_GLNewEntityOwnerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_GLNewEntityOwnerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  GeneralLiabilityLine, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_GLNewEntityOwnerPopup, {line, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  GLPolicyEntityOwner_TDIC) : void {
    __widgetOf(this, pcf.TDIC_GLNewEntityOwnerPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  GeneralLiabilityLine, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_GLNewEntityOwnerPopup, {line, contactType}, 0).push()
  }
  
  
}