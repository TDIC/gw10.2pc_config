package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_EREQuoteTotalsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($quote :  GLEREQuickQuote_TDIC) : void {
    __widgetOf(this, pcf.TDIC_EREQuoteTotalsDV, SECTION_WIDGET_CLASS).setVariables(false, {$quote})
  }
  
  function refreshVariables ($quote :  GLEREQuickQuote_TDIC) : void {
    __widgetOf(this, pcf.TDIC_EREQuoteTotalsDV, SECTION_WIDGET_CLASS).setVariables(true, {$quote})
  }
  
  
}