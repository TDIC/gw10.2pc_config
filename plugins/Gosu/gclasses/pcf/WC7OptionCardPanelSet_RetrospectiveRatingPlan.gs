package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7OptionCardPanelSet_RetrospectiveRatingPlan extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wc7Line :  WC7WorkersCompLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionCardPanelSet_RetrospectiveRatingPlan, SECTION_WIDGET_CLASS).setVariables(false, {$wc7Line, $openForEdit})
  }
  
  function refreshVariables ($wc7Line :  WC7WorkersCompLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionCardPanelSet_RetrospectiveRatingPlan, SECTION_WIDGET_CLASS).setVariables(true, {$wc7Line, $openForEdit})
  }
  
  
}