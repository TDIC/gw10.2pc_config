package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPTotalLocationPremiumDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPTotalLocationPremiumDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($totalLocPremium :  gw.pl.currency.MonetaryAmount) : void {
    __widgetOf(this, pcf.TDIC_BOPTotalLocationPremiumDV, SECTION_WIDGET_CLASS).setVariables(false, {$totalLocPremium})
  }
  
  function refreshVariables ($totalLocPremium :  gw.pl.currency.MonetaryAmount) : void {
    __widgetOf(this, pcf.TDIC_BOPTotalLocationPremiumDV, SECTION_WIDGET_CLASS).setVariables(true, {$totalLocPremium})
  }
  
  
}