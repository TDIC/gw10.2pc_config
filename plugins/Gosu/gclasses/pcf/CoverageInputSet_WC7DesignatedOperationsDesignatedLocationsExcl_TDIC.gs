package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($exclusionPattern :  gw.api.productmodel.ClausePattern, $coverable :  Coverable, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC, SECTION_WIDGET_CLASS).setVariables(false, {$exclusionPattern, $coverable, $openForEdit})
  }
  
  function refreshVariables ($exclusionPattern :  gw.api.productmodel.ClausePattern, $coverable :  Coverable, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC, SECTION_WIDGET_CLASS).setVariables(true, {$exclusionPattern, $coverable, $openForEdit})
  }
  
  
}