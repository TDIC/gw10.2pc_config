package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_StartEREQuote.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_StartEREQuote extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0)
  }
  
  static function drilldown (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0).goInMain()
  }
  
  static function printPage (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0).printPage()
  }
  
  static function push (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_StartEREQuote, {policyPeriod, latestQuote}, 0).push()
  }
  
  
}