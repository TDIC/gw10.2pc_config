package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/Policy_Summary_ActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Policy_Summary_ActivitiesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.Policy_Summary_ActivitiesLV, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.Policy_Summary_ActivitiesLV, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod})
  }
  
  
}