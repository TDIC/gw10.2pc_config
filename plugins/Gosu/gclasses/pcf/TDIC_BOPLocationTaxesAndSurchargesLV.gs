package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationTaxesAndSurchargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPLocationTaxesAndSurchargesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($taxesAndSurcharges :  java.util.Set<BOPCost>) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationTaxesAndSurchargesLV, SECTION_WIDGET_CLASS).setVariables(false, {$taxesAndSurcharges})
  }
  
  function refreshVariables ($taxesAndSurcharges :  java.util.Set<BOPCost>) : void {
    __widgetOf(this, pcf.TDIC_BOPLocationTaxesAndSurchargesLV, SECTION_WIDGET_CLASS).setVariables(true, {$taxesAndSurcharges})
  }
  
  
}