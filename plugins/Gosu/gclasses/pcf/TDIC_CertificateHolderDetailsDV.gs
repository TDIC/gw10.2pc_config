package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_CertificateHolderDetailsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($glLine :  entity.GeneralLiabilityLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_CertificateHolderDetailsDV, SECTION_WIDGET_CLASS).setVariables(false, {$glLine, $openForEdit})
  }
  
  function refreshVariables ($glLine :  entity.GeneralLiabilityLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.TDIC_CertificateHolderDetailsDV, SECTION_WIDGET_CLASS).setVariables(true, {$glLine, $openForEdit})
  }
  
  
}