package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7AuditRateCostDetailLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isPremiumReport :  boolean, $periodCosts :  java.util.Set<WC7Cost>, $ratingPeriod :  gw.lob.wc7.rating.WC7RatingPeriod) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailLV, SECTION_WIDGET_CLASS).setVariables(false, {$isPremiumReport, $periodCosts, $ratingPeriod})
  }
  
  function refreshVariables ($isPremiumReport :  boolean, $periodCosts :  java.util.Set<WC7Cost>, $ratingPeriod :  gw.lob.wc7.rating.WC7RatingPeriod) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailLV, SECTION_WIDGET_CLASS).setVariables(true, {$isPremiumReport, $periodCosts, $ratingPeriod})
  }
  
  
}