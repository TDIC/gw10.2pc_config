package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_GLERERatingDetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (period :  PolicyPeriod, jobWizardHelper :  gw.api.web.job.JobWizardHelper, isEREJob :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_GLERERatingDetailsPopup, {period, jobWizardHelper, isEREJob}, 0)
  }
  
  static function push (period :  PolicyPeriod, jobWizardHelper :  gw.api.web.job.JobWizardHelper, isEREJob :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_GLERERatingDetailsPopup, {period, jobWizardHelper, isEREJob}, 0).push()
  }
  
  
}