package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MortgageeDetails_TDICDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bopBuilding :  entity.BOPBuilding, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.MortgageeDetails_TDICDV, SECTION_WIDGET_CLASS).setVariables(false, {$bopBuilding, $openForEdit})
  }
  
  function refreshVariables ($bopBuilding :  entity.BOPBuilding, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.MortgageeDetails_TDICDV, SECTION_WIDGET_CLASS).setVariables(true, {$bopBuilding, $openForEdit})
  }
  
  
}