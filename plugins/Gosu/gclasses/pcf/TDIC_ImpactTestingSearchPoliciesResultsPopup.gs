package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/rating/impact/TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ImpactTestingSearchPoliciesResultsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (allPolicyPeriods :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_ImpactTestingSearchPoliciesResultsPopup, {allPolicyPeriods}, 0)
  }
  
  static function push (allPolicyPeriods :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_ImpactTestingSearchPoliciesResultsPopup, {allPolicyPeriods}, 0).push()
  }
  
  
}