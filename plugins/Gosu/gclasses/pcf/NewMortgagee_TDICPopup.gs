package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/NewMortgagee_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewMortgagee_TDICPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (bopBuilding :  entity.BOPBuilding, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewMortgagee_TDICPopup, {bopBuilding, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyMortgagee_TDIC) : void {
    __widgetOf(this, pcf.NewMortgagee_TDICPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (bopBuilding :  entity.BOPBuilding, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewMortgagee_TDICPopup, {bopBuilding, contactType}, 0).push()
  }
  
  
}