package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BuildingAdditionalInsuredsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bopBuilding :  BOPBuilding, $openForEdit :  boolean, $displayLabel :  boolean, $additionalInformationVisible :  boolean) : void {
    __widgetOf(this, pcf.TDIC_BuildingAdditionalInsuredsDV, SECTION_WIDGET_CLASS).setVariables(false, {$bopBuilding, $openForEdit, $displayLabel, $additionalInformationVisible})
  }
  
  function refreshVariables ($bopBuilding :  BOPBuilding, $openForEdit :  boolean, $displayLabel :  boolean, $additionalInformationVisible :  boolean) : void {
    __widgetOf(this, pcf.TDIC_BuildingAdditionalInsuredsDV, SECTION_WIDGET_CLASS).setVariables(true, {$bopBuilding, $openForEdit, $displayLabel, $additionalInformationVisible})
  }
  
  
}