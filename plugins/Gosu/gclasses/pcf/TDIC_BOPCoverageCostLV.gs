package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPCoverageCostLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($coverable :  Coverable, $costs :  Set<BOPCost>, $costWrappers :  gw.api.ui.BOPCostWrapper_TDIC[], $footerMessage :  java.lang.String) : void {
    __widgetOf(this, pcf.TDIC_BOPCoverageCostLV, SECTION_WIDGET_CLASS).setVariables(false, {$coverable, $costs, $costWrappers, $footerMessage})
  }
  
  function refreshVariables ($coverable :  Coverable, $costs :  Set<BOPCost>, $costWrappers :  gw.api.ui.BOPCostWrapper_TDIC[], $footerMessage :  java.lang.String) : void {
    __widgetOf(this, pcf.TDIC_BOPCoverageCostLV, SECTION_WIDGET_CLASS).setVariables(true, {$coverable, $costs, $costWrappers, $footerMessage})
  }
  
  
}