package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuilding_DetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BOPBuilding_DetailsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($building :  BOPBuilding, $openForEdit :  boolean, $bopLine :  BOPLine, $quoteType :  QuoteType) : void {
    __widgetOf(this, pcf.BOPBuilding_DetailsDV, SECTION_WIDGET_CLASS).setVariables(false, {$building, $openForEdit, $bopLine, $quoteType})
  }
  
  function refreshVariables ($building :  BOPBuilding, $openForEdit :  boolean, $bopLine :  BOPLine, $quoteType :  QuoteType) : void {
    __widgetOf(this, pcf.BOPBuilding_DetailsDV, SECTION_WIDGET_CLASS).setVariables(true, {$building, $openForEdit, $bopLine, $quoteType})
  }
  
  
}