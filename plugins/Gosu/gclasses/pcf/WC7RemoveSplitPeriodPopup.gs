package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7RemoveSplitPeriodPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (wcLine :  productmodel.WC7Line, jurisdictions :  WC7Jurisdiction[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7RemoveSplitPeriodPopup, {wcLine, jurisdictions}, 0)
  }
  
  static function push (wcLine :  productmodel.WC7Line, jurisdictions :  WC7Jurisdiction[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7RemoveSplitPeriodPopup, {wcLine, jurisdictions}, 0).push()
  }
  
  
}