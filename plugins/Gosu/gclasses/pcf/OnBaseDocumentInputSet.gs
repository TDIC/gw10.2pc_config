package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBaseDocumentInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($LinkedEntity :  KeyableBean, $LinkType :  acc.onbase.configuration.DocumentLinkType, $Beans :  KeyableBean[]) : void {
    __widgetOf(this, pcf.OnBaseDocumentInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$LinkedEntity, $LinkType, $Beans})
  }
  
  function refreshVariables ($LinkedEntity :  KeyableBean, $LinkType :  acc.onbase.configuration.DocumentLinkType, $Beans :  KeyableBean[]) : void {
    __widgetOf(this, pcf.OnBaseDocumentInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$LinkedEntity, $LinkType, $Beans})
  }
  
  
}