package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7LineCoverageCV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wcLine :  WC7WorkersCompLine, $openForEdit :  boolean, $jobWizardHelper :  gw.api.web.job.JobWizardHelper) : void {
    __widgetOf(this, pcf.WC7LineCoverageCV, SECTION_WIDGET_CLASS).setVariables(false, {$wcLine, $openForEdit, $jobWizardHelper})
  }
  
  function refreshVariables ($wcLine :  WC7WorkersCompLine, $openForEdit :  boolean, $jobWizardHelper :  gw.api.web.job.JobWizardHelper) : void {
    __widgetOf(this, pcf.WC7LineCoverageCV, SECTION_WIDGET_CLASS).setVariables(true, {$wcLine, $openForEdit, $jobWizardHelper})
  }
  
  
}