package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseWebFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBaseWebFramePanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.OnBaseWebFramePanelSet, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.OnBaseWebFramePanelSet, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}