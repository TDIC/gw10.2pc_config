package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLinePropertyCoverageDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BOPLinePropertyCoverageDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine, $coverable :  Coverable, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPLinePropertyCoverageDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline, $coverable, $openForEdit})
  }
  
  function refreshVariables ($policyline :  PolicyLine, $coverable :  Coverable, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPLinePropertyCoverageDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline, $coverable, $openForEdit})
  }
  
  
}