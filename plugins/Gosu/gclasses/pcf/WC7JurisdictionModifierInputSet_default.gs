package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7JurisdictionModifierInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($modifiers :  java.util.List<WC7Modifier>, $policyPeriod :  PolicyPeriod, $openForEdit :  boolean, $stateConfig :  gw.lob.wc7.stateconfigs.WC7StateConfig) : void {
    __widgetOf(this, pcf.WC7JurisdictionModifierInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$modifiers, $policyPeriod, $openForEdit, $stateConfig})
  }
  
  function refreshVariables ($modifiers :  java.util.List<WC7Modifier>, $policyPeriod :  PolicyPeriod, $openForEdit :  boolean, $stateConfig :  gw.lob.wc7.stateconfigs.WC7StateConfig) : void {
    __widgetOf(this, pcf.WC7JurisdictionModifierInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$modifiers, $policyPeriod, $openForEdit, $stateConfig})
  }
  
  
}