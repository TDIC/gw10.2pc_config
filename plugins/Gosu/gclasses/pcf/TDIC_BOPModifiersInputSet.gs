package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPModifiersInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($modifiers :  List<Modifier>, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.TDIC_BOPModifiersInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$modifiers, $policyPeriod})
  }
  
  function refreshVariables ($modifiers :  List<Modifier>, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.TDIC_BOPModifiersInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$modifiers, $policyPeriod})
  }
  
  
}