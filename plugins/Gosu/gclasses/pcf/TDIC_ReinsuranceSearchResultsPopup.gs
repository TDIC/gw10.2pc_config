package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ReinsuranceSearchResultsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (reinsuranceSearchResult :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_ReinsuranceSearchResultsPopup, {reinsuranceSearchResult}, 0)
  }
  
  static function push (reinsuranceSearchResult :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_ReinsuranceSearchResultsPopup, {reinsuranceSearchResult}, 0).push()
  }
  
  
}