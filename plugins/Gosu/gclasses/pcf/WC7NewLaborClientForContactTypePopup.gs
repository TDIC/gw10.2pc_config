package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborClientForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7NewLaborClientForContactTypePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.WC7NewLaborClientForContactTypePopup, {presenter}, 0)
  }
  
  function pickValueAndCommit (value :  WC7LaborContactDetail) : void {
    __widgetOf(this, pcf.WC7NewLaborClientForContactTypePopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : pcf.api.Location {
    return __newDestinationForLocation(pcf.WC7NewLaborClientForContactTypePopup, {presenter}, 0).push()
  }
  
  
}