package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7AuditRateCostDetailAggRowSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isPremiumReport :  boolean, $aggCost :  WC7JurisdictionCost, $basedOnAggCost :  WC7JurisdictionCost) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(false, {$isPremiumReport, $aggCost, $basedOnAggCost})
  }
  
  function refreshVariables ($isPremiumReport :  boolean, $aggCost :  WC7JurisdictionCost, $basedOnAggCost :  WC7JurisdictionCost) : void {
    __widgetOf(this, pcf.WC7AuditRateCostDetailAggRowSet, SECTION_WIDGET_CLASS).setVariables(true, {$isPremiumReport, $aggCost, $basedOnAggCost})
  }
  
  
}