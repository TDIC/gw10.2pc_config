package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/bop/TDIC_BOPNewEntityOwnerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_BOPNewEntityOwnerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (line :  BusinessOwnersLine, contactType :  ContactType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_BOPNewEntityOwnerPopup, {line, contactType}, 0)
  }
  
  function pickValueAndCommit (value :  BOPPolicyEntityOwner_TDIC) : void {
    __widgetOf(this, pcf.TDIC_BOPNewEntityOwnerPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (line :  BusinessOwnersLine, contactType :  ContactType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_BOPNewEntityOwnerPopup, {line, contactType}, 0).push()
  }
  
  
}