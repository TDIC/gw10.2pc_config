package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLineLiabilityCoverageDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BOPLineLiabilityCoverageDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine, $coverable :  Coverable, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPLineLiabilityCoverageDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline, $coverable, $openForEdit})
  }
  
  function refreshVariables ($policyline :  PolicyLine, $coverable :  Coverable, $openForEdit :  Boolean) : void {
    __widgetOf(this, pcf.BOPLineLiabilityCoverageDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline, $coverable, $openForEdit})
  }
  
  
}