package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyOfficialIDInputSet.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyOfficialIDInputSet_Company extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyContactRole :  PolicyContactRole) : void {
    __widgetOf(this, pcf.PolicyOfficialIDInputSet_Company, SECTION_WIDGET_CLASS).setVariables(false, {$policyContactRole})
  }
  
  function refreshVariables ($policyContactRole :  PolicyContactRole) : void {
    __widgetOf(this, pcf.PolicyOfficialIDInputSet_Company, SECTION_WIDGET_CLASS).setVariables(true, {$policyContactRole})
  }
  
  
}