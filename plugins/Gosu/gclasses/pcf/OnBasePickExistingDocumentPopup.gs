package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBasePickExistingDocumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBasePickExistingDocumentPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, Beans :  KeyableBean[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.OnBasePickExistingDocumentPopup, {Entity, LinkType, Beans}, 0)
  }
  
  static function push (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, Beans :  KeyableBean[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.OnBasePickExistingDocumentPopup, {Entity, LinkType, Beans}, 0).push()
  }
  
  
}