package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLIRPMModifiersDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_GLIRPMModifiersDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyline :  PolicyLine) : void {
    __widgetOf(this, pcf.TDIC_GLIRPMModifiersDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyline})
  }
  
  function refreshVariables ($policyline :  PolicyLine) : void {
    __widgetOf(this, pcf.TDIC_GLIRPMModifiersDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyline})
  }
  
  
}