package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.ManuscriptOption.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7OptionCardPanelSet_ManuscriptOption extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wcLine :  WC7WorkersCompLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionCardPanelSet_ManuscriptOption, SECTION_WIDGET_CLASS).setVariables(false, {$wcLine, $openForEdit})
  }
  
  function refreshVariables ($wcLine :  WC7WorkersCompLine, $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7OptionCardPanelSet_ManuscriptOption, SECTION_WIDGET_CLASS).setVariables(true, {$wcLine, $openForEdit})
  }
  
  
}