package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ReinsuranceSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.TDIC_ReinsuranceSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.TDIC_ReinsuranceSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}