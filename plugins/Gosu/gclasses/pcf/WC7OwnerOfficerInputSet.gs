package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7OwnerOfficerInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7OwnerOfficerInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyOwnerOfficer :  WC7PolicyOwnerOfficer) : void {
    __widgetOf(this, pcf.WC7OwnerOfficerInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$policyOwnerOfficer})
  }
  
  function refreshVariables ($policyOwnerOfficer :  WC7PolicyOwnerOfficer) : void {
    __widgetOf(this, pcf.WC7OwnerOfficerInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$policyOwnerOfficer})
  }
  
  
}