package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionConditionsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WC7JurisdictionConditionsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wc7Jurisdiction :  WC7Jurisdiction, $includedCoverageCategories :  String[], $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7JurisdictionConditionsDV, SECTION_WIDGET_CLASS).setVariables(false, {$wc7Jurisdiction, $includedCoverageCategories, $openForEdit})
  }
  
  function refreshVariables ($wc7Jurisdiction :  WC7Jurisdiction, $includedCoverageCategories :  String[], $openForEdit :  boolean) : void {
    __widgetOf(this, pcf.WC7JurisdictionConditionsDV, SECTION_WIDGET_CLASS).setVariables(true, {$wc7Jurisdiction, $includedCoverageCategories, $openForEdit})
  }
  
  
}