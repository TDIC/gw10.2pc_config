package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contactfile/ContactFile_Details.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactFile_DetailsExpressions {
  @javax.annotation.Generated("config/web/pcf/contactfile/ContactFile_Details.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactFile_DetailsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (contact :  Contact) : int {
      return 0
    }
    
    static function __constructorIndex (contact :  Contact, startEdit :  boolean) : int {
      return 1
    }
    
    // 'def' attribute on PanelRef at ContactFile_Details.pcf: line 64, column 57
    function def_onEnter_6 (def :  pcf.AccountContactDV) : void {
      def.onEnter(contact,null)
    }
    
    // 'def' attribute on PanelRef at ContactFile_Details.pcf: line 72, column 65
    function def_onEnter_8 (def :  pcf.AddressesPanelSet) : void {
      def.onEnter(contact,false,null,null)
    }
    
    // 'def' attribute on PanelRef at ContactFile_Details.pcf: line 64, column 57
    function def_refreshVariables_7 (def :  pcf.AccountContactDV) : void {
      def.refreshVariables(contact,null)
    }
    
    // 'def' attribute on PanelRef at ContactFile_Details.pcf: line 72, column 65
    function def_refreshVariables_9 (def :  pcf.AddressesPanelSet) : void {
      def.refreshVariables(contact,false,null,null)
    }
    
    // 'infoBar' attribute on Page (id=ContactFile_Details) at ContactFile_Details.pcf: line 11, column 71
    function infoBar_onEnter_10 (def :  pcf.ContactFileInfoBar) : void {
      def.onEnter(contact)
    }
    
    // 'infoBar' attribute on Page (id=ContactFile_Details) at ContactFile_Details.pcf: line 11, column 71
    function infoBar_refreshVariables_11 (def :  pcf.ContactFileInfoBar) : void {
      def.refreshVariables(contact)
    }
    
    // 'initialValue' attribute on Variable at ContactFile_Details.pcf: line 26, column 76
    function initialValue_0 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at ContactFile_Details.pcf: line 38, column 50
    function initialValue_1 () : java.util.List<PolicyPeriod> {
      return contact.AssociationFinder.findLatestBoundPolicyPeriods()
    }
    
    // 'initialValue' attribute on Variable at ContactFile_Details.pcf: line 42, column 25
    function initialValue_2 () : Account[] {
      return contact.AssociationFinder.findAccounts()
    }
    
    // EditButtons at ContactFile_Details.pcf: line 47, column 88
    function label_3 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on Verbatim (id=ContactUsageImpact_TJT) at ContactFile_Details.pcf: line 54, column 25
    function label_5 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisContactMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // Page (id=ContactFile_Details) at ContactFile_Details.pcf: line 11, column 71
    static function parent_12 (contact :  Contact, startEdit :  boolean) : pcf.api.Destination {
      return pcf.ContactFile.createDestination(contact)
    }
    
    // 'startInEditMode' attribute on Page (id=ContactFile_Details) at ContactFile_Details.pcf: line 11, column 71
    function startInEditMode_13 () : java.lang.Boolean {
      return startEdit
    }
    
    // 'visible' attribute on Verbatim (id=ContactUsageImpact_TJT) at ContactFile_Details.pcf: line 54, column 25
    function visible_4 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    override property get CurrentLocation () : pcf.ContactFile_Details {
      return super.CurrentLocation as pcf.ContactFile_Details
    }
    
    property get contact () : Contact {
      return getVariableValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get displayMembershipCheckError () : boolean {
      return getVariableValue("displayMembershipCheckError", 0) as java.lang.Boolean
    }
    
    property set displayMembershipCheckError ($arg :  boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedAccounts_TDIC () : Account[] {
      return getVariableValue("impactedAccounts_TDIC", 0) as Account[]
    }
    
    property set impactedAccounts_TDIC ($arg :  Account[]) {
      setVariableValue("impactedAccounts_TDIC", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.List<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.List<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get revealAccountsIgnoringViewPermission_TDIC () : boolean {
      return getVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0) as java.lang.Boolean
    }
    
    property set revealAccountsIgnoringViewPermission_TDIC ($arg :  boolean) {
      setVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0, $arg)
    }
    
    property get startEdit () : boolean {
      return getVariableValue("startEdit", 0) as java.lang.Boolean
    }
    
    property set startEdit ($arg :  boolean) {
      setVariableValue("startEdit", 0, $arg)
    }
    
    
  }
  
  
}