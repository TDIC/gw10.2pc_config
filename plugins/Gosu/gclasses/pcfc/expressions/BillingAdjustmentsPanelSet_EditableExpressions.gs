package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/BillingAdjustmentsPanelSet.Editable.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingAdjustmentsPanelSet_EditableExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/BillingAdjustmentsPanelSet.Editable.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingAdjustmentsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at BillingAdjustmentsPanelSet.Editable.pcf: line 52, column 169
    function def_onEnter_16 (def :  pcf.AccountContactBillingInputSet) : void {
      def.onEnter(policyPeriod.Policy.Account, true, policyPeriodBillingInstructionsManager, policyPeriod.PreferredSettlementCurrency)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentsPlanDV) at BillingAdjustmentsPanelSet.Editable.pcf: line 83, column 84
    function def_onEnter_26 (def :  pcf.PaymentsPlanInputSet_Installments) : void {
      def.onEnter(policyPeriodBillingInstructionsManager)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentsPlanDV) at BillingAdjustmentsPanelSet.Editable.pcf: line 83, column 84
    function def_onEnter_28 (def :  pcf.PaymentsPlanInputSet_ReportingPlan) : void {
      def.onEnter(policyPeriodBillingInstructionsManager)
    }
    
    // 'def' attribute on InputSetRef at BillingAdjustmentsPanelSet.Editable.pcf: line 52, column 169
    function def_refreshVariables_17 (def :  pcf.AccountContactBillingInputSet) : void {
      def.refreshVariables(policyPeriod.Policy.Account, true, policyPeriodBillingInstructionsManager, policyPeriod.PreferredSettlementCurrency)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentsPlanDV) at BillingAdjustmentsPanelSet.Editable.pcf: line 83, column 84
    function def_refreshVariables_27 (def :  pcf.PaymentsPlanInputSet_Installments) : void {
      def.refreshVariables(policyPeriodBillingInstructionsManager)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentsPlanDV) at BillingAdjustmentsPanelSet.Editable.pcf: line 83, column 84
    function def_refreshVariables_29 (def :  pcf.PaymentsPlanInputSet_ReportingPlan) : void {
      def.refreshVariables(policyPeriodBillingInstructionsManager)
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingMethod = (__VALUE_TO_SET as typekey.BillingMethod)
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 76, column 100
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriodBillingInstructionsManager.PaymentMethodChoice = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'initialValue' attribute on Variable at BillingAdjustmentsPanelSet.Editable.pcf: line 18, column 39
    function initialValue_0 () : typekey.BillingMethod[] {
      return policyPeriodBillingInstructionsManager.AvailableBillingMethods
    }
    
    // 'initialValue' attribute on Variable at BillingAdjustmentsPanelSet.Editable.pcf: line 22, column 37
    function initialValue_1 () : typekey.BillingMethod {
      return policyPeriodBillingInstructionsManager.BillingMethod
    }
    
    // 'mode' attribute on InputSetRef (id=PaymentsPlanDV) at BillingAdjustmentsPanelSet.Editable.pcf: line 83, column 84
    function mode_30 () : java.lang.Object {
      return policyPeriodBillingInstructionsManager.PaymentMethodChoice
    }
    
    // 'onChange' attribute on PostOnChange at BillingAdjustmentsPanelSet.Editable.pcf: line 78, column 97
    function onChange_18 () : void {
      policyPeriodBillingInstructionsManager.chooseDefaultPaymentPlan()
    }
    
    // 'onChange' attribute on PostOnChange at BillingAdjustmentsPanelSet.Editable.pcf: line 49, column 102
    function onChange_7 () : void {
      policyPeriodBillingInstructionsManager.BillingMethodType = billingMethod
    }
    
    // 'onPick' attribute on TypeKeyInput (id=PaymentMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 76, column 100
    function onPick_20 (PickedValue :  java.lang.Object) : void {
      policyPeriodBillingInstructionsManager.updateUpFrontPaymentStateContainer()
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function valueRange_11 () : java.lang.Object {
      return getBOPBillingMethodsSelectionsNoListBill() // GPC-205/574: List Bill not available for BOP //availableBillingMethods
    }
    
    // 'value' attribute on TextInput (id=BillingLevel_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 36, column 136
    function valueRoot_4 () : java.lang.Object {
      return policyPeriodBillingInstructionsManager
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 76, column 100
    function value_21 () : typekey.PaymentMethod {
      return policyPeriodBillingInstructionsManager.PaymentMethodChoice
    }
    
    // 'value' attribute on TextInput (id=BillingLevel_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 36, column 136
    function value_3 () : typekey.BillingLevel {
      return policyPeriodBillingInstructionsManager.BillingLevel
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function value_9 () : typekey.BillingMethod {
      return billingMethod
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.BillingMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function verifyValueRange_13 () : void {
      var __valueRangeArg = getBOPBillingMethodsSelectionsNoListBill() // GPC-205/574: List Bill not available for BOP //availableBillingMethods
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=PaymentMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 76, column 100
    function visible_19 () : java.lang.Boolean {
      return policyPeriodBillingInstructionsManager.HasBothInstallmentsAndReportPlans
    }
    
    // 'visible' attribute on TextInput (id=BillingLevel_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 36, column 136
    function visible_2 () : java.lang.Boolean {
      return false // GPC-574/205, Hide for all transactions //policyPeriodBillingInstructionsManager.BillingLevel != null
    }
    
    // 'visible' attribute on RangeInput (id=BillingMethod_Input) at BillingAdjustmentsPanelSet.Editable.pcf: line 47, column 97
    function visible_8 () : java.lang.Boolean {
      return availableBillingMethods != null and availableBillingMethods.HasElements
    }
    
    property get availableBillingMethods () : typekey.BillingMethod[] {
      return getVariableValue("availableBillingMethods", 0) as typekey.BillingMethod[]
    }
    
    property set availableBillingMethods ($arg :  typekey.BillingMethod[]) {
      setVariableValue("availableBillingMethods", 0, $arg)
    }
    
    property get billingMethod () : typekey.BillingMethod {
      return getVariableValue("billingMethod", 0) as typekey.BillingMethod
    }
    
    property set billingMethod ($arg :  typekey.BillingMethod) {
      setVariableValue("billingMethod", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get policyPeriodBillingInstructionsManager () : gw.billing.PolicyPeriodBillingInstructionsManager {
      return getRequireValue("policyPeriodBillingInstructionsManager", 0) as gw.billing.PolicyPeriodBillingInstructionsManager
    }
    
    property set policyPeriodBillingInstructionsManager ($arg :  gw.billing.PolicyPeriodBillingInstructionsManager) {
      setRequireValue("policyPeriodBillingInstructionsManager", 0, $arg)
    }
    
    function getBOPBillingMethodsSelectionsNoListBill() : typekey.BillingMethod[] {
      var filteredillingMethods = new ArrayList<BillingMethod>()
      if (policyPeriod.BOPLineExists and availableBillingMethods.HasElements) {
        for (var avlBM in availableBillingMethods) {
          if (avlBM != typekey.BillingMethod.TC_LISTBILL) {
            filteredillingMethods.add(avlBM)
          }
        }
      }
      return filteredillingMethods.toTypedArray()
    }
    
    
  }
  
  
}