package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/ContactPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/ContactPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=ContactCurrency) at ContactPanelSet.pcf: line 55, column 37
    function def_onEnter_13 (def :  pcf.ContactCurrencyInputSet) : void {
      def.onEnter(contact, address, contact.New)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 62, column 96
    function def_onEnter_17 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 62, column 96
    function def_onEnter_19 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function def_onEnter_2 (def :  pcf.ContactNameInputSet_company) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on PanelRef at ContactPanelSet.pcf: line 70, column 62
    function def_onEnter_22 (def :  pcf.AddressesPanelSet) : void {
      def.onEnter(contact,false, null, null)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function def_onEnter_4 (def :  pcf.ContactNameInputSet_person) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on InputSetRef (id=ContactCurrency) at ContactPanelSet.pcf: line 55, column 37
    function def_refreshVariables_14 (def :  pcf.ContactCurrencyInputSet) : void {
      def.refreshVariables(contact, address, contact.New)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 62, column 96
    function def_refreshVariables_18 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 62, column 96
    function def_refreshVariables_20 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'def' attribute on PanelRef at ContactPanelSet.pcf: line 70, column 62
    function def_refreshVariables_23 (def :  pcf.AddressesPanelSet) : void {
      def.refreshVariables(contact,false, null, null)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function def_refreshVariables_3 (def :  pcf.ContactNameInputSet_company) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'def' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function def_refreshVariables_5 (def :  pcf.ContactNameInputSet_person) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at ContactPanelSet.pcf: line 51, column 44
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      address.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=AddressDescription_Input) at ContactPanelSet.pcf: line 51, column 44
    function editable_7 () : java.lang.Boolean {
      return address.LinkedAddress == null
    }
    
    // 'initialValue' attribute on Variable at ContactPanelSet.pcf: line 14, column 30
    function initialValue_0 () : entity.Address {
      return contact.PrimaryAddress
    }
    
    // 'mode' attribute on InputSetRef at ContactPanelSet.pcf: line 62, column 96
    function mode_21 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'mode' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function mode_6 () : java.lang.Object {
      return contact.Subtype.Code
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at ContactPanelSet.pcf: line 51, column 44
    function valueRoot_10 () : java.lang.Object {
      return address
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at ContactPanelSet.pcf: line 51, column 44
    function value_8 () : java.lang.String {
      return address.Description
    }
    
    // 'visible' attribute on InputSetRef at ContactPanelSet.pcf: line 26, column 42
    function visible_1 () : java.lang.Boolean {
      return contact != null
    }
    
    // 'visible' attribute on Label at ContactPanelSet.pcf: line 58, column 96
    function visible_15 () : java.lang.Boolean {
      return contact != null and contact.Subtype != typekey.Contact.TC_USERCONTACT
    }
    
    property get address () : entity.Address {
      return getVariableValue("address", 0) as entity.Address
    }
    
    property set address ($arg :  entity.Address) {
      setVariableValue("address", 0, $arg)
    }
    
    property get contact () : Contact {
      return getRequireValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setRequireValue("contact", 0, $arg)
    }
    
    
  }
  
  
}