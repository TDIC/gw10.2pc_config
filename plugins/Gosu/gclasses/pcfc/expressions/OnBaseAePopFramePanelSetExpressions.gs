package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseAePopFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseAePopFramePanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseAePopFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseAePopFramePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // TemplatePanel at OnBaseAePopFramePanelSet.pcf: line 10, column 20
    function renderCall_0 (__writer :  java.io.Writer, __escaper :  gw.lang.parser.template.StringEscaper, __helper :  gw.api.web.template.TemplatePanelHelper) : void {
      pcfc.expressions.OnBaseAePopFramePanelSet_TemplatePanel1.render(__writer, __escaper, scrapeXml)
    }
    
    property get scrapeXml () : String[] {
      return getRequireValue("scrapeXml", 0) as String[]
    }
    
    property set scrapeXml ($arg :  String[]) {
      setRequireValue("scrapeXml", 0, $arg)
    }
    
    
  }
  
  
}