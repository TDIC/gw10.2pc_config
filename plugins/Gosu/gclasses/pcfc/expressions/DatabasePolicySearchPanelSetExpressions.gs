package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DatabasePolicySearchPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DatabasePolicySearchPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=GroupUser_TDIC) at DatabasePolicySearchPanelSet.pcf: line 311, column 59
    function action_193 () : void {
      searchCriteria.redistributeToSpecificUW_TDIC(searchResult, searchCriteria.SearchObjectType, eachGroupUser)
    }
    
    // 'confirmMessage' attribute on MenuItem (id=GroupUser_TDIC) at DatabasePolicySearchPanelSet.pcf: line 311, column 59
    function confirmMessage_194 () : java.lang.String {
      return "Are you sure that you want to redistribute " + (searchResult != null ? searchResult.Count : 0) + " items?\nItems in unassignable statuses (e.g. Quoted) will be ignored."
    }
    
    // 'label' attribute on MenuItem (id=GroupUser_TDIC) at DatabasePolicySearchPanelSet.pcf: line 311, column 59
    function label_195 () : java.lang.Object {
      return eachGroupUser.User.DisplayName
    }
    
    property get eachGroupUser () : GroupUser {
      return getIteratedValue(2) as GroupUser
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesMenuItem (id=SpecificUW_TDIC) at DatabasePolicySearchPanelSet.pcf: line 285, column 57
    function allCheckedRowsAction_188 (CheckedValues :  PolicyPeriodSummary[], CheckedValuesErrors :  java.util.Map) : void {
      searchCriteria.redistributeToSpecificUW_TDIC(CheckedValues.asQuery(), searchCriteria.SearchObjectType, eachGroupUser)
    }
    
    // 'label' attribute on CheckedValuesMenuItem (id=SpecificUW_TDIC) at DatabasePolicySearchPanelSet.pcf: line 285, column 57
    function label_187 () : java.lang.Object {
      return eachGroupUser.User.DisplayName
    }
    
    property get eachGroupUser () : GroupUser {
      return getIteratedValue(2) as GroupUser
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchDVExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on OrgInput (id=Producer_Input) at DatabasePolicySearchPanelSet.pcf: line 194, column 46
    function action_100 () : void {
      OrganizationSearchPopup.push(null, true)
    }
    
    // 'pickLocation' attribute on ProducerCodeInput (id=ProducerCode_Input) at ProducerCodeWidget.xml: line 2, column 156
    function action_108 () : void {
      ProducerCodeSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 9, column 49
    function action_117 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 12, column 49
    function action_119 () : void {
      pcf.UserSelectPopup.push()
    }
    
    // 'pickLocation' attribute on OrgInput (id=Producer_Input) at DatabasePolicySearchPanelSet.pcf: line 194, column 46
    function action_dest_101 () : pcf.api.Destination {
      return pcf.OrganizationSearchPopup.createDestination(null, true)
    }
    
    // 'pickLocation' attribute on ProducerCodeInput (id=ProducerCode_Input) at ProducerCodeWidget.xml: line 2, column 156
    function action_dest_109 () : pcf.api.Destination {
      return pcf.ProducerCodeSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 9, column 49
    function action_dest_118 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 12, column 49
    function action_dest_120 () : pcf.api.Destination {
      return pcf.UserSelectPopup.createDestination()
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function available_144 () : java.lang.Boolean {
      return searchCriteria.DateCriteria.DateSearchType == DateSearchType.TC_FROMLIST
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function available_153 () : java.lang.Boolean {
      return searchCriteria.DateCriteria.DateSearchType == DateSearchType.TC_ENTEREDRANGE
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 252, column 45
    function def_onEnter_183 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 65, column 60
    function def_onEnter_22 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.search.BasicNameOwner(policyNameAdapter),null)
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 65, column 60
    function def_onEnter_24 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.search.BasicNameOwner(policyNameAdapter),null)
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 81, column 128
    function def_onEnter_27 (def :  pcf.AddressOwnerAddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressCountryCityStatePostalCodeOwner (policyAddrAdapter))
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 90, column 60
    function def_onEnter_34 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.search.BasicNameOwner(policyNameAdapter))
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 90, column 60
    function def_onEnter_36 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.search.BasicNameOwner(policyNameAdapter))
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 252, column 45
    function def_refreshVariables_184 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 65, column 60
    function def_refreshVariables_23 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.search.BasicNameOwner(policyNameAdapter),null)
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 65, column 60
    function def_refreshVariables_25 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.search.BasicNameOwner(policyNameAdapter),null)
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 81, column 128
    function def_refreshVariables_28 (def :  pcf.AddressOwnerAddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressCountryCityStatePostalCodeOwner (policyAddrAdapter))
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 90, column 60
    function def_refreshVariables_35 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.search.BasicNameOwner(policyNameAdapter))
    }
    
    // 'def' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 90, column 60
    function def_refreshVariables_37 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.search.BasicNameOwner(policyNameAdapter))
    }
    
    // 'value' attribute on OrgInput (id=Producer_Input) at DatabasePolicySearchPanelSet.pcf: line 194, column 46
    function defaultSetter_104 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Producer = (__VALUE_TO_SET as entity.Organization)
    }
    
    // 'value' attribute on ProducerCodeInput (id=ProducerCode_Input) at ProducerCodeWidget.xml: line 2, column 156
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'value' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.User_TDIC = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function defaultSetter_138 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria.DateFieldToSearch = (__VALUE_TO_SET as typekey.DateFieldsToSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function defaultSetter_146 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria.DateRangeChoice = (__VALUE_TO_SET as typekey.DateRangeChoiceType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function defaultSetter_151 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria.DateSearchType = (__VALUE_TO_SET as typekey.DateSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function defaultSetter_155 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SearchObjectType = (__VALUE_TO_SET as gw.policy.PolicyPeriodSearchCriteria.PolicyPeriodSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function defaultSetter_161 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function defaultSetter_169 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateCriteria = (__VALUE_TO_SET as entity.DateCriteria)
    }
    
    // 'value' attribute on DateInput (id=AsOfDate_Input) at DatabasePolicySearchPanelSet.pcf: line 242, column 52
    function defaultSetter_175 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AsOfDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=legacyPolicyNumber_TDIC_Input) at DatabasePolicySearchPanelSet.pcf: line 248, column 61
    function defaultSetter_180 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LegacyPolicyNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Phone_Input) at DatabasePolicySearchPanelSet.pcf: line 87, column 57
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PrimaryInsuredPhone = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at DatabasePolicySearchPanelSet.pcf: line 103, column 48
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.OfficialId = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at DatabasePolicySearchPanelSet.pcf: line 111, column 51
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumberCriterion_TDIC_Input) at DatabasePolicySearchPanelSet.pcf: line 126, column 70
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=SubmissionNumber_Input) at DatabasePolicySearchPanelSet.pcf: line 133, column 70
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.JobNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.IncludeArchived = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at DatabasePolicySearchPanelSet.pcf: line 139, column 50
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AssignedRisk = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.UWCompany = (__VALUE_TO_SET as entity.UWCompany)
    }
    
    // 'value' attribute on TypeKeyInput (id=RejectReason_Input) at DatabasePolicySearchPanelSet.pcf: line 155, column 70
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.RejectReason = (__VALUE_TO_SET as typekey.ReasonCode)
    }
    
    // 'value' attribute on TypeKeyInput (id=NonRenewalCode_Input) at DatabasePolicySearchPanelSet.pcf: line 163, column 67
    function defaultSetter_78 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.NonRenewalCode = (__VALUE_TO_SET as typekey.NonRenewalCode)
    }
    
    // 'value' attribute on TypeKeyInput (id=AuditMethod_Input) at DatabasePolicySearchPanelSet.pcf: line 171, column 51
    function defaultSetter_84 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ActualAuditMethod = (__VALUE_TO_SET as typekey.AuditMethod)
    }
    
    // 'value' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Product = (__VALUE_TO_SET as gw.api.productmodel.Product)
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at DatabasePolicySearchPanelSet.pcf: line 187, column 47
    function defaultSetter_97 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.State = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'editable' attribute on OrgInput (id=Producer_Input) at DatabasePolicySearchPanelSet.pcf: line 194, column 46
    function editable_102 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser
    }
    
    // 'helpText' attribute on TextInput (id=Phone_Input) at DatabasePolicySearchPanelSet.pcf: line 87, column 57
    function helpText_29 () : java.lang.String {
      return DisplayKey.get("Java.PhoneUtil.Example.ToolTip", User.util.CurrentUser.UserDefaultPhoneCountry, gw.api.util.PhoneUtil.getExampleNumberWithExtension(User.util.CurrentUser.UserDefaultPhoneCountry))
    }
    
    // 'initialValue' attribute on Variable at DatabasePolicySearchPanelSet.pcf: line 22, column 58
    function initialValue_0 () : java.util.List<entity.UWCompany> {
      return UWCompany.finder.findUWCompanies().toList()
    }
    
    // 'initialValue' attribute on Variable at DatabasePolicySearchPanelSet.pcf: line 27, column 60
    function initialValue_1 () : gw.globalization.PolicySearchNameAdapter {
      return new gw.globalization.PolicySearchNameAdapter(searchCriteria)
    }
    
    // 'initialValue' attribute on Variable at DatabasePolicySearchPanelSet.pcf: line 32, column 63
    function initialValue_2 () : gw.globalization.PolicySearchAddressAdapter {
      return new gw.globalization.PolicySearchAddressAdapter(searchCriteria)
    }
    
    // 'initialValue' attribute on Variable at DatabasePolicySearchPanelSet.pcf: line 36, column 56
    function initialValue_3 () : gw.pcf.UniqueProductDisplayNameMaker {
      return new gw.pcf.UniqueProductDisplayNameMaker()
    }
    
    // 'inputConversion' attribute on ProducerCodeInput (id=ProducerCode_Input) at ProducerCodeWidget.xml: line 2, column 156
    function inputConversion_110 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.ProducerCodePickerUtil.convertValueFromString(VALUE, searchCriteria.ProducerCode)
    }
    
    // 'mode' attribute on InputSetRef at DatabasePolicySearchPanelSet.pcf: line 65, column 60
    function mode_26 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'onChange' attribute on PostOnChange at DatabasePolicySearchPanelSet.pcf: line 58, column 68
    function onChange_14 () : void {
      gw.api.util.SearchUtil.resetResultOnly()
    }
    
    // 'optionLabel' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function optionLabel_132 (VALUE :  entity.User) : java.lang.String {
      return VALUE as String
    }
    
    // 'optionLabel' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function optionLabel_8 (VALUE :  java.lang.Boolean) : java.lang.String {
      return gw.policy.PolicyPeriodSearchCriteria.getIncludeArchivedLabel(VALUE)
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function optionLabel_91 (VALUE :  gw.api.productmodel.Product) : java.lang.String {
      return uniqueProductDisplayNameMaker.make(VALUE)
    }
    
    // 'valueRange' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function valueRange_123 () : java.lang.Object {
      return entity.User.util.getUsersInCurrentUsersGroup()
    }
    
    // 'valueRange' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function valueRange_133 () : java.lang.Object {
      return searchCriteria.getUnderwriters()
    }
    
    // 'valueRange' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function valueRange_140 () : java.lang.Object {
      return searchCriteria.SearchObjectType.DateFieldsSearchType
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function valueRange_18 () : java.lang.Object {
      return gw.policy.PolicyPeriodSearchCriteria.PolicyPeriodSearchType.values()
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function valueRange_66 () : java.lang.Object {
      return uwCompanies
    }
    
    // 'valueRange' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function valueRange_9 () : java.lang.Object {
      return {false, true}
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function valueRange_92 () : java.lang.Object {
      return uniqueProductDisplayNameMaker.Products
    }
    
    // 'value' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function valueRoot_139 () : java.lang.Object {
      return searchCriteria.DateCriteria
    }
    
    // 'value' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function valueRoot_7 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on OrgInput (id=Producer_Input) at DatabasePolicySearchPanelSet.pcf: line 194, column 46
    function value_103 () : entity.Organization {
      return searchCriteria.Producer
    }
    
    // 'value' attribute on ProducerCodeInput (id=ProducerCode_Input) at ProducerCodeWidget.xml: line 2, column 156
    function value_111 () : entity.ProducerCode {
      return searchCriteria.ProducerCode
    }
    
    // 'value' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function value_121 () : entity.User {
      return searchCriteria.User_TDIC
    }
    
    // 'value' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function value_137 () : typekey.DateFieldsToSearchType {
      return searchCriteria.DateCriteria.DateFieldToSearch
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_145 () : typekey.DateRangeChoiceType {
      return searchCriteria.DateCriteria.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_149 () : java.lang.Object {
      return searchCriteria.DateCriteria.DateRangeChoice
    }
    
    // 'value' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function value_15 () : gw.policy.PolicyPeriodSearchCriteria.PolicyPeriodSearchType {
      return searchCriteria.SearchObjectType
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_150 () : typekey.DateSearchType {
      return searchCriteria.DateCriteria.DateSearchType
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_154 () : java.util.Date {
      return searchCriteria.DateCriteria.StartDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_160 () : java.util.Date {
      return searchCriteria.DateCriteria.EndDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateRangeForJobSearch_Input) at DatabasePolicySearchPanelSet.pcf: line 235, column 31
    function value_168 () : entity.DateCriteria {
      return searchCriteria.DateCriteria
    }
    
    // 'value' attribute on DateInput (id=AsOfDate_Input) at DatabasePolicySearchPanelSet.pcf: line 242, column 52
    function value_174 () : java.util.Date {
      return searchCriteria.AsOfDate
    }
    
    // 'value' attribute on TextInput (id=legacyPolicyNumber_TDIC_Input) at DatabasePolicySearchPanelSet.pcf: line 248, column 61
    function value_179 () : java.lang.String {
      return searchCriteria.LegacyPolicyNumber_TDIC
    }
    
    // 'value' attribute on TextInput (id=Phone_Input) at DatabasePolicySearchPanelSet.pcf: line 87, column 57
    function value_30 () : java.lang.String {
      return searchCriteria.PrimaryInsuredPhone
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at DatabasePolicySearchPanelSet.pcf: line 103, column 48
    function value_39 () : java.lang.String {
      return searchCriteria.OfficialId
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at DatabasePolicySearchPanelSet.pcf: line 111, column 51
    function value_43 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=PolicyNumberCriterion_TDIC_Input) at DatabasePolicySearchPanelSet.pcf: line 126, column 70
    function value_48 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function value_5 () : java.lang.Boolean {
      return searchCriteria.IncludeArchived
    }
    
    // 'value' attribute on TextInput (id=SubmissionNumber_Input) at DatabasePolicySearchPanelSet.pcf: line 133, column 70
    function value_54 () : java.lang.String {
      return searchCriteria.JobNumber
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at DatabasePolicySearchPanelSet.pcf: line 139, column 50
    function value_59 () : java.lang.Boolean {
      return searchCriteria.AssignedRisk
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function value_63 () : entity.UWCompany {
      return searchCriteria.UWCompany
    }
    
    // 'value' attribute on TypeKeyInput (id=RejectReason_Input) at DatabasePolicySearchPanelSet.pcf: line 155, column 70
    function value_71 () : typekey.ReasonCode {
      return searchCriteria.RejectReason
    }
    
    // 'value' attribute on TypeKeyInput (id=NonRenewalCode_Input) at DatabasePolicySearchPanelSet.pcf: line 163, column 67
    function value_77 () : typekey.NonRenewalCode {
      return searchCriteria.NonRenewalCode
    }
    
    // 'value' attribute on TypeKeyInput (id=AuditMethod_Input) at DatabasePolicySearchPanelSet.pcf: line 171, column 51
    function value_83 () : typekey.AuditMethod {
      return searchCriteria.ActualAuditMethod
    }
    
    // 'value' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function value_88 () : gw.api.productmodel.Product {
      return searchCriteria.Product
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at DatabasePolicySearchPanelSet.pcf: line 187, column 47
    function value_96 () : typekey.Jurisdiction {
      return searchCriteria.State
    }
    
    // 'valueRange' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.lang.Boolean[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_124 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_124 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_124 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function verifyValueRangeIsAllowedType_134 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function verifyValueRangeIsAllowedType_134 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function verifyValueRangeIsAllowedType_134 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function verifyValueRangeIsAllowedType_141 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function verifyValueRangeIsAllowedType_141 ($$arg :  typekey.DateFieldsToSearchType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function verifyValueRangeIsAllowedType_19 ($$arg :  gw.policy.PolicyPeriodSearchCriteria.PolicyPeriodSearchType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function verifyValueRangeIsAllowedType_67 ($$arg :  entity.UWCompany[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function verifyValueRangeIsAllowedType_67 ($$arg :  gw.api.database.IQueryBeanResult<entity.UWCompany>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function verifyValueRangeIsAllowedType_67 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function verifyValueRangeIsAllowedType_93 ($$arg :  gw.api.productmodel.Product[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function verifyValueRangeIsAllowedType_93 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function verifyValueRange_11 () : void {
      var __valueRangeArg = {false, true}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'valueRange' attribute on UserInput (id=User_TDIC_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRange_125 () : void {
      var __valueRangeArg = entity.User.util.getUsersInCurrentUsersGroup()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_124(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=uwUser_Input) at DatabasePolicySearchPanelSet.pcf: line 215, column 37
    function verifyValueRange_135 () : void {
      var __valueRangeArg = searchCriteria.getUnderwriters()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_134(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DateFieldToSearchType_Input) at DatabasePolicySearchPanelSet.pcf: line 228, column 59
    function verifyValueRange_142 () : void {
      var __valueRangeArg = searchCriteria.SearchObjectType.DateFieldsSearchType
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_141(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at DatabasePolicySearchPanelSet.pcf: line 56, column 85
    function verifyValueRange_20 () : void {
      var __valueRangeArg = gw.policy.PolicyPeriodSearchCriteria.PolicyPeriodSearchType.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at DatabasePolicySearchPanelSet.pcf: line 147, column 43
    function verifyValueRange_68 () : void {
      var __valueRangeArg = uwCompanies
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_67(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductName_Input) at DatabasePolicySearchPanelSet.pcf: line 180, column 54
    function verifyValueRange_94 () : void {
      var __valueRangeArg = uniqueProductDisplayNameMaker.Products
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_93(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at DatabasePolicySearchPanelSet.pcf: line 221, column 55
    function visible_172 () : java.lang.Boolean {
      return not searchCriteria.PolicySearch
    }
    
    // 'visible' attribute on DateInput (id=AsOfDate_Input) at DatabasePolicySearchPanelSet.pcf: line 242, column 52
    function visible_173 () : java.lang.Boolean {
      return searchCriteria.PolicySearch
    }
    
    // 'visible' attribute on RangeInput (id=IncludeArchived_Input) at DatabasePolicySearchPanelSet.pcf: line 46, column 73
    function visible_4 () : java.lang.Boolean {
      return gw.api.archive.PCArchivingUtil.isArchiveEnabled()
    }
    
    // 'visible' attribute on TextInput (id=PolicyNumberCriterion_TDIC_Input) at DatabasePolicySearchPanelSet.pcf: line 126, column 70
    function visible_47 () : java.lang.Boolean {
      return searchCriteria.SearchObjectType != Submission
    }
    
    // 'visible' attribute on TextInput (id=SubmissionNumber_Input) at DatabasePolicySearchPanelSet.pcf: line 133, column 70
    function visible_53 () : java.lang.Boolean {
      return searchCriteria.SearchObjectType == Submission
    }
    
    // 'visible' attribute on TypeKeyInput (id=NonRenewalCode_Input) at DatabasePolicySearchPanelSet.pcf: line 163, column 67
    function visible_76 () : java.lang.Boolean {
      return searchCriteria.SearchObjectType == Renewal
    }
    
    // 'visible' attribute on TypeKeyInput (id=AuditMethod_Input) at DatabasePolicySearchPanelSet.pcf: line 171, column 51
    function visible_82 () : java.lang.Boolean {
      return searchCriteria.AuditSearch
    }
    
    property get policyAddrAdapter () : gw.globalization.PolicySearchAddressAdapter {
      return getVariableValue("policyAddrAdapter", 2) as gw.globalization.PolicySearchAddressAdapter
    }
    
    property set policyAddrAdapter ($arg :  gw.globalization.PolicySearchAddressAdapter) {
      setVariableValue("policyAddrAdapter", 2, $arg)
    }
    
    property get policyNameAdapter () : gw.globalization.PolicySearchNameAdapter {
      return getVariableValue("policyNameAdapter", 2) as gw.globalization.PolicySearchNameAdapter
    }
    
    property set policyNameAdapter ($arg :  gw.globalization.PolicySearchNameAdapter) {
      setVariableValue("policyNameAdapter", 2, $arg)
    }
    
    property get uniqueProductDisplayNameMaker () : gw.pcf.UniqueProductDisplayNameMaker {
      return getVariableValue("uniqueProductDisplayNameMaker", 2) as gw.pcf.UniqueProductDisplayNameMaker
    }
    
    property set uniqueProductDisplayNameMaker ($arg :  gw.pcf.UniqueProductDisplayNameMaker) {
      setVariableValue("uniqueProductDisplayNameMaker", 2, $arg)
    }
    
    property get uwCompanies () : java.util.List<entity.UWCompany> {
      return getVariableValue("uwCompanies", 2) as java.util.List<entity.UWCompany>
    }
    
    property set uwCompanies ($arg :  java.util.List<entity.UWCompany>) {
      setVariableValue("uwCompanies", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DatabasePolicySearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DatabasePolicySearchPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=PrintMe) at DatabasePolicySearchPanelSet.pcf: line 265, column 74
    function action_185 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions( "PolicySearch_ResultsLV" )
    }
    
    // 'action' attribute on MenuItem (id=RedistributeRoundRobin_TDIC) at DatabasePolicySearchPanelSet.pcf: line 298, column 81
    function action_191 () : void {
      searchCriteria.redistributeRoundRobin_TDIC(searchResult, searchCriteria.SearchObjectType, gw.api.database.Queries.findByIdOrPublicId<entity.Group>("pc-tdic:17"))
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesMenuItem (id=RedistributeToAllUnderwriters_TDIC) at DatabasePolicySearchPanelSet.pcf: line 275, column 81
    function allCheckedRowsAction_186 (CheckedValues :  PolicyPeriodSummary[], CheckedValuesErrors :  java.util.Map) : void {
      searchCriteria.redistributeRoundRobin_TDIC(CheckedValues.asQuery(), searchCriteria.SearchObjectType, gw.api.database.Queries.findByIdOrPublicId<entity.Group>("pc-tdic:17"))
    }
    
    // 'available' attribute on ToolbarButton (id=ReassignmentAllButton) at DatabasePolicySearchPanelSet.pcf: line 293, column 64
    function available_197 () : java.lang.Boolean {
      return searchResult == null ? false : (searchResult.Count > 0 ? true : false) 
    }
    
    // 'confirmMessage' attribute on MenuItem (id=RedistributeRoundRobin_TDIC) at DatabasePolicySearchPanelSet.pcf: line 298, column 81
    function confirmMessage_192 () : java.lang.String {
      return "Are you sure that you want to redistribute " + (searchResult != null ? searchResult.Count : 0) + " item(s)?\nItems in unassignable statuses (e.g. Quoted) will be ignored."
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_199 (def :  pcf.PolicySearch_ResultsLV_Audit) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_201 (def :  pcf.PolicySearch_ResultsLV_Cancellation) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_203 (def :  pcf.PolicySearch_ResultsLV_Issuance) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_205 (def :  pcf.PolicySearch_ResultsLV_MigratedRenewal) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_207 (def :  pcf.PolicySearch_ResultsLV_Policy) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_209 (def :  pcf.PolicySearch_ResultsLV_PolicyChange) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_211 (def :  pcf.PolicySearch_ResultsLV_Reinstatement) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_213 (def :  pcf.PolicySearch_ResultsLV_Renewal) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_215 (def :  pcf.PolicySearch_ResultsLV_Rewrite) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_217 (def :  pcf.PolicySearch_ResultsLV_RewriteNewAccount) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_onEnter_219 (def :  pcf.PolicySearch_ResultsLV_Submission) : void {
      def.onEnter(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_200 (def :  pcf.PolicySearch_ResultsLV_Audit) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_202 (def :  pcf.PolicySearch_ResultsLV_Cancellation) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_204 (def :  pcf.PolicySearch_ResultsLV_Issuance) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_206 (def :  pcf.PolicySearch_ResultsLV_MigratedRenewal) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_208 (def :  pcf.PolicySearch_ResultsLV_Policy) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_210 (def :  pcf.PolicySearch_ResultsLV_PolicyChange) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_212 (def :  pcf.PolicySearch_ResultsLV_Reinstatement) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_214 (def :  pcf.PolicySearch_ResultsLV_Renewal) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_216 (def :  pcf.PolicySearch_ResultsLV_Rewrite) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_218 (def :  pcf.PolicySearch_ResultsLV_RewriteNewAccount) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'def' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function def_refreshVariables_220 (def :  pcf.PolicySearch_ResultsLV_Submission) : void {
      def.refreshVariables(searchResult)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DatabasePolicySearchPanelSet.pcf: line 16, column 87
    function maxSearchResults_222 () : java.lang.Object {
      return gw.policy.PolicyPeriodSearchCriteria.MaxSearchResults
    }
    
    // 'mode' attribute on PanelRef at DatabasePolicySearchPanelSet.pcf: line 257, column 45
    function mode_221 () : java.lang.Object {
      return searchCriteria.ResultsLVMode
    }
    
    // 'searchCriteria' attribute on SearchPanel at DatabasePolicySearchPanelSet.pcf: line 16, column 87
    function searchCriteria_224 () : gw.policy.PolicyPeriodSearchCriteria {
      return new gw.policy.PolicyPeriodSearchCriteria() { :Secure = true, :Producer = User.util.CurrentUser.Producer, /*:FirstNameExact = false, :LastNameExact = false, :CompanyNameExact = false,*/ :TDIC_LastNameContains = true, :PermissiveSearch = false}
    }
    
    // 'search' attribute on SearchPanel at DatabasePolicySearchPanelSet.pcf: line 16, column 87
    function search_223 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'value' attribute on CheckedValuesMenuItemIterator (id=GroupUser_TDIC) at DatabasePolicySearchPanelSet.pcf: line 281, column 46
    function value_189 () : entity.GroupUser[] {
      return gw.api.database.Query.make(entity.GroupUser).compare("Group", gw.api.database.Relop.Equals, gw.api.database.Queries.findByIdOrPublicId<entity.Group>("pc-tdic:17")).select().toTypedArray()
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=ReassignmentButton) at DatabasePolicySearchPanelSet.pcf: line 271, column 64
    function visible_190 () : java.lang.Boolean {
      return perm.System.userroleassignmentbulkassign
    }
    
    property get searchCriteria () : gw.policy.PolicyPeriodSearchCriteria {
      return getCriteriaValue(1) as gw.policy.PolicyPeriodSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.policy.PolicyPeriodSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get searchResult () : gw.api.database.IQueryBeanResult<PolicyPeriodSummary> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicyPeriodSummary>
    }
    
    
  }
  
  
}