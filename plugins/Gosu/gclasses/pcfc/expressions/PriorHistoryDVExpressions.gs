package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/PriorHistoryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PriorHistoryDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/PriorHistoryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PriorHistoryDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function action_15 () : void {
      TDIC_CarrierSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function action_dest_16 () : pcf.api.Destination {
      return pcf.TDIC_CarrierSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=AnnualPremium_Cell) at PriorHistoryDV.pcf: line 84, column 52
    function currency_45 () : typekey.Currency {
      return policyPeriod.PreferredSettlementCurrency
    }
    
    // 'value' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.Carrier = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PriorHistoryDV.pcf: line 63, column 51
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PriorHistoryDV.pcf: line 69, column 52
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PriorHistoryDV.pcf: line 75, column 53
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AnnualPremium_Cell) at PriorHistoryDV.pcf: line 84, column 52
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.AnnualPremium = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=ExpMod_Cell) at PriorHistoryDV.pcf: line 114, column 56
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      priorPolicy.ExpMod = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'valueRange' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function valueRange_25 () : java.lang.Object {
      return LossHistoryCarrier_TDIC.AllTypeKeys*.DisplayName
    }
    
    // 'value' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function valueRoot_19 () : java.lang.Object {
      return priorPolicy
    }
    
    // 'value' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function value_17 () : java.lang.String {
      return priorPolicy.Carrier
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PriorHistoryDV.pcf: line 63, column 51
    function value_30 () : java.lang.String {
      return priorPolicy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PriorHistoryDV.pcf: line 69, column 52
    function value_34 () : java.util.Date {
      return priorPolicy.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PriorHistoryDV.pcf: line 75, column 53
    function value_38 () : java.util.Date {
      return priorPolicy.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AnnualPremium_Cell) at PriorHistoryDV.pcf: line 84, column 52
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return priorPolicy.AnnualPremium
    }
    
    // 'value' attribute on TextCell (id=ExpMod_Cell) at PriorHistoryDV.pcf: line 114, column 56
    function value_47 () : java.math.BigDecimal {
      return priorPolicy.ExpMod
    }
    
    // 'valueRange' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function verifyValueRange_27 () : void {
      var __valueRangeArg = LossHistoryCarrier_TDIC.AllTypeKeys*.DisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'visible' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function visible_20 () : java.lang.Boolean {
      return policyPeriod.GLLineExists or policyPeriod.BOPLineExists
    }
    
    // 'visible' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function visible_28 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on TextCell (id=ExpMod_Cell) at PriorHistoryDV.pcf: line 114, column 56
    function visible_50 () : java.lang.Boolean {
      return policyPeriod.HasWorkersComp
    }
    
    property get priorPolicy () : entity.PriorPolicy {
      return getIteratedValue(1) as entity.PriorPolicy
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/submission/PriorHistoryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PriorHistoryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function sortValue_0 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.Carrier
    }
    
    // 'value' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function sortValue_2 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.Carrier
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PriorHistoryDV.pcf: line 63, column 51
    function sortValue_4 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PriorHistoryDV.pcf: line 69, column 52
    function sortValue_5 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PriorHistoryDV.pcf: line 75, column 53
    function sortValue_6 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AnnualPremium_Cell) at PriorHistoryDV.pcf: line 84, column 52
    function sortValue_7 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.AnnualPremium
    }
    
    // 'value' attribute on TextCell (id=ExpMod_Cell) at PriorHistoryDV.pcf: line 114, column 56
    function sortValue_8 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.ExpMod
    }
    
    // '$$sumValue' attribute on RowIterator at PriorHistoryDV.pcf: line 84, column 52
    function sumValueRoot_13 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy
    }
    
    // 'footerSumValue' attribute on RowIterator at PriorHistoryDV.pcf: line 84, column 52
    function sumValue_12 (priorPolicy :  entity.PriorPolicy) : java.lang.Object {
      return priorPolicy.AnnualPremium
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at PriorHistoryDV.pcf: line 29, column 46
    function toCreateAndAdd_52 () : entity.PriorPolicy {
      var pp = new PriorPolicy(); policyPeriod.Policy.addToPriorPolicies(pp); return pp;
    }
    
    // 'toRemove' attribute on RowIterator at PriorHistoryDV.pcf: line 29, column 46
    function toRemove_53 (priorPolicy :  entity.PriorPolicy) : void {
      policyPeriod.Policy.removeFromPriorPolicies(priorPolicy)
    }
    
    // 'value' attribute on RowIterator at PriorHistoryDV.pcf: line 29, column 46
    function value_54 () : entity.PriorPolicy[] {
      return policyPeriod.Policy.PriorPolicies
    }
    
    // 'visible' attribute on PickerCell (id=Carrier_PLCP_Cell) at PriorHistoryDV.pcf: line 48, column 84
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.GLLineExists or policyPeriod.BOPLineExists
    }
    
    // 'visible' attribute on RangeCell (id=Carrier_Cell) at PriorHistoryDV.pcf: line 57, column 55
    function visible_3 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on TextCell (id=ExpMod_Cell) at PriorHistoryDV.pcf: line 114, column 56
    function visible_9 () : java.lang.Boolean {
      return policyPeriod.HasWorkersComp
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}