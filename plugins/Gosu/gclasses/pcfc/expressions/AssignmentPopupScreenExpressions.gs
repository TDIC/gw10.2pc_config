package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/assignment/AssignmentPopupScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AssignmentPopupScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/assignment/AssignmentPopupScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssignmentPopupScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AssignmentPopupScreen_CancelButton) at AssignmentPopupScreen.pcf: line 18, column 23
    function action_0 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function def_onEnter_2 (def :  pcf.FailedAssignmentsLV_Activity) : void {
      def.onEnter(AssignmentPopup)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function def_onEnter_4 (def :  pcf.FailedAssignmentsLV_default) : void {
      def.onEnter(AssignmentPopup)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function def_refreshVariables_3 (def :  pcf.FailedAssignmentsLV_Activity) : void {
      def.refreshVariables(AssignmentPopup)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function def_refreshVariables_5 (def :  pcf.FailedAssignmentsLV_default) : void {
      def.refreshVariables(AssignmentPopup)
    }
    
    // 'mode' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function mode_6 () : java.lang.Object {
      return AssignmentPopup.AssignableType.RelativeName
    }
    
    // 'visible' attribute on PanelRef at AssignmentPopupScreen.pcf: line 23, column 45
    function visible_1 () : java.lang.Boolean {
      return AssignmentPopup.hasErrors()
    }
    
    property get AssignmentPopup () : gw.api.assignment.AssignmentPopup {
      return getRequireValue("AssignmentPopup", 0) as gw.api.assignment.AssignmentPopup
    }
    
    property set AssignmentPopup ($arg :  gw.api.assignment.AssignmentPopup) {
      setRequireValue("AssignmentPopup", 0, $arg)
    }
    
    property get activities () : entity.Activity[] {
      return getRequireValue("activities", 0) as entity.Activity[]
    }
    
    property set activities ($arg :  entity.Activity[]) {
      setRequireValue("activities", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/assignment/AssignmentPopupScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends AssignmentPopupScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=InternalSearchResults) at AssignmentPopupScreen.pcf: line 40, column 154
    function def_onEnter_10 (def :  pcf.AssignmentUserLV) : void {
      def.onEnter(SearchResult.Users)
    }
    
    // 'def' attribute on PanelRef (id=ExternalSearchResults) at AssignmentPopupScreen.pcf: line 46, column 150
    function def_onEnter_13 (def :  pcf.AssignmentUserLV) : void {
      def.onEnter(SearchResult?.getUsersForExternalUser(SearchCriteria) as gw.api.database.IQueryBeanResult<GroupUser>)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 51, column 112
    function def_onEnter_16 (def :  pcf.AssignmentGroupLV) : void {
      def.onEnter(SearchResult.Groups)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 56, column 112
    function def_onEnter_19 (def :  pcf.AssignmentQueueLV) : void {
      def.onEnter(SearchResult.Queues)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 36, column 93
    function def_onEnter_7 (def :  pcf.AssignmentPopupDV) : void {
      def.onEnter(AssignmentPopup, SearchResult, SearchCriteria, activities)
    }
    
    // 'def' attribute on PanelRef (id=InternalSearchResults) at AssignmentPopupScreen.pcf: line 40, column 154
    function def_refreshVariables_11 (def :  pcf.AssignmentUserLV) : void {
      def.refreshVariables(SearchResult.Users)
    }
    
    // 'def' attribute on PanelRef (id=ExternalSearchResults) at AssignmentPopupScreen.pcf: line 46, column 150
    function def_refreshVariables_14 (def :  pcf.AssignmentUserLV) : void {
      def.refreshVariables(SearchResult?.getUsersForExternalUser(SearchCriteria) as gw.api.database.IQueryBeanResult<GroupUser>)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 51, column 112
    function def_refreshVariables_17 (def :  pcf.AssignmentGroupLV) : void {
      def.refreshVariables(SearchResult.Groups)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 56, column 112
    function def_refreshVariables_20 (def :  pcf.AssignmentQueueLV) : void {
      def.refreshVariables(SearchResult.Queues)
    }
    
    // 'def' attribute on PanelRef at AssignmentPopupScreen.pcf: line 36, column 93
    function def_refreshVariables_8 (def :  pcf.AssignmentPopupDV) : void {
      def.refreshVariables(AssignmentPopup, SearchResult, SearchCriteria, activities)
    }
    
    // 'searchCriteria' attribute on SearchPanel at AssignmentPopupScreen.pcf: line 34, column 68
    function searchCriteria_22 () : gw.api.assignment.AssignmentSearchCriteria {
      return new gw.api.assignment.AssignmentSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at AssignmentPopupScreen.pcf: line 34, column 68
    function search_21 () : java.lang.Object {
      return SearchCriteria.performSearch()
    }
    
    // 'visible' attribute on PanelRef (id=ExternalSearchResults) at AssignmentPopupScreen.pcf: line 46, column 150
    function visible_12 () : java.lang.Boolean {
      return (SearchCriteria.SearchType == TC_USER) and (AssignmentPopup.SelectionType == TC_FROMSEARCH) and User.util.CurrentUser.ExternalUser
    }
    
    // 'visible' attribute on PanelRef at AssignmentPopupScreen.pcf: line 51, column 112
    function visible_15 () : java.lang.Boolean {
      return (SearchCriteria.SearchType == TC_GROUP) and (AssignmentPopup.SelectionType == TC_FROMSEARCH)
    }
    
    // 'visible' attribute on PanelRef at AssignmentPopupScreen.pcf: line 56, column 112
    function visible_18 () : java.lang.Boolean {
      return (SearchCriteria.SearchType == TC_QUEUE) and (AssignmentPopup.SelectionType == TC_FROMSEARCH)
    }
    
    // 'visible' attribute on PanelRef (id=InternalSearchResults) at AssignmentPopupScreen.pcf: line 40, column 154
    function visible_9 () : java.lang.Boolean {
      return (SearchCriteria.SearchType == TC_USER) and (AssignmentPopup.SelectionType == TC_FROMSEARCH) and not User.util.CurrentUser.ExternalUser
    }
    
    property get SearchCriteria () : gw.api.assignment.AssignmentSearchCriteria {
      return getCriteriaValue(1) as gw.api.assignment.AssignmentSearchCriteria
    }
    
    property set SearchCriteria ($arg :  gw.api.assignment.AssignmentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get SearchResult () : gw.api.assignment.AssignmentSearchResult {
      return getResultsValue(1) as gw.api.assignment.AssignmentSearchResult
    }
    
    
  }
  
  
}