package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RemoveSplitPeriodPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Period_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 86, column 53
    function valueRoot_11 () : java.lang.Object {
      return wc7JurSplitPeriod.StartDate
    }
    
    // 'value' attribute on TextCell (id=type_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 94, column 36
    function valueRoot_14 () : java.lang.Object {
      return wc7JurSplitPeriod
    }
    
    // 'value' attribute on TextCell (id=Period_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 86, column 53
    function value_10 () : java.lang.String {
      return wc7JurSplitPeriod.StartDate.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=type_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 94, column 36
    function value_13 () : typekey.RPSDType {
      return wc7JurSplitPeriod.Type
    }
    
    property get wc7JurSplitPeriod () : entity.WC7RatingPeriodStartDate {
      return getIteratedValue(2) as entity.WC7RatingPeriodStartDate
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'condition' attribute on ToolbarFlag at WC7RemoveSplitPeriodPopup.pcf: line 42, column 33
    function condition_3 () : java.lang.Boolean {
      return covJuris.CanRemove
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 48, column 51
    function valueRoot_5 () : java.lang.Object {
      return covJuris
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 48, column 51
    function value_4 () : typekey.Jurisdiction {
      return covJuris.Jurisdiction
    }
    
    property get covJuris () : entity.WC7Jurisdiction {
      return getIteratedValue(2) as entity.WC7Jurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends WC7RemoveSplitPeriodPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewPanel (id=SplitPeriodsPerStateConfig_LV) at WC7RemoveSplitPeriodPopup.pcf: line 31, column 26
    function available_8 () : java.lang.Boolean {
      return selectedJurisdiction != null 
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RemoveSplitPeriodPopup.pcf: line 39, column 30
    function sortBy_1 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 48, column 51
    function sortValue_2 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=type_Cell) at WC7RemoveSplitPeriodPopup.pcf: line 94, column 36
    function sortValue_9 (wc7JurSplitPeriod :  entity.WC7RatingPeriodStartDate) : java.lang.Object {
      return wc7JurSplitPeriod.Type
    }
    
    // 'toRemove' attribute on RowIterator (id=WC7JurSplitPeriod) at WC7RemoveSplitPeriodPopup.pcf: line 79, column 65
    function toRemove_16 (wc7JurSplitPeriod :  entity.WC7RatingPeriodStartDate) : void {
      wcLine.removeRPSD( wc7JurSplitPeriod.StartDate, wc7JurSplitPeriod.Type, selectedJurisdiction )
    }
    
    // 'value' attribute on RowIterator (id=WC7JurSplitPeriod) at WC7RemoveSplitPeriodPopup.pcf: line 79, column 65
    function value_17 () : entity.WC7RatingPeriodStartDate[] {
      return selectedJurisdiction.WC7RatingPeriodStartDates.where( \ elt ->  elt.Type == RPSDType.TC_FORCEDRERATING or elt.Type == RPSDType.TC_LATEMOD).sortBy(\elt -> elt.StartDate)
    }
    
    // 'value' attribute on RowIterator at WC7RemoveSplitPeriodPopup.pcf: line 36, column 50
    function value_7 () : entity.WC7Jurisdiction[] {
      return jurisdictions
    }
    
    property get selectedJurisdiction () : WC7Jurisdiction {
      return getCurrentSelection(1) as WC7Jurisdiction
    }
    
    property set selectedJurisdiction ($arg :  WC7Jurisdiction) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RemoveSplitPeriodPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RemoveSplitPeriodPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (wcLine :  productmodel.WC7Line, jurisdictions :  WC7Jurisdiction[]) : int {
      return 0
    }
    
    // EditButtons at WC7RemoveSplitPeriodPopup.pcf: line 23, column 34
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.WC7RemoveSplitPeriodPopup {
      return super.CurrentLocation as pcf.WC7RemoveSplitPeriodPopup
    }
    
    property get jurisdictions () : WC7Jurisdiction[] {
      return getVariableValue("jurisdictions", 0) as WC7Jurisdiction[]
    }
    
    property set jurisdictions ($arg :  WC7Jurisdiction[]) {
      setVariableValue("jurisdictions", 0, $arg)
    }
    
    property get wcLine () : productmodel.WC7Line {
      return getVariableValue("wcLine", 0) as productmodel.WC7Line
    }
    
    property set wcLine ($arg :  productmodel.WC7Line) {
      setVariableValue("wcLine", 0, $arg)
    }
    
    
  }
  
  
}