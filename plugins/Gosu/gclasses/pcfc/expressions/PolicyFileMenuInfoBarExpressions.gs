package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.util.Date
@javax.annotation.Generated("config/web/pcf/policyfile/PolicyFileMenuInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFileMenuInfoBarExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/PolicyFileMenuInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFileMenuInfoBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at PolicyFileMenuInfoBar.pcf: line 32, column 58
    function action_5 () : void {
      AccountFileForward.go(policyPeriod.Policy.Account)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at PolicyFileMenuInfoBar.pcf: line 32, column 58
    function action_dest_6 () : pcf.api.Destination {
      return pcf.AccountFileForward.createDestination(policyPeriod.Policy.Account)
    }
    
    // 'icon' attribute on InfoBarElement (id=LOBLabel) at PolicyFileMenuInfoBar.pcf: line 19, column 41
    function icon_1 () : java.lang.String {
      return policyPeriod.Policy.Product.Icon
    }
    
    // 'icon' attribute on InfoBarElement (id=StatusAndExpDate) at PolicyFileMenuInfoBar.pcf: line 45, column 108
    function icon_12 () : java.lang.String {
      return policyPeriod.Canceled or showRenewalWarning_TDIC ? "warning_tdic.gif" : null
    }
    
    // 'label' attribute on InfoBarElement (id=LOBLabel) at PolicyFileMenuInfoBar.pcf: line 19, column 41
    function label_0 () : java.lang.Object {
      return displayProductOffering()
    }
    
    // 'label' attribute on InfoBarElement (id=StatusAndExpDate) at PolicyFileMenuInfoBar.pcf: line 45, column 108
    function label_10 () : java.lang.Object {
      return formatStatusLabel(policyPeriod)
    }
    
    // 'valueIcons' attribute on InfoBarElement (id=StatusAndExpDate) at PolicyFileMenuInfoBar.pcf: line 45, column 108
    function valueIcons_11 () : java.lang.Object {
      return policyPeriod.Canceled or showRenewalWarning_TDIC ? "warning_tdic.gif" : null
    }
    
    // 'value' attribute on InfoBarElement (id=AccountName) at PolicyFileMenuInfoBar.pcf: line 22, column 48
    function value_2 () : java.lang.Object {
      return policyPeriod.PrimaryInsuredName
    }
    
    // 'value' attribute on InfoBarElement (id=MLD) at PolicyFileMenuInfoBar.pcf: line 27, column 33
    function value_4 () : java.lang.Object {
      return policyPeriod.WC7LineExists ? "(" + gw.api.web.dashboard.ui.policy.CurrentPolicyHelper.getMultiLineforWC_TDIC(policyPeriod)+ ")" : "(" + policyPeriod.MultiLineDiscount_TDIC.Description + ")"
    }
    
    // 'value' attribute on InfoBarElement (id=AccountNumber) at PolicyFileMenuInfoBar.pcf: line 32, column 58
    function value_7 () : java.lang.Object {
      return policyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on InfoBarElement (id=PolicyNumber) at PolicyFileMenuInfoBar.pcf: line 36, column 55
    function value_8 () : java.lang.Object {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on InfoBarElement (id=Underwriter) at PolicyFileMenuInfoBar.pcf: line 40, column 93
    function value_9 () : java.lang.Object {
      return policyPeriod.Policy.getUserRoleAssignmentByRole(TC_UNDERWRITER).AssignedUser
    }
    
    // 'visible' attribute on InfoBarElement (id=MLD) at PolicyFileMenuInfoBar.pcf: line 27, column 33
    function visible_3 () : java.lang.Boolean {
      return isMLDVisible()
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function formatStatusLabel(pp : PolicyPeriod) : String {
      if (pp.Canceled) {
        var cancDate = pp.CancellationDate
        var cancDateStr = gw.api.util.StringUtil.formatDate(cancDate,"short")
        if (cancDate == pp.PeriodStart) {
         return DisplayKey.get("Web.Wizard.InfoBar.CanceledFlatStatus", cancDateStr) 
        }
        else {
         return DisplayKey.get("Web.Wizard.InfoBar.CanceledStatus", cancDateStr) 
        }
      }
      else {
        var endDateStr = gw.api.util.StringUtil.formatDate(pp.PeriodEnd,"short")
        if (Date.CurrentDate >= policyPeriod.PeriodStart) {
          var status = pp.RenewalStatus_TDIC
          if(status != null)
            return DisplayKey.get("Web.Wizard.InfoBar.StatusAndExpDate", pp.RenewalStatus_TDIC,endDateStr)
          else
            return DisplayKey.get("Web.Wizard.InfoBar.StatusAndExpDate", pp.PeriodDisplayStatus,endDateStr)
        }else{
          return DisplayKey.get("Web.Wizard.InfoBar.StatusAndExpDate", pp.PeriodDisplayStatus,endDateStr)  
        }
      }
    }
    
    property get showRenewalWarning_TDIC() : boolean {
      var retval = false;
      if (Date.CurrentDate >= policyPeriod.PeriodStart) {
        var renewalStatus = policyPeriod.RenewalStatus_TDIC;
        if (renewalStatus == PolicyPeriodStatus.TC_NONRENEWED or renewalStatus == PolicyPeriodStatus.TC_NOTTAKEN) {
          retval = true;
        }
      }
      return retval;
    }
    
    function displayProductOffering():String{
      if(policyPeriod.GLLineExists){
        return policyPeriod.Offering.DisplayName
      }
      if(policyPeriod.BOPLineExists){
        return policyPeriod.Offering.DisplayName
      }
      if(policyPeriod.WC7LineExists){
        return "Workers' Compensation"
      }
      return null
    }
    
    function isMLDVisible():Boolean{
      if(policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") return false
      if(policyPeriod.WC7LineExists && gw.api.web.dashboard.ui.policy.CurrentPolicyHelper.getMultiLineforWC_TDIC(policyPeriod)!=null) return true
      if(!policyPeriod.WC7LineExists && policyPeriod.getMultiLineDiscount_TDIC()!=null) return true
      return false
    }
    
    
  }
  
  
}