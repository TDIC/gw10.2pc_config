package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7OtherStatesInsurance.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7OtherStatesInsuranceExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7OtherStatesInsurance.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 35, column 90
    function available_39 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'value' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      otherStatesInsurance.WC7OtherStatesOptTerm.Value = (__VALUE_TO_SET as typekey.OtherStates)
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      otherStatesInsurance.WC7IncludedStatesTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      otherStatesInsurance.WC7ExcludedStatesTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 20, column 41
    function initialValue_0 () : entity.WC7WorkersCompLine {
      return coverable as entity.WC7WorkersCompLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 25, column 52
    function initialValue_1 () : productmodel.WC7OtherStatesInsurance {
      return wc7Line.WC7OtherStatesInsurance
    }
    
    // 'label' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function label_19 () : java.lang.Object {
      return otherStatesInsurance.WC7IncludedStatesTerm.Pattern.DisplayName
    }
    
    // 'label' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function label_29 () : java.lang.Object {
      return otherStatesInsurance.WC7ExcludedStatesTerm.Pattern.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 35, column 90
    function label_40 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function label_7 () : java.lang.Object {
      return otherStatesInsurance.WC7OtherStatesOptTerm.Pattern.DisplayName
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 46, column 78
    function onChange_5 () : void {
      wc7Line.setDefaultExcludedStates(otherStatesInsurance)
    }
    
    // 'required' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function required_20 () : java.lang.Boolean {
      return otherStatesInsurance.WC7IncludedStatesTerm.Pattern.Required
    }
    
    // 'required' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function required_30 () : java.lang.Boolean {
      return otherStatesInsurance.WC7ExcludedStatesTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function required_8 () : java.lang.Boolean {
      return otherStatesInsurance.WC7OtherStatesOptTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 35, column 90
    function setter_41 (VALUE :  java.lang.Boolean) : void {
      coverable.setCoverageConditionOrExclusionExists(coveragePattern, VALUE)
    }
    
    // 'validationExpression' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function validationExpression_18 () : java.lang.Object {
      return wc7Line.validateIncludedStates(otherStatesInsurance.WC7IncludedStatesTerm.Value)
    }
    
    // 'validationExpression' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function validationExpression_28 () : java.lang.Object {
      return wc7Line.validateExcludedStatesContainMonopolisticStates(otherStatesInsurance.WC7ExcludedStatesTerm.Value)
    }
    
    // 'valueRange' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function valueRange_12 () : java.lang.Object {
      return otherStatesInsurance.WC7OtherStatesOptTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'value' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function valueRoot_11 () : java.lang.Object {
      return otherStatesInsurance.WC7OtherStatesOptTerm
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function valueRoot_23 () : java.lang.Object {
      return otherStatesInsurance.WC7IncludedStatesTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 29, column 37
    function valueRoot_3 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function valueRoot_33 () : java.lang.Object {
      return otherStatesInsurance.WC7ExcludedStatesTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 29, column 37
    function value_2 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 57, column 41
    function value_21 () : java.lang.String {
      return otherStatesInsurance.WC7IncludedStatesTerm.Value
    }
    
    // 'value' attribute on TextInput (id=WC7ExcludedStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 68, column 41
    function value_31 () : java.lang.String {
      return otherStatesInsurance.WC7ExcludedStatesTerm.Value
    }
    
    // 'value' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function value_9 () : typekey.OtherStates {
      return otherStatesInsurance.WC7OtherStatesOptTerm.Value
    }
    
    // 'valueRange' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function verifyValueRangeIsAllowedType_13 ($$arg :  typekey.OtherStates[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function verifyValueRange_14 () : void {
      var __valueRangeArg = otherStatesInsurance.WC7OtherStatesOptTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 49, column 168
    function visible_27 () : java.lang.Boolean {
      return otherStatesInsurance.hasCovTermByCodeIdentifier("WC7IncludedStates") and otherStatesInsurance.WC7OtherStatesOptTerm.Value == TC_LISTEDONLY
    }
    
    // 'visible' attribute on InputSet at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 60, column 167
    function visible_37 () : java.lang.Boolean {
      return otherStatesInsurance.hasCovTermByCodeIdentifier("WC7ExcludedStates") and otherStatesInsurance.WC7OtherStatesOptTerm.Value == TC_ALLEXCEPT
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 35, column 90
    function visible_38 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 73, column 100
    function visible_44 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on RangeInput (id=OtherStatesTermInput_Input) at CoverageInputSet.WC7OtherStatesInsurance.pcf: line 44, column 98
    function visible_6 () : java.lang.Boolean {
      return otherStatesInsurance.hasCovTermByCodeIdentifier("WC7OtherStatesOpt")
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherStatesInsurance () : productmodel.WC7OtherStatesInsurance {
      return getVariableValue("otherStatesInsurance", 0) as productmodel.WC7OtherStatesInsurance
    }
    
    property set otherStatesInsurance ($arg :  productmodel.WC7OtherStatesInsurance) {
      setVariableValue("otherStatesInsurance", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  
}