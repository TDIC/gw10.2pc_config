package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExclExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 34, column 106
    function available_47 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'editable' attribute on RowIterator (id=Exclusions) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 54, column 55
    function editable_10 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 20, column 38
    function initialValue_0 () : gw.api.domain.StateSet {
      return gw.api.domain.StateSet.get( gw.api.domain.StateSet.US50 )
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 24, column 41
    function initialValue_1 () : entity.WC7WorkersCompLine {
      return coverable as entity.WC7WorkersCompLine
    }
    
    // 'label' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 34, column 106
    function label_48 () : java.lang.Object {
      return exclusionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 34, column 106
    function setter_49 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(wc7Line, exclusionPattern, VALUE)
    }
    
    // 'value' attribute on TextCell (id=ExcludedItem_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 63, column 29
    function sortValue_5 (exclusion :  WC7ExcludedWorkplace) : java.lang.Object {
      return exclusion.ExcludedItem
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 70, column 29
    function sortValue_6 (exclusion :  WC7ExcludedWorkplace) : java.lang.Object {
      return exclusion.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 77, column 29
    function sortValue_7 (exclusion :  WC7ExcludedWorkplace) : java.lang.Object {
      return exclusion.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 84, column 29
    function sortValue_8 (exclusion :  WC7ExcludedWorkplace) : java.lang.Object {
      return exclusion.City
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 93, column 30
    function sortValue_9 (exclusion :  WC7ExcludedWorkplace) : java.lang.Object {
      return exclusion.Jurisdiction
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=Exclusions) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 54, column 55
    function toCreateAndAdd_42 () : WC7ExcludedWorkplace {
      return wc7Line.createAndAddWC7ExcludedWorkplace(wc7Line.WC7DesignatedWorkplacesExclEndorsementExcl)
    }
    
    // 'toRemove' attribute on RowIterator (id=Exclusions) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 54, column 55
    function toRemove_43 (exclusion :  WC7ExcludedWorkplace) : void {
      wc7Line.removeFromWC7ExcludedWorkplaces(exclusion)
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 28, column 37
    function valueRoot_3 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 28, column 37
    function value_2 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=Exclusions) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 54, column 55
    function value_44 () : entity.WC7ExcludedWorkplace[] {
      return wc7Line.WC7ExcludedWorkplaces
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 34, column 106
    function visible_46 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 100, column 101
    function visible_52 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get usStates () : gw.api.domain.StateSet {
      return getVariableValue("usStates", 0) as gw.api.domain.StateSet
    }
    
    property set usStates ($arg :  gw.api.domain.StateSet) {
      setVariableValue("usStates", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ExcludedItem_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 63, column 29
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      exclusion.ExcludedItem = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 70, column 29
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      exclusion.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 77, column 29
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      exclusion.AddressLine2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 84, column 29
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      exclusion.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 93, column 30
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      exclusion.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'editable' attribute on TextCell (id=ExcludedItem_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 63, column 29
    function editable_11 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'filter' attribute on TypeKeyCell (id=State_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 93, column 30
    function filter_39 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return usStates.contains(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(VALUE))
    }
    
    // 'value' attribute on TextCell (id=ExcludedItem_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 63, column 29
    function valueRoot_14 () : java.lang.Object {
      return exclusion
    }
    
    // 'value' attribute on TextCell (id=ExcludedItem_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 63, column 29
    function value_12 () : java.lang.String {
      return exclusion.ExcludedItem
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 70, column 29
    function value_18 () : java.lang.String {
      return exclusion.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 77, column 29
    function value_24 () : java.lang.String {
      return exclusion.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 84, column 29
    function value_30 () : java.lang.String {
      return exclusion.City
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at CoverageInputSet.WC7DesignatedWorkplacesExclEndorsementExcl.pcf: line 93, column 30
    function value_36 () : typekey.Jurisdiction {
      return exclusion.Jurisdiction
    }
    
    property get exclusion () : WC7ExcludedWorkplace {
      return getIteratedValue(1) as WC7ExcludedWorkplace
    }
    
    
  }
  
  
}