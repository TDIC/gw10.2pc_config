package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_EREQuoteTotalsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_EREQuoteTotalsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=EREPremium_Input) at TDIC_EREQuoteTotalsDV.pcf: line 16, column 43
    function valueRoot_1 () : java.lang.Object {
      return quote
    }
    
    // 'value' attribute on TextInput (id=EREPremium_Input) at TDIC_EREQuoteTotalsDV.pcf: line 16, column 43
    function value_0 () : java.math.BigDecimal {
      return quote.TotalEREPremium
    }
    
    // 'value' attribute on TextInput (id=ERETaxesSurcharges_Input) at TDIC_EREQuoteTotalsDV.pcf: line 22, column 43
    function value_3 () : java.math.BigDecimal {
      return quote.TaxesAndSurcharges
    }
    
    // 'value' attribute on TextInput (id=ERETotal_Input) at TDIC_EREQuoteTotalsDV.pcf: line 28, column 43
    function value_6 () : java.math.BigDecimal {
      return quote.TotalPremium
    }
    
    property get quote () : GLEREQuickQuote_TDIC {
      return getRequireValue("quote", 0) as GLEREQuickQuote_TDIC
    }
    
    property set quote ($arg :  GLEREQuickQuote_TDIC) {
      setRequireValue("quote", 0, $arg)
    }
    
    
  }
  
  
}