package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.util.ArrayList
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateCoverageScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateCoverageScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 41, column 58
    function def_onEnter_23 (def :  pcf.OOSEPanelSet) : void {
      def.onEnter(policyPeriod, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 43, column 63
    function def_onEnter_25 (def :  pcf.WarningsPanelSet) : void {
      def.onEnter(multiLineDiscountWarnings_TDIC)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 45, column 85
    function def_onEnter_27 (def :  pcf.WC7StateCoverageCV) : void {
      def.onEnter(policyPeriod.WC7Line, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 41, column 58
    function def_refreshVariables_24 (def :  pcf.OOSEPanelSet) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 43, column 63
    function def_refreshVariables_26 (def :  pcf.WarningsPanelSet) : void {
      def.refreshVariables(multiLineDiscountWarnings_TDIC)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageScreen.pcf: line 45, column 85
    function def_refreshVariables_28 (def :  pcf.WC7StateCoverageCV) : void {
      def.refreshVariables(policyPeriod.WC7Line, openForEdit, jobWizardHelper)
    }
    
    // 'editable' attribute on Screen (id=WC7StateCoverageScreen) at WC7StateCoverageScreen.pcf: line 7, column 33
    function editable_29 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageScreen.pcf: line 20, column 71
    function initialValue_0 () : tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper {
      return new tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageScreen.pcf: line 24, column 23
    function initialValue_1 () : boolean {
      return jobWizardHelper.stateCoveragesScreenOnFirstEnter(policyPeriod.Job)
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageScreen.pcf: line 29, column 44
    function initialValue_2 () : java.util.List<String> {
      return openForEdit ? policyPeriod.JobProcess.setMultiLineDiscount_TDIC() : new java.util.ArrayList<String>()
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function mode_3 () : java.lang.Object {
      return policyPeriod.Job.Subtype
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_10 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_12 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_14 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_16 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_18 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_20 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_4 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_6 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_onEnter_8 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.onEnter(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_11 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_13 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_15 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_17 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_19 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_21 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_5 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_7 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at WC7StateCoverageScreen.pcf: line 34, column 104
    function toolbarButtonSet_refreshVariables_9 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.refreshVariables(policyPeriod, policyPeriod.Job, jobWizardHelper)
    }
    
    // 'visible' attribute on AlertBar (id=QuoteRequestedAlert) at WC7StateCoverageScreen.pcf: line 39, column 80
    function visible_22 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.isQuoteRequestInProgress(policyPeriod)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get multiLineDiscountWarnings_TDIC () : java.util.List<String> {
      return getVariableValue("multiLineDiscountWarnings_TDIC", 0) as java.util.List<String>
    }
    
    property set multiLineDiscountWarnings_TDIC ($arg :  java.util.List<String>) {
      setVariableValue("multiLineDiscountWarnings_TDIC", 0, $arg)
    }
    
    property get onEnter_TDIC () : boolean {
      return getVariableValue("onEnter_TDIC", 0) as java.lang.Boolean
    }
    
    property set onEnter_TDIC ($arg :  boolean) {
      setVariableValue("onEnter_TDIC", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get stateCoveragesHelper () : tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper {
      return getVariableValue("stateCoveragesHelper", 0) as tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper
    }
    
    property set stateCoveragesHelper ($arg :  tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper) {
      setVariableValue("stateCoveragesHelper", 0, $arg)
    }
    
    
    function setMultiLineDiscountAndWarnings() : java.util.List<String> {
      var warnings = new ArrayList<String>();
      
      if (policyPeriod.Job typeis Submission and policyPeriod.Job.FullMode) {
        if (policyPeriod.MultiLineDiscountADANums_TDIC == null or 
            policyPeriod.MultiLineDiscountADANums_TDIC != "QuickQuote") {
            
        }
      }
      
      return warnings;
    }
    
    
  }
  
  
}