package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRoleNameInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactRoleNameInputSet_personExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRoleNameInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactRoleNameInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRoleNameInputSet.person.pcf: line 34, column 54
    function def_onEnter_2 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new tdic.pc.config.pcf.contacts.name.TDIC_RequiredBasicNameOwner(policyContactRoleAdapter),period)
    }
    
    // 'def' attribute on InputSetRef (id=HomePhone) at PolicyContactRoleNameInputSet.person.pcf: line 69, column 113
    function def_onEnter_28 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate(policyContactRole.AccountContactRole.AccountContact.Contact, Contact#HomePhone),DisplayKey.get("Web.ContactDetail.Person.HomePhone"),(policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_HOME))
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at PolicyContactRoleNameInputSet.person.pcf: line 73, column 113
    function def_onEnter_31 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Contact#WorkPhone), DisplayKey.get("Web.ContactDetail.Person.WorkPhone"), (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_WORK))
    }
    
    // 'def' attribute on InputSetRef (id=CellPhone) at PolicyContactRoleNameInputSet.person.pcf: line 78, column 113
    function def_onEnter_34 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Person#CellPhone), DisplayKey.get("Web.ContactDetail.Person.CellPhone"), (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_MOBILE))
    }
    
    // 'def' attribute on InputSetRef (id=FaxPhone) at PolicyContactRoleNameInputSet.person.pcf: line 83, column 113
    function def_onEnter_37 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Contact#FaxPhone), DisplayKey.get("Web.ContactDetail.Person.FaxPhone"), false))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRoleNameInputSet.person.pcf: line 34, column 54
    function def_onEnter_4 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new tdic.pc.config.pcf.contacts.name.TDIC_RequiredBasicNameOwner(policyContactRoleAdapter),period)
    }
    
    // 'def' attribute on InputSetRef (id=HomePhone) at PolicyContactRoleNameInputSet.person.pcf: line 69, column 113
    function def_refreshVariables_29 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate(policyContactRole.AccountContactRole.AccountContact.Contact, Contact#HomePhone),DisplayKey.get("Web.ContactDetail.Person.HomePhone"),(policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_HOME))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRoleNameInputSet.person.pcf: line 34, column 54
    function def_refreshVariables_3 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new tdic.pc.config.pcf.contacts.name.TDIC_RequiredBasicNameOwner(policyContactRoleAdapter),period)
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at PolicyContactRoleNameInputSet.person.pcf: line 73, column 113
    function def_refreshVariables_32 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Contact#WorkPhone), DisplayKey.get("Web.ContactDetail.Person.WorkPhone"), (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_WORK))
    }
    
    // 'def' attribute on InputSetRef (id=CellPhone) at PolicyContactRoleNameInputSet.person.pcf: line 78, column 113
    function def_refreshVariables_35 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Person#CellPhone), DisplayKey.get("Web.ContactDetail.Person.CellPhone"), (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone == TC_MOBILE))
    }
    
    // 'def' attribute on InputSetRef (id=FaxPhone) at PolicyContactRoleNameInputSet.person.pcf: line 83, column 113
    function def_refreshVariables_38 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.contact.PersonPhoneOwner(new gw.api.phone.ContactPhoneDelegate((policyContactRole.AccountContactRole.AccountContact.Contact as Person), Contact#FaxPhone), DisplayKey.get("Web.ContactDetail.Person.FaxPhone"), false))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRoleNameInputSet.person.pcf: line 34, column 54
    function def_refreshVariables_5 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new tdic.pc.config.pcf.contacts.name.TDIC_RequiredBasicNameOwner(policyContactRoleAdapter),period)
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at PolicyContactRoleNameInputSet.person.pcf: line 46, column 46
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.DateOfBirth = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at PolicyContactRoleNameInputSet.person.pcf: line 54, column 24
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.MaritalStatus = (__VALUE_TO_SET as typekey.MaritalStatus)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at PolicyContactRoleNameInputSet.person.pcf: line 62, column 112
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone = (__VALUE_TO_SET as typekey.PrimaryPhoneType)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at PolicyContactRoleNameInputSet.person.pcf: line 89, column 113
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      (policyContactRole.AccountContactRole.AccountContact.Contact as Person).EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress2_Input) at PolicyContactRoleNameInputSet.person.pcf: line 95, column 24
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      (policyContactRole.AccountContactRole.AccountContact.Contact as Person).EmailAddress2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PolicyContactRoleNameInputSet.person.pcf: line 40, column 44
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.Credential = (__VALUE_TO_SET as typekey.Credential_TDIC)
    }
    
    // 'editable' attribute on TypeKeyInput (id=MaritalStatus_Input) at PolicyContactRoleNameInputSet.person.pcf: line 54, column 24
    function editable_15 () : java.lang.Boolean {
      return period.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at PolicyContactRoleNameInputSet.person.pcf: line 15, column 78
    function initialValue_0 () : tdic.pc.config.globalization.TDIC_PolicyContactRoleNameAdapter {
      return new tdic.pc.config.globalization.TDIC_PolicyContactRoleNameAdapter(policyContactRole)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactRoleNameInputSet.person.pcf: line 25, column 28
    function initialValue_1 () : PolicyPeriod {
      return policyContactRole.Branch
    }
    
    // 'mode' attribute on InputSetRef at PolicyContactRoleNameInputSet.person.pcf: line 34, column 54
    function mode_6 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at PolicyContactRoleNameInputSet.person.pcf: line 62, column 112
    function valueRoot_24 () : java.lang.Object {
      return (policyContactRole.AccountContactRole.AccountContact.Contact as Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PolicyContactRoleNameInputSet.person.pcf: line 40, column 44
    function valueRoot_9 () : java.lang.Object {
      return policyContactRole
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at PolicyContactRoleNameInputSet.person.pcf: line 46, column 46
    function value_11 () : java.util.Date {
      return policyContactRole.DateOfBirth
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at PolicyContactRoleNameInputSet.person.pcf: line 54, column 24
    function value_16 () : typekey.MaritalStatus {
      return policyContactRole.MaritalStatus
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at PolicyContactRoleNameInputSet.person.pcf: line 62, column 112
    function value_22 () : typekey.PrimaryPhoneType {
      return (policyContactRole.AccountContactRole.AccountContact.Contact as Person).PrimaryPhone
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at PolicyContactRoleNameInputSet.person.pcf: line 89, column 113
    function value_40 () : java.lang.String {
      return (policyContactRole.AccountContactRole.AccountContact.Contact as Person).EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=EmailAddress2_Input) at PolicyContactRoleNameInputSet.person.pcf: line 95, column 24
    function value_46 () : java.lang.String {
      return (policyContactRole.AccountContactRole.AccountContact.Contact as Person).EmailAddress2
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PolicyContactRoleNameInputSet.person.pcf: line 40, column 44
    function value_7 () : typekey.Credential_TDIC {
      return policyContactRole.Credential
    }
    
    // 'visible' attribute on TypeKeyInput (id=PrimaryPhone_Input) at PolicyContactRoleNameInputSet.person.pcf: line 62, column 112
    function visible_21 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.getVisibilityOfPolicContactFields_TDIC(policyContactRole)
    }
    
    property get period () : PolicyPeriod {
      return getVariableValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    property get policyContactRoleAdapter () : tdic.pc.config.globalization.TDIC_PolicyContactRoleNameAdapter {
      return getVariableValue("policyContactRoleAdapter", 0) as tdic.pc.config.globalization.TDIC_PolicyContactRoleNameAdapter
    }
    
    property set policyContactRoleAdapter ($arg :  tdic.pc.config.globalization.TDIC_PolicyContactRoleNameAdapter) {
      setVariableValue("policyContactRoleAdapter", 0, $arg)
    }
    
    
  }
  
  
}