package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NotesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NotesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at NotesLV.pcf: line 51, column 49
    function def_onEnter_0 (def :  pcf.NoteRowSet) : void {
      def.onEnter(note, viewOnly, null)
    }
    
    // 'def' attribute on RowSetRef at NotesLV.pcf: line 51, column 49
    function def_refreshVariables_1 (def :  pcf.NoteRowSet) : void {
      def.refreshVariables(note, viewOnly, null)
    }
    
    property get note () : entity.Note {
      return getIteratedValue(1) as entity.Note
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NotesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator at NotesLV.pcf: line 49, column 33
    function value_2 () : entity.Note[] {
      return pcfSuppressAccountNotes_TDIC()
    }
    
    property get notes () : gw.api.database.IQueryBeanResult<Note> {
      return getRequireValue("notes", 0) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set notes ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setRequireValue("notes", 0, $arg)
    }
    
    property get passedInRelatedTo_TDIC () : Object {
      return getRequireValue("passedInRelatedTo_TDIC", 0) as Object
    }
    
    property set passedInRelatedTo_TDIC ($arg :  Object) {
      setRequireValue("passedInRelatedTo_TDIC", 0, $arg)
    }
    
    property get viewOnly () : boolean {
      return getRequireValue("viewOnly", 0) as java.lang.Boolean
    }
    
    property set viewOnly ($arg :  boolean) {
      setRequireValue("viewOnly", 0, $arg)
    }
    
    function pcfSuppressAccountNotes_TDIC() : Note[] {
      var tmpNotes : Note[]  
      if (passedInRelatedTo_TDIC typeis entity.Account) {
          tmpNotes = notes.where( \elt -> elt.Policy == null and elt.PolicyPeriod == null and elt.Activity == null).toTypedArray()
      } else {
        tmpNotes = notes.toTypedArray()
      }
      // Jeff, Per GPC-245/442, to return account and policy notes details since the detailed Notes not displayed, gap b/w V8 and V10.
      return notes.toTypedArray()
      //return tmpNotes
    }
    
    
  }
  
  
}