package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseWebFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseWebFramePanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseWebFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseWebFramePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}