package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RatingCumulDetailsPanelSet_GLLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends SurchargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 166, column 58
    function currency_65 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 148, column 81
    function valueRoot_53 () : java.lang.Object {
      return (surchargeCost as GLStateCost).StateCostType
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 153, column 91
    function valueRoot_56 () : java.lang.Object {
      return surchargeCost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 148, column 81
    function value_52 () : java.lang.String {
      return (surchargeCost as GLStateCost).StateCostType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 153, column 91
    function value_55 () : java.util.Date {
      return surchargeCost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 158, column 91
    function value_59 () : java.util.Date {
      return surchargeCost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 166, column 58
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return surchargeCost.ActualAmountBilling
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 153, column 91
    function visible_57 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get surchargeCost () : GLCost {
      return getIteratedValue(2) as GLCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends policyIteratorLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function actionAvailable_34 () : java.lang.Boolean {
      return perm.System.viewquoteterms_tdic and stateCostWrapper.Description == DisplayKey.get("TDIC.Web.Policy.GL.Header.TotalEREPremium")
    }
    
    // 'action' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function action_32 () : void {
      TDIC_GLERERatingDetailsPopup.push(period, jobWizardHelper, isEREJob)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function action_dest_33 () : pcf.api.Destination {
      return pcf.TDIC_GLERERatingDetailsPopup.createDestination(period, jobWizardHelper, isEREJob)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function currency_37 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 82, column 53
    function valueRoot_24 () : java.lang.Object {
      return stateCostWrapper
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 82, column 53
    function value_23 () : java.lang.String {
      return stateCostWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 87, column 135
    function value_26 () : java.util.Date {
      return stateCostWrapper.Cost != null ? stateCostWrapper.Cost.EffectiveDate : null
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 92, column 135
    function value_29 () : java.util.Date {
      return stateCostWrapper.Cost != null ? stateCostWrapper.Cost.ExpirationDate : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return stateCostWrapper.Total
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 87, column 135
    function visible_27 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC and not period.validateERECyberChangeReason
    }
    
    property get stateCostWrapper () : gw.api.ui.GL_CostWrapper {
      return getIteratedValue(2) as gw.api.ui.GL_CostWrapper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingCumulDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 189, column 71
    function currency_69 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.GLLine.pcf: line 46, column 36
    function def_onEnter_5 (def :  pcf.RatingOverrideButtonDV) : void {
      def.onEnter(period, period.GLLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef at RatingCumulDetailsPanelSet.GLLine.pcf: line 50, column 117
    function def_onEnter_8 (def :  pcf.TDIC_GLTermsPanelSet) : void {
      def.onEnter(period, glCosts, isEREJob)
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.GLLine.pcf: line 46, column 36
    function def_refreshVariables_6 (def :  pcf.RatingOverrideButtonDV) : void {
      def.refreshVariables(period, period.GLLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef at RatingCumulDetailsPanelSet.GLLine.pcf: line 50, column 117
    function def_refreshVariables_9 (def :  pcf.TDIC_GLTermsPanelSet) : void {
      def.refreshVariables(period, glCosts, isEREJob)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 21, column 50
    function initialValue_0 () : java.util.Set<entity.GLCost> {
      return period.GLLine.Costs.cast(GLCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 27, column 105
    function initialValue_1 () : java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>> {
      return glCosts.whereTypeIs(GLCovExposureCost).toSet().byFixedLocation()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 31, column 94
    function initialValue_2 () : java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>> {
      return glCosts.whereTypeIs(GLCost)?.partition(\c -> c.State)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 35, column 65
    function initialValue_3 () : java.util.Set<entity.GLHistoricalCost_TDIC> {
      return period.AllCosts.whereTypeIs(GLHistoricalCost_TDIC).toSet()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 43, column 23
    function initialValue_4 () : boolean {
      return quoteScreenHelper.isEREJob(period, jobWizardHelper)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 189, column 71
    function value_68 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalCost(period, isEREJob)
    }
    
    // 'visible' attribute on PanelRef at RatingCumulDetailsPanelSet.GLLine.pcf: line 50, column 117
    function visible_7 () : java.lang.Boolean {
      return perm.System.viewquoteterms_tdic and period.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
    }
    
    property get costsByState () : java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>> {
      return getVariableValue("costsByState", 0) as java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>>
    }
    
    property set costsByState ($arg :  java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>>) {
      setVariableValue("costsByState", 0, $arg)
    }
    
    property get glCosts () : java.util.Set<entity.GLCost> {
      return getVariableValue("glCosts", 0) as java.util.Set<entity.GLCost>
    }
    
    property set glCosts ($arg :  java.util.Set<entity.GLCost>) {
      setVariableValue("glCosts", 0, $arg)
    }
    
    property get glHistoricalCosts () : java.util.Set<entity.GLHistoricalCost_TDIC> {
      return getVariableValue("glHistoricalCosts", 0) as java.util.Set<entity.GLHistoricalCost_TDIC>
    }
    
    property set glHistoricalCosts ($arg :  java.util.Set<entity.GLHistoricalCost_TDIC>) {
      setVariableValue("glHistoricalCosts", 0, $arg)
    }
    
    property get isEREJob () : boolean {
      return getVariableValue("isEREJob", 0) as java.lang.Boolean
    }
    
    property set isEREJob ($arg :  boolean) {
      setVariableValue("isEREJob", 0, $arg)
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get locLevelCovCostMap () : java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>> {
      return getVariableValue("locLevelCovCostMap", 0) as java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>>
    }
    
    property set locLevelCovCostMap ($arg :  java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>>) {
      setVariableValue("locLevelCovCostMap", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get quoteScreenHelper () : tdic.web.pcf.helper.GLQuoteScreenHelper {
      return getVariableValue("quoteScreenHelper", 0) as tdic.web.pcf.helper.GLQuoteScreenHelper
    }
    
    property set quoteScreenHelper ($arg :  tdic.web.pcf.helper.GLQuoteScreenHelper) {
      setVariableValue("quoteScreenHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SurchargesLVExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 142, column 32
    function editable_51 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 137, column 37
    function initialValue_40 () : Set<GLCost> {
      return isEREJob ? quoteScreenHelper.getERETaxesAndSurcharges(glCosts) : quoteScreenHelper.getNonERETaxesAndSurcharges(glCosts)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 148, column 81
    function sortValue_41 (surchargeCost :  GLCost) : java.lang.Object {
      return (surchargeCost as GLStateCost).StateCostType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 153, column 91
    function sortValue_42 (surchargeCost :  GLCost) : java.lang.Object {
      return surchargeCost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 158, column 91
    function sortValue_44 (surchargeCost :  GLCost) : java.lang.Object {
      return surchargeCost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 166, column 58
    function sortValue_46 (surchargeCost :  GLCost) : java.lang.Object {
      return surchargeCost.ActualAmountBilling
    }
    
    // '$$sumValue' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 166, column 58
    function sumValueRoot_50 (surchargeCost :  GLCost) : java.lang.Object {
      return surchargeCost
    }
    
    // 'footerSumValue' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 166, column 58
    function sumValue_49 (surchargeCost :  GLCost) : java.lang.Object {
      return surchargeCost.ActualAmountBilling
    }
    
    // 'value' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 142, column 32
    function value_67 () : GLCost[] {
      return surchargeCosts.toTypedArray()
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 153, column 91
    function visible_43 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get surchargeCosts () : Set<GLCost> {
      return getVariableValue("surchargeCosts", 1) as Set<GLCost>
    }
    
    property set surchargeCosts ($arg :  Set<GLCost>) {
      setVariableValue("surchargeCosts", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/RatingCumulDetailsPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class policyIteratorLVExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 73, column 50
    function editable_22 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.GLLine.pcf: line 67, column 46
    function initialValue_10 () : gw.api.ui.GL_CostWrapper[] {
      return quoteScreenHelper.validateERECyberChangeReason(period) ? quoteScreenHelper.EREStandardPremVal(period) : (isEREJob) ? quoteScreenHelper.getEREStandardPremiumWrappers(period) : quoteScreenHelper.getNonEREStandardPremiumWrappers(period)
    }
    
    // 'sortBy' attribute on IteratorSort at RatingCumulDetailsPanelSet.GLLine.pcf: line 76, column 28
    function sortBy_11 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Order
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 82, column 53
    function sortValue_12 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 87, column 135
    function sortValue_13 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Cost != null ? stateCostWrapper.Cost.EffectiveDate : null
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 92, column 135
    function sortValue_15 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Cost != null ? stateCostWrapper.Cost.ExpirationDate : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function sortValue_17 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Total
    }
    
    // '$$sumValue' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function sumValueRoot_21 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper
    }
    
    // 'footerSumValue' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 102, column 47
    function sumValue_20 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Total
    }
    
    // 'value' attribute on RowIterator at RatingCumulDetailsPanelSet.GLLine.pcf: line 73, column 50
    function value_39 () : gw.api.ui.GL_CostWrapper[] {
      return stateCostWrappers
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at RatingCumulDetailsPanelSet.GLLine.pcf: line 87, column 135
    function visible_14 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC and not period.validateERECyberChangeReason
    }
    
    property get stateCostWrappers () : gw.api.ui.GL_CostWrapper[] {
      return getVariableValue("stateCostWrappers", 1) as gw.api.ui.GL_CostWrapper[]
    }
    
    property set stateCostWrappers ($arg :  gw.api.ui.GL_CostWrapper[]) {
      setVariableValue("stateCostWrappers", 1, $arg)
    }
    
    
  }
  
  
}