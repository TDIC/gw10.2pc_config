package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7OptionsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=OptionName_Cell) at WC7OptionsPanelSet.pcf: line 61, column 47
    function valueRoot_7 () : java.lang.Object {
      return wc7Option
    }
    
    // 'value' attribute on TextCell (id=OptionName_Cell) at WC7OptionsPanelSet.pcf: line 61, column 47
    function value_6 () : java.lang.String {
      return wc7Option.Label
    }
    
    property get wc7Option () : gw.lob.wc7.options.WC7Option {
      return getIteratedValue(2) as gw.lob.wc7.options.WC7Option
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7OptionsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=WC7OptionItem) at WC7OptionsPanelSet.pcf: line 34, column 88
    function label_1 () : java.lang.Object {
      return wc7Option.Label
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=WC7OptionItem) at WC7OptionsPanelSet.pcf: line 34, column 88
    function toCreateAndAdd_2 (CheckedValues :  Object[]) : java.lang.Object {
      wc7Option.moveFromMenuToActiveList(); return wc7Option
    }
    
    property get wc7Option () : gw.lob.wc7.options.WC7Option {
      return getIteratedValue(1) as gw.lob.wc7.options.WC7Option
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends WC7OptionsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_onEnter_11 (def :  pcf.WC7OptionCardPanelSet_ManuscriptOption) : void {
      def.onEnter(period.WC7Line, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_onEnter_13 (def :  pcf.WC7OptionCardPanelSet_ParticipatingPlan) : void {
      def.onEnter(period.WC7Line, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_onEnter_15 (def :  pcf.WC7OptionCardPanelSet_RetrospectiveRatingPlan) : void {
      def.onEnter(period.WC7Line, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_refreshVariables_12 (def :  pcf.WC7OptionCardPanelSet_ManuscriptOption) : void {
      def.refreshVariables(period.WC7Line, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_refreshVariables_14 (def :  pcf.WC7OptionCardPanelSet_ParticipatingPlan) : void {
      def.refreshVariables(period.WC7Line, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function def_refreshVariables_16 (def :  pcf.WC7OptionCardPanelSet_RetrospectiveRatingPlan) : void {
      def.refreshVariables(period.WC7Line, openForEdit)
    }
    
    // 'mode' attribute on PanelRef at WC7OptionsPanelSet.pcf: line 71, column 46
    function mode_17 () : java.lang.Object {
      return selectedWC7Option.Mode
    }
    
    // 'value' attribute on TextCell (id=OptionName_Cell) at WC7OptionsPanelSet.pcf: line 61, column 47
    function sortValue_5 (wc7Option :  gw.lob.wc7.options.WC7Option) : java.lang.Object {
      return wc7Option.Label
    }
    
    // 'title' attribute on Card (id=OptionCard) at WC7OptionsPanelSet.pcf: line 68, column 45
    function title_18 () : java.lang.String {
      return selectedWC7Option.Label
    }
    
    // 'toRemove' attribute on RowIterator at WC7OptionsPanelSet.pcf: line 55, column 56
    function toRemove_9 (wc7Option :  gw.lob.wc7.options.WC7Option) : void {
      wc7Option.moveToMenuFromActiveList()
    }
    
    // 'value' attribute on RowIterator at WC7OptionsPanelSet.pcf: line 55, column 56
    function value_10 () : gw.lob.wc7.options.WC7Option[] {
      return wc7Options.where(\ option -> not option.ShowOnMenu).toTypedArray()
    }
    
    property get selectedWC7Option () : gw.lob.wc7.options.WC7Option {
      return getCurrentSelection(1) as gw.lob.wc7.options.WC7Option
    }
    
    property set selectedWC7Option ($arg :  gw.lob.wc7.options.WC7Option) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7OptionsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7OptionsPanelSet.pcf: line 13, column 66
    function initialValue_0 () : java.util.List<gw.lob.wc7.options.WC7Option> {
      return period.WC7Line.WC7Options
    }
    
    // 'value' attribute on AddMenuItemIterator (id=wc7Option) at WC7OptionsPanelSet.pcf: line 29, column 56
    function value_3 () : gw.lob.wc7.options.WC7Option[] {
      return wc7Options.where(\ option -> option.ShowOnMenu).toTypedArray()
    }
    
    // 'visible' attribute on AddButton (id=OptionIteratorButton) at WC7OptionsPanelSet.pcf: line 23, column 33
    function visible_4 () : java.lang.Boolean {
      return openForEdit
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get wc7Options () : java.util.List<gw.lob.wc7.options.WC7Option> {
      return getVariableValue("wc7Options", 0) as java.util.List<gw.lob.wc7.options.WC7Option>
    }
    
    property set wc7Options ($arg :  java.util.List<gw.lob.wc7.options.WC7Option>) {
      setVariableValue("wc7Options", 0, $arg)
    }
    
    
  }
  
  
}