package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TaxLocationSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7TaxLocationSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TaxLocationSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7TaxLocationSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7TaxLocationSearchDV.pcf: line 49, column 41
    function def_onEnter_23 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at WC7TaxLocationSearchDV.pcf: line 49, column 41
    function def_refreshVariables_24 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7TaxLocationSearchDV.pcf: line 16, column 39
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=TLPrefix_Input) at WC7TaxLocationSearchDV.pcf: line 39, column 39
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TaxLocationPrefix = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at WC7TaxLocationSearchDV.pcf: line 45, column 39
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=City_Input) at WC7TaxLocationSearchDV.pcf: line 22, column 39
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=County_Input) at WC7TaxLocationSearchDV.pcf: line 28, column 39
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.County = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7TaxLocationSearchDV.pcf: line 16, column 39
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7TaxLocationSearchDV.pcf: line 16, column 39
    function value_0 () : java.lang.String {
      return searchCriteria.Code
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at WC7TaxLocationSearchDV.pcf: line 33, column 43
    function value_12 () : typekey.Jurisdiction {
      return searchCriteria.State
    }
    
    // 'value' attribute on TextInput (id=TLPrefix_Input) at WC7TaxLocationSearchDV.pcf: line 39, column 39
    function value_15 () : java.lang.String {
      return searchCriteria.TaxLocationPrefix
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at WC7TaxLocationSearchDV.pcf: line 45, column 39
    function value_19 () : java.lang.String {
      return searchCriteria.Description
    }
    
    // 'value' attribute on TextInput (id=City_Input) at WC7TaxLocationSearchDV.pcf: line 22, column 39
    function value_4 () : java.lang.String {
      return searchCriteria.City
    }
    
    // 'value' attribute on TextInput (id=County_Input) at WC7TaxLocationSearchDV.pcf: line 28, column 39
    function value_8 () : java.lang.String {
      return searchCriteria.County
    }
    
    property get searchCriteria () : gw.lob.common.TaxLocationSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.lob.common.TaxLocationSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.lob.common.TaxLocationSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}