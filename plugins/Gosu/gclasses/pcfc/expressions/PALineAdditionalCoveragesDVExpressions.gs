package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/pa/policy/PALineAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PALineAdditionalCoveragesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PALineAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InputSetExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PALineAdditionalCoveragesDV.pcf: line 29, column 61
    function initialValue_0 () : gw.api.productmodel.CoveragePattern[] {
      return paLine.Pattern.getCoverageCategoryByPublicId(category).coveragePatternsForEntity(PersonalAutoLine).whereSelectedOrAvailable(paLine, openForEdit)
    }
    
    // 'value' attribute on InputIterator (id=LineCoveragesIterator) at PALineAdditionalCoveragesDV.pcf: line 35, column 65
    function value_108 () : gw.api.productmodel.CoveragePattern[] {
      return includedCategoryCoveragePatterns
    }
    
    property get includedCategoryCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("includedCategoryCoveragePatterns", 2) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set includedCategoryCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("includedCategoryCoveragePatterns", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PALineAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends InputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_1 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_105 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_11 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_13 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_17 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_19 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_3 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_55 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_57 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_63 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_65 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_67 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_2 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_4 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at PALineAdditionalCoveragesDV.pcf: line 38, column 50
    function mode_107 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(3) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PALineAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PALineAdditionalCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get category () : java.lang.String {
      return getIteratedValue(1) as java.lang.String
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PALineAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PALineAdditionalCoveragesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on InputIterator (id=categories) at PALineAdditionalCoveragesDV.pcf: line 23, column 42
    function value_109 () : java.lang.String[] {
      return includedCoverageCategories
    }
    
    // 'visible' attribute on InputSet at PALineAdditionalCoveragesDV.pcf: line 18, column 34
    function visible_110 () : java.lang.Boolean {
      return paLine != null
    }
    
    property get includedCoverageCategories () : String[] {
      return getRequireValue("includedCoverageCategories", 0) as String[]
    }
    
    property set includedCoverageCategories ($arg :  String[]) {
      setRequireValue("includedCoverageCategories", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get paLine () : PersonalAutoLine {
      return getRequireValue("paLine", 0) as PersonalAutoLine
    }
    
    property set paLine ($arg :  PersonalAutoLine) {
      setRequireValue("paLine", 0, $arg)
    }
    
    
  }
  
  
}