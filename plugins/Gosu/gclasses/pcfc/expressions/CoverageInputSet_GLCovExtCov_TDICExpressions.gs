package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCovExtCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLCovExtCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCovExtCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 33, column 99
    function available_16 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 33, column 99
    function label_17 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 33, column 99
    function setter_18 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 61, column 56
    function sortValue_2 (scheduledItem :  entity.GLMobileDCSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 68, column 57
    function sortValue_3 (scheduledItem :  entity.GLMobileDCSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 53, column 55
    function toCreateAndAdd_12 () : entity.GLMobileDCSched_TDIC {
      return glLine.createAndAddMobileDCSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 53, column 55
    function toRemove_13 (scheduledItem :  entity.GLMobileDCSched_TDIC) : void {
      glLine.removeFromGLMobileDCSched_TDIC(scheduledItem); 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 53, column 55
    function value_14 () : entity.GLMobileDCSched_TDIC[] {
      return glLine.GLMobileDCSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 33, column 99
    function visible_15 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 75, column 100
    function visible_21 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCovExtCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 61, column 56
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.LTEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 68, column 57
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.LTExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 61, column 56
    function valueRoot_6 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 61, column 56
    function value_4 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLCovExtCov_TDIC.pcf: line 68, column 57
    function value_8 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    property get scheduledItem () : entity.GLMobileDCSched_TDIC {
      return getIteratedValue(1) as entity.GLMobileDCSched_TDIC
    }
    
    
  }
  
  
}