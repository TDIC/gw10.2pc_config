package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContractorsEquipmentPartPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContractorsEquipmentPartPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ContractorsEquipmentPartPanelSet.pcf: line 163, column 101
    function def_onEnter_351 (def :  pcf.ScheduledEquipmentPanelSet) : void {
      def.onEnter(contractorsEquipmentPart, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ContractorsEquipmentPartPanelSet.pcf: line 169, column 74
    function def_onEnter_353 (def :  pcf.QuestionSetsDV) : void {
      def.onEnter(questionSets, policyPeriod.IMLine, null)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at ContractorsEquipmentPartPanelSet.pcf: line 35, column 50
    function def_onEnter_4 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(contractorsEquipmentPart, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ContractorsEquipmentPartPanelSet.pcf: line 163, column 101
    function def_refreshVariables_352 (def :  pcf.ScheduledEquipmentPanelSet) : void {
      def.refreshVariables(contractorsEquipmentPart, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ContractorsEquipmentPartPanelSet.pcf: line 169, column 74
    function def_refreshVariables_354 (def :  pcf.QuestionSetsDV) : void {
      def.refreshVariables(questionSets, policyPeriod.IMLine, null)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at ContractorsEquipmentPartPanelSet.pcf: line 35, column 50
    function def_refreshVariables_5 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(contractorsEquipmentPart, openForEdit, jobWizardHelper)
    }
    
    // 'value' attribute on TypeKeyInput (id=coinsurance_Input) at ContractorsEquipmentPartPanelSet.pcf: line 63, column 58
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      contractorsEquipmentPart.Coinsurance = (__VALUE_TO_SET as typekey.Coinsurance)
    }
    
    // 'value' attribute on TextInput (id=perOccurrenceLimit_Input) at ContractorsEquipmentPartPanelSet.pcf: line 69, column 56
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      contractorsEquipmentPart.PerOccurrenceLimit = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=reporting_Input) at ContractorsEquipmentPartPanelSet.pcf: line 74, column 69
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      contractorsEquipmentPart.Reporting = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=contrEqContractorType_Input) at ContractorsEquipmentPartPanelSet.pcf: line 56, column 61
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      contractorsEquipmentPart.ContractorType = (__VALUE_TO_SET as typekey.ContractorType)
    }
    
    // 'editable' attribute on PanelSet (id=ContractorsEquipmentPartPanelSet) at ContractorsEquipmentPartPanelSet.pcf: line 7, column 43
    function editable_355 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 20, column 52
    function initialValue_0 () : gw.api.productmodel.CoverageCategory {
      return contractorsEquipmentPart.InlandMarineLine.Pattern.getCoverageCategoryByPublicId("ContractorsEquipPartCategory")
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 24, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return contractorsEquipmentPart.InlandMarineLine.Pattern.getCoverageCategoryByPublicId("ContractorsEquipPolicywideUnscheduled")
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 28, column 35
    function initialValue_2 () : entity.PolicyPeriod {
      return contractorsEquipmentPart.InlandMarineLine.Branch
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 32, column 49
    function initialValue_3 () : gw.api.productmodel.QuestionSet[] {
      return new gw.api.productmodel.QuestionSet[]{contractorsEquipmentPart.InlandMarineLine.Branch.Policy.Product.getQuestionSetByPublicId( "ContractorsEquipmentQuestion" )}
    }
    
    // 'value' attribute on TypeKeyInput (id=contrEqContractorType_Input) at ContractorsEquipmentPartPanelSet.pcf: line 56, column 61
    function valueRoot_8 () : java.lang.Object {
      return contractorsEquipmentPart
    }
    
    // 'value' attribute on TypeKeyInput (id=coinsurance_Input) at ContractorsEquipmentPartPanelSet.pcf: line 63, column 58
    function value_10 () : typekey.Coinsurance {
      return contractorsEquipmentPart.Coinsurance
    }
    
    // 'value' attribute on TextInput (id=perOccurrenceLimit_Input) at ContractorsEquipmentPartPanelSet.pcf: line 69, column 56
    function value_14 () : java.lang.Integer {
      return contractorsEquipmentPart.PerOccurrenceLimit
    }
    
    // 'value' attribute on BooleanRadioInput (id=reporting_Input) at ContractorsEquipmentPartPanelSet.pcf: line 74, column 69
    function value_18 () : java.lang.Boolean {
      return contractorsEquipmentPart.Reporting
    }
    
    // 'value' attribute on TypeKeyInput (id=contrEqContractorType_Input) at ContractorsEquipmentPartPanelSet.pcf: line 56, column 61
    function value_6 () : typekey.ContractorType {
      return contractorsEquipmentPart.ContractorType
    }
    
    property get contractorsEquipPartCategory () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("contractorsEquipPartCategory", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set contractorsEquipPartCategory ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("contractorsEquipPartCategory", 0, $arg)
    }
    
    property get contractorsEquipPolicywideUnscheduled () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("contractorsEquipPolicywideUnscheduled", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set contractorsEquipPolicywideUnscheduled ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("contractorsEquipPolicywideUnscheduled", 0, $arg)
    }
    
    property get contractorsEquipmentPart () : ContractorsEquipPart {
      return getRequireValue("contractorsEquipmentPart", 0) as ContractorsEquipPart
    }
    
    property set contractorsEquipmentPart ($arg :  ContractorsEquipPart) {
      setRequireValue("contractorsEquipmentPart", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get questionSets () : gw.api.productmodel.QuestionSet[] {
      return getVariableValue("questionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set questionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setVariableValue("questionSets", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel2ExpressionsImpl extends ContractorsEquipmentPartPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 113, column 65
    function initialValue_131 () : gw.api.productmodel.CoveragePattern[] {
      return contractorsEquipmentPart == null ? null : contractorsEquipPolicywideUnscheduled.coveragePatternsForEntity(ContractorsEquipPart).whereSelectedOrAvailable(contractorsEquipmentPart, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at ContractorsEquipmentPartPanelSet.pcf: line 123, column 38
    function sortBy_132 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=policywideUnscheduledCovs) at ContractorsEquipmentPartPanelSet.pcf: line 120, column 71
    function value_240 () : gw.api.productmodel.CoveragePattern[] {
      return unscheduledEquipmentCoveragePatterns
    }
    
    property get unscheduledEquipmentCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("unscheduledEquipmentCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set unscheduledEquipmentCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("unscheduledEquipmentCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel3ExpressionsImpl extends ContractorsEquipmentPartPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 142, column 65
    function initialValue_241 () : gw.api.productmodel.CoveragePattern[] {
      return contractorsEquipmentPart == null ? null : contractorsEquipPartCategory.coveragePatternsForEntity(ContractorsEquipPart).whereSelectedOrAvailable(contractorsEquipmentPart, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at ContractorsEquipmentPartPanelSet.pcf: line 152, column 38
    function sortBy_242 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=partLevelCovs) at ContractorsEquipmentPartPanelSet.pcf: line 149, column 71
    function value_350 () : gw.api.productmodel.CoveragePattern[] {
      return partLevelCoveragePatterns
    }
    
    property get partLevelCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("partLevelCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set partLevelCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("partLevelCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends ContractorsEquipmentPartPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ContractorsEquipmentPartPanelSet.pcf: line 86, column 72
    function initialValue_22 () : gw.api.productmodel.ExclusionPattern[] {
      return contractorsEquipmentPart == null ? null : contractorsEquipPartCategory.exclusionPatternsForEntity(ContractorsEquipPart).where(\ e -> contractorsEquipmentPart.isExclusionSelectedOrAvailable(e))
    }
    
    // 'value' attribute on InputIterator (id=partLevelExclusions) at ContractorsEquipmentPartPanelSet.pcf: line 93, column 78
    function value_130 () : gw.api.productmodel.ExclusionPattern[] {
      return partLevelExclusionPatterns
    }
    
    property get partLevelExclusionPatterns () : gw.api.productmodel.ExclusionPattern[] {
      return getVariableValue("partLevelExclusionPatterns", 1) as gw.api.productmodel.ExclusionPattern[]
    }
    
    property set partLevelExclusionPatterns ($arg :  gw.api.productmodel.ExclusionPattern[]) {
      setVariableValue("partLevelExclusionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_133 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_135 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_137 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_139 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_141 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_143 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_145 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_147 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_149 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_151 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_153 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_155 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_157 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_159 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_161 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_163 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_165 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_167 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_169 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_171 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_173 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_175 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_177 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_179 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_181 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_183 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_185 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_187 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_189 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_191 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_193 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_195 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_197 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_199 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_201 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_203 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_205 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_207 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_209 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_211 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_213 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_215 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_219 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_221 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_223 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_225 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_227 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_229 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_231 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_233 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_235 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_onEnter_237 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_134 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_136 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_138 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_140 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_142 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_144 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_146 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_148 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_222 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_226 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_228 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_230 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_232 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_234 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_236 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function def_refreshVariables_238 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 126, column 56
    function mode_239 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DetailViewPanel3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_243 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_245 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_247 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_249 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_251 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_253 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_255 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_257 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_259 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_261 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_263 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_265 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_267 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_269 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_271 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_273 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_275 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_277 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_279 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_281 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_283 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_285 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_287 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_289 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_291 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_293 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_295 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_297 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_299 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_301 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_303 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_305 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_307 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_309 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_311 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_313 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_315 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_317 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_319 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_321 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_323 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_325 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_327 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_329 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_331 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_333 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_335 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_337 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_339 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_341 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_343 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_345 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_onEnter_347 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_244 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_246 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_248 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_250 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_252 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_254 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_256 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_258 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_260 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_262 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_264 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_266 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_268 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_270 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_272 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_274 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_276 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_278 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_280 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_282 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_284 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_286 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_288 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_290 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_292 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_294 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_296 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_298 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_300 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_302 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_304 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_306 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_308 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_310 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_312 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_314 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_316 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_318 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_320 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_322 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_324 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_326 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_328 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_330 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_332 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_334 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_336 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_338 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_340 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_342 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_344 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_346 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function def_refreshVariables_348 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, contractorsEquipmentPart, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 155, column 56
    function mode_349 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/im/parts/contractorsequip/ContractorsEquipmentPartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_109 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_111 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_113 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_115 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_117 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_119 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_121 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_123 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_125 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_127 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_23 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_25 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_27 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_29 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_31 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_33 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_35 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_37 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_59 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_61 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_63 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_65 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_67 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_69 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_71 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_73 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_75 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_77 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_79 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_81 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_83 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_85 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_112 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_114 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_116 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_118 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_120 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_122 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_124 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_126 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_128 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'def' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables( exclusionPattern, contractorsEquipmentPart, openForEdit )
    }
    
    // 'mode' attribute on InputSetRef at ContractorsEquipmentPartPanelSet.pcf: line 96, column 54
    function mode_129 () : java.lang.Object {
      return exclusionPattern
    }
    
    property get exclusionPattern () : gw.api.productmodel.ExclusionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ExclusionPattern
    }
    
    
  }
  
  
}