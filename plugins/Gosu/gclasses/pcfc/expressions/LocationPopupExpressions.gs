package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lob.wc7.WC7PolicyLineMethods
@javax.annotation.Generated("config/web/pcf/line/common/LocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LocationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/common/LocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends LocationPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at LocationPopup.pcf: line 118, column 46
    function action_23 () : void {
      helper.replaceLocationWithSuggestedAddress(suggestedAddress,thePolicyLocation); addressList = helper.getAddressList(); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible() 
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at LocationPopup.pcf: line 123, column 58
    function valueRoot_25 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at LocationPopup.pcf: line 123, column 58
    function value_24 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at LocationPopup.pcf: line 127, column 58
    function value_27 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at LocationPopup.pcf: line 131, column 50
    function value_30 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at LocationPopup.pcf: line 136, column 46
    function value_33 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at LocationPopup.pcf: line 140, column 56
    function value_36 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(1) as entity.Address
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/common/LocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (theAccountLocation :  AccountLocation, thePolicyLocation :  PolicyLocation, policyPeriod :  PolicyPeriod, openForEdit :  boolean, startInEdit :  boolean, supportsNonSpecificLocation :  boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Standardize) at LocationPopup.pcf: line 74, column 57
    function action_6 () : void {
      addressList = helper.validatePolicyLocation(thePolicyLocation); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); helper.displayExceptions()
    }
    
    // 'action' attribute on ToolbarButton (id=Override) at LocationPopup.pcf: line 80, column 38
    function action_9 () : void {
      CurrentLocation.pickValueAndCommit(thePolicyLocation)
    }
    
    // 'afterCommit' attribute on Popup (id=LocationPopup) at LocationPopup.pcf: line 14, column 105
    function afterCommit_41 (TopLocation :  pcf.api.Location) : void {
      gw.api.web.PebblesUtil.invalidateIterators(TopLocation, PolicyLocation);
    }
    
    // 'afterEnter' attribute on Popup (id=LocationPopup) at LocationPopup.pcf: line 14, column 105
    function afterEnter_42 () : void {
      maybeCreateLocation()
    }
    
    // 'available' attribute on ToolbarButton (id=Standardize) at LocationPopup.pcf: line 74, column 57
    function available_4 () : java.lang.Boolean {
      return standardizeVisible
    }
    
    // 'available' attribute on ToolbarButton (id=Override) at LocationPopup.pcf: line 80, column 38
    function available_7 () : java.lang.Boolean {
      return overrideVisible
    }
    
    // 'beforeCommit' attribute on Popup (id=LocationPopup) at LocationPopup.pcf: line 14, column 105
    function beforeCommit_43 (pickedValue :  PolicyLocation) : void {
      checkChangeableState(); handleBeforeCommitForProduct(); helper.updateAddressStatus(thePolicyLocation.AccountLocation); thePolicyLocation.setReinsuranceSearchTag()
    }
    
    // 'canEdit' attribute on Popup (id=LocationPopup) at LocationPopup.pcf: line 14, column 105
    function canEdit_44 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'def' attribute on PanelRef at LocationPopup.pcf: line 95, column 96
    function def_onEnter_14 (def :  pcf.LocationDetailCV) : void {
      def.onEnter(thePolicyLocation, openForEdit, supportsNonSpecificLocation)
    }
    
    // 'def' attribute on PanelRef at LocationPopup.pcf: line 95, column 96
    function def_refreshVariables_15 (def :  pcf.LocationDetailCV) : void {
      def.refreshVariables(thePolicyLocation, openForEdit, supportsNonSpecificLocation)
    }
    
    // 'initialValue' attribute on Variable at LocationPopup.pcf: line 35, column 29
    function initialValue_0 () : typekey.State {
      return thePolicyLocation.State
    }
    
    // 'initialValue' attribute on Variable at LocationPopup.pcf: line 39, column 31
    function initialValue_1 () : typekey.Country {
      return thePolicyLocation.Country
    }
    
    // 'initialValue' attribute on Variable at LocationPopup.pcf: line 53, column 76
    function initialValue_2 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at LocationPopup.pcf: line 65, column 53
    function initialValue_3 () : java.util.HashSet<PolicyPeriod> {
      return thePolicyLocation != null ? thePolicyLocation.getImpactedPoliciesForPolicyLocationChange_TDIC() : (theAccountLocation != null ? theAccountLocation.getImpactedPoliciesForAccountLocationChange_TDIC() : null)
    }
    
    // 'label' attribute on Verbatim at LocationPopup.pcf: line 91, column 25
    function label_13 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisLocationMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // 'pickValue' attribute on EditButtons at LocationPopup.pcf: line 85, column 42
    function pickValue_10 () : PolicyLocation {
      return thePolicyLocation
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at LocationPopup.pcf: line 123, column 58
    function sortValue_17 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at LocationPopup.pcf: line 127, column 58
    function sortValue_18 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at LocationPopup.pcf: line 131, column 50
    function sortValue_19 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at LocationPopup.pcf: line 136, column 46
    function sortValue_20 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at LocationPopup.pcf: line 140, column 56
    function sortValue_21 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'startInEditMode' attribute on Popup (id=LocationPopup) at LocationPopup.pcf: line 14, column 105
    function startInEditMode_45 () : java.lang.Boolean {
      return startInEdit
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at LocationPopup.pcf: line 110, column 67
    function value_39 () : java.util.ArrayList<entity.Address> {
      return addressList
    }
    
    // 'updateVisible' attribute on EditButtons at LocationPopup.pcf: line 85, column 42
    function visible_11 () : java.lang.Boolean {
      return updateVisible
    }
    
    // 'visible' attribute on Verbatim at LocationPopup.pcf: line 91, column 25
    function visible_12 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    // 'visible' attribute on TitleBar (id=SelectTitle) at LocationPopup.pcf: line 101, column 66
    function visible_16 () : java.lang.Boolean {
      return addressList ?.Count > 0 ? true : false
    }
    
    // 'visible' attribute on ToolbarButton (id=Standardize) at LocationPopup.pcf: line 74, column 57
    function visible_5 () : java.lang.Boolean {
      return standardizeVisible and openForEdit
    }
    
    override property get CurrentLocation () : pcf.LocationPopup {
      return super.CurrentLocation as pcf.LocationPopup
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.HashSet<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.HashSet<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.HashSet<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get overrideVisible () : boolean {
      return getVariableValue("overrideVisible", 0) as java.lang.Boolean
    }
    
    property set overrideVisible ($arg :  boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get standardizeVisible () : boolean {
      return getVariableValue("standardizeVisible", 0) as java.lang.Boolean
    }
    
    property set standardizeVisible ($arg :  boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get startCountry () : typekey.Country {
      return getVariableValue("startCountry", 0) as typekey.Country
    }
    
    property set startCountry ($arg :  typekey.Country) {
      setVariableValue("startCountry", 0, $arg)
    }
    
    property get startInEdit () : boolean {
      return getVariableValue("startInEdit", 0) as java.lang.Boolean
    }
    
    property set startInEdit ($arg :  boolean) {
      setVariableValue("startInEdit", 0, $arg)
    }
    
    property get startState () : typekey.State {
      return getVariableValue("startState", 0) as typekey.State
    }
    
    property set startState ($arg :  typekey.State) {
      setVariableValue("startState", 0, $arg)
    }
    
    property get supportsNonSpecificLocation () : boolean {
      return getVariableValue("supportsNonSpecificLocation", 0) as java.lang.Boolean
    }
    
    property set supportsNonSpecificLocation ($arg :  boolean) {
      setVariableValue("supportsNonSpecificLocation", 0, $arg)
    }
    
    property get theAccountLocation () : AccountLocation {
      return getVariableValue("theAccountLocation", 0) as AccountLocation
    }
    
    property set theAccountLocation ($arg :  AccountLocation) {
      setVariableValue("theAccountLocation", 0, $arg)
    }
    
    property get thePolicyLocation () : PolicyLocation {
      return getVariableValue("thePolicyLocation", 0) as PolicyLocation
    }
    
    property set thePolicyLocation ($arg :  PolicyLocation) {
      setVariableValue("thePolicyLocation", 0, $arg)
    }
    
    property get updateVisible () : boolean {
      return getVariableValue("updateVisible", 0) as java.lang.Boolean
    }
    
    property set updateVisible ($arg :  boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    
        function checkChangeableState() {
      if(startState != null and not thePolicyLocation.canChangeState()){
        // the state cannot be changed
        if(thePolicyLocation.State != startState or thePolicyLocation.Country != startCountry){
            throw new gw.api.util.DisplayableException(DisplayKey.get("Web.Policy.Address.Validation.StateNotChangeable"))
        }
      }
    }
    
    function handleBeforeCommitForProduct() {
      if (policyPeriod.HasWorkersComp) {
        maybeCreateWCJurisdiction(thePolicyLocation)
      }
      if (policyPeriod.WC7LineExists) {
        maybeCreateWC7Jurisdiction(thePolicyLocation)
      }
      policyPeriod.Lines.each(\ p -> p.validateLocations(thePolicyLocation))
    }
    
    function maybeCreateLocation() {
      if (startInEdit and openForEdit) {
        if (thePolicyLocation == null) {
          if (theAccountLocation != null) {
            thePolicyLocation = policyPeriod.newLocation(theAccountLocation)
            for(var tc in thePolicyLocation.TerritoryCodes)
              tc.fillWithFirst()
          } else {
            thePolicyLocation = policyPeriod.newLocation()
            thePolicyLocation.State = gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(policyPeriod.BaseState)
          }
        }
        gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions(policyPeriod.PolicyLocations, null)
      }
    }
    
    function maybeCreateWCJurisdiction(location : PolicyLocation) {
      var loc = gw.api.util.JurisdictionMappingUtil.getJurisdiction(location)
      if (policyPeriod.WorkersCompLine.getJurisdiction(loc) == null) {
        policyPeriod.WorkersCompLine.addJurisdiction(loc)
      }
    }
    
    function maybeCreateWC7Jurisdiction(location : PolicyLocation) {
      var loc = gw.api.util.JurisdictionMappingUtil.getJurisdiction(location)
      if (policyPeriod.WC7Line.getJurisdiction(loc) == null) {
        policyPeriod.WC7Line.addJurisdiction(loc)
      }
    }
    
    function handleAfterReturnFromPopUp() {
      if (policyPeriod.WC7LineExists) {
        new WC7PolicyLineMethods(policyPeriod.WC7Line).validateLocationAfterReturnFromPopUp(thePolicyLocation)
      }
    }
    
    
  }
  
  
}