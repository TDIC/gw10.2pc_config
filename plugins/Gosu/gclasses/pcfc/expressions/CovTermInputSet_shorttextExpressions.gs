package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.shorttext.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermInputSet_shorttextExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.shorttext.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      (term as gw.api.domain.covterm.GenericCovTerm<String>).Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=StringTermInput1_Input) at CovTermInputSet.shorttext.pcf: line 32, column 144
    function editable_10 () : java.lang.Boolean {
      return setEditability(term)
    }
    
    // 'helpText' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function helpText_2 () : java.lang.String {
      return (term.PatternCodeIdentifier == "TDIC_Title10_NotInsured") ? "Enter information explaining who is not insured and the reason" : ""
    }
    
    // 'label' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function label_1 () : java.lang.Object {
      return term.Pattern.DisplayName
    }
    
    // 'required' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function required_3 () : java.lang.Boolean {
      return term.Pattern.Required
    }
    
    // 'value' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function valueRoot_6 () : java.lang.Object {
      return (term as gw.api.domain.covterm.GenericCovTerm<String>)
    }
    
    // 'value' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function value_4 () : java.lang.String {
      return (term as gw.api.domain.covterm.GenericCovTerm<String>).Value
    }
    
    // 'visible' attribute on TextAreaInput (id=StringTermInput_Input) at CovTermInputSet.shorttext.pcf: line 24, column 147
    function visible_0 () : java.lang.Boolean {
      return !(term.Pattern.CodeIdentifier == "GLERPReason_TDIC" || term.Pattern.CodeIdentifier == "GLCybEREReason_TDIC")
    }
    
    // 'visible' attribute on TextInput (id=StringTermInput1_Input) at CovTermInputSet.shorttext.pcf: line 32, column 144
    function visible_11 () : java.lang.Boolean {
      return term.Pattern.CodeIdentifier == "GLERPReason_TDIC" || term.Pattern.CodeIdentifier == "GLCybEREReason_TDIC"
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getRequireValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setRequireValue("term", 0, $arg)
    }
    
    function setEditability(directCov : gw.api.domain.covterm.CovTerm) : Boolean {
      var covArray = {"GLERPReason_TDIC","GLCybEREReason_TDIC"}
      if (covArray.contains(directCov.Pattern.CodeIdentifier)) return false
      return true
    }
    
    function isVisible(directCov : gw.api.domain.covterm.CovTerm) : Boolean {
      var covArray = {"GLERPReason_TDIC","GLCybEREReason_TDIC"}
      if (covArray.contains(directCov.Pattern.CodeIdentifier)) return true
      return true
    }
    
    
  }
  
  
}