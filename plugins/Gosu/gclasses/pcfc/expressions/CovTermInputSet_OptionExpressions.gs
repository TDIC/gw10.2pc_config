package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lob.gl.TDIC_CyberCoverageSubLimits
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.Option.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermInputSet_OptionExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.Option.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      (term as gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>>).OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>>)
    }
    
    // 'editable' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function editable_1 () : java.lang.Boolean {
      return isEditible()//openForEdit and gw.web.productmodel.ChoiceCovTermUtil.isEditable(term) and !(term.PatternCodeIdentifier == "BOPEQDeductible_TDIC" || term.PatternCodeIdentifier == "BOPEQSLDeductible_TDIC") and not ((term.PatternCodeIdentifier == "BOPEQTerritory_TDIC" || term.PatternCodeIdentifier == "BOPEQSLTerritory_TDIC"))
    }
    
    // 'label' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function label_3 () : java.lang.Object {
      return term.Pattern.DisplayName
    }
    
    // 'onChange' attribute on PostOnChange at CovTermInputSet.Option.pcf: line 28, column 45
    function onChange_0 () : void {
      invokeOnChangeMethods()
    }
    
    // 'required' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function required_4 () : java.lang.Boolean {
      return term.Pattern.Required
    }
    
    // 'valueRange' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function valueRange_8 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(term as gw.api.domain.covterm.OptionCovTerm, openForEdit)
    }
    
    // 'value' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function valueRoot_7 () : java.lang.Object {
      return (term as gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>>)
    }
    
    // 'value' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function value_5 () : gw.api.productmodel.CovTermOpt<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>> {
      return (term as gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>>).OptionValue
    }
    
    // 'valueRange' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function verifyValueRangeIsAllowedType_9 ($$arg :  gw.api.productmodel.CovTermOpt<gw.api.domain.covterm.OptionCovTerm<gw.api.domain.covterm.OptionCovTerm>>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function verifyValueRange_10 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(term as gw.api.domain.covterm.OptionCovTerm, openForEdit)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=OptionTermInput_Input) at CovTermInputSet.Option.pcf: line 26, column 72
    function visible_2 () : java.lang.Boolean {
      return term.PatternCodeIdentifier != "EQDeductible"
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getRequireValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setRequireValue("term", 0, $arg)
    }
    
    
    function isEditible() : boolean {
      var patternCode = {"GLNJDedAggLimit_TDIC", "GLAggLimitAB_TDIC", "GLAggLimitCovTerm_A_TDIC", "GLAggLimitCovTerm_B_TDIC","BOPEQSLBldgClass_TDIC","BOPEQSLZone_TDIC","BOPEQSLTerritory_TDIC","BOPEQSLDeductible_TDIC","BOPEQSLZipCode_TDIC","BOPEQDeductible_TDIC"}
      if (patternCode.contains(term.PatternCodeIdentifier)) return false
      return openForEdit and gw.web.productmodel.ChoiceCovTermUtil.isEditable(term)
    }
    
    function plCovRules() {
      if (term.Clause.OwningCoverable typeis GeneralLiabilityLine) {
        (term.Clause.OwningCoverable as GeneralLiabilityLine).setCybDeductibleLimit_TDIC()
        (term.Clause.OwningCoverable as GeneralLiabilityLine).setNJEndorsementLimits_TDIC()
        (term.Clause.OwningCoverable as GeneralLiabilityLine).checkCovALimits_TDIC()
        (term.Clause.OwningCoverable as GeneralLiabilityLine).setCovBLimits_TDIC()
        (term.Clause.OwningCoverable as GeneralLiabilityLine).checkAggLimitCovA_TDIC()
        (term.Clause.OwningCoverable as GeneralLiabilityLine).checkAggLimitCovB_TDIC()
        var cybLimits = new TDIC_CyberCoverageSubLimits()
        cybLimits.getCyberSubLimits(term.Clause.OwningCoverable as GeneralLiabilityLine)
      }
    }
    
    function invokeOnChangeMethods(){
      plCovRules();
      if (term.Clause.OwningCoverable typeis BOPBuilding) {
        if (term.Pattern.CodeIdentifier == "BOPEQBldgClass_TDIC") {
          (term.Clause.OwningCoverable as BOPBuilding).setEarthquakeDeductibles_TDIC();
          (term.Clause.OwningCoverable as BOPBuilding).setEarthquakeSLDeductibles_TDIC();
        }
        if (term.Pattern.CodeIdentifier == "BOPEQZone_TDIC")
          (term.Clause.OwningCoverable as BOPBuilding).setEQSLZone_TDIC();
      }
    }
    
    
  }
  
  
}