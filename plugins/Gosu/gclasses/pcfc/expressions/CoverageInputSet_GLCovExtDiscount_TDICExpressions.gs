package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCovExtDiscount_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLCovExtDiscount_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCovExtDiscount_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 31, column 99
    function available_22 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'value' attribute on TextInput (id=GLCovExtDiscountDescription_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 48, column 114
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.GLCovExtDiscount_TDIC.GLCEDescription_TDICTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionGLCEReason_TDICType>)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 20, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 24, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on TextInput (id=GLCovExtDiscountDescription_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 48, column 114
    function label_13 () : java.lang.Object {
      return glLine.GLCovExtDiscount_TDIC.GLCEDescription_TDICTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function label_2 () : java.lang.Object {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 31, column 99
    function label_23 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'required' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function required_3 () : java.lang.Boolean {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 31, column 99
    function setter_24 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'valueRange' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function valueRange_7 () : java.lang.Object {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm?.Pattern.Options
    }
    
    // 'value' attribute on TextInput (id=GLCovExtDiscountDescription_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 48, column 114
    function valueRoot_17 () : java.lang.Object {
      return glLine.GLCovExtDiscount_TDIC.GLCEDescription_TDICTerm
    }
    
    // 'value' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function valueRoot_6 () : java.lang.Object {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm
    }
    
    // 'value' attribute on TextInput (id=GLCovExtDiscountDescription_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 48, column 114
    function value_15 () : java.lang.String {
      return glLine.GLCovExtDiscount_TDIC.GLCEDescription_TDICTerm.Value
    }
    
    // 'value' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function value_4 () : gw.api.productmodel.CovTermOpt<productmodel.OptionGLCEReason_TDICType> {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm.OptionValue
    }
    
    // 'valueRange' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionGLCEReason_TDICType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=GLCovExtDiscountReason_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 39, column 98
    function verifyValueRange_9 () : void {
      var __valueRangeArg = glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm?.Pattern.Options
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=GLCovExtDiscountDescription_Input) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 48, column 114
    function visible_12 () : java.lang.Boolean {
      return glLine.GLCovExtDiscount_TDIC.GLCEReason_TDICTerm.OptionValue.Description == "Other"
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCovExtDiscount_TDIC.pcf: line 31, column 99
    function visible_21 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    
  }
  
  
}