package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper
@javax.annotation.Generated("config/web/pcf/job/cancellation/CancellationWizard_QuoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CancellationWizard_QuoteScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/job/cancellation/CancellationWizard_QuoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CancellationWizard_QuoteScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CreateCancellationQuote) at CancellationWizard_QuoteScreen.pcf: line 46, column 47
    function action_23 () : void {
      printQuote()
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 49, column 76
    function def_onEnter_24 (def :  pcf.WarningsPanelSet) : void {
      def.onEnter(policyPeriod.getWizardQuoteScreenWarnings())
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 51, column 43
    function def_onEnter_26 (def :  pcf.Quote_SummaryDV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_30 (def :  pcf.RatingCumulDetailsPanelSet_BOPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_32 (def :  pcf.RatingCumulDetailsPanelSet_BusinessAutoLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_34 (def :  pcf.RatingCumulDetailsPanelSet_CPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_36 (def :  pcf.RatingCumulDetailsPanelSet_GLLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_38 (def :  pcf.RatingCumulDetailsPanelSet_HOPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_40 (def :  pcf.RatingCumulDetailsPanelSet_IMLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_42 (def :  pcf.RatingCumulDetailsPanelSet_ManualLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_44 (def :  pcf.RatingCumulDetailsPanelSet_PersonalAutoLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_46 (def :  pcf.RatingCumulDetailsPanelSet_WC7Line) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_48 (def :  pcf.RatingCumulDetailsPanelSet_WorkersCompLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_50 (def :  pcf.RatingCumulDetailsPanelSet_default) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_55 (def :  pcf.RatingTxDetailsPanelSet_BOPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_57 (def :  pcf.RatingTxDetailsPanelSet_BusinessAutoLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_59 (def :  pcf.RatingTxDetailsPanelSet_CPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_61 (def :  pcf.RatingTxDetailsPanelSet_GLLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_63 (def :  pcf.RatingTxDetailsPanelSet_HOPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_65 (def :  pcf.RatingTxDetailsPanelSet_IMLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_67 (def :  pcf.RatingTxDetailsPanelSet_PersonalAutoLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_69 (def :  pcf.RatingTxDetailsPanelSet_WC7Line) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_71 (def :  pcf.RatingTxDetailsPanelSet_WorkersCompLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_73 (def :  pcf.RatingTxDetailsPanelSet_default) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 49, column 76
    function def_refreshVariables_25 (def :  pcf.WarningsPanelSet) : void {
      def.refreshVariables(policyPeriod.getWizardQuoteScreenWarnings())
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 51, column 43
    function def_refreshVariables_27 (def :  pcf.Quote_SummaryDV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_31 (def :  pcf.RatingCumulDetailsPanelSet_BOPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_33 (def :  pcf.RatingCumulDetailsPanelSet_BusinessAutoLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_35 (def :  pcf.RatingCumulDetailsPanelSet_CPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_37 (def :  pcf.RatingCumulDetailsPanelSet_GLLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_39 (def :  pcf.RatingCumulDetailsPanelSet_HOPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_41 (def :  pcf.RatingCumulDetailsPanelSet_IMLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_43 (def :  pcf.RatingCumulDetailsPanelSet_ManualLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_45 (def :  pcf.RatingCumulDetailsPanelSet_PersonalAutoLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_47 (def :  pcf.RatingCumulDetailsPanelSet_WC7Line) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_49 (def :  pcf.RatingCumulDetailsPanelSet_WorkersCompLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_51 (def :  pcf.RatingCumulDetailsPanelSet_default) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_56 (def :  pcf.RatingTxDetailsPanelSet_BOPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_58 (def :  pcf.RatingTxDetailsPanelSet_BusinessAutoLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_60 (def :  pcf.RatingTxDetailsPanelSet_CPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_62 (def :  pcf.RatingTxDetailsPanelSet_GLLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_64 (def :  pcf.RatingTxDetailsPanelSet_HOPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_66 (def :  pcf.RatingTxDetailsPanelSet_IMLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_68 (def :  pcf.RatingTxDetailsPanelSet_PersonalAutoLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_70 (def :  pcf.RatingTxDetailsPanelSet_WC7Line) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_72 (def :  pcf.RatingTxDetailsPanelSet_WorkersCompLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_74 (def :  pcf.RatingTxDetailsPanelSet_default) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'initialValue' attribute on Variable at CancellationWizard_QuoteScreen.pcf: line 20, column 28
    function initialValue_0 () : PolicyPeriod {
      return cancellation.PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at CancellationWizard_QuoteScreen.pcf: line 24, column 46
    function initialValue_1 () : gw.document.DocumentProduction {
      return new gw.document.DocumentProduction()
    }
    
    // 'initialValue' attribute on Variable at CancellationWizard_QuoteScreen.pcf: line 28, column 33
    function initialValue_2 () : entity.PolicyLine {
      return policyPeriod.RepresentativePolicyLines.first()
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function mode_3 () : java.lang.Object {
      return policyPeriod.Job.Subtype
    }
    
    // 'mode' attribute on PanelRef at CancellationWizard_QuoteScreen.pcf: line 60, column 40
    function mode_52 () : java.lang.Object {
      return line.Pattern.PublicID
    }
    
    // 'title' attribute on TitleBar at CancellationWizard_QuoteScreen.pcf: line 63, column 76
    function title_29 () : java.lang.String {
      return line.Pattern.DisplayName
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_10 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_12 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_14 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_16 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_18 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_20 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_4 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_6 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_onEnter_8 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.onEnter(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_11 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_13 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_15 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_17 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_19 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_21 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_5 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_7 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at CancellationWizard_QuoteScreen.pcf: line 33, column 100
    function toolbarButtonSet_refreshVariables_9 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.refreshVariables(policyPeriod, cancellation, jobWizardHelper)
    }
    
    // 'visible' attribute on ToolbarButton (id=CreateCancellationQuote) at CancellationWizard_QuoteScreen.pcf: line 46, column 47
    function visible_22 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on TitleBar at CancellationWizard_QuoteScreen.pcf: line 63, column 76
    function visible_28 () : java.lang.Boolean {
      return policyPeriod.RepresentativePolicyLines.Count > 1
    }
    
    property get cancellation () : Cancellation {
      return getRequireValue("cancellation", 0) as Cancellation
    }
    
    property set cancellation ($arg :  Cancellation) {
      setRequireValue("cancellation", 0, $arg)
    }
    
    property get documentProduction () : gw.document.DocumentProduction {
      return getVariableValue("documentProduction", 0) as gw.document.DocumentProduction
    }
    
    property set documentProduction ($arg :  gw.document.DocumentProduction) {
      setVariableValue("documentProduction", 0, $arg)
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get line () : entity.PolicyLine {
      return getVariableValue("line", 0) as entity.PolicyLine
    }
    
    property set line ($arg :  entity.PolicyLine) {
      setVariableValue("line", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
    function printQuote() {
      (new TDIC_PCExstreamHelper()).printQuoteDocs(policyPeriod)
    }
    
    
  }
  
  
}