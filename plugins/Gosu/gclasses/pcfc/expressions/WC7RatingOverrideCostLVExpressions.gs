package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RatingOverrideCostLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7RatingOverrideCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 228, column 68
    function def_onEnter_62 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 228, column 68
    function def_refreshVariables_63 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends WC7RatingOverrideCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 242, column 68
    function def_onEnter_66 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 242, column 68
    function def_refreshVariables_67 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends WC7RatingOverrideCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 256, column 68
    function def_onEnter_70 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 256, column 68
    function def_refreshVariables_71 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends WC7RatingOverrideCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 270, column 68
    function def_onEnter_74 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideCostLV.pcf: line 270, column 68
    function def_refreshVariables_75 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7RatingOverrideCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostLV.pcf: line 193, column 47
    function currency_41 () : typekey.Currency {
      return (cost.BranchUntyped as PolicyPeriod).PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostLV.pcf: line 182, column 45
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideBaseRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=OverrideAdjustedRate_Cell) at WC7RatingOverrideCostLV.pcf: line 188, column 45
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideAdjRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostLV.pcf: line 193, column 47
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideAmountBilling = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at WC7RatingOverrideCostLV.pcf: line 199, column 41
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostLV.pcf: line 182, column 45
    function editable_25 () : java.lang.Boolean {
      return cost.Overridable
    }
    
    // 'initialValue' attribute on Variable at WC7RatingOverrideCostLV.pcf: line 138, column 36
    function initialValue_3 () : WC7CoveredEmployee {
      return cost.WC7CoveredEmployee
    }
    
    // RowIterator at WC7RatingOverrideCostLV.pcf: line 134, column 42
    function initializeVariables_59 () : void {
        wcCovEmp = cost.WC7CoveredEmployee;

    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7RatingOverrideCostLV.pcf: line 147, column 42
    function valueRoot_5 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7RatingOverrideCostLV.pcf: line 156, column 24
    function value_10 () : java.lang.String {
      return cost.Description
    }
    
    // 'value' attribute on TextCell (id=BaseRate_Cell) at WC7RatingOverrideCostLV.pcf: line 161, column 45
    function value_13 () : java.math.BigDecimal {
      return cost.ActualBaseRate
    }
    
    // 'value' attribute on TextCell (id=AdjustedRate_Cell) at WC7RatingOverrideCostLV.pcf: line 166, column 45
    function value_16 () : java.math.BigDecimal {
      return cost.ActualAdjRate
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7RatingOverrideCostLV.pcf: line 171, column 45
    function value_19 () : java.math.BigDecimal {
      return cost.Basis
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at WC7RatingOverrideCostLV.pcf: line 176, column 54
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostLV.pcf: line 182, column 45
    function value_26 () : java.math.BigDecimal {
      return cost.OverrideBaseRate
    }
    
    // 'value' attribute on TextCell (id=OverrideAdjustedRate_Cell) at WC7RatingOverrideCostLV.pcf: line 188, column 45
    function value_32 () : java.math.BigDecimal {
      return cost.OverrideAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostLV.pcf: line 193, column 47
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return cost.OverrideAmountBilling
    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7RatingOverrideCostLV.pcf: line 147, column 42
    function value_4 () : java.lang.Integer {
      return cost.LocationNum
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at WC7RatingOverrideCostLV.pcf: line 199, column 41
    function value_45 () : java.lang.String {
      return cost.OverrideReason
    }
    
    // 'value' attribute on TextCell (id=StandardBaseRate_Cell) at WC7RatingOverrideCostLV.pcf: line 204, column 45
    function value_50 () : java.math.BigDecimal {
      return cost.StandardBaseRate
    }
    
    // 'value' attribute on TextCell (id=StandardAdjustedRate_Cell) at WC7RatingOverrideCostLV.pcf: line 209, column 45
    function value_53 () : java.math.BigDecimal {
      return cost.StandardAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at WC7RatingOverrideCostLV.pcf: line 213, column 54
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return cost.StandardAmountBilling
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RatingOverrideCostLV.pcf: line 151, column 41
    function value_7 () : java.lang.String {
      return cost.ClassCode
    }
    
    property get cost () : WC7CovEmpCost {
      return getIteratedValue(1) as WC7CovEmpCost
    }
    
    property get wcCovEmp () : WC7CoveredEmployee {
      return getVariableValue("wcCovEmp", 1) as WC7CoveredEmployee
    }
    
    property set wcCovEmp ($arg :  WC7CoveredEmployee) {
      setVariableValue("wcCovEmp", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RatingOverrideCostLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7RatingOverrideCostLV.pcf: line 25, column 57
    function initialValue_0 () : java.util.Set<entity.WC7CovEmpCost> {
      return periodCosts.whereTypeIs(WC7CovEmpCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at WC7RatingOverrideCostLV.pcf: line 30, column 63
    function initialValue_1 () : java.util.Set<entity.WC7JurisdictionCost> {
      return periodCosts.whereTypeIs(WC7JurisdictionCost).toSet()
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RatingOverrideCostLV.pcf: line 141, column 24
    function sortBy_2 (cost :  WC7CovEmpCost) : java.lang.Object {
      return cost.CalcOrder
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RatingOverrideCostLV.pcf: line 226, column 24
    function sortBy_61 (aggCost :  WC7JurisdictionCost) : java.lang.Object {
      return aggCost.CalcOrder
    }
    
    // 'value' attribute on RowIterator at WC7RatingOverrideCostLV.pcf: line 134, column 42
    function value_60 () : entity.WC7CovEmpCost[] {
      return costs.toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=lt100) at WC7RatingOverrideCostLV.pcf: line 223, column 48
    function value_64 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder( 0, 100 ).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=f100t200) at WC7RatingOverrideCostLV.pcf: line 237, column 48
    function value_68 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder( 101, 200 ).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=f200t300) at WC7RatingOverrideCostLV.pcf: line 251, column 48
    function value_72 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder( 201, 300 ).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=f300t400) at WC7RatingOverrideCostLV.pcf: line 265, column 48
    function value_76 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder( 301, 400 ).toTypedArray()
    }
    
    property get aggCosts () : java.util.Set<entity.WC7JurisdictionCost> {
      return getVariableValue("aggCosts", 0) as java.util.Set<entity.WC7JurisdictionCost>
    }
    
    property set aggCosts ($arg :  java.util.Set<entity.WC7JurisdictionCost>) {
      setVariableValue("aggCosts", 0, $arg)
    }
    
    property get costs () : java.util.Set<entity.WC7CovEmpCost> {
      return getVariableValue("costs", 0) as java.util.Set<entity.WC7CovEmpCost>
    }
    
    property set costs ($arg :  java.util.Set<entity.WC7CovEmpCost>) {
      setVariableValue("costs", 0, $arg)
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get periodCosts () : java.util.Set<WC7Cost> {
      return getRequireValue("periodCosts", 0) as java.util.Set<WC7Cost>
    }
    
    property set periodCosts ($arg :  java.util.Set<WC7Cost>) {
      setRequireValue("periodCosts", 0, $arg)
    }
    
    property get periodEnd () : java.util.Date {
      return getRequireValue("periodEnd", 0) as java.util.Date
    }
    
    property set periodEnd ($arg :  java.util.Date) {
      setRequireValue("periodEnd", 0, $arg)
    }
    
    property get periodStart () : java.util.Date {
      return getRequireValue("periodStart", 0) as java.util.Date
    }
    
    property set periodStart ($arg :  java.util.Date) {
      setRequireValue("periodStart", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}