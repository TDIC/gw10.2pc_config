package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPModifiersInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BOPModifiersInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.TypeKeyModifier = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=ModifierInput_Date_Input) at TDIC_BOPModifiersInputSet.pcf: line 59, column 66
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.DateModifier = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at TDIC_BOPModifiersInputSet.pcf: line 45, column 69
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.BooleanModifier = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at TDIC_BOPModifiersInputSet.pcf: line 45, column 69
    function label_6 () : java.lang.Object {
      return modifier.Pattern.Name
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function valueRange_18 () : java.lang.Object {
      return gw.util.TypekeyUtil.getTypeKeys(modifier.TypeList).map(\ t -> (t as gw.entity.TypeKey).Code)
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at TDIC_BOPModifiersInputSet.pcf: line 28, column 39
    function valueRoot_3 () : java.lang.Object {
      return modifier.Pattern
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at TDIC_BOPModifiersInputSet.pcf: line 45, column 69
    function valueRoot_9 () : java.lang.Object {
      return modifier
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function value_15 () : java.lang.String {
      return modifier.TypeKeyModifier
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at TDIC_BOPModifiersInputSet.pcf: line 28, column 39
    function value_2 () : java.lang.String {
      return modifier.Pattern.Name
    }
    
    // 'value' attribute on DateInput (id=ModifierInput_Date_Input) at TDIC_BOPModifiersInputSet.pcf: line 59, column 66
    function value_26 () : java.util.Date {
      return modifier.DateModifier
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at TDIC_BOPModifiersInputSet.pcf: line 45, column 69
    function value_7 () : java.lang.Boolean {
      return modifier.BooleanModifier
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function verifyValueRange_20 () : void {
      var __valueRangeArg = gw.util.TypekeyUtil.getTypeKeys(modifier.TypeList).map(\ t -> (t as gw.entity.TypeKey).Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at TDIC_BOPModifiersInputSet.pcf: line 53, column 69
    function visible_13 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_TYPEKEY
    }
    
    // 'visible' attribute on DateInput (id=ModifierInput_Date_Input) at TDIC_BOPModifiersInputSet.pcf: line 59, column 66
    function visible_24 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_DATE
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at TDIC_BOPModifiersInputSet.pcf: line 45, column 69
    function visible_5 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_BOOLEAN
    }
    
    property get modifier () : entity.Modifier {
      return getIteratedValue(1) as entity.Modifier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPModifiersInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPModifiersInputSet.pcf: line 20, column 24
    function sortBy_0 (modifier :  entity.Modifier) : java.lang.Object {
      return modifier.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPModifiersInputSet.pcf: line 24, column 24
    function sortBy_1 (modifier :  entity.Modifier) : java.lang.Object {
      return modifier.Pattern.Name
    }
    
    // 'value' attribute on InputIterator (id=ModIterator) at TDIC_BOPModifiersInputSet.pcf: line 17, column 37
    function value_32 () : entity.Modifier[] {
      return modifiers.where( \ mod -> mod.DataType != TC_RATE or mod.ScheduleRate).toTypedArray()
    }
    
    property get modifiers () : List<Modifier> {
      return getRequireValue("modifiers", 0) as List<Modifier>
    }
    
    property set modifiers ($arg :  List<Modifier>) {
      setRequireValue("modifiers", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function toggleEligible(mod : Modifier) {
      if (mod.Eligible) {
        mod.RateModifier = null
        mod.Eligible = false
      } else {
        mod.Eligible = true
      }
    }
    
    
  }
  
  
}