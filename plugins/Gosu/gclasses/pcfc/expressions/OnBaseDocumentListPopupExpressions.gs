package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDocumentListPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentListPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, EntityDescription :  java.lang.String, Beans :  KeyableBean[]) : int {
      return 0
    }
    
    // 'def' attribute on InputSetRef at OnBaseDocumentListPopup.pcf: line 57, column 68
    function def_onEnter_10 (def :  pcf.OnBaseDocumentInputSet) : void {
      def.onEnter(Entity, LinkType, Beans)
    }
    
    // 'def' attribute on InputSetRef at OnBaseDocumentListPopup.pcf: line 57, column 68
    function def_refreshVariables_11 (def :  pcf.OnBaseDocumentInputSet) : void {
      def.refreshVariables(Entity, LinkType, Beans)
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 19, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 23, column 58
    function initialValue_1 () : acc.onbase.api.application.DocumentLinking {
      return new acc.onbase.api.application.DocumentLinking()
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 31, column 43
    function initialValue_2 () : List<entity.Document> {
      return DocumentLinking.getDocumentsLinkedToEntity(Entity, LinkType)
    }
    
    // 'value' attribute on TextInput (id=EntityId_Input) at OnBaseDocumentListPopup.pcf: line 41, column 38
    function valueRoot_4 () : java.lang.Object {
      return Entity
    }
    
    // 'value' attribute on TextInput (id=EntityId_Input) at OnBaseDocumentListPopup.pcf: line 41, column 38
    function value_3 () : java.lang.String {
      return Entity.PublicID
    }
    
    // 'value' attribute on TextInput (id=EntityType_Input) at OnBaseDocumentListPopup.pcf: line 46, column 68
    function value_6 () : acc.onbase.configuration.DocumentLinkType {
      return LinkType
    }
    
    // 'value' attribute on TextAreaInput (id=EntityDesc_Input) at OnBaseDocumentListPopup.pcf: line 51, column 40
    function value_8 () : java.lang.String {
      return EntityDescription
    }
    
    property get Beans () : KeyableBean[] {
      return getVariableValue("Beans", 0) as KeyableBean[]
    }
    
    property set Beans ($arg :  KeyableBean[]) {
      setVariableValue("Beans", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.OnBaseDocumentListPopup {
      return super.CurrentLocation as pcf.OnBaseDocumentListPopup
    }
    
    property get DocumentLinking () : acc.onbase.api.application.DocumentLinking {
      return getVariableValue("DocumentLinking", 0) as acc.onbase.api.application.DocumentLinking
    }
    
    property set DocumentLinking ($arg :  acc.onbase.api.application.DocumentLinking) {
      setVariableValue("DocumentLinking", 0, $arg)
    }
    
    property get Entity () : KeyableBean {
      return getVariableValue("Entity", 0) as KeyableBean
    }
    
    property set Entity ($arg :  KeyableBean) {
      setVariableValue("Entity", 0, $arg)
    }
    
    property get EntityDescription () : String {
      return getVariableValue("EntityDescription", 0) as String
    }
    
    property set EntityDescription ($arg :  String) {
      setVariableValue("EntityDescription", 0, $arg)
    }
    
    property get LinkType () : acc.onbase.configuration.DocumentLinkType {
      return getVariableValue("LinkType", 0) as acc.onbase.configuration.DocumentLinkType
    }
    
    property set LinkType ($arg :  acc.onbase.configuration.DocumentLinkType) {
      setVariableValue("LinkType", 0, $arg)
    }
    
    property get LinkedDocuments () : List<entity.Document> {
      return getVariableValue("LinkedDocuments", 0) as List<entity.Document>
    }
    
    property set LinkedDocuments ($arg :  List<entity.Document>) {
      setVariableValue("LinkedDocuments", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}