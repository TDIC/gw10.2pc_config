package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lang.parser.ScriptabilityModifiers
uses gw.lang.reflect.TypeSystem
uses gw.lang.parser.GosuParserFactory
uses java.io.PrintStream
uses java.io.ByteArrayOutputStream
@javax.annotation.Generated("config/web/pcf/admin/gosuscriptrunner/GosuScriptRunnerPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GosuScriptRunnerPageExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/gosuscriptrunner/GosuScriptRunnerPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GosuScriptRunnerPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ButtonInput (id=Execute_Input) at GosuScriptRunnerPage.pcf: line 37, column 42
    function action_4 () : void {
      executeScript()
    }
    
    // 'canVisit' attribute on Page (id=GosuScriptRunnerPage) at GosuScriptRunnerPage.pcf: line 12, column 44
    static function canVisit_7 () : java.lang.Boolean {
      return perm.User.ViewDataChange
    }
    
    // 'value' attribute on TextAreaInput (id=Script_Input) at GosuScriptRunnerPage.pcf: line 33, column 39
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      gosuScript = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on Label at GosuScriptRunnerPage.pcf: line 24, column 103
    function label_0 () : java.lang.String {
      return "Environment - " + java.lang.System.getProperty("gw.pc.env")
    }
    
    // 'parent' attribute on Page (id=GosuScriptRunnerPage) at GosuScriptRunnerPage.pcf: line 12, column 44
    static function parent_8 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'value' attribute on TextAreaInput (id=Script_Input) at GosuScriptRunnerPage.pcf: line 33, column 39
    function value_1 () : java.lang.String {
      return gosuScript
    }
    
    // 'value' attribute on TextAreaInput (id=Result_Input) at GosuScriptRunnerPage.pcf: line 46, column 29
    function value_5 () : java.lang.String {
      return result
    }
    
    override property get CurrentLocation () : pcf.GosuScriptRunnerPage {
      return super.CurrentLocation as pcf.GosuScriptRunnerPage
    }
    
    property get gosuScript () : String {
      return getVariableValue("gosuScript", 0) as String
    }
    
    property set gosuScript ($arg :  String) {
      setVariableValue("gosuScript", 0, $arg)
    }
    
    property get result () : String {
      return getVariableValue("result", 0) as String
    }
    
    property set result ($arg :  String) {
      setVariableValue("result", 0, $arg)
    }
    
    
    
        function executeScript() {
          var out = new ByteArrayOutputStream()
          var results = ""
          var os = System.out
          using (var ps = new PrintStream(out)) {
            System.setOut(ps)
            var p = GosuParserFactory.createParser(TypeSystem.getCompiledGosuClassSymbolTable(), ScriptabilityModifiers.SCRIPTABLE)
            p.setThrowParseExceptionForWarnings(false)
            p.setWarnOnCaseIssue(false)
            p.setEditorParser(false)
            p.setDontOptimizeStatementLists(false)
            p.setScript(gosuScript)
    
            var expr = p.parseExpOrProgram(null)
            var res = expr.evaluate()
            results = (res != null) ? res as String : ""
          }
          result = out.toString() + "\n" + results
          System.setOut(os)
        }
        
    
    
  }
  
  
}