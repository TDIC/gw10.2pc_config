package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.text.DecimalFormat
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermDirectInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermDirectInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermDirectInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermDirectInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      directCovTerm.Value = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'editable' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function editable_2 () : java.lang.Boolean {
      return isEditable
    }
    
    // 'initialValue' attribute on Variable at CovTermDirectInputSet.pcf: line 17, column 23
    function initialValue_0 () : Boolean {
      return setEditability(directCovTerm)
    }
    
    // 'inputConversion' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function inputConversion_6 (VALUE :  java.lang.String) : java.lang.Object {
      return getConvertValueFromString(VALUE)//gw.pcf.coverage.CovTermDirectInputSetHelper.convertFromString(VALUE)
    }
    
    // 'inputMask' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function inputMask_12 () : java.lang.String {
      return getInputMaskIfZipCode()
    }
    
    // 'label' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function label_5 () : java.lang.Object {
      return directCovTerm.Pattern.DisplayName
    }
    
    // 'onChange' attribute on PostOnChange at CovTermDirectInputSet.pcf: line 31, column 194
    function onChange_1 () : void {
      if (directCovTerm.Pattern.CodeIdentifier == "BOPBldgLim" || directCovTerm.Pattern.CodeIdentifier == "BOPBPPBldgLim") {setCovTermLimits()} ;plCovRules();
    }
    
    // 'onChange' attribute on PostOnChange at CovTermDirectInputSet.pcf: line 46, column 194
    function onChange_18 () : void {
      if (directCovTerm.Pattern.CodeIdentifier == "BOPBldgLim" || directCovTerm.Pattern.CodeIdentifier == "BOPBPPBldgLim") {setCovTermLimits()} ;plCovRules();
    }
    
    // 'onChange' attribute on PostOnChange at CovTermDirectInputSet.pcf: line 61, column 194
    function onChange_34 () : void {
      if (directCovTerm.Pattern.CodeIdentifier == "BOPBldgLim" || directCovTerm.Pattern.CodeIdentifier == "BOPBPPBldgLim") {setCovTermLimits()} ;plCovRules();
    }
    
    // 'onChange' attribute on PostOnChange at CovTermDirectInputSet.pcf: line 76, column 194
    function onChange_50 () : void {
      if (directCovTerm.Pattern.CodeIdentifier == "BOPBldgLim" || directCovTerm.Pattern.CodeIdentifier == "BOPBPPBldgLim") {setCovTermLimits()} ;plCovRules();
    }
    
    // 'outputConversion' attribute on TextInput (id=DirectTermInputMoney_Input) at CovTermDirectInputSet.pcf: line 44, column 154
    function outputConversion_24 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.getOutputConversion_TDIC(VALUE,directCovTerm) 
    }
    
    // 'outputConversion' attribute on TextInput (id=DirectTermInputMoney2_Input) at CovTermDirectInputSet.pcf: line 59, column 155
    function outputConversion_40 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return VALUE!=null?"$"+gw.pcf.coverage.CovTermDirectInputSetHelper.getOutputConversion_TDIC(VALUE,directCovTerm).toString():null 
    }
    
    // 'outputConversion' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function outputConversion_7 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.getOutputConversion_TDIC(VALUE,directCovTerm)
    }
    
    // 'required' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function required_8 () : java.lang.Boolean {
      return directCovTerm.Pattern.Required
    }
    
    // 'validationExpression' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function validationExpression_3 () : java.lang.Object {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.validate(directCovTerm)
    }
    
    // 'value' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function valueRoot_11 () : java.lang.Object {
      return directCovTerm
    }
    
    // 'value' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function value_9 () : java.math.BigDecimal {
      return directCovTerm.Value
    }
    
    // 'visible' attribute on TextInput (id=DirectTermInputMoney_Input) at CovTermDirectInputSet.pcf: line 44, column 154
    function visible_21 () : java.lang.Boolean {
      return setVisibility(directCovTerm) && directCovTerm.ValueType == CovTermModelVal.TC_MONEY && isEditable && openForEdit
    }
    
    // 'visible' attribute on TextInput (id=DirectTermInputMoney2_Input) at CovTermDirectInputSet.pcf: line 59, column 155
    function visible_37 () : java.lang.Boolean {
      return setVisibility(directCovTerm) && directCovTerm.ValueType == CovTermModelVal.TC_MONEY && isEditable && !openForEdit
    }
    
    // 'visible' attribute on TextInput (id=DirectTermInput_Input) at CovTermDirectInputSet.pcf: line 29, column 112
    function visible_4 () : java.lang.Boolean {
      return setVisibility(directCovTerm) && !(directCovTerm.ValueType == CovTermModelVal.TC_MONEY)
    }
    
    // 'visible' attribute on TextInput (id=DirectTermInputMoney1_Input) at CovTermDirectInputSet.pcf: line 74, column 132
    function visible_53 () : java.lang.Boolean {
      return setVisibility(directCovTerm) && directCovTerm.ValueType == CovTermModelVal.TC_MONEY && !isEditable
    }
    
    property get directCovTerm () : gw.api.domain.covterm.DirectCovTerm {
      return getRequireValue("directCovTerm", 0) as gw.api.domain.covterm.DirectCovTerm
    }
    
    property set directCovTerm ($arg :  gw.api.domain.covterm.DirectCovTerm) {
      setRequireValue("directCovTerm", 0, $arg)
    }
    
    property get isEditable () : Boolean {
      return getVariableValue("isEditable", 0) as Boolean
    }
    
    property set isEditable ($arg :  Boolean) {
      setVariableValue("isEditable", 0, $arg)
    }
    
    property get openForEdit () : Boolean {
      return getRequireValue("openForEdit", 0) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    
    function setEditability(directCov : gw.api.domain.covterm.DirectCovTerm) : Boolean {
      var covArray = {"BOPEmpDisIncBasicLimit_TDIC", "BOPGoldIncBasicLimit_TDIC", "BOPExtraExpIncBasicLimit_TDIC", "BOPFineArtsIncBasicLimit_TDIC", "BOPFungiIncBasicLimit_TDIC", "BOPIncBasicLimit_TDIC", "BOPMoneySecInclBasicLimit_TDIC", "BOPIncBasicLimitSigns_TDIC",
          "BOPValIncBasicLimit_TDIC", "BOPAccIncBasicLimit_TDIC", "BOPEDPInclLimit_TDIC", "BOPDigitalXRayInclLimit_TDIC", "BOPLockandKeyCovIncLimit_TDIC", "BOPExpedExpLimit_TDIC", "BOPCompEquipLimit_TDIC", "BOPHazardSubLimit_TDIC", "BOPNewBPPIncLimit_TDIC",
          "BOPNewBuildIncLimit_TDIC", "BOPOrdLawUndamageLimit_TDIC", "BOPOrdLawDemolitionLimit_TDIC", "BOPOrdLawIncCostLimit_TDIC", "BOPArsonRewardIncLimit_TDIC", "BOPEQSLDeductible_TDIC", "BOPEQDeductible_TDIC", "BOPEQSLInclLimit_TDIC", "BOPEQInclLimit_TDIC",
          "BOPDebrisRemovInclLimit_TDIC", "BOPBPPDebrisRemoInclLimit_TDIC", "BOPAddDebrisRemovalIncLimit_TDIC", "BOPFireDeptSerChargeInclLimit_TDIC", "BOPFireExtRechargeInclLimit_TDIC", "BOPPersonalEffectsIncLimit_TDIC", "BOPPollCleanRemovInclLimit_TDIC", "BOPLossOutdoorInclLimit_TDIC",
          "BOPBPPOffPremInclLimit_TDIC", "BOPILMineSubInclLimit_TDIC", "GLDMWCoPay_TDIC", "GLCybLiabDedLimit_TDIC", "GLDBLPerClaimLimit_TDIC", "GLDBLAggLimit_TDIC", "GLCyb1RegFinesSL_TDIC", "GLCyb1PCIFinesSL_TDIC", "GLCyb1DCRExpDed_TDIC", "GLERPRated_TDIC", "GLCybERERated_TDIC",
          "BOPEQZipCode_TDIC", "BOPEQSLZipCode_TDIC", "GLDBLPerOccLimit_TDIC", "GLERPReason_TDIC", "GLCybDeductibleLimit_TDIC", "GLCyb1Limit_TDIC", "GLCyb1NamedMalCodesSL_TDIC", "GLCyb1ForITSL_TDIC", "GLCyb1LegReviewSL_TDIC", "GLCyb1PubRelSL_TDIC", "GLCyb4NamedMalwareSL_TDIC",
          "GLCyb2Limit_TDIC", "GLCyb2LossBussSL_TDIC", "GLCyb2PRSL_TDIC", "GLCyb2ExtSL_TDIC", "GLCyb2CompAttack_TDIC", "GLCyb4Limit_TDIC", "GLCyb4DataComp_TDIC", "GLCyb5Limit_TDIC", "GLCyb5NSLDed_TDIC", "GLCyb6Limit_TDIC", "GLCyb6ElecMedia_TDIC", "GLCyb1NamedMalwareSL_TDIC" ,
      "BOPDMWLDAggLimit_TDIC","BOPDEBLAggLimit_TDIC","BOPEQSLBldgClass_TDIC","BOPEQSLZone_TDIC","BOPEQSLTerritory_TDIC","BOPEQSLDeductible_TDIC","BOPEQSLZipCode_TDIC", "BOPWASGEACCTDIC", "BOPWASGEEMPTDIC", "BOPWASGPLIMTDIC", "BOPEncCovFineCondIncLimit_TDIC", "BOPEncCovMoneySCondIncLimit_TDIC"}
    
      if (covArray.contains(directCov.Pattern.CodeIdentifier)) return false
      return true
    }
    
    function setVisibility(directCov : gw.api.domain.covterm.DirectCovTerm) : Boolean {
      var covArray = {"GLExpCov_TDIC", "GLLostWagesDaily_TDIC", "GLLossWagesTotal_TDIC", "GLMentalHealth_TDIC" }
      if (covArray.contains(directCov.Pattern.CodeIdentifier)) return false
      return true
    }
    
    function setCovTermLimits() {
      if (directCovTerm.Clause.OwningCoverable typeis BOPBuilding) {
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setOrdinanceTermLimits_TDIC();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).syncCoverages();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setEarthquakeLimits_TDIC()
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setBPPDebrisLimit_TDIC();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setBuildingDebrisLimit_TDIC();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setILSubLimit_TDIC();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).setEnhancedCoverageLimits();
        (directCovTerm.Clause.OwningCoverable as BOPBuilding).remEnhancedCoverageLimits();
      }
    }
    
    function plCovRules() {
      if (directCovTerm.Clause.OwningCoverable typeis GeneralLiabilityLine) {
        (directCovTerm.Clause.OwningCoverable as GeneralLiabilityLine).setCybDeductibleLimit_TDIC()
        (directCovTerm.Clause.OwningCoverable as GeneralLiabilityLine).setNJEndorsementLimits_TDIC()
        (directCovTerm.Clause.OwningCoverable as GeneralLiabilityLine).checkCovALimits_TDIC()
        (directCovTerm.Clause.OwningCoverable as GeneralLiabilityLine).checkAggLimitCovA_TDIC()
        (directCovTerm.Clause.OwningCoverable as GeneralLiabilityLine).checkAggLimitCovB_TDIC()
      }
    }
    
    function getInputMaskIfZipCode() : String {
      var covTermArray = {"BOPEQZipCode_TDIC", "BOPEQSLZipCode_TDIC" }
      if (covTermArray.contains(directCovTerm.Pattern.CodeIdentifier)) {
        return "#####"
      }
      return null
    }
    
    function getConvertValueFromString(val : String) : Object {
      var covTermArray = {"BOPEQZipCode_TDIC", "BOPEQSLZipCode_TDIC" }
      if (covTermArray.contains(directCovTerm.Pattern.CodeIdentifier)) {
        return val as Object
      }
      return gw.pcf.coverage.CovTermDirectInputSetHelper.convertFromString(val)
    }
    
    
  }
  
  
}