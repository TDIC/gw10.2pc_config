package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLIRPMModifiersDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLIRPMModifiersDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLIRPMModifiersDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_GLIRPMModifiersDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewInput at TDIC_GLIRPMModifiersDV.pcf: line 44, column 39
    function available_7 () : java.lang.Boolean {
      return modifiers != null
    }
    
    // 'def' attribute on ListViewInput at TDIC_GLIRPMModifiersDV.pcf: line 44, column 39
    function def_onEnter_9 (def :  pcf.ScheduleRateLV) : void {
      def.onEnter(scheduleRate)
    }
    
    // 'def' attribute on ListViewInput at TDIC_GLIRPMModifiersDV.pcf: line 44, column 39
    function def_refreshVariables_10 (def :  pcf.ScheduleRateLV) : void {
      def.refreshVariables(scheduleRate)
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at TDIC_GLIRPMModifiersDV.pcf: line 38, column 45
    function valueRoot_5 () : java.lang.Object {
      return scheduleRate
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at TDIC_GLIRPMModifiersDV.pcf: line 38, column 45
    function value_4 () : java.lang.String {
      return scheduleRate.DisplayName
    }
    
    property get scheduleRate () : entity.Modifier {
      return getIteratedValue(1) as entity.Modifier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLIRPMModifiersDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLIRPMModifiersDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on DetailViewPanel (id=TDIC_GLIRPMModifiersDV) at TDIC_GLIRPMModifiersDV.pcf: line 9, column 80
    function editable_12 () : java.lang.Boolean {
      return glLine.JobType == typekey.Job.TC_POLICYCHANGE ? false : true 
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLIRPMModifiersDV.pcf: line 16, column 35
    function initialValue_0 () : productmodel.GLLine {
      return policyline as GLLine
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLIRPMModifiersDV.pcf: line 20, column 26
    function initialValue_1 () : Modifier[] {
      return setModValues()
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLIRPMModifiersDV.pcf: line 32, column 26
    function sortBy_3 (scheduleRate :  entity.Modifier) : java.lang.Object {
      return scheduleRate.Pattern.Priority
    }
    
    // 'value' attribute on InputIterator at TDIC_GLIRPMModifiersDV.pcf: line 29, column 39
    function value_11 () : entity.Modifier[] {
      return modifiers
    }
    
    // 'visible' attribute on DetailViewPanel (id=TDIC_GLIRPMModifiersDV) at TDIC_GLIRPMModifiersDV.pcf: line 9, column 80
    function visible_13 () : java.lang.Boolean {
      return glLine.GLIRPMDiscount_TDICExists and isIRPMRatingInputsVisible()
    }
    
    // 'visible' attribute on Label at TDIC_GLIRPMModifiersDV.pcf: line 24, column 80
    function visible_2 () : java.lang.Boolean {
      return not glLine.GLIRPMDiscount_TDICExists//scheduleRates.IsEmpty
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get modifiers () : Modifier[] {
      return getVariableValue("modifiers", 0) as Modifier[]
    }
    
    property set modifiers ($arg :  Modifier[]) {
      setVariableValue("modifiers", 0, $arg)
    }
    
    property get policyline () : PolicyLine {
      return getRequireValue("policyline", 0) as PolicyLine
    }
    
    property set policyline ($arg :  PolicyLine) {
      setRequireValue("policyline", 0, $arg)
    }
    
    function setModValues():Modifier[]{
      return glLine.GLModifiers.Count > 0? glLine.GLModifiers.where(\mod -> mod.PatternCode == "z14gaumjcl22ae407klkm2j1n0b") : new Modifier[]{}
    }
    
    function isIRPMRatingInputsVisible(): boolean {
      if (not User.util.CurrentUser.ExternalUser) {
        return (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic)
      }
      return false
    }
    
    
  }
  
  
}