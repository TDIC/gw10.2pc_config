package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7AuditRateCostDetailStateLVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7AuditRateCostDetailStateLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 116, column 32
    function initialValue_19 () : entity.WC7Cost {
      return stateCostMap.get( cost.CostKey )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 121, column 32
    function initialValue_20 () : entity.WC7Cost {
      return basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder)//basedOnCostMap.get( cost.CostKey )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 126, column 47
    function initialValue_21 () : gw.pl.currency.MonetaryAmount {
      return aggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : aggCost.ActualTermAmountBilling
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 130, column 47
    function initialValue_22 () : gw.pl.currency.MonetaryAmount {
      return basedOnAggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : basedOnAggCost.ActualTermAmountBilling
    }
    
    // RowIterator (id=f400t500) at WC7AuditRateCostDetailStateLV.pcf: line 111, column 36
    function initializeVariables_45 () : void {
        aggCost = stateCostMap.get( cost.CostKey );
  basedOnAggCost = basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder)//basedOnCostMap.get( cost.CostKey );
  aggCostTermAmount = aggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : aggCost.ActualTermAmountBilling;
  basedOnTermAmt = basedOnAggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : basedOnAggCost.ActualTermAmountBilling;

    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 142, column 41
    function valueRoot_24 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 142, column 41
    function value_23 () : java.lang.String {
      return cost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 146, column 41
    function value_26 () : java.lang.String {
      return cost.Description
    }
    
    // 'value' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 152, column 42
    function value_29 () : java.lang.String {
      return basedOnAggCost == null or basedOnAggCost.Basis == 0 ? "" : basedOnAggCost.Basis.DisplayValue
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 157, column 41
    function value_32 () : java.lang.String {
      return aggCost == null or aggCost.Basis == 0 ? "" : aggCost.Basis.DisplayValue
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 163, column 42
    function value_34 () : java.lang.String {
      return basedOnAggCost == null or basedOnAggCost.ActualAdjRate == 0 ? "" : basedOnAggCost.ActualAdjRate as String
    }
    
    // 'value' attribute on MonetaryAmountCell (id=EstPremium_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 169, column 42
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return basedOnTermAmt
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 180, column 38
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return aggCostTermAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 186, column 42
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return aggCostTermAmount - basedOnTermAmt
    }
    
    // 'visible' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 152, column 42
    function visible_30 () : java.lang.Boolean {
      return not isPremiumReport
    }
    
    property get aggCost () : entity.WC7Cost {
      return getVariableValue("aggCost", 1) as entity.WC7Cost
    }
    
    property set aggCost ($arg :  entity.WC7Cost) {
      setVariableValue("aggCost", 1, $arg)
    }
    
    property get aggCostTermAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("aggCostTermAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set aggCostTermAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("aggCostTermAmount", 1, $arg)
    }
    
    property get basedOnAggCost () : entity.WC7Cost {
      return getVariableValue("basedOnAggCost", 1) as entity.WC7Cost
    }
    
    property set basedOnAggCost ($arg :  entity.WC7Cost) {
      setVariableValue("basedOnAggCost", 1, $arg)
    }
    
    property get basedOnTermAmt () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("basedOnTermAmt", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set basedOnTermAmt ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("basedOnTermAmt", 1, $arg)
    }
    
    property get cost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7AuditRateCostDetailStateLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 23, column 51
    function initialValue_0 () : java.util.Set<entity.WC7Cost> {
      return stateCosts.byCalcOrder_TDIC(401, 1000000, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM)//stateCosts.byCalcOrder(401, 1000000)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 28, column 51
    function initialValue_1 () : java.util.Set<entity.WC7Cost> {
      return basedOnStateCosts.byCalcOrder_TDIC(401, 1000000, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM)//basedOnStateCosts.byCalcOrder(401, 1000000)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 33, column 51
    function initialValue_2 () : java.util.Set<entity.WC7Cost> {
      return getAllCosts( stateAggCosts, basedOnAggCosts )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 38, column 84
    function initialValue_3 () : java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost> {
      return stateAggCosts.partitionUniquely( \ c -> c.CostKey )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 43, column 84
    function initialValue_4 () : java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost> {
      return basedOnAggCosts.partitionUniquely( \ c -> c.CostKey )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailStateLV.pcf: line 47, column 32
    function initialValue_5 () : typekey.Currency {
      return jurisdiction.Branch.PreferredSettlementCurrency
    }
    
    // 'sortBy' attribute on IteratorSort at WC7AuditRateCostDetailStateLV.pcf: line 133, column 24
    function sortBy_18 (cost :  WC7JurisdictionCost) : java.lang.Object {
      return cost.CalcOrder
    }
    
    // 'value' attribute on TextCell (id=EstPremium_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 83, column 40
    function value_12 () : java.lang.String {
      return premiumLabel()//amountLabel()
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 92, column 39
    function value_15 () : java.lang.String {
      return isPremiumReport ? DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Premium") : DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Audited")
    }
    
    // 'value' attribute on RowIterator (id=f400t500) at WC7AuditRateCostDetailStateLV.pcf: line 111, column 36
    function value_46 () : entity.WC7Cost[] {
      return allCosts.byCalcOrder_TDIC( 401, 500, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=EstTotalPremium_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 225, column 40
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return basedOnStateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//basedOnStateCosts.where( \ w -> w.CalcOrder < 501 ).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountSubtotal400_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 235, column 191
    function value_53 () : gw.pl.currency.MonetaryAmount {
      return stateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//stateCosts.where( \ w -> w.CalcOrder < 501 ).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PremiumDifference_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 241, column 40
    function value_55 () : gw.pl.currency.MonetaryAmount {
      return stateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency) - basedOnStateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//stateCosts.where( \ w -> w.CalcOrder < 501 ).AmountSum(currency) - basedOnStateCosts.where( \ w -> w.CalcOrder < 501 ).AmountSum(currency)
    }
    
    // 'value' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 69, column 40
    function value_6 () : java.lang.String {
      return basisLabel()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=EstSubtotal_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 369, column 40
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return basedOnStateCosts.AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CumulAmountSubtotal500_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 379, column 49
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return stateCosts.AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SubtotalDifference_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 385, column 40
    function value_65 () : gw.pl.currency.MonetaryAmount {
      return stateCosts.AmountSum(currency) - basedOnStateCosts.AmountSum(currency)
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 73, column 39
    function value_9 () : java.lang.String {
      return isPremiumReport ? DisplayKey.get("Web.AuditWizard.Basis") : DisplayKey.get("Web.AuditWizard.AuditedBasis")
    }
    
    // 'type' attribute on RowIterator (id=f400t500) at WC7AuditRateCostDetailStateLV.pcf: line 111, column 36
    function verifyIteratorType_47 () : void {
      var entry : entity.WC7Cost = null
      var typedEntry : WC7JurisdictionCost
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as WC7JurisdictionCost
    }
    
    // 'visible' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailStateLV.pcf: line 69, column 40
    function visible_7 () : java.lang.Boolean {
      return not isPremiumReport
    }
    
    property get allCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("allCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set allCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("allCosts", 0, $arg)
    }
    
    property get basedOnAggCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("basedOnAggCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set basedOnAggCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("basedOnAggCosts", 0, $arg)
    }
    
    property get basedOnCostMap () : java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost> {
      return getVariableValue("basedOnCostMap", 0) as java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost>
    }
    
    property set basedOnCostMap ($arg :  java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost>) {
      setVariableValue("basedOnCostMap", 0, $arg)
    }
    
    property get basedOnStateCosts () : java.util.Set<WC7Cost> {
      return getRequireValue("basedOnStateCosts", 0) as java.util.Set<WC7Cost>
    }
    
    property set basedOnStateCosts ($arg :  java.util.Set<WC7Cost>) {
      setRequireValue("basedOnStateCosts", 0, $arg)
    }
    
    property get currency () : typekey.Currency {
      return getVariableValue("currency", 0) as typekey.Currency
    }
    
    property set currency ($arg :  typekey.Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get isPremiumReport () : boolean {
      return getRequireValue("isPremiumReport", 0) as java.lang.Boolean
    }
    
    property set isPremiumReport ($arg :  boolean) {
      setRequireValue("isPremiumReport", 0, $arg)
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get stateAggCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("stateAggCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set stateAggCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("stateAggCosts", 0, $arg)
    }
    
    property get stateCostMap () : java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost> {
      return getVariableValue("stateCostMap", 0) as java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost>
    }
    
    property set stateCostMap ($arg :  java.util.Map<gw.api.domain.financials.CostKey, entity.WC7Cost>) {
      setVariableValue("stateCostMap", 0, $arg)
    }
    
    property get stateCosts () : java.util.Set<WC7Cost> {
      return getRequireValue("stateCosts", 0) as java.util.Set<WC7Cost>
    }
    
    property set stateCosts ($arg :  java.util.Set<WC7Cost>) {
      setRequireValue("stateCosts", 0, $arg)
    }
    
    property get wc7RatingUtil () : tdic.pc.config.rating.wc7.WC7RatingUtil {
      return getVariableValue("wc7RatingUtil", 0) as tdic.pc.config.rating.wc7.WC7RatingUtil
    }
    
    property set wc7RatingUtil ($arg :  tdic.pc.config.rating.wc7.WC7RatingUtil) {
      setVariableValue("wc7RatingUtil", 0, $arg)
    }
    
    function basisLabel() : String {
      return (isRevisedAudit() ?
        DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") :
        DisplayKey.get("Web.AuditWizard.EstBasis"))
    }
    
    function amountLabel() : String {
      return (isRevisedAudit() ?
        DisplayKey.get("Web.AuditWizard.PriorAuditedAmount") :
        DisplayKey.get("Web.AuditWizard.EstAmount"))
    }
    function premiumLabel() : String {
      return (isRevisedAudit() ?
          DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.PriorAudited") :
          DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Estimated"))
    }
    private function isRevisedAudit() : boolean {
      return jurisdiction.Branch.Audit.AuditInformation.IsRevision
    }
    
    private function getAllCosts( curCosts : java.util.Set<WC7Cost>, priorCosts : java.util.Set<WC7Cost> ) : java.util.Set<WC7Cost>
    {
      var curCostKeys = curCosts.map( \ curCost -> curCost.CostKey )
      var missingPriorCosts = priorCosts.where( \ priorCost -> not curCostKeys.contains( priorCost.CostKey ) ).toSet()
      return curCosts.union( missingPriorCosts )
    }
    
    
  }
  
  
}