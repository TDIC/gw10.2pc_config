package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_WebAddressForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_WebAddressForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_WebAddressForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_WebAddressForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (WebUrl :  String) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.TDIC_WebAddressForward {
      return super.CurrentLocation as pcf.TDIC_WebAddressForward
    }
    
    property get WebUrl () : String {
      return getVariableValue("WebUrl", 0) as String
    }
    
    property set WebUrl ($arg :  String) {
      setVariableValue("WebUrl", 0, $arg)
    }
    
    
  }
  
  
}