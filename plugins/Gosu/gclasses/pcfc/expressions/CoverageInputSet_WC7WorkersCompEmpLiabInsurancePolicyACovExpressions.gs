package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACovExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 35, column 90
    function available_84 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachAccTDICType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabPolicyLimitType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachEmpTDICType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7StopGapTerm.Value = (__VALUE_TO_SET as typekey.StopGap)
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7EmpLiabCov.WC7EmpLiabLimitTerm.PackageValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermPack<productmodel.PackageWC7EmpLiabLimitType>)
    }
    
    // 'editable' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function editable_18 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function editable_31 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function editable_44 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function editable_5 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function editable_56 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7StopGapTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7StopGapTerm)
    }
    
    // 'editable' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function editable_70 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 20, column 41
    function initialValue_0 () : entity.WC7WorkersCompLine {
      return coverable as WC7WorkersCompLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 25, column 69
    function initialValue_1 () : productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov {
      return wc7line.WC7WorkersCompEmpLiabInsurancePolicyACov
    }
    
    // 'label' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function label_19 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function label_32 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function label_45 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function label_58 () : java.lang.Object {
      return wc7EmpLiabCov.WC7StopGapTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function label_6 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitTerm.DisplayName
    }
    
    // 'label' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function label_73 () : java.lang.Object {
      return wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 35, column 90
    function label_85 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 54, column 306
    function onChange_17 () : void {
      var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.CoveragePattern>(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -> i.BaseMessage).join("\n"))};
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 66, column 306
    function onChange_30 () : void {
      var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.CoveragePattern>(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -> i.BaseMessage).join("\n"))};
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 78, column 306
    function onChange_43 () : void {
      var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.CoveragePattern>(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -> i.BaseMessage).join("\n"))};
    }
    
    // 'required' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function required_20 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function required_33 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function required_46 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function required_59 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7StopGapTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function required_7 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7EmpLiabLimitTerm.Pattern.Required
    }
    
    // 'required' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function required_74 () : java.lang.Boolean {
      return wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 35, column 90
    function setter_86 (VALUE :  java.lang.Boolean) : void {
      coverable.setCoverageConditionOrExclusionExists(coveragePattern, VALUE)
    }
    
    // 'validationExpression' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function validationExpression_71 () : java.lang.Object {
      return wc7line.validateIncludedMonopolisticStates(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Value)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function valueRange_11 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function valueRange_24 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm, openForEdit)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function valueRange_37 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function valueRange_50 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm, openForEdit)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function valueRange_63 () : java.lang.Object {
      return wc7EmpLiabCov.WC7StopGapTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function valueRoot_10 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function valueRoot_23 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 29, column 37
    function valueRoot_3 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function valueRoot_36 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function valueRoot_49 () : java.lang.Object {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function valueRoot_62 () : java.lang.Object {
      return wc7EmpLiabCov.WC7StopGapTerm
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function valueRoot_77 () : java.lang.Object {
      return wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 29, column 37
    function value_2 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function value_21 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachAccTDICType> {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function value_34 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabPolicyLimitType> {
      return wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function value_47 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachEmpTDICType> {
      return wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function value_60 () : typekey.StopGap {
      return wc7EmpLiabCov.WC7StopGapTerm.Value
    }
    
    // 'value' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function value_75 () : java.lang.String {
      return wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Value
    }
    
    // 'value' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function value_8 () : gw.api.productmodel.CovTermPack<productmodel.PackageWC7EmpLiabLimitType> {
      return wc7EmpLiabCov.WC7EmpLiabLimitTerm.PackageValue
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function verifyValueRangeIsAllowedType_12 ($$arg :  gw.api.productmodel.CovTermPack<productmodel.PackageWC7EmpLiabLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function verifyValueRangeIsAllowedType_25 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachAccTDICType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function verifyValueRangeIsAllowedType_25 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function verifyValueRangeIsAllowedType_38 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabPolicyLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7EmpLiabLimitEachEmpTDICType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function verifyValueRangeIsAllowedType_64 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function verifyValueRangeIsAllowedType_64 ($$arg :  typekey.StopGap[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 43, column 101
    function verifyValueRange_13 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachAcc_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 51, column 109
    function verifyValueRange_26 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm, openForEdit)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_25(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabPolicyLimitTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 63, column 104
    function verifyValueRange_39 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7EmpLiabLimitEachEmp_TDIC_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 75, column 109
    function verifyValueRange_52 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm, openForEdit)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function verifyValueRange_65 () : void {
      var __valueRangeArg = wc7EmpLiabCov.WC7StopGapTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_64(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=WC7StopGapTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 88, column 84
    function visible_57 () : java.lang.Boolean {
      return wc7EmpLiabCov.hasCovTermByCodeIdentifier("WC7StopGap")
    }
    
    // 'visible' attribute on TextInput (id=WC7IncludedMonopolisticStatesTermInput_Input) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 99, column 149
    function visible_72 () : java.lang.Boolean {
      return wc7EmpLiabCov.HasWC7IncludedMonopolisticStatesTerm and wc7EmpLiabCov.WC7StopGapTerm.Value == typekey.StopGap.TC_LISTEDSTATESONLY
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7WorkersCompEmpLiabInsurancePolicyACov.pcf: line 35, column 90
    function visible_83 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wc7EmpLiabCov () : productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov {
      return getVariableValue("wc7EmpLiabCov", 0) as productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov
    }
    
    property set wc7EmpLiabCov ($arg :  productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov) {
      setVariableValue("wc7EmpLiabCov", 0, $arg)
    }
    
    property get wc7line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7line", 0, $arg)
    }
    
    
  }
  
  
}