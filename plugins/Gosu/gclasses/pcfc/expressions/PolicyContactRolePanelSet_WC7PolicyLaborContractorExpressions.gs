package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactRolePanelSet_WC7PolicyLaborContractorExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyContactRolePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=LaborContractorEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 50, column 77
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborContractorDetail.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=LaborContractorExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 56, column 78
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborContractorDetail.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborContractorDetail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function valueRange_11 () : java.lang.Object {
      return wc7PolicyLaborContractorDetail.WC7LaborContact.WC7WorkersCompLine.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborContractorInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 35, column 46
    function valueRoot_6 () : java.lang.Object {
      return wc7PolicyLaborContractorDetail
    }
    
    // 'value' attribute on DateCell (id=LaborContractorEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 50, column 77
    function value_15 () : java.util.Date {
      return wc7PolicyLaborContractorDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=LaborContractorExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 56, column 78
    function value_19 () : java.util.Date {
      return wc7PolicyLaborContractorDetail.ContractExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborContractorInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 35, column 46
    function value_5 () : typekey.Inclusion {
      return wc7PolicyLaborContractorDetail.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function value_8 () : typekey.Jurisdiction {
      return wc7PolicyLaborContractorDetail.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function verifyValueRange_13 () : void {
      var __valueRangeArg = wc7PolicyLaborContractorDetail.WC7LaborContact.WC7WorkersCompLine.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    property get wc7PolicyLaborContractorDetail () : entity.WC7LaborContactDetail {
      return getIteratedValue(1) as entity.WC7LaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactRolePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 14, column 47
    function initialValue_0 () : entity.WC7PolicyLaborContractor {
      return policyContactRole as WC7PolicyLaborContractor
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborContractorInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 35, column 46
    function sortValue_1 (wc7PolicyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborContractorDetail.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=LaborContractorState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 44, column 49
    function sortValue_2 (wc7PolicyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborContractorDetail.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=LaborContractorEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 50, column 77
    function sortValue_3 (wc7PolicyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborContractorDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=LaborContractorExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 56, column 78
    function sortValue_4 (wc7PolicyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborContractorDetail.ContractExpirationDate
    }
    
    // 'value' attribute on RowIterator at PolicyContactRolePanelSet.WC7PolicyLaborContractor.pcf: line 28, column 54
    function value_23 () : entity.WC7LaborContactDetail[] {
      return wc7PolicyLaborContractor.LaborContactDetails
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    property get wc7PolicyLaborContractor () : entity.WC7PolicyLaborContractor {
      return getVariableValue("wc7PolicyLaborContractor", 0) as entity.WC7PolicyLaborContractor
    }
    
    property set wc7PolicyLaborContractor ($arg :  entity.WC7PolicyLaborContractor) {
      setVariableValue("wc7PolicyLaborContractor", 0, $arg)
    }
    
    
  }
  
  
}