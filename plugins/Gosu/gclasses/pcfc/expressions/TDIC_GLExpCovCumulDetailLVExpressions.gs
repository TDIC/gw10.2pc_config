package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLExpCovCumulDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLExpCovCumulDetailLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLExpCovCumulDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_GLExpCovCumulDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=LocIndex_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 42, column 25
    function valueRoot_16 () : java.lang.Object {
      return cost.GLExposure.Location
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 56, column 41
    function valueRoot_19 () : java.lang.Object {
      return cost.GLCostType
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 87, column 25
    function valueRoot_24 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=LocIndex_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 42, column 25
    function value_15 () : java.lang.Integer {
      return cost.GLExposure.Location.LocationNum
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 56, column 41
    function value_18 () : java.lang.String {
      return cost.GLCostType.Name
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 79, column 25
    function value_21 () : java.lang.String {
      return cost.Basis == null or cost.Basis == 0? "" : cost.Basis.DisplayValue
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 87, column 25
    function value_23 () : java.math.BigDecimal {
      return cost.ActualAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 95, column 25
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualTermAmountBilling
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 102, column 25
    function value_30 () : java.util.Date {
      return cost.EffDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 109, column 25
    function value_34 () : java.util.Date {
      return cost.ExpDate
    }
    
    // 'value' attribute on TextCell (id=Proration_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 117, column 25
    function value_38 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(cost.ProRataByDaysValue, "#0.0000")
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 126, column 25
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 95, column 25
    function visible_28 () : java.lang.Boolean {
      return true //prorated
    }
    
    property get cost () : entity.GLCovExposureCost {
      return getIteratedValue(1) as entity.GLCovExposureCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLExpCovCumulDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLExpCovCumulDetailLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLExpCovCumulDetailLV.pcf: line 15, column 23
    function initialValue_0 () : boolean {
      return locCosts.AnyProrated
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLExpCovCumulDetailLV.pcf: line 24, column 24
    function sortBy_1 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost.Location.LocationNum
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLExpCovCumulDetailLV.pcf: line 27, column 24
    function sortBy_2 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost.GLExposure.ClassCode
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLExpCovCumulDetailLV.pcf: line 30, column 24
    function sortBy_3 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost.Subline
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLExpCovCumulDetailLV.pcf: line 33, column 24
    function sortBy_4 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost.SplitType
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_GLExpCovCumulDetailLV.pcf: line 126, column 25
    function sumValueRoot_14 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_GLExpCovCumulDetailLV.pcf: line 126, column 25
    function sumValue_13 (cost :  entity.GLCovExposureCost) : java.lang.Object {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on RowIterator at TDIC_GLExpCovCumulDetailLV.pcf: line 21, column 46
    function value_44 () : entity.GLCovExposureCost[] {
      return locCosts?.toTypedArray()
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_GLExpCovCumulDetailLV.pcf: line 95, column 25
    function visible_5 () : java.lang.Boolean {
      return true //prorated
    }
    
    property get locCosts () : java.util.Set<GLCovExposureCost> {
      return getRequireValue("locCosts", 0) as java.util.Set<GLCovExposureCost>
    }
    
    property set locCosts ($arg :  java.util.Set<GLCovExposureCost>) {
      setRequireValue("locCosts", 0, $arg)
    }
    
    property get prorated () : boolean {
      return getVariableValue("prorated", 0) as java.lang.Boolean
    }
    
    property set prorated ($arg :  boolean) {
      setVariableValue("prorated", 0, $arg)
    }
    
    
  }
  
  
}