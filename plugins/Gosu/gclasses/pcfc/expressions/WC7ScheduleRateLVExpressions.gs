package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleRateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ScheduleRateLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleRateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7ScheduleRateLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=CreditDebit_Cell) at WC7ScheduleRateLV.pcf: line 66, column 44
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateFactor.AssessmentWithinLimits = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateFactor.Justification = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function editable_36 () : java.lang.Boolean {
      return rateFactor.Pattern.ModifierPattern.DisplayJustification
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=CreditDebit_Cell) at WC7ScheduleRateLV.pcf: line 66, column 44
    function requestValidationExpression_31 (VALUE :  java.math.BigDecimal) : java.lang.Object {
      return checkValueRanges(rateFactor, VALUE)
    }
    
    // 'tooltip' attribute on Link (id=ContextLink) at WC7ScheduleRateLV.pcf: line 35, column 76
    function tooltip_20 () : java.lang.String {
      return stateConfig.getRateFactorDescriptionFor(rateFactor)
    }
    
    // 'validationExpression' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function validationExpression_37 () : java.lang.Object {
      return verifyRateFactor(rateFactor)
    }
    
    // 'value' attribute on TextCell (id=Minimum_Cell) at WC7ScheduleRateLV.pcf: line 51, column 50
    function valueRoot_24 () : java.lang.Object {
      return rateFactor
    }
    
    // 'value' attribute on TextCell (id=Category_Cell) at WC7ScheduleRateLV.pcf: line 44, column 24
    function value_21 () : java.lang.String {
      return stateConfig.getRateFactorNameFor(rateFactor)
    }
    
    // 'value' attribute on TextCell (id=Minimum_Cell) at WC7ScheduleRateLV.pcf: line 51, column 50
    function value_23 () : java.math.BigDecimal {
      return rateFactor.Minimum
    }
    
    // 'value' attribute on TextCell (id=Maximum_Cell) at WC7ScheduleRateLV.pcf: line 58, column 50
    function value_27 () : java.math.BigDecimal {
      return rateFactor.Maximum
    }
    
    // 'value' attribute on TextCell (id=CreditDebit_Cell) at WC7ScheduleRateLV.pcf: line 66, column 44
    function value_32 () : java.math.BigDecimal {
      return rateFactor.AssessmentWithinLimits
    }
    
    // 'value' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function value_38 () : java.lang.String {
      return rateFactor.Justification
    }
    
    // 'visible' attribute on TextCell (id=Minimum_Cell) at WC7ScheduleRateLV.pcf: line 51, column 50
    function visible_25 () : java.lang.Boolean {
      return credit.Pattern.DisplayRange
    }
    
    // 'visible' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function visible_43 () : java.lang.Boolean {
      return credit.Pattern.DisplayJustification
    }
    
    property get rateFactor () : entity.RateFactor {
      return getIteratedValue(1) as entity.RateFactor
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleRateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ScheduleRateLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7ScheduleRateLV.pcf: line 16, column 38
    function initialValue_0 () : entity.WC7Jurisdiction {
      return coverable as WC7Jurisdiction
    }
    
    // 'initialValue' attribute on Variable at WC7ScheduleRateLV.pcf: line 20, column 54
    function initialValue_1 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(wc7Jurisdiction.Jurisdiction)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7ScheduleRateLV.pcf: line 28, column 24
    function sortBy_2 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return rateFactor.Pattern.Priority
    }
    
    // 'value' attribute on TextCell (id=Category_Cell) at WC7ScheduleRateLV.pcf: line 44, column 24
    function sortValue_3 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return stateConfig.getRateFactorNameFor(rateFactor)
    }
    
    // 'value' attribute on TextCell (id=Minimum_Cell) at WC7ScheduleRateLV.pcf: line 51, column 50
    function sortValue_4 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return rateFactor.Minimum
    }
    
    // 'value' attribute on TextCell (id=Maximum_Cell) at WC7ScheduleRateLV.pcf: line 58, column 50
    function sortValue_6 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return rateFactor.Maximum
    }
    
    // 'value' attribute on TextCell (id=CreditDebit_Cell) at WC7ScheduleRateLV.pcf: line 66, column 44
    function sortValue_8 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return rateFactor.AssessmentWithinLimits
    }
    
    // 'value' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function sortValue_9 (rateFactor :  entity.RateFactor) : java.lang.Object {
      return rateFactor.Justification
    }
    
    // 'value' attribute on RowIterator at WC7ScheduleRateLV.pcf: line 51, column 50
    function valueRoot_12 () : java.lang.Object {
      return credit
    }
    
    // 'footerLabel' attribute on RowIterator at WC7ScheduleRateLV.pcf: line 51, column 50
    function value_11 () : java.lang.Object {
      return credit.Minimum as String
    }
    
    // 'footerLabel' attribute on RowIterator at WC7ScheduleRateLV.pcf: line 58, column 50
    function value_14 () : java.lang.Object {
      return credit.Maximum as String
    }
    
    // 'footerLabel' attribute on RowIterator at WC7ScheduleRateLV.pcf: line 66, column 44
    function value_17 () : java.lang.Object {
      return credit.RateModifier as String
    }
    
    // 'value' attribute on RowIterator at WC7ScheduleRateLV.pcf: line 25, column 39
    function value_45 () : entity.RateFactor[] {
      return credit.RateFactors
    }
    
    // 'visible' attribute on TextCell (id=Justification_Cell) at WC7ScheduleRateLV.pcf: line 76, column 58
    function visible_10 () : java.lang.Boolean {
      return credit.Pattern.DisplayJustification
    }
    
    // 'visible' attribute on TextCell (id=Minimum_Cell) at WC7ScheduleRateLV.pcf: line 51, column 50
    function visible_5 () : java.lang.Boolean {
      return credit.Pattern.DisplayRange
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get credit () : Modifier {
      return getRequireValue("credit", 0) as Modifier
    }
    
    property set credit ($arg :  Modifier) {
      setRequireValue("credit", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 0, $arg)
    }
    
    property get wc7Jurisdiction () : entity.WC7Jurisdiction {
      return getVariableValue("wc7Jurisdiction", 0) as entity.WC7Jurisdiction
    }
    
    property set wc7Jurisdiction ($arg :  entity.WC7Jurisdiction) {
      setVariableValue("wc7Jurisdiction", 0, $arg)
    }
    
    
        function verifyRateFactor(rate : RateFactor) : String {
          if (rate.Assessment != 0 and rate.Justification == null) {
            return DisplayKey.get("Web.RatingFactors.JustificationNeeded")
          }
          return null
        }
    
        function checkValueRanges(rate : RateFactor, value : java.math.BigDecimal) : String {
          if (!rate.isValueWithinRange(value as String)) {
            return DisplayKey.get("Web.Policy.RateFactor.OutOfRange", value, rate.Minimum, rate.Maximum)
          }
          return null
        }
    
    
  }
  
  
}