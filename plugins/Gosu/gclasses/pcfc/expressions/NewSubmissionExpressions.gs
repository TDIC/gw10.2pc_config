package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/new/NewSubmission.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewSubmissionExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/new/NewSubmission.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewSubmissionExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=addSubmission) at NewSubmission.pcf: line 194, column 52
    function action_60 () : void {
      manualProduct.createSubmission(acct, selectionOfProducer)  
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewSubmission.pcf: line 199, column 53
    function valueRoot_62 () : java.lang.Object {
      return manualProduct
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewSubmission.pcf: line 199, column 53
    function value_61 () : java.lang.String {
      return manualProduct.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewSubmission.pcf: line 203, column 60
    function value_64 () : java.lang.String {
      return manualProduct.Description
    }
    
    property get manualProduct () : APDProduct {
      return getIteratedValue(1) as APDProduct
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/submission/new/NewSubmission.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewSubmissionExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (acct :  Account) : int {
      return 1
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at NewSubmission.pcf: line 109, column 53
    function action_18 () : void {
      AccountFileDoRetrievalForward.go(acct.AccountNumber)
    }
    
    // 'pickLocation' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function action_26 () : void {
      OrganizationSearchPopup.push(null, true)
    }
    
    // 'action' attribute on MenuItem (id=NewProduct) at NewSubmission.pcf: line 168, column 63
    function action_52 () : void {
      APDNewProductPopup.push(acct, selectionOfProducer)
    }
    
    // 'action' attribute on MenuItem (id=FromTemplate) at NewSubmission.pcf: line 172, column 115
    function action_54 () : void {
      APDLoadProductTemplatePopup.push(acct, selectionOfProducer)
    }
    
    // 'action' attribute on MenuItem (id=FromXmind) at NewSubmission.pcf: line 176, column 112
    function action_56 () : void {
      APDLoadXmindPopup.push(acct, selectionOfProducer)
    }
    
    // 'pickLocation' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function action_9 () : void {
      NewAccountPopup.push(true)
    }
    
    // 'action' attribute on ToolbarButton (id=MakeSubmissions) at NewSubmission.pcf: line 275, column 47
    function action_90 () : void {
      uiHelper.createMultipleSubmissions(productOffers, acct, selectionOfProducer, typeOfQuote)
    }
    
    // 'pickLocation' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function action_dest_10 () : pcf.api.Destination {
      return pcf.NewAccountPopup.createDestination(true)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at NewSubmission.pcf: line 109, column 53
    function action_dest_19 () : pcf.api.Destination {
      return pcf.AccountFileDoRetrievalForward.createDestination(acct.AccountNumber)
    }
    
    // 'pickLocation' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function action_dest_27 () : pcf.api.Destination {
      return pcf.OrganizationSearchPopup.createDestination(null, true)
    }
    
    // 'action' attribute on MenuItem (id=NewProduct) at NewSubmission.pcf: line 168, column 63
    function action_dest_53 () : pcf.api.Destination {
      return pcf.APDNewProductPopup.createDestination(acct, selectionOfProducer)
    }
    
    // 'action' attribute on MenuItem (id=FromTemplate) at NewSubmission.pcf: line 172, column 115
    function action_dest_55 () : pcf.api.Destination {
      return pcf.APDLoadProductTemplatePopup.createDestination(acct, selectionOfProducer)
    }
    
    // 'action' attribute on MenuItem (id=FromXmind) at NewSubmission.pcf: line 176, column 112
    function action_dest_57 () : pcf.api.Destination {
      return pcf.APDLoadXmindPopup.createDestination(acct, selectionOfProducer)
    }
    
    // 'afterEnter' attribute on Page (id=NewSubmission) at NewSubmission.pcf: line 17, column 36
    function afterEnter_96 () : void {
      uiHelper.setBaseState_TDIC(selectionOfProducer)
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=NewSubmission) at NewSubmission.pcf: line 17, column 36
    function afterReturnFromPopup_97 (popupCommitted :  boolean) : void {
      uiHelper.refreshProductOffers(acct, selectionOfProducer)
    }
    
    // 'available' attribute on DetailViewPanel (id=ProductOffersDV) at NewSubmission.pcf: line 265, column 32
    function available_94 () : java.lang.Boolean {
      return typeOfQuote != null
    }
    
    // 'beforeCommit' attribute on Page (id=NewSubmission) at NewSubmission.pcf: line 17, column 36
    function beforeCommit_98 (pickedValue :  java.lang.Object) : void {
      throw "NewSubmission should never commit"
    }
    
    // 'conversionExpression' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function conversionExpression_12 (PickedValue :  AccountSummary) : entity.Account {
      return Account.checkedFindByNumber(PickedValue.AccountNumber, \ a -> perm.Account.newSubmission(a))
    }
    
    // 'def' attribute on ListViewInput at NewSubmission.pcf: line 269, column 85
    function def_onEnter_92 (def :  pcf.ProductSelectionLV) : void {
      def.onEnter(productOffers, acct, selectionOfProducer, createSingle, typeOfQuote, uiHelper)
    }
    
    // 'def' attribute on ListViewInput at NewSubmission.pcf: line 269, column 85
    function def_refreshVariables_93 (def :  pcf.ProductSelectionLV) : void {
      def.refreshVariables(productOffers, acct, selectionOfProducer, createSingle, typeOfQuote, uiHelper)
    }
    
    // 'value' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      acct = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectionOfProducer.Producer = (__VALUE_TO_SET as entity.Organization)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectionOfProducer.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateSingle_Input) at NewSubmission.pcf: line 230, column 36
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      createSingle = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=QuoteType_Input) at NewSubmission.pcf: line 239, column 45
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      typeOfQuote = (__VALUE_TO_SET as typekey.QuoteType)
    }
    
    // 'value' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function defaultSetter_78 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectionOfProducer.State = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on DateInput (id=DefaultPPEffDate_Input) at NewSubmission.pcf: line 258, column 60
    function defaultSetter_86 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectionOfProducer.DefaultPPEffDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function editable_28 () : java.lang.Boolean {
      return gw.api.web.producer.ProducerUtil.canEditOrganization()
    }
    
    // 'filter' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function filter_80 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(acct.getPrimaryLocation().Country)
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 31, column 66
    function initialValue_0 () : tdic.web.job.submission.TDIC_NewSubmissionUIHelper {
      return new tdic.web.job.submission.TDIC_NewSubmissionUIHelper(CurrentLocation)
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 35, column 23
    function initialValue_1 () : Account {
      return null
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 39, column 23
    function initialValue_2 () : boolean {
      return acct == null
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 49, column 33
    function initialValue_3 () : ProducerSelection {
      return uiHelper.initializeProducerSelection_TDIC(acct)
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 54, column 34
    function initialValue_4 () : ProductSelection[] {
      return uiHelper.performNameClearance(acct, selectionOfProducer)
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 68, column 70
    function initialValue_5 () : gw.api.web.userpreference.UserPreferencesOfCurrentUser {
      return new gw.api.web.userpreference.UserPreferencesOfCurrentUser()
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 72, column 25
    function initialValue_6 () : QuoteType {
      return QuoteType.TC_QUICK
    }
    
    // 'initialValue' attribute on Variable at NewSubmission.pcf: line 76, column 30
    function initialValue_7 () : java.util.Date {
      return null
    }
    
    // 'inputConversion' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function inputConversion_13 (VALUE :  java.lang.String) : java.lang.Object {
      return Account.checkedFindByNumber(VALUE, \ a -> perm.Account.newSubmission(a))
    }
    
    // 'onChange' attribute on PostOnChange at NewSubmission.pcf: line 126, column 177
    function onChange_25 () : void {
      uiHelper.changedProducer(acct, selectionOfProducer);gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(selectionOfProducer,true)
    }
    
    // 'onChange' attribute on PostOnChange at NewSubmission.pcf: line 141, column 86
    function onChange_37 () : void {
      uiHelper.refreshProductOffers(acct, selectionOfProducer)
    }
    
    // 'onChange' attribute on PostOnChange at NewSubmission.pcf: line 101, column 86
    function onChange_8 () : void {
      uiHelper.refreshProductOffers(acct, selectionOfProducer)
    }
    
    // 'onPick' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function onPick_11 (PickedValue :  AccountSummary) : void {
      uiHelper.refreshProductOffers(acct, selectionOfProducer)
    }
    
    // 'onPick' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function onPick_30 (PickedValue :  Organization) : void {
      uiHelper.changedProducer(acct, selectionOfProducer);gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(selectionOfProducer,true)
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function optionLabel_43 (VALUE :  entity.ProducerCode) : java.lang.String {
      return DisplayKey.get("Web.ProducerSelection.ProducerCode.OptionLabel", VALUE.Code, VALUE.Description  != null ? VALUE.Description : "" )
    }
    
    // 'parent' attribute on Page (id=NewSubmission) at NewSubmission.pcf: line 17, column 36
    static function parent_99 (acct :  Account) : pcf.api.Destination {
      return pcf.AccountForward.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewSubmission.pcf: line 199, column 53
    function sortValue_58 (manualProduct :  APDProduct) : java.lang.Object {
      return manualProduct.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewSubmission.pcf: line 203, column 60
    function sortValue_59 (manualProduct :  APDProduct) : java.lang.Object {
      return manualProduct.Description
    }
    
    // 'validationExpression' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function validationExpression_29 () : java.lang.Object {
      return selectionOfProducer.validateProducer()
    }
    
    // 'validationExpression' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function validationExpression_38 () : java.lang.Object {
      return selectionOfProducer.validateProducerCodeForSubmission()
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function valueRange_44 () : java.lang.Object {
      return selectionOfProducer.getRangeOfActiveProducerCodesForCurrentUser()
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function valueRange_81 () : java.lang.Object {
      return gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(selectionOfProducer,true)
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewSubmission.pcf: line 109, column 53
    function valueRoot_21 () : java.lang.Object {
      return acct.AccountHolder
    }
    
    // 'value' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function valueRoot_33 () : java.lang.Object {
      return selectionOfProducer
    }
    
    // 'value' attribute on PickerInput (id=Account_Input) at NewSubmission.pcf: line 99, column 30
    function value_14 () : entity.Account {
      return acct
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewSubmission.pcf: line 109, column 53
    function value_20 () : entity.AccountContact {
      return acct.AccountHolder.AccountContact
    }
    
    // 'value' attribute on OrgInput (id=Producer_Input) at NewSubmission.pcf: line 124, column 52
    function value_31 () : entity.Organization {
      return selectionOfProducer.Producer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function value_40 () : entity.ProducerCode {
      return selectionOfProducer.ProducerCode
    }
    
    // 'value' attribute on RowIterator at NewSubmission.pcf: line 184, column 46
    function value_67 () : APDProduct[] {
      return APDProduct.ManualProducts.where(\product -> product.canVisualizeForAccount(acct))
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateSingle_Input) at NewSubmission.pcf: line 230, column 36
    function value_71 () : java.lang.Boolean {
      return createSingle
    }
    
    // 'value' attribute on TypeKeyInput (id=QuoteType_Input) at NewSubmission.pcf: line 239, column 45
    function value_74 () : typekey.QuoteType {
      return typeOfQuote
    }
    
    // 'value' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function value_77 () : typekey.Jurisdiction {
      return selectionOfProducer.State
    }
    
    // 'value' attribute on DateInput (id=DefaultPPEffDate_Input) at NewSubmission.pcf: line 258, column 60
    function value_85 () : java.util.Date {
      return selectionOfProducer.DefaultPPEffDate
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function verifyValueRangeIsAllowedType_45 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function verifyValueRangeIsAllowedType_45 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function verifyValueRangeIsAllowedType_45 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function verifyValueRangeIsAllowedType_82 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function verifyValueRangeIsAllowedType_82 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function verifyValueRange_46 () : void {
      var __valueRangeArg = selectionOfProducer.getRangeOfActiveProducerCodesForCurrentUser()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_45(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultBaseState_Input) at NewSubmission.pcf: line 250, column 48
    function verifyValueRange_83 () : void {
      var __valueRangeArg = gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(selectionOfProducer,true)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_82(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=AccountName_Input) at NewSubmission.pcf: line 109, column 53
    function visible_17 () : java.lang.Boolean {
      return acct.AccountNumber != null
    }
    
    // 'visible' attribute on InputSet at NewSubmission.pcf: line 84, column 37
    function visible_24 () : java.lang.Boolean {
      return selectAccount
    }
    
    // 'visible' attribute on RangeInput (id=ProducerCode_Input) at NewSubmission.pcf: line 139, column 62
    function visible_39 () : java.lang.Boolean {
      return selectionOfProducer.Producer != null
    }
    
    // 'visible' attribute on Card (id=Installed) at NewSubmission.pcf: line 151, column 49
    function visible_50 () : java.lang.Boolean {
      return productOffers.Count > 0
    }
    
    // 'visible' attribute on MenuItem (id=NewProduct) at NewSubmission.pcf: line 168, column 63
    function visible_51 () : java.lang.Boolean {
      return !userPreferences.inUseMode()
    }
    
    // 'visible' attribute on DetailViewPanel at NewSubmission.pcf: line 157, column 83
    function visible_68 () : java.lang.Boolean {
      return uiHelper.canPerformNameClearance(acct, selectionOfProducer)
    }
    
    // 'visible' attribute on Card (id=Visualized) at NewSubmission.pcf: line 155, column 108
    function visible_69 () : java.lang.Boolean {
      return !userPreferences.inHiddenMode() and gw.api.system.PCDependenciesGateway.ServerMode.Dev
    }
    
    // 'visible' attribute on Label (id=NoProducts) at NewSubmission.pcf: line 223, column 51
    function visible_70 () : java.lang.Boolean {
      return productOffers.Count == 0
    }
    
    // 'visible' attribute on ToolbarButton (id=MakeSubmissions) at NewSubmission.pcf: line 275, column 47
    function visible_89 () : java.lang.Boolean {
      return not createSingle
    }
    
    override property get CurrentLocation () : pcf.NewSubmission {
      return super.CurrentLocation as pcf.NewSubmission
    }
    
    property get acct () : Account {
      return getVariableValue("acct", 0) as Account
    }
    
    property set acct ($arg :  Account) {
      setVariableValue("acct", 0, $arg)
    }
    
    property get createSingle () : boolean {
      return getVariableValue("createSingle", 0) as java.lang.Boolean
    }
    
    property set createSingle ($arg :  boolean) {
      setVariableValue("createSingle", 0, $arg)
    }
    
    property get nullDate () : java.util.Date {
      return getVariableValue("nullDate", 0) as java.util.Date
    }
    
    property set nullDate ($arg :  java.util.Date) {
      setVariableValue("nullDate", 0, $arg)
    }
    
    property get productOffers () : ProductSelection[] {
      return getVariableValue("productOffers", 0) as ProductSelection[]
    }
    
    property set productOffers ($arg :  ProductSelection[]) {
      setVariableValue("productOffers", 0, $arg)
    }
    
    property get selectAccount () : boolean {
      return getVariableValue("selectAccount", 0) as java.lang.Boolean
    }
    
    property set selectAccount ($arg :  boolean) {
      setVariableValue("selectAccount", 0, $arg)
    }
    
    property get selectionOfProducer () : ProducerSelection {
      return getVariableValue("selectionOfProducer", 0) as ProducerSelection
    }
    
    property set selectionOfProducer ($arg :  ProducerSelection) {
      setVariableValue("selectionOfProducer", 0, $arg)
    }
    
    property get typeOfQuote () : QuoteType {
      return getVariableValue("typeOfQuote", 0) as QuoteType
    }
    
    property set typeOfQuote ($arg :  QuoteType) {
      setVariableValue("typeOfQuote", 0, $arg)
    }
    
    property get uiHelper () : tdic.web.job.submission.TDIC_NewSubmissionUIHelper {
      return getVariableValue("uiHelper", 0) as tdic.web.job.submission.TDIC_NewSubmissionUIHelper
    }
    
    property set uiHelper ($arg :  tdic.web.job.submission.TDIC_NewSubmissionUIHelper) {
      setVariableValue("uiHelper", 0, $arg)
    }
    
    property get userPreferences () : gw.api.web.userpreference.UserPreferencesOfCurrentUser {
      return getVariableValue("userPreferences", 0) as gw.api.web.userpreference.UserPreferencesOfCurrentUser
    }
    
    property set userPreferences ($arg :  gw.api.web.userpreference.UserPreferencesOfCurrentUser) {
      setVariableValue("userPreferences", 0, $arg)
    }
    
    
  }
  
  
}