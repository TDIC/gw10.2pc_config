package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 35, column 53
    function available_135 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'def' attribute on InputSetRef (id=WC7AddlWaivers) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 38, column 30
    function def_onEnter_5 (def :  pcf.WC7AddlWaiversInputSet) : void {
      def.onEnter(wc7Line, conditionPattern)
    }
    
    // 'def' attribute on InputSetRef (id=WC7AddlWaivers) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 38, column 30
    function def_refreshVariables_6 (def :  pcf.WC7AddlWaiversInputSet) : void {
      def.refreshVariables(wc7Line, conditionPattern)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 20, column 41
    function initialValue_0 () : entity.WC7WorkersCompLine {
      return coverable as entity.WC7WorkersCompLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 25, column 66
    function initialValue_1 () : WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond {
      return wc7Line.LineAsOfPeriodStart.getCondition(conditionPattern as gw.api.productmodel.ConditionPattern) as WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond
    }
    
    // 'label' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 35, column 53
    function label_136 () : java.lang.Object {
      return conditionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 35, column 53
    function setter_137 (VALUE :  java.lang.Boolean) : void {
      wc7Line.setWaiverCondExists(VALUE)
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 29, column 37
    function valueRoot_3 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 29, column 37
    function value_2 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 35, column 53
    function visible_134 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 273, column 101
    function visible_140 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on Label (id=specificwaiverHeader) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 43, column 128
    function visible_7 () : java.lang.Boolean {
      return wc7Line.WC7WaiverOfSubros.where(\w -> w.Type == typekey.WC7WaiverOfSubrogation.TC_SPECIFIC).Count > 0
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    property get wc7WaiverCond () : WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond {
      return getVariableValue("wc7WaiverCond", 0) as WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond
    }
    
    property set wc7WaiverCond ($arg :  WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) {
      setVariableValue("wc7WaiverCond", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function defaultSetter_102 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.Location = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 239, column 50
    function defaultSetter_116 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro2.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function editable_100 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.canEditJurisdiction()
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function valueRange_104 () : java.lang.Object {
      return wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function valueRoot_103 () : java.lang.Object {
      return wc7SpecificWaiverOfSubro
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 205, column 49
    function valueRoot_92 () : java.lang.Object {
      return wc7SpecificWaiverOfSubro2
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function value_101 () : entity.PolicyLocation {
      return wc7SpecificWaiverOfSubro.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 227, column 52
    function value_109 () : entity.WC7ClassCode {
      return wc7SpecificWaiverOfSubro2.ClassCode
    }
    
    // 'value' attribute on TextCell (id=WCLaw_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 231, column 56
    function value_112 () : typekey.WC7GoverningLaw {
      return wc7SpecificWaiverOfSubro2.GoverningLaw
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 239, column 50
    function value_115 () : java.lang.Integer {
      return wc7SpecificWaiverOfSubro2.BasisAmount
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 244, column 91
    function value_120 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro2.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 250, column 35
    function value_124 () : java.util.Date {
      return wc7SpecificWaiverOfSubro2.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 256, column 35
    function value_127 () : java.util.Date {
      return wc7SpecificWaiverOfSubro2.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 205, column 49
    function value_91 () : java.lang.String {
      return wc7SpecificWaiverOfSubro2.Description
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 209, column 49
    function value_94 () : java.lang.String {
      return wc7SpecificWaiverOfSubro2.JobID
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 214, column 53
    function value_97 () : typekey.Jurisdiction {
      return wc7SpecificWaiverOfSubro2.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function verifyValueRangeIsAllowedType_105 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function verifyValueRangeIsAllowedType_105 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function verifyValueRangeIsAllowedType_105 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 223, column 32
    function verifyValueRange_106 () : void {
      var __valueRangeArg = wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_105(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 244, column 91
    function visible_119 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 244, column 91
    function visible_122 () : java.lang.Boolean {
      return coverageInputSetHelper.isDiseaseLoadedColumnVisible(wc7Line)
    }
    
    property get wc7SpecificWaiverOfSubro2 () : entity.WC7WaiverOfSubro {
      return getIteratedValue(3) as entity.WC7WaiverOfSubro
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7SpecificWaiverOfSubroLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaCell (id=Description_TDIC_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 101, column 63
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 109, column 47
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.JobID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.Location = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 159, column 30
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.GoverningLaw = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'value' attribute on TextCell (id=AnnualPayroll_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 168, column 48
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.SpecificDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 183, column 33
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.WaiverEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 191, column 33
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SpecificWaiverOfSubro.WaiverExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on TextCell (id=JobID_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 109, column 47
    function editable_27 () : java.lang.Boolean {
      return true and perm.System.editratingoverrides
    }
    
    // 'editable' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function editable_33 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.canEditJurisdiction()
    }
    
    // 'editable' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function editable_53 () : java.lang.Boolean {
      return (wc7SpecificWaiverOfSubro.Type == TC_SPECIFIC) and  (wc7SpecificWaiverOfSubro.Jurisdiction != null)
    }
    
    // 'editable' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 183, column 33
    function editable_79 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.canSetEffectiveWindow()
    }
    
    // 'filter' attribute on TypeKeyCell (id=WCLaw_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 159, column 30
    function filter_67 (VALUE :  typekey.WC7GoverningLaw, VALUES :  typekey.WC7GoverningLaw[]) : java.lang.Boolean {
      return VALUE.hasCategory(typekey.WC7LiabilityAct.TC_WORKERSCOMP)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 133, column 121
    function onChange_42 () : void {
      wc7SpecificWaiverOfSubro.JobID = wc7SpecificWaiverOfSubro.Location.LocationNum.toString()
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 148, column 121
    function onChange_52 () : void {
      coverageInputSetHelper.clearupSpecificWaiverDiseaseLoadedColumn(wc7SpecificWaiverOfSubro)
    }
    
    // 'onPick' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function onPick_54 (PickedValue :  java.lang.Object) : void {
      coverageInputSetHelper.clearupSpecificWaiverDiseaseLoadedColumn(wc7SpecificWaiverOfSubro)
    }
    
    // 'optionLabel' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function optionLabel_58 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE as java.lang.String
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function valueRange_37 () : java.lang.Object {
      return (wc7Line.stateFilterFor(conditionPattern).TypeKeys).sortBy(\s -> s.Code)
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function valueRange_47 () : java.lang.Object {
      return wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function valueRange_59 () : java.lang.Object {
      return wc7Line.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
    }
    
    // 'value' attribute on TextAreaCell (id=Description_TDIC_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 101, column 63
    function valueRoot_25 () : java.lang.Object {
      return wc7SpecificWaiverOfSubro
    }
    
    // 'value' attribute on RowIterator (id=WC7SpecificWaiverOfSubro2) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 199, column 53
    function value_130 () : entity.WC7WaiverOfSubro[] {
      return wc7SpecificWaiverOfSubro.AdditionalVersions.cast(entity.WC7WaiverOfSubro)
    }
    
    // 'value' attribute on TextAreaCell (id=Description_TDIC_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 101, column 63
    function value_23 () : java.lang.String {
      return wc7SpecificWaiverOfSubro.Description
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 109, column 47
    function value_28 () : java.lang.String {
      return wc7SpecificWaiverOfSubro.JobID
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function value_34 () : typekey.Jurisdiction {
      return wc7SpecificWaiverOfSubro.Jurisdiction
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function value_44 () : entity.PolicyLocation {
      return wc7SpecificWaiverOfSubro.Location
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function value_55 () : entity.WC7ClassCode {
      return wc7SpecificWaiverOfSubro.ClassCode
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 159, column 30
    function value_64 () : typekey.WC7GoverningLaw {
      return wc7SpecificWaiverOfSubro.GoverningLaw
    }
    
    // 'value' attribute on TextCell (id=AnnualPayroll_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 168, column 48
    function value_69 () : java.lang.Integer {
      return wc7SpecificWaiverOfSubro.BasisAmount
    }
    
    // 'value' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function value_74 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 183, column 33
    function value_80 () : java.util.Date {
      return wc7SpecificWaiverOfSubro.WaiverEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 191, column 33
    function value_86 () : java.util.Date {
      return wc7SpecificWaiverOfSubro.WaiverExpirationDate
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function verifyValueRangeIsAllowedType_38 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function verifyValueRangeIsAllowedType_48 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function verifyValueRangeIsAllowedType_48 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function verifyValueRangeIsAllowedType_48 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function verifyValueRangeIsAllowedType_60 ($$arg :  entity.WC7ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function verifyValueRangeIsAllowedType_60 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function verifyValueRangeIsAllowedType_60 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function verifyValueRange_39 () : void {
      var __valueRangeArg = (wc7Line.stateFilterFor(conditionPattern).TypeKeys).sortBy(\s -> s.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function verifyValueRange_49 () : void {
      var __valueRangeArg = wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_48(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function verifyValueRange_61 () : void {
      var __valueRangeArg = wc7Line.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(wc7SpecificWaiverOfSubro.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_60(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function visible_73 () : java.lang.Boolean {
      return wc7SpecificWaiverOfSubro.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function visible_77 () : java.lang.Boolean {
      return coverageInputSetHelper.isDiseaseLoadedColumnVisible(wc7Line)
    }
    
    property get wc7SpecificWaiverOfSubro () : WC7WaiverOfSubro {
      return getIteratedValue(2) as WC7WaiverOfSubro
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7SpecificWaiverOfSubroLVExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 58, column 56
    function initialValue_8 () : gw.pcf.WC7CoverageInputSetUIHelper {
      return new gw.pcf.WC7CoverageInputSetUIHelper()
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 78, column 30
    function sortBy_10 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.JobID
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 81, column 30
    function sortBy_11 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.Description
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 84, column 30
    function sortBy_12 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.EffectiveDate
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 75, column 30
    function sortBy_9 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.Jurisdiction
    }
    
    // 'value' attribute on TextAreaCell (id=Description_TDIC_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 101, column 63
    function sortValue_13 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.Description
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 109, column 47
    function sortValue_14 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.JobID
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 119, column 29
    function sortValue_15 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.Jurisdiction
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 131, column 29
    function sortValue_16 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.Location
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 146, column 29
    function sortValue_17 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.ClassCode
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 159, column 30
    function sortValue_18 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.GoverningLaw
    }
    
    // 'value' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function sortValue_19 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 183, column 33
    function sortValue_21 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.WaiverEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 191, column 33
    function sortValue_22 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7SpecificWaiverOfSubro.WaiverExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 72, column 51
    function toCreateAndAdd_131 () : WC7WaiverOfSubro {
      return wc7Line.createAndAddSpecificWaiverOfSubroWM()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 72, column 51
    function toRemove_132 (wc7SpecificWaiverOfSubro :  WC7WaiverOfSubro) : void {
      wc7SpecificWaiverOfSubro.removeWM()
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 72, column 51
    function value_133 () : entity.WC7WaiverOfSubro[] {
      return wc7Line.WC7SpecificWaiversWM
    }
    
    // 'visible' attribute on CheckBoxCell (id=DiseaseLoaded_Cell) at CoverageInputSet.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond.pcf: line 175, column 89
    function visible_20 () : java.lang.Boolean {
      return coverageInputSetHelper.isDiseaseLoadedColumnVisible(wc7Line)
    }
    
    property get coverageInputSetHelper () : gw.pcf.WC7CoverageInputSetUIHelper {
      return getVariableValue("coverageInputSetHelper", 1) as gw.pcf.WC7CoverageInputSetUIHelper
    }
    
    property set coverageInputSetHelper ($arg :  gw.pcf.WC7CoverageInputSetUIHelper) {
      setVariableValue("coverageInputSetHelper", 1, $arg)
    }
    
    
  }
  
  
}