package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBasePickExistingDocumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBasePickExistingDocumentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBasePickExistingDocumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends OnBasePickExistingDocumentPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLink) at OnBasePickExistingDocumentPopup.pcf: line 85, column 152
    function action_18 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLink2) at OnBasePickExistingDocumentPopup.pcf: line 94, column 150
    function action_23 () : void {
      Document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLink) at OnBasePickExistingDocumentPopup.pcf: line 85, column 152
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, documentsActionsHelper.DocumentContentActionsAvailable)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at OnBasePickExistingDocumentPopup.pcf: line 64, column 36
    function icon_12 () : java.lang.String {
      return (Document as Document).Icon
    }
    
    // 'label' attribute on Link (id=NameLink) at OnBasePickExistingDocumentPopup.pcf: line 85, column 152
    function label_19 () : java.lang.Object {
      return Document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLink) at OnBasePickExistingDocumentPopup.pcf: line 85, column 152
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBasePickExistingDocumentPopup.pcf: line 69, column 40
    function valueRoot_14 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBasePickExistingDocumentPopup.pcf: line 69, column 40
    function value_13 () : java.lang.String {
      return Document.DocUID
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBasePickExistingDocumentPopup.pcf: line 100, column 49
    function value_26 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at OnBasePickExistingDocumentPopup.pcf: line 105, column 62
    function value_29 () : typekey.OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBasePickExistingDocumentPopup.pcf: line 110, column 55
    function value_32 () : typekey.DocumentStatusType {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBasePickExistingDocumentPopup.pcf: line 115, column 40
    function value_35 () : java.lang.String {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBasePickExistingDocumentPopup.pcf: line 123, column 46
    function value_38 () : java.util.Date {
      return Document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLink) at OnBasePickExistingDocumentPopup.pcf: line 85, column 152
    function visible_17 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLink2) at OnBasePickExistingDocumentPopup.pcf: line 94, column 150
    function visible_22 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get Document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBasePickExistingDocumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBasePickExistingDocumentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, Beans :  KeyableBean[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=OnBasePickExistingDocumentPopup_CancelButton) at OnBasePickExistingDocumentPopup.pcf: line 41, column 99
    function action_4 () : void {
      CurrentLocation.cancel();
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=OnBasePickExistingDocumentPopup_LinkButton) at OnBasePickExistingDocumentPopup.pcf: line 37, column 99
    function allCheckedRowsAction_3 (CheckedValues :  entity.Document[], CheckedValuesErrors :  java.util.Map) : void {
      DocumentLinking.linkDocumentsToEntity(Entity, CheckedValues, LinkType); CurrentLocation.cancel();
    }
    
    // 'initialValue' attribute on Variable at OnBasePickExistingDocumentPopup.pcf: line 19, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at OnBasePickExistingDocumentPopup.pcf: line 26, column 58
    function initialValue_1 () : acc.onbase.api.application.DocumentLinking {
      return new acc.onbase.api.application.DocumentLinking()
    }
    
    // 'initialValue' attribute on Variable at OnBasePickExistingDocumentPopup.pcf: line 30, column 36
    function initialValue_2 () : List<Document> {
      return DocumentLinking.getDocumentsNotLinkedToEntity(Entity, LinkType, Beans)
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBasePickExistingDocumentPopup.pcf: line 115, column 40
    function sortValue_10 (Document :  entity.Document) : java.lang.Object {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBasePickExistingDocumentPopup.pcf: line 123, column 46
    function sortValue_11 (Document :  entity.Document) : java.lang.Object {
      return Document.DateModified
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBasePickExistingDocumentPopup.pcf: line 69, column 40
    function sortValue_5 (Document :  entity.Document) : java.lang.Object {
      return Document.DocUID
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at OnBasePickExistingDocumentPopup.pcf: line 76, column 27
    function sortValue_6 (Document :  entity.Document) : java.lang.Object {
      return (Document as Document).Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBasePickExistingDocumentPopup.pcf: line 100, column 49
    function sortValue_7 (Document :  entity.Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at OnBasePickExistingDocumentPopup.pcf: line 105, column 62
    function sortValue_8 (Document :  entity.Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBasePickExistingDocumentPopup.pcf: line 110, column 55
    function sortValue_9 (Document :  entity.Document) : java.lang.Object {
      return Document.Status
    }
    
    // 'value' attribute on RowIterator at OnBasePickExistingDocumentPopup.pcf: line 57, column 44
    function value_41 () : List<Document> {
      return NotLinkedDocuments
    }
    
    // 'valueType' attribute on RowIterator at OnBasePickExistingDocumentPopup.pcf: line 57, column 44
    function verifyValueTypeIsAllowedType_42 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBasePickExistingDocumentPopup.pcf: line 57, column 44
    function verifyValueTypeIsAllowedType_42 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBasePickExistingDocumentPopup.pcf: line 57, column 44
    function verifyValueTypeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBasePickExistingDocumentPopup.pcf: line 57, column 44
    function verifyValueType_43 () : void {
      var __valueTypeArg : List<Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_42(__valueTypeArg)
    }
    
    property get Beans () : KeyableBean[] {
      return getVariableValue("Beans", 0) as KeyableBean[]
    }
    
    property set Beans ($arg :  KeyableBean[]) {
      setVariableValue("Beans", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.OnBasePickExistingDocumentPopup {
      return super.CurrentLocation as pcf.OnBasePickExistingDocumentPopup
    }
    
    property get DocumentLinking () : acc.onbase.api.application.DocumentLinking {
      return getVariableValue("DocumentLinking", 0) as acc.onbase.api.application.DocumentLinking
    }
    
    property set DocumentLinking ($arg :  acc.onbase.api.application.DocumentLinking) {
      setVariableValue("DocumentLinking", 0, $arg)
    }
    
    property get Entity () : KeyableBean {
      return getVariableValue("Entity", 0) as KeyableBean
    }
    
    property set Entity ($arg :  KeyableBean) {
      setVariableValue("Entity", 0, $arg)
    }
    
    property get LinkType () : acc.onbase.configuration.DocumentLinkType {
      return getVariableValue("LinkType", 0) as acc.onbase.configuration.DocumentLinkType
    }
    
    property set LinkType ($arg :  acc.onbase.configuration.DocumentLinkType) {
      setVariableValue("LinkType", 0, $arg)
    }
    
    property get NotLinkedDocuments () : List<Document> {
      return getVariableValue("NotLinkedDocuments", 0) as List<Document>
    }
    
    property set NotLinkedDocuments ($arg :  List<Document>) {
      setVariableValue("NotLinkedDocuments", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}