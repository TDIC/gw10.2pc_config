package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_PolicyInfoScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubmissionWizard_PolicyInfoScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_PolicyInfoScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubmissionWizard_PolicyInfoScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 36, column 73
    function def_onEnter_21 (def :  pcf.WarningsPanelSet) : void {
      def.onEnter(period.Policy.ContingencyWarningMessages)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 38, column 86
    function def_onEnter_23 (def :  pcf.SubmissionWizard_PolicyInfoDV) : void {
      def.onEnter(period, submission, openForEdit,quoteType)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 43, column 39
    function def_onEnter_26 (def :  pcf.TDIC_WC7PolicyOwnerOfficerDV) : void {
      def.onEnter(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 46, column 39
    function def_onEnter_29 (def :  pcf.TDIC_BOPPolicyOwnerOfficerDV) : void {
      def.onEnter(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 49, column 105
    function def_onEnter_32 (def :  pcf.TDIC_GLPolicyOwnerOfficerDV) : void {
      def.onEnter(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 52, column 39
    function def_onEnter_35 (def :  pcf.AdditionalNamedInsuredsDV) : void {
      def.onEnter(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 36, column 73
    function def_refreshVariables_22 (def :  pcf.WarningsPanelSet) : void {
      def.refreshVariables(period.Policy.ContingencyWarningMessages)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 38, column 86
    function def_refreshVariables_24 (def :  pcf.SubmissionWizard_PolicyInfoDV) : void {
      def.refreshVariables(period, submission, openForEdit,quoteType)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 43, column 39
    function def_refreshVariables_27 (def :  pcf.TDIC_WC7PolicyOwnerOfficerDV) : void {
      def.refreshVariables(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 46, column 39
    function def_refreshVariables_30 (def :  pcf.TDIC_BOPPolicyOwnerOfficerDV) : void {
      def.refreshVariables(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 49, column 105
    function def_refreshVariables_33 (def :  pcf.TDIC_GLPolicyOwnerOfficerDV) : void {
      def.refreshVariables(period, openForEdit)
    }
    
    // 'def' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 52, column 39
    function def_refreshVariables_36 (def :  pcf.AdditionalNamedInsuredsDV) : void {
      def.refreshVariables(period, openForEdit)
    }
    
    // 'editable' attribute on Screen (id=SubmissionWizard_PolicyInfoScreen) at SubmissionWizard_PolicyInfoScreen.pcf: line 7, column 44
    function editable_37 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at SubmissionWizard_PolicyInfoScreen.pcf: line 24, column 33
    function initialValue_0 () : typekey.QuoteType {
      return submission.QuoteType
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function mode_1 () : java.lang.Object {
      return period.Job.Subtype
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_10 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_12 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_14 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_16 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_18 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_2 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_4 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_6 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_onEnter_8 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.onEnter(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_11 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_13 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_15 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_17 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_19 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_3 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_5 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_7 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at SubmissionWizard_PolicyInfoScreen.pcf: line 29, column 92
    function toolbarButtonSet_refreshVariables_9 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.refreshVariables(period, submission, jobWizardHelper)
    }
    
    // 'visible' attribute on AlertBar (id=QuoteRequestedAlert) at SubmissionWizard_PolicyInfoScreen.pcf: line 34, column 74
    function visible_20 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.isQuoteRequestInProgress(period)
    }
    
    // 'visible' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 43, column 39
    function visible_25 () : java.lang.Boolean {
      return period.WC7LineExists
    }
    
    // 'visible' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 46, column 39
    function visible_28 () : java.lang.Boolean {
      return period.BOPLineExists
    }
    
    // 'visible' attribute on PanelRef at SubmissionWizard_PolicyInfoScreen.pcf: line 49, column 105
    function visible_31 () : java.lang.Boolean {
      return period.GLLineExists and period.Offering.CodeIdentifier == "PLCyberLiab_TDIC"
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get quoteType () : typekey.QuoteType {
      return getVariableValue("quoteType", 0) as typekey.QuoteType
    }
    
    property set quoteType ($arg :  typekey.QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    property get submission () : Submission {
      return getRequireValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setRequireValue("submission", 0, $arg)
    }
    
    
  }
  
  
}