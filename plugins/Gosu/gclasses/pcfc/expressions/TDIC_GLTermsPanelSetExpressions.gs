package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLTermsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLTermsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLTermsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscountCov_Cell) at TDIC_GLTermsPanelSet.pcf: line 140, column 74
    function currency_49 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLTermsPanelSet.pcf: line 89, column 30
    function initialValue_11 () : GLExposure {
      return quoteScreenHelper.getTermExposure(termCost)
    }
    
    // RowIterator (id=TermIterator) at TDIC_GLTermsPanelSet.pcf: line 85, column 30
    function initializeVariables_93 () : void {
        glExposure = quoteScreenHelper.getTermExposure(termCost);

    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at TDIC_GLTermsPanelSet.pcf: line 98, column 77
    function valueRoot_13 () : java.lang.Object {
      return (termCost as GLCovExposureCost).GLCostType
    }
    
    // 'value' attribute on DateCell (id=policyChangeEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 105, column 74
    function valueRoot_18 () : java.lang.Object {
      return (termCost as GLCovExposureCost)
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 118, column 60
    function valueRoot_30 () : java.lang.Object {
      return glExposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=territoryCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 121, column 64
    function valueRoot_33 () : java.lang.Object {
      return glExposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=specialityCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 124, column 65
    function valueRoot_36 () : java.lang.Object {
      return glExposure.SpecialityCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=discount_Cell) at TDIC_GLTermsPanelSet.pcf: line 127, column 80
    function valueRoot_39 () : java.lang.Object {
      return (termCost as GLCovExposureCost).Discount_TDIC
    }
    
    // 'value' attribute on TextCell (id=StepYearCov_Cell) at TDIC_GLTermsPanelSet.pcf: line 130, column 76
    function valueRoot_42 () : java.lang.Object {
      return tdic.pc.config.rating.RatingConstants
    }
    
    // 'value' attribute on TextCell (id=histPremType_Cell) at TDIC_GLTermsPanelSet.pcf: line 147, column 79
    function valueRoot_53 () : java.lang.Object {
      return (termCost as GLHistoricalCost_TDIC).CostType
    }
    
    // 'value' attribute on DateCell (id=policChangeHistEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 154, column 74
    function valueRoot_58 () : java.lang.Object {
      return (termCost as GLHistoricalCost_TDIC)
    }
    
    // 'value' attribute on DateCell (id=histEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 161, column 84
    function valueRoot_66 () : java.lang.Object {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters
    }
    
    // 'value' attribute on TextCell (id=histDiscount_Cell) at TDIC_GLTermsPanelSet.pcf: line 176, column 97
    function valueRoot_80 () : java.lang.Object {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.Discount
    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at TDIC_GLTermsPanelSet.pcf: line 98, column 77
    function value_12 () : java.lang.String {
      return (termCost as GLCovExposureCost).GLCostType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_GLTermsPanelSet.pcf: line 101, column 68
    function value_15 () : java.lang.String {
      return quoteScreenHelper.getLiabilityLimit(termCost)
    }
    
    // 'value' attribute on DateCell (id=policyChangeEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 105, column 74
    function value_17 () : java.util.Date {
      return (termCost as GLCovExposureCost).EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyChangeExpirationDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 109, column 74
    function value_21 () : java.util.Date {
      return (termCost as GLCovExposureCost).ExpirationDate
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 112, column 102
    function value_25 () : java.util.Date {
      return isEREJob ? glExposure.SliceDate : (termCost as GLCovExposureCost).EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 115, column 133
    function value_27 () : java.util.Date {
      return isEREJob ? quoteScreenHelper.getERETermExpirationDate(termCost):(termCost as GLCovExposureCost).ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 118, column 60
    function value_29 () : java.lang.String {
      return glExposure.ClassCode_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=territoryCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 121, column 64
    function value_32 () : java.lang.String {
      return glExposure.GLTerritory_TDIC.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=specialityCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 124, column 65
    function value_35 () : java.lang.String {
      return glExposure.SpecialityCode_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=discount_Cell) at TDIC_GLTermsPanelSet.pcf: line 127, column 80
    function value_38 () : java.lang.String {
      return (termCost as GLCovExposureCost).Discount_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StepYearCov_Cell) at TDIC_GLTermsPanelSet.pcf: line 130, column 76
    function value_41 () : java.lang.String {
      return tdic.pc.config.rating.RatingConstants.currentStepYear
    }
    
    // 'value' attribute on TextCell (id=TermPremiumCov_Cell) at TDIC_GLTermsPanelSet.pcf: line 134, column 47
    function value_44 () : java.math.BigDecimal {
      return (termCost as GLCovExposureCost).PreDiscountPremium_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscountCov_Cell) at TDIC_GLTermsPanelSet.pcf: line 140, column 74
    function value_47 () : gw.pl.currency.MonetaryAmount {
      return (termCost as GLCovExposureCost).ActualAmountBilling
    }
    
    // 'value' attribute on TextCell (id=histPremType_Cell) at TDIC_GLTermsPanelSet.pcf: line 147, column 79
    function value_52 () : java.lang.String {
      return (termCost as GLHistoricalCost_TDIC).CostType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=policChangeHistEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 154, column 74
    function value_57 () : java.util.Date {
      return (termCost as GLHistoricalCost_TDIC).EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyChangeExpDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 158, column 74
    function value_61 () : java.util.Date {
      return (termCost as GLHistoricalCost_TDIC).ExpirationDate
    }
    
    // 'value' attribute on DateCell (id=histEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 161, column 84
    function value_65 () : java.util.Date {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.EffDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 164, column 84
    function value_68 () : java.util.Date {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.ExpDate
    }
    
    // 'value' attribute on TextCell (id=histClassCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 167, column 86
    function value_71 () : java.lang.String {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.ClassCode
    }
    
    // 'value' attribute on TextCell (id=histTerritoryCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 170, column 90
    function value_74 () : java.lang.String {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=histSpecialityCode_Cell) at TDIC_GLTermsPanelSet.pcf: line 173, column 67
    function value_77 () : java.lang.String {
      return quoteScreenHelper.getSpecialtyCode(termCost)
    }
    
    // 'value' attribute on TextCell (id=histDiscount_Cell) at TDIC_GLTermsPanelSet.pcf: line 176, column 97
    function value_79 () : java.lang.String {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.Discount.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StepYear_Cell) at TDIC_GLTermsPanelSet.pcf: line 179, column 87
    function value_82 () : java.lang.String {
      return (termCost as GLHistoricalCost_TDIC).GLSplitParameters.splitYear 
    }
    
    // 'value' attribute on TextCell (id=TermPremium_Cell) at TDIC_GLTermsPanelSet.pcf: line 184, column 47
    function value_85 () : java.math.BigDecimal {
      return (termCost as GLHistoricalCost_TDIC).PreDiscountPremium_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLTermsPanelSet.pcf: line 191, column 71
    function value_88 () : gw.pl.currency.MonetaryAmount {
      return (termCost as GLHistoricalCost_TDIC).ActualAmount
    }
    
    // 'visible' attribute on DateCell (id=policyChangeEffDate_Cell) at TDIC_GLTermsPanelSet.pcf: line 105, column 74
    function visible_19 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not isEREJob
    }
    
    // 'visible' attribute on Row at TDIC_GLTermsPanelSet.pcf: line 95, column 55
    function visible_51 () : java.lang.Boolean {
      return termCost typeis GLCovExposureCost
    }
    
    // 'visible' attribute on Row at TDIC_GLTermsPanelSet.pcf: line 143, column 161
    function visible_92 () : java.lang.Boolean {
      return (termCost typeis GLHistoricalCost_TDIC) and (termCost.Branch.Offering.CodeIdentifier == tdic.pc.config.rating.RatingConstants.plCMOffering)
    }
    
    property get glExposure () : GLExposure {
      return getVariableValue("glExposure", 2) as GLExposure
    }
    
    property set glExposure ($arg :  GLExposure) {
      setVariableValue("glExposure", 2, $arg)
    }
    
    property get termCost () : GLCost {
      return getIteratedValue(2) as GLCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLTermsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListViewPanelExpressionsImpl extends TDIC_GLTermsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator (id=TermIterator) at TDIC_GLTermsPanelSet.pcf: line 85, column 30
    function editable_10 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLTermsPanelSet.pcf: line 33, column 35
    function initialValue_2 () : Set<GLCost> {
      return isEREJob ? quoteScreenHelper.getERETermPremiums(glCosts) : quoteScreenHelper.getNonERETermPremiums(glCosts)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLTermsPanelSet.pcf: line 93, column 26
    function sortBy_5 (termCost :  GLCost) : java.lang.Object {
      return (termCost typeis GLHistoricalCost_TDIC)?(termCost as GLHistoricalCost_TDIC).GLSplitParameters.EffDate : (isEREJob ? quoteScreenHelper.getERETermExpirationDate(termCost):(termCost as GLCovExposureCost).ExpirationDate)
    }
    
    // 'footerSumValue' attribute on RowIterator (id=TermIterator) at TDIC_GLTermsPanelSet.pcf: line 184, column 47
    function sumValue_8 (termCost :  GLCost) : java.lang.Object {
      return termCost typeis GLHistoricalCost_TDIC ? (termCost as GLHistoricalCost_TDIC).PreDiscountPremium_TDIC : termCost typeis GLCovExposureCost ? (termCost as GLCovExposureCost).PreDiscountPremium_TDIC : null 
    }
    
    // 'footerSumValue' attribute on RowIterator (id=TermIterator) at TDIC_GLTermsPanelSet.pcf: line 191, column 71
    function sumValue_9 (termCost :  GLCost) : java.lang.Object {
      return termCost typeis GLHistoricalCost_TDIC ? (termCost as GLHistoricalCost_TDIC).ActualAmountBilling : termCost typeis GLCovExposureCost ? (termCost as GLCovExposureCost).ActualAmountBilling : null 
    }
    
    // 'value' attribute on RowIterator (id=TermIterator) at TDIC_GLTermsPanelSet.pcf: line 85, column 30
    function value_94 () : GLCost[] {
      return termCosts.toTypedArray()
    }
    
    // 'visible' attribute on TextCell (id=policyChangeEffDateTeHeader_Cell) at TDIC_GLTermsPanelSet.pcf: line 45, column 72
    function visible_3 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not isEREJob
    }
    
    property get termCosts () : Set<GLCost> {
      return getVariableValue("termCosts", 1) as Set<GLCost>
    }
    
    property set termCosts ($arg :  Set<GLCost>) {
      setVariableValue("termCosts", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLTermsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLTermsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_GLTermsPanelSet.pcf: line 20, column 107
    function visible_0 () : java.lang.Boolean {
      return (period.Offering.CodeIdentifier != tdic.pc.config.rating.RatingConstants.plCyberOffering)
    }
    
    property get glCosts () : Set<entity.GLCost> {
      return getRequireValue("glCosts", 0) as Set<entity.GLCost>
    }
    
    property set glCosts ($arg :  Set<entity.GLCost>) {
      setRequireValue("glCosts", 0, $arg)
    }
    
    property get isEREJob () : boolean {
      return getRequireValue("isEREJob", 0) as java.lang.Boolean
    }
    
    property set isEREJob ($arg :  boolean) {
      setRequireValue("isEREJob", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get quoteScreenHelper () : tdic.web.pcf.helper.GLQuoteScreenHelper {
      return getVariableValue("quoteScreenHelper", 0) as tdic.web.pcf.helper.GLQuoteScreenHelper
    }
    
    property set quoteScreenHelper ($arg :  tdic.web.pcf.helper.GLQuoteScreenHelper) {
      setVariableValue("quoteScreenHelper", 0, $arg)
    }
    
    
  }
  
  
}