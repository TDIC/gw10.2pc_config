package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7BenefitsDedCov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7BenefitsDedCovExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7BenefitsDedCov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7BenefitsDedCov.pcf: line 20, column 38
    function initialValue_0 () : entity.WC7Jurisdiction {
      return coverable as WC7Jurisdiction
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7BenefitsDedCov.pcf: line 25, column 71
    function initialValue_1 () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return wc7Jurisdiction.RatingPeriods
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 29, column 37
    function valueRoot_3 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 29, column 37
    function value_2 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on InputIterator (id=ratingperiod) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 34, column 55
    function value_29 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.toTypedArray()
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7BenefitsDedCov.pcf: line 79, column 100
    function visible_30 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get ratingPeriods () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return getVariableValue("ratingPeriods", 0) as java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>
    }
    
    property set ratingPeriods ($arg :  java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>) {
      setVariableValue("ratingPeriods", 0, $arg)
    }
    
    property get wc7Jurisdiction () : entity.WC7Jurisdiction {
      return getVariableValue("wc7Jurisdiction", 0) as entity.WC7Jurisdiction
    }
    
    property set wc7Jurisdiction ($arg :  entity.WC7Jurisdiction) {
      setVariableValue("wc7Jurisdiction", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7BenefitsDedCov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at CoverageInputSet.WC7BenefitsDedCov.pcf: line 38, column 47
    function label_6 () : java.lang.String {
      return DisplayKey.get("Web.Policy.WC.Period", ratingPeriod.Number)
    }
    
    // 'visible' attribute on Label at CoverageInputSet.WC7BenefitsDedCov.pcf: line 38, column 47
    function visible_5 () : java.lang.Boolean {
      return ratingPeriods.Count > 1
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(1) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7BenefitsDedCov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7SplitableInputSetExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      benefitsDedCov.WC7DeductibleTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7DeductibleType>)
    }
    
    // 'editable' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function editable_9 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(benefitsDedCov.WC7DeductibleTerm)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7BenefitsDedCov.pcf: line 45, column 54
    function initialValue_7 () : gw.pcf.WC7CoverageInputSetUIHelper {
      return new gw.pcf.WC7CoverageInputSetUIHelper()
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7BenefitsDedCov.pcf: line 49, column 50
    function initialValue_8 () : productmodel.WC7BenefitsDedCov {
      return coverageInputSetHelper.getBenefitsDeductibleCovInRatingPeriod(ratingPeriod)
    }
    
    // 'label' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function label_11 () : java.lang.Object {
      return benefitsDedCov.WC7DeductibleTerm.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 55, column 109
    function label_24 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'required' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function required_12 () : java.lang.Boolean {
      return benefitsDedCov.WC7DeductibleTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 55, column 109
    function setter_25 (VALUE :  java.lang.Boolean) : void {
      benefitsDedCov = coverageInputSetHelper.setBenefitsDedCovOnToggle(VALUE, ratingPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function valueRange_16 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(benefitsDedCov.WC7DeductibleTerm, openForEdit)
    }
    
    // 'value' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function valueRoot_15 () : java.lang.Object {
      return benefitsDedCov.WC7DeductibleTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function value_13 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7DeductibleType> {
      return benefitsDedCov.WC7DeductibleTerm.OptionValue
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7DeductibleType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function verifyValueRange_18 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(benefitsDedCov.WC7DeductibleTerm, openForEdit)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=WC7DeductibleTermInput_Input) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 74, column 202
    function visible_10 () : java.lang.Boolean {
      return wc7Jurisdiction.getSlice(ratingPeriod.Start).isCovTermAvailable(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.CovTermPattern>("WC7Deductible"))
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 55, column 109
    function visible_23 () : java.lang.Boolean {
      return benefitsDedCov != null and wc7Jurisdiction.getSlice(ratingPeriod.Start).WC7BenefitsDedCovExists
    }
    
    // 'visible' attribute on InputSet (id=WC7SplitableInputSet) at CoverageInputSet.WC7BenefitsDedCov.pcf: line 41, column 93
    function visible_28 () : java.lang.Boolean {
      return wc7Jurisdiction.isCoverageConditionOrExclusionAvailable(coveragePattern )
    }
    
    property get benefitsDedCov () : productmodel.WC7BenefitsDedCov {
      return getVariableValue("benefitsDedCov", 2) as productmodel.WC7BenefitsDedCov
    }
    
    property set benefitsDedCov ($arg :  productmodel.WC7BenefitsDedCov) {
      setVariableValue("benefitsDedCov", 2, $arg)
    }
    
    property get coverageInputSetHelper () : gw.pcf.WC7CoverageInputSetUIHelper {
      return getVariableValue("coverageInputSetHelper", 2) as gw.pcf.WC7CoverageInputSetUIHelper
    }
    
    property set coverageInputSetHelper ($arg :  gw.pcf.WC7CoverageInputSetUIHelper) {
      setVariableValue("coverageInputSetHelper", 2, $arg)
    }
    
    
  }
  
  
}