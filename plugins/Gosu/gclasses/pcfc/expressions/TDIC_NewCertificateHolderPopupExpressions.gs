package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_NewCertificateHolderPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_NewCertificateHolderPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_NewCertificateHolderPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_NewCertificateHolderPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (glLine :  entity.GeneralLiabilityLine, contactType :  ContactType) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at TDIC_NewCertificateHolderPopup.pcf: line 58, column 62
    function action_11 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at TDIC_NewCertificateHolderPopup.pcf: line 50, column 62
    function action_6 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(policyCertificateHolder))
    }
    
    // 'beforeCommit' attribute on Popup (id=TDIC_NewCertificateHolderPopup) at TDIC_NewCertificateHolderPopup.pcf: line 12, column 103
    function beforeCommit_18 (pickedValue :  PolicyCertificateHolder_TDIC) : void {
      policyCertificateHolder.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch); helper.validateAndUpdateStatusOfAddresses(contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_NewCertificateHolderPopup.pcf: line 71, column 78
    function def_onEnter_16 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(policyCertificateHolder, false)
    }
    
    // 'def' attribute on PanelRef at TDIC_NewCertificateHolderPopup.pcf: line 71, column 78
    function def_refreshVariables_17 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(policyCertificateHolder, false)
    }
    
    // 'value' attribute on TextInput (id=DescriptionOfInterest_Input) at TDIC_NewCertificateHolderPopup.pcf: line 67, column 58
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyCertificateHolder.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at TDIC_NewCertificateHolderPopup.pcf: line 25, column 51
    function initialValue_0 () : entity.PolicyCertificateHolder_TDIC {
      return glLine.addNewPolicyCertificateHolderOfContactType_TDIC(contactType)
    }
    
    // 'initialValue' attribute on Variable at TDIC_NewCertificateHolderPopup.pcf: line 29, column 25
    function initialValue_1 () : Contact[] {
      return glLine.GLCertificateHolders_TDIC.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at TDIC_NewCertificateHolderPopup.pcf: line 33, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(policyCertificateHolder.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'initialValue' attribute on Variable at TDIC_NewCertificateHolderPopup.pcf: line 37, column 76
    function initialValue_3 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at TDIC_NewCertificateHolderPopup.pcf: line 41, column 30
    function initialValue_4 () : entity.Contact {
      return policyCertificateHolder.AccountContactRole.AccountContact.Contact
    }
    
    // EditButtons at TDIC_NewCertificateHolderPopup.pcf: line 53, column 72
    function label_9 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at TDIC_NewCertificateHolderPopup.pcf: line 53, column 72
    function pickValue_7 () : PolicyCertificateHolder_TDIC {
      return policyCertificateHolder
    }
    
    // 'title' attribute on Popup (id=TDIC_NewCertificateHolderPopup) at TDIC_NewCertificateHolderPopup.pcf: line 12, column 103
    static function title_19 (contactType :  ContactType, glLine :  entity.GeneralLiabilityLine) : java.lang.Object {
      return DisplayKey.get("Web.Contact.NewContact", "Policy Certificate Holder")
    }
    
    // 'value' attribute on TextInput (id=DescriptionOfInterest_Input) at TDIC_NewCertificateHolderPopup.pcf: line 67, column 58
    function valueRoot_14 () : java.lang.Object {
      return policyCertificateHolder
    }
    
    // 'value' attribute on TextInput (id=DescriptionOfInterest_Input) at TDIC_NewCertificateHolderPopup.pcf: line 67, column 58
    function value_12 () : java.lang.String {
      return policyCertificateHolder.Description
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at TDIC_NewCertificateHolderPopup.pcf: line 50, column 62
    function visible_5 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at TDIC_NewCertificateHolderPopup.pcf: line 53, column 72
    function visible_8 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.TDIC_NewCertificateHolderPopup {
      return super.CurrentLocation as pcf.TDIC_NewCertificateHolderPopup
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactType () : ContactType {
      return getVariableValue("contactType", 0) as ContactType
    }
    
    property set contactType ($arg :  ContactType) {
      setVariableValue("contactType", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get glLine () : entity.GeneralLiabilityLine {
      return getVariableValue("glLine", 0) as entity.GeneralLiabilityLine
    }
    
    property set glLine ($arg :  entity.GeneralLiabilityLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get policyCertificateHolder () : entity.PolicyCertificateHolder_TDIC {
      return getVariableValue("policyCertificateHolder", 0) as entity.PolicyCertificateHolder_TDIC
    }
    
    property set policyCertificateHolder ($arg :  entity.PolicyCertificateHolder_TDIC) {
      setVariableValue("policyCertificateHolder", 0, $arg)
    }
    
    
  }
  
  
}