package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class HOPAdditionalCoveragesPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel2ExpressionsImpl extends HOPAdditionalCoveragesPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at HOPAdditionalCoveragesPanelSet.pcf: line 52, column 61
    function initialValue_110 () : gw.api.productmodel.CoveragePattern[] {
      return HOPClausePanelSetHelper.getLineAdditionalCoveragesPatterns(hopLine, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at HOPAdditionalCoveragesPanelSet.pcf: line 61, column 34
    function sortBy_111 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=liabilityCoveragePatternIterator) at HOPAdditionalCoveragesPanelSet.pcf: line 58, column 67
    function value_219 () : gw.api.productmodel.CoveragePattern[] {
      return liabilityCoveragePatterns
    }
    
    property get liabilityCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("liabilityCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set liabilityCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("liabilityCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends HOPAdditionalCoveragesPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at HOPAdditionalCoveragesPanelSet.pcf: line 27, column 61
    function initialValue_0 () : gw.api.productmodel.CoveragePattern[] {
      return HOPClausePanelSetHelper.getDwellingAdditionalCoveragesPatterns(dwelling, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at HOPAdditionalCoveragesPanelSet.pcf: line 36, column 34
    function sortBy_1 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=dwellingCoveragePatternIterator) at HOPAdditionalCoveragesPanelSet.pcf: line 33, column 67
    function value_109 () : gw.api.productmodel.CoveragePattern[] {
      return dwellingCoveragePatterns
    }
    
    property get dwellingCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("dwellingCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set dwellingCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("dwellingCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HOPAdditionalCoveragesPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get HOPClausePanelSetHelper () : gw.web.line.hop.policy.HOPClausePanelSetHelper {
      return getVariableValue("HOPClausePanelSetHelper", 0) as gw.web.line.hop.policy.HOPClausePanelSetHelper
    }
    
    property set HOPClausePanelSetHelper ($arg :  gw.web.line.hop.policy.HOPClausePanelSetHelper) {
      setVariableValue("HOPClausePanelSetHelper", 0, $arg)
    }
    
    property get dwelling () : HOPDwelling {
      return getRequireValue("dwelling", 0) as HOPDwelling
    }
    
    property set dwelling ($arg :  HOPDwelling) {
      setRequireValue("dwelling", 0, $arg)
    }
    
    property get hopLine () : productmodel.HOPLine {
      return getRequireValue("hopLine", 0) as productmodel.HOPLine
    }
    
    property set hopLine ($arg :  productmodel.HOPLine) {
      setRequireValue("hopLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_112 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_114 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_116 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_118 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_120 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_122 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_124 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_126 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_128 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_130 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_132 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_134 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_136 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_138 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_140 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_142 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_144 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_146 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_148 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_150 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_152 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_154 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_156 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_158 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_166 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_168 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_170 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_172 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_174 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_176 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_178 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_180 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_182 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_184 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_186 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_188 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_190 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_192 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_194 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_196 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_198 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_200 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_202 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_204 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_206 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_onEnter_216 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, hopLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 64, column 58
    function mode_218 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPAdditionalCoveragesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_10 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_106 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_12 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_14 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_16 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_18 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_2 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_20 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_22 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_24 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_26 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_28 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_30 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_32 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_34 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_36 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_4 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_56 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_58 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_6 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_60 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_62 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_64 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_66 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_68 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_70 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_72 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_74 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_76 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_78 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_8 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_80 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_82 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_84 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_11 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_13 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_15 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_17 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_19 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_21 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_3 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_5 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_7 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_9 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, dwelling, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPAdditionalCoveragesPanelSet.pcf: line 39, column 58
    function mode_108 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  
}