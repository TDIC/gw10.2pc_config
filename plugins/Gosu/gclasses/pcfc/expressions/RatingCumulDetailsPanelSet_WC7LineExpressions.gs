package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingCumulDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RatingCumulDetailsPanelSet_WC7LineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingCumulDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at RatingCumulDetailsPanelSet.WC7Line.pcf: line 75, column 112
    function def_onEnter_11 (def :  pcf.WC7StateCostsLV) : void {
      def.onEnter(periodCosts.getStandardPremiums(period.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on ListViewInput at RatingCumulDetailsPanelSet.WC7Line.pcf: line 75, column 112
    function def_refreshVariables_12 (def :  pcf.WC7StateCostsLV) : void {
      def.refreshVariables(periodCosts.getStandardPremiums(period.PreferredSettlementCurrency))
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 66, column 59
    function initialValue_9 () : java.util.Set<entity.WC7Cost> {
      return stateCosts.byRatingPeriod(ratingPeriod)
    }
    
    // PanelIterator at RatingCumulDetailsPanelSet.WC7Line.pcf: line 61, column 61
    function initializeVariables_14 () : void {
        periodCosts = stateCosts.byRatingPeriod(ratingPeriod);

    }
    
    // 'label' attribute on Label at RatingCumulDetailsPanelSet.WC7Line.pcf: line 73, column 90
    function label_10 () : java.lang.String {
      return standardPremLabel(ratingPeriods.Count > 1, ratingPeriod)
    }
    
    // 'visible' attribute on PanelRef at RatingCumulDetailsPanelSet.WC7Line.pcf: line 68, column 44
    function visible_13 () : java.lang.Boolean {
      return !periodCosts.Empty
    }
    
    property get periodCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("periodCosts", 2) as java.util.Set<entity.WC7Cost>
    }
    
    property set periodCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("periodCosts", 2, $arg)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingCumulDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at RatingCumulDetailsPanelSet.WC7Line.pcf: line 89, column 126
    function def_onEnter_16 (def :  pcf.WC7StateCostsLV) : void {
      def.onEnter(stateCosts.toSet().getOtherPremiumAndSurcharges(period.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on ListViewInput at RatingCumulDetailsPanelSet.WC7Line.pcf: line 89, column 126
    function def_refreshVariables_17 (def :  pcf.WC7StateCostsLV) : void {
      def.refreshVariables(stateCosts.toSet().getOtherPremiumAndSurcharges(period.PreferredSettlementCurrency))
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 46, column 53
    function initialValue_6 () : java.util.Set<entity.WC7Cost> {
      return partitionCosts.get(jurisdiction.Jurisdiction).where(\elt -> elt.ActualAmount != 0 or (elt typeis  WC7CovEmpCost)).toSet()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 51, column 73
    function initialValue_7 () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return jurisdiction.RatingPeriods
    }
    
    // PanelIterator at RatingCumulDetailsPanelSet.WC7Line.pcf: line 38, column 44
    function initializeVariables_19 () : void {
        stateCosts = partitionCosts.get(jurisdiction.Jurisdiction).where(\elt -> elt.ActualAmount != 0 or (elt typeis  WC7CovEmpCost)).toSet();
  ratingPeriods = jurisdiction.RatingPeriods;

    }
    
    // 'title' attribute on TitleBar at RatingCumulDetailsPanelSet.WC7Line.pcf: line 55, column 45
    function title_8 () : java.lang.String {
      return jurisdiction.DisplayName
    }
    
    // 'value' attribute on PanelIterator at RatingCumulDetailsPanelSet.WC7Line.pcf: line 61, column 61
    function value_15 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.toTypedArray()
    }
    
    // 'visible' attribute on PanelRef at RatingCumulDetailsPanelSet.WC7Line.pcf: line 53, column 42
    function visible_18 () : java.lang.Boolean {
      return stateCosts.HasElements
    }
    
    property get jurisdiction () : entity.WC7Jurisdiction {
      return getIteratedValue(1) as entity.WC7Jurisdiction
    }
    
    property get ratingPeriods () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return getVariableValue("ratingPeriods", 1) as java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>
    }
    
    property set ratingPeriods ($arg :  java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>) {
      setVariableValue("ratingPeriods", 1, $arg)
    }
    
    property get stateCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("stateCosts", 1) as java.util.Set<entity.WC7Cost>
    }
    
    property set stateCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("stateCosts", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingCumulDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingCumulDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.WC7Line.pcf: line 34, column 36
    function def_onEnter_3 (def :  pcf.RatingOverrideButtonDV) : void {
      def.onEnter(period, period.WC7Line, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.WC7Line.pcf: line 34, column 36
    function def_refreshVariables_4 (def :  pcf.RatingOverrideButtonDV) : void {
      def.refreshVariables(period, period.WC7Line, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 21, column 51
    function initialValue_0 () : java.util.Set<entity.WC7Cost> {
      return period.WC7Line.Costs.cast(WC7Cost)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 26, column 93
    function initialValue_1 () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>> {
      return lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.WC7Line.pcf: line 31, column 40
    function initialValue_2 () : entity.WC7Jurisdiction[] {
      return period.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -> juris.Jurisdiction)
    }
    
    // 'sortBy' attribute on IteratorSort at RatingCumulDetailsPanelSet.WC7Line.pcf: line 41, column 24
    function sortBy_5 (jurisdiction :  entity.WC7Jurisdiction) : java.lang.Object {
      return jurisdictions
    }
    
    // 'title' attribute on TitleBar (id=grandTotalTitle) at RatingCumulDetailsPanelSet.WC7Line.pcf: line 102, column 214
    function title_21 () : java.lang.String {
      return DisplayKey.get("Web.Quote.TotalCostLabel.Total2", gw.api.util.StringUtil.formatNumber(lineCosts.AmountSum(period.PreferredSettlementCurrency) as java.lang.Double, "currency"))
    }
    
    // 'value' attribute on PanelIterator at RatingCumulDetailsPanelSet.WC7Line.pcf: line 38, column 44
    function value_20 () : entity.WC7Jurisdiction[] {
      return jurisdictions
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get jurisdictions () : entity.WC7Jurisdiction[] {
      return getVariableValue("jurisdictions", 0) as entity.WC7Jurisdiction[]
    }
    
    property set jurisdictions ($arg :  entity.WC7Jurisdiction[]) {
      setVariableValue("jurisdictions", 0, $arg)
    }
    
    property get lineCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("lineCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set lineCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("lineCosts", 0, $arg)
    }
    
    property get partitionCosts () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>> {
      return getVariableValue("partitionCosts", 0) as java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>>
    }
    
    property set partitionCosts ($arg :  java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>>) {
      setVariableValue("partitionCosts", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    function standardPremLabel(splitPeriod : boolean, ratingPeriod : gw.lob.wc7.rating.WC7RatingPeriod) : String {
      if (splitPeriod) {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.SplitPeriod", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"),
          gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short"))
      } else {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.OnePeriod")
      }
    }
    
    function sortedDates(jurisdiction : WC7Jurisdiction) : java.util.Date[] {
      var rpsds = jurisdiction.getSortedRPSDs().toList().map(\ r -> r.StartDate)
      rpsds.add(period.PeriodStart)
      java.util.Collections.sort(rpsds)
      return rpsds.toTypedArray()
    }
    
    
  }
  
  
}