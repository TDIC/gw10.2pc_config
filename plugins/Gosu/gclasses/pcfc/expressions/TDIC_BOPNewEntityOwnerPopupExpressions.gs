package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/bop/TDIC_BOPNewEntityOwnerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPNewEntityOwnerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/bop/TDIC_BOPNewEntityOwnerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPNewEntityOwnerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (line :  BusinessOwnersLine, contactType :  ContactType) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at TDIC_BOPNewEntityOwnerPopup.pcf: line 62, column 62
    function action_11 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at TDIC_BOPNewEntityOwnerPopup.pcf: line 54, column 62
    function action_6 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(policyEntityOwner))
    }
    
    // 'beforeCommit' attribute on Popup (id=TDIC_BOPNewEntityOwnerPopup) at TDIC_BOPNewEntityOwnerPopup.pcf: line 13, column 117
    function beforeCommit_21 (pickedValue :  BOPPolicyEntityOwner_TDIC) : void {
      policyEntityOwner.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch); helper.validateAndUpdateStatusOfAddresses(contact)
    }
    
    // 'beforeValidate' attribute on Popup (id=TDIC_BOPNewEntityOwnerPopup) at TDIC_BOPNewEntityOwnerPopup.pcf: line 13, column 117
    function beforeValidate_22 (pickedValue :  BOPPolicyEntityOwner_TDIC) : void {
      displayMembershipCheckError = tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(policyEntityOwner.AccountContactRole.AccountContact.Contact, displayMembershipCheckError, policyEntityOwner.Branch.BaseState)
    }
    
    // 'def' attribute on PanelRef at TDIC_BOPNewEntityOwnerPopup.pcf: line 81, column 72
    function def_onEnter_19 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(policyEntityOwner, false)
    }
    
    // 'def' attribute on PanelRef at TDIC_BOPNewEntityOwnerPopup.pcf: line 81, column 72
    function def_refreshVariables_20 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(policyEntityOwner, false)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyEntityOwner.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPNewEntityOwnerPopup.pcf: line 25, column 41
    function initialValue_0 () : BOPPolicyEntityOwner_TDIC {
      return line.addNewBOPPolicyEntityOwnerOfContactType_TDIC(contactType)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPNewEntityOwnerPopup.pcf: line 29, column 25
    function initialValue_1 () : Contact[] {
      return line.BOPPolicyOwnerOfficer_TDIC.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPNewEntityOwnerPopup.pcf: line 33, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(policyEntityOwner.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPNewEntityOwnerPopup.pcf: line 37, column 76
    function initialValue_3 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPNewEntityOwnerPopup.pcf: line 41, column 30
    function initialValue_4 () : entity.Contact {
      return policyEntityOwner.AccountContactRole.AccountContact.Contact
    }
    
    // EditButtons at TDIC_BOPNewEntityOwnerPopup.pcf: line 57, column 72
    function label_9 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at TDIC_BOPNewEntityOwnerPopup.pcf: line 57, column 72
    function pickValue_7 () : BOPPolicyEntityOwner_TDIC {
      return policyEntityOwner
    }
    
    // 'title' attribute on Popup (id=TDIC_BOPNewEntityOwnerPopup) at TDIC_BOPNewEntityOwnerPopup.pcf: line 13, column 117
    static function title_23 (contactType :  ContactType, line :  BusinessOwnersLine) : java.lang.Object {
      return DisplayKey.get("Web.Contact.NewContact", BOPPolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function valueRange_15 () : java.lang.Object {
      return typekey.Jurisdiction.AllTypeKeys
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function valueRoot_14 () : java.lang.Object {
      return policyEntityOwner
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function value_12 () : typekey.Jurisdiction {
      return policyEntityOwner.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at TDIC_BOPNewEntityOwnerPopup.pcf: line 75, column 46
    function verifyValueRange_17 () : void {
      var __valueRangeArg = typekey.Jurisdiction.AllTypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at TDIC_BOPNewEntityOwnerPopup.pcf: line 54, column 62
    function visible_5 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at TDIC_BOPNewEntityOwnerPopup.pcf: line 57, column 72
    function visible_8 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.TDIC_BOPNewEntityOwnerPopup {
      return super.CurrentLocation as pcf.TDIC_BOPNewEntityOwnerPopup
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactType () : ContactType {
      return getVariableValue("contactType", 0) as ContactType
    }
    
    property set contactType ($arg :  ContactType) {
      setVariableValue("contactType", 0, $arg)
    }
    
    property get displayMembershipCheckError () : boolean {
      return getVariableValue("displayMembershipCheckError", 0) as java.lang.Boolean
    }
    
    property set displayMembershipCheckError ($arg :  boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get line () : BusinessOwnersLine {
      return getVariableValue("line", 0) as BusinessOwnersLine
    }
    
    property set line ($arg :  BusinessOwnersLine) {
      setVariableValue("line", 0, $arg)
    }
    
    property get policyEntityOwner () : BOPPolicyEntityOwner_TDIC {
      return getVariableValue("policyEntityOwner", 0) as BOPPolicyEntityOwner_TDIC
    }
    
    property set policyEntityOwner ($arg :  BOPPolicyEntityOwner_TDIC) {
      setVariableValue("policyEntityOwner", 0, $arg)
    }
    
    
  }
  
  
}