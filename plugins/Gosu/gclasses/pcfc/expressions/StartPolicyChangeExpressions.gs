package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.plugin.policyperiod.IEffectiveTimePlugin
uses gw.plugin.Plugins
uses gw.api.util.DateUtil
@javax.annotation.Generated("config/web/pcf/job/policychange/StartPolicyChange.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class StartPolicyChangeExpressions {
  @javax.annotation.Generated("config/web/pcf/job/policychange/StartPolicyChange.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends StartPolicyChangeExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 154, column 64
    function valueRoot_27 () : java.lang.Object {
      return policyChangeReason
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 154, column 64
    function value_26 () : typekey.ChangeReasonType_TDIC {
      return policyChangeReason.ChangeReason
    }
    
    property get policyChangeReason () : ChangeReason_TDIC {
      return getIteratedValue(1) as ChangeReason_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/policychange/StartPolicyChange.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends StartPolicyChangeExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 120, column 115
    function value_20 () : typekey.ChangeReasonType_TDIC {
      return policyChangeReason
    }
    
    // 'valueVisible' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 120, column 115
    function visible_19 () : java.lang.Boolean {
      return (!job.ChangeReasons*.ChangeReason.contains(policyChangeReason))? true : false
    }
    
    property get policyChangeReason () : typekey.ChangeReasonType_TDIC {
      return getIteratedValue(1) as typekey.ChangeReasonType_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/policychange/StartPolicyChange.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class StartPolicyChangeExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewPolicyChange) at StartPolicyChange.pcf: line 53, column 25
    function action_4 () : void {
      gw.web.job.policychange.StartPolicyChangeUIHelper.verifyPolicyChangeReason_TDIC(job,policyPeriod);gw.web.job.policychange.StartPolicyChangeUIHelper.validatePolicyChangeOnFlatCancelledPolicy_TDIC(policyPeriod, effectiveDate, job);if (gw.web.job.policychange.StartPolicyChangeUIHelper.refreshAndStartJobAndCommit(job, policyPeriod, effectiveDate, CurrentLocation)) { PolicyChangeWizard.go(job, job.LatestPeriod) }
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at StartPolicyChange.pcf: line 57, column 62
    function action_6 () : void {
      CurrentLocation.cancel()
    }
    
    // 'afterCancel' attribute on Page (id=StartPolicyChange) at StartPolicyChange.pcf: line 14, column 76
    function afterCancel_36 () : void {
      PolicyFileForward.go(policyPeriod.PolicyNumber)
    }
    
    // 'afterCancel' attribute on Page (id=StartPolicyChange) at StartPolicyChange.pcf: line 14, column 76
    function afterCancel_dest_37 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(policyPeriod.PolicyNumber)
    }
    
    // 'available' attribute on ToolbarButton (id=NewPolicyChange) at StartPolicyChange.pcf: line 53, column 25
    function available_3 () : java.lang.Boolean {
      return policyPeriod.Policy.canStartPolicyChange(policyPeriod.EditEffectiveDate) == null
    }
    
    // 'canVisit' attribute on Page (id=StartPolicyChange) at StartPolicyChange.pcf: line 14, column 76
    static function canVisit_38 (policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.view(policyPeriod) and perm.PolicyChange.create
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AddChangeReason) at StartPolicyChange.pcf: line 98, column 101
    function checkedRowAction_17 (policyChangeReason :  typekey.ChangeReasonType_TDIC, CheckedValue :  typekey.ChangeReasonType_TDIC) : void {
      var changeReason = new ChangeReason_TDIC(); changeReason.ChangeReason = CheckedValue; job.addToChangeReasons(changeReason); gw.web.job.policychange.StartPolicyChangeUIHelper.checkChangeReason_TDIC(changeReason); effectiveDate = gw.web.job.policychange.StartPolicyChangeUIHelper.getEffDate_TDIC(policyPeriod, changedEffDate_TDIC, changeReason); checkChangeReason_TDIC(CheckedValue); policyPeriod.refresh()//return CheckedValue;
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=RemoveChangeReason) at StartPolicyChange.pcf: line 134, column 104
    function checkedRowAction_24 (policyChangeReason :  ChangeReason_TDIC, CheckedValue :  ChangeReason_TDIC) : void {
      if(job.ChangeReasons.hasMatch(\reason -> reason == CheckedValue))isEffDateEditable=true;job.removeFromChangeReasons(CheckedValue); gw.web.job.policychange.StartPolicyChangeUIHelper.checkRemovedReason_TDIC(CheckedValue); effectiveDate = gw.web.job.policychange.StartPolicyChangeUIHelper.getEffDate_TDIC(policyPeriod, changedEffDate_TDIC, CheckedValue);  policyPeriod.refresh()//return CheckedValue;  
    }
    
    // 'confirmMessage' attribute on ToolbarButton (id=NewPolicyChange) at StartPolicyChange.pcf: line 53, column 25
    function confirmMessage_5 () : java.lang.String {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.getConfirmMessage(inForcePeriod, effectiveDate)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at StartPolicyChange.pcf: line 77, column 35
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      effectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=CommentBox_Input) at StartPolicyChange.pcf: line 172, column 42
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      job.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at StartPolicyChange.pcf: line 77, column 35
    function editable_12 () : java.lang.Boolean {
      return isEffDateEditable
    }
    
    // 'initialValue' attribute on Variable at StartPolicyChange.pcf: line 23, column 30
    function initialValue_0 () : java.util.Date {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.getEffDate_TDIC(policyPeriod, changedEffDate_TDIC, null)
    }
    
    // 'initialValue' attribute on Variable at StartPolicyChange.pcf: line 27, column 28
    function initialValue_1 () : PolicyChange {
      return new PolicyChange()
    }
    
    // 'initialValue' attribute on Variable at StartPolicyChange.pcf: line 32, column 28
    function initialValue_2 () : PolicyPeriod {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.getInForcePeriodWithBasedOn(policyPeriod, effectiveDate)
    }
    
    // 'label' attribute on Verbatim (id=WarningMessage) at StartPolicyChange.pcf: line 68, column 25
    function label_10 () : java.lang.String {
      return DisplayKey.get("Web.PolicyChange.StartPolicyChange.Warning", gw.web.job.policychange.StartPolicyChangeUIHelper.getPolicyChangeWarningMessage(inForcePeriod, effectiveDate))
    }
    
    // 'label' attribute on Verbatim (id=ErrorMessage) at StartPolicyChange.pcf: line 63, column 25
    function label_8 () : java.lang.String {
      return DisplayKey.get("Web.PolicyChange.StartPolicyChange.Error", policyPeriod.Policy.canStartPolicyChange(effectiveDate))
    }
    
    // 'onChange' attribute on PostOnChange at StartPolicyChange.pcf: line 79, column 86
    function onChange_11 () : void {
      addEffTimeToEffDate(); changedEffDate_TDIC = effectiveDate
    }
    
    // 'parent' attribute on Page (id=StartPolicyChange) at StartPolicyChange.pcf: line 14, column 76
    static function parent_39 (policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod, policyPeriod.EditEffectiveDate)
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 120, column 115
    function sortValue_18 (policyChangeReason :  typekey.ChangeReasonType_TDIC) : java.lang.Object {
      return policyChangeReason
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at StartPolicyChange.pcf: line 154, column 64
    function sortValue_25 (policyChangeReason :  ChangeReason_TDIC) : java.lang.Object {
      return policyChangeReason.ChangeReason
    }
    
    // 'validationExpression' attribute on TextInput (id=CommentBox_Input) at StartPolicyChange.pcf: line 172, column 42
    function validationExpression_30 () : java.lang.Object {
      return job.hasDescriptionContainsSpecialChars()
    }
    
    // 'value' attribute on TextInput (id=CommentBox_Input) at StartPolicyChange.pcf: line 172, column 42
    function valueRoot_33 () : java.lang.Object {
      return job
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at StartPolicyChange.pcf: line 77, column 35
    function value_13 () : java.util.Date {
      return effectiveDate
    }
    
    // 'value' attribute on RowIterator (id=policyChangeReasonIterator) at StartPolicyChange.pcf: line 110, column 61
    function value_23 () : typekey.ChangeReasonType_TDIC[] {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.getChangeReasons_TDIC(policyPeriod).toTypedArray()//ChangeReasonType_TDIC.getTypeKeys(false).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=policyChangeReasonIterator) at StartPolicyChange.pcf: line 147, column 49
    function value_29 () : ChangeReason_TDIC[] {
      return job.ChangeReasons
    }
    
    // 'value' attribute on TextInput (id=CommentBox_Input) at StartPolicyChange.pcf: line 172, column 42
    function value_31 () : java.lang.String {
      return job.Description
    }
    
    // 'visible' attribute on Verbatim (id=ErrorMessage) at StartPolicyChange.pcf: line 63, column 25
    function visible_7 () : java.lang.Boolean {
      return policyPeriod.Policy.canStartPolicyChange(effectiveDate) != null  and isChanged
    }
    
    // 'visible' attribute on Verbatim (id=WarningMessage) at StartPolicyChange.pcf: line 68, column 25
    function visible_9 () : java.lang.Boolean {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.getPolicyChangeWarningMessage(inForcePeriod, effectiveDate) != null
    }
    
    override property get CurrentLocation () : pcf.StartPolicyChange {
      return super.CurrentLocation as pcf.StartPolicyChange
    }
    
    property get changedEffDate_TDIC () : Date {
      return getVariableValue("changedEffDate_TDIC", 0) as Date
    }
    
    property set changedEffDate_TDIC ($arg :  Date) {
      setVariableValue("changedEffDate_TDIC", 0, $arg)
    }
    
    property get effectiveDate () : java.util.Date {
      return getVariableValue("effectiveDate", 0) as java.util.Date
    }
    
    property set effectiveDate ($arg :  java.util.Date) {
      setVariableValue("effectiveDate", 0, $arg)
    }
    
    property get inForcePeriod () : PolicyPeriod {
      return getVariableValue("inForcePeriod", 0) as PolicyPeriod
    }
    
    property set inForcePeriod ($arg :  PolicyPeriod) {
      setVariableValue("inForcePeriod", 0, $arg)
    }
    
    property get isChanged () : boolean {
      return getVariableValue("isChanged", 0) as java.lang.Boolean
    }
    
    property set isChanged ($arg :  boolean) {
      setVariableValue("isChanged", 0, $arg)
    }
    
    property get isEffDateEditable () : Boolean {
      return getVariableValue("isEffDateEditable", 0) as Boolean
    }
    
    property set isEffDateEditable ($arg :  Boolean) {
      setVariableValue("isEffDateEditable", 0, $arg)
    }
    
    property get job () : PolicyChange {
      return getVariableValue("job", 0) as PolicyChange
    }
    
    property set job ($arg :  PolicyChange) {
      setVariableValue("job", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
    var ereReasons_TDIC = {"ereCyber", "ereCyberNR", "ereCyberRescinded", "erePLOffer", "erePLOfferNR", "ereCyberNA", "ereCyberOffer", "erePLOfferNA", "erePLRescinded", "extendedreportingperiodendorsementplcm", "erePLDocGen", "ereCybDocGen"}
    
    var regen_TDIC = {"regenerateDeclaration"}
    
    function addEffTimeToEffDate() {
      var effectiveTimePlugin = Plugins.get(IEffectiveTimePlugin);
      var effectiveTime = effectiveTimePlugin.getPolicyChangeEffectiveTime(effectiveDate, policyPeriod, job);
      effectiveDate = DateUtil.mergeDateAndTime(effectiveDate, effectiveTime);
      if(effectiveDate == null){isChanged = true}
    }
    
    function checkChangeReason_TDIC(changeReason: ChangeReasonType_TDIC) {
      if (ereReasons_TDIC.contains(changeReason.Code) or regen_TDIC.contains(changeReason.Code)) {
        isEffDateEditable = false
      }
    
    }    
    
    
  }
  
  
}