package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7JurisdictionModifierInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7JurisdictionModifierInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function actionAvailable_59 () : java.lang.Boolean {
      return policyPeriod.CanViewModifiers and modifier.Pattern.CodeIdentifier == "WC7ScheduleMod"
    }
    
    // 'action' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function action_56 () : void {
      WC7ScheduleCreditPopup.push(modifier, policyPeriod.OpenForEdit)
    }
    
    // 'action' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function action_dest_57 () : pcf.api.Destination {
      return pcf.WC7ScheduleCreditPopup.createDestination(modifier, policyPeriod.OpenForEdit)
    }
    
    // 'available' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function available_91 () : java.lang.Boolean {
      return uiHelper.typekeyModifierAvailable(modifier, policyPeriod)
    }
    
    // 'value' attribute on DateInput (id=ModifierInput_Date_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 151, column 66
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.DateModifier = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.RateWithinLimits = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 117, column 214
    function defaultSetter_73 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.BooleanModifier = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.TypeKeyModifier = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'disablePostOnEnter' attribute on PostOnChange at WC7JurisdictionModifierInputSet.default.pcf: line 144, column 87
    function disablePostOnEnter_90 () : java.lang.Boolean {
      return uiHelper.typekeyModifierDisablePostOnChange(modifier)
    }
    
    // 'editable' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function editable_54 () : java.lang.Boolean {
      return modifier.Pattern.CodeIdentifier != "WC7ScheduleMod"
    }
    
    // 'editable' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 117, column 214
    function editable_69 () : java.lang.Boolean {
      return canEditBooleanModifier(modifier)
    }
    
    // 'label' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function label_58 () : java.lang.Object {
      return modifier.Pattern.Name
    }
    
    // 'label' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 117, column 214
    function label_71 () : java.lang.Object {
      return stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction)
    }
    
    // 'onChange' attribute on PostOnChange at WC7JurisdictionModifierInputSet.default.pcf: line 119, column 87
    function onChange_68 () : void {
      wc7JurisdictionSliced.syncModifiers(); onChange_TDIC(modifier);
    }
    
    // 'onChange' attribute on PostOnChange at WC7JurisdictionModifierInputSet.default.pcf: line 131, column 87
    function onChange_79 () : void {
      wc7JurisdictionSliced.syncModifiers(); onChange_TDIC(modifier);
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function requestValidationExpression_60 (VALUE :  java.math.BigDecimal) : java.lang.Object {
      return modifier.getModifierValidation(VALUE as String)
    }
    
    // 'required' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function required_94 () : java.lang.Boolean {
      return stateConfig.isTypeKeyModifierRequired(modifier)
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function valueRange_98 () : java.lang.Object {
      return uiHelper.filterTypekeyModifier(modifier)
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 100, column 39
    function valueRoot_52 () : java.lang.Object {
      return modifier.Pattern
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function valueRoot_63 () : java.lang.Object {
      return modifier
    }
    
    // 'value' attribute on DateInput (id=ModifierInput_Date_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 151, column 66
    function value_107 () : java.util.Date {
      return modifier.DateModifier
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 100, column 39
    function value_51 () : java.lang.String {
      return modifier.Pattern.Name
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function value_61 () : java.math.BigDecimal {
      return modifier.RateWithinLimits
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 117, column 214
    function value_72 () : java.lang.Boolean {
      return modifier.BooleanModifier
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function value_95 () : java.lang.String {
      return modifier.TypeKeyModifier
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function verifyValueRangeIsAllowedType_99 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function verifyValueRangeIsAllowedType_99 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function verifyValueRange_100 () : void {
      var __valueRangeArg = uiHelper.filterTypekeyModifier(modifier)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_99(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=ModifierInput_Date_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 151, column 66
    function visible_105 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_DATE
    }
    
    // 'visible' attribute on TextInput (id=ModifierInput_Rate_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 110, column 66
    function visible_55 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_RATE
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ModifierInput_Boolean_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 117, column 214
    function visible_70 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_BOOLEAN && !stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction).equalsIgnoreCase("Multiline Discount")
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ModifierInput_Boolean1_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 129, column 213
    function visible_81 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_BOOLEAN && stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction).equalsIgnoreCase("Multiline Discount")
    }
    
    // 'visible' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 142, column 68
    function visible_92 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_TYPEKEY
    }
    
    property get modifier () : entity.WC7Modifier {
      return getIteratedValue(1) as entity.WC7Modifier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7JurisdictionModifierInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function actionAvailable_14 () : java.lang.Boolean {
      return rateModifier.Pattern.CodeIdentifier == "WC7ScheduleMod"
    }
    
    // 'action' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function action_11 () : void {
      WC7ScheduleCreditPopup.push(rateModifier, policyPeriod.OpenForEdit)
    }
    
    // 'action' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function action_dest_12 () : pcf.api.Destination {
      return pcf.WC7ScheduleCreditPopup.createDestination(rateModifier, policyPeriod.OpenForEdit)
    }
    
    // 'allowToggle' attribute on InputGroup (id=EligibilityInputGroup) at WC7JurisdictionModifierInputSet.default.pcf: line 51, column 49
    function available_42 () : java.lang.Boolean {
      return uiHelper.rateModifierAvailable(rateModifier, policyPeriod)
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateModifier.RateWithinLimits = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ValueFinal_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 70, column 72
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateModifier.ValueFinal = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateModifier.ExperienceModifierStatus = (__VALUE_TO_SET as typekey.WC7ExpModStatus)
    }
    
    // 'disablePostOnEnter' attribute on PostOnChange at WC7JurisdictionModifierInputSet.default.pcf: line 73, column 236
    function disablePostOnEnter_5 () : java.lang.Boolean {
      return uiHelper.rateModifierDisablePostOnChange(rateModifier)
    }
    
    // 'editable' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function editable_10 () : java.lang.Boolean {
      return rateModifier.Pattern.CodeIdentifier != "WC7ScheduleMod"
    }
    
    // 'editable' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function editable_29 () : java.lang.Boolean {
      return uiHelper.filterTypeKeyExperienceModifierStatus(rateModifier).Count > 1
    }
    
    // 'label' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function label_13 () : java.lang.Object {
      return stateConfig.getModifierName(rateModifier.Pattern.Name)
    }
    
    // 'label' attribute on InputGroup (id=EligibilityInputGroup) at WC7JurisdictionModifierInputSet.default.pcf: line 51, column 49
    function label_43 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Modifiers.Eligibility", stateConfig.getModifierName(rateModifier.Pattern))
    }
    
    // 'onChange' attribute on PostOnChange at WC7JurisdictionModifierInputSet.default.pcf: line 73, column 236
    function onChange_6 () : void {
      uiHelper.updateModifierDependencies(rateModifier); policyPeriod.WC7Line.WC7Jurisdictions.firstWhere( \ slicedJurisdiction -> slicedJurisdiction.FixedId == rateModifier.WC7Jurisdiction.FixedId).syncModifiers();
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function requestValidationExpression_15 (VALUE :  java.math.BigDecimal) : java.lang.Object {
      return rateModifier.getModifierValidation(VALUE as String)
    }
    
    // 'onToggle' attribute on InputGroup (id=EligibilityInputGroup) at WC7JurisdictionModifierInputSet.default.pcf: line 51, column 49
    function setter_44 (VALUE :  java.lang.Boolean) : void {
      toggleEligible(rateModifier)
    }
    
    // 'valueRange' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function valueRange_34 () : java.lang.Object {
      return uiHelper.filterTypeKeyExperienceModifierStatus(rateModifier)
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function valueRoot_18 () : java.lang.Object {
      return rateModifier
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 55, column 41
    function valueRoot_8 () : java.lang.Object {
      return rateModifier.Pattern
    }
    
    // 'value' attribute on TextInput (id=ModifierInput_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 64, column 45
    function value_16 () : java.math.BigDecimal {
      return rateModifier.RateWithinLimits
    }
    
    // 'value' attribute on BooleanRadioInput (id=ValueFinal_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 70, column 72
    function value_23 () : java.lang.Boolean {
      return rateModifier.ValueFinal
    }
    
    // 'value' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function value_31 () : typekey.WC7ExpModStatus {
      return rateModifier.ExperienceModifierStatus
    }
    
    // 'value' attribute on HiddenInput (id=ModifierName_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 55, column 41
    function value_7 () : java.lang.String {
      return rateModifier.Pattern.Name
    }
    
    // 'valueRange' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function verifyValueRangeIsAllowedType_35 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function verifyValueRangeIsAllowedType_35 ($$arg :  typekey.WC7ExpModStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ExpModStatus_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 82, column 72
    function verifyValueRange_36 () : void {
      var __valueRangeArg = uiHelper.filterTypeKeyExperienceModifierStatus(rateModifier)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_35(__valueRangeArg)
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ValueFinal_Input) at WC7JurisdictionModifierInputSet.default.pcf: line 70, column 72
    function visible_22 () : java.lang.Boolean {
      return rateModifier.PatternCode == "WC7ExpMod"
    }
    
    // 'childrenVisible' attribute on InputGroup (id=EligibilityInputGroup) at WC7JurisdictionModifierInputSet.default.pcf: line 51, column 49
    function visible_41 () : java.lang.Boolean {
      return rateModifier.Eligible
    }
    
    property get rateModifier () : entity.WC7Modifier {
      return getIteratedValue(1) as entity.WC7Modifier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7JurisdictionModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7JurisdictionModifierInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on InputSet (id=WC7JurisdictionModifierInputSet) at WC7JurisdictionModifierInputSet.default.pcf: line 8, column 20
    function editable_114 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at WC7JurisdictionModifierInputSet.default.pcf: line 24, column 26
    function initialValue_0 () : Modifier[] {
      return modifiers?.where( \ mod -> mod.DataType != TC_RATE or mod.ScheduleRate)?.toTypedArray()
    }
    
    // 'initialValue' attribute on Variable at WC7JurisdictionModifierInputSet.default.pcf: line 28, column 51
    function initialValue_1 () : gw.pcf.WC7ModifiersInputSetUIHelper {
      return new gw.pcf.WC7ModifiersInputSetUIHelper()
    }
    
    // 'initialValue' attribute on Variable at WC7JurisdictionModifierInputSet.default.pcf: line 33, column 38
    function initialValue_2 () : entity.WC7Jurisdiction {
      return (modifiers?.first()?.OwningModifiable as WC7Jurisdiction)?.getSlice(policyPeriod.EditEffectiveDate)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7JurisdictionModifierInputSet.default.pcf: line 41, column 24
    function sortBy_3 (rateModifier :  entity.WC7Modifier) : java.lang.Object {
      return rateModifier.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at WC7JurisdictionModifierInputSet.default.pcf: line 45, column 24
    function sortBy_4 (rateModifier :  entity.WC7Modifier) : java.lang.Object {
      return rateModifier.Pattern.Name
    }
    
    // 'sortBy' attribute on IteratorSort at WC7JurisdictionModifierInputSet.default.pcf: line 92, column 24
    function sortBy_49 (modifier :  entity.WC7Modifier) : java.lang.Object {
      return modifier.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at WC7JurisdictionModifierInputSet.default.pcf: line 96, column 24
    function sortBy_50 (modifier :  entity.WC7Modifier) : java.lang.Object {
      return modifier.Pattern.Name
    }
    
    // 'value' attribute on InputIterator (id=ModIterator) at WC7JurisdictionModifierInputSet.default.pcf: line 89, column 40
    function value_113 () : entity.WC7Modifier[] {
      return modifiers.where( \ mod -> mod.DataType != TC_RATE or mod.ScheduleRate).toTypedArray()
    }
    
    // 'value' attribute on InputIterator (id=ModIteratorEligible) at WC7JurisdictionModifierInputSet.default.pcf: line 38, column 40
    function value_48 () : entity.WC7Modifier[] {
      return modifiers.where( \ mod -> mod.DataType == TC_RATE and not mod.ScheduleRate).toTypedArray()
    }
    
    property get modifiers () : java.util.List<WC7Modifier> {
      return getRequireValue("modifiers", 0) as java.util.List<WC7Modifier>
    }
    
    property set modifiers ($arg :  java.util.List<WC7Modifier>) {
      setRequireValue("modifiers", 0, $arg)
    }
    
    property get nonRateOrSchedRateModifiers () : Modifier[] {
      return getVariableValue("nonRateOrSchedRateModifiers", 0) as Modifier[]
    }
    
    property set nonRateOrSchedRateModifiers ($arg :  Modifier[]) {
      setVariableValue("nonRateOrSchedRateModifiers", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getRequireValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setRequireValue("stateConfig", 0, $arg)
    }
    
    property get uiHelper () : gw.pcf.WC7ModifiersInputSetUIHelper {
      return getVariableValue("uiHelper", 0) as gw.pcf.WC7ModifiersInputSetUIHelper
    }
    
    property set uiHelper ($arg :  gw.pcf.WC7ModifiersInputSetUIHelper) {
      setVariableValue("uiHelper", 0, $arg)
    }
    
    property get wc7JurisdictionSliced () : entity.WC7Jurisdiction {
      return getVariableValue("wc7JurisdictionSliced", 0) as entity.WC7Jurisdiction
    }
    
    property set wc7JurisdictionSliced ($arg :  entity.WC7Jurisdiction) {
      setVariableValue("wc7JurisdictionSliced", 0, $arg)
    }
    
    function toggleEligible(mod : Modifier) {
      if (mod.Eligible) {
        mod.RateModifier = null
        mod.Eligible = false
      } else {
        mod.Eligible = true
      }
    }
    
    function onChange_TDIC (modifier : WC7Modifier) : void {
      if (policyPeriod.Job typeis Submission and policyPeriod.Job.QuickMode) {
        if (modifier.PatternCode == "WC7TDICMultiLineDiscount") {
          policyPeriod.MultiLineDiscountADANums_TDIC = "QuickQuote";
        }
      }
    }
    
    function canEditBooleanModifier(modifier : WC7Modifier) : boolean {
      if(modifier.PatternCode == "WC7CertifiedSafety"){
        return false//perm.System.editsafetygroup_TDIC //GPC-3531 - Safety Program Credit is not editable
      } else if(modifier.PatternCode == "WC7TDICMultiLineDiscount") {
        if(policyPeriod.Job.Subtype == TC_POLICYCHANGE and modifier.BasedOn != null and modifier.BasedOn.BooleanModifier) {
          return false
        }
      }
      return true
    }
    
    
  }
  
  
}