package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/ClaimsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/ClaimsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ClaimsLV.pcf: line 32, column 33
    function filter_0 () : gw.api.filters.IFilter {
      return filterSet.getAllFilter()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ClaimsLV.pcf: line 35, column 74
    function filter_1 () : gw.api.filters.IFilter {
      return filterSet.getNoPolicyInforceFilter()
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ClaimsLV.pcf: line 37, column 81
    function filters_2 () : gw.api.filters.IFilter[] {
      return filterSet.getClaimPolicyPeriodFilters().getFilterOptions()
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at ClaimsLV.pcf: line 74, column 66
    function sortValue_10 (claim :  entity.Claim) : java.lang.Object {
      return claim.PolicyPeriod.Policy.Product.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ClaimsLV.pcf: line 80, column 45
    function sortValue_11 (claim :  entity.Claim) : java.lang.Object {
      return claim.TotalIncurred_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidIndemnity_Cell) at ClaimsLV.pcf: line 86, column 47
    function sortValue_12 (claim :  entity.Claim) : java.lang.Object {
      return claim.PaidIndemntyAmt_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidExpense_Cell) at ClaimsLV.pcf: line 91, column 47
    function sortValue_13 (claim :  entity.Claim) : java.lang.Object {
      return claim.PaidExpenseAmnt_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalPaidAmt_Cell) at ClaimsLV.pcf: line 96, column 42
    function sortValue_14 (claim :  entity.Claim) : java.lang.Object {
      return claim.AmountPaid_TDIC
    }
    
    // 'value' attribute on DateCell (id=ReportDate_Cell) at ClaimsLV.pcf: line 100, column 44
    function sortValue_15 (claim :  entity.Claim) : java.lang.Object {
      return claim.ReportedDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=LegacyPolicyNumber_Cell) at ClaimsLV.pcf: line 104, column 63
    function sortValue_16 (claim :  entity.Claim) : java.lang.Object {
      return claim.PolicyPeriod.LegacyPolicyNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function sortValue_3 (claim :  entity.Claim) : java.lang.Object {
      return claim.PolicyInForce ? claim.PolicyPeriod.getPolicyPeriodDateDisplay() : DisplayKey.get("Web.Claims.NoPolicyPeriod")
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ClaimsLV.pcf: line 50, column 35
    function sortValue_4 (claim :  entity.Claim) : java.lang.Object {
      return claim.LossDate
    }
    
    // 'value' attribute on TextCell (id=ClaimNumber_Cell) at ClaimsLV.pcf: line 54, column 38
    function sortValue_5 (claim :  entity.Claim) : java.lang.Object {
      return claim.ClaimNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at ClaimsLV.pcf: line 58, column 33
    function sortValue_6 (claim :  entity.Claim) : java.lang.Object {
      return claim.Status
    }
    
    // 'value' attribute on TextCell (id=ClaimsAdjuster_Cell) at ClaimsLV.pcf: line 62, column 44
    function sortValue_7 (claim :  entity.Claim) : java.lang.Object {
      return claim.AdjusterName_TDIC
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at ClaimsLV.pcf: line 66, column 43
    function sortValue_8 (claim :  entity.Claim) : java.lang.Object {
      return claim.InsuredName_TDIC
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ClaimsLV.pcf: line 70, column 44
    function sortValue_9 (claim :  entity.Claim) : java.lang.Object {
      return claim.PolicyNumber_TDIC
    }
    
    // 'footerSumValue' attribute on RowIterator at ClaimsLV.pcf: line 80, column 45
    function sumValue_17 (claim :  entity.Claim) : java.lang.Object {
      return claim.TotalIncurred_TDIC == null ? null : claim.TotalIncurred_TDIC?.convertAmount(preferredCurrency)
    }
    
    // 'footerSumValue' attribute on RowIterator at ClaimsLV.pcf: line 86, column 47
    function sumValue_18 (claim :  entity.Claim) : java.lang.Object {
      return claim.PaidIndemntyAmt_TDIC == null ? null : claim.PaidIndemntyAmt_TDIC?.convertAmount(preferredCurrency)
    }
    
    // 'footerSumValue' attribute on RowIterator at ClaimsLV.pcf: line 91, column 47
    function sumValue_19 (claim :  entity.Claim) : java.lang.Object {
      return claim.PaidExpenseAmnt_TDIC == null ? null : claim.PaidExpenseAmnt_TDIC?.convertAmount(preferredCurrency)
    }
    
    // 'footerSumValue' attribute on RowIterator at ClaimsLV.pcf: line 96, column 42
    function sumValue_20 (claim :  entity.Claim) : java.lang.Object {
      return claim.AmountPaid_TDIC == null ? null : claim.AmountPaid_TDIC?.convertAmount(preferredCurrency)
    }
    
    // 'value' attribute on RowIterator at ClaimsLV.pcf: line 24, column 34
    function value_65 () : entity.Claim[] {
      return claims
    }
    
    property get claims () : Claim[] {
      return getRequireValue("claims", 0) as Claim[]
    }
    
    property set claims ($arg :  Claim[]) {
      setRequireValue("claims", 0, $arg)
    }
    
    property get filterSet () : gw.losshistory.ClaimPolicyPeriodFilterSet {
      return getRequireValue("filterSet", 0) as gw.losshistory.ClaimPolicyPeriodFilterSet
    }
    
    property set filterSet ($arg :  gw.losshistory.ClaimPolicyPeriodFilterSet) {
      setRequireValue("filterSet", 0, $arg)
    }
    
    property get preferredCurrency () : Currency {
      return getRequireValue("preferredCurrency", 0) as Currency
    }
    
    property set preferredCurrency ($arg :  Currency) {
      setRequireValue("preferredCurrency", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/ClaimsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ClaimsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function fontColor_21 () : java.lang.Object {
      return claim.PolicyInForce ? null : gw.api.web.color.GWColor.THEME_ALERT_ERROR
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ClaimsLV.pcf: line 50, column 35
    function valueRoot_27 () : java.lang.Object {
      return claim
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at ClaimsLV.pcf: line 74, column 66
    function valueRoot_45 () : java.lang.Object {
      return claim.PolicyPeriod.Policy.Product
    }
    
    // 'value' attribute on TextCell (id=LegacyPolicyNumber_Cell) at ClaimsLV.pcf: line 104, column 63
    function valueRoot_63 () : java.lang.Object {
      return claim.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function value_22 () : java.lang.String {
      return claim.PolicyInForce ? claim.PolicyPeriod.getPolicyPeriodDateDisplay() : DisplayKey.get("Web.Claims.NoPolicyPeriod")
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ClaimsLV.pcf: line 50, column 35
    function value_26 () : java.util.Date {
      return claim.LossDate
    }
    
    // 'value' attribute on TextCell (id=ClaimNumber_Cell) at ClaimsLV.pcf: line 54, column 38
    function value_29 () : java.lang.String {
      return claim.ClaimNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at ClaimsLV.pcf: line 58, column 33
    function value_32 () : java.lang.String {
      return claim.Status
    }
    
    // 'value' attribute on TextCell (id=ClaimsAdjuster_Cell) at ClaimsLV.pcf: line 62, column 44
    function value_35 () : java.lang.String {
      return claim.AdjusterName_TDIC
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at ClaimsLV.pcf: line 66, column 43
    function value_38 () : java.lang.String {
      return claim.InsuredName_TDIC
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ClaimsLV.pcf: line 70, column 44
    function value_41 () : java.lang.String {
      return claim.PolicyNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at ClaimsLV.pcf: line 74, column 66
    function value_44 () : java.lang.String {
      return claim.PolicyPeriod.Policy.Product.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ClaimsLV.pcf: line 80, column 45
    function value_47 () : gw.pl.currency.MonetaryAmount {
      return claim.TotalIncurred_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidIndemnity_Cell) at ClaimsLV.pcf: line 86, column 47
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return claim.PaidIndemntyAmt_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidExpense_Cell) at ClaimsLV.pcf: line 91, column 47
    function value_53 () : gw.pl.currency.MonetaryAmount {
      return claim.PaidExpenseAmnt_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalPaidAmt_Cell) at ClaimsLV.pcf: line 96, column 42
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return claim.AmountPaid_TDIC
    }
    
    // 'value' attribute on DateCell (id=ReportDate_Cell) at ClaimsLV.pcf: line 100, column 44
    function value_59 () : java.util.Date {
      return claim.ReportedDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=LegacyPolicyNumber_Cell) at ClaimsLV.pcf: line 104, column 63
    function value_62 () : java.lang.String {
      return claim.PolicyPeriod.LegacyPolicyNumber_TDIC
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function verifyFontColorIsAllowedType_23 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function verifyFontColorIsAllowedType_23 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyPeriod_Cell) at ClaimsLV.pcf: line 44, column 146
    function verifyFontColor_24 () : void {
      var __fontColorArg = claim.PolicyInForce ? null : gw.api.web.color.GWColor.THEME_ALERT_ERROR
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_23(__fontColorArg)
    }
    
    property get claim () : entity.Claim {
      return getIteratedValue(1) as entity.Claim
    }
    
    
  }
  
  
}