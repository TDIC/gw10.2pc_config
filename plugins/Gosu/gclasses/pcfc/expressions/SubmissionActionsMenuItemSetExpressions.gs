package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/submgr/SubmissionActionsMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubmissionActionsMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/account/submgr/SubmissionActionsMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubmissionActionsMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=Withdraw) at SubmissionActionsMenuItemSet.pcf: line 17, column 125
    function action_1 () : void {
      TDIC_WithDrawReasonPopup.push(submission, policyPeriod, null)
    }
    
    // 'action' attribute on MenuItem (id=Decline) at SubmissionActionsMenuItemSet.pcf: line 22, column 124
    function action_4 () : void {
      DeclineReasonPopup.push(submission, policyPeriod, null)
    }
    
    // 'action' attribute on MenuItem (id=NotTakenJob) at SubmissionActionsMenuItemSet.pcf: line 27, column 139
    function action_7 () : void {
      NotTakenReasonPopup.push(submission, policyPeriod, null)
    }
    
    // 'action' attribute on MenuItem (id=Withdraw) at SubmissionActionsMenuItemSet.pcf: line 17, column 125
    function action_dest_2 () : pcf.api.Destination {
      return pcf.TDIC_WithDrawReasonPopup.createDestination(submission, policyPeriod, null)
    }
    
    // 'action' attribute on MenuItem (id=Decline) at SubmissionActionsMenuItemSet.pcf: line 22, column 124
    function action_dest_5 () : pcf.api.Destination {
      return pcf.DeclineReasonPopup.createDestination(submission, policyPeriod, null)
    }
    
    // 'action' attribute on MenuItem (id=NotTakenJob) at SubmissionActionsMenuItemSet.pcf: line 27, column 139
    function action_dest_8 () : pcf.api.Destination {
      return pcf.NotTakenReasonPopup.createDestination(submission, policyPeriod, null)
    }
    
    // 'visible' attribute on MenuItem (id=Withdraw) at SubmissionActionsMenuItemSet.pcf: line 17, column 125
    function visible_0 () : java.lang.Boolean {
      return policyPeriod.SubmissionProcess.canWithdrawJob().Okay and policyPeriod.SubmissionProcess.isFullQuote_TDIC()
    }
    
    // 'visible' attribute on MenuItem (id=Decline) at SubmissionActionsMenuItemSet.pcf: line 22, column 124
    function visible_3 () : java.lang.Boolean {
      return policyPeriod.SubmissionProcess.canDeclineJob().Okay and policyPeriod.SubmissionProcess.isFullQuote_TDIC()
    }
    
    // 'visible' attribute on MenuItem (id=NotTakenJob) at SubmissionActionsMenuItemSet.pcf: line 27, column 139
    function visible_6 () : java.lang.Boolean {
      return policyPeriod.SubmissionProcess.canNotTakeJob().Okay && policyPeriod.SubmissionProcess.canConvertToFullApp().Okay
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get submission () : Submission {
      return getRequireValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setRequireValue("submission", 0, $arg)
    }
    
    
  }
  
  
}