package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPCoverageCostLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=CovTermValue_Cell) at TDIC_BOPCoverageCostLV.pcf: line 71, column 43
    function valueRoot_21 () : java.lang.Object {
      return covTerm
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPCoverageCostLV.pcf: line 66, column 100
    function value_18 () : java.lang.String {
      return bopScreenHelper.getCoverageTermDisplayName(theCoverage, covTerm).leftPad( 7 )
    }
    
    // 'value' attribute on TextCell (id=CovTermValue_Cell) at TDIC_BOPCoverageCostLV.pcf: line 71, column 43
    function value_20 () : java.lang.String {
      return covTerm.DisplayValue
    }
    
    // 'visible' attribute on EmptyCell (id=TermAmount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 74, column 33
    function visible_23 () : java.lang.Boolean {
      return prorated
    }
    
    // 'visible' attribute on Row at TDIC_BOPCoverageCostLV.pcf: line 62, column 74
    function visible_27 () : java.lang.Boolean {
      return bopScreenHelper.canDisaplyTerm(theCoverage, covTerm)
    }
    
    property get covTerm () : gw.api.domain.covterm.CovTerm {
      return getIteratedValue(2) as gw.api.domain.covterm.CovTerm
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BOPCoverageCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPCoverageCostLV.pcf: line 40, column 32
    function initialValue_13 () : entity.BOPCost {
      return wrapper.Cost as entity.BOPCost
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPCoverageCostLV.pcf: line 44, column 33
    function initialValue_14 () : entity.Coverage {
      return wrapper.Cost != null ? (wrapper.Cost as BOPCost).Coverage : bopScreenHelper.getCoverage(coverable, wrapper.Description)
    }
    
    // RowIterator at TDIC_BOPCoverageCostLV.pcf: line 36, column 51
    function initializeVariables_51 () : void {
        cost = wrapper.Cost as entity.BOPCost;
  theCoverage = wrapper.Cost != null ? (wrapper.Cost as BOPCost).Coverage : bopScreenHelper.getCoverage(coverable, wrapper.Description);

    }
    
    // 'label' attribute on EmptyCell (id=CovTerm_Cell) at TDIC_BOPCoverageCostLV.pcf: line 105, column 187
    function label_31 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyLine.Coverage.CovTermValue",  gw.pcf.line.common.CostUIHelper.getDisplayableCurrency(new java.util.ArrayList<Cost>(costs)) )
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPCoverageCostLV.pcf: line 57, column 26
    function sortBy_15 (covTerm :  gw.api.domain.covterm.CovTerm) : java.lang.Object {
      return covTerm.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPCoverageCostLV.pcf: line 60, column 26
    function sortBy_16 (covTerm :  gw.api.domain.covterm.CovTerm) : java.lang.Object {
      return covTerm.Pattern.PublicID
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 113, column 25
    function valueRoot_34 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on RowIterator at TDIC_BOPCoverageCostLV.pcf: line 54, column 35
    function value_28 () : gw.api.domain.covterm.CovTerm[] {
      return bopScreenHelper.hideBuildingCovTermsToDisplay(theCoverage)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPCoverageCostLV.pcf: line 100, column 113
    function value_29 () : java.lang.String {
      return theCoverage != null ? bopScreenHelper.getCoverageDisplayName(theCoverage) : cost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 113, column 25
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualTermAmountBilling
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_BOPCoverageCostLV.pcf: line 121, column 25
    function value_37 () : java.util.Date {
      return cost.EffDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at TDIC_BOPCoverageCostLV.pcf: line 129, column 25
    function value_41 () : java.util.Date {
      return cost.ExpDate
    }
    
    // 'value' attribute on TextCell (id=Proration_Cell) at TDIC_BOPCoverageCostLV.pcf: line 137, column 25
    function value_45 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(cost.ProRataByDaysValue, "#0.0000")
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 146, column 25
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return wrapper.Total//cost.ActualAmountBilling
    }
    
    // 'visible' attribute on RowIterator at TDIC_BOPCoverageCostLV.pcf: line 54, column 35
    function visible_17 () : java.lang.Boolean {
      return wrapper.Visible
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 113, column 25
    function visible_35 () : java.lang.Boolean {
      return prorated
    }
    
    // 'visible' attribute on Row at TDIC_BOPCoverageCostLV.pcf: line 92, column 86
    function visible_50 () : java.lang.Boolean {
      return wrapper.Visible and bopScreenHelper.canDisplayPremium(theCoverage)
    }
    
    property get cost () : entity.BOPCost {
      return getVariableValue("cost", 1) as entity.BOPCost
    }
    
    property set cost ($arg :  entity.BOPCost) {
      setVariableValue("cost", 1, $arg)
    }
    
    property get theCoverage () : entity.Coverage {
      return getVariableValue("theCoverage", 1) as entity.Coverage
    }
    
    property set theCoverage ($arg :  entity.Coverage) {
      setVariableValue("theCoverage", 1, $arg)
    }
    
    property get wrapper () : gw.api.ui.BOPCostWrapper_TDIC {
      return getIteratedValue(1) as gw.api.ui.BOPCostWrapper_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPCoverageCostLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPCoverageCostLV.pcf: line 27, column 23
    function initialValue_0 () : boolean {
      return costs.AnyProrated
    }
    
    // 'label' attribute on EmptyCell (id=CovTerm_Cell) at TDIC_BOPCoverageCostLV.pcf: line 105, column 187
    function label_2 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyLine.Coverage.CovTermValue",  gw.pcf.line.common.CostUIHelper.getDisplayableCurrency(new java.util.ArrayList<Cost>(costs)) )
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPCoverageCostLV.pcf: line 47, column 24
    function sortBy_1 (wrapper :  gw.api.ui.BOPCostWrapper_TDIC) : java.lang.Object {
      return wrapper.Order
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_BOPCoverageCostLV.pcf: line 146, column 25
    function sumValue_12 (wrapper :  gw.api.ui.BOPCostWrapper_TDIC) : java.lang.Object {
      return wrapper.Total//(wrapper.Cost as entity.BOPCost).ActualAmountBilling
    }
    
    // 'value' attribute on RowIterator at TDIC_BOPCoverageCostLV.pcf: line 36, column 51
    function value_52 () : gw.api.ui.BOPCostWrapper_TDIC[] {
      return costWrappers//bopScreenHelper.wrapCosts(costs)
    }
    
    // 'footerLabel' attribute on RowIterator at TDIC_BOPCoverageCostLV.pcf: line 100, column 113
    function value_7 () : java.lang.Object {
      return footerMessage
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_BOPCoverageCostLV.pcf: line 113, column 25
    function visible_3 () : java.lang.Boolean {
      return prorated
    }
    
    // 'visible' attribute on ListViewPanel (id=TDIC_BOPCoverageCostLV) at TDIC_BOPCoverageCostLV.pcf: line 11, column 49
    function visible_53 () : java.lang.Boolean {
      return costs != null and not costs.Empty
    }
    
    property get bopScreenHelper () : tdic.web.pcf.helper.BOPQuoteScreenHelper {
      return getVariableValue("bopScreenHelper", 0) as tdic.web.pcf.helper.BOPQuoteScreenHelper
    }
    
    property set bopScreenHelper ($arg :  tdic.web.pcf.helper.BOPQuoteScreenHelper) {
      setVariableValue("bopScreenHelper", 0, $arg)
    }
    
    property get costWrappers () : gw.api.ui.BOPCostWrapper_TDIC[] {
      return getRequireValue("costWrappers", 0) as gw.api.ui.BOPCostWrapper_TDIC[]
    }
    
    property set costWrappers ($arg :  gw.api.ui.BOPCostWrapper_TDIC[]) {
      setRequireValue("costWrappers", 0, $arg)
    }
    
    property get costs () : Set<BOPCost> {
      return getRequireValue("costs", 0) as Set<BOPCost>
    }
    
    property set costs ($arg :  Set<BOPCost>) {
      setRequireValue("costs", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get footerMessage () : java.lang.String {
      return getRequireValue("footerMessage", 0) as java.lang.String
    }
    
    property set footerMessage ($arg :  java.lang.String) {
      setRequireValue("footerMessage", 0, $arg)
    }
    
    property get prorated () : boolean {
      return getVariableValue("prorated", 0) as java.lang.Boolean
    }
    
    property set prorated ($arg :  boolean) {
      setVariableValue("prorated", 0, $arg)
    }
    
    
  }
  
  
}