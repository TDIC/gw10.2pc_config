package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.validation.PCValidationContext
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleCreditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ScheduleCreditPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ScheduleCreditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ScheduleCreditPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (scheduleRate :  WC7Modifier, openForEdit :  boolean) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=WC7ScheduleCreditPopup) at WC7ScheduleCreditPopup.pcf: line 13, column 80
    function afterCommit_6 (TopLocation :  pcf.api.Location) : void {
      setScheduleModEligibility(scheduleRate)
    }
    
    // 'beforeCommit' attribute on Popup (id=WC7ScheduleCreditPopup) at WC7ScheduleCreditPopup.pcf: line 13, column 80
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      validateValueRange(scheduleRate)
    }
    
    // 'def' attribute on ListViewInput at WC7ScheduleCreditPopup.pcf: line 41, column 31
    function def_onEnter_3 (def :  pcf.WC7ScheduleRateLV) : void {
      def.onEnter(scheduleRate, jurisdiction)
    }
    
    // 'def' attribute on ListViewInput at WC7ScheduleCreditPopup.pcf: line 41, column 31
    function def_refreshVariables_4 (def :  pcf.WC7ScheduleRateLV) : void {
      def.refreshVariables(scheduleRate, jurisdiction)
    }
    
    // 'editable' attribute on Screen at WC7ScheduleCreditPopup.pcf: line 27, column 30
    function editable_5 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at WC7ScheduleCreditPopup.pcf: line 25, column 38
    function initialValue_0 () : entity.WC7Jurisdiction {
      return scheduleRate.WC7Jurisdiction
    }
    
    // EditButtons at WC7ScheduleCreditPopup.pcf: line 30, column 32
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on Label (id=ScheduleCreditName) at WC7ScheduleCreditPopup.pcf: line 37, column 48
    function label_2 () : java.lang.String {
      return scheduleRate.Pattern.Name
    }
    
    override property get CurrentLocation () : pcf.WC7ScheduleCreditPopup {
      return super.CurrentLocation as pcf.WC7ScheduleCreditPopup
    }
    
    property get jurisdiction () : entity.WC7Jurisdiction {
      return getVariableValue("jurisdiction", 0) as entity.WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  entity.WC7Jurisdiction) {
      setVariableValue("jurisdiction", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get scheduleRate () : WC7Modifier {
      return getVariableValue("scheduleRate", 0) as WC7Modifier
    }
    
    property set scheduleRate ($arg :  WC7Modifier) {
      setVariableValue("scheduleRate", 0, $arg)
    }
    
    
    function setScheduleModEligibility(mod : Modifier){
      if (mod.Pattern.CodeIdentifier.equals("WC7ScheduleMod")){
        mod.Eligible = false
        if (mod.RateWithinLimits != 0){
          mod.Eligible = true
        }
      }
    }
    
    function validateValueRange(credit : WC7Modifier) {
      var totalCredit = credit.RateFactors*.AssessmentWithinLimits.sum()
      if (!credit.isValueWithinRange(totalCredit)) {
        PCValidationContext.doPageLevelValidation(\context -> {
          context.Result.addError(credit.WC7Jurisdiction.WCLine, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.RateFactor.RateOutOfRange", totalCredit, credit.Minimum, credit.Maximum))
        })
      }
    }
    
    
  }
  
  
}