package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.BigToSmall.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GlobalAddressInputSet_BigToSmallExpressions {
  @javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.BigToSmall.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GlobalAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function available_25 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'available' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function available_41 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'available' attribute on TextInput (id=CityKanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 100, column 87
    function available_52 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CITYKANJI)
    }
    
    // 'available' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function available_62 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'available' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function available_75 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1KANJI)
    }
    
    // 'available' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function available_8 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'available' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function available_87 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'available' attribute on TextInput (id=AddressLine2Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 130, column 95
    function available_98 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2KANJI)
    }
    
    // 'value' attribute on TextInput (id=AddressLine2Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 130, column 95
    function defaultSetter_102 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine2Kanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.PostalCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.State = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=CityKanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 100, column 87
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.CityKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine1Kanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function defaultSetter_92 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function editable_26 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'editable' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function editable_42 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'editable' attribute on TextInput (id=CityKanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 100, column 87
    function editable_53 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CITYKANJI)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function editable_63 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function editable_76 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1KANJI)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function editable_88 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'editable' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function editable_9 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine2Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 130, column 95
    function editable_99 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2KANJI)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.BigToSmall.pcf: line 14, column 50
    function initialValue_0 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("PostalCode","PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.BigToSmall.pcf: line 21, column 33
    function initialValue_1 () : java.lang.Integer {
      if (addressOwner != null) addressOwner.InEditMode = CurrentLocation.InEditMode; return 0
    }
    
    // 'inputConversion' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function inputConversion_13 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.address.PostalCodeInputFormatter.convertPostalCode(VALUE, addressOwner.SelectedCountry)
    }
    
    // 'inputMask' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function inputMask_18 () : java.lang.String {
      return gw.api.contact.AddressAutocompleteUtil.getInputMask(addressOwner.AddressDelegate, "PostalCode")
    }
    
    // 'label' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function label_12 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel
    }
    
    // 'label' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 27, column 50
    function label_3 () : java.lang.Object {
      return addressOwner.AddressNameLabel
    }
    
    // 'label' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function label_65 () : java.lang.Object {
      return addressOwner.AddressLine1PhoneticLabel
    }
    
    // 'label' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function label_78 () : java.lang.Object {
      return addressOwner.AddressLine1Label
    }
    
    // 'required' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function required_14 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'required' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function required_29 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'required' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function required_44 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'required' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function required_66 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'required' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function required_90 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'validationExpression' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function validationExpression_10 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "PostalCode", gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel)
    }
    
    // 'validationExpression' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function validationExpression_27 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "State")
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function valueRange_33 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.getStates(addressOwner.AddressDelegate.Country)
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function valueRoot_17 () : java.lang.Object {
      return addressOwner.AddressDelegate
    }
    
    // 'value' attribute on TextInput (id=AddressLine2Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 130, column 95
    function value_101 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine2Kanji
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function value_15 () : java.lang.String {
      return addressOwner.AddressDelegate.PostalCode
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function value_30 () : typekey.State {
      return addressOwner.AddressDelegate.State
    }
    
    // 'value' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 27, column 50
    function value_4 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(addressOwner.AddressDelegate, "\n")
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function value_45 () : java.lang.String {
      return addressOwner.AddressDelegate.City
    }
    
    // 'value' attribute on TextInput (id=CityKanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 100, column 87
    function value_55 () : java.lang.String {
      return addressOwner.AddressDelegate.CityKanji
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function value_67 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine1
    }
    
    // 'value' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function value_79 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine1Kanji
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function value_91 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine2
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function verifyValueRangeIsAllowedType_34 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function verifyValueRangeIsAllowedType_34 ($$arg :  typekey.State[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function verifyValueRange_35 () : void {
      var __valueRangeArg = gw.api.contact.AddressAutocompleteUtil.getStates(addressOwner.AddressDelegate.Country)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_34(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine2Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 130, column 95
    function visible_100 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2KANJI)
    }
    
    // 'visible' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 74, column 88
    function visible_11 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'visible' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 27, column 50
    function visible_2 () : java.lang.Boolean {
      return addressOwner.ShowAddressSummary
    }
    
    // 'visible' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 85, column 83
    function visible_28 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'visible' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 93, column 82
    function visible_43 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'visible' attribute on TextInput (id=CityKanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 100, column 87
    function visible_54 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CITYKANJI)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 108, column 90
    function visible_64 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine1Kanji_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 115, column 95
    function visible_77 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1KANJI)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.BigToSmall.pcf: line 123, column 90
    function visible_89 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    property get pchandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("pchandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set pchandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("pchandler", 0, $arg)
    }
    
    property get saveEditMode () : java.lang.Integer {
      return getVariableValue("saveEditMode", 0) as java.lang.Integer
    }
    
    property set saveEditMode ($arg :  java.lang.Integer) {
      setVariableValue("saveEditMode", 0, $arg)
    }
    
    
  }
  
  
}