package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/common/PolicyInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyInfoInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/job/common/PolicyInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyInfoInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 142, column 67
    function action_75 () : void {
      if (perm.PolicyPeriod.view(rewriteSourcePolicy.LatestBoundPeriod)) {pcf.PolicyFileForward.go(rewriteSourcePolicy.LatestBoundPeriod.PolicyNumber)}
    }
    
    // 'action' attribute on TextInput (id=DividedSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 148, column 66
    function action_80 () : void {
      pcf.PolicyFileForward.go(dividedSourcePolicy.LatestBoundPeriod.PolicyNumber)
    }
    
    // 'action' attribute on TextInput (id=DividedSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 148, column 66
    function action_dest_81 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(dividedSourcePolicy.LatestBoundPeriod.PolicyNumber)
    }
    
    // 'value' attribute on TextInput (id=LegacySourceSystem_Input) at PolicyInfoInputSet.pcf: line 173, column 141
    function defaultSetter_106 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.LegacyPolicySource_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyInfoInputSet.pcf: line 78, column 29
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      effectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyInfoInputSet.pcf: line 90, column 38
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PeriodEnd = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=WrittenDate_Input) at PolicyInfoInputSet.pcf: line 105, column 148
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.WrittenDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.RateAsOfDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at PolicyInfoInputSet.pcf: line 136, column 112
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.Policy.PrimaryLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      termType = (__VALUE_TO_SET as typekey.TermType)
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumber_Input) at PolicyInfoInputSet.pcf: line 159, column 141
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.LegacyPolicyNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicySuffix_Input) at PolicyInfoInputSet.pcf: line 165, column 141
    function defaultSetter_98 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.LegacyPolicySuffix_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on InputSet (id=PolicyInfoInputSet) at PolicyInfoInputSet.pcf: line 7, column 29
    function editable_111 () : java.lang.Boolean {
      return openForEditInit.get()
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at PolicyInfoInputSet.pcf: line 78, column 29
    function editable_19 () : java.lang.Boolean {
      return !(policyPeriod.Job typeis PolicyChange || policyPeriod.Job typeis Renewal) and policyPeriod.CanUpdatePeriodDates
    }
    
    // 'editable' attribute on DateInput (id=ExpirationDate_Input) at PolicyInfoInputSet.pcf: line 90, column 38
    function editable_28 () : java.lang.Boolean {
      return gw.pcf.job.PolicyInfoHelper.isExpirationDateEditable(policyPeriod, termType)
    }
    
    // 'editable' attribute on DateInput (id=WrittenDate_Input) at PolicyInfoInputSet.pcf: line 105, column 148
    function editable_40 () : java.lang.Boolean {
      return policyPeriod.Job typeis Renewal? false : (perm.System.editwrittendate and policyPeriod.canEditWrittenDate_TDIC)
    }
    
    // 'editable' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function editable_48 () : java.lang.Boolean {
      return perm.System.editrateasofdate and not(policyPeriod.JobProcess typeis gw.job.NewTermProcess)
    }
    
    // 'editable' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function editable_7 () : java.lang.Boolean {
      return policyPeriod.isTermTypeEditable
    }
    
    // 'editable' attribute on TextInput (id=LegacyPolicyNumber_Input) at PolicyInfoInputSet.pcf: line 159, column 141
    function editable_87 () : java.lang.Boolean {
      return policyPeriod.Job typeis Submission or (policyPeriod.BasedOn == null or policyPeriod.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION)
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 27, column 75
    function initialValue_0 () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> policyPeriod.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 32, column 30
    function initialValue_1 () : java.util.Date {
      return policyPeriod.PeriodStart
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 36, column 32
    function initialValue_2 () : typekey.TermType {
      return policyPeriod.TermType
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 40, column 22
    function initialValue_3 () : Policy {
      return policyPeriod.Policy.RewrittenToNewAccountSource
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 44, column 22
    function initialValue_4 () : Policy {
      return policyPeriod.Policy.DividedSourcePolicy
    }
    
    // 'initialValue' attribute on Variable at PolicyInfoInputSet.pcf: line 48, column 23
    function initialValue_5 () : boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'onChange' attribute on PostOnChange at PolicyInfoInputSet.pcf: line 81, column 103
    function onChange_18 () : void {
      gw.pcf.job.PolicyInfoHelper.setPeriodDates(policyPeriod, effectiveDate, termType)
    }
    
    // 'onChange' attribute on PostOnChange at PolicyInfoInputSet.pcf: line 92, column 54
    function onChange_27 () : void {
      termType = policyPeriod.TermType
    }
    
    // 'onChange' attribute on PostOnChange at PolicyInfoInputSet.pcf: line 63, column 54
    function onChange_6 () : void {
      policyPeriod.TermType = termType
    }
    
    // 'requestValidationExpression' attribute on DateInput (id=EffectiveDate_Input) at PolicyInfoInputSet.pcf: line 78, column 29
    function requestValidationExpression_21 (VALUE :  java.util.Date) : java.lang.Object {
      return tdic.pc.config.pcf.job.TDIC_PolicyInfoHelper.isEffectiveDateValid(VALUE, policyPeriod) ? null : DisplayKey.get("TDIC.Web.SubmissionWizard.Validation.FutureEffectiveDate")
    }
    
    // 'required' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function required_51 () : java.lang.Boolean {
      return not(policyPeriod.JobProcess typeis gw.job.NewTermProcess)
    }
    
    // 'required' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function required_60 () : java.lang.Boolean {
      return policyPeriod.Lines.hasMatch(\ line -> line.BaseStateRequired)
    }
    
    // 'validationExpression' attribute on DateInput (id=EffectiveDate_Input) at PolicyInfoInputSet.pcf: line 78, column 29
    function validationExpression_20 () : java.lang.Object {
      return gw.policy.PolicyPeriodValidation.validateDateForRewriteNewAccount(policyPeriod) 
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at PolicyInfoInputSet.pcf: line 90, column 38
    function validationExpression_29 () : java.lang.Object {
      return gw.policy.PolicyPeriodValidation.validatePeriodEndDoesNotOverlapRewrittenNewAccountPolicy(policyPeriod) 
    }
    
    // 'validationExpression' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function validationExpression_49 () : java.lang.Object {
      return policyPeriod.RateAsOfDate.beforeOrEqual(java.util.Date.CurrentDate) ? null : DisplayKey.get("Web.Rating.Errors.FutureRateAsOfDate")
    }
    
    // 'valueRange' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function valueRange_10 () : java.lang.Object {
      return gw.pcf.job.PolicyInfoHelper.getTermTypes(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function valueRange_63 () : java.lang.Object {
      return gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(policyPeriod,false)
    }
    
    // 'value' attribute on TextInput (id=TermNumber_Input) at PolicyInfoInputSet.pcf: line 69, column 38
    function valueRoot_16 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at PolicyInfoInputSet.pcf: line 136, column 112
    function valueRoot_71 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TextInput (id=LegacySourceSystem_Input) at PolicyInfoInputSet.pcf: line 173, column 141
    function value_105 () : java.lang.String {
      return policyPeriod.LegacyPolicySource_TDIC
    }
    
    // 'value' attribute on TextInput (id=TermNumber_Input) at PolicyInfoInputSet.pcf: line 69, column 38
    function value_15 () : java.lang.Integer {
      return policyPeriod.TermNumber
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyInfoInputSet.pcf: line 78, column 29
    function value_22 () : java.util.Date {
      return effectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyInfoInputSet.pcf: line 90, column 38
    function value_30 () : java.util.Date {
      return policyPeriod.PeriodEnd
    }
    
    // 'value' attribute on DateInput (id=originalEffDate_Input) at PolicyInfoInputSet.pcf: line 98, column 114
    function value_37 () : java.util.Date {
      return policyPeriod.Policy.OriginalEffectiveDate != null? policyPeriod.Policy.OriginalEffectiveDate : policyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateInput (id=WrittenDate_Input) at PolicyInfoInputSet.pcf: line 105, column 148
    function value_42 () : java.util.Date {
      return policyPeriod.WrittenDate
    }
    
    // 'value' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function value_52 () : java.util.Date {
      return policyPeriod.RateAsOfDate
    }
    
    // 'value' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function value_61 () : typekey.Jurisdiction {
      return policyPeriod.BaseState
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at PolicyInfoInputSet.pcf: line 136, column 112
    function value_69 () : typekey.LanguageType {
      return policyPeriod.Policy.PrimaryLanguage
    }
    
    // 'value' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 142, column 67
    function value_76 () : java.lang.String {
      return DisplayKey.get("Web.PolicyFile.Summary.PolicyNumOnAccount", rewriteSourcePolicy.LatestBoundPeriod.PolicyNumber, rewriteSourcePolicy.Account.AccountNumber)
    }
    
    // 'value' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function value_8 () : typekey.TermType {
      return termType
    }
    
    // 'value' attribute on TextInput (id=DividedSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 148, column 66
    function value_82 () : java.lang.String {
      return DisplayKey.get("Web.PolicyFile.Summary.PolicyNumOnAccount", dividedSourcePolicy.LatestBoundPeriod.PolicyNumberDisplayString, dividedSourcePolicy.Account.AccountNumber)
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumber_Input) at PolicyInfoInputSet.pcf: line 159, column 141
    function value_89 () : java.lang.String {
      return policyPeriod.LegacyPolicyNumber_TDIC
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicySuffix_Input) at PolicyInfoInputSet.pcf: line 165, column 141
    function value_97 () : java.lang.String {
      return policyPeriod.LegacyPolicySuffix_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.TermType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function verifyValueRangeIsAllowedType_64 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function verifyValueRangeIsAllowedType_64 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TermType_Input) at PolicyInfoInputSet.pcf: line 61, column 36
    function verifyValueRange_12 () : void {
      var __valueRangeArg = gw.pcf.job.PolicyInfoHelper.getTermTypes(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function verifyValueRange_65 () : void {
      var __valueRangeArg = gw.web.job.submission.NewSubmissionUtil.setBaseStateAndSelection_TDIC(policyPeriod,false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_64(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=originalEffDate_Input) at PolicyInfoInputSet.pcf: line 98, column 114
    function visible_36 () : java.lang.Boolean {
      return !policyPeriod.BOPLineExists && policyPeriod.Job.Subtype != typekey.Job.TC_REINSTATEMENT
    }
    
    // 'visible' attribute on DateInput (id=WrittenDate_Input) at PolicyInfoInputSet.pcf: line 105, column 148
    function visible_41 () : java.lang.Boolean {
      return showWrittenDate && !(quoteType == QuoteType.TC_QUICK) && policyPeriod.Job.Subtype != typekey.Job.TC_REINSTATEMENT
    }
    
    // 'visible' attribute on DateInput (id=RateAsOfDate_Input) at PolicyInfoInputSet.pcf: line 115, column 74
    function visible_50 () : java.lang.Boolean {
      return policyPeriod.Job.Subtype != typekey.Job.TC_REINSTATEMENT
    }
    
    // 'visible' attribute on RangeInput (id=BaseState_Input) at PolicyInfoInputSet.pcf: line 126, column 87
    function visible_59 () : java.lang.Boolean {
      return gw.web.policy.PolicyInfoUIHelper.getBaseStateVisibility(policyPeriod)
    }
    
    // 'visible' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at PolicyInfoInputSet.pcf: line 136, column 112
    function visible_68 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().size() > 1 and policyPeriod.Reinstatement == null
    }
    
    // 'visible' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 142, column 67
    function visible_74 () : java.lang.Boolean {
      return rewriteSourcePolicy != null and showRewriteLinks
    }
    
    // 'visible' attribute on TextInput (id=DividedSourcePolicy_Input) at PolicyInfoInputSet.pcf: line 148, column 66
    function visible_79 () : java.lang.Boolean {
      return dividedSourcePolicy != null and showSplitSource
    }
    
    // 'visible' attribute on InputDivider at PolicyInfoInputSet.pcf: line 150, column 80
    function visible_85 () : java.lang.Boolean {
      return !isWorkersComp7 && !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on Label at PolicyInfoInputSet.pcf: line 153, column 141
    function visible_86 () : java.lang.Boolean {
      return !isWorkersComp7 and !(quoteType == QuoteType.TC_QUICK) && policyPeriod.Job.Subtype != typekey.Job.TC_REINSTATEMENT
    }
    
    property get dividedSourcePolicy () : Policy {
      return getVariableValue("dividedSourcePolicy", 0) as Policy
    }
    
    property set dividedSourcePolicy ($arg :  Policy) {
      setVariableValue("dividedSourcePolicy", 0, $arg)
    }
    
    property get effectiveDate () : java.util.Date {
      return getVariableValue("effectiveDate", 0) as java.util.Date
    }
    
    property set effectiveDate ($arg :  java.util.Date) {
      setVariableValue("effectiveDate", 0, $arg)
    }
    
    property get isWorkersComp7 () : boolean {
      return getVariableValue("isWorkersComp7", 0) as java.lang.Boolean
    }
    
    property set isWorkersComp7 ($arg :  boolean) {
      setVariableValue("isWorkersComp7", 0, $arg)
    }
    
    property get openForEditInit () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return getVariableValue("openForEditInit", 0) as gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>
    }
    
    property set openForEditInit ($arg :  gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>) {
      setVariableValue("openForEditInit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getRequireValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    property get rewriteSourcePolicy () : Policy {
      return getVariableValue("rewriteSourcePolicy", 0) as Policy
    }
    
    property set rewriteSourcePolicy ($arg :  Policy) {
      setVariableValue("rewriteSourcePolicy", 0, $arg)
    }
    
    property get showRewriteLinks () : boolean {
      return getRequireValue("showRewriteLinks", 0) as java.lang.Boolean
    }
    
    property set showRewriteLinks ($arg :  boolean) {
      setRequireValue("showRewriteLinks", 0, $arg)
    }
    
    property get showSplitSource () : boolean {
      return getRequireValue("showSplitSource", 0) as java.lang.Boolean
    }
    
    property set showSplitSource ($arg :  boolean) {
      setRequireValue("showSplitSource", 0, $arg)
    }
    
    property get showWrittenDate () : boolean {
      return getRequireValue("showWrittenDate", 0) as java.lang.Boolean
    }
    
    property set showWrittenDate ($arg :  boolean) {
      setRequireValue("showWrittenDate", 0, $arg)
    }
    
    property get termType () : typekey.TermType {
      return getVariableValue("termType", 0) as typekey.TermType
    }
    
    property set termType ($arg :  typekey.TermType) {
      setVariableValue("termType", 0, $arg)
    }
    
    function setPeriodDates() {
      if (policyPeriod.Submission != null and !policyPeriod.HasWorkersComp and !policyPeriod.WC7LineExists) {
        policyPeriod.SubmissionProcess.beforePeriodStartChanged(effectiveDate)
      }
      if (termType == TC_OTHER) {
        policyPeriod.PeriodStart = effectiveDate
      } else {
        var policyPeriodPlugin = gw.plugin.Plugins.get( gw.plugin.policyperiod.IPolicyTermPlugin )
        var expirationDate = gw.api.util.DateUtil.mergeDateAndTime(
            policyPeriodPlugin.calculatePeriodEnd(effectiveDate, termType, policyPeriod), policyPeriod.PeriodEnd)
        policyPeriod.setPeriodWindow( effectiveDate, expirationDate )
      }
      //20161021 TJT GW-2371 GW-1762
      if (policyPeriod.WC7LineExists){
        policyPeriod.WC7Line.initializeAndTransformWC7ClassCodes_TDIC() //GW-2371
        policyPeriod.WC7Line.initializeAndTransformSafetyGroupSchedRatingFactorToModifier_TDIC() //GW-1762
      }
    }
    
    function getTermTypes() : java.util.List<typekey.TermType> {
      if (policyPeriod.HasWorkersComp or policyPeriod.WC7LineExists){
        return policyPeriod.Policy.Product.AllowedPolicyTerms
      } else {
        return policyPeriod.getAvailablePolicyTermsForCurrentOffering()
      }
    }
    
    function isExpirationDateEditable() : boolean {
      if (policyPeriod.HasWorkersComp or policyPeriod.WC7LineExists){
        return (termType == TC_ANNUAL or termType == TC_OTHER) and policyPeriod.CanUpdatePeriodDates
      } else {
        return termType == TC_OTHER and policyPeriod.CanUpdatePeriodDates
      }
    }
    
    function isOrganizationTDICAndIS() : boolean {
      if (policyPeriod.ProducerCodeOfRecord.Organization.DisplayName == "TDIC Insurance Solutions" or
          policyPeriod.ProducerCodeOfRecord.Organization.DisplayName == "The Dentists Insurance Company") {
        return true
      }
      return false
    }
    
    
  }
  
  
}