package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7EmployeeLeasingClientEndorsementCondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientCondPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 38, column 106
    function available_68 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 70, column 71
    function conversionExpression_12 (PickedValue :  Contact) : entity.WC7IncludedLaborContactDetail {
      return addNewLaborClientDetailForContactOnCondition(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 20, column 54
    function initialValue_0 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 24, column 36
    function initialValue_1 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 28, column 54
    function initialValue_2 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(wc7Line.WC7Jurisdictions.first().Jurisdiction)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 75, column 127
    function label_18 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.Existing", WC7PolicyLaborClient.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientCondPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 38, column 106
    function label_69 () : java.lang.Object {
      return stateConfig.getClauseName(conditionPattern, wc7Line)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 186, column 104
    function onChange_6 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncCoverages({coverable}, null)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 70, column 71
    function pickLocation_13 () : void {
      ContactSearchPopup.push(TC_LABORCLIENT)
    }
    
    // 'onToggle' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientCondPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 38, column 106
    function setter_70 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(wc7Line, conditionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 82, column 34
    function sortBy_14 (acctContact :  entity.AccountContact) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 57, column 32
    function sortBy_7 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 132, column 53
    function sortValue_23 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.WC7LaborContact
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function sortValue_24 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 152, column 76
    function sortValue_25 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 158, column 77
    function sortValue_26 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.ContractExpirationDate
    }
    
    // 'value' attribute on TextCell (id=NumberOfLeasedWorkers_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 166, column 48
    function sortValue_27 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.NumberOfEmployees
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfDuties_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 173, column 47
    function sortValue_28 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.DescriptionOfDuties
    }
    
    // 'value' attribute on TextCell (id=WorkLocation_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 180, column 47
    function sortValue_29 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailCond.WorkLocation
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 124, column 64
    function toRemove_65 (policyLaborClientDetailCond :  entity.WC7IncludedLaborContactDetail) : void {
      policyLaborClientDetailCond.WC7LaborContact.removeDetailFromCurrentAndChild(policyLaborClientDetailCond)
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 32, column 37
    function valueRoot_4 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 54, column 49
    function value_11 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLABORCLIENT)
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 79, column 53
    function value_17 () : entity.AccountContact[] {
      return wc7Line.WC7PolicyLaborClientDetailExistingCandidates
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 98, column 53
    function value_22 () : entity.AccountContact[] {
      return wc7Line.WC7PolicyLaborClientDetailOtherCandidates
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 32, column 37
    function value_3 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 124, column 64
    function value_66 () : entity.WC7IncludedLaborContactDetail[] {
      return wc7Line.getIncludedLaborClientDetails(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ClausePattern>("WC7EmployeeLeasingClientEndorsementCond"))
    }
    
    // 'childrenVisible' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientCondPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 38, column 106
    function visible_67 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 189, column 101
    function visible_73 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 0, $arg)
    }
    
    property get wc7Line () : productmodel.WC7Line {
      return getVariableValue("wc7Line", 0) as productmodel.WC7Line
    }
    
    property set wc7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    function addNewLaborClientDetailForContactOnCondition(aContact : Contact) : WC7IncludedLaborContactDetail {
      var newLaborClient = wc7Line.addIncludedLaborClientDetailForContact(aContact,
          wc7Line.WC7EmployeeLeasingClientEndorsementCond)
      return newLaborClient
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 87, column 103
    function label_15 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 87, column 103
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborClientDetailForContactOnCondition(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 106, column 167
    function label_20 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 106, column 167
    function toCreateAndAdd_21 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborClientDetailForContactOnCondition(acctContact.Contact)//wc7Line.addNewLaborClientDetailForContact(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 132, column 53
    function action_30 () : void {
      EditPolicyContactRolePopup.push(policyLaborClientDetailCond.WC7LaborContact, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 132, column 53
    function action_dest_31 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyLaborClientDetailCond.WC7LaborContact, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 152, column 76
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 158, column 77
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=NumberOfLeasedWorkers_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 166, column 48
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.NumberOfEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfDuties_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 173, column 47
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.DescriptionOfDuties = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=WorkLocation_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 180, column 47
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailCond.WorkLocation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function valueRange_41 () : java.lang.Object {
      return wc7Line.stateFilterFor(conditionPattern).TypeKeys
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 132, column 53
    function valueRoot_33 () : java.lang.Object {
      return policyLaborClientDetailCond
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 132, column 53
    function value_32 () : entity.WC7LaborContact {
      return policyLaborClientDetailCond.WC7LaborContact
    }
    
    // 'value' attribute on TypeKeyCell (id=Inclusion_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 137, column 34
    function value_35 () : typekey.Inclusion {
      return policyLaborClientDetailCond.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function value_38 () : typekey.Jurisdiction {
      return policyLaborClientDetailCond.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 152, column 76
    function value_45 () : java.util.Date {
      return policyLaborClientDetailCond.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 158, column 77
    function value_49 () : java.util.Date {
      return policyLaborClientDetailCond.ContractExpirationDate
    }
    
    // 'value' attribute on TextCell (id=NumberOfLeasedWorkers_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 166, column 48
    function value_53 () : java.lang.Integer {
      return policyLaborClientDetailCond.NumberOfEmployees
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfDuties_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 173, column 47
    function value_57 () : java.lang.String {
      return policyLaborClientDetailCond.DescriptionOfDuties
    }
    
    // 'value' attribute on TextCell (id=WorkLocation_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 180, column 47
    function value_61 () : java.lang.String {
      return policyLaborClientDetailCond.WorkLocation
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function verifyValueRangeIsAllowedType_42 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 146, column 51
    function verifyValueRange_43 () : void {
      var __valueRangeArg = wc7Line.stateFilterFor(conditionPattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    property get policyLaborClientDetailCond () : entity.WC7IncludedLaborContactDetail {
      return getIteratedValue(1) as entity.WC7IncludedLaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 63, column 200
    function conversionExpression_9 (PickedValue :  WC7LaborContactDetail) : entity.WC7IncludedLaborContactDetail {
      return PickedValue as WC7IncludedLaborContactDetail
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 63, column 200
    function label_8 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientEndorsementCond.pcf: line 63, column 200
    function pickLocation_10 () : void {
      WC7NewLaborClientForContactTypePopup.push(gw.lob.wc7.schedule.WC7ScheduleClientPresenter.forLaborClient(contactType, wc7Line.WC7EmployeeLeasingClientEndorsementCond))
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}