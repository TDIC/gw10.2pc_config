package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/rewritenewaccount/RewriteNewAccountWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RewriteNewAccountWizard_PolicyInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/rewritenewaccount/RewriteNewAccountWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RewriteNewAccountWizard_PolicyInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonInput (id=AutoSelectUWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 63, column 53
    function action_27 () : void {
      policyPeriod.autoSelectUWCompany()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 30, column 77
    function currency_5 () : typekey.Currency {
      return policyPeriod.PreferredSettlementCurrency
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 34, column 60
    function def_onEnter_10 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 42, column 38
    function def_onEnter_12 (def :  pcf.PolicyInfoInputSet) : void {
      def.onEnter(policyPeriod, true, true, false,null)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 45, column 102
    function def_onEnter_14 (def :  pcf.PolicyInfoProducerOfRecordInputSet) : void {
      def.onEnter(policyPeriod, ProducerStatusUse.TC_SUBMISSIONOKAY)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 66, column 56
    function def_onEnter_29 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 32, column 55
    function def_onEnter_8 (def :  pcf.AccountInfoInputSet) : void {
      def.onEnter(policyPeriod,null)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 34, column 60
    function def_refreshVariables_11 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 42, column 38
    function def_refreshVariables_13 (def :  pcf.PolicyInfoInputSet) : void {
      def.refreshVariables(policyPeriod, true, true, false,null)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 45, column 102
    function def_refreshVariables_15 (def :  pcf.PolicyInfoProducerOfRecordInputSet) : void {
      def.refreshVariables(policyPeriod, ProducerStatusUse.TC_SUBMISSIONOKAY)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 66, column 56
    function def_refreshVariables_30 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 32, column 55
    function def_refreshVariables_9 (def :  pcf.AccountInfoInputSet) : void {
      def.refreshVariables(policyPeriod,null)
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.UWCompany = (__VALUE_TO_SET as entity.UWCompany)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 30, column 77
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.EstimatedPremium = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function editable_16 () : java.lang.Boolean {
      return perm.System.multicompquote
    }
    
    // 'initialValue' attribute on Variable at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 19, column 43
    function initialValue_0 () : gw.job.AvailableUWCompanies {
      return new gw.job.AvailableUWCompanies(policyPeriod)
    }
    
    // 'optionLabel' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function optionLabel_20 (VALUE :  entity.UWCompany) : java.lang.String {
      return VALUE.DisplayName
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function valueRange_21 () : java.lang.Object {
      return availableUWCompanies.Value
    }
    
    // 'value' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 30, column 77
    function valueRoot_4 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function value_17 () : entity.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 30, column 77
    function value_2 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.EstimatedPremium
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_22 ($$arg :  entity.UWCompany[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_22 ($$arg :  gw.api.database.IQueryBeanResult<entity.UWCompany>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_22 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 57, column 41
    function verifyValueRange_23 () : void {
      var __valueRangeArg = availableUWCompanies.Value
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_22(__valueRangeArg)
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 30, column 77
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.Policy.Product.ProductType == TC_COMMERCIAL
    }
    
    // 'visible' attribute on ButtonInput (id=AutoSelectUWCompany_Input) at RewriteNewAccountWizard_PolicyInfoDV.pcf: line 63, column 53
    function visible_26 () : java.lang.Boolean {
      return not perm.System.multicompquote
    }
    
    property get availableUWCompanies () : gw.job.AvailableUWCompanies {
      return getVariableValue("availableUWCompanies", 0) as gw.job.AvailableUWCompanies
    }
    
    property set availableUWCompanies ($arg :  gw.job.AvailableUWCompanies) {
      setVariableValue("availableUWCompanies", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get rewriteNewAccount () : RewriteNewAccount {
      return getRequireValue("rewriteNewAccount", 0) as RewriteNewAccount
    }
    
    property set rewriteNewAccount ($arg :  RewriteNewAccount) {
      setRequireValue("rewriteNewAccount", 0, $arg)
    }
    
    
  }
  
  
}