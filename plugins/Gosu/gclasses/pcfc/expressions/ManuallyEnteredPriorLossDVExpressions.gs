package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/common/ManuallyEnteredPriorLossDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ManuallyEnteredPriorLossDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/common/ManuallyEnteredPriorLossDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ManuallyEnteredPriorLossDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=TotalNumberOfClaims_Input) at ManuallyEnteredPriorLossDV.pcf: line 17, column 26
    function valueRoot_1 () : java.lang.Object {
      return policyPeriod.Policy.PriorLosses
    }
    
    // 'value' attribute on TextInput (id=TotalNumberOfClaims_Input) at ManuallyEnteredPriorLossDV.pcf: line 17, column 26
    function value_0 () : int {
      return policyPeriod.Policy.PriorLosses.Count
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}