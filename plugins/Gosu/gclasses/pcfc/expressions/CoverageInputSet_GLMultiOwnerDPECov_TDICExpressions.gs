package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLMultiOwnerDPECov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 33, column 99
    function available_9 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'value' attribute on TextAreaInput (id=corpName_Input) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 40, column 75
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.GLMultiOwnerDPECov_TDIC.GLCorpName_TDICTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 33, column 99
    function label_10 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'label' attribute on TextAreaInput (id=corpName_Input) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 40, column 75
    function label_2 () : java.lang.Object {
      return glLine.GLMultiOwnerDPECov_TDIC.GLCorpName_TDICTerm.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 33, column 99
    function setter_11 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on TextAreaInput (id=corpName_Input) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 40, column 75
    function valueRoot_5 () : java.lang.Object {
      return glLine.GLMultiOwnerDPECov_TDIC.GLCorpName_TDICTerm
    }
    
    // 'value' attribute on TextAreaInput (id=corpName_Input) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 40, column 75
    function value_3 () : java.lang.String {
      return glLine.GLMultiOwnerDPECov_TDIC.GLCorpName_TDICTerm.Value
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 43, column 46
    function visible_14 () : java.lang.Boolean {
      return isMultiOwnerDPECovVisible()
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMultiOwnerDPECov_TDIC.pcf: line 33, column 99
    function visible_8 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isMultiOwnerDPECovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLMultiOwnerDPECov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
              coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
              coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLMultiOwnerDPECov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                    coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    
  }
  
  
}