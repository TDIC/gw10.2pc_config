package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TerritoryCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7TerritoryCodeSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TerritoryCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends WC7TerritoryCodeSearchPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7TerritoryCodeSearchPopup.pcf: line 73, column 47
    function def_onEnter_23 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at WC7TerritoryCodeSearchPopup.pcf: line 77, column 64
    function def_onEnter_25 (def :  pcf.WC7TerritoryCodeSearchResultsLV) : void {
      def.onEnter(searchResults)
    }
    
    // 'def' attribute on InputSetRef at WC7TerritoryCodeSearchPopup.pcf: line 73, column 47
    function def_refreshVariables_24 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on PanelRef at WC7TerritoryCodeSearchPopup.pcf: line 77, column 64
    function def_refreshVariables_26 (def :  pcf.WC7TerritoryCodeSearchResultsLV) : void {
      def.refreshVariables(searchResults)
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at WC7TerritoryCodeSearchPopup.pcf: line 57, column 45
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PostalCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7TerritoryCodeSearchPopup.pcf: line 63, column 45
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at WC7TerritoryCodeSearchPopup.pcf: line 69, column 45
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=City_Input) at WC7TerritoryCodeSearchPopup.pcf: line 45, column 45
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=County_Input) at WC7TerritoryCodeSearchPopup.pcf: line 51, column 45
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.County = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel at WC7TerritoryCodeSearchPopup.pcf: line 31, column 41
    function searchCriteria_28 () : gw.lob.common.TerritoryLookupCriteria {
      return territoryCode.createLookupCriteria()
    }
    
    // 'search' attribute on SearchPanel at WC7TerritoryCodeSearchPopup.pcf: line 31, column 41
    function search_27 () : java.lang.Object {
      return territoryCode.getTerritoryCodes(searchCriteria)
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at WC7TerritoryCodeSearchPopup.pcf: line 39, column 49
    function valueRoot_1 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at WC7TerritoryCodeSearchPopup.pcf: line 39, column 49
    function value_0 () : typekey.Jurisdiction {
      return searchCriteria.State
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at WC7TerritoryCodeSearchPopup.pcf: line 57, column 45
    function value_11 () : java.lang.String {
      return searchCriteria.PostalCode
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7TerritoryCodeSearchPopup.pcf: line 63, column 45
    function value_15 () : java.lang.String {
      return searchCriteria.Code
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at WC7TerritoryCodeSearchPopup.pcf: line 69, column 45
    function value_19 () : java.lang.String {
      return searchCriteria.Description
    }
    
    // 'value' attribute on TextInput (id=City_Input) at WC7TerritoryCodeSearchPopup.pcf: line 45, column 45
    function value_3 () : java.lang.String {
      return searchCriteria.City
    }
    
    // 'value' attribute on TextInput (id=County_Input) at WC7TerritoryCodeSearchPopup.pcf: line 51, column 45
    function value_7 () : java.lang.String {
      return searchCriteria.County
    }
    
    property get searchCriteria () : gw.lob.common.TerritoryLookupCriteria {
      return getCriteriaValue(1) as gw.lob.common.TerritoryLookupCriteria
    }
    
    property set searchCriteria ($arg :  gw.lob.common.TerritoryLookupCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get searchResults () : Territory[] {
      return getResultsValue(1) as Territory[]
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7TerritoryCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7TerritoryCodeSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, territoryCode :  TerritoryCode) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.WC7TerritoryCodeSearchPopup {
      return super.CurrentLocation as pcf.WC7TerritoryCodeSearchPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get territoryCode () : TerritoryCode {
      return getVariableValue("territoryCode", 0) as TerritoryCode
    }
    
    property set territoryCode ($arg :  TerritoryCode) {
      setVariableValue("territoryCode", 0, $arg)
    }
    
    
  }
  
  
}