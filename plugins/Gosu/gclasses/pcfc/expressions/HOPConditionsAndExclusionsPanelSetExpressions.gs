package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class HOPConditionsAndExclusionsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel2ExpressionsImpl extends HOPConditionsAndExclusionsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at HOPConditionsAndExclusionsPanelSet.pcf: line 73, column 62
    function initialValue_220 () : gw.api.productmodel.ExclusionPattern[] {
      return HOPClausePanelSetHelper.getLineExclusionPatterns(hopLine, openForEdit)
    }
    
    // 'initialValue' attribute on Variable at HOPConditionsAndExclusionsPanelSet.pcf: line 78, column 62
    function initialValue_221 () : gw.api.productmodel.ExclusionPattern[] {
      return HOPClausePanelSetHelper.getDwellingExclusionPatterns(dwelling, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at HOPConditionsAndExclusionsPanelSet.pcf: line 87, column 34
    function sortBy_222 (dwellingExclusionPattern :  gw.api.productmodel.ExclusionPattern) : java.lang.Object {
      return dwellingExclusionPattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at HOPConditionsAndExclusionsPanelSet.pcf: line 99, column 34
    function sortBy_331 (lineExclusionPattern :  gw.api.productmodel.ExclusionPattern) : java.lang.Object {
      return lineExclusionPattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=dwellingExclusionPatternIterator) at HOPConditionsAndExclusionsPanelSet.pcf: line 84, column 68
    function value_330 () : gw.api.productmodel.ExclusionPattern[] {
      return dwellingExclusionPatterns
    }
    
    // 'value' attribute on InputIterator (id=lineExclusionPatternIterator) at HOPConditionsAndExclusionsPanelSet.pcf: line 96, column 68
    function value_439 () : gw.api.productmodel.ExclusionPattern[] {
      return lineExclusionPatterns
    }
    
    property get dwellingExclusionPatterns () : gw.api.productmodel.ExclusionPattern[] {
      return getVariableValue("dwellingExclusionPatterns", 1) as gw.api.productmodel.ExclusionPattern[]
    }
    
    property set dwellingExclusionPatterns ($arg :  gw.api.productmodel.ExclusionPattern[]) {
      setVariableValue("dwellingExclusionPatterns", 1, $arg)
    }
    
    property get lineExclusionPatterns () : gw.api.productmodel.ExclusionPattern[] {
      return getVariableValue("lineExclusionPatterns", 1) as gw.api.productmodel.ExclusionPattern[]
    }
    
    property set lineExclusionPatterns ($arg :  gw.api.productmodel.ExclusionPattern[]) {
      setVariableValue("lineExclusionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends HOPConditionsAndExclusionsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at HOPConditionsAndExclusionsPanelSet.pcf: line 29, column 62
    function initialValue_0 () : gw.api.productmodel.ConditionPattern[] {
      return HOPClausePanelSetHelper.getLineConditionPatterns(hopLine, openForEdit)
    }
    
    // 'initialValue' attribute on Variable at HOPConditionsAndExclusionsPanelSet.pcf: line 34, column 62
    function initialValue_1 () : gw.api.productmodel.ConditionPattern[] {
      return HOPClausePanelSetHelper.getDwellingConditionPatterns(dwelling, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at HOPConditionsAndExclusionsPanelSet.pcf: line 55, column 34
    function sortBy_111 (lineConditionPattern :  gw.api.productmodel.ConditionPattern) : java.lang.Object {
      return lineConditionPattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at HOPConditionsAndExclusionsPanelSet.pcf: line 43, column 34
    function sortBy_2 (dwellingConditionPattern :  gw.api.productmodel.ConditionPattern) : java.lang.Object {
      return dwellingConditionPattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=dwellingConditionPatternIterator) at HOPConditionsAndExclusionsPanelSet.pcf: line 40, column 68
    function value_110 () : gw.api.productmodel.ConditionPattern[] {
      return dwellingConditionPatterns
    }
    
    // 'value' attribute on InputIterator (id=lineConditionPatternIterator) at HOPConditionsAndExclusionsPanelSet.pcf: line 52, column 68
    function value_219 () : gw.api.productmodel.ConditionPattern[] {
      return lineConditionPatterns
    }
    
    property get dwellingConditionPatterns () : gw.api.productmodel.ConditionPattern[] {
      return getVariableValue("dwellingConditionPatterns", 1) as gw.api.productmodel.ConditionPattern[]
    }
    
    property set dwellingConditionPatterns ($arg :  gw.api.productmodel.ConditionPattern[]) {
      setVariableValue("dwellingConditionPatterns", 1, $arg)
    }
    
    property get lineConditionPatterns () : gw.api.productmodel.ConditionPattern[] {
      return getVariableValue("lineConditionPatterns", 1) as gw.api.productmodel.ConditionPattern[]
    }
    
    property set lineConditionPatterns ($arg :  gw.api.productmodel.ConditionPattern[]) {
      setVariableValue("lineConditionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HOPConditionsAndExclusionsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get HOPClausePanelSetHelper () : gw.web.line.hop.policy.HOPClausePanelSetHelper {
      return getVariableValue("HOPClausePanelSetHelper", 0) as gw.web.line.hop.policy.HOPClausePanelSetHelper
    }
    
    property set HOPClausePanelSetHelper ($arg :  gw.web.line.hop.policy.HOPClausePanelSetHelper) {
      setVariableValue("HOPClausePanelSetHelper", 0, $arg)
    }
    
    property get dwelling () : HOPDwelling {
      return getRequireValue("dwelling", 0) as HOPDwelling
    }
    
    property set dwelling ($arg :  HOPDwelling) {
      setRequireValue("dwelling", 0, $arg)
    }
    
    property get hopLine () : productmodel.HOPLine {
      return getRequireValue("hopLine", 0) as productmodel.HOPLine
    }
    
    property set hopLine ($arg :  productmodel.HOPLine) {
      setRequireValue("hopLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_112 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_114 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_116 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_118 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_120 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_122 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_124 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_126 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_128 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_130 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_132 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_134 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_136 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_138 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_140 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_142 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_144 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_146 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_148 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_150 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_152 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_154 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_156 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_158 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_166 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_168 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_170 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_172 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_174 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_176 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_178 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_180 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_182 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_184 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_186 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_188 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_190 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_192 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_194 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_196 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_198 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_200 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_202 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_204 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_206 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_onEnter_216 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(lineConditionPattern, hopLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 58, column 63
    function mode_218 () : java.lang.Object {
      return lineConditionPattern.CodeIdentifier
    }
    
    property get lineConditionPattern () : gw.api.productmodel.ConditionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ConditionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_223 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_225 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_227 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_229 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_231 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_233 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_235 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_237 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_239 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_241 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_243 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_245 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_247 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_249 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_251 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_253 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_255 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_257 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_259 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_261 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_263 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_265 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_267 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_269 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_271 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_273 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_275 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_277 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_279 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_281 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_283 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_285 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_287 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_289 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_291 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_293 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_295 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_297 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_299 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_301 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_303 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_305 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_307 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_309 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_311 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_313 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_315 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_317 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_319 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_321 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_323 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_325 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_onEnter_327 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_226 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_228 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_230 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_232 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_234 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_236 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_238 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_240 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_242 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_244 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_246 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_248 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_250 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_252 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_254 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_256 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_258 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_260 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_262 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_264 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_266 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_268 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_270 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_272 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_274 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_276 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_278 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_280 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_282 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_284 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_286 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_288 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_290 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_292 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_294 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_296 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_298 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_300 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_302 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_304 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_306 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_308 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_310 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_312 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_314 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_316 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_318 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_320 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_322 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_324 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_326 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function def_refreshVariables_328 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(dwellingExclusionPattern, dwelling, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 90, column 67
    function mode_329 () : java.lang.Object {
      return dwellingExclusionPattern.CodeIdentifier
    }
    
    property get dwellingExclusionPattern () : gw.api.productmodel.ExclusionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ExclusionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_332 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_334 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_336 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_338 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_340 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_342 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_344 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_346 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_348 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_350 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_352 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_354 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_356 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_358 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_360 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_362 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_364 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_366 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_368 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_370 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_372 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_374 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_376 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_378 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_380 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_382 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_384 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_386 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_388 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_390 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_392 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_394 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_396 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_398 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_400 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_402 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_404 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_406 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_408 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_410 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_412 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_414 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_416 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_418 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_420 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_422 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_424 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_426 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_428 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_430 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_432 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_434 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_onEnter_436 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_333 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_335 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_337 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_339 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_341 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_343 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_345 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_347 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_349 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_351 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_353 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_355 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_357 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_359 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_361 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_363 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_365 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_367 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_369 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_371 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_373 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_375 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_377 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_379 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_381 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_383 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_385 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_387 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_389 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_391 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_393 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_395 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_397 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_399 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_401 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_403 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_405 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_407 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_409 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_411 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_413 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_415 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_417 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_419 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_421 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_423 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_425 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_427 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_429 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_431 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_433 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_435 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function def_refreshVariables_437 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(lineExclusionPattern, hopLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 102, column 63
    function mode_438 () : java.lang.Object {
      return lineExclusionPattern.CodeIdentifier
    }
    
    property get lineExclusionPattern () : gw.api.productmodel.ExclusionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ExclusionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/hop/policy/HOPConditionsAndExclusionsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_107 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_13 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_19 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_3 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_57 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_65 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_67 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_4 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(dwellingConditionPattern, dwelling, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at HOPConditionsAndExclusionsPanelSet.pcf: line 46, column 67
    function mode_109 () : java.lang.Object {
      return dwellingConditionPattern.CodeIdentifier
    }
    
    property get dwellingConditionPattern () : gw.api.productmodel.ConditionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ConditionPattern
    }
    
    
  }
  
  
}