package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/ContactUsageImpact_TDICWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactUsageImpact_TDICWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/ContactUsageImpact_TDICWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssociatedPoliciesLVExpressionsImpl extends ContactUsageImpact_TDICWorksheetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ContactUsageImpact_TDICWorksheet.pcf: line 56, column 39
    function filter_4 () : gw.api.filters.IFilter {
      return productCodeFilterSet.getAllFilter()
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ContactUsageImpact_TDICWorksheet.pcf: line 47, column 72
    function filters_3 () : gw.api.filters.IFilter[] {
      return new gw.policy.PolicyFilters().FilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ContactUsageImpact_TDICWorksheet.pcf: line 58, column 79
    function filters_5 () : gw.api.filters.IFilter[] {
      return productCodeFilterSet.getClaimPolicyPeriodFilters()
    }
    
    // 'initialValue' attribute on Variable at ContactUsageImpact_TDICWorksheet.pcf: line 36, column 53
    function initialValue_2 () : gw.account.ProductCodeFilterSet {
      return new gw.account.ProductCodeFilterSet(contact.AccountContacts.map(\ac -> ac.Account))
    }
    
    // 'value' attribute on DateCell (id=PolicyStarted_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 85, column 64
    function sortValue_10 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.findEarliestPeriodStart()
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 89, column 68
    function sortValue_11 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 93, column 66
    function sortValue_12 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PeriodEnd
    }
    
    // 'value' attribute on TextCell (id=PolicyAddress_TJT_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 98, column 51
    function sortValue_13 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PolicyAddress
    }
    
    // 'value' attribute on TextCell (id=PolicyAddressDesc_TJT_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 102, column 88
    function sortValue_14 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PrimaryNamedInsured.DisplayName
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 66, column 60
    function sortValue_6 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 71, column 57
    function sortValue_7 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.PolicyNumberDisplayString
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyType_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 77, column 58
    function sortValue_8 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.ProductCode
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 81, column 76
    function sortValue_9 (item :  entity.PolicyPeriod) : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PeriodDisplayStatus
    }
    
    // 'value' attribute on RowIterator at ContactUsageImpact_TDICWorksheet.pcf: line 41, column 67
    function value_46 () : java.util.List<entity.PolicyPeriod> {
      return passedInImpactedPolicyPeriods
    }
    
    property get productCodeFilterSet () : gw.account.ProductCodeFilterSet {
      return getVariableValue("productCodeFilterSet", 1) as gw.account.ProductCodeFilterSet
    }
    
    property set productCodeFilterSet ($arg :  gw.account.ProductCodeFilterSet) {
      setVariableValue("productCodeFilterSet", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/ContactUsageImpact_TDICWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactUsageImpact_TDICWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (contact :  Contact, passedInImpactedPolicyPeriods :  java.util.List<PolicyPeriod>) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at ContactUsageImpact_TDICWorksheet.pcf: line 29, column 70
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'label' attribute on Verbatim at ContactUsageImpact_TDICWorksheet.pcf: line 23, column 25
    function label_0 () : java.lang.String {
      return DisplayKey.get("TDIC.ThisContactIsBeingReused") 
    }
    
    // 'tabLabel' attribute on Worksheet (id=ContactUsageImpact_TDICWorksheet) at ContactUsageImpact_TDICWorksheet.pcf: line 9, column 60
    function tabLabel_48 () : java.lang.String {
      return DisplayKey.get("TDIC.Usage") 
    }
    
    // 'visible' attribute on DetailViewPanel at ContactUsageImpact_TDICWorksheet.pcf: line 108, column 73
    function visible_47 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    override property get CurrentLocation () : pcf.ContactUsageImpact_TDICWorksheet {
      return super.CurrentLocation as pcf.ContactUsageImpact_TDICWorksheet
    }
    
    property get contact () : Contact {
      return getVariableValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get passedInImpactedPolicyPeriods () : java.util.List<PolicyPeriod> {
      return getVariableValue("passedInImpactedPolicyPeriods", 0) as java.util.List<PolicyPeriod>
    }
    
    property set passedInImpactedPolicyPeriods ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("passedInImpactedPolicyPeriods", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/ContactUsageImpact_TDICWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AssociatedPoliciesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 66, column 60
    function action_15 () : void {
      AccountFileDoRetrievalForward.goInMain(item.Policy.Account.AccountNumber)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 71, column 57
    function action_20 () : void {
      PolicyFileForward.goInMain(item.PolicyNumber, item.PeriodStart)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 66, column 60
    function action_dest_16 () : pcf.api.Destination {
      return pcf.AccountFileDoRetrievalForward.createDestination(item.Policy.Account.AccountNumber)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 71, column 57
    function action_dest_21 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(item.PolicyNumber, item.PeriodStart)
    }
    
    // 'useArchivedStyle' attribute on Row at ContactUsageImpact_TDICWorksheet.pcf: line 61, column 64
    function useArchivedStyle_45 () : java.lang.Boolean {
      return item.PolicyTerm.CheckArchived
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 66, column 60
    function valueRoot_18 () : java.lang.Object {
      return item.Policy.Account
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 71, column 57
    function valueRoot_23 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=PolicyType_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 77, column 58
    function valueRoot_26 () : java.lang.Object {
      return item.Policy
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 81, column 76
    function valueRoot_29 () : java.lang.Object {
      return item.Policy.LatestBoundPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyAddressDesc_TJT_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 102, column 88
    function valueRoot_43 () : java.lang.Object {
      return item.Policy.LatestBoundPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 66, column 60
    function value_17 () : java.lang.String {
      return item.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 71, column 57
    function value_22 () : java.lang.String {
      return item.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextCell (id=PolicyType_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 77, column 58
    function value_25 () : gw.api.productmodel.Product {
      return item.Policy.Product
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 81, column 76
    function value_28 () : java.lang.String {
      return item.Policy.LatestBoundPeriod.PeriodDisplayStatus
    }
    
    // 'value' attribute on DateCell (id=PolicyStarted_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 85, column 64
    function value_31 () : java.util.Date {
      return item.Policy.findEarliestPeriodStart()
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 89, column 68
    function value_33 () : java.util.Date {
      return item.Policy.LatestBoundPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 93, column 66
    function value_36 () : java.util.Date {
      return item.Policy.LatestBoundPeriod.PeriodEnd
    }
    
    // 'value' attribute on TextCell (id=PolicyAddress_TJT_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 98, column 51
    function value_39 () : entity.PolicyAddress {
      return item.Policy.LatestBoundPeriod.PolicyAddress
    }
    
    // 'value' attribute on TextCell (id=PolicyAddressDesc_TJT_Cell) at ContactUsageImpact_TDICWorksheet.pcf: line 102, column 88
    function value_42 () : java.lang.String {
      return item.Policy.LatestBoundPeriod.PrimaryNamedInsured.DisplayName
    }
    
    property get item () : entity.PolicyPeriod {
      return getIteratedValue(2) as entity.PolicyPeriod
    }
    
    
  }
  
  
}