package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policyfile/PolicyFile_WC7StateCoverages.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_WC7StateCoveragesExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policyfile/PolicyFile_WC7StateCoverages.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_WC7StateCoveragesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=PolicyFile_WC7StateCoverages) at PolicyFile_WC7StateCoverages.pcf: line 11, column 74
    function afterEnter_2 () : void {
      gw.api.web.PebblesUtil.addWebMessages(CurrentLocation, policyPeriod.PolicyFileMessages)
    }
    
    // 'canVisit' attribute on Page (id=PolicyFile_WC7StateCoverages) at PolicyFile_WC7StateCoverages.pcf: line 11, column 74
    static function canVisit_3 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.view(policyPeriod) and perm.System.pfiledetails
    }
    
    // 'def' attribute on PanelRef at PolicyFile_WC7StateCoverages.pcf: line 28, column 100
    function def_onEnter_0 (def :  pcf.WC7StateCoverageCV) : void {
      def.onEnter(policyPeriod.WC7Line, policyPeriod.OpenForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_WC7StateCoverages.pcf: line 28, column 100
    function def_refreshVariables_1 (def :  pcf.WC7StateCoverageCV) : void {
      def.refreshVariables(policyPeriod.WC7Line, policyPeriod.OpenForEdit, jobWizardHelper)
    }
    
    // 'parent' attribute on Page (id=PolicyFile_WC7StateCoverages) at PolicyFile_WC7StateCoverages.pcf: line 11, column 74
    static function parent_4 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod, asOfDate)
    }
    
    override property get CurrentLocation () : pcf.PolicyFile_WC7StateCoverages {
      return super.CurrentLocation as pcf.PolicyFile_WC7StateCoverages
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getVariableValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setVariableValue("jobWizardHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}