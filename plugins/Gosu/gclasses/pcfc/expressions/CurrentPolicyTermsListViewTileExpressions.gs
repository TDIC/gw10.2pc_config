package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/overview/CurrentPolicyTermsListViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CurrentPolicyTermsListViewTileExpressions {
  @javax.annotation.Generated("config/web/pcf/account/overview/CurrentPolicyTermsListViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CurrentPolicyTermsListViewTileExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TileAction (id=RecalculateLossRatio) at CurrentPolicyTermsListViewTile.pcf: line 29, column 59
    function action_51 () : void {
      lossRatioHelper.recalculateLossRatio()
    }
    
    // 'viewMoreAction' attribute on ListViewTile (id=CurrentPolicyTermsListViewTile) at CurrentPolicyTermsListViewTile.pcf: line 12, column 61
    function action_53 () : void {
      currentPoliciesHelper.viewMore()
    }
    
    // 'filter' attribute on ToolbarFilterOption at CurrentPolicyTermsListViewTile.pcf: line 42, column 33
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.api.web.policy.ViewablePolicyPeriodQueryFilter()
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at CurrentPolicyTermsListViewTile.pcf: line 49, column 66
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.policy.PolicyFilters().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at CurrentPolicyTermsListViewTile.pcf: line 22, column 66
    function initialValue_49 () : gw.api.web.dashboard.ui.policy.CurrentPolicyHelper {
      return gw.api.web.dashboard.ui.policy.CurrentPolicyHelper.forAccount(account, lossRatioHelper)
    }
    
    // 'sortBy' attribute on IteratorSort at CurrentPolicyTermsListViewTile.pcf: line 54, column 24
    function sortBy_2 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PeriodStart
    }
    
    // 'value' attribute on TextCell (id=cancelReason_Cell) at CurrentPolicyTermsListViewTile.pcf: line 104, column 25
    function sortValue_10 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Cancellation?.CancelReasonCode?.DisplayName
    }
    
    // 'value' attribute on TextCell (id=cancelReasonDesc_Cell) at CurrentPolicyTermsListViewTile.pcf: line 110, column 25
    function sortValue_11 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Cancellation?.CancelReasonCode?.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Premium_Cell) at CurrentPolicyTermsListViewTile.pcf: line 115, column 25
    function sortValue_12 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TotalPremiumRPT
    }
    
    // 'value' attribute on TextCell (id=LossRatio_Cell) at CurrentPolicyTermsListViewTile.pcf: line 122, column 25
    function sortValue_13 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return gw.api.web.dashboard.ui.policy.PremiumHelper.lossRatioWithDate(policyPeriod)
    }
    
    // 'value' attribute on TextCell (id=Policy_Cell) at CurrentPolicyTermsListViewTile.pcf: line 63, column 25
    function sortValue_3 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at CurrentPolicyTermsListViewTile.pcf: line 69, column 31
    function sortValue_4 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.getSlice(policyPeriod.EditEffectiveDate).WC7LineExists ? policyPeriod.getSlice(policyPeriod.EditEffectiveDate).Policy.Product.DisplayName  : policyPeriod.getSlice(policyPeriod.EditEffectiveDate).Offering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=MLD_Cell) at CurrentPolicyTermsListViewTile.pcf: line 74, column 269
    function sortValue_5 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.getSlice(policyPeriod.EditEffectiveDate).WC7LineExists ? gw.api.web.dashboard.ui.policy.CurrentPolicyHelper.getMultiLineforWC_TDIC(policyPeriod.getSlice(policyPeriod.EditEffectiveDate)) : policyPeriod.MultiLineDiscount_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at CurrentPolicyTermsListViewTile.pcf: line 80, column 25
    function sortValue_6 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PeriodDisplayStatus
    }
    
    // 'value' attribute on TextCell (id=DatesEffective_Cell) at CurrentPolicyTermsListViewTile.pcf: line 86, column 25
    function sortValue_7 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return currentPoliciesHelper.effectiveDates(policyPeriod)
    }
    
    // 'value' attribute on DateCell (id=CancellationDate_Cell) at CurrentPolicyTermsListViewTile.pcf: line 92, column 25
    function sortValue_8 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.CancellationDate
    }
    
    // 'value' attribute on DateCell (id=orginalEffDate_Cell) at CurrentPolicyTermsListViewTile.pcf: line 98, column 25
    function sortValue_9 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.OriginalEffectiveDate
    }
    
    // 'value' attribute on RowIterator at CurrentPolicyTermsListViewTile.pcf: line 35, column 41
    function value_48 () : entity.PolicyPeriod[] {
      return currentPoliciesHelper.PolicyPeriods
    }
    
    // 'visible' attribute on TextCell (id=LossRatio_Cell) at CurrentPolicyTermsListViewTile.pcf: line 122, column 25
    function visible_14 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser
    }
    
    // 'viewMoreVisible' attribute on ListViewTile (id=CurrentPolicyTermsListViewTile) at CurrentPolicyTermsListViewTile.pcf: line 12, column 61
    function visible_52 () : java.lang.Boolean {
      return currentPoliciesHelper.ViewMoreVisible
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get currentPoliciesHelper () : gw.api.web.dashboard.ui.policy.CurrentPolicyHelper {
      return getVariableValue("currentPoliciesHelper", 0) as gw.api.web.dashboard.ui.policy.CurrentPolicyHelper
    }
    
    property set currentPoliciesHelper ($arg :  gw.api.web.dashboard.ui.policy.CurrentPolicyHelper) {
      setVariableValue("currentPoliciesHelper", 0, $arg)
    }
    
    property get lossRatioHelper () : gw.api.web.dashboard.ui.claims.LossRatioHelper {
      return getRequireValue("lossRatioHelper", 0) as gw.api.web.dashboard.ui.claims.LossRatioHelper
    }
    
    property set lossRatioHelper ($arg :  gw.api.web.dashboard.ui.claims.LossRatioHelper) {
      setRequireValue("lossRatioHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/overview/CurrentPolicyTermsListViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CurrentPolicyTermsListViewTileExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Policy_Cell) at CurrentPolicyTermsListViewTile.pcf: line 63, column 25
    function action_15 () : void {
      PolicyFileForward.go(policyPeriod.PolicyNumber, policyPeriod.PeriodStart)
    }
    
    // 'action' attribute on TextCell (id=Policy_Cell) at CurrentPolicyTermsListViewTile.pcf: line 63, column 25
    function action_dest_16 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(policyPeriod.PolicyNumber, policyPeriod.PeriodStart)
    }
    
    // 'useArchivedStyle' attribute on Row at CurrentPolicyTermsListViewTile.pcf: line 56, column 50
    function useArchivedStyle_47 () : java.lang.Boolean {
      return policyPeriod.Archived
    }
    
    // 'value' attribute on TextCell (id=Policy_Cell) at CurrentPolicyTermsListViewTile.pcf: line 63, column 25
    function valueRoot_18 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on DateCell (id=orginalEffDate_Cell) at CurrentPolicyTermsListViewTile.pcf: line 98, column 25
    function valueRoot_33 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=cancelReason_Cell) at CurrentPolicyTermsListViewTile.pcf: line 104, column 25
    function valueRoot_36 () : java.lang.Object {
      return policyPeriod.Cancellation?.CancelReasonCode
    }
    
    // 'value' attribute on TextCell (id=Policy_Cell) at CurrentPolicyTermsListViewTile.pcf: line 63, column 25
    function value_17 () : String {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at CurrentPolicyTermsListViewTile.pcf: line 69, column 31
    function value_20 () : String {
      return policyPeriod.getSlice(policyPeriod.EditEffectiveDate).WC7LineExists ? policyPeriod.getSlice(policyPeriod.EditEffectiveDate).Policy.Product.DisplayName  : policyPeriod.getSlice(policyPeriod.EditEffectiveDate).Offering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=MLD_Cell) at CurrentPolicyTermsListViewTile.pcf: line 74, column 269
    function value_22 () : java.lang.String {
      return policyPeriod.getSlice(policyPeriod.EditEffectiveDate).WC7LineExists ? gw.api.web.dashboard.ui.policy.CurrentPolicyHelper.getMultiLineforWC_TDIC(policyPeriod.getSlice(policyPeriod.EditEffectiveDate)) : policyPeriod.MultiLineDiscount_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at CurrentPolicyTermsListViewTile.pcf: line 80, column 25
    function value_24 () : java.lang.String {
      return policyPeriod.PeriodDisplayStatus
    }
    
    // 'value' attribute on TextCell (id=DatesEffective_Cell) at CurrentPolicyTermsListViewTile.pcf: line 86, column 25
    function value_27 () : java.lang.String {
      return currentPoliciesHelper.effectiveDates(policyPeriod)
    }
    
    // 'value' attribute on DateCell (id=CancellationDate_Cell) at CurrentPolicyTermsListViewTile.pcf: line 92, column 25
    function value_29 () : java.util.Date {
      return policyPeriod.CancellationDate
    }
    
    // 'value' attribute on DateCell (id=orginalEffDate_Cell) at CurrentPolicyTermsListViewTile.pcf: line 98, column 25
    function value_32 () : java.util.Date {
      return policyPeriod.Policy.OriginalEffectiveDate
    }
    
    // 'value' attribute on TextCell (id=cancelReason_Cell) at CurrentPolicyTermsListViewTile.pcf: line 104, column 25
    function value_35 () : java.lang.String {
      return policyPeriod.Cancellation?.CancelReasonCode?.DisplayName
    }
    
    // 'value' attribute on TextCell (id=cancelReasonDesc_Cell) at CurrentPolicyTermsListViewTile.pcf: line 110, column 25
    function value_38 () : java.lang.String {
      return policyPeriod.Cancellation?.CancelReasonCode?.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Premium_Cell) at CurrentPolicyTermsListViewTile.pcf: line 115, column 25
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalPremiumRPT
    }
    
    // 'value' attribute on TextCell (id=LossRatio_Cell) at CurrentPolicyTermsListViewTile.pcf: line 122, column 25
    function value_44 () : java.lang.String {
      return gw.api.web.dashboard.ui.policy.PremiumHelper.lossRatioWithDate(policyPeriod)
    }
    
    // 'visible' attribute on TextCell (id=LossRatio_Cell) at CurrentPolicyTermsListViewTile.pcf: line 122, column 25
    function visible_45 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  
}