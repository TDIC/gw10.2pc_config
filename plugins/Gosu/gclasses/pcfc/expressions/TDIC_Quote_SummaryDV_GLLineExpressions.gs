package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/common/TDIC_Quote_SummaryDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_Quote_SummaryDV_GLLineExpressions {
  @javax.annotation.Generated("config/web/pcf/job/common/TDIC_Quote_SummaryDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_Quote_SummaryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at TDIC_Quote_SummaryDV.GLLine.pcf: line 54, column 31
    function def_onEnter_19 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.onEnter(policyPeriod, true)
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at TDIC_Quote_SummaryDV.GLLine.pcf: line 54, column 31
    function def_refreshVariables_20 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.refreshVariables(policyPeriod, true)
    }
    
    // 'initialValue' attribute on Variable at TDIC_Quote_SummaryDV.GLLine.pcf: line 19, column 45
    function initialValue_0 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TransactionCostRPT
    }
    
    // 'initialValue' attribute on Variable at TDIC_Quote_SummaryDV.GLLine.pcf: line 27, column 23
    function initialValue_1 () : boolean {
      return quoteScreenHelper.isEREJob(policyPeriod, jobWizardHelper)
    }
    
    // 'label' attribute on TextInput (id=JobNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 41, column 51
    function label_8 () : java.lang.Object {
      return JobNumberLabel
    }
    
    // 'value' attribute on TextInput (id=JobNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 41, column 51
    function valueRoot_10 () : java.lang.Object {
      return policyPeriod.Job
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 51, column 65
    function valueRoot_17 () : java.lang.Object {
      return policyPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 35, column 63
    function valueRoot_4 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriod_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 46, column 194
    function value_14 () : java.lang.String {
      return gw.api.util.StringUtil.formatDate(policyPeriod.PeriodStart,"short") + " - " + gw.api.util.StringUtil.formatDate(policyPeriod.PeriodEnd,"short")
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 51, column 65
    function value_16 () : java.lang.String {
      return policyPeriod.PrimaryNamedInsured.DisplayName
    }
    
    // 'value' attribute on TextInput (id=UWCompany_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 62, column 35
    function value_22 () : entity.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on DateInput (id=transaction_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 87, column 63
    function value_27 () : java.util.Date {
      return policyPeriod.EditEffectiveDate
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 35, column 63
    function value_3 () : java.lang.String {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextInput (id=TransactionReasonTDIC_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 93, column 59
    function value_32 () : java.lang.String {
      return policyPeriod.Job.PolicyTransactionReason_TDIC
    }
    
    // 'value' attribute on TextInput (id=TransactionDescription_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 99, column 59
    function value_37 () : java.lang.String {
      return policyPeriod.Job.PolicyTransactionDescription_TDIC
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalPremium_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 106, column 108
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalPremium(policyPeriod, isEREJob)//policyPeriod.TotalPremiumRPT
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Taxes_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 113, column 118
    function value_43 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalTaxSurcharges(policyPeriod, isEREJob)//policyPeriod.TaxAndSurchargesRPT
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCost_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 120, column 102
    function value_45 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalCost(policyPeriod, isEREJob)//policyPeriod.TotalCostRPT
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ChangeInCost_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 129, column 102
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return transactionSum
    }
    
    // 'value' attribute on TextInput (id=JobNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 41, column 51
    function value_9 () : java.lang.String {
      return policyPeriod.Job.JobNumber
    }
    
    // 'visible' attribute on TextInput (id=PolicyNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 35, column 63
    function visible_2 () : java.lang.Boolean {
      return not policyPeriod.Job?.createsNewPolicy()
    }
    
    // 'visible' attribute on TextInput (id=UWCompany_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 62, column 35
    function visible_21 () : java.lang.Boolean {
      return perm.Role.view
    }
    
    // 'visible' attribute on TextInput (id=TransactionReasonTDIC_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 93, column 59
    function visible_31 () : java.lang.Boolean {
      return policyPeriod.Job typeis PolicyChange
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ChangeInCost_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 129, column 102
    function visible_47 () : java.lang.Boolean {
      return ShowCostChange and not isEREJob and not policyPeriod.validateERECyberChangeReason
    }
    
    // 'visible' attribute on TextInput (id=HeldSubjectToFinalAudit_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 137, column 71
    function visible_51 () : java.lang.Boolean {
      return policyPeriod.JobProcess.BillingSubjectToFinalAudit
    }
    
    // 'visible' attribute on TextInput (id=JobNumber_Input) at TDIC_Quote_SummaryDV.GLLine.pcf: line 41, column 51
    function visible_7 () : java.lang.Boolean {
      return JobNumberLabel.length > 0
    }
    
    property get isEREJob () : boolean {
      return getVariableValue("isEREJob", 0) as java.lang.Boolean
    }
    
    property set isEREJob ($arg :  boolean) {
      setVariableValue("isEREJob", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteScreenHelper () : tdic.web.pcf.helper.GLQuoteScreenHelper {
      return getVariableValue("quoteScreenHelper", 0) as tdic.web.pcf.helper.GLQuoteScreenHelper
    }
    
    property set quoteScreenHelper ($arg :  tdic.web.pcf.helper.GLQuoteScreenHelper) {
      setVariableValue("quoteScreenHelper", 0, $arg)
    }
    
    property get transactionSum () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("transactionSum", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set transactionSum ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("transactionSum", 0, $arg)
    }
    
    
    property get JobNumberLabel() : String
    {
      if ( policyPeriod.Job typeis Submission ) return DisplayKey.get("Web.SubmissionWizard.Quote.SubmissionNo")
      if ( policyPeriod.Job typeis Renewal )    return DisplayKey.get("Web.RenewalWizard.Quote.RenewalNo")
      return null
    }
    
    property get ShowCostChange() : boolean
    {
      return (policyPeriod.Job typeis Issuance)
          or (policyPeriod.Job typeis PolicyChange)
          or (policyPeriod.Job typeis Cancellation)
          or (policyPeriod.Job typeis Reinstatement)
    }
          
        
    
    
  }
  
  
}