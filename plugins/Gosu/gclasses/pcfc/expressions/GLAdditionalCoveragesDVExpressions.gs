package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GLAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GLAdditionalCoveragesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GLAdditionalCoveragesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at GLAdditionalCoveragesDV.pcf: line 13, column 35
    function initialValue_0 () : productmodel.GLLine {
      return policyline as GLLine
    }
    
    // 'initialValue' attribute on Variable at GLAdditionalCoveragesDV.pcf: line 17, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return glLine.Pattern.getCoverageCategoryByCodeIdentifier("PLOptionalCat_TDIC")
    }
    
    // 'initialValue' attribute on Variable at GLAdditionalCoveragesDV.pcf: line 22, column 53
    function initialValue_2 () : gw.api.productmodel.CoveragePattern[] {
      return glAdditionalRequiredCat.coveragePatternsForEntity(GeneralLiabilityLine).whereSelectedOrAvailable(glLine, CurrentLocation.InEditMode)
    }
    
    // 'sortBy' attribute on IteratorSort at GLAdditionalCoveragesDV.pcf: line 40, column 26
    function sortBy_3 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=glAdditionalRequiredCatIterator) at GLAdditionalCoveragesDV.pcf: line 37, column 59
    function value_112 () : gw.api.productmodel.CoveragePattern[] {
      return glAdditionalRequiredCatCoveragePatterns
    }
    
    property get glAdditionalRequiredCat () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("glAdditionalRequiredCat", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set glAdditionalRequiredCat ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("glAdditionalRequiredCat", 0, $arg)
    }
    
    property get glAdditionalRequiredCatCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("glAdditionalRequiredCatCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set glAdditionalRequiredCatCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("glAdditionalRequiredCatCoveragePatterns", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get policyline () : PolicyLine {
      return getRequireValue("policyline", 0) as PolicyLine
    }
    
    property set policyline ($arg :  PolicyLine) {
      setRequireValue("policyline", 0, $arg)
    }
    
    function isVisibleRight(covPattern:gw.api.productmodel.CoveragePattern):Boolean{
      if (covPattern.CodeIdentifier == "GLBLAICov_TDIC" || covPattern.CodeIdentifier == "GLAdditionalInsuredCov_TDIC" ||
            covPattern.CodeIdentifier == "GLMultiOwnerDPECov_TDIC")
          return true
      return false
    }
    
    function isQuickClaim(covPattern:gw.api.productmodel.CoveragePattern):Boolean{
      if(covPattern.CodeIdentifier == "GLIDTheftREcoveryCov_TDIC"){
        return true
      }else{
        return false
      }
    }
    
    function chkVisibilty(covPattern:gw.api.productmodel.CoveragePattern):Boolean{
      if(glLine.Branch.isQuickQuote_TDIC)
          return false
      return isVisibleRight(covPattern)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends GLAdditionalCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_115 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_117 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_119 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_121 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_123 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_125 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_127 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_129 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_131 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_133 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_135 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_137 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_139 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_141 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_143 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_145 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_147 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_149 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_151 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_153 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_155 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_157 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_159 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_161 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_163 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_165 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_167 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_169 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_171 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_173 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_175 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_177 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_179 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_181 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_183 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_185 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_187 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_189 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_191 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_193 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_195 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_197 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_199 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_201 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_203 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_205 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_207 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_209 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_211 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_213 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_215 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_onEnter_219 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_116 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_118 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_120 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_122 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_124 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_126 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_128 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_130 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_132 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_134 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_136 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_138 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_140 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_142 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_144 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_146 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_148 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'mode' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function mode_221 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    // 'visible' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 70, column 53
    function visible_114 () : java.lang.Boolean {
      return chkVisibilty(coveragePattern) 
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GLAdditionalCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_109 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_13 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_19 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_65 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_67 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'mode' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function mode_111 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    // 'visible' attribute on InputSetRef at GLAdditionalCoveragesDV.pcf: line 44, column 123
    function visible_4 () : java.lang.Boolean {
      return glLine.Branch.isQuickQuote_TDIC ? isQuickClaim(coveragePattern) : !isVisibleRight((coveragePattern))
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  
}