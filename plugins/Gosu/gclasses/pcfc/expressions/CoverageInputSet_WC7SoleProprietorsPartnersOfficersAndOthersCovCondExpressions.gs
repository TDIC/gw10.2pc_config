package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 46, column 50
    function available_71 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 77, column 72
    function conversionExpression_12 (PickedValue :  Contact) : entity.WC7IncludedOwnerOfficer {
      return addOwnerOfficerForContactOnCondition(PickedValue)
    }
    
    // 'editable' attribute on RowIterator at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 141, column 58
    function editable_32 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 22, column 36
    function initialValue_0 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 26, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 31, column 39
    function initialValue_2 () : entity.AccountContact[] {
      return theWC7Line.UnassignedOwnerOfficers
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 36, column 39
    function initialValue_3 () : entity.AccountContact[] {
      return theWC7Line.OwnerOfficerOtherCandidates
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 82, column 30
    function label_20 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 46, column 50
    function label_72 () : java.lang.Object {
      return conditionPattern.DisplayName
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 77, column 72
    function pickLocation_13 () : void {
      ContactSearchPopup.push(TC_OWNEROFFICER)
    }
    
    // 'onToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 46, column 50
    function setter_73 (VALUE :  java.lang.Boolean) : void {
      executeOnToggleActions(VALUE); 
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 90, column 34
    function sortBy_14 (ownerOfficer :  entity.AccountContact) : java.lang.Object {
      return ownerOfficer.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 116, column 34
    function sortBy_21 (otherContact :  entity.AccountContact) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 64, column 32
    function sortBy_7 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 148, column 47
    function sortValue_26 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function sortValue_27 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function sortValue_28 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.WC7ClassCode
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 177, column 51
    function sortValue_29 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 184, column 48
    function sortValue_30 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=Payroll_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 191, column 48
    function sortValue_31 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.Payroll
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 102, column 66
    function toCreateAndAdd_19 (CheckedValues :  Object[]) : java.lang.Object {
      return addAllOwnerOfficersOnCondition()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 141, column 58
    function toRemove_68 (policyOwnerOfficer :  entity.WC7IncludedOwnerOfficer) : void {
      theWC7Line.removeFromWC7PolicyOwnerOfficers(policyOwnerOfficer)
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 40, column 37
    function valueRoot_5 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewContact) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 61, column 49
    function value_11 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYOWNEROFFICER)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 87, column 53
    function value_17 () : entity.AccountContact[] {
      return unassignedOwnerOfficers
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 113, column 53
    function value_24 () : entity.AccountContact[] {
      return otherContacts
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 40, column 37
    function value_4 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 141, column 58
    function value_69 () : entity.WC7IncludedOwnerOfficer[] {
      return theWC7Line.IncludedOwnerOfficers
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 102, column 66
    function visible_18 () : java.lang.Boolean {
      return unassignedOwnerOfficers.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddOtherContact) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 108, column 52
    function visible_25 () : java.lang.Boolean {
      return otherContacts.Count > 0
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 46, column 50
    function visible_70 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 199, column 101
    function visible_76 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherContacts () : entity.AccountContact[] {
      return getVariableValue("otherContacts", 0) as entity.AccountContact[]
    }
    
    property set otherContacts ($arg :  entity.AccountContact[]) {
      setVariableValue("otherContacts", 0, $arg)
    }
    
    property get theWC7Line () : productmodel.WC7Line {
      return getVariableValue("theWC7Line", 0) as productmodel.WC7Line
    }
    
    property set theWC7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    property get unassignedOwnerOfficers () : entity.AccountContact[] {
      return getVariableValue("unassignedOwnerOfficers", 0) as entity.AccountContact[]
    }
    
    property set unassignedOwnerOfficers ($arg :  entity.AccountContact[]) {
      setVariableValue("unassignedOwnerOfficers", 0, $arg)
    }
    
    function addOwnerOfficerForContactOnCondition(aContact : entity.Contact) : entity.WC7IncludedOwnerOfficer {
      var newOfficer = theWC7Line.addIncludedOwnerOfficer(aContact, theWC7Line.WC7SoleProprietorsPartnersOfficersAndOthersCovCond)
      return newOfficer
    }
    
    function addAllOwnerOfficersOnCondition() : WC7IncludedOwnerOfficer[] {
      var resultOfficers = theWC7Line.addAllExistingOwnerOfficersToCondition(theWC7Line.WC7SoleProprietorsPartnersOfficersAndOthersCovCond)
      return resultOfficers.toTypedArray()
    }
    
    function isClassCodeEditable(anIncludedOwnerOfficer : WC7IncludedOwnerOfficer) : boolean {
      return (anIncludedOwnerOfficer.Jurisdiction != null) and 
        anIncludedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(anIncludedOwnerOfficer.Jurisdiction)).HasElements  
    }
    
    function executeOnToggleActions(value : boolean){
      var uiHelper = new gw.pcf.WC7CoverageInputSetUIHelper()
      uiHelper.toggleClause(theWC7Line, conditionPattern, value)
      uiHelper.syncConditionsIfRequired(coverable, openForEdit)
      
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 95, column 96
    function label_15 () : java.lang.Object {
      return ownerOfficer
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 95, column 96
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return addOwnerOfficerForContactOnCondition(ownerOfficer.Contact)
    }
    
    property get ownerOfficer () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 121, column 96
    function label_22 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 121, column 96
    function toCreateAndAdd_23 (CheckedValues :  Object[]) : java.lang.Object {
      return addOwnerOfficerForContactOnCondition(otherContact.Contact)
    }
    
    property get otherContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 148, column 47
    function action_33 () : void {
      EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 148, column 47
    function action_dest_34 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyOwnerOfficer, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 177, column 51
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.RelationshipTitle = (__VALUE_TO_SET as typekey.Relationship)
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 184, column 48
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7OwnershipPct = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=Payroll_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 191, column 48
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.Payroll = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function editable_45 () : java.lang.Boolean {
      return isClassCodeEditable(policyOwnerOfficer)
    }
    
    // 'filter' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 177, column 51
    function filter_58 (VALUE :  typekey.Relationship, VALUES :  typekey.Relationship[]) : java.lang.Boolean {
      return Relationship.TF_WC7OWNEROFFICERRELATIONSHIP.TypeKeys.contains(VALUE)
    }
    
    // 'optionLabel' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function optionLabel_49 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE as java.lang.String
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function valueRange_41 () : java.lang.Object {
      return theWC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function valueRange_50 () : java.lang.Object {
      return theWC7Line.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(policyOwnerOfficer.Jurisdiction))
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 148, column 47
    function valueRoot_36 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 148, column 47
    function value_35 () : java.lang.String {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function value_38 () : typekey.Jurisdiction {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function value_46 () : entity.WC7ClassCode {
      return policyOwnerOfficer.WC7ClassCode
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 177, column 51
    function value_55 () : typekey.Relationship {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 184, column 48
    function value_60 () : java.lang.Integer {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=Payroll_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 191, column 48
    function value_64 () : java.lang.Integer {
      return policyOwnerOfficer.Payroll
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function verifyValueRangeIsAllowedType_42 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function verifyValueRangeIsAllowedType_51 ($$arg :  entity.WC7ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 157, column 50
    function verifyValueRange_43 () : void {
      var __valueRangeArg = theWC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 170, column 30
    function verifyValueRange_52 () : void {
      var __valueRangeArg = theWC7Line.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(policyOwnerOfficer.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    property get policyOwnerOfficer () : entity.WC7IncludedOwnerOfficer {
      return getIteratedValue(1) as entity.WC7IncludedOwnerOfficer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 70, column 120
    function conversionExpression_9 (PickedValue :  WC7PolicyOwnerOfficer) : entity.WC7IncludedOwnerOfficer {
      return PickedValue as WC7IncludedOwnerOfficer
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 70, column 120
    function label_8 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7SoleProprietorsPartnersOfficersAndOthersCovCond.pcf: line 70, column 120
    function pickLocation_10 () : void {
      WC7NewOwnerOfficerPopup.push(theWC7Line.Branch.WC7Line, contactType, conditionPattern)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}