package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/EditPolicyAddressPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditPolicyAddressPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/EditPolicyAddressPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditPolicyAddressPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (period :  PolicyPeriod, isNew :  boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at EditPolicyAddressPopup.pcf: line 71, column 71
    function action_10 () : void {
      if (impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact, impactedPolicies_TDIC)}
    }
    
    // 'action' attribute on ToolbarButton (id=Standardize) at EditPolicyAddressPopup.pcf: line 63, column 41
    function action_7 () : void {
      addressList = helper.validateAddress(tmpAddress, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); helper.displayExceptions()
    }
    
    // 'available' attribute on ToolbarButton (id=Standardize) at EditPolicyAddressPopup.pcf: line 63, column 41
    function available_5 () : java.lang.Boolean {
      return standardizeVisible
    }
    
    // 'beforeCommit' attribute on Popup (id=EditPolicyAddressPopup) at EditPolicyAddressPopup.pcf: line 10, column 73
    function beforeCommit_46 (pickedValue :  java.lang.Object) : void {
      period.PolicyAddress.copyFromAddress(tmpAddress);  tmpAddress.remove(); helper.updateAddressStatus(period.PolicyAddress.Address)
    }
    
    // 'def' attribute on InputSetRef at EditPolicyAddressPopup.pcf: line 84, column 112
    function def_onEnter_13 (def :  pcf.AddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressInputSetAddressOwner(tmpAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at EditPolicyAddressPopup.pcf: line 87, column 41
    function def_onEnter_15 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.onEnter(tmpAddress)
    }
    
    // 'def' attribute on InputSetRef at EditPolicyAddressPopup.pcf: line 84, column 112
    function def_refreshVariables_14 (def :  pcf.AddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressInputSetAddressOwner(tmpAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at EditPolicyAddressPopup.pcf: line 87, column 41
    function def_refreshVariables_16 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.refreshVariables(tmpAddress)
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditPolicyAddressPopup.pcf: line 100, column 47
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      tmpAddress.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at EditPolicyAddressPopup.pcf: line 22, column 23
    function initialValue_0 () : Address {
      return initializePolicyAddressAndCreateTmpAddress()
    }
    
    // 'initialValue' attribute on Variable at EditPolicyAddressPopup.pcf: line 27, column 75
    function initialValue_1 () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> period.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at EditPolicyAddressPopup.pcf: line 32, column 33
    function initialValue_2 () : java.lang.Boolean {
      return period != null ? openForEditInit.get() : CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at EditPolicyAddressPopup.pcf: line 51, column 76
    function initialValue_3 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at EditPolicyAddressPopup.pcf: line 55, column 50
    function initialValue_4 () : java.util.List<PolicyPeriod> {
      return period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact == null ? null : period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.AssociationFinder.findLatestBoundPolicyPeriods().where( \ elt -> elt.Policy != period.Policy)
    }
    
    // 'label' attribute on Verbatim (id=ContactUsageImpact_TDIC) at EditPolicyAddressPopup.pcf: line 78, column 25
    function label_12 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisAddressMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // 'updateLabel' attribute on EditButtons at EditPolicyAddressPopup.pcf: line 66, column 61
    function label_9 () : java.lang.Object {
      return overrideVisible ? DisplayKey.get("Web.AccountFile.Locations.Override") : DisplayKey.get("Web.AccountFile.Locations.Update")
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditPolicyAddressPopup.pcf: line 131, column 56
    function sortValue_22 (suggestedAddress :  Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at EditPolicyAddressPopup.pcf: line 135, column 56
    function sortValue_23 (suggestedAddress :  Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at EditPolicyAddressPopup.pcf: line 139, column 48
    function sortValue_24 (suggestedAddress :  Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at EditPolicyAddressPopup.pcf: line 144, column 44
    function sortValue_25 (suggestedAddress :  Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at EditPolicyAddressPopup.pcf: line 148, column 54
    function sortValue_26 (suggestedAddress :  Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditPolicyAddressPopup.pcf: line 100, column 47
    function valueRoot_19 () : java.lang.Object {
      return tmpAddress
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditPolicyAddressPopup.pcf: line 100, column 47
    function value_17 () : java.lang.String {
      return tmpAddress.Description
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at EditPolicyAddressPopup.pcf: line 118, column 65
    function value_44 () : java.util.ArrayList<Address> {
      return addressList
    }
    
    // 'visible' attribute on Verbatim (id=ContactUsageImpact_TDIC) at EditPolicyAddressPopup.pcf: line 78, column 25
    function visible_11 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    // 'visible' attribute on TitleBar (id=SelectTitle) at EditPolicyAddressPopup.pcf: line 109, column 64
    function visible_21 () : java.lang.Boolean {
      return addressList ?.Count > 0 ? true : false
    }
    
    // 'updateVisible' attribute on EditButtons at EditPolicyAddressPopup.pcf: line 66, column 61
    function visible_8 () : java.lang.Boolean {
      return updateVisible or overrideVisible
    }
    
    override property get CurrentLocation () : pcf.EditPolicyAddressPopup {
      return super.CurrentLocation as pcf.EditPolicyAddressPopup
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.List<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.List<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get isNew () : boolean {
      return getVariableValue("isNew", 0) as java.lang.Boolean
    }
    
    property set isNew ($arg :  boolean) {
      setVariableValue("isNew", 0, $arg)
    }
    
    property get openForEdit () : java.lang.Boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  java.lang.Boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get openForEditInit () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return getVariableValue("openForEditInit", 0) as gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>
    }
    
    property set openForEditInit ($arg :  gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>) {
      setVariableValue("openForEditInit", 0, $arg)
    }
    
    property get overrideVisible () : Boolean {
      return getVariableValue("overrideVisible", 0) as Boolean
    }
    
    property set overrideVisible ($arg :  Boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getVariableValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get tmpAddress () : Address {
      return getVariableValue("tmpAddress", 0) as Address
    }
    
    property set tmpAddress ($arg :  Address) {
      setVariableValue("tmpAddress", 0, $arg)
    }
    
    property get updateVisible () : Boolean {
      return getVariableValue("updateVisible", 0) as Boolean
    }
    
    property set updateVisible ($arg :  Boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    function initializePolicyAddressAndCreateTmpAddress() : Address {
      if (isNew) {
        // we do this here because none of the page before/after triggers occur at the right time
        var addr = new Address();
        period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.addAddress(addr);
        period.PolicyAddress.assignToSource(addr);
      }
      return period.PolicyAddress.copyToNewAddress()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/EditPolicyAddressPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditPolicyAddressPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at EditPolicyAddressPopup.pcf: line 126, column 44
    function action_28 () : void {
      helper.replaceAddressWithSuggested(suggestedAddress,tmpAddress, true); addressList = helper.getAddressList(); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible() 
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditPolicyAddressPopup.pcf: line 131, column 56
    function valueRoot_30 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditPolicyAddressPopup.pcf: line 131, column 56
    function value_29 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at EditPolicyAddressPopup.pcf: line 135, column 56
    function value_32 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at EditPolicyAddressPopup.pcf: line 139, column 48
    function value_35 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at EditPolicyAddressPopup.pcf: line 144, column 44
    function value_38 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at EditPolicyAddressPopup.pcf: line 148, column 54
    function value_41 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : Address {
      return getIteratedValue(1) as Address
    }
    
    
  }
  
  
}