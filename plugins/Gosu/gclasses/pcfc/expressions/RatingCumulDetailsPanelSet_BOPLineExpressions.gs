package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/RatingCumulDetailsPanelSet.BOPLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RatingCumulDetailsPanelSet_BOPLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/RatingCumulDetailsPanelSet.BOPLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at RatingCumulDetailsPanelSet.BOPLine.pcf: line 94, column 63
    function def_onEnter_13 (def :  pcf.TDIC_BOPDeductibleDV) : void {
      def.onEnter(buildingOfProperty)
    }
    
    // 'def' attribute on PanelRef (id=PropertyPanel_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 97, column 39
    function def_onEnter_16 (def :  pcf.TDIC_BOPCoverageCostLV) : void {
      def.onEnter(buildingOfProperty, propertyCosts, bopQuoteHelper.wrapPropertyCosts(propertyCosts), DisplayKey.get("Web.Policy.BOP.SubTotal.Property.Premium"))
    }
    
    // 'def' attribute on PanelRef (id=LiabilityPanel_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 112, column 43
    function def_onEnter_19 (def :  pcf.TDIC_BOPCoverageCostLV) : void {
      def.onEnter(buildingOfProperty, liabilityCosts, bopQuoteHelper.wrapLiabilityCosts(liabilityCosts), DisplayKey.get("Web.Policy.BOP.SubTotal.Liability.Premium"))
    }
    
    // 'def' attribute on PanelRef at RatingCumulDetailsPanelSet.BOPLine.pcf: line 94, column 63
    function def_refreshVariables_14 (def :  pcf.TDIC_BOPDeductibleDV) : void {
      def.refreshVariables(buildingOfProperty)
    }
    
    // 'def' attribute on PanelRef (id=PropertyPanel_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 97, column 39
    function def_refreshVariables_17 (def :  pcf.TDIC_BOPCoverageCostLV) : void {
      def.refreshVariables(buildingOfProperty, propertyCosts, bopQuoteHelper.wrapPropertyCosts(propertyCosts), DisplayKey.get("Web.Policy.BOP.SubTotal.Property.Premium"))
    }
    
    // 'def' attribute on PanelRef (id=LiabilityPanel_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 112, column 43
    function def_refreshVariables_20 (def :  pcf.TDIC_BOPCoverageCostLV) : void {
      def.refreshVariables(buildingOfProperty, liabilityCosts, bopQuoteHelper.wrapLiabilityCosts(liabilityCosts), DisplayKey.get("Web.Policy.BOP.SubTotal.Liability.Premium"))
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 88, column 42
    function initialValue_11 () : Set<BOPCost> {
      return (buildingCoveragePremiumMap.get(buildingOfProperty).union(buildingOtherPremiumMap.get(buildingOfProperty))).byProperty_TDIC()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 92, column 42
    function initialValue_12 () : Set<BOPCost> {
      return (buildingCoveragePremiumMap.get(buildingOfProperty).union(buildingOtherPremiumMap.get(buildingOfProperty))).byLiability_TDIC()
    }
    
    // PanelIterator (id=buildingPropertyIterator) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 81, column 46
    function initializeVariables_21 () : void {
        propertyCosts = (buildingCoveragePremiumMap.get(buildingOfProperty).union(buildingOtherPremiumMap.get(buildingOfProperty))).byProperty_TDIC();
  liabilityCosts = (buildingCoveragePremiumMap.get(buildingOfProperty).union(buildingOtherPremiumMap.get(buildingOfProperty))).byLiability_TDIC();

    }
    
    // 'label' attribute on Verbatim at RatingCumulDetailsPanelSet.BOPLine.pcf: line 105, column 44
    function label_15 () : java.lang.String {
      return DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title.Property.Premium")//DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title")
    }
    
    // 'label' attribute on Verbatim at RatingCumulDetailsPanelSet.BOPLine.pcf: line 109, column 193
    function label_18 () : java.lang.String {
      return DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title.Liability.Premium")//DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title")
    }
    
    property get buildingOfProperty () : entity.BOPBuilding {
      return getIteratedValue(2) as entity.BOPBuilding
    }
    
    property get liabilityCosts () : Set<BOPCost> {
      return getVariableValue("liabilityCosts", 2) as Set<BOPCost>
    }
    
    property set liabilityCosts ($arg :  Set<BOPCost>) {
      setVariableValue("liabilityCosts", 2, $arg)
    }
    
    property get propertyCosts () : Set<BOPCost> {
      return getVariableValue("propertyCosts", 2) as Set<BOPCost>
    }
    
    property set propertyCosts ($arg :  Set<BOPCost>) {
      setVariableValue("propertyCosts", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/RatingCumulDetailsPanelSet.BOPLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=locationStandardPremium) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 117, column 42
    function def_onEnter_23 (def :  pcf.TDIC_BOPLocationStandardPremiumLV) : void {
      def.onEnter((locationCoveragePremiumMap.get(location).union(locationOtherPremiumMap.get(location))).getLocationStandardPremiums_TDIC(location.Branch.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on PanelRef (id=TaxesAndSurcharges_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 123, column 42
    function def_onEnter_25 (def :  pcf.TDIC_BOPLocationTaxesAndSurchargesLV) : void {
      def.onEnter((locationCoveragePremiumMap.get(location).union(locationOtherPremiumMap.get(location))).TaxSurcharges)
    }
    
    // 'def' attribute on PanelRef (id=locationStandardPremium) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 117, column 42
    function def_refreshVariables_24 (def :  pcf.TDIC_BOPLocationStandardPremiumLV) : void {
      def.refreshVariables((locationCoveragePremiumMap.get(location).union(locationOtherPremiumMap.get(location))).getLocationStandardPremiums_TDIC(location.Branch.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on PanelRef (id=TaxesAndSurcharges_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 123, column 42
    function def_refreshVariables_26 (def :  pcf.TDIC_BOPLocationTaxesAndSurchargesLV) : void {
      def.refreshVariables((locationCoveragePremiumMap.get(location).union(locationOtherPremiumMap.get(location))).TaxSurcharges)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 58, column 94
    function initialValue_7 () : java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>> {
      return locationCoveragePremiumMap.get(location).byFixedBuilding()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 66, column 94
    function initialValue_8 () : java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>> {
      return locationOtherPremiumMap.get(location).byFixedBuilding()
    }
    
    // PanelIterator (id=locationIterator) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 50, column 40
    function initializeVariables_27 () : void {
        buildingCoveragePremiumMap = locationCoveragePremiumMap.get(location).byFixedBuilding();
  buildingOtherPremiumMap = locationOtherPremiumMap.get(location).byFixedBuilding();

    }
    
    // 'sortBy' attribute on IteratorSort at RatingCumulDetailsPanelSet.BOPLine.pcf: line 84, column 30
    function sortBy_10 (buildingOfProperty :  entity.BOPBuilding) : java.lang.Object {
      return buildingOfProperty.FixedId
    }
    
    // 'title' attribute on TitleBar at RatingCumulDetailsPanelSet.BOPLine.pcf: line 69, column 146
    function title_9 () : java.lang.String {
      return DisplayKey.get("Web.Policy.BOP.Quote.Location.Title", location.Location.LocationNum, location.Location.CompactName)
    }
    
    // 'value' attribute on PanelIterator (id=buildingPropertyIterator) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 81, column 46
    function value_22 () : entity.BOPBuilding[] {
      return buildingCoveragePremiumMap.Keys.where(\ b -> b != null).toTypedArray()
    }
    
    property get bopQuoteHelper () : tdic.web.pcf.helper.BOPQuoteScreenHelper {
      return getVariableValue("bopQuoteHelper", 1) as tdic.web.pcf.helper.BOPQuoteScreenHelper
    }
    
    property set bopQuoteHelper ($arg :  tdic.web.pcf.helper.BOPQuoteScreenHelper) {
      setVariableValue("bopQuoteHelper", 1, $arg)
    }
    
    property get buildingCoveragePremiumMap () : java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>> {
      return getVariableValue("buildingCoveragePremiumMap", 1) as java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>>
    }
    
    property set buildingCoveragePremiumMap ($arg :  java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>>) {
      setVariableValue("buildingCoveragePremiumMap", 1, $arg)
    }
    
    property get buildingOtherPremiumMap () : java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>> {
      return getVariableValue("buildingOtherPremiumMap", 1) as java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>>
    }
    
    property set buildingOtherPremiumMap ($arg :  java.util.Map<entity.BOPBuilding, java.util.Set<entity.BOPCost>>) {
      setVariableValue("buildingOtherPremiumMap", 1, $arg)
    }
    
    property get location () : entity.BOPLocation {
      return getIteratedValue(1) as entity.BOPLocation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/RatingCumulDetailsPanelSet.BOPLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingCumulDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=TotalLocationPremium_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 132, column 39
    function def_onEnter_29 (def :  pcf.TDIC_BOPLocationsTotalPremiumLV) : void {
      def.onEnter(locationCoveragePremiumMap.Values.flatMap(\elt -> elt).AmountSum(period.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 38, column 36
    function def_onEnter_4 (def :  pcf.RatingOverrideButtonDV) : void {
      def.onEnter(period, period.BOPLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef (id=TotalLocationPremium_TDIC) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 132, column 39
    function def_refreshVariables_30 (def :  pcf.TDIC_BOPLocationsTotalPremiumLV) : void {
      def.refreshVariables(locationCoveragePremiumMap.Values.flatMap(\elt -> elt).AmountSum(period.PreferredSettlementCurrency))
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 38, column 36
    function def_refreshVariables_5 (def :  pcf.RatingOverrideButtonDV) : void {
      def.refreshVariables(period, period.BOPLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 21, column 91
    function initialValue_0 () : java.util.Map<java.lang.Boolean, java.util.Set<entity.BOPCost>> {
      return period.BOPLine.Costs.cast(BOPCost).toSet().byCoveragePremium()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 26, column 92
    function initialValue_1 () : java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>> {
      return coverageOrOtherPremiumMap.get(true).byFixedLocation()
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 31, column 51
    function initialValue_2 () : java.util.Set<entity.BOPCost> {
      return locationCoveragePremiumMap.get(null)
    }
    
    // 'initialValue' attribute on Variable at RatingCumulDetailsPanelSet.BOPLine.pcf: line 35, column 92
    function initialValue_3 () : java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>> {
      return coverageOrOtherPremiumMap.get(false).byFixedLocation()
    }
    
    // 'sortBy' attribute on IteratorSort at RatingCumulDetailsPanelSet.BOPLine.pcf: line 61, column 24
    function sortBy_6 (location :  entity.BOPLocation) : java.lang.Object {
      return location.FixedId
    }
    
    // 'value' attribute on PanelIterator (id=locationIterator) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 50, column 40
    function value_28 () : entity.BOPLocation[] {
      return locationCoveragePremiumMap.Keys.where(\ l -> l != null).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=premiumAmount_Cell) at RatingCumulDetailsPanelSet.BOPLine.pcf: line 155, column 27
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return period.BOPLine.Costs.AmountSum(period.PreferredSettlementCurrency)
    }
    
    property get coverageOrOtherPremiumMap () : java.util.Map<java.lang.Boolean, java.util.Set<entity.BOPCost>> {
      return getVariableValue("coverageOrOtherPremiumMap", 0) as java.util.Map<java.lang.Boolean, java.util.Set<entity.BOPCost>>
    }
    
    property set coverageOrOtherPremiumMap ($arg :  java.util.Map<java.lang.Boolean, java.util.Set<entity.BOPCost>>) {
      setVariableValue("coverageOrOtherPremiumMap", 0, $arg)
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get lineLevelCoveragePremiums () : java.util.Set<entity.BOPCost> {
      return getVariableValue("lineLevelCoveragePremiums", 0) as java.util.Set<entity.BOPCost>
    }
    
    property set lineLevelCoveragePremiums ($arg :  java.util.Set<entity.BOPCost>) {
      setVariableValue("lineLevelCoveragePremiums", 0, $arg)
    }
    
    property get locationCoveragePremiumMap () : java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>> {
      return getVariableValue("locationCoveragePremiumMap", 0) as java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>>
    }
    
    property set locationCoveragePremiumMap ($arg :  java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>>) {
      setVariableValue("locationCoveragePremiumMap", 0, $arg)
    }
    
    property get locationOtherPremiumMap () : java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>> {
      return getVariableValue("locationOtherPremiumMap", 0) as java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>>
    }
    
    property set locationOtherPremiumMap ($arg :  java.util.Map<entity.BOPLocation, java.util.Set<entity.BOPCost>>) {
      setVariableValue("locationOtherPremiumMap", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    
  }
  
  
}