package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/forms/WC7FormClassCodeSelectionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7FormClassCodeSelectionLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/forms/WC7FormClassCodeSelectionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7FormClassCodeSelectionLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7FormClassCodeSelectionLV.pcf: line 31, column 41
    function valueRoot_4 () : java.lang.Object {
      return WC7FormPatternClassCode
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7FormClassCodeSelectionLV.pcf: line 31, column 41
    function value_3 () : java.lang.String {
      return WC7FormPatternClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7FormClassCodeSelectionLV.pcf: line 36, column 41
    function value_6 () : java.lang.String {
      return WC7FormPatternClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at WC7FormClassCodeSelectionLV.pcf: line 41, column 45
    function value_9 () : typekey.Jurisdiction {
      return WC7FormPatternClassCode.Jurisdiction
    }
    
    property get WC7FormPatternClassCode () : entity.WC7FormPatternClassCode {
      return getIteratedValue(1) as entity.WC7FormPatternClassCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/forms/WC7FormClassCodeSelectionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7FormClassCodeSelectionLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7FormClassCodeSelectionLV.pcf: line 31, column 41
    function sortValue_0 (WC7FormPatternClassCode :  entity.WC7FormPatternClassCode) : java.lang.Object {
      return WC7FormPatternClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7FormClassCodeSelectionLV.pcf: line 36, column 41
    function sortValue_1 (WC7FormPatternClassCode :  entity.WC7FormPatternClassCode) : java.lang.Object {
      return WC7FormPatternClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at WC7FormClassCodeSelectionLV.pcf: line 41, column 45
    function sortValue_2 (WC7FormPatternClassCode :  entity.WC7FormPatternClassCode) : java.lang.Object {
      return WC7FormPatternClassCode.Jurisdiction
    }
    
    // 'toRemove' attribute on RowIterator at WC7FormClassCodeSelectionLV.pcf: line 23, column 52
    function toRemove_12 (WC7FormPatternClassCode :  entity.WC7FormPatternClassCode) : void {
      formPattern.removeFromWC7FormPatternClassCodes(WC7FormPatternClassCode)
    }
    
    // 'value' attribute on RowIterator at WC7FormClassCodeSelectionLV.pcf: line 23, column 52
    function value_13 () : entity.WC7FormPatternClassCode[] {
      return formPattern.WC7FormPatternClassCodes
    }
    
    property get formPattern () : FormPattern {
      return getRequireValue("formPattern", 0) as FormPattern
    }
    
    property set formPattern ($arg :  FormPattern) {
      setRequireValue("formPattern", 0, $arg)
    }
    
    
  }
  
  
}