package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_IntegerField_TotalBPPLimit_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'onChange' attribute on PostOnChange at QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf: line 26, column 64
    function onChange_0 () : void {
      if (onChangeBlock != null) onChangeBlock()
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf: line 24, column 288
    function requestValidationExpression_1 (VALUE :  java.lang.String) : java.lang.Object {
      return question.getLengthForQuestion_TDIC(VALUE?.toString().length)
    }
    
    // 'required' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf: line 24, column 288
    function required_2 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_TotalBPPLimit_TDIC.pcf: line 24, column 288
    function value_3 () : java.lang.String {
      if((answerContainer as PolicyLocation).BPPTotalLimit_TDIC == null ){return ""}else{return"$"+tdic.web.pcf.helper.GLCPUWQuestionAnswersCurrencyHelper.getUWQuestionCurrencyAnswerNoDecimal(question, (answerContainer as PolicyLocation).BPPTotalLimit_TDIC)}
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}