package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ClassesInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 302, column 29
    function action_133 () : void {
      WC7ClassCodeSearchPopup.push(wcCoveredEmployee.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, gw.pcf.WC7ClassesInputSetUIHelper.getExcludedClassCodeTypes(wcEmployee.GoverningLaw))
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 302, column 29
    function action_dest_134 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wcCoveredEmployee.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, gw.pcf.WC7ClassesInputSetUIHelper.getExcludedClassCodeTypes(wcEmployee.GoverningLaw))
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 292, column 30
    function available_124 () : java.lang.Boolean {
      return !wcCoveredEmployee.IfAnyExposureAndClearBasisAmount
    }
    
    // 'available' attribute on Reflect at WC7ClassesInputSet.default.pcf: line 305, column 37
    function available_131 (TRIGGER_INDEX :  int, VALUE :  entity.PolicyLocation) : java.lang.Object {
      return VALUE.State != null
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 280, column 30
    function defaultSetter_121 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcCoveredEmployee.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 292, column 30
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcCoveredEmployee.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 251, column 43
    function initialValue_108 () : entity.WC7ClassCode {
      return isNewTerm ? null : wcCoveredEmployee.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 256, column 42
    function initialValue_109 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wcCoveredEmployee, wcCoveredEmployee.LocationWM, addlVersionPreviousWC7ClassCode)
    }
    
    // RowIterator (id=CoveredEmployee) at WC7ClassesInputSet.default.pcf: line 247, column 53
    function initializeVariables_183 () : void {
        addlVersionPreviousWC7ClassCode = isNewTerm ? null : wcCoveredEmployee.BasedOn.ClassCode;
  addlVersionClassCodeShortDescs = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wcCoveredEmployee, wcCoveredEmployee.LocationWM, addlVersionPreviousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 302, column 29
    function inputConversion_135 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCode(VALUE, wcCoveredEmployee, addlVersionPreviousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at WC7ClassesInputSet.default.pcf: line 307, column 108
    function onChange_132 () : void {
      if (wcCoveredEmployee.ClassCode == null) addlVersionClassCodeShortDescs = {}
    }
    
    // 'onChange' attribute on PostOnChange at WC7ClassesInputSet.default.pcf: line 318, column 176
    function onChange_140 () : void {
      wcEmployee.ClassCode = gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCode(wcEmployee.ClassCode.Code, wcEmployee, previousWC7ClassCode)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 302, column 29
    function outputConversion_136 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'required' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 280, column 30
    function required_119 () : java.lang.Boolean {
      return typekey.DataSource_TDIC.get(ScriptParameters.TDIC_DataSource) == typekey.DataSource_TDIC.TC_POLICYCENTER
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function valueRange_115 () : java.lang.Object {
      return wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 316, column 29
    function valueRange_143 () : java.lang.Object {
      return stateConfig.getGoverningLaws(jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function valueRange_149 () : java.lang.Object {
      return addlVersionClassCodeShortDescs
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function valueRoot_114 () : java.lang.Object {
      return wcCoveredEmployee
    }
    
    // 'value' attribute on TextCell (id=coveredemployee_description_Cell) at WC7ClassesInputSet.default.pcf: line 334, column 30
    function valueRoot_155 () : java.lang.Object {
      return wcCoveredEmployee.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Period2_Cell) at WC7ClassesInputSet.default.pcf: line 263, column 52
    function value_110 () : java.lang.String {
      return gw.pcf.WC7ClassesInputSetUIHelper.getPeriodNumbers(wcCoveredEmployee, splitDates)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function value_113 () : entity.PolicyLocation {
      return wcCoveredEmployee.LocationWM
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 280, column 30
    function value_120 () : java.lang.Integer {
      return wcCoveredEmployee.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 292, column 30
    function value_126 () : java.lang.Integer {
      return wcCoveredEmployee.BasisAmount
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 302, column 29
    function value_137 () : entity.WC7ClassCode {
      return wcCoveredEmployee.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 316, column 29
    function value_141 () : typekey.WC7GoverningLaw {
      return wcCoveredEmployee.GoverningLaw
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function value_147 () : java.lang.String {
      return wcCoveredEmployee.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=coveredemployee_description_Cell) at WC7ClassesInputSet.default.pcf: line 334, column 30
    function value_154 () : java.lang.String {
      return wcCoveredEmployee.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at WC7ClassesInputSet.default.pcf: line 351, column 30
    function value_160 () : java.lang.String {
      return wcCoveredEmployee.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 360, column 30
    function value_165 () : java.math.BigDecimal {
      return wcCoveredEmployee.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 365, column 107
    function value_170 () : java.lang.Boolean {
      return wcCoveredEmployee.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7ClassesInputSet.default.pcf: line 385, column 33
    function value_174 () : java.util.Date {
      return wcCoveredEmployee.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7ClassesInputSet.default.pcf: line 391, column 33
    function value_177 () : java.util.Date {
      return wcCoveredEmployee.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=WC7ClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 395, column 53
    function value_180 () : java.lang.String {
      return wcCoveredEmployee.ClassCode.EffectiveDate + " " + wcCoveredEmployee.ClassCode.ExpirationDate + " " + wcCoveredEmployee.ClassCode.RegulatoryRate_TDIC
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function verifyValueRangeIsAllowedType_116 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function verifyValueRangeIsAllowedType_116 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function verifyValueRangeIsAllowedType_116 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 316, column 29
    function verifyValueRangeIsAllowedType_144 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 316, column 29
    function verifyValueRangeIsAllowedType_144 ($$arg :  typekey.WC7GoverningLaw[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function verifyValueRangeIsAllowedType_150 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function verifyValueRangeIsAllowedType_150 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 272, column 30
    function verifyValueRange_117 () : void {
      var __valueRangeArg = wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_116(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 316, column 29
    function verifyValueRange_145 () : void {
      var __valueRangeArg = stateConfig.getGoverningLaws(jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_144(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function verifyValueRange_151 () : void {
      var __valueRangeArg = addlVersionClassCodeShortDescs
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_150(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=Period2_Cell) at WC7ClassesInputSet.default.pcf: line 263, column 52
    function visible_111 () : java.lang.Boolean {
      return splitDates.Count > 2
    }
    
    // 'visible' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 328, column 30
    function visible_152 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'visible' attribute on TextCell (id=coveredemployee_description_Cell) at WC7ClassesInputSet.default.pcf: line 334, column 30
    function visible_156 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status != PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 360, column 30
    function visible_164 () : java.lang.Boolean {
      return wcEmployee.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 360, column 30
    function visible_167 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isRateColumnVisible(jurisdiction)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 365, column 107
    function visible_169 () : java.lang.Boolean {
      return wcCoveredEmployee.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 365, column 107
    function visible_172 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    // 'visible' attribute on TextCell (id=WC7ClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 395, column 53
    function visible_181 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    property get addlVersionClassCodeShortDescs () : java.lang.String[] {
      return getVariableValue("addlVersionClassCodeShortDescs", 2) as java.lang.String[]
    }
    
    property set addlVersionClassCodeShortDescs ($arg :  java.lang.String[]) {
      setVariableValue("addlVersionClassCodeShortDescs", 2, $arg)
    }
    
    property get addlVersionPreviousWC7ClassCode () : entity.WC7ClassCode {
      return getVariableValue("addlVersionPreviousWC7ClassCode", 2) as entity.WC7ClassCode
    }
    
    property set addlVersionPreviousWC7ClassCode ($arg :  entity.WC7ClassCode) {
      setVariableValue("addlVersionPreviousWC7ClassCode", 2, $arg)
    }
    
    property get wcCoveredEmployee () : entity.WC7CoveredEmployee {
      return getIteratedValue(2) as entity.WC7CoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7ClassesInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function action_47 () : void {
      WC7ClassCodeSearchPopup.push(wcEmployee.LocationWM, wc7Line, previousWC7ClassCode, gw.pcf.WC7ClassesInputSetUIHelper.getExcludedClassCodeTypes(wcEmployee.GoverningLaw))
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function action_dest_48 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wcEmployee.LocationWM, wc7Line, previousWC7ClassCode, gw.pcf.WC7ClassesInputSetUIHelper.getExcludedClassCodeTypes(wcEmployee.GoverningLaw))
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 106, column 28
    function available_38 () : java.lang.Boolean {
      return !wcEmployee.IfAnyExposureAndClearBasisAmount
    }
    
    // 'available' attribute on Reflect at WC7ClassesInputSet.default.pcf: line 123, column 35
    function available_45 (TRIGGER_INDEX :  int, VALUE :  entity.PolicyLocation) : java.lang.Object {
      return VALUE.State != null
    }
    
    // 'available' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function available_49 () : java.lang.Boolean {
      return wcEmployee.LocationWM != null
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.LocationWM = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 93, column 28
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 106, column 28
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.GoverningLaw = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.ClassCodeShortDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function defaultSetter_88 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.Rate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 195, column 33
    function defaultSetter_94 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcEmployee.SpecificDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function editable_24 () : java.lang.Boolean {
      return wcEmployee.canEditLocation(splitDates)
    }
    
    // 'editable' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function editable_66 () : java.lang.Boolean {
      return classCodeShortDescriptions.length > 1
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 58, column 41
    function initialValue_19 () : entity.WC7ClassCode {
      return isNewTerm ? null : wcEmployee.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 63, column 40
    function initialValue_20 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wcEmployee, wcEmployee.LocationWM, previousWC7ClassCode)
    }
    
    // RowIterator (id=EmployeeVersionList) at WC7ClassesInputSet.default.pcf: line 54, column 51
    function initializeVariables_185 () : void {
        previousWC7ClassCode = isNewTerm ? null : wcEmployee.BasedOn.ClassCode;
  classCodeShortDescriptions = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wcEmployee, wcEmployee.LocationWM, previousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function inputConversion_51 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCode(VALUE, wcEmployee, previousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at WC7ClassesInputSet.default.pcf: line 125, column 155
    function onChange_46 () : void {
      gw.pcf.WC7ClassesInputSetUIHelper.clearupRates(wcEmployee); if (wcEmployee.ClassCode == null) classCodeShortDescriptions = {}
    }
    
    // 'onChange' attribute on PostOnChange at WC7ClassesInputSet.default.pcf: line 138, column 174
    function onChange_58 () : void {
      wcEmployee.ClassCode = gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCode(wcEmployee.ClassCode.Code, wcEmployee, previousWC7ClassCode)
    }
    
    // 'onPick' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function onPick_50 (PickedValue :  WC7ClassCode) : void {
      gw.pcf.WC7ClassesInputSetUIHelper.clearupRates(wcEmployee)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function outputConversion_52 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'required' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 93, column 28
    function required_33 () : java.lang.Boolean {
      return typekey.DataSource_TDIC.get(ScriptParameters.TDIC_DataSource) == typekey.DataSource_TDIC.TC_POLICYCENTER
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function valueRange_28 () : java.lang.Object {
      return wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function valueRange_62 () : java.lang.Object {
      return stateConfig.getGoverningLaws(jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function valueRange_70 () : java.lang.Object {
      return classCodeShortDescriptions
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function valueRoot_27 () : java.lang.Object {
      return wcEmployee
    }
    
    // 'value' attribute on TextCell (id=description_quote_Cell) at WC7ClassesInputSet.default.pcf: line 157, column 28
    function valueRoot_77 () : java.lang.Object {
      return wcEmployee.ClassCode
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7ClassesInputSet.default.pcf: line 233, column 31
    function value_101 () : java.util.Date {
      return wcEmployee.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=WCClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 239, column 51
    function value_104 () : java.math.BigDecimal {
      return wcEmployee.ClassCode.RegulatoryRate_TDIC
    }
    
    // 'value' attribute on RowIterator (id=CoveredEmployee) at WC7ClassesInputSet.default.pcf: line 247, column 53
    function value_184 () : entity.WC7CoveredEmployee[] {
      return wcEmployee.AdditionalVersions.whereTypeIs(WC7CoveredEmployee)
    }
    
    // 'value' attribute on TextCell (id=Period1_Cell) at WC7ClassesInputSet.default.pcf: line 71, column 49
    function value_21 () : java.lang.String {
      return gw.pcf.WC7ClassesInputSetUIHelper.getPeriodNumbers(wcEmployee, splitDates)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function value_25 () : entity.PolicyLocation {
      return wcEmployee.LocationWM
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 93, column 28
    function value_34 () : java.lang.Integer {
      return wcEmployee.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7ClassesInputSet.default.pcf: line 106, column 28
    function value_40 () : java.lang.Integer {
      return wcEmployee.BasisAmount
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function value_53 () : entity.WC7ClassCode {
      return wcEmployee.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function value_59 () : typekey.WC7GoverningLaw {
      return wcEmployee.GoverningLaw
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function value_67 () : java.lang.String {
      return wcEmployee.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=description_quote_Cell) at WC7ClassesInputSet.default.pcf: line 157, column 28
    function value_76 () : java.lang.String {
      return wcEmployee.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at WC7ClassesInputSet.default.pcf: line 176, column 28
    function value_82 () : java.lang.String {
      return wcEmployee.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function value_87 () : java.math.BigDecimal {
      return wcEmployee.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 195, column 33
    function value_93 () : java.lang.Boolean {
      return wcEmployee.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7ClassesInputSet.default.pcf: line 226, column 31
    function value_98 () : java.util.Date {
      return wcEmployee.EffectiveDate
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function verifyValueRangeIsAllowedType_29 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function verifyValueRangeIsAllowedType_29 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function verifyValueRangeIsAllowedType_63 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function verifyValueRangeIsAllowedType_63 ($$arg :  typekey.WC7GoverningLaw[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function verifyValueRangeIsAllowedType_71 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function verifyValueRangeIsAllowedType_71 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function verifyValueRange_30 () : void {
      var __valueRangeArg = wc7Line.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function verifyValueRange_64 () : void {
      var __valueRangeArg = stateConfig.getGoverningLaws(jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_63(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function verifyValueRange_72 () : void {
      var __valueRangeArg = classCodeShortDescriptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_71(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=WCClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 239, column 51
    function visible_106 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    // 'visible' attribute on TextCell (id=Period1_Cell) at WC7ClassesInputSet.default.pcf: line 71, column 49
    function visible_22 () : java.lang.Boolean {
      return splitDates.Count > 2
    }
    
    // 'visible' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function visible_74 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'visible' attribute on TextCell (id=description_quote_Cell) at WC7ClassesInputSet.default.pcf: line 157, column 28
    function visible_78 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status != PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function visible_86 () : java.lang.Boolean {
      return wcEmployee.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function visible_90 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isRateColumnVisible(jurisdiction)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 195, column 33
    function visible_92 () : java.lang.Boolean {
      return wcEmployee.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 195, column 33
    function visible_96 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get classCodeShortDescriptions () : java.lang.String[] {
      return getVariableValue("classCodeShortDescriptions", 1) as java.lang.String[]
    }
    
    property set classCodeShortDescriptions ($arg :  java.lang.String[]) {
      setVariableValue("classCodeShortDescriptions", 1, $arg)
    }
    
    property get previousWC7ClassCode () : entity.WC7ClassCode {
      return getVariableValue("previousWC7ClassCode", 1) as entity.WC7ClassCode
    }
    
    property set previousWC7ClassCode ($arg :  entity.WC7ClassCode) {
      setVariableValue("previousWC7ClassCode", 1, $arg)
    }
    
    property get wcEmployee () : entity.WC7CoveredEmployee {
      return getIteratedValue(1) as entity.WC7CoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ClassesInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 20, column 23
    function initialValue_0 () : boolean {
      return wc7Line.Branch.Job.NewTerm
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 25, column 52
    function initialValue_1 () : java.util.List<java.util.Date> {
      return jurisdiction.SplitDates
    }
    
    // 'initialValue' attribute on Variable at WC7ClassesInputSet.default.pcf: line 29, column 23
    function initialValue_2 () : Boolean {
      return wc7Line.Branch.OpenForEdit ? tdic.pc.config.pcf.submission.TDIC_StateCoveragesHelper.createCoveredEmployeeObject(wc7Line, jurisdiction) : null
    }
    
    // 'value' attribute on TextCell (id=description_quote_Cell) at WC7ClassesInputSet.default.pcf: line 157, column 28
    function sortValue_10 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function sortValue_12 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.Rate
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7ClassesInputSet.default.pcf: line 226, column 31
    function sortValue_15 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7ClassesInputSet.default.pcf: line 233, column 31
    function sortValue_16 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=WCClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 239, column 51
    function sortValue_17 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.ClassCode.RegulatoryRate_TDIC
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7ClassesInputSet.default.pcf: line 84, column 28
    function sortValue_4 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.LocationWM
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at WC7ClassesInputSet.default.pcf: line 93, column 28
    function sortValue_5 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.NumEmployees
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at WC7ClassesInputSet.default.pcf: line 120, column 27
    function sortValue_6 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=WC7SpecialCoverage_Cell) at WC7ClassesInputSet.default.pcf: line 136, column 27
    function sortValue_7 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.GoverningLaw
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function sortValue_8 (wcEmployee :  entity.WC7CoveredEmployee) : java.lang.Object {
      return wcEmployee.ClassCodeShortDescription
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=EmployeeVersionList) at WC7ClassesInputSet.default.pcf: line 54, column 51
    function toCreateAndAdd_186 () : entity.WC7CoveredEmployee {
      return wc7Line.addCoveredEmployeeWM_TDIC(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
    }
    
    // 'toRemove' attribute on RowIterator (id=EmployeeVersionList) at WC7ClassesInputSet.default.pcf: line 54, column 51
    function toRemove_187 (wcEmployee :  entity.WC7CoveredEmployee) : void {
      wcEmployee.removeWM()
    }
    
    // 'value' attribute on RowIterator (id=EmployeeVersionList) at WC7ClassesInputSet.default.pcf: line 54, column 51
    function value_188 () : entity.WC7CoveredEmployee[] {
      return wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)
    }
    
    // 'visible' attribute on TextCell (id=description_quote_Cell) at WC7ClassesInputSet.default.pcf: line 157, column 28
    function visible_11 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status != PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at WC7ClassesInputSet.default.pcf: line 186, column 28
    function visible_13 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isRateColumnVisible(jurisdiction)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at WC7ClassesInputSet.default.pcf: line 195, column 33
    function visible_14 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    // 'visible' attribute on TextCell (id=WCClassInfo_TDIC_Cell) at WC7ClassesInputSet.default.pcf: line 239, column 51
    function visible_18 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    // 'visible' attribute on TextCell (id=Period1_Cell) at WC7ClassesInputSet.default.pcf: line 71, column 49
    function visible_3 () : java.lang.Boolean {
      return splitDates.Count > 2
    }
    
    // 'visible' attribute on RangeCell (id=Description_Cell) at WC7ClassesInputSet.default.pcf: line 150, column 28
    function visible_9 () : java.lang.Boolean {
      return wc7Line.AssociatedPolicyPeriod.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    property get createClass () : Boolean {
      return getVariableValue("createClass", 0) as Boolean
    }
    
    property set createClass ($arg :  Boolean) {
      setVariableValue("createClass", 0, $arg)
    }
    
    property get isNewTerm () : boolean {
      return getVariableValue("isNewTerm", 0) as java.lang.Boolean
    }
    
    property set isNewTerm ($arg :  boolean) {
      setVariableValue("isNewTerm", 0, $arg)
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get splitDates () : java.util.List<java.util.Date> {
      return getVariableValue("splitDates", 0) as java.util.List<java.util.Date>
    }
    
    property set splitDates ($arg :  java.util.List<java.util.Date>) {
      setVariableValue("splitDates", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getRequireValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setRequireValue("stateConfig", 0, $arg)
    }
    
    property get wc7Line () : WC7WorkersCompLine {
      return getRequireValue("wc7Line", 0) as WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  WC7WorkersCompLine) {
      setRequireValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  
}