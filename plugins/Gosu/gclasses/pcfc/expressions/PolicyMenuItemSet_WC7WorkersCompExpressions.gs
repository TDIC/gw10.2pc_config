package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policyfile/PolicyMenuItemSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyMenuItemSet_WC7WorkersCompExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policyfile/PolicyMenuItemSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_PolicyInfo) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 17, column 70
    function action_0 () : void {
      PolicyFile_PolicyInfo.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Pricing) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 37, column 67
    function action_10 () : void {
      PolicyFile_Pricing.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Endorsements) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 41, column 72
    function action_12 () : void {
      PolicyFile_Forms_WC7.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Locations) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 21, column 69
    function action_2 () : void {
      PolicyFile_Locations.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WCStateCoverages) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 25, column 77
    function action_4 () : void {
      PolicyFile_WC7StateCoverages.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WCLineCoverages) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 29, column 77
    function action_6 () : void {
      PolicyFile_WC7LineCoverages.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WC7Options) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 33, column 69
    function action_8 () : void {
      PolicyFile_WC7Options.go(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_PolicyInfo) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 17, column 70
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicyFile_PolicyInfo.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Pricing) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 37, column 67
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PolicyFile_Pricing.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Endorsements) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 41, column 72
    function action_dest_13 () : pcf.api.Destination {
      return pcf.PolicyFile_Forms_WC7.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_Locations) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 21, column 69
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicyFile_Locations.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WCStateCoverages) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 25, column 77
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PolicyFile_WC7StateCoverages.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WCLineCoverages) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 29, column 77
    function action_dest_7 () : pcf.api.Destination {
      return pcf.PolicyFile_WC7LineCoverages.createDestination(period, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=PolicyMenuItemSet_WC7Options) at PolicyMenuItemSet.WC7WorkersComp.pcf: line 33, column 69
    function action_dest_9 () : pcf.api.Destination {
      return pcf.PolicyFile_WC7Options.createDestination(period, asOfDate)
    }
    
    property get asOfDate () : java.util.Date {
      return getRequireValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setRequireValue("asOfDate", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    
  }
  
  
}