package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7OwnerOfficerInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7OwnerOfficerInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7OwnerOfficerInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7OwnerOfficerInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      includedOwnerOfficer.WC7ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on TextInput (id=Ownership_Input) at WC7OwnerOfficerInputSet.pcf: line 52, column 38
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7OwnershipPct = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=Relationship_Input) at WC7OwnerOfficerInputSet.pcf: line 59, column 41
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.RelationshipTitle = (__VALUE_TO_SET as typekey.Relationship)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'editable' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function editable_14 () : java.lang.Boolean {
      return isClassCodeEditable(includedOwnerOfficer)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Relationship_Input) at WC7OwnerOfficerInputSet.pcf: line 59, column 41
    function filter_32 (VALUE :  typekey.Relationship, VALUES :  typekey.Relationship[]) : java.lang.Boolean {
      return Relationship.TF_WC7OWNEROFFICERRELATIONSHIP.TypeKeys.contains(VALUE)
    }
    
    // 'initialValue' attribute on Variable at WC7OwnerOfficerInputSet.pcf: line 13, column 23
    function initialValue_0 () : boolean {
      return policyOwnerOfficer.isIncluded()
    }
    
    // 'initialValue' attribute on Variable at WC7OwnerOfficerInputSet.pcf: line 17, column 22
    function initialValue_1 () : String {
      return isIncluded ? DisplayKey.get("Java.ProductModel.Name.Condition") : DisplayKey.get("Java.ProductModel.Name.Exclusion")
    }
    
    // 'initialValue' attribute on Variable at WC7OwnerOfficerInputSet.pcf: line 21, column 39
    function initialValue_2 () : WC7IncludedOwnerOfficer {
      return policyOwnerOfficer typeis WC7IncludedOwnerOfficer ? policyOwnerOfficer : null
    }
    
    // 'label' attribute on TextInput (id=ScheduleParent_Input) at WC7OwnerOfficerInputSet.pcf: line 26, column 41
    function label_3 () : java.lang.Object {
      return scheduleLabel
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function valueRange_10 () : java.lang.Object {
      return policyOwnerOfficer.Branch.WC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function valueRange_19 () : java.lang.Object {
      return includedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(includedOwnerOfficer.Jurisdiction))
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function valueRoot_18 () : java.lang.Object {
      return includedOwnerOfficer
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function valueRoot_9 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function value_16 () : entity.WC7ClassCode {
      return includedOwnerOfficer.WC7ClassCode
    }
    
    // 'value' attribute on TextInput (id=Ownership_Input) at WC7OwnerOfficerInputSet.pcf: line 52, column 38
    function value_25 () : java.lang.Integer {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TypeKeyInput (id=Relationship_Input) at WC7OwnerOfficerInputSet.pcf: line 59, column 41
    function value_29 () : typekey.Relationship {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7OwnerOfficerInputSet.pcf: line 26, column 41
    function value_4 () : gw.api.domain.Clause {
      return getParentClause()
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function value_7 () : typekey.Jurisdiction {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function verifyValueRangeIsAllowedType_20 ($$arg :  entity.WC7ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7OwnerOfficerInputSet.pcf: line 35, column 40
    function verifyValueRange_12 () : void {
      var __valueRangeArg = policyOwnerOfficer.Branch.WC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function verifyValueRange_21 () : void {
      var __valueRangeArg = includedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(includedOwnerOfficer.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=ClassCode_Input) at WC7OwnerOfficerInputSet.pcf: line 46, column 29
    function visible_15 () : java.lang.Boolean {
      return isIncluded
    }
    
    property get includedOwnerOfficer () : WC7IncludedOwnerOfficer {
      return getVariableValue("includedOwnerOfficer", 0) as WC7IncludedOwnerOfficer
    }
    
    property set includedOwnerOfficer ($arg :  WC7IncludedOwnerOfficer) {
      setVariableValue("includedOwnerOfficer", 0, $arg)
    }
    
    property get isIncluded () : boolean {
      return getVariableValue("isIncluded", 0) as java.lang.Boolean
    }
    
    property set isIncluded ($arg :  boolean) {
      setVariableValue("isIncluded", 0, $arg)
    }
    
    property get policyOwnerOfficer () : WC7PolicyOwnerOfficer {
      return getRequireValue("policyOwnerOfficer", 0) as WC7PolicyOwnerOfficer
    }
    
    property set policyOwnerOfficer ($arg :  WC7PolicyOwnerOfficer) {
      setRequireValue("policyOwnerOfficer", 0, $arg)
    }
    
    property get scheduleLabel () : String {
      return getVariableValue("scheduleLabel", 0) as String
    }
    
    property set scheduleLabel ($arg :  String) {
      setVariableValue("scheduleLabel", 0, $arg)
    }
    
    function isClassCodeEditable(anIncludedOwnerOfficer : WC7IncludedOwnerOfficer) : boolean {
      return (anIncludedOwnerOfficer.Jurisdiction != null) and 
        (anIncludedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(anIncludedOwnerOfficer.Jurisdiction)).HasElements)
    }
    
    function getParentClause() : gw.api.domain.Clause {
      var clauseValue : gw.api.domain.Clause = null
      if (policyOwnerOfficer typeis entity.WC7IncludedOwnerOfficer){
        clauseValue = policyOwnerOfficer.OwnerOfficerCondition
      } else if (policyOwnerOfficer typeis entity.WC7ExcludedOwnerOfficer){
        clauseValue = policyOwnerOfficer.OwnerOfficerExclusion
      }
      return clauseValue
    }
    
    
  }
  
  
}