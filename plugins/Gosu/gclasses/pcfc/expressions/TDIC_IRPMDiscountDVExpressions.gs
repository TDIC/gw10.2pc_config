package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_IRPMDiscountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_IRPMDiscountDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_IRPMDiscountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_IRPMDiscountDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_109 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_13 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_19 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_65 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_67 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'def' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, true)
    }
    
    // 'mode' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function mode_111 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    // 'visible' attribute on InputSetRef at TDIC_IRPMDiscountDV.pcf: line 40, column 130
    function visible_4 () : java.lang.Boolean {
      return chkVisibility(coveragePattern) && coveragePattern.CodeIdentifier == "GLIRPMDiscount_TDIC"
    }
    
    property get coveragePattern () : gw.api.productmodel.ConditionPattern {
      return getIteratedValue(1) as gw.api.productmodel.ConditionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_IRPMDiscountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_IRPMDiscountDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_IRPMDiscountDV.pcf: line 17, column 35
    function initialValue_0 () : productmodel.GLLine {
      return policyline as GLLine
    }
    
    // 'initialValue' attribute on Variable at TDIC_IRPMDiscountDV.pcf: line 21, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return glLine.Pattern.getCoverageCategoryByCodeIdentifier("GLDiscountsCat_TDIC")
    }
    
    // 'initialValue' attribute on Variable at TDIC_IRPMDiscountDV.pcf: line 26, column 54
    function initialValue_2 () : gw.api.productmodel.ConditionPattern[] {
      return glDiscountRequiredCat.conditionPatternsForEntity(GeneralLiabilityLine).whereSelectedOrAvailable(glLine, CurrentLocation.InEditMode).sortBy(\ cond -> cond.Priority)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_IRPMDiscountDV.pcf: line 36, column 26
    function sortBy_3 (coveragePattern :  gw.api.productmodel.ConditionPattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=glAdditionalRequiredCatIterator1) at TDIC_IRPMDiscountDV.pcf: line 33, column 60
    function value_112 () : gw.api.productmodel.ConditionPattern[] {
      return glDiscountConditionPatterns//getDscountsFor(glDiscountConditionPatterns, 6)
    }
    
    // 'visible' attribute on DetailViewPanel (id=TDIC_IRPMDiscountDV) at TDIC_IRPMDiscountDV.pcf: line 7, column 127
    function visible_113 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser and (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic)
    }
    
    property get glDiscountConditionPatterns () : gw.api.productmodel.ConditionPattern[] {
      return getVariableValue("glDiscountConditionPatterns", 0) as gw.api.productmodel.ConditionPattern[]
    }
    
    property set glDiscountConditionPatterns ($arg :  gw.api.productmodel.ConditionPattern[]) {
      setVariableValue("glDiscountConditionPatterns", 0, $arg)
    }
    
    property get glDiscountRequiredCat () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("glDiscountRequiredCat", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set glDiscountRequiredCat ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("glDiscountRequiredCat", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get policyline () : PolicyLine {
      return getRequireValue("policyline", 0) as PolicyLine
    }
    
    property set policyline ($arg :  PolicyLine) {
      setRequireValue("policyline", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getRequireValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    function getDscountsFor(patterns : gw.api.productmodel.ConditionPattern[],  numOfPatterns: Integer = null) : gw.api.productmodel.ConditionPattern[] {
          if(patterns != null and patterns.Count > 0) {
            if(numOfPatterns != null) {
              return patterns.limit(numOfPatterns)
            }
            return patterns.subtract(patterns.limit(6)).toTypedArray()
          }
          return {}
        }
    
        function chkVisibility(covPattern:gw.api.productmodel.ConditionPattern):Boolean{
          if(quoteType==QuoteType.TC_QUICK && (covPattern.CodeIdentifier == "GLServiceMembersCRADiscount_TDIC" || covPattern.CodeIdentifier == "GLTempDisDiscount_TDIC" || covPattern.CodeIdentifier == "GLCovExtDiscount_TDIC")) return false
          return true
        }
    
    
  }
  
  
}