package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactRolePanelSet_WC7PolicyLaborClientExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyContactRolePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=LaborClientEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 50, column 73
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborClientDetail.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=LaborClientExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 56, column 74
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborClientDetail.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7PolicyLaborClientDetail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function valueRange_11 () : java.lang.Object {
      return wc7PolicyLaborClientDetail.WC7LaborContact.WC7WorkersCompLine.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborClientInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 35, column 46
    function valueRoot_6 () : java.lang.Object {
      return wc7PolicyLaborClientDetail
    }
    
    // 'value' attribute on DateCell (id=LaborClientEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 50, column 73
    function value_15 () : java.util.Date {
      return wc7PolicyLaborClientDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=LaborClientExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 56, column 74
    function value_19 () : java.util.Date {
      return wc7PolicyLaborClientDetail.ContractExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborClientInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 35, column 46
    function value_5 () : typekey.Inclusion {
      return wc7PolicyLaborClientDetail.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function value_8 () : typekey.Jurisdiction {
      return wc7PolicyLaborClientDetail.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function verifyValueRange_13 () : void {
      var __valueRangeArg = wc7PolicyLaborClientDetail.WC7LaborContact.WC7WorkersCompLine.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    property get wc7PolicyLaborClientDetail () : entity.WC7LaborContactDetail {
      return getIteratedValue(1) as entity.WC7LaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactRolePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 14, column 43
    function initialValue_0 () : entity.WC7PolicyLaborClient {
      return policyContactRole as WC7PolicyLaborClient
    }
    
    // 'value' attribute on TypeKeyCell (id=LaborClientInclusion_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 35, column 46
    function sortValue_1 (wc7PolicyLaborClientDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborClientDetail.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=LaborClientState_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 44, column 49
    function sortValue_2 (wc7PolicyLaborClientDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborClientDetail.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=LaborClientEffectiveDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 50, column 73
    function sortValue_3 (wc7PolicyLaborClientDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborClientDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=LaborClientExpirationDate_Cell) at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 56, column 74
    function sortValue_4 (wc7PolicyLaborClientDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return wc7PolicyLaborClientDetail.ContractExpirationDate
    }
    
    // 'value' attribute on RowIterator at PolicyContactRolePanelSet.WC7PolicyLaborClient.pcf: line 28, column 54
    function value_23 () : entity.WC7LaborContactDetail[] {
      return wc7PolicyLaborClient.LaborContactDetails
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    property get wc7PolicyLaborClient () : entity.WC7PolicyLaborClient {
      return getVariableValue("wc7PolicyLaborClient", 0) as entity.WC7PolicyLaborClient
    }
    
    property set wc7PolicyLaborClient ($arg :  entity.WC7PolicyLaborClient) {
      setVariableValue("wc7PolicyLaborClient", 0, $arg)
    }
    
    
  }
  
  
}