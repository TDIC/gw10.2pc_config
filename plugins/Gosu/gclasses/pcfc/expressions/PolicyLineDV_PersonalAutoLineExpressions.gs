package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/pa/policy/PolicyLineDV.PersonalAutoLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyLineDV_PersonalAutoLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PolicyLineDV.PersonalAutoLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PolicyLineDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at PolicyLineDV.PersonalAutoLine.pcf: line 50, column 40
    function label_113 () : java.lang.String {
      return vehicle.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 58, column 28
    function sortBy_114 (coverage :  entity.PersonalVehicleCov) : java.lang.Object {
      return coverage.Pattern.CoverageCategory.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 61, column 28
    function sortBy_115 (coverage :  entity.PersonalVehicleCov) : java.lang.Object {
      return coverage.Pattern.CoverageCategory.PublicID
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 64, column 28
    function sortBy_116 (coverage :  entity.PersonalVehicleCov) : java.lang.Object {
      return coverage.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 67, column 28
    function sortBy_117 (coverage :  entity.PersonalVehicleCov) : java.lang.Object {
      return coverage.Pattern.Name
    }
    
    // 'value' attribute on InputIterator (id=CoveragesIterator) at PolicyLineDV.PersonalAutoLine.pcf: line 55, column 51
    function value_225 () : entity.PersonalVehicleCov[] {
      return vehicle.Coverages
    }
    
    property get vehicle () : entity.PersonalVehicle {
      return getIteratedValue(1) as entity.PersonalVehicle
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PolicyLineDV.PersonalAutoLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntry2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_118 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_120 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_122 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_124 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_126 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_128 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_130 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_132 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_134 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_136 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_138 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_140 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_142 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_144 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_146 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_148 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_150 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_152 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_154 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_156 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_158 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_166 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_168 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_170 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_172 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_174 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_176 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_178 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_180 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_182 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_184 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_186 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_188 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_190 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_192 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_194 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_196 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_198 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_200 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_202 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_204 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_206 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_216 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_218 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_220 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_onEnter_222 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_219 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_221 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function def_refreshVariables_223 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coverage.Pattern, vehicle, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 70, column 47
    function mode_224 () : java.lang.Object {
      return coverage.Pattern.PublicID
    }
    
    property get coverage () : entity.PersonalVehicleCov {
      return getIteratedValue(2) as entity.PersonalVehicleCov
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PolicyLineDV.PersonalAutoLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyLineDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_109 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_13 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_19 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_65 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_67 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coverage.Pattern, paLine, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineDV.PersonalAutoLine.pcf: line 40, column 45
    function mode_111 () : java.lang.Object {
      return coverage.Pattern.PublicID
    }
    
    property get coverage () : entity.PersonalAutoCov {
      return getIteratedValue(1) as entity.PersonalAutoCov
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PolicyLineDV.PersonalAutoLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyLineDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.PersonalAutoLine.pcf: line 14, column 32
    function initialValue_0 () : PersonalAutoLine {
      return policyLine as PersonalAutoLine
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 28, column 26
    function sortBy_1 (coverage :  entity.PersonalAutoCov) : java.lang.Object {
      return coverage.Pattern.CoverageCategory.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 31, column 26
    function sortBy_2 (coverage :  entity.PersonalAutoCov) : java.lang.Object {
      return coverage.Pattern.CoverageCategory.PublicID
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 34, column 26
    function sortBy_3 (coverage :  entity.PersonalAutoCov) : java.lang.Object {
      return coverage.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.PersonalAutoLine.pcf: line 37, column 26
    function sortBy_4 (coverage :  entity.PersonalAutoCov) : java.lang.Object {
      return coverage.Pattern.Name
    }
    
    // 'value' attribute on InputIterator at PolicyLineDV.PersonalAutoLine.pcf: line 25, column 46
    function value_112 () : entity.PersonalAutoCov[] {
      return paLine.PALineCoverages
    }
    
    // 'value' attribute on InputIterator (id=EUs) at PolicyLineDV.PersonalAutoLine.pcf: line 48, column 46
    function value_226 () : entity.PersonalVehicle[] {
      return paLine.Vehicles
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get paLine () : PersonalAutoLine {
      return getVariableValue("paLine", 0) as PersonalAutoLine
    }
    
    property set paLine ($arg :  PersonalAutoLine) {
      setVariableValue("paLine", 0, $arg)
    }
    
    property get policyLine () : PolicyLine {
      return getRequireValue("policyLine", 0) as PolicyLine
    }
    
    property set policyLine ($arg :  PolicyLine) {
      setRequireValue("policyLine", 0, $arg)
    }
    
    
  }
  
  
}