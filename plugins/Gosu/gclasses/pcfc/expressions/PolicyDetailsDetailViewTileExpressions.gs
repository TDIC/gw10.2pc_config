package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/summary/PolicyDetailsDetailViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailsDetailViewTileExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/summary/PolicyDetailsDetailViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyDetailsDetailViewTileExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TileAction (id=item) at PolicyDetailsDetailViewTile.pcf: line 62, column 72
    function action_14 () : void {
      JobForward.go(cancellationJob, cancellationJob.PolicyPeriod)
    }
    
    // 'action' attribute on TileAction (id=item) at PolicyDetailsDetailViewTile.pcf: line 62, column 72
    function action_dest_15 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(cancellationJob, cancellationJob.PolicyPeriod)
    }
    
    // 'label' attribute on TileAction (id=item) at PolicyDetailsDetailViewTile.pcf: line 62, column 72
    function label_16 () : java.lang.Object {
      return DisplayKey.get('Web.PolicyFile.RescindCancellationFull', gw.web.policyfile.PolicyFileMenuActionsUIHelper.getCancellationLabel(cancellationJob))
    }
    
    // 'visible' attribute on TileAction (id=item) at PolicyDetailsDetailViewTile.pcf: line 62, column 72
    function visible_13 () : java.lang.Boolean {
      return perm.System.cancelrescind and not period.Archived
    }
    
    property get cancellationJob () : entity.Cancellation {
      return getIteratedValue(1) as entity.Cancellation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/summary/PolicyDetailsDetailViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailsDetailViewTileExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function actionAvailable_80 () : java.lang.Boolean {
      return gw.api.web.dashboard.ui.DashboardParameters.PolicySummaryPermissions.Instance.canSeePolicy(period.Policy.RewrittenToNewAccountSource.Periods.first())
    }
    
    // 'actionAvailable' attribute on TextInput (id=RewrittenTargetPolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 179, column 92
    function actionAvailable_88 () : java.lang.Boolean {
      return gw.api.web.dashboard.ui.DashboardParameters.PolicySummaryPermissions.Instance.canSeePolicy(period.Policy.RewrittenToNewAccountDestination.Periods.first())
    }
    
    // 'action' attribute on TextInput (id=PreRenewalDirection_Input) at PolicyDetailsDetailViewTile.pcf: line 196, column 66
    function action_102 () : void {
      PreRenewalDirectionPage.go(period, period.Policy, asOfDate)
    }
    
    // 'action' attribute on TileAction (id=CancelPolicy) at PolicyDetailsDetailViewTile.pcf: line 52, column 127
    function action_11 () : void {
      StartCancellation.go(period)
    }
    
    // 'action' attribute on TileAction (id=ReinstatePolicy) at PolicyDetailsDetailViewTile.pcf: line 68, column 106
    function action_19 () : void {
      gw.web.policyfile.PolicyFileMenuActionsUIHelper.startReinstatement(period, CurrentLocation)
    }
    
    // 'action' attribute on TileAction (id=RewriteFullTerm) at PolicyDetailsDetailViewTile.pcf: line 73, column 185
    function action_21 () : void {
      uiHelper.startRewrite(TC_REWRITEFULLTERM, inForcePeriod, CurrentLocation)
    }
    
    // 'action' attribute on TileAction (id=RewriteRemainderOfTerm) at PolicyDetailsDetailViewTile.pcf: line 78, column 196
    function action_23 () : void {
      uiHelper.startRewrite(TC_REWRITEREMAINDEROFTERM, inForcePeriod, CurrentLocation)
    }
    
    // 'action' attribute on TileAction (id=RewriteNewTerm) at PolicyDetailsDetailViewTile.pcf: line 83, column 184
    function action_25 () : void {
      uiHelper.startRewrite(TC_REWRITENEWTERM, inForcePeriod, CurrentLocation)
    }
    
    // 'action' attribute on TileAction (id=RenewPolicy) at PolicyDetailsDetailViewTile.pcf: line 88, column 120
    function action_27 () : void {
      gw.web.policyfile.PolicyFileMenuActionsUIHelper.startRenewal(period, CurrentLocation)
    }
    
    // 'action' attribute on TileAction (id=RequestRestoreButton) at PolicyDetailsDetailViewTile.pcf: line 93, column 77
    function action_29 () : void {
      RequestRestorePopup.push(period.PolicyTerm)
    }
    
    // 'action' attribute on TextInput (id=PrimaryNamedInsured_Input) at PolicyDetailsDetailViewTile.pcf: line 140, column 40
    function action_48 () : void {
      ContactForward.go(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact)
    }
    
    // 'action' attribute on TileAction (id=IssueSubmission) at PolicyDetailsDetailViewTile.pcf: line 42, column 121
    function action_6 () : void {
      gw.web.policyfile.PolicyFileMenuActionsUIHelper.startIssuance(period, CurrentLocation)
    }
    
    // 'action' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function action_70 () : void {
      AccountFileForward.go(period.Policy.MovedPolicySourceAccount)
    }
    
    // 'action' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function action_78 () : void {
      pcf.PolicyFileForward.go(period.Policy.RewrittenToNewAccountSource.Periods.first().PolicyNumber)
    }
    
    // 'action' attribute on TileAction (id=ChangePolicy) at PolicyDetailsDetailViewTile.pcf: line 47, column 102
    function action_8 () : void {
      StartPolicyChange.go(period)
    }
    
    // 'action' attribute on TextInput (id=RewrittenTargetPolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 179, column 92
    function action_86 () : void {
      pcf.PolicyFileForward.go(period.Policy.RewrittenToNewAccountDestination.Periods.first().PolicyNumber)
    }
    
    // 'action' attribute on TextInput (id=SplitSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 185, column 62
    function action_94 () : void {
      pcf.PolicyFileForward.go(period.Policy.DividedSourcePolicy.LatestBoundPeriod.PolicyNumber)
    }
    
    // 'action' attribute on TextInput (id=PreRenewalDirection_Input) at PolicyDetailsDetailViewTile.pcf: line 196, column 66
    function action_dest_103 () : pcf.api.Destination {
      return pcf.PreRenewalDirectionPage.createDestination(period, period.Policy, asOfDate)
    }
    
    // 'action' attribute on TileAction (id=CancelPolicy) at PolicyDetailsDetailViewTile.pcf: line 52, column 127
    function action_dest_12 () : pcf.api.Destination {
      return pcf.StartCancellation.createDestination(period)
    }
    
    // 'action' attribute on TileAction (id=RequestRestoreButton) at PolicyDetailsDetailViewTile.pcf: line 93, column 77
    function action_dest_30 () : pcf.api.Destination {
      return pcf.RequestRestorePopup.createDestination(period.PolicyTerm)
    }
    
    // 'action' attribute on TextInput (id=PrimaryNamedInsured_Input) at PolicyDetailsDetailViewTile.pcf: line 140, column 40
    function action_dest_49 () : pcf.api.Destination {
      return pcf.ContactForward.createDestination(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact)
    }
    
    // 'action' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function action_dest_71 () : pcf.api.Destination {
      return pcf.AccountFileForward.createDestination(period.Policy.MovedPolicySourceAccount)
    }
    
    // 'action' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function action_dest_79 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(period.Policy.RewrittenToNewAccountSource.Periods.first().PolicyNumber)
    }
    
    // 'action' attribute on TextInput (id=RewrittenTargetPolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 179, column 92
    function action_dest_87 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(period.Policy.RewrittenToNewAccountDestination.Periods.first().PolicyNumber)
    }
    
    // 'action' attribute on TileAction (id=ChangePolicy) at PolicyDetailsDetailViewTile.pcf: line 47, column 102
    function action_dest_9 () : pcf.api.Destination {
      return pcf.StartPolicyChange.createDestination(period)
    }
    
    // 'action' attribute on TextInput (id=SplitSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 185, column 62
    function action_dest_95 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(period.Policy.DividedSourcePolicy.LatestBoundPeriod.PolicyNumber)
    }
    
    // 'available' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function available_68 () : java.lang.Boolean {
      return gw.api.web.dashboard.ui.DashboardParameters.AccountOverviewPermissions.Instance.canSeeAccount(period.Policy.MovedPolicySourceAccount)
    }
    
    // 'backgroundColor' attribute on DetailViewTile (id=PolicyDetailsDetailViewTile) at PolicyDetailsDetailViewTile.pcf: line 7, column 74
    function backgroundColor_135 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_TIER_1
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_onEnter_60 (def :  pcf.ContactDetailsTileInputSet_company) : void {
      def.onEnter(accountDetailsHelper,true)
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_onEnter_62 (def :  pcf.ContactDetailsTileInputSet_default) : void {
      def.onEnter(accountDetailsHelper,true)
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_onEnter_64 (def :  pcf.ContactDetailsTileInputSet_person) : void {
      def.onEnter(accountDetailsHelper,true)
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_refreshVariables_61 (def :  pcf.ContactDetailsTileInputSet_company) : void {
      def.refreshVariables(accountDetailsHelper,true)
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_refreshVariables_63 (def :  pcf.ContactDetailsTileInputSet_default) : void {
      def.refreshVariables(accountDetailsHelper,true)
    }
    
    // 'def' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function def_refreshVariables_65 (def :  pcf.ContactDetailsTileInputSet_person) : void {
      def.refreshVariables(accountDetailsHelper,true)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailsDetailViewTile.pcf: line 17, column 23
    function initialValue_0 () : boolean {
      return gw.plugin.Plugins.get(gw.plugin.policy.IPolicyPlugin).canStartReinstatement(period) == null
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailsDetailViewTile.pcf: line 21, column 65
    function initialValue_1 () : gw.pcf.policyfile.StartRewriteMenuItemSetUIHelper {
      return new gw.pcf.policyfile.StartRewriteMenuItemSetUIHelper(period)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailsDetailViewTile.pcf: line 26, column 28
    function initialValue_2 () : PolicyPeriod {
      return uiHelper.getInForcePeriod()
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailsDetailViewTile.pcf: line 30, column 65
    function initialValue_3 () : gw.api.web.dashboard.ui.policy.PolicyDetailHelper {
      return new gw.api.web.dashboard.ui.policy.PolicyDetailHelper(period)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailsDetailViewTile.pcf: line 35, column 67
    function initialValue_4 () : gw.api.web.dashboard.ui.account.AccountDetailHelper {
      return new gw.api.web.dashboard.ui.account.AccountDetailHelper(period.Policy.Account)
    }
    
    // 'mode' attribute on TileInputSetRef at PolicyDetailsDetailViewTile.pcf: line 155, column 68
    function mode_66 () : java.lang.Object {
      return period.Policy.Account.AccountHolderContact.Subtype
    }
    
    // 'text' attribute on DetailViewTile (id=PolicyDetailsDetailViewTile) at PolicyDetailsDetailViewTile.pcf: line 7, column 74
    function text_134 () : java.lang.String {
      return period.Archived ? DisplayKey.get('Web.Archive.Archived') : period.PeriodDisplayStatus
    }
    
    // 'value' attribute on TextInput (id=PreRenewalDirection_Input) at PolicyDetailsDetailViewTile.pcf: line 196, column 66
    function valueRoot_105 () : java.lang.Object {
      return period.PolicyTerm
    }
    
    // 'value' attribute on TextInput (id=CancellationReason_Input) at PolicyDetailsDetailViewTile.pcf: line 229, column 36
    function valueRoot_126 () : java.lang.Object {
      return period.Cancellation.CancelReasonCode
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyDetailsDetailViewTile.pcf: line 102, column 51
    function valueRoot_32 () : java.lang.Object {
      return period
    }
    
    // 'value' attribute on TextInput (id=Product_Input) at PolicyDetailsDetailViewTile.pcf: line 107, column 29
    function valueRoot_35 () : java.lang.Object {
      return period.Policy
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at PolicyDetailsDetailViewTile.pcf: line 145, column 34
    function valueRoot_55 () : java.lang.Object {
      return period.Policy.getUserRoleAssignmentByRole(TC_UNDERWRITER)
    }
    
    // 'value' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function valueRoot_73 () : java.lang.Object {
      return period.Policy.MovedPolicySourceAccount
    }
    
    // 'value' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function valueRoot_82 () : java.lang.Object {
      return detailHelper
    }
    
    // 'value' attribute on TextInput (id=PreRenewalDirection_Input) at PolicyDetailsDetailViewTile.pcf: line 196, column 66
    function value_104 () : typekey.PreRenewalDirection {
      return period.PolicyTerm.PreRenewalDirection
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyDetailsDetailViewTile.pcf: line 203, column 37
    function value_108 () : java.util.Date {
      return period.PeriodStart
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyDetailsDetailViewTile.pcf: line 208, column 35
    function value_111 () : java.util.Date {
      return period.PeriodEnd
    }
    
    // 'value' attribute on TextInput (id=TermNumber_Input) at PolicyDetailsDetailViewTile.pcf: line 213, column 30
    function value_114 () : Integer {
      return period.TermNumber
    }
    
    // 'value' attribute on DateInput (id=IssueDate_Input) at PolicyDetailsDetailViewTile.pcf: line 219, column 52
    function value_118 () : java.util.Date {
      return period.Policy.IssueDate
    }
    
    // 'value' attribute on TextInput (id=isUssed_Input) at PolicyDetailsDetailViewTile.pcf: line 223, column 71
    function value_122 () : java.lang.String {
      return period.Policy.Issued? "Yes" : ""
    }
    
    // 'value' attribute on TextInput (id=CancellationReason_Input) at PolicyDetailsDetailViewTile.pcf: line 229, column 36
    function value_125 () : String {
      return period.Cancellation.CancelReasonCode.DisplayName
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at PolicyDetailsDetailViewTile.pcf: line 235, column 36
    function value_130 () : java.util.Date {
      return period.CancellationDate
    }
    
    // 'value' attribute on TileActionIterator (id=RescindCancellation) at PolicyDetailsDetailViewTile.pcf: line 57, column 43
    function value_17 () : entity.Cancellation[] {
      return gw.web.policyfile.PolicyFileMenuActionsUIHelper.getRescindableCancellations(period)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyDetailsDetailViewTile.pcf: line 102, column 51
    function value_31 () : java.lang.String {
      return period.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextInput (id=Product_Input) at PolicyDetailsDetailViewTile.pcf: line 107, column 29
    function value_34 () : String {
      return period.Policy.ProductDisplayName
    }
    
    // 'value' attribute on TextInput (id=Offering_Input) at PolicyDetailsDetailViewTile.pcf: line 113, column 64
    function value_38 () : gw.api.productmodel.Offering {
      return period.Offering
    }
    
    // 'value' attribute on TextInput (id=NameWhenArchived_Input) at PolicyDetailsDetailViewTile.pcf: line 133, column 36
    function value_43 () : java.lang.String {
      return period.PrimaryInsuredName
    }
    
    // 'value' attribute on TextInput (id=PrimaryNamedInsured_Input) at PolicyDetailsDetailViewTile.pcf: line 140, column 40
    function value_50 () : entity.PolicyPriNamedInsured {
      return period.PrimaryNamedInsured
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at PolicyDetailsDetailViewTile.pcf: line 145, column 34
    function value_54 () : entity.User {
      return period.Policy.getUserRoleAssignmentByRole(TC_UNDERWRITER).AssignedUser
    }
    
    // 'value' attribute on TextInput (id=UnderwritingCompany_Input) at PolicyDetailsDetailViewTile.pcf: line 150, column 39
    function value_57 () : entity.UWCompany {
      return period.UWCompany
    }
    
    // 'value' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function value_72 () : java.lang.String {
      return period.Policy.MovedPolicySourceAccount.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function value_81 () : java.lang.String {
      return detailHelper.SourcePolicyValue
    }
    
    // 'value' attribute on TextInput (id=RewrittenTargetPolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 179, column 92
    function value_89 () : java.lang.String {
      return detailHelper.TargetPolicyValue
    }
    
    // 'value' attribute on TextInput (id=SplitSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 185, column 62
    function value_96 () : java.lang.String {
      return detailHelper.SplitSourcePolicyValue
    }
    
    // 'visible' attribute on TileAction (id=CancelPolicy) at PolicyDetailsDetailViewTile.pcf: line 52, column 127
    function visible_10 () : java.lang.Boolean {
      return perm.PolicyPeriod.cancel(period) and period.CancellationDate != period.PeriodStart and not period.Archived
    }
    
    // 'visible' attribute on Label (id=PreRenewalDirectionLabel) at PolicyDetailsDetailViewTile.pcf: line 189, column 66
    function visible_100 () : java.lang.Boolean {
      return period.PolicyTerm.PreRenewalDirection != null
    }
    
    // 'visible' attribute on DateInput (id=IssueDate_Input) at PolicyDetailsDetailViewTile.pcf: line 219, column 52
    function visible_117 () : java.lang.Boolean {
      return period.Policy.IssueDate != null
    }
    
    // 'visible' attribute on TextInput (id=CancellationReason_Input) at PolicyDetailsDetailViewTile.pcf: line 229, column 36
    function visible_124 () : java.lang.Boolean {
      return period.Canceled
    }
    
    // 'visible' attribute on TileAction (id=ReinstatePolicy) at PolicyDetailsDetailViewTile.pcf: line 68, column 106
    function visible_18 () : java.lang.Boolean {
      return perm.PolicyPeriod.reinstate(period) and canStartReinstatement and not period.Archived
    }
    
    // 'visible' attribute on TileAction (id=RewriteFullTerm) at PolicyDetailsDetailViewTile.pcf: line 73, column 185
    function visible_20 () : java.lang.Boolean {
      return (inForcePeriod.GLLineExists or inForcePeriod.BOPLineExists) and inForcePeriod.Canceled? false : (uiHelper.CanRewriteFullTerm(inForcePeriod) and not period.Archived)
    }
    
    // 'visible' attribute on TileAction (id=RewriteRemainderOfTerm) at PolicyDetailsDetailViewTile.pcf: line 78, column 196
    function visible_22 () : java.lang.Boolean {
      return (inForcePeriod.GLLineExists or inForcePeriod.BOPLineExists) and inForcePeriod.Canceled? false : (uiHelper.CanRewriteRemainderOfTermTerm(inForcePeriod) and not period.Archived)
    }
    
    // 'visible' attribute on TileAction (id=RewriteNewTerm) at PolicyDetailsDetailViewTile.pcf: line 83, column 184
    function visible_24 () : java.lang.Boolean {
      return (inForcePeriod.GLLineExists or inForcePeriod.BOPLineExists) and inForcePeriod.Canceled? false : (uiHelper.canRewriteNewTerm(inForcePeriod) and not period.Archived)
    }
    
    // 'visible' attribute on TileAction (id=RenewPolicy) at PolicyDetailsDetailViewTile.pcf: line 88, column 120
    function visible_26 () : java.lang.Boolean {
      return perm.PolicyPeriod.renew(period) and period.Policy.canStartRenewal() == null and not period.Archived
    }
    
    // 'visible' attribute on TileAction (id=RequestRestoreButton) at PolicyDetailsDetailViewTile.pcf: line 93, column 77
    function visible_28 () : java.lang.Boolean {
      return period.Archived and perm.PolicyPeriod.restorefromarchive
    }
    
    // 'visible' attribute on TextInput (id=Offering_Input) at PolicyDetailsDetailViewTile.pcf: line 113, column 64
    function visible_37 () : java.lang.Boolean {
      return period.Policy.Product.Offerings.HasElements
    }
    
    // 'visible' attribute on TextInput (id=NameWhenArchived_Input) at PolicyDetailsDetailViewTile.pcf: line 133, column 36
    function visible_42 () : java.lang.Boolean {
      return period.Archived
    }
    
    // 'visible' attribute on TextInput (id=PrimaryNamedInsured_Input) at PolicyDetailsDetailViewTile.pcf: line 140, column 40
    function visible_47 () : java.lang.Boolean {
      return not period.Archived
    }
    
    // 'visible' attribute on TileAction (id=IssueSubmission) at PolicyDetailsDetailViewTile.pcf: line 42, column 121
    function visible_5 () : java.lang.Boolean {
      return perm.PolicyPeriod.issue(period) and period.Policy.canStartIssuance() == null and not period.Archived
    }
    
    // 'visible' attribute on InputDivider (id=SourceDivider) at PolicyDetailsDetailViewTile.pcf: line 158, column 54
    function visible_67 () : java.lang.Boolean {
      return detailHelper.SourceDividerVisible
    }
    
    // 'visible' attribute on TextInput (id=SourceAccount_Input) at PolicyDetailsDetailViewTile.pcf: line 165, column 67
    function visible_69 () : java.lang.Boolean {
      return period.Policy.MovedPolicySourceAccount != null
    }
    
    // 'visible' attribute on TileAction (id=ChangePolicy) at PolicyDetailsDetailViewTile.pcf: line 47, column 102
    function visible_7 () : java.lang.Boolean {
      return perm.PolicyPeriod.change(period) and period.Policy.Issued and not period.Archived
    }
    
    // 'visible' attribute on TextInput (id=RewrittenSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 172, column 70
    function visible_77 () : java.lang.Boolean {
      return period.Policy.RewrittenToNewAccountSource != null
    }
    
    // 'visible' attribute on TextInput (id=RewrittenTargetPolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 179, column 92
    function visible_85 () : java.lang.Boolean {
      return period.Policy.RewrittenToNewAccountDestination.BoundPeriods.HasElements
    }
    
    // 'visible' attribute on TextInput (id=SplitSourcePolicy_Input) at PolicyDetailsDetailViewTile.pcf: line 185, column 62
    function visible_93 () : java.lang.Boolean {
      return period.Policy.DividedSourcePolicy != null
    }
    
    property get accountDetailsHelper () : gw.api.web.dashboard.ui.account.AccountDetailHelper {
      return getVariableValue("accountDetailsHelper", 0) as gw.api.web.dashboard.ui.account.AccountDetailHelper
    }
    
    property set accountDetailsHelper ($arg :  gw.api.web.dashboard.ui.account.AccountDetailHelper) {
      setVariableValue("accountDetailsHelper", 0, $arg)
    }
    
    property get asOfDate () : java.util.Date {
      return getRequireValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setRequireValue("asOfDate", 0, $arg)
    }
    
    property get canStartReinstatement () : boolean {
      return getVariableValue("canStartReinstatement", 0) as java.lang.Boolean
    }
    
    property set canStartReinstatement ($arg :  boolean) {
      setVariableValue("canStartReinstatement", 0, $arg)
    }
    
    property get detailHelper () : gw.api.web.dashboard.ui.policy.PolicyDetailHelper {
      return getVariableValue("detailHelper", 0) as gw.api.web.dashboard.ui.policy.PolicyDetailHelper
    }
    
    property set detailHelper ($arg :  gw.api.web.dashboard.ui.policy.PolicyDetailHelper) {
      setVariableValue("detailHelper", 0, $arg)
    }
    
    property get inForcePeriod () : PolicyPeriod {
      return getVariableValue("inForcePeriod", 0) as PolicyPeriod
    }
    
    property set inForcePeriod ($arg :  PolicyPeriod) {
      setVariableValue("inForcePeriod", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get uiHelper () : gw.pcf.policyfile.StartRewriteMenuItemSetUIHelper {
      return getVariableValue("uiHelper", 0) as gw.pcf.policyfile.StartRewriteMenuItemSetUIHelper
    }
    
    property set uiHelper ($arg :  gw.pcf.policyfile.StartRewriteMenuItemSetUIHelper) {
      setVariableValue("uiHelper", 0, $arg)
    }
    
    
  }
  
  
}