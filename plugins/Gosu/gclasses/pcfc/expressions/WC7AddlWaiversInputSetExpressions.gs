package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7AddlWaiversInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7AddlWaiversInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7AddlWaiversInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7AddlWaiversInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getRequireValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setRequireValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  
}