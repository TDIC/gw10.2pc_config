package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.domain.Clause
uses gw.api.productmodel.ExclusionPattern
uses gw.api.productmodel.ConditionPattern
uses java.lang.IllegalArgumentException
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborContractorForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7NewLaborContractorForContactTypePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborContractorForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7NewLaborContractorForContactTypePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (line :  WC7Line, contactType :  typekey.ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at WC7NewLaborContractorForContactTypePopup.pcf: line 61, column 62
    function action_11 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewLaborContractorForContactTypePopup.pcf: line 53, column 62
    function action_6 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(detail))
    }
    
    // 'beforeCommit' attribute on Popup (id=WC7NewLaborContractorForContactTypePopup) at WC7NewLaborContractorForContactTypePopup.pcf: line 13, column 123
    function beforeCommit_17 (pickedValue :  WC7LaborContactDetail) : void {
      detail.WC7LaborContact.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch)
    }
    
    // 'beforeValidate' attribute on Popup (id=WC7NewLaborContractorForContactTypePopup) at WC7NewLaborContractorForContactTypePopup.pcf: line 13, column 123
    function beforeValidate_18 (pickedValue :  WC7LaborContactDetail) : void {
      gw.lob.wc7.WC7NewLaborContactValidation.validate(detail)
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborContractorForContactTypePopup.pcf: line 65, column 58
    function def_onEnter_12 (def :  pcf.WC7LaborContractorInfoDV_default) : void {
      def.onEnter(detail, theInclusion, getParentClause())
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborContractorForContactTypePopup.pcf: line 67, column 77
    function def_onEnter_15 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(detail.WC7LaborContact, false)
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborContractorForContactTypePopup.pcf: line 65, column 58
    function def_refreshVariables_13 (def :  pcf.WC7LaborContractorInfoDV_default) : void {
      def.refreshVariables(detail, theInclusion, getParentClause())
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborContractorForContactTypePopup.pcf: line 67, column 77
    function def_refreshVariables_16 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(detail.WC7LaborContact, false)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborContractorForContactTypePopup.pcf: line 28, column 33
    function initialValue_0 () : typekey.Inclusion {
      return line.inclusionTypeForClause(clausePattern)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborContractorForContactTypePopup.pcf: line 32, column 44
    function initialValue_1 () : entity.WC7LaborContactDetail {
      return addNewLaborContractorDetailForContactType()
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborContractorForContactTypePopup.pcf: line 36, column 25
    function initialValue_2 () : Contact[] {
      return line.WC7PolicyLaborContractors.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborContractorForContactTypePopup.pcf: line 40, column 69
    function initialValue_3 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(detail.WC7LaborContact.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborContractorForContactTypePopup.pcf: line 44, column 22
    function initialValue_4 () : String {
      return theInclusion == Inclusion.TC_INCL ? DisplayKey.get("Java.ProductModel.Name.Condition") : DisplayKey.get("Java.ProductModel.Name.Exclusion")
    }
    
    // EditButtons at WC7NewLaborContractorForContactTypePopup.pcf: line 56, column 72
    function label_9 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on PanelRef at WC7NewLaborContractorForContactTypePopup.pcf: line 65, column 58
    function mode_14 () : java.lang.Object {
      return getParentClause().Pattern.CodeIdentifier
    }
    
    // 'pickValue' attribute on EditButtons at WC7NewLaborContractorForContactTypePopup.pcf: line 56, column 72
    function pickValue_7 () : WC7LaborContactDetail {
      return detail
    }
    
    // 'title' attribute on Popup (id=WC7NewLaborContractorForContactTypePopup) at WC7NewLaborContractorForContactTypePopup.pcf: line 13, column 123
    static function title_19 (clausePattern :  gw.api.productmodel.ClausePattern, contactType :  typekey.ContactType, line :  WC7Line) : java.lang.Object {
      return DisplayKey.get("Web.Contact.NewContact", entity.WC7PolicyLaborContractor.Type.TypeInfo.DisplayName)
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewLaborContractorForContactTypePopup.pcf: line 53, column 62
    function visible_5 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at WC7NewLaborContractorForContactTypePopup.pcf: line 56, column 72
    function visible_8 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.WC7NewLaborContractorForContactTypePopup {
      return super.CurrentLocation as pcf.WC7NewLaborContractorForContactTypePopup
    }
    
    property get clausePattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("clausePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set clausePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("clausePattern", 0, $arg)
    }
    
    property get contactType () : typekey.ContactType {
      return getVariableValue("contactType", 0) as typekey.ContactType
    }
    
    property set contactType ($arg :  typekey.ContactType) {
      setVariableValue("contactType", 0, $arg)
    }
    
    property get detail () : entity.WC7LaborContactDetail {
      return getVariableValue("detail", 0) as entity.WC7LaborContactDetail
    }
    
    property set detail ($arg :  entity.WC7LaborContactDetail) {
      setVariableValue("detail", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get line () : WC7Line {
      return getVariableValue("line", 0) as WC7Line
    }
    
    property set line ($arg :  WC7Line) {
      setVariableValue("line", 0, $arg)
    }
    
    property get scheduleLabel () : String {
      return getVariableValue("scheduleLabel", 0) as String
    }
    
    property set scheduleLabel ($arg :  String) {
      setVariableValue("scheduleLabel", 0, $arg)
    }
    
    property get theInclusion () : typekey.Inclusion {
      return getVariableValue("theInclusion", 0) as typekey.Inclusion
    }
    
    property set theInclusion ($arg :  typekey.Inclusion) {
      setVariableValue("theInclusion", 0, $arg)
    }
    
    
    function addNewLaborContractorDetailForContactType() : WC7LaborContactDetail {
      if (theInclusion == TC_EXCL)
        return line.addNewExcludedLaborContractorDetailForContactType(contactType,
            line.getExclusion(clausePattern as ExclusionPattern))
      else if (theInclusion == TC_INCL)
        return line.addNewIncludedLaborContractorDetailForContactType(contactType,
            line.getCondition(clausePattern as ConditionPattern))
      else
        throw new IllegalArgumentException("Unexpected inclusion type: " + theInclusion)
    }
    
    function getParentClause() : gw.api.domain.Clause {
      if (detail typeis WC7IncludedLaborContactDetail)
        return detail.LaborContactCondition
      else if (detail typeis WC7ExcludedLaborContactDetail)
        return detail.LaborContactExclusion
      else
        throw new IllegalArgumentException("Unexpected detail type: " + (typeof detail))
    }
    
    
  }
  
  
}