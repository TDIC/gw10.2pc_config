package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policyfile/PolicyFile_EREScreen_GL.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_EREScreen_GLExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policyfile/PolicyFile_EREScreen_GL.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_EREScreen_GLExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=PolicyFile_EREScreen_GL) at PolicyFile_EREScreen_GL.pcf: line 10, column 135
    function afterEnter_2 () : void {
      gw.pcf.policysummary.PolicySummaryHelper.setEndorsementDetails_TDIC(policyPeriod);
    }
    
    // 'canVisit' attribute on Page (id=PolicyFile_EREScreen_GL) at PolicyFile_EREScreen_GL.pcf: line 10, column 135
    static function canVisit_3 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.view(policyPeriod) and perm.System.pfiledetails
    }
    
    // 'def' attribute on PanelRef at PolicyFile_EREScreen_GL.pcf: line 23, column 55
    function def_onEnter_0 (def :  pcf.TDIC_ExtendedReportingDV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_EREScreen_GL.pcf: line 23, column 55
    function def_refreshVariables_1 (def :  pcf.TDIC_ExtendedReportingDV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'parent' attribute on Page (id=PolicyFile_EREScreen_GL) at PolicyFile_EREScreen_GL.pcf: line 10, column 135
    static function parent_4 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod, asOfDate)
    }
    
    // 'title' attribute on Page (id=PolicyFile_EREScreen_GL) at PolicyFile_EREScreen_GL.pcf: line 10, column 135
    static function title_5 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Policy.GL.ExtendedReport")+ " " + policyPeriod.Policy.MostRecentPolicyNumber
    }
    
    override property get CurrentLocation () : pcf.PolicyFile_EREScreen_GL {
      return super.CurrentLocation as pcf.PolicyFile_EREScreen_GL
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}