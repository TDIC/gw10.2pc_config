package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_Currency_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_IntegerField_Currency_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_Currency_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      answerContainer.getAnswer(question).IntegerAnswer = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function editable_2 () : java.lang.Boolean {
      return question.isQuestionEditable_TDIC(answerContainer)
    }
    
    // 'initialValue' attribute on Variable at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 20, column 23
    function initialValue_0 () : block() {
      return \ -> question
    }
    
    // 'onChange' attribute on PostOnChange at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 31, column 78
    function onChange_1 () : void {
      if (onChangeBlock != null) onChangeBlock();ChangeBlock()
    }
    
    // 'outputConversion' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function outputConversion_3 (VALUE :  java.lang.Integer) : java.lang.String {
      return tdic.web.pcf.helper.GLCPUWQuestionAnswersCurrencyHelper.getUWQuestionCurrencyAnswerNoDecimal(question, VALUE)
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function requestValidationExpression_4 (VALUE :  java.lang.Integer) : java.lang.Object {
      return question.getLengthForQuestion_TDIC(VALUE?.toString().length)
    }
    
    // 'required' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function required_5 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function valueRoot_8 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_Currency_TDIC.pcf: line 29, column 37
    function value_6 () : java.lang.Integer {
      return answerContainer.getAnswer(question).IntegerAnswer
    }
    
    property get ChangeBlock () : block() {
      return getVariableValue("ChangeBlock", 0) as block()
    }
    
    property set ChangeBlock ($arg :  block()) {
      setVariableValue("ChangeBlock", 0, $arg)
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}