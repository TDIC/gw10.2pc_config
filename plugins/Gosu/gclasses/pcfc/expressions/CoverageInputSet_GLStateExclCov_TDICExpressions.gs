package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLStateExclCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLStateExclCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLStateExclCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 49, column 62
    function allCheckedRowsAction_5 (CheckedValues :  entity.GLStateExclSched_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 33, column 99
    function available_24 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable) and perm.System.vieweditplseend_tdic
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 49, column 62
    function available_3 () : java.lang.Boolean {
      return glLine.Branch.Job typeis PolicyChange
    }
    
    // 'editable' attribute on ListViewInput (id=GLSchedEquipLV) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 38, column 27
    function editable_22 () : java.lang.Boolean {
      return perm.System.vieweditplseend_tdic
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 33, column 99
    function label_25 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 33, column 99
    function setter_26 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 69, column 46
    function sortValue_6 (scheduledItem :  entity.GLStateExclSched_TDIC) : java.lang.Object {
      return scheduledItem.State
    }
    
    // 'value' attribute on DateCell (id=EndorsementEffDate_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 74, column 56
    function sortValue_7 (scheduledItem :  entity.GLStateExclSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=EndorsementExpDate_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 79, column 57
    function sortValue_8 (scheduledItem :  entity.GLStateExclSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 62, column 56
    function toCreateAndAdd_19 () : entity.GLStateExclSched_TDIC {
      return glLine.createAndAddStateExclSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 62, column 56
    function toRemove_20 (scheduledItem :  entity.GLStateExclSched_TDIC) : void {
      glLine.toRemoveFromStateExclSched_TDIC(scheduledItem) 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 62, column 56
    function value_21 () : entity.GLStateExclSched_TDIC[] {
      return glLine.GLStateExclSched_TDIC
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 42, column 179
    function visible_2 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLStateExclSched_TDIC.hasMatch(\sched -> sched.BasedOn == null)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 33, column 99
    function visible_23 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 86, column 42
    function visible_29 () : java.lang.Boolean {
      return isStateExclCovVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isStateExclCovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLStateExclCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
              coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
              coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLStateExclCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                    coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    function setExpirationDate(stateExclScheds : GLStateExclSched_TDIC[]) {
      stateExclScheds.each(\stateExclSched -> {
        if(stateExclSched.LTExpirationDate == null) {
          stateExclSched.LTExpirationDate = stateExclSched.Branch.EditEffectiveDate
        }
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLStateExclCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 69, column 46
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.State = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 69, column 46
    function valueRoot_11 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=EndorsementEffDate_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 74, column 56
    function value_13 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=EndorsementExpDate_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 79, column 57
    function value_16 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at CoverageInputSet.GLStateExclCov_TDIC.pcf: line 69, column 46
    function value_9 () : java.lang.String {
      return scheduledItem.State
    }
    
    property get scheduledItem () : entity.GLStateExclSched_TDIC {
      return getIteratedValue(1) as entity.GLStateExclSched_TDIC
    }
    
    
  }
  
  
}