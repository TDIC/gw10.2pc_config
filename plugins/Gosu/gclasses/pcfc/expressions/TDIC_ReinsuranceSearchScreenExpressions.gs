package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ReinsuranceSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Matches_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 103, column 38
    function action_30 () : void {
      TDIC_ReinsuranceSearchResultsPopup.push(reinsuranceSearchResultEntry)
    }
    
    // 'action' attribute on TextCell (id=Matches_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 103, column 38
    function action_dest_31 () : pcf.api.Destination {
      return pcf.TDIC_ReinsuranceSearchResultsPopup.createDestination(reinsuranceSearchResultEntry)
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 80, column 75
    function valueRoot_19 () : java.lang.Object {
      return reinsuranceSearchResultEntry
    }
    
    // 'value' attribute on TextCell (id=Matches_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 103, column 38
    function valueRoot_33 () : java.lang.Object {
      return reinsuranceSearchResultEntry.ReinsuranceGroup
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 80, column 75
    function value_18 () : java.lang.String {
      return reinsuranceSearchResultEntry.RelatedAddressLine1
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 86, column 30
    function value_21 () : java.lang.String {
      return reinsuranceSearchResultEntry.RelatedCity
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 92, column 30
    function value_24 () : typekey.State {
      return reinsuranceSearchResultEntry.RelatedState
    }
    
    // 'value' attribute on TextCell (id=PostalCode_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 97, column 30
    function value_27 () : java.lang.String {
      return reinsuranceSearchResultEntry.RelatedPostalCode
    }
    
    // 'value' attribute on TextCell (id=Matches_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 103, column 38
    function value_32 () : Integer {
      return reinsuranceSearchResultEntry.ReinsuranceGroup.Count
    }
    
    property get reinsuranceSearchResultEntry () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult {
      return getIteratedValue(2) as tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends TDIC_ReinsuranceSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ReinsuranceSearchScreen.pcf: line 61, column 45
    function def_onEnter_11 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at TDIC_ReinsuranceSearchScreen.pcf: line 61, column 45
    function def_refreshVariables_12 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Product = (__VALUE_TO_SET as gw.api.productmodel.Product)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 32, column 52
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel at TDIC_ReinsuranceSearchScreen.pcf: line 14, column 91
    function searchCriteria_37 () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria {
      return new tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at TDIC_ReinsuranceSearchScreen.pcf: line 14, column 91
    function search_36 () : java.lang.Object {
      return tdic.web.admin.shared.SharedUIHelper.doReinsuranceSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 80, column 75
    function sortValue_13 (reinsuranceSearchResultEntry :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : java.lang.Object {
      return reinsuranceSearchResultEntry.RelatedAddressLine1
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 86, column 30
    function sortValue_14 (reinsuranceSearchResultEntry :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : java.lang.Object {
      return reinsuranceSearchResultEntry.RelatedCity
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 92, column 30
    function sortValue_15 (reinsuranceSearchResultEntry :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : java.lang.Object {
      return reinsuranceSearchResultEntry.RelatedState
    }
    
    // 'value' attribute on TextCell (id=PostalCode_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 97, column 30
    function sortValue_16 (reinsuranceSearchResultEntry :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : java.lang.Object {
      return reinsuranceSearchResultEntry.RelatedPostalCode
    }
    
    // 'value' attribute on TextCell (id=Matches_Cell) at TDIC_ReinsuranceSearchScreen.pcf: line 103, column 38
    function sortValue_17 (reinsuranceSearchResultEntry :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : java.lang.Object {
      return reinsuranceSearchResultEntry.ReinsuranceGroup.Count
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function valueRange_3 () : java.lang.Object {
      return gw.rating.rtm.util.ProductModelUtils.getSpecificProducts_TDIC({"WC7Line", "BOPLine"})
    }
    
    // 'value' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function value_0 () : gw.api.productmodel.Product {
      return searchCriteria.Product
    }
    
    // 'value' attribute on RowIterator at TDIC_ReinsuranceSearchScreen.pcf: line 75, column 89
    function value_35 () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult[] {
      return reinsuranceSearchResults
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 32, column 52
    function value_7 () : java.lang.String {
      return searchCriteria.AddressLine1
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.api.productmodel.Product[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at TDIC_ReinsuranceSearchScreen.pcf: line 25, column 54
    function verifyValueRange_5 () : void {
      var __valueRangeArg = gw.rating.rtm.util.ProductModelUtils.getSpecificProducts_TDIC({"WC7Line", "BOPLine"})
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    property get reinsuranceSearchResults () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult[] {
      return getResultsValue(1) as tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult[]
    }
    
    property get searchCriteria () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria {
      return getCriteriaValue(1) as tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria
    }
    
    property set searchCriteria ($arg :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ReinsuranceSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}