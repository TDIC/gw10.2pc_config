package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/submgr/TDIC_WithDrawReasonPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_WithDrawReasonPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/submgr/TDIC_WithDrawReasonPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_WithDrawReasonPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (submission :  Submission, policyPeriod :  PolicyPeriod, wizard :  pcf.api.Wizard) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=TDIC_WithDrawReasonPopup) at TDIC_WithDrawReasonPopup.pcf: line 13, column 70
    function afterCommit_2 (TopLocation :  pcf.api.Location) : void {
      tdic.web.account.submgr.WithdrawReasonPopupUIHelper.withdrawSubmission(submission, policyPeriod, wizard)
    }
    
    // 'beforeValidate' attribute on Popup (id=TDIC_WithDrawReasonPopup) at TDIC_WithDrawReasonPopup.pcf: line 13, column 70
    function beforeValidate_3 (pickedValue :  java.lang.Object) : void {
      if (policyPeriod.BOPLineExists) {gw.lob.bop.BOPLineValidation.checkRequiredWithdrawnAndDeclineReasonText_TDIC(policyPeriod.BOPLine)}
    }
    
    // 'canVisit' attribute on Popup (id=TDIC_WithDrawReasonPopup) at TDIC_WithDrawReasonPopup.pcf: line 13, column 70
    static function canVisit_4 (policyPeriod :  PolicyPeriod, submission :  Submission, wizard :  pcf.api.Wizard) : java.lang.Boolean {
      return perm.Submission.edit(submission)
    }
    
    // 'def' attribute on PanelRef at TDIC_WithDrawReasonPopup.pcf: line 34, column 74
    function def_onEnter_0 (def :  pcf.RejectReasonDV) : void {
      def.onEnter(submission, TC_WITHDRAW_TDIC,policyPeriod)
    }
    
    // 'def' attribute on PanelRef at TDIC_WithDrawReasonPopup.pcf: line 34, column 74
    function def_refreshVariables_1 (def :  pcf.RejectReasonDV) : void {
      def.refreshVariables(submission, TC_WITHDRAW_TDIC,policyPeriod)
    }
    
    override property get CurrentLocation () : pcf.TDIC_WithDrawReasonPopup {
      return super.CurrentLocation as pcf.TDIC_WithDrawReasonPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get submission () : Submission {
      return getVariableValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setVariableValue("submission", 0, $arg)
    }
    
    property get wizard () : pcf.api.Wizard {
      return getVariableValue("wizard", 0) as pcf.api.Wizard
    }
    
    property set wizard ($arg :  pcf.api.Wizard) {
      setVariableValue("wizard", 0, $arg)
    }
    
    
  }
  
  
}