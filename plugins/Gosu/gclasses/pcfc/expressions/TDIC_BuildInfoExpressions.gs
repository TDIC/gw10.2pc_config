package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_BuildInfo.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BuildInfoExpressions {
  @javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_BuildInfo.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BuildInfoExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'defaultValue' attribute on ExitPointParameter at TDIC_BuildInfo.pcf: line 13, column 23
    function initialValue_0 () : String {
      return gw.api.web.WebUtil.getLocalizedResourcesPath()
    }
    
    override property get CurrentLocation () : pcf.TDIC_BuildInfo {
      return super.CurrentLocation as pcf.TDIC_BuildInfo
    }
    
    property get url () : String {
      return getVariableValue("url", 0) as String
    }
    
    property set url ($arg :  String) {
      setVariableValue("url", 0, $arg)
    }
    
    
  }
  
  
}