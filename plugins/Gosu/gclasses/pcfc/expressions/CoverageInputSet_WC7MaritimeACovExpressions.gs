package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MaritimeACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7MaritimeACovExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MaritimeACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function available_254 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'confirmMessage' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function confirmMessage_255 () : java.lang.String {
      return DisplayKey.get("Web.Policy.WC7.Confirm.RemovingFELAMaritimeCovRemovesExposures", coveragePattern)
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeLimitType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeAggLimitType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm.Value = (__VALUE_TO_SET as typekey.WC7CovProgramType)
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm.Value = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'value' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm.Value = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'editable' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function editable_19 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm) and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function editable_31 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm) and wc7LineAtPeriodStart.WC7MaritimeCoveredEmployeeVLs.Empty and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function editable_43 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm) and wc7LineAtPeriodStart.WC7MaritimeCoveredEmployeeVLs.Empty and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function editable_7 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm) and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 20, column 41
    function initialValue_0 () : entity.WC7WorkersCompLine {
      return coverable as WC7WorkersCompLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 24, column 23
    function initialValue_1 () : boolean {
      return wc7Line.Branch.Job.NewTerm
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 28, column 41
    function initialValue_2 () : entity.WC7WorkersCompLine {
      return wc7Line.getVersionList().AllVersions.first().getSlice(wc7Line.Branch.PeriodStart)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 32, column 50
    function initialValue_3 () : gw.pcf.WC7CoverageInputSetUIHelper {
      return new gw.pcf.WC7CoverageInputSetUIHelper()
    }
    
    // 'inputConversion' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function inputConversion_60 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.convertFromString(VALUE)
    }
    
    // 'label' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function label_20 () : java.lang.Object {
      return wc7Line.WC7MaritimeACov.WC7MaritimeAggLimitTerm.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function label_256 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function label_32 () : java.lang.Object {
      return wc7Line.WC7MaritimeACov.WC7MaritimeProgramTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function label_45 () : java.lang.Object {
      return wc7Line.WC7MaritimeACov.WC7MaritimeLiabLawTerm.DisplayName
    }
    
    // 'label' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function label_59 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function label_8 () : java.lang.Object {
      return wc7Line.WC7MaritimeACov.WC7MaritimeLimitTerm.DisplayName
    }
    
    // 'outputConversion' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function outputConversion_61 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.convertToString(VALUE)
    }
    
    // 'required' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function required_21 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function required_33 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function required_46 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm.Pattern.Required
    }
    
    // 'required' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function required_62 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function required_9 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function setter_258 (VALUE :  java.lang.Boolean) : void {
      wc7Line.setMaritimeCovExists(VALUE)
    }
    
    // 'showConfirmMessage' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function showConfirmMessage_257 () : java.lang.Boolean {
      return wc7Line.WC7MaritimeACovExists and wc7Line.WC7MaritimeCoveredEmployeesWM.HasElements
    }
    
    // 'validationExpression' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function validationExpression_57 () : java.lang.Object {
      return gw.pcf.coverage.CovTermDirectInputSetHelper.validate(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function valueRange_13 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function valueRange_25 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function valueRange_37 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function valueRange_50 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function valueRoot_12 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function valueRoot_24 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function valueRoot_36 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function valueRoot_49 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 36, column 37
    function valueRoot_5 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function valueRoot_65 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function value_10 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeLimitType> {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function value_22 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeAggLimitType> {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function value_34 () : typekey.WC7CovProgramType {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm.Value
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 36, column 37
    function value_4 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function value_47 () : typekey.WC7GoverningLaw {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm.Value
    }
    
    // 'value' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function value_63 () : java.math.BigDecimal {
      return wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeManualPremiumTerm.Value
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function verifyValueRangeIsAllowedType_26 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7MaritimeAggLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_38 ($$arg :  typekey.WC7CovProgramType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function verifyValueRangeIsAllowedType_51 ($$arg :  typekey.WC7GoverningLaw[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 52, column 100
    function verifyValueRange_15 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeAggLimitTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 60, column 103
    function verifyValueRange_27 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeAggLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeProgramTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 68, column 47
    function verifyValueRange_39 () : void {
      var __valueRangeArg = wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function verifyValueRange_52 () : void {
      var __valueRangeArg = wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeLiabLawTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7MaritimeACov.pcf: line 44, column 112
    function visible_253 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7MaritimeACov.pcf: line 409, column 100
    function visible_261 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on RangeInput (id=WC7MaritimeLiabLawTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 79, column 195
    function visible_44 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7MaritimeACov.HasWC7MaritimeProgramTerm and wc7LineAtPeriodStart.WC7MaritimeACov.WC7MaritimeProgramTerm.Value == typekey.WC7CovProgramType.TC_PROGRAMII
    }
    
    // 'visible' attribute on TextInput (id=WC7MaritimeManualPremiumTermInput_Input) at CoverageInputSet.WC7MaritimeACov.pcf: line 90, column 185
    function visible_58 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.isCovTermAvailable(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.CovTermPattern>("WC7MaritimeManualPremium"))
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coverageInputSetHelper () : gw.pcf.WC7CoverageInputSetUIHelper {
      return getVariableValue("coverageInputSetHelper", 0) as gw.pcf.WC7CoverageInputSetUIHelper
    }
    
    property set coverageInputSetHelper ($arg :  gw.pcf.WC7CoverageInputSetUIHelper) {
      setVariableValue("coverageInputSetHelper", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get isNewTerm () : boolean {
      return getVariableValue("isNewTerm", 0) as java.lang.Boolean
    }
    
    property set isNewTerm ($arg :  boolean) {
      setVariableValue("isNewTerm", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    property get wc7LineAtPeriodStart () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7LineAtPeriodStart", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7LineAtPeriodStart ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7LineAtPeriodStart", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MaritimeACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 308, column 51
    function action_182 () : void {
      WC7ClassCodeSearchPopup.push(wc7MaritimeCovEmp2.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 308, column 51
    function action_dest_183 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wc7MaritimeCovEmp2.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 348, column 88
    function available_210 () : java.lang.Boolean {
      return !wc7MaritimeCovEmp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function defaultSetter_191 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp2.ClassCodeShortDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 326, column 50
    function defaultSetter_199 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp2.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=Vessel_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 332, column 49
    function defaultSetter_203 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp2.Vessel = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 336, column 79
    function defaultSetter_207 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp2.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 348, column 88
    function defaultSetter_213 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp2.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function editable_189 () : java.lang.Boolean {
      return addlVersionClassCodeShortDescs.length > 1
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 279, column 45
    function initialValue_170 () : entity.WC7ClassCode {
      return isNewTerm ? null : wc7MaritimeCovEmp2.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 284, column 44
    function initialValue_171 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wc7MaritimeCovEmp2, wc7MaritimeCovEmp2.LocationWM, addlVersionPreviousWC7ClassCode)
    }
    
    // RowIterator (id=WC7MaritimeCovEmployees2) at CoverageInputSet.WC7MaritimeACov.pcf: line 275, column 63
    function initializeVariables_247 () : void {
        addlVersionPreviousWC7ClassCode = isNewTerm ? null : wc7MaritimeCovEmp2.BasedOn.ClassCode;
  addlVersionClassCodeShortDescs = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wc7MaritimeCovEmp2, wc7MaritimeCovEmp2.LocationWM, addlVersionPreviousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 308, column 51
    function inputConversion_184 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeMaritime(VALUE, wc7MaritimeCovEmp2, wc7Line.WC7MaritimeACov, addlVersionPreviousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7MaritimeACov.pcf: line 310, column 107
    function onChange_181 () : void {
      if (wc7MaritimeCovEmp2.ClassCode == null) classCodeShortDescriptions = {}
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 308, column 51
    function outputConversion_185 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : (VALUE).Code
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function valueRange_174 () : java.lang.Object {
      return wc7Line.Branch.PolicyLocationsWM
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function valueRange_193 () : java.lang.Object {
      return addlVersionClassCodeShortDescs
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function valueRoot_173 () : java.lang.Object {
      return wc7MaritimeCovEmp2
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 299, column 46
    function valueRoot_179 () : java.lang.Object {
      return wc7MaritimeCovEmp.LocationWM
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function value_172 () : entity.PolicyLocation {
      return wc7MaritimeCovEmp2.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 299, column 46
    function value_178 () : typekey.State {
      return wc7MaritimeCovEmp.LocationWM.State
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 308, column 51
    function value_186 () : entity.WC7ClassCode {
      return wc7MaritimeCovEmp2.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function value_190 () : java.lang.String {
      return wc7MaritimeCovEmp2.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 326, column 50
    function value_198 () : java.lang.Integer {
      return wc7MaritimeCovEmp2.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=Vessel_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 332, column 49
    function value_202 () : java.lang.String {
      return wc7MaritimeCovEmp2.Vessel
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 336, column 79
    function value_206 () : java.lang.Boolean {
      return wc7MaritimeCovEmp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 348, column 88
    function value_212 () : java.lang.Integer {
      return wc7MaritimeCovEmp2.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 357, column 88
    function value_219 () : java.lang.String {
      return wc7MaritimeCovEmp2.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 365, column 95
    function value_224 () : java.math.BigDecimal {
      return wc7MaritimeCovEmp2.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 374, column 37
    function value_229 () : java.lang.Boolean {
      return wc7MaritimeCovEmp2.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 379, column 37
    function value_233 () : java.lang.Boolean {
      return wc7MaritimeCovEmp2.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 389, column 37
    function value_237 () : java.math.BigDecimal {
      return wc7MaritimeCovEmp2.SupplementalLoadingRate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 395, column 35
    function value_241 () : java.util.Date {
      return wc7MaritimeCovEmp2.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 401, column 35
    function value_244 () : java.util.Date {
      return wc7MaritimeCovEmp2.ExpirationDate
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function verifyValueRangeIsAllowedType_175 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function verifyValueRangeIsAllowedType_175 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function verifyValueRangeIsAllowedType_175 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function verifyValueRangeIsAllowedType_194 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function verifyValueRangeIsAllowedType_194 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 294, column 32
    function verifyValueRange_176 () : void {
      var __valueRangeArg = wc7Line.Branch.PolicyLocationsWM
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_175(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 320, column 32
    function verifyValueRange_195 () : void {
      var __valueRangeArg = addlVersionClassCodeShortDescs
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_194(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 365, column 95
    function visible_223 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 365, column 95
    function visible_226 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 374, column 37
    function visible_228 () : java.lang.Boolean {
      return wc7MaritimeCovEmp2.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 374, column 37
    function visible_231 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 389, column 37
    function visible_236 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'visible' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 389, column 37
    function visible_239 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get addlVersionClassCodeShortDescs () : java.lang.String[] {
      return getVariableValue("addlVersionClassCodeShortDescs", 3) as java.lang.String[]
    }
    
    property set addlVersionClassCodeShortDescs ($arg :  java.lang.String[]) {
      setVariableValue("addlVersionClassCodeShortDescs", 3, $arg)
    }
    
    property get addlVersionPreviousWC7ClassCode () : entity.WC7ClassCode {
      return getVariableValue("addlVersionPreviousWC7ClassCode", 3) as entity.WC7ClassCode
    }
    
    property set addlVersionPreviousWC7ClassCode ($arg :  entity.WC7ClassCode) {
      setVariableValue("addlVersionPreviousWC7ClassCode", 3, $arg)
    }
    
    property get wc7MaritimeCovEmp2 () : entity.WC7MaritimeCoveredEmployee {
      return getIteratedValue(3) as entity.WC7MaritimeCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MaritimeACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7MaritimeCovEmpLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function action_100 () : void {
      WC7ClassCodeSearchPopup.push(wc7MaritimeCovEmp.LocationWM, wc7Line, previousWC7ClassCode, WC7ClassCodeType.TC_ADMIRALTY, classCodeProgramType)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function action_dest_101 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wc7MaritimeCovEmp.LocationWM, wc7Line, previousWC7ClassCode, WC7ClassCodeType.TC_ADMIRALTY, classCodeProgramType)
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function defaultSetter_107 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function defaultSetter_113 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.ClassCodeShortDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 179, column 48
    function defaultSetter_121 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=Vessel_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 186, column 47
    function defaultSetter_125 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.Vessel = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 191, column 76
    function defaultSetter_129 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 203, column 85
    function defaultSetter_134 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function defaultSetter_143 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.Rate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 232, column 35
    function defaultSetter_149 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.SpecificDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 239, column 34
    function defaultSetter_155 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.SupplementalDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 253, column 35
    function defaultSetter_160 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.SupplementalLoadingRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7MaritimeCovEmp.LocationWM = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'editable' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function editable_102 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.LocationWM.State != null
    }
    
    // 'editable' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function editable_111 () : java.lang.Boolean {
      return classCodeShortDescriptions.length > 1
    }
    
    // 'editable' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function editable_87 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.CanEditLocation
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 122, column 36
    function initialValue_84 () : WC7ClassCode {
      return isNewTerm ? null : wc7MaritimeCovEmp.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 127, column 42
    function initialValue_85 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptionsForLineCoverage(wc7MaritimeCovEmp, wc7MaritimeCovEmp.LocationWM, previousWC7ClassCode)
    }
    
    // RowIterator (id=WC7MaritimeCovEmployees) at CoverageInputSet.WC7MaritimeACov.pcf: line 118, column 61
    function initializeVariables_249 () : void {
        previousWC7ClassCode = isNewTerm ? null : wc7MaritimeCovEmp.BasedOn.ClassCode;
  classCodeShortDescriptions = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptionsForLineCoverage(wc7MaritimeCovEmp, wc7MaritimeCovEmp.LocationWM, previousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function inputConversion_104 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeMaritime(VALUE, wc7MaritimeCovEmp, wc7Line.WC7MaritimeACov, previousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7MaritimeACov.pcf: line 241, column 87
    function onChange_153 () : void {
      wc7MaritimeCovEmp.SupplementalDiseaseLoadingRate = null
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7MaritimeACov.pcf: line 140, column 230
    function onChange_86 () : void {
      wc7MaritimeCovEmp.ClassCode = gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeMaritime(wc7MaritimeCovEmp.ClassCode.Code, wc7MaritimeCovEmp, wc7Line.WC7MaritimeACov, previousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7MaritimeACov.pcf: line 161, column 179
    function onChange_99 () : void {
      coverageInputSetHelper.clearupWC7MaritimeEmployeeRates(wc7MaritimeCovEmp); if (wc7MaritimeCovEmp.ClassCode == null) classCodeShortDescriptions = {}
    }
    
    // 'onPick' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function onPick_103 (PickedValue :  WC7ClassCode) : void {
      coverageInputSetHelper.clearupWC7MaritimeEmployeeRates(wc7MaritimeCovEmp)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function outputConversion_105 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : (VALUE).Code
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function valueRange_115 () : java.lang.Object {
      return classCodeShortDescriptions
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function valueRange_91 () : java.lang.Object {
      return wc7Line.Branch.PolicyLocationsWM
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function valueRoot_90 () : java.lang.Object {
      return wc7MaritimeCovEmp
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 147, column 44
    function valueRoot_97 () : java.lang.Object {
      return wc7MaritimeCovEmp.LocationWM
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function value_106 () : entity.WC7ClassCode {
      return wc7MaritimeCovEmp.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function value_112 () : java.lang.String {
      return wc7MaritimeCovEmp.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 179, column 48
    function value_120 () : java.lang.Integer {
      return wc7MaritimeCovEmp.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=Vessel_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 186, column 47
    function value_124 () : java.lang.String {
      return wc7MaritimeCovEmp.Vessel
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 191, column 76
    function value_128 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 203, column 85
    function value_133 () : java.lang.Integer {
      return wc7MaritimeCovEmp.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 212, column 85
    function value_138 () : java.lang.String {
      return wc7MaritimeCovEmp.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function value_142 () : java.math.BigDecimal {
      return wc7MaritimeCovEmp.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 232, column 35
    function value_148 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 239, column 34
    function value_154 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 253, column 35
    function value_159 () : java.math.BigDecimal {
      return wc7MaritimeCovEmp.SupplementalLoadingRate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 260, column 33
    function value_164 () : java.util.Date {
      return wc7MaritimeCovEmp.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 267, column 33
    function value_167 () : java.util.Date {
      return wc7MaritimeCovEmp.ExpirationDate
    }
    
    // 'value' attribute on RowIterator (id=WC7MaritimeCovEmployees2) at CoverageInputSet.WC7MaritimeACov.pcf: line 275, column 63
    function value_248 () : entity.WC7MaritimeCoveredEmployee[] {
      return wc7MaritimeCovEmp.AdditionalVersions.whereTypeIs(WC7MaritimeCoveredEmployee)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function value_88 () : entity.PolicyLocation {
      return wc7MaritimeCovEmp.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 147, column 44
    function value_96 () : typekey.State {
      return wc7MaritimeCovEmp.LocationWM.State
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function verifyValueRangeIsAllowedType_116 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function verifyValueRangeIsAllowedType_116 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function verifyValueRangeIsAllowedType_92 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function verifyValueRangeIsAllowedType_92 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function verifyValueRangeIsAllowedType_92 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function verifyValueRange_117 () : void {
      var __valueRangeArg = classCodeShortDescriptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_116(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function verifyValueRange_93 () : void {
      var __valueRangeArg = wc7Line.Branch.PolicyLocationsWM
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_92(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 203, column 85
    function visible_132 () : java.lang.Boolean {
      return !wc7MaritimeCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function visible_141 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function visible_145 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 232, column 35
    function visible_147 () : java.lang.Boolean {
      return wc7MaritimeCovEmp.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 232, column 35
    function visible_151 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 253, column 35
    function visible_162 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get classCodeShortDescriptions () : java.lang.String[] {
      return getVariableValue("classCodeShortDescriptions", 2) as java.lang.String[]
    }
    
    property set classCodeShortDescriptions ($arg :  java.lang.String[]) {
      setVariableValue("classCodeShortDescriptions", 2, $arg)
    }
    
    property get previousWC7ClassCode () : WC7ClassCode {
      return getVariableValue("previousWC7ClassCode", 2) as WC7ClassCode
    }
    
    property set previousWC7ClassCode ($arg :  WC7ClassCode) {
      setVariableValue("previousWC7ClassCode", 2, $arg)
    }
    
    property get wc7MaritimeCovEmp () : WC7MaritimeCoveredEmployee {
      return getIteratedValue(2) as WC7MaritimeCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MaritimeACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7MaritimeCovEmpLVExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MaritimeACov.pcf: line 103, column 53
    function initialValue_70 () : typekey.WC7ClassCodeProgramType {
      return gw.pcf.WC7ClassesInputSetUIHelper.deriveClassCodeProgramType(wc7Line.WC7MaritimeACov.WC7MaritimeProgramTerm.Value, wc7Line.WC7MaritimeACov.WC7MaritimeLiabLawTerm.Value)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 138, column 51
    function sortValue_71 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 147, column 44
    function sortValue_72 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.LocationWM.State
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 159, column 49
    function sortValue_73 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 172, column 30
    function sortValue_74 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 179, column 48
    function sortValue_75 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=Vessel_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 186, column 47
    function sortValue_76 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.Vessel
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 191, column 76
    function sortValue_77 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function sortValue_78 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.Rate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 260, column 33
    function sortValue_82 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 267, column 33
    function sortValue_83 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wc7MaritimeCovEmp.ExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=WC7MaritimeCovEmployees) at CoverageInputSet.WC7MaritimeACov.pcf: line 118, column 61
    function toCreateAndAdd_250 () : WC7MaritimeCoveredEmployee {
      return wc7Line.createAndAddMaritimeCoveredEmployeeWM()
    }
    
    // 'toRemove' attribute on RowIterator (id=WC7MaritimeCovEmployees) at CoverageInputSet.WC7MaritimeACov.pcf: line 118, column 61
    function toRemove_251 (wc7MaritimeCovEmp :  WC7MaritimeCoveredEmployee) : void {
      wc7MaritimeCovEmp.removeWM()
    }
    
    // 'value' attribute on RowIterator (id=WC7MaritimeCovEmployees) at CoverageInputSet.WC7MaritimeACov.pcf: line 118, column 61
    function value_252 () : entity.WC7MaritimeCoveredEmployee[] {
      return wc7Line.WC7MaritimeCoveredEmployeesWM
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 221, column 93
    function visible_79 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 232, column 35
    function visible_80 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7MaritimeACov.pcf: line 253, column 35
    function visible_81 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get classCodeProgramType () : typekey.WC7ClassCodeProgramType {
      return getVariableValue("classCodeProgramType", 1) as typekey.WC7ClassCodeProgramType
    }
    
    property set classCodeProgramType ($arg :  typekey.WC7ClassCodeProgramType) {
      setVariableValue("classCodeProgramType", 1, $arg)
    }
    
    
  }
  
  
}