package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLAdditionalInsuredCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 54, column 62
    function allCheckedRowsAction_6 (CheckedValues :  entity.GLAdditionInsdSched_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 38, column 99
    function available_28 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable) and perm.System.vieweditplaiend_tdic
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 54, column 62
    function available_4 () : java.lang.Boolean {
      return glLine.Branch.Job typeis PolicyChange
    }
    
    // 'editable' attribute on ListViewInput (id=GLSchedEquipLV) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 43, column 27
    function editable_26 () : java.lang.Boolean {
      return perm.System.vieweditplaiend_tdic
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 31, column 36
    function initialValue_2 () : AccountContactView[] {
      return null
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 38, column 99
    function label_29 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 38, column 99
    function setter_30 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 73, column 69
    function sortValue_7 (scheduledItem :  entity.GLAdditionInsdSched_TDIC) : java.lang.Object {
      return scheduledItem.AdditionalInsured.DisplayName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 93, column 56
    function sortValue_8 (scheduledItem :  entity.GLAdditionInsdSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 98, column 57
    function sortValue_9 (scheduledItem :  entity.GLAdditionInsdSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 67, column 59
    function toCreateAndAdd_23 () : entity.GLAdditionInsdSched_TDIC {
      return glLine.createAndAddPLAISched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 67, column 59
    function toRemove_24 (scheduledItem :  entity.GLAdditionInsdSched_TDIC) : void {
      glLine.toRemoveFromPLAISched_TDIC(scheduledItem) 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 67, column 59
    function value_25 () : entity.GLAdditionInsdSched_TDIC[] {
      return glLine.GLAdditionInsdSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 38, column 99
    function visible_27 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 47, column 182
    function visible_3 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLAdditionInsdSched_TDIC.hasMatch(\sched -> sched.BasedOn == null)
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 105, column 43
    function visible_33 () : java.lang.Boolean {
      return isGLAddlInsuredVisible()
    }
    
    property get additionalInsureds () : AccountContactView[] {
      return getVariableValue("additionalInsureds", 0) as AccountContactView[]
    }
    
    property set additionalInsureds ($arg :  AccountContactView[]) {
      setVariableValue("additionalInsureds", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    
    function getAdditionalInsureds() : AccountContactView[] {
      if (additionalInsureds == null) {
        var all = glLine.AdditionalInsureds*.AccountContactRole*.AccountContact
        if(all.HasElements) {
          var addedContacts = glLine.GLAdditionInsdSched_TDIC*.AdditionalInsured*.PolicyAddlInsured*.AccountContactRole*.AccountContact
          var remaining = all?.subtract(addedContacts)
          if(remaining.HasElements) {
            additionalInsureds = remaining?.toTypedArray()?.asViews()
          }
        }
      }
      return additionalInsureds
    }
    
    function setAdditionalInsureds(scheduledItem : GLAdditionInsdSched_TDIC, contact : AccountContact ) {
      scheduledItem.AdditionalInsured = glLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\elt -> elt.PolicyAddlInsured?.AccountContactRole?.AccountContact == contact)
    }
    
    function isGLAddlInsuredVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLAdditionalInsuredCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
              coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
              coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLAdditionalInsuredCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                    coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    function setExpirationDate(plAddlInsds : GLAdditionInsdSched_TDIC[]) {
      plAddlInsds.each(\plAddlInsd -> {
        if(plAddlInsd.LTExpirationDate == null) {
          plAddlInsd.LTExpirationDate = plAddlInsd.Branch.EditEffectiveDate
        }
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 85, column 48
    function action_11 () : void {
      setAdditionalInsureds(scheduledItem, unassignedContact.AccountContact)
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 85, column 48
    function label_12 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.AccountContactView {
      return getIteratedValue(2) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 81, column 36
    function sortBy_10 (unassignedContact :  entity.AccountContactView) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 73, column 69
    function valueRoot_15 () : java.lang.Object {
      return scheduledItem.AdditionalInsured
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 93, column 56
    function valueRoot_18 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 78, column 59
    function value_13 () : entity.AccountContactView[] {
      return getAdditionalInsureds()
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 73, column 69
    function value_14 () : java.lang.String {
      return scheduledItem.AdditionalInsured.DisplayName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 93, column 56
    function value_17 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLAdditionalInsuredCov_TDIC.pcf: line 98, column 57
    function value_20 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    property get scheduledItem () : entity.GLAdditionInsdSched_TDIC {
      return getIteratedValue(1) as entity.GLAdditionInsdSched_TDIC
    }
    
    
  }
  
  
}