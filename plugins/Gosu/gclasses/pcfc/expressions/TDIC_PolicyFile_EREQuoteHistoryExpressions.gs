package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyFile_EREQuoteHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PolicyFile_EREQuoteHistoryExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyFile_EREQuoteHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EREQuoteListDPExpressionsImpl extends TDIC_PolicyFile_EREQuoteHistoryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TDIC_PolicyFile_EREQuoteHistory.pcf: line 80, column 61
    function def_onEnter_26 (def :  pcf.TDIC_EREQuoteTotalsDV) : void {
      def.onEnter(selectedQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_PolicyFile_EREQuoteHistory.pcf: line 83, column 69
    function def_onEnter_28 (def :  pcf.TDIC_EREQuotePremiumsPanelSet) : void {
      def.onEnter(selectedQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_PolicyFile_EREQuoteHistory.pcf: line 80, column 61
    function def_refreshVariables_27 (def :  pcf.TDIC_EREQuoteTotalsDV) : void {
      def.refreshVariables(selectedQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_PolicyFile_EREQuoteHistory.pcf: line 83, column 69
    function def_refreshVariables_29 (def :  pcf.TDIC_EREQuotePremiumsPanelSet) : void {
      def.refreshVariables(selectedQuote)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_PolicyFile_EREQuoteHistory.pcf: line 37, column 32
    function sortBy_0 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.CreateTime
    }
    
    // 'value' attribute on TextCell (id=quoteid_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 42, column 49
    function sortValue_1 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.QuoteNumber
    }
    
    // 'value' attribute on TextCell (id=quotedate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 47, column 39
    function sortValue_2 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.CreateTime.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=policyeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 52, column 39
    function sortValue_3 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.PeriodStart.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=ereeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 57, column 39
    function sortValue_4 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.EREEffectiveDate.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=userquoted_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 62, column 37
    function sortValue_5 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.CreateUser
    }
    
    // 'value' attribute on TextCell (id=eretotal_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 68, column 53
    function sortValue_6 (ereQuote :  entity.GLEREQuickQuote_TDIC) : java.lang.Object {
      return ereQuote.TotalPremium
    }
    
    // 'value' attribute on RowIterator at TDIC_PolicyFile_EREQuoteHistory.pcf: line 33, column 57
    function value_25 () : entity.GLEREQuickQuote_TDIC[] {
      return policyPeriod.Policy.GLEREQuickQuotes_TDIC
    }
    
    // 'visible' attribute on Card (id=EREQuoteCard) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 78, column 47
    function visible_30 () : java.lang.Boolean {
      return selectedQuote != null
    }
    
    property get selectedQuote () : GLEREQuickQuote_TDIC {
      return getCurrentSelection(1) as GLEREQuickQuote_TDIC
    }
    
    property set selectedQuote ($arg :  GLEREQuickQuote_TDIC) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyFile_EREQuoteHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EREQuoteListDPExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=quotedate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 47, column 39
    function valueRoot_11 () : java.lang.Object {
      return ereQuote.CreateTime
    }
    
    // 'value' attribute on TextCell (id=policyeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 52, column 39
    function valueRoot_14 () : java.lang.Object {
      return ereQuote.PeriodStart
    }
    
    // 'value' attribute on TextCell (id=ereeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 57, column 39
    function valueRoot_17 () : java.lang.Object {
      return ereQuote.EREEffectiveDate
    }
    
    // 'value' attribute on TextCell (id=quoteid_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 42, column 49
    function valueRoot_8 () : java.lang.Object {
      return ereQuote
    }
    
    // 'value' attribute on TextCell (id=quotedate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 47, column 39
    function value_10 () : String {
      return ereQuote.CreateTime.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=policyeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 52, column 39
    function value_13 () : String {
      return ereQuote.PeriodStart.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=ereeffdate_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 57, column 39
    function value_16 () : String {
      return ereQuote.EREEffectiveDate.ShortFormat
    }
    
    // 'value' attribute on TextCell (id=userquoted_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 62, column 37
    function value_19 () : User {
      return ereQuote.CreateUser
    }
    
    // 'value' attribute on TextCell (id=eretotal_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 68, column 53
    function value_22 () : java.math.BigDecimal {
      return ereQuote.TotalPremium
    }
    
    // 'value' attribute on TextCell (id=quoteid_Cell) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 42, column 49
    function value_7 () : java.lang.String {
      return ereQuote.QuoteNumber
    }
    
    property get ereQuote () : entity.GLEREQuickQuote_TDIC {
      return getIteratedValue(2) as entity.GLEREQuickQuote_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyFile_EREQuoteHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PolicyFile_EREQuoteHistoryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=TDIC_PolicyFile_EREQuoteHistory) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 10, column 88
    function afterEnter_31 () : void {
      gw.api.web.PebblesUtil.addWebMessages(CurrentLocation, policyPeriod.PolicyFileMessages)
    }
    
    // 'canVisit' attribute on Page (id=TDIC_PolicyFile_EREQuoteHistory) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 10, column 88
    static function canVisit_32 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return tdic.pc.config.job.ere.GLEREQuoteUIHelper.canVisitEREQuoteHistory(policyPeriod)
    }
    
    // Page (id=TDIC_PolicyFile_EREQuoteHistory) at TDIC_PolicyFile_EREQuoteHistory.pcf: line 10, column 88
    static function parent_33 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod,asOfDate)
    }
    
    override property get CurrentLocation () : pcf.TDIC_PolicyFile_EREQuoteHistory {
      return super.CurrentLocation as pcf.TDIC_PolicyFile_EREQuoteHistory
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function getSearchCriteria() : gw.history.HistorySearchCriteria {
      var sc = new gw.history.HistorySearchCriteria() {:RelatedItem = (policyPeriod.Policy != null)? policyPeriod.Policy: policyPeriod.PolicyTerm}
      return sc
      }
    
    
  }
  
  
}