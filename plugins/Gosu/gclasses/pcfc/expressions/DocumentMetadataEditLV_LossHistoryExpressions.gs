package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.LossHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentMetadataEditLV_LossHistoryExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.LossHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentMetadataEditLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.LossHistory.pcf: line 24, column 53
    function initialValue_0 () : gw.api.web.document.DocumentPCContext {
      return DocumentApplicationContext as gw.api.web.document.DocumentPCContext
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 52, column 34
    function sortValue_1 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 105, column 51
    function sortValue_10 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 112, column 53
    function sortValue_11 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.SecurityType
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function sortValue_12 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function sortValue_13 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 58, column 41
    function sortValue_2 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function sortValue_3 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.MimeType
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function sortValue_4 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Language
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 81, column 36
    function sortValue_6 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Author
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 87, column 39
    function sortValue_7 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Recipient
    }
    
    // 'value' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function sortValue_8 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Level
    }
    
    // 'toRemove' attribute on RowIterator at DocumentMetadataEditLV.LossHistory.pcf: line 34, column 54
    function toRemove_79 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : void {
      DocumentCreationInfos.remove(DocumentCreationInfo); DocumentCreationInfo.Document.remove()
    }
    
    // 'value' attribute on RowIterator at DocumentMetadataEditLV.LossHistory.pcf: line 34, column 54
    function value_80 () : gw.document.DocumentCreationInfo[] {
      return DocumentCreationInfos.toTypedArray()
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function visible_5 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function visible_9 () : java.lang.Boolean {
      return documentPCContext.LevelsVisible
    }
    
    property get DocumentApplicationContext () : gw.document.DocumentApplicationContext {
      return getRequireValue("DocumentApplicationContext", 0) as gw.document.DocumentApplicationContext
    }
    
    property set DocumentApplicationContext ($arg :  gw.document.DocumentApplicationContext) {
      setRequireValue("DocumentApplicationContext", 0, $arg)
    }
    
    property get DocumentCreationInfos () : java.util.Collection<gw.document.DocumentCreationInfo> {
      return getRequireValue("DocumentCreationInfos", 0) as java.util.Collection<gw.document.DocumentCreationInfo>
    }
    
    property set DocumentCreationInfos ($arg :  java.util.Collection<gw.document.DocumentCreationInfo>) {
      setRequireValue("DocumentCreationInfos", 0, $arg)
    }
    
    property get documentPCContext () : gw.api.web.document.DocumentPCContext {
      return getVariableValue("documentPCContext", 0) as gw.api.web.document.DocumentPCContext
    }
    
    property set documentPCContext ($arg :  gw.api.web.document.DocumentPCContext) {
      setVariableValue("documentPCContext", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.LossHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentMetadataEditLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 52, column 34
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 58, column 41
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 81, column 36
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 87, column 39
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Recipient = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 105, column 51
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 112, column 53
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.SecurityType = (__VALUE_TO_SET as typekey.DocumentSecurityType)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 46, column 32
    function icon_15 () : java.lang.String {
      return document.Icon
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.LossHistory.pcf: line 38, column 33
    function initialValue_14 () : entity.Document {
      return DocumentCreationInfo.Document
    }
    
    // RowIterator at DocumentMetadataEditLV.LossHistory.pcf: line 34, column 54
    function initializeVariables_78 () : void {
        document = DocumentCreationInfo.Document;

    }
    
    // 'optionLabel' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function optionLabel_26 (VALUE :  java.lang.String) : java.lang.String {
      return gw.document.DocumentsUtilBase.getMimeTypeDescription(VALUE)
    }
    
    // 'optionLabel' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function optionLabel_49 (VALUE :  gw.api.domain.linkedobject.LinkedObjectContainer) : java.lang.String {
      return Note.getLevelDisplayString(VALUE)
    }
    
    // 'value' attribute on Reflect at DocumentMetadataEditLV.LossHistory.pcf: line 132, column 83
    function reflectionValue_69 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function valueRange_27 () : java.lang.Object {
      return gw.document.DocumentsUtilBase.getMimeTypes()
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function valueRange_34 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function valueRange_50 () : java.lang.Object {
      return documentPCContext.GenerateLevels
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function valueRange_65 () : java.lang.Object {
      return DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
    }
    
    // 'valueRange' attribute on Reflect at DocumentMetadataEditLV.LossHistory.pcf: line 132, column 83
    function valueRange_71 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function valueRange_74 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(document.Type)
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 52, column 34
    function valueRoot_18 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 52, column 34
    function value_16 () : java.lang.String {
      return document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 58, column 41
    function value_20 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function value_24 () : java.lang.String {
      return document.MimeType
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function value_31 () : typekey.LanguageType {
      return document.Language
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 81, column 36
    function value_39 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 87, column 39
    function value_43 () : java.lang.String {
      return document.Recipient
    }
    
    // 'value' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function value_47 () : gw.api.domain.linkedobject.LinkedObjectContainer {
      return document.Level
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 105, column 51
    function value_55 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 112, column 53
    function value_59 () : typekey.DocumentSecurityType {
      return document.SecurityType
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function value_63 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function value_72 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function verifyValueRangeIsAllowedType_35 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function verifyValueRangeIsAllowedType_35 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.domain.linkedobject.LinkedObjectContainer[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function verifyValueRangeIsAllowedType_66 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function verifyValueRangeIsAllowedType_66 ($$arg :  typekey.DocumentType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function verifyValueRangeIsAllowedType_75 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function verifyValueRangeIsAllowedType_75 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 67, column 41
    function verifyValueRange_29 () : void {
      var __valueRangeArg = gw.document.DocumentsUtilBase.getMimeTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function verifyValueRange_36 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_35(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function verifyValueRange_52 () : void {
      var __valueRangeArg = documentPCContext.GenerateLevels
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 120, column 45
    function verifyValueRange_67 () : void {
      var __valueRangeArg = DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_66(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 128, column 57
    function verifyValueRange_76 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(document.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_75(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 75, column 69
    function visible_37 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.LossHistory.pcf: line 98, column 54
    function visible_53 () : java.lang.Boolean {
      return documentPCContext.LevelsVisible
    }
    
    property get DocumentCreationInfo () : gw.document.DocumentCreationInfo {
      return getIteratedValue(1) as gw.document.DocumentCreationInfo
    }
    
    property get document () : entity.Document {
      return getVariableValue("document", 1) as entity.Document
    }
    
    property set document ($arg :  entity.Document) {
      setVariableValue("document", 1, $arg)
    }
    
    
  }
  
  
}