package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lob.wc7.services.WC7ServiceLocator
uses gw.api.productmodel.ClausePattern
uses java.util.Collection
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7LineCoverageCVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7ConditionsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_119 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_121 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_123 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_125 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_127 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_129 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_131 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_133 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_135 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_137 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_139 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_141 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_143 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_145 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_147 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_149 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_151 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_153 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_155 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_157 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_159 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_161 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_163 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_165 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_167 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_169 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_171 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_173 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_175 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_177 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_179 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_181 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_183 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_185 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_187 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_189 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_191 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_193 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_195 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_197 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_199 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_201 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_203 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_205 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_207 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_209 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_211 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_213 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_215 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_219 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_221 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_onEnter_223 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_120 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_122 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_124 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_126 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_128 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_130 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_132 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_134 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_136 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_138 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_140 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_142 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_144 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_146 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_148 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_222 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(conditionPattern, wcLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 109, column 64
    function mode_225 () : java.lang.Object {
      return modeForClausePattern(conditionPattern)
    }
    
    property get conditionPattern () : gw.api.productmodel.ConditionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ConditionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends WC7ExclusionsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_229 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_231 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_233 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_235 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_237 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_239 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_241 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_243 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_245 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_247 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_249 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_251 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_253 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_255 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_257 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_259 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_261 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_263 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_265 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_267 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_269 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_271 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_273 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_275 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_277 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_279 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_281 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_283 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_285 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_287 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_289 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_291 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_293 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_295 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_297 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_299 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_301 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_303 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_305 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_307 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_309 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_311 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_313 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_315 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_317 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_319 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_321 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_323 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_325 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_327 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_329 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_331 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_onEnter_333 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_230 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_232 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_234 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_236 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_238 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_240 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_242 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_244 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_246 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_248 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_250 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_252 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_254 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_256 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_258 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_260 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_262 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_264 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_266 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_268 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_270 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_272 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_274 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_276 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_278 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_280 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_282 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_284 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_286 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_288 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_290 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_292 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_294 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_296 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_298 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_300 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_302 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_304 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_306 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_308 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_310 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_312 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_314 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_316 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_318 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_320 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_322 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_324 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_326 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_328 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_330 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_332 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function def_refreshVariables_334 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(exclusionPattern, wcLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 139, column 55
    function mode_335 () : java.lang.Object {
      return exclusionPattern.CodeIdentifier
    }
    
    property get exclusionPattern () : gw.api.productmodel.ExclusionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ExclusionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7PolicyCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_10 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_112 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_12 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_14 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_16 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_18 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_20 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_22 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_24 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_26 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_28 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_30 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_32 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_34 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_36 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_62 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_64 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_66 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_68 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_70 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_72 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_74 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_76 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_8 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_80 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_82 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_11 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_13 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_15 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_17 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_19 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_21 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_9 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, wcLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at WC7LineCoverageCV.pcf: line 71, column 56
    function mode_114 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ConditionsDVExpressionsImpl extends WC7LineCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 94, column 80
    function initialValue_117 () : java.util.List<gw.api.productmodel.ConditionPattern> {
      return conditionPatternsEmployeeLeasingPartition.get(false)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7LineCoverageCV.pcf: line 106, column 32
    function sortBy_118 (conditionPattern :  gw.api.productmodel.ConditionPattern) : java.lang.Object {
      return conditionPattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=WC7ConditionGroupIterator) at WC7LineCoverageCV.pcf: line 103, column 86
    function value_226 () : java.util.List<gw.api.productmodel.ConditionPattern> {
      return wc7GroupConditionPatterns
    }
    
    property get wc7GroupConditionPatterns () : java.util.List<gw.api.productmodel.ConditionPattern> {
      return getVariableValue("wc7GroupConditionPatterns", 1) as java.util.List<gw.api.productmodel.ConditionPattern>
    }
    
    property set wc7GroupConditionPatterns ($arg :  java.util.List<gw.api.productmodel.ConditionPattern>) {
      setVariableValue("wc7GroupConditionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ExclusionsDVExpressionsImpl extends WC7LineCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 124, column 78
    function initialValue_227 () : java.util.List<gw.api.productmodel.ExclusionPattern> {
      return exclusionPatternsEmployeeLeasingPartition.get(false)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7LineCoverageCV.pcf: line 136, column 30
    function sortBy_228 (exclusionPattern :  gw.api.productmodel.ExclusionPattern) : java.lang.Object {
      return exclusionPattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=WC7ExclusionGroupIterator) at WC7LineCoverageCV.pcf: line 133, column 84
    function value_336 () : java.util.List<gw.api.productmodel.ExclusionPattern> {
      return wc7GroupExclusionPatterns
    }
    
    property get wc7GroupExclusionPatterns () : java.util.List<gw.api.productmodel.ExclusionPattern> {
      return getVariableValue("wc7GroupExclusionPatterns", 1) as java.util.List<gw.api.productmodel.ExclusionPattern>
    }
    
    property set wc7GroupExclusionPatterns ($arg :  java.util.List<gw.api.productmodel.ExclusionPattern>) {
      setVariableValue("wc7GroupExclusionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7LineCoverageCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=SplitBases) at WC7LineCoverageCV.pcf: line 86, column 75
    function action_116 () : void {
      updateAllBasis()
    }
    
    // 'action' attribute on ToolbarButton (id=SplitBases) at WC7LineCoverageCV.pcf: line 43, column 75
    function action_3 () : void {
      updateAllBasis()
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 20, column 113
    function initialValue_0 () : java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ConditionPattern>> {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompLineCategoryCond").availableConditionPatternsForCoverable(wcLine).partition(\ c -> gw.lob.wc7.WC7EmployeeLeasingClauseIdentifier.isEmployeeLeasingClause(c))
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 25, column 113
    function initialValue_1 () : java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ExclusionPattern>> {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompLineCategoryExcl").availableExclusionPatternsForCoverable(wcLine, openForEdit).partition(\ c -> gw.lob.wc7.WC7EmployeeLeasingClauseIdentifier.isEmployeeLeasingClause(c))
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 30, column 23
    function initialValue_2 () : boolean {
      return invalidateIteratorsIfNecessary()
    }
    
    property get conditionPatternsEmployeeLeasingPartition () : java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ConditionPattern>> {
      return getVariableValue("conditionPatternsEmployeeLeasingPartition", 0) as java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ConditionPattern>>
    }
    
    property set conditionPatternsEmployeeLeasingPartition ($arg :  java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ConditionPattern>>) {
      setVariableValue("conditionPatternsEmployeeLeasingPartition", 0, $arg)
    }
    
    property get dummyVar () : boolean {
      return getVariableValue("dummyVar", 0) as java.lang.Boolean
    }
    
    property set dummyVar ($arg :  boolean) {
      setVariableValue("dummyVar", 0, $arg)
    }
    
    property get exclusionPatternsEmployeeLeasingPartition () : java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ExclusionPattern>> {
      return getVariableValue("exclusionPatternsEmployeeLeasingPartition", 0) as java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ExclusionPattern>>
    }
    
    property set exclusionPatternsEmployeeLeasingPartition ($arg :  java.util.Map<java.lang.Boolean, java.util.List<gw.api.productmodel.ExclusionPattern>>) {
      setVariableValue("exclusionPatternsEmployeeLeasingPartition", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get previousEmpLeasingDetails () : entity.WC7IncludedLaborContactDetail[] {
      return getVariableValue("previousEmpLeasingDetails", 0) as entity.WC7IncludedLaborContactDetail[]
    }
    
    property set previousEmpLeasingDetails ($arg :  entity.WC7IncludedLaborContactDetail[]) {
      setVariableValue("previousEmpLeasingDetails", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getRequireValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    
    function dummyCreateAndAdd() : WC7Jurisdiction {
      return null
    }
    
    function updateAllBasis(){
      if((CurrentLocation as pcf.api.Wizard).saveDraft()){
        wcLine.updateWCExposuresAndModifiers()
      }
    }
          
    property get JurisdictionsThatCanBeAdded(): Jurisdiction[] {
      var existingJurisdictions = wcLine.WC7Jurisdictions.map(\j -> j.Jurisdiction).toSet()
      var possibleJurisdicitons = wcLine.Branch.LocationStates.toSet()
      possibleJurisdicitons.removeAll(existingJurisdictions)
      return possibleJurisdicitons.toTypedArray()
    }
    
    function getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(covJuris : WC7Jurisdiction) : entity.OfficialID[] {
      return wcLine.Branch.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.OfficialIDs
            .where(\ officialID ->
              officialID.State == covJuris.Jurisdiction)
    }
    
    function outputConverterForOfficialIDs(VALUE : OfficialID[]) : String {
      var str = ""
      var first = true
      for (var Item in VALUE) {
        var idValue = Item.getOfficialIDValue()
        if(idValue != null) {
          if(!first) {
            str = str + ", "
          }
          first = false
          str = str + idValue
        }
      }
      return str
    }
    
    function modeForClausePattern(pattern : ClausePattern) : String {
      return WC7ServiceLocator.Instance.CoverageInputSetModeFinder(pattern)
    }
    
    function invalidateIteratorsIfNecessary() : boolean {
      var currentEmpLeasingDetails = wcLine.getIncludedLaborClientDetails(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ClausePattern>("WC7EmployeeLeasingClientEndorsementCond"))
      
      if(previousEmpLeasingDetails != null and not currentEmpLeasingDetails.toList().containsAll(previousEmpLeasingDetails?.toList())) {
        gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, WC7ContactDetail)
      }
      previousEmpLeasingDetails = currentEmpLeasingDetails
      return true
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7LineCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7PolicyCoveragesDVExpressionsImpl extends WC7LineCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7LineCoverageCV.pcf: line 51, column 59
    function initialValue_4 () : gw.api.productmodel.CoveragePattern[] {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompLineCategory").availableCoveragePatternsForCoverable(wcLine, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7LineCoverageCV.pcf: line 68, column 32
    function sortBy_7 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=WC7CoverageGroupIterator) at WC7LineCoverageCV.pcf: line 65, column 65
    function value_115 () : gw.api.productmodel.CoveragePattern[] {
      return wc7GroupCoveragePatterns
    }
    
    // 'value' attribute on TextInput (id=CoveredStates_Input) at WC7LineCoverageCV.pcf: line 59, column 45
    function value_5 () : java.lang.String {
      return wcLine.CoveredStates.join(",")
    }
    
    property get wc7GroupCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("wc7GroupCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set wc7GroupCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("wc7GroupCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  
}