package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AddressesPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AddressesPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelSet (id=AddressesPanelSet) at AddressesPanelSet.pcf: line 8, column 28
    function available_69 () : java.lang.Boolean {
      return period.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at AddressesPanelSet.pcf: line 25, column 75
    function initialValue_0 () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> period.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at AddressesPanelSet.pcf: line 30, column 33
    function initialValue_1 () : java.lang.Boolean {
      return period != null ? openForEditInit.get() : CurrentLocation.InEditMode
    }
    
    property get account () : entity.Account {
      return getRequireValue("account", 0) as entity.Account
    }
    
    property set account ($arg :  entity.Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get contact () : Contact {
      return getRequireValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get openForEdit () : java.lang.Boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  java.lang.Boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get openForEditInit () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return getVariableValue("openForEditInit", 0) as gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>
    }
    
    property set openForEditInit ($arg :  gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>) {
      setVariableValue("openForEditInit", 0, $arg)
    }
    
    property get period () : entity.PolicyPeriod {
      return getRequireValue("period", 0) as entity.PolicyPeriod
    }
    
    property set period ($arg :  entity.PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get showAddressTools () : boolean {
      return getRequireValue("showAddressTools", 0) as java.lang.Boolean
    }
    
    property set showAddressTools ($arg :  boolean) {
      setRequireValue("showAddressTools", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at AddressesPanelSet.pcf: line 166, column 48
    function action_45 () : void {
      helper.replaceAddressWithSuggested(suggestedAddress, standardizedSelectedAddress, false); addressList = null
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AddressesPanelSet.pcf: line 171, column 60
    function valueRoot_47 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AddressesPanelSet.pcf: line 171, column 60
    function value_46 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at AddressesPanelSet.pcf: line 175, column 60
    function value_49 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AddressesPanelSet.pcf: line 179, column 52
    function value_52 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at AddressesPanelSet.pcf: line 184, column 48
    function value_55 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at AddressesPanelSet.pcf: line 188, column 58
    function value_58 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(2) as entity.Address
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=survivorAddress) at AddressesPanelSet.pcf: line 215, column 58
    function action_63 () : void {
      contact.mergeAddresses(survivorAddress, selectedAddress);gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, entity.Address)
    }
    
    // 'confirmMessage' attribute on MenuItem (id=survivorAddress) at AddressesPanelSet.pcf: line 215, column 58
    function confirmMessage_64 () : java.lang.String {
      return DisplayKey.get("Web.ContactDetail.AddressTools.MergeAddress.Confirm", survivorAddress, selectedAddress)
    }
    
    // 'label' attribute on MenuItem (id=survivorAddress) at AddressesPanelSet.pcf: line 215, column 58
    function label_65 () : java.lang.Object {
      return survivorAddress.DisplayName
    }
    
    property get survivorAddress () : entity.Address {
      return getIteratedValue(2) as entity.Address
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on RadioButtonCell (id=Primary_Cell) at AddressesPanelSet.pcf: line 84, column 59
    function action_10 () : void {
      contact.makePrimaryAddress(address)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at AddressesPanelSet.pcf: line 75, column 42
    function checkBoxVisible_21 () : java.lang.Boolean {
      return address != contact.PrimaryAddress && address != period?.PolicyAddress.Address
    }
    
    // 'value' attribute on TypeKeyCell (id=AddressType_Cell) at AddressesPanelSet.pcf: line 91, column 50
    function valueRoot_14 () : java.lang.Object {
      return address
    }
    
    // 'value' attribute on RadioButtonCell (id=Primary_Cell) at AddressesPanelSet.pcf: line 84, column 59
    function value_11 () : java.lang.Boolean {
      return address == contact.PrimaryAddress
    }
    
    // 'value' attribute on TypeKeyCell (id=AddressType_Cell) at AddressesPanelSet.pcf: line 91, column 50
    function value_13 () : typekey.AddressType {
      return address.AddressType
    }
    
    // 'value' attribute on TextCell (id=DisplayedName_Cell) at AddressesPanelSet.pcf: line 96, column 30
    function value_16 () : java.lang.String {
      return address.addressString(",", false, false)
    }
    
    // 'value' attribute on TextCell (id=Description_TDIC_Cell) at AddressesPanelSet.pcf: line 101, column 46
    function value_18 () : java.lang.String {
      return address.Description
    }
    
    property get address () : entity.Address {
      return getIteratedValue(2) as entity.Address
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AddressesPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AddressesPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Standardize) at AddressesPanelSet.pcf: line 58, column 59
    function action_5 () : void {
      addressList = helper.validateAddress(selectedAddress, false); standardizedSelectedAddress = selectedAddress; helper.displayExceptions()
    }
    
    // 'available' attribute on PanelRef at AddressesPanelSet.pcf: line 49, column 48
    function available_25 () : java.lang.Boolean {
      return period?.profileChange_TDIC
    }
    
    // 'available' attribute on ToolbarButton (id=Standardize) at AddressesPanelSet.pcf: line 58, column 59
    function available_3 () : java.lang.Boolean {
      return standardizeVisible and openForEdit
    }
    
    // 'available' attribute on ToolbarButton (id=MergeAddresses) at AddressesPanelSet.pcf: line 206, column 101
    function available_67 () : java.lang.Boolean {
      return showAddressTools and contact.PrimaryAddress != selectedAddress and not selectedAddress.New
    }
    
    // 'def' attribute on InputSetRef at AddressesPanelSet.pcf: line 117, column 102
    function def_onEnter_27 (def :  pcf.LinkedAddressInputSet) : void {
      def.onEnter(selectedAddress, contact, period, account, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at AddressesPanelSet.pcf: line 120, column 67
    function def_onEnter_30 (def :  pcf.AddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at AddressesPanelSet.pcf: line 131, column 43
    function def_onEnter_32 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.onEnter(selectedAddress)
    }
    
    // 'def' attribute on InputSetRef at AddressesPanelSet.pcf: line 117, column 102
    function def_refreshVariables_28 (def :  pcf.LinkedAddressInputSet) : void {
      def.refreshVariables(selectedAddress, contact, period, account, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at AddressesPanelSet.pcf: line 120, column 67
    function def_refreshVariables_31 (def :  pcf.AddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at AddressesPanelSet.pcf: line 131, column 43
    function def_refreshVariables_33 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.refreshVariables(selectedAddress)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AddressesPanelSet.pcf: line 136, column 54
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedAddress.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on InputSetRef at AddressesPanelSet.pcf: line 120, column 67
    function editable_29 () : java.lang.Boolean {
      return selectedAddress.LinkedAddress == null
    }
    
    // 'initialValue' attribute on Variable at AddressesPanelSet.pcf: line 41, column 78
    function initialValue_2 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AddressesPanelSet.pcf: line 171, column 60
    function sortValue_40 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at AddressesPanelSet.pcf: line 175, column 60
    function sortValue_41 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AddressesPanelSet.pcf: line 179, column 52
    function sortValue_42 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at AddressesPanelSet.pcf: line 184, column 48
    function sortValue_43 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at AddressesPanelSet.pcf: line 188, column 58
    function sortValue_44 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'value' attribute on RadioButtonCell (id=Primary_Cell) at AddressesPanelSet.pcf: line 84, column 59
    function sortValue_6 (address :  entity.Address) : java.lang.Object {
      return address == contact.PrimaryAddress
    }
    
    // 'value' attribute on TypeKeyCell (id=AddressType_Cell) at AddressesPanelSet.pcf: line 91, column 50
    function sortValue_7 (address :  entity.Address) : java.lang.Object {
      return address.AddressType
    }
    
    // 'value' attribute on TextCell (id=DisplayedName_Cell) at AddressesPanelSet.pcf: line 96, column 30
    function sortValue_8 (address :  entity.Address) : java.lang.Object {
      return address.addressString(",", false, false)
    }
    
    // 'value' attribute on TextCell (id=Description_TDIC_Cell) at AddressesPanelSet.pcf: line 101, column 46
    function sortValue_9 (address :  entity.Address) : java.lang.Object {
      return address.Description
    }
    
    // 'toAdd' attribute on RowIterator at AddressesPanelSet.pcf: line 75, column 42
    function toAdd_22 (address :  entity.Address) : void {
      contact.addAddress(address)
    }
    
    // 'toRemove' attribute on RowIterator at AddressesPanelSet.pcf: line 75, column 42
    function toRemove_23 (address :  entity.Address) : void {
      if (address != contact.PrimaryAddress && address != period?.PolicyAddress.Address) contact.safeRemoveAddress(address) 
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AddressesPanelSet.pcf: line 136, column 54
    function valueRoot_37 () : java.lang.Object {
      return selectedAddress
    }
    
    // 'value' attribute on RowIterator at AddressesPanelSet.pcf: line 75, column 42
    function value_24 () : entity.Address[] {
      return contact.AllAddresses
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AddressesPanelSet.pcf: line 136, column 54
    function value_35 () : java.lang.String {
      return selectedAddress.Description
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at AddressesPanelSet.pcf: line 158, column 46
    function value_61 () : entity.Address[] {
      return addressList.toTypedArray()
    }
    
    // 'value' attribute on MenuItemIterator at AddressesPanelSet.pcf: line 210, column 48
    function value_66 () : entity.Address[] {
      return contact.AllAddresses.subtract({selectedAddress}).where(\ a -> not a.New).toTypedArray()
    }
    
    // 'visible' attribute on Label at AddressesPanelSet.pcf: line 115, column 77
    function visible_26 () : java.lang.Boolean {
      return selectedAddress == period?.PolicyAddress.Address
    }
    
    // 'visible' attribute on PanelRef at AddressesPanelSet.pcf: line 147, column 83
    function visible_62 () : java.lang.Boolean {
      return (addressList ?.Count > 0 ? true : false) and openForEdit
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 1) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 1, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 1) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 1, $arg)
    }
    
    property get selectedAddress () : Address {
      return getCurrentSelection(1) as Address
    }
    
    property set selectedAddress ($arg :  Address) {
      setCurrentSelection(1, $arg)
    }
    
    property get standardizedSelectedAddress () : Address {
      return getVariableValue("standardizedSelectedAddress", 1) as Address
    }
    
    property set standardizedSelectedAddress ($arg :  Address) {
      setVariableValue("standardizedSelectedAddress", 1, $arg)
    }
    
    
  }
  
  
}