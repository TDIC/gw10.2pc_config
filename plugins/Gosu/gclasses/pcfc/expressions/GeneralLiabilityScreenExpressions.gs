package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.productmodel.ClausePatternLookup
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GeneralLiabilityScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GeneralLiabilityScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function available_28 () : java.lang.Boolean {
      return policyPeriod.getLineExists(glLine.Pattern)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at GeneralLiabilityScreen.pcf: line 51, column 50
    function def_onEnter_24 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(glLine, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 53, column 58
    function def_onEnter_26 (def :  pcf.OOSEPanelSet) : void {
      def.onEnter(policyPeriod, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function def_onEnter_29 (def :  pcf.PolicyLineDV_GLLine) : void {
      def.onEnter(glLine, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function def_onEnter_31 (def :  pcf.PolicyLineDV_PersonalAutoLine) : void {
      def.onEnter(glLine, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 69, column 50
    function def_onEnter_34 (def :  pcf.GLAdditionalCoveragesDV) : void {
      def.onEnter(glLine)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 92, column 78
    function def_onEnter_38 (def :  pcf.TDIC_AdditionalInsuredsDV) : void {
      def.onEnter(glLine, openForEdit, true, false)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 99, column 71
    function def_onEnter_41 (def :  pcf.TDIC_CertificateHolderDetailsDV) : void {
      def.onEnter(glLine, openForEdit)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 109, column 57
    function def_onEnter_44 (def :  pcf.TDIC_GLDiscountsDV) : void {
      def.onEnter(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 113, column 27
    function def_onEnter_46 (def :  pcf.GLRiskManagementDiscount_TDICDV) : void {
      def.onEnter(glLine)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 117, column 58
    function def_onEnter_48 (def :  pcf.TDIC_IRPMDiscountDV) : void {
      def.onEnter(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 121, column 172
    function def_onEnter_52 (def :  pcf.TDIC_GLIRPMModifiersDV) : void {
      def.onEnter(glLine)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at GeneralLiabilityScreen.pcf: line 135, column 29
    function def_onEnter_56 (def :  pcf.QuestionSets_TDICDV) : void {
      def.onEnter(underwritingQuestionSets,glLine.PolicyLine, null,true)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at GeneralLiabilityScreen.pcf: line 51, column 50
    function def_refreshVariables_25 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(glLine, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 53, column 58
    function def_refreshVariables_27 (def :  pcf.OOSEPanelSet) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function def_refreshVariables_30 (def :  pcf.PolicyLineDV_GLLine) : void {
      def.refreshVariables(glLine, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function def_refreshVariables_32 (def :  pcf.PolicyLineDV_PersonalAutoLine) : void {
      def.refreshVariables(glLine, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 69, column 50
    function def_refreshVariables_35 (def :  pcf.GLAdditionalCoveragesDV) : void {
      def.refreshVariables(glLine)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 92, column 78
    function def_refreshVariables_39 (def :  pcf.TDIC_AdditionalInsuredsDV) : void {
      def.refreshVariables(glLine, openForEdit, true, false)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 99, column 71
    function def_refreshVariables_42 (def :  pcf.TDIC_CertificateHolderDetailsDV) : void {
      def.refreshVariables(glLine, openForEdit)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 109, column 57
    function def_refreshVariables_45 (def :  pcf.TDIC_GLDiscountsDV) : void {
      def.refreshVariables(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 113, column 27
    function def_refreshVariables_47 (def :  pcf.GLRiskManagementDiscount_TDICDV) : void {
      def.refreshVariables(glLine)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 117, column 58
    function def_refreshVariables_49 (def :  pcf.TDIC_IRPMDiscountDV) : void {
      def.refreshVariables(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 121, column 172
    function def_refreshVariables_53 (def :  pcf.TDIC_GLIRPMModifiersDV) : void {
      def.refreshVariables(glLine)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at GeneralLiabilityScreen.pcf: line 135, column 29
    function def_refreshVariables_57 (def :  pcf.QuestionSets_TDICDV) : void {
      def.refreshVariables(underwritingQuestionSets,glLine.PolicyLine, null,true)
    }
    
    // 'editable' attribute on Screen (id=GeneralLiabilityScreen) at GeneralLiabilityScreen.pcf: line 7, column 33
    function editable_86 () : java.lang.Boolean {
      return openForEdit && policyPeriod.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityScreen.pcf: line 23, column 35
    function initialValue_0 () : productmodel.GLLine {
      return policyPeriod.GLLine
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityScreen.pcf: line 28, column 49
    function initialValue_1 () : gw.api.productmodel.QuestionSet[] {
      return policyPeriod.getGLUWQuestionSets_TDIC()
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityScreen.pcf: line 33, column 25
    function initialValue_2 () : QuoteType {
      return setInitialQuoteType()
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityScreen.pcf: line 38, column 23
    function initialValue_3 () : Boolean {
      return removeCoverageD()
    }
    
    // 'mode' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 61, column 43
    function mode_33 () : java.lang.Object {
      return glLine.Pattern.PublicID
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function mode_4 () : java.lang.Object {
      return job.Subtype
    }
    
    // 'onSelect' attribute on Card (id=GLAddlCov) at GeneralLiabilityScreen.pcf: line 67, column 102
    function onSelect_37 () : void {
      glLine.syncCoverages();removeCoverageD(); glLine.removeScheduledItems_TDIC()
    }
    
    // 'onSelect' attribute on Card (id=GeneralLiability_PremiumModificationCard) at GeneralLiabilityScreen.pcf: line 105, column 103
    function onSelect_55 () : void {
      tdic.web.pcf.helper.JobWizardHelper.onSelectOfPremiumModificationTab(glLine)
    }
    
    // 'onSelect' attribute on Card (id=GeneralLiability_UWInformationCard) at GeneralLiabilityScreen.pcf: line 132, column 96
    function onSelect_85 () : void {
      tdic.web.pcf.helper.JobWizardHelper.onSelectOfUWInformationTab(policyPeriod)
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at GeneralLiabilityScreen.pcf: line 165, column 50
    function sortValue_58 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.State
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 172, column 55
    function sortValue_59 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.PracticedInThisState_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 179, column 55
    function sortValue_60 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.PlantoPracticeInThisState_TDIC
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at GeneralLiabilityScreen.pcf: line 186, column 70
    function sortValue_61 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.DatesOfPractice_TDIC
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at GeneralLiabilityScreen.pcf: line 156, column 65
    function toCreateAndAdd_79 () : entity.GLDentLicenseStates_TDIC {
      return glLine.createAndAddGLDentLicenseStates_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at GeneralLiabilityScreen.pcf: line 156, column 65
    function toRemove_80 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : void {
      glLine.removeFromGLDentLicenseStates_TDIC(dentLicenseState)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_11 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_13 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_15 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_17 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_19 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_21 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_5 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_7 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_onEnter_9 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.onEnter(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_10 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_12 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_14 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_16 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_18 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_20 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_22 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_6 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at GeneralLiabilityScreen.pcf: line 43, column 91
    function toolbarButtonSet_refreshVariables_8 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.refreshVariables(policyPeriod, job, jobWizardHelper)
    }
    
    // 'validationExpression' attribute on ListViewPanel (id=DentLicenseStatesLV) at GeneralLiabilityScreen.pcf: line 149, column 177
    function validationExpression_82 () : java.lang.Object {
      return glLine.GLDentLicenseStates_TDIC?.Count == 0 ? DisplayKey.get("TDIC.Web.Policy.GL.DentalLicenseInOtherStatesValidation") : null
    }
    
    // 'value' attribute on RowIterator at GeneralLiabilityScreen.pcf: line 156, column 65
    function value_81 () : entity.GLDentLicenseStates_TDIC[] {
      return glLine.GLDentLicenseStates_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=QuoteRequestedAlert) at GeneralLiabilityScreen.pcf: line 48, column 80
    function visible_23 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.isQuoteRequestInProgress(policyPeriod)
    }
    
    // 'visible' attribute on Card (id=GLAddlCov) at GeneralLiabilityScreen.pcf: line 67, column 102
    function visible_36 () : java.lang.Boolean {
      return tdic.web.pcf.helper.JobWizardHelper.canAdditionalCoveragesTabVisible(policyPeriod)
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_AdditionalInsuredCard) at GeneralLiabilityScreen.pcf: line 90, column 216
    function visible_40 () : java.lang.Boolean {
      return tdic.web.pcf.helper.JobWizardHelper.canAdditionalInsuredTabVisible(policyPeriod)//!(policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") && quoteType != QuoteType.TC_QUICK
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_CertificateHoldersCard) at GeneralLiabilityScreen.pcf: line 97, column 171
    function visible_43 () : java.lang.Boolean {
      return tdic.web.pcf.helper.JobWizardHelper.canCertificateHoldersTabVisible(policyPeriod)//policyPeriod.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
    }
    
    // 'visible' attribute on TitleBar at GeneralLiabilityScreen.pcf: line 124, column 64
    function visible_50 () : java.lang.Boolean {
      return glLine.GLIRPMDiscount_TDICExists //true
    }
    
    // 'visible' attribute on PanelRef at GeneralLiabilityScreen.pcf: line 121, column 172
    function visible_51 () : java.lang.Boolean {
      return glLine.GLIRPMDiscount_TDICExists and (not User.util.CurrentUser.ExternalUser and (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic))
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_PremiumModificationCard) at GeneralLiabilityScreen.pcf: line 105, column 103
    function visible_54 () : java.lang.Boolean {
      return tdic.web.pcf.helper.JobWizardHelper.canPremiumModificationsTabVisible(policyPeriod)
    }
    
    // 'visible' attribute on DetailViewPanel at GeneralLiabilityScreen.pcf: line 137, column 293
    function visible_83 () : java.lang.Boolean {
      return (policyPeriod.Offering.CodeIdentifier != "PLCyberLiab_TDIC" and glLine.getAnswerForGLUWQuestion_TDIC("HoldDentalLicenseInOtherStates_TDIC")) and policyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION and !policyPeriod.isQuickQuote_TDIC
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_UWInformationCard) at GeneralLiabilityScreen.pcf: line 132, column 96
    function visible_84 () : java.lang.Boolean {
      return tdic.web.pcf.helper.JobWizardHelper.canUWInformationTabVisible(policyPeriod)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getVariableValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    property get removeCoverageD () : Boolean {
      return getVariableValue("removeCoverageD", 0) as Boolean
    }
    
    property set removeCoverageD ($arg :  Boolean) {
      setVariableValue("removeCoverageD", 0, $arg)
    }
    
    property get underwritingQuestionSets () : gw.api.productmodel.QuestionSet[] {
      return getVariableValue("underwritingQuestionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set underwritingQuestionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setVariableValue("underwritingQuestionSets", 0, $arg)
    }
    
    
    function setInitialQuoteType() : QuoteType {
          if (policyPeriod.Job typeis Submission) {
            return (policyPeriod.Job as Submission).QuoteType
          }
          return null
        }
    
    function removeCoverageD():Boolean{
      //BR-026
      if(job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements){
        var pattern1 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLDentalEmpPracLiabCov_TDIC")
        glLine.setCoverageExists(pattern1,false)
      }
      //BR-027
      var patternERE = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodCov_TDIC")
      if(job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).HasElements 
        and (glLine.Branch.BasedOn.Canceled or glLine.Branch.BasedOn.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)
        and job.ChangeReasons.Count == 1){
        glLine.setCoverageExists(patternERE,true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternERE,glLine)
      }else{
        glLine.setCoverageExists(patternERE,false)
      }
      //BR-027
      var patternERECYB = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLCyvSupplementalERECov_TDIC")
      if(job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).HasElements and (glLine.Branch.BasedOn.Canceled
      or glLine.Branch.BasedOn.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code or glLine.Branch.BasedOn.RenewalStatus_TDIC.DisplayName == PolicyPeriodStatus.TC_NONRENEWED.DisplayName)
        and job.ChangeReasons.Count == 1){
        glLine.setCoverageExists(patternERECYB,true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternERECYB,glLine)
      }else{
        glLine.setCoverageExists(patternERECYB,false)
      }
      //BR-028
      var patternEREDEPL = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodDPLCov_TDIC")
      if(job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements){
        glLine.setCoverageExists(patternEREDEPL,true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternEREDEPL,glLine)
      }else{
        glLine.setCoverageExists(patternEREDEPL,false)
      }
      return false
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GeneralLiabilityScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at GeneralLiabilityScreen.pcf: line 165, column 50
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.State = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 172, column 55
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.PracticedInThisState_TDIC = (__VALUE_TO_SET as typekey.YesNo_TDIC)
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 179, column 55
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.PlantoPracticeInThisState_TDIC = (__VALUE_TO_SET as typekey.YesNo_TDIC)
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at GeneralLiabilityScreen.pcf: line 186, column 70
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.DatesOfPractice_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filter' attribute on TypeKeyCell (id=state_Cell) at GeneralLiabilityScreen.pcf: line 165, column 50
    function filter_65 (VALUE :  typekey.State, VALUES :  typekey.State[]) : java.util.List<typekey.State> {
      return VALUES.where(\elt -> elt.hasCategory(Country.TC_US)).toList()
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at GeneralLiabilityScreen.pcf: line 165, column 50
    function valueRoot_64 () : java.lang.Object {
      return dentLicenseState
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at GeneralLiabilityScreen.pcf: line 165, column 50
    function value_62 () : typekey.State {
      return dentLicenseState.State
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 172, column 55
    function value_67 () : typekey.YesNo_TDIC {
      return dentLicenseState.PracticedInThisState_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at GeneralLiabilityScreen.pcf: line 179, column 55
    function value_71 () : typekey.YesNo_TDIC {
      return dentLicenseState.PlantoPracticeInThisState_TDIC
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at GeneralLiabilityScreen.pcf: line 186, column 70
    function value_75 () : java.lang.String {
      return dentLicenseState.DatesOfPractice_TDIC
    }
    
    property get dentLicenseState () : entity.GLDentLicenseStates_TDIC {
      return getIteratedValue(1) as entity.GLDentLicenseStates_TDIC
    }
    
    
  }
  
  
}