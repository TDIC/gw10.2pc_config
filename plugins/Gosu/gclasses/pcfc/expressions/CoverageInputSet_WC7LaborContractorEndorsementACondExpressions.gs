package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7LaborContractorEndorsementACondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 34, column 109
    function available_56 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 66, column 75
    function conversionExpression_10 (PickedValue :  Contact) : entity.WC7IncludedLaborContactDetail {
      return addNewLaborContractorDetailForContact(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 20, column 34
    function initialValue_0 () : WC7WorkersCompLine {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 24, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 71, column 131
    function label_16 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.Existing", WC7PolicyLaborContractor.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 34, column 109
    function label_57 () : java.lang.Object {
      return conditionPattern.DisplayName
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 66, column 75
    function pickLocation_11 () : void {
      ContactSearchPopup.push(TC_LABORCONTRACTOR)
    }
    
    // 'onToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 34, column 109
    function setter_58 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, conditionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 78, column 34
    function sortBy_12 (acctContact :  entity.AccountContact) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 53, column 32
    function sortBy_5 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 128, column 53
    function sortValue_21 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.WC7LaborContact
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function sortValue_22 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 149, column 76
    function sortValue_23 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 155, column 77
    function sortValue_24 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.ContractExpirationDate
    }
    
    // 'value' attribute on TextCell (id=ContractOrProject_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 162, column 47
    function sortValue_25 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.ContractProject
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 120, column 84
    function toRemove_53 (policyLaborContractorDetail :  entity.WC7IncludedLaborContactDetail) : void {
      policyLaborContractorDetail.WC7LaborContact.removeDetail(policyLaborContractorDetail)
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 28, column 37
    function valueRoot_3 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 75, column 53
    function value_15 () : entity.AccountContact[] {
      return theWC7Line.WC7PolicyLaborContractorDetailExistingCandidates
    }
    
    // 'value' attribute on HiddenInput (id=CondPatternDisplayName_Input) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 28, column 37
    function value_2 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 94, column 53
    function value_20 () : entity.AccountContact[] {
      return theWC7Line.WC7PolicyLaborContractorDetailOtherCandidates
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 120, column 84
    function value_54 () : java.util.List<entity.WC7IncludedLaborContactDetail> {
      return theWC7Line.includedLaborContactDetailsFor(theWC7Line.WC7LaborContractorEndorsementACond)
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 50, column 49
    function value_9 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLABORCONTRACTOR)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 34, column 109
    function visible_55 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 169, column 101
    function visible_61 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get theWC7Line () : WC7WorkersCompLine {
      return getVariableValue("theWC7Line", 0) as WC7WorkersCompLine
    }
    
    property set theWC7Line ($arg :  WC7WorkersCompLine) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    function addNewLaborContractorDetailForContact(aContact : entity.Contact) : WC7IncludedLaborContactDetail {
      var newLaborContractor = theWC7Line.addIncludedLaborContractorDetailForContact(aContact, theWC7Line.WC7LaborContractorEndorsementACond)
      return newLaborContractor
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 83, column 96
    function label_13 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 83, column 96
    function toCreateAndAdd_14 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborContractorDetailForContact(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 102, column 96
    function label_18 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 102, column 96
    function toCreateAndAdd_19 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborContractorDetailForContact(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 128, column 53
    function action_26 () : void {
      EditPolicyContactRolePopup.push(policyLaborContractorDetail.WC7LaborContact, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 128, column 53
    function action_dest_27 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyLaborContractorDetail.WC7LaborContact, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborContractorDetail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 149, column 76
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborContractorDetail.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 155, column 77
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborContractorDetail.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=ContractOrProject_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 162, column 47
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborContractorDetail.ContractProject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function valueRange_37 () : java.lang.Object {
      return theWC7Line.stateFilterFor(conditionPattern).TypeKeys
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 128, column 53
    function valueRoot_29 () : java.lang.Object {
      return policyLaborContractorDetail
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 128, column 53
    function value_28 () : entity.WC7LaborContact {
      return policyLaborContractorDetail.WC7LaborContact
    }
    
    // 'value' attribute on TypeKeyCell (id=Inclusion_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 134, column 34
    function value_31 () : typekey.Inclusion {
      return policyLaborContractorDetail.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function value_34 () : typekey.Jurisdiction {
      return policyLaborContractorDetail.Jurisdiction
    }
    
    // 'value' attribute on DateCell (id=ContractEffectiveDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 149, column 76
    function value_41 () : java.util.Date {
      return policyLaborContractorDetail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ContractExpirationDate_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 155, column 77
    function value_45 () : java.util.Date {
      return policyLaborContractorDetail.ContractExpirationDate
    }
    
    // 'value' attribute on TextCell (id=ContractOrProject_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 162, column 47
    function value_49 () : java.lang.String {
      return policyLaborContractorDetail.ContractProject
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function verifyValueRangeIsAllowedType_38 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 143, column 51
    function verifyValueRange_39 () : void {
      var __valueRangeArg = theWC7Line.stateFilterFor(conditionPattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    property get policyLaborContractorDetail () : entity.WC7IncludedLaborContactDetail {
      return getIteratedValue(1) as entity.WC7IncludedLaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorEndorsementACond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 59, column 137
    function conversionExpression_7 (PickedValue :  WC7LaborContactDetail) : entity.WC7IncludedLaborContactDetail {
      return PickedValue as WC7IncludedLaborContactDetail
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 59, column 137
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7LaborContractorEndorsementACond.pcf: line 59, column 137
    function pickLocation_8 () : void {
      WC7NewLaborContractorForContactTypePopup.push(theWC7Line.Branch.WC7Line, contactType, conditionPattern)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}