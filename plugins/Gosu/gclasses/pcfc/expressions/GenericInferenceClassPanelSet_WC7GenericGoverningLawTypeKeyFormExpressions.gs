package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/forms/GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GenericInferenceClassPanelSet_WC7GenericGoverningLawTypeKeyFormExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/forms/GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GenericInferenceClassPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=SelectedGoverningLaw_Input) at GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf: line 25, column 48
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      formPattern.GoverningLawType = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'initialValue' attribute on Variable at GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf: line 16, column 33
    function initialValue_0 () : WC7GoverningLaw[] {
      return  WC7GoverningLaw.getTypeKeys( false ).where( \typeKey -> typeKey.hasCategory(typekey.WC7LiabilityAct.TC_WORKERSCOMP) ).toTypedArray()
    }
    
    // 'value' attribute on TypeKeyInput (id=SelectedGoverningLaw_Input) at GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf: line 25, column 48
    function valueRoot_3 () : java.lang.Object {
      return formPattern
    }
    
    // 'value' attribute on TypeKeyInput (id=SelectedGoverningLaw_Input) at GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf: line 25, column 48
    function value_1 () : typekey.WC7GoverningLaw {
      return formPattern.GoverningLawType
    }
    
    // 'visible' attribute on PanelSet (id=GenericInferenceClassPanelSet) at GenericInferenceClassPanelSet.WC7GenericGoverningLawTypeKeyForm.pcf: line 8, column 76
    function visible_5 () : java.lang.Boolean {
      return formPattern.PolicyLinePatternRef.Code == "WC7Line"
    }
    
    property get formPattern () : FormPattern {
      return getRequireValue("formPattern", 0) as FormPattern
    }
    
    property set formPattern ($arg :  FormPattern) {
      setRequireValue("formPattern", 0, $arg)
    }
    
    property get governingLaw () : WC7GoverningLaw[] {
      return getVariableValue("governingLaw", 0) as WC7GoverningLaw[]
    }
    
    property set governingLaw ($arg :  WC7GoverningLaw[]) {
      setVariableValue("governingLaw", 0, $arg)
    }
    
    
  }
  
  
}