package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BALineCoveragePanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BALineCoveragePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at BALineCoveragePanelSet.pcf: line 26, column 38
    function def_onEnter_3 (def :  pcf.BALineDV) : void {
      def.onEnter(baLine.Branch)
    }
    
    // 'def' attribute on PanelRef at BALineCoveragePanelSet.pcf: line 26, column 38
    function def_refreshVariables_4 (def :  pcf.BALineDV) : void {
      def.refreshVariables(baLine.Branch)
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 16, column 52
    function initialValue_0 () : gw.api.productmodel.CoverageCategory {
      return baLine.Pattern.getCoverageCategoryByPublicId("BAPOwnedLiabGrp")
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 20, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return baLine.Pattern.getCoverageCategoryByPublicId("BAPHiredGrp")
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 24, column 52
    function initialValue_2 () : gw.api.productmodel.CoverageCategory {
      return baLine.Pattern.getCoverageCategoryByPublicId("BAPNonownedGrp")
    }
    
    // 'title' attribute on TitleBar at BALineCoveragePanelSet.pcf: line 55, column 43
    function title_115 () : java.lang.String {
      return baHiredGroup.DisplayName
    }
    
    // 'title' attribute on TitleBar at BALineCoveragePanelSet.pcf: line 152, column 44
    function title_255 () : java.lang.String {
      return nonOwnedGroup.DisplayName
    }
    
    // 'title' attribute on TitleBar at BALineCoveragePanelSet.pcf: line 29, column 45
    function title_5 () : java.lang.String {
      return baOwnedLiabCat.DisplayName
    }
    
    property get baHiredGroup () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("baHiredGroup", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set baHiredGroup ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("baHiredGroup", 0, $arg)
    }
    
    property get baLine () : BusinessAutoLine {
      return getRequireValue("baLine", 0) as BusinessAutoLine
    }
    
    property set baLine ($arg :  BusinessAutoLine) {
      setRequireValue("baLine", 0, $arg)
    }
    
    property get baOwnedLiabCat () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("baOwnedLiabCat", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set baOwnedLiabCat ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("baOwnedLiabCat", 0, $arg)
    }
    
    property get nonOwnedGroup () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("nonOwnedGroup", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set nonOwnedGroup ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("nonOwnedGroup", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel2ExpressionsImpl extends BALineCoveragePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function defaultSetter_228 (__VALUE_TO_SET :  java.lang.Object) : void {
      newState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 61, column 62
    function initialValue_116 () : java.util.List<typekey.Jurisdiction> {
      return baLine.UnusedHiredAutoStates
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 66, column 32
    function initialValue_117 () : Jurisdiction {
      return unusedStates[0]
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 71, column 57
    function initialValue_118 () : gw.api.productmodel.CoveragePattern[] {
      return baHiredGroup.coveragePatternsForEntity(BusinessAutoLine).whereSelectedOrAvailable(baLine, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at BALineCoveragePanelSet.pcf: line 81, column 30
    function sortBy_119 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 126, column 55
    function sortValue_233 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.State
    }
    
    // 'value' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function sortValue_234 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.HiredAutoBasis.Basis
    }
    
    // 'value' attribute on BooleanRadioCell (id=IfAny_Cell) at BALineCoveragePanelSet.pcf: line 139, column 71
    function sortValue_235 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.HiredAutoBasis.IfAnyExposure
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=HiredAutoIterator) at BALineCoveragePanelSet.pcf: line 120, column 53
    function toCreateAndAdd_252 () : entity.BAJurisdiction {
      return baLine.createOrAddHiredAutoJurisdiction(newState)
    }
    
    // 'toRemove' attribute on RowIterator (id=HiredAutoIterator) at BALineCoveragePanelSet.pcf: line 120, column 53
    function toRemove_253 (jurisdiction :  entity.BAJurisdiction) : void {
      baLine.removeAsHiredAutoJurisdiction(jurisdiction)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function valueRange_229 () : java.lang.Object {
      return unusedStates
    }
    
    // 'value' attribute on InputIterator (id=BAPHiredGrpIterator) at BALineCoveragePanelSet.pcf: line 78, column 63
    function value_226 () : gw.api.productmodel.CoveragePattern[] {
      return baHiredGroupCoveragePatterns
    }
    
    // 'value' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function value_227 () : typekey.Jurisdiction {
      return newState
    }
    
    // 'value' attribute on RowIterator (id=HiredAutoIterator) at BALineCoveragePanelSet.pcf: line 120, column 53
    function value_254 () : entity.BAJurisdiction[] {
      return baLine.HiredAutoJurisdictions
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function verifyValueRangeIsAllowedType_230 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function verifyValueRangeIsAllowedType_230 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateHiredAuto_Input) at BALineCoveragePanelSet.pcf: line 103, column 51
    function verifyValueRange_231 () : void {
      var __valueRangeArg = unusedStates
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_230(__valueRangeArg)
    }
    
    property get baHiredGroupCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("baHiredGroupCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set baHiredGroupCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("baHiredGroupCoveragePatterns", 1, $arg)
    }
    
    property get newState () : Jurisdiction {
      return getVariableValue("newState", 1) as Jurisdiction
    }
    
    property set newState ($arg :  Jurisdiction) {
      setVariableValue("newState", 1, $arg)
    }
    
    property get unusedStates () : java.util.List<typekey.Jurisdiction> {
      return getVariableValue("unusedStates", 1) as java.util.List<typekey.Jurisdiction>
    }
    
    property set unusedStates ($arg :  java.util.List<typekey.Jurisdiction>) {
      setVariableValue("unusedStates", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanel3ExpressionsImpl extends BALineCoveragePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function defaultSetter_367 (__VALUE_TO_SET :  java.lang.Object) : void {
      newState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 158, column 62
    function initialValue_256 () : java.util.List<typekey.Jurisdiction> {
      return baLine.UnusedNonOwnedStates
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 163, column 32
    function initialValue_257 () : Jurisdiction {
      return unusedStates[0]
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 168, column 57
    function initialValue_258 () : gw.api.productmodel.CoveragePattern[] {
      return nonOwnedGroup.coveragePatternsForEntity(BusinessAutoLine).whereSelectedOrAvailable(baLine, openForEdit)
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 223, column 55
    function sortValue_372 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.State
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at BALineCoveragePanelSet.pcf: line 229, column 52
    function sortValue_373 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.NonOwnedBasis.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=TotalPartners_Cell) at BALineCoveragePanelSet.pcf: line 235, column 52
    function sortValue_374 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.NonOwnedBasis.NumPartners
    }
    
    // 'value' attribute on TextCell (id=TotalVolunteers_Cell) at BALineCoveragePanelSet.pcf: line 241, column 52
    function sortValue_375 (jurisdiction :  entity.BAJurisdiction) : java.lang.Object {
      return jurisdiction.NonOwnedBasis.NumVolunteers
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=NonownedIterator) at BALineCoveragePanelSet.pcf: line 217, column 53
    function toCreateAndAdd_391 () : entity.BAJurisdiction {
      return baLine.createOrAddNonOwnedJurisdiction(newState)
    }
    
    // 'toRemove' attribute on RowIterator (id=NonownedIterator) at BALineCoveragePanelSet.pcf: line 217, column 53
    function toRemove_392 (jurisdiction :  entity.BAJurisdiction) : void {
      baLine.removeAsNonOwnedJurisdiction(jurisdiction)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function valueRange_368 () : java.lang.Object {
      return unusedStates
    }
    
    // 'value' attribute on InputIterator (id=BAPNonownedGrpIterator) at BALineCoveragePanelSet.pcf: line 175, column 63
    function value_365 () : gw.api.productmodel.CoveragePattern[] {
      return nonOwnedGroupCoveragePatterns
    }
    
    // 'value' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function value_366 () : typekey.Jurisdiction {
      return newState
    }
    
    // 'value' attribute on RowIterator (id=NonownedIterator) at BALineCoveragePanelSet.pcf: line 217, column 53
    function value_393 () : entity.BAJurisdiction[] {
      return baLine.NonOwnedJurisdictions
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function verifyValueRangeIsAllowedType_369 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function verifyValueRangeIsAllowedType_369 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=SelectStateNonowned_Input) at BALineCoveragePanelSet.pcf: line 196, column 51
    function verifyValueRange_370 () : void {
      var __valueRangeArg = unusedStates
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_369(__valueRangeArg)
    }
    
    property get newState () : Jurisdiction {
      return getVariableValue("newState", 1) as Jurisdiction
    }
    
    property set newState ($arg :  Jurisdiction) {
      setVariableValue("newState", 1, $arg)
    }
    
    property get nonOwnedGroupCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("nonOwnedGroupCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set nonOwnedGroupCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("nonOwnedGroupCoveragePatterns", 1, $arg)
    }
    
    property get unusedStates () : java.util.List<typekey.Jurisdiction> {
      return getVariableValue("unusedStates", 1) as java.util.List<typekey.Jurisdiction>
    }
    
    property set unusedStates ($arg :  java.util.List<typekey.Jurisdiction>) {
      setVariableValue("unusedStates", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends BALineCoveragePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BALineCoveragePanelSet.pcf: line 35, column 57
    function initialValue_6 () : gw.api.productmodel.CoveragePattern[] {
      return baOwnedLiabCat.coveragePatternsForEntity(BusinessAutoLine).whereSelectedOrAvailable(baLine, openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at BALineCoveragePanelSet.pcf: line 45, column 30
    function sortBy_7 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=baLineLiabCatIterator) at BALineCoveragePanelSet.pcf: line 42, column 63
    function value_114 () : gw.api.productmodel.CoveragePattern[] {
      return baOwnedLiabCatCoveragePatterns
    }
    
    property get baOwnedLiabCatCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("baOwnedLiabCatCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set baOwnedLiabCatCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("baOwnedLiabCatCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_120 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_122 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_124 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_126 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_128 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_130 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_132 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_134 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_136 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_138 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_140 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_142 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_144 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_146 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_148 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_150 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_152 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_154 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_156 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_158 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_166 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_168 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_170 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_172 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_174 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_176 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_178 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_180 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_182 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_184 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_186 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_188 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_190 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_192 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_194 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_196 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_198 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_200 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_202 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_204 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_206 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_216 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_218 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_220 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_222 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_onEnter_224 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_219 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_221 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_223 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BAHiredGroupInputSet) at BALineCoveragePanelSet.pcf: line 84, column 42
    function def_refreshVariables_225 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DetailViewPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function available_239 () : java.lang.Boolean {
      return jurisdiction.HiredAutoBasis.IfAnyExposure == false
    }
    
    // 'value' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function defaultSetter_242 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.HiredAutoBasis.Basis = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioCell (id=IfAny_Cell) at BALineCoveragePanelSet.pcf: line 139, column 71
    function defaultSetter_249 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.HiredAutoBasis.IfAnyExposure = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'onChange' attribute on PostOnChange at BALineCoveragePanelSet.pcf: line 141, column 125
    function onChange_247 () : void {
      if (jurisdiction.HiredAutoBasis.IfAnyExposure) {jurisdiction.HiredAutoBasis.Basis = null}
    }
    
    // 'validationExpression' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function validationExpression_240 () : java.lang.Object {
      return baLine.getCostOfHireValidation(jurisdiction.HiredAutoBasis.Basis, jurisdiction.HiredAutoBasis.IfAnyExposure)
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 126, column 55
    function valueRoot_237 () : java.lang.Object {
      return jurisdiction
    }
    
    // 'value' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function valueRoot_243 () : java.lang.Object {
      return jurisdiction.HiredAutoBasis
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 126, column 55
    function value_236 () : typekey.Jurisdiction {
      return jurisdiction.State
    }
    
    // 'value' attribute on TextCell (id=CostHire_Cell) at BALineCoveragePanelSet.pcf: line 134, column 52
    function value_241 () : java.lang.Integer {
      return jurisdiction.HiredAutoBasis.Basis
    }
    
    // 'value' attribute on BooleanRadioCell (id=IfAny_Cell) at BALineCoveragePanelSet.pcf: line 139, column 71
    function value_248 () : java.lang.Boolean {
      return jurisdiction.HiredAutoBasis.IfAnyExposure
    }
    
    property get jurisdiction () : entity.BAJurisdiction {
      return getIteratedValue(2) as entity.BAJurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends DetailViewPanel3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_259 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_261 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_263 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_265 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_267 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_269 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_271 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_273 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_275 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_277 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_279 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_281 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_283 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_285 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_287 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_289 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_291 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_293 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_295 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_297 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_299 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_301 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_303 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_305 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_307 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_309 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_311 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_313 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_315 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_317 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_319 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_321 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_323 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_325 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_327 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_329 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_331 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_333 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_335 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_337 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_339 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_341 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_343 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_345 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_347 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_349 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_351 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_353 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_355 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_357 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_359 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_361 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_onEnter_363 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_260 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_262 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_264 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_266 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_268 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_270 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_272 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_274 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_276 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_278 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_280 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_282 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_284 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_286 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_288 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_290 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_292 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_294 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_296 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_298 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_300 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_302 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_304 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_306 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_308 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_310 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_312 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_314 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_316 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_318 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_320 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_322 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_324 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_326 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_328 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_330 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_332 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_334 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_336 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_338 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_340 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_342 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_344 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_346 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_348 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_350 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_352 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_354 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_356 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_358 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_360 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_362 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BANonOwnedGroupInputSet) at BALineCoveragePanelSet.pcf: line 178, column 45
    function def_refreshVariables_364 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends DetailViewPanel3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at BALineCoveragePanelSet.pcf: line 229, column 52
    function defaultSetter_380 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.NonOwnedBasis.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=TotalPartners_Cell) at BALineCoveragePanelSet.pcf: line 235, column 52
    function defaultSetter_384 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.NonOwnedBasis.NumPartners = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=TotalVolunteers_Cell) at BALineCoveragePanelSet.pcf: line 241, column 52
    function defaultSetter_388 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.NonOwnedBasis.NumVolunteers = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 223, column 55
    function valueRoot_377 () : java.lang.Object {
      return jurisdiction
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at BALineCoveragePanelSet.pcf: line 229, column 52
    function valueRoot_381 () : java.lang.Object {
      return jurisdiction.NonOwnedBasis
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at BALineCoveragePanelSet.pcf: line 223, column 55
    function value_376 () : typekey.Jurisdiction {
      return jurisdiction.State
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at BALineCoveragePanelSet.pcf: line 229, column 52
    function value_379 () : java.lang.Integer {
      return jurisdiction.NonOwnedBasis.NumEmployees
    }
    
    // 'value' attribute on TextCell (id=TotalPartners_Cell) at BALineCoveragePanelSet.pcf: line 235, column 52
    function value_383 () : java.lang.Integer {
      return jurisdiction.NonOwnedBasis.NumPartners
    }
    
    // 'value' attribute on TextCell (id=TotalVolunteers_Cell) at BALineCoveragePanelSet.pcf: line 241, column 52
    function value_387 () : java.lang.Integer {
      return jurisdiction.NonOwnedBasis.NumVolunteers
    }
    
    property get jurisdiction () : entity.BAJurisdiction {
      return getIteratedValue(2) as entity.BAJurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policy/BALineCoveragePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_10 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_112 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_12 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_14 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_16 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_18 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_20 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_22 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_24 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_26 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_28 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_30 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_32 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_34 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_36 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_62 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_64 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_66 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_68 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_70 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_72 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_74 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_76 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_8 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_80 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_82 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_11 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_13 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_15 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_17 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_19 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_21 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_9 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=BALiabGroupInputSet) at BALineCoveragePanelSet.pcf: line 48, column 41
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, baLine, openForEdit)
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  
}