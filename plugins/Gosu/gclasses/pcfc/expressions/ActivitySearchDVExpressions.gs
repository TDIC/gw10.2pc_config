package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ActivitySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivitySearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivitySearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at ActivitySearchDV.pcf: line 17, column 54
    function action_0 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at ActivitySearchDV.pcf: line 17, column 54
    function action_dest_1 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'def' attribute on InputSetRef at ActivitySearchDV.pcf: line 115, column 41
    function def_onEnter_67 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ActivitySearchDV.pcf: line 115, column 41
    function def_refreshVariables_68 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at ActivitySearchDV.pcf: line 29, column 47
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=subject_TDIC_Input) at ActivitySearchDV.pcf: line 34, column 46
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Subject_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityStatus_Input) at ActivitySearchDV.pcf: line 44, column 45
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SearchedActivityStatus = (__VALUE_TO_SET as typekey.ActivityStatus)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityPriority_Input) at ActivitySearchDV.pcf: line 50, column 39
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SearchedPriority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on BooleanDropdownInput (id=OverdueNow_Input) at ActivitySearchDV.pcf: line 56, column 44
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.OverdueNow = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on AltUserInput (id=AssignedUser_Input) at ActivitySearchDV.pcf: line 17, column 54
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SearchedAssignedUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on TypeKeyInput (id=DetainedReason_Input) at ActivitySearchDV.pcf: line 63, column 50
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DetainedReason = (__VALUE_TO_SET as typekey.DetainedReason_TDIC)
    }
    
    // 'value' attribute on DateInput (id=CreateFromDate_Input) at ActivitySearchDV.pcf: line 73, column 56
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CreatedFromDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=CreateToDate_Input) at ActivitySearchDV.pcf: line 78, column 54
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CreatedToDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=CompletedFromDate_Input) at ActivitySearchDV.pcf: line 89, column 58
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CompletedFromDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=CompletedToDate_Input) at ActivitySearchDV.pcf: line 94, column 56
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CompletedToDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=DueFromDate_Input) at ActivitySearchDV.pcf: line 105, column 52
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DueFromDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=DueToDate_Input) at ActivitySearchDV.pcf: line 110, column 50
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DueToDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ActivitySearchDV.pcf: line 23, column 46
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on AltUserInput (id=AssignedUser_Input) at ActivitySearchDV.pcf: line 17, column 54
    function valueRoot_4 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at ActivitySearchDV.pcf: line 29, column 47
    function value_10 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=subject_TDIC_Input) at ActivitySearchDV.pcf: line 34, column 46
    function value_14 () : java.lang.String {
      return searchCriteria.Subject_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityStatus_Input) at ActivitySearchDV.pcf: line 44, column 45
    function value_18 () : typekey.ActivityStatus {
      return searchCriteria.SearchedActivityStatus
    }
    
    // 'value' attribute on AltUserInput (id=AssignedUser_Input) at ActivitySearchDV.pcf: line 17, column 54
    function value_2 () : entity.User {
      return searchCriteria.SearchedAssignedUser
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityPriority_Input) at ActivitySearchDV.pcf: line 50, column 39
    function value_22 () : typekey.Priority {
      return searchCriteria.SearchedPriority
    }
    
    // 'value' attribute on BooleanDropdownInput (id=OverdueNow_Input) at ActivitySearchDV.pcf: line 56, column 44
    function value_26 () : java.lang.Boolean {
      return searchCriteria.OverdueNow
    }
    
    // 'value' attribute on TypeKeyInput (id=DetainedReason_Input) at ActivitySearchDV.pcf: line 63, column 50
    function value_30 () : typekey.DetainedReason_TDIC {
      return searchCriteria.DetainedReason
    }
    
    // 'value' attribute on DateInput (id=CreateFromDate_Input) at ActivitySearchDV.pcf: line 73, column 56
    function value_34 () : java.util.Date {
      return searchCriteria.CreatedFromDate_TDIC
    }
    
    // 'value' attribute on DateInput (id=CreateToDate_Input) at ActivitySearchDV.pcf: line 78, column 54
    function value_38 () : java.util.Date {
      return searchCriteria.CreatedToDate_TDIC
    }
    
    // 'value' attribute on DateInput (id=CompletedFromDate_Input) at ActivitySearchDV.pcf: line 89, column 58
    function value_45 () : java.util.Date {
      return searchCriteria.CompletedFromDate_TDIC
    }
    
    // 'value' attribute on DateInput (id=CompletedToDate_Input) at ActivitySearchDV.pcf: line 94, column 56
    function value_49 () : java.util.Date {
      return searchCriteria.CompletedToDate_TDIC
    }
    
    // 'value' attribute on DateInput (id=DueFromDate_Input) at ActivitySearchDV.pcf: line 105, column 52
    function value_56 () : java.util.Date {
      return searchCriteria.DueFromDate_TDIC
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ActivitySearchDV.pcf: line 23, column 46
    function value_6 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on DateInput (id=DueToDate_Input) at ActivitySearchDV.pcf: line 110, column 50
    function value_60 () : java.util.Date {
      return searchCriteria.DueToDate_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=createdDateInputGroup) at ActivitySearchDV.pcf: line 68, column 103
    function visible_42 () : java.lang.Boolean {
      return searchCriteria.DateAvailable_TDIC
    }
    
    property get searchCriteria () : gw.activity.ActivitySearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.activity.ActivitySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.activity.ActivitySearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}