package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ExclAlternateCoverages_TDICInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ExclAlternateCoverages_TDICInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ExclAlternateCoverages_TDICInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7ExclAlternateCoverages_TDICInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 51, column 28
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      policy.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 58, column 28
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      policy.PolicyEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextAreaCell (id=InsuredName_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 35, column 28
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      policy.InsuredName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      policy.InsurerCode = (__VALUE_TO_SET as typekey.InsurerCodes_TDIC)
    }
    
    // 'valueRange' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function valueRange_11 () : java.lang.Object {
      return typekey.InsurerCodes_TDIC.getTypeKeys(false)
    }
    
    // 'value' attribute on TextAreaCell (id=InsuredName_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 35, column 28
    function valueRoot_6 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 51, column 28
    function value_15 () : java.lang.String {
      return policy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 58, column 28
    function value_19 () : java.util.Date {
      return policy.PolicyEffectiveDate
    }
    
    // 'value' attribute on TextAreaCell (id=InsuredName_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 35, column 28
    function value_4 () : java.lang.String {
      return policy.InsuredName
    }
    
    // 'value' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function value_8 () : typekey.InsurerCodes_TDIC {
      return policy.InsurerCode
    }
    
    // 'valueRange' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.InsurerCodes_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function verifyValueRange_13 () : void {
      var __valueRangeArg = typekey.InsurerCodes_TDIC.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    property get policy () : entity.WC7ExclAlternateCov_TDIC {
      return getIteratedValue(1) as entity.WC7ExclAlternateCov_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ExclAlternateCoverages_TDICInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ExclAlternateCoverages_TDICInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaCell (id=InsuredName_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 35, column 28
    function sortValue_0 (policy :  entity.WC7ExclAlternateCov_TDIC) : java.lang.Object {
      return policy.InsuredName
    }
    
    // 'value' attribute on RangeCell (id=InsurerCode_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 44, column 54
    function sortValue_1 (policy :  entity.WC7ExclAlternateCov_TDIC) : java.lang.Object {
      return policy.InsurerCode
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 51, column 28
    function sortValue_2 (policy :  entity.WC7ExclAlternateCov_TDIC) : java.lang.Object {
      return policy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 58, column 28
    function sortValue_3 (policy :  entity.WC7ExclAlternateCov_TDIC) : java.lang.Object {
      return policy.PolicyEffectiveDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=Policies) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 27, column 57
    function toCreateAndAdd_23 () : entity.WC7ExclAlternateCov_TDIC {
      return exclusion.createAndAddWC7AlternateCoverage()
    }
    
    // 'toRemove' attribute on RowIterator (id=Policies) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 27, column 57
    function toRemove_24 (policy :  entity.WC7ExclAlternateCov_TDIC) : void {
      exclusion.removeFromWC7AlternateCoverages_TDIC(policy)
    }
    
    // 'value' attribute on RowIterator (id=Policies) at WC7ExclAlternateCoverages_TDICInputSet.pcf: line 27, column 57
    function value_25 () : entity.WC7ExclAlternateCov_TDIC[] {
      return exclusion.WC7AlternateCoverages_TDIC
    }
    
    property get exclusion () : WC7WorkersCompExcl {
      return getRequireValue("exclusion", 0) as WC7WorkersCompExcl
    }
    
    property set exclusion ($arg :  WC7WorkersCompExcl) {
      setRequireValue("exclusion", 0, $arg)
    }
    
    
  }
  
  
}