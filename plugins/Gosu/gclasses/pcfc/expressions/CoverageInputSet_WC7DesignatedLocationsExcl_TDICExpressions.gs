package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7DesignatedLocationsExcl_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function available_48 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 82, column 138
    function def_onEnter_23 (def :  pcf.WC7ExclAlternateCoverages_TDICInputSet) : void {
      def.onEnter(coverable.getCoverageConditionOrExclusion(exclusionPattern) as WC7WorkersCompExcl)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 82, column 138
    function def_refreshVariables_24 (def :  pcf.WC7ExclAlternateCoverages_TDICInputSet) : void {
      def.refreshVariables(coverable.getCoverageConditionOrExclusion(exclusionPattern) as WC7WorkersCompExcl)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 20, column 38
    function initialValue_0 () : gw.api.domain.StateSet {
      return gw.api.domain.StateSet.get( gw.api.domain.StateSet.US50 )
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 24, column 41
    function initialValue_1 () : entity.WC7WorkersCompLine {
      return coverable as entity.WC7WorkersCompLine
    }
    
    // 'label' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function label_49 () : java.lang.Object {
      return exclusionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function setter_50 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(wc7Line, exclusionPattern, VALUE)
    }
    
    // 'value' attribute on TextAreaCell (id=OperationName_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function sortValue_5 (operation :  entity.WC7DesLocExclOp_TDIC) : java.lang.Object {
      return operation.OperationName
    }
    
    // 'value' attribute on TextAreaCell (id=OperationTitle_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 69, column 30
    function sortValue_6 (operation :  entity.WC7DesLocExclOp_TDIC) : java.lang.Object {
      return operation.OperationTitle
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 76, column 30
    function sortValue_7 (operation :  entity.WC7DesLocExclOp_TDIC) : java.lang.Object {
      return operation.LocationAddress
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 54, column 55
    function toCreateAndAdd_20 () : entity.WC7DesLocExclOp_TDIC {
      return wc7Line.createAndAddWC7DesignatedLocationExclOperation_TDIC(wc7Line.WC7DesignatedLocationsExcl_TDIC)
    }
    
    // 'toRemove' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 54, column 55
    function toRemove_21 (operation :  entity.WC7DesLocExclOp_TDIC) : void {
      wc7Line.removeFromWC7DesignatedLocationExclOperations_TDIC(operation)
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 28, column 37
    function valueRoot_3 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 28, column 37
    function value_2 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 54, column 55
    function value_22 () : entity.WC7DesLocExclOp_TDIC[] {
      return wc7Line.WC7DesignatedLocationExclOperations_TDIC
    }
    
    // 'value' attribute on InputIterator (id=CovTermIterator) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 89, column 53
    function value_46 () : gw.api.domain.covterm.CovTerm[] {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern).CovTerms.sortBy( \ term -> term.Pattern.Priority )
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function visible_47 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 96, column 101
    function visible_53 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get usStates () : gw.api.domain.StateSet {
      return getVariableValue("usStates", 0) as gw.api.domain.StateSet
    }
    
    property set usStates ($arg :  gw.api.domain.StateSet) {
      setVariableValue("usStates", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_25 (def :  pcf.CovTermInputSet_Direct) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_27 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_29 (def :  pcf.CovTermInputSet_Option) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_31 (def :  pcf.CovTermInputSet_Package) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_33 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_35 (def :  pcf.CovTermInputSet_bit) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_37 (def :  pcf.CovTermInputSet_datetime) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_39 (def :  pcf.CovTermInputSet_decimal) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_41 (def :  pcf.CovTermInputSet_default) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_onEnter_43 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_26 (def :  pcf.CovTermInputSet_Direct) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_28 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_30 (def :  pcf.CovTermInputSet_Option) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_32 (def :  pcf.CovTermInputSet_Package) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_34 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_36 (def :  pcf.CovTermInputSet_bit) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_38 (def :  pcf.CovTermInputSet_datetime) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_40 (def :  pcf.CovTermInputSet_decimal) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_42 (def :  pcf.CovTermInputSet_default) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function def_refreshVariables_44 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'mode' attribute on InputSetRef at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 92, column 38
    function mode_45 () : java.lang.Object {
      return term.ValueTypeName
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getIteratedValue(1) as gw.api.domain.covterm.CovTerm
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaCell (id=OperationTitle_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 69, column 30
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.OperationTitle = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 76, column 30
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.LocationAddress = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaCell (id=OperationName_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.OperationName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaCell (id=OperationName_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function valueRoot_10 () : java.lang.Object {
      return operation
    }
    
    // 'value' attribute on TextAreaCell (id=OperationTitle_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 69, column 30
    function value_12 () : java.lang.String {
      return operation.OperationTitle
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 76, column 30
    function value_16 () : java.lang.String {
      return operation.LocationAddress
    }
    
    // 'value' attribute on TextAreaCell (id=OperationName_Cell) at CoverageInputSet.WC7DesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function value_8 () : java.lang.String {
      return operation.OperationName
    }
    
    property get operation () : entity.WC7DesLocExclOp_TDIC {
      return getIteratedValue(1) as entity.WC7DesLocExclOp_TDIC
    }
    
    
  }
  
  
}