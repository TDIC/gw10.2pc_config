package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/overview/ContactDetailsTileInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactDetailsTileInputSet_personExpressions {
  @javax.annotation.Generated("config/web/pcf/account/overview/ContactDetailsTileInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactDetailsTileInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=SSN_Input) at ContactDetailsTileInputSet.person.pcf: line 23, column 37
    function encryptionExpression_4 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(person.SSNOfficialID)
    }
    
    // 'initialValue' attribute on Variable at ContactDetailsTileInputSet.person.pcf: line 17, column 22
    function initialValue_0 () : Person {
      return (accountDetailsTileHelper.Account.AccountHolderContact as Person)
    }
    
    // 'inputMask' attribute on PrivacyInput (id=SSN_Input) at ContactDetailsTileInputSet.person.pcf: line 23, column 37
    function inputMask_3 () : java.lang.String {
      return '###-##-####'
    }
    
    // 'value' attribute on TextInput (id=licenseState_Input) at ContactDetailsTileInputSet.person.pcf: line 39, column 48
    function valueRoot_16 () : java.lang.Object {
      return person.LicenseState
    }
    
    // 'value' attribute on TextInput (id=licenseStatus_Input) at ContactDetailsTileInputSet.person.pcf: line 43, column 54
    function valueRoot_19 () : java.lang.Object {
      return person.LicenseStatus_TDIC
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ContactDetailsTileInputSet.person.pcf: line 23, column 37
    function valueRoot_2 () : java.lang.Object {
      return person
    }
    
    // 'value' attribute on TextInput (id=cdaMembershipStatus_Input) at ContactDetailsTileInputSet.person.pcf: line 47, column 60
    function valueRoot_22 () : java.lang.Object {
      return person.CDAMembershipStatus_TDIC
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ContactDetailsTileInputSet.person.pcf: line 23, column 37
    function value_1 () : java.lang.String {
      return person.SSNOfficialID
    }
    
    // 'value' attribute on TextInput (id=licenseNumber_Input) at ContactDetailsTileInputSet.person.pcf: line 35, column 42
    function value_12 () : java.lang.String {
      return person.LicenseNumber_TDIC
    }
    
    // 'value' attribute on TextInput (id=licenseState_Input) at ContactDetailsTileInputSet.person.pcf: line 39, column 48
    function value_15 () : java.lang.String {
      return person.LicenseState.DisplayName
    }
    
    // 'value' attribute on TextInput (id=licenseStatus_Input) at ContactDetailsTileInputSet.person.pcf: line 43, column 54
    function value_18 () : java.lang.String {
      return person.LicenseStatus_TDIC.DisplayName
    }
    
    // 'value' attribute on TextInput (id=cdaMembershipStatus_Input) at ContactDetailsTileInputSet.person.pcf: line 47, column 60
    function value_21 () : java.lang.String {
      return person.CDAMembershipStatus_TDIC.DisplayName
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at ContactDetailsTileInputSet.person.pcf: line 27, column 38
    function value_6 () : java.lang.String {
      return person.FEINOfficialID
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactDetailsTileInputSet.person.pcf: line 31, column 48
    function value_9 () : java.lang.String {
      return person.ADANumberOfficialID_TDIC
    }
    
    property get accountDetailsTileHelper () : gw.api.web.dashboard.ui.account.AccountDetailHelper {
      return getRequireValue("accountDetailsTileHelper", 0) as gw.api.web.dashboard.ui.account.AccountDetailHelper
    }
    
    property set accountDetailsTileHelper ($arg :  gw.api.web.dashboard.ui.account.AccountDetailHelper) {
      setRequireValue("accountDetailsTileHelper", 0, $arg)
    }
    
    property get fromPolicyFile () : Boolean {
      return getRequireValue("fromPolicyFile", 0) as Boolean
    }
    
    property set fromPolicyFile ($arg :  Boolean) {
      setRequireValue("fromPolicyFile", 0, $arg)
    }
    
    property get person () : Person {
      return getVariableValue("person", 0) as Person
    }
    
    property set person ($arg :  Person) {
      setVariableValue("person", 0, $arg)
    }
    
    
  }
  
  
}