package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/forms/GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GenericInferenceClassPanelSet_WC7GenericClassCodeSpecificFormExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/forms/GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GenericInferenceClassPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=moreClassCodes) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 53, column 93
    function action_7 () : void {
      WC7MoreFormClassCodesPopup.push(formPattern, availableWC7ClassCodes)
    }
    
    // 'action' attribute on MenuItem (id=moreClassCodes) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 53, column 93
    function action_dest_8 () : pcf.api.Destination {
      return pcf.WC7MoreFormClassCodesPopup.createDestination(formPattern, availableWC7ClassCodes)
    }
    
    // 'available' attribute on ToolbarButton (id=AddFormPatternClassCodeValue) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 34, column 63
    function available_9 () : java.lang.Boolean {
      return availableWC7ClassCodes.Count > 0
    }
    
    // 'def' attribute on PanelRef at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 26, column 54
    function def_onEnter_10 (def :  pcf.WC7FormClassCodeSelectionLV) : void {
      def.onEnter(formPattern)
    }
    
    // 'def' attribute on PanelRef at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 26, column 54
    function def_refreshVariables_11 (def :  pcf.WC7FormClassCodeSelectionLV) : void {
      def.refreshVariables(formPattern)
    }
    
    // 'initialValue' attribute on Variable at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 15, column 30
    function initialValue_0 () : WC7ClassCode[] {
      return gw.api.database.Query.make(WC7ClassCode).select().toTypedArray()
    }
    
    // 'initialValue' attribute on Variable at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 19, column 19
    function initialValue_1 () : int {
      return 10
    }
    
    // 'initialValue' attribute on Variable at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 24, column 30
    function initialValue_2 () : WC7ClassCode[] {
      return availableWC7ClassCodes()
    }
    
    // 'value' attribute on MenuItemIterator (id=formPatternClassCodeValues) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 43, column 49
    function value_5 () : entity.WC7ClassCode[] {
      return getLimitedNumberOfClassCodes()
    }
    
    // 'visible' attribute on PanelSet (id=GenericInferenceClassPanelSet) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 8, column 76
    function visible_12 () : java.lang.Boolean {
      return formPattern.PolicyLinePatternRef.Code == "WC7Line"
    }
    
    // 'visible' attribute on MenuItem (id=moreClassCodes) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 53, column 93
    function visible_6 () : java.lang.Boolean {
      return true//availableWC7FormPatternClassCodes().Count > maxClassCodes
    }
    
    property get availableWC7ClassCodes () : WC7ClassCode[] {
      return getVariableValue("availableWC7ClassCodes", 0) as WC7ClassCode[]
    }
    
    property set availableWC7ClassCodes ($arg :  WC7ClassCode[]) {
      setVariableValue("availableWC7ClassCodes", 0, $arg)
    }
    
    property get classCodes () : WC7ClassCode[] {
      return getVariableValue("classCodes", 0) as WC7ClassCode[]
    }
    
    property set classCodes ($arg :  WC7ClassCode[]) {
      setVariableValue("classCodes", 0, $arg)
    }
    
    property get formPattern () : FormPattern {
      return getRequireValue("formPattern", 0) as FormPattern
    }
    
    property set formPattern ($arg :  FormPattern) {
      setRequireValue("formPattern", 0, $arg)
    }
    
    property get maxClassCodes () : int {
      return getVariableValue("maxClassCodes", 0) as java.lang.Integer
    }
    
    property set maxClassCodes ($arg :  int) {
      setVariableValue("maxClassCodes", 0, $arg)
    }
    
    function availableWC7FormPatternClassCodes() : WC7FormPatternClassCode[] {
      var addedClassCodes =  formPattern.WC7FormPatternClassCodes.map(\ f -> f.Code).toSet()
      return formPattern.WC7FormPatternClassCodes.where(\ c -> !addedClassCodes.contains(c.Code)).sortBy(\ c -> c.Code)
    }
    
    function addToWC7FormPatternClassCode(classCode : WC7ClassCode) {
      var formPatternClassCode = new WC7FormPatternClassCode()
      formPatternClassCode.Code= classCode.Code
      formPatternClassCode.Classification = classCode.Classification
      formPatternClassCode.Jurisdiction = classCode.Jurisdiction
      formPattern.addToWC7FormPatternClassCodes(formPatternClassCode)
    }
    
    function availableWC7ClassCodes() : WC7ClassCode[] {
      if (formPattern.WC7FormPatternClassCodes.IsEmpty) {
        return classCodes
      } else {
        var addedFormPatternClassCodes = formPattern.WC7FormPatternClassCodes
        return classCodes.where(\ cc -> !(addedFormPatternClassCodes.hasMatch(\ fpcc -> fpcc.Code == cc.Code and fpcc.Classification == cc.Classification and fpcc.Jurisdiction == cc.Jurisdiction )))
      }
    }
    
    function getLimitedNumberOfClassCodes() : WC7ClassCode[] {
      var sortedClassCodes = availableWC7ClassCodes.toList().sortBy(\cc -> cc.Code)
      if (sortedClassCodes.Count > maxClassCodes) {
        sortedClassCodes = sortedClassCodes.subList(0, maxClassCodes)
      }
      return sortedClassCodes.toTypedArray()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/forms/GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GenericInferenceClassPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=classCodeValueItem) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 47, column 52
    function action_3 () : void {
      addToWC7FormPatternClassCode(formPatternClassCodeValue)
    }
    
    // 'label' attribute on MenuItem (id=classCodeValueItem) at GenericInferenceClassPanelSet.WC7GenericClassCodeSpecificForm.pcf: line 47, column 52
    function label_4 () : java.lang.Object {
      return formPatternClassCodeValue
    }
    
    property get formPatternClassCodeValue () : entity.WC7ClassCode {
      return getIteratedValue(1) as entity.WC7ClassCode
    }
    
    
  }
  
  
}