package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7FederalEmployersLiabilityActACovExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function available_236 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'confirmMessage' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function confirmMessage_237 () : java.lang.String {
      return DisplayKey.get("Web.Policy.WC7.Confirm.RemovingFELAMaritimeCovRemovesExposures", coveragePattern)
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabLimitType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm.OptionValue = (__VALUE_TO_SET as gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabAggLimitType>)
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.Value = (__VALUE_TO_SET as typekey.WC7CovProgramType)
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm.Value = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'editable' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function editable_19 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm) and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function editable_31 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm) and wc7LineAtPeriodStart.WC7FedCoveredEmployeeVLs.Empty and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function editable_43 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm) and wc7LineAtPeriodStart.WC7FedCoveredEmployeeVLs.Empty and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm)
    }
    
    // 'editable' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function editable_7 () : java.lang.Boolean {
      return gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm) and wc7LineAtPeriodStart.isLineCoverageTermEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 20, column 41
    function initialValue_0 () : entity.WC7WorkersCompLine {
      return (coverable as WC7WorkersCompLine)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 24, column 23
    function initialValue_1 () : boolean {
      return wc7Line.Branch.Job.NewTerm
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 28, column 41
    function initialValue_2 () : entity.WC7WorkersCompLine {
      return wc7Line.getVersionList().AllVersions.first().getSlice(wc7Line.Branch.PeriodStart)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 32, column 50
    function initialValue_3 () : gw.pcf.WC7CoverageInputSetUIHelper {
      return new gw.pcf.WC7CoverageInputSetUIHelper()
    }
    
    // 'label' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function label_20 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm.DisplayName
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function label_238 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function label_32 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function label_45 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm.DisplayName
    }
    
    // 'label' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function label_8 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm.DisplayName
    }
    
    // 'required' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function required_21 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function required_33 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function required_46 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm.Pattern.Required
    }
    
    // 'required' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function required_9 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm.Pattern.Required
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function setter_240 (VALUE :  java.lang.Boolean) : void {
      wc7Line.setFELACovExists(VALUE)
    }
    
    // 'showConfirmMessage' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function showConfirmMessage_239 () : java.lang.Boolean {
      return wc7Line.WC7FederalEmployersLiabilityActACovExists and wc7Line.WC7FedCoveredEmployeesWM.HasElements
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function valueRange_13 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function valueRange_25 () : java.lang.Object {
      return gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm))
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function valueRange_37 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function valueRange_50 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm?.Pattern.OrderedAvailableValues
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function valueRoot_12 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function valueRoot_24 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function valueRoot_36 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function valueRoot_49 () : java.lang.Object {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 36, column 37
    function valueRoot_5 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function value_10 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabLimitType> {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function value_22 () : gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabAggLimitType> {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm.OptionValue
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function value_34 () : typekey.WC7CovProgramType {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.Value
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 36, column 37
    function value_4 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function value_47 () : typekey.WC7GoverningLaw {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm.Value
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function verifyValueRangeIsAllowedType_26 ($$arg :  gw.api.productmodel.CovTermOpt<productmodel.OptionWC7FedEmpLiabAggLimitType>[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_38 ($$arg :  typekey.WC7CovProgramType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function verifyValueRangeIsAllowedType_51 ($$arg :  typekey.WC7GoverningLaw[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 52, column 102
    function verifyValueRange_15 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabAggLimitTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 60, column 105
    function verifyValueRange_27 () : void {
      var __valueRangeArg = gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabAggLimitTerm))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabProgramTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 68, column 47
    function verifyValueRange_39 () : void {
      var __valueRangeArg = wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function verifyValueRange_52 () : void {
      var __valueRangeArg = wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm?.Pattern.OrderedAvailableValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 44, column 127
    function visible_235 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 389, column 100
    function visible_243 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on RangeInput (id=WC7FedEmpLiabLawTermInput_Input) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 79, column 239
    function visible_44 () : java.lang.Boolean {
      return wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.HasWC7FedEmpLiabProgramTerm and wc7LineAtPeriodStart.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.Value == typekey.WC7CovProgramType.TC_PROGRAMII
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coverageInputSetHelper () : gw.pcf.WC7CoverageInputSetUIHelper {
      return getVariableValue("coverageInputSetHelper", 0) as gw.pcf.WC7CoverageInputSetUIHelper
    }
    
    property set coverageInputSetHelper ($arg :  gw.pcf.WC7CoverageInputSetUIHelper) {
      setVariableValue("coverageInputSetHelper", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get isNewTerm () : boolean {
      return getVariableValue("isNewTerm", 0) as java.lang.Boolean
    }
    
    property set isNewTerm ($arg :  boolean) {
      setVariableValue("isNewTerm", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    property get wc7LineAtPeriodStart () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7LineAtPeriodStart", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7LineAtPeriodStart ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7LineAtPeriodStart", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 292, column 51
    function action_168 () : void {
      WC7ClassCodeSearchPopup.push(wc7FedCovEmp2.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 292, column 51
    function action_dest_169 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wc7FedCovEmp2.LocationWM, wc7Line, addlVersionPreviousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 326, column 83
    function available_192 () : java.lang.Boolean {
      return !wc7FedCovEmp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function defaultSetter_177 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp2.ClassCodeShortDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 310, column 50
    function defaultSetter_185 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp2.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 314, column 74
    function defaultSetter_189 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp2.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 326, column 83
    function defaultSetter_195 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp2.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function editable_175 () : java.lang.Boolean {
      return addlVersionClassCodeShortDescs.length > 1
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 263, column 45
    function initialValue_156 () : entity.WC7ClassCode {
      return isNewTerm ? null : wc7FedCovEmp2.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 268, column 44
    function initialValue_157 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wc7FedCovEmp2, wc7FedCovEmp2.LocationWM, addlVersionPreviousWC7ClassCode)
    }
    
    // RowIterator (id=WC7FedCovEmployee2) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 259, column 58
    function initializeVariables_229 () : void {
        addlVersionPreviousWC7ClassCode = isNewTerm ? null : wc7FedCovEmp2.BasedOn.ClassCode;
  addlVersionClassCodeShortDescs = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptions(wc7FedCovEmp2, wc7FedCovEmp2.LocationWM, addlVersionPreviousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 292, column 51
    function inputConversion_170 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeFedEmp(VALUE, wc7FedCovEmp2, wc7Line.WC7FederalEmployersLiabilityActACov, addlVersionPreviousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 294, column 102
    function onChange_167 () : void {
      if (wc7FedCovEmp2.ClassCode == null) classCodeShortDescriptions = {}
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 292, column 51
    function outputConversion_171 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : (VALUE).Code
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function valueRange_160 () : java.lang.Object {
      return wc7Line.Branch.PolicyLocationsWM
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function valueRange_179 () : java.lang.Object {
      return addlVersionClassCodeShortDescs
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function valueRoot_159 () : java.lang.Object {
      return wc7FedCovEmp2
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 283, column 46
    function valueRoot_165 () : java.lang.Object {
      return wc7FedCovEmp.LocationWM
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function value_158 () : entity.PolicyLocation {
      return wc7FedCovEmp2.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 283, column 46
    function value_164 () : typekey.State {
      return wc7FedCovEmp.LocationWM.State
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 292, column 51
    function value_172 () : entity.WC7ClassCode {
      return wc7FedCovEmp2.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function value_176 () : java.lang.String {
      return wc7FedCovEmp2.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 310, column 50
    function value_184 () : java.lang.Integer {
      return wc7FedCovEmp2.NumEmployees
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 314, column 74
    function value_188 () : java.lang.Boolean {
      return wc7FedCovEmp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 326, column 83
    function value_194 () : java.lang.Integer {
      return wc7FedCovEmp2.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 335, column 83
    function value_201 () : java.lang.String {
      return wc7FedCovEmp2.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 343, column 90
    function value_206 () : java.math.BigDecimal {
      return wc7FedCovEmp2.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 352, column 37
    function value_211 () : java.lang.Boolean {
      return wc7FedCovEmp2.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 357, column 36
    function value_215 () : java.lang.Boolean {
      return wc7FedCovEmp2.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 369, column 37
    function value_219 () : java.math.BigDecimal {
      return wc7FedCovEmp2.SupplementalLoadingRate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 375, column 35
    function value_223 () : java.util.Date {
      return wc7FedCovEmp2.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 381, column 35
    function value_226 () : java.util.Date {
      return wc7FedCovEmp2.ExpirationDate
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function verifyValueRangeIsAllowedType_161 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function verifyValueRangeIsAllowedType_161 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function verifyValueRangeIsAllowedType_161 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function verifyValueRangeIsAllowedType_180 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function verifyValueRangeIsAllowedType_180 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 278, column 32
    function verifyValueRange_162 () : void {
      var __valueRangeArg = wc7Line.Branch.PolicyLocationsWM
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_161(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 304, column 32
    function verifyValueRange_181 () : void {
      var __valueRangeArg = addlVersionClassCodeShortDescs
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_180(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 343, column 90
    function visible_205 () : java.lang.Boolean {
      return wc7FedCovEmp.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 343, column 90
    function visible_208 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 352, column 37
    function visible_210 () : java.lang.Boolean {
      return wc7FedCovEmp2.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 352, column 37
    function visible_213 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 369, column 37
    function visible_218 () : java.lang.Boolean {
      return wc7FedCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'visible' attribute on TextCell (id=supplementalDiseaseLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 369, column 37
    function visible_221 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get addlVersionClassCodeShortDescs () : java.lang.String[] {
      return getVariableValue("addlVersionClassCodeShortDescs", 3) as java.lang.String[]
    }
    
    property set addlVersionClassCodeShortDescs ($arg :  java.lang.String[]) {
      setVariableValue("addlVersionClassCodeShortDescs", 3, $arg)
    }
    
    property get addlVersionPreviousWC7ClassCode () : entity.WC7ClassCode {
      return getVariableValue("addlVersionPreviousWC7ClassCode", 3) as entity.WC7ClassCode
    }
    
    property set addlVersionPreviousWC7ClassCode ($arg :  entity.WC7ClassCode) {
      setVariableValue("addlVersionPreviousWC7ClassCode", 3, $arg)
    }
    
    property get wc7FedCovEmp2 () : entity.WC7FedCoveredEmployee {
      return getIteratedValue(3) as entity.WC7FedCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7FedCovEmpLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function action_86 () : void {
      WC7ClassCodeSearchPopup.push(wc7FedCovEmp.LocationWM, wc7Line, previousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function action_dest_87 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wc7FedCovEmp.LocationWM, wc7Line, previousWC7ClassCode, WC7ClassCodeType.TC_FELA, classCodeProgramType)
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 186, column 80
    function available_114 () : java.lang.Boolean {
      return !wc7FedCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 168, column 48
    function defaultSetter_107 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.NumEmployees = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 173, column 71
    function defaultSetter_111 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 186, column 80
    function defaultSetter_117 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function defaultSetter_129 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.Rate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 216, column 35
    function defaultSetter_135 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.SpecificDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 223, column 34
    function defaultSetter_141 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.SupplementalDiseaseLoaded = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 237, column 35
    function defaultSetter_146 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.SupplementalLoadingRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.LocationWM = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function defaultSetter_99 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7FedCovEmp.ClassCodeShortDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function editable_73 () : java.lang.Boolean {
      return wc7FedCovEmp.CanEditLocation
    }
    
    // 'editable' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function editable_88 () : java.lang.Boolean {
      return wc7FedCovEmp.LocationWM.State != null
    }
    
    // 'editable' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function editable_97 () : java.lang.Boolean {
      return classCodeShortDescriptions.length > 1
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 111, column 36
    function initialValue_70 () : WC7ClassCode {
      return isNewTerm ? null : wc7FedCovEmp.BasedOn.ClassCode
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 116, column 42
    function initialValue_71 () : java.lang.String[] {
      return gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptionsForLineCoverage(wc7FedCovEmp, wc7FedCovEmp.LocationWM, previousWC7ClassCode)
    }
    
    // RowIterator (id=WC7FedCovEmployee) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 107, column 56
    function initializeVariables_231 () : void {
        previousWC7ClassCode = isNewTerm ? null : wc7FedCovEmp.BasedOn.ClassCode;
  classCodeShortDescriptions = gw.pcf.WC7ClassesInputSetUIHelper.getClassCodeShortDescriptionsForLineCoverage(wc7FedCovEmp, wc7FedCovEmp.LocationWM, previousWC7ClassCode);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function inputConversion_90 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeFedEmp(VALUE, wc7FedCovEmp, wc7Line.WC7FederalEmployersLiabilityActACov, previousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 225, column 82
    function onChange_139 () : void {
      wc7FedCovEmp.SupplementalDiseaseLoadingRate = null
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 129, column 233
    function onChange_72 () : void {
      wc7FedCovEmp.ClassCode = gw.pcf.WC7ClassesInputSetUIHelper.findFirstMatchingClassCodeFedEmp(wc7FedCovEmp.ClassCode.Code, wc7FedCovEmp, wc7Line.WC7FederalEmployersLiabilityActACov, previousWC7ClassCode)
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 150, column 164
    function onChange_85 () : void {
      coverageInputSetHelper.clearupWC7FedEmployeeRates(wc7FedCovEmp); if (wc7FedCovEmp.ClassCode == null) classCodeShortDescriptions = {}
    }
    
    // 'onPick' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function onPick_89 (PickedValue :  WC7ClassCode) : void {
      coverageInputSetHelper.clearupWC7FedEmployeeRates(wc7FedCovEmp)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function outputConversion_91 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : (VALUE).Code
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function valueRange_101 () : java.lang.Object {
      return classCodeShortDescriptions
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function valueRange_77 () : java.lang.Object {
      return wc7Line.Branch.PolicyLocationsWM
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function valueRoot_76 () : java.lang.Object {
      return wc7FedCovEmp
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 136, column 44
    function valueRoot_83 () : java.lang.Object {
      return wc7FedCovEmp.LocationWM
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 168, column 48
    function value_106 () : java.lang.Integer {
      return wc7FedCovEmp.NumEmployees
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 173, column 71
    function value_110 () : java.lang.Boolean {
      return wc7FedCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 186, column 80
    function value_116 () : java.lang.Integer {
      return wc7FedCovEmp.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 196, column 80
    function value_123 () : java.lang.String {
      return wc7FedCovEmp.ClassCodeBasisDescription
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function value_128 () : java.math.BigDecimal {
      return wc7FedCovEmp.Rate
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 216, column 35
    function value_134 () : java.lang.Boolean {
      return wc7FedCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 223, column 34
    function value_140 () : java.lang.Boolean {
      return wc7FedCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 237, column 35
    function value_145 () : java.math.BigDecimal {
      return wc7FedCovEmp.SupplementalLoadingRate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 244, column 33
    function value_150 () : java.util.Date {
      return wc7FedCovEmp.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 251, column 33
    function value_153 () : java.util.Date {
      return wc7FedCovEmp.ExpirationDate
    }
    
    // 'value' attribute on RowIterator (id=WC7FedCovEmployee2) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 259, column 58
    function value_230 () : entity.WC7FedCoveredEmployee[] {
      return wc7FedCovEmp.AdditionalVersions.whereTypeIs(WC7FedCoveredEmployee)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function value_74 () : entity.PolicyLocation {
      return wc7FedCovEmp.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 136, column 44
    function value_82 () : typekey.State {
      return wc7FedCovEmp.LocationWM.State
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function value_92 () : entity.WC7ClassCode {
      return wc7FedCovEmp.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function value_98 () : java.lang.String {
      return wc7FedCovEmp.ClassCodeShortDescription
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function verifyValueRangeIsAllowedType_102 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function verifyValueRangeIsAllowedType_102 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function verifyValueRangeIsAllowedType_78 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function verifyValueRangeIsAllowedType_78 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function verifyValueRangeIsAllowedType_78 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function verifyValueRange_103 () : void {
      var __valueRangeArg = classCodeShortDescriptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_102(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function verifyValueRange_79 () : void {
      var __valueRangeArg = wc7Line.Branch.PolicyLocationsWM
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_78(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function visible_127 () : java.lang.Boolean {
      return wc7FedCovEmp.ClassCode.ARatedType
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function visible_131 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 216, column 35
    function visible_133 () : java.lang.Boolean {
      return wc7FedCovEmp.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 216, column 35
    function visible_137 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 237, column 35
    function visible_148 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get classCodeShortDescriptions () : java.lang.String[] {
      return getVariableValue("classCodeShortDescriptions", 2) as java.lang.String[]
    }
    
    property set classCodeShortDescriptions ($arg :  java.lang.String[]) {
      setVariableValue("classCodeShortDescriptions", 2, $arg)
    }
    
    property get previousWC7ClassCode () : WC7ClassCode {
      return getVariableValue("previousWC7ClassCode", 2) as WC7ClassCode
    }
    
    property set previousWC7ClassCode ($arg :  WC7ClassCode) {
      setVariableValue("previousWC7ClassCode", 2, $arg)
    }
    
    property get wc7FedCovEmp () : WC7FedCoveredEmployee {
      return getIteratedValue(2) as WC7FedCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7FedCovEmpLVExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 92, column 53
    function initialValue_57 () : typekey.WC7ClassCodeProgramType {
      return gw.pcf.WC7ClassesInputSetUIHelper.deriveClassCodeProgramType(wc7Line.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabProgramTerm.Value, wc7Line.WC7FederalEmployersLiabilityActACov.WC7FedEmpLiabLawTerm.Value)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 127, column 51
    function sortValue_58 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.LocationWM
    }
    
    // 'value' attribute on TextCell (id=Jurisdiction_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 136, column 44
    function sortValue_59 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.LocationWM.State
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 148, column 49
    function sortValue_60 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 161, column 30
    function sortValue_61 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.ClassCodeShortDescription
    }
    
    // 'value' attribute on TextCell (id=NumEmployees_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 168, column 48
    function sortValue_62 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.NumEmployees
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 173, column 71
    function sortValue_63 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function sortValue_64 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.Rate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 244, column 33
    function sortValue_68 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 251, column 33
    function sortValue_69 (wc7FedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wc7FedCovEmp.ExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=WC7FedCovEmployee) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 107, column 56
    function toCreateAndAdd_232 () : WC7FedCoveredEmployee {
      return wc7Line.createAndAddFedCoveredEmployeeWM()
    }
    
    // 'toRemove' attribute on RowIterator (id=WC7FedCovEmployee) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 107, column 56
    function toRemove_233 (wc7FedCovEmp :  WC7FedCoveredEmployee) : void {
      wc7FedCovEmp.removeWM()
    }
    
    // 'value' attribute on RowIterator (id=WC7FedCovEmployee) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 107, column 56
    function value_234 () : entity.WC7FedCoveredEmployee[] {
      return wc7Line.WC7FedCoveredEmployeesWM
    }
    
    // 'visible' attribute on TextCell (id=Rate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 205, column 88
    function visible_65 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpRateColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 216, column 35
    function visible_66 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSpecificDiseaseColumnVisible(wc7Line)
    }
    
    // 'visible' attribute on TextCell (id=SupplementalLoadingRate_Cell) at CoverageInputSet.WC7FederalEmployersLiabilityActACov.pcf: line 237, column 35
    function visible_67 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSupplementalLoadingRateColumnVisible(wc7Line)
    }
    
    property get classCodeProgramType () : typekey.WC7ClassCodeProgramType {
      return getVariableValue("classCodeProgramType", 1) as typekey.WC7ClassCodeProgramType
    }
    
    property set classCodeProgramType ($arg :  typekey.WC7ClassCodeProgramType) {
      setVariableValue("classCodeProgramType", 1, $arg)
    }
    
    
  }
  
  
}