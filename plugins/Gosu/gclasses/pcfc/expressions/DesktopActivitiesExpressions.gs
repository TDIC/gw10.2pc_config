package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopActivitiesExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopActivitiesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on AlertBar (id=DesktopActivities_CanceledActivitiesAlertBar) at DesktopActivities.pcf: line 25, column 92
    function action_1 () : void {
      gw.api.web.activity.CanceledActivitiesAlertUtil.goToCanceledActivities()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopActivities.pcf: line 36, column 27
    function allCheckedRowsAction_2 (CheckedValues :  Activity[], CheckedValuesErrors :  java.util.Map) : void {
      AssignActivitiesPopup.push(new gw.api.web.activity.ActivityAssignmentPopup(CheckedValues, gw.assignment.AssignmentUtil.DefaultUser), CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_SkipButton) at DesktopActivities.pcf: line 43, column 27
    function allCheckedRowsAction_3 (CheckedValues :  Activity[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.activity.ActivityUtil.skipActivities(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_CompleteButton) at DesktopActivities.pcf: line 50, column 27
    function allCheckedRowsAction_4 (CheckedValues :  Activity[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.activity.ActivityUtil.completeActivities(CheckedValues)
    }
    
    // 'canVisit' attribute on Page (id=DesktopActivities) at DesktopActivities.pcf: line 10, column 63
    static function canVisit_8 () : java.lang.Boolean {
      return perm.System.viewmyactivities
    }
    
    // 'def' attribute on PanelRef at DesktopActivities.pcf: line 28, column 69
    function def_onEnter_5 (def :  pcf.DesktopActivitiesLV) : void {
      def.onEnter(activities)
    }
    
    // 'def' attribute on PanelRef at DesktopActivities.pcf: line 28, column 69
    function def_refreshVariables_6 (def :  pcf.DesktopActivitiesLV) : void {
      def.refreshVariables(activities)
    }
    
    // 'infoBar' attribute on Page (id=DesktopActivities) at DesktopActivities.pcf: line 10, column 63
    function infoBar_onEnter_9 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on Page (id=DesktopActivities) at DesktopActivities.pcf: line 10, column 63
    function infoBar_refreshVariables_10 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 17, column 71
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return Activity.finder.getAssignedToCurrentUser()
    }
    
    // 'mode' attribute on PanelRef at DesktopActivities.pcf: line 28, column 69
    function mode_7 () : java.lang.Object {
      return gw.api.web.desktop.data.FederatedDataUIHelper.mode()
    }
    
    // Page (id=DesktopActivities) at DesktopActivities.pcf: line 10, column 63
    static function parent_11 () : pcf.api.Destination {
      return pcf.Desktop.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DesktopActivities {
      return super.CurrentLocation as pcf.DesktopActivities
    }
    
    property get activities () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return getVariableValue("activities", 0) as gw.api.database.IQueryBeanResult<entity.Activity>
    }
    
    property set activities ($arg :  gw.api.database.IQueryBeanResult<entity.Activity>) {
      setVariableValue("activities", 0, $arg)
    }
    
    
  }
  
  
}