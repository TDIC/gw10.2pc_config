package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7LaborContractorExclEndorsementExclExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 42, column 109
    function available_43 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 73, column 75
    function conversionExpression_11 (PickedValue :  Contact) : entity.WC7LaborContactDetail {
      return addNewLaborContractorDetailForContact(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 20, column 34
    function initialValue_0 () : WC7WorkersCompLine {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 24, column 36
    function initialValue_1 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 28, column 54
    function initialValue_2 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 32, column 54
    function initialValue_3 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(theWC7Line.WC7Jurisdictions.first().Jurisdiction)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 78, column 131
    function label_17 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.Existing", WC7PolicyLaborContractor.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 42, column 109
    function label_44 () : java.lang.Object {
      return stateConfig.getClauseName(exclusionPattern, wc7Line)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 73, column 75
    function pickLocation_12 () : void {
      ContactSearchPopup.push(TC_LABORCONTRACTOR)
    }
    
    // 'onToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 42, column 109
    function setter_45 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, exclusionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 85, column 34
    function sortBy_13 (acctContact :  entity.AccountContact) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 61, column 32
    function sortBy_7 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 135, column 53
    function sortValue_22 (policyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.WC7LaborContact
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function sortValue_23 (policyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 150, column 45
    function sortValue_24 (policyLaborContractorDetail :  entity.WC7LaborContactDetail) : java.lang.Object {
      return policyLaborContractorDetail.Address
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 127, column 56
    function toRemove_40 (policyLaborContractorDetail :  entity.WC7LaborContactDetail) : void {
      policyLaborContractorDetail.WC7LaborContact.removeDetail(policyLaborContractorDetail)
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 36, column 37
    function valueRoot_5 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 58, column 49
    function value_10 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLABORCONTRACTOR)
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 82, column 53
    function value_16 () : entity.AccountContact[] {
      return theWC7Line.WC7PolicyLaborContractorDetailExistingCandidates
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 101, column 53
    function value_21 () : entity.AccountContact[] {
      return theWC7Line.WC7PolicyLaborContractorDetailOtherCandidates
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 36, column 37
    function value_4 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 127, column 56
    function value_41 () : entity.WC7LaborContactDetail[] {
      return theWC7Line.getExcludedLaborContractorDetails(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ClausePattern>("WC7LaborContractorExclEndorsementExcl"))
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 42, column 109
    function visible_42 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 157, column 101
    function visible_48 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 0, $arg)
    }
    
    property get theWC7Line () : WC7WorkersCompLine {
      return getVariableValue("theWC7Line", 0) as WC7WorkersCompLine
    }
    
    property set theWC7Line ($arg :  WC7WorkersCompLine) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    property get wc7Line () : productmodel.WC7Line {
      return getVariableValue("wc7Line", 0) as productmodel.WC7Line
    }
    
    property set wc7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    function addNewLaborContractorDetailForContact(aContact : entity.Contact) : WC7LaborContactDetail {
      var newLaborContractor = theWC7Line.addExcludedLaborContractorDetailForContact(aContact, theWC7Line.WC7LaborContractorExclEndorsementExcl)
      return newLaborContractor
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 90, column 96
    function label_14 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 90, column 96
    function toCreateAndAdd_15 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborContractorDetailForContact(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 109, column 96
    function label_19 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 109, column 96
    function toCreateAndAdd_20 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborContractorDetailForContact(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 135, column 53
    function action_25 () : void {
      EditPolicyContactRolePopup.push(policyLaborContractorDetail.WC7LaborContact, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 135, column 53
    function action_dest_26 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyLaborContractorDetail.WC7LaborContact, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborContractorDetail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function valueRange_33 () : java.lang.Object {
      return theWC7Line.stateFilterFor(exclusionPattern).TypeKeys
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 135, column 53
    function valueRoot_28 () : java.lang.Object {
      return policyLaborContractorDetail
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 135, column 53
    function value_27 () : entity.WC7LaborContact {
      return policyLaborContractorDetail.WC7LaborContact
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function value_30 () : typekey.Jurisdiction {
      return policyLaborContractorDetail.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 150, column 45
    function value_37 () : entity.Address {
      return policyLaborContractorDetail.Address
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function verifyValueRangeIsAllowedType_34 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function verifyValueRangeIsAllowedType_34 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 144, column 51
    function verifyValueRange_35 () : void {
      var __valueRangeArg = theWC7Line.stateFilterFor(exclusionPattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_34(__valueRangeArg)
    }
    
    property get policyLaborContractorDetail () : entity.WC7LaborContactDetail {
      return getIteratedValue(1) as entity.WC7LaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 66, column 137
    function label_8 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7LaborContractorExclEndorsementExcl.pcf: line 66, column 137
    function pickLocation_9 () : void {
      WC7NewLaborContractorForContactTypePopup.push(theWC7Line.Branch.WC7Line, contactType, exclusionPattern)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}