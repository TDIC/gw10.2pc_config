package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MortgageeDetails_TDICDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends MortgageeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ExistingMortgagees) at MortgageeDetails_TDICDV.pcf: line 76, column 107
    function label_10 () : java.lang.Object {
      return mortgagee
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=ExistingMortgagees) at MortgageeDetails_TDICDV.pcf: line 76, column 107
    function toCreateAndAdd_11 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addPolicyMortgagee_TDIC(mortgagee.AccountContact.Contact)
    }
    
    property get mortgagee () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends MortgageeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=acctContact) at MortgageeDetails_TDICDV.pcf: line 95, column 109
    function label_15 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=acctContact) at MortgageeDetails_TDICDV.pcf: line 95, column 109
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addPolicyMortgagee_TDIC(acctContact.AccountContact.Contact)
    }
    
    property get acctContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends MortgageeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at MortgageeDetails_TDICDV.pcf: line 142, column 58
    function action_32 () : void {
      EditPolicyContactRolePopup.push(mortgageeDetail, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at MortgageeDetails_TDICDV.pcf: line 142, column 58
    function action_dest_33 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(mortgageeDetail, openForEdit)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at MortgageeDetails_TDICDV.pcf: line 122, column 55
    function checkBoxVisible_40 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at MortgageeDetails_TDICDV.pcf: line 147, column 59
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      mortgageeDetail.LoanNumberString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at MortgageeDetails_TDICDV.pcf: line 135, column 62
    function valueRoot_30 () : java.lang.Object {
      return mortgageeDetail
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at MortgageeDetails_TDICDV.pcf: line 129, column 91
    function value_27 () : java.util.Date {
      return getEffectiveDate(mortgageeDetail)//mortgageeDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at MortgageeDetails_TDICDV.pcf: line 135, column 62
    function value_29 () : java.util.Date {
      return mortgageeDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at MortgageeDetails_TDICDV.pcf: line 142, column 58
    function value_34 () : entity.PolicyMortgagee_TDIC {
      return mortgageeDetail
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at MortgageeDetails_TDICDV.pcf: line 147, column 59
    function value_36 () : java.lang.String {
      return mortgageeDetail.LoanNumberString
    }
    
    property get mortgageeDetail () : entity.PolicyMortgagee_TDIC {
      return getIteratedValue(1) as entity.PolicyMortgagee_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends MortgageeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at MortgageeDetails_TDICDV.pcf: line 53, column 87
    function label_5 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at MortgageeDetails_TDICDV.pcf: line 53, column 87
    function pickLocation_6 () : void {
      NewMortgagee_TDICPopup.push(bopBuilding, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/MortgageeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MortgageeDetails_TDICDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at MortgageeDetails_TDICDV.pcf: line 110, column 61
    function allCheckedRowsAction_22 (CheckedValues :  entity.PolicyMortgagee_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at MortgageeDetails_TDICDV.pcf: line 110, column 61
    function available_20 () : java.lang.Boolean {
      return policyPeriod.Job typeis PolicyChange
    }
    
    // 'available' attribute on DetailViewPanel (id=MortgageeDetails_TDICDV) at MortgageeDetails_TDICDV.pcf: line 9, column 83
    function available_43 () : java.lang.Boolean {
      return policyPeriod?.profileChange_TDIC
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at MortgageeDetails_TDICDV.pcf: line 61, column 32
    function conversionExpression_8 (PickedValue :  Contact) : entity.PolicyMortgagee_TDIC {
      return bopBuilding.addPolicyMortgagee_TDIC(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at MortgageeDetails_TDICDV.pcf: line 20, column 35
    function initialValue_0 () : entity.PolicyPeriod {
      return bopBuilding.PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at MortgageeDetails_TDICDV.pcf: line 24, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at MortgageeDetails_TDICDV.pcf: line 29, column 36
    function initialValue_2 () : AccountContactView[] {
      return null
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at MortgageeDetails_TDICDV.pcf: line 66, column 30
    function label_13 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", DisplayKey.get("TDIC.entity.Mortgagee"))
    }
    
    // 'label' attribute on Label at MortgageeDetails_TDICDV.pcf: line 32, column 95
    function label_3 () : java.lang.String {
      return DisplayKey.get("Web.Policy.Mortgagee_TDIC", bopBuilding.TypeLabel)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at MortgageeDetails_TDICDV.pcf: line 61, column 32
    function pickLocation_9 () : void {
      ContactSearchPopup.push(typekey.AccountContactRole.TC_MORTGAGEE_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at MortgageeDetails_TDICDV.pcf: line 90, column 34
    function sortBy_14 (acctContact :  entity.AccountContactView) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at MortgageeDetails_TDICDV.pcf: line 48, column 32
    function sortBy_4 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at MortgageeDetails_TDICDV.pcf: line 129, column 91
    function sortValue_23 (mortgageeDetail :  entity.PolicyMortgagee_TDIC) : java.lang.Object {
      return getEffectiveDate(mortgageeDetail)//mortgageeDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at MortgageeDetails_TDICDV.pcf: line 135, column 62
    function sortValue_24 (mortgageeDetail :  entity.PolicyMortgagee_TDIC) : java.lang.Object {
      return mortgageeDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at MortgageeDetails_TDICDV.pcf: line 142, column 58
    function sortValue_25 (mortgageeDetail :  entity.PolicyMortgagee_TDIC) : java.lang.Object {
      return mortgageeDetail
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at MortgageeDetails_TDICDV.pcf: line 147, column 59
    function sortValue_26 (mortgageeDetail :  entity.PolicyMortgagee_TDIC) : java.lang.Object {
      return mortgageeDetail.LoanNumberString
    }
    
    // 'toRemove' attribute on RowIterator at MortgageeDetails_TDICDV.pcf: line 122, column 55
    function toRemove_41 (mortgageeDetail :  entity.PolicyMortgagee_TDIC) : void {
      bopBuilding.toRemoveFromBOPBldgMortgagees(mortgageeDetail)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at MortgageeDetails_TDICDV.pcf: line 71, column 57
    function value_12 () : entity.AccountContactView[] {
      return getExistingMortgagees()
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at MortgageeDetails_TDICDV.pcf: line 87, column 57
    function value_17 () : entity.AccountContactView[] {
      return bopBuilding.MortgageeOtherCandidates_TDIC.asViews()
    }
    
    // 'value' attribute on RowIterator at MortgageeDetails_TDICDV.pcf: line 122, column 55
    function value_42 () : entity.PolicyMortgagee_TDIC[] {
      return bopBuilding.BOPBldgMortgagees
    }
    
    // 'value' attribute on AddMenuItemIterator at MortgageeDetails_TDICDV.pcf: line 45, column 49
    function value_7 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYMORTGAGEE_TDIC)
    }
    
    // 'visible' attribute on AddButton (id=AddContactsButton) at MortgageeDetails_TDICDV.pcf: line 41, column 35
    function visible_18 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'removeVisible' attribute on IteratorButtons (id=IteratorButtons) at MortgageeDetails_TDICDV.pcf: line 103, column 186
    function visible_19 () : java.lang.Boolean {
      return policyPeriod.Job typeis Submission or policyPeriod.Job typeis Renewal or bopBuilding.BOPBldgMortgagees.hasMatch(\mortgagee -> mortgagee.BasedOn == null)
    }
    
    // 'visible' attribute on DetailViewPanel (id=MortgageeDetails_TDICDV) at MortgageeDetails_TDICDV.pcf: line 9, column 83
    function visible_45 () : java.lang.Boolean {
      return !(policyPeriod.Job typeis Submission) or perm.System.viewsubmission
    }
    
    property get bopBuilding () : entity.BOPBuilding {
      return getRequireValue("bopBuilding", 0) as entity.BOPBuilding
    }
    
    property set bopBuilding ($arg :  entity.BOPBuilding) {
      setRequireValue("bopBuilding", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get existingMortgagee () : AccountContactView[] {
      return getVariableValue("existingMortgagee", 0) as AccountContactView[]
    }
    
    property set existingMortgagee ($arg :  AccountContactView[]) {
      setVariableValue("existingMortgagee", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function setExpirationDate(policyMortgagee : PolicyMortgagee_TDIC[]) {
      policyMortgagee.each(\mortgagee -> {
        if(mortgagee.ExpirationDate_TDIC == null) {
          mortgagee.ExpirationDate_TDIC = mortgagee.Branch.EditEffectiveDate
        }
      })
    }
    
    function getExistingMortgagees() : AccountContactView[] {
      if (existingMortgagee == null) {
        var addedContacts = bopBuilding.BOPBldgLossPayees*.AccountContactRole*.AccountContact
        var all = bopBuilding.ExistingLossPayees_TDIC
        var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
        existingMortgagee = remaining //policyLine.ExistingAdditionalInsureds.asViews()
      }
      return existingMortgagee
    }
    
    function getEffectiveDate(mortgagee : PolicyMortgagee_TDIC) : Date {
      var effDates = bopBuilding.Branch.AllEffectiveDates.toSet()
      for(effDate in effDates.order()) {
        var version = mortgagee.VersionList.AsOf(effDate)
        if(version != null) {
          return version.EffectiveDate
        }
      }
      return mortgagee.EffectiveDate
    }
    
    
  }
  
  
}