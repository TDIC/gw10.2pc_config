package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RatingOverridePanelSet_GLLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=StandardTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 219, column 55
    function currency_85 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 237, column 44
    function defaultSetter_100 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 233, column 51
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideAmountBilling = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 233, column 51
    function editable_91 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and cost.Overridable
    }
    
    // 'initialValue' attribute on Variable at RatingOverridePanelSet.GLLine.pcf: line 185, column 32
    function initialValue_56 () : GLExposure {
      return cost.GLExposure
    }
    
    // RowIterator (id=TermIterator1) at RatingOverridePanelSet.GLLine.pcf: line 181, column 70
    function initializeVariables_112 () : void {
        exposure = cost.GLExposure;

    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at RatingOverridePanelSet.GLLine.pcf: line 189, column 52
    function valueRoot_58 () : java.lang.Object {
      return cost.GLCostType
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 195, column 64
    function valueRoot_63 () : java.lang.Object {
      return cost.GeneralLiabilityLine
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 201, column 60
    function valueRoot_69 () : java.lang.Object {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=territoryCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 204, column 62
    function valueRoot_72 () : java.lang.Object {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=specialityCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 207, column 65
    function valueRoot_75 () : java.lang.Object {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=stepYear_Cell) at RatingOverridePanelSet.GLLine.pcf: line 210, column 78
    function valueRoot_78 () : java.lang.Object {
      return tdic.pc.config.rating.RatingConstants
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 213, column 41
    function valueRoot_81 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on MonetaryAmountCell (id=actualtermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 243, column 42
    function value_104 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 249, column 49
    function value_108 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at RatingOverridePanelSet.GLLine.pcf: line 189, column 52
    function value_57 () : java.lang.String {
      return cost.GLCostType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at RatingOverridePanelSet.GLLine.pcf: line 192, column 297
    function value_60 () : java.lang.String {
      return (period.Offering.CodeIdentifier == tdic.pc.config.rating.RatingConstants.plCMOffering)?cost.GeneralLiabilityLine.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.ValueAsString:cost.GeneralLiabilityLine.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.ValueAsString
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 195, column 64
    function value_62 () : java.util.Date {
      return cost.GeneralLiabilityLine.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 198, column 65
    function value_65 () : java.util.Date {
      return cost.GeneralLiabilityLine.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 201, column 60
    function value_68 () : java.lang.String {
      return exposure.ClassCode_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=territoryCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 204, column 62
    function value_71 () : java.lang.String {
      return exposure.GLTerritory_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=specialityCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 207, column 65
    function value_74 () : java.lang.String {
      return exposure.SpecialityCode_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=stepYear_Cell) at RatingOverridePanelSet.GLLine.pcf: line 210, column 78
    function value_77 () : java.lang.String {
      return tdic.pc.config.rating.RatingConstants.currentStepYear
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 213, column 41
    function value_80 () : java.lang.String {
      return cost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 219, column 55
    function value_83 () : gw.pl.currency.MonetaryAmount {
      return cost.StandardTermAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 225, column 51
    function value_87 () : gw.pl.currency.MonetaryAmount {
      return cost.StandardAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 233, column 51
    function value_92 () : gw.pl.currency.MonetaryAmount {
      return cost.OverrideAmountBilling
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 237, column 44
    function value_99 () : java.lang.String {
      return cost.OverrideReason
    }
    
    property get cost () : entity.GLCovExposureCost {
      return getIteratedValue(2) as entity.GLCovExposureCost
    }
    
    property get exposure () : GLExposure {
      return getVariableValue("exposure", 2) as GLExposure
    }
    
    property set exposure ($arg :  GLExposure) {
      setVariableValue("exposure", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends policyIteratorLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=StandardTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 329, column 55
    function currency_121 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 343, column 51
    function defaultSetter_129 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideAmountBilling = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 347, column 44
    function defaultSetter_136 (__VALUE_TO_SET :  java.lang.Object) : void {
      cost.OverrideReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 343, column 51
    function editable_127 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and cost.Overridable
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 323, column 41
    function valueRoot_117 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 323, column 41
    function value_116 () : java.lang.String {
      return cost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 329, column 55
    function value_119 () : gw.pl.currency.MonetaryAmount {
      return cost.StandardTermAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 335, column 51
    function value_123 () : gw.pl.currency.MonetaryAmount {
      return cost.StandardAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 343, column 51
    function value_128 () : gw.pl.currency.MonetaryAmount {
      return cost.OverrideAmountBilling
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 347, column 44
    function value_135 () : java.lang.String {
      return cost.OverrideReason
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 353, column 49
    function value_140 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 359, column 42
    function value_144 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmount
    }
    
    property get cost () : entity.GLCost {
      return getIteratedValue(2) as entity.GLCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 142, column 55
    function currency_27 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 156, column 55
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      histCost.OverrideAmountBilling = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 160, column 48
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      histCost.OverrideReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 156, column 55
    function editable_33 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and histCost.Overridable
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 136, column 45
    function valueRoot_23 () : java.lang.Object {
      return histCost
    }
    
    // 'value' attribute on DateCell (id=histEffDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 118, column 65
    function valueRoot_5 () : java.lang.Object {
      return histCost.GLSplitParameters
    }
    
    // 'value' attribute on TextCell (id=histClassCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 124, column 61
    function value_10 () : java.lang.String {
      return histCost.GLSplitParameters.ClassCode
    }
    
    // 'value' attribute on TextCell (id=histTerritoryCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 127, column 65
    function value_13 () : java.lang.String {
      return histCost.GLSplitParameters.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=histSpecialityCode_Cell) at RatingOverridePanelSet.GLLine.pcf: line 130, column 66
    function value_16 () : java.lang.String {
      return histCost.GLSplitParameters.SpecialityCode
    }
    
    // 'value' attribute on TextCell (id=histDescription_Cell) at RatingOverridePanelSet.GLLine.pcf: line 133, column 61
    function value_19 () : java.lang.String {
      return histCost.GLSplitParameters.splitYear
    }
    
    // 'value' attribute on TextCell (id=histLimitHeader_Cell) at RatingOverridePanelSet.GLLine.pcf: line 115, column 305
    function value_2 () : java.lang.String {
      return (period.Offering.CodeIdentifier == tdic.pc.config.rating.RatingConstants.plCMOffering)?histCost.GeneralLiabilityLine.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.ValueAsString:histCost.GeneralLiabilityLine.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.ValueAsString
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at RatingOverridePanelSet.GLLine.pcf: line 136, column 45
    function value_22 () : java.lang.String {
      return histCost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 142, column 55
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return histCost.StandardAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardTermAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 148, column 59
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return histCost.StandardTermAmountBilling
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideTermAmountBilling_Cell) at RatingOverridePanelSet.GLLine.pcf: line 156, column 55
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return histCost.OverrideAmountBilling
    }
    
    // 'value' attribute on DateCell (id=histEffDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 118, column 65
    function value_4 () : java.util.Date {
      return histCost.GLSplitParameters.EffectiveDate
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at RatingOverridePanelSet.GLLine.pcf: line 160, column 48
    function value_41 () : java.lang.String {
      return histCost.OverrideReason
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualTermAmount1_Cell) at RatingOverridePanelSet.GLLine.pcf: line 166, column 46
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return histCost.ActualAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualAmount_Cell) at RatingOverridePanelSet.GLLine.pcf: line 172, column 53
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return histCost.ActualAmountBilling
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at RatingOverridePanelSet.GLLine.pcf: line 121, column 66
    function value_7 () : java.util.Date {
      return histCost.GLSplitParameters.ExpirationDate
    }
    
    property get histCost () : entity.GLHistoricalCost_TDIC {
      return getIteratedValue(2) as entity.GLHistoricalCost_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListViewPanelExpressionsImpl extends RatingOverridePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator (id=TermIterator) at RatingOverridePanelSet.GLLine.pcf: line 108, column 74
    function editable_1 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at RatingOverridePanelSet.GLLine.pcf: line 19, column 38
    function initialValue_0 () : List<GLCost> {
      return period.GLLine.Costs.whereTypeIs(GLCost).where(\cst -> !(cst typeis GLStateCost or cst typeis GLAddnlInsdSchedCost_TDIC)).toList()
    }
    
    // 'value' attribute on RowIterator (id=TermIterator1) at RatingOverridePanelSet.GLLine.pcf: line 181, column 70
    function value_113 () : java.util.List<entity.GLCovExposureCost> {
      return costs.whereTypeIs(GLCovExposureCost)
    }
    
    // 'value' attribute on RowIterator (id=TermIterator) at RatingOverridePanelSet.GLLine.pcf: line 108, column 74
    function value_54 () : java.util.List<entity.GLHistoricalCost_TDIC> {
      return costs.whereTypeIs(GLHistoricalCost_TDIC)
    }
    
    property get costs () : List<GLCost> {
      return getVariableValue("costs", 1) as List<GLCost>
    }
    
    property set costs ($arg :  List<GLCost>) {
      setVariableValue("costs", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingOverridePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/ratingoverride/RatingOverridePanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class policyIteratorLVExpressionsImpl extends RatingOverridePanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at RatingOverridePanelSet.GLLine.pcf: line 319, column 59
    function editable_115 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at RatingOverridePanelSet.GLLine.pcf: line 263, column 38
    function initialValue_114 () : List<GLCost> {
      return period.GLLine.Costs?.whereTypeIs(GLCost).where(\cst -> (cst typeis GLStateCost or cst typeis GLAddnlInsdSchedCost_TDIC or cst typeis GLCovCost_TDIC)).toList()
    }
    
    // 'value' attribute on RowIterator at RatingOverridePanelSet.GLLine.pcf: line 319, column 59
    function value_148 () : java.util.List<entity.GLCost> {
      return glCosts
    }
    
    property get glCosts () : List<GLCost> {
      return getVariableValue("glCosts", 1) as List<GLCost>
    }
    
    property set glCosts ($arg :  List<GLCost>) {
      setVariableValue("glCosts", 1, $arg)
    }
    
    
  }
  
  
}