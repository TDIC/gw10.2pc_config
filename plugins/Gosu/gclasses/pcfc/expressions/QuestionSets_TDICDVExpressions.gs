package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionSets_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionSets_TDICDVExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionSets_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends QuestionSets_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at QuestionSets_TDICDV.pcf: line 36, column 31
    function def_onEnter_4 (def :  pcf.QuestionSet_TDICLV) : void {
      def.onEnter(questionSet, answerContainer, onChangeBlock,openForEdit)
    }
    
    // 'def' attribute on ListViewInput at QuestionSets_TDICDV.pcf: line 36, column 31
    function def_refreshVariables_5 (def :  pcf.QuestionSet_TDICLV) : void {
      def.refreshVariables(questionSet, answerContainer, onChangeBlock,openForEdit)
    }
    
    // 'value' attribute on HiddenInput (id=QuestionSetName_Input) at QuestionSets_TDICDV.pcf: line 30, column 30
    function valueRoot_1 () : java.lang.Object {
      return questionSet
    }
    
    // 'value' attribute on HiddenInput (id=QuestionSetName_Input) at QuestionSets_TDICDV.pcf: line 30, column 30
    function value_0 () : java.lang.String {
      return questionSet.Name
    }
    
    // 'visible' attribute on InputSet at QuestionSets_TDICDV.pcf: line 25, column 76
    function visible_6 () : java.lang.Boolean {
      return answerContainer.hasAnswerForQuestionSet( questionSet )
    }
    
    property get questionSet () : gw.api.productmodel.QuestionSet {
      return getIteratedValue(1) as gw.api.productmodel.QuestionSet
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionSets_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionSets_TDICDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on InputIterator at QuestionSets_TDICDV.pcf: line 23, column 55
    function value_7 () : gw.api.productmodel.QuestionSet[] {
      return questionSets
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get openForEdit () : Boolean {
      return getRequireValue("openForEdit", 0) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get questionSets () : gw.api.productmodel.QuestionSet[] {
      return getRequireValue("questionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set questionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setRequireValue("questionSets", 0, $arg)
    }
    
    
  }
  
  
}