package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/losshistory/LossHistoryInputSet.att.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LossHistoryInputSet_attExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/losshistory/LossHistoryInputSet.att.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossHistoryInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at LossHistoryInputSet.att.pcf: line 17, column 23
    function initialValue_0 () : boolean {
      return documentsActionsHelper.DocumentContentServerAvailable
    }
    
    // 'initialValue' attribute on Variable at LossHistoryInputSet.att.pcf: line 21, column 23
    function initialValue_1 () : boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'value' attribute on TextInput (id=NumberOfLosses_Input) at LossHistoryInputSet.att.pcf: line 28, column 46
    function valueRoot_4 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TextInput (id=NumberOfLosses_Input) at LossHistoryInputSet.att.pcf: line 28, column 46
    function value_3 () : java.lang.Integer {
      return policyPeriod.Policy.NumPriorLosses
    }
    
    // 'visible' attribute on TextInput (id=NumberOfLosses_Input) at LossHistoryInputSet.att.pcf: line 28, column 46
    function visible_2 () : java.lang.Boolean {
      return !policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on TextInput (id=NumberOfLossesNew_Input) at LossHistoryInputSet.att.pcf: line 35, column 45
    function visible_7 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    property get contentActionsEnabled () : boolean {
      return getVariableValue("contentActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set contentActionsEnabled ($arg :  boolean) {
      setVariableValue("contentActionsEnabled", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get metadataActionsEnabled () : boolean {
      return getVariableValue("metadataActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set metadataActionsEnabled ($arg :  boolean) {
      setVariableValue("metadataActionsEnabled", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}