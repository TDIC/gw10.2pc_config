package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLManuscriptEndor_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLManuscriptEndor_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLManuscriptEndor_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 49, column 62
    function allCheckedRowsAction_5 (CheckedValues :  entity.GLManuscript_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=resetButton) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 56, column 62
    function allCheckedRowsAction_8 (CheckedValues :  entity.GLManuscript_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      resetAiOrCh(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 49, column 62
    function available_3 () : java.lang.Boolean {
      return glLine.Branch.Job typeis PolicyChange
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLManuscriptInputGroup) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 33, column 99
    function available_58 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable) and perm.System.editmanuscript_tdic
    }
    
    // 'editable' attribute on ListViewInput (id=GLManuscriptLV) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 38, column 27
    function editable_56 () : java.lang.Boolean {
      return perm.System.editmanuscript_tdic
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'onToggle' attribute on InputGroup (id=GLManuscriptInputGroup) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 33, column 99
    function setter_59 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 82, column 65
    function sortValue_10 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function sortValue_11 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptType
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 100, column 281
    function sortValue_12 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return (scheduledItem.PolicyAddInsured_TDIC.DisplayName==null? "" : scheduledItem.PolicyAddInsured_TDIC.DisplayName) + " " + (scheduledItem.PolicyAddInsured_TDIC.Desciption==null? "" : scheduledItem.PolicyAddInsured_TDIC.Desciption)
    }
    
    // 'value' attribute on TextCell (id=PolicyCertificateHolder_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 124, column 267
    function sortValue_13 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return (scheduledItem.CertificateHolder.DisplayName==null? "" : scheduledItem.CertificateHolder.DisplayName) + " " + (scheduledItem.CertificateHolder.Description==null? "" : scheduledItem.CertificateHolder.Description)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 148, column 62
    function sortValue_14 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptDescription
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 75, column 64
    function sortValue_9 (scheduledItem :  entity.GLManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptEffectiveDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 68, column 52
    function toCreateAndAdd_53 () : entity.GLManuscript_TDIC {
      return glLine.createAndAddGLManuscript_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 68, column 52
    function toRemove_54 (scheduledItem :  entity.GLManuscript_TDIC) : void {
      if(scheduledItem.New) {glLine.removeFromGLManuscript_TDIC(scheduledItem)}; 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 68, column 52
    function value_55 () : entity.GLManuscript_TDIC[] {
      return glLine.GLManuscript_TDIC
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 42, column 161
    function visible_2 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLManuscript_TDIC.hasMatch(\elt1 -> elt1.New)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLManuscriptInputGroup) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 33, column 99
    function visible_57 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function validateEndorsementExpDate(date: Date): String {
      var transEffDate = coverable.PolicyLine.Branch.EditEffectiveDate
      var allowedDate = (date.afterOrEqual(transEffDate.addWeeks(2)) and date.beforeOrEqual(transEffDate.addDays(90)))
      if(!allowedDate){
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLManuscriptCovTDIC.ExpDate")
      }
      return null
    }
    
    function setTypeValue() : GLManuscriptType_TDIC[]{
      if(glLine.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || glLine.Branch.Offering.CodeIdentifier == "PLOccurence_TDIC"){
        return GLManuscriptType_TDIC.TF_PL.TypeKeys.toTypedArray()
      }else{
        return GLManuscriptType_TDIC.TF_CYB.TypeKeys.toTypedArray()
      }
    }
    function setExpirationDate(manuScripts : GLManuscript_TDIC[]) {
      manuScripts.each(\manuScript -> {
        if(manuScript.ManuscriptExpirationDate == null) {
          manuScript.ManuscriptExpirationDate = manuScript.Branch.EditEffectiveDate
        }
      })
    }
    
    function resetAiOrCh (manuScripts : GLManuscript_TDIC[]) {
      manuScripts.each(\manuScript -> {
        if(manuScript.PolicyAddInsured_TDIC != null) {
          manuScript.PolicyAddInsured_TDIC = null
        }
        if (manuScript.CertificateHolder != null) {
          manuScript.CertificateHolder = null
        }
        
      })
      
      
    }
    
    function getAdditionalInsureds() : AccountContactView[] {
      return glLine?.AdditionalInsureds*.AccountContactRole*.AccountContact?.asViews()
    }
    
    function getCertificateHolders() : AccountContactView[] {
      return glLine?.GLCertificateHolders_TDIC*.AccountContactRole*.AccountContact?.asViews()
    }
    
    function setAdditionalInsureds(scheduledItem : GLManuscript_TDIC, contact : AccountContact ) {
      scheduledItem.PolicyAddInsured_TDIC = glLine?.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\elt -> elt.PolicyAddlInsured?.AccountContactRole?.AccountContact == contact)
    }
    
    function setCertificateHolder(scheduledItem : GLManuscript_TDIC, contact : AccountContact ) {
      scheduledItem.CertificateHolder = glLine?.GLCertificateHolders_TDIC?.firstWhere(\elt -> elt.AccountContactRole?.AccountContact == contact)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLManuscriptEndor_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 115, column 48
    function action_32 () : void {
      setAdditionalInsureds(scheduledItem, unassignedContact.AccountContact)
    }
    
    // 'available' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 115, column 48
    function available_31 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 115, column 48
    function label_33 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.AccountContactView {
      return getIteratedValue(2) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLManuscriptEndor_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 139, column 48
    function action_42 () : void {
      setCertificateHolder(scheduledItem, unassignedContact.AccountContact)
    }
    
    // 'available' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 139, column 48
    function available_41 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 139, column 48
    function label_43 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.AccountContactView {
      return getIteratedValue(2) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLManuscriptEndor_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 100, column 281
    function available_35 () : java.lang.Boolean {
      return (scheduledItem.ManuscriptType==GLManuscriptType_TDIC.TC_ADDITIONALINSUREDCONTINUED)
    }
    
    // 'available' attribute on TextCell (id=PolicyCertificateHolder_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 124, column 267
    function available_45 () : java.lang.Boolean {
      return (scheduledItem.ManuscriptType==GLManuscriptType_TDIC.TC_CERTIFICATEHOLDERNAMEAMENDEDTOREAD)
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.ManuscriptType = (__VALUE_TO_SET as typekey.GLManuscriptType_TDIC)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 148, column 62
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.ManuscriptDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'requestValidationExpression' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 82, column 65
    function requestValidationExpression_18 (VALUE :  java.util.Date) : java.lang.Object {
      return validateEndorsementExpDate(VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 110, column 36
    function sortBy_29 (unassignedContact :  entity.AccountContactView) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function valueRange_25 () : java.lang.Object {
      return setTypeValue()
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 75, column 64
    function valueRoot_16 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 75, column 64
    function value_15 () : java.util.Date {
      return scheduledItem.ManuscriptEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 82, column 65
    function value_19 () : java.util.Date {
      return scheduledItem.ManuscriptExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function value_22 () : typekey.GLManuscriptType_TDIC {
      return scheduledItem.ManuscriptType
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 107, column 81
    function value_34 () : entity.AccountContactView[] {
      return getAdditionalInsureds()
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 100, column 281
    function value_36 () : java.lang.String {
      return (scheduledItem.PolicyAddInsured_TDIC.DisplayName==null? "" : scheduledItem.PolicyAddInsured_TDIC.DisplayName) + " " + (scheduledItem.PolicyAddInsured_TDIC.Desciption==null? "" : scheduledItem.PolicyAddInsured_TDIC.Desciption)
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 131, column 81
    function value_44 () : entity.AccountContactView[] {
      return getCertificateHolders()
    }
    
    // 'value' attribute on TextCell (id=PolicyCertificateHolder_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 124, column 267
    function value_46 () : java.lang.String {
      return (scheduledItem.CertificateHolder.DisplayName==null? "" : scheduledItem.CertificateHolder.DisplayName) + " " + (scheduledItem.CertificateHolder.Description==null? "" : scheduledItem.CertificateHolder.Description)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 148, column 62
    function value_49 () : java.lang.String {
      return scheduledItem.ManuscriptDescription
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function verifyValueRangeIsAllowedType_26 ($$arg :  typekey.GLManuscriptType_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 91, column 59
    function verifyValueRange_27 () : void {
      var __valueRangeArg = setTypeValue()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'visible' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.GLManuscriptEndor_TDIC.pcf: line 107, column 81
    function visible_30 () : java.lang.Boolean {
      return glLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    property get scheduledItem () : entity.GLManuscript_TDIC {
      return getIteratedValue(1) as entity.GLManuscript_TDIC
    }
    
    
  }
  
  
}