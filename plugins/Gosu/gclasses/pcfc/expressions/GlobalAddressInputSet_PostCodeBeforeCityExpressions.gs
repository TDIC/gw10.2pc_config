package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.PostCodeBeforeCity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GlobalAddressInputSet_PostCodeBeforeCityExpressions {
  @javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.PostCodeBeforeCity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GlobalAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function available_22 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'available' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function available_33 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'available' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function available_44 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'available' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function available_61 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'available' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function available_77 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CEDEX)
    }
    
    // 'available' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function available_88 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CEDEXBUREAU)
    }
    
    // 'available' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function available_9 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine3 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.PostalCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.CEDEX = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.CEDEXBureau = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function editable_10 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function editable_23 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function editable_34 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'editable' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function editable_45 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'editable' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function editable_62 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'editable' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function editable_78 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CEDEX)
    }
    
    // 'editable' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function editable_89 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CEDEXBUREAU)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 14, column 50
    function initialValue_0 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("City","City,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 18, column 50
    function initialValue_1 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("PostalCode","City,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 25, column 33
    function initialValue_2 () : java.lang.Integer {
      if (addressOwner != null) addressOwner.InEditMode = CurrentLocation.InEditMode; return 0
    }
    
    // 'inputConversion' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function inputConversion_49 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.address.PostalCodeInputFormatter.convertPostalCode(VALUE, addressOwner.SelectedCountry)
    }
    
    // 'inputMask' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function inputMask_54 () : java.lang.String {
      return gw.api.contact.AddressAutocompleteUtil.getInputMask(addressOwner.AddressDelegate, "PostalCode")
    }
    
    // 'label' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function label_12 () : java.lang.Object {
      return addressOwner.AddressLine1Label
    }
    
    // 'label' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 31, column 50
    function label_4 () : java.lang.Object {
      return addressOwner.AddressNameLabel
    }
    
    // 'label' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function label_48 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel
    }
    
    // 'label' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function label_65 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).CityLabel
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 141, column 113
    function onChange_76 () : void {
      if (not addressOwner.AddressDelegate.CEDEX) addressOwner.AddressDelegate.CEDEXBureau = null
    }
    
    // 'required' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function required_13 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'required' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function required_25 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'required' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function required_36 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'required' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function required_50 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'required' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function required_66 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'required' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function required_80 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.CEDEX)
    }
    
    // 'required' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function required_91 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.CEDEXBUREAU)
    }
    
    // 'validationExpression' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function validationExpression_46 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "PostalCode", gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel)
    }
    
    // 'validationExpression' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function validationExpression_63 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "City")
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function valueRoot_16 () : java.lang.Object {
      return addressOwner.AddressDelegate
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function value_14 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine1
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function value_26 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine2
    }
    
    // 'value' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function value_37 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine3
    }
    
    // 'value' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 31, column 50
    function value_5 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(addressOwner.AddressDelegate, "\n")
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function value_51 () : java.lang.String {
      return addressOwner.AddressDelegate.PostalCode
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function value_67 () : java.lang.String {
      return addressOwner.AddressDelegate.City
    }
    
    // 'value' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function value_81 () : java.lang.Boolean {
      return addressOwner.AddressDelegate.CEDEX
    }
    
    // 'value' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function value_92 () : java.lang.String {
      return addressOwner.AddressDelegate.CEDEXBureau
    }
    
    // 'visible' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 54, column 90
    function visible_11 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 62, column 90
    function visible_24 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'visible' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 31, column 50
    function visible_3 () : java.lang.Boolean {
      return addressOwner.ShowAddressSummary
    }
    
    // 'visible' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 70, column 90
    function visible_35 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'visible' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 121, column 88
    function visible_47 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'visible' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 130, column 82
    function visible_64 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'visible' attribute on CheckBoxInput (id=CEDEX_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 138, column 82
    function visible_79 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CEDEX)
    }
    
    // 'visible' attribute on TextInput (id=CEDEXBureau_Input) at GlobalAddressInputSet.PostCodeBeforeCity.pcf: line 150, column 128
    function visible_90 () : java.lang.Boolean {
      return addressOwner.AddressDelegate.CEDEX and addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CEDEXBUREAU)
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    property get cityhandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("cityhandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set cityhandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("cityhandler", 0, $arg)
    }
    
    property get pchandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("pchandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set pchandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("pchandler", 0, $arg)
    }
    
    property get saveEditMode () : java.lang.Integer {
      return getVariableValue("saveEditMode", 0) as java.lang.Integer
    }
    
    property set saveEditMode ($arg :  java.lang.Integer) {
      setVariableValue("saveEditMode", 0, $arg)
    }
    
    
  }
  
  
}