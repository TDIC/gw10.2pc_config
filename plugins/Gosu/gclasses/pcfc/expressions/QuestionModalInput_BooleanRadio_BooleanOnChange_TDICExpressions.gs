package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_BooleanRadio_BooleanOnChange_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioCell (id=BooleanRadioInput_Cell) at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 25, column 65
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      answerContainer.getAnswer(question).BooleanAnswer = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on BooleanRadioCell (id=BooleanRadioInput_Cell) at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 25, column 65
    function editable_2 () : java.lang.Boolean {
      return question.isQuestionEditable_TDIC(answerContainer)
    }
    
    // 'initialValue' attribute on Variable at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 20, column 23
    function initialValue_0 () : block() {
      return \ -> question
    }
    
    // 'onChange' attribute on PostOnChange at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 27, column 78
    function onChange_1 () : void {
      if (onChangeBlock != null) onChangeBlock();ChangeBlock()
    }
    
    // 'required' attribute on BooleanRadioCell (id=BooleanRadioInput_Cell) at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 25, column 65
    function required_3 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on BooleanRadioCell (id=BooleanRadioInput_Cell) at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 25, column 65
    function valueRoot_6 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on BooleanRadioCell (id=BooleanRadioInput_Cell) at QuestionModalInput.BooleanRadio_BooleanOnChange_TDIC.pcf: line 25, column 65
    function value_4 () : java.lang.Boolean {
      return answerContainer.getAnswer(question).BooleanAnswer
    }
    
    property get ChangeBlock () : block() {
      return getVariableValue("ChangeBlock", 0) as block()
    }
    
    property set ChangeBlock ($arg :  block()) {
      setVariableValue("ChangeBlock", 0, $arg)
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}