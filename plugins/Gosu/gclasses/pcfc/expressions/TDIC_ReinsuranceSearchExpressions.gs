package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ReinsuranceSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ReinsuranceSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=TDIC_ReinsuranceSearch) at TDIC_ReinsuranceSearch.pcf: line 8, column 80
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.reinsuranceSearch_TDIC
    }
    
    // 'def' attribute on ScreenRef at TDIC_ReinsuranceSearch.pcf: line 10, column 45
    function def_onEnter_0 (def :  pcf.TDIC_ReinsuranceSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at TDIC_ReinsuranceSearch.pcf: line 10, column 45
    function def_refreshVariables_1 (def :  pcf.TDIC_ReinsuranceSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=TDIC_ReinsuranceSearch) at TDIC_ReinsuranceSearch.pcf: line 8, column 80
    static function parent_3 () : pcf.api.Destination {
      return pcf.Search.createDestination()
    }
    
    override property get CurrentLocation () : pcf.TDIC_ReinsuranceSearch {
      return super.CurrentLocation as pcf.TDIC_ReinsuranceSearch
    }
    
    
  }
  
  
}