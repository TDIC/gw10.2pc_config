package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/EditPolicyContactRolePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditPolicyContactRolePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/EditPolicyContactRolePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditPolicyContactRolePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyContactRole :  PolicyContactRole, canEdit :  Boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at EditPolicyContactRolePopup.pcf: line 53, column 71
    function action_6 () : void {
      if (impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(policyContactRole.ContactDenorm, impactedPolicies_TDIC)}
    }
    
    // 'beforeCommit' attribute on Popup (id=EditPolicyContactRolePopup) at EditPolicyContactRolePopup.pcf: line 13, column 146
    function beforeCommit_11 (pickedValue :  PolicyContactRole) : void {
      validatePolicyContacts(policyContactRole.Branch); helper.validateAndUpdateStatusOfAddressIfNotProfileChange(policyContactRole);gw.policy.PolicyContactRoleValidation.validatePNIChanges_TDIC(policyContactRole, policyContactRole.Changed)
    }
    
    // 'beforeValidate' attribute on Popup (id=EditPolicyContactRolePopup) at EditPolicyContactRolePopup.pcf: line 13, column 146
    function beforeValidate_12 (pickedValue :  PolicyContactRole) : void {
      displayMembershipCheckError = tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(contact, displayMembershipCheckError, policyContactRole.Branch.BaseState)
    }
    
    // 'canEdit' attribute on Popup (id=EditPolicyContactRolePopup) at EditPolicyContactRolePopup.pcf: line 13, column 146
    function canEdit_13 () : java.lang.Boolean {
      return canEdit
    }
    
    // 'def' attribute on PanelRef at EditPolicyContactRolePopup.pcf: line 62, column 71
    function def_onEnter_9 (def :  pcf.PolicyContactRoleDetailsCV) : void {
      def.onEnter(policyContactRole, canEdit)
    }
    
    // 'def' attribute on PanelRef at EditPolicyContactRolePopup.pcf: line 62, column 71
    function def_refreshVariables_10 (def :  pcf.PolicyContactRoleDetailsCV) : void {
      def.refreshVariables(policyContactRole, canEdit)
    }
    
    // 'initialValue' attribute on Variable at EditPolicyContactRolePopup.pcf: line 25, column 23
    function initialValue_0 () : boolean {
      return policyContactRole.Subtype == typekey.PolicyContactRole.TC_WC7POLICYOWNEROFFICER
    }
    
    // 'initialValue' attribute on Variable at EditPolicyContactRolePopup.pcf: line 29, column 76
    function initialValue_1 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at EditPolicyContactRolePopup.pcf: line 33, column 30
    function initialValue_2 () : entity.Contact {
      return policyContactRole.AccountContactRole.AccountContact.Contact
    }
    
    // 'initialValue' attribute on Variable at EditPolicyContactRolePopup.pcf: line 41, column 50
    function initialValue_3 () : java.util.List<PolicyPeriod> {
      return policyContactRole.ContactDenorm == null ? null : policyContactRole.ContactDenorm.AssociationFinder.findLatestBoundPolicyPeriods().where( \ elt -> elt.Policy != policyContactRole.Branch.Policy)
    }
    
    // EditButtons at EditPolicyContactRolePopup.pcf: line 47, column 42
    function label_5 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on Verbatim (id=ContactUsageImpact_TDIC) at EditPolicyContactRolePopup.pcf: line 60, column 25
    function label_8 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisContactMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // 'pickValue' attribute on EditButtons at EditPolicyContactRolePopup.pcf: line 47, column 42
    function pickValue_4 () : PolicyContactRole {
      return policyContactRole
    }
    
    // 'title' attribute on Popup (id=EditPolicyContactRolePopup) at EditPolicyContactRolePopup.pcf: line 13, column 146
    static function title_15 (canEdit :  Boolean, policyContactRole :  PolicyContactRole) : java.lang.Object {
      return DisplayKey.get("Web.EditPolicyContactRolePopup.Title", (typeof policyContactRole).TypeInfo.DisplayName, policyContactRole)
    }
    
    // 'visible' attribute on Verbatim (id=ContactUsageImpact_TDIC) at EditPolicyContactRolePopup.pcf: line 60, column 25
    function visible_7 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    override property get CurrentLocation () : pcf.EditPolicyContactRolePopup {
      return super.CurrentLocation as pcf.EditPolicyContactRolePopup
    }
    
    property get canEdit () : Boolean {
      return getVariableValue("canEdit", 0) as Boolean
    }
    
    property set canEdit ($arg :  Boolean) {
      setVariableValue("canEdit", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get displayMembershipCheckError () : boolean {
      return getVariableValue("displayMembershipCheckError", 0) as java.lang.Boolean
    }
    
    property set displayMembershipCheckError ($arg :  boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.List<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.List<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get isWC7PolicyOwnerOfficer () : boolean {
      return getVariableValue("isWC7PolicyOwnerOfficer", 0) as java.lang.Boolean
    }
    
    property set isWC7PolicyOwnerOfficer ($arg :  boolean) {
      setVariableValue("isWC7PolicyOwnerOfficer", 0, $arg)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getVariableValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setVariableValue("policyContactRole", 0, $arg)
    }
    
    function validatePolicyContacts(policyPeriod : PolicyPeriod) {
      
      gw.validation.PCValidationContext.doPageLevelValidation( \ context -> {
        for (line in policyPeriod.Lines) {
          for (policyAddlInsured in line.AdditionalInsureds) {     
            var addlInsuredValidator = new gw.policy.PolicyAddlInsuredAndTypeUniqueValidation(context, policyAddlInsured)
            addlInsuredValidator.validate()
          }
        }
        
        // Check to make sure the user didn't delete the policy address
        var validator = new gw.policy.PolicyPeriodValidation(context, policyPeriod  )
        validator.checkPolicyAddress()
      })
      
    }
    
    
  }
  
  
}