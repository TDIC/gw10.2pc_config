package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/Policy_JobInfoDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Policy_JobInfoDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/Policy_JobInfoDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy_JobInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=JobFile_Input) at Policy_JobInfoDV.default.pcf: line 44, column 33
    function action_19 () : void {
      JobForward.go(job)
    }
    
    // 'action' attribute on TextInput (id=JobFile_Input) at Policy_JobInfoDV.default.pcf: line 44, column 33
    function action_dest_20 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(job)
    }
    
    // 'def' attribute on InputSetRef at Policy_JobInfoDV.default.pcf: line 68, column 29
    function def_onEnter_32 (def :  pcf.Policy_JobDetailsInputSet_Submission) : void {
      def.onEnter(job)
    }
    
    // 'def' attribute on InputSetRef at Policy_JobInfoDV.default.pcf: line 68, column 29
    function def_onEnter_34 (def :  pcf.Policy_JobDetailsInputSet_default) : void {
      def.onEnter(job)
    }
    
    // 'def' attribute on InputSetRef at Policy_JobInfoDV.default.pcf: line 68, column 29
    function def_refreshVariables_33 (def :  pcf.Policy_JobDetailsInputSet_Submission) : void {
      def.refreshVariables(job)
    }
    
    // 'def' attribute on InputSetRef at Policy_JobInfoDV.default.pcf: line 68, column 29
    function def_refreshVariables_35 (def :  pcf.Policy_JobDetailsInputSet_default) : void {
      def.refreshVariables(job)
    }
    
    // 'value' attribute on DateInput (id=PeriodEffDate_Input) at Policy_JobInfoDV.default.pcf: line 19, column 53
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      job.LatestPeriod.EditEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'mode' attribute on InputSetRef at Policy_JobInfoDV.default.pcf: line 68, column 29
    function mode_36 () : java.lang.Object {
      return job.Subtype
    }
    
    // 'value' attribute on DateInput (id=PeriodEffDate_Input) at Policy_JobInfoDV.default.pcf: line 19, column 53
    function valueRoot_2 () : java.lang.Object {
      return job.LatestPeriod
    }
    
    // 'value' attribute on TextInput (id=PeriodStatus_Input) at Policy_JobInfoDV.default.pcf: line 54, column 65
    function valueRoot_23 () : java.lang.Object {
      return job.ResultingBoundPeriod
    }
    
    // 'value' attribute on DateInput (id=CreateDate_Input) at Policy_JobInfoDV.default.pcf: line 28, column 33
    function valueRoot_9 () : java.lang.Object {
      return job
    }
    
    // 'value' attribute on DateInput (id=PeriodEffDate_Input) at Policy_JobInfoDV.default.pcf: line 19, column 53
    function value_0 () : java.util.Date {
      return job.LatestPeriod.EditEffectiveDate
    }
    
    // 'value' attribute on DateInput (id=CloseDate_Input) at Policy_JobInfoDV.default.pcf: line 32, column 32
    function value_11 () : java.util.Date {
      return job.CloseDate
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at Policy_JobInfoDV.default.pcf: line 36, column 36
    function value_14 () : java.lang.String {
      return job.DisplayStatus
    }
    
    // 'value' attribute on TextInput (id=PeriodStatus_Input) at Policy_JobInfoDV.default.pcf: line 54, column 65
    function value_22 () : java.lang.String {
      return job.ResultingBoundPeriod.PeriodDisplayStatus
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at Policy_JobInfoDV.default.pcf: line 60, column 57
    function value_25 () : java.util.Date {
      return job.ResultingBoundPeriod.PeriodStart
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at Policy_JobInfoDV.default.pcf: line 64, column 55
    function value_28 () : java.util.Date {
      return job.ResultingBoundPeriod.PeriodEnd
    }
    
    // 'value' attribute on DateInput (id=ProcessDate_TDIC_Input) at Policy_JobInfoDV.default.pcf: line 24, column 46
    function value_5 () : java.util.Date {
      return (job typeis Cancellation) ? job.CancelProcessDate : null
    }
    
    // 'value' attribute on DateInput (id=CreateDate_Input) at Policy_JobInfoDV.default.pcf: line 28, column 33
    function value_8 () : java.util.Date {
      return job.CreateTime
    }
    
    // 'visible' attribute on Label at Policy_JobInfoDV.default.pcf: line 39, column 47
    function visible_17 () : java.lang.Boolean {
      return !(job typeis Cancellation)
    }
    
    // 'visible' attribute on TextInput (id=JobFile_Input) at Policy_JobInfoDV.default.pcf: line 44, column 33
    function visible_18 () : java.lang.Boolean {
      return job.Viewable
    }
    
    // 'visible' attribute on InputSet at Policy_JobInfoDV.default.pcf: line 48, column 71
    function visible_31 () : java.lang.Boolean {
      return job.isComplete() and job.ResultingBoundPeriod!=null
    }
    
    // 'visible' attribute on DateInput (id=ProcessDate_TDIC_Input) at Policy_JobInfoDV.default.pcf: line 24, column 46
    function visible_4 () : java.lang.Boolean {
      return (job typeis Cancellation)
    }
    
    property get asOfDate () : java.util.Date {
      return getRequireValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setRequireValue("asOfDate", 0, $arg)
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    
  }
  
  
}