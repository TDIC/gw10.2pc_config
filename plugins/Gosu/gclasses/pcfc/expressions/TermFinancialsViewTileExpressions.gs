package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/summary/TermFinancialsViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TermFinancialsViewTileExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/summary/TermFinancialsViewTile.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TermFinancialsViewTileExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TileAction (id=RecalculateLossRatio) at TermFinancialsViewTile.pcf: line 29, column 59
    function action_4 () : void {
      lossRatioHelper.recalculateLossRatio()
    }
    
    // 'available' attribute on TileAction (id=RecalculateLossRatio) at TermFinancialsViewTile.pcf: line 29, column 59
    function available_2 () : java.lang.Boolean {
      return not period.Archived
    }
    
    // 'initialValue' attribute on Variable at TermFinancialsViewTile.pcf: line 18, column 60
    function initialValue_0 () : gw.api.web.dashboard.ui.policy.PremiumHelper {
      return new gw.api.web.dashboard.ui.policy.PremiumHelper(period)
    }
    
    // 'initialValue' attribute on Variable at TermFinancialsViewTile.pcf: line 22, column 51
    function initialValue_1 () : gw.job.audit.ReportingTrendSynopsis {
      return period.reportingTrendSynopsis(true)
    }
    
    // 'label' attribute on PerformanceIndicator (id=LossRatio) at TermFinancialsViewTile.pcf: line 65, column 83
    function label_14 () : java.lang.String {
      return DisplayKey.get('Web.Dashboard.Tile.TermFinancials.LossRatio', helper.LossRatioCalculationDate)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=CommercialLine1_Input) at TermFinancialsViewTile.pcf: line 92, column 78
    function label_28 () : java.lang.Object {
      return helper.CommercialLines[1].Key
    }
    
    // 'label' attribute on MonetaryAmountInput (id=CommercialLine0_Input) at TermFinancialsViewTile.pcf: line 118, column 78
    function label_49 () : java.lang.Object {
      return helper.CommercialLines[0].Key
    }
    
    // 'label' attribute on MonetaryAmountInput (id=CommercialLine2_Input) at TermFinancialsViewTile.pcf: line 123, column 78
    function label_55 () : java.lang.Object {
      return helper.CommercialLines[2].Key
    }
    
    // 'label' attribute on PerformanceIndicator (id=EarnedPremium) at TermFinancialsViewTile.pcf: line 51, column 40
    function label_9 () : java.lang.String {
      return helper.EarnedPremiumLabel
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalEstimatedPremium_Input) at TermFinancialsViewTile.pcf: line 77, column 46
    function valueRoot_18 () : java.lang.Object {
      return synopsis
    }
    
    // 'value' attribute on TextInput (id=IncludesEBURorNot_Input) at TermFinancialsViewTile.pcf: line 113, column 46
    function valueRoot_45 () : java.lang.Object {
      return helper
    }
    
    // 'value' attribute on PerformanceIndicator (id=TotalIncurred) at TermFinancialsViewTile.pcf: line 58, column 40
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return helper.TotalIncurred
    }
    
    // 'value' attribute on PerformanceIndicator (id=LossRatio) at TermFinancialsViewTile.pcf: line 65, column 83
    function value_13 () : String {
      return helper.LossRatio
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalEstimatedPremium_Input) at TermFinancialsViewTile.pcf: line 77, column 46
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return synopsis.TotalEstimatedPremium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalReportedPremium_Input) at TermFinancialsViewTile.pcf: line 83, column 46
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return synopsis.TotalReportedPremium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommercialLine1_Input) at TermFinancialsViewTile.pcf: line 92, column 78
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return helper.CommercialLines[1].Value
    }
    
    // 'value' attribute on TextInput (id=PremiumRatio_Input) at TermFinancialsViewTile.pcf: line 101, column 46
    function value_34 () : java.math.BigDecimal {
      return synopsis.PremiumRatio
    }
    
    // 'value' attribute on TextInput (id=DaysReported_Input) at TermFinancialsViewTile.pcf: line 108, column 46
    function value_39 () : java.lang.Integer {
      return synopsis.DaysReported
    }
    
    // 'value' attribute on TextInput (id=IncludesEBURorNot_Input) at TermFinancialsViewTile.pcf: line 113, column 46
    function value_44 () : java.lang.String {
      return helper.IncludesEarnedButUnreportedText
    }
    
    // 'value' attribute on PerformanceIndicator (id=TotalPremium) at TermFinancialsViewTile.pcf: line 38, column 52
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return helper.TotalPremium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommercialLine0_Input) at TermFinancialsViewTile.pcf: line 118, column 78
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return helper.CommercialLines[0].Value
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommercialLine2_Input) at TermFinancialsViewTile.pcf: line 123, column 78
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return helper.CommercialLines[2].Value
    }
    
    // 'value' attribute on PerformanceIndicator (id=TaxesAndFees) at TermFinancialsViewTile.pcf: line 44, column 52
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return helper.TaxesAndFees
    }
    
    // 'value' attribute on PerformanceIndicator (id=EarnedPremium) at TermFinancialsViewTile.pcf: line 51, column 40
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return helper.EarnedPremium
    }
    
    // 'visible' attribute on PerformanceIndicator (id=LossRatio) at TermFinancialsViewTile.pcf: line 65, column 83
    function visible_12 () : java.lang.Boolean {
      return not period.Archived and not User.util.CurrentUser.ExternalUser
    }
    
    // 'visible' attribute on Label (id=TrendAnalysisLabel) at TermFinancialsViewTile.pcf: line 71, column 46
    function visible_15 () : java.lang.Boolean {
      return helper.IsPremiumReporting
    }
    
    // 'visible' attribute on Label (id=EarnedPremiumLines1) at TermFinancialsViewTile.pcf: line 87, column 37
    function visible_26 () : java.lang.Boolean {
      return period.MultiLine
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=CommercialLine1_Input) at TermFinancialsViewTile.pcf: line 92, column 78
    function visible_27 () : java.lang.Boolean {
      return period.MultiLine and helper.CommercialLines.length > 1
    }
    
    // 'visible' attribute on TileAction (id=RecalculateLossRatio) at TermFinancialsViewTile.pcf: line 29, column 59
    function visible_3 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=CommercialLine0_Input) at TermFinancialsViewTile.pcf: line 118, column 78
    function visible_48 () : java.lang.Boolean {
      return period.MultiLine and helper.CommercialLines.length > 0
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=CommercialLine2_Input) at TermFinancialsViewTile.pcf: line 123, column 78
    function visible_54 () : java.lang.Boolean {
      return period.MultiLine and helper.CommercialLines.length > 2
    }
    
    property get helper () : gw.api.web.dashboard.ui.policy.PremiumHelper {
      return getVariableValue("helper", 0) as gw.api.web.dashboard.ui.policy.PremiumHelper
    }
    
    property set helper ($arg :  gw.api.web.dashboard.ui.policy.PremiumHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get lossRatioHelper () : gw.api.web.dashboard.ui.claims.LossRatioHelper {
      return getRequireValue("lossRatioHelper", 0) as gw.api.web.dashboard.ui.claims.LossRatioHelper
    }
    
    property set lossRatioHelper ($arg :  gw.api.web.dashboard.ui.claims.LossRatioHelper) {
      setRequireValue("lossRatioHelper", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get synopsis () : gw.job.audit.ReportingTrendSynopsis {
      return getVariableValue("synopsis", 0) as gw.job.audit.ReportingTrendSynopsis
    }
    
    property set synopsis ($arg :  gw.job.audit.ReportingTrendSynopsis) {
      setVariableValue("synopsis", 0, $arg)
    }
    
    
  }
  
  
}