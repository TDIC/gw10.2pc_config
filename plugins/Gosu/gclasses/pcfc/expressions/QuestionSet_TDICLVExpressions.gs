package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionSet_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionSet_TDICLVExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionSet_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=subQuestionText_Cell) at QuestionSet_TDICLV.pcf: line 64, column 26
    function value_66 () : java.lang.String {
      return gw.api.web.HtmlUtil.indent(supplementalText.Text, question.Indent)
    }
    
    property get supplementalText () : gw.api.productmodel.SupplementalText {
      return getIteratedValue(2) as gw.api.productmodel.SupplementalText
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionSet_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends QuestionSet_TDICLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_10 (def :  pcf.QuestionModalInput_BooleanCheckbox_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_12 (def :  pcf.QuestionModalInput_BooleanRadio) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_14 (def :  pcf.QuestionModalInput_BooleanRadio_BooleanOnChange_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_16 (def :  pcf.QuestionModalInput_BooleanRadio_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_18 (def :  pcf.QuestionModalInput_BooleanSelect) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_20 (def :  pcf.QuestionModalInput_BooleanSelect_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_22 (def :  pcf.QuestionModalInput_ChoiceRadio) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_24 (def :  pcf.QuestionModalInput_ChoiceRadio_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_26 (def :  pcf.QuestionModalInput_ChoiceSelect) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_28 (def :  pcf.QuestionModalInput_ChoiceSelect_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_30 (def :  pcf.QuestionModalInput_DateField) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_32 (def :  pcf.QuestionModalInput_DateField_NewGradDiscEffDate_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_34 (def :  pcf.QuestionModalInput_DateField_NewGradDiscExpDate_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_36 (def :  pcf.QuestionModalInput_DateField_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_38 (def :  pcf.QuestionModalInput_IntegerField) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_40 (def :  pcf.QuestionModalInput_IntegerFieldNonNegative) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_42 (def :  pcf.QuestionModalInput_IntegerFieldNonNegative_Nopost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_44 (def :  pcf.QuestionModalInput_IntegerField_Currency_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_46 (def :  pcf.QuestionModalInput_IntegerField_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_48 (def :  pcf.QuestionModalInput_IntegerField_NonEditCurrency_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_50 (def :  pcf.QuestionModalInput_IntegerField_TotalBPPLimit_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_52 (def :  pcf.QuestionModalInput_StringField) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_54 (def :  pcf.QuestionModalInput_StringField_BoldLabel_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_56 (def :  pcf.QuestionModalInput_StringField_DentalSchool_TDIC) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_58 (def :  pcf.QuestionModalInput_StringField_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_60 (def :  pcf.QuestionModalInput_StringTextArea) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_62 (def :  pcf.QuestionModalInput_StringTextArea_NoPost) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_onEnter_8 (def :  pcf.QuestionModalInput_BooleanCheckbox) : void {
      def.onEnter(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_11 (def :  pcf.QuestionModalInput_BooleanCheckbox_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_13 (def :  pcf.QuestionModalInput_BooleanRadio) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_15 (def :  pcf.QuestionModalInput_BooleanRadio_BooleanOnChange_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_17 (def :  pcf.QuestionModalInput_BooleanRadio_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_19 (def :  pcf.QuestionModalInput_BooleanSelect) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_21 (def :  pcf.QuestionModalInput_BooleanSelect_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_23 (def :  pcf.QuestionModalInput_ChoiceRadio) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_25 (def :  pcf.QuestionModalInput_ChoiceRadio_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_27 (def :  pcf.QuestionModalInput_ChoiceSelect) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_29 (def :  pcf.QuestionModalInput_ChoiceSelect_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_31 (def :  pcf.QuestionModalInput_DateField) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_33 (def :  pcf.QuestionModalInput_DateField_NewGradDiscEffDate_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_35 (def :  pcf.QuestionModalInput_DateField_NewGradDiscExpDate_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_37 (def :  pcf.QuestionModalInput_DateField_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_39 (def :  pcf.QuestionModalInput_IntegerField) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_41 (def :  pcf.QuestionModalInput_IntegerFieldNonNegative) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_43 (def :  pcf.QuestionModalInput_IntegerFieldNonNegative_Nopost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_45 (def :  pcf.QuestionModalInput_IntegerField_Currency_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_47 (def :  pcf.QuestionModalInput_IntegerField_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_49 (def :  pcf.QuestionModalInput_IntegerField_NonEditCurrency_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_51 (def :  pcf.QuestionModalInput_IntegerField_TotalBPPLimit_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_53 (def :  pcf.QuestionModalInput_StringField) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_55 (def :  pcf.QuestionModalInput_StringField_BoldLabel_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_57 (def :  pcf.QuestionModalInput_StringField_DentalSchool_TDIC) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_59 (def :  pcf.QuestionModalInput_StringField_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_61 (def :  pcf.QuestionModalInput_StringTextArea) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_63 (def :  pcf.QuestionModalInput_StringTextArea_NoPost) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'def' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function def_refreshVariables_9 (def :  pcf.QuestionModalInput_BooleanCheckbox) : void {
      def.refreshVariables(question, answerContainer, gw.pcf.QuestionSetUIHelper.wrapOnChangeBlock(answerContainer, question, tdic.web.pcf.helper.GLQuestionSetUIHelper.checkOnChangeBlock(answerContainer, question, onChangeBlock), CurrentLocation))
    }
    
    // 'mode' attribute on ModalCellRef at QuestionSet_TDICLV.pcf: line 49, column 88
    function mode_64 () : java.lang.Object {
      return question.getInputSetMode(answerContainer, onChangeBlock,openForEdit)
    }
    
    // 'sortBy' attribute on IteratorSort at QuestionSet_TDICLV.pcf: line 59, column 26
    function sortBy_65 (supplementalText :  gw.api.productmodel.SupplementalText) : java.lang.Object {
      return supplementalText.Priority
    }
    
    // 'value' attribute on TextCell (id=questionText_Cell) at QuestionSet_TDICLV.pcf: line 39, column 24
    function value_2 () : java.lang.String {
      return gw.api.web.HtmlUtil.indent(question.Text, question.Indent)
    }
    
    // 'value' attribute on RowIterator at QuestionSet_TDICLV.pcf: line 56, column 80
    function value_68 () : java.util.List<gw.api.productmodel.SupplementalText> {
      return question.SupplementalTexts
    }
    
    // 'visible' attribute on TextCell (id=questionText_Cell) at QuestionSet_TDICLV.pcf: line 39, column 24
    function visible_3 () : java.lang.Boolean {
      return !question.getBoldLabelVisibility_TDIC()
    }
    
    // 'visible' attribute on TextCell (id=questionTextBPP_Cell) at QuestionSet_TDICLV.pcf: line 45, column 24
    function visible_6 () : java.lang.Boolean {
      return question.getBoldLabelVisibility_TDIC()
    }
    
    property get question () : gw.api.productmodel.Question {
      return getIteratedValue(1) as gw.api.productmodel.Question
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionSet_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionSet_TDICLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at QuestionSet_TDICLV.pcf: line 23, column 46
    function initialValue_0 () : gw.api.productmodel.Question[] {
      return questionSet.getOrderedQuestions().where(\ q -> answerContainer.getAnswer(q) != null and q.isQuestionVisible(answerContainer))
    }
    
    // 'sortBy' attribute on IteratorSort at QuestionSet_TDICLV.pcf: line 33, column 24
    function sortBy_1 (question :  gw.api.productmodel.Question) : java.lang.Object {
      return question.Priority
    }
    
    // 'value' attribute on RowIterator at QuestionSet_TDICLV.pcf: line 30, column 50
    function value_69 () : gw.api.productmodel.Question[] {
      return questions
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get openForEdit () : Boolean {
      return getRequireValue("openForEdit", 0) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get questionSet () : gw.api.productmodel.QuestionSet {
      return getRequireValue("questionSet", 0) as gw.api.productmodel.QuestionSet
    }
    
    property set questionSet ($arg :  gw.api.productmodel.QuestionSet) {
      setRequireValue("questionSet", 0, $arg)
    }
    
    property get questions () : gw.api.productmodel.Question[] {
      return getVariableValue("questions", 0) as gw.api.productmodel.Question[]
    }
    
    property set questions ($arg :  gw.api.productmodel.Question[]) {
      setVariableValue("questions", 0, $arg)
    }
    
    
  }
  
  
}