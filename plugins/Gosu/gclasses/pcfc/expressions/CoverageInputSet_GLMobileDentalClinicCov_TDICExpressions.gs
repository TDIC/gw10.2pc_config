package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLMobileDentalClinicCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 49, column 62
    function allCheckedRowsAction_5 (CheckedValues :  entity.GLMobileDCSched_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 33, column 99
    function available_18 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 49, column 62
    function available_3 () : java.lang.Boolean {
      return glLine.Branch.Job typeis PolicyChange
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 33, column 99
    function label_19 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 33, column 99
    function setter_20 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 68, column 56
    function sortValue_6 (scheduledItem :  entity.GLMobileDCSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 73, column 57
    function sortValue_7 (scheduledItem :  entity.GLMobileDCSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 61, column 55
    function toCreateAndAdd_14 () : entity.GLMobileDCSched_TDIC {
      return glLine.createAndAddMobileDCSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 61, column 55
    function toRemove_15 (scheduledItem :  entity.GLMobileDCSched_TDIC) : void {
      glLine.toRemoveFromMobileDCSched_TDIC(scheduledItem) 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 61, column 55
    function value_16 () : entity.GLMobileDCSched_TDIC[] {
      return glLine.GLMobileDCSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 33, column 99
    function visible_17 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 42, column 178
    function visible_2 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLMobileDCSched_TDIC.hasMatch(\sched -> sched.BasedOn == null)
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 80, column 51
    function visible_23 () : java.lang.Boolean {
      return isMobileDentalClinicCovVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isMobileDentalClinicCovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLMobileDentalClinicCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status        == PolicyPeriodStatus.TC_QUOTED) or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLMobileDentalClinicCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    function setExpirationDate(mobileDCScheds : GLMobileDCSched_TDIC[]) {
      mobileDCScheds.each(\mobileDCSched -> {
        if(mobileDCSched.LTExpirationDate == null) {
          mobileDCSched.LTExpirationDate = mobileDCSched.Branch.EditEffectiveDate
        }
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 68, column 56
    function valueRoot_9 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 73, column 57
    function value_11 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLMobileDentalClinicCov_TDIC.pcf: line 68, column 56
    function value_8 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    property get scheduledItem () : entity.GLMobileDCSched_TDIC {
      return getIteratedValue(1) as entity.GLMobileDCSched_TDIC
    }
    
    
  }
  
  
}