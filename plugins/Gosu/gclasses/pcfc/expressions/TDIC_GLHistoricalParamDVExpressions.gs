package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/TDIC_GLHistoricalParamDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLHistoricalParamDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/TDIC_GLHistoricalParamDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_GLHistoricalParamDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=CurrentIterator) at TDIC_GLHistoricalParamDV.pcf: line 66, column 62
    function checkBoxVisible_49 () : java.lang.Boolean {
      return not historicalParam.IsLegacy
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 83, column 56
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 89, column 57
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=component_Cell) at TDIC_GLHistoricalParamDV.pcf: line 96, column 52
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.Component = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=TerritoryCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 103, column 56
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.TerritoryCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 110, column 52
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.ClassCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=specCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 118, column 57
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.SpecialityCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=discount_Cell) at TDIC_GLHistoricalParamDV.pcf: line 124, column 64
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      historicalParam.Discount = (__VALUE_TO_SET as typekey.GLHistoricalDiscount_TDIC)
    }
    
    // 'editable' attribute on Row at TDIC_GLHistoricalParamDV.pcf: line 72, column 62
    function editable_48 () : java.lang.Boolean {
      return policyPeriod.OverrideGLHistory_TDIC
    }
    
    // 'outputConversion' attribute on TextCell (id=specCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 118, column 57
    function outputConversion_39 (VALUE :  java.lang.String) : java.lang.String {
      return typekey.SpecialityCode_TDIC.get(VALUE).DisplayName
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 83, column 56
    function valueRoot_21 () : java.lang.Object {
      return historicalParam
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 83, column 56
    function value_19 () : java.util.Date {
      return historicalParam.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 89, column 57
    function value_23 () : java.util.Date {
      return historicalParam.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=component_Cell) at TDIC_GLHistoricalParamDV.pcf: line 96, column 52
    function value_27 () : java.lang.String {
      return historicalParam.Component
    }
    
    // 'value' attribute on TextCell (id=TerritoryCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 103, column 56
    function value_31 () : java.lang.String {
      return historicalParam.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 110, column 52
    function value_35 () : java.lang.String {
      return historicalParam.ClassCode
    }
    
    // 'value' attribute on TextCell (id=specCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 118, column 57
    function value_40 () : java.lang.String {
      return historicalParam.SpecialityCode
    }
    
    // 'value' attribute on TypeKeyCell (id=discount_Cell) at TDIC_GLHistoricalParamDV.pcf: line 124, column 64
    function value_44 () : typekey.GLHistoricalDiscount_TDIC {
      return historicalParam.Discount
    }
    
    property get historicalParam () : entity.GLHistoricalParameters_TDIC {
      return getIteratedValue(1) as entity.GLHistoricalParameters_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/submission/TDIC_GLHistoricalParamDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLHistoricalParamDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ManualOverride) at TDIC_GLHistoricalParamDV.pcf: line 29, column 67
    function action_1 () : void {
      toggleManualOverride()
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateAS400Data_TDIC) at TDIC_GLHistoricalParamDV.pcf: line 40, column 75
    function action_8 () : void {
      new tdic.pc.config.gl.historicaldata.GLHistoricalParamsBuilder(policyPeriod).updateLegacyHistory()
    }
    
    // 'available' attribute on ToolbarButton (id=UpdateAS400Data_TDIC) at TDIC_GLHistoricalParamDV.pcf: line 40, column 75
    function available_6 () : java.lang.Boolean {
      return historyHelper.canRefreshAS400History(policyPeriod)
    }
    
    // 'editable' attribute on RowIterator (id=CurrentIterator) at TDIC_GLHistoricalParamDV.pcf: line 66, column 62
    function editable_18 () : java.lang.Boolean {
      return policyPeriod.OverrideGLHistory_TDIC == Boolean.TRUE
    }
    
    // 'editable' attribute on ListViewInput (id=HistoricalParams_TDIC) at TDIC_GLHistoricalParamDV.pcf: line 20, column 27
    function editable_54 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'icon' attribute on ToolbarButton (id=ManualOverride) at TDIC_GLHistoricalParamDV.pcf: line 29, column 67
    function icon_3 () : java.lang.String {
      return policyPeriod.OverrideGLHistory_TDIC == true ? "warning_tdic.gif" : null
    }
    
    // 'label' attribute on ToolbarButton (id=ManualOverride) at TDIC_GLHistoricalParamDV.pcf: line 29, column 67
    function label_2 () : java.lang.Object {
      return getOverrideLabel()
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLHistoricalParamDV.pcf: line 70, column 30
    function sortBy_10 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 83, column 56
    function sortValue_11 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TDIC_GLHistoricalParamDV.pcf: line 89, column 57
    function sortValue_12 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=component_Cell) at TDIC_GLHistoricalParamDV.pcf: line 96, column 52
    function sortValue_13 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.Component
    }
    
    // 'value' attribute on TextCell (id=TerritoryCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 103, column 56
    function sortValue_14 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 110, column 52
    function sortValue_15 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.ClassCode
    }
    
    // 'value' attribute on TextCell (id=specCode_Cell) at TDIC_GLHistoricalParamDV.pcf: line 118, column 57
    function sortValue_16 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.SpecialityCode
    }
    
    // 'value' attribute on TypeKeyCell (id=discount_Cell) at TDIC_GLHistoricalParamDV.pcf: line 124, column 64
    function sortValue_17 (historicalParam :  entity.GLHistoricalParameters_TDIC) : java.lang.Object {
      return historicalParam.Discount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=CurrentIterator) at TDIC_GLHistoricalParamDV.pcf: line 66, column 62
    function toCreateAndAdd_50 () : entity.GLHistoricalParameters_TDIC {
      var hp = new GLHistoricalParameters_TDIC(policyPeriod); policyPeriod.addToGLHistoricalParameters_TDIC(hp); return hp;
    }
    
    // 'toRemove' attribute on RowIterator (id=CurrentIterator) at TDIC_GLHistoricalParamDV.pcf: line 66, column 62
    function toRemove_51 (historicalParam :  entity.GLHistoricalParameters_TDIC) : void {
      policyPeriod.removeFromGLHistoricalParameters_TDIC(historicalParam)
    }
    
    // 'value' attribute on RowIterator (id=CurrentIterator) at TDIC_GLHistoricalParamDV.pcf: line 66, column 62
    function value_52 () : entity.GLHistoricalParameters_TDIC[] {
      return tdic.pc.config.gl.historicaldata.GLHistoryHelper.getHistoryFor(policyPeriod)
    }
    
    // 'visible' attribute on ToolbarButton (id=ManualOverride) at TDIC_GLHistoricalParamDV.pcf: line 29, column 67
    function visible_0 () : java.lang.Boolean {
      return historyHelper.canEditHistory(policyPeriod)
    }
    
    // 'addVisible' attribute on IteratorButtons at TDIC_GLHistoricalParamDV.pcf: line 33, column 127
    function visible_4 () : java.lang.Boolean {
      return policyPeriod.OverrideGLHistory_TDIC == Boolean.TRUE and perm.System.addhistoricalparameters_tdic
    }
    
    // 'visible' attribute on Toolbar (id=TDIC_HistoricalParamLV_tb) at TDIC_GLHistoricalParamDV.pcf: line 22, column 72
    function visible_9 () : java.lang.Boolean {
      return policyPeriod.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    property get historyHelper () : tdic.pc.config.gl.historicaldata.GLHistoryHelper {
      return getVariableValue("historyHelper", 0) as tdic.pc.config.gl.historicaldata.GLHistoryHelper
    }
    
    property set historyHelper ($arg :  tdic.pc.config.gl.historicaldata.GLHistoryHelper) {
      setVariableValue("historyHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function toggleManualOverride() {
      if(policyPeriod.OverrideGLHistory_TDIC != true) {
        policyPeriod.OverrideGLHistory_TDIC = true
      } else {
        policyPeriod.OverrideGLHistory_TDIC = false
      }
    }
    function getOverrideLabel() : String {
      if(policyPeriod.OverrideGLHistory_TDIC == true) {
        return DisplayKey.get("TDIC.Web.Policy.RiskAnalysis.GLHistory.CancelOverride")
      } 
      return DisplayKey.get("TDIC.Web.Policy.RiskAnalysis.GLHistory.OverrideHistory")
    }
    
    
  }
  
  
}