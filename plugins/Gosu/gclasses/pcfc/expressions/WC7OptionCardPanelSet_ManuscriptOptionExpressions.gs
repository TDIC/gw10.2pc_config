package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.ManuscriptOption.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7OptionCardPanelSet_ManuscriptOptionExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.ManuscriptOption.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 33, column 54
    function checkBoxVisible_7 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on TextCell (id=Premium_Cell) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 40, column 51
    function valueRoot_3 () : java.lang.Object {
      return wc7ManuscriptOption
    }
    
    // 'value' attribute on TextCell (id=Premium_Cell) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 40, column 51
    function value_2 () : java.math.BigDecimal {
      return wc7ManuscriptOption.Premium
    }
    
    // 'value' attribute on TextCell (id=ManuscriptDescription_Cell) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 46, column 47
    function value_5 () : java.lang.String {
      return (wc7ManuscriptOption.Description.length > 100) ? wc7ManuscriptOption.Description.substring(0, 100) : wc7ManuscriptOption.Description
    }
    
    property get wc7ManuscriptOption () : entity.WC7ManuscriptOption {
      return getIteratedValue(2) as entity.WC7ManuscriptOption
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.ManuscriptOption.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends WC7OptionCardPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewPanel (id=WC7ManuscriptOptionsLV) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 25, column 26
    function available_11 () : java.lang.Boolean {
      return selectedWC7ManuscriptOption != null
    }
    
    // 'value' attribute on TextInput (id=Premium_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 63, column 53
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedWC7ManuscriptOption.Premium = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextAreaInput (id=ManuscriptDescription_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 71, column 68
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedWC7ManuscriptOption.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=Premium_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 63, column 53
    function editable_12 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on TextCell (id=Premium_Cell) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 40, column 51
    function sortValue_0 (wc7ManuscriptOption :  entity.WC7ManuscriptOption) : java.lang.Object {
      return wc7ManuscriptOption.Premium
    }
    
    // 'value' attribute on TextCell (id=ManuscriptDescription_Cell) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 46, column 47
    function sortValue_1 (wc7ManuscriptOption :  entity.WC7ManuscriptOption) : java.lang.Object {
      return (wc7ManuscriptOption.Description.length > 100) ? wc7ManuscriptOption.Description.substring(0, 100) : wc7ManuscriptOption.Description
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 33, column 54
    function toCreateAndAdd_8 () : entity.WC7ManuscriptOption {
      return wcLine.createAndAddWC7ManuscriptOption()
    }
    
    // 'toRemove' attribute on RowIterator at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 33, column 54
    function toRemove_9 (wc7ManuscriptOption :  entity.WC7ManuscriptOption) : void {
      wcLine.removeFromWC7ManuscriptOptions(wc7ManuscriptOption)
    }
    
    // 'value' attribute on TextInput (id=Premium_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 63, column 53
    function valueRoot_15 () : java.lang.Object {
      return selectedWC7ManuscriptOption
    }
    
    // 'value' attribute on RowIterator at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 33, column 54
    function value_10 () : entity.WC7ManuscriptOption[] {
      return wcLine.WC7ManuscriptOptions
    }
    
    // 'value' attribute on TextInput (id=Premium_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 63, column 53
    function value_13 () : java.math.BigDecimal {
      return selectedWC7ManuscriptOption.Premium
    }
    
    // 'value' attribute on TextAreaInput (id=ManuscriptDescription_Input) at WC7OptionCardPanelSet.ManuscriptOption.pcf: line 71, column 68
    function value_19 () : java.lang.String {
      return selectedWC7ManuscriptOption.Description
    }
    
    property get selectedWC7ManuscriptOption () : WC7ManuscriptOption {
      return getCurrentSelection(1) as WC7ManuscriptOption
    }
    
    property set selectedWC7ManuscriptOption ($arg :  WC7ManuscriptOption) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.ManuscriptOption.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7OptionCardPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getRequireValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    
  }
  
  
}