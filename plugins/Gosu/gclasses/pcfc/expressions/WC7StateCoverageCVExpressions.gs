package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses pcf.api.Wizard
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateCoverageCVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CardViewPanelExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageCV.pcf: line 144, column 96
    function def_onEnter_35 (def :  pcf.WC7PolicyLinePerStateConfigDV) : void {
      def.onEnter(wcLine, selectedJurisdiction,openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7StateCoverageCV.pcf: line 152, column 70
    function def_onEnter_37 (def :  pcf.WC7ClassesInputSet_default) : void {
      def.onEnter(selectedJurisdiction, wcLine, stateConfig)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageCV.pcf: line 402, column 115
    function def_onEnter_41 (def :  pcf.WC7JurisdictionConditionsDV) : void {
      def.onEnter(selectedJurisdiction, jurisdictionCondCategories, openForEdit)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageCV.pcf: line 144, column 96
    function def_refreshVariables_36 (def :  pcf.WC7PolicyLinePerStateConfigDV) : void {
      def.refreshVariables(wcLine, selectedJurisdiction,openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7StateCoverageCV.pcf: line 152, column 70
    function def_refreshVariables_38 (def :  pcf.WC7ClassesInputSet_default) : void {
      def.refreshVariables(selectedJurisdiction, wcLine, stateConfig)
    }
    
    // 'def' attribute on PanelRef at WC7StateCoverageCV.pcf: line 402, column 115
    function def_refreshVariables_42 (def :  pcf.WC7JurisdictionConditionsDV) : void {
      def.refreshVariables(selectedJurisdiction, jurisdictionCondCategories, openForEdit)
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageCV.pcf: line 133, column 32
    function initialValue_33 () : String[] {
      return {"WC7WorkCompStateCategoryCond"}
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageCV.pcf: line 138, column 62
    function initialValue_34 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(selectedJurisdiction.Jurisdiction)
    }
    
    // 'mode' attribute on InputSetRef at WC7StateCoverageCV.pcf: line 152, column 70
    function mode_39 () : java.lang.Object {
      return selectedJurisdiction.Jurisdiction.Code
    }
    
    // 'onSelect' attribute on Card (id=GeneralInfoCard) at WC7StateCoverageCV.pcf: line 142, column 106
    function onSelect_40 () : void {
      selectedJurisdiction.setTypeKeyModifierDefaultValue(selectedJurisdiction)
    }
    
    // 'onSelect' attribute on Card (id=ConditionsCard) at WC7StateCoverageCV.pcf: line 400, column 118
    function onSelect_44 () : void {
      stateConfig.syncSupplementaryDisease(jobWizardHelper, wcLine, selectedJurisdiction)
    }
    
    // 'visible' attribute on Card (id=ConditionsCard) at WC7StateCoverageCV.pcf: line 400, column 118
    function visible_43 () : java.lang.Boolean {
      return selectedJurisdiction?.hasAvailableConditionPatterns(jurisdictionCondCategories, openForEdit)
    }
    
    property get jurisdictionCondCategories () : String[] {
      return getVariableValue("jurisdictionCondCategories", 2) as String[]
    }
    
    property set jurisdictionCondCategories ($arg :  String[]) {
      setVariableValue("jurisdictionCondCategories", 2, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 2) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7StateCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=addJurisdiction) at WC7StateCoverageCV.pcf: line 64, column 80
    function label_12 () : java.lang.Object {
      return locationState
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=addJurisdiction) at WC7StateCoverageCV.pcf: line 64, column 80
    function toCreateAndAdd_13 (CheckedValues :  Object[]) : java.lang.Object {
      return wcLine.addJurisdiction(locationState)
    }
    
    property get locationState () : typekey.Jurisdiction {
      return getIteratedValue(1) as typekey.Jurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at WC7StateCoverageCV.pcf: line 106, column 52
    function checkBoxVisible_28 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'condition' attribute on ToolbarFlag at WC7StateCoverageCV.pcf: line 112, column 35
    function condition_21 () : java.lang.Boolean {
      return covJuris.CanRemove
    }
    
    // 'outputConversion' attribute on TextCell (id=RiskID_Cell) at WC7StateCoverageCV.pcf: line 125, column 52
    function outputConversion_25 (VALUE :  entity.OfficialID[]) : java.lang.String {
      return outputConverterForOfficialIDs(VALUE)
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7StateCoverageCV.pcf: line 118, column 53
    function valueRoot_23 () : java.lang.Object {
      return covJuris
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7StateCoverageCV.pcf: line 118, column 53
    function value_22 () : typekey.Jurisdiction {
      return covJuris.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=RiskID_Cell) at WC7StateCoverageCV.pcf: line 125, column 52
    function value_26 () : entity.OfficialID[] {
      return getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(covJuris)
    }
    
    property get covJuris () : entity.WC7Jurisdiction {
      return getIteratedValue(2) as entity.WC7Jurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7StateCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7StateCoverageCV.pcf: line 44, column 44
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      officialId.OfficialIDValue = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7StateCoverageCV.pcf: line 44, column 44
    function label_2 () : java.lang.Object {
      return officialId.OfficialIDInsuredAndType
    }
    
    // 'validationExpression' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7StateCoverageCV.pcf: line 44, column 44
    function validationExpression_1 () : java.lang.Object {
      return officialId.validateValue()
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7StateCoverageCV.pcf: line 44, column 44
    function valueRoot_5 () : java.lang.Object {
      return officialId
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7StateCoverageCV.pcf: line 44, column 44
    function value_3 () : java.lang.String {
      return officialId.OfficialIDValue
    }
    
    property get officialId () : entity.OfficialID {
      return getIteratedValue(1) as entity.OfficialID
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends WC7StateCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewPanel (id=PolicyLinePerStateConfig_LV) at WC7StateCoverageCV.pcf: line 98, column 28
    function available_32 () : java.lang.Boolean {
      return selectedJurisdiction != null
    }
    
    // 'sortBy' attribute on IteratorSort at WC7StateCoverageCV.pcf: line 109, column 32
    function sortBy_18 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WC7StateCoverageCV.pcf: line 118, column 53
    function sortValue_19 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=RiskID_Cell) at WC7StateCoverageCV.pcf: line 125, column 52
    function sortValue_20 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(covJuris)
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at WC7StateCoverageCV.pcf: line 106, column 52
    function toCreateAndAdd_29 () : entity.WC7Jurisdiction {
      return dummyCreateAndAdd()
    }
    
    // 'toRemove' attribute on RowIterator at WC7StateCoverageCV.pcf: line 106, column 52
    function toRemove_30 (covJuris :  entity.WC7Jurisdiction) : void {
      wcLine.removeJurisdiction( covJuris ); gw.api.util.LocationUtil.addRequestScopedInfoMessage(DisplayKey.get("Web.Policy.WC.RemoveJurisdictionWarning")) 
    }
    
    // 'value' attribute on RowIterator at WC7StateCoverageCV.pcf: line 106, column 52
    function value_31 () : entity.WC7Jurisdiction[] {
      return wcLine.WC7Jurisdictions
    }
    
    property get selectedJurisdiction () : WC7Jurisdiction {
      return getCurrentSelection(1) as WC7Jurisdiction
    }
    
    property set selectedJurisdiction ($arg :  WC7Jurisdiction) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateCoverageCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=SplitBases) at WC7StateCoverageCV.pcf: line 78, column 75
    function action_15 () : void {
      updateAllBasis()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SplitPeriod) at WC7StateCoverageCV.pcf: line 84, column 76
    function allCheckedRowsAction_16 (CheckedValues :  entity.WC7Jurisdiction[], CheckedValuesErrors :  java.util.Map) : void {
      WC7SplitPeriodPopup.push( wcLine.Branch.WC7Line, CheckedValues )
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=RemoveSplitPeriod) at WC7StateCoverageCV.pcf: line 90, column 83
    function allCheckedRowsAction_17 (CheckedValues :  entity.WC7Jurisdiction[], CheckedValuesErrors :  java.util.Map) : void {
      WC7RemoveSplitPeriodPopup.push( wcLine.Branch.WC7Line, CheckedValues )
    }
    
    // 'available' attribute on DetailViewPanel (id=NamedInsuredOfficialIDDV) at WC7StateCoverageCV.pcf: line 27, column 56
    function available_10 () : java.lang.Boolean {
      return namedInsuredOfficialIDs.Count > 0
    }
    
    // 'initialValue' attribute on Variable at WC7StateCoverageCV.pcf: line 20, column 28
    function initialValue_0 () : OfficialID[] {
      return wcLine.InterstateNamedInsuredOfficialIDs
    }
    
    // 'value' attribute on AddMenuItemIterator at WC7StateCoverageCV.pcf: line 59, column 50
    function value_14 () : typekey.Jurisdiction[] {
      return JurisdictionsThatCanBeAdded
    }
    
    // 'value' attribute on InputIterator (id=officialIDs) at WC7StateCoverageCV.pcf: line 36, column 45
    function value_9 () : entity.OfficialID[] {
      return namedInsuredOfficialIDs
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get namedInsuredOfficialIDs () : OfficialID[] {
      return getVariableValue("namedInsuredOfficialIDs", 0) as OfficialID[]
    }
    
    property set namedInsuredOfficialIDs ($arg :  OfficialID[]) {
      setVariableValue("namedInsuredOfficialIDs", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getRequireValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    
    function dummyCreateAndAdd() : WC7Jurisdiction {
      return null
    }
                                            
    function updateAllBasis(){
      if((CurrentLocation as Wizard).saveDraft()){
        wcLine.updateWCExposuresAndModifiers()
      }
    }
          
    property get JurisdictionsThatCanBeAdded(): Jurisdiction[] {
      var existingJurisdictions = wcLine.WC7Jurisdictions.map(\j -> j.Jurisdiction).toSet()
      var possibleJurisdicitons = wcLine.Branch.LocationStates.toSet()
      possibleJurisdicitons.removeAll(existingJurisdictions)
      return possibleJurisdicitons.toTypedArray()
    }
    
    function getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(covJuris : WC7Jurisdiction) : entity.OfficialID[] {
      return wcLine.Branch.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.OfficialIDs
            .where(\ officialID ->
              officialID.State == covJuris.Jurisdiction)
    }
    
    function outputConverterForOfficialIDs(VALUE : OfficialID[]) : String {
      var str = ""
      var first = true
      for (var Item in VALUE) {
        var idValue = Item.getOfficialIDValue()
        if(idValue != null) {
          if(!first) {
            str = str + ", "
          }
          first = false
          str = str + idValue
        }
      }
      return str
    }
    
    
  }
  
  
}