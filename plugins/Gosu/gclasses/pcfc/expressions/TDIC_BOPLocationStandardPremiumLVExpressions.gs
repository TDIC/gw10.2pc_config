package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationStandardPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPLocationStandardPremiumLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationStandardPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BOPLocationStandardPremiumLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPLocationStandardPremiumLV.pcf: line 29, column 40
    function valueRoot_3 () : java.lang.Object {
      return aggCost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPLocationStandardPremiumLV.pcf: line 29, column 40
    function value_2 () : java.lang.String {
      return aggCost.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TDIC_BOPLocationStandardPremiumLV.pcf: line 50, column 25
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return aggCost.Total
    }
    
    property get aggCost () : gw.api.ui.BOPCostWrapper_TDIC {
      return getIteratedValue(1) as gw.api.ui.BOPCostWrapper_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationStandardPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPLocationStandardPremiumLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_BOPLocationStandardPremiumLV.pcf: line 50, column 25
    function sumValueRoot_1 (aggCost :  gw.api.ui.BOPCostWrapper_TDIC) : java.lang.Object {
      return aggCost
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_BOPLocationStandardPremiumLV.pcf: line 50, column 25
    function sumValue_0 (aggCost :  gw.api.ui.BOPCostWrapper_TDIC) : java.lang.Object {
      return aggCost.Total
    }
    
    // 'value' attribute on RowIterator at TDIC_BOPLocationStandardPremiumLV.pcf: line 19, column 51
    function value_8 () : gw.api.ui.BOPCostWrapper_TDIC[] {
      return costWrapper
    }
    
    property get costWrapper () : gw.api.ui.BOPCostWrapper_TDIC[] {
      return getRequireValue("costWrapper", 0) as gw.api.ui.BOPCostWrapper_TDIC[]
    }
    
    property set costWrapper ($arg :  gw.api.ui.BOPCostWrapper_TDIC[]) {
      setRequireValue("costWrapper", 0, $arg)
    }
    
    
  }
  
  
}