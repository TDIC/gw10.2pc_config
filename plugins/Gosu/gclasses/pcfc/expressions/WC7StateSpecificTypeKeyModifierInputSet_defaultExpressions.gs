package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateSpecificTypeKeyModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateSpecificTypeKeyModifierInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateSpecificTypeKeyModifierInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateSpecificTypeKeyModifierInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function available_2 () : java.lang.Boolean {
      return uiHelper.typekeyModifierAvailable(modifier, policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      modifier.TypeKeyModifier = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'disablePostOnEnter' attribute on PostOnChange at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 28, column 85
    function disablePostOnEnter_1 () : java.lang.Boolean {
      return uiHelper.typekeyModifierDisablePostOnChange(modifier)
    }
    
    // 'initialValue' attribute on Variable at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 17, column 51
    function initialValue_0 () : gw.pcf.WC7ModifiersInputSetUIHelper {
      return new gw.pcf.WC7ModifiersInputSetUIHelper()
    }
    
    // 'label' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function label_4 () : java.lang.Object {
      return modifier.Pattern.Name
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function valueRange_8 () : java.lang.Object {
      return uiHelper.filterTypekeyModifier(modifier)
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function valueRoot_7 () : java.lang.Object {
      return modifier
    }
    
    // 'value' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function value_5 () : java.lang.String {
      return modifier.TypeKeyModifier
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function verifyValueRange_10 () : void {
      var __valueRangeArg = uiHelper.filterTypekeyModifier(modifier)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=ModifierInput_TypeKey_Input) at WC7StateSpecificTypeKeyModifierInputSet.default.pcf: line 26, column 66
    function visible_3 () : java.lang.Boolean {
      return modifier.DataType == ModifierDataType.TC_TYPEKEY
    }
    
    property get modifier () : entity.Modifier {
      return getRequireValue("modifier", 0) as entity.Modifier
    }
    
    property set modifier ($arg :  entity.Modifier) {
      setRequireValue("modifier", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get uiHelper () : gw.pcf.WC7ModifiersInputSetUIHelper {
      return getVariableValue("uiHelper", 0) as gw.pcf.WC7ModifiersInputSetUIHelper
    }
    
    property set uiHelper ($arg :  gw.pcf.WC7ModifiersInputSetUIHelper) {
      setVariableValue("uiHelper", 0, $arg)
    }
    
    
  }
  
  
}