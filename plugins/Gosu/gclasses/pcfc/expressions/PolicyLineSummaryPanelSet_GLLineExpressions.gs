package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineSummaryPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyLineSummaryPanelSet_GLLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineSummaryPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyLineSummaryPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'onChange' attribute on PostOnChange at PolicyLineSummaryPanelSet.GLLine.pcf: line 58, column 74
    function onChange_14 () : void {
      exposure.setGLExposureDescription_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at PolicyLineSummaryPanelSet.GLLine.pcf: line 73, column 187
    function onChange_24 () : void {
      tdic.web.pcf.helper.GLExposureUnitsHelper.setTerritoryCodeByComponent(exposure.GLTerritory_TDIC.StateCode, exposure.GLTerritory_TDIC.Component, exposure)
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function valueRange_10 () : java.lang.Object {
      return getSpecialityCode()
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function valueRange_17 () : java.lang.Object {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(glLine.Branch,exposure)
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function valueRange_27 () : java.lang.Object {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(glLine.Branch.BaseState.Code)
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 79, column 69
    function valueRoot_32 () : java.lang.Object {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function valueRoot_9 () : java.lang.Object {
      return exposure
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function value_15 () : typekey.ClassCode_TDIC {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=SpecialityCodeClassCodeDescription_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 63, column 54
    function value_21 () : java.lang.String {
      return exposure.Description_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function value_25 () : entity.GLTerritory_TDIC {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 79, column 69
    function value_31 () : java.lang.String {
      return exposure.GLTerritory_TDIC?.TerritoryCode
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function value_8 () : typekey.SpecialityCode_TDIC {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.SpecialityCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function verifyValueRangeIsAllowedType_18 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function verifyValueRangeIsAllowedType_18 ($$arg :  typekey.ClassCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function verifyValueRangeIsAllowedType_28 ($$arg :  entity.GLTerritory_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function verifyValueRangeIsAllowedType_28 ($$arg :  gw.api.database.IQueryBeanResult<entity.GLTerritory_TDIC>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function verifyValueRange_12 () : void {
      var __valueRangeArg = getSpecialityCode()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function verifyValueRange_19 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(glLine.Branch,exposure)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_18(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function verifyValueRange_29 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(glLine.Branch.BaseState.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    property get exposure () : entity.GLExposure {
      return getIteratedValue(1) as entity.GLExposure
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineSummaryPanelSet.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyLineSummaryPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at PolicyLineSummaryPanelSet.GLLine.pcf: line 93, column 29
    function def_onEnter_36 (def :  pcf.CoverageSummaryLV) : void {
      def.onEnter(glLine.GLLineCoveragesAndScheduleCovsWithNoCovTerms.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=ExclusionSummaryLV) at PolicyLineSummaryPanelSet.GLLine.pcf: line 103, column 57
    function def_onEnter_40 (def :  pcf.CoverageSummaryLV) : void {
      def.onEnter(glLine.GLLineExclusions.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=ConditionSummaryLV) at PolicyLineSummaryPanelSet.GLLine.pcf: line 113, column 57
    function def_onEnter_44 (def :  pcf.CoverageSummaryLV) : void {
      def.onEnter(glLine.GLLineConditions.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=GLScheduleCoveragesLVI) at PolicyLineSummaryPanelSet.GLLine.pcf: line 123, column 67
    function def_onEnter_48 (def :  pcf.ScheduleCoverageSummaryLV) : void {
      def.onEnter(glLine.GLScheduleCovsWithCovTerms.toList())
    }
    
    // 'def' attribute on ListViewInput at PolicyLineSummaryPanelSet.GLLine.pcf: line 93, column 29
    function def_refreshVariables_37 (def :  pcf.CoverageSummaryLV) : void {
      def.refreshVariables(glLine.GLLineCoveragesAndScheduleCovsWithNoCovTerms.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=ExclusionSummaryLV) at PolicyLineSummaryPanelSet.GLLine.pcf: line 103, column 57
    function def_refreshVariables_41 (def :  pcf.CoverageSummaryLV) : void {
      def.refreshVariables(glLine.GLLineExclusions.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=ConditionSummaryLV) at PolicyLineSummaryPanelSet.GLLine.pcf: line 113, column 57
    function def_refreshVariables_45 (def :  pcf.CoverageSummaryLV) : void {
      def.refreshVariables(glLine.GLLineConditions.toList(), glLine)
    }
    
    // 'def' attribute on ListViewInput (id=GLScheduleCoveragesLVI) at PolicyLineSummaryPanelSet.GLLine.pcf: line 123, column 67
    function def_refreshVariables_49 (def :  pcf.ScheduleCoverageSummaryLV) : void {
      def.refreshVariables(glLine.GLScheduleCovsWithCovTerms.toList())
    }
    
    // 'initialValue' attribute on Variable at PolicyLineSummaryPanelSet.GLLine.pcf: line 17, column 35
    function initialValue_0 () : productmodel.GLLine {
      return line as GLLine
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.GLLine.pcf: line 38, column 32
    function sortBy_1 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.GLLine.pcf: line 41, column 32
    function sortBy_2 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 49, column 60
    function sortValue_3 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ClassCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 56, column 54
    function sortValue_4 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=SpecialityCodeClassCodeDescription_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 63, column 54
    function sortValue_5 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.Description_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 71, column 55
    function sortValue_6 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at PolicyLineSummaryPanelSet.GLLine.pcf: line 79, column 69
    function sortValue_7 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.GLTerritory_TDIC?.TerritoryCode
    }
    
    // 'value' attribute on RowIterator at PolicyLineSummaryPanelSet.GLLine.pcf: line 35, column 47
    function value_34 () : entity.GLExposure[] {
      return (line as GeneralLiabilityLine).Exposures
    }
    
    // 'visible' attribute on PanelRef at PolicyLineSummaryPanelSet.GLLine.pcf: line 24, column 105
    function visible_35 () : java.lang.Boolean {
      return glLine.Branch.Offering.CodeIdentifier == "PLCyberLiab_TDIC"? false : true
    }
    
    // 'visible' attribute on Label at PolicyLineSummaryPanelSet.GLLine.pcf: line 98, column 58
    function visible_38 () : java.lang.Boolean {
      return not glLine.GLLineExclusions.IsEmpty
    }
    
    // 'visible' attribute on Label at PolicyLineSummaryPanelSet.GLLine.pcf: line 108, column 58
    function visible_42 () : java.lang.Boolean {
      return not glLine.GLLineConditions.IsEmpty
    }
    
    // 'visible' attribute on Label at PolicyLineSummaryPanelSet.GLLine.pcf: line 118, column 68
    function visible_46 () : java.lang.Boolean {
      return not glLine.GLScheduleCovsWithCovTerms.IsEmpty
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get line () : PolicyLine {
      return getRequireValue("line", 0) as PolicyLine
    }
    
    property set line ($arg :  PolicyLine) {
      setRequireValue("line", 0, $arg)
    }
    
    function getSpecialityCode() : List<SpecialityCode_TDIC> {
          var typekeys : List<SpecialityCode_TDIC> = {}
          if (glLine.Branch.BaseState == Jurisdiction.TC_PA) {
            return SpecialityCode_TDIC.getTypeKeys(false)
          } else {
            typekeys.add(SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_10ORALSURGERY_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_15ENDODONTICS_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_20ORTHODONTICS_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_30PEDIATRICDENTISTRY_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_40PERIODONTICS_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_50PROSTHODONTICS_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_60ORALPATHOLOGY_TDIC)
            typekeys.add(SpecialityCode_TDIC.TC_90DENTALANESTHESIOLOGY_TDIC)
    
          }
          return typekeys
        }
    
    
  }
  
  
}