package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPAdditionalCoveragesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPAdditionalCoveragesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on DetailViewPanel (id=BOPAdditionalCoveragesDV) at BOPAdditionalCoveragesDV.pcf: line 7, column 35
    function editable_113 () : java.lang.Boolean {
      return bopLine?.Branch?.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at BOPAdditionalCoveragesDV.pcf: line 23, column 36
    function initialValue_0 () : productmodel.BOPLine {
      return policyline as BOPLine
    }
    
    // 'initialValue' attribute on Variable at BOPAdditionalCoveragesDV.pcf: line 27, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return bopLine.Pattern.getCoverageCategoryByCodeIdentifier("BOPBuildingOtherCat")
    }
    
    // 'initialValue' attribute on Variable at BOPAdditionalCoveragesDV.pcf: line 32, column 53
    function initialValue_2 () : gw.api.productmodel.CoveragePattern[] {
      return coverable != null ? bopPropertyRequiredCat.coveragePatternsForEntity(BOPBuilding).whereSelectedOrAvailable(coverable, CurrentLocation.InEditMode) : null
    }
    
    // 'sortBy' attribute on IteratorSort at BOPAdditionalCoveragesDV.pcf: line 50, column 26
    function sortBy_3 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=BOPPropertyRequiredCatIterator) at BOPAdditionalCoveragesDV.pcf: line 47, column 59
    function value_112 () : gw.api.productmodel.CoveragePattern[] {
      return bopPropertyRequiredCatCoveragePatterns
    }
    
    property get bopLine () : productmodel.BOPLine {
      return getVariableValue("bopLine", 0) as productmodel.BOPLine
    }
    
    property set bopLine ($arg :  productmodel.BOPLine) {
      setVariableValue("bopLine", 0, $arg)
    }
    
    property get bopPropertyRequiredCat () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("bopPropertyRequiredCat", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set bopPropertyRequiredCat ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("bopPropertyRequiredCat", 0, $arg)
    }
    
    property get bopPropertyRequiredCatCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("bopPropertyRequiredCatCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set bopPropertyRequiredCatCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("bopPropertyRequiredCatCoveragePatterns", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : Boolean {
      return getRequireValue("openForEdit", 0) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyline () : PolicyLine {
      return getRequireValue("policyline", 0) as PolicyLine
    }
    
    property set policyline ($arg :  PolicyLine) {
      setRequireValue("policyline", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getRequireValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    function chkCoverageVisibility_TDIC(cov: gw.api.productmodel.CoveragePattern):Boolean{
      if({"BOPNewBuildingCov_TDIC","BOPNewBPP_TDIC","BOPEquipBreakCov_TDIC","BOPLossOutdoorTSP_TDIC","BOPBuilDebrisRemoval_TDIC","BOPPollCleanRemoval_TDIC","BOPFireExtRecharge_TDIC",
          "BOPFireDepSerCharge_TDIC","BOPBPPDebrisRemoval_TDIC","BOPOrdLaw_TDIC","BOPArsonReward_TDIC","BOPBPPOffPremises_TDIC","BOPPersonalEffects_TDIC","BOPAddDebrisRemoval_TDIC",
          "BOPOutdoorProperty_TDIC","BOPRentalIncome_TDIC", "BOPEncCovFineCond_TDIC", "BOPEncCovMoneySCond_TDIC"}.contains(cov.CodeIdentifier)){
        return false
      }
      if(quoteType == QuoteType.TC_QUICK && cov.CodeIdentifier == "BOPManuscriptEndorsement_TDIC") return false
     return true 
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPAdditionalCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BOPAdditionalCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_109 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_13 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_19 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_65 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_67 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, coverable, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function mode_111 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    // 'visible' attribute on InputSetRef at BOPAdditionalCoveragesDV.pcf: line 54, column 66
    function visible_4 () : java.lang.Boolean {
      return chkCoverageVisibility_TDIC(coveragePattern)
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  
}