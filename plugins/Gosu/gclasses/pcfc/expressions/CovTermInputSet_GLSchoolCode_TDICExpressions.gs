package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.GLSchoolCode_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermInputSet_GLSchoolCode_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.GLSchoolCode_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function action_1 () : void {
      GLSchoolCodeSearch_TDICPopup.push(term)
    }
    
    // 'pickLocation' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function action_dest_2 () : pcf.api.Destination {
      return pcf.GLSchoolCodeSearch_TDICPopup.createDestination(term)
    }
    
    // 'value' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      stringCovTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 21, column 51
    function initialValue_0 () : gw.api.domain.covterm.StringCovTerm {
      return term as gw.api.domain.covterm.StringCovTerm
    }
    
    // 'label' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function label_3 () : java.lang.Object {
      return stringCovTerm.Pattern.DisplayName
    }
    
    // 'required' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function required_4 () : java.lang.Boolean {
      return stringCovTerm.Pattern.Required
    }
    
    // 'value' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function valueRoot_7 () : java.lang.Object {
      return stringCovTerm
    }
    
    // 'value' attribute on PickerInput (id=GLFTPGGSchoolCode_Input) at CovTermInputSet.GLSchoolCode_TDIC.pcf: line 29, column 37
    function value_5 () : java.lang.String {
      return stringCovTerm.Value
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get stringCovTerm () : gw.api.domain.covterm.StringCovTerm {
      return getVariableValue("stringCovTerm", 0) as gw.api.domain.covterm.StringCovTerm
    }
    
    property set stringCovTerm ($arg :  gw.api.domain.covterm.StringCovTerm) {
      setVariableValue("stringCovTerm", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getRequireValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setRequireValue("term", 0, $arg)
    }
    
    
  }
  
  
}