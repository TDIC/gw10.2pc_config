package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/new/CreateAccountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateAccountDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/new/CreateAccountDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateAccountDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_46 () : void {
      IndustryCodeSearchPopup.push(typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_dest_47 () : pcf.api.Destination {
      return pcf.IndustryCodeSearchPopup.createDestination(typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 40, column 128
    function def_onEnter_13 (def :  pcf.LinkedAddressInputSet) : void {
      def.onEnter(selectedAddress, account.AccountHolderContact, null, account, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 43, column 59
    function def_onEnter_16 (def :  pcf.AddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at CreateAccountDV.pcf: line 53, column 35
    function def_onEnter_18 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.onEnter(selectedAddress)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at CreateAccountDV.pcf: line 64, column 67
    function def_onEnter_28 (def :  pcf.AccountCurrencyInputSet) : void {
      def.onEnter(account, selectedAddress, true)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 86, column 54
    function def_onEnter_31 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 86, column 54
    function def_onEnter_33 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 33, column 54
    function def_onEnter_4 (def :  pcf.CreateAccountContactInputSet_default) : void {
      def.onEnter(account.AccountHolderContact)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 33, column 54
    function def_onEnter_6 (def :  pcf.CreateAccountContactInputSet_person) : void {
      def.onEnter(account.AccountHolderContact)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 40, column 128
    function def_refreshVariables_14 (def :  pcf.LinkedAddressInputSet) : void {
      def.refreshVariables(selectedAddress, account.AccountHolderContact, null, account, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 43, column 59
    function def_refreshVariables_17 (def :  pcf.AddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at CreateAccountDV.pcf: line 53, column 35
    function def_refreshVariables_19 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.refreshVariables(selectedAddress)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at CreateAccountDV.pcf: line 64, column 67
    function def_refreshVariables_29 (def :  pcf.AccountCurrencyInputSet) : void {
      def.refreshVariables(account, selectedAddress, true)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 86, column 54
    function def_refreshVariables_32 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 86, column 54
    function def_refreshVariables_34 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 33, column 54
    function def_refreshVariables_5 (def :  pcf.CreateAccountContactInputSet_default) : void {
      def.refreshVariables(account.AccountHolderContact)
    }
    
    // 'def' attribute on InputSetRef at CreateAccountDV.pcf: line 33, column 54
    function def_refreshVariables_7 (def :  pcf.CreateAccountContactInputSet_person) : void {
      def.refreshVariables(account.AccountHolderContact)
    }
    
    // 'value' attribute on TextInput (id=webAddress_Input) at CreateAccountDV.pcf: line 38, column 42
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.WebAddress_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at CreateAccountDV.pcf: line 59, column 26
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedAddress.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at CreateAccountDV.pcf: line 92, column 35
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.Nickname = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at CreateAccountDV.pcf: line 100, column 75
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.PrimaryLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.IndustryCode = (__VALUE_TO_SET as entity.IndustryCode)
    }
    
    // 'value' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerSelection.Producer = (__VALUE_TO_SET as Organization)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerSelection.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'editable' attribute on InputSetRef at CreateAccountDV.pcf: line 43, column 59
    function editable_15 () : java.lang.Boolean {
      return selectedAddress.LinkedAddress == null
    }
    
    // 'editable' attribute on InputSetRef (id=AccountCurrency) at CreateAccountDV.pcf: line 64, column 67
    function editable_26 () : java.lang.Boolean {
      return account.Editable
    }
    
    // 'editable' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function editable_61 () : java.lang.Boolean {
      return gw.api.web.producer.ProducerUtil.canEditOrganization()
    }
    
    // 'initialValue' attribute on Variable at CreateAccountDV.pcf: line 17, column 30
    function initialValue_0 () : entity.Address {
      return account.AccountHolderContact.PrimaryAddress
    }
    
    // 'initialValue' attribute on Variable at CreateAccountDV.pcf: line 21, column 57
    function initialValue_1 () : java.util.List<entity.ProducerCode> {
      return producerSelection.getRangeOfActiveProducerCodesForCurrentUser(true)
    }
    
    // 'initialValue' attribute on Variable at CreateAccountDV.pcf: line 25, column 50
    function initialValue_2 () : java.util.List<Organization> {
      return producerSelection.getRangeOfActiveOrgaName()
    }
    
    // 'initialValue' attribute on Variable at CreateAccountDV.pcf: line 29, column 35
    function initialValue_3 () : entity.Organization {
      return getDefaulOrgname()
    }
    
    // 'inputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function inputConversion_49 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.findCode(VALUE, typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'mode' attribute on InputSetRef at CreateAccountDV.pcf: line 33, column 54
    function mode_8 () : java.lang.Object {
      return account.AccountHolderContact.Subtype
    }
    
    // 'onChange' attribute on PostOnChange at CreateAccountDV.pcf: line 129, column 43
    function onChange_60 () : void {
      changedProducer()
    }
    
    // 'onPick' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function onPick_63 (PickedValue :  java.lang.Object) : void {
      changedProducer()
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function optionLabel_78 (VALUE :  entity.ProducerCode) : java.lang.String {
      return DisplayKey.get("Web.ProducerSelection.ProducerCode.OptionLabel", VALUE.Code, VALUE.Description  != null ? VALUE.Description : "" )
    }
    
    // 'outputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function outputConversion_50 (VALUE :  entity.IndustryCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'requestValidationExpression' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function requestValidationExpression_51 (VALUE :  entity.IndustryCode) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.validateValue(VALUE, null, null)
    }
    
    // 'validationExpression' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function validationExpression_62 () : java.lang.Object {
      return producerSelection.validateProducer()
    }
    
    // 'validationExpression' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function validationExpression_73 () : java.lang.Object {
      return producerSelection.validateProducerCodeForAccount()
    }
    
    // 'valueRange' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function valueRange_67 () : java.lang.Object {
      return organizationName
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function valueRange_79 () : java.lang.Object {
      return producerCodeRange
    }
    
    // 'value' attribute on TextInput (id=webAddress_Input) at CreateAccountDV.pcf: line 38, column 42
    function valueRoot_11 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at CreateAccountDV.pcf: line 59, column 26
    function valueRoot_23 () : java.lang.Object {
      return selectedAddress
    }
    
    // 'value' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function valueRoot_66 () : java.lang.Object {
      return producerSelection
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at CreateAccountDV.pcf: line 59, column 26
    function value_21 () : java.lang.String {
      return selectedAddress.Description
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at CreateAccountDV.pcf: line 92, column 35
    function value_36 () : java.lang.String {
      return account.Nickname
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at CreateAccountDV.pcf: line 100, column 75
    function value_41 () : typekey.LanguageType {
      return account.PrimaryLanguage
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function value_52 () : entity.IndustryCode {
      return account.IndustryCode
    }
    
    // 'value' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function value_64 () : Organization {
      return producerSelection.Producer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function value_75 () : entity.ProducerCode {
      return producerSelection.ProducerCode
    }
    
    // 'value' attribute on TextInput (id=webAddress_Input) at CreateAccountDV.pcf: line 38, column 42
    function value_9 () : java.lang.String {
      return account.WebAddress_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function verifyValueRangeIsAllowedType_68 ($$arg :  Organization[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function verifyValueRangeIsAllowedType_68 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function verifyValueRangeIsAllowedType_68 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function verifyValueRangeIsAllowedType_80 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function verifyValueRangeIsAllowedType_80 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function verifyValueRangeIsAllowedType_80 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Producer_Input) at CreateAccountDV.pcf: line 126, column 36
    function verifyValueRange_69 () : void {
      var __valueRangeArg = organizationName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_68(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function verifyValueRange_81 () : void {
      var __valueRangeArg = producerCodeRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_80(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSetRef (id=AccountCurrency) at CreateAccountDV.pcf: line 64, column 67
    function visible_27 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on InputSet at CreateAccountDV.pcf: line 66, column 63
    function visible_30 () : java.lang.Boolean {
      return account.AccountHolderContact typeis Company
    }
    
    // 'visible' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at CreateAccountDV.pcf: line 100, column 75
    function visible_40 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().size() > 1
    }
    
    // 'visible' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function visible_48 () : java.lang.Boolean {
      return account.AccountHolder.AccountContact.Company
    }
    
    // 'visible' attribute on RangeInput (id=ProducerCode_Input) at CreateAccountDV.pcf: line 141, column 57
    function visible_74 () : java.lang.Boolean {
      return producerSelection.Producer != null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get defaultValueOrgNameList () : entity.Organization {
      return getVariableValue("defaultValueOrgNameList", 0) as entity.Organization
    }
    
    property set defaultValueOrgNameList ($arg :  entity.Organization) {
      setVariableValue("defaultValueOrgNameList", 0, $arg)
    }
    
    property get organizationName () : java.util.List<Organization> {
      return getVariableValue("organizationName", 0) as java.util.List<Organization>
    }
    
    property set organizationName ($arg :  java.util.List<Organization>) {
      setVariableValue("organizationName", 0, $arg)
    }
    
    property get producerCodeRange () : java.util.List<entity.ProducerCode> {
      return getVariableValue("producerCodeRange", 0) as java.util.List<entity.ProducerCode>
    }
    
    property set producerCodeRange ($arg :  java.util.List<entity.ProducerCode>) {
      setVariableValue("producerCodeRange", 0, $arg)
    }
    
    property get producerSelection () : ProducerSelection {
      return getRequireValue("producerSelection", 0) as ProducerSelection
    }
    
    property set producerSelection ($arg :  ProducerSelection) {
      setRequireValue("producerSelection", 0, $arg)
    }
    
    property get selectedAddress () : entity.Address {
      return getVariableValue("selectedAddress", 0) as entity.Address
    }
    
    property set selectedAddress ($arg :  entity.Address) {
      setVariableValue("selectedAddress", 0, $arg)
    }
    
    function changedProducer() {
      producerCodeRange = producerSelection.getRangeOfActiveProducerCodesForCurrentUser(true)
    
      if (producerCodeRange.Count == 1) {
        producerSelection.ProducerCode = producerCodeRange[0]
      } else {
        producerSelection.ProducerCode = null
      }
    }
    
    function getDefaulOrgname():Organization {
      organizationName = producerSelection.getRangeOfActiveOrgaName()
      if (producerSelection.Producer == null){
        if (organizationName.Count == 1) {
          producerSelection.Producer = organizationName[0]
        } else {
          producerSelection.Producer = organizationName.last()
        }
        changedProducer()
      }
      return producerSelection.Producer
    }
    
    
  }
  
  
}