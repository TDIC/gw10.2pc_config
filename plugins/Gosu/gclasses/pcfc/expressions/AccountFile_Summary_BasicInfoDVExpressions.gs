package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountFile_Summary_BasicInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountFile_Summary_BasicInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountFile_Summary_BasicInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountFile_Summary_BasicInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=WebAddressLink) at AccountFile_Summary_BasicInfoDV.pcf: line 65, column 37
    function action_33 () : void {
      TDIC_WebAddressForward.push(account.WebAddress_TDIC.toString().trim())
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_57 () : void {
      IndustryCodeSearchPopup.push(TC_SIC)
    }
    
    // 'action' attribute on Link (id=WebAddressLink) at AccountFile_Summary_BasicInfoDV.pcf: line 65, column 37
    function action_dest_34 () : pcf.api.Destination {
      return pcf.TDIC_WebAddressForward.createDestination(account.WebAddress_TDIC.toString().trim())
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_dest_58 () : pcf.api.Destination {
      return pcf.IndustryCodeSearchPopup.createDestination(TC_SIC)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 72, column 32
    function def_onEnter_36 (def :  pcf.AddressShortInputSet) : void {
      def.onEnter(address)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at AccountFile_Summary_BasicInfoDV.pcf: line 77, column 67
    function def_onEnter_40 (def :  pcf.AccountCurrencyInputSet) : void {
      def.onEnter(account, address, false)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 84, column 54
    function def_onEnter_42 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 84, column 54
    function def_onEnter_44 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 72, column 32
    function def_refreshVariables_37 (def :  pcf.AddressShortInputSet) : void {
      def.refreshVariables(address)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at AccountFile_Summary_BasicInfoDV.pcf: line 77, column 67
    function def_refreshVariables_41 (def :  pcf.AccountCurrencyInputSet) : void {
      def.refreshVariables(account, address, false)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 84, column 54
    function def_refreshVariables_43 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(account.AccountHolderContact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 84, column 54
    function def_refreshVariables_45 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(account.AccountHolderContact, null)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 41, column 75
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.PrimaryLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.IndustryCode = (__VALUE_TO_SET as entity.IndustryCode)
    }
    
    // 'editable' attribute on InputSetRef (id=AccountCurrency) at AccountFile_Summary_BasicInfoDV.pcf: line 77, column 67
    function editable_38 () : java.lang.Boolean {
      return account.Editable
    }
    
    // 'initialValue' attribute on Variable at AccountFile_Summary_BasicInfoDV.pcf: line 13, column 30
    function initialValue_0 () : entity.Address {
      return account.AccountHolderContact.PrimaryAddress
    }
    
    // 'inputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function inputConversion_59 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.findCode(VALUE, typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'label' attribute on Link (id=WebAddressLink) at AccountFile_Summary_BasicInfoDV.pcf: line 65, column 37
    function label_35 () : java.lang.Object {
      return account.WebAddress_TDIC
    }
    
    // 'mode' attribute on InputSetRef at AccountFile_Summary_BasicInfoDV.pcf: line 84, column 54
    function mode_46 () : java.lang.Object {
      return account.AccountHolderContact.Subtype
    }
    
    // 'outputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function outputConversion_60 (VALUE :  entity.IndustryCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'requestValidationExpression' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function requestValidationExpression_61 (VALUE :  entity.IndustryCode) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.validateValue(VALUE, null, null)
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AccountFile_Summary_BasicInfoDV.pcf: line 138, column 48
    function sortValue_70 (apc :  entity.AccountProducerCode) : java.lang.Object {
      return apc.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=desc_Cell) at AccountFile_Summary_BasicInfoDV.pcf: line 143, column 30
    function sortValue_71 (apc :  entity.AccountProducerCode) : java.lang.Object {
      return apc.ProducerCode.Description
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 18, column 40
    function valueRoot_2 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 22, column 59
    function valueRoot_5 () : java.lang.Object {
      return account.AccountHolderContact
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 18, column 40
    function value_1 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on DateInput (id=AccountStatusUpdateTime_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 34, column 72
    function value_11 () : java.util.Date {
      return account.AccountStatusUpdateTime
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 41, column 75
    function value_16 () : typekey.LanguageType {
      return account.PrimaryLanguage
    }
    
    // 'value' attribute on TypeKeyInput (id=ServiceTier_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 46, column 50
    function value_21 () : typekey.CustomerServiceTier {
      return account.ServiceTier
    }
    
    // 'value' attribute on TextInput (id=PhoneNumber_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 50, column 65
    function value_24 () : java.lang.String {
      return account.AccountHolderContact.PrimaryPhoneValue
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 54, column 61
    function value_27 () : java.lang.String {
      return account.AccountHolderContact.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 58, column 35
    function value_30 () : java.lang.String {
      return account.Nickname
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 22, column 59
    function value_4 () : java.lang.String {
      return account.AccountHolderContact.DisplayName
    }
    
    // 'value' attribute on TextInput (id=DUNS_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 89, column 72
    function value_48 () : java.lang.String {
      return account.AccountHolderContact.DUNSOfficialID
    }
    
    // 'value' attribute on TextInput (id=TUNS_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 94, column 72
    function value_53 () : java.lang.String {
      return account.AccountHolderContact.TUNSOfficialID
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function value_62 () : entity.IndustryCode {
      return account.IndustryCode
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountStatus_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 27, column 44
    function value_7 () : typekey.AccountStatus {
      return account.AccountStatus
    }
    
    // 'value' attribute on RowIterator at AccountFile_Summary_BasicInfoDV.pcf: line 133, column 54
    function value_78 () : entity.AccountProducerCode[] {
      return account.ProducerCodes
    }
    
    // 'visible' attribute on DateInput (id=AccountStatusUpdateTime_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 34, column 72
    function visible_10 () : java.lang.Boolean {
      return account.AccountStatus == AccountStatus.TC_WITHDRAWN
    }
    
    // 'visible' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 41, column 75
    function visible_15 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().size() > 1
    }
    
    // 'visible' attribute on InputSetRef (id=AccountCurrency) at AccountFile_Summary_BasicInfoDV.pcf: line 77, column 67
    function visible_39 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextInput (id=DUNS_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 89, column 72
    function visible_47 () : java.lang.Boolean {
      return account.AccountHolderContact.DUNSOfficialID != null
    }
    
    // 'visible' attribute on TextInput (id=TUNS_Input) at AccountFile_Summary_BasicInfoDV.pcf: line 94, column 72
    function visible_52 () : java.lang.Boolean {
      return account.AccountHolderContact.TUNSOfficialID != null
    }
    
    // 'visible' attribute on InputDivider at AccountFile_Summary_BasicInfoDV.pcf: line 121, column 55
    function visible_68 () : java.lang.Boolean {
      return account.ProducerCodes.Count != 0
    }
    
    // 'visible' attribute on InputSet at AccountFile_Summary_BasicInfoDV.pcf: line 97, column 63
    function visible_69 () : java.lang.Boolean {
      return account.AccountHolderContact typeis Company
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get address () : entity.Address {
      return getVariableValue("address", 0) as entity.Address
    }
    
    property set address ($arg :  entity.Address) {
      setVariableValue("address", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountFile_Summary_BasicInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountFile_Summary_BasicInfoDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AccountFile_Summary_BasicInfoDV.pcf: line 138, column 48
    function valueRoot_73 () : java.lang.Object {
      return apc.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AccountFile_Summary_BasicInfoDV.pcf: line 138, column 48
    function value_72 () : java.lang.String {
      return apc.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=desc_Cell) at AccountFile_Summary_BasicInfoDV.pcf: line 143, column 30
    function value_75 () : java.lang.String {
      return apc.ProducerCode.Description
    }
    
    property get apc () : entity.AccountProducerCode {
      return getIteratedValue(1) as entity.AccountProducerCode
    }
    
    
  }
  
  
}