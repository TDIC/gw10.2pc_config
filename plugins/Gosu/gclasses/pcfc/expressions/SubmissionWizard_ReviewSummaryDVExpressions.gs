package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_ReviewSummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubmissionWizard_ReviewSummaryDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_ReviewSummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubmissionWizard_ReviewSummaryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 35, column 144
    function currency_13 () : typekey.Currency {
      return policyPeriod.PreferredSettlementCurrency
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at SubmissionWizard_ReviewSummaryDV.pcf: line 21, column 29
    function def_onEnter_3 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.onEnter(policyPeriod, true)
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at SubmissionWizard_ReviewSummaryDV.pcf: line 21, column 29
    function def_refreshVariables_4 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.refreshVariables(policyPeriod, true)
    }
    
    // 'value' attribute on TextInput (id=PrimaryNamedInsured_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 18, column 63
    function valueRoot_1 () : java.lang.Object {
      return policyPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 35, column 144
    function valueRoot_12 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextInput (id=Product_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 43, column 50
    function valueRoot_17 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 27, column 92
    function valueRoot_7 () : java.lang.Object {
      return submission
    }
    
    // 'value' attribute on TextInput (id=PrimaryNamedInsured_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 18, column 63
    function value_0 () : java.lang.String {
      return policyPeriod.PrimaryNamedInsured.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 35, column 144
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.EstimatedPremium
    }
    
    // 'value' attribute on TextInput (id=Product_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 43, column 50
    function value_16 () : gw.api.productmodel.Product {
      return policyPeriod.Policy.Product
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 48, column 43
    function value_19 () : java.util.Date {
      return policyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 53, column 41
    function value_22 () : java.util.Date {
      return policyPeriod.PeriodEnd
    }
    
    // 'value' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 27, column 92
    function value_6 () : java.util.Date {
      return submission.DateQuoteNeeded
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=estimatedPremiumInput_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 35, column 144
    function visible_10 () : java.lang.Boolean {
      return (policyPeriod.BOPLineExists or policyPeriod.GLLineExists)? false : policyPeriod.Policy.Product.ProductType == TC_COMMERCIAL
    }
    
    // 'visible' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_ReviewSummaryDV.pcf: line 27, column 92
    function visible_5 () : java.lang.Boolean {
      return (policyPeriod.BOPLineExists or policyPeriod.GLLineExists)? false : true
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get submission () : Submission {
      return getRequireValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setRequireValue("submission", 0, $arg)
    }
    
    
  }
  
  
}