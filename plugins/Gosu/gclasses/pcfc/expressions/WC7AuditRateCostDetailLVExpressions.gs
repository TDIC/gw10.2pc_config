package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7AuditRateCostDetailLVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7AuditRateCostDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 173, column 179
    function def_onEnter_49 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.onEnter(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 173, column 179
    function def_refreshVariables_50 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.refreshVariables(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends WC7AuditRateCostDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 232, column 179
    function def_onEnter_62 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.onEnter(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 232, column 179
    function def_refreshVariables_63 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.refreshVariables(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends WC7AuditRateCostDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 291, column 179
    function def_onEnter_75 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.onEnter(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 291, column 179
    function def_refreshVariables_76 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.refreshVariables(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends WC7AuditRateCostDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 350, column 179
    function def_onEnter_88 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.onEnter(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    // 'def' attribute on RowSetRef at WC7AuditRateCostDetailLV.pcf: line 350, column 179
    function def_refreshVariables_89 (def :  pcf.WC7AuditRateCostDetailAggRowSet) : void {
      def.refreshVariables(isPremiumReport, aggCost, basedOnAggCosts.toList().firstWhere( \ b -> wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder))
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7AuditRateCostDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 101, column 36
    function initialValue_19 () : WC7CoveredEmployee {
      return wcEmpCost.WC7CoveredEmployee
    }
    
    // RowIterator at WC7AuditRateCostDetailLV.pcf: line 96, column 42
    function initializeVariables_46 () : void {
        wcCovEmp = wcEmpCost.WC7CoveredEmployee;

    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7AuditRateCostDetailLV.pcf: line 111, column 42
    function valueRoot_21 () : java.lang.Object {
      return wcEmpCost
    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7AuditRateCostDetailLV.pcf: line 111, column 42
    function value_20 () : java.lang.Integer {
      return wcEmpCost.LocationNum
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7AuditRateCostDetailLV.pcf: line 115, column 41
    function value_23 () : java.lang.String {
      return wcEmpCost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7AuditRateCostDetailLV.pcf: line 120, column 24
    function value_26 () : java.lang.String {
      return wcEmpCost.Description
    }
    
    // 'value' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailLV.pcf: line 125, column 42
    function value_29 () : java.math.BigDecimal {
      return payrollAmount(wcCovEmp)
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7AuditRateCostDetailLV.pcf: line 130, column 41
    function value_32 () : java.lang.String {
      return wcCovEmp.AuditedAmount == 0 ? "" : wcCovEmp.AuditedAmount as String
    }
    
    // 'value' attribute on TextCell (id=ModifiedTermRate_Cell) at WC7AuditRateCostDetailLV.pcf: line 135, column 41
    function value_34 () : java.lang.String {
      return wcEmpCost.ActualAdjRate == 0 ? "" : wcEmpCost.ActualAdjRate as String
    }
    
    // 'value' attribute on MonetaryAmountCell (id=EstPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 140, column 42
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return premiumAmount(wcCovEmp)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at WC7AuditRateCostDetailLV.pcf: line 145, column 43
    function value_39 () : gw.pl.currency.MonetaryAmount {
      return wcEmpCost.ActualAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Change_Cell) at WC7AuditRateCostDetailLV.pcf: line 151, column 42
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return wcEmpCost.ActualAmountBilling - premiumAmount(wcCovEmp)
    }
    
    // 'visible' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailLV.pcf: line 125, column 42
    function visible_30 () : java.lang.Boolean {
      return not isPremiumReport
    }
    
    // 'visible' attribute on Row (id=euCostRow) at WC7AuditRateCostDetailLV.pcf: line 107, column 60
    function visible_45 () : java.lang.Boolean {
      return not wcEmpCost.ActualAmountBilling.IsZero
    }
    
    property get wcCovEmp () : WC7CoveredEmployee {
      return getVariableValue("wcCovEmp", 1) as WC7CoveredEmployee
    }
    
    property set wcCovEmp ($arg :  WC7CoveredEmployee) {
      setVariableValue("wcCovEmp", 1, $arg)
    }
    
    property get wcEmpCost () : WC7CovEmpCost {
      return getIteratedValue(1) as WC7CovEmpCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/WC7AuditRateCostDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7AuditRateCostDetailLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 20, column 51
    function initialValue_0 () : java.util.Set<entity.WC7Cost> {
      return ratingPeriod.Jurisdiction.WCLine.BasedOn.Costs.cast( WC7Cost ).where( \ w -> w.JurisdictionState==ratingPeriod.Jurisdiction.Jurisdiction).toSet().byRatingPeriod( ratingPeriod )
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 25, column 57
    function initialValue_1 () : java.util.Set<entity.WC7CovEmpCost> {
      return periodCosts.whereTypeIs(WC7CovEmpCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 30, column 63
    function initialValue_2 () : java.util.Set<entity.WC7JurisdictionCost> {
      return periodCosts.whereTypeIs(WC7JurisdictionCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 35, column 63
    function initialValue_3 () : java.util.Set<entity.WC7JurisdictionCost> {
      return basedOnCosts.whereTypeIs(WC7JurisdictionCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at WC7AuditRateCostDetailLV.pcf: line 39, column 32
    function initialValue_4 () : typekey.Currency {
      return ratingPeriod.Jurisdiction.Branch.PreferredSettlementCurrency
    }
    
    // 'sortBy' attribute on IteratorSort at WC7AuditRateCostDetailLV.pcf: line 104, column 24
    function sortBy_18 (wcEmpCost :  WC7CovEmpCost) : java.lang.Object {
      return wcEmpCost.CalcOrder
    }
    
    // 'sortBy' attribute on IteratorSort at WC7AuditRateCostDetailLV.pcf: line 171, column 24
    function sortBy_48 (aggCost :  WC7JurisdictionCost) : java.lang.Object {
      return aggCost.CalcOrder
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7AuditRateCostDetailLV.pcf: line 65, column 39
    function value_10 () : java.lang.String {
      return isPremiumReport ? DisplayKey.get("Web.AuditWizard.Basis") : DisplayKey.get("Web.AuditWizard.AuditedBasis")
    }
    
    // 'value' attribute on TextCell (id=EstPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 74, column 40
    function value_12 () : java.lang.String {
      return premiumLabel()
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at WC7AuditRateCostDetailLV.pcf: line 78, column 39
    function value_15 () : java.lang.String {
      return isPremiumReport ? DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Premium") : DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Audited")
    }
    
    // 'value' attribute on RowIterator at WC7AuditRateCostDetailLV.pcf: line 96, column 42
    function value_47 () : entity.WC7CovEmpCost[] {
      return euCosts.toTypedArray()
    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7AuditRateCostDetailLV.pcf: line 48, column 39
    function value_5 () : java.lang.String {
      return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Loc") 
    }
    
    // 'value' attribute on RowIterator (id=lt100) at WC7AuditRateCostDetailLV.pcf: line 168, column 48
    function value_51 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder_TDIC( 0, 100, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BasedOnManualPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 202, column 40
    function value_53 () : gw.pl.currency.MonetaryAmount {
      return basedOnCosts.byCalcOrder_TDIC(0, 100, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountSubtotal_Cell) at WC7AuditRateCostDetailLV.pcf: line 207, column 118
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 100, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DiffManualPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 213, column 40
    function value_58 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 100, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM).AmountSum(currency) - basedOnCosts.byCalcOrder_TDIC(0, 100, WC7PremiumLevelType.TC_TOTALMANUALPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on RowIterator (id=f100t200) at WC7AuditRateCostDetailLV.pcf: line 227, column 48
    function value_64 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder_TDIC( 101, 200, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM ).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BasedOnSubjPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 261, column 40
    function value_66 () : gw.pl.currency.MonetaryAmount {
      return basedOnCosts.byCalcOrder_TDIC(0, 200, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountSubtotal100_Cell) at WC7AuditRateCostDetailLV.pcf: line 266, column 119
    function value_69 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 200, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailLV.pcf: line 61, column 40
    function value_7 () : java.lang.String {
      return basisLabel()//payrollLabel()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DiffSubjPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 272, column 40
    function value_71 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 200, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM).AmountSum(currency) - basedOnCosts.byCalcOrder_TDIC(0, 200, WC7PremiumLevelType.TC_TOTALSUBJECTPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on RowIterator (id=f200t300) at WC7AuditRateCostDetailLV.pcf: line 286, column 48
    function value_77 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder_TDIC( 201, 300, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BasedOnModPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 320, column 40
    function value_79 () : gw.pl.currency.MonetaryAmount {
      return basedOnCosts.byCalcOrder_TDIC(0, 300, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountSubtotal200_Cell) at WC7AuditRateCostDetailLV.pcf: line 325, column 120
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 300, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DiffModPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 331, column 40
    function value_84 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 300, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM).AmountSum(currency) - basedOnCosts.byCalcOrder_TDIC(0, 300, WC7PremiumLevelType.TC_TOTALMODIFIEDPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on RowIterator (id=f300t400) at WC7AuditRateCostDetailLV.pcf: line 345, column 48
    function value_90 () : entity.WC7JurisdictionCost[] {
      return aggCosts.byCalcOrder_TDIC( 301, 400, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM ).toTypedArray()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BasedOnStdPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 379, column 40
    function value_92 () : gw.pl.currency.MonetaryAmount {
      return basedOnCosts.byCalcOrder_TDIC(0, 400, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountSubtotal300_Cell) at WC7AuditRateCostDetailLV.pcf: line 385, column 120
    function value_95 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 400, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).AmountSum(currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DiffStdPremium_Cell) at WC7AuditRateCostDetailLV.pcf: line 391, column 40
    function value_97 () : gw.pl.currency.MonetaryAmount {
      return periodCosts.byCalcOrder_TDIC(0, 400, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).AmountSum(currency) - basedOnCosts.byCalcOrder_TDIC(0, 400, WC7PremiumLevelType.TC_TOTALSTANDARDPREMIUM).AmountSum(currency)
    }
    
    // 'visible' attribute on TextCell (id=EstBasis_Cell) at WC7AuditRateCostDetailLV.pcf: line 61, column 40
    function visible_8 () : java.lang.Boolean {
      return not isPremiumReport
    }
    
    property get aggCosts () : java.util.Set<entity.WC7JurisdictionCost> {
      return getVariableValue("aggCosts", 0) as java.util.Set<entity.WC7JurisdictionCost>
    }
    
    property set aggCosts ($arg :  java.util.Set<entity.WC7JurisdictionCost>) {
      setVariableValue("aggCosts", 0, $arg)
    }
    
    property get basedOnAggCosts () : java.util.Set<entity.WC7JurisdictionCost> {
      return getVariableValue("basedOnAggCosts", 0) as java.util.Set<entity.WC7JurisdictionCost>
    }
    
    property set basedOnAggCosts ($arg :  java.util.Set<entity.WC7JurisdictionCost>) {
      setVariableValue("basedOnAggCosts", 0, $arg)
    }
    
    property get basedOnCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("basedOnCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set basedOnCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("basedOnCosts", 0, $arg)
    }
    
    property get currency () : typekey.Currency {
      return getVariableValue("currency", 0) as typekey.Currency
    }
    
    property set currency ($arg :  typekey.Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get euCosts () : java.util.Set<entity.WC7CovEmpCost> {
      return getVariableValue("euCosts", 0) as java.util.Set<entity.WC7CovEmpCost>
    }
    
    property set euCosts ($arg :  java.util.Set<entity.WC7CovEmpCost>) {
      setVariableValue("euCosts", 0, $arg)
    }
    
    property get isPremiumReport () : boolean {
      return getRequireValue("isPremiumReport", 0) as java.lang.Boolean
    }
    
    property set isPremiumReport ($arg :  boolean) {
      setRequireValue("isPremiumReport", 0, $arg)
    }
    
    property get periodCosts () : java.util.Set<WC7Cost> {
      return getRequireValue("periodCosts", 0) as java.util.Set<WC7Cost>
    }
    
    property set periodCosts ($arg :  java.util.Set<WC7Cost>) {
      setRequireValue("periodCosts", 0, $arg)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getRequireValue("ratingPeriod", 0) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    property set ratingPeriod ($arg :  gw.lob.wc7.rating.WC7RatingPeriod) {
      setRequireValue("ratingPeriod", 0, $arg)
    }
    
    property get wc7RatingUtil () : tdic.pc.config.rating.wc7.WC7RatingUtil {
      return getVariableValue("wc7RatingUtil", 0) as tdic.pc.config.rating.wc7.WC7RatingUtil
    }
    
    property set wc7RatingUtil ($arg :  tdic.pc.config.rating.wc7.WC7RatingUtil) {
      setVariableValue("wc7RatingUtil", 0, $arg)
    }
    
    function premiumLabel() : String {
      return (isRevisedAudit() ?
        DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.PriorAudited") :
        DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Estimated"))
    }
    function premiumAmount(wcCovEmp : WC7CoveredEmployee) : gw.pl.currency.MonetaryAmount {
      var value = wcCovEmp.BasedOn.WC7CovEmpCost.ActualAmount 
      return value ?: gw.api.util.MonetaryAmounts.zeroOf(wcCovEmp.Branch.PreferredSettlementCurrency)
    }
    function payrollLabel() : String {
      return (isRevisedAudit() ?
        DisplayKey.get("Web.AuditWizard.PriorAuditedPayroll") :
        DisplayKey.get("Web.AuditWizard.EstPayroll"))
    }
    function basisLabel() : String {
      return (isRevisedAudit() ?
          DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") :
          DisplayKey.get("Web.AuditWizard.EstBasis"))
    }
    function payrollAmount(wcCovEmp : WC7CoveredEmployee) : java.math.BigDecimal {
      return wcCovEmp.LastBilledCoveredEmployee.BasisForRating
    }
    private function isRevisedAudit() : boolean {
      return ratingPeriod.Jurisdiction.Branch.Audit.AuditInformation.IsRevision
    }
    
    
  }
  
  
}