package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/Policy_JobUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Policy_JobUsersLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/Policy_JobUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Policy_JobUsersLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function action_14 () : void {
      pcf.GroupSearchPopup.push()
    }
    
    // AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function action_16 () : void {
      pcf.OrganizationGroupTreePopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at Policy_JobUsersLV.pcf: line 25, column 44
    function action_9 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at Policy_JobUsersLV.pcf: line 25, column 44
    function action_dest_10 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function action_dest_15 () : pcf.api.Destination {
      return pcf.GroupSearchPopup.createDestination()
    }
    
    // AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function action_dest_17 () : pcf.api.Destination {
      return pcf.OrganizationGroupTreePopup.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Phone_Cell) at Policy_JobUsersLV.pcf: line 33, column 70
    function valueRoot_22 () : java.lang.Object {
      return assignment.AssignedUser.Contact
    }
    
    // 'value' attribute on DateCell (id=LegacyTimeStamp_Cell) at Policy_JobUsersLV.pcf: line 38, column 47
    function valueRoot_25 () : java.lang.Object {
      return assignment.Job
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at Policy_JobUsersLV.pcf: line 21, column 41
    function valueRoot_7 () : java.lang.Object {
      return assignment
    }
    
    // 'value' attribute on AltUserCell (id=User_Cell) at Policy_JobUsersLV.pcf: line 25, column 44
    function value_11 () : entity.User {
      return assignment.AssignedUser
    }
    
    // 'value' attribute on AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function value_18 () : entity.Group {
      return assignment.AssignedGroup
    }
    
    // 'value' attribute on TextCell (id=Phone_Cell) at Policy_JobUsersLV.pcf: line 33, column 70
    function value_21 () : java.lang.String {
      return assignment.AssignedUser.Contact.PrimaryPhoneValue
    }
    
    // 'value' attribute on DateCell (id=LegacyTimeStamp_Cell) at Policy_JobUsersLV.pcf: line 38, column 47
    function value_24 () : java.util.Date {
      return assignment.Job.LegacyTimeStamp_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at Policy_JobUsersLV.pcf: line 21, column 41
    function value_6 () : typekey.UserRole {
      return assignment.Role
    }
    
    // 'visible' attribute on DateCell (id=LegacyTimeStamp_Cell) at Policy_JobUsersLV.pcf: line 38, column 47
    function visible_26 () : java.lang.Boolean {
      return job.MigrationJobInd_TDIC
    }
    
    property get assignment () : entity.JobUserRoleAssignment {
      return getIteratedValue(1) as entity.JobUserRoleAssignment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/Policy_JobUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy_JobUsersLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at Policy_JobUsersLV.pcf: line 21, column 41
    function sortValue_0 (assignment :  entity.JobUserRoleAssignment) : java.lang.Object {
      return assignment.Role
    }
    
    // 'value' attribute on AltUserCell (id=User_Cell) at Policy_JobUsersLV.pcf: line 25, column 44
    function sortValue_1 (assignment :  entity.JobUserRoleAssignment) : java.lang.Object {
      return assignment.AssignedUser
    }
    
    // 'value' attribute on AltGroupCell (id=Group_Cell) at Policy_JobUsersLV.pcf: line 29, column 45
    function sortValue_2 (assignment :  entity.JobUserRoleAssignment) : java.lang.Object {
      return assignment.AssignedGroup
    }
    
    // 'value' attribute on TextCell (id=Phone_Cell) at Policy_JobUsersLV.pcf: line 33, column 70
    function sortValue_3 (assignment :  entity.JobUserRoleAssignment) : java.lang.Object {
      return assignment.AssignedUser.Contact.PrimaryPhoneValue
    }
    
    // 'value' attribute on DateCell (id=LegacyTimeStamp_Cell) at Policy_JobUsersLV.pcf: line 38, column 47
    function sortValue_4 (assignment :  entity.JobUserRoleAssignment) : java.lang.Object {
      return assignment.Job.LegacyTimeStamp_TDIC
    }
    
    // 'value' attribute on RowIterator at Policy_JobUsersLV.pcf: line 15, column 50
    function value_28 () : entity.JobUserRoleAssignment[] {
      return job.RoleAssignments
    }
    
    // 'visible' attribute on DateCell (id=LegacyTimeStamp_Cell) at Policy_JobUsersLV.pcf: line 38, column 47
    function visible_5 () : java.lang.Boolean {
      return job.MigrationJobInd_TDIC
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    
  }
  
  
}