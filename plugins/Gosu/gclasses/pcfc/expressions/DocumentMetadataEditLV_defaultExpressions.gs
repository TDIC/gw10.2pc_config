package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentMetadataEditLV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentMetadataEditLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.default.pcf: line 24, column 53
    function initialValue_0 () : gw.api.web.document.DocumentPCContext {
      return DocumentApplicationContext as gw.api.web.document.DocumentPCContext
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.default.pcf: line 52, column 34
    function sortValue_1 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.default.pcf: line 109, column 53
    function sortValue_10 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.SecurityType
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function sortValue_11 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function sortValue_12 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.default.pcf: line 58, column 41
    function sortValue_2 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function sortValue_3 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.MimeType
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.default.pcf: line 71, column 36
    function sortValue_4 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Author
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function sortValue_5 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Language
    }
    
    // 'value' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function sortValue_7 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Level
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.default.pcf: line 103, column 51
    function sortValue_9 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Status
    }
    
    // 'toRemove' attribute on RowIterator at DocumentMetadataEditLV.default.pcf: line 34, column 54
    function toRemove_74 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : void {
      DocumentCreationInfos.remove(DocumentCreationInfo); DocumentCreationInfo.Document.remove()
    }
    
    // 'value' attribute on RowIterator at DocumentMetadataEditLV.default.pcf: line 34, column 54
    function value_75 () : gw.document.DocumentCreationInfo[] {
      return DocumentCreationInfos.toTypedArray()
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function visible_6 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function visible_8 () : java.lang.Boolean {
      return documentPCContext.LevelsVisible
    }
    
    property get DocumentApplicationContext () : gw.document.DocumentApplicationContext {
      return getRequireValue("DocumentApplicationContext", 0) as gw.document.DocumentApplicationContext
    }
    
    property set DocumentApplicationContext ($arg :  gw.document.DocumentApplicationContext) {
      setRequireValue("DocumentApplicationContext", 0, $arg)
    }
    
    property get DocumentCreationInfos () : java.util.Collection<gw.document.DocumentCreationInfo> {
      return getRequireValue("DocumentCreationInfos", 0) as java.util.Collection<gw.document.DocumentCreationInfo>
    }
    
    property set DocumentCreationInfos ($arg :  java.util.Collection<gw.document.DocumentCreationInfo>) {
      setRequireValue("DocumentCreationInfos", 0, $arg)
    }
    
    property get documentPCContext () : gw.api.web.document.DocumentPCContext {
      return getVariableValue("documentPCContext", 0) as gw.api.web.document.DocumentPCContext
    }
    
    property set documentPCContext ($arg :  gw.api.web.document.DocumentPCContext) {
      setVariableValue("documentPCContext", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentMetadataEditLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.default.pcf: line 52, column 34
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.default.pcf: line 58, column 41
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Level = (__VALUE_TO_SET as gw.api.domain.linkedobject.LinkedObjectContainer)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function defaultSetter_67 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Subtype = (__VALUE_TO_SET as typekey.OnBaseDocumentSubtype_Ext)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at DocumentMetadataEditLV.default.pcf: line 46, column 32
    function icon_14 () : java.lang.String {
      return document.Icon
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.default.pcf: line 38, column 33
    function initialValue_13 () : entity.Document {
      return DocumentCreationInfo.Document
    }
    
    // RowIterator at DocumentMetadataEditLV.default.pcf: line 34, column 54
    function initializeVariables_73 () : void {
        document = DocumentCreationInfo.Document;

    }
    
    // 'optionLabel' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function optionLabel_25 (VALUE :  java.lang.String) : java.lang.String {
      return gw.document.DocumentsUtilBase.getMimeTypeDescription(VALUE)
    }
    
    // 'optionLabel' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function optionLabel_44 (VALUE :  gw.api.domain.linkedobject.LinkedObjectContainer) : java.lang.String {
      return Note.getLevelDisplayString(VALUE)
    }
    
    // 'value' attribute on Reflect at DocumentMetadataEditLV.default.pcf: line 129, column 83
    function reflectionValue_63 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function valueRange_26 () : java.lang.Object {
      return gw.document.DocumentsUtilBase.getMimeTypes()
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function valueRange_36 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function valueRange_45 () : java.lang.Object {
      return documentPCContext.GenerateLevels
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function valueRange_59 () : java.lang.Object {
      return DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
    }
    
    // 'valueRange' attribute on Reflect at DocumentMetadataEditLV.default.pcf: line 129, column 83
    function valueRange_65 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function valueRange_69 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(document.Type)
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.default.pcf: line 52, column 34
    function valueRoot_17 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.default.pcf: line 52, column 34
    function value_15 () : java.lang.String {
      return document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.default.pcf: line 58, column 41
    function value_19 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function value_23 () : java.lang.String {
      return document.MimeType
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.default.pcf: line 71, column 36
    function value_30 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function value_33 () : typekey.LanguageType {
      return document.Language
    }
    
    // 'value' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function value_41 () : gw.api.domain.linkedobject.LinkedObjectContainer {
      return document.Level
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.default.pcf: line 103, column 51
    function value_50 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.default.pcf: line 109, column 53
    function value_53 () : typekey.DocumentSecurityType {
      return document.SecurityType
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function value_56 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function value_66 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function verifyValueRangeIsAllowedType_27 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function verifyValueRangeIsAllowedType_27 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function verifyValueRangeIsAllowedType_37 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function verifyValueRangeIsAllowedType_37 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function verifyValueRangeIsAllowedType_46 ($$arg :  gw.api.domain.linkedobject.LinkedObjectContainer[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function verifyValueRangeIsAllowedType_46 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function verifyValueRangeIsAllowedType_60 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function verifyValueRangeIsAllowedType_60 ($$arg :  typekey.DocumentType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function verifyValueRangeIsAllowedType_70 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function verifyValueRangeIsAllowedType_70 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.default.pcf: line 66, column 41
    function verifyValueRange_28 () : void {
      var __valueRangeArg = gw.document.DocumentsUtilBase.getMimeTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_27(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function verifyValueRange_38 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_37(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function verifyValueRange_47 () : void {
      var __valueRangeArg = documentPCContext.GenerateLevels
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_46(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentMetadataEditLV.default.pcf: line 117, column 45
    function verifyValueRange_61 () : void {
      var __valueRangeArg = DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_60(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentMetadataEditLV.default.pcf: line 125, column 57
    function verifyValueRange_71 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(document.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_70(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.default.pcf: line 86, column 69
    function visible_39 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeCell (id=RelatedTo_Cell) at DocumentMetadataEditLV.default.pcf: line 97, column 54
    function visible_48 () : java.lang.Boolean {
      return documentPCContext.LevelsVisible
    }
    
    property get DocumentCreationInfo () : gw.document.DocumentCreationInfo {
      return getIteratedValue(1) as gw.document.DocumentCreationInfo
    }
    
    property get document () : entity.Document {
      return getVariableValue("document", 1) as entity.Document
    }
    
    property set document ($arg :  entity.Document) {
      setVariableValue("document", 1, $arg)
    }
    
    
  }
  
  
}