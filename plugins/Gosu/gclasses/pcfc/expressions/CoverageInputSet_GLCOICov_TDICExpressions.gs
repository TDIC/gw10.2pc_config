package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCOICov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLCOICov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCOICov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLCOICov_TDIC.pcf: line 49, column 62
    function allCheckedRowsAction_5 (CheckedValues :  entity.GLCertofInsSched_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCOICov_TDIC.pcf: line 33, column 99
    function available_26 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.GLCOICov_TDIC.pcf: line 49, column 62
    function available_3 () : java.lang.Boolean {
      return glLine.Branch.Job typeis PolicyChange
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCOICov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLCOICov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCOICov_TDIC.pcf: line 33, column 99
    function label_27 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCOICov_TDIC.pcf: line 33, column 99
    function setter_28 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 68, column 56
    function sortValue_6 (scheduledItem :  entity.GLCertofInsSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 73, column 57
    function sortValue_7 (scheduledItem :  entity.GLCertofInsSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function sortValue_8 (scheduledItem :  entity.GLCertofInsSched_TDIC) : java.lang.Object {
      return scheduledItem.CertificateHolder
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLCOICov_TDIC.pcf: line 61, column 56
    function toCreateAndAdd_22 () : entity.GLCertofInsSched_TDIC {
      return glLine.createAndAddCertofInsSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLCOICov_TDIC.pcf: line 61, column 56
    function toRemove_23 (scheduledItem :  entity.GLCertofInsSched_TDIC) : void {
      glLine.toRemoveFromGLCertofInsSched_TDIC(scheduledItem)
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLCOICov_TDIC.pcf: line 61, column 56
    function value_24 () : entity.GLCertofInsSched_TDIC[] {
      return glLine.GLCertofInsSched_TDIC
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.GLCOICov_TDIC.pcf: line 42, column 179
    function visible_2 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLCertofInsSched_TDIC.hasMatch(\sched -> sched.BasedOn == null)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLCOICov_TDIC.pcf: line 33, column 99
    function visible_25 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLCOICov_TDIC.pcf: line 98, column 36
    function visible_31 () : java.lang.Boolean {
      return isCOICovVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isCOICovVisible() : boolean {
      if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_QUOTED) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
            coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
        return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLCOICov_TDICExists
    
      } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                  coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
        return true
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    function setExpirationDate(glCertOfInsScheds : GLCertofInsSched_TDIC[]) {
      glCertOfInsScheds.each(\glCertOfInsSched -> {
        if(glCertOfInsSched.LTExpirationDate == null) {
          glCertOfInsSched.LTExpirationDate = glCertOfInsSched.Branch.EditEffectiveDate
        }
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLCOICov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.CertificateHolder = (__VALUE_TO_SET as entity.PolicyCertificateHolder_TDIC)
    }
    
    // 'valueRange' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function valueRange_18 () : java.lang.Object {
      return glLine.GLCertificateHolders_TDIC
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 68, column 56
    function valueRoot_10 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 73, column 57
    function value_12 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function value_15 () : entity.PolicyCertificateHolder_TDIC {
      return scheduledItem.CertificateHolder
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 68, column 56
    function value_9 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'valueRange' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function verifyValueRangeIsAllowedType_19 ($$arg :  entity.PolicyCertificateHolder_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function verifyValueRangeIsAllowedType_19 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyCertificateHolder_TDIC>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=certHolder_Cell) at CoverageInputSet.GLCOICov_TDIC.pcf: line 81, column 66
    function verifyValueRange_20 () : void {
      var __valueRangeArg = glLine.GLCertificateHolders_TDIC
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    property get scheduledItem () : entity.GLCertofInsSched_TDIC {
      return getIteratedValue(1) as entity.GLCertofInsSched_TDIC
    }
    
    
  }
  
  
}