package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policyfile/PolicyFile_Coverage_GLScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_Coverage_GLScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policyfile/PolicyFile_Coverage_GLScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyFile_Coverage_GLScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 123, column 50
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.State = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 130, column 55
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.PracticedInThisState_TDIC = (__VALUE_TO_SET as typekey.YesNo_TDIC)
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 137, column 55
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.PlantoPracticeInThisState_TDIC = (__VALUE_TO_SET as typekey.YesNo_TDIC)
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 144, column 70
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      dentLicenseState.DatesOfPractice_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filter' attribute on TypeKeyCell (id=state_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 123, column 50
    function filter_40 (VALUE :  typekey.State, VALUES :  typekey.State[]) : java.util.List<typekey.State> {
      return VALUES.where(\elt -> elt.hasCategory(Country.TC_US)).toList()
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 123, column 50
    function valueRoot_39 () : java.lang.Object {
      return dentLicenseState
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 123, column 50
    function value_37 () : typekey.State {
      return dentLicenseState.State
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 130, column 55
    function value_42 () : typekey.YesNo_TDIC {
      return dentLicenseState.PracticedInThisState_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 137, column 55
    function value_46 () : typekey.YesNo_TDIC {
      return dentLicenseState.PlantoPracticeInThisState_TDIC
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 144, column 70
    function value_50 () : java.lang.String {
      return dentLicenseState.DatesOfPractice_TDIC
    }
    
    property get dentLicenseState () : entity.GLDentLicenseStates_TDIC {
      return getIteratedValue(1) as entity.GLDentLicenseStates_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policyfile/PolicyFile_Coverage_GLScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_Coverage_GLScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function available_5 () : java.lang.Boolean {
      return policyPeriod.getLineExists(policyPeriod.GLLine.Pattern)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 40, column 137
    function def_onEnter_11 (def :  pcf.GLAdditionalCoveragesDV) : void {
      def.onEnter(glLine) //AdditionalCoveragesDV(policyPeriod.GLLine, new String[]{"GLGroup"}, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 54, column 85
    function def_onEnter_13 (def :  pcf.TDIC_AdditionalInsuredsDV) : void {
      def.onEnter(policyPeriod.GLLine, false, true, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 61, column 78
    function def_onEnter_16 (def :  pcf.TDIC_CertificateHolderDetailsDV) : void {
      def.onEnter(policyPeriod.GLLine, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 70, column 65
    function def_onEnter_19 (def :  pcf.TDIC_GLDiscountsDV) : void {
      def.onEnter(policyPeriod.GLLine,null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 74, column 72
    function def_onEnter_22 (def :  pcf.GLRiskManagementDiscount_TDICDV) : void {
      def.onEnter(policyPeriod.GLLine)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 78, column 58
    function def_onEnter_24 (def :  pcf.TDIC_IRPMDiscountDV) : void {
      def.onEnter(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 82, column 67
    function def_onEnter_28 (def :  pcf.TDIC_GLIRPMModifiersDV) : void {
      def.onEnter(policyPeriod.GLLine)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at PolicyFile_Coverage_GLScreen.pcf: line 26, column 50
    function def_onEnter_3 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(policyPeriod.GLLine, false, null)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at PolicyFile_Coverage_GLScreen.pcf: line 93, column 29
    function def_onEnter_31 (def :  pcf.QuestionSets_TDICDV) : void {
      def.onEnter(underwritingQuestionSets,glLine.PolicyLine, null,false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function def_onEnter_6 (def :  pcf.PolicyLineDV_GLLine) : void {
      def.onEnter(policyPeriod.GLLine, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function def_onEnter_8 (def :  pcf.PolicyLineDV_PersonalAutoLine) : void {
      def.onEnter(policyPeriod.GLLine, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 40, column 137
    function def_refreshVariables_12 (def :  pcf.GLAdditionalCoveragesDV) : void {
      def.refreshVariables(glLine) //AdditionalCoveragesDV(policyPeriod.GLLine, new String[]{"GLGroup"}, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 54, column 85
    function def_refreshVariables_14 (def :  pcf.TDIC_AdditionalInsuredsDV) : void {
      def.refreshVariables(policyPeriod.GLLine, false, true, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 61, column 78
    function def_refreshVariables_17 (def :  pcf.TDIC_CertificateHolderDetailsDV) : void {
      def.refreshVariables(policyPeriod.GLLine, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 70, column 65
    function def_refreshVariables_20 (def :  pcf.TDIC_GLDiscountsDV) : void {
      def.refreshVariables(policyPeriod.GLLine,null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 74, column 72
    function def_refreshVariables_23 (def :  pcf.GLRiskManagementDiscount_TDICDV) : void {
      def.refreshVariables(policyPeriod.GLLine)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 78, column 58
    function def_refreshVariables_25 (def :  pcf.TDIC_IRPMDiscountDV) : void {
      def.refreshVariables(glLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 82, column 67
    function def_refreshVariables_29 (def :  pcf.TDIC_GLIRPMModifiersDV) : void {
      def.refreshVariables(policyPeriod.GLLine)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at PolicyFile_Coverage_GLScreen.pcf: line 93, column 29
    function def_refreshVariables_32 (def :  pcf.QuestionSets_TDICDV) : void {
      def.refreshVariables(underwritingQuestionSets,glLine.PolicyLine, null,false)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at PolicyFile_Coverage_GLScreen.pcf: line 26, column 50
    function def_refreshVariables_4 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(policyPeriod.GLLine, false, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function def_refreshVariables_7 (def :  pcf.PolicyLineDV_GLLine) : void {
      def.refreshVariables(policyPeriod.GLLine, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function def_refreshVariables_9 (def :  pcf.PolicyLineDV_PersonalAutoLine) : void {
      def.refreshVariables(policyPeriod.GLLine, null)
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_Coverage_GLScreen.pcf: line 14, column 49
    function initialValue_0 () : gw.api.productmodel.QuestionSet[] {
      return policyPeriod.getGLUWQuestionSets_TDIC()
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_Coverage_GLScreen.pcf: line 18, column 35
    function initialValue_1 () : productmodel.GLLine {
      return policyPeriod.GLLine
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_Coverage_GLScreen.pcf: line 23, column 25
    function initialValue_2 () : QuoteType {
      return setInitialQuoteType()
    }
    
    // 'mode' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 34, column 56
    function mode_10 () : java.lang.Object {
      return policyPeriod.GLLine.Pattern.PublicID
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 123, column 50
    function sortValue_33 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.State
    }
    
    // 'value' attribute on TypeKeyCell (id=practicedinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 130, column 55
    function sortValue_34 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.PracticedInThisState_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=plantopracticeinthisstate_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 137, column 55
    function sortValue_35 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.PlantoPracticeInThisState_TDIC
    }
    
    // 'value' attribute on TextCell (id=datesofpractice_Cell) at PolicyFile_Coverage_GLScreen.pcf: line 144, column 70
    function sortValue_36 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : java.lang.Object {
      return dentLicenseState.DatesOfPractice_TDIC
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at PolicyFile_Coverage_GLScreen.pcf: line 114, column 65
    function toCreateAndAdd_54 () : entity.GLDentLicenseStates_TDIC {
      return glLine.createAndAddGLDentLicenseStates_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at PolicyFile_Coverage_GLScreen.pcf: line 114, column 65
    function toRemove_55 (dentLicenseState :  entity.GLDentLicenseStates_TDIC) : void {
      glLine.removeFromGLDentLicenseStates_TDIC(dentLicenseState)
    }
    
    // 'validationExpression' attribute on ListViewPanel (id=DentLicenseStatesLV) at PolicyFile_Coverage_GLScreen.pcf: line 107, column 177
    function validationExpression_57 () : java.lang.Object {
      return glLine.GLDentLicenseStates_TDIC?.Count == 0 ? DisplayKey.get("TDIC.Web.Policy.GL.DentalLicenseInOtherStatesValidation") : null
    }
    
    // 'value' attribute on RowIterator at PolicyFile_Coverage_GLScreen.pcf: line 114, column 65
    function value_56 () : entity.GLDentLicenseStates_TDIC[] {
      return glLine.GLDentLicenseStates_TDIC
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_AdditionalInsuredCard) at PolicyFile_Coverage_GLScreen.pcf: line 52, column 88
    function visible_15 () : java.lang.Boolean {
      return policyPeriod.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
    }
    
    // 'visible' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 74, column 72
    function visible_21 () : java.lang.Boolean {
      return policyPeriod.GLLine.GLRiskMgmtDiscount_TDICExists
    }
    
    // 'visible' attribute on TitleBar at PolicyFile_Coverage_GLScreen.pcf: line 85, column 77
    function visible_26 () : java.lang.Boolean {
      return policyPeriod.GLLine.GLIRPMDiscount_TDICExists //true
    }
    
    // 'visible' attribute on PanelRef at PolicyFile_Coverage_GLScreen.pcf: line 82, column 67
    function visible_27 () : java.lang.Boolean {
      return policyPeriod.GLLine.GLIRPMDiscount_TDICExists
    }
    
    // 'visible' attribute on Card (id=GeneralLiability_PremiumModificationCardGene) at PolicyFile_Coverage_GLScreen.pcf: line 66, column 91
    function visible_30 () : java.lang.Boolean {
      return !(policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC")
    }
    
    // 'visible' attribute on DetailViewPanel at PolicyFile_Coverage_GLScreen.pcf: line 95, column 540
    function visible_58 () : java.lang.Boolean {
      return (policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and glLine.getAnswerForGLUWQuestion_TDIC("HoldDentalLicenseInOtherStates_TDIC")) or ((policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC") and glLine.getAnswerForGLUWQuestion_TDIC("HoldDentalLicensesInOtherStatesPLCMOC_TDIC")) and policyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION and !policyPeriod.isQuickQuote_TDIC
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getVariableValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    property get underwritingQuestionSets () : gw.api.productmodel.QuestionSet[] {
      return getVariableValue("underwritingQuestionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set underwritingQuestionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setVariableValue("underwritingQuestionSets", 0, $arg)
    }
    
    function setInitialQuoteType() : QuoteType {
          if (policyPeriod.Job typeis Submission) {
            return (policyPeriod.Job as Submission).QuoteType
          }
          return null
        }
    
    
  }
  
  
}