package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/rating/impact/TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ImpactTestingSearchPoliciesResultsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/rating/impact/TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_ImpactTestingSearchPoliciesResultsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=TDIC_PNICell_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 77, column 133
    function valueRoot_17 () : java.lang.Object {
      return policyPeriod.getSlice(policyPeriod.PeriodEnd.addSeconds(-1)).PrimaryNamedInsured.ContactDenorm
    }
    
    // 'value' attribute on TextCell (id=resultPolicyNumber_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 34, column 65
    function valueRoot_5 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on DateCell (id=resultExpirationDate_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 68, column 51
    function value_10 () : java.util.Date {
      return policyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=resultEffectiveDate_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 72, column 49
    function value_13 () : java.util.Date {
      return policyPeriod.PeriodEnd
    }
    
    // 'value' attribute on TextCell (id=TDIC_PNICell_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 77, column 133
    function value_16 () : java.lang.String {
      return policyPeriod.getSlice(policyPeriod.PeriodEnd.addSeconds(-1)).PrimaryNamedInsured.ContactDenorm.DisplayName
    }
    
    // 'value' attribute on TextCell (id=TDIC_ADACell_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 82, column 146
    function value_19 () : java.lang.String {
      return policyPeriod.getSlice(policyPeriod.PeriodEnd.addSeconds(-1)).PrimaryNamedInsured.ContactDenorm.ADANumber_TDICOfficialID
    }
    
    // 'value' attribute on TextCell (id=resultPolicyNumber_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 34, column 65
    function value_4 () : java.lang.String {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on TextCell (id=resultPolicyTerm_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 39, column 38
    function value_7 () : Integer {
      return policyPeriod.TermNumber
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/rating/impact/TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ImpactTestingSearchPoliciesResultsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (allPolicyPeriods :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=PrintMe) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 20, column 74
    function action_0 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions( "TDIC_ImpactTestingSearchPoliciesResultsLV" )
    }
    
    // 'value' attribute on TextCell (id=resultPolicyTerm_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 39, column 38
    function sortValue_1 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TermNumber
    }
    
    // 'value' attribute on DateCell (id=resultExpirationDate_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 68, column 51
    function sortValue_2 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=resultEffectiveDate_Cell) at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 72, column 49
    function sortValue_3 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PeriodEnd
    }
    
    // 'value' attribute on RowIterator at TDIC_ImpactTestingSearchPoliciesResultsPopup.pcf: line 28, column 85
    function value_22 () : gw.api.database.IQueryBeanResult<entity.PolicyPeriod> {
      return allPolicyPeriods
    }
    
    override property get CurrentLocation () : pcf.TDIC_ImpactTestingSearchPoliciesResultsPopup {
      return super.CurrentLocation as pcf.TDIC_ImpactTestingSearchPoliciesResultsPopup
    }
    
    property get allPolicyPeriods () : gw.api.database.IQueryBeanResult<entity.PolicyPeriod> {
      return getVariableValue("allPolicyPeriods", 0) as gw.api.database.IQueryBeanResult<entity.PolicyPeriod>
    }
    
    property set allPolicyPeriods ($arg :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) {
      setVariableValue("allPolicyPeriods", 0, $arg)
    }
    
    
  }
  
  
}