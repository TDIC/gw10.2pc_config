package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdditionalNamedInsuredsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdditionalNamedInsuredsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at AdditionalNamedInsuredsDV.pcf: line 70, column 74
    function conversionExpression_9 (PickedValue :  Contact) : entity.PolicyAddlNamedInsured {
      return period.addNewPolicyAddlNamedInsuredForContact_TDIC(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 17, column 54
    function initialValue_0 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 22, column 36
    function initialValue_1 () : AccountContactView[] {
      return null
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 31, column 72
    function initialValue_3 () : com.guidewire.pl.system.integration.plugins.PluginConfig {
      return gw.api.system.PLDependenciesGateway.getPluginConfig()
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 35, column 23
    function initialValue_4 () : boolean {
      return period.WC7LineExists
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at AdditionalNamedInsuredsDV.pcf: line 75, column 32
    function label_17 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyAddlNamedInsured.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at AdditionalNamedInsuredsDV.pcf: line 70, column 74
    function pickLocation_10 () : void {
      ContactSearchPopup.push(TC_NAMEDINSURED)
    }
    
    // 'sortBy' attribute on IteratorSort at AdditionalNamedInsuredsDV.pcf: line 83, column 36
    function sortBy_11 (namedInsured :  entity.AccountContactView) : java.lang.Object {
      return namedInsured.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at AdditionalNamedInsuredsDV.pcf: line 109, column 36
    function sortBy_18 (otherContact :  entity.AccountContactView) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at AdditionalNamedInsuredsDV.pcf: line 146, column 32
    function sortBy_24 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : java.lang.Object {
      return additionalNamedInsured
    }
    
    // 'sortBy' attribute on IteratorSort at AdditionalNamedInsuredsDV.pcf: line 58, column 34
    function sortBy_5 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdditionalNamedInsuredsDV.pcf: line 154, column 33
    function sortValue_25 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : java.lang.Object {
      return additionalNamedInsured.AccountContactRole.AccountContact.fullNameWithProfessionalCredentials_TDIC
    }
    
    // 'value' attribute on TextCell (id=Relationship_Cell) at AdditionalNamedInsuredsDV.pcf: line 160, column 32
    function sortValue_26 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : java.lang.Object {
      return additionalNamedInsured.Relationship
    }
    
    // 'value' attribute on IndustryCodeCell (id=IndustryCode_Cell) at AdditionalNamedInsuredsDV.pcf: line 173, column 33
    function sortValue_27 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : java.lang.Object {
      return (additionalNamedInsured.AccountContactRole as NamedInsured).IndustryCode
    }
    
    // 'value' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function sortValue_29 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : java.lang.Object {
      return additionalNamedInsured.TaxID
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at AdditionalNamedInsuredsDV.pcf: line 95, column 78
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return period.addAllExistingAdditionalNamedInsureds_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at AdditionalNamedInsuredsDV.pcf: line 131, column 59
    function toRemove_67 (additionalNamedInsured :  entity.PolicyAddlNamedInsured) : void {
      period.removePolicyNamedInsured(additionalNamedInsured)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at AdditionalNamedInsuredsDV.pcf: line 80, column 59
    function value_14 () : entity.AccountContactView[] {
      return getUnassignedAdditionalNamedInsureds()
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at AdditionalNamedInsuredsDV.pcf: line 106, column 59
    function value_21 () : entity.AccountContactView[] {
      return getOtherContacts()
    }
    
    // 'value' attribute on RowIterator at AdditionalNamedInsuredsDV.pcf: line 131, column 59
    function value_68 () : entity.PolicyAddlNamedInsured[] {
      return period.PolicyContactRoles.whereTypeIs(PolicyAddlNamedInsured)
    }
    
    // 'value' attribute on AddMenuItemIterator at AdditionalNamedInsuredsDV.pcf: line 55, column 51
    function value_8 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(TC_POLICYADDLNAMEDINSURED)
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at AdditionalNamedInsuredsDV.pcf: line 95, column 78
    function visible_15 () : java.lang.Boolean {
      return unassignedAdditionalNamedInsureds.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddOtherContact) at AdditionalNamedInsuredsDV.pcf: line 101, column 59
    function visible_22 () : java.lang.Boolean {
      return getOtherContacts().Count > 0
    }
    
    // 'visible' attribute on AddButton (id=AddContactsButton) at AdditionalNamedInsuredsDV.pcf: line 51, column 37
    function visible_23 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'visible' attribute on IndustryCodeCell (id=IndustryCode_Cell) at AdditionalNamedInsuredsDV.pcf: line 173, column 33
    function visible_28 () : java.lang.Boolean {
      return period.Policy.Product.isContactTypeSuitableForProductAccountType(Company)
    }
    
    // 'visible' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function visible_30 () : java.lang.Boolean {
      return isWC7Line
    }
    
    // 'visible' attribute on DetailViewPanel (id=AdditionalNamedInsuredsDV) at AdditionalNamedInsuredsDV.pcf: line 7, column 133
    function visible_69 () : java.lang.Boolean {
      return not period.Policy.Product.Personal and (period.Submission.QuoteType == TC_FULL or not (period.Job typeis Submission))
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get isWC7Line () : boolean {
      return getVariableValue("isWC7Line", 0) as java.lang.Boolean
    }
    
    property set isWC7Line ($arg :  boolean) {
      setVariableValue("isWC7Line", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherContacts () : AccountContactView[] {
      return getVariableValue("otherContacts", 0) as AccountContactView[]
    }
    
    property set otherContacts ($arg :  AccountContactView[]) {
      setVariableValue("otherContacts", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get pluginConfig () : com.guidewire.pl.system.integration.plugins.PluginConfig {
      return getVariableValue("pluginConfig", 0) as com.guidewire.pl.system.integration.plugins.PluginConfig
    }
    
    property set pluginConfig ($arg :  com.guidewire.pl.system.integration.plugins.PluginConfig) {
      setVariableValue("pluginConfig", 0, $arg)
    }
    
    property get unassignedAdditionalNamedInsureds () : AccountContactView[] {
      return getVariableValue("unassignedAdditionalNamedInsureds", 0) as AccountContactView[]
    }
    
    property set unassignedAdditionalNamedInsureds ($arg :  AccountContactView[]) {
      setVariableValue("unassignedAdditionalNamedInsureds", 0, $arg)
    }
    
    function getUnassignedAdditionalNamedInsureds() : AccountContactView[] {
      if (unassignedAdditionalNamedInsureds == null) {
        unassignedAdditionalNamedInsureds = period.UnassignedAdditionalNamedInsureds.asViews()
      }
      return unassignedAdditionalNamedInsureds
    }
    
    function getOtherContacts() : AccountContactView[] {
      if (otherContacts == null) {
        otherContacts = period.AdditionalNamedInsuredOtherCandidates.asViews()
      }
      return otherContacts
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AdditionalNamedInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ExistingAdditionalNamedInsured) at AdditionalNamedInsuredsDV.pcf: line 88, column 127
    function label_12 () : java.lang.Object {
      return namedInsured
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=ExistingAdditionalNamedInsured) at AdditionalNamedInsuredsDV.pcf: line 88, column 127
    function toCreateAndAdd_13 (CheckedValues :  Object[]) : java.lang.Object {
      return period.addNewPolicyAddlNamedInsuredForContact_TDIC(namedInsured.AccountContact.Contact)
    }
    
    property get namedInsured () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends AdditionalNamedInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at AdditionalNamedInsuredsDV.pcf: line 114, column 127
    function label_19 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at AdditionalNamedInsuredsDV.pcf: line 114, column 127
    function toCreateAndAdd_20 (CheckedValues :  Object[]) : java.lang.Object {
      return period.addNewPolicyAddlNamedInsuredForContact_TDIC(otherContact.AccountContact.Contact)
    }
    
    property get otherContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends AdditionalNamedInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AdditionalNamedInsuredsDV.pcf: line 154, column 33
    function action_35 () : void {
      EditPolicyContactRolePopup.push(additionalNamedInsured, openForEdit)
    }
    
    // 'pickLocation' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function action_44 () : void {
      IndustryCodeSearchPopup.push(typekey.IndustryCodeType.TC_SIC, referenceDate, previousIndustryCode)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AdditionalNamedInsuredsDV.pcf: line 154, column 33
    function action_dest_36 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(additionalNamedInsured, openForEdit)
    }
    
    // 'pickLocation' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function action_dest_45 () : pcf.api.Destination {
      return pcf.IndustryCodeSearchPopup.createDestination(typekey.IndustryCodeType.TC_SIC, referenceDate, previousIndustryCode)
    }
    
    // 'value' attribute on TextCell (id=Relationship_Cell) at AdditionalNamedInsuredsDV.pcf: line 160, column 32
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      additionalNamedInsured.Relationship = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      (additionalNamedInsured.AccountContactRole as NamedInsured).IndustryCode = (__VALUE_TO_SET as entity.IndustryCode)
    }
    
    // 'value' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      additionalNamedInsured.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on Row at AdditionalNamedInsuredsDV.pcf: line 148, column 40
    function editable_65 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 135, column 38
    function initialValue_32 () : Jurisdiction {
      return gw.api.util.JurisdictionMappingUtil.getJurisdiction(additionalNamedInsured.AccountContactRole.AccountContact.Contact.PrimaryAddress)
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 139, column 40
    function initialValue_33 () : java.util.Date {
      return referenceState == null ? period.EditEffectiveDate : period.getReferenceDateForCurrentJob(referenceState)
    }
    
    // 'initialValue' attribute on Variable at AdditionalNamedInsuredsDV.pcf: line 143, column 38
    function initialValue_34 () : IndustryCode {
      return period.Job.NewTerm ? null : additionalNamedInsured.IndustryCode
    }
    
    // RowIterator at AdditionalNamedInsuredsDV.pcf: line 131, column 59
    function initializeVariables_66 () : void {
        referenceState = gw.api.util.JurisdictionMappingUtil.getJurisdiction(additionalNamedInsured.AccountContactRole.AccountContact.Contact.PrimaryAddress);
  referenceDate = referenceState == null ? period.EditEffectiveDate : period.getReferenceDateForCurrentJob(referenceState);
  previousIndustryCode = period.Job.NewTerm ? null : additionalNamedInsured.IndustryCode;

    }
    
    // 'inputConversion' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function inputConversion_47 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.findCode(VALUE, IndustryCodeType.TC_SIC)
    }
    
    // 'inputMask' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function inputMask_61 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.InputMask
    }
    
    // 'outputConversion' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function outputConversion_48 (VALUE :  entity.IndustryCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'regex' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function regex_62 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.Regex
    }
    
    // 'requestValidationExpression' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function requestValidationExpression_49 (VALUE :  entity.IndustryCode) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.validateValue(VALUE, null, referenceDate)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdditionalNamedInsuredsDV.pcf: line 154, column 33
    function valueRoot_38 () : java.lang.Object {
      return additionalNamedInsured.AccountContactRole.AccountContact
    }
    
    // 'value' attribute on TextCell (id=Relationship_Cell) at AdditionalNamedInsuredsDV.pcf: line 160, column 32
    function valueRoot_42 () : java.lang.Object {
      return additionalNamedInsured
    }
    
    // 'value' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function valueRoot_52 () : java.lang.Object {
      return (additionalNamedInsured.AccountContactRole as NamedInsured)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdditionalNamedInsuredsDV.pcf: line 154, column 33
    function value_37 () : java.lang.String {
      return additionalNamedInsured.AccountContactRole.AccountContact.fullNameWithProfessionalCredentials_TDIC
    }
    
    // 'value' attribute on TextCell (id=Relationship_Cell) at AdditionalNamedInsuredsDV.pcf: line 160, column 32
    function value_40 () : java.lang.String {
      return additionalNamedInsured.Relationship
    }
    
    // 'value' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function value_50 () : entity.IndustryCode {
      return (additionalNamedInsured.AccountContactRole as NamedInsured).IndustryCode
    }
    
    // 'value' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function value_58 () : java.lang.String {
      return additionalNamedInsured.TaxID
    }
    
    // 'visible' attribute on IndustryCodeCell (id=IndustryCode_Cell) at IndustryCodeWidget.xml: line 5, column 47
    function visible_46 () : java.lang.Boolean {
      return additionalNamedInsured.AccountContactRole.AccountContact.Company or additionalNamedInsured.AccountContactRole.AccountContact.Person
    }
    
    // 'visible' attribute on IndustryCodeCell (id=IndustryCode_Cell) at AdditionalNamedInsuredsDV.pcf: line 173, column 33
    function visible_56 () : java.lang.Boolean {
      return period.Policy.Product.isContactTypeSuitableForProductAccountType(Company)
    }
    
    // 'visible' attribute on TextCell (id=fein_Cell) at AdditionalNamedInsuredsDV.pcf: line 181, column 40
    function visible_63 () : java.lang.Boolean {
      return isWC7Line
    }
    
    property get additionalNamedInsured () : entity.PolicyAddlNamedInsured {
      return getIteratedValue(1) as entity.PolicyAddlNamedInsured
    }
    
    property get previousIndustryCode () : IndustryCode {
      return getVariableValue("previousIndustryCode", 1) as IndustryCode
    }
    
    property set previousIndustryCode ($arg :  IndustryCode) {
      setVariableValue("previousIndustryCode", 1, $arg)
    }
    
    property get referenceDate () : java.util.Date {
      return getVariableValue("referenceDate", 1) as java.util.Date
    }
    
    property set referenceDate ($arg :  java.util.Date) {
      setVariableValue("referenceDate", 1, $arg)
    }
    
    property get referenceState () : Jurisdiction {
      return getVariableValue("referenceState", 1) as Jurisdiction
    }
    
    property set referenceState ($arg :  Jurisdiction) {
      setVariableValue("referenceState", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/AdditionalNamedInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdditionalNamedInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at AdditionalNamedInsuredsDV.pcf: line 63, column 92
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at AdditionalNamedInsuredsDV.pcf: line 63, column 92
    function pickLocation_7 () : void {
      NewAdditionalNamedInsuredPopup.push(period, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}