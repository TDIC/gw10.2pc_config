package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_StartEREQuote.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_StartEREQuoteExpressions {
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_StartEREQuote.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_StartEREQuoteExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, latestQuote :  GLEREQuickQuote_TDIC) : int {
      return 0
    }
    
    // 'action' attribute on ButtonInput (id=erebutton_Input) at TDIC_StartEREQuote.pcf: line 75, column 106
    function action_12 () : void {
      latestQuote = quoteUIHelper.requestEREQuote(policyPeriod, effectiveDate)
    }
    
    // 'afterCancel' attribute on Page (id=TDIC_StartEREQuote) at TDIC_StartEREQuote.pcf: line 14, column 97
    function afterCancel_19 () : void {
      PolicyFileForward.go(policyPeriod.PolicyNumber)
    }
    
    // 'afterCancel' attribute on Page (id=TDIC_StartEREQuote) at TDIC_StartEREQuote.pcf: line 14, column 97
    function afterCancel_dest_20 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(policyPeriod.PolicyNumber)
    }
    
    // 'available' attribute on ButtonInput (id=erebutton_Input) at TDIC_StartEREQuote.pcf: line 75, column 106
    function available_11 () : java.lang.Boolean {
      return quoteUIHelper.canStartEREQuote(policyPeriod.Policy, effectiveDate) == null and latestQuote == null
    }
    
    // 'available' attribute on DateInput (id=EffectiveDate_Input) at TDIC_StartEREQuote.pcf: line 61, column 35
    function available_4 () : java.lang.Boolean {
      return latestQuote == null
    }
    
    // 'canVisit' attribute on Page (id=TDIC_StartEREQuote) at TDIC_StartEREQuote.pcf: line 14, column 97
    static function canVisit_21 (latestQuote :  GLEREQuickQuote_TDIC, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.view(policyPeriod) and policyPeriod.Policy.Issued and policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
    }
    
    // 'def' attribute on PanelRef at TDIC_StartEREQuote.pcf: line 83, column 40
    function def_onEnter_17 (def :  pcf.TDIC_EREQuoteDetailPanelSet) : void {
      def.onEnter(latestQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_StartEREQuote.pcf: line 83, column 40
    function def_refreshVariables_18 (def :  pcf.TDIC_EREQuoteDetailPanelSet) : void {
      def.refreshVariables(latestQuote)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at TDIC_StartEREQuote.pcf: line 61, column 35
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      effectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at TDIC_StartEREQuote.pcf: line 61, column 35
    function editable_5 () : java.lang.Boolean {
      return isEffDateEditable
    }
    
    // 'initialValue' attribute on Variable at TDIC_StartEREQuote.pcf: line 23, column 30
    function initialValue_0 () : java.util.Date {
      return policyPeriod.EditEffectiveDate
    }
    
    // 'initialValue' attribute on Variable at TDIC_StartEREQuote.pcf: line 28, column 28
    function initialValue_1 () : PolicyPeriod {
      return tdic.pc.config.job.ere.GLEREQuoteUIHelper.getInForcePeriodWithBasedOn(policyPeriod, effectiveDate)
    }
    
    // 'label' attribute on Verbatim (id=ErrorMessage) at TDIC_StartEREQuote.pcf: line 46, column 25
    function label_3 () : java.lang.String {
      return DisplayKey.get("TDIC.Web.EREQuickQuote.StartEREQuickQuote.Error", quoteUIHelper.canStartEREQuote(policyPeriod.Policy, effectiveDate))
    }
    
    // 'parent' attribute on Page (id=TDIC_StartEREQuote) at TDIC_StartEREQuote.pcf: line 14, column 97
    static function parent_22 (latestQuote :  GLEREQuickQuote_TDIC, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod, policyPeriod.EditEffectiveDate)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at TDIC_StartEREQuote.pcf: line 61, column 35
    function value_6 () : java.util.Date {
      return effectiveDate
    }
    
    // 'visible' attribute on PanelDivider at TDIC_StartEREQuote.pcf: line 79, column 40
    function visible_14 () : java.lang.Boolean {
      return latestQuote != null
    }
    
    // 'visible' attribute on Verbatim (id=ErrorMessage) at TDIC_StartEREQuote.pcf: line 46, column 25
    function visible_2 () : java.lang.Boolean {
      return quoteUIHelper.canStartEREQuote(policyPeriod.Policy, effectiveDate) != null
    }
    
    override property get CurrentLocation () : pcf.TDIC_StartEREQuote {
      return super.CurrentLocation as pcf.TDIC_StartEREQuote
    }
    
    property get effectiveDate () : java.util.Date {
      return getVariableValue("effectiveDate", 0) as java.util.Date
    }
    
    property set effectiveDate ($arg :  java.util.Date) {
      setVariableValue("effectiveDate", 0, $arg)
    }
    
    property get inForcePeriod () : PolicyPeriod {
      return getVariableValue("inForcePeriod", 0) as PolicyPeriod
    }
    
    property set inForcePeriod ($arg :  PolicyPeriod) {
      setVariableValue("inForcePeriod", 0, $arg)
    }
    
    property get isEffDateEditable () : Boolean {
      return getVariableValue("isEffDateEditable", 0) as Boolean
    }
    
    property set isEffDateEditable ($arg :  Boolean) {
      setVariableValue("isEffDateEditable", 0, $arg)
    }
    
    property get latestQuote () : GLEREQuickQuote_TDIC {
      return getVariableValue("latestQuote", 0) as GLEREQuickQuote_TDIC
    }
    
    property set latestQuote ($arg :  GLEREQuickQuote_TDIC) {
      setVariableValue("latestQuote", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get quoteUIHelper () : tdic.pc.config.job.ere.GLEREQuoteUIHelper {
      return getVariableValue("quoteUIHelper", 0) as tdic.pc.config.job.ere.GLEREQuoteUIHelper
    }
    
    property set quoteUIHelper ($arg :  tdic.pc.config.job.ere.GLEREQuoteUIHelper) {
      setVariableValue("quoteUIHelper", 0, $arg)
    }
    
    
  }
  
  
}