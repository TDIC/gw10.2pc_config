package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7LaborContractorInfoDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7LaborContractorInfoDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7LaborContractorInfoDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7LaborContractorInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at WC7LaborContractorInfoDV.default.pcf: line 49, column 47
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at WC7LaborContractorInfoDV.default.pcf: line 55, column 48
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at WC7LaborContractorInfoDV.default.pcf: line 20, column 22
    function initialValue_0 () : String {
      return inclusionType == Inclusion.TC_INCL ? DisplayKey.get("Java.ProductModel.Name.Condition") : DisplayKey.get("Java.ProductModel.Name.Exclusion")
    }
    
    // 'label' attribute on TextInput (id=ScheduleParent_Input) at WC7LaborContractorInfoDV.default.pcf: line 26, column 43
    function label_1 () : java.lang.Object {
      return scheduleLabel
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function valueRange_10 () : java.lang.Object {
      return detail.Branch.WC7Line.stateFilterFor(clause.Pattern).TypeKeys
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function valueRoot_9 () : java.lang.Object {
      return detail
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at WC7LaborContractorInfoDV.default.pcf: line 49, column 47
    function value_14 () : java.util.Date {
      return detail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at WC7LaborContractorInfoDV.default.pcf: line 55, column 48
    function value_18 () : java.util.Date {
      return detail.ContractExpirationDate
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7LaborContractorInfoDV.default.pcf: line 26, column 43
    function value_2 () : gw.api.domain.Clause {
      return clause
    }
    
    // 'value' attribute on TypeKeyInput (id=Inclusion_Input) at WC7LaborContractorInfoDV.default.pcf: line 32, column 40
    function value_5 () : typekey.Inclusion {
      return inclusionType
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function value_7 () : typekey.Jurisdiction {
      return detail.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7LaborContractorInfoDV.default.pcf: line 41, column 42
    function verifyValueRange_12 () : void {
      var __valueRangeArg = detail.Branch.WC7Line.stateFilterFor(clause.Pattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    property get clause () : gw.api.domain.Clause {
      return getRequireValue("clause", 0) as gw.api.domain.Clause
    }
    
    property set clause ($arg :  gw.api.domain.Clause) {
      setRequireValue("clause", 0, $arg)
    }
    
    property get detail () : entity.WC7LaborContactDetail {
      return getRequireValue("detail", 0) as entity.WC7LaborContactDetail
    }
    
    property set detail ($arg :  entity.WC7LaborContactDetail) {
      setRequireValue("detail", 0, $arg)
    }
    
    property get inclusionType () : Inclusion {
      return getRequireValue("inclusionType", 0) as Inclusion
    }
    
    property set inclusionType ($arg :  Inclusion) {
      setRequireValue("inclusionType", 0, $arg)
    }
    
    property get scheduleLabel () : String {
      return getVariableValue("scheduleLabel", 0) as String
    }
    
    property set scheduleLabel ($arg :  String) {
      setVariableValue("scheduleLabel", 0, $arg)
    }
    
    
  }
  
  
}