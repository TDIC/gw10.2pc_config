package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/losshistory/ManualLossHistoryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ManualLossHistoryLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/losshistory/ManualLossHistoryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ManualLossHistoryLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ManualLossHistoryLV.pcf: line 109, column 51
    function currency_84 () : typekey.Currency {
      return policyPeriod.PreferredCoverageCurrency
    }
    
    // 'value' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.PolicyLinePattern = (__VALUE_TO_SET as gw.api.productmodel.PolicyLinePattern)
    }
    
    // 'value' attribute on DateCell (id=OccurrenceDate_Cell) at ManualLossHistoryLV.pcf: line 36, column 52
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.OccurrenceDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ManualLossHistoryLV.pcf: line 43, column 55
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.PolicyNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Carrier_Cell) at ManualLossHistoryLV.pcf: line 49, column 50
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.Carrier_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodEffDate_Cell) at ManualLossHistoryLV.pcf: line 56, column 24
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.PolicyPeriodEffDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodExpDate_Cell) at ManualLossHistoryLV.pcf: line 63, column 24
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.PolicyPeriodExpDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=RunDate_Cell) at ManualLossHistoryLV.pcf: line 68, column 50
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.RunDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=ClaimNumber_Cell) at ManualLossHistoryLV.pcf: line 74, column 54
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.ClaimNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ManualLossHistoryLV.pcf: line 80, column 51
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.LossDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ManualLossHistoryLV.pcf: line 87, column 24
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.Status_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on DateCell (id=ClosedDate_Cell) at ManualLossHistoryLV.pcf: line 101, column 53
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.ClosedDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ManualLossHistoryLV.pcf: line 109, column 51
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.TotalIncurred = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountPaid_Cell) at ManualLossHistoryLV.pcf: line 117, column 48
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.AmountPaid = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountReserved_Cell) at ManualLossHistoryLV.pcf: line 125, column 48
    function defaultSetter_92 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossHistoryEntry.AmountResv = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'validationExpression' attribute on DateCell (id=ClosedDate_Cell) at ManualLossHistoryLV.pcf: line 101, column 53
    function validationExpression_75 () : java.lang.Object {
      return (lossHistoryEntry.Status_TDIC == "Closed" && lossHistoryEntry.ClosedDate_TDIC == null)? "You must enter a date in the Closed Date" : null
    }
    
    // 'valueRange' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function valueRange_27 () : java.lang.Object {
      return policyPeriod.Policy.Product.LinePatterns
    }
    
    // 'valueRange' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function valueRange_71 () : java.lang.Object {
      return {"Open","Closed"}
    }
    
    // 'value' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function valueRoot_26 () : java.lang.Object {
      return lossHistoryEntry
    }
    
    // 'value' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function value_24 () : gw.api.productmodel.PolicyLinePattern {
      return lossHistoryEntry.PolicyLinePattern
    }
    
    // 'value' attribute on DateCell (id=OccurrenceDate_Cell) at ManualLossHistoryLV.pcf: line 36, column 52
    function value_32 () : java.util.Date {
      return lossHistoryEntry.OccurrenceDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ManualLossHistoryLV.pcf: line 43, column 55
    function value_36 () : java.lang.String {
      return lossHistoryEntry.PolicyNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=Carrier_Cell) at ManualLossHistoryLV.pcf: line 49, column 50
    function value_40 () : java.lang.String {
      return lossHistoryEntry.Carrier_TDIC
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodEffDate_Cell) at ManualLossHistoryLV.pcf: line 56, column 24
    function value_44 () : java.util.Date {
      return lossHistoryEntry.PolicyPeriodEffDate_TDIC
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodExpDate_Cell) at ManualLossHistoryLV.pcf: line 63, column 24
    function value_48 () : java.util.Date {
      return lossHistoryEntry.PolicyPeriodExpDate_TDIC
    }
    
    // 'value' attribute on DateCell (id=RunDate_Cell) at ManualLossHistoryLV.pcf: line 68, column 50
    function value_52 () : java.util.Date {
      return lossHistoryEntry.RunDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClaimNumber_Cell) at ManualLossHistoryLV.pcf: line 74, column 54
    function value_56 () : java.lang.String {
      return lossHistoryEntry.ClaimNumber_TDIC
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ManualLossHistoryLV.pcf: line 80, column 51
    function value_60 () : java.util.Date {
      return lossHistoryEntry.LossDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ManualLossHistoryLV.pcf: line 87, column 24
    function value_64 () : java.lang.String {
      return lossHistoryEntry.Description
    }
    
    // 'value' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function value_68 () : String {
      return lossHistoryEntry.Status_TDIC
    }
    
    // 'value' attribute on DateCell (id=ClosedDate_Cell) at ManualLossHistoryLV.pcf: line 101, column 53
    function value_76 () : java.util.Date {
      return lossHistoryEntry.ClosedDate_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ManualLossHistoryLV.pcf: line 109, column 51
    function value_81 () : gw.pl.currency.MonetaryAmount {
      return lossHistoryEntry.TotalIncurred
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountPaid_Cell) at ManualLossHistoryLV.pcf: line 117, column 48
    function value_86 () : gw.pl.currency.MonetaryAmount {
      return lossHistoryEntry.AmountPaid
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountReserved_Cell) at ManualLossHistoryLV.pcf: line 125, column 48
    function value_91 () : gw.pl.currency.MonetaryAmount {
      return lossHistoryEntry.AmountResv
    }
    
    // 'valueRange' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function verifyValueRangeIsAllowedType_28 ($$arg :  gw.api.productmodel.PolicyLinePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function verifyValueRangeIsAllowedType_72 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function verifyValueRangeIsAllowedType_72 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function verifyValueRange_29 () : void {
      var __valueRangeArg = policyPeriod.Policy.Product.LinePatterns
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function verifyValueRange_73 () : void {
      var __valueRangeArg = {"Open","Closed"}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_72(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function visible_30 () : java.lang.Boolean {
      return policyPeriod.MultiLine
    }
    
    property get lossHistoryEntry () : entity.LossHistoryEntry {
      return getIteratedValue(1) as entity.LossHistoryEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/losshistory/ManualLossHistoryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ManualLossHistoryLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'autoAdd' attribute on RowIterator at ManualLossHistoryLV.pcf: line 22, column 45
    function autoAdd_23 () : java.lang.Boolean {
      return false//policyPeriod.Policy.PriorLosses.Count == 0
    }
    
    // 'value' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function sortValue_0 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.PolicyLinePattern
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ManualLossHistoryLV.pcf: line 87, column 24
    function sortValue_10 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.Description
    }
    
    // 'value' attribute on RangeCell (id=Status_Cell) at ManualLossHistoryLV.pcf: line 95, column 31
    function sortValue_11 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.Status_TDIC
    }
    
    // 'value' attribute on DateCell (id=ClosedDate_Cell) at ManualLossHistoryLV.pcf: line 101, column 53
    function sortValue_12 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.ClosedDate_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalIncurred_Cell) at ManualLossHistoryLV.pcf: line 109, column 51
    function sortValue_13 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.TotalIncurred
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountPaid_Cell) at ManualLossHistoryLV.pcf: line 117, column 48
    function sortValue_14 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.AmountPaid
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountReserved_Cell) at ManualLossHistoryLV.pcf: line 125, column 48
    function sortValue_15 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.AmountResv
    }
    
    // 'value' attribute on DateCell (id=OccurrenceDate_Cell) at ManualLossHistoryLV.pcf: line 36, column 52
    function sortValue_2 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.OccurrenceDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ManualLossHistoryLV.pcf: line 43, column 55
    function sortValue_3 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.PolicyNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=Carrier_Cell) at ManualLossHistoryLV.pcf: line 49, column 50
    function sortValue_4 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.Carrier_TDIC
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodEffDate_Cell) at ManualLossHistoryLV.pcf: line 56, column 24
    function sortValue_5 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.PolicyPeriodEffDate_TDIC
    }
    
    // 'value' attribute on DateCell (id=PolicyPeriodExpDate_Cell) at ManualLossHistoryLV.pcf: line 63, column 24
    function sortValue_6 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.PolicyPeriodExpDate_TDIC
    }
    
    // 'value' attribute on DateCell (id=RunDate_Cell) at ManualLossHistoryLV.pcf: line 68, column 50
    function sortValue_7 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.RunDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClaimNumber_Cell) at ManualLossHistoryLV.pcf: line 74, column 54
    function sortValue_8 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.ClaimNumber_TDIC
    }
    
    // 'value' attribute on DateCell (id=LossDate_Cell) at ManualLossHistoryLV.pcf: line 80, column 51
    function sortValue_9 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.LossDate_TDIC
    }
    
    // '$$sumValue' attribute on RowIterator at ManualLossHistoryLV.pcf: line 109, column 51
    function sumValueRoot_18 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry
    }
    
    // 'footerSumValue' attribute on RowIterator at ManualLossHistoryLV.pcf: line 109, column 51
    function sumValue_17 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.TotalIncurred
    }
    
    // 'footerSumValue' attribute on RowIterator at ManualLossHistoryLV.pcf: line 117, column 48
    function sumValue_19 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.AmountPaid
    }
    
    // 'footerSumValue' attribute on RowIterator at ManualLossHistoryLV.pcf: line 125, column 48
    function sumValue_21 (lossHistoryEntry :  entity.LossHistoryEntry) : java.lang.Object {
      return lossHistoryEntry.AmountResv
    }
    
    // 'toAdd' attribute on RowIterator at ManualLossHistoryLV.pcf: line 22, column 45
    function toAdd_96 (lossHistoryEntry :  entity.LossHistoryEntry) : void {
      policyPeriod.Policy.addToPriorLosses(lossHistoryEntry)
    }
    
    // 'toRemove' attribute on RowIterator at ManualLossHistoryLV.pcf: line 22, column 45
    function toRemove_97 (lossHistoryEntry :  entity.LossHistoryEntry) : void {
      policyPeriod.Policy.removeFromPriorLosses(lossHistoryEntry)
    }
    
    // 'value' attribute on RowIterator at ManualLossHistoryLV.pcf: line 22, column 45
    function value_98 () : entity.LossHistoryEntry[] {
      return policyPeriod.Policy.PriorLosses
    }
    
    // 'visible' attribute on RangeCell (id=PolicyLine_Cell) at ManualLossHistoryLV.pcf: line 31, column 45
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.MultiLine
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}