package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GLSchoolCodeSearch_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GLSchoolCodeSearch_TDICPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLSchoolCodeSearch_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GLSchoolCodeSearch_TDICPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 1
    }
    
    static function __constructorIndex (term :  gw.api.domain.covterm.CovTerm) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.GLSchoolCodeSearch_TDICPopup {
      return super.CurrentLocation as pcf.GLSchoolCodeSearch_TDICPopup
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getVariableValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setVariableValue("term", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLSchoolCodeSearch_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickValue' attribute on RowIterator at GLSchoolCodeSearch_TDICPopup.pcf: line 64, column 92
    function pickValue_18 () : entity.GLSchoolCode_TDIC {
      return gLSchoolCode
    }
    
    // 'value' attribute on TextCell (id=SchoolCode_Cell) at GLSchoolCodeSearch_TDICPopup.pcf: line 70, column 33
    function valueRoot_13 () : java.lang.Object {
      return gLSchoolCode
    }
    
    // 'value' attribute on TextCell (id=SchoolCode_Cell) at GLSchoolCodeSearch_TDICPopup.pcf: line 70, column 33
    function value_12 () : java.lang.String {
      return gLSchoolCode.SchoolCode
    }
    
    // 'value' attribute on TextCell (id=SchoolName_Cell) at GLSchoolCodeSearch_TDICPopup.pcf: line 76, column 52
    function value_15 () : java.lang.String {
      return gLSchoolCode.SchoolName
    }
    
    property get gLSchoolCode () : entity.GLSchoolCode_TDIC {
      return getIteratedValue(2) as entity.GLSchoolCode_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLSchoolCodeSearch_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends GLSchoolCodeSearch_TDICPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at GLSchoolCodeSearch_TDICPopup.pcf: line 47, column 49
    function def_onEnter_8 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at GLSchoolCodeSearch_TDICPopup.pcf: line 47, column 49
    function def_refreshVariables_9 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=SchoolCode_Input) at GLSchoolCodeSearch_TDICPopup.pcf: line 37, column 52
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SchoolCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=SchoolName_Input) at GLSchoolCodeSearch_TDICPopup.pcf: line 43, column 52
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SchoolName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel at GLSchoolCodeSearch_TDICPopup.pcf: line 27, column 87
    function searchCriteria_21 () : tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC {
      return new tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC()
    }
    
    // 'search' attribute on SearchPanel at GLSchoolCodeSearch_TDICPopup.pcf: line 27, column 87
    function search_20 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'value' attribute on TextCell (id=SchoolCode_Cell) at GLSchoolCodeSearch_TDICPopup.pcf: line 70, column 33
    function sortValue_10 (gLSchoolCode :  entity.GLSchoolCode_TDIC) : java.lang.Object {
      return gLSchoolCode.SchoolCode
    }
    
    // 'value' attribute on TextCell (id=SchoolName_Cell) at GLSchoolCodeSearch_TDICPopup.pcf: line 76, column 52
    function sortValue_11 (gLSchoolCode :  entity.GLSchoolCode_TDIC) : java.lang.Object {
      return gLSchoolCode.SchoolName
    }
    
    // 'value' attribute on TextInput (id=SchoolCode_Input) at GLSchoolCodeSearch_TDICPopup.pcf: line 37, column 52
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=SchoolCode_Input) at GLSchoolCodeSearch_TDICPopup.pcf: line 37, column 52
    function value_0 () : java.lang.String {
      return searchCriteria.SchoolCode
    }
    
    // 'value' attribute on RowIterator at GLSchoolCodeSearch_TDICPopup.pcf: line 64, column 92
    function value_19 () : gw.api.database.IQueryBeanResult<entity.GLSchoolCode_TDIC> {
      return gLSchoolCodes
    }
    
    // 'value' attribute on TextInput (id=SchoolName_Input) at GLSchoolCodeSearch_TDICPopup.pcf: line 43, column 52
    function value_4 () : java.lang.String {
      return searchCriteria.SchoolName
    }
    
    property get gLSchoolCodes () : gw.api.database.IQueryBeanResult<GLSchoolCode_TDIC> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<GLSchoolCode_TDIC>
    }
    
    property get searchCriteria () : tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC {
      return getCriteriaValue(1) as tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC
    }
    
    property set searchCriteria ($arg :  tdic.pc.config.gl.GLSchoolCodeSearchCriteria_TDIC) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}