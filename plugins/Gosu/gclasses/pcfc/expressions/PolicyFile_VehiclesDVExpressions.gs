package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_VehiclesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends PolicyFile_VehiclesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_VehiclesDV.pcf: line 109, column 61
    function initialValue_41 () : gw.api.productmodel.CoveragePattern[] {
      return selectedVehicle == null ? null : physDamCovCategory.coveragePatternsForEntity(BusinessVehicle).whereSelectedOrAvailable(selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_VehiclesDV.pcf: line 114, column 61
    function initialValue_42 () : gw.api.productmodel.CoveragePattern[] {
      return selectedVehicle == null ? null : baPIPCoverageCat.coveragePatternsForEntity(BusinessVehicle).whereSelectedOrAvailable(selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyFile_VehiclesDV.pcf: line 124, column 34
    function sortBy_43 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=PhysDamCovIterator) at PolicyFile_VehiclesDV.pcf: line 121, column 67
    function value_151 () : gw.api.productmodel.CoveragePattern[] {
      return physDamCovCategoryCoveragePatterns
    }
    
    // 'value' attribute on InputIterator (id=PIPCovIterator) at PolicyFile_VehiclesDV.pcf: line 136, column 67
    function value_260 () : gw.api.productmodel.CoveragePattern[] {
      return baPIPCoverageCatCoveragePatterns
    }
    
    property get baPIPCoverageCatCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("baPIPCoverageCatCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set baPIPCoverageCatCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("baPIPCoverageCatCoveragePatterns", 1, $arg)
    }
    
    property get physDamCovCategoryCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("physDamCovCategoryCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set physDamCovCategoryCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("physDamCovCategoryCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_100 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_102 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_104 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_106 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_112 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_114 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_116 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_118 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_120 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_122 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_124 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_126 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_128 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_130 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_132 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_134 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_136 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_138 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_140 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_142 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_144 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_146 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_148 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_44 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_46 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_48 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_50 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_52 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_54 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_56 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_58 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_62 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_64 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_66 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_68 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_70 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_72 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_74 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_76 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_78 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_80 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_82 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_84 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_86 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_88 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_90 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_92 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_94 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_96 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_onEnter_98 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, CurrentLocation.InEditMode)
    }
    
    // 'mode' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 127, column 52
    function mode_150 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_153 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_155 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_157 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_159 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_161 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_163 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_165 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_167 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_169 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_171 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_173 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_175 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_177 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_179 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_181 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_183 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_185 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_187 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_189 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_191 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_193 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_195 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_197 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_199 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_201 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_203 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_205 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_207 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_209 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_211 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_213 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_215 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_219 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_221 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_223 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_225 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_227 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_229 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_231 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_233 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_235 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_237 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_239 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_241 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_243 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_245 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_247 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_249 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_251 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_253 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_255 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_onEnter_257 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_222 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_226 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_228 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_230 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_232 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_234 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_236 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_238 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_240 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_242 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_244 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_246 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_248 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_250 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_252 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_254 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_256 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function def_refreshVariables_258 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, selectedVehicle, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyFile_VehiclesDV.pcf: line 142, column 52
    function mode_259 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyFile_VehiclesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=VehicleNumber_Cell) at PolicyFile_VehiclesDV.pcf: line 40, column 44
    function valueRoot_11 () : java.lang.Object {
      return vehicle
    }
    
    // 'value' attribute on TextCell (id=VehicleNumber_Cell) at PolicyFile_VehiclesDV.pcf: line 40, column 44
    function value_10 () : java.lang.Integer {
      return vehicle.VehicleNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at PolicyFile_VehiclesDV.pcf: line 46, column 46
    function value_13 () : typekey.VehicleType {
      return vehicle.VehicleType
    }
    
    // 'value' attribute on TextCell (id=Year_Cell) at PolicyFile_VehiclesDV.pcf: line 52, column 44
    function value_16 () : java.lang.Integer {
      return vehicle.Year
    }
    
    // 'value' attribute on TextCell (id=Make_Cell) at PolicyFile_VehiclesDV.pcf: line 57, column 35
    function value_19 () : java.lang.String {
      return vehicle.Make
    }
    
    // 'value' attribute on TextCell (id=Model_Cell) at PolicyFile_VehiclesDV.pcf: line 62, column 36
    function value_22 () : java.lang.String {
      return vehicle.Model
    }
    
    // 'value' attribute on TypeKeyCell (id=BodyType_Cell) at PolicyFile_VehiclesDV.pcf: line 68, column 43
    function value_25 () : typekey.BodyType {
      return vehicle.BodyType
    }
    
    // 'value' attribute on TextCell (id=Vin_Cell) at PolicyFile_VehiclesDV.pcf: line 73, column 34
    function value_28 () : java.lang.String {
      return vehicle.Vin
    }
    
    property get vehicle () : entity.BusinessVehicle {
      return getIteratedValue(1) as entity.BusinessVehicle
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/ba/policyfile/PolicyFile_VehiclesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_VehiclesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at PolicyFile_VehiclesDV.pcf: line 81, column 42
    function available_32 () : java.lang.Boolean {
      return selectedVehicle != null
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 153, column 29
    function def_onEnter_261 (def :  pcf.AdditionalCoveragesDV) : void {
      def.onEnter(selectedVehicle, new String[]{"BAPOwnedPhysDamGrp", "BAPIPCoverageCat"}, false)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at PolicyFile_VehiclesDV.pcf: line 81, column 42
    function def_onEnter_34 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(selectedVehicle, false, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 90, column 33
    function def_onEnter_36 (def :  pcf.VehicleDV) : void {
      def.onEnter(selectedVehicle, policyPeriod, policyPeriod.BusinessAutoLine, false, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 94, column 74
    function def_onEnter_38 (def :  pcf.AdditionalInterestDetailsDV) : void {
      def.onEnter(selectedVehicle, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 153, column 29
    function def_refreshVariables_262 (def :  pcf.AdditionalCoveragesDV) : void {
      def.refreshVariables(selectedVehicle, new String[]{"BAPOwnedPhysDamGrp", "BAPIPCoverageCat"}, false)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at PolicyFile_VehiclesDV.pcf: line 81, column 42
    function def_refreshVariables_35 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(selectedVehicle, false, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 90, column 33
    function def_refreshVariables_37 (def :  pcf.VehicleDV) : void {
      def.refreshVariables(selectedVehicle, policyPeriod, policyPeriod.BusinessAutoLine, false, null)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_VehiclesDV.pcf: line 94, column 74
    function def_refreshVariables_39 (def :  pcf.AdditionalInterestDetailsDV) : void {
      def.refreshVariables(selectedVehicle, false)
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_VehiclesDV.pcf: line 15, column 45
    function initialValue_0 () : productmodel.BusinessAutoLine {
      return policyPeriod.BusinessAutoLine
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_VehiclesDV.pcf: line 19, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return businessAutoLine.Pattern.getCoverageCategoryByPublicId("BAPOwnedPhysDamGrp")
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_VehiclesDV.pcf: line 23, column 52
    function initialValue_2 () : gw.api.productmodel.CoverageCategory {
      return businessAutoLine.Pattern.getCoverageCategoryByPublicId("BAPIPCoverageCat")
    }
    
    // 'sortBy' attribute on TextCell (id=VehicleNumber_Cell) at PolicyFile_VehiclesDV.pcf: line 40, column 44
    function sortValue_3 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.VehicleNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at PolicyFile_VehiclesDV.pcf: line 46, column 46
    function sortValue_4 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.VehicleType
    }
    
    // 'value' attribute on TextCell (id=Year_Cell) at PolicyFile_VehiclesDV.pcf: line 52, column 44
    function sortValue_5 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.Year
    }
    
    // 'value' attribute on TextCell (id=Make_Cell) at PolicyFile_VehiclesDV.pcf: line 57, column 35
    function sortValue_6 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.Make
    }
    
    // 'value' attribute on TextCell (id=Model_Cell) at PolicyFile_VehiclesDV.pcf: line 62, column 36
    function sortValue_7 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.Model
    }
    
    // 'value' attribute on TypeKeyCell (id=BodyType_Cell) at PolicyFile_VehiclesDV.pcf: line 68, column 43
    function sortValue_8 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.BodyType
    }
    
    // 'value' attribute on TextCell (id=Vin_Cell) at PolicyFile_VehiclesDV.pcf: line 73, column 34
    function sortValue_9 (vehicle :  entity.BusinessVehicle) : java.lang.Object {
      return vehicle.Vin
    }
    
    // 'title' attribute on TitleBar at PolicyFile_VehiclesDV.pcf: line 103, column 53
    function title_40 () : java.lang.String {
      return physDamCovCategory.DisplayName
    }
    
    // 'value' attribute on RowIterator at PolicyFile_VehiclesDV.pcf: line 31, column 46
    function value_31 () : entity.BusinessVehicle[] {
      return businessAutoLine.Vehicles
    }
    
    property get baPIPCoverageCat () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("baPIPCoverageCat", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set baPIPCoverageCat ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("baPIPCoverageCat", 0, $arg)
    }
    
    property get businessAutoLine () : productmodel.BusinessAutoLine {
      return getVariableValue("businessAutoLine", 0) as productmodel.BusinessAutoLine
    }
    
    property set businessAutoLine ($arg :  productmodel.BusinessAutoLine) {
      setVariableValue("businessAutoLine", 0, $arg)
    }
    
    property get physDamCovCategory () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("physDamCovCategory", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set physDamCovCategory ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("physDamCovCategory", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get selectedVehicle () : BusinessVehicle {
      return getCurrentSelection(0) as BusinessVehicle
    }
    
    property set selectedVehicle ($arg :  BusinessVehicle) {
      setCurrentSelection(0, $arg)
    }
    
    
  }
  
  
}