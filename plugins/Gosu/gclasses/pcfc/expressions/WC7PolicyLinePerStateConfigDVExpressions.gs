package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7PolicyLinePerStateConfigDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7PolicyLinePerStateConfigDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_112 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_114 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_116 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_118 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_120 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_122 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_124 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_126 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_22 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_24 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_26 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_28 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_30 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_32 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_34 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_36 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_62 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_64 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_66 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_68 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_70 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_72 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_74 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_76 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_78 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_80 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_82 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_84 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, jurisdiction, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 82, column 82
    function mode_128 () : java.lang.Object {
      return coveragePattern.CodeIdentifier
    }
    
    // 'visible' attribute on Label at WC7PolicyLinePerStateConfigDV.pcf: line 78, column 82
    function visible_20 () : java.lang.Boolean {
      return jurisdiction.isCoverageSelectedOrAvailable(coveragePattern)
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends WC7PolicyLinePerStateConfigDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function defaultSetter_137 (__VALUE_TO_SET :  java.lang.Object) : void {
      officialID.OfficialIDValueUnenc_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function label_134 () : java.lang.Object {
      return officialID.OfficialIDInsuredAndType
    }
    
    // 'onChange' attribute on PostOnChange at WC7PolicyLinePerStateConfigDV.pcf: line 105, column 91
    function onChange_132 () : void {
      officialID.OfficialIDValue = officialID.OfficialIDValueUnenc_TDIC
    }
    
    // 'required' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function required_135 () : java.lang.Boolean {
      return officialID.isStateIDRequired(jurisdiction)
    }
    
    // 'validationExpression' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function validationExpression_133 () : java.lang.Object {
      return officialID.validateValue()
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function valueRoot_138 () : java.lang.Object {
      return officialID
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 103, column 40
    function value_136 () : java.lang.String {
      return officialID.OfficialIDValueUnenc_TDIC
    }
    
    property get officialID () : entity.OfficialID {
      return getIteratedValue(1) as entity.OfficialID
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends WC7PolicyLinePerStateConfigDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 129, column 49
    function def_onEnter_149 (def :  pcf.WC7ModifiersInputSet) : void {
      def.onEnter(getSplitModifiers(ratingPeriod.Start, ratingPeriod.End), wcLine.Branch, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at WC7PolicyLinePerStateConfigDV.pcf: line 129, column 49
    function def_refreshVariables_150 (def :  pcf.WC7ModifiersInputSet) : void {
      def.refreshVariables(getSplitModifiers(ratingPeriod.Start, ratingPeriod.End), wcLine.Branch, openForEdit)
    }
    
    // 'label' attribute on Label at WC7PolicyLinePerStateConfigDV.pcf: line 126, column 49
    function label_147 () : java.lang.String {
      return DisplayKey.get("Web.Policy.WC.Period", ratingPeriod.Number)
    }
    
    // 'visible' attribute on Label at WC7PolicyLinePerStateConfigDV.pcf: line 126, column 49
    function visible_146 () : java.lang.Boolean {
      return ratingPeriods.Count > 1
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(1) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7PolicyLinePerStateConfigDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextInput (id=Period_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 63, column 90
    function label_13 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.WC.Period", period.Number)
    }
    
    // 'value' attribute on TextInput (id=Period_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 63, column 90
    function value_14 () : java.lang.String {
      return "${period.Start.ShortFormat} - ${period.End.ShortFormat}"
    }
    
    property get period () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(1) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7PolicyLinePerStateConfigDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7PolicyLinePerStateConfigDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=UnsplitModifiers) at WC7PolicyLinePerStateConfigDV.pcf: line 116, column 32
    function def_onEnter_144 (def :  pcf.WC7ModifiersInputSet) : void {
      def.onEnter(getUnsplitModifiers(), wcLine.Branch, openForEdit)
    }
    
    // 'def' attribute on InputSetRef (id=UnsplitModifiers) at WC7PolicyLinePerStateConfigDV.pcf: line 116, column 32
    function def_refreshVariables_145 (def :  pcf.WC7ModifiersInputSet) : void {
      def.refreshVariables(getUnsplitModifiers(), wcLine.Branch, openForEdit)
    }
    
    // 'value' attribute on DateInput (id=AnniversaryDate_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 51, column 48
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdiction.AnniversaryDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=AnniversaryDate_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 51, column 48
    function editable_6 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    // 'initialValue' attribute on Variable at WC7PolicyLinePerStateConfigDV.pcf: line 21, column 28
    function initialValue_0 () : OfficialID[] {
      return jurisdiction != null ? wcLine.Branch.getNamedInsuredOfficialIDsForState(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction)): null
    }
    
    // 'initialValue' attribute on Variable at WC7PolicyLinePerStateConfigDV.pcf: line 25, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompStateCategory")
    }
    
    // 'initialValue' attribute on Variable at WC7PolicyLinePerStateConfigDV.pcf: line 30, column 56
    function initialValue_2 () : java.util.List<entity.WC7Modifier> {
      return jurisdiction.AllModifierVersions
    }
    
    // 'initialValue' attribute on Variable at WC7PolicyLinePerStateConfigDV.pcf: line 35, column 71
    function initialValue_3 () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return jurisdiction.RatingPeriods
    }
    
    // 'initialValue' attribute on Variable at WC7PolicyLinePerStateConfigDV.pcf: line 40, column 53
    function initialValue_4 () : gw.api.productmodel.CoveragePattern[] {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompStateCategory").coveragePatternsForEntity(WC7Jurisdiction).where(\ c -> jurisdiction?.isCoverageSelectedOrAvailable(c))
    }
    
    // 'onChange' attribute on PostOnChange at WC7PolicyLinePerStateConfigDV.pcf: line 53, column 42
    function onChange_5 () : void {
      updateAllBasis()
    }
    
    // 'requestValidationExpression' attribute on DateInput (id=AnniversaryDate_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 51, column 48
    function requestValidationExpression_7 (VALUE :  java.util.Date) : java.lang.Object {
      return validateAnniversaryDate(VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7PolicyLinePerStateConfigDV.pcf: line 94, column 26
    function sortBy_131 (officialID :  entity.OfficialID) : java.lang.Object {
      return officialID
    }
    
    // 'sortBy' attribute on IteratorSort at WC7PolicyLinePerStateConfigDV.pcf: line 75, column 26
    function sortBy_19 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on DateInput (id=AnniversaryDate_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 51, column 48
    function valueRoot_10 () : java.lang.Object {
      return jurisdiction
    }
    
    // 'value' attribute on InputIterator (id=WC7CoverageGroupIterator) at WC7PolicyLinePerStateConfigDV.pcf: line 72, column 59
    function value_129 () : gw.api.productmodel.CoveragePattern[] {
      return wc7GroupCoveragePatterns
    }
    
    // 'value' attribute on InputIterator (id=officialIDs) at WC7PolicyLinePerStateConfigDV.pcf: line 91, column 41
    function value_142 () : entity.OfficialID[] {
      return namedInsuredOfficialIDs
    }
    
    // 'value' attribute on InputIterator (id=SplitModifiers) at WC7PolicyLinePerStateConfigDV.pcf: line 122, column 57
    function value_151 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.toTypedArray()
    }
    
    // 'value' attribute on InputIterator (id=RatingPeriodsIterator) at WC7PolicyLinePerStateConfigDV.pcf: line 59, column 59
    function value_17 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.Count > 1 ? ratingPeriods.toTypedArray() : new gw.lob.wc7.rating.WC7RatingPeriod[0]
    }
    
    // 'value' attribute on DateInput (id=AnniversaryDate_Input) at WC7PolicyLinePerStateConfigDV.pcf: line 51, column 48
    function value_8 () : java.util.Date {
      return jurisdiction.AnniversaryDate
    }
    
    // 'visible' attribute on Label at WC7PolicyLinePerStateConfigDV.pcf: line 86, column 57
    function visible_130 () : java.lang.Boolean {
      return namedInsuredOfficialIDs.Count > 0
    }
    
    // 'visible' attribute on Label at WC7PolicyLinePerStateConfigDV.pcf: line 113, column 53
    function visible_143 () : java.lang.Boolean {
      return allModifierVersions.Count > 0
    }
    
    // 'visible' attribute on InputSet at WC7PolicyLinePerStateConfigDV.pcf: line 43, column 132
    function visible_18 () : java.lang.Boolean {
      return new gw.lob.wc7.WC7JurisdictionARD().hasSplittableARD(jurisdiction.Jurisdiction, jurisdiction.Branch.PeriodStart)
    }
    
    property get allModifierVersions () : java.util.List<entity.WC7Modifier> {
      return getVariableValue("allModifierVersions", 0) as java.util.List<entity.WC7Modifier>
    }
    
    property set allModifierVersions ($arg :  java.util.List<entity.WC7Modifier>) {
      setVariableValue("allModifierVersions", 0, $arg)
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get namedInsuredOfficialIDs () : OfficialID[] {
      return getVariableValue("namedInsuredOfficialIDs", 0) as OfficialID[]
    }
    
    property set namedInsuredOfficialIDs ($arg :  OfficialID[]) {
      setVariableValue("namedInsuredOfficialIDs", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get ratingPeriods () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return getVariableValue("ratingPeriods", 0) as java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>
    }
    
    property set ratingPeriods ($arg :  java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>) {
      setVariableValue("ratingPeriods", 0, $arg)
    }
    
    property get wc7GroupCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("wc7GroupCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set wc7GroupCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("wc7GroupCoveragePatterns", 0, $arg)
    }
    
    property get wc7StateGrp () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("wc7StateGrp", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set wc7StateGrp ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("wc7StateGrp", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getRequireValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    function getUnsplitModifiers() : java.util.List<WC7Modifier>{
      if (ratingPeriods.Count > 1) {
        return allModifierVersions.where( \ mod -> not ((mod.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary)
      } else { 
        return allModifierVersions
      }
    }
    
    function getSplitModifiers(start : java.util.Date, end : java.util.Date) : java.util.List<WC7Modifier>{
      return allModifierVersions.where( \ m -> ((m.Pattern) as gw.api.productmodel.ModifierPattern).SplitOnAnniversary 
        and gw.api.util.DateUtil.compareIgnoreTime(m.EffectiveDate, start) == 0
        and gw.api.util.DateUtil.compareIgnoreTime(m.ExpirationDate, end) == 0 )
    }
    
    function validateAnniversaryDate(date: java.util.Date): String {
      var policyPeriod = jurisdiction.Branch
      if (date > policyPeriod.PeriodStart) {
        return DisplayKey.get("Web.SubmissionWizard.PolicyInfo.AnniversaryDate.Error1")
      } else if (!(date > policyPeriod.PeriodStart.addYears(-1))) {
        return DisplayKey.get("Web.SubmissionWizard.PolicyInfo.AnniversaryDate.Error2")
      }
      return null
    }
    
    function updateAllBasis(){
      if((CurrentLocation as pcf.api.Wizard).saveDraft()){
        wcLine.updateWCExposuresAndModifiers()
      }
    }
    
    
  }
  
  
}