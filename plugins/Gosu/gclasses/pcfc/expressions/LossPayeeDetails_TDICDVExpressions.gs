package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LossPayeeDetails_TDICDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends LossPayeeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ExistingMortgagees) at LossPayeeDetails_TDICDV.pcf: line 76, column 107
    function label_10 () : java.lang.Object {
      return losspayee
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=ExistingMortgagees) at LossPayeeDetails_TDICDV.pcf: line 76, column 107
    function toCreateAndAdd_11 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addPolicyLossPayee_TDIC(losspayee.AccountContact.Contact)
    }
    
    property get losspayee () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends LossPayeeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=acctContact) at LossPayeeDetails_TDICDV.pcf: line 95, column 109
    function label_15 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=acctContact) at LossPayeeDetails_TDICDV.pcf: line 95, column 109
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addPolicyLossPayee_TDIC(acctContact.AccountContact.Contact)
    }
    
    property get acctContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends LossPayeeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at LossPayeeDetails_TDICDV.pcf: line 143, column 58
    function action_32 () : void {
      EditPolicyContactRolePopup.push(lossPayeeDetail, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at LossPayeeDetails_TDICDV.pcf: line 143, column 58
    function action_dest_33 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(lossPayeeDetail, openForEdit)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at LossPayeeDetails_TDICDV.pcf: line 122, column 55
    function checkBoxVisible_40 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at LossPayeeDetails_TDICDV.pcf: line 148, column 59
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      lossPayeeDetail.LoanNumberString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at LossPayeeDetails_TDICDV.pcf: line 136, column 62
    function valueRoot_30 () : java.lang.Object {
      return lossPayeeDetail
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at LossPayeeDetails_TDICDV.pcf: line 129, column 91
    function value_27 () : java.util.Date {
      return getEffectiveDate(lossPayeeDetail)//lossPayeeDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at LossPayeeDetails_TDICDV.pcf: line 136, column 62
    function value_29 () : java.util.Date {
      return lossPayeeDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at LossPayeeDetails_TDICDV.pcf: line 143, column 58
    function value_34 () : entity.PolicyLossPayee_TDIC {
      return lossPayeeDetail
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at LossPayeeDetails_TDICDV.pcf: line 148, column 59
    function value_36 () : java.lang.String {
      return lossPayeeDetail.LoanNumberString
    }
    
    property get lossPayeeDetail () : entity.PolicyLossPayee_TDIC {
      return getIteratedValue(1) as entity.PolicyLossPayee_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends LossPayeeDetails_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at LossPayeeDetails_TDICDV.pcf: line 53, column 87
    function label_5 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at LossPayeeDetails_TDICDV.pcf: line 53, column 87
    function pickLocation_6 () : void {
      NewLossPayee_TDICPopup.push(bopBuilding, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/LossPayeeDetails_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossPayeeDetails_TDICDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at LossPayeeDetails_TDICDV.pcf: line 110, column 61
    function allCheckedRowsAction_22 (CheckedValues :  entity.PolicyLossPayee_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at LossPayeeDetails_TDICDV.pcf: line 110, column 61
    function available_20 () : java.lang.Boolean {
      return policyPeriod.Job typeis PolicyChange
    }
    
    // 'available' attribute on DetailViewPanel (id=LossPayeeDetails_TDICDV) at LossPayeeDetails_TDICDV.pcf: line 9, column 83
    function available_43 () : java.lang.Boolean {
      return policyPeriod?.profileChange_TDIC
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at LossPayeeDetails_TDICDV.pcf: line 61, column 32
    function conversionExpression_8 (PickedValue :  Contact) : entity.PolicyLossPayee_TDIC {
      return bopBuilding.addPolicyLossPayee_TDIC(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at LossPayeeDetails_TDICDV.pcf: line 20, column 35
    function initialValue_0 () : entity.PolicyPeriod {
      return bopBuilding.PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at LossPayeeDetails_TDICDV.pcf: line 24, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at LossPayeeDetails_TDICDV.pcf: line 29, column 36
    function initialValue_2 () : AccountContactView[] {
      return null
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at LossPayeeDetails_TDICDV.pcf: line 66, column 30
    function label_13 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", DisplayKey.get("TDIC.entity.LossPayee"))
    }
    
    // 'label' attribute on Label at LossPayeeDetails_TDICDV.pcf: line 32, column 95
    function label_3 () : java.lang.String {
      return DisplayKey.get("Web.Policy.LossPayee_TDIC", bopBuilding.TypeLabel)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at LossPayeeDetails_TDICDV.pcf: line 61, column 32
    function pickLocation_9 () : void {
      ContactSearchPopup.push(typekey.AccountContactRole.TC_LOSSPAYEE_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at LossPayeeDetails_TDICDV.pcf: line 90, column 34
    function sortBy_14 (acctContact :  entity.AccountContactView) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at LossPayeeDetails_TDICDV.pcf: line 48, column 32
    function sortBy_4 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at LossPayeeDetails_TDICDV.pcf: line 129, column 91
    function sortValue_23 (lossPayeeDetail :  entity.PolicyLossPayee_TDIC) : java.lang.Object {
      return getEffectiveDate(lossPayeeDetail)//lossPayeeDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at LossPayeeDetails_TDICDV.pcf: line 136, column 62
    function sortValue_24 (lossPayeeDetail :  entity.PolicyLossPayee_TDIC) : java.lang.Object {
      return lossPayeeDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at LossPayeeDetails_TDICDV.pcf: line 143, column 58
    function sortValue_25 (lossPayeeDetail :  entity.PolicyLossPayee_TDIC) : java.lang.Object {
      return lossPayeeDetail
    }
    
    // 'value' attribute on TextCell (id=LoanNumber_Cell) at LossPayeeDetails_TDICDV.pcf: line 148, column 59
    function sortValue_26 (lossPayeeDetail :  entity.PolicyLossPayee_TDIC) : java.lang.Object {
      return lossPayeeDetail.LoanNumberString
    }
    
    // 'toRemove' attribute on RowIterator at LossPayeeDetails_TDICDV.pcf: line 122, column 55
    function toRemove_41 (lossPayeeDetail :  entity.PolicyLossPayee_TDIC) : void {
      bopBuilding.toRemoveFromBOPBldgLossPayees(lossPayeeDetail)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at LossPayeeDetails_TDICDV.pcf: line 71, column 57
    function value_12 () : entity.AccountContactView[] {
      return getExistingLossPayees()
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at LossPayeeDetails_TDICDV.pcf: line 87, column 57
    function value_17 () : entity.AccountContactView[] {
      return bopBuilding.LossPayeeOtherCandidates_TDIC.asViews()
    }
    
    // 'value' attribute on RowIterator at LossPayeeDetails_TDICDV.pcf: line 122, column 55
    function value_42 () : entity.PolicyLossPayee_TDIC[] {
      return bopBuilding.BOPBldgLossPayees
    }
    
    // 'value' attribute on AddMenuItemIterator at LossPayeeDetails_TDICDV.pcf: line 45, column 49
    function value_7 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC)
    }
    
    // 'visible' attribute on AddButton (id=AddContactsButton) at LossPayeeDetails_TDICDV.pcf: line 41, column 35
    function visible_18 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'removeVisible' attribute on IteratorButtons (id=IteratorButtons) at LossPayeeDetails_TDICDV.pcf: line 103, column 172
    function visible_19 () : java.lang.Boolean {
      return policyPeriod.Job typeis Submission or policyPeriod.Job typeis Renewal or bopBuilding.BOPBldgLossPayees.hasMatch(\lp -> lp.BasedOn == null)
    }
    
    // 'visible' attribute on DetailViewPanel (id=LossPayeeDetails_TDICDV) at LossPayeeDetails_TDICDV.pcf: line 9, column 83
    function visible_45 () : java.lang.Boolean {
      return !(policyPeriod.Job typeis Submission) or perm.System.viewsubmission
    }
    
    property get bopBuilding () : entity.BOPBuilding {
      return getRequireValue("bopBuilding", 0) as entity.BOPBuilding
    }
    
    property set bopBuilding ($arg :  entity.BOPBuilding) {
      setRequireValue("bopBuilding", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get existingLossPayee () : AccountContactView[] {
      return getVariableValue("existingLossPayee", 0) as AccountContactView[]
    }
    
    property set existingLossPayee ($arg :  AccountContactView[]) {
      setVariableValue("existingLossPayee", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function setExpirationDate(policyLossPayees : PolicyLossPayee_TDIC[]) {
      policyLossPayees.each(\lossPayee -> {
        if(lossPayee.ExpirationDate_TDIC == null) {
          lossPayee.ExpirationDate_TDIC = lossPayee.Branch.EditEffectiveDate
        }
      })
    }
    
    function getExistingLossPayees() : AccountContactView[] {
      if (existingLossPayee == null) {
        var addedContacts = bopBuilding.BOPBldgLossPayees*.AccountContactRole*.AccountContact
        var all = bopBuilding.ExistingLossPayees_TDIC
        var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
        existingLossPayee = remaining
      }
      return existingLossPayee
    }
    function getEffectiveDate(lossPayee : PolicyLossPayee_TDIC) : Date {
      var effDates = bopBuilding.Branch.AllEffectiveDates.toSet()
      for(effDate in effDates.order()) {
        var version = lossPayee.VersionList.AsOf(effDate)
        if(version != null) {
          return version.EffectiveDate
        }
      }
      return lossPayee.EffectiveDate
    }
    
    
  }
  
  
}