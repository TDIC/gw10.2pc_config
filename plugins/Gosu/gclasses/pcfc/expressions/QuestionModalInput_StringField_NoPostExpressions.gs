package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.StringField_NoPost.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_StringField_NoPostExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.StringField_NoPost.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      answerContainer.getAnswer(question).TextAnswer = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function editable_0 () : java.lang.Boolean {
      return question.isQuestionEditable_TDIC(answerContainer)
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function requestValidationExpression_1 (VALUE :  java.lang.String) : java.lang.Object {
      return question.getLengthForQuestion_TDIC(VALUE?.length)
    }
    
    // 'required' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function required_2 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function valueRoot_5 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on TextCell (id=StringFieldInput_NoPost_Cell) at QuestionModalInput.StringField_NoPost.pcf: line 23, column 20
    function value_3 () : java.lang.String {
      return answerContainer.getAnswer(question).TextAnswer
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}