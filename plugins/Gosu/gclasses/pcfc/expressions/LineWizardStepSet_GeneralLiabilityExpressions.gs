package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/job/LineWizardStepSet.GeneralLiability.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LineWizardStepSet_GeneralLiabilityExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/job/LineWizardStepSet.GeneralLiability.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LineWizardStepSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.GeneralLiability.pcf: line 26, column 73
    function beforeSave_0 () : void {
      gw.policy.PolicyLocationValidation.validateLocationsStep(policyPeriod.PolicyLocations)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function beforeSave_14 () : void {
      gw.lob.gl.GLLineValidation_TDIC.validateExposures(policyPeriod.GLLine)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function beforeSave_5 () : void {
      gw.lob.gl.GLLineValidation.validateGLScreenBeforeSave(policyPeriod.GLLine);gw.lob.gl.GLLineValidation_TDIC.validateGLScreenBeforeSave(policyPeriod.GLLine);
    }
    
    // 'onChange' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function onChange_12 () : void {
      gw.lob.gl.GLLineValidation_TDIC.validatePolicyContacts(policyPeriod.GLLine)
    }
    
    // 'onEnter' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function onEnter_15 () : void {
      if (policyPeriod.GLLine.GLExposuresWM.Count == 0) {policyPeriod.GLLine.addExposureWM()} 
    }
    
    // 'onEnter' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function onEnter_6 () : void {
      if (openForEdit) { gw.web.productmodel.ProductModelSyncIssuesHandler.sync(policyPeriod.GLLine.AllCoverables, policyPeriod.GLLine.AllModifiables, null, null, jobWizardHelper); gw.web.productmodel.ProductModelSyncIssuesHandler.syncConditions(policyPeriod.GLLine.AllCoverables, jobWizardHelper);gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions({policyPeriod.GLLine}, jobWizardHelper )};gw.web.job.policychange.StartPolicyChangeUIHelper.setEREReasons_TDIC(policyPeriod,policyPeriod.EditEffectiveDate);
    }
    
    // 'onExit' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.GeneralLiability.pcf: line 26, column 73
    function onExit_1 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'onExit' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function onExit_16 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'onExit' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function onExit_7 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'onFirstEnter' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function onFirstEnter_8 () : void {
      policyPeriod.setRetroDate_TDIC()
    }
    
    // 'save' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.GeneralLiability.pcf: line 26, column 73
    function save_2 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'screen' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function screen_onEnter_10 (def :  pcf.GeneralLiabilityScreen) : void {
      def.onEnter(job, policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function screen_onEnter_18 (def :  pcf.GeneralLiabilityEUScreen) : void {
      def.onEnter(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.GeneralLiability.pcf: line 26, column 73
    function screen_onEnter_3 (def :  pcf.LocationsScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper, policyPeriod.GLLine.SupportsNonSpecificLocations)
    }
    
    // 'screen' attribute on JobWizardStep (id=GLLine) at LineWizardStepSet.GeneralLiability.pcf: line 36, column 68
    function screen_refreshVariables_11 (def :  pcf.GeneralLiabilityScreen) : void {
      def.refreshVariables(job, policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function screen_refreshVariables_19 (def :  pcf.GeneralLiabilityEUScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.GeneralLiability.pcf: line 26, column 73
    function screen_refreshVariables_4 (def :  pcf.LocationsScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper, policyPeriod.GLLine.SupportsNonSpecificLocations)
    }
    
    // 'visible' attribute on JobWizardStep (id=GLLineEU) at LineWizardStepSet.GeneralLiability.pcf: line 45, column 87
    function visible_13 () : java.lang.Boolean {
      return policyPeriod.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}