package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/OfficialIDInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OfficialIDInputSet_personExpressions {
  @javax.annotation.Generated("config/web/pcf/account/OfficialIDInputSet.person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OfficialIDInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input_Input) at OfficialIDInputSet.person.pcf: line 41, column 48
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.ADANumberOfficialID_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=LicenseNumber_Input) at OfficialIDInputSet.person.pcf: line 47, column 42
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.LicenseNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseState_Input_Input) at OfficialIDInputSet.person.pcf: line 53, column 41
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.LicenseState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at OfficialIDInputSet.person.pcf: line 25, column 37
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.SSNOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseStatus_Input_Input) at OfficialIDInputSet.person.pcf: line 59, column 46
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.LicenseStatus_TDIC = (__VALUE_TO_SET as typekey.GlobalStatus_TDIC)
    }
    
    // 'value' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.Component_TDIC = (__VALUE_TO_SET as typekey.Component_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=CDAMembershipStatus_Input_Input) at OfficialIDInputSet.person.pcf: line 73, column 46
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.CDAMembershipStatus_TDIC = (__VALUE_TO_SET as typekey.GlobalStatus_TDIC)
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDVFEIN_Input_Input) at OfficialIDInputSet.person.pcf: line 32, column 45
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      personContact.FEINOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at OfficialIDInputSet.person.pcf: line 25, column 37
    function editable_1 () : java.lang.Boolean {
      return period?.profileChange_TDIC
    }
    
    // 'editable' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function editable_36 () : java.lang.Boolean {
      return (period != null) ? period.profileChange_TDIC : true
    }
    
    // 'editable' attribute on TypeKeyInput (id=CDAMembershipStatus_Input_Input) at OfficialIDInputSet.person.pcf: line 73, column 46
    function editable_47 () : java.lang.Boolean {
      return (period==null?true : period.profileChange_TDIC)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at OfficialIDInputSet.person.pcf: line 25, column 37
    function encryptionExpression_5 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(person.SSNOfficialID)
    }
    
    // 'initialValue' attribute on Variable at OfficialIDInputSet.person.pcf: line 18, column 22
    function initialValue_0 () : Person {
      return (personContact as Person)
    }
    
    // 'inputMask' attribute on TextInput (id=OfficialIDDVFEIN_Input_Input) at OfficialIDInputSet.person.pcf: line 32, column 45
    function inputMask_11 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.InputMask
    }
    
    // 'regex' attribute on TextInput (id=OfficialIDDVFEIN_Input_Input) at OfficialIDInputSet.person.pcf: line 32, column 45
    function regex_12 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.Regex
    }
    
    // 'valueRange' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function valueRange_41 () : java.lang.Object {
      return getStateComponentSelection()
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDVFEIN_Input_Input) at OfficialIDInputSet.person.pcf: line 32, column 45
    function valueRoot_10 () : java.lang.Object {
      return personContact
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at OfficialIDInputSet.person.pcf: line 25, column 37
    function valueRoot_4 () : java.lang.Object {
      return person
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input_Input) at OfficialIDInputSet.person.pcf: line 41, column 48
    function value_14 () : java.lang.String {
      return person.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=LicenseNumber_Input) at OfficialIDInputSet.person.pcf: line 47, column 42
    function value_19 () : java.lang.String {
      return person.LicenseNumber_TDIC
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at OfficialIDInputSet.person.pcf: line 25, column 37
    function value_2 () : java.lang.String {
      return person.SSNOfficialID
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseState_Input_Input) at OfficialIDInputSet.person.pcf: line 53, column 41
    function value_25 () : typekey.Jurisdiction {
      return person.LicenseState
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseStatus_Input_Input) at OfficialIDInputSet.person.pcf: line 59, column 46
    function value_31 () : typekey.GlobalStatus_TDIC {
      return person.LicenseStatus_TDIC
    }
    
    // 'value' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function value_38 () : typekey.Component_TDIC {
      return person.Component_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=CDAMembershipStatus_Input_Input) at OfficialIDInputSet.person.pcf: line 73, column 46
    function value_48 () : typekey.GlobalStatus_TDIC {
      return person.CDAMembershipStatus_TDIC
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDVFEIN_Input_Input) at OfficialIDInputSet.person.pcf: line 32, column 45
    function value_8 () : java.lang.String {
      return personContact.FEINOfficialID
    }
    
    // 'valueRange' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function verifyValueRangeIsAllowedType_42 ($$arg :  typekey.Component_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function verifyValueRange_43 () : void {
      var __valueRangeArg = getStateComponentSelection()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=Component_Input_Input) at OfficialIDInputSet.person.pcf: line 67, column 58
    function visible_37 () : java.lang.Boolean {
      return period != null and !period.GLLineExists
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get person () : Person {
      return getVariableValue("person", 0) as Person
    }
    
    property set person ($arg :  Person) {
      setVariableValue("person", 0, $arg)
    }
    
    property get personContact () : Contact {
      return getRequireValue("personContact", 0) as Contact
    }
    
    property set personContact ($arg :  Contact) {
      setRequireValue("personContact", 0, $arg)
    }
    
    function checkValidSSN(contact : Contact, ssn : String) : String {
      var query = gw.api.database.Query.make(OfficialID)
      query.compare(OfficialID#OfficialIDValue.PropertyInfo.Name, Equals, ssn)
      query.compare(OfficialID#OfficialIDType.PropertyInfo.Name, Equals, OfficialIDType.TC_SSN)
      var officialID = query.select().FirstResult
      if(officialID != null and officialID.Contact != contact) {
        return DisplayKey.get("Web.OfficialID.DuplicateSSN")
      }
      return null
    }
    
    function getStateComponentSelection() : typekey.Component_TDIC[] {
      var accountHolderState = person.PrimaryAddress.State != null? person.PrimaryAddress.State : 
                              (personContact.PrimaryAddress.State != null? personContact.PrimaryAddress.State : null)
      switch (accountHolderState) {
        case TC_CA:
          return Component_TDIC.TF_COMPONENT_CA.TypeKeys.toTypedArray()
        case TC_AK:
          return Component_TDIC.TF_COMPONENT_AK.TypeKeys.toTypedArray()
        case TC_AZ:
          return Component_TDIC.TF_COMPONENT_AZ.TypeKeys.toTypedArray()
        case TC_HI:
          return Component_TDIC.TF_COMPONENT_HI.TypeKeys.toTypedArray()
        case TC_IL:
          return Component_TDIC.TF_COMPONENT_IL.TypeKeys.toTypedArray()
        case TC_MN:
          return Component_TDIC.TF_COMPONENT_MN.TypeKeys.toTypedArray()
        case TC_NV:
          return Component_TDIC.TF_COMPONENT_NV.TypeKeys.toTypedArray()
        case TC_ND:
          return Component_TDIC.TF_COMPONENT_ND.TypeKeys.toTypedArray()
        case TC_NJ:
          return Component_TDIC.TF_COMPONENT_NJ.TypeKeys.toTypedArray()
        case TC_PA:
          return Component_TDIC.TF_COMPONENT_PA.TypeKeys.toTypedArray()
        default:
          return typekey.Component_TDIC.getTypeKeys(false).toTypedArray()
      }
    }
    
    
  }
  
  
}