package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.StateJurisdictionMappingUtil
@javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WorkersCompCoverageCVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WorkersCompCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=addJurisdiction) at WorkersCompCoverageCV.pcf: line 61, column 80
    function label_12 () : java.lang.Object {
      return locationState
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=addJurisdiction) at WorkersCompCoverageCV.pcf: line 61, column 80
    function toCreateAndAdd_13 (CheckedValues :  Object[]) : java.lang.Object {
      return wcLine.addJurisdiction(locationState)
    }
    
    property get locationState () : typekey.Jurisdiction {
      return getIteratedValue(1) as typekey.Jurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at WorkersCompCoverageCV.pcf: line 95, column 51
    function checkBoxVisible_27 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'condition' attribute on ToolbarFlag at WorkersCompCoverageCV.pcf: line 101, column 35
    function condition_20 () : java.lang.Boolean {
      return covJuris.CanRemove
    }
    
    // 'outputConversion' attribute on TextCell (id=RiskID_Cell) at WorkersCompCoverageCV.pcf: line 115, column 32
    function outputConversion_24 (VALUE :  entity.OfficialID[]) : java.lang.String {
      return gw.web.line.wc.policy.WorkersCompCoverageCVUIHelper.outputConverterForOfficialIDs(VALUE)
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WorkersCompCoverageCV.pcf: line 107, column 53
    function valueRoot_22 () : java.lang.Object {
      return covJuris
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WorkersCompCoverageCV.pcf: line 107, column 53
    function value_21 () : typekey.Jurisdiction {
      return covJuris.State
    }
    
    // 'value' attribute on TextCell (id=RiskID_Cell) at WorkersCompCoverageCV.pcf: line 115, column 32
    function value_25 () : entity.OfficialID[] {
      return gw.web.line.wc.policy.WorkersCompCoverageCVUIHelper.getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(wcLine, covJuris)
    }
    
    property get covJuris () : entity.WCJurisdiction {
      return getIteratedValue(2) as entity.WCJurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends WCPolicyCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_100 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_102 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_112 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_114 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_116 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_118 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_120 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_122 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_124 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_126 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_128 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_130 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_132 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_134 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_136 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_138 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_140 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_142 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_144 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_40 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_42 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_44 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_46 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_48 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_50 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_52 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_54 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_62 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_64 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_66 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_68 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_70 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_72 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_74 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_76 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_78 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_80 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_82 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_84 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_86 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_88 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_90 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_92 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_94 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_96 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_onEnter_98 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, wcLine, true)
    }
    
    // 'mode' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 174, column 50
    function mode_146 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(2) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends WCPolicyCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_149 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_151 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_153 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_155 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_157 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_159 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_161 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_163 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_165 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_167 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_169 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_171 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_173 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_175 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_177 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_179 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_181 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_183 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_185 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_187 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_189 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_191 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_193 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_195 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_197 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_199 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_201 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_203 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_205 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_207 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_209 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_211 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_213 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_215 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_219 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_221 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_223 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_225 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_227 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_229 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_231 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_233 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_235 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_237 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_239 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_241 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_243 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_245 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_247 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_249 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_251 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_onEnter_253 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_222 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_226 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_228 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_230 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_232 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_234 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_236 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_238 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_240 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_242 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_244 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_246 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_248 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_250 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_252 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function def_refreshVariables_254 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(exclusionPattern, wcLine, true)
    }
    
    // 'mode' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 187, column 51
    function mode_255 () : java.lang.Object {
      return exclusionPattern.PublicID
    }
    
    property get exclusionPattern () : gw.api.productmodel.ExclusionPattern {
      return getIteratedValue(2) as gw.api.productmodel.ExclusionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WorkersCompCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WorkersCompCoverageCV.pcf: line 40, column 50
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      officialId.OfficialIDValue = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WorkersCompCoverageCV.pcf: line 40, column 50
    function label_2 () : java.lang.Object {
      return officialId.OfficialIDInsuredAndType
    }
    
    // 'validationExpression' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WorkersCompCoverageCV.pcf: line 40, column 50
    function validationExpression_1 () : java.lang.Object {
      return officialId.validateValue()
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WorkersCompCoverageCV.pcf: line 40, column 50
    function valueRoot_5 () : java.lang.Object {
      return officialId
    }
    
    // 'value' attribute on TextInput (id=ANI_OfficialIDEntry_Input) at WorkersCompCoverageCV.pcf: line 40, column 50
    function value_3 () : java.lang.String {
      return officialId.OfficialIDValue
    }
    
    property get officialId () : entity.OfficialID {
      return getIteratedValue(1) as entity.OfficialID
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends WorkersCompCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewPanel (id=PolicyLinePerStateConfig_LV) at WorkersCompCoverageCV.pcf: line 88, column 28
    function available_30 () : java.lang.Boolean {
      return selectedJurisdiction != null
    }
    
    // 'def' attribute on PanelRef at WorkersCompCoverageCV.pcf: line 124, column 81
    function def_onEnter_31 (def :  pcf.PolicyLinePerStateConfigDV) : void {
      def.onEnter(wcLine, selectedJurisdiction)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 131, column 87
    function def_onEnter_33 (def :  pcf.WorkersCompClassesInputSet) : void {
      def.onEnter(selectedJurisdiction, wcLine)
    }
    
    // 'def' attribute on PanelRef at WorkersCompCoverageCV.pcf: line 124, column 81
    function def_refreshVariables_32 (def :  pcf.PolicyLinePerStateConfigDV) : void {
      def.refreshVariables(wcLine, selectedJurisdiction)
    }
    
    // 'def' attribute on InputSetRef at WorkersCompCoverageCV.pcf: line 131, column 87
    function def_refreshVariables_34 (def :  pcf.WorkersCompClassesInputSet) : void {
      def.refreshVariables(selectedJurisdiction, wcLine)
    }
    
    // 'sortBy' attribute on IteratorSort at WorkersCompCoverageCV.pcf: line 98, column 32
    function sortBy_17 (covJuris :  entity.WCJurisdiction) : java.lang.Object {
      return covJuris.State.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at WorkersCompCoverageCV.pcf: line 107, column 53
    function sortValue_18 (covJuris :  entity.WCJurisdiction) : java.lang.Object {
      return covJuris.State
    }
    
    // 'value' attribute on TextCell (id=RiskID_Cell) at WorkersCompCoverageCV.pcf: line 115, column 32
    function sortValue_19 (covJuris :  entity.WCJurisdiction) : java.lang.Object {
      return gw.web.line.wc.policy.WorkersCompCoverageCVUIHelper.getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(wcLine, covJuris)
    }
    
    // 'toRemove' attribute on RowIterator at WorkersCompCoverageCV.pcf: line 95, column 51
    function toRemove_28 (covJuris :  entity.WCJurisdiction) : void {
      wcLine.removeJurisdiction( covJuris ); gw.api.util.LocationUtil.addRequestScopedInfoMessage(DisplayKey.get("Web.Policy.WC.RemoveJurisdictionWarning")) 
    }
    
    // 'value' attribute on RowIterator at WorkersCompCoverageCV.pcf: line 95, column 51
    function value_29 () : entity.WCJurisdiction[] {
      return wcLine.Jurisdictions
    }
    
    property get selectedJurisdiction () : WCJurisdiction {
      return getCurrentSelection(1) as WCJurisdiction
    }
    
    property set selectedJurisdiction ($arg :  WCJurisdiction) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WCPolicyCoveragesDVExpressionsImpl extends WorkersCompCoverageCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WorkersCompCoverageCV.pcf: line 150, column 59
    function initialValue_35 () : gw.api.productmodel.CoveragePattern[] {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WorkersCompGrp").coveragePatternsForEntity(WorkersCompLine).whereSelectedOrAvailable(wcLine, openForEdit).where(\ c -> c.DisplayName != "Workers' Comp")
    }
    
    // 'initialValue' attribute on Variable at WorkersCompCoverageCV.pcf: line 155, column 60
    function initialValue_36 () : gw.api.productmodel.ExclusionPattern[] {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WorkersCompGrp").exclusionPatternsForEntity(WorkersCompLine).whereSelectedOrAvailable(wcLine, openForEdit).where(\ e -> e.DisplayName != "Workers' Comp")
    }
    
    // 'sortBy' attribute on IteratorSort at WorkersCompCoverageCV.pcf: line 184, column 32
    function sortBy_148 (exclusionPattern :  gw.api.productmodel.ExclusionPattern) : java.lang.Object {
      return exclusionPattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at WorkersCompCoverageCV.pcf: line 171, column 32
    function sortBy_39 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=WCCoverageGroupIterator) at WorkersCompCoverageCV.pcf: line 168, column 65
    function value_147 () : gw.api.productmodel.CoveragePattern[] {
      return wcGroupCoveragePatterns
    }
    
    // 'value' attribute on InputIterator (id=WCExclusionGroupIterator) at WorkersCompCoverageCV.pcf: line 181, column 66
    function value_256 () : gw.api.productmodel.ExclusionPattern[] {
      return wcGroupExclusionPatterns
    }
    
    // 'value' attribute on TextInput (id=CoveredStates_Input) at WorkersCompCoverageCV.pcf: line 162, column 72
    function value_37 () : java.lang.String {
      return wcLine.Jurisdictions*.State.join(",")
    }
    
    property get wcGroupCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("wcGroupCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set wcGroupCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("wcGroupCoveragePatterns", 1, $arg)
    }
    
    property get wcGroupExclusionPatterns () : gw.api.productmodel.ExclusionPattern[] {
      return getVariableValue("wcGroupExclusionPatterns", 1) as gw.api.productmodel.ExclusionPattern[]
    }
    
    property set wcGroupExclusionPatterns ($arg :  gw.api.productmodel.ExclusionPattern[]) {
      setVariableValue("wcGroupExclusionPatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc/policy/WorkersCompCoverageCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WorkersCompCoverageCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=SplitBases) at WorkersCompCoverageCV.pcf: line 73, column 75
    function action_15 () : void {
      gw.web.line.wc.policy.WorkersCompCoverageCVUIHelper.updateAllBasis(wcLine, CurrentLocation)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SplitPeriod) at WorkersCompCoverageCV.pcf: line 80, column 76
    function allCheckedRowsAction_16 (CheckedValues :  entity.WCJurisdiction[], CheckedValuesErrors :  java.util.Map) : void {
      SplitPeriodPopup.push( wcLine, CheckedValues )
    }
    
    // 'available' attribute on DetailViewPanel (id=NamedInsuredOfficialIDDV) at WorkersCompCoverageCV.pcf: line 24, column 56
    function available_10 () : java.lang.Boolean {
      return namedInsuredOfficialIDs.Count > 0
    }
    
    // 'initialValue' attribute on Variable at WorkersCompCoverageCV.pcf: line 17, column 28
    function initialValue_0 () : OfficialID[] {
      return wcLine.InterstateNamedInsuredOfficialIDs
    }
    
    // 'value' attribute on AddMenuItemIterator at WorkersCompCoverageCV.pcf: line 56, column 50
    function value_14 () : typekey.Jurisdiction[] {
      return gw.web.line.wc.policy.WorkersCompCoverageCVUIHelper.JurisdictionsThatCanBeAdded(wcLine)
    }
    
    // 'value' attribute on InputIterator (id=officialIDs) at WorkersCompCoverageCV.pcf: line 33, column 45
    function value_9 () : entity.OfficialID[] {
      return namedInsuredOfficialIDs
    }
    
    property get namedInsuredOfficialIDs () : OfficialID[] {
      return getVariableValue("namedInsuredOfficialIDs", 0) as OfficialID[]
    }
    
    property set namedInsuredOfficialIDs ($arg :  OfficialID[]) {
      setVariableValue("namedInsuredOfficialIDs", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wcLine () : WorkersCompLine {
      return getRequireValue("wcLine", 0) as WorkersCompLine
    }
    
    property set wcLine ($arg :  WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    
    //function updateAllBasis(){
    //  if((CurrentLocation as pcf.api.Wizard).saveDraft()){
    //    wcLine.updateWCExposuresAndModifiers()
    //  }
    //}
    
    //property get JurisdictionsThatCanBeAdded(): Jurisdiction[] {
    //  var existingJurisdictions = wcLine.Jurisdictions.map(\j -> j.State).toSet()
    //  var possibleJurisdicitons = wcLine.Branch.LocationStates.toSet()
    //  possibleJurisdicitons.removeAll(existingJurisdictions)
    //  return possibleJurisdicitons.toTypedArray()
    //}
    
    //function getOfficalIDsForJurisdictionThatMatchPNIContactsOfficialIDs(covJuris : WCJurisdiction) : entity.OfficialID[] {
    //  return wcLine.Branch.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.OfficialIDs
    //        .where(\ officialID ->
    //          officialID.State == StateJurisdictionMappingUtil.getStateMappingForJurisdiction(covJuris.State))
    //}
    
    //function outputConverterForOfficialIDs(VALUE : OfficialID[]) : String {
    //  var str = ""
    //  var first = true
    //  for (var Item in VALUE) {
    //    var idValue = Item.getOfficialIDValue()
    //    if(idValue != null) {
    //      if(!first) {
    //        str = str + ", "
    //      }
    //      first = false
    //      str = str + idValue
    //    }
    //  }
    //  return str
    //}
    
    
  }
  
  
}