package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDemoPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDemoPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=customQueryButton) at OnBaseDemoPopup.pcf: line 30, column 51
    function action_1 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUnityUtils.constructCustomQueryURL(policyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=webFolderPopButton) at OnBaseDemoPopup.pcf: line 68, column 45
    function action_10 () : void {
      OnBaseUrl.push(acc.onbase.util.OnBaseWebUtils.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebFolderPop") as String)?.trim() , {policyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'action' attribute on ToolbarButton (id=onFoldersButton) at OnBaseDemoPopup.pcf: line 35, column 53
    function action_2 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUnityUtils.constructFolderURL(policyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=uploadButton) at OnBaseDemoPopup.pcf: line 40, column 54
    function action_3 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUnityUtils.uploadDocURL(policyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=uFormButton) at OnBaseDemoPopup.pcf: line 45, column 56
    function action_4 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUnityUtils.unityFormURL(policyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=docPacketButton) at OnBaseDemoPopup.pcf: line 50, column 58
    function action_5 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUnityUtils.generateDocPacketURL("POL - Account Packet", policyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at OnBaseDemoPopup.pcf: line 63, column 47
    function action_8 () : void {
      OnBaseUrl.push(acc.onbase.util.OnBaseWebUtils.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {policyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'action' attribute on ToolbarButton (id=webFolderPopButton) at OnBaseDemoPopup.pcf: line 68, column 45
    function action_dest_11 () : pcf.api.Destination {
      return pcf.OnBaseUrl.createDestination(acc.onbase.util.OnBaseWebUtils.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebFolderPop") as String)?.trim() , {policyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at OnBaseDemoPopup.pcf: line 63, column 47
    function action_dest_9 () : pcf.api.Destination {
      return pcf.OnBaseUrl.createDestination(acc.onbase.util.OnBaseWebUtils.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {policyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 56, column 40
    function def_onEnter_12 (def :  pcf.OnBaseWebFramePanelSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 23, column 55
    function def_onEnter_6 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.onEnter(scrapeXml)
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 56, column 40
    function def_refreshVariables_13 (def :  pcf.OnBaseWebFramePanelSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 23, column 55
    function def_refreshVariables_7 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.refreshVariables(scrapeXml)
    }
    
    // 'initialValue' attribute on Variable at OnBaseDemoPopup.pcf: line 16, column 24
    function initialValue_0 () : String[] {
      return new String[]{""}
    }
    
    override property get CurrentLocation () : pcf.OnBaseDemoPopup {
      return super.CurrentLocation as pcf.OnBaseDemoPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get scrapeXml () : String[] {
      return getVariableValue("scrapeXml", 0) as String[]
    }
    
    property set scrapeXml ($arg :  String[]) {
      setVariableValue("scrapeXml", 0, $arg)
    }
    
    
  }
  
  
}