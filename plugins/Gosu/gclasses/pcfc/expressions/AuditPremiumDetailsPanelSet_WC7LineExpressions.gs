package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/AuditPremiumDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AuditPremiumDetailsPanelSet_WC7LineExpressions {
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditPremiumDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AuditPremiumDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 20, column 23
    function initialValue_0 () : boolean {
      return auditInfo.IsPremiumReport
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 24, column 23
    function initialValue_1 () : boolean {
      return auditInfo.IsFinalAudit
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 29, column 51
    function initialValue_2 () : java.util.Set<entity.WC7Cost> {
      return period.WC7Line.Costs.cast( WC7Cost )
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 34, column 93
    function initialValue_3 () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>> {
      return lineCosts.partition( \ c -> c.JurisdictionState ).mapValues(\i -> i.toSet()).toAutoMap( \ st -> java.util.Collections.emptySet<WC7Cost>() )
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 39, column 40
    function initialValue_4 () : entity.WC7Jurisdiction[] {
      return period.WC7Line.RepresentativeJurisdictions
    }
    
    // 'sortBy' attribute on IteratorSort at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 46, column 24
    function sortBy_5 (jurisdiction :  entity.WC7Jurisdiction) : java.lang.Object {
      return jurisdictions
    }
    
    // 'title' attribute on TitleBar (id=grandTotalTitle) at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 113, column 214
    function title_23 () : java.lang.String {
      return DisplayKey.get("Web.Quote.TotalCostLabel.Total2", gw.api.util.StringUtil.formatNumber(lineCosts.AmountSum(period.PreferredSettlementCurrency) as java.lang.Double, "currency"))
    }
    
    // 'value' attribute on PanelIterator at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 43, column 44
    function value_22 () : entity.WC7Jurisdiction[] {
      return jurisdictions
    }
    
    property get auditInfo () : AuditInformation {
      return getRequireValue("auditInfo", 0) as AuditInformation
    }
    
    property set auditInfo ($arg :  AuditInformation) {
      setRequireValue("auditInfo", 0, $arg)
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get isFinalAudit () : boolean {
      return getVariableValue("isFinalAudit", 0) as java.lang.Boolean
    }
    
    property set isFinalAudit ($arg :  boolean) {
      setVariableValue("isFinalAudit", 0, $arg)
    }
    
    property get isPremiumReport () : boolean {
      return getVariableValue("isPremiumReport", 0) as java.lang.Boolean
    }
    
    property set isPremiumReport ($arg :  boolean) {
      setVariableValue("isPremiumReport", 0, $arg)
    }
    
    property get jurisdictions () : entity.WC7Jurisdiction[] {
      return getVariableValue("jurisdictions", 0) as entity.WC7Jurisdiction[]
    }
    
    property set jurisdictions ($arg :  entity.WC7Jurisdiction[]) {
      setVariableValue("jurisdictions", 0, $arg)
    }
    
    property get lineCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("lineCosts", 0) as java.util.Set<entity.WC7Cost>
    }
    
    property set lineCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("lineCosts", 0, $arg)
    }
    
    property get partitionCosts () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>> {
      return getVariableValue("partitionCosts", 0) as java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>>
    }
    
    property set partitionCosts ($arg :  java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Cost>>) {
      setVariableValue("partitionCosts", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    function standardPremLabel(splitPeriod : boolean, ratingPeriod : gw.lob.wc7.rating.WC7RatingPeriod) : String {
      if (isFinalAudit and not splitPeriod) {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.OnePeriod")
      }
      else {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.SplitPeriod", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"),
          gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditPremiumDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 81, column 96
    function def_onEnter_12 (def :  pcf.WC7AuditRateCostDetailLV) : void {
      def.onEnter(isPremiumReport, periodCosts, ratingPeriod)
    }
    
    // 'def' attribute on ListViewInput at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 81, column 96
    function def_refreshVariables_13 (def :  pcf.WC7AuditRateCostDetailLV) : void {
      def.refreshVariables(isPremiumReport, periodCosts, ratingPeriod)
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 72, column 59
    function initialValue_10 () : java.util.Set<entity.WC7Cost> {
      return stateCosts.byRatingPeriod( ratingPeriod )
    }
    
    // PanelIterator at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 67, column 61
    function initializeVariables_15 () : void {
        periodCosts = stateCosts.byRatingPeriod( ratingPeriod );

    }
    
    // 'label' attribute on Label at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 79, column 90
    function label_11 () : java.lang.String {
      return standardPremLabel(ratingPeriods.Count > 1, ratingPeriod)
    }
    
    // 'visible' attribute on PanelRef at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 74, column 47
    function visible_14 () : java.lang.Boolean {
      return not periodCosts.Empty
    }
    
    property get periodCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("periodCosts", 2) as java.util.Set<entity.WC7Cost>
    }
    
    property set periodCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("periodCosts", 2, $arg)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditPremiumDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends AuditPremiumDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 51, column 53
    function initialValue_6 () : java.util.Set<entity.WC7Cost> {
      return partitionCosts.get(jurisdiction.Jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 56, column 73
    function initialValue_7 () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return jurisdiction.AuditRatingPeriods
    }
    
    // PanelIterator at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 43, column 44
    function initializeVariables_21 () : void {
        stateCosts = partitionCosts.get(jurisdiction.Jurisdiction);
  ratingPeriods = jurisdiction.AuditRatingPeriods;

    }
    
    // 'title' attribute on TitleBar at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 61, column 49
    function title_9 () : java.lang.String {
      return jurisdiction.DisplayName
    }
    
    // 'value' attribute on PanelIterator at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 67, column 61
    function value_16 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.toTypedArray()
    }
    
    // 'visible' attribute on PanelRef at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 58, column 40
    function visible_20 () : java.lang.Boolean {
      return not stateCosts.Empty
    }
    
    // 'visible' attribute on TitleBar at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 61, column 49
    function visible_8 () : java.lang.Boolean {
      return jurisdictions.Count > 1
    }
    
    property get jurisdiction () : entity.WC7Jurisdiction {
      return getIteratedValue(1) as entity.WC7Jurisdiction
    }
    
    property get ratingPeriods () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return getVariableValue("ratingPeriods", 1) as java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>
    }
    
    property set ratingPeriods ($arg :  java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>) {
      setVariableValue("ratingPeriods", 1, $arg)
    }
    
    property get stateCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("stateCosts", 1) as java.util.Set<entity.WC7Cost>
    }
    
    property set stateCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("stateCosts", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditPremiumDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class StateSummaryDetailDVExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 100, column 117
    function def_onEnter_18 (def :  pcf.WC7AuditRateCostDetailStateLV) : void {
      def.onEnter(isPremiumReport, stateCosts, basedOnStateCosts, jurisdiction)
    }
    
    // 'def' attribute on ListViewInput at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 100, column 117
    function def_refreshVariables_19 (def :  pcf.WC7AuditRateCostDetailStateLV) : void {
      def.refreshVariables(isPremiumReport, stateCosts, basedOnStateCosts, jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at AuditPremiumDetailsPanelSet.WC7Line.pcf: line 95, column 61
    function initialValue_17 () : java.util.Set<entity.WC7Cost> {
      return jurisdiction.WCLine.BasedOn.Costs.cast( WC7Cost ).where( \ w -> w.JurisdictionState==jurisdiction.Jurisdiction).toSet()
    }
    
    property get basedOnStateCosts () : java.util.Set<entity.WC7Cost> {
      return getVariableValue("basedOnStateCosts", 2) as java.util.Set<entity.WC7Cost>
    }
    
    property set basedOnStateCosts ($arg :  java.util.Set<entity.WC7Cost>) {
      setVariableValue("basedOnStateCosts", 2, $arg)
    }
    
    
  }
  
  
}