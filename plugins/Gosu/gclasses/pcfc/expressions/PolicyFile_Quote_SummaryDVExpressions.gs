package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_Quote_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_Quote_SummaryDVExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_Quote_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_Quote_SummaryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at PolicyFile_Quote_SummaryDV.pcf: line 39, column 31
    function def_onEnter_11 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.onEnter(policyPeriod, true)
    }
    
    // 'def' attribute on InputSetRef (id=PolicyAddress) at PolicyFile_Quote_SummaryDV.pcf: line 39, column 31
    function def_refreshVariables_12 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.refreshVariables(policyPeriod, true)
    }
    
    // 'initialValue' attribute on Variable at PolicyFile_Quote_SummaryDV.pcf: line 15, column 45
    function initialValue_0 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TransactionCostRPT
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyFile_Quote_SummaryDV.pcf: line 26, column 90
    function valueRoot_3 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at PolicyFile_Quote_SummaryDV.pcf: line 36, column 65
    function valueRoot_9 () : java.lang.Object {
      return policyPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on TextInput (id=UWCompany_Input) at PolicyFile_Quote_SummaryDV.pcf: line 47, column 47
    function value_14 () : entity.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalPremium_Input) at PolicyFile_Quote_SummaryDV.pcf: line 57, column 159
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.GLLineExists ? tdic.web.pcf.helper.GLQuoteScreenHelper.getPolicyFileTotalPremium(policyPeriod) : policyPeriod.TotalPremiumRPT
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyFile_Quote_SummaryDV.pcf: line 26, column 90
    function value_2 () : java.lang.String {
      return policyPeriod.PolicyNumberDisplayString
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Taxes_Input) at PolicyFile_Quote_SummaryDV.pcf: line 64, column 165
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.GLLineExists ? tdic.web.pcf.helper.GLQuoteScreenHelper.getTotalNonERETaxSurcharges(policyPeriod) : policyPeriod.TaxAndSurchargesRPT
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCost_Input) at PolicyFile_Quote_SummaryDV.pcf: line 71, column 75
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalCostPolicyFile(policyPeriod)
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriod_Input) at PolicyFile_Quote_SummaryDV.pcf: line 31, column 194
    function value_6 () : java.lang.String {
      return gw.api.util.StringUtil.formatDate(policyPeriod.PeriodStart,"short") + " - " + gw.api.util.StringUtil.formatDate(policyPeriod.PeriodEnd,"short")
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at PolicyFile_Quote_SummaryDV.pcf: line 36, column 65
    function value_8 () : java.lang.String {
      return policyPeriod.PrimaryNamedInsured.DisplayName
    }
    
    // 'visible' attribute on TextInput (id=PolicyNumber_Input) at PolicyFile_Quote_SummaryDV.pcf: line 26, column 90
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.Promoted or (not policyPeriod.Job?.createsNewPolicy())
    }
    
    // 'visible' attribute on TextInput (id=UWCompany_Input) at PolicyFile_Quote_SummaryDV.pcf: line 47, column 47
    function visible_13 () : java.lang.Boolean {
      return perm.System.multicompquote
    }
    
    // 'visible' attribute on TextInput (id=HeldSubjectToFinalAudit_Input) at PolicyFile_Quote_SummaryDV.pcf: line 78, column 71
    function visible_24 () : java.lang.Boolean {
      return policyPeriod.JobProcess.BillingSubjectToFinalAudit
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteScreenHelper () : tdic.web.pcf.helper.GLQuoteScreenHelper {
      return getVariableValue("quoteScreenHelper", 0) as tdic.web.pcf.helper.GLQuoteScreenHelper
    }
    
    property set quoteScreenHelper ($arg :  tdic.web.pcf.helper.GLQuoteScreenHelper) {
      setVariableValue("quoteScreenHelper", 0, $arg)
    }
    
    property get transactionSum () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("transactionSum", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set transactionSum ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("transactionSum", 0, $arg)
    }
    
    
  }
  
  
}