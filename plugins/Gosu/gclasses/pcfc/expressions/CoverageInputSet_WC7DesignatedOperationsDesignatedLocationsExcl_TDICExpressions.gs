package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function available_58 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 95, column 138
    function def_onEnter_33 (def :  pcf.WC7ExclAlternateCoverages_TDICInputSet) : void {
      def.onEnter(coverable.getCoverageConditionOrExclusion(exclusionPattern) as WC7WorkersCompExcl)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 95, column 138
    function def_refreshVariables_34 (def :  pcf.WC7ExclAlternateCoverages_TDICInputSet) : void {
      def.refreshVariables(coverable.getCoverageConditionOrExclusion(exclusionPattern) as WC7WorkersCompExcl)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 20, column 38
    function initialValue_0 () : gw.api.domain.StateSet {
      return gw.api.domain.StateSet.get( gw.api.domain.StateSet.US50 )
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 24, column 41
    function initialValue_1 () : entity.WC7WorkersCompLine {
      return coverable as entity.WC7WorkersCompLine
    }
    
    // 'label' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function label_59 () : java.lang.Object {
      return exclusionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function setter_60 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(wc7Line, exclusionPattern, VALUE)
    }
    
    // 'value' attribute on TextAreaCell (id=Operation_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function sortValue_5 (operation :  entity.WC7DesOpDesLocExclOp_TDIC) : java.lang.Object {
      return operation.Operation
    }
    
    // 'value' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function sortValue_6 (operation :  entity.WC7DesOpDesLocExclOp_TDIC) : java.lang.Object {
      return operation.StandardClassificationCode
    }
    
    // 'value' attribute on TextAreaCell (id=AbbreviatedPhraseology_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 82, column 30
    function sortValue_7 (operation :  entity.WC7DesOpDesLocExclOp_TDIC) : java.lang.Object {
      return operation.StandardClassificationCode.ShortDesc
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 89, column 30
    function sortValue_8 (operation :  entity.WC7DesOpDesLocExclOp_TDIC) : java.lang.Object {
      return operation.LocationAddress
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 54, column 60
    function toCreateAndAdd_30 () : entity.WC7DesOpDesLocExclOp_TDIC {
      return wc7Line.createAndAddWC7DesignatedOperationDesignatedLocationExclOperation_TDIC(wc7Line.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC)
    }
    
    // 'toRemove' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 54, column 60
    function toRemove_31 (operation :  entity.WC7DesOpDesLocExclOp_TDIC) : void {
      wc7Line.removeFromWC7DesignatedOperationDesignatedLocationExclOperations_TDIC(operation)
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 28, column 37
    function valueRoot_3 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 28, column 37
    function value_2 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=Operations) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 54, column 60
    function value_32 () : entity.WC7DesOpDesLocExclOp_TDIC[] {
      return wc7Line.WC7DesignatedOperationDesignatedLocationExclOperations_TDIC
    }
    
    // 'value' attribute on InputIterator (id=CovTermIterator) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 102, column 53
    function value_56 () : gw.api.domain.covterm.CovTerm[] {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern).CovTerms.sortBy( \ term -> term.Pattern.Priority )
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 34, column 106
    function visible_57 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 109, column 101
    function visible_63 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get usStates () : gw.api.domain.StateSet {
      return getVariableValue("usStates", 0) as gw.api.domain.StateSet
    }
    
    property set usStates ($arg :  gw.api.domain.StateSet) {
      setVariableValue("usStates", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getVariableValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_35 (def :  pcf.CovTermInputSet_Direct) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_37 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_39 (def :  pcf.CovTermInputSet_Option) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_41 (def :  pcf.CovTermInputSet_Package) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_43 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_45 (def :  pcf.CovTermInputSet_bit) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_47 (def :  pcf.CovTermInputSet_datetime) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_49 (def :  pcf.CovTermInputSet_decimal) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_51 (def :  pcf.CovTermInputSet_default) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_onEnter_53 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_36 (def :  pcf.CovTermInputSet_Direct) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_38 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_40 (def :  pcf.CovTermInputSet_Option) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_42 (def :  pcf.CovTermInputSet_Package) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_44 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_46 (def :  pcf.CovTermInputSet_bit) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_48 (def :  pcf.CovTermInputSet_datetime) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_50 (def :  pcf.CovTermInputSet_decimal) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_52 (def :  pcf.CovTermInputSet_default) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function def_refreshVariables_54 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'mode' attribute on InputSetRef at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 105, column 38
    function mode_55 () : java.lang.Object {
      return term.ValueTypeName
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getIteratedValue(1) as gw.api.domain.covterm.CovTerm
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function action_13 () : void {
      WC7ClassCodeSearchPopup.push(wc7Line.Branch.PrimaryLocation, wc7Line, operation.StandardClassificationCode.ClassCodeType)
    }
    
    // 'pickLocation' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function action_dest_14 () : pcf.api.Destination {
      return pcf.WC7ClassCodeSearchPopup.createDestination(wc7Line.Branch.PrimaryLocation, wc7Line, operation.StandardClassificationCode.ClassCodeType)
    }
    
    // 'available' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function available_15 () : java.lang.Boolean {
      return wc7Line.Branch.BaseState != null
    }
    
    // 'value' attribute on TextAreaCell (id=Operation_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.Operation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.StandardClassificationCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 89, column 30
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      operation.LocationAddress = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'inputConversion' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function inputConversion_16 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.pcf.WC7CoverageInputSetUIHelper.findFirstMatchingClassCode(VALUE, wc7Line.Branch.BaseState, wc7Line)
    }
    
    // 'outputConversion' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function outputConversion_17 (VALUE :  entity.WC7ClassCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'value' attribute on TextAreaCell (id=Operation_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function valueRoot_11 () : java.lang.Object {
      return operation
    }
    
    // 'value' attribute on TextAreaCell (id=AbbreviatedPhraseology_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 82, column 30
    function valueRoot_24 () : java.lang.Object {
      return operation.StandardClassificationCode
    }
    
    // 'value' attribute on PickerCell (id=StandardClassificationCode_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 74, column 49
    function value_18 () : entity.WC7ClassCode {
      return operation.StandardClassificationCode
    }
    
    // 'value' attribute on TextAreaCell (id=AbbreviatedPhraseology_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 82, column 30
    function value_23 () : java.lang.String {
      return operation.StandardClassificationCode.ShortDesc
    }
    
    // 'value' attribute on TextAreaCell (id=LocationAddress_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 89, column 30
    function value_26 () : java.lang.String {
      return operation.LocationAddress
    }
    
    // 'value' attribute on TextAreaCell (id=Operation_Cell) at CoverageInputSet.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC.pcf: line 62, column 30
    function value_9 () : java.lang.String {
      return operation.Operation
    }
    
    property get operation () : entity.WC7DesOpDesLocExclOp_TDIC {
      return getIteratedValue(1) as entity.WC7DesOpDesLocExclOp_TDIC
    }
    
    
  }
  
  
}