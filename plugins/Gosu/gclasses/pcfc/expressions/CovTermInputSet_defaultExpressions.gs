package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      stringTerm.Value = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=DefaultInput_Input) at CovTermInputSet.default.pcf: line 29, column 153
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      term = (__VALUE_TO_SET as gw.api.domain.covterm.CovTerm)
    }
    
    // 'initialValue' attribute on Variable at CovTermInputSet.default.pcf: line 21, column 51
    function initialValue_0 () : gw.api.domain.covterm.StringCovTerm {
      return term as gw.api.domain.covterm.StringCovTerm
    }
    
    // 'label' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function label_10 () : java.lang.Object {
      return stringTerm.Pattern.DisplayName
    }
    
    // 'label' attribute on TextInput (id=DefaultInput_Input) at CovTermInputSet.default.pcf: line 29, column 153
    function label_2 () : java.lang.Object {
      return term.Pattern.DisplayName
    }
    
    // 'required' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function required_11 () : java.lang.Boolean {
      return stringTerm.Pattern.Required
    }
    
    // 'required' attribute on TextInput (id=DefaultInput_Input) at CovTermInputSet.default.pcf: line 29, column 153
    function required_3 () : java.lang.Boolean {
      return term.Pattern.Required
    }
    
    // 'value' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function valueRoot_14 () : java.lang.Object {
      return stringTerm
    }
    
    // 'value' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function value_12 () : java.lang.String {
      return stringTerm.Value
    }
    
    // 'value' attribute on TextInput (id=DefaultInput_Input) at CovTermInputSet.default.pcf: line 29, column 153
    function value_4 () : gw.api.domain.covterm.CovTerm {
      return term
    }
    
    // 'visible' attribute on TextInput (id=DefaultInput_Input) at CovTermInputSet.default.pcf: line 29, column 153
    function visible_1 () : java.lang.Boolean {
      return !(term.PatternCodeIdentifier == "GLManuscriptDesc_TDIC" || term.PatternCodeIdentifier == "BOPManuscriptDesc_TDIC")
    }
    
    // 'visible' attribute on TextAreaInput (id=DefaultInput1_Input) at CovTermInputSet.default.pcf: line 36, column 162
    function visible_9 () : java.lang.Boolean {
      return stringTerm.PatternCodeIdentifier == "GLManuscriptDesc_TDIC" || stringTerm.PatternCodeIdentifier == "BOPManuscriptDesc_TDIC"
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get stringTerm () : gw.api.domain.covterm.StringCovTerm {
      return getVariableValue("stringTerm", 0) as gw.api.domain.covterm.StringCovTerm
    }
    
    property set stringTerm ($arg :  gw.api.domain.covterm.StringCovTerm) {
      setVariableValue("stringTerm", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getRequireValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setRequireValue("term", 0, $arg)
    }
    
    
  }
  
  
}