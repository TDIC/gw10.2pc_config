package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPPolicyOwnerOfficerDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 61, column 103
    function conversionExpression_9 (PickedValue :  BOPPolicyEntityOwner_TDIC) : entity.BOPPolicyOwnerOfficer_TDIC {
      return PickedValue as BOPPolicyEntityOwner_TDIC
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 61, column 103
    function label_8 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewEntityOwnerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 61, column 103
    function pickLocation_10 () : void {
      TDIC_BOPNewEntityOwnerPopup.push(bopLine.Branch.BOPLine, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 92, column 97
    function label_17 () : java.lang.Object {
      return ownerOfficer
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 92, column 97
    function toCreateAndAdd_18 (CheckedValues :  Object[]) : java.lang.Object {
      return bopLine.addBOPPolicyOwnerOfficer_TDIC(ownerOfficer.Contact)
    }
    
    property get ownerOfficer () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 118, column 95
    function label_24 () : java.lang.Object {
      return entityOwner
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 118, column 95
    function toCreateAndAdd_25 (CheckedValues :  Object[]) : java.lang.Object {
      return bopLine.addBOPPolicyEntityOwner_TDIC(entityOwner.Contact)
    }
    
    property get entityOwner () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 144, column 97
    function label_31 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 144, column 97
    function toCreateAndAdd_32 (CheckedValues :  Object[]) : java.lang.Object {
      return bopLine.addBOPPolicyOwnerOfficer_TDIC(otherContact.Contact)
    }
    
    property get otherContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 171, column 31
    function action_38 () : void {
      EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 171, column 31
    function action_dest_39 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyOwnerOfficer, openForEdit)
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 179, column 111
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=ADA_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 179, column 111
    function editable_44 () : java.lang.Boolean {
      return policyOwnerOfficer.ContactDenorm.Subtype == TC_PERSON
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 182, column 182
    function onChange_43 () : void {
      tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(policyOwnerOfficer.AccountContactRole.AccountContact.Contact, true, period.BaseState)
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 171, column 31
    function valueRoot_41 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 179, column 111
    function valueRoot_47 () : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 171, column 31
    function value_40 () : java.lang.String {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 179, column 111
    function value_45 () : java.lang.String {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    property get policyOwnerOfficer () : entity.BOPPolicyOwnerOfficer_TDIC {
      return getIteratedValue(1) as entity.BOPPolicyOwnerOfficer_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 46, column 104
    function conversionExpression_4 (PickedValue :  BOPPolicyOwnerOfficer_TDIC) : entity.BOPPolicyOwnerOfficer_TDIC {
      return PickedValue as BOPPolicyOwnerOfficer_TDIC
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 46, column 104
    function label_3 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewOwnerOfficerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 46, column 104
    function pickLocation_5 () : void {
      TDIC_BOPNewOwnerOfficerPopup.push(bopLine.Branch.BOPLine, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BOPPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPPolicyOwnerOfficerDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 68, column 72
    function conversionExpression_12 (PickedValue :  Contact) : entity.BOPPolicyOwnerOfficer_TDIC {
      return bopLine.addBOPPolicyOwnerOfficer_TDIC(PickedValue)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 74, column 103
    function conversionExpression_14 (PickedValue :  Contact) : entity.BOPPolicyOwnerOfficer_TDIC {
      return bopLine.addBOPPolicyEntityOwner_TDIC(PickedValue)
    }
    
    // 'editable' attribute on RowIterator at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 164, column 61
    function editable_37 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 16, column 23
    function initialValue_0 () : BOPLine {
      return period.BOPLine
    }
    
    // 'initialValue' attribute on Variable at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 20, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 79, column 30
    function label_22 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingEntityOwnerContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 105, column 30
    function label_29 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 68, column 72
    function pickLocation_13 () : void {
      ContactSearchPopup.push(TC_OWNEROFFICER)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 74, column 103
    function pickLocation_15 () : void {
      ContactSearchPopup.push(typekey.AccountContactRole.TC_ENTITYOWNER_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 87, column 34
    function sortBy_16 (ownerOfficer :  entity.AccountContact) : java.lang.Object {
      return ownerOfficer.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 40, column 32
    function sortBy_2 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 113, column 34
    function sortBy_23 (entityOwner :  entity.AccountContact) : java.lang.Object {
      return OwnerOfficer.Type.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 139, column 34
    function sortBy_30 (otherContact :  entity.AccountContact) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 171, column 31
    function sortValue_35 (policyOwnerOfficer :  entity.BOPPolicyOwnerOfficer_TDIC) : java.lang.Object {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 179, column 111
    function sortValue_36 (policyOwnerOfficer :  entity.BOPPolicyOwnerOfficer_TDIC) : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 99, column 79
    function toCreateAndAdd_21 (CheckedValues :  Object[]) : java.lang.Object {
      return bopLine.addAllExistingBOPPolicyOwnerOfficer_TDIC()
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 125, column 81
    function toCreateAndAdd_28 (CheckedValues :  Object[]) : java.lang.Object {
      return bopLine.addAllExistingBOPPolicyEntityOwners_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 164, column 61
    function toRemove_50 (policyOwnerOfficer :  entity.BOPPolicyOwnerOfficer_TDIC) : void {
      bopLine.removeFromBOPPolicyOwnerOfficer_TDIC(policyOwnerOfficer)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewEntityOwnerContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 52, column 49
    function value_11 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_BOPPOLICYENTITYOWNER_TDIC)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 84, column 53
    function value_19 () : entity.AccountContact[] {
      return bopLine.UnassignedOwnerOfficers_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 110, column 53
    function value_26 () : entity.AccountContact[] {
      return bopLine.UnassignedBOPEntityOwners_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 136, column 53
    function value_33 () : entity.AccountContact[] {
      return bopLine.BOPOwnerOfficerOtherCandidates_TDIC
    }
    
    // 'value' attribute on RowIterator at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 164, column 61
    function value_51 () : entity.BOPPolicyOwnerOfficer_TDIC[] {
      return bopLine.BOPPolicyOwnerOfficer_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewOwnerOfficerContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 37, column 49
    function value_6 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_BOPPOLICYOWNEROFFICER_TDIC)
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 99, column 79
    function visible_20 () : java.lang.Boolean {
      return bopLine.UnassignedOwnerOfficers_TDIC.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 125, column 81
    function visible_27 () : java.lang.Boolean {
      return bopLine.UnassignedBOPEntityOwners_TDIC.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddOtherContact) at TDIC_BOPPolicyOwnerOfficerDV.pcf: line 131, column 82
    function visible_34 () : java.lang.Boolean {
      return bopLine.BOPOwnerOfficerOtherCandidates_TDIC.Count > 0
    }
    
    property get bopLine () : BOPLine {
      return getVariableValue("bopLine", 0) as BOPLine
    }
    
    property set bopLine ($arg :  BOPLine) {
      setVariableValue("bopLine", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    
  }
  
  
}