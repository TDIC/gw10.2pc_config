package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewOwnerOfficerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7NewOwnerOfficerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewOwnerOfficerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7NewOwnerOfficerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (line :  WC7Line, contactType :  ContactType) : int {
      return 0
    }
    
    static function __constructorIndex (line :  WC7Line, contactType :  ContactType, clausePattern :  gw.api.productmodel.ClausePattern) : int {
      return 1
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewOwnerOfficerPopup.pcf: line 72, column 62
    function action_10 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(policyOwnerOfficer))
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at WC7NewOwnerOfficerPopup.pcf: line 80, column 62
    function action_15 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'beforeCommit' attribute on Popup (id=WC7NewOwnerOfficerPopup) at WC7NewOwnerOfficerPopup.pcf: line 13, column 117
    function beforeCommit_49 (pickedValue :  WC7PolicyOwnerOfficer) : void {
      policyOwnerOfficer.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch); helper.validateAndUpdateStatusOfAddresses(contact)
    }
    
    // 'beforeValidate' attribute on Popup (id=WC7NewOwnerOfficerPopup) at WC7NewOwnerOfficerPopup.pcf: line 13, column 117
    function beforeValidate_50 (pickedValue :  WC7PolicyOwnerOfficer) : void {
      displayMembershipCheckError = tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(policyOwnerOfficer.AccountContactRole.AccountContact.Contact, displayMembershipCheckError, policyOwnerOfficer.Branch.BaseState)
    }
    
    // 'def' attribute on PanelRef at WC7NewOwnerOfficerPopup.pcf: line 126, column 73
    function def_onEnter_47 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(policyOwnerOfficer, false)
    }
    
    // 'def' attribute on PanelRef at WC7NewOwnerOfficerPopup.pcf: line 126, column 73
    function def_refreshVariables_48 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(policyOwnerOfficer, false)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      includedOwnerOfficer.WC7ClassCode = (__VALUE_TO_SET as entity.WC7ClassCode)
    }
    
    // 'value' attribute on TextInput (id=Ownership_Input) at WC7NewOwnerOfficerPopup.pcf: line 115, column 44
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7OwnershipPct = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=Relationship_Input) at WC7NewOwnerOfficerPopup.pcf: line 122, column 47
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.RelationshipTitle = (__VALUE_TO_SET as typekey.Relationship)
    }
    
    // 'editable' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function editable_27 () : java.lang.Boolean {
      return isClassCodeEditable(includedOwnerOfficer)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Relationship_Input) at WC7NewOwnerOfficerPopup.pcf: line 122, column 47
    function filter_45 (VALUE :  typekey.Relationship, VALUES :  typekey.Relationship[]) : java.lang.Boolean {
      return Relationship.TF_WC7OWNEROFFICERRELATIONSHIP.TypeKeys.contains(VALUE)
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 27, column 37
    function initialValue_0 () : WC7PolicyOwnerOfficer {
      return createNewOwnerOfficer()
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 31, column 25
    function initialValue_1 () : Contact[] {
      return line.WC7PolicyOwnerOfficers.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 35, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(policyOwnerOfficer.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 39, column 49
    function initialValue_3 () : gw.api.productmodel.ClausePattern {
      return null
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 43, column 23
    function initialValue_4 () : boolean {
      return policyOwnerOfficer.isIncluded()
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 47, column 22
    function initialValue_5 () : String {
      return isIncluded ? DisplayKey.get("Java.ProductModel.Name.Condition") : DisplayKey.get("Java.ProductModel.Name.Exclusion")
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 51, column 39
    function initialValue_6 () : WC7IncludedOwnerOfficer {
      return policyOwnerOfficer typeis WC7IncludedOwnerOfficer ? policyOwnerOfficer : null
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 55, column 76
    function initialValue_7 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at WC7NewOwnerOfficerPopup.pcf: line 59, column 23
    function initialValue_8 () : Contact {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact
    }
    
    // EditButtons at WC7NewOwnerOfficerPopup.pcf: line 75, column 72
    function label_13 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on TextInput (id=ScheduleParent_Input) at WC7NewOwnerOfficerPopup.pcf: line 89, column 47
    function label_16 () : java.lang.Object {
      return scheduleLabel
    }
    
    // 'pickValue' attribute on EditButtons at WC7NewOwnerOfficerPopup.pcf: line 75, column 72
    function pickValue_11 () : WC7PolicyOwnerOfficer {
      return policyOwnerOfficer
    }
    
    // 'title' attribute on Popup (id=WC7NewOwnerOfficerPopup) at WC7NewOwnerOfficerPopup.pcf: line 13, column 117
    static function title_51 (clausePattern :  gw.api.productmodel.ClausePattern, contactType :  ContactType, line :  WC7Line) : java.lang.Object {
      return DisplayKey.get("Web.Contact.NewContact", entity.PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function valueRange_23 () : java.lang.Object {
      return policyOwnerOfficer.Branch.WC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function valueRange_32 () : java.lang.Object {
      return includedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(includedOwnerOfficer.Jurisdiction))
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function valueRoot_22 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function valueRoot_31 () : java.lang.Object {
      return includedOwnerOfficer
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7NewOwnerOfficerPopup.pcf: line 89, column 47
    function value_17 () : gw.api.domain.Clause {
      return getParentClause()
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function value_20 () : typekey.Jurisdiction {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'value' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function value_29 () : entity.WC7ClassCode {
      return includedOwnerOfficer.WC7ClassCode
    }
    
    // 'value' attribute on TextInput (id=Ownership_Input) at WC7NewOwnerOfficerPopup.pcf: line 115, column 44
    function value_38 () : java.lang.Integer {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TypeKeyInput (id=Relationship_Input) at WC7NewOwnerOfficerPopup.pcf: line 122, column 47
    function value_42 () : typekey.Relationship {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function verifyValueRangeIsAllowedType_24 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function verifyValueRangeIsAllowedType_33 ($$arg :  entity.WC7ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function verifyValueRangeIsAllowedType_33 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewOwnerOfficerPopup.pcf: line 98, column 46
    function verifyValueRange_25 () : void {
      var __valueRangeArg = policyOwnerOfficer.Branch.WC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function verifyValueRange_34 () : void {
      var __valueRangeArg = includedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(includedOwnerOfficer.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'updateVisible' attribute on EditButtons at WC7NewOwnerOfficerPopup.pcf: line 75, column 72
    function visible_12 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    // 'visible' attribute on RangeInput (id=ClassCode_Input) at WC7NewOwnerOfficerPopup.pcf: line 109, column 35
    function visible_28 () : java.lang.Boolean {
      return isIncluded
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewOwnerOfficerPopup.pcf: line 72, column 62
    function visible_9 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.WC7NewOwnerOfficerPopup {
      return super.CurrentLocation as pcf.WC7NewOwnerOfficerPopup
    }
    
    property get clausePattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("clausePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set clausePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("clausePattern", 0, $arg)
    }
    
    property get contact () : Contact {
      return getVariableValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactType () : ContactType {
      return getVariableValue("contactType", 0) as ContactType
    }
    
    property set contactType ($arg :  ContactType) {
      setVariableValue("contactType", 0, $arg)
    }
    
    property get displayMembershipCheckError () : boolean {
      return getVariableValue("displayMembershipCheckError", 0) as java.lang.Boolean
    }
    
    property set displayMembershipCheckError ($arg :  boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get includedOwnerOfficer () : WC7IncludedOwnerOfficer {
      return getVariableValue("includedOwnerOfficer", 0) as WC7IncludedOwnerOfficer
    }
    
    property set includedOwnerOfficer ($arg :  WC7IncludedOwnerOfficer) {
      setVariableValue("includedOwnerOfficer", 0, $arg)
    }
    
    property get isIncluded () : boolean {
      return getVariableValue("isIncluded", 0) as java.lang.Boolean
    }
    
    property set isIncluded ($arg :  boolean) {
      setVariableValue("isIncluded", 0, $arg)
    }
    
    property get line () : WC7Line {
      return getVariableValue("line", 0) as WC7Line
    }
    
    property set line ($arg :  WC7Line) {
      setVariableValue("line", 0, $arg)
    }
    
    property get policyOwnerOfficer () : WC7PolicyOwnerOfficer {
      return getVariableValue("policyOwnerOfficer", 0) as WC7PolicyOwnerOfficer
    }
    
    property set policyOwnerOfficer ($arg :  WC7PolicyOwnerOfficer) {
      setVariableValue("policyOwnerOfficer", 0, $arg)
    }
    
    property get scheduleLabel () : String {
      return getVariableValue("scheduleLabel", 0) as String
    }
    
    property set scheduleLabel ($arg :  String) {
      setVariableValue("scheduleLabel", 0, $arg)
    }
    
    function createNewOwnerOfficer() : WC7PolicyOwnerOfficer {
      var ownerOfficerCondition : gw.api.productmodel.ConditionPattern = gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ConditionPattern>("WC7SoleProprietorsPartnersOfficersAndOthersCovCond")
      var ownerOfficerExclusion : gw.api.productmodel.ExclusionPattern = gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ExclusionPattern>("WC7PartnersOfficersAndOthersExclEndorsementExcl")
      
      var resultOwnerOfficer : WC7PolicyOwnerOfficer = null
      if(clausePattern != null) {
        if (clausePattern == ownerOfficerCondition){
          resultOwnerOfficer = line.addNewIncludedOwnerOfficerOfContactType(contactType, line.WC7SoleProprietorsPartnersOfficersAndOthersCovCond)
        } else if (clausePattern == ownerOfficerExclusion){
          resultOwnerOfficer = line.addNewExcludedOwnerOfficerOfContactType(contactType, line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
        }
      } else {
        var acctContact = line.Branch.Policy.Account.addNewAccountContactOfType(contactType)
        resultOwnerOfficer = line.addWC7PolicyOwnerOfficer(acctContact.Contact)
      }
      
      return resultOwnerOfficer
    }
    
    function getParentClause() : gw.api.domain.Clause {
      var clauseValue : gw.api.domain.Clause = null
      if (policyOwnerOfficer typeis entity.WC7IncludedOwnerOfficer){
        clauseValue = policyOwnerOfficer.OwnerOfficerCondition
      } else if (policyOwnerOfficer typeis entity.WC7ExcludedOwnerOfficer){
        clauseValue = policyOwnerOfficer.OwnerOfficerExclusion
      }
      return clauseValue
    }
    
    function isClassCodeEditable(anIncludedOwnerOfficer : WC7IncludedOwnerOfficer) : boolean {
      return (anIncludedOwnerOfficer.Jurisdiction != null) and 
        (anIncludedOwnerOfficer.WC7WorkersCompLine.getClassCodesForWC7CoveredEmployees(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(anIncludedOwnerOfficer.Jurisdiction)).HasElements)  
    }
    
    
  }
  
  
}