package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostRowSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateCostRowSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostRowSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateCostRowSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7StateCostRowSet.default.pcf: line 14, column 30
    function initialValue_0 () : entity.WC7Cost {
      return costWrapper.Cost as WC7Cost
    }
    
    // 'value' attribute on TextCell (id=LocNumber_Cell) at WC7StateCostRowSet.default.pcf: line 21, column 40
    function valueRoot_2 () : java.lang.Object {
      return costWrapper
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7StateCostRowSet.default.pcf: line 25, column 39
    function valueRoot_5 () : java.lang.Object {
      return aggCost
    }
    
    // 'value' attribute on TextCell (id=LocNumber_Cell) at WC7StateCostRowSet.default.pcf: line 21, column 40
    function value_1 () : java.lang.Integer {
      return costWrapper.LocNumber
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7StateCostRowSet.default.pcf: line 34, column 39
    function value_10 () : java.lang.String {
      return (aggCost.HasOverride and aggCost.Basis==0)? "" : aggCost.Basis?.DisplayValue?.split("\\.")?.first()
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7StateCostRowSet.default.pcf: line 40, column 39
    function value_12 () : java.lang.String {
      return aggCost.ActualAdjRate.asString()
    }
    
    // 'value' attribute on TextCell (id=TermAmount_Cell) at WC7StateCostRowSet.default.pcf: line 46, column 52
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return aggCost.ActualAmount
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7StateCostRowSet.default.pcf: line 25, column 39
    function value_4 () : java.lang.String {
      return aggCost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7StateCostRowSet.default.pcf: line 29, column 39
    function value_7 () : java.lang.String {
      return aggCost.Description
    }
    
    // 'visible' attribute on Row (id=AggCostRow) at WC7StateCostRowSet.default.pcf: line 17, column 37
    function visible_17 () : java.lang.Boolean {
      return costWrapper.Visible
    }
    
    property get aggCost () : entity.WC7Cost {
      return getVariableValue("aggCost", 0) as entity.WC7Cost
    }
    
    property set aggCost ($arg :  entity.WC7Cost) {
      setVariableValue("aggCost", 0, $arg)
    }
    
    property get costWrapper () : gw.api.ui.WC7CostWrapper {
      return getRequireValue("costWrapper", 0) as gw.api.ui.WC7CostWrapper
    }
    
    property set costWrapper ($arg :  gw.api.ui.WC7CostWrapper) {
      setRequireValue("costWrapper", 0, $arg)
    }
    
    
  }
  
  
}