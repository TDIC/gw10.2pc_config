package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GlobalAddressInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/address/GlobalAddressInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GlobalAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function available_11 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'available' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function available_25 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'available' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function available_36 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'available' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function available_48 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'available' attribute on TextInput (id=County_Input) at GlobalAddressInputSet.default.pcf: line 121, column 23
    function available_63 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'available' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function available_73 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'available' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function available_92 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function defaultSetter_100 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.PostalCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine3 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.City = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=County_Input) at GlobalAddressInputSet.default.pcf: line 121, column 23
    function defaultSetter_67 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.County = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.State = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function editable_12 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function editable_26 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function editable_37 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'editable' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function editable_49 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'editable' attribute on TextInput (id=County_Input) at GlobalAddressInputSet.default.pcf: line 121, column 23
    function editable_64 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'editable' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function editable_74 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'editable' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function editable_93 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.default.pcf: line 14, column 50
    function initialValue_0 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("City","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.default.pcf: line 18, column 50
    function initialValue_1 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("County","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.default.pcf: line 22, column 50
    function initialValue_2 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("PostalCode","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at GlobalAddressInputSet.default.pcf: line 29, column 33
    function initialValue_3 () : java.lang.Integer {
      if (addressOwner != null) addressOwner.InEditMode = CurrentLocation.InEditMode; return 0
    }
    
    // 'inputConversion' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function inputConversion_97 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.address.PostalCodeInputFormatter.convertPostalCode(VALUE, addressOwner.SelectedCountry)
    }
    
    // 'inputMask' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function inputMask_102 () : java.lang.String {
      return gw.api.contact.AddressAutocompleteUtil.getInputMask(addressOwner.AddressDelegate, "PostalCode")
    }
    
    // 'label' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function label_14 () : java.lang.Object {
      return addressOwner.AddressLine1Label
    }
    
    // 'label' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.default.pcf: line 34, column 50
    function label_5 () : java.lang.Object {
      return addressOwner.AddressNameLabel
    }
    
    // 'label' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function label_52 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).CityLabel
    }
    
    // 'label' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function label_77 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).StateLabel
    }
    
    // 'label' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function label_96 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.default.pcf: line 59, column 103
    function onChange_10 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.default.pcf: line 70, column 103
    function onChange_24 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.default.pcf: line 112, column 103
    function onChange_47 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.default.pcf: line 154, column 103
    function onChange_72 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'onChange' attribute on PostOnChange at GlobalAddressInputSet.default.pcf: line 189, column 103
    function onChange_91 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'required' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function required_15 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'required' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function required_28 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'required' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function required_39 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'required' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function required_53 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'required' attribute on TextInput (id=County_Input) at GlobalAddressInputSet.default.pcf: line 121, column 23
    function required_65 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'required' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function required_78 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'required' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function required_98 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    // 'validationExpression' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function validationExpression_50 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "City")
    }
    
    // 'validationExpression' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function validationExpression_75 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "State")
    }
    
    // 'validationExpression' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function validationExpression_94 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.validate(addressOwner.AddressDelegate, "PostalCode", gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PostalCodeLabel)
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function valueRange_82 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.getStates(addressOwner.AddressDelegate.Country)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function valueRoot_18 () : java.lang.Object {
      return addressOwner.AddressDelegate
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function value_16 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine1
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function value_29 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine2
    }
    
    // 'value' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function value_40 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine3
    }
    
    // 'value' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function value_54 () : java.lang.String {
      return addressOwner.AddressDelegate.City
    }
    
    // 'value' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.default.pcf: line 34, column 50
    function value_6 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(addressOwner.AddressDelegate, "\n")
    }
    
    // 'value' attribute on TextInput (id=County_Input) at GlobalAddressInputSet.default.pcf: line 121, column 23
    function value_66 () : java.lang.String {
      return addressOwner.AddressDelegate.County
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function value_79 () : typekey.State {
      return addressOwner.AddressDelegate.State
    }
    
    // 'value' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function value_99 () : java.lang.String {
      return addressOwner.AddressDelegate.PostalCode
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function verifyValueRangeIsAllowedType_83 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function verifyValueRangeIsAllowedType_83 ($$arg :  typekey.State[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function verifyValueRange_84 () : void {
      var __valueRangeArg = gw.api.contact.AddressAutocompleteUtil.getStates(addressOwner.AddressDelegate.Country)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_83(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine1_Input) at GlobalAddressInputSet.default.pcf: line 57, column 89
    function visible_13 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine2_Input) at GlobalAddressInputSet.default.pcf: line 68, column 89
    function visible_27 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine3_Input) at GlobalAddressInputSet.default.pcf: line 79, column 90
    function visible_38 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE3)
    }
    
    // 'visible' attribute on TextInput (id=AddressSummary_Input) at GlobalAddressInputSet.default.pcf: line 34, column 50
    function visible_4 () : java.lang.Boolean {
      return addressOwner.ShowAddressSummary
    }
    
    // 'visible' attribute on TextInput (id=City_Input) at GlobalAddressInputSet.default.pcf: line 110, column 81
    function visible_51 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.CITY)
    }
    
    // 'visible' attribute on RangeInput (id=State_Input) at GlobalAddressInputSet.default.pcf: line 148, column 82
    function visible_76 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.STATE)
    }
    
    // 'visible' attribute on TextInput (id=PostalCode_Input) at GlobalAddressInputSet.default.pcf: line 187, column 87
    function visible_95 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.POSTALCODE)
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    property get cityhandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("cityhandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set cityhandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("cityhandler", 0, $arg)
    }
    
    property get countyhandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("countyhandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set countyhandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("countyhandler", 0, $arg)
    }
    
    property get pchandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("pchandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set pchandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("pchandler", 0, $arg)
    }
    
    property get saveEditMode () : java.lang.Integer {
      return getVariableValue("saveEditMode", 0) as java.lang.Integer
    }
    
    property set saveEditMode ($arg :  java.lang.Integer) {
      setVariableValue("saveEditMode", 0, $arg)
    }
    
    
  }
  
  
}