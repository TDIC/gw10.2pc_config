package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInfoInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInfoInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AdditionalNamedCompanyAdder) at AccountInfoInputSet.pcf: line 65, column 94
    function action_10 () : void {
      if ( gw.web.account.AccountInfoInputSetUIHelper .canChangePrimaryNamedInsured(period, primaryNamedInsured)){ NewPrimaryNamedInsuredPopup.push(period, typekey.ContactType.TC_COMPANY) }
    }
    
    // 'action' attribute on MenuItem (id=AdditionalNamedPersonAdder) at AccountInfoInputSet.pcf: line 70, column 93
    function action_13 () : void {
      if( gw.web.account.AccountInfoInputSetUIHelper .canChangePrimaryNamedInsured(period, primaryNamedInsured)){ NewPrimaryNamedInsuredPopup.push(period, typekey.ContactType.TC_PERSON) }
    }
    
    // 'action' attribute on PickerMenuItem (id=PrimaryNamedInsuredABContactAdder) at AccountInfoInputSet.pcf: line 76, column 40
    function action_16 () : void {
      ContactSearchPopup.push(TC_NAMEDINSURED, period.Policy.Product)
    }
    
    // 'action' attribute on TextInput (id=Name_Input) at AccountInfoInputSet.pcf: line 100, column 144
    function action_27 () : void {
      EditPolicyContactRolePopup.push(primaryNamedInsured, openForEdit)
    }
    
    // 'action' attribute on MenuItem (id=EditPolicyAddressMenuItem) at AccountInfoInputSet.pcf: line 139, column 76
    function action_38 () : void {
      gw.web.account.AccountInfoInputSetUIHelper .openEditAddressPopup(period)
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_49 () : void {
      IndustryCodeSearchPopup.push(typekey.IndustryCodeType.TC_SIC, referenceDate, previousIndustryCode)
    }
    
    // 'action' attribute on PickerMenuItem (id=PrimaryNamedInsuredABContactAdder) at AccountInfoInputSet.pcf: line 76, column 40
    function action_dest_17 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(TC_NAMEDINSURED, period.Policy.Product)
    }
    
    // 'action' attribute on TextInput (id=Name_Input) at AccountInfoInputSet.pcf: line 100, column 144
    function action_dest_28 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(primaryNamedInsured, openForEdit)
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_dest_50 () : pcf.api.Destination {
      return pcf.IndustryCodeSearchPopup.createDestination(typekey.IndustryCodeType.TC_SIC, referenceDate, previousIndustryCode)
    }
    
    // 'available' attribute on TextInput (id=ChangePrimaryNamedInsuredButton_Input) at AccountInfoInputSet.pcf: line 60, column 29
    function available_23 () : java.lang.Boolean {
      return period.profileChange_TDIC
    }
    
    // 'def' attribute on InputSetRef (id=Phone) at AccountInfoInputSet.pcf: line 104, column 19
    function def_onEnter_31 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BasicPhoneOwner(new gw.api.phone.ContactPhoneDelegate(primaryNamedInsured.AccountContactRole.AccountContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetail.Phone") , false))
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 143, column 25
    function def_onEnter_41 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.onEnter(period, not openForEdit)
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 150, column 86
    function def_onEnter_44 (def :  pcf.PolicyOfficialIDInputSet_Company) : void {
      def.onEnter(primaryNamedInsured)
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 150, column 86
    function def_onEnter_46 (def :  pcf.PolicyOfficialIDInputSet_Person) : void {
      def.onEnter(primaryNamedInsured)
    }
    
    // 'def' attribute on InputSetRef (id=Phone) at AccountInfoInputSet.pcf: line 104, column 19
    function def_refreshVariables_32 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BasicPhoneOwner(new gw.api.phone.ContactPhoneDelegate(primaryNamedInsured.AccountContactRole.AccountContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetail.Phone") , false))
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 143, column 25
    function def_refreshVariables_42 (def :  pcf.PolicyAddressDisplayInputSet) : void {
      def.refreshVariables(period, not openForEdit)
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 150, column 86
    function def_refreshVariables_45 (def :  pcf.PolicyOfficialIDInputSet_Company) : void {
      def.refreshVariables(primaryNamedInsured)
    }
    
    // 'def' attribute on InputSetRef at AccountInfoInputSet.pcf: line 150, column 86
    function def_refreshVariables_47 (def :  pcf.PolicyOfficialIDInputSet_Person) : void {
      def.refreshVariables(primaryNamedInsured)
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      primaryNamedInsured.IndustryCode = (__VALUE_TO_SET as entity.IndustryCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at AccountInfoInputSet.pcf: line 175, column 42
    function defaultSetter_67 (__VALUE_TO_SET :  java.lang.Object) : void {
      period.AssignedRisk = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=YearBusinessStarted_Input) at AccountInfoInputSet.pcf: line 181, column 40
    function defaultSetter_73 (__VALUE_TO_SET :  java.lang.Object) : void {
      period.Policy.Account.YearBusinessStarted = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      period.PolicyOrgType_TDIC = (__VALUE_TO_SET as typekey.PolicyOrgType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=OrganizationTypeOtherDescription_Input) at AccountInfoInputSet.pcf: line 233, column 108
    function defaultSetter_94 (__VALUE_TO_SET :  java.lang.Object) : void {
      period.Policy.Account.OtherOrgTypeDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=YearBusinessStarted_Input) at AccountInfoInputSet.pcf: line 181, column 40
    function editable_71 () : java.lang.Boolean {
      return period.Policy.Account.Editable
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 17, column 75
    function initialValue_0 () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> period.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 22, column 33
    function initialValue_1 () : java.lang.Boolean {
      return openForEditInit.get()
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 26, column 23
    function initialValue_2 () : boolean {
      return gw.web.account.AccountInfoInputSetUIHelper .getInitialValueForOfficalIDsUpdated(period,  CurrentLocation.InEditMode)
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 31, column 37
    function initialValue_3 () : PolicyPriNamedInsured {
      return period.PrimaryNamedInsured
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 36, column 28
    function initialValue_4 () : Jurisdiction {
      return gw.api.util.JurisdictionMappingUtil.getJurisdiction(period.PolicyAddress.Address)
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 41, column 30
    function initialValue_5 () : java.util.Date {
      return referenceState != null ? period.getReferenceDateForCurrentJob(referenceState) : period.EditEffectiveDate
    }
    
    // 'initialValue' attribute on Variable at AccountInfoInputSet.pcf: line 45, column 28
    function initialValue_6 () : IndustryCode {
      return period.Job.NewTerm ? null : primaryNamedInsured.BasedOn.IndustryCode
    }
    
    // 'inputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function inputConversion_52 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.findCode(VALUE, typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'label' attribute on MenuItem (id=AdditionalNamedCompanyAdder) at AccountInfoInputSet.pcf: line 65, column 94
    function label_11 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", typekey.ContactType.TC_COMPANY)
    }
    
    // 'label' attribute on MenuItem (id=AdditionalNamedPersonAdder) at AccountInfoInputSet.pcf: line 70, column 93
    function label_14 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", typekey.ContactType.TC_PERSON)
    }
    
    // 'mode' attribute on InputSetRef at AccountInfoInputSet.pcf: line 150, column 86
    function mode_48 () : java.lang.Object {
      return primaryNamedInsured.AccountContactRole.AccountContact.ContactType.Code
    }
    
    // 'onChange' attribute on PostOnChange at AccountInfoInputSet.pcf: line 225, column 103
    function onChange_79 () : void {
      tdic.pc.config.pcf.job.TDIC_PolicyInfoHelper.onPolicyOrgTypeChange_TDIC(period)
    }
    
    // 'onPick' attribute on PickerMenuItem (id=PrimaryNamedInsuredABContactAdder) at AccountInfoInputSet.pcf: line 76, column 40
    function onPick_18 (PickedValue :  Contact) : void {
      gw.web.account.AccountInfoInputSetUIHelper .changePrimaryNamedInsured(period, primaryNamedInsured, PickedValue);gw.policy.PolicyContactRoleValidation.validatePNIChanges_TDIC(period.PrimaryNamedInsured, true)
    }
    
    // 'outputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function outputConversion_53 (VALUE :  entity.IndustryCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'requestValidationExpression' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function requestValidationExpression_54 (VALUE :  entity.IndustryCode) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.validateValue(VALUE, previousIndustryCode.Code, referenceDate)
    }
    
    // 'required' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function required_55 () : java.lang.Boolean {
      return primaryNamedInsured.Branch.HasWorkersComp
    }
    
    // 'sortBy' attribute on IteratorSort at AccountInfoInputSet.pcf: line 88, column 28
    function sortBy_19 (unassignedContact :  entity.AccountContactView) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at AccountInfoInputSet.pcf: line 122, column 28
    function sortBy_33 (pniAddress :  entity.Address) : java.lang.Object {
      return pniAddress.DisplayName
    }
    
    // 'valueRange' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function valueRange_85 () : java.lang.Object {
      return getAccountOrgTypeSelections_TDIC()
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function valueRoot_58 () : java.lang.Object {
      return primaryNamedInsured
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at AccountInfoInputSet.pcf: line 175, column 42
    function valueRoot_68 () : java.lang.Object {
      return period
    }
    
    // 'value' attribute on TextInput (id=YearBusinessStarted_Input) at AccountInfoInputSet.pcf: line 181, column 40
    function valueRoot_74 () : java.lang.Object {
      return period.Policy.Account
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at AccountInfoInputSet.pcf: line 85, column 51
    function value_22 () : entity.AccountContactView[] {
      return gw.web.account.AccountInfoInputSetUIHelper.getOtherContacts(period)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AccountInfoInputSet.pcf: line 100, column 144
    function value_29 () : java.lang.String {
      return primaryNamedInsured.DisplayName//primaryNamedInsured.AccountContactRole.AccountContact.fullNameWithProfessionalCredentials_TDIC
    }
    
    // 'value' attribute on MenuItemIterator (id=pniAddressMenuItemIterator) at AccountInfoInputSet.pcf: line 119, column 40
    function value_36 () : entity.Address[] {
      return period.PolicyAddressCandidates
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function value_56 () : entity.IndustryCode {
      return primaryNamedInsured.IndustryCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at AccountInfoInputSet.pcf: line 175, column 42
    function value_66 () : java.lang.Boolean {
      return period.AssignedRisk
    }
    
    // 'value' attribute on TextInput (id=YearBusinessStarted_Input) at AccountInfoInputSet.pcf: line 181, column 40
    function value_72 () : java.lang.Integer {
      return period.Policy.Account.YearBusinessStarted
    }
    
    // 'value' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function value_82 () : typekey.PolicyOrgType_TDIC {
      return period.PolicyOrgType_TDIC
    }
    
    // 'value' attribute on TextInput (id=OrganizationTypeOtherDescription_Input) at AccountInfoInputSet.pcf: line 233, column 108
    function value_93 () : java.lang.String {
      return period.Policy.Account.OtherOrgTypeDescription
    }
    
    // 'valueRange' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function verifyValueRangeIsAllowedType_86 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function verifyValueRangeIsAllowedType_86 ($$arg :  typekey.PolicyOrgType_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function verifyValueRange_87 () : void {
      var __valueRangeArg = getAccountOrgTypeSelections_TDIC()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_86(__valueRangeArg)
    }
    
    // 'visible' attribute on MenuItem (id=AdditionalNamedPersonAdder) at AccountInfoInputSet.pcf: line 70, column 93
    function visible_12 () : java.lang.Boolean {
      return period.Policy.Product.isContactTypeSuitableForProductAccountType(Person)
    }
    
    // 'visible' attribute on PickerMenuItem (id=PrimaryNamedInsuredABContactAdder) at AccountInfoInputSet.pcf: line 76, column 40
    function visible_15 () : java.lang.Boolean {
      return not period.Promoted
    }
    
    // 'visible' attribute on TextInput (id=ChangePrimaryNamedInsuredButton_Input) at AccountInfoInputSet.pcf: line 60, column 29
    function visible_24 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'visible' attribute on Label at AccountInfoInputSet.pcf: line 147, column 84
    function visible_43 () : java.lang.Boolean {
      return !(period.GLLineExists && quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function visible_51 () : java.lang.Boolean {
      return primaryNamedInsured.Branch.WC7LineExists and primaryNamedInsured.Branch.Policy.Product.isContactTypeSuitableForProductAccountType(Company)
    }
    
    // 'visible' attribute on BooleanRadioInput (id=AssignedRisk_Input) at AccountInfoInputSet.pcf: line 175, column 42
    function visible_65 () : java.lang.Boolean {
      return period.HasWorkersComp
    }
    
    // 'visible' attribute on TextInput (id=PrimaryNamedInsuredLabel_Input) at AccountInfoInputSet.pcf: line 52, column 34
    function visible_7 () : java.lang.Boolean {
      return not openForEdit
    }
    
    // 'visible' attribute on InputSet (id=CommercialInputSet) at AccountInfoInputSet.pcf: line 166, column 118
    function visible_78 () : java.lang.Boolean {
      return period.Policy.Product.Commercial and not period.WC7LineExists and !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on RangeInput (id=OrganizationType_Input) at AccountInfoInputSet.pcf: line 223, column 54
    function visible_81 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on MenuItem (id=AdditionalNamedCompanyAdder) at AccountInfoInputSet.pcf: line 65, column 94
    function visible_9 () : java.lang.Boolean {
      return period.Policy.Product.isContactTypeSuitableForProductAccountType(Company)
    }
    
    // 'visible' attribute on TextInput (id=OrganizationTypeOtherDescription_Input) at AccountInfoInputSet.pcf: line 233, column 108
    function visible_92 () : java.lang.Boolean {
      return (period.GLLineExists or period.BOPLineExists) and period.PolicyOrgType_TDIC == TC_OTHER
    }
    
    property get officialIDsUpdated () : boolean {
      return getVariableValue("officialIDsUpdated", 0) as java.lang.Boolean
    }
    
    property set officialIDsUpdated ($arg :  boolean) {
      setVariableValue("officialIDsUpdated", 0, $arg)
    }
    
    property get openForEdit () : java.lang.Boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  java.lang.Boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get openForEditInit () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return getVariableValue("openForEditInit", 0) as gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>
    }
    
    property set openForEditInit ($arg :  gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>) {
      setVariableValue("openForEditInit", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get previousIndustryCode () : IndustryCode {
      return getVariableValue("previousIndustryCode", 0) as IndustryCode
    }
    
    property set previousIndustryCode ($arg :  IndustryCode) {
      setVariableValue("previousIndustryCode", 0, $arg)
    }
    
    property get primaryNamedInsured () : PolicyPriNamedInsured {
      return getVariableValue("primaryNamedInsured", 0) as PolicyPriNamedInsured
    }
    
    property set primaryNamedInsured ($arg :  PolicyPriNamedInsured) {
      setVariableValue("primaryNamedInsured", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getRequireValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    property get referenceDate () : java.util.Date {
      return getVariableValue("referenceDate", 0) as java.util.Date
    }
    
    property set referenceDate ($arg :  java.util.Date) {
      setVariableValue("referenceDate", 0, $arg)
    }
    
    property get referenceState () : Jurisdiction {
      return getVariableValue("referenceState", 0) as Jurisdiction
    }
    
    property set referenceState ($arg :  Jurisdiction) {
      setVariableValue("referenceState", 0, $arg)
    }
    
    function getAccountOrgTypeSelections_TDIC() : typekey.PolicyOrgType_TDIC[] {
      if (period.WC7LineExists) {
        return typekey.PolicyOrgType_TDIC.AllTypeKeys.where(\elt -> elt != TC_OTHER).toTypedArray()
      }
      if(period.GLLineExists) {
        if (period.Offering.CodeIdentifier == "PLCyberLiab_TDIC")
          return typekey.PolicyOrgType_TDIC.getAllTypeKeys().where(\elt -> elt.Code != "nonprofit").toTypedArray()
        else
          return PolicyOrgType_TDIC.getAllTypeKeys().where(\elt -> elt.Code == "individual").toTypedArray()
      }       
      return typekey.PolicyOrgType_TDIC.AllTypeKeys.toTypedArray()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountInfoInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=pniAddressMenuItem) at AccountInfoInputSet.pcf: line 126, column 33
    function action_34 () : void {
      period.changePolicyAddressTo(pniAddress)
    }
    
    // 'label' attribute on MenuItem (id=pniAddressMenuItem) at AccountInfoInputSet.pcf: line 126, column 33
    function label_35 () : java.lang.Object {
      return pniAddress
    }
    
    property get pniAddress () : entity.Address {
      return getIteratedValue(1) as entity.Address
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountInfoInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at AccountInfoInputSet.pcf: line 92, column 40
    function action_20 () : void {
      gw.web.account.AccountInfoInputSetUIHelper .changeToExistingContact(period, primaryNamedInsured, unassignedContact.AccountContact.Contact); gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, entity.PolicyAddlNamedInsured);gw.policy.PolicyContactRoleValidation.validatePNIChanges_TDIC(period.PrimaryNamedInsured, true)
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at AccountInfoInputSet.pcf: line 92, column 40
    function label_21 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  
}