package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyOfficialIDInputSet.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyOfficialIDInputSet_CompanyExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyOfficialIDInputSet.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyOfficialIDInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.AccountContactRole.AccountContact.Contact.FEINOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'inputMask' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function inputMask_5 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.InputMask
    }
    
    // 'regex' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function regex_6 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.Regex
    }
    
    // 'required' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function required_1 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.isFEINRequired(policyContactRole)
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function valueRoot_4 () : java.lang.Object {
      return policyContactRole.AccountContactRole.AccountContact.Contact
    }
    
    // 'value' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function value_2 () : java.lang.String {
      return policyContactRole.AccountContactRole.AccountContact.Contact.FEINOfficialID
    }
    
    // 'visible' attribute on TextInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Company.pcf: line 19, column 113
    function visible_0 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.getVisibilityOfPolicContactFields_TDIC(policyContactRole)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    
  }
  
  
}