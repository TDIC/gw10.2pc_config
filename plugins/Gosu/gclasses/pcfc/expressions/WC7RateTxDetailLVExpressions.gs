package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RateTxDetailLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7RateTxDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RateTxDetailLV.pcf: line 117, column 48
    function def_onEnter_30 (def :  pcf.WC7RateTxDetailAggRowSet) : void {
      def.onEnter(aggTx)
    }
    
    // 'def' attribute on RowSetRef at WC7RateTxDetailLV.pcf: line 117, column 48
    function def_refreshVariables_31 (def :  pcf.WC7RateTxDetailAggRowSet) : void {
      def.refreshVariables(aggTx)
    }
    
    property get aggTx () : WC7Transaction {
      return getIteratedValue(1) as WC7Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7RateTxDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7RateTxDetailLV.pcf: line 57, column 38
    function initialValue_6 () : entity.WC7CovEmpCost {
      return covEmpTx.WC7Cost as WC7CovEmpCost
    }
    
    // RowIterator at WC7RateTxDetailLV.pcf: line 53, column 43
    function initializeVariables_26 () : void {
        cost = covEmpTx.WC7Cost as WC7CovEmpCost;

    }
    
    // 'value' attribute on MonetaryAmountCell (id=TxAmount_Cell) at WC7RateTxDetailLV.pcf: line 99, column 43
    function valueRoot_23 () : java.lang.Object {
      return covEmpTx
    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7RateTxDetailLV.pcf: line 76, column 42
    function valueRoot_8 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RateTxDetailLV.pcf: line 80, column 41
    function value_10 () : java.lang.String {
      return cost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7RateTxDetailLV.pcf: line 85, column 24
    function value_13 () : java.lang.String {
      return cost.Description
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7RateTxDetailLV.pcf: line 90, column 45
    function value_16 () : java.math.BigDecimal {
      return cost.Basis
    }
    
    // 'value' attribute on TextCell (id=ModifiedTermRate_Cell) at WC7RateTxDetailLV.pcf: line 95, column 45
    function value_19 () : java.math.BigDecimal {
      return cost.ActualAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TxAmount_Cell) at WC7RateTxDetailLV.pcf: line 99, column 43
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return covEmpTx.AmountBilling
    }
    
    // 'value' attribute on TextCell (id=LocationNum_Cell) at WC7RateTxDetailLV.pcf: line 76, column 42
    function value_7 () : java.lang.Integer {
      return cost.LocationNum
    }
    
    // 'visible' attribute on Row (id=CovEmpTxAmountRow) at WC7RateTxDetailLV.pcf: line 72, column 53
    function visible_25 () : java.lang.Boolean {
      return not covEmpTx.AmountBilling.IsZero
    }
    
    property get cost () : entity.WC7CovEmpCost {
      return getVariableValue("cost", 1) as entity.WC7CovEmpCost
    }
    
    property set cost ($arg :  entity.WC7CovEmpCost) {
      setVariableValue("cost", 1, $arg)
    }
    
    property get covEmpTx () : WC7Transaction {
      return getIteratedValue(1) as WC7Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RateTxDetailLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7RateTxDetailLV.pcf: line 14, column 51
    function initialValue_0 () : java.util.Set<WC7Transaction> {
      return periodTxs.findByCostType( WC7CovEmpCost )
    }
    
    // 'initialValue' attribute on Variable at WC7RateTxDetailLV.pcf: line 19, column 51
    function initialValue_1 () : java.util.Set<WC7Transaction> {
      return periodTxs.findByCostType( WC7JurisdictionCost )
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 60, column 24
    function sortBy_2 (covEmpTx :  WC7Transaction) : java.lang.Object {
      return covEmpTx.WC7Cost.LocationNum
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 112, column 24
    function sortBy_28 (aggTx :  WC7Transaction) : java.lang.Object {
      return (aggTx.WC7Cost as WC7JurisdictionCost).CalcOrder
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 115, column 24
    function sortBy_29 (aggTx :  WC7Transaction) : java.lang.Object {
      return aggTx.Proration
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 63, column 24
    function sortBy_3 (covEmpTx :  WC7Transaction) : java.lang.Object {
      return covEmpTx.WC7Cost.ClassCode
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 66, column 24
    function sortBy_4 (covEmpTx :  WC7Transaction) : java.lang.Object {
      return covEmpTx.EffDate
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailLV.pcf: line 69, column 24
    function sortBy_5 (covEmpTx :  WC7Transaction) : java.lang.Object {
      return covEmpTx.Proration
    }
    
    // 'value' attribute on RowIterator at WC7RateTxDetailLV.pcf: line 53, column 43
    function value_27 () : entity.WC7Transaction[] {
      return covEmpTxs.toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=lt400) at WC7RateTxDetailLV.pcf: line 109, column 43
    function value_32 () : entity.WC7Transaction[] {
      return aggTxs.byCalcOrder( 0, 400 ).toTypedArray().sortBy(\ elt -> elt.WC7Cost.LocationNum)
    }
    
    // 'value' attribute on TextCell (id=TxAmountSubtotal300_Cell) at WC7RateTxDetailLV.pcf: line 142, column 39
    function value_33 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(periodTxs.byCalcOrder( 0, 400 )*.AmountBilling*.Amount.sum() as java.lang.Double, "currency")
    }
    
    property get aggTxs () : java.util.Set<WC7Transaction> {
      return getVariableValue("aggTxs", 0) as java.util.Set<WC7Transaction>
    }
    
    property set aggTxs ($arg :  java.util.Set<WC7Transaction>) {
      setVariableValue("aggTxs", 0, $arg)
    }
    
    property get covEmpTxs () : java.util.Set<WC7Transaction> {
      return getVariableValue("covEmpTxs", 0) as java.util.Set<WC7Transaction>
    }
    
    property set covEmpTxs ($arg :  java.util.Set<WC7Transaction>) {
      setVariableValue("covEmpTxs", 0, $arg)
    }
    
    property get periodTxs () : java.util.Set<WC7Transaction> {
      return getRequireValue("periodTxs", 0) as java.util.Set<WC7Transaction>
    }
    
    property set periodTxs ($arg :  java.util.Set<WC7Transaction>) {
      setRequireValue("periodTxs", 0, $arg)
    }
    
    
  }
  
  
}