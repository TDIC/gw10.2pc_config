package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_RateCumulGLHistoricalItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_RateCumulGLHistoricalItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_RateCumulGLHistoricalItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_RateCumulGLHistoricalItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 44, column 25
    function valueRoot_14 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 68, column 25
    function valueRoot_24 () : java.lang.Object {
      return cost.GLSplitParameters
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 44, column 25
    function value_13 () : java.math.BigDecimal {
      return cost.Basis
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 52, column 25
    function value_16 () : java.math.BigDecimal {
      return cost.ActualAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 61, column 25
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualTermAmountBilling
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 68, column 25
    function value_23 () : java.util.Date {
      return cost.GLSplitParameters.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 75, column 25
    function value_27 () : java.util.Date {
      return cost.GLSplitParameters.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Proration_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 83, column 25
    function value_31 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(cost.ProRataByDaysValue, "#0.0000")
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 92, column 25
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 61, column 25
    function visible_21 () : java.lang.Boolean {
      return prorated
    }
    
    property get cost () : entity.GLHistoricalCost_TDIC {
      return getIteratedValue(1) as entity.GLHistoricalCost_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_RateCumulGLHistoricalItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_RateCumulGLHistoricalItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 13, column 23
    function initialValue_0 () : boolean {
      return glHistCosts.AnyProrated
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 22, column 24
    function sortBy_1 (cost :  entity.GLHistoricalCost_TDIC) : java.lang.Object {
      return cost.Coverage.CoverageCategory.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 25, column 24
    function sortBy_2 (cost :  entity.GLHistoricalCost_TDIC) : java.lang.Object {
      return cost.Coverage.Pattern.Priority
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 92, column 25
    function sumValueRoot_12 (cost :  entity.GLHistoricalCost_TDIC) : java.lang.Object {
      return cost
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 92, column 25
    function sumValue_11 (cost :  entity.GLHistoricalCost_TDIC) : java.lang.Object {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on RowIterator at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 19, column 50
    function value_37 () : entity.GLHistoricalCost_TDIC[] {
      return glHistCosts.toTypedArray()
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at TDIC_RateCumulGLHistoricalItemsLV.pcf: line 61, column 25
    function visible_3 () : java.lang.Boolean {
      return prorated
    }
    
    property get glHistCosts () : java.util.Set<GLHistoricalCost_TDIC> {
      return getRequireValue("glHistCosts", 0) as java.util.Set<GLHistoricalCost_TDIC>
    }
    
    property set glHistCosts ($arg :  java.util.Set<GLHistoricalCost_TDIC>) {
      setRequireValue("glHistCosts", 0, $arg)
    }
    
    property get prorated () : boolean {
      return getVariableValue("prorated", 0) as java.lang.Boolean
    }
    
    property set prorated ($arg :  boolean) {
      setVariableValue("prorated", 0, $arg)
    }
    
    
  }
  
  
}