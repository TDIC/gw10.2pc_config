package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentsLV.pcf: line 27, column 23
    function initialValue_0 () : boolean {
      return documentsActionsHelper.DocumentContentActionsAvailable
    }
    
    // 'initialValue' attribute on Variable at DocumentsLV.pcf: line 31, column 23
    function initialValue_1 () : boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'sortBy' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 183, column 53
    function sortValue_10 (Document :  entity.Document) : java.lang.Object {
      return Document.Obsolete
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at DocumentsLV.pcf: line 65, column 25
    function sortValue_2 (Document :  entity.Document) : java.lang.Object {
      return Document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 137, column 41
    function sortValue_3 (Document :  entity.Document) : java.lang.Object {
      return Document.Description
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function sortValue_4 (Document :  entity.Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function sortValue_5 (Document :  entity.Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 160, column 25
    function sortValue_6 (Document :  entity.Document) : java.lang.Object {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 165, column 36
    function sortValue_7 (Document :  entity.Document) : java.lang.Object {
      return Document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 169, column 40
    function sortValue_8 (Document :  entity.Document) : java.lang.Object {
      return Document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at DocumentsLV.pcf: line 176, column 25
    function sortValue_9 (Document :  entity.Document) : java.lang.Object {
      return Document.DateModified
    }
    
    // 'value' attribute on RowIterator at DocumentsLV.pcf: line 39, column 75
    function value_68 () : gw.api.database.IQueryBeanResult<entity.Document> {
      return docQuery
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 183, column 53
    function visible_11 () : java.lang.Boolean {
      return searchCriteria.IncludeObsoletes
    }
    
    property get contentActionsEnabled () : boolean {
      return getVariableValue("contentActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set contentActionsEnabled ($arg :  boolean) {
      setVariableValue("contentActionsEnabled", 0, $arg)
    }
    
    property get docQuery () : gw.api.database.IQueryBeanResult<Document> {
      return getRequireValue("docQuery", 0) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property set docQuery ($arg :  gw.api.database.IQueryBeanResult<Document>) {
      setRequireValue("docQuery", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get metadataActionsEnabled () : boolean {
      return getVariableValue("metadataActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set metadataActionsEnabled ($arg :  boolean) {
      setVariableValue("metadataActionsEnabled", 0, $arg)
    }
    
    property get searchCriteria () : DocumentSearchCriteria {
      return getRequireValue("searchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set searchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get viewOnly () : boolean {
      return getRequireValue("viewOnly", 0) as java.lang.Boolean
    }
    
    property set viewOnly ($arg :  boolean) {
      setRequireValue("viewOnly", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 73, column 148
    function action_18 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 81, column 146
    function action_23 () : void {
      Document.downloadContent()
    }
    
    // 'action' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 92, column 124
    function action_27 () : void {
      new tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper ().finalizeLiveDocument(Document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 99, column 117
    function action_29 () : void {
      DocumentDetailsPopup.push(Document)
    }
    
    // 'action' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 127, column 85
    function action_33 () : void {
      gw.api.web.document.DocumentsHelper.deleteDocument(Document); gw.api.web.document.DocumentsHelper.evictDeletedDocument(Document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 99, column 117
    function action_dest_30 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(Document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 73, column 148
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, contentActionsEnabled) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 81, column 146
    function available_21 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, contentActionsEnabled)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 99, column 117
    function available_28 () : java.lang.Boolean {
      return metadataActionsEnabled
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 42, column 35
    function condition_12 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(Document) and documentsActionsHelper.isDeleteDocumentLinkAvailable(Document)
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 47, column 34
    function condition_13 () : java.lang.Boolean {
      return perm.Document.edit(Document)
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 50, column 24
    function condition_14 () : java.lang.Boolean {
      return Document.Obsolete
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at DocumentsLV.pcf: line 58, column 32
    function icon_15 () : java.lang.String {
      return Document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 73, column 148
    function label_19 () : java.lang.Object {
      return Document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at DocumentsLV.pcf: line 132, column 75
    function label_35 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'value' attribute on Reflect at DocumentsLV.pcf: line 153, column 83
    function reflectionValue_45 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 73, column 148
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function valueRange_41 () : java.lang.Object {
      return DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
    }
    
    // 'valueRange' attribute on Reflect at DocumentsLV.pcf: line 153, column 83
    function valueRange_47 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function valueRange_50 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(Document.Type)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 137, column 41
    function valueRoot_37 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 137, column 41
    function value_36 () : java.lang.String {
      return Document.Description
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function value_39 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function value_48 () : typekey.OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 160, column 25
    function value_54 () : typekey.DocumentStatusType {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 165, column 36
    function value_57 () : java.lang.String {
      return Document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 169, column 40
    function value_60 () : java.lang.String {
      return Document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at DocumentsLV.pcf: line 176, column 25
    function value_63 () : java.util.Date {
      return Document.DateModified
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function verifyValueRangeIsAllowedType_42 ($$arg :  typekey.DocumentType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function verifyValueRangeIsAllowedType_51 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at DocumentsLV.pcf: line 143, column 45
    function verifyValueRange_43 () : void {
      var __valueRangeArg = DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at DocumentsLV.pcf: line 149, column 57
    function verifyValueRange_52 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(Document.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 73, column 148
    function visible_17 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 81, column 146
    function visible_22 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 92, column 124
    function visible_26 () : java.lang.Boolean {
      return Document.MimeType == "application/dlf" and Document.Status == DocumentStatusType.TC_DRAFT
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 127, column 85
    function visible_32 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(Document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at DocumentsLV.pcf: line 132, column 75
    function visible_34 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(Document)
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 183, column 53
    function visible_67 () : java.lang.Boolean {
      return searchCriteria.IncludeObsoletes
    }
    
    property get Document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}