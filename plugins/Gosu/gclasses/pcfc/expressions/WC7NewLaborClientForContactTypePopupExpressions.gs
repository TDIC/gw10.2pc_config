package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborClientForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7NewLaborClientForContactTypePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewLaborClientForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7NewLaborClientForContactTypePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewLaborClientForContactTypePopup.pcf: line 39, column 62
    function action_4 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(detail))
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at WC7NewLaborClientForContactTypePopup.pcf: line 47, column 62
    function action_9 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'beforeCommit' attribute on Popup (id=WC7NewLaborClientForContactTypePopup) at WC7NewLaborClientForContactTypePopup.pcf: line 13, column 34
    function beforeCommit_38 (pickedValue :  WC7LaborContactDetail) : void {
      detail.WC7LaborContact.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch)
    }
    
    // 'beforeValidate' attribute on Popup (id=WC7NewLaborClientForContactTypePopup) at WC7NewLaborClientForContactTypePopup.pcf: line 13, column 34
    function beforeValidate_39 (pickedValue :  WC7LaborContactDetail) : void {
      gw.lob.wc7.WC7NewLaborContactValidation.validate(detail)
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborClientForContactTypePopup.pcf: line 86, column 77
    function def_onEnter_36 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(detail.WC7LaborContact, false)
    }
    
    // 'def' attribute on PanelRef at WC7NewLaborClientForContactTypePopup.pcf: line 86, column 77
    function def_refreshVariables_37 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(detail.WC7LaborContact, false)
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 75, column 52
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.ContractEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 82, column 52
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      detail.ContractExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborClientForContactTypePopup.pcf: line 22, column 44
    function initialValue_0 () : entity.WC7LaborContactDetail {
      return presenter.addNewDetail() as WC7LaborContactDetail
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborClientForContactTypePopup.pcf: line 26, column 25
    function initialValue_1 () : Contact[] {
      return presenter.Line.WC7PolicyLaborClients.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at WC7NewLaborClientForContactTypePopup.pcf: line 30, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(detail.WC7Contact.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'label' attribute on TextInput (id=ScheduleParent_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 56, column 47
    function label_10 () : java.lang.Object {
      return presenter.ScheduleLabel
    }
    
    // EditButtons at WC7NewLaborClientForContactTypePopup.pcf: line 42, column 72
    function label_7 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at WC7NewLaborClientForContactTypePopup.pcf: line 42, column 72
    function pickValue_5 () : WC7LaborContactDetail {
      return detail
    }
    
    // 'title' attribute on Popup (id=WC7NewLaborClientForContactTypePopup) at WC7NewLaborClientForContactTypePopup.pcf: line 13, column 34
    static function title_40 (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : java.lang.Object {
      return presenter.PopupTitle
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function valueRange_19 () : java.lang.Object {
      return detail.Branch.WC7Line.stateFilterFor(presenter.ParentClause.Pattern).TypeKeys
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 56, column 47
    function valueRoot_12 () : java.lang.Object {
      return presenter
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function valueRoot_18 () : java.lang.Object {
      return detail
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 56, column 47
    function value_11 () : gw.api.domain.Clause {
      return presenter.ParentClause
    }
    
    // 'value' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function value_16 () : typekey.Jurisdiction {
      return detail.Jurisdiction
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 75, column 52
    function value_25 () : java.util.Date {
      return detail.ContractEffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 82, column 52
    function value_31 () : java.util.Date {
      return detail.ContractExpirationDate
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function verifyValueRangeIsAllowedType_20 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function verifyValueRange_21 () : void {
      var __valueRangeArg = detail.Branch.WC7Line.stateFilterFor(presenter.ParentClause.Pattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=State_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 66, column 43
    function visible_15 () : java.lang.Boolean {
      return presenter.ShowState
    }
    
    // 'visible' attribute on DateInput (id=EffectiveDate_Input) at WC7NewLaborClientForContactTypePopup.pcf: line 75, column 52
    function visible_24 () : java.lang.Boolean {
      return presenter.ShowContractDates
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewLaborClientForContactTypePopup.pcf: line 39, column 62
    function visible_3 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at WC7NewLaborClientForContactTypePopup.pcf: line 42, column 72
    function visible_6 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.WC7NewLaborClientForContactTypePopup {
      return super.CurrentLocation as pcf.WC7NewLaborClientForContactTypePopup
    }
    
    property get detail () : entity.WC7LaborContactDetail {
      return getVariableValue("detail", 0) as entity.WC7LaborContactDetail
    }
    
    property set detail ($arg :  entity.WC7LaborContactDetail) {
      setVariableValue("detail", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get presenter () : gw.lob.wc7.schedule.WC7ScheduleClientPresenter {
      return getVariableValue("presenter", 0) as gw.lob.wc7.schedule.WC7ScheduleClientPresenter
    }
    
    property set presenter ($arg :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) {
      setVariableValue("presenter", 0, $arg)
    }
    
    
  }
  
  
}