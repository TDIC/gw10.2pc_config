package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/BillingAdjustmentsInstallmentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingAdjustmentsInstallmentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/BillingAdjustmentsInstallmentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingAdjustmentsInstallmentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BillingAdjustmentsInstallmentsLV.pcf: line 20, column 61
    function initialValue_0 () : gw.web.policy.InstallmentPlanDataRenderHelper {
      return new gw.web.policy.InstallmentPlanDataRenderHelper(policyPeriod.TotalPremiumRPT)
    }
    
    // 'initialValue' attribute on Variable at BillingAdjustmentsInstallmentsLV.pcf: line 24, column 32
    function initialValue_1 () : java.lang.String {
      return tdic.web.pcf.helper.PolicyPaymentPlanScreenHelper.getDefaultIntallmentPlanBillingID(installmentPlans, policyPeriod, policyPeriodBillingInstructionsManager)
    }
    
    // 'value' attribute on RadioButtonCell (id=InstallmentPlan_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 42, column 24
    function sortValue_2 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return defaultPaymentPlanChoiceID == plan.BillingId//policyPeriodBillingInstructionsManager.PaymentPlanChoice.BillingId == plan.BillingId
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 54, column 24
    function sortValue_4 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return plan.Name
    }
    
    // 'sortBy' attribute on FormatCell (id=DownPayment) at BillingAdjustmentsInstallmentsLV.pcf: line 73, column 24
    function sortValue_6 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return plan.DownPayment
    }
    
    // 'sortBy' attribute on FormatCell (id=Installment) at BillingAdjustmentsInstallmentsLV.pcf: line 92, column 24
    function sortValue_7 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return plan.Installment
    }
    
    // 'sortBy' attribute on FormatCell (id=Total) at BillingAdjustmentsInstallmentsLV.pcf: line 111, column 24
    function sortValue_8 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return plan.Total
    }
    
    // 'sortBy' attribute on TextCell (id=Notes_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 130, column 31
    function sortValue_9 (plan :  gw.plugin.billing.InstallmentPlanData) : java.lang.Object {
      return plan.Notes
    }
    
    // 'value' attribute on RowIterator at BillingAdjustmentsInstallmentsLV.pcf: line 29, column 59
    function value_35 () : gw.plugin.billing.InstallmentPlanData[] {
      return installmentPlans
    }
    
    // 'visible' attribute on RadioButtonCell (id=InstallmentPlan_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 42, column 24
    function visible_3 () : java.lang.Boolean {
      return installmentPlans.Count > 1
    }
    
    // 'visible' attribute on LinkCell (id=GoToSchedule) at BillingAdjustmentsInstallmentsLV.pcf: line 59, column 73
    function visible_5 () : java.lang.Boolean {
      return policyPeriod.Status == PolicyPeriodStatus.TC_QUOTED
    }
    
    property get defaultPaymentPlanChoiceID () : java.lang.String {
      return getVariableValue("defaultPaymentPlanChoiceID", 0) as java.lang.String
    }
    
    property set defaultPaymentPlanChoiceID ($arg :  java.lang.String) {
      setVariableValue("defaultPaymentPlanChoiceID", 0, $arg)
    }
    
    property get installmentPlans () : gw.plugin.billing.InstallmentPlanData[] {
      return getRequireValue("installmentPlans", 0) as gw.plugin.billing.InstallmentPlanData[]
    }
    
    property set installmentPlans ($arg :  gw.plugin.billing.InstallmentPlanData[]) {
      setRequireValue("installmentPlans", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get policyPeriodBillingInstructionsManager () : gw.billing.PolicyPeriodBillingInstructionsManager {
      return getRequireValue("policyPeriodBillingInstructionsManager", 0) as gw.billing.PolicyPeriodBillingInstructionsManager
    }
    
    property set policyPeriodBillingInstructionsManager ($arg :  gw.billing.PolicyPeriodBillingInstructionsManager) {
      setRequireValue("policyPeriodBillingInstructionsManager", 0, $arg)
    }
    
    property get renderHelper () : gw.web.policy.InstallmentPlanDataRenderHelper {
      return getVariableValue("renderHelper", 0) as gw.web.policy.InstallmentPlanDataRenderHelper
    }
    
    property set renderHelper ($arg :  gw.web.policy.InstallmentPlanDataRenderHelper) {
      setVariableValue("renderHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/BillingAdjustmentsInstallmentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BillingAdjustmentsInstallmentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on RadioButtonCell (id=InstallmentPlan_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 42, column 24
    function action_11 () : void {
      policyPeriod.selectPaymentPlan(plan)
    }
    
    // 'action' attribute on Link (id=SchedulePreviewButton) at BillingAdjustmentsInstallmentsLV.pcf: line 66, column 85
    function action_19 () : void {
      policyPeriodBillingInstructionsManager.previewPayments(plan)
    }
    
    // 'available' attribute on Link (id=SchedulePreviewButton) at BillingAdjustmentsInstallmentsLV.pcf: line 66, column 85
    function available_18 () : java.lang.Boolean {
      return policyPeriodBillingInstructionsManager.PaymentPlanChoice.BillingId == plan.BillingId
    }
    
    // 'label' attribute on Link (id=DownPaymentPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 78, column 69
    function label_21 () : java.lang.Object {
      return renderHelper.renderDownPayment(plan)
    }
    
    // 'label' attribute on Link (id=InstallmentPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 97, column 69
    function label_25 () : java.lang.Object {
      return renderHelper.renderInstallment(plan)
    }
    
    // 'label' attribute on Link (id=TotalPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 116, column 63
    function label_29 () : java.lang.Object {
      return renderHelper.renderTotal(plan)
    }
    
    // 'onChange' attribute on PostOnChange at BillingAdjustmentsInstallmentsLV.pcf: line 44, column 133
    function onChange_10 () : void {
      policyPeriodBillingInstructionsManager.selectPaymentPlan(plan); defaultPaymentPlanChoiceID = plan.BillingId
    }
    
    // 'tooltip' attribute on Link (id=DownPaymentPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 78, column 69
    function tooltip_22 () : java.lang.String {
      return renderHelper.renderDownPaymentDetail(plan)
    }
    
    // 'tooltip' attribute on Link (id=InstallmentPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 97, column 69
    function tooltip_26 () : java.lang.String {
      return renderHelper.renderInstallmentDetail(plan)
    }
    
    // 'tooltip' attribute on Link (id=TotalPreview) at BillingAdjustmentsInstallmentsLV.pcf: line 116, column 63
    function tooltip_30 () : java.lang.String {
      return renderHelper.renderTotalDetail(plan)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 54, column 24
    function valueRoot_16 () : java.lang.Object {
      return plan
    }
    
    // 'value' attribute on RadioButtonCell (id=InstallmentPlan_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 42, column 24
    function value_12 () : java.lang.Boolean {
      return defaultPaymentPlanChoiceID == plan.BillingId//policyPeriodBillingInstructionsManager.PaymentPlanChoice.BillingId == plan.BillingId
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 54, column 24
    function value_15 () : java.lang.String {
      return plan.Name
    }
    
    // 'value' attribute on TextCell (id=Notes_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 130, column 31
    function value_32 () : java.lang.String {
      return plan.Notes
    }
    
    // 'visible' attribute on RadioButtonCell (id=InstallmentPlan_Cell) at BillingAdjustmentsInstallmentsLV.pcf: line 42, column 24
    function visible_13 () : java.lang.Boolean {
      return installmentPlans.Count > 1
    }
    
    // 'visible' attribute on LinkCell (id=GoToSchedule) at BillingAdjustmentsInstallmentsLV.pcf: line 59, column 73
    function visible_20 () : java.lang.Boolean {
      return policyPeriod.Status == PolicyPeriodStatus.TC_QUOTED
    }
    
    // 'visible' attribute on Link (id=DownPaymentPreviewIcon) at BillingAdjustmentsInstallmentsLV.pcf: line 84, column 105
    function visible_23 () : java.lang.Boolean {
      return plan.DownPayment != null and plan.DownPayment.IsNotZero and plan.Tax.IsNotZero
    }
    
    // 'visible' attribute on Link (id=InstallmentPreviewIcon) at BillingAdjustmentsInstallmentsLV.pcf: line 103, column 126
    function visible_27 () : java.lang.Boolean {
      return plan.Installment != null and plan.Installment.IsNotZero and plan.Fee != null and plan.Fee.IsNotZero
    }
    
    property get plan () : gw.plugin.billing.InstallmentPlanData {
      return getIteratedValue(1) as gw.plugin.billing.InstallmentPlanData
    }
    
    
  }
  
  
}