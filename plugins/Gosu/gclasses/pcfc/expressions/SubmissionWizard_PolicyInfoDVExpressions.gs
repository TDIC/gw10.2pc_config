package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubmissionWizard_PolicyInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/submission/SubmissionWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubmissionWizard_PolicyInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonInput (id=AutoSelectUWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 84, column 53
    function action_26 () : void {
      policyPeriod.autoSelectUWCompany()
    }
    
    // 'available' attribute on ButtonInput (id=AutoSelectUWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 84, column 53
    function available_24 () : java.lang.Boolean {
      return not ( policyPeriod.ValidQuote or policyPeriod.Locked )
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 61, column 27
    function def_onEnter_11 (def :  pcf.PolicyInfoProducerOfRecordInputSet) : void {
      def.onEnter(policyPeriod, ProducerStatusUse.TC_SUBMISSIONOKAY)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 101, column 56
    function def_onEnter_40 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 44, column 60
    function def_onEnter_5 (def :  pcf.AccountInfoInputSet) : void {
      def.onEnter(policyPeriod,quoteType)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 46, column 60
    function def_onEnter_7 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 57, column 38
    function def_onEnter_9 (def :  pcf.PolicyInfoInputSet) : void {
      def.onEnter(policyPeriod, true, false, true,quoteType)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 57, column 38
    function def_refreshVariables_10 (def :  pcf.PolicyInfoInputSet) : void {
      def.refreshVariables(policyPeriod, true, false, true,quoteType)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 61, column 27
    function def_refreshVariables_12 (def :  pcf.PolicyInfoProducerOfRecordInputSet) : void {
      def.refreshVariables(policyPeriod, ProducerStatusUse.TC_SUBMISSIONOKAY)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 101, column 56
    function def_refreshVariables_41 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 44, column 60
    function def_refreshVariables_6 (def :  pcf.AccountInfoInputSet) : void {
      def.refreshVariables(policyPeriod,quoteType)
    }
    
    // 'def' attribute on InputSetRef at SubmissionWizard_PolicyInfoDV.pcf: line 46, column 60
    function def_refreshVariables_8 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.UWCompany = (__VALUE_TO_SET as entity.UWCompany)
    }
    
    // 'value' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 30, column 45
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      submission.DateQuoteNeeded = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.MultiLineDiscount_TDIC = (__VALUE_TO_SET as typekey.MultiLineDiscount_TDIC)
    }
    
    // 'editable' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function editable_13 () : java.lang.Boolean {
      return perm.System.multicompquote
    }
    
    // 'initialValue' attribute on Variable at SubmissionWizard_PolicyInfoDV.pcf: line 22, column 43
    function initialValue_0 () : gw.job.AvailableUWCompanies {
      return new gw.job.AvailableUWCompanies(policyPeriod)
    }
    
    // 'optionLabel' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function optionLabel_17 (VALUE :  entity.UWCompany) : java.lang.String {
      return VALUE.DisplayName
    }
    
    // 'required' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function required_31 () : java.lang.Boolean {
      return tdic.web.pcf.helper.PolicyInfoHelper.isMultiLineDiscountRequired(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function valueRange_18 () : java.lang.Object {
      return availableUWCompanies.Value
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function valueRange_35 () : java.lang.Object {
      return tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function valueRoot_16 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 30, column 45
    function valueRoot_3 () : java.lang.Object {
      return submission
    }
    
    // 'value' attribute on DateInput (id=DateQuoteNeeded_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 30, column 45
    function value_1 () : java.util.Date {
      return submission.DateQuoteNeeded
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function value_14 () : entity.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function value_32 () : typekey.MultiLineDiscount_TDIC {
      return policyPeriod.MultiLineDiscount_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function verifyValueRangeIsAllowedType_19 ($$arg :  entity.UWCompany[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function verifyValueRangeIsAllowedType_19 ($$arg :  gw.api.database.IQueryBeanResult<entity.UWCompany>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.MultiLineDiscount_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 73, column 41
    function verifyValueRange_20 () : void {
      var __valueRangeArg = availableUWCompanies.Value
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 96, column 235
    function verifyValueRange_37 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    // 'visible' attribute on Label (id=InvalidUWCompanyLabel) at SubmissionWizard_PolicyInfoDV.pcf: line 77, column 147
    function visible_23 () : java.lang.Boolean {
      return availableUWCompanies.Value.Count > 0 and not availableUWCompanies.Value.hasMatch( \ u -> u == policyPeriod.UWCompany )
    }
    
    // 'visible' attribute on ButtonInput (id=AutoSelectUWCompany_Input) at SubmissionWizard_PolicyInfoDV.pcf: line 84, column 53
    function visible_25 () : java.lang.Boolean {
      return not perm.System.multicompquote
    }
    
    // 'visible' attribute on InputDivider at SubmissionWizard_PolicyInfoDV.pcf: line 87, column 236
    function visible_29 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists ? false : (tdic.web.pcf.helper.PolicyInfoHelper.isMultiLineDiscountVisible(policyPeriod) && tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)!=null)
    }
    
    property get availableUWCompanies () : gw.job.AvailableUWCompanies {
      return getVariableValue("availableUWCompanies", 0) as gw.job.AvailableUWCompanies
    }
    
    property set availableUWCompanies ($arg :  gw.job.AvailableUWCompanies) {
      setVariableValue("availableUWCompanies", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : typekey.QuoteType {
      return getRequireValue("quoteType", 0) as typekey.QuoteType
    }
    
    property set quoteType ($arg :  typekey.QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    property get submission () : Submission {
      return getRequireValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setRequireValue("submission", 0, $arg)
    }
    
    
  }
  
  
}