package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLNameOTDCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLNameOTDCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLNameOTDCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 33, column 99
    function available_19 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 33, column 99
    function label_20 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 33, column 99
    function setter_21 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 60, column 56
    function sortValue_2 (scheduledItem :  entity.GLNameODSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 66, column 57
    function sortValue_3 (scheduledItem :  entity.GLNameODSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 73, column 50
    function sortValue_4 (scheduledItem :  entity.GLNameODSched_TDIC) : java.lang.Object {
      return scheduledItem.DoctorName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 53, column 53
    function toCreateAndAdd_15 () : entity.GLNameODSched_TDIC {
      return glLine.createAndAddNameODSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 53, column 53
    function toRemove_16 (scheduledItem :  entity.GLNameODSched_TDIC) : void {
      glLine.removeFromGLNameODSched_TDIC(scheduledItem); 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 53, column 53
    function value_17 () : entity.GLNameODSched_TDIC[] {
      return glLine.GLNameODSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 33, column 99
    function visible_18 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 82, column 43
    function visible_24 () : java.lang.Boolean {
      return isNameOnTheDoorVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isNameOnTheDoorVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLNameOTDCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
            coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLNameOTDCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLNameOTDCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 73, column 50
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.DoctorName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 60, column 56
    function valueRoot_6 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 73, column 50
    function value_11 () : java.lang.String {
      return scheduledItem.DoctorName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 60, column 56
    function value_5 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLNameOTDCov_TDIC.pcf: line 66, column 57
    function value_8 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    property get scheduledItem () : entity.GLNameODSched_TDIC {
      return getIteratedValue(1) as entity.GLNameODSched_TDIC
    }
    
    
  }
  
  
}