package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingTxDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RatingTxDetailsPanelSet_WC7LineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingTxDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at RatingTxDetailsPanelSet.WC7Line.pcf: line 79, column 56
    function def_onEnter_10 (def :  pcf.WC7RateTxDetailLV) : void {
      def.onEnter(periodTxs)
    }
    
    // 'def' attribute on ListViewInput at RatingTxDetailsPanelSet.WC7Line.pcf: line 79, column 56
    function def_refreshVariables_11 (def :  pcf.WC7RateTxDetailLV) : void {
      def.refreshVariables(periodTxs)
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 70, column 66
    function initialValue_8 () : java.util.Set<entity.WC7Transaction> {
      return stateTxs.byRatingPeriod( ratingPeriod )
    }
    
    // PanelIterator at RatingTxDetailsPanelSet.WC7Line.pcf: line 65, column 61
    function initializeVariables_13 () : void {
        periodTxs = stateTxs.byRatingPeriod( ratingPeriod );

    }
    
    // 'label' attribute on Label at RatingTxDetailsPanelSet.WC7Line.pcf: line 77, column 90
    function label_9 () : java.lang.String {
      return standardPremLabel(ratingPeriods.Count > 1, ratingPeriod)
    }
    
    // 'visible' attribute on PanelRef at RatingTxDetailsPanelSet.WC7Line.pcf: line 72, column 45
    function visible_12 () : java.lang.Boolean {
      return not periodTxs.Empty
    }
    
    property get periodTxs () : java.util.Set<entity.WC7Transaction> {
      return getVariableValue("periodTxs", 2) as java.util.Set<entity.WC7Transaction>
    }
    
    property set periodTxs ($arg :  java.util.Set<entity.WC7Transaction>) {
      setVariableValue("periodTxs", 2, $arg)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingTxDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends RatingTxDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at RatingTxDetailsPanelSet.WC7Line.pcf: line 93, column 72
    function def_onEnter_15 (def :  pcf.WC7RateTxDetailStateLV) : void {
      def.onEnter(stateTxs, jurisdiction)
    }
    
    // 'def' attribute on ListViewInput at RatingTxDetailsPanelSet.WC7Line.pcf: line 93, column 72
    function def_refreshVariables_16 (def :  pcf.WC7RateTxDetailStateLV) : void {
      def.refreshVariables(stateTxs, jurisdiction)
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 49, column 60
    function initialValue_4 () : java.util.Set<entity.WC7Transaction> {
      return partitionedTxs.get( jurisdiction.Jurisdiction )
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 54, column 73
    function initialValue_5 () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return jurisdiction.RatingPeriods
    }
    
    // PanelIterator at RatingTxDetailsPanelSet.WC7Line.pcf: line 41, column 44
    function initializeVariables_18 () : void {
        stateTxs = partitionedTxs.get( jurisdiction.Jurisdiction );
  ratingPeriods = jurisdiction.RatingPeriods;

    }
    
    // 'title' attribute on TitleBar at RatingTxDetailsPanelSet.WC7Line.pcf: line 59, column 49
    function title_7 () : java.lang.String {
      return jurisdiction.DisplayName
    }
    
    // 'value' attribute on PanelIterator at RatingTxDetailsPanelSet.WC7Line.pcf: line 65, column 61
    function value_14 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods.toTypedArray()
    }
    
    // 'visible' attribute on PanelRef at RatingTxDetailsPanelSet.WC7Line.pcf: line 56, column 38
    function visible_17 () : java.lang.Boolean {
      return not stateTxs.Empty
    }
    
    // 'visible' attribute on TitleBar at RatingTxDetailsPanelSet.WC7Line.pcf: line 59, column 49
    function visible_6 () : java.lang.Boolean {
      return jurisdictions.Count > 1
    }
    
    property get jurisdiction () : entity.WC7Jurisdiction {
      return getIteratedValue(1) as entity.WC7Jurisdiction
    }
    
    property get ratingPeriods () : java.util.List<gw.lob.wc7.rating.WC7RatingPeriod> {
      return getVariableValue("ratingPeriods", 1) as java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>
    }
    
    property set ratingPeriods ($arg :  java.util.List<gw.lob.wc7.rating.WC7RatingPeriod>) {
      setVariableValue("ratingPeriods", 1, $arg)
    }
    
    property get stateTxs () : java.util.Set<entity.WC7Transaction> {
      return getVariableValue("stateTxs", 1) as java.util.Set<entity.WC7Transaction>
    }
    
    property set stateTxs ($arg :  java.util.Set<entity.WC7Transaction>) {
      setVariableValue("stateTxs", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/RatingTxDetailsPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingTxDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 27, column 58
    function initialValue_0 () : java.util.Set<entity.WC7Transaction> {
      return thePeriod.WC7Line.WC7Transactions.toSet()
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 32, column 100
    function initialValue_1 () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Transaction>> {
      return lineTxs.partition( \ t -> t.WC7Cost.JurisdictionState ).toAutoMap( \ st -> java.util.Collections.emptySet<WC7Transaction>() )
    }
    
    // 'initialValue' attribute on Variable at RatingTxDetailsPanelSet.WC7Line.pcf: line 37, column 40
    function initialValue_2 () : entity.WC7Jurisdiction[] {
      return getJurisdictions()
    }
    
    // 'sortBy' attribute on IteratorSort at RatingTxDetailsPanelSet.WC7Line.pcf: line 44, column 24
    function sortBy_3 (jurisdiction :  entity.WC7Jurisdiction) : java.lang.Object {
      return jurisdiction
    }
    
    // 'title' attribute on TitleBar (id=grandTotalTitle) at RatingTxDetailsPanelSet.WC7Line.pcf: line 106, column 215
    function title_20 () : java.lang.String {
      return DisplayKey.get("Web.Quote.TotalCostLabel.Total2", gw.api.util.StringUtil.formatNumber(lineTxs.AmountSum(thePeriod.PreferredSettlementCurrency) as java.lang.Double, "currency"))
    }
    
    // 'value' attribute on PanelIterator at RatingTxDetailsPanelSet.WC7Line.pcf: line 41, column 44
    function value_19 () : entity.WC7Jurisdiction[] {
      return jurisdictions
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get jurisdictions () : entity.WC7Jurisdiction[] {
      return getVariableValue("jurisdictions", 0) as entity.WC7Jurisdiction[]
    }
    
    property set jurisdictions ($arg :  entity.WC7Jurisdiction[]) {
      setVariableValue("jurisdictions", 0, $arg)
    }
    
    property get lineTxs () : java.util.Set<entity.WC7Transaction> {
      return getVariableValue("lineTxs", 0) as java.util.Set<entity.WC7Transaction>
    }
    
    property set lineTxs ($arg :  java.util.Set<entity.WC7Transaction>) {
      setVariableValue("lineTxs", 0, $arg)
    }
    
    property get partitionedTxs () : java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Transaction>> {
      return getVariableValue("partitionedTxs", 0) as java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Transaction>>
    }
    
    property set partitionedTxs ($arg :  java.util.Map<typekey.Jurisdiction, java.util.Set<entity.WC7Transaction>>) {
      setVariableValue("partitionedTxs", 0, $arg)
    }
    
    property get revOpenForEdit () : boolean {
      return getRequireValue("revOpenForEdit", 0) as java.lang.Boolean
    }
    
    property set revOpenForEdit ($arg :  boolean) {
      setRequireValue("revOpenForEdit", 0, $arg)
    }
    
    property get thePeriod () : PolicyPeriod {
      return getRequireValue("thePeriod", 0) as PolicyPeriod
    }
    
    property set thePeriod ($arg :  PolicyPeriod) {
      setRequireValue("thePeriod", 0, $arg)
    }
    
    property get totalCostLabel () : String {
      return getRequireValue("totalCostLabel", 0) as String
    }
    
    property set totalCostLabel ($arg :  String) {
      setRequireValue("totalCostLabel", 0, $arg)
    }
    
    property get totalPremLabel () : String {
      return getRequireValue("totalPremLabel", 0) as String
    }
    
    property set totalPremLabel ($arg :  String) {
      setRequireValue("totalPremLabel", 0, $arg)
    }
    
    function standardPremLabel(splitPeriod : boolean, ratingPeriod : gw.lob.wc7.rating.WC7RatingPeriod ) : String {
      if (splitPeriod) {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.SplitPeriod", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"),
          gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
      } else {
        return DisplayKey.get("Web.Quote.WC.StandardPremium.OnePeriod")
      }
    }
    
    // Get any jurisdictions that exist on this branch, but additionally any that exist in the prior branch.
    // This is necessary in case a jurisdiction was removed as of the policy start on this branch.  In that case,
    // the jurisdiction does not exist, and should not have cost, it *will* have transactions that need to
    // be displayed (and they'll refer to a cost on the prior branch).
    function getJurisdictions() : WC7Jurisdiction[]
    {
      var jurisByIDs = thePeriod.WC7Line.RepresentativeJurisdictions.partitionUniquely( \ juris -> juris.FixedId )
      for ( juris in thePeriod.BasedOn.WC7Line.RepresentativeJurisdictions )
      {
        if ( not jurisByIDs.containsKey( juris.FixedId ) )  // in case we removed a jurisdiction as of the start of the period in this branch
        {
          jurisByIDs.put( juris.FixedId, juris )
        }
      }
      return jurisByIDs.Values.toTypedArray().sortBy( \ juris -> juris.Jurisdiction )
    }
    
    
  }
  
  
}