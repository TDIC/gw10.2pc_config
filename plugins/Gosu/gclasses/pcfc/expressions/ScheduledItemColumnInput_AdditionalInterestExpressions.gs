package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/ScheduledItemColumnInput.AdditionalInterest.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ScheduledItemColumnInput_AdditionalInterestExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/ScheduledItemColumnInput.AdditionalInterest.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ScheduledItemColumnInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AddlInterest_Cell) at ScheduledItemColumnInput.AdditionalInterest.pcf: line 24, column 46
    function action_1 () : void {
      EditPolicyContactRolePopup.push(scheduledItem.getAdditionalInterestColumn().PolicyAddlInterest, true)
    }
    
    // 'action' attribute on TextCell (id=AddlInterest_Cell) at ScheduledItemColumnInput.AdditionalInterest.pcf: line 24, column 46
    function action_dest_2 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(scheduledItem.getAdditionalInterestColumn().PolicyAddlInterest, true)
    }
    
    // 'initialValue' attribute on Variable at ScheduledItemColumnInput.AdditionalInterest.pcf: line 17, column 91
    function initialValue_0 () : gw.api.productmodel.SchedulePropertyValueProvider<AddlInterestDetail> {
      return new gw.api.productmodel.SchedulePropertyValueProvider<AddlInterestDetail>(scheduledItem, schedulePropertyInfo.PropertyInfo)
    }
    
    // 'required' attribute on TextCell (id=AddlInterest_Cell) at ScheduledItemColumnInput.AdditionalInterest.pcf: line 24, column 46
    function required_3 () : java.lang.Boolean {
      return schedulePropertyInfo.Required
    }
    
    // 'value' attribute on TextCell (id=AddlInterest_Cell) at ScheduledItemColumnInput.AdditionalInterest.pcf: line 24, column 46
    function valueRoot_5 () : java.lang.Object {
      return valueProvider
    }
    
    // 'value' attribute on TextCell (id=AddlInterest_Cell) at ScheduledItemColumnInput.AdditionalInterest.pcf: line 24, column 46
    function value_4 () : entity.AddlInterestDetail {
      return valueProvider.Value
    }
    
    property get schedulePropertyInfo () : gw.api.productmodel.SchedulePropertyInfo {
      return getRequireValue("schedulePropertyInfo", 0) as gw.api.productmodel.SchedulePropertyInfo
    }
    
    property set schedulePropertyInfo ($arg :  gw.api.productmodel.SchedulePropertyInfo) {
      setRequireValue("schedulePropertyInfo", 0, $arg)
    }
    
    property get scheduledItem () : ScheduledItem {
      return getRequireValue("scheduledItem", 0) as ScheduledItem
    }
    
    property set scheduledItem ($arg :  ScheduledItem) {
      setRequireValue("scheduledItem", 0, $arg)
    }
    
    property get valueProvider () : gw.api.productmodel.SchedulePropertyValueProvider<AddlInterestDetail> {
      return getVariableValue("valueProvider", 0) as gw.api.productmodel.SchedulePropertyValueProvider<AddlInterestDetail>
    }
    
    property set valueProvider ($arg :  gw.api.productmodel.SchedulePropertyValueProvider<AddlInterestDetail>) {
      setVariableValue("valueProvider", 0, $arg)
    }
    
    
  }
  
  
}