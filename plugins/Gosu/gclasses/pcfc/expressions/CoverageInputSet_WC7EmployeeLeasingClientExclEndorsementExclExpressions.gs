package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExclExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientExclPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 34, column 106
    function available_45 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 65, column 71
    function conversionExpression_10 (PickedValue :  Contact) : entity.WC7ExcludedLaborContactDetail {
      return addNewLaborClientDetailForContactOnExclusion(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 20, column 54
    function initialValue_0 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 24, column 36
    function initialValue_1 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 70, column 127
    function label_16 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.Existing", WC7PolicyLaborClient.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientExclPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 34, column 106
    function label_46 () : java.lang.Object {
      return exclusionPattern.DisplayName
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 65, column 71
    function pickLocation_11 () : void {
      ContactSearchPopup.push(TC_LABORCLIENT)
    }
    
    // 'onToggle' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientExclPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 34, column 106
    function setter_47 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(wc7Line, exclusionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 77, column 34
    function sortBy_12 (acctContact :  entity.AccountContact) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 52, column 32
    function sortBy_5 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 127, column 53
    function sortValue_21 (policyLaborClientDetailExcl :  entity.WC7ExcludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailExcl.WC7LaborContact
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function sortValue_22 (policyLaborClientDetailExcl :  entity.WC7ExcludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailExcl.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 147, column 45
    function sortValue_23 (policyLaborClientDetailExcl :  entity.WC7ExcludedLaborContactDetail) : java.lang.Object {
      return policyLaborClientDetailExcl.Address
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 119, column 64
    function toRemove_42 (policyLaborClientDetailExcl :  entity.WC7ExcludedLaborContactDetail) : void {
      policyLaborClientDetailExcl.WC7LaborContact.removeDetail(policyLaborClientDetailExcl)
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 28, column 37
    function valueRoot_3 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 74, column 53
    function value_15 () : entity.AccountContact[] {
      return wc7Line.WC7PolicyLaborClientDetailExistingCandidates
    }
    
    // 'value' attribute on HiddenInput (id=ExclPatternDisplayName_Input) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 28, column 37
    function value_2 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 93, column 53
    function value_20 () : entity.AccountContact[] {
      return wc7Line.WC7PolicyLaborClientDetailOtherCandidates
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 119, column 64
    function value_43 () : entity.WC7ExcludedLaborContactDetail[] {
      return wc7Line.getExcludedLaborClientDetails(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.ClausePattern>("WC7EmployeeLeasingClientExclEndorsementExcl"))
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 49, column 49
    function value_9 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLABORCLIENT)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=WC7EmployeeLeasingLaborClientExclPatternInputGroup) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 34, column 106
    function visible_44 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 154, column 101
    function visible_50 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get wc7Line () : productmodel.WC7Line {
      return getVariableValue("wc7Line", 0) as productmodel.WC7Line
    }
    
    property set wc7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("wc7Line", 0, $arg)
    }
    
    function addNewLaborClientDetailForContactOnExclusion(aContact : Contact) : WC7ExcludedLaborContactDetail {
      var newLaborClient = wc7Line.addExcludedLaborClientDetailForContact(aContact,
          wc7Line.WC7EmployeeLeasingClientExclEndorsementExcl)
      return newLaborClient
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 82, column 103
    function label_13 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 82, column 103
    function toCreateAndAdd_14 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborClientDetailForContactOnExclusion(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 101, column 103
    function label_18 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AccountContact) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 101, column 103
    function toCreateAndAdd_19 (CheckedValues :  Object[]) : java.lang.Object {
      return addNewLaborClientDetailForContactOnExclusion(acctContact.Contact)
    }
    
    property get acctContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 127, column 53
    function action_24 () : void {
      EditPolicyContactRolePopup.push(policyLaborClientDetailExcl.WC7LaborContact, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 127, column 53
    function action_dest_25 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyLaborClientDetailExcl.WC7LaborContact, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLaborClientDetailExcl.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function valueRange_35 () : java.lang.Object {
      return wc7Line.stateFilterFor(exclusionPattern).TypeKeys
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 127, column 53
    function valueRoot_27 () : java.lang.Object {
      return policyLaborClientDetailExcl
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 127, column 53
    function value_26 () : entity.WC7LaborContact {
      return policyLaborClientDetailExcl.WC7LaborContact
    }
    
    // 'value' attribute on TypeKeyCell (id=Inclusion_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 132, column 34
    function value_29 () : typekey.Inclusion {
      return policyLaborClientDetailExcl.Inclusion
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function value_32 () : typekey.Jurisdiction {
      return policyLaborClientDetailExcl.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 147, column 45
    function value_39 () : entity.Address {
      return policyLaborClientDetailExcl.Address
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 141, column 51
    function verifyValueRange_37 () : void {
      var __valueRangeArg = wc7Line.stateFilterFor(exclusionPattern).TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    property get policyLaborClientDetailExcl () : entity.WC7ExcludedLaborContactDetail {
      return getIteratedValue(1) as entity.WC7ExcludedLaborContactDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 58, column 204
    function conversionExpression_7 (PickedValue :  WC7LaborContactDetail) : entity.WC7ExcludedLaborContactDetail {
      return PickedValue as WC7ExcludedLaborContactDetail
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 58, column 204
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at CoverageInputSet.WC7EmployeeLeasingClientExclEndorsementExcl.pcf: line 58, column 204
    function pickLocation_8 () : void {
      WC7NewLaborClientForContactTypePopup.push(gw.lob.wc7.schedule.WC7ScheduleClientPresenter.forLaborClient(contactType, wc7Line.WC7EmployeeLeasingClientExclEndorsementExcl))
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  
}