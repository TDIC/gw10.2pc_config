package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/job/LineWizardStepSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LineWizardStepSet_WC7WorkersCompExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/job/LineWizardStepSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LineWizardStepSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function beforeSave_0 () : void {
      gw.policy.PolicyLocationValidation.validateLocationsStep(policyPeriod.PolicyLocations)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=WC7WorkersCompLineCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 43, column 89
    function beforeSave_12 () : void {
      gw.lob.wc7.WC7ValidationFactory.getScreenPolicyLineValidation(policyPeriod.WC7Line).validateWCLineCoveragesStep()
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=WorkersCompSupplemental) at LineWizardStepSet.WC7WorkersComp.pcf: line 53, column 129
    function beforeSave_19 () : void {
      gw.lob.wc7.WC7LineValidation.validateWCSupplementalStep( policyPeriod.WC7Line ); 
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=WorkersCompOptions) at LineWizardStepSet.WC7WorkersComp.pcf: line 61, column 54
    function beforeSave_25 () : void {
      gw.lob.wc7.WC7LineValidation.validateWorkersCompOptionsStep( policyPeriod.WC7Line )
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=WC7WorkersCompStateCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 35, column 90
    function beforeSave_6 () : void {
      gw.lob.wc7.WC7LineValidation.validateWCStateCoveragesStep( policyPeriod.WC7Line );policyPeriod.WC7Line.syncWC7SupplementaryDisease(jobWizardHelper)
    }
    
    // 'onChange' attribute on JobWizardStep (id=WC7WorkersCompStateCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 35, column 90
    function onChange_11 () : void {
      policyPeriod.WC7Line.recalculateGoverningClasscode(); policyPeriod.WC7Line.syncWC7CoveredEmployeeVersions(); policyPeriod.WC7Line.syncWC7SupplementaryDisease(jobWizardHelper) ;policyPeriod.WC7Line.syncWC7AtomicEnergyExposureVersions();
    }
    
    // 'onChange' attribute on JobWizardStep (id=WC7WorkersCompLineCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 43, column 89
    function onChange_17 () : void {
      policyPeriod.WC7Line.recalculateGoverningClasscode(); policyPeriod.WC7Line.syncWC7FedCoveredEmployeeVersions(); policyPeriod.WC7Line.syncWC7MaritimeCoveredEmployeeVersions(); policyPeriod.WC7Line.syncSpecificWaiversOfSubroVersions(); policyPeriod.WC7Line.clearIncludedStatesOrExcludedStatesForOtherStates();
    }
    
    // 'onChange' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function onChange_5 () : void {
      policyPeriod.WC7Line.recalculateGoverningClasscode();
    }
    
    // 'onEnter' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function onEnter_1 () : void {
      if (openForEdit) { gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions(policyPeriod.PolicyLocations, jobWizardHelper) }
    }
    
    // 'onEnter' attribute on JobWizardStep (id=WorkersCompSupplemental) at LineWizardStepSet.WC7WorkersComp.pcf: line 53, column 129
    function onEnter_20 () : void {
      if(openForEdit) { gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions( {policyPeriod.WC7Line}, jobWizardHelper )}
    }
    
    // 'onEnter' attribute on JobWizardStep (id=WC7WorkersCompStateCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 35, column 90
    function onEnter_7 () : void {
      if (openForEdit) { policyPeriod.syncComputedValues(); gw.web.productmodel.ProductModelSyncIssuesHandler.sync(policyPeriod.WC7Line.AllCoverables, policyPeriod.WC7Line.AllModifiables, null, null, jobWizardHelper) }
    }
    
    // 'save' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function save_2 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'screen' attribute on JobWizardStep (id=WC7WorkersCompLineCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 43, column 89
    function screen_onEnter_15 (def :  pcf.WC7LineCoverageScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WorkersCompSupplemental) at LineWizardStepSet.WC7WorkersComp.pcf: line 53, column 129
    function screen_onEnter_22 (def :  pcf.WC7SupplementalScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WorkersCompOptions) at LineWizardStepSet.WC7WorkersComp.pcf: line 61, column 54
    function screen_onEnter_27 (def :  pcf.WC7CoverageOptionsScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function screen_onEnter_3 (def :  pcf.LocationsScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper, policyPeriod.WC7Line.SupportsNonSpecificLocations)
    }
    
    // 'screen' attribute on JobWizardStep (id=WC7WorkersCompStateCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 35, column 90
    function screen_onEnter_9 (def :  pcf.WC7StateCoverageScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WC7WorkersCompStateCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 35, column 90
    function screen_refreshVariables_10 (def :  pcf.WC7StateCoverageScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WC7WorkersCompLineCoverages) at LineWizardStepSet.WC7WorkersComp.pcf: line 43, column 89
    function screen_refreshVariables_16 (def :  pcf.WC7LineCoverageScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WorkersCompSupplemental) at LineWizardStepSet.WC7WorkersComp.pcf: line 53, column 129
    function screen_refreshVariables_23 (def :  pcf.WC7SupplementalScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=WorkersCompOptions) at LineWizardStepSet.WC7WorkersComp.pcf: line 61, column 54
    function screen_refreshVariables_28 (def :  pcf.WC7CoverageOptionsScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.WC7WorkersComp.pcf: line 27, column 73
    function screen_refreshVariables_4 (def :  pcf.LocationsScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper, policyPeriod.WC7Line.SupportsNonSpecificLocations)
    }
    
    // 'visible' attribute on JobWizardStep (id=WorkersCompSupplemental) at LineWizardStepSet.WC7WorkersComp.pcf: line 53, column 129
    function visible_18 () : java.lang.Boolean {
      return policyPeriod.WC7Line.hasSupplementalQuestions() and (!(job typeis Submission) or (job as Submission).FullMode)
    }
    
    // 'visible' attribute on JobWizardStep (id=WorkersCompOptions) at LineWizardStepSet.WC7WorkersComp.pcf: line 61, column 54
    function visible_24 () : java.lang.Boolean {
      return perm.System.policyInfoWCOption_TDIC
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}