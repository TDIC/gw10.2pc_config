package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationTaxesAndSurchargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPLocationTaxesAndSurchargesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationTaxesAndSurchargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BOPLocationTaxesAndSurchargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 29, column 37
    function valueRoot_3 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 29, column 37
    function value_2 () : java.lang.String {
      return cost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 50, column 25
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    property get cost () : entity.BOPCost {
      return getIteratedValue(1) as entity.BOPCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationTaxesAndSurchargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPLocationTaxesAndSurchargesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 50, column 25
    function sumValueRoot_1 (cost :  entity.BOPCost) : java.lang.Object {
      return cost
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 50, column 25
    function sumValue_0 (cost :  entity.BOPCost) : java.lang.Object {
      return cost.ActualAmountBilling
    }
    
    // 'value' attribute on RowIterator at TDIC_BOPLocationTaxesAndSurchargesLV.pcf: line 19, column 36
    function value_8 () : entity.BOPCost[] {
      return taxesAndSurcharges.toTypedArray()
    }
    
    property get taxesAndSurcharges () : java.util.Set<BOPCost> {
      return getRequireValue("taxesAndSurcharges", 0) as java.util.Set<BOPCost>
    }
    
    property set taxesAndSurcharges ($arg :  java.util.Set<BOPCost>) {
      setRequireValue("taxesAndSurcharges", 0, $arg)
    }
    
    
  }
  
  
}