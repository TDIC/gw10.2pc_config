package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/Policy_Summary_ActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Policy_Summary_ActivitiesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/Policy_Summary_ActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Policy_Summary_ActivitiesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at Policy_Summary_ActivitiesLV.pcf: line 30, column 37
    function action_13 () : void {
      ActivityForward.go(activity, policyPeriod)
    }
    
    // 'action' attribute on Link (id=LinkedDocument) at Policy_Summary_ActivitiesLV.pcf: line 41, column 156
    function action_22 () : void {
      OnBaseDocumentListPopup.push(activity, acc.onbase.configuration.DocumentLinkType.activityid, activity.toString(), new KeyableBean[]{activity.Policy})  
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at Policy_Summary_ActivitiesLV.pcf: line 30, column 37
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ActivityForward.createDestination(activity, policyPeriod)
    }
    
    // 'available' attribute on TextCell (id=Subject_Cell) at Policy_Summary_ActivitiesLV.pcf: line 30, column 37
    function available_12 () : java.lang.Boolean {
      return perm.Activity.view(activity)
    }
    
    // 'label' attribute on Link (id=LinkedDocument) at Policy_Summary_ActivitiesLV.pcf: line 41, column 156
    function label_23 () : java.lang.Object {
      return acc.onbase.api.application.DocumentLinking.getLinkingDocumentUILabel(activity, acc.onbase.configuration.DocumentLinkType.activityid) 
    }
    
    // 'label' attribute on LinkCell at Policy_Summary_ActivitiesLV.pcf: line 37, column 102
    function label_24 () : java.lang.Object {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Documents") 
    }
    
    // 'value' attribute on DateCell (id=Due_Cell) at Policy_Summary_ActivitiesLV.pcf: line 19, column 40
    function valueRoot_6 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at Policy_Summary_ActivitiesLV.pcf: line 30, column 37
    function value_15 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at Policy_Summary_ActivitiesLV.pcf: line 35, column 50
    function value_19 () : java.lang.String {
      return activity.AssignedTo as String
    }
    
    // 'value' attribute on DateCell (id=Due_Cell) at Policy_Summary_ActivitiesLV.pcf: line 19, column 40
    function value_5 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at Policy_Summary_ActivitiesLV.pcf: line 24, column 33
    function value_8 () : Priority {
      return activity.Priority
    }
    
    // 'valueType' attribute on TypeKeyCell (id=Priority_Cell) at Policy_Summary_ActivitiesLV.pcf: line 24, column 33
    function verifyValueType_11 () : void {
      var __valueTypeArg : Priority
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get activity () : entity.Activity {
      return getIteratedValue(1) as entity.Activity
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/Policy_Summary_ActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy_Summary_ActivitiesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on LinkCell at Policy_Summary_ActivitiesLV.pcf: line 37, column 102
    function label_4 () : java.lang.Object {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Documents") 
    }
    
    // 'value' attribute on DateCell (id=Due_Cell) at Policy_Summary_ActivitiesLV.pcf: line 19, column 40
    function sortValue_0 (activity :  entity.Activity) : java.lang.Object {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at Policy_Summary_ActivitiesLV.pcf: line 24, column 33
    function sortValue_1 (activity :  entity.Activity) : java.lang.Object {
      return activity.Priority
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at Policy_Summary_ActivitiesLV.pcf: line 30, column 37
    function sortValue_2 (activity :  entity.Activity) : java.lang.Object {
      return activity.Subject
    }
    
    // 'sortBy' attribute on TextCell (id=AssignedTo_Cell) at Policy_Summary_ActivitiesLV.pcf: line 35, column 50
    function sortValue_3 (activity :  entity.Activity) : java.lang.Object {
      return activity.AssignedUser
    }
    
    // 'value' attribute on RowIterator at Policy_Summary_ActivitiesLV.pcf: line 14, column 75
    function value_25 () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return policyPeriod.Policy.AllOpenActivities
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}