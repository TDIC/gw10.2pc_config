package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostRowSet.total.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateCostRowSet_totalExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostRowSet.total.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateCostRowSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at WC7StateCostRowSet.total.pcf: line 23, column 39
    function valueRoot_1 () : java.lang.Object {
      return costWrapper
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at WC7StateCostRowSet.total.pcf: line 23, column 39
    function value_0 () : java.lang.String {
      return costWrapper.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=total_Cell) at WC7StateCostRowSet.total.pcf: line 35, column 36
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return costWrapper.Total
    }
    
    // 'value' attribute on TextCell (id=Empty6_Cell) at WC7StateCostRowSet.total.pcf: line 51, column 39
    function value_7 () : java.lang.String {
      return costWrapper.ClassCode_TDIC
    }
    
    // 'visible' attribute on Row at WC7StateCostRowSet.total.pcf: line 44, column 38
    function visible_16 () : java.lang.Boolean {
      return not costWrapper.Bold
    }
    
    // 'visible' attribute on Row at WC7StateCostRowSet.total.pcf: line 12, column 34
    function visible_6 () : java.lang.Boolean {
      return costWrapper.Bold
    }
    
    property get costWrapper () : gw.api.ui.WC7CostWrapper {
      return getRequireValue("costWrapper", 0) as gw.api.ui.WC7CostWrapper
    }
    
    property set costWrapper ($arg :  gw.api.ui.WC7CostWrapper) {
      setRequireValue("costWrapper", 0, $arg)
    }
    
    
  }
  
  
}