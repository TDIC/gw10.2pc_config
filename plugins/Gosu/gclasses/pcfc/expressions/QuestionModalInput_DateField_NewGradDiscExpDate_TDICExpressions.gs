package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.DateField_NewGradDiscExpDate_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_DateField_NewGradDiscExpDate_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.DateField_NewGradDiscExpDate_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'required' attribute on DateCell (id=DateFieldInput_NoPost_Cell) at QuestionModalInput.DateField_NewGradDiscExpDate_TDIC.pcf: line 21, column 63
    function required_0 () : java.lang.Boolean {
      return answerContainer.isNewGradEffectiveDateMandatory_TDIC()  or question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on DateCell (id=DateFieldInput_NoPost_Cell) at QuestionModalInput.DateField_NewGradDiscExpDate_TDIC.pcf: line 21, column 63
    function valueRoot_2 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on DateCell (id=DateFieldInput_NoPost_Cell) at QuestionModalInput.DateField_NewGradDiscExpDate_TDIC.pcf: line 21, column 63
    function value_1 () : java.util.Date {
      return answerContainer.getAnswer(question).DateAnswer
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}