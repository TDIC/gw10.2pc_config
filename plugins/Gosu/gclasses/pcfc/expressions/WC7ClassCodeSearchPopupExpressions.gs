package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.lob.wc7.WC7ClassCodeSearchCriteria
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ClassCodeSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickValue' attribute on RowIterator at WC7ClassCodeSearchPopup.pcf: line 102, column 87
    function pickValue_48 () : WC7ClassCode {
      return wcClassCode
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7ClassCodeSearchPopup.pcf: line 108, column 49
    function valueRoot_28 () : java.lang.Object {
      return wcClassCode
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7ClassCodeSearchPopup.pcf: line 108, column 49
    function value_27 () : java.lang.String {
      return wcClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7ClassCodeSearchPopup.pcf: line 114, column 49
    function value_30 () : java.lang.String {
      return wcClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=WC7ClassCodeType_Cell) at WC7ClassCodeSearchPopup.pcf: line 119, column 57
    function value_33 () : typekey.WC7ClassCodeType {
      return wcClassCode.ClassCodeType
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 124, column 55
    function value_36 () : java.util.Date {
      return wcClassCode.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 129, column 55
    function value_40 () : java.util.Date {
      return wcClassCode.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=RegulatoryRate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 135, column 55
    function value_44 () : java.math.BigDecimal {
      return wcClassCode.RegulatoryRate_TDIC
    }
    
    // 'visible' attribute on DateCell (id=EffectiveDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 124, column 55
    function visible_38 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    property get wcClassCode () : entity.WC7ClassCode {
      return getIteratedValue(2) as entity.WC7ClassCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends WC7ClassCodeSearchPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at WC7ClassCodeSearchPopup.pcf: line 87, column 49
    function def_onEnter_16 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at WC7ClassCodeSearchPopup.pcf: line 87, column 49
    function def_refreshVariables_17 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on BooleanRadioInput (id=ContractingClassCodes_Input) at WC7ClassCodeSearchPopup.pcf: line 83, column 140
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ConstructionType = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7ClassCodeSearchPopup.pcf: line 69, column 47
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Classification_Input) at WC7ClassCodeSearchPopup.pcf: line 76, column 47
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Classification = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel at WC7ClassCodeSearchPopup.pcf: line 58, column 82
    function searchCriteria_51 () : gw.lob.wc7.WC7ClassCodeSearchCriteria {
      return createCriteria()
    }
    
    // 'search' attribute on SearchPanel at WC7ClassCodeSearchPopup.pcf: line 58, column 82
    function search_50 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7ClassCodeSearchPopup.pcf: line 108, column 49
    function sortValue_18 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7ClassCodeSearchPopup.pcf: line 114, column 49
    function sortValue_19 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=WC7ClassCodeType_Cell) at WC7ClassCodeSearchPopup.pcf: line 119, column 57
    function sortValue_20 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.ClassCodeType
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 124, column 55
    function sortValue_21 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 129, column 55
    function sortValue_23 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=RegulatoryRate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 135, column 55
    function sortValue_25 (wcClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wcClassCode.RegulatoryRate_TDIC
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7ClassCodeSearchPopup.pcf: line 69, column 47
    function valueRoot_4 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on BooleanRadioInput (id=ContractingClassCodes_Input) at WC7ClassCodeSearchPopup.pcf: line 83, column 140
    function value_11 () : java.lang.Boolean {
      return searchCriteria.ConstructionType
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7ClassCodeSearchPopup.pcf: line 69, column 47
    function value_2 () : java.lang.String {
      return searchCriteria.Code
    }
    
    // 'value' attribute on RowIterator at WC7ClassCodeSearchPopup.pcf: line 102, column 87
    function value_49 () : gw.api.database.IQueryBeanResult<entity.WC7ClassCode> {
      return wcClassCodes
    }
    
    // 'value' attribute on TextInput (id=Classification_Input) at WC7ClassCodeSearchPopup.pcf: line 76, column 47
    function value_6 () : java.lang.String {
      return searchCriteria.Classification
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ContractingClassCodes_Input) at WC7ClassCodeSearchPopup.pcf: line 83, column 140
    function visible_10 () : java.lang.Boolean {
      var criteria = createCriteria(); criteria.ConstructionType = true; return criteria.performSearch().Count > 0
    }
    
    // 'visible' attribute on DateCell (id=EffectiveDate_TDIC_Cell) at WC7ClassCodeSearchPopup.pcf: line 124, column 55
    function visible_22 () : java.lang.Boolean {
      return perm.System.ratebookview
    }
    
    property get searchCriteria () : gw.lob.wc7.WC7ClassCodeSearchCriteria {
      return getCriteriaValue(1) as gw.lob.wc7.WC7ClassCodeSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.lob.wc7.WC7ClassCodeSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get wcClassCodes () : gw.api.database.IQueryBeanResult<WC7ClassCode> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<WC7ClassCode>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ClassCodeSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ClassCodeSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (polJurisdiction :  Jurisdiction, wcLine :  WC7WorkersCompLine) : int {
      return 3
    }
    
    static function __constructorIndex (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, classCodeType :  WC7ClassCodeType, programType :  WC7ClassCodeProgramType) : int {
      return 1
    }
    
    static function __constructorIndex (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, previousWC7ClassCode :  WC7ClassCode, excludedClassCodeTypes :  java.util.List<WC7ClassCodeType>) : int {
      return 0
    }
    
    static function __constructorIndex (locationWM :  PolicyLocation, wcLine :  WC7WorkersCompLine, classCodeType :  WC7ClassCodeType) : int {
      return 2
    }
    
    // 'value' attribute on TextInput (id=LocationLabel_Input) at WC7ClassCodeSearchPopup.pcf: line 47, column 43
    function value_0 () : java.lang.String {
      return locationWM != null ? DisplayKey.get("Web.Policy.WC.ClassCode.Location", locationWM.DisplayName ) : ""
    }
    
    override property get CurrentLocation () : pcf.WC7ClassCodeSearchPopup {
      return super.CurrentLocation as pcf.WC7ClassCodeSearchPopup
    }
    
    property get classCodeType () : WC7ClassCodeType {
      return getVariableValue("classCodeType", 0) as WC7ClassCodeType
    }
    
    property set classCodeType ($arg :  WC7ClassCodeType) {
      setVariableValue("classCodeType", 0, $arg)
    }
    
    property get excludedClassCodeTypes () : java.util.List<WC7ClassCodeType> {
      return getVariableValue("excludedClassCodeTypes", 0) as java.util.List<WC7ClassCodeType>
    }
    
    property set excludedClassCodeTypes ($arg :  java.util.List<WC7ClassCodeType>) {
      setVariableValue("excludedClassCodeTypes", 0, $arg)
    }
    
    property get locationWM () : PolicyLocation {
      return getVariableValue("locationWM", 0) as PolicyLocation
    }
    
    property set locationWM ($arg :  PolicyLocation) {
      setVariableValue("locationWM", 0, $arg)
    }
    
    property get polJurisdiction () : Jurisdiction {
      return getVariableValue("polJurisdiction", 0) as Jurisdiction
    }
    
    property set polJurisdiction ($arg :  Jurisdiction) {
      setVariableValue("polJurisdiction", 0, $arg)
    }
    
    property get previousWC7ClassCode () : WC7ClassCode {
      return getVariableValue("previousWC7ClassCode", 0) as WC7ClassCode
    }
    
    property set previousWC7ClassCode ($arg :  WC7ClassCode) {
      setVariableValue("previousWC7ClassCode", 0, $arg)
    }
    
    property get programType () : WC7ClassCodeProgramType {
      return getVariableValue("programType", 0) as WC7ClassCodeProgramType
    }
    
    property set programType ($arg :  WC7ClassCodeProgramType) {
      setVariableValue("programType", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getVariableValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setVariableValue("wcLine", 0, $arg)
    }
    
    
    function createCriteria() : WC7ClassCodeSearchCriteria {
      var criteria = new WC7ClassCodeSearchCriteria()
      criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(locationWM))
      if (previousWC7ClassCode != null) {
        criteria.PreviousSelectedClassCode = previousWC7ClassCode
      }
      if (classCodeType != null) {
        criteria.ClassCodeType = classCodeType
      } 
      if (programType != null) {
        criteria.ProgramType = programType
      }
      if(locationWM != null){
        criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(locationWM))
        criteria.Jurisdiction = typekey.Jurisdiction.get(JurisdictionMappingUtil.getJurisdiction(locationWM) as String)
      }else{
        criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(polJurisdiction)
        criteria.Jurisdiction = polJurisdiction
      } 
      criteria.ExcludedClassCodeTypes = excludedClassCodeTypes
    
      return criteria
    }
    
    
  }
  
  
}