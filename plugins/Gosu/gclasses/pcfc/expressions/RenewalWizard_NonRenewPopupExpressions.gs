package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/renewal/RenewalWizard_NonRenewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RenewalWizard_NonRenewPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/job/renewal/RenewalWizard_NonRenewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends RenewalWizard_NonRenewPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 122, column 57
    function valueRoot_14 () : java.lang.Object {
      return policyRenewalReason
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 122, column 57
    function value_13 () : typekey.NonRenewalCode {
      return policyRenewalReason.NonRenewalCode
    }
    
    property get policyRenewalReason () : entity.RenewalReason_TDIC {
      return getIteratedValue(1) as entity.RenewalReason_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/renewal/RenewalWizard_NonRenewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends RenewalWizard_NonRenewPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyRenewalReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 88, column 134
    function value_7 () : typekey.NonRenewalCode {
      return policyNonRenewalReason
    }
    
    // 'valueVisible' attribute on TypeKeyCell (id=PolicyRenewalReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 88, column 134
    function visible_6 () : java.lang.Boolean {
      return (!policyPeriod.Job.RenewalReasons*.NonRenewalCode.contains(policyNonRenewalReason))? true : false
    }
    
    property get policyNonRenewalReason () : typekey.NonRenewalCode {
      return getIteratedValue(1) as typekey.NonRenewalCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/renewal/RenewalWizard_NonRenewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RenewalWizard_NonRenewPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (renewal :  Renewal, policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=RenewalWizard_NonRenewPopup) at RenewalWizard_NonRenewPopup.pcf: line 12, column 74
    function afterCommit_24 (TopLocation :  pcf.api.Location) : void {
       policyPeriod.RenewalProcess.pendingNonRenew(); TopLocation.commit(); JobComplete.go(renewal, policyPeriod)
    }
    
    // 'beforeCommit' attribute on Popup (id=RenewalWizard_NonRenewPopup) at RenewalWizard_NonRenewPopup.pcf: line 12, column 74
    function beforeCommit_25 (pickedValue :  java.lang.Object) : void {
      policyPeriod.Job.createCustomHistoryEvent(CustomHistoryType.TC_RENEWAL, \ -> DisplayKey.get("Job.Renewal.History.NonRenewPopup"));policyPeriod.Renewal.validateNonRenewalReason_TDIC()
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=RemoveChangeReason) at RenewalWizard_NonRenewPopup.pcf: line 104, column 112
    function checkedRowAction_11 (policyRenewalReason :  entity.RenewalReason_TDIC, CheckedValue :  entity.RenewalReason_TDIC) : void {
      policyPeriod.Job.removeFromRenewalReasons(CheckedValue); //return CheckedValue;  
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AddNonRenewalReason) at RenewalWizard_NonRenewPopup.pcf: line 66, column 109
    function checkedRowAction_4 (policyNonRenewalReason :  typekey.NonRenewalCode, CheckedValue :  typekey.NonRenewalCode) : void {
      var nonRenewalReason = new RenewalReason_TDIC(); nonRenewalReason.NonRenewalCode = CheckedValue; policyPeriod.Job.addToRenewalReasons(nonRenewalReason); //return CheckedValue;  
    }
    
    // 'value' attribute on TextAreaInput (id=CommentBoxN_Input) at RenewalWizard_NonRenewPopup.pcf: line 149, column 53
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.Job.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at RenewalWizard_NonRenewPopup.pcf: line 24, column 22
    function initialValue_0 () : Policy {
      return policyPeriod.Policy
    }
    
    // 'initialValue' attribute on Variable at RenewalWizard_NonRenewPopup.pcf: line 28, column 26
    function initialValue_1 () : PolicyTerm {
      return policyPeriod.PolicyTerm
    }
    
    // 'initialValue' attribute on Variable at RenewalWizard_NonRenewPopup.pcf: line 32, column 24
    function initialValue_2 () : String[] {
      return new String[]{""}
    }
    
    // EditButtons at RenewalWizard_NonRenewPopup.pcf: line 37, column 102
    function label_3 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'required' attribute on TextAreaInput (id=CommentBoxN_Input) at RenewalWizard_NonRenewPopup.pcf: line 149, column 53
    function required_18 () : java.lang.Boolean {
      return policyPeriod.Job.RenewalReasons.countWhere(\elt -> elt.NonRenewalCode==NonRenewalCode.TC_MISCUNDERWRITING_TDIC)==1
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyChangeReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 122, column 57
    function sortValue_12 (policyRenewalReason :  entity.RenewalReason_TDIC) : java.lang.Object {
      return policyRenewalReason.NonRenewalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyRenewalReasonTL_Cell) at RenewalWizard_NonRenewPopup.pcf: line 88, column 134
    function sortValue_5 (policyNonRenewalReason :  typekey.NonRenewalCode) : java.lang.Object {
      return policyNonRenewalReason
    }
    
    // 'validationExpression' attribute on TextAreaInput (id=CommentBoxN_Input) at RenewalWizard_NonRenewPopup.pcf: line 149, column 53
    function validationExpression_17 () : java.lang.Object {
      return policyPeriod.Job.hasDescriptionContainsSpecialChars()
    }
    
    // 'value' attribute on TextAreaInput (id=CommentBoxN_Input) at RenewalWizard_NonRenewPopup.pcf: line 149, column 53
    function valueRoot_21 () : java.lang.Object {
      return policyPeriod.Job
    }
    
    // 'value' attribute on RowIterator (id=policyNonRenewalReasonIterator) at RenewalWizard_NonRenewPopup.pcf: line 78, column 54
    function value_10 () : typekey.NonRenewalCode[] {
      return tdic.web.pcf.helper.PolicyRenewalScreenHelper.getFilteredNonRenewReasonCodes(policyPeriod)
    }
    
    // 'value' attribute on RowIterator (id=policyRenewalReasonIterator) at RenewalWizard_NonRenewPopup.pcf: line 115, column 57
    function value_16 () : entity.RenewalReason_TDIC[] {
      return policyPeriod.Job.RenewalReasons
    }
    
    // 'value' attribute on TextAreaInput (id=CommentBoxN_Input) at RenewalWizard_NonRenewPopup.pcf: line 149, column 53
    function value_19 () : java.lang.String {
      return policyPeriod.Job.Description
    }
    
    override property get CurrentLocation () : pcf.RenewalWizard_NonRenewPopup {
      return super.CurrentLocation as pcf.RenewalWizard_NonRenewPopup
    }
    
    property get descriptions () : String[] {
      return getVariableValue("descriptions", 0) as String[]
    }
    
    property set descriptions ($arg :  String[]) {
      setVariableValue("descriptions", 0, $arg)
    }
    
    property get policy () : Policy {
      return getVariableValue("policy", 0) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setVariableValue("policy", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyTerm () : PolicyTerm {
      return getVariableValue("policyTerm", 0) as PolicyTerm
    }
    
    property set policyTerm ($arg :  PolicyTerm) {
      setVariableValue("policyTerm", 0, $arg)
    }
    
    property get renewal () : Renewal {
      return getVariableValue("renewal", 0) as Renewal
    }
    
    property set renewal ($arg :  Renewal) {
      setVariableValue("renewal", 0, $arg)
    }
    
    
  }
  
  
}