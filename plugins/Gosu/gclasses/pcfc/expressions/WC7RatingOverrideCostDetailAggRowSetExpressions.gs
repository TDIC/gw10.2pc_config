package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RatingOverrideCostDetailAggRowSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideCostDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RatingOverrideCostDetailAggRowSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 61, column 48
    function currency_33 () : typekey.Currency {
      return (aggCost.BranchUntyped as PolicyPeriod).PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 50, column 43
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggCost.OverrideBaseRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=OverrideAdjustedRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 56, column 43
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggCost.OverrideAdjRate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 61, column 48
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggCost.OverrideAmountBilling = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 67, column 39
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggCost.OverrideReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 50, column 43
    function editable_17 () : java.lang.Boolean {
      return aggCost.Overridable
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 21, column 39
    function valueRoot_1 () : java.lang.Object {
      return aggCost
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 21, column 39
    function value_0 () : java.lang.String {
      return aggCost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 40, column 39
    function value_12 () : java.lang.String {
      return aggCost.Basis.asString()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ActualAmount_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 44, column 52
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return aggCost.ActualAmountBilling
    }
    
    // 'value' attribute on TextCell (id=OverrideBaseRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 50, column 43
    function value_18 () : java.math.BigDecimal {
      return aggCost.OverrideBaseRate
    }
    
    // 'value' attribute on TextCell (id=OverrideAdjustedRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 56, column 43
    function value_24 () : java.math.BigDecimal {
      return aggCost.OverrideAdjRate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 25, column 39
    function value_3 () : java.lang.String {
      return aggCost.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 61, column 48
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return aggCost.OverrideAmountBilling
    }
    
    // 'value' attribute on TextCell (id=OverrideReason_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 67, column 39
    function value_37 () : java.lang.String {
      return aggCost.OverrideReason
    }
    
    // 'value' attribute on TextCell (id=StandardBaseRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 72, column 43
    function value_42 () : java.math.BigDecimal {
      return aggCost.StandardBaseRate
    }
    
    // 'value' attribute on TextCell (id=StandardAdjustedRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 77, column 43
    function value_45 () : java.math.BigDecimal {
      return aggCost.StandardAdjRate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StandardAmount_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 81, column 52
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return aggCost.StandardAmountBilling
    }
    
    // 'value' attribute on TextCell (id=ActualBaseRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 30, column 43
    function value_6 () : java.math.BigDecimal {
      return aggCost.ActualBaseRate
    }
    
    // 'value' attribute on TextCell (id=ActualAdjustedRate_Cell) at WC7RatingOverrideCostDetailAggRowSet.pcf: line 35, column 43
    function value_9 () : java.math.BigDecimal {
      return aggCost.ActualAdjRate
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getRequireValue("aggCost", 0) as WC7JurisdictionCost
    }
    
    property set aggCost ($arg :  WC7JurisdictionCost) {
      setRequireValue("aggCost", 0, $arg)
    }
    
    property get basedOnAggCost () : WC7JurisdictionCost {
      return getRequireValue("basedOnAggCost", 0) as WC7JurisdictionCost
    }
    
    property set basedOnAggCost ($arg :  WC7JurisdictionCost) {
      setRequireValue("basedOnAggCost", 0, $arg)
    }
    
    
  }
  
  
}