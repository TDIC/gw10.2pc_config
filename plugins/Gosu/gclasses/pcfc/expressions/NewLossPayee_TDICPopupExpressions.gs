package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/NewLossPayee_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewLossPayee_TDICPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/NewLossPayee_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewLossPayee_TDICPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (bopBuilding :  entity.BOPBuilding, contactType :  ContactType) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at NewLossPayee_TDICPopup.pcf: line 58, column 62
    function action_11 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at NewLossPayee_TDICPopup.pcf: line 50, column 62
    function action_6 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(policyLossPayee))
    }
    
    // 'beforeCommit' attribute on Popup (id=NewLossPayee_TDICPopup) at NewLossPayee_TDICPopup.pcf: line 12, column 143
    function beforeCommit_18 (pickedValue :  PolicyLossPayee_TDIC) : void {
      policyLossPayee.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch); helper.validateAndUpdateStatusOfAddresses(contact)
    }
    
    // 'def' attribute on PanelRef at NewLossPayee_TDICPopup.pcf: line 71, column 70
    function def_onEnter_16 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(policyLossPayee, false)
    }
    
    // 'def' attribute on PanelRef at NewLossPayee_TDICPopup.pcf: line 71, column 70
    function def_refreshVariables_17 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(policyLossPayee, false)
    }
    
    // 'value' attribute on TextInput (id=LoanNumber_Input) at NewLossPayee_TDICPopup.pcf: line 67, column 55
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyLossPayee.LoanNumberString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at NewLossPayee_TDICPopup.pcf: line 25, column 43
    function initialValue_0 () : entity.PolicyLossPayee_TDIC {
      return bopBuilding.addNewPolicyLossOfPayeeOfContactType_TDIC(contactType)
    }
    
    // 'initialValue' attribute on Variable at NewLossPayee_TDICPopup.pcf: line 29, column 25
    function initialValue_1 () : Contact[] {
      return bopBuilding.BOPBldgLossPayees.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at NewLossPayee_TDICPopup.pcf: line 33, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(policyLossPayee.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'initialValue' attribute on Variable at NewLossPayee_TDICPopup.pcf: line 37, column 76
    function initialValue_3 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at NewLossPayee_TDICPopup.pcf: line 41, column 30
    function initialValue_4 () : entity.Contact {
      return policyLossPayee.AccountContactRole.AccountContact.Contact
    }
    
    // EditButtons at NewLossPayee_TDICPopup.pcf: line 53, column 72
    function label_9 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at NewLossPayee_TDICPopup.pcf: line 53, column 72
    function pickValue_7 () : PolicyLossPayee_TDIC {
      return policyLossPayee
    }
    
    // 'title' attribute on Popup (id=NewLossPayee_TDICPopup) at NewLossPayee_TDICPopup.pcf: line 12, column 143
    static function title_19 (bopBuilding :  entity.BOPBuilding, contactType :  ContactType) : java.lang.Object {
      return DisplayKey.get("Web.Contact.NewContact2", PolicyLossPayee_TDIC.Type.TypeInfo.DisplayName, bopBuilding.OwnerDisplayName)
    }
    
    // 'value' attribute on TextInput (id=LoanNumber_Input) at NewLossPayee_TDICPopup.pcf: line 67, column 55
    function valueRoot_14 () : java.lang.Object {
      return policyLossPayee
    }
    
    // 'value' attribute on TextInput (id=LoanNumber_Input) at NewLossPayee_TDICPopup.pcf: line 67, column 55
    function value_12 () : java.lang.String {
      return policyLossPayee.LoanNumberString
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at NewLossPayee_TDICPopup.pcf: line 50, column 62
    function visible_5 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at NewLossPayee_TDICPopup.pcf: line 53, column 72
    function visible_8 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.NewLossPayee_TDICPopup {
      return super.CurrentLocation as pcf.NewLossPayee_TDICPopup
    }
    
    property get bopBuilding () : entity.BOPBuilding {
      return getVariableValue("bopBuilding", 0) as entity.BOPBuilding
    }
    
    property set bopBuilding ($arg :  entity.BOPBuilding) {
      setVariableValue("bopBuilding", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactType () : ContactType {
      return getVariableValue("contactType", 0) as ContactType
    }
    
    property set contactType ($arg :  ContactType) {
      setVariableValue("contactType", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get policyLossPayee () : entity.PolicyLossPayee_TDIC {
      return getVariableValue("policyLossPayee", 0) as entity.PolicyLossPayee_TDIC
    }
    
    property set policyLossPayee ($arg :  entity.PolicyLossPayee_TDIC) {
      setVariableValue("policyLossPayee", 0, $arg)
    }
    
    
  }
  
  
}