package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_WC7PolicyOwnerOfficerDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 61, column 103
    function conversionExpression_9 (PickedValue :  PolicyEntityOwner_TDIC) : entity.WC7PolicyOwnerOfficer {
      return PickedValue as PolicyEntityOwner_TDIC
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 61, column 103
    function label_8 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewEntityOwnerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 61, column 103
    function pickLocation_10 () : void {
      TDIC_NewEntityOwnerPopup.push(theWC7Line.Branch.WC7Line, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 92, column 95
    function label_17 () : java.lang.Object {
      return ownerOfficer
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 92, column 95
    function toCreateAndAdd_18 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addWC7PolicyOwnerOfficer(ownerOfficer.Contact)
    }
    
    property get ownerOfficer () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 118, column 95
    function label_24 () : java.lang.Object {
      return entityOwner
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 118, column 95
    function toCreateAndAdd_25 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addPolicyEntityOwner_TDIC(entityOwner.Contact)
    }
    
    property get entityOwner () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 144, column 95
    function label_31 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 144, column 95
    function toCreateAndAdd_32 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addWC7PolicyOwnerOfficer(otherContact.Contact)
    }
    
    property get otherContact () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 171, column 31
    function action_40 () : void {
      EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 171, column 31
    function action_dest_41 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyOwnerOfficer, openForEdit)
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 180, column 30
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7OwnershipPct = (__VALUE_TO_SET as Integer)
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 188, column 111
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=ADA_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 188, column 111
    function editable_50 () : java.lang.Boolean {
      return policyOwnerOfficer.ContactDenorm.Subtype == TC_PERSON
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 191, column 182
    function onChange_49 () : void {
      tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(policyOwnerOfficer.AccountContactRole.AccountContact.Contact, true, period.BaseState)
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 171, column 31
    function valueRoot_43 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 188, column 111
    function valueRoot_53 () : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 196, column 71
    function valueRoot_57 () : java.lang.Object {
      return policyOwnerOfficer.IntrinsicType
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 171, column 31
    function value_42 () : java.lang.String {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 180, column 30
    function value_45 () : Integer {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 188, column 111
    function value_51 () : java.lang.String {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 196, column 71
    function value_56 () : java.lang.String {
      return policyOwnerOfficer.IntrinsicType.DisplayName
    }
    
    property get policyOwnerOfficer () : entity.WC7PolicyOwnerOfficer {
      return getIteratedValue(1) as entity.WC7PolicyOwnerOfficer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 46, column 102
    function conversionExpression_4 (PickedValue :  WC7PolicyOwnerOfficer) : entity.WC7PolicyOwnerOfficer {
      return PickedValue as WC7PolicyOwnerOfficer
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 46, column 102
    function label_3 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewOwnerOfficerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 46, column 102
    function pickLocation_5 () : void {
      WC7NewOwnerOfficerPopup.push(theWC7Line.Branch.WC7Line, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/TDIC_WC7PolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_WC7PolicyOwnerOfficerDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 68, column 72
    function conversionExpression_12 (PickedValue :  Contact) : entity.WC7PolicyOwnerOfficer {
      return theWC7Line.addWC7PolicyOwnerOfficer(PickedValue)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 74, column 76
    function conversionExpression_14 (PickedValue :  Contact) : entity.WC7PolicyOwnerOfficer {
      return theWC7Line.addPolicyEntityOwner_TDIC(PickedValue)
    }
    
    // 'editable' attribute on RowIterator at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 164, column 56
    function editable_39 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 16, column 23
    function initialValue_0 () : WC7Line {
      return period.WC7Line
    }
    
    // 'initialValue' attribute on Variable at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 20, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 79, column 30
    function label_22 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingEntityOwnerContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 105, column 30
    function label_29 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 68, column 72
    function pickLocation_13 () : void {
      ContactSearchPopup.push(TC_OWNEROFFICER)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 74, column 76
    function pickLocation_15 () : void {
      ContactSearchPopup.push(TC_ENTITYOWNER_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 87, column 34
    function sortBy_16 (ownerOfficer :  entity.AccountContact) : java.lang.Object {
      return ownerOfficer.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 40, column 32
    function sortBy_2 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 113, column 34
    function sortBy_23 (entityOwner :  entity.AccountContact) : java.lang.Object {
      return OwnerOfficer.Type.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 139, column 34
    function sortBy_30 (otherContact :  entity.AccountContact) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 171, column 31
    function sortValue_35 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 180, column 30
    function sortValue_36 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 188, column 111
    function sortValue_37 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 196, column 71
    function sortValue_38 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.IntrinsicType.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 99, column 77
    function toCreateAndAdd_21 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addAllExistingWC7PolicyOwnerOfficer()
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 125, column 81
    function toCreateAndAdd_28 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addAllExistingPolicyEntityOwners_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 164, column 56
    function toRemove_59 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : void {
      theWC7Line.removeFromWC7PolicyOwnerOfficers(policyOwnerOfficer)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewEntityOwnerContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 52, column 49
    function value_11 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYENTITYOWNER_TDIC)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 84, column 53
    function value_19 () : entity.AccountContact[] {
      return theWC7Line.UnassignedOwnerOfficers
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 110, column 53
    function value_26 () : entity.AccountContact[] {
      return theWC7Line.UnassignedEntityOwners_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 136, column 53
    function value_33 () : entity.AccountContact[] {
      return theWC7Line.OwnerOfficerOtherCandidates
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewOwnerOfficerContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 37, column 49
    function value_6 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYOWNEROFFICER)
    }
    
    // 'value' attribute on RowIterator at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 164, column 56
    function value_60 () : entity.WC7PolicyOwnerOfficer[] {
      return theWC7Line.WC7PolicyOwnerOfficers
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 99, column 77
    function visible_20 () : java.lang.Boolean {
      return theWC7Line.UnassignedOwnerOfficers.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 125, column 81
    function visible_27 () : java.lang.Boolean {
      return theWC7Line.UnassignedEntityOwners_TDIC.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddOtherContact) at TDIC_WC7PolicyOwnerOfficerDV.pcf: line 131, column 77
    function visible_34 () : java.lang.Boolean {
      return theWC7Line.OwnerOfficerOtherCandidates.Count > 0
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    property get theWC7Line () : WC7Line {
      return getVariableValue("theWC7Line", 0) as WC7Line
    }
    
    property set theWC7Line ($arg :  WC7Line) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    
  }
  
  
}