package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper
@javax.annotation.Generated("config/web/pcf/job/reinstatement/ReinstatementWizard_QuoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReinstatementWizard_QuoteScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/job/reinstatement/ReinstatementWizard_QuoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReinstatementWizard_QuoteScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ReinstateQuote) at ReinstatementWizard_QuoteScreen.pcf: line 46, column 47
    function action_22 () : void {
      printQuote()
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 49, column 76
    function def_onEnter_23 (def :  pcf.WarningsPanelSet) : void {
      def.onEnter(policyPeriod.getWizardQuoteScreenWarnings())
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 51, column 43
    function def_onEnter_25 (def :  pcf.Quote_SummaryDV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_29 (def :  pcf.RatingCumulDetailsPanelSet_BOPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_31 (def :  pcf.RatingCumulDetailsPanelSet_BusinessAutoLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_33 (def :  pcf.RatingCumulDetailsPanelSet_CPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_35 (def :  pcf.RatingCumulDetailsPanelSet_GLLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_37 (def :  pcf.RatingCumulDetailsPanelSet_HOPLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_39 (def :  pcf.RatingCumulDetailsPanelSet_IMLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_41 (def :  pcf.RatingCumulDetailsPanelSet_ManualLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_43 (def :  pcf.RatingCumulDetailsPanelSet_PersonalAutoLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_45 (def :  pcf.RatingCumulDetailsPanelSet_WC7Line) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_47 (def :  pcf.RatingCumulDetailsPanelSet_WorkersCompLine) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_onEnter_49 (def :  pcf.RatingCumulDetailsPanelSet_default) : void {
      def.onEnter(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_54 (def :  pcf.RatingTxDetailsPanelSet_BOPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_56 (def :  pcf.RatingTxDetailsPanelSet_BusinessAutoLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_58 (def :  pcf.RatingTxDetailsPanelSet_CPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_60 (def :  pcf.RatingTxDetailsPanelSet_GLLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_62 (def :  pcf.RatingTxDetailsPanelSet_HOPLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_64 (def :  pcf.RatingTxDetailsPanelSet_IMLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_66 (def :  pcf.RatingTxDetailsPanelSet_PersonalAutoLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_68 (def :  pcf.RatingTxDetailsPanelSet_WC7Line) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_70 (def :  pcf.RatingTxDetailsPanelSet_WorkersCompLine) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_onEnter_72 (def :  pcf.RatingTxDetailsPanelSet_default) : void {
      def.onEnter(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 49, column 76
    function def_refreshVariables_24 (def :  pcf.WarningsPanelSet) : void {
      def.refreshVariables(policyPeriod.getWizardQuoteScreenWarnings())
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 51, column 43
    function def_refreshVariables_26 (def :  pcf.Quote_SummaryDV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_30 (def :  pcf.RatingCumulDetailsPanelSet_BOPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_32 (def :  pcf.RatingCumulDetailsPanelSet_BusinessAutoLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_34 (def :  pcf.RatingCumulDetailsPanelSet_CPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_36 (def :  pcf.RatingCumulDetailsPanelSet_GLLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_38 (def :  pcf.RatingCumulDetailsPanelSet_HOPLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_40 (def :  pcf.RatingCumulDetailsPanelSet_IMLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_42 (def :  pcf.RatingCumulDetailsPanelSet_ManualLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_44 (def :  pcf.RatingCumulDetailsPanelSet_PersonalAutoLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_46 (def :  pcf.RatingCumulDetailsPanelSet_WC7Line) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_48 (def :  pcf.RatingCumulDetailsPanelSet_WorkersCompLine) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function def_refreshVariables_50 (def :  pcf.RatingCumulDetailsPanelSet_default) : void {
      def.refreshVariables(policyPeriod, jobWizardHelper, isEditable)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_55 (def :  pcf.RatingTxDetailsPanelSet_BOPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_57 (def :  pcf.RatingTxDetailsPanelSet_BusinessAutoLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_59 (def :  pcf.RatingTxDetailsPanelSet_CPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_61 (def :  pcf.RatingTxDetailsPanelSet_GLLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_63 (def :  pcf.RatingTxDetailsPanelSet_HOPLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_65 (def :  pcf.RatingTxDetailsPanelSet_IMLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_67 (def :  pcf.RatingTxDetailsPanelSet_PersonalAutoLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_69 (def :  pcf.RatingTxDetailsPanelSet_WC7Line) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_71 (def :  pcf.RatingTxDetailsPanelSet_WorkersCompLine) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 71, column 40
    function def_refreshVariables_73 (def :  pcf.RatingTxDetailsPanelSet_default) : void {
      def.refreshVariables(policyPeriod, DisplayKey.get("Web.Quote.TotalPremLabel.Total"), DisplayKey.get("Web.Quote.TotalCostLabel.Total"), isEditable, jobWizardHelper)
    }
    
    // 'initialValue' attribute on Variable at ReinstatementWizard_QuoteScreen.pcf: line 23, column 46
    function initialValue_0 () : gw.document.DocumentProduction {
      return new gw.document.DocumentProduction()
    }
    
    // 'initialValue' attribute on Variable at ReinstatementWizard_QuoteScreen.pcf: line 27, column 33
    function initialValue_1 () : entity.PolicyLine {
      return policyPeriod.RepresentativePolicyLines.first()
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function mode_2 () : java.lang.Object {
      return policyPeriod.Job.Subtype
    }
    
    // 'mode' attribute on PanelRef at ReinstatementWizard_QuoteScreen.pcf: line 60, column 40
    function mode_51 () : java.lang.Object {
      return line.Pattern.PublicID
    }
    
    // 'title' attribute on TitleBar at ReinstatementWizard_QuoteScreen.pcf: line 63, column 76
    function title_28 () : java.lang.String {
      return line.Pattern.DisplayName
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_11 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_13 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_15 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_17 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_19 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_3 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_5 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_7 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_onEnter_9 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.onEnter(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_10 (def :  pcf.JobWizardToolbarButtonSet_PolicyChange) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_12 (def :  pcf.JobWizardToolbarButtonSet_Reinstatement) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_14 (def :  pcf.JobWizardToolbarButtonSet_Renewal) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_16 (def :  pcf.JobWizardToolbarButtonSet_Rewrite) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_18 (def :  pcf.JobWizardToolbarButtonSet_RewriteNewAccount) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_20 (def :  pcf.JobWizardToolbarButtonSet_Submission) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_4 (def :  pcf.JobWizardToolbarButtonSet_Audit) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_6 (def :  pcf.JobWizardToolbarButtonSet_Cancellation) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ReinstatementWizard_QuoteScreen.pcf: line 32, column 101
    function toolbarButtonSet_refreshVariables_8 (def :  pcf.JobWizardToolbarButtonSet_Issuance) : void {
      def.refreshVariables(policyPeriod, reinstatement, jobWizardHelper)
    }
    
    // 'visible' attribute on ToolbarButton (id=ReinstateQuote) at ReinstatementWizard_QuoteScreen.pcf: line 46, column 47
    function visible_21 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on TitleBar at ReinstatementWizard_QuoteScreen.pcf: line 63, column 76
    function visible_27 () : java.lang.Boolean {
      return policyPeriod.RepresentativePolicyLines.Count > 1
    }
    
    property get documentProduction () : gw.document.DocumentProduction {
      return getVariableValue("documentProduction", 0) as gw.document.DocumentProduction
    }
    
    property set documentProduction ($arg :  gw.document.DocumentProduction) {
      setVariableValue("documentProduction", 0, $arg)
    }
    
    property get isEditable () : boolean {
      return getRequireValue("isEditable", 0) as java.lang.Boolean
    }
    
    property set isEditable ($arg :  boolean) {
      setRequireValue("isEditable", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get line () : entity.PolicyLine {
      return getVariableValue("line", 0) as entity.PolicyLine
    }
    
    property set line ($arg :  entity.PolicyLine) {
      setVariableValue("line", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get reinstatement () : Reinstatement {
      return getRequireValue("reinstatement", 0) as Reinstatement
    }
    
    property set reinstatement ($arg :  Reinstatement) {
      setRequireValue("reinstatement", 0, $arg)
    }
    
    
    function printQuote() {
      (new TDIC_PCExstreamHelper()).printQuoteDocs(policyPeriod)
    }
    
    
  }
  
  
}