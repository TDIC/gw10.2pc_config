package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_IntegerField_NonEditCurrency_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf: line 20, column 23
    function initialValue_0 () : block() {
      return \ -> question
    }
    
    // 'onChange' attribute on PostOnChange at QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf: line 28, column 78
    function onChange_1 () : void {
      if (onChangeBlock != null) onChangeBlock();ChangeBlock()
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf: line 26, column 279
    function requestValidationExpression_2 (VALUE :  java.lang.String) : java.lang.Object {
      return question.getLengthForQuestion_TDIC(VALUE?.toString().length)
    }
    
    // 'required' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf: line 26, column 279
    function required_3 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on TextCell (id=IntegerFieldInput_Currency_Cell) at QuestionModalInput.IntegerField_NonEditCurrency_TDIC.pcf: line 26, column 279
    function value_4 () : java.lang.String {
      if(answerContainer.getAnswer(question).IntegerAnswer == null ){return ""}else{return "$"+tdic.web.pcf.helper.GLCPUWQuestionAnswersCurrencyHelper.getUWQuestionCurrencyAnswerNoDecimal(question, answerContainer.getAnswer(question).IntegerAnswer)}
    }
    
    property get ChangeBlock () : block() {
      return getVariableValue("ChangeBlock", 0) as block()
    }
    
    property set ChangeBlock ($arg :  block()) {
      setVariableValue("ChangeBlock", 0, $arg)
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}