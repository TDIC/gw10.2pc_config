package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses pcf.api.Wizard
uses gw.api.database.Query
uses gw.api.database.Relop
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GeneralLiabilityEUDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GeneralLiabilityEUDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=End) at GeneralLiabilityEUDV.pcf: line 69, column 61
    function allCheckedRowsAction_10 (CheckedValues :  entity.GLExposure[], CheckedValuesErrors :  java.util.Map) : void {
      endExposure(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Split) at GeneralLiabilityEUDV.pcf: line 77, column 61
    function allCheckedRowsAction_12 (CheckedValues :  entity.GLExposure[], CheckedValuesErrors :  java.util.Map) : void {
      splitExposure(CheckedValues)
    }
    
    // 'allowToggle' attribute on InputGroup (id=localAnesthesiaInputGroup) at GeneralLiabilityEUDV.pcf: line 321, column 49
    function available_169 () : java.lang.Boolean {
      return !isNoneChecked
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 14, column 23
    function initialValue_0 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == typekey.AnestheticModilities_TDIC.TC_GENERALANESTHESIAINOFFICE)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 19, column 23
    function initialValue_1 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == typekey.AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINOFFICE)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 24, column 23
    function initialValue_2 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == typekey.AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINHOSPITAL)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 29, column 23
    function initialValue_3 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == AnestheticModilities_TDIC.TC_NONE)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 34, column 23
    function initialValue_4 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == AnestheticModilities_TDIC.TC_LOCALANESTHESIA)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 39, column 23
    function initialValue_5 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == AnestheticModilities_TDIC.TC_N2OO2ANALGESIA)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 44, column 23
    function initialValue_6 () : boolean {
      return policyPeriod.GLLine.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == AnestheticModilities_TDIC.TC_ORALCONSCIOUSSEDATION)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 49, column 25
    function initialValue_7 () : QuoteType {
      return setInitialQuoteType()
    }
    
    // 'onToggle' attribute on InputGroup (id=noneInputGroup) at GeneralLiabilityEUDV.pcf: line 312, column 49
    function setter_165 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_NONE,VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=localAnesthesiaInputGroup) at GeneralLiabilityEUDV.pcf: line 321, column 49
    function setter_170 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_LOCALANESTHESIA,VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=N2O2InputGroup) at GeneralLiabilityEUDV.pcf: line 330, column 49
    function setter_175 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_N2OO2ANALGESIA,VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=oralConsciousSedationInputGroup) at GeneralLiabilityEUDV.pcf: line 339, column 49
    function setter_180 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_ORALCONSCIOUSSEDATION,VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=ivimSedationApplicantInputGroup) at GeneralLiabilityEUDV.pcf: line 348, column 49
    function setter_185 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINHOSPITAL, VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=ivimSedationHospitalInputGroup) at GeneralLiabilityEUDV.pcf: line 357, column 49
    function setter_220 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINOFFICE,VALUE)
    }
    
    // 'onToggle' attribute on InputGroup (id=generalAnesthesiaInOfficeInputGroup) at GeneralLiabilityEUDV.pcf: line 409, column 49
    function setter_255 (VALUE :  java.lang.Boolean) : void {
      policyPeriod.GLLine.setGLAnestheticModalitiesTypeKey_TDIC(AnestheticModilities_TDIC.TC_GENERALANESTHESIAINOFFICE,VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at GeneralLiabilityEUDV.pcf: line 96, column 30
    function sortBy_13 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode.Code
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at GeneralLiabilityEUDV.pcf: line 102, column 33
    function sortValue_14 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at GeneralLiabilityEUDV.pcf: line 109, column 33
    function sortValue_15 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function sortValue_16 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.LocationWM
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function sortValue_17 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function sortValue_18 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 378, column 59
    function sortValue_188 (ivimSedationHospital :  entity.IVIMSedationInHospital_TDIC) : java.lang.Object {
      return ivimSedationHospital.Name_TDIC
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 383, column 74
    function sortValue_189 (ivimSedationHospital :  entity.IVIMSedationInHospital_TDIC) : java.lang.Object {
      return ivimSedationHospital.PermitLicenseNumber_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function sortValue_19 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 388, column 64
    function sortValue_190 (ivimSedationHospital :  entity.IVIMSedationInHospital_TDIC) : java.lang.Object {
      return ivimSedationHospital.Specialty_TDIC
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant1_Cell) at GeneralLiabilityEUDV.pcf: line 393, column 73
    function sortValue_191 (ivimSedationHospital :  entity.IVIMSedationInHospital_TDIC) : java.lang.Object {
      return ivimSedationHospital.AdministerApplicant_TDIC
    }
    
    // 'value' attribute on TextCell (id=SpecialityCodeClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 165, column 52
    function sortValue_20 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.Description_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 170, column 34
    function sortValue_21 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode.Classification
    }
    
    // 'value' attribute on TextCell (id=BasisAmount_Cell) at GeneralLiabilityEUDV.pcf: line 179, column 34
    function sortValue_22 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 430, column 56
    function sortValue_223 (generalAnesthesia :  entity.GeneralAnesthesiaInOffice_TDIC) : java.lang.Object {
      return generalAnesthesia.Name_TDIC
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 435, column 71
    function sortValue_224 (generalAnesthesia :  entity.GeneralAnesthesiaInOffice_TDIC) : java.lang.Object {
      return generalAnesthesia.PermitLicenseNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 440, column 61
    function sortValue_225 (generalAnesthesia :  entity.GeneralAnesthesiaInOffice_TDIC) : java.lang.Object {
      return generalAnesthesia.Specialty_TDIC
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant_Cell) at GeneralLiabilityEUDV.pcf: line 445, column 70
    function sortValue_226 (generalAnesthesia :  entity.GeneralAnesthesiaInOffice_TDIC) : java.lang.Object {
      return generalAnesthesia.AdministerApplicant_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClassCodeUnits_Cell) at GeneralLiabilityEUDV.pcf: line 185, column 34
    function sortValue_23 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.ClassCode.Basis.Description
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function sortValue_24 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at GeneralLiabilityEUDV.pcf: line 202, column 67
    function sortValue_25 (exposure :  entity.GLExposure) : java.lang.Object {
      return exposure.GLTerritory_TDIC?.TerritoryCode
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at GeneralLiabilityEUDV.pcf: line 89, column 45
    function toCreateAndAdd_160 () : entity.GLExposure {
      return policyPeriod.GLLine.addExposureWM()
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=ivimSedationHosipitalIterator) at GeneralLiabilityEUDV.pcf: line 372, column 64
    function toCreateAndAdd_215 () : entity.IVIMSedationInHospital_TDIC {
      return policyPeriod.GLLine.createAndAddGLIVIMSedationInHospital_TDIC()
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=GenralAneshesiaInOfficeIterator) at GeneralLiabilityEUDV.pcf: line 424, column 67
    function toCreateAndAdd_250 () : entity.GeneralAnesthesiaInOffice_TDIC {
      return policyPeriod.GLLine.createAndAddGLGeneralAnesthesiaInOffice_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at GeneralLiabilityEUDV.pcf: line 89, column 45
    function toRemove_161 (exposure :  entity.GLExposure) : void {
      exposure.removeWM()
    }
    
    // 'toRemove' attribute on RowIterator (id=ivimSedationHosipitalIterator) at GeneralLiabilityEUDV.pcf: line 372, column 64
    function toRemove_216 (ivimSedationHospital :  entity.IVIMSedationInHospital_TDIC) : void {
      policyPeriod.GLLine.removeFromGLIVIMSedationInHospital_TDIC(ivimSedationHospital)
    }
    
    // 'toRemove' attribute on RowIterator (id=GenralAneshesiaInOfficeIterator) at GeneralLiabilityEUDV.pcf: line 424, column 67
    function toRemove_251 (generalAnesthesia :  entity.GeneralAnesthesiaInOffice_TDIC) : void {
      policyPeriod.GLLine.removeFromGLGeneralAnesthesiaInOffice_TDIC(generalAnesthesia)
    }
    
    // 'value' attribute on RowIterator at GeneralLiabilityEUDV.pcf: line 89, column 45
    function value_162 () : entity.GLExposure[] {
      return policyPeriod.GLLine.GLExposuresWM
    }
    
    // 'value' attribute on RowIterator (id=ivimSedationHosipitalIterator) at GeneralLiabilityEUDV.pcf: line 372, column 64
    function value_217 () : entity.IVIMSedationInHospital_TDIC[] {
      return policyPeriod.GLLine.GLIVIMSedationInHospital_TDIC
    }
    
    // 'value' attribute on RowIterator (id=GenralAneshesiaInOfficeIterator) at GeneralLiabilityEUDV.pcf: line 424, column 67
    function value_252 () : entity.GeneralAnesthesiaInOffice_TDIC[] {
      return policyPeriod.GLLine.GLGeneralAnesthesiaInOffice_TDIC
    }
    
    // 'visible' attribute on Label at GeneralLiabilityEUDV.pcf: line 306, column 50
    function visible_163 () : java.lang.Boolean {
      return quoteType!=QuoteType.TC_QUICK
    }
    
    // 'childrenVisible' attribute on InputGroup (id=noneInputGroup) at GeneralLiabilityEUDV.pcf: line 312, column 49
    function visible_164 () : java.lang.Boolean {
      return isNoneChecked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=localAnesthesiaInputGroup) at GeneralLiabilityEUDV.pcf: line 321, column 49
    function visible_168 () : java.lang.Boolean {
      return isLocalAnesthesiaChecked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=N2O2InputGroup) at GeneralLiabilityEUDV.pcf: line 330, column 49
    function visible_173 () : java.lang.Boolean {
      return isN2OO2Checked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=oralConsciousSedationInputGroup) at GeneralLiabilityEUDV.pcf: line 339, column 49
    function visible_178 () : java.lang.Boolean {
      return isOralConsciousSedationChecked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ivimSedationApplicantInputGroup) at GeneralLiabilityEUDV.pcf: line 348, column 49
    function visible_183 () : java.lang.Boolean {
      return isSedationInHospitalChecked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ivimSedationHospitalInputGroup) at GeneralLiabilityEUDV.pcf: line 357, column 49
    function visible_218 () : java.lang.Boolean {
      return isSedationByApplicantChecked
    }
    
    // 'childrenVisible' attribute on InputGroup (id=generalAnesthesiaInOfficeInputGroup) at GeneralLiabilityEUDV.pcf: line 409, column 49
    function visible_253 () : java.lang.Boolean {
      return isGeneralAnesthesiaInOfficeChecked
    }
    
    // 'addVisible' attribute on IteratorButtons at GeneralLiabilityEUDV.pcf: line 61, column 61
    function visible_8 () : java.lang.Boolean {
      return policyPeriod.GLLine.GLExposuresWM.Count > 0 ? false : true
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=End) at GeneralLiabilityEUDV.pcf: line 69, column 61
    function visible_9 () : java.lang.Boolean {
      return policyPeriod.Job typeis PolicyChange
    }
    
    property get isGeneralAnesthesiaInOfficeChecked () : boolean {
      return getVariableValue("isGeneralAnesthesiaInOfficeChecked", 0) as java.lang.Boolean
    }
    
    property set isGeneralAnesthesiaInOfficeChecked ($arg :  boolean) {
      setVariableValue("isGeneralAnesthesiaInOfficeChecked", 0, $arg)
    }
    
    property get isLocalAnesthesiaChecked () : boolean {
      return getVariableValue("isLocalAnesthesiaChecked", 0) as java.lang.Boolean
    }
    
    property set isLocalAnesthesiaChecked ($arg :  boolean) {
      setVariableValue("isLocalAnesthesiaChecked", 0, $arg)
    }
    
    property get isN2OO2Checked () : boolean {
      return getVariableValue("isN2OO2Checked", 0) as java.lang.Boolean
    }
    
    property set isN2OO2Checked ($arg :  boolean) {
      setVariableValue("isN2OO2Checked", 0, $arg)
    }
    
    property get isNoneChecked () : boolean {
      return getVariableValue("isNoneChecked", 0) as java.lang.Boolean
    }
    
    property set isNoneChecked ($arg :  boolean) {
      setVariableValue("isNoneChecked", 0, $arg)
    }
    
    property get isOralConsciousSedationChecked () : boolean {
      return getVariableValue("isOralConsciousSedationChecked", 0) as java.lang.Boolean
    }
    
    property set isOralConsciousSedationChecked ($arg :  boolean) {
      setVariableValue("isOralConsciousSedationChecked", 0, $arg)
    }
    
    property get isSedationByApplicantChecked () : boolean {
      return getVariableValue("isSedationByApplicantChecked", 0) as java.lang.Boolean
    }
    
    property set isSedationByApplicantChecked ($arg :  boolean) {
      setVariableValue("isSedationByApplicantChecked", 0, $arg)
    }
    
    property get isSedationInHospitalChecked () : boolean {
      return getVariableValue("isSedationInHospitalChecked", 0) as java.lang.Boolean
    }
    
    property set isSedationInHospitalChecked ($arg :  boolean) {
      setVariableValue("isSedationInHospitalChecked", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getVariableValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    
    
    function findFirstMatchingClassCode(code : String, exposure : GLExposure) : GLClassCode{
      var classCode = exposure.firstMatchingClassCode(code)
      if (classCode == null) {
        gw.api.util.LocationUtil.addRequestScopedErrorMessage(DisplayKey.get("Java.ClassCodePickerWidget.InvalidCode", code))
      }
      return classCode
    }
    
    function endExposure(exposures : GLExposure[]){
      if((CurrentLocation as Wizard).saveDraft()){ // to keep the data up-to-date before changing LV PL-5377
        for(exposure in exposures){
          exposure.endDateWM(policyPeriod.EditEffectiveDate)
        }
      }
    }
    
    function splitExposure(exposures : GLExposure[]){
      if((CurrentLocation as Wizard).saveDraft()){ // to keep the data up-to-date before changing LV PL-5377
        for(exposure in exposures){
          exposure.splitWM(policyPeriod.EditEffectiveDate)
        }
      }
    }
    
    function getPolicyLocationsFilteredByNonSpecificLocationSupport(exposure:GLExposure) : PolicyLocation[] {
      var locations = policyPeriod.PolicyLocationsWM
      if (policyPeriod.GLLine.SupportsNonSpecificLocations) {
        return locations
      }
      return locations.where(\ l -> not l.AccountLocation.NonSpecific)
    }
    
    /*
    function getStateComponents(stateCode: String) : GLTerritory_TDIC[] {
      if (stateCode != null) {
        var query = Query.make( GLTerritory_TDIC )
        query.compare("StateCode", Relop.Equals, stateCode)
        var componentTerritoryPairs = query.select()
        if (componentTerritoryPairs != null) {
          glStateComponents = componentTerritoryPairs.toTypedArray()
          return componentTerritoryPairs.toTypedArray()
        }
      }
      return null
    }
    
    function getComponentTerritoryCode(stateCode: String, componentCode: String) : String {
      if (stateCode != null and componentCode != null) {
        var query = Query.make( GLTerritory_TDIC )
        query.compare("StateCode", Relop.Equals, stateCode)
        query.compare("Component", Relop.Equals, componentCode)
        var componentTerritoryPair = query.select().FirstResult
        if (componentTerritoryPair != null) {
          componentTerritoryCode = componentTerritoryPair.TerritoryCode
          return componentTerritoryPair.TerritoryCode
        }
      }
      return null
    }
    
    */
    
    function getSpecialityCode():List<SpecialityCode_TDIC>{
      var typekeys : List<SpecialityCode_TDIC> = {}
      if(policyPeriod.BaseState == Jurisdiction.TC_PA){
        return SpecialityCode_TDIC.getTypeKeys(false)
      }else{
        typekeys.add(SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_10ORALSURGERY_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_15ENDODONTICS_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_20ORTHODONTICS_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_30PEDIATRICDENTISTRY_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_40PERIODONTICS_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_50PROSTHODONTICS_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_60ORALPATHOLOGY_TDIC)
        typekeys.add(SpecialityCode_TDIC.TC_90DENTALANESTHESIOLOGY_TDIC)
        
      }
      return typekeys
    }
    
    function setInitialQuoteType():QuoteType{
      if(policyPeriod.Job typeis Submission){
        return (policyPeriod.Job as Submission).QuoteType
      }
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function action_106 () : void {
      GLClassCodeSearchPopup.push(policyPeriod.GLLine, version)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function action_dest_107 () : pcf.api.Destination {
      return pcf.GLClassCodeSearchPopup.createDestination(policyPeriod.GLLine, version)
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      version.ClassCode = (__VALUE_TO_SET as entity.GLClassCode)
    }
    
    // 'value' attribute on TypeKeyCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 252, column 59
    function defaultSetter_118 (__VALUE_TO_SET :  java.lang.Object) : void {
      version.SpecialityCode_TDIC = (__VALUE_TO_SET as typekey.SpecialityCode_TDIC)
    }
    
    // 'value' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function defaultSetter_124 (__VALUE_TO_SET :  java.lang.Object) : void {
      version.ClassCode_TDIC = (__VALUE_TO_SET as typekey.ClassCode_TDIC)
    }
    
    // 'value' attribute on TextCell (id=BasisAmount_Cell) at GeneralLiabilityEUDV.pcf: line 279, column 36
    function defaultSetter_138 (__VALUE_TO_SET :  java.lang.Object) : void {
      version.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function defaultSetter_147 (__VALUE_TO_SET :  java.lang.Object) : void {
      version.GLTerritory_TDIC = (__VALUE_TO_SET as entity.GLTerritory_TDIC)
    }
    
    // 'editable' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function editable_108 () : java.lang.Boolean {
      return version.LocationWM != null
    }
    
    // 'editable' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function editable_122 () : java.lang.Boolean {
      return versionClassCodes.Count == 1 ? false : true
    }
    
    // 'editable' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function editable_145 () : java.lang.Boolean {
      return version.NewExposure
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 214, column 52
    function initialValue_95 () : List<ClassCode_TDIC> {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,version)
    }
    
    // RowIterator (id=VersionIterator) at GeneralLiabilityEUDV.pcf: line 210, column 47
    function initializeVariables_157 () : void {
        versionClassCodes = tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,version);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function inputConversion_109 (VALUE :  java.lang.String) : java.lang.Object {
      return findFirstMatchingClassCode(VALUE, version)
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 235, column 125
    function onChange_102 () : void {
      tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(version.LocationWM.State.Code)
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 254, column 211
    function onChange_116 () : void {
      version.ClassCode_TDIC = null ; versionClassCodes = tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,version);version.setGLExposureDescription_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 264, column 73
    function onChange_121 () : void {
      version.setGLExposureDescription_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 293, column 186
    function onChange_144 () : void {
      tdic.web.pcf.helper.GLExposureUnitsHelper.setTerritoryCodeByComponent(exposure.GLTerritory_TDIC.StateCode, exposure.GLTerritory_TDIC.Component, version)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function outputConversion_110 (VALUE :  entity.GLClassCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function valueRange_126 () : java.lang.Object {
      return versionClassCodes
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function valueRange_149 () : java.lang.Object {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(policyPeriod.BaseState.Code)
    }
    
    // 'value' attribute on TextCell (id=ClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 272, column 36
    function valueRoot_135 () : java.lang.Object {
      return version.ClassCode
    }
    
    // 'value' attribute on TextCell (id=ClassCodeUnits_Cell) at GeneralLiabilityEUDV.pcf: line 284, column 36
    function valueRoot_142 () : java.lang.Object {
      return version.ClassCode.Basis
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode2_Cell) at GeneralLiabilityEUDV.pcf: line 298, column 68
    function valueRoot_155 () : java.lang.Object {
      return version.GLTerritory_TDIC
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at GeneralLiabilityEUDV.pcf: line 219, column 35
    function valueRoot_97 () : java.lang.Object {
      return version
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 233, column 35
    function value_103 () : entity.PolicyLocation {
      return version.LocationWM
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 246, column 36
    function value_111 () : entity.GLClassCode {
      return version.ClassCode
    }
    
    // 'value' attribute on TypeKeyCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 252, column 59
    function value_117 () : typekey.SpecialityCode_TDIC {
      return version.SpecialityCode_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function value_123 () : typekey.ClassCode_TDIC {
      return version.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=SpecialityCodeClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 268, column 53
    function value_131 () : java.lang.String {
      return version.Description_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 272, column 36
    function value_134 () : java.lang.String {
      return version.ClassCode.Classification
    }
    
    // 'value' attribute on TextCell (id=BasisAmount_Cell) at GeneralLiabilityEUDV.pcf: line 279, column 36
    function value_137 () : java.lang.Integer {
      return version.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=ClassCodeUnits_Cell) at GeneralLiabilityEUDV.pcf: line 284, column 36
    function value_141 () : java.lang.String {
      return version.ClassCode.Basis.Description
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function value_146 () : entity.GLTerritory_TDIC {
      return version.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode2_Cell) at GeneralLiabilityEUDV.pcf: line 298, column 68
    function value_154 () : java.lang.String {
      return version.GLTerritory_TDIC?.TerritoryCode
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at GeneralLiabilityEUDV.pcf: line 219, column 35
    function value_96 () : java.util.Date {
      return version.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at GeneralLiabilityEUDV.pcf: line 225, column 35
    function value_99 () : java.util.Date {
      return version.ExpirationDate
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function verifyValueRangeIsAllowedType_127 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function verifyValueRangeIsAllowedType_127 ($$arg :  typekey.ClassCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function verifyValueRangeIsAllowedType_150 ($$arg :  entity.GLTerritory_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function verifyValueRangeIsAllowedType_150 ($$arg :  gw.api.database.IQueryBeanResult<entity.GLTerritory_TDIC>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function verifyValueRangeIsAllowedType_150 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 262, column 54
    function verifyValueRange_128 () : void {
      var __valueRangeArg = versionClassCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_127(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory2_Cell) at GeneralLiabilityEUDV.pcf: line 291, column 55
    function verifyValueRange_151 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(policyPeriod.BaseState.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_150(__valueRangeArg)
    }
    
    property get version () : entity.GLExposure {
      return getIteratedValue(2) as entity.GLExposure
    }
    
    property get versionClassCodes () : List<ClassCode_TDIC> {
      return getVariableValue("versionClassCodes", 2) as List<ClassCode_TDIC>
    }
    
    property set versionClassCodes ($arg :  List<ClassCode_TDIC>) {
      setVariableValue("versionClassCodes", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends GeneralLiabilityEUDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 378, column 59
    function defaultSetter_194 (__VALUE_TO_SET :  java.lang.Object) : void {
      ivimSedationHospital.Name_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 383, column 74
    function defaultSetter_200 (__VALUE_TO_SET :  java.lang.Object) : void {
      ivimSedationHospital.PermitLicenseNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 388, column 64
    function defaultSetter_206 (__VALUE_TO_SET :  java.lang.Object) : void {
      ivimSedationHospital.Specialty_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant1_Cell) at GeneralLiabilityEUDV.pcf: line 393, column 73
    function defaultSetter_212 (__VALUE_TO_SET :  java.lang.Object) : void {
      ivimSedationHospital.AdministerApplicant_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 378, column 59
    function editable_192 () : java.lang.Boolean {
      return !ivimSedationHospital.AdministerApplicant_TDIC
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 395, column 212
    function onChange_210 () : void {
      if(ivimSedationHospital.AdministerApplicant_TDIC){ivimSedationHospital.Name_TDIC=null;ivimSedationHospital.PermitLicenseNumber_TDIC=null;ivimSedationHospital.Specialty_TDIC=null}
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 378, column 59
    function valueRoot_195 () : java.lang.Object {
      return ivimSedationHospital
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 378, column 59
    function value_193 () : java.lang.String {
      return ivimSedationHospital.Name_TDIC
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 383, column 74
    function value_199 () : java.lang.String {
      return ivimSedationHospital.PermitLicenseNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 388, column 64
    function value_205 () : java.lang.String {
      return ivimSedationHospital.Specialty_TDIC
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant1_Cell) at GeneralLiabilityEUDV.pcf: line 393, column 73
    function value_211 () : java.lang.Boolean {
      return ivimSedationHospital.AdministerApplicant_TDIC
    }
    
    property get ivimSedationHospital () : entity.IVIMSedationInHospital_TDIC {
      return getIteratedValue(1) as entity.IVIMSedationInHospital_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends GeneralLiabilityEUDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 430, column 56
    function defaultSetter_229 (__VALUE_TO_SET :  java.lang.Object) : void {
      generalAnesthesia.Name_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 435, column 71
    function defaultSetter_235 (__VALUE_TO_SET :  java.lang.Object) : void {
      generalAnesthesia.PermitLicenseNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 440, column 61
    function defaultSetter_241 (__VALUE_TO_SET :  java.lang.Object) : void {
      generalAnesthesia.Specialty_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant_Cell) at GeneralLiabilityEUDV.pcf: line 445, column 70
    function defaultSetter_247 (__VALUE_TO_SET :  java.lang.Object) : void {
      generalAnesthesia.AdministerApplicant_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 430, column 56
    function editable_227 () : java.lang.Boolean {
      return !generalAnesthesia.AdministerApplicant_TDIC
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 447, column 200
    function onChange_245 () : void {
      if(generalAnesthesia.AdministerApplicant_TDIC){generalAnesthesia.Name_TDIC=null;generalAnesthesia.PermitLicenseNumber_TDIC=null;generalAnesthesia.Specialty_TDIC=null}
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 430, column 56
    function valueRoot_230 () : java.lang.Object {
      return generalAnesthesia
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at GeneralLiabilityEUDV.pcf: line 430, column 56
    function value_228 () : java.lang.String {
      return generalAnesthesia.Name_TDIC
    }
    
    // 'value' attribute on TextCell (id=permitLicenseNumber_Cell) at GeneralLiabilityEUDV.pcf: line 435, column 71
    function value_234 () : java.lang.String {
      return generalAnesthesia.PermitLicenseNumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=speciality_Cell) at GeneralLiabilityEUDV.pcf: line 440, column 61
    function value_240 () : java.lang.String {
      return generalAnesthesia.Specialty_TDIC
    }
    
    // 'value' attribute on CheckBoxCell (id=adminsterApplicant_Cell) at GeneralLiabilityEUDV.pcf: line 445, column 70
    function value_246 () : java.lang.Boolean {
      return generalAnesthesia.AdministerApplicant_TDIC
    }
    
    property get generalAnesthesia () : entity.GeneralAnesthesiaInOffice_TDIC {
      return getIteratedValue(1) as entity.GeneralAnesthesiaInOffice_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GeneralLiabilityEUDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GeneralLiabilityEUDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function action_43 () : void {
      GLClassCodeSearchPopup.push(policyPeriod.GLLine, exposure)
    }
    
    // 'pickLocation' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function action_dest_44 () : pcf.api.Destination {
      return pcf.GLClassCodeSearchPopup.createDestination(policyPeriod.GLLine, exposure)
    }
    
    // 'available' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function available_45 () : java.lang.Boolean {
      return exposure.LocationWM != null
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.LocationWM = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.ClassCode = (__VALUE_TO_SET as entity.GLClassCode)
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.SpecialityCode_TDIC = (__VALUE_TO_SET as typekey.SpecialityCode_TDIC)
    }
    
    // 'value' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.ClassCode_TDIC = (__VALUE_TO_SET as typekey.ClassCode_TDIC)
    }
    
    // 'value' attribute on TextCell (id=BasisAmount_Cell) at GeneralLiabilityEUDV.pcf: line 179, column 34
    function defaultSetter_78 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function defaultSetter_86 (__VALUE_TO_SET :  java.lang.Object) : void {
      exposure.GLTerritory_TDIC = (__VALUE_TO_SET as entity.GLTerritory_TDIC)
    }
    
    // 'editable' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function editable_34 () : java.lang.Boolean {
      return exposure.NewExposure
    }
    
    // 'editable' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function editable_62 () : java.lang.Boolean {
      return exposureClassCodes.Count == 1 ? false : (!(policyPeriod.Job typeis Submission) && (policyPeriod.BasedOn.GLLine.GLExposuresWM.first().ClassCode_TDIC == ClassCode_TDIC.TC_01 || policyPeriod.BasedOn.GLLine.GLExposuresWM.first().ClassCode_TDIC == ClassCode_TDIC.TC_02) ? false : true)
    }
    
    // 'initialValue' attribute on Variable at GeneralLiabilityEUDV.pcf: line 93, column 50
    function initialValue_26 () : List<ClassCode_TDIC> {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,exposure)
    }
    
    // RowIterator at GeneralLiabilityEUDV.pcf: line 89, column 45
    function initializeVariables_159 () : void {
        exposureClassCodes = tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,exposure);

    }
    
    // 'inputConversion' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function inputConversion_46 (VALUE :  java.lang.String) : java.lang.Object {
      return findFirstMatchingClassCode(VALUE, exposure)
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 123, column 124
    function onChange_33 () : void {
      tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(exposure.LocationWM.State.Code)
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 149, column 350
    function onChange_53 () : void {
      if ((!(policyPeriod.Job typeis Submission) && policyPeriod.GLLine.GLExposuresWM.first().ClassCode_TDIC != ClassCode_TDIC.TC_01)) exposure.ClassCode_TDIC = null ; exposureClassCodes = tdic.web.pcf.helper.GLExposureUnitsHelper.filterClassCodes_TDIC(policyPeriod,exposure);exposure.setGLExposureDescription_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 160, column 72
    function onChange_61 () : void {
      exposure.setGLExposureDescription_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at GeneralLiabilityEUDV.pcf: line 196, column 185
    function onChange_84 () : void {
      tdic.web.pcf.helper.GLExposureUnitsHelper.setTerritoryCodeByComponent(exposure.GLTerritory_TDIC.StateCode, exposure.GLTerritory_TDIC.Component, exposure)
    }
    
    // 'outputConversion' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function outputConversion_47 (VALUE :  entity.GLClassCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function valueRange_38 () : java.lang.Object {
      return getPolicyLocationsFilteredByNonSpecificLocationSupport(exposure)
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function valueRange_57 () : java.lang.Object {
      return getSpecialityCode()
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function valueRange_66 () : java.lang.Object {
      return exposureClassCodes
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function valueRange_88 () : java.lang.Object {
      return tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(policyPeriod.BaseState.Code)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at GeneralLiabilityEUDV.pcf: line 102, column 33
    function valueRoot_28 () : java.lang.Object {
      return exposure
    }
    
    // 'value' attribute on TextCell (id=ClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 170, column 34
    function valueRoot_75 () : java.lang.Object {
      return exposure.ClassCode
    }
    
    // 'value' attribute on TextCell (id=ClassCodeUnits_Cell) at GeneralLiabilityEUDV.pcf: line 185, column 34
    function valueRoot_82 () : java.lang.Object {
      return exposure.ClassCode.Basis
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at GeneralLiabilityEUDV.pcf: line 202, column 67
    function valueRoot_93 () : java.lang.Object {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on RowIterator (id=VersionIterator) at GeneralLiabilityEUDV.pcf: line 210, column 47
    function value_158 () : entity.GLExposure[] {
      return exposure.AdditionalVersions.cast(GLExposure)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at GeneralLiabilityEUDV.pcf: line 102, column 33
    function value_27 () : java.util.Date {
      return exposure.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at GeneralLiabilityEUDV.pcf: line 109, column 33
    function value_30 () : java.util.Date {
      return exposure.ExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function value_35 () : entity.PolicyLocation {
      return exposure.LocationWM
    }
    
    // 'value' attribute on PickerCell (id=ClassCode_Cell) at GeneralLiabilityEUDV.pcf: line 137, column 33
    function value_48 () : entity.GLClassCode {
      return exposure.ClassCode
    }
    
    // 'value' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function value_54 () : typekey.SpecialityCode_TDIC {
      return exposure.SpecialityCode_TDIC
    }
    
    // 'value' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function value_63 () : typekey.ClassCode_TDIC {
      return exposure.ClassCode_TDIC
    }
    
    // 'value' attribute on TextCell (id=SpecialityCodeClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 165, column 52
    function value_71 () : java.lang.String {
      return exposure.Description_TDIC
    }
    
    // 'value' attribute on TextCell (id=ClassCodeDescription_Cell) at GeneralLiabilityEUDV.pcf: line 170, column 34
    function value_74 () : java.lang.String {
      return exposure.ClassCode.Classification
    }
    
    // 'value' attribute on TextCell (id=BasisAmount_Cell) at GeneralLiabilityEUDV.pcf: line 179, column 34
    function value_77 () : java.lang.Integer {
      return exposure.BasisAmount
    }
    
    // 'value' attribute on TextCell (id=ClassCodeUnits_Cell) at GeneralLiabilityEUDV.pcf: line 185, column 34
    function value_81 () : java.lang.String {
      return exposure.ClassCode.Basis.Description
    }
    
    // 'value' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function value_85 () : entity.GLTerritory_TDIC {
      return exposure.GLTerritory_TDIC
    }
    
    // 'value' attribute on TextCell (id=glTerritoryCode_Cell) at GeneralLiabilityEUDV.pcf: line 202, column 67
    function value_92 () : java.lang.String {
      return exposure.GLTerritory_TDIC?.TerritoryCode
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function verifyValueRangeIsAllowedType_39 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function verifyValueRangeIsAllowedType_39 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function verifyValueRangeIsAllowedType_39 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function verifyValueRangeIsAllowedType_58 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function verifyValueRangeIsAllowedType_58 ($$arg :  typekey.SpecialityCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function verifyValueRangeIsAllowedType_67 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function verifyValueRangeIsAllowedType_67 ($$arg :  typekey.ClassCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function verifyValueRangeIsAllowedType_89 ($$arg :  entity.GLTerritory_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function verifyValueRangeIsAllowedType_89 ($$arg :  gw.api.database.IQueryBeanResult<entity.GLTerritory_TDIC>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function verifyValueRangeIsAllowedType_89 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Location_Cell) at GeneralLiabilityEUDV.pcf: line 121, column 33
    function verifyValueRange_40 () : void {
      var __valueRangeArg = getPolicyLocationsFilteredByNonSpecificLocationSupport(exposure)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_39(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=SpecialityCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 147, column 57
    function verifyValueRange_59 () : void {
      var __valueRangeArg = getSpecialityCode()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_58(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ClassCodeDropdown_Cell) at GeneralLiabilityEUDV.pcf: line 158, column 52
    function verifyValueRange_68 () : void {
      var __valueRangeArg = exposureClassCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_67(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ComponentTerritory_Cell) at GeneralLiabilityEUDV.pcf: line 194, column 53
    function verifyValueRange_90 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.GLExposureUnitsHelper.getStateComponents(policyPeriod.BaseState.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_89(__valueRangeArg)
    }
    
    property get exposure () : entity.GLExposure {
      return getIteratedValue(1) as entity.GLExposure
    }
    
    property get exposureClassCodes () : List<ClassCode_TDIC> {
      return getVariableValue("exposureClassCodes", 1) as List<ClassCode_TDIC>
    }
    
    property set exposureClassCodes ($arg :  List<ClassCode_TDIC>) {
      setVariableValue("exposureClassCodes", 1, $arg)
    }
    
    
  }
  
  
}