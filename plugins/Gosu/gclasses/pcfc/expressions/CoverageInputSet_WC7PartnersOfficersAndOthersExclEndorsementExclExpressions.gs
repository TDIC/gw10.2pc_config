package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExclExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 46, column 109
    function available_54 () : java.lang.Boolean {
      return exclusionPattern.allowToggle(coverable)
    }
    
    // 'editable' attribute on RowIterator at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 169, column 56
    function editable_26 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 22, column 36
    function initialValue_0 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 26, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 31, column 39
    function initialValue_2 () : WC7PolicyOwnerOfficer[] {
      return theWC7Line.WC7PolicyOwnerOfficers.where( \ owner -> !owner.isExcluded_TDIC)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 36, column 39
    function initialValue_3 () : entity.AccountContact[] {
      return theWC7Line.OwnerOfficerOtherCandidates
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingOwnerOfficerContact) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 83, column 30
    function label_13 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingEntityOwnerContact) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 109, column 30
    function label_20 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 46, column 109
    function label_55 () : java.lang.Object {
      return exclusionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 46, column 109
    function setter_56 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, exclusionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 117, column 34
    function sortBy_14 (entityOwner :  entity.PolicyEntityOwner_TDIC) : java.lang.Object {
      return OwnerOfficer.Type.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 91, column 34
    function sortBy_7 (ownerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return ownerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 176, column 47
    function sortValue_21 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function sortValue_22 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 196, column 51
    function sortValue_23 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 204, column 48
    function sortValue_24 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 208, column 71
    function sortValue_25 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : java.lang.Object {
      return policyOwnerOfficer.IntrinsicType.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 103, column 92
    function toCreateAndAdd_12 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addAllExistingOwnerOfficersToExclusion()
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 129, column 96
    function toCreateAndAdd_19 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addAllExistingPolicyEntityOwnersToExclusion_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 169, column 56
    function toRemove_51 (policyOwnerOfficer :  entity.WC7PolicyOwnerOfficer) : void {
      theWC7Line.removeWC7ExcludedPolicyOwnerOfficer(policyOwnerOfficer)
    }
    
    // 'value' attribute on HiddenInput (id=ExclusionPatternDisplayName_Input) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 40, column 37
    function valueRoot_5 () : java.lang.Object {
      return exclusionPattern
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 88, column 60
    function value_10 () : entity.WC7PolicyOwnerOfficer[] {
      return theWC7Line.WC7PolicyOwnerOfficersSubtractExcluded
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 114, column 61
    function value_17 () : entity.PolicyEntityOwner_TDIC[] {
      return theWC7Line.WC7PolicyEntityOwnersSubtractExcluded_TDIC
    }
    
    // 'value' attribute on HiddenInput (id=ExclusionPatternDisplayName_Input) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 40, column 37
    function value_4 () : java.lang.String {
      return exclusionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 169, column 56
    function value_52 () : entity.WC7PolicyOwnerOfficer[] {
      return theWC7Line.WC7PolicyOwnerOfficers.where( \ owner -> owner.isExcluded_TDIC )
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 103, column 92
    function visible_11 () : java.lang.Boolean {
      return theWC7Line.WC7PolicyOwnerOfficersSubtractExcluded.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 129, column 96
    function visible_18 () : java.lang.Boolean {
      return theWC7Line.WC7PolicyEntityOwnersSubtractExcluded_TDIC.length > 0
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ExclusionInputGroup) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 46, column 109
    function visible_53 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 215, column 101
    function visible_59 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get exclusionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("exclusionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set exclusionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("exclusionPattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherContacts () : entity.AccountContact[] {
      return getVariableValue("otherContacts", 0) as entity.AccountContact[]
    }
    
    property set otherContacts ($arg :  entity.AccountContact[]) {
      setVariableValue("otherContacts", 0, $arg)
    }
    
    property get theWC7Line () : productmodel.WC7Line {
      return getVariableValue("theWC7Line", 0) as productmodel.WC7Line
    }
    
    property set theWC7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    property get unassignedOwnerOfficers () : WC7PolicyOwnerOfficer[] {
      return getVariableValue("unassignedOwnerOfficers", 0) as WC7PolicyOwnerOfficer[]
    }
    
    property set unassignedOwnerOfficers ($arg :  WC7PolicyOwnerOfficer[]) {
      setVariableValue("unassignedOwnerOfficers", 0, $arg)
    }
    
    function addOwnerOfficerForContactOnExclusion(ownerOfficer : WC7PolicyOwnerOfficer) : WC7PolicyOwnerOfficer {
      return theWC7Line.addExcludedOwnerOfficer_TDIC(ownerOfficer)
    }
    
    function addAllOwnerOfficersOnExclusion() : WC7PolicyOwnerOfficer[] {
      var resultOfficers = theWC7Line.addAllExistingOwnerOfficersToExclusion(theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
      return resultOfficers.toTypedArray()
    }
    
    function addOwnerOfficerForContactOnExclusion(aContact : entity.Contact) : WC7ExcludedOwnerOfficer {
      var newOfficer = theWC7Line.addExcludedOwnerOfficer(aContact, theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
      return newOfficer
    }
    /*  OOTB
    function addAllOwnerOfficersOnExclusion() : WC7ExcludedOwnerOfficer[] {
      var resultOfficers = theWC7Line.addAllExistingOwnerOfficersToExclusion(theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
      return resultOfficers.toTypedArray()
    }
    */
    function isOrgTypeCorp() : Boolean {
      return theWC7Line.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_LLC 
          or theWC7Line.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_PRIVATECORP
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedEntityOwner) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 122, column 90
    function label_15 () : java.lang.Object {
      return entityOwner
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedEntityOwner) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 122, column 90
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addExcludedOwnerOfficer_TDIC(entityOwner)
    }
    
    property get entityOwner () : entity.PolicyEntityOwner_TDIC {
      return getIteratedValue(1) as entity.PolicyEntityOwner_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 176, column 47
    function action_27 () : void {
      EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 176, column 47
    function action_dest_28 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyOwnerOfficer, openForEdit)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 196, column 51
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.RelationshipTitle = (__VALUE_TO_SET as typekey.Relationship)
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 204, column 48
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.WC7OwnershipPct = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'filter' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 196, column 51
    function filter_42 (VALUE :  typekey.Relationship, VALUES :  typekey.Relationship[]) : java.lang.Boolean {
      return isOrgTypeCorp() ? Relationship.TF_WC7OWNEROFFICERRELATIONSHIP.TypeKeys.contains(VALUE) : Relationship.TF_TDICWC7PARTNERSHIP.TypeKeys.contains(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function valueRange_35 () : java.lang.Object {
      return theWC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 176, column 47
    function valueRoot_30 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 208, column 71
    function valueRoot_49 () : java.lang.Object {
      return policyOwnerOfficer.IntrinsicType
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 176, column 47
    function value_29 () : java.lang.String {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function value_32 () : typekey.Jurisdiction {
      return policyOwnerOfficer.Jurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=Relationship_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 196, column 51
    function value_39 () : typekey.Relationship {
      return policyOwnerOfficer.RelationshipTitle
    }
    
    // 'value' attribute on TextCell (id=Ownership_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 204, column 48
    function value_44 () : java.lang.Integer {
      return policyOwnerOfficer.WC7OwnershipPct
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 208, column 71
    function value_48 () : java.lang.String {
      return policyOwnerOfficer.IntrinsicType.DisplayName
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 185, column 50
    function verifyValueRange_37 () : void {
      var __valueRangeArg = theWC7Line.WC7Jurisdictions.map(\j -> j.Jurisdiction)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    property get policyOwnerOfficer () : entity.WC7PolicyOwnerOfficer {
      return getIteratedValue(1) as entity.WC7PolicyOwnerOfficer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 96, column 91
    function label_8 () : java.lang.Object {
      return ownerOfficer
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at CoverageInputSet.WC7PartnersOfficersAndOthersExclEndorsementExcl.pcf: line 96, column 91
    function toCreateAndAdd_9 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.addExcludedOwnerOfficer_TDIC(ownerOfficer)
    }
    
    property get ownerOfficer () : entity.WC7PolicyOwnerOfficer {
      return getIteratedValue(1) as entity.WC7PolicyOwnerOfficer
    }
    
    
  }
  
  
}