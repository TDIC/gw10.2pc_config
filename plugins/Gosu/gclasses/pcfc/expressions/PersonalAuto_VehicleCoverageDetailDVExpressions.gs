package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_VehicleCoverageDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PersonalAuto_VehicleCoverageDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_VehicleCoverageDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PersonalAuto_VehicleCoverageDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_10 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_106 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_12 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_14 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_16 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_18 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_2 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_20 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_22 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_24 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_26 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_28 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_30 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_32 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_34 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_36 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_4 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_56 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_58 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_6 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_60 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_62 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_64 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_66 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_68 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_70 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_72 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_74 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_76 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_78 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_8 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_80 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_82 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_84 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_11 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_13 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_15 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_17 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_19 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_21 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_3 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_5 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_7 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_9 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, vehicle, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at PersonalAuto_VehicleCoverageDetailDV.pcf: line 30, column 44
    function mode_108 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_VehicleCoverageDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PersonalAuto_VehicleCoverageDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PersonalAuto_VehicleCoverageDetailDV.pcf: line 17, column 53
    function initialValue_0 () : gw.api.productmodel.CoveragePattern[] {
      return vehicle != null ? vehicle.PolicyLine.Pattern.getCoverageCategoryByPublicId("PAPPhysDamGrp").coveragePatternsForEntity(PersonalVehicle).whereSelectedOrAvailable(vehicle, openForEdit) : null
    }
    
    // 'sortBy' attribute on IteratorSort at PersonalAuto_VehicleCoverageDetailDV.pcf: line 27, column 26
    function sortBy_1 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=PAPhysDamCategoryIterator) at PersonalAuto_VehicleCoverageDetailDV.pcf: line 24, column 59
    function value_109 () : gw.api.productmodel.CoveragePattern[] {
      return physDamGrpCoveragePatterns
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get physDamGrpCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("physDamGrpCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set physDamGrpCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("physDamGrpCoveragePatterns", 0, $arg)
    }
    
    property get vehicle () : PersonalVehicle {
      return getRequireValue("vehicle", 0) as PersonalVehicle
    }
    
    property set vehicle ($arg :  PersonalVehicle) {
      setRequireValue("vehicle", 0, $arg)
    }
    
    
  }
  
  
}