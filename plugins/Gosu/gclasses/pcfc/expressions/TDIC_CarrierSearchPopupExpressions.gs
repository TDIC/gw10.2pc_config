package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_CarrierSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_CarrierSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_CarrierSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickValue' attribute on RowIterator at TDIC_CarrierSearchPopup.pcf: line 54, column 94
    function pickValue_10 () : entity.GLCarrierNames_TDIC {
      return carrierName
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at TDIC_CarrierSearchPopup.pcf: line 61, column 33
    function valueRoot_8 () : java.lang.Object {
      return carrierName
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at TDIC_CarrierSearchPopup.pcf: line 61, column 33
    function value_7 () : java.lang.String {
      return carrierName.CarrierName
    }
    
    property get carrierName () : entity.GLCarrierNames_TDIC {
      return getIteratedValue(2) as entity.GLCarrierNames_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_CarrierSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends TDIC_CarrierSearchPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at TDIC_CarrierSearchPopup.pcf: line 37, column 49
    function def_onEnter_4 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at TDIC_CarrierSearchPopup.pcf: line 37, column 49
    function def_refreshVariables_5 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at TDIC_CarrierSearchPopup.pcf: line 33, column 53
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CarrierName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel at TDIC_CarrierSearchPopup.pcf: line 23, column 89
    function searchCriteria_13 () : tdic.pc.config.gl.GLCarrierSearchCriteria_TDIC {
      return new tdic.pc.config.gl.GLCarrierSearchCriteria_TDIC()
    }
    
    // 'search' attribute on SearchPanel at TDIC_CarrierSearchPopup.pcf: line 23, column 89
    function search_12 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at TDIC_CarrierSearchPopup.pcf: line 61, column 33
    function sortValue_6 (carrierName :  entity.GLCarrierNames_TDIC) : java.lang.Object {
      return carrierName.CarrierName
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at TDIC_CarrierSearchPopup.pcf: line 33, column 53
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at TDIC_CarrierSearchPopup.pcf: line 33, column 53
    function value_0 () : java.lang.String {
      return searchCriteria.CarrierName
    }
    
    // 'value' attribute on RowIterator at TDIC_CarrierSearchPopup.pcf: line 54, column 94
    function value_11 () : gw.api.database.IQueryBeanResult<entity.GLCarrierNames_TDIC> {
      return carrierNames
    }
    
    property get carrierNames () : gw.api.database.IQueryBeanResult<GLCarrierNames_TDIC> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<GLCarrierNames_TDIC>
    }
    
    property get searchCriteria () : tdic.pc.config.gl.GLCarrierSearchCriteria_TDIC {
      return getCriteriaValue(1) as tdic.pc.config.gl.GLCarrierSearchCriteria_TDIC
    }
    
    property set searchCriteria ($arg :  tdic.pc.config.gl.GLCarrierSearchCriteria_TDIC) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_CarrierSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_CarrierSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.TDIC_CarrierSearchPopup {
      return super.CurrentLocation as pcf.TDIC_CarrierSearchPopup
    }
    
    property get glLine () : GLLine {
      return getVariableValue("glLine", 0) as GLLine
    }
    
    property set glLine ($arg :  GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    
  }
  
  
}