package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ReinsuranceSearchResultsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_ReinsuranceSearchResultsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 85, column 84
    function action_42 () : void {
      PolicyFileForward.go(reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber, retrieveAsOfDate)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 85, column 84
    function action_dest_43 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber, retrieveAsOfDate)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 85, column 84
    function valueRoot_45 () : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=LocationNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 103, column 38
    function valueRoot_57 () : java.lang.Object {
      return reinsuranceGroupEntry
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 85, column 84
    function value_44 () : java.lang.String {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=PrimaryNamedInsured_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 90, column 59
    function value_47 () : entity.PolicyPriNamedInsured {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 94, column 83
    function value_50 () : java.util.Date {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 98, column 81
    function value_53 () : java.util.Date {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodEnd
    }
    
    // 'value' attribute on TextCell (id=LocationNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 103, column 38
    function value_56 () : Integer {
      return reinsuranceGroupEntry.LocationNum
    }
    
    // 'value' attribute on TextCell (id=AddressLines1and2_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 107, column 175
    function value_59 () : java.lang.String {
      return reinsuranceGroupEntry.AddressLine1 +"\n"+ (reinsuranceGroupEntry.AddressLine2 == null ? "" : reinsuranceGroupEntry.AddressLine2)
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 111, column 53
    function value_61 () : java.lang.String {
      return reinsuranceGroupEntry.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 116, column 44
    function value_64 () : typekey.State {
      return reinsuranceGroupEntry.State
    }
    
    // 'value' attribute on TextCell (id=PostalCode_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 120, column 59
    function value_67 () : java.lang.String {
      return reinsuranceGroupEntry.PostalCode
    }
    
    // 'value' attribute on TextCell (id=YearBuilt_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 125, column 38
    function value_70 () : Integer {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBuildingYearBuilt(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null
    }
    
    // 'value' attribute on TextCell (id=ConstructionType_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 129, column 259
    function value_72 () : java.lang.String {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getConstructionType(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null
    }
    
    // 'value' attribute on TextCell (id=PropertyValue_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 135, column 51
    function value_74 () : java.math.BigDecimal {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBOPLinePropertyValue(reinsuranceGroupEntry) : null
    }
    
    // 'value' attribute on TextCell (id=NumberOfEmployees_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 141, column 59
    function value_76 () : Integer {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalNumEmployeesForPolicyLocation_TDIC(reinsuranceGroupEntry) : null
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 147, column 59
    function value_79 () : Integer {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalBasisForPolicyLocation_TDIC(reinsuranceGroupEntry) : null
    }
    
    // 'visible' attribute on TextCell (id=NumberOfEmployees_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 141, column 59
    function visible_77 () : java.lang.Boolean {
      return isBasisAndNumberOfEmpVisible()
    }
    
    property get reinsuranceGroupEntry () : entity.PolicyLocation {
      return getIteratedValue(1) as entity.PolicyLocation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TDIC_ReinsuranceSearchResultsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ReinsuranceSearchResultsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (reinsuranceSearchResult :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at TDIC_ReinsuranceSearchResultsPopup.pcf: line 17, column 20
    function initialValue_0 () : Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_ReinsuranceSearchResultsPopup.pcf: line 67, column 30
    function sortBy_21 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_ReinsuranceSearchResultsPopup.pcf: line 70, column 30
    function sortBy_22 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBuildingYearBuilt(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum).intValue()
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_ReinsuranceSearchResultsPopup.pcf: line 73, column 30
    function sortBy_23 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodStart
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_ReinsuranceSearchResultsPopup.pcf: line 76, column 30
    function sortBy_24 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PrimaryNamedInsured
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_ReinsuranceSearchResultsPopup.pcf: line 79, column 30
    function sortBy_25 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBOPLinePropertyValue(reinsuranceGroupEntry).DisplayValue
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 85, column 84
    function sortValue_26 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=PrimaryNamedInsured_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 90, column 59
    function sortValue_27 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PrimaryNamedInsured
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 94, column 83
    function sortValue_28 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodStart
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 98, column 81
    function sortValue_29 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodEnd
    }
    
    // 'value' attribute on TextCell (id=LocationNumber_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 103, column 38
    function sortValue_30 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.LocationNum
    }
    
    // 'value' attribute on TextCell (id=AddressLines1and2_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 107, column 175
    function sortValue_31 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AddressLine1 +"\n"+ (reinsuranceGroupEntry.AddressLine2 == null ? "" : reinsuranceGroupEntry.AddressLine2)
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 111, column 53
    function sortValue_32 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 116, column 44
    function sortValue_33 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.State
    }
    
    // 'value' attribute on TextCell (id=PostalCode_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 120, column 59
    function sortValue_34 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.PostalCode
    }
    
    // 'value' attribute on TextCell (id=YearBuilt_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 125, column 38
    function sortValue_35 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBuildingYearBuilt(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null
    }
    
    // 'value' attribute on TextCell (id=ConstructionType_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 129, column 259
    function sortValue_36 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getConstructionType(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null
    }
    
    // 'value' attribute on TextCell (id=PropertyValue_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 135, column 51
    function sortValue_37 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBOPLinePropertyValue(reinsuranceGroupEntry) : null
    }
    
    // 'value' attribute on TextCell (id=NumberOfEmployees_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 141, column 59
    function sortValue_38 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalNumEmployeesForPolicyLocation_TDIC(reinsuranceGroupEntry) : null
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 147, column 59
    function sortValue_40 (reinsuranceGroupEntry :  entity.PolicyLocation) : java.lang.Object {
      return reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalBasisForPolicyLocation_TDIC(reinsuranceGroupEntry) : null
    }
    
    // 'value' attribute on TextInput (id=Location_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 24, column 66
    function valueRoot_2 () : java.lang.Object {
      return reinsuranceSearchResult
    }
    
    // 'value' attribute on TextInput (id=Matches_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 35, column 34
    function valueRoot_7 () : java.lang.Object {
      return reinsuranceSearchResult.ReinsuranceGroup
    }
    
    // 'value' attribute on TextInput (id=Location_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 24, column 66
    function value_1 () : java.lang.String {
      return reinsuranceSearchResult.RelatedAddressLine1
    }
    
    // 'value' attribute on TextInput (id=TotalEmployees_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 41, column 75
    function value_10 () : Integer {
      return reinsuranceSearchResult.TotalEmployees_TDIC
    }
    
    // 'value' attribute on TextInput (id=TotalBasis_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 47, column 71
    function value_15 () : Integer {
      return reinsuranceSearchResult.TotalBasis_TDIC
    }
    
    // 'value' attribute on TextInput (id=TotalValue_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 53, column 47
    function value_19 () : java.math.BigDecimal {
      return reinsuranceSearchResult.ReinsuranceGroup.hasMatch(\elt -> elt.AssociatedPolicyPeriod?.BOPLineExists) ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getTotalBOPLinePropertyValue(reinsuranceSearchResult.ReinsuranceGroup) : null
    }
    
    // 'value' attribute on TextInput (id=ControlAddress_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 28, column 126
    function value_4 () : java.lang.String {
      return (reinsuranceSearchResult.ControlAddress != null)? reinsuranceSearchResult.ControlAddress : ""
    }
    
    // 'value' attribute on TextInput (id=Matches_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 35, column 34
    function value_6 () : Integer {
      return reinsuranceSearchResult.ReinsuranceGroup.Count
    }
    
    // 'value' attribute on RowIterator (id=ReinsuranceGroupLV) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 64, column 75
    function value_82 () : gw.util.IOrderedList<entity.PolicyLocation> {
      return reinsuranceSearchResult.ReinsuranceGroup.orderBy( \ elt -> elt.ReinsuranceSearchTag_TDIC)
    }
    
    // 'visible' attribute on TextInput (id=TotalBasis_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 47, column 71
    function visible_14 () : java.lang.Boolean {
      return reinsuranceSearchResult.TotalBasis_TDIC > 0
    }
    
    // 'visible' attribute on TextCell (id=NumberOfEmployees_Cell) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 141, column 59
    function visible_39 () : java.lang.Boolean {
      return isBasisAndNumberOfEmpVisible()
    }
    
    // 'visible' attribute on TextInput (id=TotalEmployees_Input) at TDIC_ReinsuranceSearchResultsPopup.pcf: line 41, column 75
    function visible_9 () : java.lang.Boolean {
      return reinsuranceSearchResult.TotalEmployees_TDIC > 0
    }
    
    override property get CurrentLocation () : pcf.TDIC_ReinsuranceSearchResultsPopup {
      return super.CurrentLocation as pcf.TDIC_ReinsuranceSearchResultsPopup
    }
    
    property get reinsuranceSearchResult () : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult {
      return getVariableValue("reinsuranceSearchResult", 0) as tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult
    }
    
    property set reinsuranceSearchResult ($arg :  tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult) {
      setVariableValue("reinsuranceSearchResult", 0, $arg)
    }
    
    property get retrieveAsOfDate () : Date {
      return getVariableValue("retrieveAsOfDate", 0) as Date
    }
    
    property set retrieveAsOfDate ($arg :  Date) {
      setVariableValue("retrieveAsOfDate", 0, $arg)
    }
    
    function isBasisAndNumberOfEmpVisible() : boolean {
      return reinsuranceSearchResult.ReinsuranceGroup.hasMatch(\polLoc -> polLoc.AssociatedPolicyPeriod.WC7LineExists)
    }
    
    
  }
  
  
}