package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNoteDVExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NewNoteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNoteDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewNoteDV.pcf: line 39, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      newNoteHelper.Note.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      newNoteHelper.Note.Level = (__VALUE_TO_SET as gw.api.domain.linkedobject.LinkedObjectContainer)
    }
    
    // 'value' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      newNoteHelper.Note.Topic    = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityLevel_Input) at NewNoteDV.pcf: line 58, column 47
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      newNoteHelper.Note.SecurityType = (__VALUE_TO_SET as typekey.NoteSecurityType)
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at NewNoteDV.pcf: line 65, column 42
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      newNoteHelper.Note.Body = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function editable_20 () : java.lang.Boolean {
      return newNoteHelper.RelatedToEditable
    }
    
    // 'filter' attribute on TypeKeyInput (id=SecurityLevel_Input) at NewNoteDV.pcf: line 58, column 47
    function filter_35 (VALUE :  typekey.NoteSecurityType, VALUES :  typekey.NoteSecurityType[]) : java.lang.Boolean {
      return newNoteHelper.Note.hasCreatePermission(VALUE)
    }
    
    // 'initialValue' attribute on Variable at NewNoteDV.pcf: line 16, column 23
    function initialValue_0 () : Boolean {
      return setDefaultSecurityLevel()
    }
    
    // 'optionLabel' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function optionLabel_25 (VALUE :  gw.api.domain.linkedobject.LinkedObjectContainer) : java.lang.String {
      return newNoteHelper.Note.getLevelDisplayString(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function valueRange_26 () : java.lang.Object {
      return newNoteHelper.LevelsValueRange
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function valueRange_5 () : java.lang.Object {
      return getNoteTopicRange()
    }
    
    // 'value' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function valueRoot_4 () : java.lang.Object {
      return newNoteHelper.Note
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewNoteDV.pcf: line 39, column 45
    function value_16 () : java.lang.String {
      return newNoteHelper.Note.Subject
    }
    
    // 'value' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function value_2 () : typekey.NoteTopicType {
      return newNoteHelper.Note.Topic   
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function value_22 () : gw.api.domain.linkedobject.LinkedObjectContainer {
      return newNoteHelper.Note.Level
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityLevel_Input) at NewNoteDV.pcf: line 58, column 47
    function value_32 () : typekey.NoteSecurityType {
      return newNoteHelper.Note.SecurityType
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at NewNoteDV.pcf: line 65, column 42
    function value_37 () : java.lang.String {
      return newNoteHelper.Note.Body
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function verifyValueRangeIsAllowedType_27 ($$arg :  gw.api.domain.linkedobject.LinkedObjectContainer[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function verifyValueRangeIsAllowedType_27 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function verifyValueRangeIsAllowedType_6 ($$arg :  typekey.NoteTopicType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function verifyValueRange_28 () : void {
      var __valueRangeArg = newNoteHelper.LevelsValueRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_27(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function verifyValueRange_7 () : void {
      var __valueRangeArg = getNoteTopicRange()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=Topic_Input) at NewNoteDV.pcf: line 26, column 76
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.BOPLineExists || policyPeriod.GLLineExists
    }
    
    // 'visible' attribute on TypeKeyInput (id=TopicWC_Input) at NewNoteDV.pcf: line 34, column 72
    function visible_10 () : java.lang.Boolean {
      return policyPeriod.WC7Line!= null || policyPeriod == null
    }
    
    // 'visible' attribute on RangeInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 50, column 107
    function visible_21 () : java.lang.Boolean {
      return (!(newNoteHelper.Job typeis Submission) or (newNoteHelper.Job as Submission).FullMode)
    }
    
    property get newNoteHelper () : gw.note.NewNoteHelper {
      return getRequireValue("newNoteHelper", 0) as gw.note.NewNoteHelper
    }
    
    property set newNoteHelper ($arg :  gw.note.NewNoteHelper) {
      setRequireValue("newNoteHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get securityLevel () : Boolean {
      return getVariableValue("securityLevel", 0) as Boolean
    }
    
    property set securityLevel ($arg :  Boolean) {
      setVariableValue("securityLevel", 0, $arg)
    }
    
    /**
     * create by: ChitraK
     * @description: Default SEcurity Level
     * @create time: 7:33 PM 8/8/2019
     * @return: 
     */
    
    function setDefaultSecurityLevel():Boolean{
      newNoteHelper.Note.SecurityType = NoteSecurityType.TC_UNRESTRICTED
      return true
    }
    /**
     * create by: ChitraK
     * @description: Set Note Topic Values
     * @create time: 7:33 PM 8/8/2019
     * @return: 
     */
    
    function getNoteTopicRange() : typekey.NoteTopicType[] {
      if (policyPeriod.WC7LineExists) {
        return typekey.NoteTopicType.TF_WC_NOTETOPICTYPES.getTypeKeys().toTypedArray()
      }
      if(policyPeriod.BOPLineExists || policyPeriod.GLLineExists) {
        return typekey.NoteTopicType.TF_BOP_GL_NOTETOPICTYPES.getTypeKeys().toTypedArray()
        // var nl = {NoteTopicType.TC_GENERAL, NoteTopicType.TC_BILLINGPAYMENT_TDIC, NoteTopicType.TC_CANCELLATIONS_TDIC, NoteTopicType.TC_COVERAGE_TDIC,
        //    NoteTopicType.TC_END_REQ_DETAILS_TDIC, NoteTopicType.TC_OTHER_TDIC, NoteTopicType.TC_UNDERWRITING_TDIC, NoteTopicType.TC_LOSSCONTROL, NoteTopicType.TC_RISK}
      }
      return typekey.NoteTopicType.getTypeKeys(false).toTypedArray()
    }
    
    
  }
  
  
}