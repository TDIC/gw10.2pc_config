package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPLocationBuildingsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPLocationBuildingsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 19, column 23
    function initialValue_0 () : BOPLine {
      return policyPeriod.BOPLine
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 24, column 49
    function initialValue_1 () : gw.api.productmodel.QuestionSet[] {
      return policyPeriod.Policy.Product.getQuestionSetsByType(QuestionSetType.TC_UNDERWRITING)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 29, column 25
    function initialValue_2 () : QuoteType {
      return setInitialQuoteType()
    }
    
    property get bopLine () : BOPLine {
      return getVariableValue("bopLine", 0) as BOPLine
    }
    
    property set bopLine ($arg :  BOPLine) {
      setVariableValue("bopLine", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getVariableValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    property get underwritingQuestionSets () : gw.api.productmodel.QuestionSet[] {
      return getVariableValue("underwritingQuestionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set underwritingQuestionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setVariableValue("underwritingQuestionSets", 0, $arg)
    }
    
    function setModValues(b:BOPBuilding):Modifier[]{
      if(b!=null) {
       return b.BOPBuildingModifiers_TDIC
      }
      return new Modifier[]{}
    }
    
    function setNSRValues(mod:Modifier[]):Modifier[]{
      if(mod!=null) {
       return mod.where(\ modi -> not modi.ScheduleRate )
      }
      return new Modifier[]{}
    }
    
    function setSRValues(mod:Modifier[]):Modifier[]{
      if(mod!=null) return mod.where(\ modi -> modi.ScheduleRate )
      return new Modifier[]{}
    }
    
    function setInitialQuoteType():QuoteType{
      if(policyPeriod.Job typeis Submission){
        return (policyPeriod.Job as Submission).QuoteType
      }
      return null
    }
    
    function isIRPMRatingInputsVisible(): boolean {
      if (not User.util.CurrentUser.ExternalUser) {
        return (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic)
      }
      return false
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewInput at BOPLocationBuildingsPanelSet.pcf: line 200, column 59
    function available_63 () : java.lang.Boolean {
      return scheduleRates != null
    }
    
    // 'def' attribute on ListViewInput at BOPLocationBuildingsPanelSet.pcf: line 200, column 59
    function def_onEnter_65 (def :  pcf.ScheduleRateLV) : void {
      def.onEnter(scheduleRate)
    }
    
    // 'def' attribute on ListViewInput at BOPLocationBuildingsPanelSet.pcf: line 200, column 59
    function def_refreshVariables_66 (def :  pcf.ScheduleRateLV) : void {
      def.refreshVariables(scheduleRate)
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at BOPLocationBuildingsPanelSet.pcf: line 194, column 61
    function valueRoot_61 () : java.lang.Object {
      return scheduleRate
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at BOPLocationBuildingsPanelSet.pcf: line 194, column 61
    function value_60 () : java.lang.String {
      return scheduleRate.DisplayName
    }
    
    property get scheduleRate () : entity.Modifier {
      return getIteratedValue(3) as entity.Modifier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function actionAvailable_15 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'action' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function action_13 () : void {
      BOPBuildingPopup.push(bopLine, bopLocation, building, openForEdit, true, jobWizardHelper)
    }
    
    // 'action' attribute on TextCell (id=DescriptionEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 94, column 40
    function action_19 () : void {
      BOPBuildingPopup.push(bopLine, bopLocation, building, openForEdit, true, jobWizardHelper)
    }
    
    // 'action' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function action_dest_14 () : pcf.api.Destination {
      return pcf.BOPBuildingPopup.createDestination(bopLine, bopLocation, building, openForEdit, true, jobWizardHelper)
    }
    
    // 'action' attribute on TextCell (id=DescriptionEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 94, column 40
    function action_dest_20 () : pcf.api.Destination {
      return pcf.BOPBuildingPopup.createDestination(bopLine, bopLocation, building, openForEdit, true, jobWizardHelper)
    }
    
    // 'value' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function valueRoot_17 () : java.lang.Object {
      return building.Building
    }
    
    // 'value' attribute on TypeKeyCell (id=CoverageCurrency_Cell) at BOPLocationBuildingsPanelSet.pcf: line 100, column 81
    function valueRoot_25 () : java.lang.Object {
      return building
    }
    
    // 'value' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function value_16 () : java.lang.Integer {
      return building.Building.BuildingNum
    }
    
    // 'value' attribute on TextCell (id=DescriptionEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 94, column 40
    function value_21 () : java.lang.String {
      return building.Building.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=CoverageCurrency_Cell) at BOPLocationBuildingsPanelSet.pcf: line 100, column 81
    function value_24 () : typekey.Currency {
      return building.PreferredCoverageCurrency
    }
    
    // 'visible' attribute on TypeKeyCell (id=CoverageCurrency_Cell) at BOPLocationBuildingsPanelSet.pcf: line 100, column 81
    function visible_26 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get building () : entity.BOPBuilding {
      return getIteratedValue(3) as entity.BOPBuilding
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanel2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPLocationBuildingsPanelSet.pcf: line 109, column 51
    function available_31 () : java.lang.Boolean {
      return selectedBuilding != null
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPLocationBuildingsPanelSet.pcf: line 109, column 51
    function def_onEnter_33 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(selectedBuilding, false, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 116, column 37
    function def_onEnter_35 (def :  pcf.BOPBuilding_DetailsDV) : void {
      def.onEnter(selectedBuilding, false,bopLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 119, column 227
    function def_onEnter_38 (def :  pcf.MortgageeDetails_TDICDV) : void {
      def.onEnter(selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 122, column 150
    function def_onEnter_41 (def :  pcf.LossPayeeDetails_TDICDV) : void {
      def.onEnter(selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 135, column 53
    function def_onEnter_44 (def :  pcf.BOPLinePropertyCoverageDV) : void {
      def.onEnter(bopLine,selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 143, column 37
    function def_onEnter_47 (def :  pcf.BOPLineLiabilityCoverageDV) : void {
      def.onEnter(bopLine,selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 146, column 187
    function def_onEnter_50 (def :  pcf.TDIC_BuildingAdditionalInsuredsDV) : void {
      def.onEnter(selectedBuilding, false, true, false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 154, column 37
    function def_onEnter_53 (def :  pcf.BOPAdditionalCoveragesDV) : void {
      def.onEnter(bopLine,selectedBuilding,quoteType,false)
    }
    
    // 'def' attribute on InputSetRef at BOPLocationBuildingsPanelSet.pcf: line 167, column 43
    function def_onEnter_56 (def :  pcf.WCModifiersInputSet) : void {
      def.onEnter(nonScheduleRates.toList(), policyPeriod)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet1) at BOPLocationBuildingsPanelSet.pcf: line 227, column 38
    function def_onEnter_71 (def :  pcf.QuestionSets_TDICDV) : void {
      def.onEnter(underwritingQuestionSets.where(\questionSet -> questionSet.CodeIdentifier == "BOPBuildingSupp"),bopLocation.PolicyLocation,\ -> bopLocation.PolicyLocation.BPPTotalLimit_TDIC,false)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPLocationBuildingsPanelSet.pcf: line 109, column 51
    function def_refreshVariables_34 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(selectedBuilding, false, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 116, column 37
    function def_refreshVariables_36 (def :  pcf.BOPBuilding_DetailsDV) : void {
      def.refreshVariables(selectedBuilding, false,bopLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 119, column 227
    function def_refreshVariables_39 (def :  pcf.MortgageeDetails_TDICDV) : void {
      def.refreshVariables(selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 122, column 150
    function def_refreshVariables_42 (def :  pcf.LossPayeeDetails_TDICDV) : void {
      def.refreshVariables(selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 135, column 53
    function def_refreshVariables_45 (def :  pcf.BOPLinePropertyCoverageDV) : void {
      def.refreshVariables(bopLine,selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 143, column 37
    function def_refreshVariables_48 (def :  pcf.BOPLineLiabilityCoverageDV) : void {
      def.refreshVariables(bopLine,selectedBuilding,false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 146, column 187
    function def_refreshVariables_51 (def :  pcf.TDIC_BuildingAdditionalInsuredsDV) : void {
      def.refreshVariables(selectedBuilding, false, true, false)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 154, column 37
    function def_refreshVariables_54 (def :  pcf.BOPAdditionalCoveragesDV) : void {
      def.refreshVariables(bopLine,selectedBuilding,quoteType,false)
    }
    
    // 'def' attribute on InputSetRef at BOPLocationBuildingsPanelSet.pcf: line 167, column 43
    function def_refreshVariables_57 (def :  pcf.WCModifiersInputSet) : void {
      def.refreshVariables(nonScheduleRates.toList(), policyPeriod)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet1) at BOPLocationBuildingsPanelSet.pcf: line 227, column 38
    function def_refreshVariables_72 (def :  pcf.QuestionSets_TDICDV) : void {
      def.refreshVariables(underwritingQuestionSets.where(\questionSet -> questionSet.CodeIdentifier == "BOPBuildingSupp"),bopLocation.PolicyLocation,\ -> bopLocation.PolicyLocation.BPPTotalLimit_TDIC,false)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 50, column 34
    function initialValue_5 () : Modifier[] {
      return setModValues(selectedBuilding)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 55, column 41
    function initialValue_6 () : entity.Modifier[] {
      return setNSRValues(modifiers)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationBuildingsPanelSet.pcf: line 60, column 41
    function initialValue_7 () : entity.Modifier[] {
      return setSRValues(modifiers)
    }
    
    // 'onSelect' attribute on Card (id=Modifiers) at BOPLocationBuildingsPanelSet.pcf: line 160, column 95
    function onSelect_70 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncModifiers( {selectedBuilding}, jobWizardHelper)
    }
    
    // 'pickLocation' attribute on RowIterator at BOPLocationBuildingsPanelSet.pcf: line 78, column 52
    function pickLocation_28 () : void {
      BOPBuildingPopup.push(bopLine, bopLocation, null, openForEdit, true, jobWizardHelper)
    }
    
    // 'sortBy' attribute on IteratorSort at BOPLocationBuildingsPanelSet.pcf: line 188, column 42
    function sortBy_59 (scheduleRate :  entity.Modifier) : java.lang.Object {
      return scheduleRate.Pattern.Priority
    }
    
    // 'value' attribute on TextCell (id=DescriptionEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 94, column 40
    function sortValue_10 (building :  entity.BOPBuilding) : java.lang.Object {
      return building.Building.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=CoverageCurrency_Cell) at BOPLocationBuildingsPanelSet.pcf: line 100, column 81
    function sortValue_11 (building :  entity.BOPBuilding) : java.lang.Object {
      return building.PreferredCoverageCurrency
    }
    
    // 'value' attribute on TextCell (id=BuildingNumEdit_Cell) at BOPLocationBuildingsPanelSet.pcf: line 88, column 54
    function sortValue_9 (building :  entity.BOPBuilding) : java.lang.Object {
      return building.Building.BuildingNum
    }
    
    // 'toRemove' attribute on RowIterator at BOPLocationBuildingsPanelSet.pcf: line 78, column 52
    function toRemove_29 (building :  entity.BOPBuilding) : void {
      bopLocation.removeFromBuildings(building)
    }
    
    // 'value' attribute on RowIterator at BOPLocationBuildingsPanelSet.pcf: line 78, column 52
    function value_30 () : entity.BOPBuilding[] {
      return bopLocation.Buildings
    }
    
    // 'value' attribute on InputIterator at BOPLocationBuildingsPanelSet.pcf: line 185, column 55
    function value_67 () : entity.Modifier[] {
      return scheduleRates
    }
    
    // 'visible' attribute on TypeKeyCell (id=CoverageCurrency_Cell) at BOPLocationBuildingsPanelSet.pcf: line 100, column 81
    function visible_12 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 119, column 227
    function visible_37 () : java.lang.Boolean {
      return (policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" || policyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") and !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 122, column 150
    function visible_40 () : java.lang.Boolean {
      return policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 135, column 53
    function visible_43 () : java.lang.Boolean {
      return selectedBuilding!=null
    }
    
    // 'visible' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 146, column 187
    function visible_49 () : java.lang.Boolean {
      return (selectedBuilding.BOPDentalGenLiabilityCov_TDICExists || selectedBuilding.BOPBuildingOwnersLiabCov_TDICExists) && !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on Label at BOPLocationBuildingsPanelSet.pcf: line 180, column 58
    function visible_58 () : java.lang.Boolean {
      return scheduleRates.IsEmpty
    }
    
    // 'visible' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 173, column 129
    function visible_68 () : java.lang.Boolean {
      return isIRPMRatingInputsVisible() and scheduleRates.HasElements and not (quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on Card (id=Modifiers) at BOPLocationBuildingsPanelSet.pcf: line 160, column 95
    function visible_69 () : java.lang.Boolean {
      return bopLine.Branch.CanViewModifiers && selectedBuilding != null
    }
    
    // 'visible' attribute on Card (id=uwinformation) at BOPLocationBuildingsPanelSet.pcf: line 223, column 180
    function visible_73 () : java.lang.Boolean {
      return (quoteType != QuoteType.TC_QUICK) /*&& !(bopLine.Branch.LegacyPolicyNumber_TDIC!=null || bopLine.Branch.BasedOn.LegacyPolicyNumber_TDIC!=null)*/
    }
    
    // 'addVisible' attribute on IteratorButtons at BOPLocationBuildingsPanelSet.pcf: line 66, column 39
    function visible_8 () : java.lang.Boolean {
      return bopLocation.Buildings.Count < 1
    }
    
    property get modifiers () : Modifier[] {
      return getVariableValue("modifiers", 2) as Modifier[]
    }
    
    property set modifiers ($arg :  Modifier[]) {
      setVariableValue("modifiers", 2, $arg)
    }
    
    property get nonScheduleRates () : entity.Modifier[] {
      return getVariableValue("nonScheduleRates", 2) as entity.Modifier[]
    }
    
    property set nonScheduleRates ($arg :  entity.Modifier[]) {
      setVariableValue("nonScheduleRates", 2, $arg)
    }
    
    property get scheduleRates () : entity.Modifier[] {
      return getVariableValue("scheduleRates", 2) as entity.Modifier[]
    }
    
    property set scheduleRates ($arg :  entity.Modifier[]) {
      setVariableValue("scheduleRates", 2, $arg)
    }
    
    property get selectedBuilding () : BOPBuilding {
      return getCurrentSelection(2) as BOPBuilding
    }
    
    property set selectedBuilding ($arg :  BOPBuilding) {
      setCurrentSelection(2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationBuildingsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends BOPLocationBuildingsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 34, column 39
    function def_onEnter_3 (def :  pcf.BOPLocationsLV) : void {
      def.onEnter(bopLine)
    }
    
    // 'def' attribute on PanelRef at BOPLocationBuildingsPanelSet.pcf: line 34, column 39
    function def_refreshVariables_4 (def :  pcf.BOPLocationsLV) : void {
      def.refreshVariables(bopLine)
    }
    
    property get bopLocation () : BOPLocation {
      return getCurrentSelection(1) as BOPLocation
    }
    
    property set bopLocation ($arg :  BOPLocation) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}