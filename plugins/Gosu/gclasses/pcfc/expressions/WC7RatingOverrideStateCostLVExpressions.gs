package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideStateCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RatingOverrideStateCostLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideStateCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7RatingOverrideStateCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideStateCostLV.pcf: line 137, column 68
    function def_onEnter_6 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideStateCostLV.pcf: line 137, column 68
    function def_refreshVariables_7 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideStateCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7RatingOverrideStateCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideStateCostLV.pcf: line 123, column 68
    function def_onEnter_1 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.onEnter(aggCost, null)
    }
    
    // 'def' attribute on RowSetRef at WC7RatingOverrideStateCostLV.pcf: line 123, column 68
    function def_refreshVariables_2 (def :  pcf.WC7RatingOverrideCostDetailAggRowSet) : void {
      def.refreshVariables(aggCost, null)
    }
    
    property get aggCost () : WC7JurisdictionCost {
      return getIteratedValue(1) as WC7JurisdictionCost
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/ratingoverride/WC7RatingOverrideStateCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RatingOverrideStateCostLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RatingOverrideStateCostLV.pcf: line 121, column 24
    function sortBy_0 (aggCost :  WC7JurisdictionCost) : java.lang.Object {
      return aggCost.CalcOrder
    }
    
    // 'value' attribute on RowIterator (id=f400t500) at WC7RatingOverrideStateCostLV.pcf: line 118, column 36
    function value_3 () : entity.WC7Cost[] {
      return stateCosts.byCalcOrder( 401, 500 ).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=gt500) at WC7RatingOverrideStateCostLV.pcf: line 132, column 36
    function value_8 () : entity.WC7Cost[] {
      return stateCosts.byCalcOrder( 501, 1000000 ).toTypedArray()
    }
    
    // 'type' attribute on RowIterator (id=f400t500) at WC7RatingOverrideStateCostLV.pcf: line 118, column 36
    function verifyIteratorType_4 () : void {
      var entry : entity.WC7Cost = null
      var typedEntry : WC7JurisdictionCost
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as WC7JurisdictionCost
    }
    
    // 'type' attribute on RowIterator (id=gt500) at WC7RatingOverrideStateCostLV.pcf: line 132, column 36
    function verifyIteratorType_9 () : void {
      var entry : entity.WC7Cost = null
      var typedEntry : WC7JurisdictionCost
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as WC7JurisdictionCost
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get stateCosts () : java.util.Set<WC7Cost> {
      return getRequireValue("stateCosts", 0) as java.util.Set<WC7Cost>
    }
    
    property set stateCosts ($arg :  java.util.Set<WC7Cost>) {
      setRequireValue("stateCosts", 0, $arg)
    }
    
    
  }
  
  
}