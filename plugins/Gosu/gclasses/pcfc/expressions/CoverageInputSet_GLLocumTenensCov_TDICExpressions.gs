package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLLocumTenensCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLLocumTenensCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLLocumTenensCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 33, column 99
    function available_22 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 33, column 99
    function label_23 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 33, column 99
    function setter_24 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 60, column 56
    function sortValue_3 (scheduledItem :  entity.GLLocumTenensSched_TDIC) : java.lang.Object {
      return scheduledItem.LTEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 68, column 57
    function sortValue_4 (scheduledItem :  entity.GLLocumTenensSched_TDIC) : java.lang.Object {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 75, column 52
    function sortValue_5 (scheduledItem :  entity.GLLocumTenensSched_TDIC) : java.lang.Object {
      return scheduledItem.DentistName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 53, column 58
    function toCreateAndAdd_18 () : entity.GLLocumTenensSched_TDIC {
      return glLine.createAndAddLocumTenesSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 53, column 58
    function toRemove_19 (scheduledItem :  entity.GLLocumTenensSched_TDIC) : void {
      glLine.removeFromGLLocumTenensSched_TDIC(scheduledItem); 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 53, column 58
    function value_20 () : entity.GLLocumTenensSched_TDIC[] {
      return glLine.GLLocumTenensSched_TDIC
    }
    
    // 'addVisible' attribute on IteratorButtons at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 42, column 37
    function visible_2 () : java.lang.Boolean {
      return glLine.GLLocumTenensSched_TDIC.Count < 2
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 33, column 99
    function visible_21 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 82, column 43
    function visible_27 () : java.lang.Boolean {
      return isLocumTeensCovVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isLocumTeensCovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLLocumTenensCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status        == PolicyPeriodStatus.TC_QUOTED) or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLLocumTenensCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    function validateEndorsementExpDate(effdate : Date,expdate: Date): String {
      var allowedDate = (expdate.afterOrEqual(effdate.addWeeks(2)) and expdate.beforeOrEqual(effdate.addDays(90)))
      if(!allowedDate){
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLLocumTenensCovTDIC.ExpDate")
      }
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLLocumTenensCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 68, column 57
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.LTExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 75, column 52
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.DentistName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'requestValidationExpression' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 68, column 57
    function requestValidationExpression_9 (VALUE :  java.util.Date) : java.lang.Object {
      return validateEndorsementExpDate(scheduledItem.LTEffectiveDate, VALUE)
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 60, column 56
    function valueRoot_7 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 68, column 57
    function value_10 () : java.util.Date {
      return scheduledItem.LTExpirationDate
    }
    
    // 'value' attribute on TextCell (id=dentistName_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 75, column 52
    function value_14 () : java.lang.String {
      return scheduledItem.DentistName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLLocumTenensCov_TDIC.pcf: line 60, column 56
    function value_6 () : java.util.Date {
      return scheduledItem.LTEffectiveDate
    }
    
    property get scheduledItem () : entity.GLLocumTenensSched_TDIC {
      return getIteratedValue(1) as entity.GLLocumTenensSched_TDIC
    }
    
    
  }
  
  
}