package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/Account_DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Account_DocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/Account_DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Account_DocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at Account_DocumentsLV.pcf: line 27, column 23
    function initialValue_0 () : boolean {
      return documentsActionsHelper.DocumentContentActionsAvailable
    }
    
    // 'initialValue' attribute on Variable at Account_DocumentsLV.pcf: line 31, column 23
    function initialValue_1 () : boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at Account_DocumentsLV.pcf: line 180, column 25
    function sortValue_10 (Document :  entity.Document) : java.lang.Object {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at Account_DocumentsLV.pcf: line 185, column 36
    function sortValue_11 (Document :  entity.Document) : java.lang.Object {
      return Document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at Account_DocumentsLV.pcf: line 189, column 40
    function sortValue_12 (Document :  entity.Document) : java.lang.Object {
      return Document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at Account_DocumentsLV.pcf: line 196, column 25
    function sortValue_13 (Document :  entity.Document) : java.lang.Object {
      return Document.DateModified
    }
    
    // 'sortBy' attribute on LinkCell (id=HiddenDocument) at Account_DocumentsLV.pcf: line 203, column 53
    function sortValue_14 (Document :  entity.Document) : java.lang.Object {
      return Document.Obsolete
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at Account_DocumentsLV.pcf: line 65, column 25
    function sortValue_2 (Document :  entity.Document) : java.lang.Object {
      return Document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at Account_DocumentsLV.pcf: line 137, column 41
    function sortValue_3 (Document :  entity.Document) : java.lang.Object {
      return Document.Description
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at Account_DocumentsLV.pcf: line 142, column 59
    function sortValue_4 (Document :  entity.Document) : java.lang.Object {
      return Document.Job.LatestPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at Account_DocumentsLV.pcf: line 147, column 43
    function sortValue_5 (Document :  entity.Document) : java.lang.Object {
      return Document.Job.JobNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Product_Cell) at Account_DocumentsLV.pcf: line 153, column 46
    function sortValue_6 (Document :  entity.Document) : java.lang.Object {
      return Document.Policy
    }
    
    // 'value' attribute on TextCell (id=JobSubType_Cell) at Account_DocumentsLV.pcf: line 157, column 53
    function sortValue_7 (Document :  entity.Document) : java.lang.Object {
      return Document.Job.Subtype.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function sortValue_8 (Document :  entity.Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function sortValue_9 (Document :  entity.Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on RowIterator at Account_DocumentsLV.pcf: line 39, column 75
    function value_88 () : gw.api.database.IQueryBeanResult<entity.Document> {
      return docQuery
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at Account_DocumentsLV.pcf: line 203, column 53
    function visible_15 () : java.lang.Boolean {
      return searchCriteria.IncludeObsoletes
    }
    
    property get contentActionsEnabled () : boolean {
      return getVariableValue("contentActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set contentActionsEnabled ($arg :  boolean) {
      setVariableValue("contentActionsEnabled", 0, $arg)
    }
    
    property get docQuery () : gw.api.database.IQueryBeanResult<Document> {
      return getRequireValue("docQuery", 0) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property set docQuery ($arg :  gw.api.database.IQueryBeanResult<Document>) {
      setRequireValue("docQuery", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get metadataActionsEnabled () : boolean {
      return getVariableValue("metadataActionsEnabled", 0) as java.lang.Boolean
    }
    
    property set metadataActionsEnabled ($arg :  boolean) {
      setVariableValue("metadataActionsEnabled", 0, $arg)
    }
    
    property get searchCriteria () : DocumentSearchCriteria {
      return getRequireValue("searchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set searchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get viewOnly () : boolean {
      return getRequireValue("viewOnly", 0) as java.lang.Boolean
    }
    
    property set viewOnly ($arg :  boolean) {
      setRequireValue("viewOnly", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/Account_DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Account_DocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at Account_DocumentsLV.pcf: line 73, column 148
    function action_22 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at Account_DocumentsLV.pcf: line 81, column 146
    function action_27 () : void {
      Document.downloadContent()
    }
    
    // 'action' attribute on Link (id=DocumentsLV_FinalizeLink) at Account_DocumentsLV.pcf: line 92, column 124
    function action_31 () : void {
      new tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper ().finalizeLiveDocument(Document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at Account_DocumentsLV.pcf: line 99, column 117
    function action_33 () : void {
      DocumentDetailsPopup.push(Document)
    }
    
    // 'action' attribute on Link (id=DocumentsLV_DeleteLink) at Account_DocumentsLV.pcf: line 127, column 85
    function action_37 () : void {
      gw.api.web.document.DocumentsHelper.deleteDocument(Document); gw.api.web.document.DocumentsHelper.evictDeletedDocument(Document)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at Account_DocumentsLV.pcf: line 142, column 59
    function action_43 () : void {
      PolicyFileForward.go(Document.Job.LatestPeriod.PolicyNumber)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at Account_DocumentsLV.pcf: line 147, column 43
    function action_48 () : void {
      JobForward.go(Document.Job)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at Account_DocumentsLV.pcf: line 99, column 117
    function action_dest_34 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(Document)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at Account_DocumentsLV.pcf: line 142, column 59
    function action_dest_44 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(Document.Job.LatestPeriod.PolicyNumber)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at Account_DocumentsLV.pcf: line 147, column 43
    function action_dest_49 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(Document.Job)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at Account_DocumentsLV.pcf: line 73, column 148
    function available_20 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, contentActionsEnabled) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at Account_DocumentsLV.pcf: line 81, column 146
    function available_25 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, contentActionsEnabled)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at Account_DocumentsLV.pcf: line 99, column 117
    function available_32 () : java.lang.Boolean {
      return metadataActionsEnabled
    }
    
    // 'condition' attribute on ToolbarFlag at Account_DocumentsLV.pcf: line 42, column 35
    function condition_16 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(Document) and documentsActionsHelper.isDeleteDocumentLinkAvailable(Document)
    }
    
    // 'condition' attribute on ToolbarFlag at Account_DocumentsLV.pcf: line 47, column 34
    function condition_17 () : java.lang.Boolean {
      return perm.Document.edit(Document)
    }
    
    // 'condition' attribute on ToolbarFlag at Account_DocumentsLV.pcf: line 50, column 24
    function condition_18 () : java.lang.Boolean {
      return Document.Obsolete
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at Account_DocumentsLV.pcf: line 58, column 32
    function icon_19 () : java.lang.String {
      return Document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at Account_DocumentsLV.pcf: line 73, column 148
    function label_23 () : java.lang.Object {
      return Document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at Account_DocumentsLV.pcf: line 132, column 75
    function label_39 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'value' attribute on Reflect at Account_DocumentsLV.pcf: line 173, column 83
    function reflectionValue_65 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at Account_DocumentsLV.pcf: line 73, column 148
    function tooltip_24 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function valueRange_61 () : java.lang.Object {
      return DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
    }
    
    // 'valueRange' attribute on Reflect at Account_DocumentsLV.pcf: line 173, column 83
    function valueRange_67 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function valueRange_70 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(Document.Type)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at Account_DocumentsLV.pcf: line 137, column 41
    function valueRoot_41 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at Account_DocumentsLV.pcf: line 142, column 59
    function valueRoot_46 () : java.lang.Object {
      return Document.Job.LatestPeriod
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at Account_DocumentsLV.pcf: line 147, column 43
    function valueRoot_51 () : java.lang.Object {
      return Document.Job
    }
    
    // 'value' attribute on TextCell (id=JobSubType_Cell) at Account_DocumentsLV.pcf: line 157, column 53
    function valueRoot_57 () : java.lang.Object {
      return Document.Job.Subtype
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at Account_DocumentsLV.pcf: line 137, column 41
    function value_40 () : java.lang.String {
      return Document.Description
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at Account_DocumentsLV.pcf: line 142, column 59
    function value_45 () : java.lang.String {
      return Document.Job.LatestPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at Account_DocumentsLV.pcf: line 147, column 43
    function value_50 () : java.lang.String {
      return Document.Job.JobNumber
    }
    
    // 'value' attribute on TextCell (id=Product_Cell) at Account_DocumentsLV.pcf: line 153, column 46
    function value_53 () : java.lang.String {
      return Document.documentOffering
    }
    
    // 'value' attribute on TextCell (id=JobSubType_Cell) at Account_DocumentsLV.pcf: line 157, column 53
    function value_56 () : java.lang.String {
      return Document.Job.Subtype.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function value_59 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function value_68 () : typekey.OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at Account_DocumentsLV.pcf: line 180, column 25
    function value_74 () : typekey.DocumentStatusType {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at Account_DocumentsLV.pcf: line 185, column 36
    function value_77 () : java.lang.String {
      return Document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at Account_DocumentsLV.pcf: line 189, column 40
    function value_80 () : java.lang.String {
      return Document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at Account_DocumentsLV.pcf: line 196, column 25
    function value_83 () : java.util.Date {
      return Document.DateModified
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function verifyValueRangeIsAllowedType_62 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function verifyValueRangeIsAllowedType_62 ($$arg :  typekey.DocumentType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function verifyValueRangeIsAllowedType_71 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function verifyValueRangeIsAllowedType_71 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at Account_DocumentsLV.pcf: line 163, column 45
    function verifyValueRange_63 () : void {
      var __valueRangeArg = DocumentType.TF_UPLOADDOCUMENTTYPES.TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_62(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=SubType_Cell) at Account_DocumentsLV.pcf: line 169, column 57
    function verifyValueRange_72 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(Document.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_71(__valueRangeArg)
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at Account_DocumentsLV.pcf: line 73, column 148
    function visible_21 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at Account_DocumentsLV.pcf: line 81, column 146
    function visible_26 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_FinalizeLink) at Account_DocumentsLV.pcf: line 92, column 124
    function visible_30 () : java.lang.Boolean {
      return Document.MimeType == "application/dlf" and Document.Status == DocumentStatusType.TC_DRAFT
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_DeleteLink) at Account_DocumentsLV.pcf: line 127, column 85
    function visible_36 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(Document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at Account_DocumentsLV.pcf: line 132, column 75
    function visible_38 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(Document)
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at Account_DocumentsLV.pcf: line 203, column 53
    function visible_87 () : java.lang.Boolean {
      return searchCriteria.IncludeObsoletes
    }
    
    property get Document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}