package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.StringField_DentalSchool_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_StringField_DentalSchool_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.StringField_DentalSchool_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function action_1 () : void {
      GLSchoolCodeSearch_TDICPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function action_dest_2 () : pcf.api.Destination {
      return pcf.GLSchoolCodeSearch_TDICPopup.createDestination()
    }
    
    // 'value' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      answerContainer.getAnswer(question).TextAnswer = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function editable_3 () : java.lang.Boolean {
      return question.isQuestionEditable_TDIC(answerContainer)
    }
    
    // 'initialValue' attribute on Variable at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 20, column 23
    function initialValue_0 () : block() {
      return \ -> question
    }
    
    // 'required' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function required_4 () : java.lang.Boolean {
      return question.isQuestionMandatory(answerContainer) //question.Required
    }
    
    // 'value' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function valueRoot_7 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on PickerCell (id=dentalSchoolInput_Cell) at QuestionModalInput.StringField_DentalSchool_TDIC.pcf: line 30, column 37
    function value_5 () : java.lang.String {
      return answerContainer.getAnswer(question).TextAnswer
    }
    
    property get ChangeBlock () : block() {
      return getVariableValue("ChangeBlock", 0) as block()
    }
    
    property set ChangeBlock ($arg :  block()) {
      setVariableValue("ChangeBlock", 0, $arg)
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getVariableValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setVariableValue("term", 0, $arg)
    }
    
    
  }
  
  
}