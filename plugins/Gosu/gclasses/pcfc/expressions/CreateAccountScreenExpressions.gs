package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/new/CreateAccountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateAccountScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/new/CreateAccountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateAccountScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at CreateAccountScreen.pcf: line 66, column 60
    function action_13 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'action' attribute on ToolbarButton (id=StandardizeButton) at CreateAccountScreen.pcf: line 51, column 39
    function action_6 () : void {
      addressList = helper.validateAddress(selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); updateVisible = helper.getUpdateVisible(); overrideVisible = helper.getOverrideVisible(); helper.displayExceptions()
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at CreateAccountScreen.pcf: line 58, column 99
    function action_8 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.commit())
    }
    
    // 'available' attribute on ToolbarButton (id=StandardizeButton) at CreateAccountScreen.pcf: line 51, column 39
    function available_4 () : java.lang.Boolean {
      return standardizeVisible
    }
    
    // 'def' attribute on PanelRef at CreateAccountScreen.pcf: line 72, column 36
    function def_onEnter_15 (def :  pcf.CreateAccountDV) : void {
      def.onEnter(account, producerSelection)
    }
    
    // 'def' attribute on PanelRef at CreateAccountScreen.pcf: line 72, column 36
    function def_refreshVariables_16 (def :  pcf.CreateAccountDV) : void {
      def.refreshVariables(account, producerSelection)
    }
    
    // 'editable' attribute on PanelRef at CreateAccountScreen.pcf: line 72, column 36
    function editable_14 () : java.lang.Boolean {
      return account.Editable
    }
    
    // 'initialValue' attribute on Variable at CreateAccountScreen.pcf: line 19, column 30
    function initialValue_0 () : entity.Address {
      return account.AccountHolderContact.PrimaryAddress
    }
    
    // 'initialValue' attribute on Variable at CreateAccountScreen.pcf: line 23, column 76
    function initialValue_1 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at CreateAccountScreen.pcf: line 28, column 23
    function initialValue_2 () : Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at CreateAccountScreen.pcf: line 36, column 23
    function initialValue_3 () : Boolean {
      return helper.isAdressStandsbuttonVisible(selectedAddress)
    }
    
    // 'label' attribute on ToolbarButton (id=ForceDupCheckUpdate) at CreateAccountScreen.pcf: line 58, column 99
    function label_9 () : java.lang.Object {
      return updateVisible ? DisplayKey.get("Button.Update") : DisplayKey.get("Web.AccountFile.Locations.Override")
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CreateAccountScreen.pcf: line 98, column 54
    function sortValue_17 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at CreateAccountScreen.pcf: line 102, column 54
    function sortValue_18 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at CreateAccountScreen.pcf: line 106, column 46
    function sortValue_19 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at CreateAccountScreen.pcf: line 111, column 42
    function sortValue_20 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at CreateAccountScreen.pcf: line 115, column 52
    function sortValue_21 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at CreateAccountScreen.pcf: line 85, column 65
    function value_38 () : java.util.ArrayList<entity.Address> {
      return addressList
    }
    
    // 'updateVisible' attribute on EditButtons at CreateAccountScreen.pcf: line 61, column 109
    function visible_10 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton and (updateVisible or overrideVisible)
    }
    
    // 'visible' attribute on ToolbarButton (id=CheckForDuplicates) at CreateAccountScreen.pcf: line 66, column 60
    function visible_12 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'visible' attribute on PanelRef at CreateAccountScreen.pcf: line 74, column 42
    function visible_39 () : java.lang.Boolean {
      return addressList.Count > 0
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at CreateAccountScreen.pcf: line 58, column 99
    function visible_7 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton and (updateVisible or overrideVisible)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getRequireValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setRequireValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get openForEdit () : Boolean {
      return getVariableValue("openForEdit", 0) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get overrideVisible () : Boolean {
      return getVariableValue("overrideVisible", 0) as Boolean
    }
    
    property set overrideVisible ($arg :  Boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get producerSelection () : ProducerSelection {
      return getRequireValue("producerSelection", 0) as ProducerSelection
    }
    
    property set producerSelection ($arg :  ProducerSelection) {
      setRequireValue("producerSelection", 0, $arg)
    }
    
    property get selectedAddress () : entity.Address {
      return getVariableValue("selectedAddress", 0) as entity.Address
    }
    
    property set selectedAddress ($arg :  entity.Address) {
      setVariableValue("selectedAddress", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get updateVisible () : Boolean {
      return getVariableValue("updateVisible", 0) as Boolean
    }
    
    property set updateVisible ($arg :  Boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/new/CreateAccountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CreateAccountScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at CreateAccountScreen.pcf: line 93, column 42
    function action_22 () : void {
      helper.replaceAddressWithSuggested(suggestedAddress, selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); addressList = null
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CreateAccountScreen.pcf: line 98, column 54
    function valueRoot_24 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at CreateAccountScreen.pcf: line 98, column 54
    function value_23 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at CreateAccountScreen.pcf: line 102, column 54
    function value_26 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at CreateAccountScreen.pcf: line 106, column 46
    function value_29 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at CreateAccountScreen.pcf: line 111, column 42
    function value_32 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at CreateAccountScreen.pcf: line 115, column 52
    function value_35 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(1) as entity.Address
    }
    
    
  }
  
  
}