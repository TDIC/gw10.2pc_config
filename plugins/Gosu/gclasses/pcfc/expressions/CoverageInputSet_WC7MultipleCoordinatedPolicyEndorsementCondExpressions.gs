package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 32, column 109
    function available_36 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 22, column 36
    function initialValue_0 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'label' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 32, column 109
    function label_37 () : java.lang.Object {
      return conditionPattern.DisplayName
    }
    
    // 'onChange' attribute on PostOnChange at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 104, column 104
    function onChange_4 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncCoverages({coverable}, null)
    }
    
    // 'onToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 32, column 109
    function setter_38 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, conditionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 49, column 32
    function sortBy_5 (policyLaborContractor :  entity.WC7PolicyLaborContractor) : java.lang.Object {
      return policyLaborContractor.AccountContactRole.DisplayName
    }
    
    // 'value' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function sortValue_10 (wc7CoordinatedPolicy :  WC7CoordinatedPolicy) : java.lang.Object {
      return wc7CoordinatedPolicy.StatePerformed
    }
    
    // 'value' attribute on TextCell (id=LaborContractor_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 86, column 47
    function sortValue_11 (wc7CoordinatedPolicy :  WC7CoordinatedPolicy) : java.lang.Object {
      return wc7CoordinatedPolicy.LaborContractor.AccountContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ContractProject_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 92, column 47
    function sortValue_12 (wc7CoordinatedPolicy :  WC7CoordinatedPolicy) : java.lang.Object {
      return wc7CoordinatedPolicy.ContractProject
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 98, column 47
    function sortValue_13 (wc7CoordinatedPolicy :  WC7CoordinatedPolicy) : java.lang.Object {
      return wc7CoordinatedPolicy.LaborContractorPolicyNumber
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 71, column 55
    function toRemove_32 (wc7CoordinatedPolicy :  WC7CoordinatedPolicy) : void {
      theWC7Line.removeFromMultipleCoordinatedPolicies( wc7CoordinatedPolicy )
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 26, column 37
    function valueRoot_2 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 26, column 37
    function value_1 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 71, column 55
    function value_33 () : entity.WC7CoordinatedPolicy[] {
      return theWC7Line.MultipleCoordinatedPolicies
    }
    
    // 'value' attribute on AddMenuItemIterator at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 46, column 30
    function value_8 () : entity.WC7PolicyLaborContractor[] {
      return theWC7Line.WC7PolicyLaborContractorsCondition
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 32, column 109
    function visible_35 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 107, column 101
    function visible_41 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on AddButton (id=AddWC7PolicyLaborContractor) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 41, column 35
    function visible_9 () : java.lang.Boolean {
      return openForEdit
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get theWC7Line () : productmodel.WC7Line {
      return getVariableValue("theWC7Line", 0) as productmodel.WC7Line
    }
    
    property set theWC7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    function getAvailableLaborContractors() : LaborContractor[] {
      var results : LaborContractor[] = {}
      results = theWC7Line.WC7PolicyLaborContractors.map(\ pl -> pl.AccountContactRole as LaborContractor)
      return results
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7CoordinatedPolicy.StatePerformed = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TextCell (id=ContractProject_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 92, column 47
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7CoordinatedPolicy.ContractProject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 98, column 47
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7CoordinatedPolicy.LaborContractorPolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'valueRange' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function valueRange_17 () : java.lang.Object {
      return theWC7Line.availableJurisdictionsForPolicyType(TC_MCP)
    }
    
    // 'value' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function valueRoot_16 () : java.lang.Object {
      return wc7CoordinatedPolicy
    }
    
    // 'value' attribute on TextCell (id=LaborContractor_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 86, column 47
    function valueRoot_22 () : java.lang.Object {
      return wc7CoordinatedPolicy.LaborContractor.AccountContact
    }
    
    // 'value' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function value_14 () : typekey.Jurisdiction {
      return wc7CoordinatedPolicy.StatePerformed
    }
    
    // 'value' attribute on TextCell (id=LaborContractor_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 86, column 47
    function value_21 () : java.lang.String {
      return wc7CoordinatedPolicy.LaborContractor.AccountContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ContractProject_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 92, column 47
    function value_24 () : java.lang.String {
      return wc7CoordinatedPolicy.ContractProject
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 98, column 47
    function value_28 () : java.lang.String {
      return wc7CoordinatedPolicy.LaborContractorPolicyNumber
    }
    
    // 'valueRange' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function verifyValueRangeIsAllowedType_18 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function verifyValueRangeIsAllowedType_18 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=jurisdiction_Cell) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 80, column 51
    function verifyValueRange_19 () : void {
      var __valueRangeArg = theWC7Line.availableJurisdictionsForPolicyType(TC_MCP)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_18(__valueRangeArg)
    }
    
    property get wc7CoordinatedPolicy () : WC7CoordinatedPolicy {
      return getIteratedValue(1) as WC7CoordinatedPolicy
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=PolicyLaborContractor) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 55, column 33
    function label_6 () : java.lang.Object {
      return policyLaborContractor
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=PolicyLaborContractor) at CoverageInputSet.WC7MultipleCoordinatedPolicyEndorsementCond.pcf: line 55, column 33
    function toCreateAndAdd_7 (CheckedValues :  Object[]) : java.lang.Object {
      return theWC7Line.createAndAddCoordinatedPolicy(theWC7Line.WC7MultipleCoordinatedPolicyEndorsementCond, policyLaborContractor)
    }
    
    property get policyLaborContractor () : entity.WC7PolicyLaborContractor {
      return getIteratedValue(1) as entity.WC7PolicyLaborContractor
    }
    
    
  }
  
  
}