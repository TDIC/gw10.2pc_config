package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7ModifiersInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7ModifiersInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7ModifiersInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=WC7JurisdictionModifierInput) at WC7ModifiersInputSet.pcf: line 27, column 28
    function def_onEnter_2 (def :  pcf.WC7JurisdictionModifierInputSet_default) : void {
      def.onEnter(modifiers, policyPeriod, openForEdit, stateConfig)
    }
    
    // 'def' attribute on InputSetRef (id=WC7JurisdictionModifierInput) at WC7ModifiersInputSet.pcf: line 27, column 28
    function def_refreshVariables_3 (def :  pcf.WC7JurisdictionModifierInputSet_default) : void {
      def.refreshVariables(modifiers, policyPeriod, openForEdit, stateConfig)
    }
    
    // 'initialValue' attribute on Variable at WC7ModifiersInputSet.pcf: line 19, column 36
    function initialValue_0 () : typekey.Jurisdiction {
      return modifiers.first().State
    }
    
    // 'initialValue' attribute on Variable at WC7ModifiersInputSet.pcf: line 23, column 54
    function initialValue_1 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(jurisdiction)
    }
    
    // 'mode' attribute on InputSetRef (id=WC7JurisdictionModifierInput) at WC7ModifiersInputSet.pcf: line 27, column 28
    function mode_4 () : java.lang.Object {
      return jurisdiction
    }
    
    property get jurisdiction () : typekey.Jurisdiction {
      return getVariableValue("jurisdiction", 0) as typekey.Jurisdiction
    }
    
    property set jurisdiction ($arg :  typekey.Jurisdiction) {
      setVariableValue("jurisdiction", 0, $arg)
    }
    
    property get modifiers () : java.util.List<WC7Modifier> {
      return getRequireValue("modifiers", 0) as java.util.List<WC7Modifier>
    }
    
    property set modifiers ($arg :  java.util.List<WC7Modifier>) {
      setRequireValue("modifiers", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 0) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 0, $arg)
    }
    
    function toggleEligible(mod : Modifier) {
      if (mod.Eligible) {
        mod.RateModifier = null
        mod.Eligible = false
      } else {
        mod.Eligible = true
      }
    }
    
    
  }
  
  
}