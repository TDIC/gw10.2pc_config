package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuilding_DetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPBuilding_DetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuilding_DetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPBuilding_DetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=ConstructionType_Input) at BOPBuilding_DetailsDV.pcf: line 48, column 50
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.ConstructionType = (__VALUE_TO_SET as typekey.BOPConstructionType)
    }
    
    // 'value' attribute on TextInput (id=NumStories_Input) at BOPBuilding_DetailsDV.pcf: line 57, column 40
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.NumStories = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=AreaFinished_Input) at BOPBuilding_DetailsDV.pcf: line 210, column 42
    function defaultSetter_198 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.AreaFinished = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=AreaUnfinished_Input) at BOPBuilding_DetailsDV.pcf: line 217, column 42
    function defaultSetter_202 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.AreaUnfinished = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=buildingImpCheck_Input) at BOPBuilding_DetailsDV.pcf: line 231, column 152
    function defaultSetter_208 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.BuildingImprovement_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=LastUpdatePlumbing_Input) at BOPBuilding_DetailsDV.pcf: line 254, column 83
    function defaultSetter_215 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.Plumbing.YearAdded = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=LastUpdateRoofing_Input) at BOPBuilding_DetailsDV.pcf: line 262, column 83
    function defaultSetter_221 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.Roofing.YearAdded = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=LastUpdateWiring_Input) at BOPBuilding_DetailsDV.pcf: line 270, column 83
    function defaultSetter_227 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.Wiring.YearAdded = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=terrotory_Input) at BOPBuilding_DetailsDV.pcf: line 281, column 52
    function defaultSetter_233 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.TerritoryDetails_TDIC = (__VALUE_TO_SET as typekey.TerritoryDetails_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=protectionClass_Input) at BOPBuilding_DetailsDV.pcf: line 289, column 51
    function defaultSetter_240 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.ProtectionClass_TDIC = (__VALUE_TO_SET as typekey.ProtectionClass_TDIC)
    }
    
    // 'value' attribute on TextInput (id=TotalArea_Input) at BOPBuilding_DetailsDV.pcf: line 65, column 40
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.TotalArea = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=TotalOfficeArea_Input) at BOPBuilding_DetailsDV.pcf: line 74, column 103
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.TotalOfficeArea_TDIC = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=NumOper_Input) at BOPBuilding_DetailsDV.pcf: line 83, column 103
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.NumOperatories_TDIC = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=NumUnits_Input) at BOPBuilding_DetailsDV.pcf: line 91, column 40
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.NumUnits_TDIC = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=YearBuilt_Input) at BOPBuilding_DetailsDV.pcf: line 41, column 40
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.YearBuilt = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=RentalIncome_Input) at BOPBuilding_DetailsDV.pcf: line 101, column 94
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.RentalIncome_TDIC = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=sprinkle_Input) at BOPBuilding_DetailsDV.pcf: line 107, column 53
    function defaultSetter_71 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.Sprinklered_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=alarm_Input) at BOPBuilding_DetailsDV.pcf: line 113, column 54
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.BurglarAlarm_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      building.Building.BOPOccupancyStatus_TDIC = (__VALUE_TO_SET as typekey.BOPOccupancyStatus_TDIC)
    }
    
    // 'editable' attribute on TypeKeyInput (id=ConstructionType_Input) at BOPBuilding_DetailsDV.pcf: line 48, column 50
    function editable_9 () : java.lang.Boolean {
      return bopLine?.Branch?.profileChange_TDIC
    }
    
    // 'filter' attribute on TypeKeyInput (id=terrotory_Input) at BOPBuilding_DetailsDV.pcf: line 281, column 52
    function filter_235 (VALUE :  typekey.TerritoryDetails_TDIC, VALUES :  typekey.TerritoryDetails_TDIC[]) : java.lang.Boolean {
      return VALUE.hasCategory(building.BOPLocation.CoverableState)
    }
    
    // 'initialValue' attribute on Variable at BOPBuilding_DetailsDV.pcf: line 23, column 53
    function initialValue_0 () : gw.api.productmodel.CoveragePattern[] {
      return building == null ? null : building.PolicyLine.Pattern.getCoverageCategoryByPublicId("BOPBuildingCat").coveragePatternsForEntity(BOPBuilding).whereSelectedOrAvailable(building, openForEdit)
    }
    
    // 'label' attribute on TextInput (id=TotalArea_Input) at BOPBuilding_DetailsDV.pcf: line 65, column 40
    function label_22 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.BOP.Building.TotalBuilding_TDIC") 
    }
    
    // 'label' attribute on TextInput (id=TotalOfficeArea_Input) at BOPBuilding_DetailsDV.pcf: line 74, column 103
    function label_32 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.BOP.Building.TotalOffice_TDIC") 
    }
    
    // 'label' attribute on TextInput (id=NumOper_Input) at BOPBuilding_DetailsDV.pcf: line 83, column 103
    function label_42 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.BOP.Building.NumberOperatories_TDIC") 
    }
    
    // 'label' attribute on TextInput (id=NumUnits_Input) at BOPBuilding_DetailsDV.pcf: line 91, column 40
    function label_50 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.BOP.Building.NumUnits_TDIC") 
    }
    
    // 'label' attribute on TextInput (id=RentalIncome_Input) at BOPBuilding_DetailsDV.pcf: line 101, column 94
    function label_59 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.BOP.Building.RentalIncome_TDIC") 
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=NumStories_Input) at BOPBuilding_DetailsDV.pcf: line 57, column 40
    function requestValidationExpression_15 (VALUE :  java.lang.Integer) : java.lang.Object {
      return checkValue_TDIC(VALUE) 
    }
    
    // 'required' attribute on TextInput (id=TotalArea_Input) at BOPBuilding_DetailsDV.pcf: line 65, column 40
    function required_23 () : java.lang.Boolean {
      return quoteType == QuoteType.TC_QUICK ? bopLine.Branch.Offering.CodeIdentifier=="BOPLessorsRisk_TDIC"?true:false : true
    }
    
    // 'required' attribute on TextInput (id=TotalOfficeArea_Input) at BOPBuilding_DetailsDV.pcf: line 74, column 103
    function required_33 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'required' attribute on TextInput (id=YearBuilt_Input) at BOPBuilding_DetailsDV.pcf: line 41, column 40
    function required_4 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK || building.Branch.isDIMSPolicy_TDIC)
    }
    
    // 'required' attribute on TextInput (id=RentalIncome_Input) at BOPBuilding_DetailsDV.pcf: line 101, column 94
    function required_60 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK || bopLine.Branch.isDIMSPolicy_TDIC)
    }
    
    // 'required' attribute on BooleanRadioInput (id=alarm_Input) at BOPBuilding_DetailsDV.pcf: line 113, column 54
    function required_75 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK || (bopLine.Branch.isDIMSPolicy_TDIC && bopLine.Branch.Offering.CodeIdentifier=="BOPBusinessOwnersPolicy_TDIC"))
    }
    
    // 'sortBy' attribute on IteratorSort at BOPBuilding_DetailsDV.pcf: line 180, column 26
    function sortBy_88 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'valueRange' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function valueRange_84 () : java.lang.Object {
      return setOccupancyValues_TDIC()
    }
    
    // 'value' attribute on TypeKeyInput (id=ConstructionType_Input) at BOPBuilding_DetailsDV.pcf: line 48, column 50
    function valueRoot_12 () : java.lang.Object {
      return building
    }
    
    // 'value' attribute on TextInput (id=LocationInfo_Input) at BOPBuilding_DetailsDV.pcf: line 31, column 63
    function valueRoot_2 () : java.lang.Object {
      return building.Building.PolicyLocation
    }
    
    // 'value' attribute on TextInput (id=LastUpdatePlumbing_Input) at BOPBuilding_DetailsDV.pcf: line 254, column 83
    function valueRoot_216 () : java.lang.Object {
      return building.Building.Plumbing
    }
    
    // 'value' attribute on TextInput (id=LastUpdateRoofing_Input) at BOPBuilding_DetailsDV.pcf: line 262, column 83
    function valueRoot_222 () : java.lang.Object {
      return building.Building.Roofing
    }
    
    // 'value' attribute on TextInput (id=LastUpdateWiring_Input) at BOPBuilding_DetailsDV.pcf: line 270, column 83
    function valueRoot_228 () : java.lang.Object {
      return building.Building.Wiring
    }
    
    // 'value' attribute on TextInput (id=YearBuilt_Input) at BOPBuilding_DetailsDV.pcf: line 41, column 40
    function valueRoot_7 () : java.lang.Object {
      return building.Building
    }
    
    // 'value' attribute on TextInput (id=LocationInfo_Input) at BOPBuilding_DetailsDV.pcf: line 31, column 63
    function value_1 () : java.lang.String {
      return building.Building.PolicyLocation.DisplayName
    }
    
    // 'value' attribute on TypeKeyInput (id=ConstructionType_Input) at BOPBuilding_DetailsDV.pcf: line 48, column 50
    function value_10 () : typekey.BOPConstructionType {
      return building.ConstructionType
    }
    
    // 'value' attribute on TextInput (id=NumStories_Input) at BOPBuilding_DetailsDV.pcf: line 57, column 40
    function value_17 () : java.lang.Integer {
      return building.Building.NumStories
    }
    
    // 'value' attribute on InputIterator (id=BOPBuildingCatIterator) at BOPBuilding_DetailsDV.pcf: line 177, column 59
    function value_196 () : gw.api.productmodel.CoveragePattern[] {
      return bopBuildingCatCoveragePatterns
    }
    
    // 'value' attribute on TextInput (id=AreaFinished_Input) at BOPBuilding_DetailsDV.pcf: line 210, column 42
    function value_197 () : java.lang.Integer {
      return building.Building.AreaFinished
    }
    
    // 'value' attribute on TextInput (id=AreaUnfinished_Input) at BOPBuilding_DetailsDV.pcf: line 217, column 42
    function value_201 () : java.lang.Integer {
      return building.Building.AreaUnfinished
    }
    
    // 'value' attribute on BooleanRadioInput (id=buildingImpCheck_Input) at BOPBuilding_DetailsDV.pcf: line 231, column 152
    function value_207 () : java.lang.Boolean {
      return building.Building.BuildingImprovement_TDIC
    }
    
    // 'value' attribute on TextInput (id=LastUpdatePlumbing_Input) at BOPBuilding_DetailsDV.pcf: line 254, column 83
    function value_214 () : java.lang.Integer {
      return building.Building.Plumbing.YearAdded
    }
    
    // 'value' attribute on TextInput (id=LastUpdateRoofing_Input) at BOPBuilding_DetailsDV.pcf: line 262, column 83
    function value_220 () : java.lang.Integer {
      return building.Building.Roofing.YearAdded
    }
    
    // 'value' attribute on TextInput (id=LastUpdateWiring_Input) at BOPBuilding_DetailsDV.pcf: line 270, column 83
    function value_226 () : java.lang.Integer {
      return building.Building.Wiring.YearAdded
    }
    
    // 'value' attribute on TypeKeyInput (id=terrotory_Input) at BOPBuilding_DetailsDV.pcf: line 281, column 52
    function value_232 () : typekey.TerritoryDetails_TDIC {
      return building.Building.TerritoryDetails_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=protectionClass_Input) at BOPBuilding_DetailsDV.pcf: line 289, column 51
    function value_239 () : typekey.ProtectionClass_TDIC {
      return building.Building.ProtectionClass_TDIC
    }
    
    // 'value' attribute on TextInput (id=TotalArea_Input) at BOPBuilding_DetailsDV.pcf: line 65, column 40
    function value_24 () : java.lang.Integer {
      return building.Building.TotalArea
    }
    
    // 'value' attribute on TextInput (id=TotalOfficeArea_Input) at BOPBuilding_DetailsDV.pcf: line 74, column 103
    function value_34 () : java.lang.Integer {
      return building.Building.TotalOfficeArea_TDIC
    }
    
    // 'value' attribute on TextInput (id=NumOper_Input) at BOPBuilding_DetailsDV.pcf: line 83, column 103
    function value_44 () : java.lang.Integer {
      return building.Building.NumOperatories_TDIC
    }
    
    // 'value' attribute on TextInput (id=YearBuilt_Input) at BOPBuilding_DetailsDV.pcf: line 41, column 40
    function value_5 () : java.lang.Integer {
      return building.Building.YearBuilt
    }
    
    // 'value' attribute on TextInput (id=NumUnits_Input) at BOPBuilding_DetailsDV.pcf: line 91, column 40
    function value_52 () : java.lang.Integer {
      return building.Building.NumUnits_TDIC
    }
    
    // 'value' attribute on TextInput (id=RentalIncome_Input) at BOPBuilding_DetailsDV.pcf: line 101, column 94
    function value_61 () : java.lang.Integer {
      return building.Building.RentalIncome_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=sprinkle_Input) at BOPBuilding_DetailsDV.pcf: line 107, column 53
    function value_70 () : java.lang.Boolean {
      return building.Building.Sprinklered_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=alarm_Input) at BOPBuilding_DetailsDV.pcf: line 113, column 54
    function value_76 () : java.lang.Boolean {
      return building.Building.BurglarAlarm_TDIC
    }
    
    // 'value' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function value_81 () : typekey.BOPOccupancyStatus_TDIC {
      return building.Building.BOPOccupancyStatus_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function verifyValueRangeIsAllowedType_85 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function verifyValueRangeIsAllowedType_85 ($$arg :  typekey.BOPOccupancyStatus_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=occStatus_Input) at BOPBuilding_DetailsDV.pcf: line 125, column 54
    function verifyValueRange_86 () : void {
      var __valueRangeArg = setOccupancyValues_TDIC()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_85(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet (id=BasementInputSet) at BOPBuilding_DetailsDV.pcf: line 203, column 100
    function visible_205 () : java.lang.Boolean {
      return building.Building.NumBasements != null and building.Building.NumBasements > 0
    }
    
    // 'visible' attribute on BooleanRadioInput (id=buildingImpCheck_Input) at BOPBuilding_DetailsDV.pcf: line 231, column 152
    function visible_206 () : java.lang.Boolean {
      return (!(quoteType == QuoteType.TC_QUICK)) and (building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC")
    }
    
    // 'visible' attribute on Label (id=buildingImprovementLabel) at BOPBuilding_DetailsDV.pcf: line 238, column 85
    function visible_212 () : java.lang.Boolean {
      return (!(quoteType == QuoteType.TC_QUICK)) and buildingImpVisibility()
    }
    
    // 'visible' attribute on TextInput (id=LastUpdatePlumbing_Input) at BOPBuilding_DetailsDV.pcf: line 254, column 83
    function visible_213 () : java.lang.Boolean {
      return !(quoteType == QuoteType.TC_QUICK) and buildingImpVisibility()
    }
    
    // 'visible' attribute on TextInput (id=TotalOfficeArea_Input) at BOPBuilding_DetailsDV.pcf: line 74, column 103
    function visible_31 () : java.lang.Boolean {
      return bopLine.Branch.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"
    }
    
    // 'visible' attribute on TextInput (id=RentalIncome_Input) at BOPBuilding_DetailsDV.pcf: line 101, column 94
    function visible_58 () : java.lang.Boolean {
      return bopLine.Branch.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC"
    }
    
    property get bopBuildingCatCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("bopBuildingCatCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set bopBuildingCatCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("bopBuildingCatCoveragePatterns", 0, $arg)
    }
    
    property get bopLine () : BOPLine {
      return getRequireValue("bopLine", 0) as BOPLine
    }
    
    property set bopLine ($arg :  BOPLine) {
      setRequireValue("bopLine", 0, $arg)
    }
    
    property get building () : BOPBuilding {
      return getRequireValue("building", 0) as BOPBuilding
    }
    
    property set building ($arg :  BOPBuilding) {
      setRequireValue("building", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getRequireValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setRequireValue("quoteType", 0, $arg)
    }
    
    /**
     * create by: ChitraK
     * @description: validation for number of floors
     * @create time: 5:58 PM 8/9/2019
     * @return: 
     */
    
    function checkValue_TDIC(floor : Integer) : String{
      if(floor < 0 || floor > 999) 
        return "Number of Floors can accept value only between 0 and 999"
      return null
    }
    /**
     * create by: ChitraK
     * @description: Set Occupancy Status Values
     * @create time: 5:58 PM 8/9/2019
     * @return:BOPOccupancyStatus_TDIC 
     */
    
    function setOccupancyValues_TDIC():List<typekey.BOPOccupancyStatus_TDIC>{
      var occList : List<BOPOccupancyStatus_TDIC> = {}
      if(bopLine.Branch.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"){
        var bopList = {BOPOccupancyStatus_TDIC.TC_TENANT,BOPOccupancyStatus_TDIC.TC_BUILDINGOWNER,BOPOccupancyStatus_TDIC.TC_CONDO,BOPOccupancyStatus_TDIC.TC_OTHER,BOPOccupancyStatus_TDIC.TC_TRIPLE}
        occList.addAll(bopList)
      }
      if(bopLine.Branch.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC"){
        var lrpList = {BOPOccupancyStatus_TDIC.TC_NOTOCCUPYING,BOPOccupancyStatus_TDIC.TC_BUILDINGOWNER,BOPOccupancyStatus_TDIC.TC_CONDO,BOPOccupancyStatus_TDIC.TC_OTHER,BOPOccupancyStatus_TDIC.TC_TRIPLE}
        occList.addAll(lrpList)
      }
      return occList
    }
    
    function buildingImpVisibility() : Boolean{
      if((building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC")){
        if(building.Building.BuildingImprovement_TDIC != null){
        return !building.Building.BuildingImprovement_TDIC}else{
          return false
        }
      }else{
        return true
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuilding_DetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BOPBuilding_DetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_101 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_103 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_105 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_107 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_109 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_111 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_113 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_115 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_117 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_119 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_121 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_123 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_125 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_127 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_129 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_131 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_133 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_135 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_137 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_139 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_141 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_143 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_145 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_147 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_149 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_151 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_153 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_155 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_157 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_159 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_161 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_163 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_165 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_167 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_169 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_171 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_173 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_175 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_177 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_179 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_181 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_183 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_185 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_187 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_189 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_191 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_193 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_89 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_91 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_93 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_95 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_97 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_onEnter_99 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_112 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_114 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_116 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_118 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_120 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_122 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_124 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_126 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_128 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_130 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_132 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_134 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_136 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_138 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_140 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_142 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_144 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_146 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_148 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, building, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at BOPBuilding_DetailsDV.pcf: line 184, column 28
    function mode_195 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  
}