package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPDeductibleDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPDeductibleDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPDeductibleDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPDeductibleDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=deductible_Input) at TDIC_BOPDeductibleDV.pcf: line 23, column 43
    function valueRoot_1 () : java.lang.Object {
      return bldg.BOPDeductibleCov_TDIC.BOPDeductibleOptionsTDICTerm
    }
    
    // 'value' attribute on TextInput (id=deductible_Input) at TDIC_BOPDeductibleDV.pcf: line 23, column 43
    function value_0 () : java.math.BigDecimal {
      return bldg.BOPDeductibleCov_TDIC.BOPDeductibleOptionsTDICTerm.Value
    }
    
    property get bldg () : entity.BOPBuilding {
      return getRequireValue("bldg", 0) as entity.BOPBuilding
    }
    
    property set bldg ($arg :  entity.BOPBuilding) {
      setRequireValue("bldg", 0, $arg)
    }
    
    
  }
  
  
}