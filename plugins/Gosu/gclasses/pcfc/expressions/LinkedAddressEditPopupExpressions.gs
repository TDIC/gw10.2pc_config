package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/LinkedAddressEditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LinkedAddressEditPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/LinkedAddressEditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LinkedAddressEditPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (address :  Address) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateAllButton) at LinkedAddressEditPopup.pcf: line 23, column 62
    function action_2 () : void {
      address.updateLinkedAddresses(); CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateAndUnlinkButton) at LinkedAddressEditPopup.pcf: line 27, column 101
    function action_3 () : void {
      address.unlink(); CurrentLocation.commit()
    }
    
    // 'def' attribute on PanelRef at LinkedAddressEditPopup.pcf: line 35, column 48
    function def_onEnter_5 (def :  pcf.LinkedAddressContactsLV) : void {
      def.onEnter(address)
    }
    
    // 'def' attribute on InputSetRef at LinkedAddressEditPopup.pcf: line 50, column 32
    function def_onEnter_7 (def :  pcf.AddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressInputSetAddressOwner(address, false, true))
    }
    
    // 'def' attribute on PanelRef at LinkedAddressEditPopup.pcf: line 35, column 48
    function def_refreshVariables_6 (def :  pcf.LinkedAddressContactsLV) : void {
      def.refreshVariables(address)
    }
    
    // 'def' attribute on InputSetRef at LinkedAddressEditPopup.pcf: line 50, column 32
    function def_refreshVariables_8 (def :  pcf.AddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressInputSetAddressOwner(address, false, true))
    }
    
    // 'value' attribute on TypeKeyInput (id=AddressType_Input) at LinkedAddressEditPopup.pcf: line 57, column 48
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      address.AddressType = (__VALUE_TO_SET as typekey.AddressType)
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at LinkedAddressEditPopup.pcf: line 62, column 44
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      address.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at LinkedAddressEditPopup.pcf: line 15, column 23
    function initialValue_0 () : Address {
      return null
    }
    
    // EditButtons (id=CancelButton) at LinkedAddressEditPopup.pcf: line 32, column 34
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TypeKeyInput (id=AddressType_Input) at LinkedAddressEditPopup.pcf: line 57, column 48
    function valueRoot_11 () : java.lang.Object {
      return address
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at LinkedAddressEditPopup.pcf: line 62, column 44
    function value_13 () : java.lang.String {
      return address.Description
    }
    
    // 'value' attribute on TypeKeyInput (id=AddressType_Input) at LinkedAddressEditPopup.pcf: line 57, column 48
    function value_9 () : typekey.AddressType {
      return address.AddressType
    }
    
    // 'visible' attribute on ToolbarButton (id=UpdateAllButton) at LinkedAddressEditPopup.pcf: line 23, column 62
    function visible_1 () : java.lang.Boolean {
      return perm.System.addressDetailUpdateBtn_TDIC
    }
    
    override property get CurrentLocation () : pcf.LinkedAddressEditPopup {
      return super.CurrentLocation as pcf.LinkedAddressEditPopup
    }
    
    property get address () : Address {
      return getVariableValue("address", 0) as Address
    }
    
    property set address ($arg :  Address) {
      setVariableValue("address", 0, $arg)
    }
    
    
  }
  
  
}