package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_ExtendedReportingDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ExtendedReportingDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_ExtendedReportingDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_ExtendedReportingDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_ExtendedReportingDV.pcf: line 48, column 59
    function valueRoot_11 () : java.lang.Object {
      return ereDetails.EREStatus
    }
    
    // 'value' attribute on TextCell (id=transaction_Cell) at TDIC_ExtendedReportingDV.pcf: line 36, column 54
    function valueRoot_5 () : java.lang.Object {
      return ereDetails
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_ExtendedReportingDV.pcf: line 48, column 59
    function value_10 () : java.lang.String {
      return ereDetails.EREStatus.DisplayName
    }
    
    // 'value' attribute on DateCell (id=date_Cell) at TDIC_ExtendedReportingDV.pcf: line 53, column 42
    function value_13 () : java.util.Date {
      return ereDetails.Date
    }
    
    // 'value' attribute on TextCell (id=transaction_Cell) at TDIC_ExtendedReportingDV.pcf: line 36, column 54
    function value_4 () : java.lang.String {
      return ereDetails.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at TDIC_ExtendedReportingDV.pcf: line 42, column 48
    function value_7 () : java.lang.String {
      return ereDetails.Description
    }
    
    property get ereDetails () : entity.EndorsementDetails_TDIC {
      return getIteratedValue(1) as entity.EndorsementDetails_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_ExtendedReportingDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ExtendedReportingDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=transaction_Cell) at TDIC_ExtendedReportingDV.pcf: line 36, column 54
    function sortValue_0 (ereDetails :  entity.EndorsementDetails_TDIC) : java.lang.Object {
      return ereDetails.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at TDIC_ExtendedReportingDV.pcf: line 42, column 48
    function sortValue_1 (ereDetails :  entity.EndorsementDetails_TDIC) : java.lang.Object {
      return ereDetails.Description
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_ExtendedReportingDV.pcf: line 48, column 59
    function sortValue_2 (ereDetails :  entity.EndorsementDetails_TDIC) : java.lang.Object {
      return ereDetails.EREStatus.DisplayName
    }
    
    // 'value' attribute on DateCell (id=date_Cell) at TDIC_ExtendedReportingDV.pcf: line 53, column 42
    function sortValue_3 (ereDetails :  entity.EndorsementDetails_TDIC) : java.lang.Object {
      return ereDetails.Date
    }
    
    // 'value' attribute on RowIterator at TDIC_ExtendedReportingDV.pcf: line 31, column 58
    function value_16 () : entity.EndorsementDetails_TDIC[] {
      return policyPeriod.Policy.EndorsementDetails_TDIC
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}