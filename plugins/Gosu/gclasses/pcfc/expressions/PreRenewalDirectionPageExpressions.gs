package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/prerenewal/PreRenewalDirectionPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PreRenewalDirectionPageExpressions {
  @javax.annotation.Generated("config/web/pcf/prerenewal/PreRenewalDirectionPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PreRenewalDirectionPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, policy :  Policy, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=AssigneePicker) at PreRenewalDirectionPage.pcf: line 110, column 220
    function action_33 () : void {
      AssigneePickerPopup.push( new gw.assignment.UserAssigneePicker() )
    }
    
    // 'action' attribute on ToolbarButton (id=PreRenewalDirectionScreen_DeleteButton) at PreRenewalDirectionPage.pcf: line 61, column 136
    function action_8 () : void {
      removePrerenewal()
    }
    
    // 'action' attribute on ToolbarButton (id=PreRenewalDirectionScreen_ViewNotesButton) at PreRenewalDirectionPage.pcf: line 66, column 82
    function action_9 () : void {
      PreRenewalNotesPage.push(policyPeriod, TC_PRERENEWAL, asOfDate)
    }
    
    // 'action' attribute on ToolbarButton (id=PreRenewalDirectionScreen_ViewNotesButton) at PreRenewalDirectionPage.pcf: line 66, column 82
    function action_dest_10 () : pcf.api.Destination {
      return pcf.PreRenewalNotesPage.createDestination(policyPeriod, TC_PRERENEWAL, asOfDate)
    }
    
    // 'action' attribute on MenuItem (id=AssigneePicker) at PreRenewalDirectionPage.pcf: line 110, column 220
    function action_dest_34 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination( new gw.assignment.UserAssigneePicker() )
    }
    
    // 'afterCancel' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    function afterCancel_90 () : void {
      PolicyFileForward.go(policyPeriod, asOfDate, "PolicySummary")
    }
    
    // 'afterCancel' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    function afterCancel_dest_91 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(policyPeriod, asOfDate, "PolicySummary")
    }
    
    // 'afterCommit' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    function afterCommit_92 (TopLocation :  pcf.api.Location) : void {
      PolicyFileForward.go(policyPeriod, asOfDate, "PolicySummary")
    }
    
    // 'beforeCommit' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    function beforeCommit_93 (pickedValue :  java.lang.Object) : void {
      createCustomHistoryEvent()
    }
    
    // 'canEdit' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    function canEdit_94 () : java.lang.Boolean {
      return perm.System.editprerenewal and policyPeriod.Policy.OpenRenewalJob == null
    }
    
    // 'canVisit' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    static function canVisit_95 (asOfDate :  java.util.Date, policy :  Policy, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.viewprerenewal and perm.PolicyPeriod.view(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTerm.PreRenewalDirection = (__VALUE_TO_SET as typekey.PreRenewalDirection)
    }
    
    // 'value' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      policy.PreRenewalOwner = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'value' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTerm.NonRenewReason = (__VALUE_TO_SET as typekey.NonRenewalCode)
    }
    
    // 'value' attribute on TextAreaInput (id=NonRenewAddExpl_Input) at PreRenewalDirectionPage.pcf: line 150, column 123
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTerm.NonRenewAddExplanation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      note.Subject = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityLevel_Input) at PreRenewalDirectionPage.pcf: line 194, column 55
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      note.SecurityType = (__VALUE_TO_SET as typekey.NoteSecurityType)
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at PreRenewalDirectionPage.pcf: line 201, column 36
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      note.Body = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filter' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function filter_26 (VALUE :  typekey.PreRenewalDirection, VALUES :  typekey.PreRenewalDirection[]) : java.lang.Boolean {
      return filterPreRenewalDirection(VALUE)
    }
    
    // 'filter' attribute on TypeKeyInput (id=SecurityLevel_Input) at PreRenewalDirectionPage.pcf: line 194, column 55
    function filter_84 (VALUE :  typekey.NoteSecurityType, VALUES :  typekey.NoteSecurityType[]) : java.lang.Boolean {
      return note.hasCreatePermission(VALUE)
    }
    
    // 'initialValue' attribute on Variable at PreRenewalDirectionPage.pcf: line 28, column 26
    function initialValue_0 () : PolicyTerm {
      return policyPeriod.PolicyTerm
    }
    
    // 'initialValue' attribute on Variable at PreRenewalDirectionPage.pcf: line 32, column 24
    function initialValue_1 () : String[] {
      return new String[]{""}
    }
    
    // 'initialValue' attribute on Variable at PreRenewalDirectionPage.pcf: line 36, column 43
    function initialValue_2 () : typekey.PreRenewalDirection {
      return policyTerm.PreRenewalDirection
    }
    
    // 'initialValue' attribute on Variable at PreRenewalDirectionPage.pcf: line 40, column 38
    function initialValue_3 () : typekey.NonRenewalCode {
      return policyTerm.NonRenewReason
    }
    
    // 'initialValue' attribute on Variable at PreRenewalDirectionPage.pcf: line 48, column 20
    function initialValue_4 () : Note {
      return policy.createPreRenewalDirectionNote_TDIC()
    }
    
    // EditButtons at PreRenewalDirectionPage.pcf: line 54, column 23
    function label_6 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onChange' attribute on PostOnChange at PreRenewalDirectionPage.pcf: line 85, column 167
    function onChange_11 () : void {
      descriptions[0] = descriptions[0] + policyTerm.createPreRenewalDirectionHistoryDescription(originalPreRenewalDirection) + " "
    }
    
    // 'onChange' attribute on PostOnChange at PreRenewalDirectionPage.pcf: line 98, column 167
    function onChange_21 () : void {
      descriptions[0] = descriptions[0] + policyTerm.createPreRenewalDirectionHistoryDescription(originalPreRenewalDirection) + " "
    }
    
    // 'onChange' attribute on PostOnChange at PreRenewalDirectionPage.pcf: line 117, column 212
    function onChange_32 () : void {
      descriptions[0] = descriptions[0] + policyTerm.createPreRenewalAssignmentHistoryDescription(policy.getUserRoleAssignmentByRole(typekey.UserRole.TC_PRERENEWALOWNER)) + " "
    }
    
    // 'onChange' attribute on PostOnChange at PreRenewalDirectionPage.pcf: line 141, column 159
    function onChange_45 () : void {
      descriptions[0] = descriptions[0] + policyTerm.createNonRenewalReasonHistoryDescription(originalNonRenewReason) + " "
    }
    
    // 'onChange' attribute on PostOnChange at PreRenewalDirectionPage.pcf: line 152, column 152
    function onChange_55 () : void {
      descriptions[0] = descriptions[0] + policyTerm.createNonRenewalAdditionalExplanationHistoryDescription() + " "
    }
    
    // 'onPick' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function onPick_36 (PickedValue :  gw.api.assignment.Assignee) : void {
      policy.PreRenewalOwner = PickedValue as gw.api.assignment.Assignee
    }
    
    // 'optionLabel' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function optionLabel_75 (VALUE :  gw.api.domain.linkedobject.LinkedObjectContainer) : java.lang.String {
      return note.getLevelDisplayString(VALUE)
    }
    
    // 'parent' attribute on Page (id=PreRenewalDirectionPage) at PreRenewalDirectionPage.pcf: line 13, column 65
    static function parent_96 (asOfDate :  java.util.Date, policy :  Policy, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod, asOfDate)
    }
    
    // 'title' attribute on TitleBar at PreRenewalDirectionPage.pcf: line 52, column 240
    function title_5 () : java.lang.String {
      return DisplayKey.get("Web.PreRenewalDirection.ScreenTitle", gw.api.util.StringUtil.formatDate(policyPeriod.PeriodStart, "short"), gw.api.util.StringUtil.formatDate(policyPeriod.PeriodEnd, "short"))
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function valueRange_16 () : java.lang.Object {
      return policyTerm.AvailablePreRenewalDirectionOptions
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function valueRange_27 () : java.lang.Object {
      return tdic.web.pcf.helper.PolicyRenewalScreenHelper.getNonWCRenewalDirections(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function valueRange_40 () : java.lang.Object {
      return policy.getSuggestedPreRenewalOwners(policyTerm.PreRenewalDirection)
    }
    
    // 'valueRange' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function valueRange_50 () : java.lang.Object {
      return tdic.web.pcf.helper.PolicyRenewalScreenHelper.getFilteredNonRenewReasonCodes(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function valueRange_68 () : java.lang.Object {
      return tdic.web.pcf.helper.PolicyRenewalScreenHelper.getSubjectSelections(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function valueRange_76 () : java.lang.Object {
      return note.generateLevels(null, policy, null, null)
    }
    
    // 'value' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function valueRoot_15 () : java.lang.Object {
      return policyTerm
    }
    
    // 'value' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function valueRoot_39 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at PreRenewalDirectionPage.pcf: line 168, column 52
    function valueRoot_63 () : java.lang.Object {
      return note
    }
    
    // 'value' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function value_37 () : gw.api.assignment.Assignee {
      return policy.PreRenewalOwner
    }
    
    // 'value' attribute on TextAreaInput (id=NonRenewAddExpl_Input) at PreRenewalDirectionPage.pcf: line 150, column 123
    function value_57 () : java.lang.String {
      return policyTerm.NonRenewAddExplanation
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at PreRenewalDirectionPage.pcf: line 168, column 52
    function value_62 () : typekey.NoteTopicType {
      return note.Topic
    }
    
    // 'value' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function value_65 () : String {
      return note.Subject
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function value_73 () : gw.api.domain.linkedobject.LinkedObjectContainer {
      return note.Level
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityLevel_Input) at PreRenewalDirectionPage.pcf: line 194, column 55
    function value_81 () : typekey.NoteSecurityType {
      return note.SecurityType
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at PreRenewalDirectionPage.pcf: line 201, column 36
    function value_86 () : java.lang.String {
      return note.Body
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function verifyValueRangeIsAllowedType_17 ($$arg :  typekey.PreRenewalDirection[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function verifyValueRangeIsAllowedType_28 ($$arg :  typekey.PreRenewalDirection[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function verifyValueRangeIsAllowedType_41 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function verifyValueRangeIsAllowedType_51 ($$arg :  typekey.NonRenewalCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function verifyValueRangeIsAllowedType_69 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function verifyValueRangeIsAllowedType_69 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function verifyValueRangeIsAllowedType_77 ($$arg :  gw.api.domain.linkedobject.LinkedObjectContainer[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function verifyValueRangeIsAllowedType_77 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function verifyValueRange_18 () : void {
      var __valueRangeArg = policyTerm.AvailablePreRenewalDirectionOptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function verifyValueRange_29 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.PolicyRenewalScreenHelper.getNonWCRenewalDirections(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function verifyValueRange_42 () : void {
      var __valueRangeArg = policy.getSuggestedPreRenewalOwners(policyTerm.PreRenewalDirection)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_41(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function verifyValueRange_52 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.PolicyRenewalScreenHelper.getFilteredNonRenewReasonCodes(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Subject_Input) at PreRenewalDirectionPage.pcf: line 176, column 37
    function verifyValueRange_70 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.PolicyRenewalScreenHelper.getSubjectSelections(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_69(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function verifyValueRange_78 () : void {
      var __valueRangeArg = note.generateLevels(null, policy, null, null)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_77(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=PreRenewalDirection_TDIC_Input) at PreRenewalDirectionPage.pcf: line 83, column 54
    function visible_12 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on RangeInput (id=PreRenewalDirection_Input) at PreRenewalDirectionPage.pcf: line 96, column 58
    function visible_22 () : java.lang.Boolean {
      return not policyPeriod.WC7LineExists
    }
    
    // 'visible' attribute on RangeInput (id=PreRenewalOwner_Input) at PreRenewalDirectionPage.pcf: line 110, column 220
    function visible_35 () : java.lang.Boolean {
      return policyTerm.PreRenewalDirection==TC_NONRENEWREFER or policyTerm.PreRenewalDirection==TC_UNDERWRITER or policyTerm.PreRenewalDirection==TC_ASSISTANT or policyTerm.PreRenewalDirection==TC_CUSTREP
    }
    
    // 'visible' attribute on RangeInput (id=NonRenewReason_Input) at PreRenewalDirectionPage.pcf: line 139, column 123
    function visible_46 () : java.lang.Boolean {
      return policyTerm.PreRenewalDirection==TC_NONRENEW or policyTerm.PreRenewalDirection==TC_NONRENEWREFER
    }
    
    // 'visible' attribute on ToolbarButton (id=PreRenewalDirectionScreen_DeleteButton) at PreRenewalDirectionPage.pcf: line 61, column 136
    function visible_7 () : java.lang.Boolean {
      return originalPreRenewalDirection != null and policyPeriod.Policy.OpenRenewalJob == null and perm.System.editprerenewal
    }
    
    // 'visible' attribute on RangeInput (id=RelatedTo_Input) at PreRenewalDirectionPage.pcf: line 186, column 43
    function visible_72 () : java.lang.Boolean {
      return policy != null
    }
    
    override property get CurrentLocation () : pcf.PreRenewalDirectionPage {
      return super.CurrentLocation as pcf.PreRenewalDirectionPage
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get descriptions () : String[] {
      return getVariableValue("descriptions", 0) as String[]
    }
    
    property set descriptions ($arg :  String[]) {
      setVariableValue("descriptions", 0, $arg)
    }
    
    property get note () : Note {
      return getVariableValue("note", 0) as Note
    }
    
    property set note ($arg :  Note) {
      setVariableValue("note", 0, $arg)
    }
    
    property get originalNonRenewReason () : typekey.NonRenewalCode {
      return getVariableValue("originalNonRenewReason", 0) as typekey.NonRenewalCode
    }
    
    property set originalNonRenewReason ($arg :  typekey.NonRenewalCode) {
      setVariableValue("originalNonRenewReason", 0, $arg)
    }
    
    property get originalPreRenewalDirection () : typekey.PreRenewalDirection {
      return getVariableValue("originalPreRenewalDirection", 0) as typekey.PreRenewalDirection
    }
    
    property set originalPreRenewalDirection ($arg :  typekey.PreRenewalDirection) {
      setVariableValue("originalPreRenewalDirection", 0, $arg)
    }
    
    property get policy () : Policy {
      return getVariableValue("policy", 0) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setVariableValue("policy", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyTerm () : PolicyTerm {
      return getVariableValue("policyTerm", 0) as PolicyTerm
    }
    
    property set policyTerm ($arg :  PolicyTerm) {
      setVariableValue("policyTerm", 0, $arg)
    }
    
    function removePrerenewal() {
      var _policyTerm = policyTerm
      gw.transaction.Transaction.runWithNewBundle( \ b -> {
        var thisTerm = b.loadBean(_policyTerm.ID) as PolicyTerm
        thisTerm.removePreRenewalDirection()
      })
      CurrentLocation.cancel()
    }
    
    // Users with selectnonrenew permission can choose any PreRenewalDirection value
    // Other users cannot choose "nonrenew" and "nonrenewrefer"
    function filterPreRenewalDirection( code : PreRenewalDirection ) : boolean {
      return perm.System.selectnonrenew or (code != TC_NONRENEW and code != TC_NONRENEWREFER)
    }
    
    // Creates a new pre-renewal note on the policy with "internalonly" security
    function createPreRenewalNote() : Note {
      var newNote = policy.newPrerenewalNote()
      newNote.SecurityType = NoteSecurityType.TC_INTERNALONLY
      return newNote
    }
    
    function createCustomHistoryEvent() {
      var _description = descriptions[0]
      policyTerm.createCustomHistoryEvent(CustomHistoryType.TC_PRERENEWAL, \ -> _description)
    }
    
    
  }
  
  
}