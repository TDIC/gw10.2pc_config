package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RateTxDetailAggRowSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailAggRowSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RateTxDetailAggRowSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7RateTxDetailAggRowSet.pcf: line 13, column 42
    function initialValue_0 () : entity.WC7JurisdictionCost {
      return aggTx.WC7Cost as WC7JurisdictionCost
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TxAmount_Cell) at WC7RateTxDetailAggRowSet.pcf: line 41, column 38
    function valueRoot_12 () : java.lang.Object {
      return aggTx
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RateTxDetailAggRowSet.pcf: line 23, column 39
    function valueRoot_2 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at WC7RateTxDetailAggRowSet.pcf: line 23, column 39
    function value_1 () : java.lang.String {
      return cost.ClassCode
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TxAmount_Cell) at WC7RateTxDetailAggRowSet.pcf: line 41, column 38
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return aggTx.AmountBilling
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7RateTxDetailAggRowSet.pcf: line 27, column 39
    function value_4 () : java.lang.String {
      return cost.Description
    }
    
    // 'value' attribute on TextCell (id=Basis_Cell) at WC7RateTxDetailAggRowSet.pcf: line 32, column 39
    function value_7 () : java.lang.String {
      return cost.Basis == 0 ? "" : cost.Basis.DisplayValue
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at WC7RateTxDetailAggRowSet.pcf: line 37, column 39
    function value_9 () : java.lang.String {
      return cost.ActualAdjRate == 0 ? "" : cost.ActualAdjRate.DisplayValue
    }
    
    // 'visible' attribute on Row (id=AggTxAmountRow) at WC7RateTxDetailAggRowSet.pcf: line 16, column 48
    function visible_14 () : java.lang.Boolean {
      return not aggTx.AmountBilling.IsZero
    }
    
    property get aggTx () : WC7Transaction {
      return getRequireValue("aggTx", 0) as WC7Transaction
    }
    
    property set aggTx ($arg :  WC7Transaction) {
      setRequireValue("aggTx", 0, $arg)
    }
    
    property get cost () : entity.WC7JurisdictionCost {
      return getVariableValue("cost", 0) as entity.WC7JurisdictionCost
    }
    
    property set cost ($arg :  entity.WC7JurisdictionCost) {
      setVariableValue("cost", 0, $arg)
    }
    
    
  }
  
  
}