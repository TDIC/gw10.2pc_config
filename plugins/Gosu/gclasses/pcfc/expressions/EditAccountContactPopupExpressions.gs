package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/EditAccountContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditAccountContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/EditAccountContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditAccountContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountContact :  AccountContact) : int {
      return 0
    }
    
    static function __constructorIndex (accountContact :  AccountContact, startInEditMode :  boolean) : int {
      return 1
    }
    
    static function __constructorIndex (accountContact :  AccountContact, resolvingConflict :  boolean, startInEditMode :  boolean) : int {
      return 2
    }
    
    static function __constructorIndex (accountContact :  AccountContact, resolvingConflict :  boolean, showRolesTab :  boolean, showAddressTools :  boolean) : int {
      return 3
    }
    
    // 'action' attribute on ToolbarButton (id=Sync) at EditAccountContactPopup.pcf: line 75, column 40
    function action_7 () : void {
      copyFromAddressBook()
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at EditAccountContactPopup.pcf: line 80, column 71
    function action_8 () : void {
      if (impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(accountContact.Contact, impactedPolicies_TDIC)}
    }
    
    // 'beforeCommit' attribute on Popup (id=EditAccountContactPopup) at EditAccountContactPopup.pcf: line 11, column 48
    function beforeCommit_13 (pickedValue :  AccountContact) : void {
      helper.validateAndUpdateStatusOfAddresses(contact)
    }
    
    // 'def' attribute on PanelRef at EditAccountContactPopup.pcf: line 90, column 26
    function def_onEnter_11 (def :  pcf.AccountContactCV) : void {
      def.onEnter(accountContact,true, showRolesTab)
    }
    
    // 'def' attribute on PanelRef at EditAccountContactPopup.pcf: line 90, column 26
    function def_refreshVariables_12 (def :  pcf.AccountContactCV) : void {
      def.refreshVariables(accountContact,true, showRolesTab)
    }
    
    // 'initialValue' attribute on Variable at EditAccountContactPopup.pcf: line 31, column 30
    function initialValue_0 () : entity.Contact {
      return accountContact.Contact
    }
    
    // 'initialValue' attribute on Variable at EditAccountContactPopup.pcf: line 39, column 76
    function initialValue_1 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at EditAccountContactPopup.pcf: line 59, column 50
    function initialValue_2 () : java.util.List<PolicyPeriod> {
      return contact.AssociationFinder.findLatestBoundPolicyPeriods()
    }
    
    // 'initialValue' attribute on Variable at EditAccountContactPopup.pcf: line 63, column 25
    function initialValue_3 () : Account[] {
      return contact.AssociationFinder.findAccounts().where(\ elt -> elt != accountContact.Account and (perm.Account.view(elt) or revealAccountsIgnoringViewPermission_TDIC))
    }
    
    // 'label' attribute on Verbatim (id=ContactUsageImpact_TJT) at EditAccountContactPopup.pcf: line 87, column 25
    function label_10 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisContactMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // EditButtons at EditAccountContactPopup.pcf: line 69, column 39
    function label_5 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at EditAccountContactPopup.pcf: line 69, column 39
    function pickValue_4 () : AccountContact {
      return accountContact
    }
    
    // 'title' attribute on Popup (id=EditAccountContactPopup) at EditAccountContactPopup.pcf: line 11, column 48
    static function title_14 (accountContact :  AccountContact, resolvingConflict :  boolean, showAddressTools :  boolean, showRolesTab :  boolean, startInEditMode :  boolean) : java.lang.Object {
      return accountContact.Contact.DisplayName
    }
    
    // 'visible' attribute on ToolbarButton (id=Sync) at EditAccountContactPopup.pcf: line 75, column 40
    function visible_6 () : java.lang.Boolean {
      return resolvingConflict
    }
    
    // 'visible' attribute on Verbatim (id=ContactUsageImpact_TJT) at EditAccountContactPopup.pcf: line 87, column 25
    function visible_9 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and (impactedAccounts_TDIC.Count > 0 or impactedPolicies_TDIC.Count > 0)
    }
    
    override property get CurrentLocation () : pcf.EditAccountContactPopup {
      return super.CurrentLocation as pcf.EditAccountContactPopup
    }
    
    property get accountContact () : AccountContact {
      return getVariableValue("accountContact", 0) as AccountContact
    }
    
    property set accountContact ($arg :  AccountContact) {
      setVariableValue("accountContact", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get displayMembershipCheckError () : boolean {
      return getVariableValue("displayMembershipCheckError", 0) as java.lang.Boolean
    }
    
    property set displayMembershipCheckError ($arg :  boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedAccounts_TDIC () : Account[] {
      return getVariableValue("impactedAccounts_TDIC", 0) as Account[]
    }
    
    property set impactedAccounts_TDIC ($arg :  Account[]) {
      setVariableValue("impactedAccounts_TDIC", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.List<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.List<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get resolvingConflict () : boolean {
      return getVariableValue("resolvingConflict", 0) as java.lang.Boolean
    }
    
    property set resolvingConflict ($arg :  boolean) {
      setVariableValue("resolvingConflict", 0, $arg)
    }
    
    property get revealAccountsIgnoringViewPermission_TDIC () : boolean {
      return getVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0) as java.lang.Boolean
    }
    
    property set revealAccountsIgnoringViewPermission_TDIC ($arg :  boolean) {
      setVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0, $arg)
    }
    
    property get showAddressTools () : boolean {
      return getVariableValue("showAddressTools", 0) as java.lang.Boolean
    }
    
    property set showAddressTools ($arg :  boolean) {
      setVariableValue("showAddressTools", 0, $arg)
    }
    
    property get showRolesTab () : boolean {
      return getVariableValue("showRolesTab", 0) as java.lang.Boolean
    }
    
    property set showRolesTab ($arg :  boolean) {
      setVariableValue("showRolesTab", 0, $arg)
    }
    
    property get startInEditMode () : boolean {
      return getVariableValue("startInEditMode", 0) as java.lang.Boolean
    }
    
    property set startInEditMode ($arg :  boolean) {
      setVariableValue("startInEditMode", 0, $arg)
    }
    
    function copyFromAddressBook(){
      contact.syncWithAddressBook()
      //BundleUtil.evict(contact)
      CurrentLocation.cancel()
      EditAccountContactPopup.push(accountContact)
    }
    
    
  }
  
  
}