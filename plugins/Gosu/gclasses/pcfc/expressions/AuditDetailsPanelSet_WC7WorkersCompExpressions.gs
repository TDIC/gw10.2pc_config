package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AuditDetailsPanelSet_WC7WorkersCompExpressions {
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AuditDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 15, column 64
    function initialValue_0 () : gw.util.concurrent.LockingLazyVar<Boolean> {
      return gw.util.concurrent.LockingLazyVar.make(\ -> policyPeriod.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 20, column 23
    function initialValue_1 () : Boolean {
      return openForEditInit_TDIC.get() and policyPeriod.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 24, column 23
    function initialValue_2 () : WC7Line {
      return policyPeriod.WC7Line
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 28, column 40
    function initialValue_3 () : entity.WC7Jurisdiction[] {
      return policyPeriod.WC7Line.WC7Jurisdictions
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 32, column 23
    function initialValue_4 () : boolean {
      return policyPeriod.Audit.AuditInformation.AuditScheduleType == typekey.AuditScheduleType.TC_PREMIUMREPORT
    }
    
    // 'sortBy' attribute on IteratorSort at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 40, column 24
    function sortBy_5 (jurisdiction :  entity.WC7Jurisdiction) : java.lang.Object {
      return jurisdiction.Jurisdiction
    }
    
    // 'value' attribute on PanelIterator (id=JurisdictionPanel) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 37, column 44
    function value_239 () : entity.WC7Jurisdiction[] {
      return jurisdictions
    }
    
    property get isPremiumReport () : boolean {
      return getVariableValue("isPremiumReport", 0) as java.lang.Boolean
    }
    
    property set isPremiumReport ($arg :  boolean) {
      setVariableValue("isPremiumReport", 0, $arg)
    }
    
    property get jurisdictions () : entity.WC7Jurisdiction[] {
      return getVariableValue("jurisdictions", 0) as entity.WC7Jurisdiction[]
    }
    
    property set jurisdictions ($arg :  entity.WC7Jurisdiction[]) {
      setVariableValue("jurisdictions", 0, $arg)
    }
    
    property get openForEditInit_TDIC () : gw.util.concurrent.LockingLazyVar<Boolean> {
      return getVariableValue("openForEditInit_TDIC", 0) as gw.util.concurrent.LockingLazyVar<Boolean>
    }
    
    property set openForEditInit_TDIC ($arg :  gw.util.concurrent.LockingLazyVar<Boolean>) {
      setVariableValue("openForEditInit_TDIC", 0, $arg)
    }
    
    property get openForEdit_TDIC () : Boolean {
      return getVariableValue("openForEdit_TDIC", 0) as Boolean
    }
    
    property set openForEdit_TDIC ($arg :  Boolean) {
      setVariableValue("openForEdit_TDIC", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get wcLine () : WC7Line {
      return getVariableValue("wcLine", 0) as WC7Line
    }
    
    property set wcLine ($arg :  WC7Line) {
      setVariableValue("wcLine", 0, $arg)
    }
    
    function validateAuditedBasis(wcCovEmp : WC7CoveredEmployee, auditedBasis : Integer) : String {
      if(policyPeriod.Audit.AuditInformation.ActualAuditMethod == AuditMethod.TC_ESTIMATED) {
        var estimatedBasis : Integer = gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcCovEmp)
        if(estimatedBasis != 0 and (auditedBasis > (estimatedBasis * 3))) {   //Audited Basis cannot exceed 300% of the Estimated Basis
          return DisplayKey.get("TDIC.Web.AuditWizard.AuditedBasis.Validate300Percentage")
        }
      }
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PanelIteratorEntry2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 195, column 49
    function initialValue_63 () : Integer {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getTotalAuditedBasis_TDIC(wcLine, aClassCode, ratingPeriod)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 199, column 48
    function initialValue_64 () : String {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getClassCodeDescription_TDIC(wcLine,aClassCode)
    }
    
    // RowIterator (id=TotalBasis) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 191, column 62
    function initializeVariables_69 () : void {
        totalAuditedBasis = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getTotalAuditedBasis_TDIC(wcLine, aClassCode, ratingPeriod);
  classCodeDesc = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getClassCodeDescription_TDIC(wcLine,aClassCode);

    }
    
    // 'value' attribute on TextCell (id=TotalClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 206, column 55
    function value_65 () : java.lang.String {
      return aClassCode
    }
    
    // 'value' attribute on TextCell (id=TotalAuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 216, column 56
    function value_67 () : Integer {
      return totalAuditedBasis
    }
    
    property get aClassCode () : String {
      return getIteratedValue(3) as String
    }
    
    property get classCodeDesc () : String {
      return getVariableValue("classCodeDesc", 3) as String
    }
    
    property set classCodeDesc ($arg :  String) {
      setVariableValue("classCodeDesc", 3, $arg)
    }
    
    property get totalAuditedBasis () : Integer {
      return getVariableValue("totalAuditedBasis", 3) as Integer
    }
    
    property set totalAuditedBasis ($arg :  Integer) {
      setVariableValue("totalAuditedBasis", 3, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends PanelIteratorEntry3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 293, column 66
    function defaultSetter_106 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcFedCovEmp.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 287, column 52
    function label_101 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 260, column 70
    function valueRoot_85 () : java.lang.Object {
      return wcFedCovEmp
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 266, column 65
    function valueRoot_88 () : java.lang.Object {
      return wcFedCovEmp.ClassCode
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 287, column 52
    function value_102 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcFedCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 293, column 66
    function value_105 () : java.lang.Integer {
      return wcFedCovEmp.AuditedAmount
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 260, column 70
    function value_84 () : entity.PolicyLocation {
      return wcFedCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 266, column 65
    function value_87 () : java.lang.String {
      return wcFedCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 271, column 65
    function value_90 () : java.lang.String {
      return wcFedCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 277, column 123
    function value_94 () : java.lang.Boolean {
      return wcFedCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 282, column 53
    function value_98 () : java.lang.Boolean {
      return wcFedCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 277, column 123
    function visible_93 () : java.lang.Boolean {
      return wcFedCovEmp.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 277, column 123
    function visible_96 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get wcFedCovEmp () : WC7FedCoveredEmployee {
      return getIteratedValue(3) as WC7FedCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends PanelIteratorEntry4ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 370, column 66
    function defaultSetter_144 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcMaritimeCovEmp.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 364, column 52
    function label_139 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 337, column 70
    function valueRoot_123 () : java.lang.Object {
      return wcMaritimeCovEmp
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 343, column 65
    function valueRoot_126 () : java.lang.Object {
      return wcMaritimeCovEmp.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 337, column 70
    function value_122 () : entity.PolicyLocation {
      return wcMaritimeCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 343, column 65
    function value_125 () : java.lang.String {
      return wcMaritimeCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 348, column 65
    function value_128 () : java.lang.String {
      return wcMaritimeCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 354, column 128
    function value_132 () : java.lang.Boolean {
      return wcMaritimeCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 359, column 53
    function value_136 () : java.lang.Boolean {
      return wcMaritimeCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 364, column 52
    function value_140 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcMaritimeCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 370, column 66
    function value_143 () : java.lang.Integer {
      return wcMaritimeCovEmp.AuditedAmount
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 354, column 128
    function visible_131 () : java.lang.Boolean {
      return wcMaritimeCovEmp.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 354, column 128
    function visible_134 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get wcMaritimeCovEmp () : WC7MaritimeCoveredEmployee {
      return getIteratedValue(3) as WC7MaritimeCoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends PanelIteratorEntry5ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 441, column 66
    function defaultSetter_178 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcWaiverOfSubro.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 435, column 52
    function label_173 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 413, column 65
    function valueRoot_160 () : java.lang.Object {
      return wcWaiverOfSubro.ClassCode
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 418, column 65
    function valueRoot_163 () : java.lang.Object {
      return wcWaiverOfSubro
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 413, column 65
    function value_159 () : java.lang.String {
      return wcWaiverOfSubro.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 418, column 65
    function value_162 () : java.lang.String {
      return wcWaiverOfSubro.JobID
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 424, column 65
    function value_165 () : java.lang.String {
      return wcWaiverOfSubro.ClassCode.Code
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 430, column 112
    function value_169 () : java.lang.Boolean {
      return wcWaiverOfSubro.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 435, column 52
    function value_174 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.waiverSubroBasisForRating(policyPeriod.Audit.AuditInformation, wcWaiverOfSubro)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 441, column 66
    function value_177 () : java.lang.Integer {
      return wcWaiverOfSubro.AuditedAmount
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 430, column 112
    function visible_168 () : java.lang.Boolean {
      return wcWaiverOfSubro.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 430, column 112
    function visible_171 () : java.lang.Boolean {
      return coverageInputSetHelper.isDiseaseLoadedColumnVisible(jurisdiction)
    }
    
    property get wcWaiverOfSubro () : WC7WaiverOfSubro {
      return getIteratedValue(3) as WC7WaiverOfSubro
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends PanelIteratorEntry6ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 507, column 66
    function defaultSetter_205 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcSupplDisease.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 501, column 52
    function label_200 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 485, column 70
    function valueRoot_192 () : java.lang.Object {
      return wcSupplDisease
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 491, column 65
    function valueRoot_195 () : java.lang.Object {
      return wcSupplDisease.DiseaseCode
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 485, column 70
    function value_191 () : entity.PolicyLocation {
      return wcSupplDisease.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 491, column 65
    function value_194 () : java.lang.String {
      return wcSupplDisease.DiseaseCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 496, column 65
    function value_197 () : java.lang.String {
      return wcSupplDisease.DiseaseCode.SupplDiseaseLoadingType
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 501, column 52
    function value_201 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.supplDiseaseBasisForRating(policyPeriod.Audit.AuditInformation, wcSupplDisease)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 507, column 66
    function value_204 () : java.lang.Integer {
      return wcSupplDisease.AuditedAmount
    }
    
    property get wcSupplDisease () : WC7SupplDiseaseExposure {
      return getIteratedValue(3) as WC7SupplDiseaseExposure
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry7ExpressionsImpl extends PanelIteratorEntry7ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 573, column 66
    function defaultSetter_232 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcAtomicEnergy.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 567, column 52
    function label_227 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 551, column 70
    function valueRoot_219 () : java.lang.Object {
      return wcAtomicEnergy
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 557, column 65
    function valueRoot_222 () : java.lang.Object {
      return wcAtomicEnergy.ClassCode
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 551, column 70
    function value_218 () : entity.PolicyLocation {
      return wcAtomicEnergy.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 557, column 65
    function value_221 () : java.lang.String {
      return wcAtomicEnergy.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 562, column 65
    function value_224 () : java.lang.String {
      return wcAtomicEnergy.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 567, column 52
    function value_228 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.atomicEnergyBasisForRating(policyPeriod.Audit.AuditInformation, wcAtomicEnergy)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 573, column 66
    function value_231 () : java.lang.Integer {
      return wcAtomicEnergy.AuditedAmount
    }
    
    property get wcAtomicEnergy () : WC7AtomicEnergyExposure {
      return getIteratedValue(3) as WC7AtomicEnergyExposure
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PanelIteratorEntry2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=NumEmplyees_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 143, column 56
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcCovEmp.AuditedNumEmployees_TDIC = (__VALUE_TO_SET as Integer)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 156, column 66
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      wcCovEmp.AuditedAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 148, column 52
    function label_45 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'requestValidationExpression' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 156, column 66
    function requestValidationExpression_49 (VALUE :  java.lang.Integer) : java.lang.Object {
      return validateAuditedBasis(wcCovEmp, VALUE)
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 112, column 70
    function valueRoot_28 () : java.lang.Object {
      return wcCovEmp
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 118, column 65
    function valueRoot_31 () : java.lang.Object {
      return wcCovEmp.ClassCode
    }
    
    // 'value' attribute on TextCell (id=BasisCalculation_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 167, column 66
    function valueRoot_58 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 112, column 70
    function value_27 () : entity.PolicyLocation {
      return wcCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 118, column 65
    function value_30 () : java.lang.String {
      return wcCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 123, column 65
    function value_33 () : java.lang.String {
      return wcCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 129, column 125
    function value_37 () : java.lang.Boolean {
      return wcCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=NumEmplyees_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 143, column 56
    function value_41 () : Integer {
      return wcCovEmp.AuditedNumEmployees_TDIC
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 148, column 52
    function value_46 () : int {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 156, column 66
    function value_50 () : java.lang.Integer {
      return wcCovEmp.AuditedAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 162, column 65
    function value_54 () : java.lang.String {
      return wcCovEmp.BasisType
    }
    
    // 'value' attribute on TextCell (id=BasisCalculation_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 167, column 66
    function value_57 () : CalculationMethod {
      return policyPeriod.RefundCalcMethod
    }
    
    // 'valueVisible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 129, column 125
    function visible_36 () : java.lang.Boolean {
      return wcCovEmp.ClassCode.DiseaseType
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 129, column 125
    function visible_39 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get wcCovEmp () : WC7CoveredEmployee {
      return getIteratedValue(3) as WC7CoveredEmployee
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 94, column 259
    function label_15 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 148, column 52
    function label_22 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 112, column 70
    function sortValue_16 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 118, column 65
    function sortValue_17 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 123, column 65
    function sortValue_18 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 129, column 125
    function sortValue_19 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=NumEmplyees_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 143, column 56
    function sortValue_21 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.AuditedNumEmployees_TDIC
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 148, column 52
    function sortValue_23 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 156, column 66
    function sortValue_24 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.AuditedAmount
    }
    
    // 'value' attribute on TextCell (id=BasisType_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 162, column 65
    function sortValue_25 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return wcCovEmp.BasisType
    }
    
    // 'value' attribute on TextCell (id=BasisCalculation_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 167, column 66
    function sortValue_26 (wcCovEmp :  WC7CoveredEmployee) : java.lang.Object {
      return policyPeriod.RefundCalcMethod
    }
    
    // 'value' attribute on TextCell (id=TotalClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 206, column 55
    function sortValue_61 (aClassCode :  String) : java.lang.Object {
      return aClassCode
    }
    
    // 'value' attribute on TextCell (id=TotalAuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 216, column 56
    function sortValue_62 (aClassCode :  String) : java.lang.Object {
      var totalAuditedBasis : Integer = (gw.pcf.WC7AuditDetailsPanelSetUIHelper.getTotalAuditedBasis_TDIC(wcLine, aClassCode, ratingPeriod))
var classCodeDesc : String = (gw.pcf.WC7AuditDetailsPanelSetUIHelper.getClassCodeDescription_TDIC(wcLine,aClassCode))
return totalAuditedBasis
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 104, column 91
    function value_60 () : java.util.List<entity.WC7CoveredEmployee> {
      return covEmpMap.get(ratingPeriod)
    }
    
    // 'value' attribute on RowIterator (id=TotalBasis) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 191, column 62
    function value_70 () : java.lang.String[] {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getClassCodes_TDIC(wcLine)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 129, column 125
    function visible_20 () : java.lang.Boolean {
      return gw.pcf.WC7ClassesInputSetUIHelper.isSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    // 'visible' attribute on DetailViewPanel at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 175, column 74
    function visible_71 () : java.lang.Boolean {
      return covEmpMap.get(ratingPeriod).Count > 1
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry3ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 242, column 259
    function label_74 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 287, column 52
    function label_81 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 260, column 70
    function sortValue_75 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 266, column 65
    function sortValue_76 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 271, column 65
    function sortValue_77 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 277, column 123
    function sortValue_78 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 282, column 53
    function sortValue_80 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 287, column 52
    function sortValue_82 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcFedCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 293, column 66
    function sortValue_83 (wcFedCovEmp :  WC7FedCoveredEmployee) : java.lang.Object {
      return wcFedCovEmp.AuditedAmount
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 252, column 94
    function value_109 () : java.util.List<entity.WC7FedCoveredEmployee> {
      return fedCovEmpMap.get(ratingPeriod)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 277, column 123
    function visible_79 () : java.lang.Boolean {
      return coverageInputSetHelper.isFedCovEmpSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry4ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 319, column 259
    function label_112 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 364, column 52
    function label_119 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 337, column 70
    function sortValue_113 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 343, column 65
    function sortValue_114 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 348, column 65
    function sortValue_115 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.ClassCode.ShortDesc
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 354, column 128
    function sortValue_116 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on CheckBoxCell (id=SupplementalDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 359, column 53
    function sortValue_118 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.SupplementalDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 364, column 52
    function sortValue_120 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.basisForRating(policyPeriod.Audit.AuditInformation, wcMaritimeCovEmp)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 370, column 66
    function sortValue_121 (wcMaritimeCovEmp :  WC7MaritimeCoveredEmployee) : java.lang.Object {
      return wcMaritimeCovEmp.AuditedAmount
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 329, column 99
    function value_147 () : java.util.List<entity.WC7MaritimeCoveredEmployee> {
      return maritimeCovEmpMap.get(ratingPeriod)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 354, column 128
    function visible_117 () : java.lang.Boolean {
      return coverageInputSetHelper.isMaritimeCovEmpSpecificDiseaseColumnVisible(jurisdiction)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry5ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 397, column 259
    function label_150 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 435, column 52
    function label_156 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 413, column 65
    function sortValue_151 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wcWaiverOfSubro.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=JobID_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 418, column 65
    function sortValue_152 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wcWaiverOfSubro.JobID
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 424, column 65
    function sortValue_153 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wcWaiverOfSubro.ClassCode.Code
    }
    
    // 'value' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 430, column 112
    function sortValue_154 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wcWaiverOfSubro.SpecificDiseaseLoaded
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 435, column 52
    function sortValue_157 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.waiverSubroBasisForRating(policyPeriod.Audit.AuditInformation, wcWaiverOfSubro)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 441, column 66
    function sortValue_158 (wcWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wcWaiverOfSubro.AuditedAmount
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 407, column 89
    function value_181 () : java.util.List<entity.WC7WaiverOfSubro> {
      return specificWaiverOfSubroMap.get(ratingPeriod)
    }
    
    // 'visible' attribute on CheckBoxCell (id=SpecificDiseaseLoaded_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 430, column 112
    function visible_155 () : java.lang.Boolean {
      return coverageInputSetHelper.isDiseaseLoadedColumnVisible(jurisdiction)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry6ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 467, column 259
    function label_184 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 501, column 52
    function label_188 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 485, column 70
    function sortValue_185 (wcSupplDisease :  WC7SupplDiseaseExposure) : java.lang.Object {
      return wcSupplDisease.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 491, column 65
    function sortValue_186 (wcSupplDisease :  WC7SupplDiseaseExposure) : java.lang.Object {
      return wcSupplDisease.DiseaseCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 496, column 65
    function sortValue_187 (wcSupplDisease :  WC7SupplDiseaseExposure) : java.lang.Object {
      return wcSupplDisease.DiseaseCode.SupplDiseaseLoadingType
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 501, column 52
    function sortValue_189 (wcSupplDisease :  WC7SupplDiseaseExposure) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.supplDiseaseBasisForRating(policyPeriod.Audit.AuditInformation, wcSupplDisease)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 507, column 66
    function sortValue_190 (wcSupplDisease :  WC7SupplDiseaseExposure) : java.lang.Object {
      return wcSupplDisease.AuditedAmount
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 477, column 96
    function value_208 () : java.util.List<entity.WC7SupplDiseaseExposure> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getSupplementaryDiseaseInJurisdiction(wcLine, ratingPeriod)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry7ExpressionsImpl extends PanelIteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 533, column 259
    function label_211 () : java.lang.String {
      return DisplayKey.get("Web.AuditWizard.Details.PeriodLabel", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"), gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
    }
    
    // 'label' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 567, column 52
    function label_215 () : java.lang.Object {
      return policyPeriod.Audit.AuditInformation.IsRevision ? DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") : DisplayKey.get("Web.AuditWizard.EstBasis")
    }
    
    // 'value' attribute on TextCell (id=Location_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 551, column 70
    function sortValue_212 (wcAtomicEnergy :  WC7AtomicEnergyExposure) : java.lang.Object {
      return wcAtomicEnergy.Location
    }
    
    // 'value' attribute on TextCell (id=ClassCode_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 557, column 65
    function sortValue_213 (wcAtomicEnergy :  WC7AtomicEnergyExposure) : java.lang.Object {
      return wcAtomicEnergy.ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 562, column 65
    function sortValue_214 (wcAtomicEnergy :  WC7AtomicEnergyExposure) : java.lang.Object {
      return wcAtomicEnergy.ClassCode.ShortDesc
    }
    
    // 'value' attribute on TextCell (id=EstPayroll_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 567, column 52
    function sortValue_216 (wcAtomicEnergy :  WC7AtomicEnergyExposure) : java.lang.Object {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.atomicEnergyBasisForRating(policyPeriod.Audit.AuditInformation, wcAtomicEnergy)
    }
    
    // 'value' attribute on TextCell (id=AuditedAmt_Cell) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 573, column 66
    function sortValue_217 (wcAtomicEnergy :  WC7AtomicEnergyExposure) : java.lang.Object {
      return wcAtomicEnergy.AuditedAmount
    }
    
    // 'value' attribute on RowIterator at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 543, column 96
    function value_235 () : java.util.List<entity.WC7AtomicEnergyExposure> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getAtomicEnergyInJurisdiction(wcLine, ratingPeriod)
    }
    
    property get ratingPeriod () : gw.lob.wc7.rating.WC7RatingPeriod {
      return getIteratedValue(2) as gw.lob.wc7.rating.WC7RatingPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditDetailsPanelSet.WC7WorkersComp.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends AuditDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 60, column 122
    function initialValue_10 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7WaiverOfSubro>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getSpecificWaiverOfSubroExposureAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 64, column 129
    function initialValue_11 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7SupplDiseaseExposure>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getSupplementaryDiseaseExposureAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 68, column 129
    function initialValue_12 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7AtomicEnergyExposure>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getAtomicEnergyExposureAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 72, column 52
    function initialValue_13 () : gw.pcf.WC7CoverageInputSetUIHelper {
      return new gw.pcf.WC7CoverageInputSetUIHelper()
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 44, column 53
    function initialValue_6 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return jurisdiction.InForceRatingPeriods.toTypedArray()
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 48, column 124
    function initialValue_7 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7CoveredEmployee>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 52, column 127
    function initialValue_8 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7FedCoveredEmployee>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getFedCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // 'initialValue' attribute on Variable at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 56, column 132
    function initialValue_9 () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7MaritimeCoveredEmployee>> {
      return gw.pcf.WC7AuditDetailsPanelSetUIHelper.getMaritimeCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods)
    }
    
    // PanelIterator (id=JurisdictionPanel) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 37, column 44
    function initializeVariables_238 () : void {
        ratingPeriods = jurisdiction.InForceRatingPeriods.toTypedArray();
  covEmpMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods);
  fedCovEmpMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getFedCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods);
  maritimeCovEmpMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getMaritimeCoveredEmployeesAcrossAllRatingPeriods(wcLine, ratingPeriods);
  specificWaiverOfSubroMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getSpecificWaiverOfSubroExposureAcrossAllRatingPeriods(wcLine, ratingPeriods);
  supplDiseaseCovEmpMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getSupplementaryDiseaseExposureAcrossAllRatingPeriods(wcLine, ratingPeriods);
  atomicEnergyCovEmpMap = gw.pcf.WC7AuditDetailsPanelSetUIHelper.getAtomicEnergyExposureAcrossAllRatingPeriods(wcLine, ratingPeriods);
  coverageInputSetHelper = new gw.pcf.WC7CoverageInputSetUIHelper();

    }
    
    // 'title' attribute on TitleBar (id=StateTitle) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 77, column 45
    function title_14 () : java.lang.String {
      return jurisdiction.DisplayName
    }
    
    // 'value' attribute on PanelIterator (id=CoveredEmployeePanelIterator) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 90, column 69
    function value_72 () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return ratingPeriods
    }
    
    // 'visible' attribute on PanelSet at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 228, column 48
    function visible_111 () : java.lang.Boolean {
      return not fedCovEmpMap.Empty
    }
    
    // 'visible' attribute on PanelSet at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 305, column 53
    function visible_149 () : java.lang.Boolean {
      return not maritimeCovEmpMap.Empty
    }
    
    // 'visible' attribute on PanelSet (id=WaiverOfSubroPanelSet) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 383, column 84
    function visible_183 () : java.lang.Boolean {
      return not specificWaiverOfSubroMap.Empty and not isPremiumReport
    }
    
    // 'visible' attribute on PanelSet (id=SupplementaryDiseasePanelSet) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 454, column 82
    function visible_210 () : java.lang.Boolean {
      return not supplDiseaseCovEmpMap.Empty  and not isPremiumReport
    }
    
    // 'visible' attribute on PanelSet (id=AtomicEnergyPanelSet) at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 520, column 81
    function visible_237 () : java.lang.Boolean {
      return not atomicEnergyCovEmpMap.Empty and not isPremiumReport
    }
    
    // 'visible' attribute on PanelSet at AuditDetailsPanelSet.WC7WorkersComp.pcf: line 81, column 45
    function visible_73 () : java.lang.Boolean {
      return not covEmpMap.Empty
    }
    
    property get atomicEnergyCovEmpMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7AtomicEnergyExposure>> {
      return getVariableValue("atomicEnergyCovEmpMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7AtomicEnergyExposure>>
    }
    
    property set atomicEnergyCovEmpMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7AtomicEnergyExposure>>) {
      setVariableValue("atomicEnergyCovEmpMap", 1, $arg)
    }
    
    property get covEmpMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7CoveredEmployee>> {
      return getVariableValue("covEmpMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7CoveredEmployee>>
    }
    
    property set covEmpMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7CoveredEmployee>>) {
      setVariableValue("covEmpMap", 1, $arg)
    }
    
    property get coverageInputSetHelper () : gw.pcf.WC7CoverageInputSetUIHelper {
      return getVariableValue("coverageInputSetHelper", 1) as gw.pcf.WC7CoverageInputSetUIHelper
    }
    
    property set coverageInputSetHelper ($arg :  gw.pcf.WC7CoverageInputSetUIHelper) {
      setVariableValue("coverageInputSetHelper", 1, $arg)
    }
    
    property get fedCovEmpMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7FedCoveredEmployee>> {
      return getVariableValue("fedCovEmpMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7FedCoveredEmployee>>
    }
    
    property set fedCovEmpMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7FedCoveredEmployee>>) {
      setVariableValue("fedCovEmpMap", 1, $arg)
    }
    
    property get jurisdiction () : entity.WC7Jurisdiction {
      return getIteratedValue(1) as entity.WC7Jurisdiction
    }
    
    property get maritimeCovEmpMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7MaritimeCoveredEmployee>> {
      return getVariableValue("maritimeCovEmpMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7MaritimeCoveredEmployee>>
    }
    
    property set maritimeCovEmpMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7MaritimeCoveredEmployee>>) {
      setVariableValue("maritimeCovEmpMap", 1, $arg)
    }
    
    property get ratingPeriods () : gw.lob.wc7.rating.WC7RatingPeriod[] {
      return getVariableValue("ratingPeriods", 1) as gw.lob.wc7.rating.WC7RatingPeriod[]
    }
    
    property set ratingPeriods ($arg :  gw.lob.wc7.rating.WC7RatingPeriod[]) {
      setVariableValue("ratingPeriods", 1, $arg)
    }
    
    property get specificWaiverOfSubroMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7WaiverOfSubro>> {
      return getVariableValue("specificWaiverOfSubroMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7WaiverOfSubro>>
    }
    
    property set specificWaiverOfSubroMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7WaiverOfSubro>>) {
      setVariableValue("specificWaiverOfSubroMap", 1, $arg)
    }
    
    property get supplDiseaseCovEmpMap () : java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7SupplDiseaseExposure>> {
      return getVariableValue("supplDiseaseCovEmpMap", 1) as java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7SupplDiseaseExposure>>
    }
    
    property set supplDiseaseCovEmpMap ($arg :  java.util.HashMap<gw.lob.wc7.rating.WC7RatingPeriod, java.util.List<entity.WC7SupplDiseaseExposure>>) {
      setVariableValue("supplDiseaseCovEmpMap", 1, $arg)
    }
    
    
  }
  
  
}