package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_PolicyInfo.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_PolicyInfoExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_PolicyInfo.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_PolicyInfoExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (period :  PolicyPeriod, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=PolicyFile_PolicyInfo) at PolicyFile_PolicyInfo.pcf: line 10, column 67
    function afterEnter_13 () : void {
      gw.api.web.PebblesUtil.addWebMessages(CurrentLocation, period.PolicyFileMessages)
    }
    
    // 'canVisit' attribute on Page (id=PolicyFile_PolicyInfo) at PolicyFile_PolicyInfo.pcf: line 10, column 67
    static function canVisit_14 (asOfDate :  java.util.Date, period :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.view(period) and perm.System.pfiledetails
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 24, column 75
    function def_onEnter_0 (def :  pcf.WarningsPanelSet) : void {
      def.onEnter(period.Policy.ContingencyWarningMessages)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 35, column 41
    function def_onEnter_11 (def :  pcf.AdditionalNamedInsuredsDV) : void {
      def.onEnter(period, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 26, column 48
    function def_onEnter_2 (def :  pcf.PolicyFile_PolicyInfoDV) : void {
      def.onEnter(period)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 29, column 41
    function def_onEnter_5 (def :  pcf.TDIC_WC7PolicyOwnerOfficerDV) : void {
      def.onEnter(period, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 32, column 41
    function def_onEnter_8 (def :  pcf.TDIC_BOPPolicyOwnerOfficerDV) : void {
      def.onEnter(period, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 24, column 75
    function def_refreshVariables_1 (def :  pcf.WarningsPanelSet) : void {
      def.refreshVariables(period.Policy.ContingencyWarningMessages)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 35, column 41
    function def_refreshVariables_12 (def :  pcf.AdditionalNamedInsuredsDV) : void {
      def.refreshVariables(period, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 26, column 48
    function def_refreshVariables_3 (def :  pcf.PolicyFile_PolicyInfoDV) : void {
      def.refreshVariables(period)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 29, column 41
    function def_refreshVariables_6 (def :  pcf.TDIC_WC7PolicyOwnerOfficerDV) : void {
      def.refreshVariables(period, false)
    }
    
    // 'def' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 32, column 41
    function def_refreshVariables_9 (def :  pcf.TDIC_BOPPolicyOwnerOfficerDV) : void {
      def.refreshVariables(period, false)
    }
    
    // 'parent' attribute on Page (id=PolicyFile_PolicyInfo) at PolicyFile_PolicyInfo.pcf: line 10, column 67
    static function parent_15 (asOfDate :  java.util.Date, period :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(period, asOfDate)
    }
    
    // 'visible' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 29, column 41
    function visible_4 () : java.lang.Boolean {
      return period.WC7LineExists
    }
    
    // 'visible' attribute on PanelRef at PolicyFile_PolicyInfo.pcf: line 32, column 41
    function visible_7 () : java.lang.Boolean {
      return period.BOPLineExists
    }
    
    override property get CurrentLocation () : pcf.PolicyFile_PolicyInfo {
      return super.CurrentLocation as pcf.PolicyFile_PolicyInfo
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getVariableValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    
  }
  
  
}