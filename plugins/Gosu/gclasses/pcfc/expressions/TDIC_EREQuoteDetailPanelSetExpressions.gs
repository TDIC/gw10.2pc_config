package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_EREQuoteDetailPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuoteDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_EREQuoteDetailPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TDIC_EREQuoteDetailPanelSet.pcf: line 12, column 49
    function def_onEnter_0 (def :  pcf.TDIC_EREQuoteTotalsDV) : void {
      def.onEnter(latestQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_EREQuoteDetailPanelSet.pcf: line 20, column 61
    function def_onEnter_2 (def :  pcf.TDIC_EREQuotePremiumsPanelSet) : void {
      def.onEnter(latestQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_EREQuoteDetailPanelSet.pcf: line 12, column 49
    function def_refreshVariables_1 (def :  pcf.TDIC_EREQuoteTotalsDV) : void {
      def.refreshVariables(latestQuote)
    }
    
    // 'def' attribute on PanelRef at TDIC_EREQuoteDetailPanelSet.pcf: line 20, column 61
    function def_refreshVariables_3 (def :  pcf.TDIC_EREQuotePremiumsPanelSet) : void {
      def.refreshVariables(latestQuote)
    }
    
    property get latestQuote () : GLEREQuickQuote_TDIC {
      return getRequireValue("latestQuote", 0) as GLEREQuickQuote_TDIC
    }
    
    property set latestQuote ($arg :  GLEREQuickQuote_TDIC) {
      setRequireValue("latestQuote", 0, $arg)
    }
    
    
  }
  
  
}