package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountLocationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountLocationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (theLocation :  AccountLocation, account :  Account, shouldEdit :  boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Standardize) at AccountLocationPopup.pcf: line 56, column 56
    function action_4 () : void {
      addressList = helper.validateAddress(theLocation, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); helper.displayExceptions()
    }
    
    // 'action' attribute on ToolbarButton (id=Override) at AccountLocationPopup.pcf: line 62, column 38
    function action_7 () : void {
      CurrentLocation.pickValueAndCommit(theLocation)
    }
    
    // 'afterEnter' attribute on Popup (id=AccountLocationPopup) at AccountLocationPopup.pcf: line 13, column 89
    function afterEnter_39 () : void {
      if (theLocation == null and shouldEdit) { theLocation = account.newLocation(); }
    }
    
    // 'available' attribute on ToolbarButton (id=Standardize) at AccountLocationPopup.pcf: line 56, column 56
    function available_2 () : java.lang.Boolean {
      return standardizeVisible
    }
    
    // 'available' attribute on ToolbarButton (id=Override) at AccountLocationPopup.pcf: line 62, column 38
    function available_5 () : java.lang.Boolean {
      return overrideVisible
    }
    
    // 'beforeCommit' attribute on Popup (id=AccountLocationPopup) at AccountLocationPopup.pcf: line 13, column 89
    function beforeCommit_40 (pickedValue :  AccountLocation) : void {
      if (!theLocation.New) {theLocation.validateStateAndCountryHaveNotChanged()} helper.updateAddressStatus(theLocation)
    }
    
    // 'canEdit' attribute on Popup (id=AccountLocationPopup) at AccountLocationPopup.pcf: line 13, column 89
    function canEdit_41 () : java.lang.Boolean {
      return shouldEdit
    }
    
    // 'def' attribute on InputSetRef at AccountLocationPopup.pcf: line 76, column 75
    function def_onEnter_12 (def :  pcf.AccountLocationDetailInputSet) : void {
      def.onEnter(theLocation, shouldEdit)
    }
    
    // 'def' attribute on InputSetRef at AccountLocationPopup.pcf: line 76, column 75
    function def_refreshVariables_13 (def :  pcf.AccountLocationDetailInputSet) : void {
      def.refreshVariables(theLocation, shouldEdit)
    }
    
    // 'infoBar' attribute on Popup (id=AccountLocationPopup) at AccountLocationPopup.pcf: line 13, column 89
    function infoBar_onEnter_42 (def :  pcf.AccountFileInfoBar) : void {
      def.onEnter(account)
    }
    
    // 'infoBar' attribute on Popup (id=AccountLocationPopup) at AccountLocationPopup.pcf: line 13, column 89
    function infoBar_refreshVariables_43 (def :  pcf.AccountFileInfoBar) : void {
      def.refreshVariables(account)
    }
    
    // 'initialValue' attribute on Variable at AccountLocationPopup.pcf: line 43, column 76
    function initialValue_0 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at AccountLocationPopup.pcf: line 47, column 53
    function initialValue_1 () : java.util.HashSet<PolicyPeriod> {
      return theLocation == null ? null : theLocation.getImpactedPoliciesForAccountLocationChange_TDIC()
    }
    
    // 'label' attribute on Verbatim at AccountLocationPopup.pcf: line 72, column 25
    function label_11 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisLocationMayImpactPolicies", impactedPolicies_TDIC.Count) + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // 'pickValue' attribute on EditButtons at AccountLocationPopup.pcf: line 66, column 42
    function pickValue_8 () : AccountLocation {
      return theLocation
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AccountLocationPopup.pcf: line 106, column 56
    function sortValue_15 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at AccountLocationPopup.pcf: line 110, column 56
    function sortValue_16 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AccountLocationPopup.pcf: line 114, column 48
    function sortValue_17 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at AccountLocationPopup.pcf: line 119, column 44
    function sortValue_18 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at AccountLocationPopup.pcf: line 123, column 54
    function sortValue_19 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at AccountLocationPopup.pcf: line 93, column 65
    function value_37 () : entity.Address[] {
      return addressList.toTypedArray()
    }
    
    // 'visible' attribute on Verbatim at AccountLocationPopup.pcf: line 72, column 25
    function visible_10 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    // 'visible' attribute on TitleBar (id=SelectTitle) at AccountLocationPopup.pcf: line 84, column 64
    function visible_14 () : java.lang.Boolean {
      return addressList ?.Count > 0 ? true : false
    }
    
    // 'visible' attribute on ToolbarButton (id=Standardize) at AccountLocationPopup.pcf: line 56, column 56
    function visible_3 () : java.lang.Boolean {
      return standardizeVisible and shouldEdit
    }
    
    // 'updateVisible' attribute on EditButtons at AccountLocationPopup.pcf: line 66, column 42
    function visible_9 () : java.lang.Boolean {
      return updateVisible
    }
    
    override property get CurrentLocation () : pcf.AccountLocationPopup {
      return super.CurrentLocation as pcf.AccountLocationPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.HashSet<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.HashSet<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.HashSet<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get overrideVisible () : Boolean {
      return getVariableValue("overrideVisible", 0) as Boolean
    }
    
    property set overrideVisible ($arg :  Boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get shouldEdit () : boolean {
      return getVariableValue("shouldEdit", 0) as java.lang.Boolean
    }
    
    property set shouldEdit ($arg :  boolean) {
      setVariableValue("shouldEdit", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get theLocation () : AccountLocation {
      return getVariableValue("theLocation", 0) as AccountLocation
    }
    
    property set theLocation ($arg :  AccountLocation) {
      setVariableValue("theLocation", 0, $arg)
    }
    
    property get updateVisible () : Boolean {
      return getVariableValue("updateVisible", 0) as Boolean
    }
    
    property set updateVisible ($arg :  Boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountLocationPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at AccountLocationPopup.pcf: line 101, column 44
    function action_21 () : void {
      helper.replaceAddressWithSuggested(suggestedAddress,theLocation, true); addressList = helper.getAddressList(); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible() 
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AccountLocationPopup.pcf: line 106, column 56
    function valueRoot_23 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at AccountLocationPopup.pcf: line 106, column 56
    function value_22 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at AccountLocationPopup.pcf: line 110, column 56
    function value_25 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AccountLocationPopup.pcf: line 114, column 48
    function value_28 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at AccountLocationPopup.pcf: line 119, column 44
    function value_31 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at AccountLocationPopup.pcf: line 123, column 54
    function value_34 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(1) as entity.Address
    }
    
    
  }
  
  
}