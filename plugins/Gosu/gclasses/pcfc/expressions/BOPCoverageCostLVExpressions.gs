package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPCoverageCostLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPCoverageCostLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BOPCoverageCostLV.pcf: line 21, column 23
    function initialValue_0 () : boolean {
      return costs.AnyProrated
    }
    
    // 'label' attribute on EmptyCell (id=CovTerm_Cell) at BOPCoverageCostLV.pcf: line 101, column 187
    function label_5 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyLine.Coverage.CovTermValue",  gw.pcf.line.common.CostUIHelper.getDisplayableCurrency(new java.util.ArrayList<Cost>(costs)) )
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 37, column 24
    function sortBy_1 (cost :  entity.BOPCost) : java.lang.Object {
      return (typeof cost).AllTypesInHierarchy.Count
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 40, column 24
    function sortBy_2 (cost :  entity.BOPCost) : java.lang.Object {
      return typeof cost
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 43, column 24
    function sortBy_3 (cost :  entity.BOPCost) : java.lang.Object {
      return cost.Coverage.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 46, column 24
    function sortBy_4 (cost :  entity.BOPCost) : java.lang.Object {
      return cost.EffDate
    }
    
    // '$$sumValue' attribute on RowIterator at BOPCoverageCostLV.pcf: line 142, column 25
    function sumValueRoot_16 (cost :  entity.BOPCost) : java.lang.Object {
      return cost
    }
    
    // 'footerSumValue' attribute on RowIterator at BOPCoverageCostLV.pcf: line 142, column 25
    function sumValue_15 (cost :  entity.BOPCost) : java.lang.Object {
      return cost.ActualAmountBilling
    }
    
    // 'footerLabel' attribute on RowIterator at BOPCoverageCostLV.pcf: line 96, column 113
    function value_10 () : java.lang.Object {
      return footerMessage
    }
    
    // 'value' attribute on RowIterator at BOPCoverageCostLV.pcf: line 30, column 36
    function value_53 () : entity.BOPCost[] {
      return costs.toTypedArray()
    }
    
    // 'visible' attribute on ListViewPanel (id=BOPCoverageCostLV) at BOPCoverageCostLV.pcf: line 11, column 49
    function visible_54 () : java.lang.Boolean {
      return costs != null and not costs.Empty
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at BOPCoverageCostLV.pcf: line 109, column 25
    function visible_6 () : java.lang.Boolean {
      return prorated
    }
    
    property get bopScreenHelper () : tdic.web.pcf.helper.BOPQuoteScreenHelper {
      return getVariableValue("bopScreenHelper", 0) as tdic.web.pcf.helper.BOPQuoteScreenHelper
    }
    
    property set bopScreenHelper ($arg :  tdic.web.pcf.helper.BOPQuoteScreenHelper) {
      setVariableValue("bopScreenHelper", 0, $arg)
    }
    
    property get costs () : java.util.Set<BOPCost> {
      return getRequireValue("costs", 0) as java.util.Set<BOPCost>
    }
    
    property set costs ($arg :  java.util.Set<BOPCost>) {
      setRequireValue("costs", 0, $arg)
    }
    
    property get footerMessage () : java.lang.String {
      return getRequireValue("footerMessage", 0) as java.lang.String
    }
    
    property set footerMessage ($arg :  java.lang.String) {
      setRequireValue("footerMessage", 0, $arg)
    }
    
    property get prorated () : boolean {
      return getVariableValue("prorated", 0) as java.lang.Boolean
    }
    
    property set prorated ($arg :  boolean) {
      setVariableValue("prorated", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=CovTermValue_Cell) at BOPCoverageCostLV.pcf: line 68, column 43
    function valueRoot_23 () : java.lang.Object {
      return covTerm
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BOPCoverageCostLV.pcf: line 63, column 100
    function value_20 () : java.lang.String {
      return bopScreenHelper.getCoverageTermDisplayName(theCoverage, covTerm).leftPad( 7 )
    }
    
    // 'value' attribute on TextCell (id=CovTermValue_Cell) at BOPCoverageCostLV.pcf: line 68, column 43
    function value_22 () : java.lang.String {
      return covTerm.DisplayValue
    }
    
    // 'visible' attribute on EmptyCell (id=TermAmount_Cell) at BOPCoverageCostLV.pcf: line 71, column 33
    function visible_25 () : java.lang.Boolean {
      return prorated
    }
    
    property get covTerm () : gw.api.domain.covterm.CovTerm {
      return getIteratedValue(2) as gw.api.domain.covterm.CovTerm
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPCoverageCostLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BOPCoverageCostLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BOPCoverageCostLV.pcf: line 34, column 33
    function initialValue_17 () : entity.Coverage {
      return cost.Coverage
    }
    
    // RowIterator at BOPCoverageCostLV.pcf: line 30, column 36
    function initializeVariables_52 () : void {
        theCoverage = cost.Coverage;

    }
    
    // 'label' attribute on EmptyCell (id=CovTerm_Cell) at BOPCoverageCostLV.pcf: line 101, column 187
    function label_32 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyLine.Coverage.CovTermValue",  gw.pcf.line.common.CostUIHelper.getDisplayableCurrency(new java.util.ArrayList<Cost>(costs)) )
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 55, column 26
    function sortBy_18 (covTerm :  gw.api.domain.covterm.CovTerm) : java.lang.Object {
      return covTerm.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at BOPCoverageCostLV.pcf: line 58, column 26
    function sortBy_19 (covTerm :  gw.api.domain.covterm.CovTerm) : java.lang.Object {
      return covTerm.Pattern.PublicID
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at BOPCoverageCostLV.pcf: line 109, column 25
    function valueRoot_35 () : java.lang.Object {
      return cost
    }
    
    // 'value' attribute on RowIterator at BOPCoverageCostLV.pcf: line 52, column 53
    function value_29 () : gw.api.domain.covterm.CovTerm[] {
      return bopScreenHelper.hideBuildingCovTermsToDisplay(theCoverage)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BOPCoverageCostLV.pcf: line 96, column 113
    function value_30 () : java.lang.String {
      return theCoverage != null ? bopScreenHelper.getCoverageDisplayName(theCoverage) : cost.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermAmount_Cell) at BOPCoverageCostLV.pcf: line 109, column 25
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualTermAmountBilling
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at BOPCoverageCostLV.pcf: line 117, column 25
    function value_38 () : java.util.Date {
      return cost.EffDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at BOPCoverageCostLV.pcf: line 125, column 25
    function value_42 () : java.util.Date {
      return cost.ExpDate
    }
    
    // 'value' attribute on TextCell (id=Proration_Cell) at BOPCoverageCostLV.pcf: line 133, column 25
    function value_46 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(cost.ProRataByDaysValue, "#0.0000")
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at BOPCoverageCostLV.pcf: line 142, column 25
    function value_49 () : gw.pl.currency.MonetaryAmount {
      return cost.ActualAmountBilling
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=TermAmount_Cell) at BOPCoverageCostLV.pcf: line 109, column 25
    function visible_36 () : java.lang.Boolean {
      return prorated
    }
    
    property get cost () : entity.BOPCost {
      return getIteratedValue(1) as entity.BOPCost
    }
    
    property get theCoverage () : entity.Coverage {
      return getVariableValue("theCoverage", 1) as entity.Coverage
    }
    
    property set theCoverage ($arg :  entity.Coverage) {
      setVariableValue("theCoverage", 1, $arg)
    }
    
    
  }
  
  
}