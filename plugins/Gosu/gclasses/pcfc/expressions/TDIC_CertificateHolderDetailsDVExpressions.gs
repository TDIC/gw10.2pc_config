package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_CertificateHolderDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_CertificateHolderDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ExistingCertificateHolders) at TDIC_CertificateHolderDetailsDV.pcf: line 79, column 118
    function label_10 () : java.lang.Object {
      return certificateHolder
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=ExistingCertificateHolders) at TDIC_CertificateHolderDetailsDV.pcf: line 79, column 118
    function toCreateAndAdd_11 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addPolicyCertificateHolder_TDIC(certificateHolder.AccountContact.Contact)
    }
    
    property get certificateHolder () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_CertificateHolderDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=acctContact) at TDIC_CertificateHolderDetailsDV.pcf: line 98, column 112
    function label_15 () : java.lang.Object {
      return acctContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=acctContact) at TDIC_CertificateHolderDetailsDV.pcf: line 98, column 112
    function toCreateAndAdd_16 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addPolicyCertificateHolder_TDIC(acctContact.AccountContact.Contact)
    }
    
    property get acctContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TDIC_CertificateHolderDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 126, column 66
    function action_21 () : void {
      EditPolicyContactRolePopup.push(certificateHolderDetail, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 126, column 66
    function action_dest_22 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(certificateHolderDetail, openForEdit)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at TDIC_CertificateHolderDetailsDV.pcf: line 118, column 63
    function checkBoxVisible_29 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfInterest_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 131, column 62
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      certificateHolderDetail.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfInterest_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 131, column 62
    function valueRoot_27 () : java.lang.Object {
      return certificateHolderDetail
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 126, column 66
    function value_23 () : entity.PolicyCertificateHolder_TDIC {
      return certificateHolderDetail
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfInterest_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 131, column 62
    function value_25 () : java.lang.String {
      return certificateHolderDetail.Description
    }
    
    property get certificateHolderDetail () : entity.PolicyCertificateHolder_TDIC {
      return getIteratedValue(1) as entity.PolicyCertificateHolder_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_CertificateHolderDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_CertificateHolderDetailsDV.pcf: line 57, column 90
    function label_5 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_CertificateHolderDetailsDV.pcf: line 57, column 90
    function pickLocation_6 () : void {
      TDIC_NewCertificateHolderPopup.push(glLine, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_CertificateHolderDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_CertificateHolderDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at TDIC_CertificateHolderDetailsDV.pcf: line 65, column 32
    function conversionExpression_8 (PickedValue :  Contact) : entity.PolicyCertificateHolder_TDIC {
      return glLine.addPolicyCertificateHolder_TDIC(PickedValue)
    }
    
    // 'initialValue' attribute on Variable at TDIC_CertificateHolderDetailsDV.pcf: line 19, column 35
    function initialValue_0 () : entity.PolicyPeriod {
      return glLine.Branch
    }
    
    // 'initialValue' attribute on Variable at TDIC_CertificateHolderDetailsDV.pcf: line 23, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at TDIC_CertificateHolderDetailsDV.pcf: line 28, column 36
    function initialValue_2 () : AccountContactView[] {
      return null
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at TDIC_CertificateHolderDetailsDV.pcf: line 69, column 138
    function label_13 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyCertificateHolder_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at TDIC_CertificateHolderDetailsDV.pcf: line 65, column 32
    function pickLocation_9 () : void {
      ContactSearchPopup.push(typekey.AccountContactRole.TC_CERTIFICATEHOLDER_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_CertificateHolderDetailsDV.pcf: line 93, column 34
    function sortBy_14 (acctContact :  entity.AccountContactView) : java.lang.Object {
      return acctContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_CertificateHolderDetailsDV.pcf: line 52, column 32
    function sortBy_4 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 126, column 66
    function sortValue_19 (certificateHolderDetail :  entity.PolicyCertificateHolder_TDIC) : java.lang.Object {
      return certificateHolderDetail
    }
    
    // 'value' attribute on TextCell (id=DescriptionOfInterest_Cell) at TDIC_CertificateHolderDetailsDV.pcf: line 131, column 62
    function sortValue_20 (certificateHolderDetail :  entity.PolicyCertificateHolder_TDIC) : java.lang.Object {
      return certificateHolderDetail.Description
    }
    
    // 'toRemove' attribute on RowIterator at TDIC_CertificateHolderDetailsDV.pcf: line 118, column 63
    function toRemove_30 (certificateHolderDetail :  entity.PolicyCertificateHolder_TDIC) : void {
      glLine.removeFromGLCertificateHolders_TDIC(certificateHolderDetail)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_CertificateHolderDetailsDV.pcf: line 74, column 57
    function value_12 () : entity.AccountContactView[] {
      return getExistingCertificateHolders()
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at TDIC_CertificateHolderDetailsDV.pcf: line 90, column 57
    function value_17 () : entity.AccountContactView[] {
      return getOtherContacts()
    }
    
    // 'value' attribute on RowIterator at TDIC_CertificateHolderDetailsDV.pcf: line 118, column 63
    function value_31 () : entity.PolicyCertificateHolder_TDIC[] {
      return glLine.GLCertificateHolders_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator at TDIC_CertificateHolderDetailsDV.pcf: line 49, column 49
    function value_7 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC)
    }
    
    // 'visible' attribute on AddButton (id=AddContactsButton) at TDIC_CertificateHolderDetailsDV.pcf: line 45, column 35
    function visible_18 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'visible' attribute on DetailViewPanel (id=TDIC_CertificateHolderDetailsDV) at TDIC_CertificateHolderDetailsDV.pcf: line 8, column 83
    function visible_33 () : java.lang.Boolean {
      return !(policyPeriod.Job typeis Submission) or perm.System.viewsubmission
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get existingCertificateHolders () : AccountContactView[] {
      return getVariableValue("existingCertificateHolders", 0) as AccountContactView[]
    }
    
    property set existingCertificateHolders ($arg :  AccountContactView[]) {
      setVariableValue("existingCertificateHolders", 0, $arg)
    }
    
    property get glLine () : entity.GeneralLiabilityLine {
      return getRequireValue("glLine", 0) as entity.GeneralLiabilityLine
    }
    
    property set glLine ($arg :  entity.GeneralLiabilityLine) {
      setRequireValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherContacts () : AccountContactView[] {
      return getVariableValue("otherContacts", 0) as AccountContactView[]
    }
    
    property set otherContacts ($arg :  AccountContactView[]) {
      setVariableValue("otherContacts", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function getExistingCertificateHolders() : AccountContactView[] {
      if (existingCertificateHolders == null) {
        var addedContacts = glLine.GLCertificateHolders_TDIC*.AccountContactRole*.AccountContact
        var all = glLine.ExistingCertificateHolder_TDIC
        var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
        existingCertificateHolders = remaining
      }
      return existingCertificateHolders
    }
    
    function getOtherContacts() : AccountContactView[] {
      if (otherContacts == null) {
        otherContacts = glLine.CertificateHolderOtherCandidates_TDIC.asViews()
      }
      return otherContacts
    }
    
    
  }
  
  
}