package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSchoolServicesCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLSchoolServicesCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSchoolServicesCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 38, column 99
    function available_39 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 23, column 36
    function initialValue_0 () : AccountContactView[] {
      return null
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 27, column 35
    function initialValue_1 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 31, column 49
    function initialValue_2 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 38, column 99
    function label_40 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 38, column 99
    function setter_41 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on TextCell (id=eventName_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 66, column 50
    function sortValue_3 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.EventName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 72, column 55
    function sortValue_4 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.EventStartDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 79, column 53
    function sortValue_5 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.EventEndDate
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 86, column 48
    function sortValue_6 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.Address
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 92, column 51
    function sortValue_7 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.CanrecComp
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 98, column 63
    function sortValue_8 (scheduledItem :  entity.GLSchoolServSched_TDIC) : java.lang.Object {
      return scheduledItem.AddlInsured.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 58, column 57
    function toCreateAndAdd_35 () : entity.GLSchoolServSched_TDIC {
      return glLine.createAndAddSchoolServSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 58, column 57
    function toRemove_36 (scheduledItem :  entity.GLSchoolServSched_TDIC) : void {
      glLine.removeFromGLSchoolServSched_TDIC(scheduledItem) 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 58, column 57
    function value_37 () : entity.GLSchoolServSched_TDIC[] {
      return glLine.GLSchoolServSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 38, column 99
    function visible_38 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 119, column 46
    function visible_44 () : java.lang.Boolean {
      return isSchoolServiceCovVisible()
    }
    
    property get additionalInsureds () : AccountContactView[] {
      return getVariableValue("additionalInsureds", 0) as AccountContactView[]
    }
    
    property set additionalInsureds ($arg :  AccountContactView[]) {
      setVariableValue("additionalInsureds", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    
    function getAdditionalInsureds() : AccountContactView[] {
      /*if (additionalInsureds == null) {
        var all = glLine.AdditionalInsureds*.AccountContactRole*.AccountContact
        var addedContacts = (all.HasElements) ? glLine.GLSchoolServSched_TDIC*.AddlInsured*.PolicyAddlInsured*.AccountContactRole*.AccountContact : null
        if(addedContacts.HasElements) {
          var remaining = all?.subtract(addedContacts)
          additionalInsureds = remaining?.toTypedArray()?.asViews()
        } else {
          additionalInsureds = all?.asViews()
        }
      }
      return additionalInsureds*/
      return glLine.AdditionalInsureds*.AccountContactRole*.AccountContact?.asViews()
    }
    
    function setAdditionalInsureds(scheduledItem : GLSchoolServSched_TDIC, contact : AccountContact ) {
      scheduledItem.AddlInsured = glLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\elt -> elt.PolicyAddlInsured?.AccountContactRole?.AccountContact == contact)
    }
    
        function isSchoolServiceCovVisible() : boolean {
          if (coveragePattern.CodeIdentifier == "GLSchoolServicesCov_TDIC") {
            if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
                  coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
                  coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
              return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLSchoolServicesCov_TDICExists
    
            } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                        coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
              return true
            }
          }
          // as before for other coverages in .default PCF file
          return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSchoolServicesCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 110, column 48
    function action_29 () : void {
      setAdditionalInsureds(scheduledItem, unassignedContact.AccountContact)
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 110, column 48
    function label_30 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.AccountContactView {
      return getIteratedValue(2) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSchoolServicesCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=eventName_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 66, column 50
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.EventName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 79, column 53
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.EventEndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 86, column 48
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.Address = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 92, column 51
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.CanrecComp = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 106, column 36
    function sortBy_28 (unassignedContact :  entity.AccountContactView) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=eventName_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 66, column 50
    function valueRoot_11 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 98, column 63
    function valueRoot_33 () : java.lang.Object {
      return scheduledItem.AddlInsured
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 72, column 55
    function value_13 () : java.util.Date {
      return scheduledItem.EventStartDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 79, column 53
    function value_16 () : java.util.Date {
      return scheduledItem.EventEndDate
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 86, column 48
    function value_20 () : java.lang.String {
      return scheduledItem.Address
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 92, column 51
    function value_24 () : java.lang.Boolean {
      return scheduledItem.CanrecComp
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 103, column 59
    function value_31 () : entity.AccountContactView[] {
      return getAdditionalInsureds()
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 98, column 63
    function value_32 () : java.lang.String {
      return scheduledItem.AddlInsured.DisplayName
    }
    
    // 'value' attribute on TextCell (id=eventName_Cell) at CoverageInputSet.GLSchoolServicesCov_TDIC.pcf: line 66, column 50
    function value_9 () : java.lang.String {
      return scheduledItem.EventName
    }
    
    property get scheduledItem () : entity.GLSchoolServSched_TDIC {
      return getIteratedValue(1) as entity.GLSchoolServSched_TDIC
    }
    
    
  }
  
  
}