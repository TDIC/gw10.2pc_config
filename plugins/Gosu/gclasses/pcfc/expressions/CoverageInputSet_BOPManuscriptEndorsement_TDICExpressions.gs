package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_BOPManuscriptEndorsement_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=resetButton) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 65, column 64
    function allCheckedRowsAction_10 (CheckedValues :  entity.BOPManuscript_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      resetAiOrCh(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 58, column 63
    function allCheckedRowsAction_7 (CheckedValues :  entity.BOPManuscript_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 58, column 63
    function available_5 () : java.lang.Boolean {
      return bopLine.Branch.Job typeis PolicyChange
    }
    
    // 'allowToggle' attribute on InputGroup (id=BOPManuscriptInputGroup) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 42, column 101
    function available_70 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable) and perm.System.editmanuscript_tdic
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=resetButton) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 65, column 64
    function available_8 () : java.lang.Boolean {
      return building.Branch.Job typeis PolicyChange
    }
    
    // 'editable' attribute on ListViewInput (id=BOPManuscriptid_LV) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 47, column 27
    function editable_68 () : java.lang.Boolean {
      return perm.System.editmanuscript_tdic
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 22, column 36
    function initialValue_0 () : productmodel.BOPLine {
      return coverable.PolicyLine as BOPLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 30, column 34
    function initialValue_2 () : entity.BOPBuilding {
      return coverable as BOPBuilding
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 35, column 36
    function initialValue_3 () : AccountContactView[] {
      return null
    }
    
    // 'onToggle' attribute on InputGroup (id=BOPManuscriptInputGroup) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 42, column 101
    function setter_71 (VALUE :  java.lang.Boolean) : void {
      building.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 84, column 64
    function sortValue_11 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 91, column 65
    function sortValue_12 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function sortValue_13 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptType
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail2_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 110, column 32
    function sortValue_14 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return (scheduledItem.policyAddInsured_TDIC.DisplayName == null ? "": scheduledItem.policyAddInsured_TDIC.DisplayName) + " " +(scheduledItem.policyAddInsured_TDIC.Desciption ==null ? "":scheduledItem.policyAddInsured_TDIC.Desciption)
    }
    
    // 'value' attribute on TextCell (id=PolicyLossPayee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 134, column 108
    function sortValue_15 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return (scheduledItem.PolicyLossPayee.DisplayName==null? "" : scheduledItem.PolicyLossPayee.DisplayName) + " " + (scheduledItem.PolicyLossPayee.LoanNumberString==null? "" : scheduledItem.PolicyLossPayee.LoanNumberString)
    }
    
    // 'value' attribute on TextCell (id=PolicyMortgagee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 157, column 269
    function sortValue_17 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return (scheduledItem.PolicyMortgagee.DisplayName==null? "" : scheduledItem.PolicyMortgagee.DisplayName) + " " + (scheduledItem.PolicyMortgagee.LoanNumberString==null? "" : scheduledItem.PolicyMortgagee.LoanNumberString)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 180, column 62
    function sortValue_18 (scheduledItem :  entity.BOPManuscript_TDIC) : java.lang.Object {
      return scheduledItem.ManuscriptDescription
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 77, column 53
    function toCreateAndAdd_65 () : entity.BOPManuscript_TDIC {
      return building.createAndAddBOPManuscript_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 77, column 53
    function toRemove_66 (scheduledItem :  entity.BOPManuscript_TDIC) : void {
      building.toRemoveFromBOPManuscript_TDIC(scheduledItem)
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 77, column 53
    function value_67 () : entity.BOPManuscript_TDIC[] {
      return building.BOPManuscript
    }
    
    // 'visible' attribute on TextCell (id=PolicyLossPayee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 134, column 108
    function visible_16 () : java.lang.Boolean {
      return bopLine.Branch.Offering.CodeIdentifier=="BOPBusinessOwnersPolicy_TDIC"
    }
    
    // 'removeVisible' attribute on IteratorButtons at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 51, column 185
    function visible_4 () : java.lang.Boolean {
      return bopLine.Branch.Job typeis Submission or bopLine.Branch.Job typeis Renewal or building.BOPManuscript.hasMatch(\manuscript -> manuscript.BasedOn == null)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=BOPManuscriptInputGroup) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 42, column 101
    function visible_69 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 187, column 43
    function visible_73 () : java.lang.Boolean {
      return isLocumTeensCovVisible()
    }
    
    property get additionalInsureds () : AccountContactView[] {
      return getVariableValue("additionalInsureds", 0) as AccountContactView[]
    }
    
    property set additionalInsureds ($arg :  AccountContactView[]) {
      setVariableValue("additionalInsureds", 0, $arg)
    }
    
    property get bopLine () : productmodel.BOPLine {
      return getVariableValue("bopLine", 0) as productmodel.BOPLine
    }
    
    property set bopLine ($arg :  productmodel.BOPLine) {
      setVariableValue("bopLine", 0, $arg)
    }
    
    property get building () : entity.BOPBuilding {
      return getVariableValue("building", 0) as entity.BOPBuilding
    }
    
    property set building ($arg :  entity.BOPBuilding) {
      setVariableValue("building", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isLocumTeensCovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLLocumTenensCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status        == PolicyPeriodStatus.TC_QUOTED) or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
            coverable.PolicyLine.Branch.Job.Subtype   == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLLocumTenensCov_TDICExists
          
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
            coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    /*
    function createAndAddPLAISched_TDIC() : BOPManuscript_TDIC {
      var item = new BOPManuscript_TDIC(building.Branch).policyAddInsured_TDIC.PolicyAddlInsured
      bopLine.addToAdditionalInsureds(item) 
      return item
    }
    */
    
    
    function getAdditionalInsureds() : PolicyAddlInsured[] {
      //return building?.AdditionalInsureds*.AccountContactRole*.AccountContact?.asViews()
      return building?.AdditionalInsureds
    }
    
    function getPolcyLossPayee() : PolicyLossPayee_TDIC[] {
      //return building?.BOPBldgLossPayees*.AccountContactRole*.AccountContact?.asViews()
      return building?.BOPBldgLossPayees
    }
    
    function getPolicyMortgagee() : PolicyMortgagee_TDIC[] {
      //return building?.BOPBldgMortgagees*.AccountContactRole*.AccountContact?.asViews()
      return building?.BOPBldgMortgagees
    }
    
    function setAdditionalInsureds(scheduledItem : BOPManuscript_TDIC, contact : PolicyAddlInsured ) {
      scheduledItem.policyAddInsured_TDIC = building?.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\elt -> elt.PolicyAddlInsured?.AccountContactRole?.AccountContact == contact.AccountContactRole.AccountContact)
    }
    function setPolicyLossPayee(scheduledItem : BOPManuscript_TDIC, contact : PolicyLossPayee_TDIC ) {
      scheduledItem.PolicyLossPayee = building?.BOPBldgLossPayees*.BOPBuildingLossPayee*.BOPBldgLossPayees.firstWhere(\elt -> elt.AccountContactRole.AccountContact== contact.AccountContactRole.AccountContact)
    
    }
    
    function setPolicyMortgagee(scheduledItem : BOPManuscript_TDIC, contact : PolicyMortgagee_TDIC ) {
      scheduledItem.PolicyMortgagee = building?.BOPBldgMortgagees*.BOPBuildingMortgagee*.BOPBldgMortgagees.firstWhere(\elt -> elt.AccountContactRole.AccountContact== contact.AccountContactRole.AccountContact)
    }
    
    /*
    function setPolicyLossPayee_New(scheduledItem : BOPManuscript_TDIC, contact : AccountContact ) {
      scheduledItem.PolicyLossPayee = building?.BOPBldgLossPayees*.BOPBuildingLossPayee*.BOPBldgLossPayees.firstWhere(\elt -> elt.AccountContactRole.AccountContact== contact)
      //BOPBuildingLossPayee?.BOPBldgLossPayees*.AccountContactRole*.AccountContact==contact)
    }
    */
    function validateEndorsementExpDate(date: Date): String {
      var transEffDate = coverable.PolicyLine.Branch.EditEffectiveDate
      var allowedDate = (date.afterOrEqual(transEffDate.addWeeks(2)) and date.beforeOrEqual(transEffDate.addDays(90)))
      if(!allowedDate){
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLLocumTenensCovTDIC.ExpDate")
      }
      return null
    }
    
    function setTypeValue() : BOPManuscriptType_TDIC[]{
      if(bopLine.Branch.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"){
        return BOPManuscriptType_TDIC.AllTypeKeys.toTypedArray()
      }else{
        return BOPManuscriptType_TDIC.TF_LRP.TypeKeys.toTypedArray()
      }
    }
    function setExpirationDate(manuScripts : BOPManuscript_TDIC[]) {
      manuScripts.each(\manuScript -> {
        if(manuScript.ManuscriptExpirationDate == null){
          manuScript.ManuscriptExpirationDate = manuScript.Branch.EditEffectiveDate
        }
      })
    }
    function resetAiOrCh (manuScripts : BOPManuscript_TDIC[]) {
      manuScripts.each(\manuScript -> {
        if(manuScript.policyAddInsured_TDIC != null) {
          manuScript.policyAddInsured_TDIC = null
        }
        if (manuScript.PolicyLossPayee != null) {
          manuScript.PolicyLossPayee = null
        }
        if (manuScript.PolicyMortgagee != null) {
          manuScript.PolicyMortgagee = null
        }
      })
    }
          
    
      
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 124, column 48
    function action_35 () : void {
      setAdditionalInsureds(scheduledItem, unassignedContact)
    }
    
    // 'available' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 124, column 48
    function available_34 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 124, column 48
    function label_36 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.PolicyAddlInsured {
      return getIteratedValue(2) as entity.PolicyAddlInsured
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 148, column 48
    function action_44 () : void {
      setPolicyLossPayee(scheduledItem, unassignedContact)
    }
    
    // 'available' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 148, column 48
    function available_43 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 148, column 48
    function label_45 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.PolicyLossPayee_TDIC {
      return getIteratedValue(2) as entity.PolicyLossPayee_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 171, column 48
    function action_54 () : void {
      setPolicyMortgagee(scheduledItem, unassignedContact)
    }
    
    // 'available' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 171, column 48
    function available_53 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'label' attribute on MenuItem (id=UnassignedContact) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 171, column 48
    function label_55 () : java.lang.Object {
      return unassignedContact
    }
    
    property get unassignedContact () : entity.PolicyMortgagee_TDIC {
      return getIteratedValue(2) as entity.PolicyMortgagee_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextCell (id=PolicyAddlInsuredDetail2_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 110, column 32
    function available_38 () : java.lang.Boolean {
      return (scheduledItem.ManuscriptType==BOPManuscriptType_TDIC.TC_ADDITIONALINSUREDCONTINUED)
    }
    
    // 'available' attribute on TextCell (id=PolicyLossPayee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 134, column 108
    function available_47 () : java.lang.Boolean {
      return (scheduledItem.ManuscriptType==BOPManuscriptType_TDIC.TC_LOSSPAYEEAMENDEDTOREAD)
    }
    
    // 'available' attribute on TextCell (id=PolicyMortgagee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 157, column 269
    function available_57 () : java.lang.Boolean {
      return (scheduledItem.ManuscriptType==BOPManuscriptType_TDIC.TC_MORTGAGEEAMENDEDTOREAD)
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.ManuscriptType = (__VALUE_TO_SET as typekey.BOPManuscriptType_TDIC)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 180, column 62
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.ManuscriptDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'requestValidationExpression' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 91, column 65
    function requestValidationExpression_22 (VALUE :  java.util.Date) : java.lang.Object {
      return validateEndorsementExpDate(VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 119, column 36
    function sortBy_33 (unassignedContact :  entity.PolicyAddlInsured) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 143, column 36
    function sortBy_42 (unassignedContact :  entity.PolicyLossPayee_TDIC) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 166, column 36
    function sortBy_52 (unassignedContact :  entity.PolicyMortgagee_TDIC) : java.lang.Object {
      return unassignedContact.DisplayName
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function valueRange_29 () : java.lang.Object {
      return setTypeValue()
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 84, column 64
    function valueRoot_20 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 84, column 64
    function value_19 () : java.util.Date {
      return scheduledItem.ManuscriptEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 91, column 65
    function value_23 () : java.util.Date {
      return scheduledItem.ManuscriptExpirationDate
    }
    
    // 'value' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function value_26 () : typekey.BOPManuscriptType_TDIC {
      return scheduledItem.ManuscriptType
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 116, column 58
    function value_37 () : entity.PolicyAddlInsured[] {
      return getAdditionalInsureds()
    }
    
    // 'value' attribute on TextCell (id=PolicyAddlInsuredDetail2_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 110, column 32
    function value_39 () : java.lang.String {
      return (scheduledItem.policyAddInsured_TDIC.DisplayName == null ? "": scheduledItem.policyAddInsured_TDIC.DisplayName) + " " +(scheduledItem.policyAddInsured_TDIC.Desciption ==null ? "":scheduledItem.policyAddInsured_TDIC.Desciption)
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 140, column 61
    function value_46 () : entity.PolicyLossPayee_TDIC[] {
      return getPolcyLossPayee()
    }
    
    // 'value' attribute on TextCell (id=PolicyLossPayee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 134, column 108
    function value_48 () : java.lang.String {
      return (scheduledItem.PolicyLossPayee.DisplayName==null? "" : scheduledItem.PolicyLossPayee.DisplayName) + " " + (scheduledItem.PolicyLossPayee.LoanNumberString==null? "" : scheduledItem.PolicyLossPayee.LoanNumberString)
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedContactIterator) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 163, column 61
    function value_56 () : entity.PolicyMortgagee_TDIC[] {
      return getPolicyMortgagee()
    }
    
    // 'value' attribute on TextCell (id=PolicyMortgagee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 157, column 269
    function value_58 () : java.lang.String {
      return (scheduledItem.PolicyMortgagee.DisplayName==null? "" : scheduledItem.PolicyMortgagee.DisplayName) + " " + (scheduledItem.PolicyMortgagee.LoanNumberString==null? "" : scheduledItem.PolicyMortgagee.LoanNumberString)
    }
    
    // 'value' attribute on TextAreaCell (id=manuscriptDescription_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 180, column 62
    function value_61 () : java.lang.String {
      return scheduledItem.ManuscriptDescription
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function verifyValueRangeIsAllowedType_30 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function verifyValueRangeIsAllowedType_30 ($$arg :  typekey.BOPManuscriptType_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=manuscriptType_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 100, column 60
    function verifyValueRange_31 () : void {
      var __valueRangeArg = setTypeValue()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_30(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=PolicyLossPayee_Cell) at CoverageInputSet.BOPManuscriptEndorsement_TDIC.pcf: line 134, column 108
    function visible_50 () : java.lang.Boolean {
      return bopLine.Branch.Offering.CodeIdentifier=="BOPBusinessOwnersPolicy_TDIC"
    }
    
    property get scheduledItem () : entity.BOPManuscript_TDIC {
      return getIteratedValue(1) as entity.BOPManuscript_TDIC
    }
    
    
  }
  
  
}