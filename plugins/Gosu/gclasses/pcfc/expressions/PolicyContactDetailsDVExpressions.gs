package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyContactDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 94, column 33
    function def_onEnter_10 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(contact, period)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 94, column 33
    function def_onEnter_12 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(contact, period)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 41, column 90
    function def_onEnter_5 (def :  pcf.PolicyContactRoleNameInputSet_company) : void {
      def.onEnter(policyContactRole)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 41, column 90
    function def_onEnter_7 (def :  pcf.PolicyContactRoleNameInputSet_person) : void {
      def.onEnter(policyContactRole)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 94, column 33
    function def_refreshVariables_11 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(contact, period)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 94, column 33
    function def_refreshVariables_13 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(contact, period)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 41, column 90
    function def_refreshVariables_6 (def :  pcf.PolicyContactRoleNameInputSet_company) : void {
      def.refreshVariables(policyContactRole)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 41, column 90
    function def_refreshVariables_8 (def :  pcf.PolicyContactRoleNameInputSet_person) : void {
      def.refreshVariables(policyContactRole)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailsDV.pcf: line 17, column 30
    function initialValue_0 () : entity.Contact {
      return policyContactRole.AccountContactRole.AccountContact.Contact
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailsDV.pcf: line 22, column 35
    function initialValue_1 () : entity.PolicyPeriod {
      return policyContactRole.Branch
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailsDV.pcf: line 27, column 30
    function initialValue_2 () : entity.Address {
      return contact.PrimaryAddress
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailsDV.pcf: line 32, column 75
    function initialValue_3 () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> period.OpenForEdit)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailsDV.pcf: line 37, column 23
    function initialValue_4 () : boolean {
      return openForEditOverride or openForEditInit.get()
    }
    
    // 'mode' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 94, column 33
    function mode_14 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'mode' attribute on InputSetRef at PolicyContactDetailsDV.pcf: line 41, column 90
    function mode_9 () : java.lang.Object {
      return policyContactRole.AccountContactRole.AccountContact.Contact.Subtype.Code
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get openForEditInit () : gw.util.concurrent.LocklessLazyVar<java.lang.Boolean> {
      return getVariableValue("openForEditInit", 0) as gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>
    }
    
    property set openForEditInit ($arg :  gw.util.concurrent.LocklessLazyVar<java.lang.Boolean>) {
      setVariableValue("openForEditInit", 0, $arg)
    }
    
    property get openForEditOverride () : boolean {
      return getRequireValue("openForEditOverride", 0) as java.lang.Boolean
    }
    
    property set openForEditOverride ($arg :  boolean) {
      setRequireValue("openForEditOverride", 0, $arg)
    }
    
    property get period () : entity.PolicyPeriod {
      return getVariableValue("period", 0) as entity.PolicyPeriod
    }
    
    property set period ($arg :  entity.PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    property get primaryAddress () : entity.Address {
      return getVariableValue("primaryAddress", 0) as entity.Address
    }
    
    property set primaryAddress ($arg :  entity.Address) {
      setVariableValue("primaryAddress", 0, $arg)
    }
    
    
  }
  
  
}