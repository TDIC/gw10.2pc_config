package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/TDIC_AccountAddressTypeInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_AccountAddressTypeInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/account/TDIC_AccountAddressTypeInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_AccountAddressTypeInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedAddress.AddressType = (__VALUE_TO_SET as typekey.AddressType)
    }
    
    // 'editable' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function editable_0 () : java.lang.Boolean {
      return selectedAddress.LinkedAddress == null
    }
    
    // 'valueRange' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function valueRange_4 () : java.lang.Object {
      return selectedAddress.AvailableAddressTypeOptions
    }
    
    // 'value' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function valueRoot_3 () : java.lang.Object {
      return selectedAddress
    }
    
    // 'value' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function value_1 () : typekey.AddressType {
      return selectedAddress.AddressType
    }
    
    // 'valueRange' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function verifyValueRangeIsAllowedType_5 ($$arg :  typekey.AddressType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AddressType_TDIC_Input) at TDIC_AccountAddressTypeInputSet.pcf: line 17, column 40
    function verifyValueRange_6 () : void {
      var __valueRangeArg = selectedAddress.AvailableAddressTypeOptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    property get selectedAddress () : Address {
      return getRequireValue("selectedAddress", 0) as Address
    }
    
    property set selectedAddress ($arg :  Address) {
      setRequireValue("selectedAddress", 0, $arg)
    }
    
    
  }
  
  
}