package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7StateCostsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7StateCostsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7StateCostsLV.pcf: line 56, column 30
    function def_onEnter_2 (def :  pcf.WC7StateCostRowSet_default) : void {
      def.onEnter(aggCost)
    }
    
    // 'def' attribute on RowSetRef at WC7StateCostsLV.pcf: line 56, column 30
    function def_onEnter_4 (def :  pcf.WC7StateCostRowSet_total) : void {
      def.onEnter(aggCost)
    }
    
    // 'def' attribute on RowSetRef at WC7StateCostsLV.pcf: line 56, column 30
    function def_refreshVariables_3 (def :  pcf.WC7StateCostRowSet_default) : void {
      def.refreshVariables(aggCost)
    }
    
    // 'def' attribute on RowSetRef at WC7StateCostsLV.pcf: line 56, column 30
    function def_refreshVariables_5 (def :  pcf.WC7StateCostRowSet_total) : void {
      def.refreshVariables(aggCost)
    }
    
    // 'mode' attribute on RowSetRef at WC7StateCostsLV.pcf: line 56, column 30
    function mode_6 () : java.lang.Object {
      return aggCost.Mode
    }
    
    property get aggCost () : gw.api.ui.WC7CostWrapper {
      return getIteratedValue(1) as gw.api.ui.WC7CostWrapper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7StateCostsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7StateCostsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7StateCostsLV.pcf: line 50, column 24
    function sortBy_0 (aggCost :  gw.api.ui.WC7CostWrapper) : java.lang.Object {
      return aggCost.Order
    }
    
    // 'sortBy' attribute on IteratorSort at WC7StateCostsLV.pcf: line 53, column 24
    function sortBy_1 (aggCost :  gw.api.ui.WC7CostWrapper) : java.lang.Object {
      return aggCost.LocNumber
    }
    
    // 'value' attribute on RowIterator at WC7StateCostsLV.pcf: line 47, column 46
    function value_7 () : gw.api.ui.WC7CostWrapper[] {
      return stateCosts
    }
    
    property get stateCosts () : gw.api.ui.WC7CostWrapper[] {
      return getRequireValue("stateCosts", 0) as gw.api.ui.WC7CostWrapper[]
    }
    
    property set stateCosts ($arg :  gw.api.ui.WC7CostWrapper[]) {
      setRequireValue("stateCosts", 0, $arg)
    }
    
    
  }
  
  
}