package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/address/TDIC_AccountSearchGlobalAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_AccountSearchGlobalAddressInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/address/TDIC_AccountSearchGlobalAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_AccountSearchGlobalAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function available_10 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'available' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function available_20 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'available' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function available_34 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'value' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.County = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.AddressDelegate.AddressLine2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function editable_11 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function editable_21 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'editable' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function editable_35 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'initialValue' attribute on Variable at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 13, column 50
    function initialValue_0 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("City","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 17, column 50
    function initialValue_1 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("County","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 21, column 50
    function initialValue_2 () : gw.api.contact.AutocompleteHandler {
      return gw.api.contact.AddressAutocompleteHandler.createHandler("PostalCode","City,County,State,PostalCode,Country",true)
    }
    
    // 'initialValue' attribute on Variable at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 28, column 33
    function initialValue_3 () : java.lang.Integer {
      if (addressOwner != null) addressOwner.InEditMode = CurrentLocation.InEditMode; return 0
    }
    
    // 'label' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function label_23 () : java.lang.Object {
      return addressOwner.AddressLine1Label
    }
    
    // 'label' attribute on TextInput (id=AddressSummary_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 33, column 50
    function label_5 () : java.lang.Object {
      return addressOwner.AddressNameLabel
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 66, column 103
    function onChange_19 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 77, column 103
    function onChange_33 () : void {
      if(addressOwner.Address != null){addressOwner.Address.revertStandardizedStatus()}
    }
    
    // 'required' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function required_12 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.COUNTY)
    }
    
    // 'required' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function required_24 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'required' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function required_37 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'value' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function valueRoot_15 () : java.lang.Object {
      return addressOwner.AddressDelegate
    }
    
    // 'value' attribute on TextInput (id=County_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 56, column 23
    function value_13 () : java.lang.String {
      return addressOwner.AddressDelegate.County
    }
    
    // 'value' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function value_25 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine1
    }
    
    // 'value' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function value_38 () : java.lang.String {
      return addressOwner.AddressDelegate.AddressLine2
    }
    
    // 'value' attribute on TextInput (id=AddressSummary_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 33, column 50
    function value_6 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(addressOwner.AddressDelegate, "\n")
    }
    
    // 'visible' attribute on TextInput (id=AddressLine1_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 64, column 89
    function visible_22 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE1)
    }
    
    // 'visible' attribute on TextInput (id=AddressLine2_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 75, column 89
    function visible_36 () : java.lang.Boolean {
      return addressOwner.isVisible(gw.api.address.AddressOwnerFieldId.ADDRESSLINE2)
    }
    
    // 'visible' attribute on TextInput (id=AddressSummary_Input) at TDIC_AccountSearchGlobalAddressInputSet.pcf: line 33, column 50
    function visible_4 () : java.lang.Boolean {
      return addressOwner.ShowAddressSummary
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    property get cityhandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("cityhandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set cityhandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("cityhandler", 0, $arg)
    }
    
    property get countyhandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("countyhandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set countyhandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("countyhandler", 0, $arg)
    }
    
    property get pchandler () : gw.api.contact.AutocompleteHandler {
      return getVariableValue("pchandler", 0) as gw.api.contact.AutocompleteHandler
    }
    
    property set pchandler ($arg :  gw.api.contact.AutocompleteHandler) {
      setVariableValue("pchandler", 0, $arg)
    }
    
    property get saveEditMode () : java.lang.Integer {
      return getVariableValue("saveEditMode", 0) as java.lang.Integer
    }
    
    property set saveEditMode ($arg :  java.lang.Integer) {
      setVariableValue("saveEditMode", 0, $arg)
    }
    
    
  }
  
  
}