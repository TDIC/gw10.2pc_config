package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyTransactionForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PolicyTransactionForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/TDIC_PolicyTransactionForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PolicyTransactionForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (offerNumber :  String, jobIndex :  int) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at TDIC_PolicyTransactionForward.pcf: line 20, column 119
    function action_1 () : void {
      PolicyFile_Jobs.go(selectedJob.LatestPeriod,selectedJob.LatestPeriod.EditEffectiveDate,selectedJob); 
    }
    
    // 'initialValue' attribute on Variable at TDIC_PolicyTransactionForward.pcf: line 18, column 26
    function initialValue_0 () : entity.Job {
      return getChargeJob()
    }
    
    override property get CurrentLocation () : pcf.TDIC_PolicyTransactionForward {
      return super.CurrentLocation as pcf.TDIC_PolicyTransactionForward
    }
    
    property get jobIndex () : int {
      return getVariableValue("jobIndex", 0) as java.lang.Integer
    }
    
    property set jobIndex ($arg :  int) {
      setVariableValue("jobIndex", 0, $arg)
    }
    
    property get offerNumber () : String {
      return getVariableValue("offerNumber", 0) as String
    }
    
    property set offerNumber ($arg :  String) {
      setVariableValue("offerNumber", 0, $arg)
    }
    
    property get selectedJob () : entity.Job {
      return getVariableValue("selectedJob", 0) as entity.Job
    }
    
    property set selectedJob ($arg :  entity.Job) {
      setVariableValue("selectedJob", 0, $arg)
    }
    
    
    //Select the Job corresponding with the selected Charge in BC. Returns the job within 
    // the jobs of the Term of the Job with Job Number equals to offerNumber with Transaction Cost different 
    // that 0 and in the position equals to indexJob. Jobs with Transaction cost equal 0 do not generate charges
    // in BC and they should be ignore
    function getChargeJob() : Job {
      var offer = Query.make(Job).compare("JobNumber", Equals, offerNumber).select().first()
      var jobsOffer = offer.Policy.Periods*.Job.where( \ job -> job.UpdateTime.afterOrEqual(offer.CreateTime) 
          and not (job.Periods.hasMatch( \ period -> period.TransactionCostRPT_amt == 0)) 
          and (job.LatestPeriod.PolicyTerm == offer.LatestPeriod.PolicyTerm)).orderBy( \ elt -> elt.CreateTime)  
      return jobsOffer[jobIndex]
    }
    
    
  }
  
  
}