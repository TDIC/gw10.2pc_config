package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_WC7AircraftPremiumEndorsementCondExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 32, column 109
    function available_37 () : java.lang.Boolean {
      return conditionPattern.allowToggle(coverable)
    }
    
    // 'editable' attribute on ListViewInput at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 36, column 27
    function editable_35 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 22, column 36
    function initialValue_0 () : productmodel.WC7Line {
      return coverable.PolicyLine as WC7Line
    }
    
    // 'label' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 32, column 109
    function label_38 () : java.lang.Object {
      return conditionPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 32, column 109
    function setter_39 (VALUE :  java.lang.Boolean) : void {
      new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, conditionPattern, VALUE)
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 55, column 30
    function sortBy_4 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.Jurisdiction
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 58, column 30
    function sortBy_5 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.AircraftNumber
    }
    
    // 'sortBy' attribute on IteratorSort at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 61, column 30
    function sortBy_6 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.Description
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatCount_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 90, column 48
    function sortValue_10 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.PassengerSeats
    }
    
    // 'value' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function sortValue_7 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatDesc_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 77, column 47
    function sortValue_8 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.Description
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftNumber_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 83, column 47
    function sortValue_9 (wc7AircraftSeat :  WC7AircraftSeat) : java.lang.Object {
      return wc7AircraftSeat.AircraftNumber
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 52, column 50
    function toCreateAndAdd_32 () : WC7AircraftSeat {
      return theWC7Line.createAndAddWC7AircraftSeat(theWC7Line.WC7AircraftPremiumEndorsementCond)
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 52, column 50
    function toRemove_33 (wc7AircraftSeat :  WC7AircraftSeat) : void {
      theWC7Line.removeFromWC7AircraftSeats( wc7AircraftSeat )
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 26, column 37
    function valueRoot_2 () : java.lang.Object {
      return conditionPattern
    }
    
    // 'value' attribute on HiddenInput (id=ConditionPatternDisplayName_Input) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 26, column 37
    function value_1 () : java.lang.String {
      return conditionPattern.DisplayName
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 52, column 50
    function value_34 () : entity.WC7AircraftSeat[] {
      return theWC7Line.WC7AircraftSeats
    }
    
    // 'childrenVisible' attribute on InputGroup (id=ConditionInputGroup) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 32, column 109
    function visible_36 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 97, column 101
    function visible_42 () : java.lang.Boolean {
      return openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get theWC7Line () : productmodel.WC7Line {
      return getVariableValue("theWC7Line", 0) as productmodel.WC7Line
    }
    
    property set theWC7Line ($arg :  productmodel.WC7Line) {
      setVariableValue("theWC7Line", 0, $arg)
    }
    
    function canEditAircraftSeat(wc7AircraftSeat : entity.WC7AircraftSeat) : boolean {
      return gw.api.util.DateUtil.compareIgnoreTime(wc7AircraftSeat.EffectiveDate, theWC7Line.Branch.EditEffectiveDate) == 0
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7AircraftSeat.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatDesc_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 77, column 47
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7AircraftSeat.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftNumber_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 83, column 47
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7AircraftSeat.AircraftNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatCount_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 90, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7AircraftSeat.PassengerSeats = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on TextCell (id=WC7AircraftSeatCount_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 90, column 48
    function editable_26 () : java.lang.Boolean {
      return canEditAircraftSeat(wc7AircraftSeat) 
    }
    
    // 'valueRange' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function valueRange_14 () : java.lang.Object {
      return theWC7Line.AircraftSeatsJurisdictions.sortBy(\ j -> j.Code)
    }
    
    // 'value' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function valueRoot_13 () : java.lang.Object {
      return wc7AircraftSeat
    }
    
    // 'value' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function value_11 () : typekey.Jurisdiction {
      return wc7AircraftSeat.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatDesc_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 77, column 47
    function value_18 () : java.lang.String {
      return wc7AircraftSeat.Description
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftNumber_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 83, column 47
    function value_22 () : java.lang.String {
      return wc7AircraftSeat.AircraftNumber
    }
    
    // 'value' attribute on TextCell (id=WC7AircraftSeatCount_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 90, column 48
    function value_27 () : java.lang.Integer {
      return wc7AircraftSeat.PassengerSeats
    }
    
    // 'valueRange' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function verifyValueRangeIsAllowedType_15 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=state_Cell) at CoverageInputSet.WC7AircraftPremiumEndorsementCond.pcf: line 70, column 51
    function verifyValueRange_16 () : void {
      var __valueRangeArg = theWC7Line.AircraftSeatsJurisdictions.sortBy(\ j -> j.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    property get wc7AircraftSeat () : WC7AircraftSeat {
      return getIteratedValue(1) as WC7AircraftSeat
    }
    
    
  }
  
  
}