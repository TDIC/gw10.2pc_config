package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class QuestionModalInput_DateField_NewGradDiscEffDate_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/question/QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class QuestionModalInputExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      answerContainer.getAnswer(question).DateAnswer = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function editable_1 () : java.lang.Boolean {
      return answerContainer.isNewGradEffectiveDateEditable_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 25, column 65
    function onChange_0 () : void {
      if (onChangeBlock != null) onChangeBlock();
    }
    
    // 'requestValidationExpression' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function requestValidationExpression_2 (VALUE :  java.util.Date) : java.lang.Object {
      return question.validateNewGradDiscountEffDate(VALUE)
    }
    
    // 'required' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function required_3 () : java.lang.Boolean {
      return answerContainer.isNewGradEffectiveDateMandatory_TDIC() or question.isQuestionMandatory(answerContainer)
    }
    
    // 'value' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function valueRoot_6 () : java.lang.Object {
      return answerContainer.getAnswer(question)
    }
    
    // 'value' attribute on DateCell (id=DateFieldInput_Cell) at QuestionModalInput.DateField_NewGradDiscEffDate_TDIC.pcf: line 23, column 62
    function value_4 () : java.util.Date {
      return answerContainer.getAnswer(question).DateAnswer
    }
    
    property get answerContainer () : AnswerContainer {
      return getRequireValue("answerContainer", 0) as AnswerContainer
    }
    
    property set answerContainer ($arg :  AnswerContainer) {
      setRequireValue("answerContainer", 0, $arg)
    }
    
    property get onChangeBlock () : block() {
      return getRequireValue("onChangeBlock", 0) as block()
    }
    
    property set onChangeBlock ($arg :  block()) {
      setRequireValue("onChangeBlock", 0, $arg)
    }
    
    property get question () : gw.api.productmodel.Question {
      return getRequireValue("question", 0) as gw.api.productmodel.Question
    }
    
    property set question ($arg :  gw.api.productmodel.Question) {
      setRequireValue("question", 0, $arg)
    }
    
    
  }
  
  
}