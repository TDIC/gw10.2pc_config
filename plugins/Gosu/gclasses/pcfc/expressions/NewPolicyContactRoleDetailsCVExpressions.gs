package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/NewPolicyContactRoleDetailsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPolicyContactRoleDetailsCVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/NewPolicyContactRoleDetailsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPolicyContactRoleDetailsCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactRoleDetailsCV.pcf: line 25, column 78
    function def_onEnter_2 (def :  pcf.PolicyContactDetailsDV) : void {
      def.onEnter(policyContactRole, openForEditOverride)
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactRoleDetailsCV.pcf: line 33, column 62
    function def_onEnter_4 (def :  pcf.AddressesPanelSet) : void {
      def.onEnter(contact,false,account,null)
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactRoleDetailsCV.pcf: line 25, column 78
    function def_refreshVariables_3 (def :  pcf.PolicyContactDetailsDV) : void {
      def.refreshVariables(policyContactRole, openForEditOverride)
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactRoleDetailsCV.pcf: line 33, column 62
    function def_refreshVariables_5 (def :  pcf.AddressesPanelSet) : void {
      def.refreshVariables(contact,false,account,null)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyContactRoleDetailsCV.pcf: line 16, column 30
    function initialValue_0 () : entity.Contact {
      return policyContactRole.AccountContactRole.AccountContact.Contact
    }
    
    // 'initialValue' attribute on Variable at NewPolicyContactRoleDetailsCV.pcf: line 20, column 30
    function initialValue_1 () : entity.Account {
      return policyContactRole.AccountContactRole.AccountContact.Account
    }
    
    property get account () : entity.Account {
      return getVariableValue("account", 0) as entity.Account
    }
    
    property set account ($arg :  entity.Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get openForEditOverride () : boolean {
      return getRequireValue("openForEditOverride", 0) as java.lang.Boolean
    }
    
    property set openForEditOverride ($arg :  boolean) {
      setRequireValue("openForEditOverride", 0, $arg)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    
  }
  
  
}