package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_AllVehicleCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PersonalAuto_AllVehicleCoveragesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_AllVehicleCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PersonalAuto_AllVehicleCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_114 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_116 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_118 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_120 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_122 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_124 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_126 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_128 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_130 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_132 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_134 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_136 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_138 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_140 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_142 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_144 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_146 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_148 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_150 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_152 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_154 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_156 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_158 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_166 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_168 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_170 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_172 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_174 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_176 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_178 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_180 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_182 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_184 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_186 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_188 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_190 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_192 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_194 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_196 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_198 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_200 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_202 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_204 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_206 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_216 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_onEnter_218 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_143 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function def_refreshVariables_219 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 59, column 44
    function mode_220 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_AllVehicleCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PersonalAuto_AllVehicleCoveragesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_101 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_103 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_105 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_107 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_109 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_11 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_13 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_15 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_17 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_19 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_21 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_23 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_25 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_27 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_29 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_31 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_33 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_35 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_37 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_39 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_41 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_43 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_45 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_47 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_49 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_5 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_51 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_53 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_55 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_57 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_59 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_61 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_63 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_65 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_67 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_69 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_7 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_71 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_73 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_75 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_77 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_79 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_81 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_83 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_85 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_87 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_89 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_9 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_91 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_93 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_95 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_97 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_onEnter_99 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_10 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_100 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_102 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_104 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_106 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_108 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_110 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_12 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_14 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_16 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_18 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_20 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_22 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_24 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_26 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_28 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_30 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_32 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_34 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_36 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_38 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_40 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_42 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_44 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_46 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_48 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_50 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_52 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_54 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_56 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_58 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_6 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_60 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_62 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_64 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_66 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_68 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_70 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_72 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_74 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_76 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_78 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_8 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_80 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_82 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_84 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_86 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_88 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_90 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_92 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_94 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_96 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function def_refreshVariables_98 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, paLine, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at PersonalAuto_AllVehicleCoveragesDV.pcf: line 44, column 44
    function mode_111 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/pa/policy/PersonalAuto_AllVehicleCoveragesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PersonalAuto_AllVehicleCoveragesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PersonalAuto_AllVehicleCoveragesDV.pcf: line 16, column 52
    function initialValue_0 () : gw.api.productmodel.CoverageCategory {
      return paLine.Pattern.getCoverageCategoryByPublicId("PAPLiabGrp")
    }
    
    // 'initialValue' attribute on Variable at PersonalAuto_AllVehicleCoveragesDV.pcf: line 21, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return paLine.Pattern.getCoverageCategoryByPublicId("PAPip")
    }
    
    // 'initialValue' attribute on Variable at PersonalAuto_AllVehicleCoveragesDV.pcf: line 26, column 53
    function initialValue_2 () : gw.api.productmodel.CoveragePattern[] {
      return liabilityCategory.coveragePatternsForEntity(PersonalAutoLine).whereSelectedOrAvailable(paLine, openForEdit)
    }
    
    // 'initialValue' attribute on Variable at PersonalAuto_AllVehicleCoveragesDV.pcf: line 31, column 53
    function initialValue_3 () : gw.api.productmodel.CoveragePattern[] {
      return pipCategory != null ? pipCategory.coveragePatternsForEntity(PersonalAutoLine).whereSelectedOrAvailable(paLine, openForEdit) : null
    }
    
    // 'sortBy' attribute on IteratorSort at PersonalAuto_AllVehicleCoveragesDV.pcf: line 41, column 26
    function sortBy_4 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on InputIterator (id=PALiabCategoryIterator) at PersonalAuto_AllVehicleCoveragesDV.pcf: line 38, column 59
    function value_112 () : gw.api.productmodel.CoveragePattern[] {
      return liabilityCategoryCoveragePatterns
    }
    
    // 'value' attribute on InputIterator (id=PAPipCategoryIterator) at PersonalAuto_AllVehicleCoveragesDV.pcf: line 53, column 59
    function value_221 () : gw.api.productmodel.CoveragePattern[] {
      return pipCategoryCoveragePatterns
    }
    
    property get liabilityCategory () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("liabilityCategory", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set liabilityCategory ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("liabilityCategory", 0, $arg)
    }
    
    property get liabilityCategoryCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("liabilityCategoryCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set liabilityCategoryCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("liabilityCategoryCoveragePatterns", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get paLine () : PersonalAutoLine {
      return getRequireValue("paLine", 0) as PersonalAutoLine
    }
    
    property set paLine ($arg :  PersonalAutoLine) {
      setRequireValue("paLine", 0, $arg)
    }
    
    property get pipCategory () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("pipCategory", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set pipCategory ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("pipCategory", 0, $arg)
    }
    
    property get pipCategoryCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("pipCategoryCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set pipCategoryCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("pipCategoryCoveragePatterns", 0, $arg)
    }
    
    
  }
  
  
}