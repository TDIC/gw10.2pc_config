package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7BlanketWaiverInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7BlanketWaiverInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7BlanketWaiverInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7BlanketWaiverInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7BlanketWaiverOfSubro.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at WC7BlanketWaiverInputSet.pcf: line 73, column 28
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7BlanketWaiverOfSubro.GoverningLaw = (__VALUE_TO_SET as typekey.WC7GoverningLaw)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 81, column 31
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7BlanketWaiverOfSubro.WaiverEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 89, column 31
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7BlanketWaiverOfSubro.WaiverExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7BlanketWaiverInputSet.pcf: line 52, column 45
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7BlanketWaiverOfSubro.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on DateCell (id=EffectiveDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 81, column 31
    function editable_24 () : java.lang.Boolean {
      return wc7BlanketWaiverOfSubro.canSetEffectiveWindow()
    }
    
    // 'filter' attribute on TypeKeyCell (id=WCLaw_Cell) at WC7BlanketWaiverInputSet.pcf: line 73, column 28
    function filter_22 (VALUE :  typekey.WC7GoverningLaw, VALUES :  typekey.WC7GoverningLaw[]) : java.lang.Boolean {
      return VALUE.hasCategory(typekey.WC7LiabilityAct.TC_WORKERSCOMP)
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function valueRange_15 () : java.lang.Object {
      return (wc7Line.stateFilterFor(conditionPattern).TypeKeys).sortBy(\s -> s.Code)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7BlanketWaiverInputSet.pcf: line 52, column 45
    function valueRoot_10 () : java.lang.Object {
      return wc7BlanketWaiverOfSubro
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function value_12 () : typekey.Jurisdiction {
      return wc7BlanketWaiverOfSubro.Jurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at WC7BlanketWaiverInputSet.pcf: line 73, column 28
    function value_19 () : typekey.WC7GoverningLaw {
      return wc7BlanketWaiverOfSubro.GoverningLaw
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 81, column 31
    function value_25 () : java.util.Date {
      return wc7BlanketWaiverOfSubro.WaiverEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 89, column 31
    function value_31 () : java.util.Date {
      return wc7BlanketWaiverOfSubro.WaiverExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7BlanketWaiverInputSet.pcf: line 52, column 45
    function value_8 () : java.lang.String {
      return wc7BlanketWaiverOfSubro.Description
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function verifyValueRange_17 () : void {
      var __valueRangeArg = (wc7Line.stateFilterFor(conditionPattern).TypeKeys).sortBy(\s -> s.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    property get wc7BlanketWaiverOfSubro () : WC7WaiverOfSubro {
      return getIteratedValue(1) as WC7WaiverOfSubro
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7BlanketWaiverInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7BlanketWaiverInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7BlanketWaiverInputSet.pcf: line 38, column 28
    function sortBy_0 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.Jurisdiction
    }
    
    // 'sortBy' attribute on IteratorSort at WC7BlanketWaiverInputSet.pcf: line 41, column 28
    function sortBy_1 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.Description
    }
    
    // 'sortBy' attribute on IteratorSort at WC7BlanketWaiverInputSet.pcf: line 44, column 28
    function sortBy_2 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.EffectiveDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7BlanketWaiverInputSet.pcf: line 52, column 45
    function sortValue_3 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.Description
    }
    
    // 'value' attribute on RangeCell (id=State_Cell) at WC7BlanketWaiverInputSet.pcf: line 61, column 48
    function sortValue_4 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.Jurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=WCLaw_Cell) at WC7BlanketWaiverInputSet.pcf: line 73, column 28
    function sortValue_5 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.GoverningLaw
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 81, column 31
    function sortValue_6 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.WaiverEffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at WC7BlanketWaiverInputSet.pcf: line 89, column 31
    function sortValue_7 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : java.lang.Object {
      return wc7BlanketWaiverOfSubro.WaiverExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at WC7BlanketWaiverInputSet.pcf: line 35, column 49
    function toCreateAndAdd_36 () : WC7WaiverOfSubro {
      return wc7Line.createAndAddBlanketWaiverOfSubroWM()
    }
    
    // 'toRemove' attribute on RowIterator at WC7BlanketWaiverInputSet.pcf: line 35, column 49
    function toRemove_37 (wc7BlanketWaiverOfSubro :  WC7WaiverOfSubro) : void {
      wc7BlanketWaiverOfSubro.removeWM()
    }
    
    // 'value' attribute on RowIterator at WC7BlanketWaiverInputSet.pcf: line 35, column 49
    function value_38 () : entity.WC7WaiverOfSubro[] {
      return wc7Line.WC7BlanketWaiversWM
    }
    
    property get conditionPattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("conditionPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set conditionPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("conditionPattern", 0, $arg)
    }
    
    property get wc7Line () : entity.WC7WorkersCompLine {
      return getRequireValue("wc7Line", 0) as entity.WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  entity.WC7WorkersCompLine) {
      setRequireValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  
}