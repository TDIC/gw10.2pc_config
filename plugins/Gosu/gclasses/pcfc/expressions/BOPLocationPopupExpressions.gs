package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPLocationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPLocationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountLocation :  AccountLocation, bopLocation :  BOPLocation, policyPeriod :  PolicyPeriod, openForEdit :  boolean, startInEdit :  boolean, jobWizardHelper :  gw.api.web.job.JobWizardHelper) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Standardize) at BOPLocationPopup.pcf: line 62, column 57
    function action_3 () : void {
      addressList = helper.validatePolicyLocation(bopLocation.Location); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); helper.displayExceptions()
    }
    
    // 'action' attribute on ToolbarButton (id=Override) at BOPLocationPopup.pcf: line 68, column 38
    function action_6 () : void {
      CurrentLocation.pickValueAndCommit(bopLocation)
    }
    
    // 'afterCommit' attribute on Popup (id=BOPLocationPopup) at BOPLocationPopup.pcf: line 14, column 105
    function afterCommit_38 (TopLocation :  pcf.api.Location) : void {
      gw.api.web.PebblesUtil.invalidateIterators(TopLocation, BOPLocation); bopLocation.Location.syncQuestions()
    }
    
    // 'available' attribute on ToolbarButton (id=Standardize) at BOPLocationPopup.pcf: line 62, column 57
    function available_1 () : java.lang.Boolean {
      return standardizeVisible
    }
    
    // 'available' attribute on ToolbarButton (id=Override) at BOPLocationPopup.pcf: line 68, column 38
    function available_4 () : java.lang.Boolean {
      return overrideVisible
    }
    
    // 'beforeCommit' attribute on Popup (id=BOPLocationPopup) at BOPLocationPopup.pcf: line 14, column 105
    function beforeCommit_39 (pickedValue :  BOPLocation) : void {
      gw.lob.bop.BOPLocationValidation.validateBOPLocation(bopLocation.Location)
    }
    
    // 'canEdit' attribute on Popup (id=BOPLocationPopup) at BOPLocationPopup.pcf: line 14, column 105
    function canEdit_40 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'def' attribute on PanelRef at BOPLocationPopup.pcf: line 79, column 120
    function def_onEnter_11 (def :  pcf.LocationDetailCV) : void {
      def.onEnter(bopLocation.Location, openForEdit, bopLocation.BOPLine.SupportsNonSpecificLocations)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPLocationPopup.pcf: line 77, column 54
    function def_onEnter_9 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(bopLocation, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPLocationPopup.pcf: line 77, column 54
    function def_refreshVariables_10 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(bopLocation, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef at BOPLocationPopup.pcf: line 79, column 120
    function def_refreshVariables_12 (def :  pcf.LocationDetailCV) : void {
      def.refreshVariables(bopLocation.Location, openForEdit, bopLocation.BOPLine.SupportsNonSpecificLocations)
    }
    
    // 'initialValue' attribute on Variable at BOPLocationPopup.pcf: line 45, column 76
    function initialValue_0 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // EditButtons at BOPLocationPopup.pcf: line 71, column 36
    function label_8 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at BOPLocationPopup.pcf: line 71, column 36
    function pickValue_7 () : BOPLocation {
      return bopLocation
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at BOPLocationPopup.pcf: line 108, column 56
    function sortValue_14 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at BOPLocationPopup.pcf: line 112, column 56
    function sortValue_15 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at BOPLocationPopup.pcf: line 116, column 48
    function sortValue_16 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at BOPLocationPopup.pcf: line 121, column 44
    function sortValue_17 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at BOPLocationPopup.pcf: line 125, column 54
    function sortValue_18 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'startEditing' attribute on Popup (id=BOPLocationPopup) at BOPLocationPopup.pcf: line 14, column 105
    function startEditing_41 () : void {
      maybeCreateLocation()
    }
    
    // 'startInEditMode' attribute on Popup (id=BOPLocationPopup) at BOPLocationPopup.pcf: line 14, column 105
    function startInEditMode_42 () : java.lang.Boolean {
      return startInEdit
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at BOPLocationPopup.pcf: line 95, column 65
    function value_36 () : java.util.ArrayList<entity.Address> {
      return addressList
    }
    
    // 'visible' attribute on TitleBar (id=SelectTitle) at BOPLocationPopup.pcf: line 86, column 64
    function visible_13 () : java.lang.Boolean {
      return addressList ?.Count > 0 ? true : false
    }
    
    // 'visible' attribute on ToolbarButton (id=Standardize) at BOPLocationPopup.pcf: line 62, column 57
    function visible_2 () : java.lang.Boolean {
      return standardizeVisible and openForEdit
    }
    
    override property get CurrentLocation () : pcf.BOPLocationPopup {
      return super.CurrentLocation as pcf.BOPLocationPopup
    }
    
    property get accountLocation () : AccountLocation {
      return getVariableValue("accountLocation", 0) as AccountLocation
    }
    
    property set accountLocation ($arg :  AccountLocation) {
      setVariableValue("accountLocation", 0, $arg)
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get bopLocation () : BOPLocation {
      return getVariableValue("bopLocation", 0) as BOPLocation
    }
    
    property set bopLocation ($arg :  BOPLocation) {
      setVariableValue("bopLocation", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getVariableValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setVariableValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get overrideVisible () : Boolean {
      return getVariableValue("overrideVisible", 0) as Boolean
    }
    
    property set overrideVisible ($arg :  Boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get startInEdit () : boolean {
      return getVariableValue("startInEdit", 0) as java.lang.Boolean
    }
    
    property set startInEdit ($arg :  boolean) {
      setVariableValue("startInEdit", 0, $arg)
    }
    
    property get updateVisible () : Boolean {
      return getVariableValue("updateVisible", 0) as Boolean
    }
    
    property set updateVisible ($arg :  Boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    function maybeCreateLocation() {
      if (startInEdit and openForEdit) {
        if (bopLocation == null) {
          if (accountLocation != null) {
            bopLocation = policyPeriod.BOPLine.addToLineSpecificLocations(accountLocation) as BOPLocation
            for(var tc in bopLocation.Location.TerritoryCodes)
              tc.fillWithFirst()
          } else {
            bopLocation = policyPeriod.BOPLine.addNewLineSpecificLocation() as BOPLocation
          }
        }
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPLocationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BOPLocationPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at BOPLocationPopup.pcf: line 103, column 44
    function action_20 () : void {
      helper.replaceLocationWithSuggestedAddress(suggestedAddress,bopLocation.Location); addressList = helper.getAddressList(); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible() 
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at BOPLocationPopup.pcf: line 108, column 56
    function valueRoot_22 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at BOPLocationPopup.pcf: line 108, column 56
    function value_21 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at BOPLocationPopup.pcf: line 112, column 56
    function value_24 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at BOPLocationPopup.pcf: line 116, column 48
    function value_27 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at BOPLocationPopup.pcf: line 121, column 44
    function value_30 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at BOPLocationPopup.pcf: line 125, column 54
    function value_33 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(1) as entity.Address
    }
    
    
  }
  
  
}