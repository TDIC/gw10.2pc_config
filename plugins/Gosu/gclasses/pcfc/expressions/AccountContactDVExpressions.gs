package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 86, column 36
    function def_onEnter_12 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 86, column 36
    function def_onEnter_14 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function def_onEnter_3 (def :  pcf.ContactNameInputSet_company) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function def_onEnter_5 (def :  pcf.ContactNameInputSet_person) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on InputSetRef (id=ContactCurrency) at AccountContactDV.pcf: line 58, column 31
    function def_onEnter_8 (def :  pcf.ContactCurrencyInputSet) : void {
      def.onEnter(contact, selectedAddress, contact.New)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 86, column 36
    function def_refreshVariables_13 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 86, column 36
    function def_refreshVariables_15 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function def_refreshVariables_4 (def :  pcf.ContactNameInputSet_company) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function def_refreshVariables_6 (def :  pcf.ContactNameInputSet_person) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'def' attribute on InputSetRef (id=ContactCurrency) at AccountContactDV.pcf: line 58, column 31
    function def_refreshVariables_9 (def :  pcf.ContactCurrencyInputSet) : void {
      def.refreshVariables(contact, selectedAddress, contact.New)
    }
    
    // 'value' attribute on CheckBoxInput (id=solicitDirectMail_Input) at AccountContactDV.pcf: line 97, column 127
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.DoNotSoliciteDirectMail_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=solicitEmail_Input) at AccountContactDV.pcf: line 103, column 127
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.DoNotSoliciteEMail_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at AccountContactDV.pcf: line 17, column 23
    function initialValue_0 () : boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at AccountContactDV.pcf: line 22, column 30
    function initialValue_1 () : entity.Address {
      return contact.PrimaryAddress
    }
    
    // 'mode' attribute on InputSetRef at AccountContactDV.pcf: line 86, column 36
    function mode_16 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'mode' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function mode_7 () : java.lang.Object {
      return contact.Subtype.Code
    }
    
    // 'value' attribute on CheckBoxInput (id=solicitDirectMail_Input) at AccountContactDV.pcf: line 97, column 127
    function valueRoot_22 () : java.lang.Object {
      return contact
    }
    
    // 'value' attribute on CheckBoxInput (id=solicitDirectMail_Input) at AccountContactDV.pcf: line 97, column 127
    function value_20 () : java.lang.Boolean {
      return contact.DoNotSoliciteDirectMail_TDIC
    }
    
    // 'value' attribute on CheckBoxInput (id=solicitEmail_Input) at AccountContactDV.pcf: line 103, column 127
    function value_26 () : java.lang.Boolean {
      return contact.DoNotSoliciteEMail_TDIC
    }
    
    // 'visible' attribute on InputDivider at AccountContactDV.pcf: line 88, column 127
    function visible_17 () : java.lang.Boolean {
      return contact.AccountContacts*.Roles?.hasMatch(\accountContactRole -> accountContactRole typeis NamedInsured)
    }
    
    // 'visible' attribute on InputSetRef at AccountContactDV.pcf: line 27, column 36
    function visible_2 () : java.lang.Boolean {
      return contact != null
    }
    
    property get anAccount () : Account {
      return getRequireValue("anAccount", 0) as Account
    }
    
    property set anAccount ($arg :  Account) {
      setRequireValue("anAccount", 0, $arg)
    }
    
    property get contact () : Contact {
      return getRequireValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get selectedAddress () : entity.Address {
      return getVariableValue("selectedAddress", 0) as entity.Address
    }
    
    property set selectedAddress ($arg :  entity.Address) {
      setVariableValue("selectedAddress", 0, $arg)
    }
    
    
  }
  
  
}