package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/job/LineWizardStepSet.BusinessOwners.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LineWizardStepSet_BusinessOwnersExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/job/LineWizardStepSet.BusinessOwners.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LineWizardStepSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function beforeSave_14 () : void {
      gw.lob.bop.BOPLineValidation.validateSupplementalStep(policyPeriod.BOPLine)
    }
    
    // 'beforeSave' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function beforeSave_6 () : void {
      gw.lob.bop.BOPLineValidation.validateBuildings(policyPeriod.BOPLine)
    }
    
    // 'handlesValidationIssue' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function handlesValidationIssue_0 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return VALUE typeis entity.BOPLocation
    }
    
    // 'handlesValidationIssue' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function handlesValidationIssue_7 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return VALUE typeis entity.BOPBuilding
    }
    
    // 'onEnter' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function onEnter_1 () : void {
      if (openForEdit) { gw.web.productmodel.ProductModelSyncIssuesHandler.sync(policyPeriod.BOPLine.BOPLocations, null, policyPeriod.PolicyLocations, null, jobWizardHelper)}
    }
    
    // 'onEnter' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function onEnter_15 () : void {
      if(openForEdit) { gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions( {policyPeriod.BOPLine}, jobWizardHelper )}
    }
    
    // 'onEnter' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function onEnter_8 () : void {
      if (openForEdit) {gw.web.productmodel.ProductModelSyncIssuesHandler.syncCoverages(policyPeriod.BOPLine.BOPLocations*.Buildings, jobWizardHelper);gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions(policyPeriod.BOPLine.PolicyLocations, jobWizardHelper )};
    }
    
    // 'onExit' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function onExit_16 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'onExit' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function onExit_2 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'onExit' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function onExit_9 () : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(policyPeriod)
    }
    
    // 'save' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function save_3 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'screen' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function screen_onEnter_11 (def :  pcf.BOPBuildingsScreen) : void {
      def.onEnter(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function screen_onEnter_18 (def :  pcf.BOPSupplementalScreen) : void {
      def.onEnter(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function screen_onEnter_4 (def :  pcf.BOPLocationsScreen) : void {
      def.onEnter(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=BOPBuildings) at LineWizardStepSet.BusinessOwners.pcf: line 49, column 73
    function screen_refreshVariables_12 (def :  pcf.BOPBuildingsScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit, jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function screen_refreshVariables_19 (def :  pcf.BOPSupplementalScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'screen' attribute on JobWizardStep (id=Locations) at LineWizardStepSet.BusinessOwners.pcf: line 40, column 73
    function screen_refreshVariables_5 (def :  pcf.BOPLocationsScreen) : void {
      def.refreshVariables(policyPeriod, openForEdit,jobWizardHelper)
    }
    
    // 'visible' attribute on JobWizardStep (id=BOPSupplemental) at LineWizardStepSet.BusinessOwners.pcf: line 60, column 139
    function visible_13 () : java.lang.Boolean {
      return false//policyPeriod.BOPLine.hasSupplementalQuestions() and (not (job typeis Submission) or (job as Submission).FullMode)
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}