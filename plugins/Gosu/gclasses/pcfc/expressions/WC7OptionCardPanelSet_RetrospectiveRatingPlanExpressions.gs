package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7OptionCardPanelSet_RetrospectiveRatingPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends WC7OptionCardPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 266, column 49
    function defaultSetter_100 (__VALUE_TO_SET :  java.lang.Object) : void {
      letter.Amount = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on DateCell (id=Effective_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 273, column 41
    function defaultSetter_104 (__VALUE_TO_SET :  java.lang.Object) : void {
      letter.ValidFrom = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=Expiration_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 280, column 39
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      letter.ValidTo = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=LOCIssuer_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 258, column 45
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      letter.IssuerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=LOCIssuer_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 258, column 45
    function valueRoot_97 () : java.lang.Object {
      return letter
    }
    
    // 'value' attribute on DateCell (id=Effective_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 273, column 41
    function value_103 () : java.util.Date {
      return letter.ValidFrom
    }
    
    // 'value' attribute on DateCell (id=Expiration_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 280, column 39
    function value_107 () : java.util.Date {
      return letter.ValidTo
    }
    
    // 'value' attribute on TextCell (id=LOCIssuer_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 258, column 45
    function value_95 () : java.lang.String {
      return letter.IssuerName
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 266, column 49
    function value_99 () : java.math.BigDecimal {
      return letter.Amount
    }
    
    property get letter () : entity.WC7RetroRatingLetterOfCredit {
      return getIteratedValue(1) as entity.WC7RetroRatingLetterOfCredit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends StateMultiplierLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=SelectState_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 192, column 28
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdictionMultiplier.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TextCell (id=StateTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 200, column 49
    function defaultSetter_73 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdictionMultiplier.JurisdictionTaxMultiplier = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=FederalTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 208, column 49
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdictionMultiplier.FederalTaxMultiplier = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=StateExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 217, column 33
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdictionMultiplier.JurisdictionExcessLossFactor = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=FederalExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 225, column 49
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      jurisdictionMultiplier.FederalExcessLossFactor = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'filter' attribute on TypeKeyCell (id=SelectState_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 192, column 28
    function filter_70 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return nonMonopolisticStates.contains(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(VALUE))
    }
    
    // 'value' attribute on TypeKeyCell (id=SelectState_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 192, column 28
    function valueRoot_69 () : java.lang.Object {
      return jurisdictionMultiplier
    }
    
    // 'value' attribute on TypeKeyCell (id=SelectState_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 192, column 28
    function value_67 () : typekey.Jurisdiction {
      return jurisdictionMultiplier.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=StateTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 200, column 49
    function value_72 () : java.math.BigDecimal {
      return jurisdictionMultiplier.JurisdictionTaxMultiplier
    }
    
    // 'value' attribute on TextCell (id=FederalTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 208, column 49
    function value_76 () : java.math.BigDecimal {
      return jurisdictionMultiplier.FederalTaxMultiplier
    }
    
    // 'value' attribute on TextCell (id=StateExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 217, column 33
    function value_80 () : java.math.BigDecimal {
      return jurisdictionMultiplier.JurisdictionExcessLossFactor
    }
    
    // 'value' attribute on TextCell (id=FederalExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 225, column 49
    function value_84 () : java.math.BigDecimal {
      return jurisdictionMultiplier.FederalExcessLossFactor
    }
    
    property get jurisdictionMultiplier () : entity.WC7JurisdictionMultiplier {
      return getIteratedValue(2) as entity.WC7JurisdictionMultiplier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class StateMultiplierLVExpressionsImpl extends WC7OptionCardPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 170, column 42
    function initialValue_61 () : gw.api.domain.StateSet {
      return gw.api.domain.StateSet.get( gw.api.domain.StateSet.WC_NOTMONOPOLISTIC )
    }
    
    // 'value' attribute on TypeKeyCell (id=SelectState_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 192, column 28
    function sortValue_62 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : java.lang.Object {
      return jurisdictionMultiplier.Jurisdiction
    }
    
    // 'value' attribute on TextCell (id=StateTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 200, column 49
    function sortValue_63 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : java.lang.Object {
      return jurisdictionMultiplier.JurisdictionTaxMultiplier
    }
    
    // 'value' attribute on TextCell (id=FederalTaxMultiplier_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 208, column 49
    function sortValue_64 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : java.lang.Object {
      return jurisdictionMultiplier.FederalTaxMultiplier
    }
    
    // 'value' attribute on TextCell (id=StateExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 217, column 33
    function sortValue_65 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : java.lang.Object {
      return jurisdictionMultiplier.JurisdictionExcessLossFactor
    }
    
    // 'value' attribute on TextCell (id=FederalExcessLossFactor_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 225, column 49
    function sortValue_66 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : java.lang.Object {
      return jurisdictionMultiplier.FederalExcessLossFactor
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 181, column 78
    function toCreateAndAdd_88 () : entity.WC7JurisdictionMultiplier {
      return retrospectiveRatingPlanWM.createAndAddWC7JurisdictionMultiplierWM()
    }
    
    // 'toRemove' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 181, column 78
    function toRemove_89 (jurisdictionMultiplier :  entity.WC7JurisdictionMultiplier) : void {
      jurisdictionMultiplier.removeWM()
    }
    
    // 'value' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 181, column 78
    function value_90 () : java.util.List<entity.WC7JurisdictionMultiplier> {
      return retrospectiveRatingPlanWM.JurisdictionMultipliersWM
    }
    
    property get nonMonopolisticStates () : gw.api.domain.StateSet {
      return getVariableValue("nonMonopolisticStates", 1) as gw.api.domain.StateSet
    }
    
    property set nonMonopolisticStates ($arg :  gw.api.domain.StateSet) {
      setVariableValue("nonMonopolisticStates", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7OptionCardPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=LossLimitAmount_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 43, column 47
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.LossLimitAmount = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextInput (id=EstimatedStandardPremium_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 51, column 47
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.EstimatedStandardPremium = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on DateInput (id=FirstComputationDate_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 57, column 69
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.FirstComputationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeALAE_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 28, column 44
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.IncludeALAE = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=LastComputationDate_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 63, column 68
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.LastComputationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=ComputationInterval_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 70, column 44
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.ComputationInterval = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=MinRetroPremiumRatio_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 77, column 47
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.MinRetroPremiumRatio = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextInput (id=MaxRetroPremiumRatio_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 84, column 47
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.MaxRetroPremiumRatio = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium1_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 121, column 47
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.PercentStandardPremium1 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium2_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 126, column 47
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.PercentStandardPremium2 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium3_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 131, column 47
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.PercentStandardPremium3 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor1_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 144, column 47
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.BasicPremiumFactor1 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor2_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 149, column 47
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.BasicPremiumFactor2 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor3_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 154, column 47
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.BasicPremiumFactor3 = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextInput (id=LossConversionFactor_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 35, column 47
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrospectiveRatingPlanWM.LossConversionFactor = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'initialValue' attribute on Variable at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 17, column 49
    function initialValue_0 () : entity.WC7RetrospectiveRatingPlan {
      return wc7Line.RetrospectiveRatingPlanWM
    }
    
    // 'value' attribute on TextCell (id=LOCIssuer_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 258, column 45
    function sortValue_91 (letter :  entity.WC7RetroRatingLetterOfCredit) : java.lang.Object {
      return letter.IssuerName
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 266, column 49
    function sortValue_92 (letter :  entity.WC7RetroRatingLetterOfCredit) : java.lang.Object {
      return letter.Amount
    }
    
    // 'value' attribute on DateCell (id=Effective_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 273, column 41
    function sortValue_93 (letter :  entity.WC7RetroRatingLetterOfCredit) : java.lang.Object {
      return letter.ValidFrom
    }
    
    // 'value' attribute on DateCell (id=Expiration_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 280, column 39
    function sortValue_94 (letter :  entity.WC7RetroRatingLetterOfCredit) : java.lang.Object {
      return letter.ValidTo
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 249, column 81
    function toCreateAndAdd_111 () : entity.WC7RetroRatingLetterOfCredit {
      return retrospectiveRatingPlanWM.createAndAddWC7LetterOfCreditWM()
    }
    
    // 'toRemove' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 249, column 81
    function toRemove_112 (letter :  entity.WC7RetroRatingLetterOfCredit) : void {
      letter.removeWM()
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeALAE_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 28, column 44
    function valueRoot_3 () : java.lang.Object {
      return retrospectiveRatingPlanWM
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeALAE_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 28, column 44
    function value_1 () : java.lang.Boolean {
      return retrospectiveRatingPlanWM.IncludeALAE
    }
    
    // 'value' attribute on RowIterator at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 249, column 81
    function value_113 () : java.util.List<entity.WC7RetroRatingLetterOfCredit> {
      return retrospectiveRatingPlanWM.LettersOfCreditWM
    }
    
    // 'value' attribute on TextInput (id=EstimatedStandardPremium_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 51, column 47
    function value_13 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.EstimatedStandardPremium
    }
    
    // 'value' attribute on DateInput (id=FirstComputationDate_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 57, column 69
    function value_17 () : java.util.Date {
      return retrospectiveRatingPlanWM.FirstComputationDate
    }
    
    // 'value' attribute on DateInput (id=LastComputationDate_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 63, column 68
    function value_21 () : java.util.Date {
      return retrospectiveRatingPlanWM.LastComputationDate
    }
    
    // 'value' attribute on TextInput (id=ComputationInterval_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 70, column 44
    function value_25 () : java.lang.Integer {
      return retrospectiveRatingPlanWM.ComputationInterval
    }
    
    // 'value' attribute on TextInput (id=MinRetroPremiumRatio_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 77, column 47
    function value_29 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.MinRetroPremiumRatio
    }
    
    // 'value' attribute on TextInput (id=MaxRetroPremiumRatio_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 84, column 47
    function value_33 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.MaxRetroPremiumRatio
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium1_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 121, column 47
    function value_37 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.PercentStandardPremium1
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium2_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 126, column 47
    function value_41 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.PercentStandardPremium2
    }
    
    // 'value' attribute on TextCell (id=PercentStandardPremium3_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 131, column 47
    function value_45 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.PercentStandardPremium3
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor1_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 144, column 47
    function value_49 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.BasicPremiumFactor1
    }
    
    // 'value' attribute on TextInput (id=LossConversionFactor_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 35, column 47
    function value_5 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.LossConversionFactor
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor2_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 149, column 47
    function value_53 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.BasicPremiumFactor2
    }
    
    // 'value' attribute on TextCell (id=BasicPremiumFactor3_Cell) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 154, column 47
    function value_57 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.BasicPremiumFactor3
    }
    
    // 'value' attribute on TextInput (id=LossLimitAmount_Input) at WC7OptionCardPanelSet.RetrospectiveRatingPlan.pcf: line 43, column 47
    function value_9 () : java.math.BigDecimal {
      return retrospectiveRatingPlanWM.LossLimitAmount
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get retrospectiveRatingPlanWM () : entity.WC7RetrospectiveRatingPlan {
      return getVariableValue("retrospectiveRatingPlanWM", 0) as entity.WC7RetrospectiveRatingPlan
    }
    
    property set retrospectiveRatingPlanWM ($arg :  entity.WC7RetrospectiveRatingPlan) {
      setVariableValue("retrospectiveRatingPlanWM", 0, $arg)
    }
    
    property get wc7Line () : WC7WorkersCompLine {
      return getRequireValue("wc7Line", 0) as WC7WorkersCompLine
    }
    
    property set wc7Line ($arg :  WC7WorkersCompLine) {
      setRequireValue("wc7Line", 0, $arg)
    }
    
    
  }
  
  
}