package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLPolicyOwnerOfficerDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_GLPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 61, column 100
    function conversionExpression_9 (PickedValue :  GLPolicyEntityOwner_TDIC) : entity.GLPolicyOwnerOfficer_TDIC {
      return PickedValue as GLPolicyEntityOwner_TDIC
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 61, column 100
    function label_8 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewEntityOwnerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 61, column 100
    function pickLocation_10 () : void {
      TDIC_GLNewEntityOwnerPopup.push(glLine.Branch.GLLine, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_GLPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 92, column 95
    function label_17 () : java.lang.Object {
      return ownerOfficer
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedOwnerOfficer) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 92, column 95
    function toCreateAndAdd_18 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addGLPolicyOwnerOfficer_TDIC(ownerOfficer.Contact)
    }
    
    property get ownerOfficer () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TDIC_GLPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 118, column 93
    function label_24 () : java.lang.Object {
      return entityOwner
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=UnassignedEntityOwner) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 118, column 93
    function toCreateAndAdd_25 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addGLPolicyEntityOwner_TDIC(entityOwner.Contact)
    }
    
    property get entityOwner () : entity.AccountContact {
      return getIteratedValue(1) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends TDIC_GLPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 156, column 31
    function action_34 () : void {
      EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=DispName_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 156, column 31
    function action_dest_35 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(policyOwnerOfficer, openForEdit)
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 164, column 111
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=ADA_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 164, column 111
    function editable_44 () : java.lang.Boolean {
      return policyOwnerOfficer.ContactDenorm.Subtype == TC_PERSON
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_GLPolicyOwnerOfficerDV.pcf: line 167, column 182
    function onChange_39 () : void {
      tdic.web.admin.shared.SharedUIHelper.performMembershipCheck_TDIC(policyOwnerOfficer.AccountContactRole.AccountContact.Contact, true, period.BaseState)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLPolicyOwnerOfficerDV.pcf: line 175, column 36
    function sortBy_40 (otherContact :  entity.AccountContact) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 156, column 31
    function valueRoot_37 () : java.lang.Object {
      return policyOwnerOfficer
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 164, column 111
    function valueRoot_47 () : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 156, column 31
    function value_36 () : java.lang.String {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 172, column 55
    function value_43 () : entity.AccountContact[] {
      return glLine.GLOwnerOfficerOtherCandidates_TDIC
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 164, column 111
    function value_45 () : java.lang.String {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    property get policyOwnerOfficer () : entity.GLPolicyOwnerOfficer_TDIC {
      return getIteratedValue(1) as entity.GLPolicyOwnerOfficer_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends IteratorEntry5ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 180, column 97
    function label_41 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 180, column 97
    function toCreateAndAdd_42 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addGLPolicyOwnerOfficer_TDIC(otherContact.Contact)
    }
    
    property get otherContact () : entity.AccountContact {
      return getIteratedValue(2) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_GLPolicyOwnerOfficerDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 46, column 101
    function conversionExpression_4 (PickedValue :  GLPolicyOwnerOfficer_TDIC) : entity.GLPolicyOwnerOfficer_TDIC {
      return PickedValue as GLPolicyOwnerOfficer_TDIC
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 46, column 101
    function label_3 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Contact.AddNewOwnerOfficerOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 46, column 101
    function pickLocation_5 () : void {
      TDIC_GLNewOwnerOfficerPopup.push(glLine.Branch.GLLine, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_GLPolicyOwnerOfficerDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLPolicyOwnerOfficerDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 68, column 72
    function conversionExpression_12 (PickedValue :  Contact) : entity.GLPolicyOwnerOfficer_TDIC {
      return glLine.addGLPolicyOwnerOfficer_TDIC(PickedValue)
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 74, column 103
    function conversionExpression_14 (PickedValue :  Contact) : entity.GLPolicyOwnerOfficer_TDIC {
      return glLine.addGLPolicyEntityOwner_TDIC(PickedValue)
    }
    
    // 'editable' attribute on RowIterator at TDIC_GLPolicyOwnerOfficerDV.pcf: line 149, column 60
    function editable_33 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLPolicyOwnerOfficerDV.pcf: line 16, column 22
    function initialValue_0 () : GLLine {
      return period.GLLine
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLPolicyOwnerOfficerDV.pcf: line 20, column 54
    function initialValue_1 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 79, column 30
    function label_22 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyOwnerOfficer.Type.TypeInfo.DisplayName)
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingEntityOwnerContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 105, column 30
    function label_29 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddOwnerOfficerFromSearch) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 68, column 72
    function pickLocation_13 () : void {
      ContactSearchPopup.push(TC_OWNEROFFICER)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddEntityOwnerFromSearch) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 74, column 103
    function pickLocation_15 () : void {
      ContactSearchPopup.push(typekey.AccountContactRole.TC_ENTITYOWNER_TDIC)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLPolicyOwnerOfficerDV.pcf: line 87, column 34
    function sortBy_16 (ownerOfficer :  entity.AccountContact) : java.lang.Object {
      return ownerOfficer.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLPolicyOwnerOfficerDV.pcf: line 40, column 32
    function sortBy_2 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLPolicyOwnerOfficerDV.pcf: line 113, column 34
    function sortBy_23 (entityOwner :  entity.AccountContact) : java.lang.Object {
      return OwnerOfficer.Type.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DispName_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 156, column 31
    function sortValue_31 (policyOwnerOfficer :  entity.GLPolicyOwnerOfficer_TDIC) : java.lang.Object {
      return policyOwnerOfficer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ADA_Cell) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 164, column 111
    function sortValue_32 (policyOwnerOfficer :  entity.GLPolicyOwnerOfficer_TDIC) : java.lang.Object {
      return policyOwnerOfficer.AccountContactRole.AccountContact.Contact.ADANumber_TDICOfficialID
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 99, column 78
    function toCreateAndAdd_21 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addAllExistingGLPolicyOwnerOfficer_TDIC()
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddAll) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 125, column 79
    function toCreateAndAdd_28 (CheckedValues :  Object[]) : java.lang.Object {
      return glLine.addAllExistingGLPolicyEntityOwners_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at TDIC_GLPolicyOwnerOfficerDV.pcf: line 149, column 60
    function toRemove_50 (policyOwnerOfficer :  entity.GLPolicyOwnerOfficer_TDIC) : void {
      glLine.removeFromGLPolicyOwnerOfficer_TDIC(policyOwnerOfficer)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewEntityOwnerContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 52, column 49
    function value_11 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_BOPPOLICYENTITYOWNER_TDIC)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 84, column 53
    function value_19 () : entity.AccountContact[] {
      return glLine.UnassignedOwnerOfficers_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 110, column 53
    function value_26 () : entity.AccountContact[] {
      return glLine.UnassignedGLEntityOwners_TDIC
    }
    
    // 'value' attribute on RowIterator at TDIC_GLPolicyOwnerOfficerDV.pcf: line 149, column 60
    function value_51 () : entity.GLPolicyOwnerOfficer_TDIC[] {
      return glLine.GLPolicyOwnerOfficer_TDIC
    }
    
    // 'value' attribute on AddMenuItemIterator (id=AddNewOwnerOfficerContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 37, column 49
    function value_6 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_GLPOLICYOWNEROFFICER_TDIC)
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 99, column 78
    function visible_20 () : java.lang.Boolean {
      return glLine.UnassignedOwnerOfficers_TDIC.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddAll) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 125, column 79
    function visible_27 () : java.lang.Boolean {
      return glLine.UnassignedGLEntityOwners_TDIC.length > 0
    }
    
    // 'visible' attribute on AddMenuItem (id=AddOtherContact) at TDIC_GLPolicyOwnerOfficerDV.pcf: line 131, column 81
    function visible_30 () : java.lang.Boolean {
      return glLine.GLOwnerOfficerOtherCandidates_TDIC.Count > 0
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get glLine () : GLLine {
      return getVariableValue("glLine", 0) as GLLine
    }
    
    property set glLine ($arg :  GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get period () : entity.PolicyPeriod {
      return getRequireValue("period", 0) as entity.PolicyPeriod
    }
    
    property set period ($arg :  entity.PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    
  }
  
  
}