package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDocumentInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends OnBaseDocumentLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLink) at OnBaseDocumentInputSet.pcf: line 80, column 152
    function action_18 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLink2) at OnBaseDocumentInputSet.pcf: line 89, column 150
    function action_23 () : void {
      Document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLink) at OnBaseDocumentInputSet.pcf: line 80, column 152
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document, documentsActionsHelper.DocumentContentActionsAvailable)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at OnBaseDocumentInputSet.pcf: line 59, column 36
    function icon_12 () : java.lang.String {
      return (Document as Document).Icon
    }
    
    // 'label' attribute on Link (id=NameLink) at OnBaseDocumentInputSet.pcf: line 80, column 152
    function label_19 () : java.lang.Object {
      return Document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLink) at OnBaseDocumentInputSet.pcf: line 80, column 152
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function valueRoot_14 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentInputSet.pcf: line 105, column 55
    function valueRoot_33 () : java.lang.Object {
      return (Document as Document)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function value_13 () : java.lang.String {
      return Document.DocUID
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentInputSet.pcf: line 95, column 49
    function value_26 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at OnBaseDocumentInputSet.pcf: line 100, column 62
    function value_29 () : typekey.OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentInputSet.pcf: line 105, column 55
    function value_32 () : typekey.DocumentStatusType {
      return (Document as Document).Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentInputSet.pcf: line 110, column 54
    function value_35 () : java.lang.String {
      return (Document as Document).Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentInputSet.pcf: line 118, column 60
    function value_38 () : java.util.Date {
      return (Document as Document).DateModified
    }
    
    // 'visible' attribute on Link (id=NameLink) at OnBaseDocumentInputSet.pcf: line 80, column 152
    function visible_17 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLink2) at OnBaseDocumentInputSet.pcf: line 89, column 150
    function visible_22 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get Document () : entity.Document {
      return getIteratedValue(2) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 33, column 106
    function action_2 () : void {
      OnBasePickExistingDocumentPopup.push(LinkedEntity, LinkType, Beans)
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 33, column 106
    function action_dest_3 () : pcf.api.Destination {
      return pcf.OnBasePickExistingDocumentPopup.createDestination(LinkedEntity, LinkType, Beans)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=OnBaseDocumentListPopup_UnlinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 38, column 108
    function allCheckedRowsAction_4 (CheckedValues :  entity.Document[], CheckedValuesErrors :  java.util.Map) : void {
      DocumentLinking.unlinkDocumentsFromEntity(LinkedEntity, CheckedValues , LinkType);
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentInputSet.pcf: line 19, column 58
    function initialValue_0 () : acc.onbase.api.application.DocumentLinking {
      return new acc.onbase.api.application.DocumentLinking()
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentInputSet.pcf: line 24, column 43
    function initialValue_1 () : List<entity.Document> {
      return DocumentLinking.getDocumentsLinkedToEntity(LinkedEntity, LinkType)
    }
    
    property get Beans () : KeyableBean[] {
      return getRequireValue("Beans", 0) as KeyableBean[]
    }
    
    property set Beans ($arg :  KeyableBean[]) {
      setRequireValue("Beans", 0, $arg)
    }
    
    property get DocumentLinking () : acc.onbase.api.application.DocumentLinking {
      return getVariableValue("DocumentLinking", 0) as acc.onbase.api.application.DocumentLinking
    }
    
    property set DocumentLinking ($arg :  acc.onbase.api.application.DocumentLinking) {
      setVariableValue("DocumentLinking", 0, $arg)
    }
    
    property get LinkType () : acc.onbase.configuration.DocumentLinkType {
      return getRequireValue("LinkType", 0) as acc.onbase.configuration.DocumentLinkType
    }
    
    property set LinkType ($arg :  acc.onbase.configuration.DocumentLinkType) {
      setRequireValue("LinkType", 0, $arg)
    }
    
    property get LinkedDocuments () : List<entity.Document> {
      return getVariableValue("LinkedDocuments", 0) as List<entity.Document>
    }
    
    property set LinkedDocuments ($arg :  List<entity.Document>) {
      setVariableValue("LinkedDocuments", 0, $arg)
    }
    
    property get LinkedEntity () : KeyableBean {
      return getRequireValue("LinkedEntity", 0) as KeyableBean
    }
    
    property set LinkedEntity ($arg :  KeyableBean) {
      setRequireValue("LinkedEntity", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentLVExpressionsImpl extends OnBaseDocumentInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentInputSet.pcf: line 110, column 54
    function sortValue_10 (Document :  entity.Document) : java.lang.Object {
      return (Document as Document).Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentInputSet.pcf: line 118, column 60
    function sortValue_11 (Document :  entity.Document) : java.lang.Object {
      return (Document as Document).DateModified
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function sortValue_5 (Document :  entity.Document) : java.lang.Object {
      return Document.DocUID
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at OnBaseDocumentInputSet.pcf: line 71, column 27
    function sortValue_6 (Document :  entity.Document) : java.lang.Object {
      return (Document as Document).Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentInputSet.pcf: line 95, column 49
    function sortValue_7 (Document :  entity.Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at OnBaseDocumentInputSet.pcf: line 100, column 62
    function sortValue_8 (Document :  entity.Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentInputSet.pcf: line 105, column 55
    function sortValue_9 (Document :  entity.Document) : java.lang.Object {
      return (Document as Document).Status
    }
    
    // 'value' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function value_41 () : List<entity.Document> {
      return LinkedDocuments
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_42 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_42 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueType_43 () : void {
      var __valueTypeArg : List<entity.Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_42(__valueTypeArg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 1) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 1, $arg)
    }
    
    
  }
  
  
}