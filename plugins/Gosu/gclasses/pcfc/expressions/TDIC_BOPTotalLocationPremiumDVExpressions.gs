package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPTotalLocationPremiumDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPTotalLocationPremiumDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPTotalLocationPremiumDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPTotalLocationPremiumDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=policyKeyWord_Input) at TDIC_BOPTotalLocationPremiumDV.pcf: line 20, column 101
    function value_0 () : java.lang.String {
      return DisplayKey.get("Web.Policy.BOP.Total.Location.Premium", totalLocPremium)
    }
    
    property get totalLocPremium () : gw.pl.currency.MonetaryAmount {
      return getRequireValue("totalLocPremium", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalLocPremium ($arg :  gw.pl.currency.MonetaryAmount) {
      setRequireValue("totalLocPremium", 0, $arg)
    }
    
    
  }
  
  
}