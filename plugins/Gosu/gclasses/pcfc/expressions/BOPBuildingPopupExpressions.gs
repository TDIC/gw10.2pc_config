package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuildingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BOPBuildingPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuildingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BOPBuildingPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (bopLine :  BOPLine, bopLocation :  BOPLocation, building :  BOPBuilding, openForEdit :  boolean, startInEdit :  boolean, jobWizardHelper :  gw.api.web.job.JobWizardHelper) : int {
      return 0
    }
    
    // 'afterReturnFromPopup' attribute on Popup (id=BOPBuildingPopup) at BOPBuildingPopup.pcf: line 14, column 96
    function afterReturnFromPopup_52 (popupCommitted :  boolean) : void {
      jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(bopLine.Branch)
    }
    
    // 'beforeCommit' attribute on Popup (id=BOPBuildingPopup) at BOPBuildingPopup.pcf: line 14, column 96
    function beforeCommit_53 (pickedValue :  BOPBuilding) : void {
      gw.lob.bop.BOPBuildingValidation.validateBuilding(building);gw.validation.ValidationUtil.checkCurrentResult();gw.lob.bop.BOPBuildingValidation.validateBuildingRules_TDIC(building,quoteType)
    }
    
    // 'canEdit' attribute on Popup (id=BOPBuildingPopup) at BOPBuildingPopup.pcf: line 14, column 96
    function canEdit_54 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 72, column 153
    function def_onEnter_11 (def :  pcf.LossPayeeDetails_TDICDV) : void {
      def.onEnter(building,openForEdit)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 83, column 69
    function def_onEnter_14 (def :  pcf.BOPLinePropertyCoverageDV) : void {
      def.onEnter(bopLine,building,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 90, column 70
    function def_onEnter_17 (def :  pcf.BOPLineLiabilityCoverageDV) : void {
      def.onEnter(bopLine,building,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 94, column 165
    function def_onEnter_21 (def :  pcf.TDIC_BuildingAdditionalInsuredsDV) : void {
      def.onEnter(building, openForEdit, true, false)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 101, column 78
    function def_onEnter_24 (def :  pcf.BOPAdditionalCoveragesDV) : void {
      def.onEnter(bopLine,building,quoteType,true)
    }
    
    // 'def' attribute on InputSetRef at BOPBuildingPopup.pcf: line 114, column 84
    function def_onEnter_28 (def :  pcf.TDIC_BOPModifiersInputSet) : void {
      def.onEnter(setNSRValues().toList(), bopLine.Branch)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPBuildingPopup.pcf: line 55, column 52
    function def_onEnter_3 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(building, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at BOPBuildingPopup.pcf: line 201, column 31
    function def_onEnter_48 (def :  pcf.QuestionSets_TDICDV) : void {
      def.onEnter(underwritingQuestionSets.where(\questionSet -> questionSet.CodeIdentifier == "BOPBuildingSupp"),bopLocation.PolicyLocation,null,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 64, column 102
    function def_onEnter_5 (def :  pcf.BOPBuilding_DetailsDV) : void {
      def.onEnter(building, CurrentLocation.InEditMode,bopLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 67, column 250
    function def_onEnter_8 (def :  pcf.MortgageeDetails_TDICDV) : void {
      def.onEnter(building,openForEdit)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 72, column 153
    function def_refreshVariables_12 (def :  pcf.LossPayeeDetails_TDICDV) : void {
      def.refreshVariables(building,openForEdit)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 83, column 69
    function def_refreshVariables_15 (def :  pcf.BOPLinePropertyCoverageDV) : void {
      def.refreshVariables(bopLine,building,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 90, column 70
    function def_refreshVariables_18 (def :  pcf.BOPLineLiabilityCoverageDV) : void {
      def.refreshVariables(bopLine,building,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 94, column 165
    function def_refreshVariables_22 (def :  pcf.TDIC_BuildingAdditionalInsuredsDV) : void {
      def.refreshVariables(building, openForEdit, true, false)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 101, column 78
    function def_refreshVariables_25 (def :  pcf.BOPAdditionalCoveragesDV) : void {
      def.refreshVariables(bopLine,building,quoteType,true)
    }
    
    // 'def' attribute on InputSetRef at BOPBuildingPopup.pcf: line 114, column 84
    function def_refreshVariables_29 (def :  pcf.TDIC_BOPModifiersInputSet) : void {
      def.refreshVariables(setNSRValues().toList(), bopLine.Branch)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at BOPBuildingPopup.pcf: line 55, column 52
    function def_refreshVariables_4 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(building, openForEdit, jobWizardHelper)
    }
    
    // 'def' attribute on PanelRef (id=QuestionSet) at BOPBuildingPopup.pcf: line 201, column 31
    function def_refreshVariables_49 (def :  pcf.QuestionSets_TDICDV) : void {
      def.refreshVariables(underwritingQuestionSets.where(\questionSet -> questionSet.CodeIdentifier == "BOPBuildingSupp"),bopLocation.PolicyLocation,null,true)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 64, column 102
    function def_refreshVariables_6 (def :  pcf.BOPBuilding_DetailsDV) : void {
      def.refreshVariables(building, CurrentLocation.InEditMode,bopLine,quoteType)
    }
    
    // 'def' attribute on PanelRef at BOPBuildingPopup.pcf: line 67, column 250
    function def_refreshVariables_9 (def :  pcf.MortgageeDetails_TDICDV) : void {
      def.refreshVariables(building,openForEdit)
    }
    
    // 'editable' attribute on PanelRef at BOPBuildingPopup.pcf: line 94, column 165
    function editable_19 () : java.lang.Boolean {
      return bopLine?.Branch?.profileChange_TDIC
    }
    
    // 'editable' attribute on InputSetRef at BOPBuildingPopup.pcf: line 114, column 84
    function editable_27 () : java.lang.Boolean {
      return bopLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT
    }
    
    // 'editable' attribute on PanelRef at BOPBuildingPopup.pcf: line 120, column 134
    function editable_43 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser && bopLine?.Branch?.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at BOPBuildingPopup.pcf: line 40, column 49
    function initialValue_0 () : gw.api.productmodel.QuestionSet[] {
      return bopLocation.Branch.Policy.Product.getQuestionSetsByType(QuestionSetType.TC_UNDERWRITING)
    }
    
    // 'initialValue' attribute on Variable at BOPBuildingPopup.pcf: line 45, column 25
    function initialValue_1 () : QuoteType {
      return setInitialQuoteType()
    }
    
    // 'onSelect' attribute on Card (id=BOPBuilding_DetailsCard) at BOPBuildingPopup.pcf: line 60, column 80
    function onSelect_13 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncModifiers( {building}, jobWizardHelper)
    }
    
    // 'onSelect' attribute on Card (id=PropertyCov) at BOPBuildingPopup.pcf: line 81, column 71
    function onSelect_16 () : void {
      building.syncCoverages()
    }
    
    // 'onSelect' attribute on Card (id=BOPAddlCov) at BOPBuildingPopup.pcf: line 99, column 91
    function onSelect_26 () : void {
      building.setOrdinanceTermLimits_TDIC();building.syncCoverages();building.setEarthquakeLimits_TDIC();building.setEQSPExistence_TDIC();building.setILSubLimit_TDIC();building.setEnhancedCoverageLimits();building.remEnhancedCoverageLimits()
    }
    
    // 'onSelect' attribute on Card (id=Modifiers) at BOPBuildingPopup.pcf: line 107, column 53
    function onSelect_46 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncModifiers( {building}, jobWizardHelper);setNSRValues();setSRValues()
    }
    
    // 'onSelect' attribute on Card (id=uwinformation) at BOPBuildingPopup.pcf: line 197, column 174
    function onSelect_51 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions( {bopLocation.PolicyLocation}, jobWizardHelper )
    }
    
    // 'pickValue' attribute on EditButtons at BOPBuildingPopup.pcf: line 51, column 86
    function pickValue_2 () : BOPBuilding {
      return building
    }
    
    // 'startEditing' attribute on Popup (id=BOPBuildingPopup) at BOPBuildingPopup.pcf: line 14, column 96
    function startEditing_55 () : void {
      if (building == null and startInEdit == true and openForEdit) {building = bopLocation.addNewLineSpecificBuilding() as BOPBuilding}
    }
    
    // 'startInEditMode' attribute on Popup (id=BOPBuildingPopup) at BOPBuildingPopup.pcf: line 14, column 96
    function startInEditMode_56 () : java.lang.Boolean {
      return startInEdit
    }
    
    // 'visible' attribute on PanelRef at BOPBuildingPopup.pcf: line 72, column 153
    function visible_10 () : java.lang.Boolean {
      return building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on PanelRef at BOPBuildingPopup.pcf: line 94, column 165
    function visible_20 () : java.lang.Boolean {
      return (building.BOPDentalGenLiabilityCov_TDICExists || building.BOPBuildingOwnersLiabCov_TDICExists) && !(quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on PanelRef at BOPBuildingPopup.pcf: line 120, column 134
    function visible_44 () : java.lang.Boolean {
      return not User.util.CurrentUser.ExternalUser and setSRValues().HasElements and not (quoteType == QuoteType.TC_QUICK)
    }
    
    // 'visible' attribute on Card (id=Modifiers) at BOPBuildingPopup.pcf: line 107, column 53
    function visible_45 () : java.lang.Boolean {
      return bopLine.Branch.CanViewModifiers
    }
    
    // 'visible' attribute on Card (id=uwinformation) at BOPBuildingPopup.pcf: line 197, column 174
    function visible_50 () : java.lang.Boolean {
      return (quoteType != QuoteType.TC_QUICK) /*&& !(bopLine.Branch.LegacyPolicyNumber_TDIC!=null || bopLine.Branch.BasedOn.LegacyPolicyNumber_TDIC!=null)*/
    }
    
    // 'visible' attribute on PanelRef at BOPBuildingPopup.pcf: line 67, column 250
    function visible_7 () : java.lang.Boolean {
      return (building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" || building.PolicyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") && !(quoteType == QuoteType.TC_QUICK)
    }
    
    override property get CurrentLocation () : pcf.BOPBuildingPopup {
      return super.CurrentLocation as pcf.BOPBuildingPopup
    }
    
    property get bopLine () : BOPLine {
      return getVariableValue("bopLine", 0) as BOPLine
    }
    
    property set bopLine ($arg :  BOPLine) {
      setVariableValue("bopLine", 0, $arg)
    }
    
    property get bopLocation () : BOPLocation {
      return getVariableValue("bopLocation", 0) as BOPLocation
    }
    
    property set bopLocation ($arg :  BOPLocation) {
      setVariableValue("bopLocation", 0, $arg)
    }
    
    property get building () : BOPBuilding {
      return getVariableValue("building", 0) as BOPBuilding
    }
    
    property set building ($arg :  BOPBuilding) {
      setVariableValue("building", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getVariableValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setVariableValue("jobWizardHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getVariableValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setVariableValue("openForEdit", 0, $arg)
    }
    
    property get quoteType () : QuoteType {
      return getVariableValue("quoteType", 0) as QuoteType
    }
    
    property set quoteType ($arg :  QuoteType) {
      setVariableValue("quoteType", 0, $arg)
    }
    
    property get startInEdit () : boolean {
      return getVariableValue("startInEdit", 0) as java.lang.Boolean
    }
    
    property set startInEdit ($arg :  boolean) {
      setVariableValue("startInEdit", 0, $arg)
    }
    
    property get underwritingQuestionSets () : gw.api.productmodel.QuestionSet[] {
      return getVariableValue("underwritingQuestionSets", 0) as gw.api.productmodel.QuestionSet[]
    }
    
    property set underwritingQuestionSets ($arg :  gw.api.productmodel.QuestionSet[]) {
      setVariableValue("underwritingQuestionSets", 0, $arg)
    }
    
    
    function setNSRValues() : Modifier[]{
      if(building!=null) {
        var mod=building.BOPBuildingModifiers_TDIC
        return mod.where(\ modi -> not modi.ScheduleRate )
      }
      return new Modifier[]{}
    }
    function setSRValues():Modifier[]{
      var SRRates : Modifier[]
      if(building!=null) {
        var mod=building.BOPBuildingModifiers_TDIC
        SRRates =  mod.where(\modi -> modi.ScheduleRate) as Modifier[]
      } 
     return SRRates
    }
    function setInitialQuoteType():QuoteType{
      if(bopLine.Branch.Job typeis Submission){
        return (bopLine.Branch.Job as Submission).QuoteType
      }
      return null
    }
    
    function isIRPMRatingInputsEditable(): boolean {
      if (not User.util.CurrentUser.ExternalUser) {
        if (building.PolicyPeriod.Job typeis Submission or
            building.PolicyPeriod.Job typeis PolicyChange or
            (building.PolicyPeriod.Job typeis Renewal and building.PolicyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION)) {
          return perm.System.vieweditirpm_tdic
        }
      }  
      return false
    }
    
    function isIRPMRatingInputsVisible(): boolean {
      if (not User.util.CurrentUser.ExternalUser) {
          return (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic)
      }
      return false
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuildingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends BOPBuildingPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BOPBuildingPopup.pcf: line 128, column 36
    function initialValue_31 () : Modifier[] {
      return setSRValues()
    }
    
    // 'sortBy' attribute on IteratorSort at BOPBuildingPopup.pcf: line 140, column 36
    function sortBy_33 (scheduleRate :  entity.Modifier) : java.lang.Object {
      return scheduleRate.Pattern.Priority
    }
    
    // 'value' attribute on InputIterator at BOPBuildingPopup.pcf: line 137, column 49
    function value_42 () : entity.Modifier[] {
      return scheduleRates
    }
    
    // 'visible' attribute on Label at BOPBuildingPopup.pcf: line 132, column 52
    function visible_32 () : java.lang.Boolean {
      return setSRValues().IsEmpty
    }
    
    property get scheduleRates () : Modifier[] {
      return getVariableValue("scheduleRates", 1) as Modifier[]
    }
    
    property set scheduleRates ($arg :  Modifier[]) {
      setVariableValue("scheduleRates", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/BOPBuildingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewInput at BOPBuildingPopup.pcf: line 152, column 53
    function available_37 () : java.lang.Boolean {
      return scheduleRates != null
    }
    
    // 'def' attribute on ListViewInput at BOPBuildingPopup.pcf: line 152, column 53
    function def_onEnter_40 (def :  pcf.ScheduleRateLV) : void {
      def.onEnter(scheduleRate)
    }
    
    // 'def' attribute on ListViewInput at BOPBuildingPopup.pcf: line 152, column 53
    function def_refreshVariables_41 (def :  pcf.ScheduleRateLV) : void {
      def.refreshVariables(scheduleRate)
    }
    
    // 'editable' attribute on ListViewInput at BOPBuildingPopup.pcf: line 152, column 53
    function editable_38 () : java.lang.Boolean {
      return (building.PolicyPeriod.Job typeis Submission or building.PolicyPeriod.Job typeis PolicyChange or building.PolicyPeriod.Job typeis Renewal or (building.PolicyPeriod.Job?.MigrationJobInd_TDIC or building.PolicyPeriod.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION))//true
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at BOPBuildingPopup.pcf: line 146, column 55
    function valueRoot_35 () : java.lang.Object {
      return scheduleRate
    }
    
    // 'value' attribute on TextInput (id=ratingInputName_Input) at BOPBuildingPopup.pcf: line 146, column 55
    function value_34 () : java.lang.String {
      return scheduleRate.DisplayName
    }
    
    property get scheduleRate () : entity.Modifier {
      return getIteratedValue(2) as entity.Modifier
    }
    
    
  }
  
  
}