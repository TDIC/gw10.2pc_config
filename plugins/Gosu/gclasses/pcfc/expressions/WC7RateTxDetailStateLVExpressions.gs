package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7RateTxDetailStateLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7RateTxDetailStateLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on RowSetRef at WC7RateTxDetailStateLV.pcf: line 55, column 48
    function def_onEnter_2 (def :  pcf.WC7RateTxDetailAggRowSet) : void {
      def.onEnter(aggTx)
    }
    
    // 'def' attribute on RowSetRef at WC7RateTxDetailStateLV.pcf: line 55, column 48
    function def_refreshVariables_3 (def :  pcf.WC7RateTxDetailAggRowSet) : void {
      def.refreshVariables(aggTx)
    }
    
    property get aggTx () : WC7Transaction {
      return getIteratedValue(1) as WC7Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7RateTxDetailStateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7RateTxDetailStateLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailStateLV.pcf: line 50, column 24
    function sortBy_0 (aggTx :  WC7Transaction) : java.lang.Object {
      return (aggTx.WC7Cost as WC7JurisdictionCost).CalcOrder
    }
    
    // 'sortBy' attribute on IteratorSort at WC7RateTxDetailStateLV.pcf: line 53, column 24
    function sortBy_1 (aggTx :  WC7Transaction) : java.lang.Object {
      return aggTx.Proration
    }
    
    // 'value' attribute on RowIterator (id=gt400) at WC7RateTxDetailStateLV.pcf: line 47, column 43
    function value_4 () : entity.WC7Transaction[] {
      return stateTxs.byCalcOrder( 401, 1000000 ).toTypedArray()
    }
    
    // 'value' attribute on TextCell (id=DescriptionFoota500_Cell) at WC7RateTxDetailStateLV.pcf: line 68, column 39
    function value_5 () : java.lang.String {
      return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.TotalCost", jurisdiction.DisplayName)
    }
    
    // 'value' attribute on TextCell (id=TxAmountSubtotal500_Cell) at WC7RateTxDetailStateLV.pcf: line 80, column 39
    function value_7 () : java.lang.String {
      return gw.api.util.StringUtil.formatNumber(stateTxs*.AmountBilling*.Amount.sum() as java.lang.Double, "currency")
    }
    
    property get jurisdiction () : WC7Jurisdiction {
      return getRequireValue("jurisdiction", 0) as WC7Jurisdiction
    }
    
    property set jurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("jurisdiction", 0, $arg)
    }
    
    property get stateTxs () : java.util.Set<WC7Transaction> {
      return getRequireValue("stateTxs", 0) as java.util.Set<WC7Transaction>
    }
    
    property set stateTxs ($arg :  java.util.Set<WC7Transaction>) {
      setRequireValue("stateTxs", 0, $arg)
    }
    
    
  }
  
  
}