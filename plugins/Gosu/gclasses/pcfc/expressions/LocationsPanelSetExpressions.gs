package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.lob.wc7.WC7PolicyLineMethods
@javax.annotation.Generated("config/web/pcf/line/common/LocationsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LocationsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/common/LocationsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends LocationsEdit_DPExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function actionAvailable_26 () : java.lang.Boolean {
      return policyPeriod.profileChange_TDIC
    }
    
    // 'action' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function action_24 () : void {
      LocationPopup.push(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'action' attribute on TextCell (id=DBA_Cell) at LocationsPanelSet.pcf: line 132, column 30
    function action_30 () : void {
      LocationPopup.push(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'action' attribute on TextCell (id=Address_Cell) at LocationsPanelSet.pcf: line 140, column 30
    function action_35 () : void {
      LocationPopup.push(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'action' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function action_dest_25 () : pcf.api.Destination {
      return pcf.LocationPopup.createDestination(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'action' attribute on TextCell (id=DBA_Cell) at LocationsPanelSet.pcf: line 132, column 30
    function action_dest_31 () : pcf.api.Destination {
      return pcf.LocationPopup.createDestination(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'action' attribute on TextCell (id=Address_Cell) at LocationsPanelSet.pcf: line 140, column 30
    function action_dest_36 () : pcf.api.Destination {
      return pcf.LocationPopup.createDestination(null, location, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'condition' attribute on ToolbarFlag at LocationsPanelSet.pcf: line 80, column 33
    function condition_18 () : java.lang.Boolean {
      return !location.PrimaryLoc
    }
    
    // 'label' attribute on TextCell (id=Description_TDIC_Cell) at LocationsPanelSet.pcf: line 144, column 47
    function label_40 () : java.lang.Object {
      return getLocatiobDescriptionLabel()
    }
    
    // 'validationExpression' attribute on RadioButtonCell (id=PrimaryLocation_Cell) at LocationsPanelSet.pcf: line 93, column 67
    function validationExpression_20 () : java.lang.Object {
      return policyPeriod.PrimaryLocation != null ? null : DisplayKey.get("Web.Policy.LocationContainer.Location.PrimaryLocationError.SelectAtleastOne")
    }
    
    // 'value' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function valueRoot_28 () : java.lang.Object {
      return location
    }
    
    // 'value' attribute on RadioButtonCell (id=PrimaryLocation_Cell) at LocationsPanelSet.pcf: line 93, column 67
    function value_21 () : java.lang.Boolean {
      return location == policyPeriod.PrimaryLocation
    }
    
    // 'value' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function value_27 () : java.lang.Integer {
      return location.LocationNum
    }
    
    // 'value' attribute on TextCell (id=DBA_Cell) at LocationsPanelSet.pcf: line 132, column 30
    function value_32 () : java.lang.String {
      return location.DBA_TDIC
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at LocationsPanelSet.pcf: line 140, column 30
    function value_38 () : java.lang.String {
      return location.addressString(",", false, false)
    }
    
    // 'value' attribute on TextCell (id=Description_TDIC_Cell) at LocationsPanelSet.pcf: line 144, column 47
    function value_41 () : java.lang.String {
      return location.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=InSync_Cell) at LocationsPanelSet.pcf: line 149, column 50
    function value_45 () : java.lang.Boolean {
      return location.isUpToDate()
    }
    
    // 'visible' attribute on BooleanRadioCell (id=InSync_Cell) at LocationsPanelSet.pcf: line 149, column 50
    function visible_46 () : java.lang.Boolean {
      return policyPeriod.Promoted
    }
    
    property get location () : entity.PolicyLocation {
      return getIteratedValue(2) as entity.PolicyLocation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/common/LocationsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends LocationsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=UnassignedAccountLocation) at LocationsPanelSet.pcf: line 51, column 50
    function action_4 () : void {
      LocationPopup.push(unassignedAccountLocation, null, policyPeriod, openForEdit, true, supportsNonSpecificLocation);
    }
    
    // 'label' attribute on MenuItem (id=UnassignedAccountLocation) at LocationsPanelSet.pcf: line 51, column 50
    function label_5 () : java.lang.Object {
      return unassignedAccountLocation
    }
    
    property get unassignedAccountLocation () : entity.AccountLocation {
      return getIteratedValue(1) as entity.AccountLocation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/common/LocationsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationsEdit_DPExpressionsImpl extends LocationsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at LocationsPanelSet.pcf: line 157, column 111
    function available_51 () : java.lang.Boolean {
      return selectedLocation != null
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at LocationsPanelSet.pcf: line 157, column 111
    function def_onEnter_53 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.onEnter(selectedLocation?.getCoverableLocation(policyPeriod), false, null)
    }
    
    // 'def' attribute on PanelRef at LocationsPanelSet.pcf: line 161, column 93
    function def_onEnter_56 (def :  pcf.LocationDetailCV) : void {
      def.onEnter(selectedLocation, false, supportsNonSpecificLocation)
    }
    
    // 'def' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at LocationsPanelSet.pcf: line 157, column 111
    function def_refreshVariables_54 (def :  pcf.PreferredCoverageCurrencyPanelSet) : void {
      def.refreshVariables(selectedLocation?.getCoverableLocation(policyPeriod), false, null)
    }
    
    // 'def' attribute on PanelRef at LocationsPanelSet.pcf: line 161, column 93
    function def_refreshVariables_57 (def :  pcf.LocationDetailCV) : void {
      def.refreshVariables(selectedLocation, false, supportsNonSpecificLocation)
    }
    
    // 'label' attribute on TextCell (id=Description_TDIC_Cell) at LocationsPanelSet.pcf: line 144, column 47
    function label_14 () : java.lang.Object {
      return getLocatiobDescriptionLabel()
    }
    
    // 'pickLocation' attribute on RowIterator at LocationsPanelSet.pcf: line 77, column 49
    function pickLocation_48 () : void {
      LocationPopup.push(null, null, policyPeriod, openForEdit, true, supportsNonSpecificLocation)
    }
    
    // 'value' attribute on RadioButtonCell (id=PrimaryLocation_Cell) at LocationsPanelSet.pcf: line 93, column 67
    function sortValue_10 (location :  entity.PolicyLocation) : java.lang.Object {
      return location == policyPeriod.PrimaryLocation
    }
    
    // 'value' attribute on TextCell (id=Loc_Cell) at LocationsPanelSet.pcf: line 102, column 48
    function sortValue_11 (location :  entity.PolicyLocation) : java.lang.Object {
      return location.LocationNum
    }
    
    // 'value' attribute on TextCell (id=DBA_Cell) at LocationsPanelSet.pcf: line 132, column 30
    function sortValue_12 (location :  entity.PolicyLocation) : java.lang.Object {
      return location.DBA_TDIC
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at LocationsPanelSet.pcf: line 140, column 30
    function sortValue_13 (location :  entity.PolicyLocation) : java.lang.Object {
      return location.addressString(",", false, false)
    }
    
    // 'value' attribute on TextCell (id=Description_TDIC_Cell) at LocationsPanelSet.pcf: line 144, column 47
    function sortValue_15 (location :  entity.PolicyLocation) : java.lang.Object {
      return location.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=InSync_Cell) at LocationsPanelSet.pcf: line 149, column 50
    function sortValue_16 (location :  entity.PolicyLocation) : java.lang.Object {
      return location.isUpToDate()
    }
    
    // 'toRemove' attribute on RowIterator at LocationsPanelSet.pcf: line 77, column 49
    function toRemove_49 (location :  entity.PolicyLocation) : void {
      policyPeriod.removeLocAndAddClassfication(location)
    }
    
    // 'value' attribute on RowIterator at LocationsPanelSet.pcf: line 77, column 49
    function value_50 () : entity.PolicyLocation[] {
      return policyPeriod.PolicyLocations
    }
    
    // 'visible' attribute on BooleanRadioCell (id=InSync_Cell) at LocationsPanelSet.pcf: line 149, column 50
    function visible_17 () : java.lang.Boolean {
      return policyPeriod.Promoted
    }
    
    // 'visible' attribute on PanelRef (id=PreferredCoverageCurrencySelectorRef) at LocationsPanelSet.pcf: line 157, column 111
    function visible_52 () : java.lang.Boolean {
      return selectedLocation != null and selectedLocation?.getCoverableLocation(policyPeriod)!= null
    }
    
    // 'visible' attribute on PanelRef at LocationsPanelSet.pcf: line 161, column 93
    function visible_55 () : java.lang.Boolean {
      return policyPeriod.PolicyLocations.Count > 0 and selectedLocation != null
    }
    
    property get selectedLocation () : PolicyLocation {
      return getCurrentSelection(1) as PolicyLocation
    }
    
    property set selectedLocation ($arg :  PolicyLocation) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/common/LocationsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=addAllLocationsButton) at LocationsPanelSet.pcf: line 59, column 108
    function action_9 () : void {
      policyPeriod.addLocations_TDIC(unassignedAccountLocations as java.util.List<entity.AccountLocation>); gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions(policyPeriod.PolicyLocations, null)
    }
    
    // 'available' attribute on ToolbarButton (id=addLocationButton) at LocationsPanelSet.pcf: line 39, column 112
    function available_7 () : java.lang.Boolean {
      return unassignedAccountLocations.Count > 0
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=setToPrimary) at LocationsPanelSet.pcf: line 34, column 87
    function checkedRowAction_2 (location :  entity.PolicyLocation, CheckedValue :  entity.PolicyLocation) : void {
      policyPeriod.PrimaryLocation = CheckedValue
    }
    
    // 'initialValue' attribute on Variable at LocationsPanelSet.pcf: line 20, column 60
    function initialValue_0 () : java.util.List<entity.AccountLocation> {
      return policyPeriod.getUnassignedAccountLocations(supportsNonSpecificLocation)
    }
    
    // 'showRemoveConfirmMessage' attribute on IteratorButtons at LocationsPanelSet.pcf: line 28, column 146
    function showConfirmMessage_1 () : java.lang.Boolean {
      return policyPeriod.Lines.where(\ p -> p.PolicyLocationCanBeRemovedWithoutRemovingAssociatedRisks).HasElements
    }
    
    // 'sortBy' attribute on IteratorSort at LocationsPanelSet.pcf: line 47, column 30
    function sortBy_3 (unassignedAccountLocation :  entity.AccountLocation) : java.lang.Object {
      return unassignedAccountLocation.LocationNum
    }
    
    // 'value' attribute on MenuItemIterator (id=UnassignedAccountLocationIterator) at LocationsPanelSet.pcf: line 44, column 70
    function value_6 () : java.util.List<entity.AccountLocation> {
      return unassignedAccountLocations
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get supportsNonSpecificLocation () : boolean {
      return getRequireValue("supportsNonSpecificLocation", 0) as java.lang.Boolean
    }
    
    property set supportsNonSpecificLocation ($arg :  boolean) {
      setRequireValue("supportsNonSpecificLocation", 0, $arg)
    }
    
    property get unassignedAccountLocations () : java.util.List<entity.AccountLocation> {
      return getVariableValue("unassignedAccountLocations", 0) as java.util.List<entity.AccountLocation>
    }
    
    property set unassignedAccountLocations ($arg :  java.util.List<entity.AccountLocation>) {
      setVariableValue("unassignedAccountLocations", 0, $arg)
    }
    
    
    /**
     * Create policy locations for all account locations in the list
     */
    function addLocations(locations: java.util.List<AccountLocation>) {
      for (loc in locations) {
        policyPeriod.newLocation(loc)
        if (policyPeriod.WC7LineExists and policyPeriod.WC7Line.getJurisdiction(JurisdictionMappingUtil.getJurisdiction(loc)) == null) {
          policyPeriod.WC7Line.addJurisdiction(JurisdictionMappingUtil.getJurisdiction(loc))
        }
      }
    }
    
    function setPrimaryLocation(period : PolicyPeriod, loc : PolicyLocation){
      if (period.WC7LineExists){
        policyPeriod.WC7Line.updatePrimaryLocation(loc)
      } else {
        policyPeriod.PrimaryLocation = loc
      }
    }
    
    function removeLocation(period : PolicyPeriod, loc : PolicyLocation){
      period.removeLocation(loc)  
      
      if (period.WC7LineExists){
        period.WC7Line?.safelyRemoveJurisdiction(loc.State)
      }
    }
    
    function handleAfterRemoveLocation(period : PolicyPeriod, loc : PolicyLocation){
      if (period.WC7LineExists){
        new WC7PolicyLineMethods(policyPeriod.WC7Line).validateAfterRemovingLocation(loc)
      }
    }
    
    /**
     * 1080: If Address is standardized, add it to the policy location directly.
    */
    function addDirectOrOpenPopup(unassignedAccountLocation : AccountLocation) {
      if( unassignedAccountLocation.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_STANDARDIZED ) {
        var policyLocation = policyPeriod.newLocation(unassignedAccountLocation)
        policyLocation.setReinsuranceSearchTag()
        if (policyPeriod.WC7LineExists and policyPeriod.WC7Line.getJurisdiction(JurisdictionMappingUtil.getJurisdictionMappingForAccountLocation(unassignedAccountLocation)) == null) {
          policyPeriod.WC7Line.addJurisdiction(JurisdictionMappingUtil.getJurisdictionMappingForAccountLocation(unassignedAccountLocation))
        }
      }
      else {
        LocationPopup.push(unassignedAccountLocation, null, policyPeriod, openForEdit, true, supportsNonSpecificLocation);
      }
    }
    
    function getLocatiobDescriptionLabel() : String {
      if(policyPeriod.BOPLineExists) {
        return DisplayKey.get("TDIC.Web.Policy.BOP.Location.ControlAddress")
      }
      return DisplayKey.get("TDIC.Description")
    }
    
    
  }
  
  
}