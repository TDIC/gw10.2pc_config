package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewBasicClientForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7NewBasicClientForContactTypePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/wc7/WC7NewBasicClientForContactTypePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7NewBasicClientForContactTypePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewBasicClientForContactTypePopup.pcf: line 38, column 62
    function action_4 () : void {
      duplicateContactsPopupHelper.checkForDuplicatesOrUpdate(\ -> CurrentLocation.pickValueAndCommit(detail))
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at WC7NewBasicClientForContactTypePopup.pcf: line 46, column 62
    function action_9 () : void {
      duplicateContactsPopupHelper.push()
    }
    
    // 'beforeCommit' attribute on Popup (id=WC7NewBasicClientForContactTypePopup) at WC7NewBasicClientForContactTypePopup.pcf: line 12, column 34
    function beforeCommit_17 (pickedValue :  WC7ContactDetail) : void {
      detail.WC7Contact.resetContactAndRoles(duplicateContactsPopupHelper.existingPCContactWithABContactMatch)
    }
    
    // 'def' attribute on PanelRef at WC7NewBasicClientForContactTypePopup.pcf: line 59, column 72
    function def_onEnter_15 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.onEnter(detail.WC7Contact, false)
    }
    
    // 'def' attribute on PanelRef at WC7NewBasicClientForContactTypePopup.pcf: line 59, column 72
    function def_refreshVariables_16 (def :  pcf.NewPolicyContactRoleDetailsCV) : void {
      def.refreshVariables(detail.WC7Contact, false)
    }
    
    // 'initialValue' attribute on Variable at WC7NewBasicClientForContactTypePopup.pcf: line 21, column 39
    function initialValue_0 () : entity.WC7ContactDetail {
      return presenter.addNewDetail()
    }
    
    // 'initialValue' attribute on Variable at WC7NewBasicClientForContactTypePopup.pcf: line 25, column 25
    function initialValue_1 () : Contact[] {
      return presenter.Line.WC7PolicyLaborClients.map(\ p -> p.AccountContactRole.AccountContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at WC7NewBasicClientForContactTypePopup.pcf: line 29, column 69
    function initialValue_2 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper(detail.WC7Contact.AccountContactRole.AccountContact.Contact, existingContacts)
    }
    
    // 'label' attribute on TextInput (id=ScheduleParent_Input) at WC7NewBasicClientForContactTypePopup.pcf: line 55, column 47
    function label_10 () : java.lang.Object {
      return presenter.ScheduleLabel
    }
    
    // EditButtons at WC7NewBasicClientForContactTypePopup.pcf: line 41, column 72
    function label_7 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at WC7NewBasicClientForContactTypePopup.pcf: line 41, column 72
    function pickValue_5 () : WC7ContactDetail {
      return detail
    }
    
    // 'title' attribute on Popup (id=WC7NewBasicClientForContactTypePopup) at WC7NewBasicClientForContactTypePopup.pcf: line 12, column 34
    static function title_18 (presenter :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) : java.lang.Object {
      return presenter.PopupTitle
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7NewBasicClientForContactTypePopup.pcf: line 55, column 47
    function valueRoot_12 () : java.lang.Object {
      return presenter
    }
    
    // 'value' attribute on TextInput (id=ScheduleParent_Input) at WC7NewBasicClientForContactTypePopup.pcf: line 55, column 47
    function value_11 () : gw.api.domain.Clause {
      return presenter.ParentClause
    }
    
    // 'visible' attribute on ToolbarButton (id=ForceDupCheckUpdate) at WC7NewBasicClientForContactTypePopup.pcf: line 38, column 62
    function visible_3 () : java.lang.Boolean {
      return duplicateContactsPopupHelper.ShowButton
    }
    
    // 'updateVisible' attribute on EditButtons at WC7NewBasicClientForContactTypePopup.pcf: line 41, column 72
    function visible_6 () : java.lang.Boolean {
      return not duplicateContactsPopupHelper.ShowButton
    }
    
    override property get CurrentLocation () : pcf.WC7NewBasicClientForContactTypePopup {
      return super.CurrentLocation as pcf.WC7NewBasicClientForContactTypePopup
    }
    
    property get detail () : entity.WC7ContactDetail {
      return getVariableValue("detail", 0) as entity.WC7ContactDetail
    }
    
    property set detail ($arg :  entity.WC7ContactDetail) {
      setVariableValue("detail", 0, $arg)
    }
    
    property get duplicateContactsPopupHelper () : gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper {
      return getVariableValue("duplicateContactsPopupHelper", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper
    }
    
    property set duplicateContactsPopupHelper ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupHelper) {
      setVariableValue("duplicateContactsPopupHelper", 0, $arg)
    }
    
    property get existingContacts () : Contact[] {
      return getVariableValue("existingContacts", 0) as Contact[]
    }
    
    property set existingContacts ($arg :  Contact[]) {
      setVariableValue("existingContacts", 0, $arg)
    }
    
    property get presenter () : gw.lob.wc7.schedule.WC7ScheduleClientPresenter {
      return getVariableValue("presenter", 0) as gw.lob.wc7.schedule.WC7ScheduleClientPresenter
    }
    
    property set presenter ($arg :  gw.lob.wc7.schedule.WC7ScheduleClientPresenter) {
      setVariableValue("presenter", 0, $arg)
    }
    
    
  }
  
  
}