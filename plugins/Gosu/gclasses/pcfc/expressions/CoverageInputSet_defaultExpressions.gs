package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/CoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.default.pcf: line 31, column 96
    function available_31 () : java.lang.Boolean {
      return jobWzardScreenHelper.checkToggle(coverable, coveragePattern)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 44, column 106
    function def_onEnter_25 (def :  pcf.ScheduleInputSet_false) : void {
      def.onEnter(coverable.getCoverageConditionOrExclusion(coveragePattern), openForEdit)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 44, column 106
    function def_onEnter_27 (def :  pcf.ScheduleInputSet_true) : void {
      def.onEnter(coverable.getCoverageConditionOrExclusion(coveragePattern), openForEdit)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 44, column 106
    function def_refreshVariables_26 (def :  pcf.ScheduleInputSet_false) : void {
      def.refreshVariables(coverable.getCoverageConditionOrExclusion(coveragePattern), openForEdit)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 44, column 106
    function def_refreshVariables_28 (def :  pcf.ScheduleInputSet_true) : void {
      def.refreshVariables(coverable.getCoverageConditionOrExclusion(coveragePattern), openForEdit)
    }
    
    // 'label' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.default.pcf: line 31, column 96
    function label_32 () : java.lang.Object {
      return coveragePattern.DisplayName
    }
    
    // 'mode' attribute on InputSetRef at CoverageInputSet.default.pcf: line 44, column 106
    function mode_29 () : java.lang.Object {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) typeis gw.api.domain.Schedule
    }
    
    // 'onToggle' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.default.pcf: line 31, column 96
    function setter_33 (VALUE :  java.lang.Boolean) : void {
      jobWzardScreenHelper.toggleCoveragePattern(coverable, coveragePattern, VALUE);coverable.PolicyLine.Branch.setDiscountEffDate_TDIC();
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.default.pcf: line 23, column 37
    function valueRoot_1 () : java.lang.Object {
      return coveragePattern
    }
    
    // 'value' attribute on HiddenInput (id=CovPatternDisplayName_Input) at CoverageInputSet.default.pcf: line 23, column 37
    function value_0 () : java.lang.String {
      return coveragePattern.DisplayName
    }
    
    // 'value' attribute on InputIterator (id=CovTermIterator) at CoverageInputSet.default.pcf: line 37, column 53
    function value_24 () : gw.api.domain.covterm.CovTerm[] {
      return coverable.getCoverageConditionOrExclusion(coveragePattern).CovTerms.sortBy( \ term -> term.Pattern.Priority )
    }
    
    // 'childrenVisible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.default.pcf: line 31, column 96
    function visible_30 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputGroup (id=CovPatternInputGroup) at CoverageInputSet.default.pcf: line 31, column 96
    function visible_35 () : java.lang.Boolean {
      return jobWzardScreenHelper.isGLNewDentistDiscountVisible(coverable, coveragePattern)
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.default.pcf: line 48, column 138
    function visible_37 () : java.lang.Boolean {
      return tdic.web.pcf.helper.GLCoverageConditionsHelper.isInputSetDefaultDividerVisible(coveragePattern, coverable, openForEdit)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get jobWzardScreenHelper () : tdic.web.pcf.helper.JobWizardHelper {
      return getVariableValue("jobWzardScreenHelper", 0) as tdic.web.pcf.helper.JobWizardHelper
    }
    
    property set jobWzardScreenHelper ($arg :  tdic.web.pcf.helper.JobWizardHelper) {
      setVariableValue("jobWzardScreenHelper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_11 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_13 (def :  pcf.CovTermInputSet_bit) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_15 (def :  pcf.CovTermInputSet_datetime) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_17 (def :  pcf.CovTermInputSet_decimal) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_19 (def :  pcf.CovTermInputSet_default) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_21 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_3 (def :  pcf.CovTermInputSet_Direct) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_5 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_7 (def :  pcf.CovTermInputSet_Option) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_onEnter_9 (def :  pcf.CovTermInputSet_Package) : void {
      def.onEnter(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_10 (def :  pcf.CovTermInputSet_Package) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_12 (def :  pcf.CovTermInputSet_Typekey) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_14 (def :  pcf.CovTermInputSet_bit) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_16 (def :  pcf.CovTermInputSet_datetime) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_18 (def :  pcf.CovTermInputSet_decimal) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_20 (def :  pcf.CovTermInputSet_default) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_22 (def :  pcf.CovTermInputSet_shorttext) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_4 (def :  pcf.CovTermInputSet_Direct) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_6 (def :  pcf.CovTermInputSet_GLSchoolCode_TDIC) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'def' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function def_refreshVariables_8 (def :  pcf.CovTermInputSet_Option) : void {
      def.refreshVariables(term, openForEdit, coverable)
    }
    
    // 'mode' attribute on InputSetRef at CoverageInputSet.default.pcf: line 40, column 38
    function mode_23 () : java.lang.Object {
      return term.ValueTypeName
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getIteratedValue(1) as gw.api.domain.covterm.CovTerm
    }
    
    
  }
  
  
}