package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BuildingAdditionalInsuredsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_BuildingAdditionalInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ExistingAdditionalInsured) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 87, column 136
    function label_12 () : java.lang.Object {
      return additionalInsured
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=ExistingAdditionalInsured) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 87, column 136
    function toCreateAndAdd_13 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(additionalInsured.AccountContact.Contact)
    }
    
    property get additionalInsured () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_BuildingAdditionalInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=OtherContact) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 106, column 131
    function label_17 () : java.lang.Object {
      return otherContact
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=OtherContact) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 106, column 131
    function toCreateAndAdd_18 (CheckedValues :  Object[]) : java.lang.Object {
      return bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(otherContact.AccountContact.Contact)
    }
    
    property get otherContact () : entity.AccountContactView {
      return getIteratedValue(1) as entity.AccountContactView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TDIC_BuildingAdditionalInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 155, column 55
    function action_39 () : void {
      EditPolicyContactRolePopup.push(additionalInsuredDetail.PolicyAddlInsured, openForEdit)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 155, column 55
    function action_dest_40 () : pcf.api.Destination {
      return pcf.EditPolicyContactRolePopup.createDestination(additionalInsuredDetail.PolicyAddlInsured, openForEdit)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at TDIC_BuildingAdditionalInsuredsDV.pcf: line 133, column 58
    function checkBoxVisible_67 () : java.lang.Boolean {
      return openForEdit
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      additionalInsuredDetail.AdditionalInsuredType = (__VALUE_TO_SET as typekey.AdditionalInsuredType)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 173, column 61
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      additionalInsuredDetail.Desciption = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=AdditionalInformation_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 186, column 57
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      additionalInsuredDetail.AdditionalInformation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=AdditionalInformation_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 186, column 57
    function editable_60 () : java.lang.Boolean {
      return helper.additionalInformationRequired(additionalInsuredDetail)
    }
    
    // 'onChange' attribute on PostOnChange at TDIC_BuildingAdditionalInsuredsDV.pcf: line 166, column 93
    function onChange_44 () : void {
      helper.onAdditionalInsuredTypeChange(additionalInsuredDetail)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function valueRange_48 () : java.lang.Object {
      return helper.filterAdditionalInsuredTypes_TDIC(bopBuilding.Branch)
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 149, column 70
    function valueRoot_37 () : java.lang.Object {
      return additionalInsuredDetail
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 143, column 107
    function value_34 () : java.util.Date {
      return getEffectiveDate(additionalInsuredDetail)//additionalInsuredDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 149, column 70
    function value_36 () : java.util.Date {
      return additionalInsuredDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 155, column 55
    function value_41 () : entity.PolicyAddlInsured {
      return additionalInsuredDetail.PolicyAddlInsured
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function value_45 () : typekey.AdditionalInsuredType {
      return additionalInsuredDetail.AdditionalInsuredType
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 173, column 61
    function value_52 () : java.lang.String {
      return additionalInsuredDetail.Desciption
    }
    
    // 'value' attribute on TypeKeyCell (id=AdditionalInformationType_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 179, column 57
    function value_56 () : AdditionalInformationType {
      return additionalInsuredDetail.additionalInformationType(additionalInsuredDetail.AdditionalInsuredType)
    }
    
    // 'value' attribute on TextCell (id=AdditionalInformation_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 186, column 57
    function value_61 () : java.lang.String {
      return additionalInsuredDetail.AdditionalInformation
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function verifyValueRangeIsAllowedType_49 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function verifyValueRangeIsAllowedType_49 ($$arg :  typekey.AdditionalInsuredType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function verifyValueRange_50 () : void {
      var __valueRangeArg = helper.filterAdditionalInsuredTypes_TDIC(bopBuilding.Branch)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_49(__valueRangeArg)
    }
    
    // 'valueType' attribute on TypeKeyCell (id=AdditionalInformationType_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 179, column 57
    function verifyValueType_59 () : void {
      var __valueTypeArg : AdditionalInformationType
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on TypeKeyCell (id=AdditionalInformationType_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 179, column 57
    function visible_57 () : java.lang.Boolean {
      return additionalInformationVisible
    }
    
    property get additionalInsuredDetail () : entity.PolicyAddlInsuredDetail {
      return getIteratedValue(1) as entity.PolicyAddlInsuredDetail
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_BuildingAdditionalInsuredsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=ContactType) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 62, column 103
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.Contact.AddNewOfType", contactType)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=ContactType) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 62, column 103
    function pickLocation_7 () : void {
      TDIC_NewBuildingAdditionalInsuredPopup.push(bopBuilding, contactType)
    }
    
    property get contactType () : typekey.ContactType {
      return getIteratedValue(1) as typekey.ContactType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/TDIC_BuildingAdditionalInsuredsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BuildingAdditionalInsuredsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=expirebutton) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 121, column 67
    function allCheckedRowsAction_23 (CheckedValues :  entity.PolicyAddlInsuredDetail[], CheckedValuesErrors :  java.util.Map) : void {
      setExpirationDate(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=expirebutton) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 121, column 67
    function available_21 () : java.lang.Boolean {
      return bopBuilding.Branch.Job typeis PolicyChange
    }
    
    // 'conversionExpression' attribute on AddMenuItem (id=AddFromSearch) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 70, column 32
    function conversionExpression_9 (PickedValue :  Contact) : entity.PolicyAddlInsuredDetail {
      return bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(PickedValue)
    }
    
    // 'editable' attribute on ListViewInput at TDIC_BuildingAdditionalInsuredsDV.pcf: line 43, column 27
    function editable_70 () : java.lang.Boolean {
      return (!(bopBuilding.Branch.Job typeis Submission) or perm.System.editsubmission) && openForEdit
    }
    
    // 'initialValue' attribute on Variable at TDIC_BuildingAdditionalInsuredsDV.pcf: line 23, column 36
    function initialValue_0 () : AccountContactView[] {
      return null
    }
    
    // 'initialValue' attribute on Variable at TDIC_BuildingAdditionalInsuredsDV.pcf: line 32, column 54
    function initialValue_2 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'initialValue' attribute on Variable at TDIC_BuildingAdditionalInsuredsDV.pcf: line 36, column 60
    function initialValue_3 () : gw.pcf.contacts.AdditionalInsuredsDVUIHelper {
      return new gw.pcf.contacts.AdditionalInsuredsDVUIHelper()
    }
    
    // 'label' attribute on AddMenuItem (id=AddExistingContact) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 74, column 127
    function label_15 () : java.lang.Object {
      return DisplayKey.get("Web.Policy.Contact.AddExisting", PolicyAddlInsured.Type.TypeInfo.DisplayName)
    }
    
    // 'pickLocation' attribute on AddMenuItem (id=AddFromSearch) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 70, column 32
    function pickLocation_10 () : void {
      ContactSearchPopup.push(TC_ADDITIONALINSURED)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BuildingAdditionalInsuredsDV.pcf: line 82, column 34
    function sortBy_11 (additionalInsured :  entity.AccountContactView) : java.lang.Object {
      return additionalInsured.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BuildingAdditionalInsuredsDV.pcf: line 101, column 34
    function sortBy_16 (otherContact :  entity.AccountContactView) : java.lang.Object {
      return otherContact.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BuildingAdditionalInsuredsDV.pcf: line 136, column 30
    function sortBy_24 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.PolicyAddlInsured
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_BuildingAdditionalInsuredsDV.pcf: line 57, column 32
    function sortBy_5 (contactType :  typekey.ContactType) : java.lang.Object {
      return contactType.DisplayName
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 143, column 107
    function sortValue_25 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return getEffectiveDate(additionalInsuredDetail)//additionalInsuredDetail.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 149, column 70
    function sortValue_26 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.ExpirationDate_TDIC
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 155, column 55
    function sortValue_27 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.PolicyAddlInsured
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 163, column 59
    function sortValue_28 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.AdditionalInsuredType
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 173, column 61
    function sortValue_29 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.Desciption
    }
    
    // 'value' attribute on TypeKeyCell (id=AdditionalInformationType_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 179, column 57
    function sortValue_30 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.additionalInformationType(additionalInsuredDetail.AdditionalInsuredType)
    }
    
    // 'value' attribute on TextCell (id=AdditionalInformation_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 186, column 57
    function sortValue_32 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : java.lang.Object {
      return additionalInsuredDetail.AdditionalInformation
    }
    
    // 'toRemove' attribute on RowIterator at TDIC_BuildingAdditionalInsuredsDV.pcf: line 133, column 58
    function toRemove_68 (additionalInsuredDetail :  entity.PolicyAddlInsuredDetail) : void {
      additionalInsuredDetail.PolicyAddlInsured.toRemoveFromPolicyAddIns(additionalInsuredDetail)
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfSameType) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 79, column 57
    function value_14 () : entity.AccountContactView[] {
      return getExistingAdditionalInsureds()
    }
    
    // 'value' attribute on AddMenuItemIterator (id=ContactOfOtherType) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 98, column 57
    function value_19 () : entity.AccountContactView[] {
      return getOtherContacts()
    }
    
    // 'value' attribute on RowIterator at TDIC_BuildingAdditionalInsuredsDV.pcf: line 133, column 58
    function value_69 () : entity.PolicyAddlInsuredDetail[] {
      return bopBuilding.AdditionalInsureds*.PolicyAdditionalInsuredDetails
    }
    
    // 'value' attribute on AddMenuItemIterator at TDIC_BuildingAdditionalInsuredsDV.pcf: line 54, column 49
    function value_8 () : typekey.ContactType[] {
      return contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYADDLINSURED)
    }
    
    // 'removeVisible' attribute on IteratorButtons (id=IteratorButtons) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 114, column 207
    function visible_20 () : java.lang.Boolean {
      return bopBuilding.Branch.Job typeis Submission or bopBuilding.Branch.Job typeis Renewal or bopBuilding.AdditionalInsureds*.PolicyAdditionalInsuredDetails.hasMatch(\elt -> elt.New)
    }
    
    // 'visible' attribute on TypeKeyCell (id=AdditionalInformationType_Cell) at TDIC_BuildingAdditionalInsuredsDV.pcf: line 179, column 57
    function visible_31 () : java.lang.Boolean {
      return additionalInformationVisible
    }
    
    // 'visible' attribute on Label at TDIC_BuildingAdditionalInsuredsDV.pcf: line 40, column 33
    function visible_4 () : java.lang.Boolean {
      return displayLabel
    }
    
    property get additionalInformationVisible () : boolean {
      return getRequireValue("additionalInformationVisible", 0) as java.lang.Boolean
    }
    
    property set additionalInformationVisible ($arg :  boolean) {
      setRequireValue("additionalInformationVisible", 0, $arg)
    }
    
    property get bopBuilding () : BOPBuilding {
      return getRequireValue("bopBuilding", 0) as BOPBuilding
    }
    
    property set bopBuilding ($arg :  BOPBuilding) {
      setRequireValue("bopBuilding", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get displayLabel () : boolean {
      return getRequireValue("displayLabel", 0) as java.lang.Boolean
    }
    
    property set displayLabel ($arg :  boolean) {
      setRequireValue("displayLabel", 0, $arg)
    }
    
    property get existingAdditionalInsureds () : AccountContactView[] {
      return getVariableValue("existingAdditionalInsureds", 0) as AccountContactView[]
    }
    
    property set existingAdditionalInsureds ($arg :  AccountContactView[]) {
      setVariableValue("existingAdditionalInsureds", 0, $arg)
    }
    
    property get helper () : gw.pcf.contacts.AdditionalInsuredsDVUIHelper {
      return getVariableValue("helper", 0) as gw.pcf.contacts.AdditionalInsuredsDVUIHelper
    }
    
    property set helper ($arg :  gw.pcf.contacts.AdditionalInsuredsDVUIHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get otherContacts () : AccountContactView[] {
      return getVariableValue("otherContacts", 0) as AccountContactView[]
    }
    
    property set otherContacts ($arg :  AccountContactView[]) {
      setVariableValue("otherContacts", 0, $arg)
    }
    
    /*function getExistingAdditionalInsureds() : AccountContactView[] {
      if (existingAdditionalInsureds == null) {
        existingAdditionalInsureds = bopBuilding.ExistingAdditionalInsureds_TDIC.asViews()
      }
      return existingAdditionalInsureds
    }*/
    
    function getExistingAdditionalInsureds() : AccountContactView[] {
      if (existingAdditionalInsureds == null) {
        var addedContacts = bopBuilding.AdditionalInsureds*.AccountContactRole*.AccountContact
        var all = bopBuilding.ExistingAdditionalInsureds_TDIC
        var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
        existingAdditionalInsureds = remaining
      }
      return existingAdditionalInsureds
    }
    
    function getOtherContacts() : AccountContactView[] {
      if (otherContacts == null) {
        otherContacts = bopBuilding.AdditionalInsuredOtherCandidates_TDIC.asViews()
      }
      return otherContacts
    }
    
    function setExpirationDate(policyInsuredDetail : entity.PolicyAddlInsuredDetail[]) {
      for (insured in policyInsuredDetail) {
        insured.ExpirationDate_TDIC = insured.Branch.EditEffectiveDate
      }
    }
    
    function getEffectiveDate(addnl : PolicyAddlInsuredDetail) : Date {
      var effDates = bopBuilding.Branch.AllEffectiveDates.toSet()
      for(effDate in effDates.order()) {
        var version = addnl.VersionList.AsOf(effDate)
        if(version != null) {
          return version.EffectiveDate
        }
      }
      return addnl.EffectiveDate
    }
    
    
  }
  
  
}