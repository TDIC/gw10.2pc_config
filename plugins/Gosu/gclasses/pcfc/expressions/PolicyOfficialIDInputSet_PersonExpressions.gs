package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyOfficialIDInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyOfficialIDInputSet_PersonExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyOfficialIDInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyOfficialIDInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.AccountContactRole.AccountContact.Contact.FEINOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.AccountContactRole.AccountContact.Contact.SSNOfficialID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function encryptionExpression_5 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(VALUE)
    }
    
    // 'inputMask' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function inputMask_13 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.InputMask
    }
    
    // 'regex' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function regex_14 () : java.lang.String {
      return gw.api.validation.FEINValidator_TDIC.Regex
    }
    
    // 'required' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function required_1 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.isSSNRequired(policyContactRole) and tdic.web.admin.shared.SharedUIHelper.isSSNRequiredDIMS(policyContactRole)
    }
    
    // 'required' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function required_9 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.isFEINRequired(policyContactRole)
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function valueRoot_4 () : java.lang.Object {
      return policyContactRole.AccountContactRole.AccountContact.Contact
    }
    
    // 'value' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function value_10 () : java.lang.String {
      return policyContactRole.AccountContactRole.AccountContact.Contact.FEINOfficialID
    }
    
    // 'value' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function value_2 () : java.lang.String {
      return policyContactRole.AccountContactRole.AccountContact.Contact.SSNOfficialID
    }
    
    // 'visible' attribute on PrivacyInput (id=OfficialIDDV_Input_Input) at PolicyOfficialIDInputSet.Person.pcf: line 19, column 95
    function visible_0 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.canSSNVisible(policyContactRole.Branch)
    }
    
    // 'visible' attribute on TextInput (id=FEINOfficialID_TDIC_Input) at PolicyOfficialIDInputSet.Person.pcf: line 28, column 113
    function visible_8 () : java.lang.Boolean {
      return tdic.web.admin.shared.SharedUIHelper.getVisibilityOfPolicContactFields_TDIC(policyContactRole)
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    
  }
  
  
}