package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSpecialEventCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CoverageInputSet_GLSpecialEventCov_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSpecialEventCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allowToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 33, column 99
    function available_45 () : java.lang.Boolean {
      return coveragePattern.allowToggle(coverable)
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 22, column 35
    function initialValue_0 () : productmodel.GLLine {
      return coverable.PolicyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 26, column 49
    function initialValue_1 () : gw.api.productmodel.ClausePattern {
      return coveragePattern
    }
    
    // 'label' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 33, column 99
    function label_46 () : java.lang.Object {
      return scheduledEquipmentPattern.DisplayName
    }
    
    // 'onToggle' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 33, column 99
    function setter_47 (VALUE :  java.lang.Boolean) : void {
      glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )
    }
    
    // 'value' attribute on TextCell (id=effDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 61, column 50
    function sortValue_2 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.EventName
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 67, column 55
    function sortValue_3 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.EventStartDate
    }
    
    // 'value' attribute on DateCell (id=endDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 74, column 53
    function sortValue_4 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.EventEndDate
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 81, column 48
    function sortValue_5 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.Address
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 87, column 47
    function sortValue_6 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.Onsite
    }
    
    // 'value' attribute on TextAreaCell (id=dentistName_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 93, column 60
    function sortValue_7 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.ManagerLessorString
    }
    
    // 'value' attribute on TextAreaCell (id=eqLessor_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 99, column 62
    function sortValue_8 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.EquipmentLessorString
    }
    
    // 'value' attribute on TextAreaCell (id=govAgency_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 105, column 57
    function sortValue_9 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : java.lang.Object {
      return scheduledItem.GovtAgencyString
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 53, column 59
    function toCreateAndAdd_41 () : entity.GLSpecialEventSched_TDIC {
      return glLine.createAndAddSpecialEventSched_TDIC()
    }
    
    // 'toRemove' attribute on RowIterator at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 53, column 59
    function toRemove_42 (scheduledItem :  entity.GLSpecialEventSched_TDIC) : void {
      glLine.removeFromGLSpecialEventSched_TDIC(scheduledItem); 
    }
    
    // 'value' attribute on RowIterator at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 53, column 59
    function value_43 () : entity.GLSpecialEventSched_TDIC[] {
      return glLine.GLSpecialEventSched_TDIC
    }
    
    // 'childrenVisible' attribute on InputGroup (id=GLSchedEquipInputGroup) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 33, column 99
    function visible_44 () : java.lang.Boolean {
      return coverable.getCoverageConditionOrExclusion(coveragePattern) != null
    }
    
    // 'visible' attribute on InputDivider at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 129, column 45
    function visible_50 () : java.lang.Boolean {
      return isSpecialEventCovVisible()
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get coveragePattern () : gw.api.productmodel.ClausePattern {
      return getRequireValue("coveragePattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set coveragePattern ($arg :  gw.api.productmodel.ClausePattern) {
      setRequireValue("coveragePattern", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get scheduledEquipmentPattern () : gw.api.productmodel.ClausePattern {
      return getVariableValue("scheduledEquipmentPattern", 0) as gw.api.productmodel.ClausePattern
    }
    
    property set scheduledEquipmentPattern ($arg :  gw.api.productmodel.ClausePattern) {
      setVariableValue("scheduledEquipmentPattern", 0, $arg)
    }
    
    function isSpecialEventCovVisible() : boolean {
      if (coveragePattern.CodeIdentifier == "GLSpecialEventCov_TDIC") {
        if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and  coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
              coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
              coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
          return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLSpecialEventCov_TDICExists
    
        } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                    coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
          return true
        }
      }
      // as before for other coverages in .default PCF file
      return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/CoverageInputSet.GLSpecialEventCov_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=effDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 61, column 50
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.EventName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=endDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 74, column 53
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.EventEndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 81, column 48
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.Address = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 87, column 47
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.Onsite = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextAreaCell (id=dentistName_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 93, column 60
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.ManagerLessorString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaCell (id=eqLessor_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 99, column 62
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.EquipmentLessorString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaCell (id=govAgency_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 105, column 57
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      scheduledItem.GovtAgencyString = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=effDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 61, column 50
    function valueRoot_12 () : java.lang.Object {
      return scheduledItem
    }
    
    // 'value' attribute on TextCell (id=effDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 61, column 50
    function value_10 () : java.lang.String {
      return scheduledItem.EventName
    }
    
    // 'value' attribute on DateCell (id=expDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 67, column 55
    function value_14 () : java.util.Date {
      return scheduledItem.EventStartDate
    }
    
    // 'value' attribute on DateCell (id=endDate_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 74, column 53
    function value_17 () : java.util.Date {
      return scheduledItem.EventEndDate
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 81, column 48
    function value_21 () : java.lang.String {
      return scheduledItem.Address
    }
    
    // 'value' attribute on BooleanRadioCell (id=onsite_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 87, column 47
    function value_25 () : java.lang.Boolean {
      return scheduledItem.Onsite
    }
    
    // 'value' attribute on TextAreaCell (id=dentistName_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 93, column 60
    function value_29 () : java.lang.String {
      return scheduledItem.ManagerLessorString
    }
    
    // 'value' attribute on TextAreaCell (id=eqLessor_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 99, column 62
    function value_33 () : java.lang.String {
      return scheduledItem.EquipmentLessorString
    }
    
    // 'value' attribute on TextAreaCell (id=govAgency_Cell) at CoverageInputSet.GLSpecialEventCov_TDIC.pcf: line 105, column 57
    function value_37 () : java.lang.String {
      return scheduledItem.GovtAgencyString
    }
    
    property get scheduledItem () : entity.GLSpecialEventSched_TDIC {
      return getIteratedValue(1) as entity.GLSpecialEventSched_TDIC
    }
    
    
  }
  
  
}