package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/submgr/RejectReasonDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RejectReasonDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/submgr/RejectReasonDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RejectReasonDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=non_BOP_RejectReason_Input) at RejectReasonDV.pcf: line 25, column 93
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      submission.RejectReason = (__VALUE_TO_SET as typekey.ReasonCode)
    }
    
    // 'value' attribute on TextAreaInput (id=RejectReasonText_Input) at RejectReasonDV.pcf: line 58, column 40
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      submission.RejectReasonText = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filter' attribute on TypeKeyInput (id=non_BOP_RejectReason_Input) at RejectReasonDV.pcf: line 25, column 93
    function filter_4 (VALUE :  typekey.ReasonCode, VALUES :  typekey.ReasonCode[]) : java.lang.Boolean {
      return VALUE.hasCategory(type)
    }
    
    // 'onChange' attribute on PostOnChange at RejectReasonDV.pcf: line 49, column 43
    function onChange_17 () : void {
      displayReasonText()
    }
    
    // 'onChange' attribute on PostOnChange at RejectReasonDV.pcf: line 36, column 43
    function onChange_7 () : void {
      displayReasonText()
    }
    
    // 'required' attribute on TextAreaInput (id=RejectReasonText_Input) at RejectReasonDV.pcf: line 58, column 40
    function required_29 () : java.lang.Boolean {
      return isRequired()
    }
    
    // 'valueRange' attribute on RangeInput (id=BOP_RejectReason_Input) at RejectReasonDV.pcf: line 34, column 57
    function valueRange_12 () : java.lang.Object {
      return getFilteredReasonCodesCP()
    }
    
    // 'valueRange' attribute on RangeInput (id=RejectReasonPL_Input) at RejectReasonDV.pcf: line 47, column 45
    function valueRange_23 () : java.lang.Object {
      return getReasonCodePL()
    }
    
    // 'value' attribute on TypeKeyInput (id=non_BOP_RejectReason_Input) at RejectReasonDV.pcf: line 25, column 93
    function valueRoot_3 () : java.lang.Object {
      return submission
    }
    
    // 'value' attribute on TypeKeyInput (id=non_BOP_RejectReason_Input) at RejectReasonDV.pcf: line 25, column 93
    function value_1 () : typekey.ReasonCode {
      return submission.RejectReason
    }
    
    // 'value' attribute on TextAreaInput (id=RejectReasonText_Input) at RejectReasonDV.pcf: line 58, column 40
    function value_30 () : java.lang.String {
      return submission.RejectReasonText
    }
    
    // 'valueRange' attribute on RangeInput (id=BOP_RejectReason_Input) at RejectReasonDV.pcf: line 34, column 57
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BOP_RejectReason_Input) at RejectReasonDV.pcf: line 34, column 57
    function verifyValueRangeIsAllowedType_13 ($$arg :  typekey.ReasonCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RejectReasonPL_Input) at RejectReasonDV.pcf: line 47, column 45
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RejectReasonPL_Input) at RejectReasonDV.pcf: line 47, column 45
    function verifyValueRangeIsAllowedType_24 ($$arg :  typekey.ReasonCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BOP_RejectReason_Input) at RejectReasonDV.pcf: line 34, column 57
    function verifyValueRange_14 () : void {
      var __valueRangeArg = getFilteredReasonCodesCP()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=RejectReasonPL_Input) at RejectReasonDV.pcf: line 47, column 45
    function verifyValueRange_25 () : void {
      var __valueRangeArg = getReasonCodePL()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=non_BOP_RejectReason_Input) at RejectReasonDV.pcf: line 25, column 93
    function visible_0 () : java.lang.Boolean {
      return not (submission.LatestPeriod.BOPLineExists || policyPeriod.GLLineExists)
    }
    
    // 'visible' attribute on RangeInput (id=RejectReasonPL_Input) at RejectReasonDV.pcf: line 47, column 45
    function visible_18 () : java.lang.Boolean {
      return policyPeriod.GLLineExists
    }
    
    // 'visible' attribute on TextAreaInput (id=RejectReasonText_Input) at RejectReasonDV.pcf: line 58, column 40
    function visible_28 () : java.lang.Boolean {
      return displayReasonText()
    }
    
    // 'visible' attribute on RangeInput (id=BOP_RejectReason_Input) at RejectReasonDV.pcf: line 34, column 57
    function visible_8 () : java.lang.Boolean {
      return submission.LatestPeriod.BOPLineExists
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get submission () : Submission {
      return getRequireValue("submission", 0) as Submission
    }
    
    property set submission ($arg :  Submission) {
      setRequireValue("submission", 0, $arg)
    }
    
    property get type () : LetterType {
      return getRequireValue("type", 0) as LetterType
    }
    
    property set type ($arg :  LetterType) {
      setRequireValue("type", 0, $arg)
    }
    
    function getFilteredReasonCodesCP(): typekey.ReasonCode[] {
      if (type == TC_WITHDRAW_TDIC) {
        return ReasonCode.TF_SUBMISSIONWITHDRAWNCP_TDIC.TypeKeys.toTypedArray()
      }
      if (type == LetterType.TC_DECLINATION) {
        return ReasonCode.TF_SUBMISSIONDECLINEDCP_TDIC.TypeKeys.toTypedArray()
      }
      return null
    }
    
    function getReasonCodePL():List{
      if(type == LetterType.TC_WITHDRAW_TDIC){
        return ReasonCode.TF_PLWITHDRAWN.TypeKeys
      }
      if(type == LetterType.TC_DECLINATION){
        /*if(policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC"){
          return ReasonCode.TF_PLDECLINEOTHERS.TypeKeys
        }
        if(policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC"){
          return ReasonCode.TF_PLDECLINECYB.TypeKeys
        }*/
        return ReasonCode.TF_PLDECLINEDREASONS.TypeKeys
      }
      return null
    }
    
    function displayReasonText():Boolean{
      
      if(policyPeriod.GLLineExists){
        var reasonArray = {"appWithdraw_TDIC", "uninsurable_TDIC", "uwCriteria_TDIC"}
        if(reasonArray.contains(submission.RejectReason.Code)) return true
      }
      if(policyPeriod.BOPLineExists){
        var reasonArray = {"appWithdraw_TDIC","miscUnderstand_TDIC","uninsurable_TDIC", "doesNotMeetUWCriteria_TDIC"}
        if(reasonArray.contains(submission.RejectReason.Code)) return true
      }
      if(policyPeriod.WC7LineExists) return true
      return false
    }
    
    function isRequired(): boolean {
      if (policyPeriod.BOPLineExists) {
        if (type == LetterType.TC_DECLINATION and (submission.RejectReason == ReasonCode.TC_UNINSURABLE_TDIC or submission.RejectReason == ReasonCode.TC_DOESNOTMEETUWCRITERIA_TDIC)) {
          return true
        }
        if (type == LetterType.TC_WITHDRAW_TDIC and (submission.RejectReason == ReasonCode.TC_APPWITHDRAW_TDIC or submission.RejectReason == ReasonCode.TC_DOESNOTMEETUWCRITERIA_TDIC)) {
          return true
        }
      }
        if (policyPeriod.GLLineExists) {
          if (type == LetterType.TC_DECLINATION and (submission.RejectReason == ReasonCode.TC_UNINSURABLE_TDIC or submission.RejectReason == ReasonCode.TC_UWCRITERIA_TDIC)) {
            return true
          }
          if (type == LetterType.TC_WITHDRAW_TDIC and (submission.RejectReason == ReasonCode.TC_APPWITHDRAW_TDIC or submission.RejectReason == ReasonCode.TC_UWCRITERIA_TDIC)) {
            return true
          }
        }
      return false
    }
    
    
    
    
        
       
    
    
  }
  
  
}