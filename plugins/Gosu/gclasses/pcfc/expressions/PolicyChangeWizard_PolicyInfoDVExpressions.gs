package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/policychange/PolicyChangeWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyChangeWizard_PolicyInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/policychange/PolicyChangeWizard_PolicyInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyChangeWizard_PolicyInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 16, column 55
    function def_onEnter_1 (def :  pcf.AccountInfoInputSet) : void {
      def.onEnter(policyPeriod,null)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 62, column 56
    function def_onEnter_30 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 19, column 53
    function def_onEnter_4 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 29, column 38
    function def_onEnter_7 (def :  pcf.PolicyInfoInputSet) : void {
      def.onEnter(policyPeriod, true, false, false,null)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 33, column 27
    function def_onEnter_9 (def :  pcf.PolicyInfoProducerInfoInputSet) : void {
      def.onEnter(policyPeriod, ProducerStatusUse.TC_OKAY)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 33, column 27
    function def_refreshVariables_10 (def :  pcf.PolicyInfoProducerInfoInputSet) : void {
      def.refreshVariables(policyPeriod, ProducerStatusUse.TC_OKAY)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 16, column 55
    function def_refreshVariables_2 (def :  pcf.AccountInfoInputSet) : void {
      def.refreshVariables(policyPeriod,null)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 62, column 56
    function def_refreshVariables_31 (def :  pcf.PreferredCurrencyInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 19, column 53
    function def_refreshVariables_5 (def :  pcf.SecondaryNamedInsuredInputSet) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 29, column 38
    function def_refreshVariables_8 (def :  pcf.PolicyInfoInputSet) : void {
      def.refreshVariables(policyPeriod, true, false, false,null)
    }
    
    // 'value' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.MultiLineDiscount_TDIC = (__VALUE_TO_SET as typekey.MultiLineDiscount_TDIC)
    }
    
    // 'editable' attribute on InputSetRef at PolicyChangeWizard_PolicyInfoDV.pcf: line 19, column 53
    function editable_3 () : java.lang.Boolean {
      return policyPeriod.profileChange_TDIC
    }
    
    // 'initialValue' attribute on Variable at PolicyChangeWizard_PolicyInfoDV.pcf: line 13, column 43
    function initialValue_0 () : gw.job.AvailableUWCompanies {
      return new gw.job.AvailableUWCompanies(policyPeriod)
    }
    
    // 'optionLabel' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function optionLabel_13 (VALUE :  entity.UWCompany) : java.lang.String {
      return VALUE.DisplayName
    }
    
    // 'required' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function required_20 () : java.lang.Boolean {
      return tdic.web.pcf.helper.PolicyInfoHelper.isMultiLineDiscountRequired(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function valueRange_14 () : java.lang.Object {
      return availableUWCompanies.Value
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function valueRange_24 () : java.lang.Object {
      return tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function valueRoot_12 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function value_11 () : entity.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function value_21 () : typekey.MultiLineDiscount_TDIC {
      return policyPeriod.MultiLineDiscount_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function verifyValueRangeIsAllowedType_15 ($$arg :  entity.UWCompany[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function verifyValueRangeIsAllowedType_15 ($$arg :  gw.api.database.IQueryBeanResult<entity.UWCompany>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function verifyValueRangeIsAllowedType_25 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function verifyValueRangeIsAllowedType_25 ($$arg :  typekey.MultiLineDiscount_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UWCompany_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 45, column 41
    function verifyValueRange_16 () : void {
      var __valueRangeArg = availableUWCompanies.Value
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=multiLineDiscount_Input) at PolicyChangeWizard_PolicyInfoDV.pcf: line 56, column 237
    function verifyValueRange_26 () : void {
      var __valueRangeArg = tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_25(__valueRangeArg)
    }
    
    // 'visible' attribute on InputDivider at PolicyChangeWizard_PolicyInfoDV.pcf: line 47, column 238
    function visible_18 () : java.lang.Boolean {
      return policyPeriod.WC7LineExists ? false : (tdic.web.pcf.helper.PolicyInfoHelper.isMultiLineDiscountVisible(policyPeriod) && tdic.web.pcf.helper.PolicyInfoHelper.getMultiLineDiscountSelections(policyPeriod)!=null)
    }
    
    property get availableUWCompanies () : gw.job.AvailableUWCompanies {
      return getVariableValue("availableUWCompanies", 0) as gw.job.AvailableUWCompanies
    }
    
    property set availableUWCompanies ($arg :  gw.job.AvailableUWCompanies) {
      setVariableValue("availableUWCompanies", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function getMultiLineDiscountSelections() : typekey.MultiLineDiscount_TDIC[] {
          if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
            return MultiLineDiscount_TDIC.getTypeKeys(false).toTypedArray()
          }
          return MultiLineDiscount_TDIC.TF_OTHER.TypeKeys.toTypedArray()
        }
    
    
  }
  
  
}