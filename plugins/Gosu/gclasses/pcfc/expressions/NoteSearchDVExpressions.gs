package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/NoteSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NoteSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/NoteSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NoteSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at NoteSearchDV.pcf: line 34, column 40
    function action_6 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at NoteSearchDV.pcf: line 34, column 40
    function action_dest_7 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'def' attribute on InputSetRef at NoteSearchDV.pcf: line 90, column 41
    function def_onEnter_52 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at NoteSearchDV.pcf: line 90, column 41
    function def_refreshVariables_53 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NoteSearchDV.pcf: line 41, column 67
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SortBy = (__VALUE_TO_SET as typekey.SortByRange)
    }
    
    // 'value' attribute on BooleanRadioInput (id=sortAscending_Input) at NoteSearchDV.pcf: line 56, column 47
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SortAscending = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at NoteSearchDV.pcf: line 29, column 38
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Text = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Topic = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.RelatedTo = __VALUE_TO_SET
    }
    
    // 'value' attribute on DateInput (id=DateFrom_Input) at NoteSearchDV.pcf: line 81, column 42
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateFrom = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=DateTo_Input) at NoteSearchDV.pcf: line 86, column 40
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DateTo = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on AltUserInput (id=Author_Input) at NoteSearchDV.pcf: line 34, column 40
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Author = (__VALUE_TO_SET as entity.User)
    }
    
    // 'initialValue' attribute on Variable at NoteSearchDV.pcf: line 19, column 34
    function initialValue_0 () : java.lang.Object[] {
      return searchCriteria.getRelatedToSearchCriteria(policyPeriod)
    }
    
    // 'initialValue' attribute on Variable at NoteSearchDV.pcf: line 23, column 23
    function initialValue_1 () : boolean {
      return initializeRelatedTo()
    }
    
    // 'optionLabel' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function optionLabel_39 (VALUE :  java.lang.Object) : java.lang.String {
      return Note.getLevelDisplayString(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function valueRange_21 () : java.lang.Object {
      return searchCriteria.SortByRange
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function valueRange_32 () : java.lang.Object {
      return getNoteTopics()
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function valueRange_40 () : java.lang.Object {
      return relatedToSearchCriteria
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at NoteSearchDV.pcf: line 29, column 38
    function valueRoot_4 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NoteSearchDV.pcf: line 41, column 67
    function value_13 () : typekey.LanguageType {
      return searchCriteria.Language
    }
    
    // 'value' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function value_18 () : typekey.SortByRange {
      return searchCriteria.SortBy
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at NoteSearchDV.pcf: line 29, column 38
    function value_2 () : java.lang.String {
      return searchCriteria.Text
    }
    
    // 'value' attribute on BooleanRadioInput (id=sortAscending_Input) at NoteSearchDV.pcf: line 56, column 47
    function value_25 () : java.lang.Boolean {
      return searchCriteria.SortAscending
    }
    
    // 'value' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function value_29 () : typekey.NoteTopicType {
      return searchCriteria.Topic
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function value_36 () : java.lang.Object {
      return searchCriteria.RelatedTo
    }
    
    // 'value' attribute on DateInput (id=DateFrom_Input) at NoteSearchDV.pcf: line 81, column 42
    function value_44 () : java.util.Date {
      return searchCriteria.DateFrom
    }
    
    // 'value' attribute on DateInput (id=DateTo_Input) at NoteSearchDV.pcf: line 86, column 40
    function value_48 () : java.util.Date {
      return searchCriteria.DateTo
    }
    
    // 'value' attribute on AltUserInput (id=Author_Input) at NoteSearchDV.pcf: line 34, column 40
    function value_8 () : entity.User {
      return searchCriteria.Author
    }
    
    // 'valueRange' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function verifyValueRangeIsAllowedType_22 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function verifyValueRangeIsAllowedType_22 ($$arg :  typekey.SortByRange[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function verifyValueRangeIsAllowedType_33 ($$arg :  typekey.NoteTopicType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.lang.Object[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SortBy_Input) at NoteSearchDV.pcf: line 49, column 42
    function verifyValueRange_23 () : void {
      var __valueRangeArg = searchCriteria.SortByRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_22(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Topic_Input) at NoteSearchDV.pcf: line 66, column 44
    function verifyValueRange_34 () : void {
      var __valueRangeArg = getNoteTopics()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at NoteSearchDV.pcf: line 76, column 39
    function verifyValueRange_42 () : void {
      var __valueRangeArg = relatedToSearchCriteria
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_41(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at NoteSearchDV.pcf: line 41, column 67
    function visible_12 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get defaultRelatedTo () : boolean {
      return getVariableValue("defaultRelatedTo", 0) as java.lang.Boolean
    }
    
    property set defaultRelatedTo ($arg :  boolean) {
      setVariableValue("defaultRelatedTo", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get relatedToSearchCriteria () : java.lang.Object[] {
      return getVariableValue("relatedToSearchCriteria", 0) as java.lang.Object[]
    }
    
    property set relatedToSearchCriteria ($arg :  java.lang.Object[]) {
      setVariableValue("relatedToSearchCriteria", 0, $arg)
    }
    
    property get searchCriteria () : NoteSearchCriteria {
      return getRequireValue("searchCriteria", 0) as NoteSearchCriteria
    }
    
    property set searchCriteria ($arg :  NoteSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    function initializeRelatedTo() : boolean {
          if (policyPeriod.Policy == null) {
            searchCriteria.RelatedTo = account
          } else {
            searchCriteria.RelatedTo = policyPeriod.Policy
          }
          return true
    }
        function getNoteTopics() : typekey.NoteTopicType[] {
          if (policyPeriod.WC7LineExists) {
             return typekey.NoteTopicType.TF_WC_NOTETOPICTYPES.getTypeKeys().toTypedArray()
          }
          if(policyPeriod.BOPLineExists || policyPeriod.GLLineExists) {
             return typekey.NoteTopicType.TF_BOP_GL_NOTETOPICTYPES.getTypeKeys().toTypedArray()
              // var nl = {NoteTopicType.TC_GENERAL, NoteTopicType.TC_BILLINGPAYMENT_TDIC, NoteTopicType.TC_CANCELLATIONS_TDIC, NoteTopicType.TC_COVERAGE_TDIC,
              //    NoteTopicType.TC_END_REQ_DETAILS_TDIC, NoteTopicType.TC_OTHER_TDIC, NoteTopicType.TC_UNDERWRITING_TDIC, NoteTopicType.TC_LOSSCONTROL, NoteTopicType.TC_RISK}
          }
          return typekey.NoteTopicType.getTypeKeys(false).toTypedArray()    
        }
    
    
  }
  
  
}