package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactCVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=ViewInContactFile) at AccountContactCV.pcf: line 31, column 41
    function action_1 () : void {
      ContactFile_Details.push(acctContact.Contact)
    }
    
    // 'action' attribute on Link (id=ViewInContactFile) at AccountContactCV.pcf: line 31, column 41
    function action_dest_2 () : pcf.api.Destination {
      return pcf.ContactFile_Details.createDestination(acctContact.Contact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 123, column 99
    function def_onEnter_33 (def :  pcf.AddressesPanelSet) : void {
      def.onEnter(acctContact.Contact, showAddressTools, acctContact.Account, null)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 133, column 62
    function def_onEnter_35 (def :  pcf.AccountFile_Contacts_WorkOrdersLV) : void {
      def.onEnter(acctContact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 144, column 60
    function def_onEnter_37 (def :  pcf.AccountFile_Contacts_PoliciesLV) : void {
      def.onEnter(acctContact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 39, column 29
    function def_onEnter_5 (def :  pcf.AccountContactDV) : void {
      def.onEnter(acctContact.Contact, acctContact.Account)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 123, column 99
    function def_refreshVariables_34 (def :  pcf.AddressesPanelSet) : void {
      def.refreshVariables(acctContact.Contact, showAddressTools, acctContact.Account, null)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 133, column 62
    function def_refreshVariables_36 (def :  pcf.AccountFile_Contacts_WorkOrdersLV) : void {
      def.refreshVariables(acctContact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 144, column 60
    function def_refreshVariables_38 (def :  pcf.AccountFile_Contacts_PoliciesLV) : void {
      def.refreshVariables(acctContact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 39, column 29
    function def_refreshVariables_6 (def :  pcf.AccountContactDV) : void {
      def.refreshVariables(acctContact.Contact, acctContact.Account)
    }
    
    // 'initialValue' attribute on Variable at AccountContactCV.pcf: line 19, column 54
    function initialValue_0 () : gw.plugin.contact.IContactConfigPlugin {
      return gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)
    }
    
    // 'sortBy' attribute on IteratorSort at AccountContactCV.pcf: line 100, column 34
    function sortBy_13 (acctContactRole :  entity.AccountContactRole) : java.lang.Object {
      return (typeof acctContactRole).TypeInfo.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at AccountContactCV.pcf: line 69, column 36
    function sortBy_7 (roleType :  typekey.AccountContactRole) : java.lang.Object {
      return roleType
    }
    
    // 'value' attribute on TextCell (id=ContactRole_Cell) at AccountContactCV.pcf: line 106, column 76
    function sortValue_14 (acctContactRole :  entity.AccountContactRole) : java.lang.Object {
      return (typeof acctContactRole).TypeInfo.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at AccountContactCV.pcf: line 97, column 57
    function toCreateAndAdd_28 () : entity.AccountContactRole {
      return dummyCreateAndAddAccountContactRole()
    }
    
    // 'toRemove' attribute on RowIterator at AccountContactCV.pcf: line 97, column 57
    function toRemove_29 (acctContactRole :  entity.AccountContactRole) : void {
      tryToRemoveRole(acctContactRole)
    }
    
    // 'value' attribute on AddMenuItemIterator at AccountContactCV.pcf: line 66, column 60
    function value_10 () : typekey.AccountContactRole[] {
      return acctContact.AvailableAccountContactRoleTypes.subtract({TC_ACCOUNTHOLDER}).toTypedArray()
    }
    
    // 'value' attribute on RowIterator at AccountContactCV.pcf: line 97, column 57
    function value_30 () : entity.AccountContactRole[] {
      return acctContact.Roles
    }
    
    // 'visible' attribute on AddButton (id=AddRoleButton) at AccountContactCV.pcf: line 62, column 55
    function visible_11 () : java.lang.Boolean {
      return acctContact.Contact != null
    }
    
    // 'visible' attribute on DetailViewPanel at AccountContactCV.pcf: line 24, column 39
    function visible_3 () : java.lang.Boolean {
      return acctContact != null
    }
    
    // 'visible' attribute on Card (id=RolesCard) at AccountContactCV.pcf: line 49, column 30
    function visible_31 () : java.lang.Boolean {
      return showRolesTab
    }
    
    // 'visible' attribute on Toolbar (id=AddressesPanelSet_tb) at AccountContactCV.pcf: line 125, column 87
    function visible_32 () : java.lang.Boolean {
      return acctContact.Account.Editable and perm.System.editaccountcontacts
    }
    
    // 'visible' attribute on Toolbar (id=AccountContactDV_tb) at AccountContactCV.pcf: line 41, column 67
    function visible_4 () : java.lang.Boolean {
      return perm.Account.edit( acctContact.Account )
    }
    
    property get acctContact () : AccountContact {
      return getRequireValue("acctContact", 0) as AccountContact
    }
    
    property set acctContact ($arg :  AccountContact) {
      setRequireValue("acctContact", 0, $arg)
    }
    
    property get contactConfigPlugin () : gw.plugin.contact.IContactConfigPlugin {
      return getVariableValue("contactConfigPlugin", 0) as gw.plugin.contact.IContactConfigPlugin
    }
    
    property set contactConfigPlugin ($arg :  gw.plugin.contact.IContactConfigPlugin) {
      setVariableValue("contactConfigPlugin", 0, $arg)
    }
    
    property get showAddressTools () : boolean {
      return getRequireValue("showAddressTools", 0) as java.lang.Boolean
    }
    
    property set showAddressTools ($arg :  boolean) {
      setRequireValue("showAddressTools", 0, $arg)
    }
    
    property get showRolesTab () : boolean {
      return getRequireValue("showRolesTab", 0) as java.lang.Boolean
    }
    
    property set showRolesTab ($arg :  boolean) {
      setRequireValue("showRolesTab", 0, $arg)
    }
    
    function tryToRemoveRole(acRole : AccountContactRole) {
     if (acRole.AccountContact.Roles.Count == 1) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.AccountContactCV.Error.CannotRemoveOnlyRole"))
      }
      if (not acRole.canRemove()) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.AccountContactCV.Error.CannotRemoveInUseRole", acRole))
      }
      
      acRole.AccountContact.removeFromRoles(acRole)
    }
    
    function dummyCreateAndAddAccountContactRole() : AccountContactRole {
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountContactCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at AccountContactCV.pcf: line 97, column 57
    function checkBoxVisible_27 () : java.lang.Boolean {
      return not (acctContactRole typeis AccountHolder)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_onEnter_18 (def :  pcf.AccountContactDisplayCell_Driver) : void {
      def.onEnter(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_onEnter_20 (def :  pcf.AccountContactDisplayCell_NamedInsured) : void {
      def.onEnter(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_onEnter_22 (def :  pcf.AccountContactDisplayCell_OwnerOfficer) : void {
      def.onEnter(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_onEnter_24 (def :  pcf.AccountContactDisplayCell_default) : void {
      def.onEnter(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_refreshVariables_19 (def :  pcf.AccountContactDisplayCell_Driver) : void {
      def.refreshVariables(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_refreshVariables_21 (def :  pcf.AccountContactDisplayCell_NamedInsured) : void {
      def.refreshVariables(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_refreshVariables_23 (def :  pcf.AccountContactDisplayCell_OwnerOfficer) : void {
      def.refreshVariables(acctContactRole)
    }
    
    // 'def' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function def_refreshVariables_25 (def :  pcf.AccountContactDisplayCell_default) : void {
      def.refreshVariables(acctContactRole)
    }
    
    // 'mode' attribute on ModalCellRef (id=ContactAdditionalInfo) at AccountContactCV.pcf: line 111, column 53
    function mode_26 () : java.lang.Object {
      return acctContactRole.Subtype
    }
    
    // 'value' attribute on TextCell (id=ContactRole_Cell) at AccountContactCV.pcf: line 106, column 76
    function valueRoot_16 () : java.lang.Object {
      return (typeof acctContactRole).TypeInfo
    }
    
    // 'value' attribute on TextCell (id=ContactRole_Cell) at AccountContactCV.pcf: line 106, column 76
    function value_15 () : java.lang.String {
      return (typeof acctContactRole).TypeInfo.DisplayName
    }
    
    property get acctContactRole () : entity.AccountContactRole {
      return getIteratedValue(1) as entity.AccountContactRole
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountContactCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=RoleType) at AccountContactCV.pcf: line 74, column 72
    function label_8 () : java.lang.Object {
      return contactConfigPlugin.getAccountContactRoleTypeDisplayName(roleType)
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=RoleType) at AccountContactCV.pcf: line 74, column 72
    function toCreateAndAdd_9 (CheckedValues :  Object[]) : java.lang.Object {
      return acctContact.addNewRole(roleType)
    }
    
    property get roleType () : typekey.AccountContactRole {
      return getIteratedValue(1) as typekey.AccountContactRole
    }
    
    
  }
  
  
}