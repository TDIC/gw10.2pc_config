package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyLineSummaryPanelSet_WC7LineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CardViewPanelExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 133, column 89
    function def_onEnter_350 (def :  pcf.WC7PolicyLinePerStateConfigDV) : void {
      def.onEnter(wcLine, selectedJurisdiction, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 141, column 68
    function def_onEnter_352 (def :  pcf.WC7ClassesInputSet_default) : void {
      def.onEnter(selectedJurisdiction, wcLine, stateConfig)
    }
    
    // 'def' attribute on PanelRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 133, column 89
    function def_refreshVariables_351 (def :  pcf.WC7PolicyLinePerStateConfigDV) : void {
      def.refreshVariables(wcLine, selectedJurisdiction, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 141, column 68
    function def_refreshVariables_353 (def :  pcf.WC7ClassesInputSet_default) : void {
      def.refreshVariables(selectedJurisdiction, wcLine, stateConfig)
    }
    
    // 'initialValue' attribute on Variable at PolicyLineSummaryPanelSet.WC7Line.pcf: line 128, column 60
    function initialValue_349 () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return gw.lob.wc7.stateconfigs.WC7StateConfig.forJurisdiction(selectedJurisdiction.Jurisdiction)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 141, column 68
    function mode_354 () : java.lang.Object {
      return selectedJurisdiction.Jurisdiction.Code
    }
    
    property get stateConfig () : gw.lob.wc7.stateconfigs.WC7StateConfig {
      return getVariableValue("stateConfig", 2) as gw.lob.wc7.stateconfigs.WC7StateConfig
    }
    
    property set stateConfig ($arg :  gw.lob.wc7.stateconfigs.WC7StateConfig) {
      setVariableValue("stateConfig", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends PolicyLineSummaryPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyLineSummaryPanelSet.WC7Line.pcf: line 27, column 55
    function initialValue_2 () : gw.api.productmodel.CoveragePattern[] {
      return wcLine.Pattern.getCoverageCategoryByPublicId("WC7WorkCompLineCategory").coveragePatternsForEntity(WC7WorkersCompLine).where(\ c -> wcLine.isCoverageSelectedOrAvailable(c))
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 51, column 28
    function sortBy_10 (linecov :  entity.Coverage) : java.lang.Object {
      return linecov.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 54, column 28
    function sortBy_11 (linecov :  entity.Coverage) : java.lang.Object {
      return linecov.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 69, column 28
    function sortBy_121 (lineexcl :  entity.Exclusion) : java.lang.Object {
      return lineexcl.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 72, column 28
    function sortBy_122 (lineexcl :  entity.Exclusion) : java.lang.Object {
      return lineexcl.DisplayName
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 87, column 28
    function sortBy_232 (linecond :  entity.PolicyCondition) : java.lang.Object {
      return linecond.Pattern.Priority
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 90, column 28
    function sortBy_233 (linecond :  entity.PolicyCondition) : java.lang.Object {
      return linecond.DisplayName
    }
    
    // 'value' attribute on TextInput (id=GoverningClass_Input) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 41, column 41
    function valueRoot_8 () : java.lang.Object {
      return wcLine.WC7GoverningClass
    }
    
    // 'value' attribute on InputIterator (id=lineCovIterator) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 48, column 41
    function value_119 () : entity.Coverage[] {
      return wcLine.CoveragesFromCoverable
    }
    
    // 'value' attribute on InputIterator (id=lineExclIterator) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 66, column 42
    function value_230 () : entity.Exclusion[] {
      return wcLine.ExclusionsFromCoverable
    }
    
    // 'value' attribute on InputIterator (id=lineCondIterator) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 84, column 48
    function value_341 () : entity.PolicyCondition[] {
      return wcLine.ConditionsFromCoverable
    }
    
    // 'value' attribute on TextInput (id=CoveredStates_Input) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 35, column 60
    function value_4 () : java.lang.String {
      return wcLine.WC7Jurisdictions*.Jurisdiction.join(",")
    }
    
    // 'value' attribute on TextInput (id=GoverningClass_Input) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 41, column 41
    function value_7 () : java.lang.String {
      return wcLine.WC7GoverningClass.Code
    }
    
    // 'visible' attribute on Label at PolicyLineSummaryPanelSet.WC7Line.pcf: line 61, column 59
    function visible_120 () : java.lang.Boolean {
      return not wcLine.WC7LineExclusions.IsEmpty
    }
    
    // 'visible' attribute on Label at PolicyLineSummaryPanelSet.WC7Line.pcf: line 79, column 59
    function visible_231 () : java.lang.Boolean {
      return not wcLine.WC7LineConditions.IsEmpty
    }
    
    // 'visible' attribute on TextInput (id=CoveredStates_Input) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 35, column 60
    function visible_3 () : java.lang.Boolean {
      return wcLine.WC7Jurisdictions.Count > 15
    }
    
    property get wcLineCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("wcLineCoveragePatterns", 1) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set wcLineCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("wcLineCoveragePatterns", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_123 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_125 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_127 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_129 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_131 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_133 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_135 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_137 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_139 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_141 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_143 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_145 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_147 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_149 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_151 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_153 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_155 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_157 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_159 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_161 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_163 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_165 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_167 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_169 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_171 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_173 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_175 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_177 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_179 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_181 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_183 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_185 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_187 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_189 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_191 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_193 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_195 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_197 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_199 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_201 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_203 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_205 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_207 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_209 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_211 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_213 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_215 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_217 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_219 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_221 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_223 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_225 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_onEnter_227 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_124 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_126 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_128 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_130 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_132 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_134 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_136 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_138 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_140 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_142 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_144 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_146 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_148 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_150 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_152 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_154 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_156 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_158 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_160 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_162 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_164 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_166 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_168 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_170 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_172 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_174 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_176 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_178 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_180 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_182 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_184 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_186 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_188 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_190 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_192 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_194 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_196 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_198 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_200 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_202 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_204 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_206 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_208 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_210 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_212 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_214 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_216 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_218 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_220 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_222 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_224 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_226 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function def_refreshVariables_228 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(lineexcl.Pattern, wcLine, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 75, column 42
    function mode_229 () : java.lang.Object {
      return lineexcl.PatternCode
    }
    
    property get lineexcl () : entity.Exclusion {
      return getIteratedValue(2) as entity.Exclusion
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_234 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_236 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_238 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_240 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_242 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_244 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_246 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_248 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_250 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_252 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_254 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_256 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_258 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_260 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_262 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_264 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_266 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_268 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_270 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_272 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_274 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_276 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_278 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_280 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_282 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_284 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_286 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_288 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_290 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_292 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_294 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_296 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_298 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_300 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_302 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_304 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_306 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_308 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_310 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_312 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_314 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_316 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_318 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_320 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_322 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_324 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_326 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_328 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_330 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_332 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_334 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_336 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_onEnter_338 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_235 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_237 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_239 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_241 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_243 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_245 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_247 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_249 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_251 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_253 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_255 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_257 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_259 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_261 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_263 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_265 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_267 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_269 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_271 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_273 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_275 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_277 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_279 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_281 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_283 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_285 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_287 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_289 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_291 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_293 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_295 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_297 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_299 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_301 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_303 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_305 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_307 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_309 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_311 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_313 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_315 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_317 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_319 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_321 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_323 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_325 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_327 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_329 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_331 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_333 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_335 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_337 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function def_refreshVariables_339 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(linecond.Pattern, wcLine, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 93, column 42
    function mode_340 () : java.lang.Object {
      return linecond.PatternCode
    }
    
    property get linecond () : entity.PolicyCondition {
      return getIteratedValue(2) as entity.PolicyCondition
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 119, column 51
    function valueRoot_345 () : java.lang.Object {
      return covJuris
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 119, column 51
    function value_344 () : typekey.Jurisdiction {
      return covJuris.Jurisdiction
    }
    
    property get covJuris () : entity.WC7Jurisdiction {
      return getIteratedValue(2) as entity.WC7Jurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_112 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_114 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_116 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_12 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_14 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_16 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_18 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_20 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_22 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_24 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_26 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_28 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_30 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_32 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_34 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_36 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_38 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_40 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_42 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_44 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_46 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_48 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_50 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_62 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_64 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_66 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_68 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_70 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_72 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_74 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_76 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_78 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_80 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_82 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_84 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_86 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_88 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_90 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_92 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_94 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_96 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_onEnter_98 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_13 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_15 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_17 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_19 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_21 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_23 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_25 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_27 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_29 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_31 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_33 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_35 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(linecov.Pattern, wcLine, false)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineSummaryPanelSet.WC7Line.pcf: line 57, column 41
    function mode_118 () : java.lang.Object {
      return linecov.PatternCode
    }
    
    property get linecov () : entity.Coverage {
      return getIteratedValue(2) as entity.Coverage
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends PolicyLineSummaryPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ListViewPanel (id=PolicyLinePerStateConfig_LV) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 105, column 26
    function available_348 () : java.lang.Boolean {
      return selectedJurisdiction != null
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineSummaryPanelSet.WC7Line.pcf: line 113, column 30
    function sortBy_342 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction.DisplayName
    }
    
    // 'value' attribute on TextCell (id=StateName_Cell) at PolicyLineSummaryPanelSet.WC7Line.pcf: line 119, column 51
    function sortValue_343 (covJuris :  entity.WC7Jurisdiction) : java.lang.Object {
      return covJuris.Jurisdiction
    }
    
    // 'value' attribute on RowIterator at PolicyLineSummaryPanelSet.WC7Line.pcf: line 110, column 50
    function value_347 () : entity.WC7Jurisdiction[] {
      return wcLine.WC7Jurisdictions
    }
    
    property get selectedJurisdiction () : WC7Jurisdiction {
      return getCurrentSelection(1) as WC7Jurisdiction
    }
    
    property set selectedJurisdiction ($arg :  WC7Jurisdiction) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/PolicyLineSummaryPanelSet.WC7Line.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyLineSummaryPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyLineSummaryPanelSet.WC7Line.pcf: line 17, column 35
    function initialValue_0 () : entity.PolicyPeriod {
      return line.Branch
    }
    
    // 'initialValue' attribute on Variable at PolicyLineSummaryPanelSet.WC7Line.pcf: line 21, column 41
    function initialValue_1 () : entity.WC7WorkersCompLine {
      return line as WC7WorkersCompLine
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get line () : PolicyLine {
      return getRequireValue("line", 0) as PolicyLine
    }
    
    property set line ($arg :  PolicyLine) {
      setRequireValue("line", 0, $arg)
    }
    
    property get period () : entity.PolicyPeriod {
      return getVariableValue("period", 0) as entity.PolicyPeriod
    }
    
    property set period ($arg :  entity.PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    property get wcLine () : entity.WC7WorkersCompLine {
      return getVariableValue("wcLine", 0) as entity.WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  entity.WC7WorkersCompLine) {
      setVariableValue("wcLine", 0, $arg)
    }
    
    function getOfficialIDs(covJuris : entity.WC7Jurisdiction) : entity.OfficialID[] {
      var officialIDs = period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.OfficialIDs
            .where( \ o -> o.State == covJuris.Jurisdiction)
      return officialIDs
    }
    
    function officialIDOutputConverter(VALUE : entity.OfficialID[]) : String {
      if (VALUE == null) {return ""}
      var officialIDValues = VALUE.map(\ id -> id.getOfficialIDValue()!=null ? id.getOfficialIDValue() : "")
      return officialIDValues.where(\ val -> val!=null).join(",")
    }
    
    
  }
  
  
}