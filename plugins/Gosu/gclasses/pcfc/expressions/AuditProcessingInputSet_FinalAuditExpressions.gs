package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
uses java.util.ArrayList
@javax.annotation.Generated("config/web/pcf/job/audit/AuditProcessingInputSet.FinalAudit.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AuditProcessingInputSet_FinalAuditExpressions {
  @javax.annotation.Generated("config/web/pcf/job/audit/AuditProcessingInputSet.FinalAudit.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AuditProcessingInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateInput (id=EstimatedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 41, column 172
    function available_18 () : java.lang.Boolean {
      return (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED) and audit.AuditInformation.IsComplete and audit.PolicyPeriod.WC7LineExists
    }
    
    // 'available' attribute on DateInput (id=ReceivedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 72, column 100
    function available_43 () : java.lang.Boolean {
      return not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)
    }
    
    // 'available' attribute on TextInput (id=Auditor_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 94, column 55
    function available_60 () : java.lang.Boolean {
      return not audit.PolicyPeriod.WC7LineExists
    }
    
    // 'available' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function available_67 () : java.lang.Boolean {
      return audit.PolicyPeriod.WC7LineExists and not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)
    }
    
    // 'available' attribute on TextInput (id=TdicAuditorEstimate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 111, column 134
    function available_78 () : java.lang.Boolean {
      return audit.PolicyPeriod.WC7LineExists and (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 35, column 100
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.AuditInformation.DueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=NoticeSentToAuditor_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 52, column 93
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.AuditInformation.NoticeSentAuditor_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=AppointmentSet_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 61, column 93
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.AuditInformation.AppointmentDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 72, column 100
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.AuditInformation.ReceivedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ReadyForReviewDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 84, column 100
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.AuditInformation.ReadyForReviewDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      audit.Auditor_TDIC = (__VALUE_TO_SET as entity.User)
    }
    
    // 'editable' attribute on DateInput (id=DueDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 35, column 100
    function editable_10 () : java.lang.Boolean {
      return perm.Audit.reschedule
    }
    
    // 'initialValue' attribute on Variable at AuditProcessingInputSet.FinalAudit.pcf: line 14, column 30
    function initialValue_0 () : pcf.api.Wizard {
      return CurrentLocation as pcf.api.Wizard
    }
    
    // 'initialValue' attribute on Variable at AuditProcessingInputSet.FinalAudit.pcf: line 18, column 42
    function initialValue_1 () : java.util.List<User> {
      return loadAvailableAuditors_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at AuditProcessingInputSet.FinalAudit.pcf: line 75, column 92
    function onChange_42 () : void {
      audit.AuditInformation.onReceivedDateFieldChange(); wizard.saveDraft()
    }
    
    // 'onChange' attribute on PostOnChange at AuditProcessingInputSet.FinalAudit.pcf: line 87, column 97
    function onChange_51 () : void {
      audit.AuditInformation.onReadyToReviewDateFieldChange(); wizard.saveDraft()
    }
    
    // 'valueRange' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function valueRange_72 () : java.lang.Object {
      return AvailableAuditors_TDIC
    }
    
    // 'value' attribute on DateInput (id=EstimatedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 41, column 172
    function valueRoot_21 () : java.lang.Object {
      return audit
    }
    
    // 'value' attribute on DateInput (id=AccountFlaggedForPhysicalAudit_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 25, column 94
    function valueRoot_4 () : java.lang.Object {
      return audit.AuditInformation
    }
    
    // 'value' attribute on TextInput (id=Auditor_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 94, column 55
    function valueRoot_63 () : java.lang.Object {
      return audit.getUserRoleAssignmentByRole(TC_AUDITOR).AssignedUser
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 35, column 100
    function value_12 () : java.util.Date {
      return audit.AuditInformation.DueDate
    }
    
    // 'value' attribute on DateInput (id=EstimatedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 41, column 172
    function value_20 () : java.util.Date {
      return audit.CloseDate
    }
    
    // 'value' attribute on DateInput (id=NoticeSentToPolicyHolder_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 46, column 94
    function value_26 () : java.util.Date {
      return audit.AuditInformation.NoticeSentHolder_TDIC
    }
    
    // 'value' attribute on DateInput (id=AccountFlaggedForPhysicalAudit_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 25, column 94
    function value_3 () : java.util.Date {
      return audit.AuditInformation.PhysicalAuditFlagged_TDIC
    }
    
    // 'value' attribute on DateInput (id=NoticeSentToAuditor_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 52, column 93
    function value_31 () : java.util.Date {
      return audit.AuditInformation.NoticeSentAuditor_TDIC
    }
    
    // 'value' attribute on DateInput (id=AppointmentSet_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 61, column 93
    function value_37 () : java.util.Date {
      return audit.AuditInformation.AppointmentDate_TDIC
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 72, column 100
    function value_45 () : java.util.Date {
      return audit.AuditInformation.ReceivedDate
    }
    
    // 'value' attribute on DateInput (id=ReadyForReviewDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 84, column 100
    function value_54 () : java.util.Date {
      return audit.AuditInformation.ReadyForReviewDate
    }
    
    // 'value' attribute on TextInput (id=Auditor_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 94, column 55
    function value_62 () : java.lang.String {
      return audit.getUserRoleAssignmentByRole(TC_AUDITOR).AssignedUser.DisplayName
    }
    
    // 'value' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function value_69 () : entity.User {
      return audit.Auditor_TDIC
    }
    
    // 'value' attribute on DateInput (id=InitDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 29, column 48
    function value_7 () : java.util.Date {
      return audit.AuditInformation.InitDate
    }
    
    // 'valueRange' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function verifyValueRangeIsAllowedType_73 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function verifyValueRangeIsAllowedType_73 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function verifyValueRangeIsAllowedType_73 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Auditor_TDIC_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 105, column 138
    function verifyValueRange_74 () : void {
      var __valueRangeArg = AvailableAuditors_TDIC
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_73(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=DueDate_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 35, column 100
    function visible_11 () : java.lang.Boolean {
      return not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL)
    }
    
    // 'visible' attribute on DateInput (id=AccountFlaggedForPhysicalAudit_Input) at AuditProcessingInputSet.FinalAudit.pcf: line 25, column 94
    function visible_2 () : java.lang.Boolean {
      return audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL
    }
    
    property get AvailableAuditors_TDIC () : java.util.List<User> {
      return getVariableValue("AvailableAuditors_TDIC", 0) as java.util.List<User>
    }
    
    property set AvailableAuditors_TDIC ($arg :  java.util.List<User>) {
      setVariableValue("AvailableAuditors_TDIC", 0, $arg)
    }
    
    property get audit () : Audit {
      return getRequireValue("audit", 0) as Audit
    }
    
    property set audit ($arg :  Audit) {
      setRequireValue("audit", 0, $arg)
    }
    
    property get wizard () : pcf.api.Wizard {
      return getVariableValue("wizard", 0) as pcf.api.Wizard
    }
    
    property set wizard ($arg :  pcf.api.Wizard) {
      setVariableValue("wizard", 0, $arg)
    }
    
    
    public function loadAvailableAuditors_TDIC() : java.util.List<User> {
      var auditors = new ArrayList<User>();
      var user : User;
      
      // Find all groups with the auditor group type.
      var groupType = audit.LatestPeriod.AuditProcess.AuditorGroupType_TDIC;
      var query = Query.make(Group).compare(Group#GroupType, Equals, groupType);
      var groups = query.select();
      
      // Build a unique list of active users from all the groups
      for (group in groups) {
        for (groupUser in group.Users) {
          user = groupUser.User;    
          if (user.Credential.Active) {
            if (auditors.contains(groupUser.User) == false) {
              auditors.add (groupUser.User);
            }
          }
        }
      }
    
      return auditors;
    }
    
    
  }
  
  
}