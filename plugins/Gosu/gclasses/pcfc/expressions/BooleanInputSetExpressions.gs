package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/BooleanInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BooleanInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/BooleanInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BooleanInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'confirmMessage' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function confirmMessage_2 () : java.lang.String {
      return displayable.ConfirmMessage
    }
    
    // 'value' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      displayable.Value = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function editable_0 () : java.lang.Boolean {
      return displayable.Editable
    }
    
    // 'label' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function label_3 () : java.lang.Object {
      return displayable.Label
    }
    
    // 'onChange' attribute on PostOnChange at BooleanInputSet.pcf: line 35, column 50
    function onChange_12 () : void {
      displayable.onChange(wizard)
    }
    
    // 'showConfirmMessage' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function showConfirmMessage_4 () : java.lang.Boolean {
      return displayable.ShowConfirmMessage
    }
    
    // 'value' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function valueRoot_7 () : java.lang.Object {
      return displayable
    }
    
    // 'value' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function value_5 () : java.lang.Boolean {
      return displayable.Value
    }
    
    // 'visible' attribute on CheckBoxInput (id=Displayable_Input) at BooleanInputSet.pcf: line 24, column 67
    function visible_1 () : java.lang.Boolean {
      return not fieldDependencyInput and displayable.Visible
    }
    
    // 'visible' attribute on CheckBoxInput (id=FieldDependencyDisplayable_Input) at BooleanInputSet.pcf: line 33, column 62
    function visible_14 () : java.lang.Boolean {
      return fieldDependencyInput and displayable.Visible
    }
    
    property get displayable () : gw.lob.common.displayable.Displayable<Boolean> {
      return getRequireValue("displayable", 0) as gw.lob.common.displayable.Displayable<Boolean>
    }
    
    property set displayable ($arg :  gw.lob.common.displayable.Displayable<Boolean>) {
      setRequireValue("displayable", 0, $arg)
    }
    
    property get fieldDependencyInput () : boolean {
      return getRequireValue("fieldDependencyInput", 0) as java.lang.Boolean
    }
    
    property set fieldDependencyInput ($arg :  boolean) {
      setRequireValue("fieldDependencyInput", 0, $arg)
    }
    
    property get wizard () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("wizard", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set wizard ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("wizard", 0, $arg)
    }
    
    
  }
  
  
}