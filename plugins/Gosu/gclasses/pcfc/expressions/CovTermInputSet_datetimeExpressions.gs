package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses typekey.Job
@javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.datetime.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CovTermInputSet_datetimeExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/CovTermInputSet.datetime.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CovTermInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateInput (id=DateTimeTermInput_RetroActive_Input) at CovTermInputSet.datetime.pcf: line 47, column 191
    function available_24 () : java.lang.Boolean {
      return (term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC" and (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT))
    }
    
    // 'available' attribute on DateInput (id=DateTimeTermInput_RetroActive1_Input) at CovTermInputSet.datetime.pcf: line 57, column 83
    function available_37 () : java.lang.Boolean {
      return term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC"
    }
    
    // 'value' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      (term as gw.api.domain.covterm.GenericCovTerm<java.util.Date>).Value = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function editable_1 () : java.lang.Boolean {
      return isRetroDatetimeFieldsEditable() //term.Pattern.CodeIdentifier == "GLNTCExpDate_TDIC" ? false : true
    }
    
    // 'editable' attribute on DateInput (id=DateTimeTermInput_RetroActive_Input) at CovTermInputSet.datetime.pcf: line 47, column 191
    function editable_25 () : java.lang.Boolean {
      return isRetroDatetimeFieldsEditable()
    }
    
    // 'initialValue' attribute on Variable at CovTermInputSet.datetime.pcf: line 20, column 28
    function initialValue_0 () : PolicyPeriod {
      return coverable.PolicyLine.Branch
    }
    
    // 'label' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function label_3 () : java.lang.Object {
      return term.Pattern.DisplayName
    }
    
    // 'requestValidationExpression' attribute on DateInput (id=DateTimeTermInput_RetroActive_Input) at CovTermInputSet.datetime.pcf: line 47, column 191
    function requestValidationExpression_28 (VALUE :  java.util.Date) : java.lang.Object {
      return validateRetroDateforPriorActs(VALUE)
    }
    
    // 'requestValidationExpression' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function requestValidationExpression_4 (VALUE :  java.util.Date) : java.lang.Object {
      return Period.isDiscountEffDateValid(VALUE)
    }
    
    // 'required' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function required_5 () : java.lang.Boolean {
      return !(term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC" and (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT)) and !(term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC")
    }
    
    // 'value' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function valueRoot_8 () : java.lang.Object {
      return (term as gw.api.domain.covterm.GenericCovTerm<java.util.Date>)
    }
    
    // 'value' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function value_6 () : java.util.Date {
      return (term as gw.api.domain.covterm.GenericCovTerm<java.util.Date>).Value
    }
    
    // 'visible' attribute on DateInput (id=EREDPLExpDate_Input) at CovTermInputSet.datetime.pcf: line 33, column 155
    function visible_13 () : java.lang.Boolean {
      return term.Pattern.CodeIdentifier == "GLERPDLExpDate_TDIC" or term.Pattern.CodeIdentifier == "GLCybEREExpirationDate_TDIC"
    }
    
    // 'visible' attribute on DateInput (id=DateTimeTermInput_Input) at CovTermInputSet.datetime.pcf: line 28, column 35
    function visible_2 () : java.lang.Boolean {
      return dateVisibility()
    }
    
    // 'visible' attribute on TextInput (id=EREDPLExpirationDate_Input) at CovTermInputSet.datetime.pcf: line 38, column 87
    function visible_20 () : java.lang.Boolean {
      return term.Pattern.CodeIdentifier == "GLERPEExpirationDate_TDIC"
    }
    
    // 'visible' attribute on DateInput (id=DateTimeTermInput_RetroActive1_Input) at CovTermInputSet.datetime.pcf: line 57, column 83
    function visible_39 () : java.lang.Boolean {
      return (term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC")
    }
    
    property get Period () : PolicyPeriod {
      return getVariableValue("Period", 0) as PolicyPeriod
    }
    
    property set Period ($arg :  PolicyPeriod) {
      setVariableValue("Period", 0, $arg)
    }
    
    property get coverable () : Coverable {
      return getRequireValue("coverable", 0) as Coverable
    }
    
    property set coverable ($arg :  Coverable) {
      setRequireValue("coverable", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get term () : gw.api.domain.covterm.CovTerm {
      return getRequireValue("term", 0) as gw.api.domain.covterm.CovTerm
    }
    
    property set term ($arg :  gw.api.domain.covterm.CovTerm) {
      setRequireValue("term", 0, $arg)
    }
    
    
    function isRetroDatetimeFieldsEditable(): boolean {
      var ereAdditionalCoveragesEffExpDates = {"GLCybEREEffectiveDate_TDIC", "GLCybEREExpirationDate_TDIC", "GLERPDLEffDate_TDIC",
          "GLERPDLExpDate_TDIC", "GLERPEEffectiveDate_TDIC", "GLERPEExpirationDate_TDIC"}
      if(ereAdditionalCoveragesEffExpDates.contains(term.Pattern.CodeIdentifier)) {
        return false
      }
      if (term.Pattern.CodeIdentifier == "GLNTCExpDate_TDIC") { // Original condition in the field of Editible property
        return false
      }
    
      var period = coverable.PolicyLine.Branch
    
      if (term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC") {
        if (period.GLLineExists) {
          if ((period.Job.Subtype == typekey.Job.TC_SUBMISSION and period.Status == PolicyPeriodStatus.TC_DRAFT) or period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
            return true
          } else if (period.Job.Subtype == typekey.Job.TC_RENEWAL and period.Status == PolicyPeriodStatus.TC_DRAFT) {
            if (term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC" and period.GLLine.BasedOn?.GLDentalEmpPracLiabCov_TDICExists) {
              return false
            }
          }
        }
      }
      // Defect, GPC-3486: BR-034 - Added Requiremen: Prior Acts Retroactive date shall be read-only for all transactions other than New Submission
      if (period.GLLineExists and period.GLLine.GLPriorActsCov_TDICExists) {
        if (term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC") {
          if ((period.Job.Subtype == typekey.Job.TC_SUBMISSION and period.Status == PolicyPeriodStatus.TC_DRAFT)) {
            return true
          } else { //if (period.Job.Subtype == typekey.Job.TC_RENEWAL and period.Status == PolicyPeriodStatus.TC_DRAFT) {
            return false
          }
        }
      }
    
      return true
    }
    /*
    function validateRetroDateforDEPL(retroDate : Date) : String {
      if (Period.GLLineExists) {
        var TransEffDate = Period.EditEffectiveDate
        if (Period.GLLine.GLDentalEmpPracLiabCov_TDICExists and retroDate != null) {
          var _01_01_1998 = DateUtil.createDateInstance(1, 1, 1998)
          var _07_01_1998 = DateUtil.createDateInstance(7, 1, 1998)
          var _07_01_1998_States = {Jurisdiction.TC_CA, Jurisdiction.TC_AZ, Jurisdiction.TC_NV}
          
          if (_07_01_1998_States.contains(Period.BaseState) and retroDate.before(_07_01_1998)) {
            return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _07_01_1998, Period.BaseState)
          } else if (retroDate.before(_01_01_1998)) {
            return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _01_01_1998, Period.BaseState)
          }
          if (retroDate.after(TransEffDate)) {
            return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.AfterDate")
          }
        }
      }
      return null
    }
    */
      function validateRetroDateforPriorActs(retroDate : Date) : String{
        if(Period.GLLine.GLPriorActsCov_TDICExists and retroDate != null){
          var PolicyEffDate = Period.PeriodStart
          var _07_01_1984 = DateUtil.createDateInstance(7, 1, 1984)
          if(retroDate.before(_07_01_1984)){
            return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLPriorActsCovTDIC.BeforeDate")
          }
          if(retroDate.after(PolicyEffDate)){
            return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLPriorActsCovTDIC.AfterDate")
          }
        }
      return null
    }
    
    function dateVisibility(): boolean{
     return !(term.Pattern.CodeIdentifier == "GLERPEExpirationDate_TDIC" or term.Pattern.CodeIdentifier == "GLCybEREExpirationDate_TDIC" or 
         term.Pattern.CodeIdentifier == "GLERPDLExpDate_TDIC") and !(term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC" and 
         (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT)) and !(term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC")
    }
    
    
  }
  
  
}