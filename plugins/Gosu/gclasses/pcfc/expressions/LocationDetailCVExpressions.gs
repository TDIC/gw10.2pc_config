package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/common/LocationDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LocationDetailCVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/common/LocationDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationDetailCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SynchronizedLink) at LocationDetailCV.pcf: line 43, column 101
    function action_12 () : void {
      AccountLocationPopup.push(polLocation.AccountLocation, polLocation.Branch.Policy.Account, false)
    }
    
    // 'action' attribute on Link (id=SynchronizedLink) at LocationDetailCV.pcf: line 43, column 101
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountLocationPopup.createDestination(polLocation.AccountLocation, polLocation.Branch.Policy.Account, false)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 23, column 98
    function def_onEnter_0 (def :  pcf.LocationDetailInputSet) : void {
      def.onEnter(polLocation, supportsNonSpecificLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_onEnter_3 (def :  pcf.LineLocationDetailInputSet_BusinessOwners) : void {
      def.onEnter(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_onEnter_5 (def :  pcf.LineLocationDetailInputSet_WC7WorkersComp) : void {
      def.onEnter(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_onEnter_7 (def :  pcf.LineLocationDetailInputSet_WorkersComp) : void {
      def.onEnter(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_onEnter_9 (def :  pcf.LineLocationDetailInputSet_default) : void {
      def.onEnter(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 23, column 98
    function def_refreshVariables_1 (def :  pcf.LocationDetailInputSet) : void {
      def.refreshVariables(polLocation, supportsNonSpecificLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_refreshVariables_10 (def :  pcf.LineLocationDetailInputSet_default) : void {
      def.refreshVariables(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_refreshVariables_4 (def :  pcf.LineLocationDetailInputSet_BusinessOwners) : void {
      def.refreshVariables(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_refreshVariables_6 (def :  pcf.LineLocationDetailInputSet_WC7WorkersComp) : void {
      def.refreshVariables(polLocation, openForEdit)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function def_refreshVariables_8 (def :  pcf.LineLocationDetailInputSet_WorkersComp) : void {
      def.refreshVariables(polLocation, openForEdit)
    }
    
    // 'mode' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function mode_11 () : java.lang.Object {
      return polLocation.Branch.Policy.Product.PublicID
    }
    
    // 'visible' attribute on DetailViewPanel (id=AccountLocationUpToDateDV) at LocationDetailCV.pcf: line 34, column 77
    function visible_14 () : java.lang.Boolean {
      return polLocation.Branch.Promoted and !polLocation.isUpToDate()
    }
    
    // 'visible' attribute on InputSetRef at LocationDetailCV.pcf: line 29, column 57
    function visible_2 () : java.lang.Boolean {
      return polLocation.Branch.BOPLineExists
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get polLocation () : PolicyLocation {
      return getRequireValue("polLocation", 0) as PolicyLocation
    }
    
    property set polLocation ($arg :  PolicyLocation) {
      setRequireValue("polLocation", 0, $arg)
    }
    
    property get supportsNonSpecificLocation () : boolean {
      return getRequireValue("supportsNonSpecificLocation", 0) as java.lang.Boolean
    }
    
    property set supportsNonSpecificLocation ($arg :  boolean) {
      setRequireValue("supportsNonSpecificLocation", 0, $arg)
    }
    
    
  }
  
  
}