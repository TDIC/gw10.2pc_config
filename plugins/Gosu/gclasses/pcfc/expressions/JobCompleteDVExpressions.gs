package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/JobCompleteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class JobCompleteDVExpressions {
  @javax.annotation.Generated("config/web/pcf/job/JobCompleteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends JobCompleteDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on BulletPointTextInput (id=ViewPreemptedPolicy_Input) at JobCompleteDV.pcf: line 51, column 44
    function actionAvailable_17 () : java.lang.Boolean {
      return (User.util.CurrentUser as User).canView( preemptedJob )
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewPreemptedPolicy_Input) at JobCompleteDV.pcf: line 51, column 44
    function action_15 () : void {
      JobForward.go(preemptedJob)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewPreemptedPolicy_Input) at JobCompleteDV.pcf: line 51, column 44
    function action_dest_16 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(preemptedJob)
    }
    
    // 'value' attribute on BulletPointTextInput (id=ViewPreemptedPolicy_Input) at JobCompleteDV.pcf: line 51, column 44
    function value_18 () : java.lang.String {
      return DisplayKey.get("Web.JobComplete.PreemptedMessage", job.DisplayType, preemptedJob.DisplayType, preemptedJob.JobNumber)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ViewPreemptedPolicy_Input) at JobCompleteDV.pcf: line 51, column 44
    function visible_14 () : java.lang.Boolean {
      return policyPeriod.Promoted
    }
    
    property get preemptedJob () : entity.Job {
      return getIteratedValue(1) as entity.Job
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/JobCompleteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class JobCompleteDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on BulletPointTextInput (id=EndorsementReminder_TDIC_Input) at JobCompleteDV.pcf: line 93, column 26
    function actionAvailable_50 () : java.lang.Boolean {
      return pcfActualImpactedPolicyNumbers != null and pcfActualImpactedPolicyNumbers.length > 0
    }
    
    // 'action' attribute on BulletPointTextInput (id=ResolveWithFutureUnboundPeriods_Input) at JobCompleteDV.pcf: line 41, column 93
    function action_10 () : void {
      gw.api.web.job.JobWizardHelper.applyChangesToFutureUnboundRenewal(policyPeriod)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewJob_Input) at JobCompleteDV.pcf: line 57, column 69
    function action_23 () : void {
      JobForward.go(job)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewPolicy_Input) at JobCompleteDV.pcf: line 62, column 72
    function action_29 () : void {
      PolicyFileForward.go(policyPeriod.PolicyNumber, policyPeriod.Policy.LastViewableBoundPeriod.EditEffectiveDate)
    }
    
    // 'action' attribute on BulletPointTextInput (id=SubmissionManager_Input) at JobCompleteDV.pcf: line 67, column 83
    function action_35 () : void {
      SubmissionManager.go(job.Policy.Account)
    }
    
    // 'action' attribute on BulletPointTextInput (id=SubmitAnother_Input) at JobCompleteDV.pcf: line 72, column 75
    function action_39 () : void {
      NewSubmission.push()
    }
    
    // 'action' attribute on BulletPointTextInput (id=ReviewChanges_Input) at JobCompleteDV.pcf: line 77, column 75
    function action_43 () : void {
      JobForward.go(job, policyPeriod)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ReturnToDesktop_Input) at JobCompleteDV.pcf: line 81, column 85
    function action_46 () : void {
      Desktop.go()
    }
    
    // 'action' attribute on BulletPointTextInput (id=EndorsementReminder_TDIC_Input) at JobCompleteDV.pcf: line 93, column 26
    function action_49 () : void {
      pcfPolicyPeriodDiffPlugin.PolicyPeriodDiffPlugin_generateEndorsementReminderActivity_TDIC(policyPeriod, pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC, pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC);DesktopActivities.go() 
    }
    
    // 'action' attribute on BulletPointTextInput (id=ResolveWithFutureBoundPeriods_Input) at JobCompleteDV.pcf: line 35, column 91
    function action_5 () : void {
      gw.api.web.job.JobWizardHelper.applyChangesToFutureBoundRenewal(policyPeriod)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewJob_Input) at JobCompleteDV.pcf: line 57, column 69
    function action_dest_24 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(job)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ViewPolicy_Input) at JobCompleteDV.pcf: line 62, column 72
    function action_dest_30 () : pcf.api.Destination {
      return pcf.PolicyFileForward.createDestination(policyPeriod.PolicyNumber, policyPeriod.Policy.LastViewableBoundPeriod.EditEffectiveDate)
    }
    
    // 'action' attribute on BulletPointTextInput (id=SubmissionManager_Input) at JobCompleteDV.pcf: line 67, column 83
    function action_dest_36 () : pcf.api.Destination {
      return pcf.SubmissionManager.createDestination(job.Policy.Account)
    }
    
    // 'action' attribute on BulletPointTextInput (id=SubmitAnother_Input) at JobCompleteDV.pcf: line 72, column 75
    function action_dest_40 () : pcf.api.Destination {
      return pcf.NewSubmission.createDestination()
    }
    
    // 'action' attribute on BulletPointTextInput (id=ReviewChanges_Input) at JobCompleteDV.pcf: line 77, column 75
    function action_dest_44 () : pcf.api.Destination {
      return pcf.JobForward.createDestination(job, policyPeriod)
    }
    
    // 'action' attribute on BulletPointTextInput (id=ReturnToDesktop_Input) at JobCompleteDV.pcf: line 81, column 85
    function action_dest_47 () : pcf.api.Destination {
      return pcf.Desktop.createDestination()
    }
    
    // 'initialValue' attribute on Variable at JobCompleteDV.pcf: line 16, column 58
    function initialValue_0 () : gw.plugin.diff.impl.PolicyPeriodDiffPlugin {
      return new gw.plugin.diff.impl.PolicyPeriodDiffPlugin()
    }
    
    // 'initialValue' attribute on Variable at JobCompleteDV.pcf: line 20, column 33
    function initialValue_1 () : java.util.HashMap {
      return pcfPolicyPeriodDiffPlugin.PolicyPeriodDiffPlugin_locateQualifyingChangesOnThisJob_TDIC(policyPeriod)
    }
    
    // 'initialValue' attribute on Variable at JobCompleteDV.pcf: line 24, column 53
    function initialValue_2 () : java.util.HashSet<PolicyPeriod> {
      return pcfPolicyPeriodDiffPlugin.PolicyPeriodDiffPlugin_locateImpactedPoliciesWithEligibleNodesAndFields_TDIC(policyPeriod, pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC)
    }
    
    // 'initialValue' attribute on Variable at JobCompleteDV.pcf: line 28, column 22
    function initialValue_3 () : String {
      return pcfPolicyPeriodDiffPlugin.getImpactedPoliciesFromPolicyPeriods_TDIC(pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC)
    }
    
    // 'label' attribute on Label (id=Test) at JobCompleteDV.pcf: line 85, column 205
    function label_48 () : java.lang.String {
      return "Impacted Policies" + (pcfActualImpactedPolicyNumbers != null and pcfActualImpactedPolicyNumbers.length > 0 ? ": " + pcfActualImpactedPolicyNumbers : "")
    }
    
    // 'value' attribute on BulletPointTextInput (id=ResolveWithFutureUnboundPeriods_Input) at JobCompleteDV.pcf: line 41, column 93
    function value_11 () : java.lang.String {
      return DisplayKey.get("Web.JobComplete.ResolveWithFutureUnboundPeriods", job.DisplayType)
    }
    
    // 'value' attribute on InputIterator at JobCompleteDV.pcf: line 45, column 34
    function value_21 () : entity.Job[] {
      return policyPeriod.PreemptedJobsIfBoundNow
    }
    
    // 'value' attribute on BulletPointTextInput (id=ViewJob_Input) at JobCompleteDV.pcf: line 57, column 69
    function value_25 () : java.lang.String {
      return gw.web.job.JobUIHelper.viewJobText(job)
    }
    
    // 'value' attribute on BulletPointTextInput (id=ViewPolicy_Input) at JobCompleteDV.pcf: line 62, column 72
    function value_31 () : java.lang.String {
      return DisplayKey.get("Web.SubmissionComplete.ViewPolicy", policyPeriod.PolicyNumberDisplayString)
    }
    
    // 'value' attribute on BulletPointTextInput (id=ResolveWithFutureBoundPeriods_Input) at JobCompleteDV.pcf: line 35, column 91
    function value_6 () : java.lang.String {
      return DisplayKey.get("Web.JobComplete.ResolveWithFutureBoundPeriods", job.DisplayType)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ViewJob_Input) at JobCompleteDV.pcf: line 57, column 69
    function visible_22 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showViewJob(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ViewPolicy_Input) at JobCompleteDV.pcf: line 62, column 72
    function visible_28 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showViewPolicy(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=SubmissionManager_Input) at JobCompleteDV.pcf: line 67, column 83
    function visible_34 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showGoToSubmissionManager(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=SubmitAnother_Input) at JobCompleteDV.pcf: line 72, column 75
    function visible_38 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showSubmitAnother(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ResolveWithFutureBoundPeriods_Input) at JobCompleteDV.pcf: line 35, column 91
    function visible_4 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showResolveWithFutureBoundPeriods(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ReviewChanges_Input) at JobCompleteDV.pcf: line 77, column 75
    function visible_42 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showReviewChanges(policyPeriod)
    }
    
    // 'visible' attribute on BulletPointTextInput (id=ResolveWithFutureUnboundPeriods_Input) at JobCompleteDV.pcf: line 41, column 93
    function visible_9 () : java.lang.Boolean {
      return gw.web.job.JobUIHelper.showResolveWithFutureUnboundPeriods(policyPeriod)
    }
    
    property get job () : Job {
      return getRequireValue("job", 0) as Job
    }
    
    property set job ($arg :  Job) {
      setRequireValue("job", 0, $arg)
    }
    
    property get pcfActualImpactedPolicyNumbers () : String {
      return getVariableValue("pcfActualImpactedPolicyNumbers", 0) as String
    }
    
    property set pcfActualImpactedPolicyNumbers ($arg :  String) {
      setVariableValue("pcfActualImpactedPolicyNumbers", 0, $arg)
    }
    
    property get pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC () : java.util.HashSet<PolicyPeriod> {
      return getVariableValue("pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC", 0) as java.util.HashSet<PolicyPeriod>
    }
    
    property set pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC ($arg :  java.util.HashSet<PolicyPeriod>) {
      setVariableValue("pcfImpactedPoliciesThatNeedImmediateEndorsement_TDIC", 0, $arg)
    }
    
    property get pcfPolicyPeriodDiffPlugin () : gw.plugin.diff.impl.PolicyPeriodDiffPlugin {
      return getVariableValue("pcfPolicyPeriodDiffPlugin", 0) as gw.plugin.diff.impl.PolicyPeriodDiffPlugin
    }
    
    property set pcfPolicyPeriodDiffPlugin ($arg :  gw.plugin.diff.impl.PolicyPeriodDiffPlugin) {
      setVariableValue("pcfPolicyPeriodDiffPlugin", 0, $arg)
    }
    
    property get pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC () : java.util.HashMap {
      return getVariableValue("pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC", 0) as java.util.HashMap
    }
    
    property set pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC ($arg :  java.util.HashMap) {
      setVariableValue("pcfQualifyingChangedNodesAndFieldsThatMayImpactOtherPolicies_TDIC", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}