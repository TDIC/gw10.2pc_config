package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/entrypoints/TDICPolicyTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDICPolicyTransactionEntryPointExpressions {
  @javax.annotation.Generated("config/web/pcf/entrypoints/TDICPolicyTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDICPolicyTransactionExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on EntryPoint (id=TDICPolicyTransaction) at TDICPolicyTransaction.pcf: line 8, column 68
    function location_0 () : pcf.api.Destination {
      return pcf.TDIC_PolicyTransactionForward.createDestination(offerNumber,jobIndex)
    }
    
    property get jobIndex () : int {
      return getVariableValue("jobIndex", 0) as java.lang.Integer
    }
    
    property set jobIndex ($arg :  int) {
      setVariableValue("jobIndex", 0, $arg)
    }
    
    property get offerNumber () : String {
      return getVariableValue("offerNumber", 0) as String
    }
    
    property set offerNumber ($arg :  String) {
      setVariableValue("offerNumber", 0, $arg)
    }
    
    
  }
  
  
}