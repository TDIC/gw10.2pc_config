package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/coverage/AdditionalInterestInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdditionalInterestInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/coverage/AdditionalInterestInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdditionalInterestInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'confirmMessage' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function confirmMessage_3 () : java.lang.String {
      return displayable.ConfirmMessage
    }
    
    // 'value' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      displayable.Value = (__VALUE_TO_SET as entity.AddlInterestDetail)
    }
    
    // 'editable' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function editable_0 () : java.lang.Boolean {
      return displayable.Editable
    }
    
    // 'label' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function label_4 () : java.lang.Object {
      return displayable.Label
    }
    
    // 'onChange' attribute on PostOnChange at AdditionalInterestInputSet.pcf: line 39, column 50
    function onChange_15 () : void {
      displayable.onChange(wizard)
    }
    
    // 'required' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function required_6 () : java.lang.Boolean {
      return displayable.Required
    }
    
    // 'showConfirmMessage' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function showConfirmMessage_5 () : java.lang.Boolean {
      return displayable.ShowConfirmMessage
    }
    
    // 'validationExpression' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function validationExpression_1 () : java.lang.Object {
      return displayable.ValidateExpression
    }
    
    // 'value' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function valueRoot_9 () : java.lang.Object {
      return displayable
    }
    
    // 'value' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function value_7 () : entity.AddlInterestDetail {
      return displayable.Value
    }
    
    // 'visible' attribute on TextInput (id=FieldDependencyDisplayable_Input) at AdditionalInterestInputSet.pcf: line 37, column 62
    function visible_18 () : java.lang.Boolean {
      return fieldDependencyInput and displayable.Visible
    }
    
    // 'visible' attribute on TextInput (id=Displayable_Input) at AdditionalInterestInputSet.pcf: line 26, column 67
    function visible_2 () : java.lang.Boolean {
      return not fieldDependencyInput and displayable.Visible
    }
    
    property get displayable () : gw.lob.common.displayable.Displayable<AddlInterestDetail> {
      return getRequireValue("displayable", 0) as gw.lob.common.displayable.Displayable<AddlInterestDetail>
    }
    
    property set displayable ($arg :  gw.lob.common.displayable.Displayable<AddlInterestDetail>) {
      setRequireValue("displayable", 0, $arg)
    }
    
    property get fieldDependencyInput () : boolean {
      return getRequireValue("fieldDependencyInput", 0) as java.lang.Boolean
    }
    
    property set fieldDependencyInput ($arg :  boolean) {
      setRequireValue("fieldDependencyInput", 0, $arg)
    }
    
    property get wizard () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("wizard", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set wizard ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("wizard", 0, $arg)
    }
    
    
  }
  
  
}