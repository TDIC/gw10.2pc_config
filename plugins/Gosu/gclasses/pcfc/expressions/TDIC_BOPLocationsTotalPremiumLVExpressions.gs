package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationsTotalPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_BOPLocationsTotalPremiumLVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/TDIC_BOPLocationsTotalPremiumLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_BOPLocationsTotalPremiumLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=premiumAmount_Cell) at TDIC_BOPLocationsTotalPremiumLV.pcf: line 27, column 23
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return totalLocationPremium
    }
    
    property get totalLocationPremium () : gw.pl.currency.MonetaryAmount {
      return getRequireValue("totalLocationPremium", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalLocationPremium ($arg :  gw.pl.currency.MonetaryAmount) {
      setRequireValue("totalLocationPremium", 0, $arg)
    }
    
    
  }
  
  
}