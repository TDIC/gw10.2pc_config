package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lob.wc7.WC7ClassCodeSearchCriteria
@javax.annotation.Generated("config/web/pcf/admin/forms/WC7MoreFormClassCodesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7MoreFormClassCodesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/forms/WC7MoreFormClassCodesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7MoreFormClassCodesPopup.pcf: line 82, column 49
    function valueRoot_16 () : java.lang.Object {
      return wc7ClassCode
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7MoreFormClassCodesPopup.pcf: line 82, column 49
    function value_15 () : java.lang.String {
      return wc7ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7MoreFormClassCodesPopup.pcf: line 88, column 49
    function value_18 () : java.lang.String {
      return wc7ClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisidiction_Cell) at WC7MoreFormClassCodesPopup.pcf: line 94, column 53
    function value_21 () : typekey.Jurisdiction {
      return wc7ClassCode.Jurisdiction
    }
    
    property get wc7ClassCode () : entity.WC7ClassCode {
      return getIteratedValue(2) as entity.WC7ClassCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/forms/WC7MoreFormClassCodesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends WC7MoreFormClassCodesPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AddSelectedClassCodes) at WC7MoreFormClassCodesPopup.pcf: line 63, column 99
    function allCheckedRowsAction_10 (CheckedValues :  entity.WC7ClassCode[], CheckedValuesErrors :  java.util.Map) : void {
      addWC7ClassCodes(CheckedValues)
    }
    
    // 'def' attribute on InputSetRef at WC7MoreFormClassCodesPopup.pcf: line 51, column 49
    function def_onEnter_8 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at WC7MoreFormClassCodesPopup.pcf: line 51, column 49
    function def_refreshVariables_9 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7MoreFormClassCodesPopup.pcf: line 40, column 47
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Classification_Input) at WC7MoreFormClassCodesPopup.pcf: line 47, column 47
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Classification = (__VALUE_TO_SET as java.lang.String)
    }
    
    // EditButtons at WC7MoreFormClassCodesPopup.pcf: line 65, column 38
    function label_11 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'searchCriteria' attribute on SearchPanel at WC7MoreFormClassCodesPopup.pcf: line 29, column 82
    function searchCriteria_26 () : gw.lob.wc7.WC7ClassCodeSearchCriteria {
      return createCriteria()
    }
    
    // 'search' attribute on SearchPanel at WC7MoreFormClassCodesPopup.pcf: line 29, column 82
    function search_25 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at WC7MoreFormClassCodesPopup.pcf: line 82, column 49
    function sortValue_12 (wc7ClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wc7ClassCode.Code
    }
    
    // 'value' attribute on TextCell (id=Classification_Cell) at WC7MoreFormClassCodesPopup.pcf: line 88, column 49
    function sortValue_13 (wc7ClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wc7ClassCode.Classification
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisidiction_Cell) at WC7MoreFormClassCodesPopup.pcf: line 94, column 53
    function sortValue_14 (wc7ClassCode :  entity.WC7ClassCode) : java.lang.Object {
      return wc7ClassCode.Jurisdiction
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7MoreFormClassCodesPopup.pcf: line 40, column 47
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at WC7MoreFormClassCodesPopup.pcf: line 40, column 47
    function value_0 () : java.lang.String {
      return searchCriteria.Code
    }
    
    // 'value' attribute on RowIterator (id=wc7ClassCodeIterator) at WC7MoreFormClassCodesPopup.pcf: line 76, column 87
    function value_24 () : gw.api.database.IQueryBeanResult<entity.WC7ClassCode> {
      return wc7ClassCodes
    }
    
    // 'value' attribute on TextInput (id=Classification_Input) at WC7MoreFormClassCodesPopup.pcf: line 47, column 47
    function value_4 () : java.lang.String {
      return searchCriteria.Classification
    }
    
    property get searchCriteria () : gw.lob.wc7.WC7ClassCodeSearchCriteria {
      return getCriteriaValue(1) as gw.lob.wc7.WC7ClassCodeSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.lob.wc7.WC7ClassCodeSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get wc7ClassCodes () : gw.api.database.IQueryBeanResult<WC7ClassCode> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<WC7ClassCode>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/forms/WC7MoreFormClassCodesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7MoreFormClassCodesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (formPattern :  FormPattern, formClassCodes :  WC7ClassCode[]) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.WC7MoreFormClassCodesPopup {
      return super.CurrentLocation as pcf.WC7MoreFormClassCodesPopup
    }
    
    property get formClassCodes () : WC7ClassCode[] {
      return getVariableValue("formClassCodes", 0) as WC7ClassCode[]
    }
    
    property set formClassCodes ($arg :  WC7ClassCode[]) {
      setVariableValue("formClassCodes", 0, $arg)
    }
    
    property get formPattern () : FormPattern {
      return getVariableValue("formPattern", 0) as FormPattern
    }
    
    property set formPattern ($arg :  FormPattern) {
      setVariableValue("formPattern", 0, $arg)
    }
    
    
    function addWC7ClassCodes(codes : WC7ClassCode[]) {
      var addedFormPatternClassCodes = formPattern.WC7FormPatternClassCodes
      for (classCode in codes) {
        if(!addedFormPatternClassCodes?.hasMatch(\ fpcc -> fpcc.Code == classCode.Code and fpcc.Classification == classCode.Classification and fpcc.Jurisdiction == classCode.Jurisdiction)) {
          var formPatternClassCode = new WC7FormPatternClassCode()
          formPatternClassCode.Code = classCode.Code
          formPatternClassCode.Classification = classCode.Classification
          formPatternClassCode.Jurisdiction = classCode.Jurisdiction
          formPattern.addToWC7FormPatternClassCodes(formPatternClassCode)
        }
      }
      CurrentLocation.commit()
    }
    
    function createCriteria() : WC7ClassCodeSearchCriteria {
      var criteria = new WC7ClassCodeSearchCriteria()
      return criteria
    }
    
    
  }
  
  
}