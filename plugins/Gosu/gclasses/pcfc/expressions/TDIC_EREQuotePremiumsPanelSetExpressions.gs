package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuotePremiumsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_EREQuotePremiumsPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuotePremiumsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_EREQuotePremiumsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 154, column 41
    function currency_52 () : typekey.Currency {
      return gw.api.util.CurrencyUtil.DefaultCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 146, column 47
    function valueRoot_48 () : java.lang.Object {
      return stdWrapper
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 146, column 47
    function value_47 () : java.lang.String {
      return stdWrapper.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 154, column 41
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return stdWrapper.Total
    }
    
    property get stdWrapper () : gw.api.ui.GL_CostWrapper {
      return getIteratedValue(1) as gw.api.ui.GL_CostWrapper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuotePremiumsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_EREQuotePremiumsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 182, column 54
    function currency_64 () : typekey.Currency {
      return gw.api.util.CurrencyUtil.DefaultCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 174, column 50
    function valueRoot_60 () : java.lang.Object {
      return surchargeCost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 174, column 50
    function value_59 () : java.lang.String {
      return surchargeCost.PremiumType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 182, column 54
    function value_62 () : gw.pl.currency.MonetaryAmount {
      return surchargeCost.ProratedPremium
    }
    
    property get surchargeCost () : GLEREQuickQuoteCost_TDIC {
      return getIteratedValue(1) as GLEREQuickQuoteCost_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuotePremiumsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_EREQuotePremiumsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TermPremiumCov_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 110, column 41
    function currency_34 () : typekey.Currency {
      return gw.api.util.CurrencyUtil.DefaultCurrency
    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 80, column 41
    function valueRoot_6 () : java.lang.Object {
      return qqCost
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 86, column 43
    function value_11 () : java.util.Date {
      return qqCost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirDate_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 89, column 44
    function value_14 () : java.util.Date {
      return qqCost.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=classCode_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 92, column 39
    function value_17 () : java.lang.String {
      return qqCost.ClassCode
    }
    
    // 'value' attribute on TextCell (id=territoryCode_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 95, column 43
    function value_20 () : java.lang.String {
      return qqCost.TerritoryCode
    }
    
    // 'value' attribute on TextCell (id=specialityCode_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 98, column 43
    function value_23 () : java.lang.String {
      return qqCost.SpecialtyCode
    }
    
    // 'value' attribute on TextCell (id=discount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 101, column 38
    function value_26 () : java.lang.String {
      return qqCost.Discount
    }
    
    // 'value' attribute on TextCell (id=StepYearCov_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 104, column 38
    function value_29 () : java.lang.String {
      return qqCost.StepYear
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TermPremiumCov_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 110, column 41
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return qqCost.TermPremium
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscountCov_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 116, column 45
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return qqCost.ProratedPremium
    }
    
    // 'value' attribute on TextCell (id=premiumType_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 80, column 41
    function value_5 () : java.lang.String {
      return qqCost.PremiumType
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 83, column 35
    function value_8 () : java.lang.String {
      return qqCost.Limit
    }
    
    property get qqCost () : GLEREQuickQuoteCost_TDIC {
      return getIteratedValue(1) as GLEREQuickQuoteCost_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/job/ere_tdic/TDIC_EREQuotePremiumsPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_EREQuotePremiumsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at TDIC_EREQuotePremiumsPanelSet.pcf: line 14, column 42
    function initialValue_0 () : GLEREQuickQuoteCost_TDIC[] {
      return quote != null ? quote.Costs.where(\elt -> elt.PremiumType == "Professional Liability") : {}
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_EREQuotePremiumsPanelSet.pcf: line 72, column 26
    function sortBy_3 (qqCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return qqCost.PremiumOrder
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_EREQuotePremiumsPanelSet.pcf: line 76, column 26
    function sortBy_4 (qqCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return qqCost.EffectiveDate
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_EREQuotePremiumsPanelSet.pcf: line 140, column 28
    function sortBy_42 (stdWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stdWrapper.Order
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 146, column 47
    function sortValue_43 (stdWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stdWrapper.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 154, column 41
    function sortValue_44 (stdWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stdWrapper.Total
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 174, column 50
    function sortValue_55 (surchargeCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return surchargeCost.PremiumType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_EREQuotePremiumsPanelSet.pcf: line 182, column 54
    function sortValue_56 (surchargeCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return surchargeCost.ProratedPremium
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 154, column 41
    function sumValueRoot_46 (stdWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stdWrapper
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 182, column 54
    function sumValueRoot_58 (surchargeCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return surchargeCost
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 154, column 41
    function sumValue_45 (stdWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stdWrapper.Total
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 182, column 54
    function sumValue_57 (surchargeCost :  GLEREQuickQuoteCost_TDIC) : java.lang.Object {
      return surchargeCost.ProratedPremium
    }
    
    // 'value' attribute on RowIterator (id=TermIterator) at TDIC_EREQuotePremiumsPanelSet.pcf: line 69, column 48
    function value_40 () : GLEREQuickQuoteCost_TDIC[] {
      return termCosts
    }
    
    // 'value' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 137, column 50
    function value_54 () : gw.api.ui.GL_CostWrapper[] {
      return tdic.pc.config.job.ere.GLEREQuoteUIHelper.getStandardCostWrappers(quote)
    }
    
    // 'value' attribute on RowIterator at TDIC_EREQuotePremiumsPanelSet.pcf: line 168, column 50
    function value_66 () : GLEREQuickQuoteCost_TDIC[] {
      return quote.Costs.where(\elt -> elt.RateAmountType == RateAmountType.TC_TAXSURCHARGE)
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_EREQuotePremiumsPanelSet.pcf: line 16, column 49
    function visible_1 () : java.lang.Boolean {
      return perm.System.viewquoteterms_tdic
    }
    
    property get quote () : GLEREQuickQuote_TDIC {
      return getRequireValue("quote", 0) as GLEREQuickQuote_TDIC
    }
    
    property set quote ($arg :  GLEREQuickQuote_TDIC) {
      setRequireValue("quote", 0, $arg)
    }
    
    property get termCosts () : GLEREQuickQuoteCost_TDIC[] {
      return getVariableValue("termCosts", 0) as GLEREQuickQuoteCost_TDIC[]
    }
    
    property set termCosts ($arg :  GLEREQuickQuoteCost_TDIC[]) {
      setVariableValue("termCosts", 0, $arg)
    }
    
    
  }
  
  
}