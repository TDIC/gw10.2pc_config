package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7SupplementaryDiseaseInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WC7SupplementaryDiseaseInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7SupplementaryDiseaseInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 173, column 87
    function available_68 () : java.lang.Boolean {
      return !wc7SupplDiseaseExp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 161, column 78
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp2.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 173, column 87
    function defaultSetter_71 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp2.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function valueRange_51 () : java.lang.Object {
      return wcLine.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(selectedJurisdiction.Jurisdiction))
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function valueRange_57 () : java.lang.Object {
      return diseaseCodesForJurisdiction
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function valueRoot_50 () : java.lang.Object {
      return wc7SupplDiseaseExp2
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 157, column 47
    function valueRoot_62 () : java.lang.Object {
      return wc7SupplDiseaseExp2.DiseaseCode
    }
    
    // 'value' attribute on TextCell (id=Period2_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 132, column 47
    function value_47 () : java.lang.String {
      return gw.api.web.util.DateRangeUtil.getPeriodNumbers(wc7SupplDiseaseExp2.EffectiveDateRange, splitDates)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function value_49 () : entity.PolicyLocation {
      return wc7SupplDiseaseExp2.LocationWM
    }
    
    // 'value' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function value_55 () : entity.WC7DiseaseCode {
      return wc7SupplDiseaseExp2.DiseaseCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 157, column 47
    function value_61 () : java.lang.String {
      return wc7SupplDiseaseExp2.DiseaseCode.SupplDiseaseLoadingType
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 161, column 78
    function value_64 () : java.lang.Boolean {
      return wc7SupplDiseaseExp2.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 173, column 87
    function value_70 () : java.lang.Integer {
      return wc7SupplDiseaseExp2.BasisAmount
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 179, column 33
    function value_75 () : java.util.Date {
      return wc7SupplDiseaseExp2.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 185, column 33
    function value_78 () : java.util.Date {
      return wc7SupplDiseaseExp2.ExpirationDate
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function verifyValueRangeIsAllowedType_52 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function verifyValueRangeIsAllowedType_52 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function verifyValueRangeIsAllowedType_52 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function verifyValueRangeIsAllowedType_58 ($$arg :  entity.WC7DiseaseCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function verifyValueRangeIsAllowedType_58 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7DiseaseCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function verifyValueRangeIsAllowedType_58 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 141, column 30
    function verifyValueRange_53 () : void {
      var __valueRangeArg = wcLine.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(selectedJurisdiction.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_52(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 148, column 51
    function verifyValueRange_59 () : void {
      var __valueRangeArg = diseaseCodesForJurisdiction
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_58(__valueRangeArg)
    }
    
    property get wc7SupplDiseaseExp2 () : entity.WC7SupplDiseaseExposure {
      return getIteratedValue(2) as entity.WC7SupplDiseaseExposure
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7SupplementaryDiseaseInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends WC7SupplementaryDiseaseInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 104, column 85
    function available_34 () : java.lang.Boolean {
      return !wc7SupplDiseaseExp1.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp1.LocationWM = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp1.DiseaseCode = (__VALUE_TO_SET as entity.WC7DiseaseCode)
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 91, column 76
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp1.IfAnyExposureAndClearBasisAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 104, column 85
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      wc7SupplDiseaseExp1.BasisAmount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function editable_11 () : java.lang.Boolean {
      return wc7SupplDiseaseExp1.canEditLocation(splitDates)
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function valueRange_15 () : java.lang.Object {
      return wcLine.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(selectedJurisdiction.Jurisdiction))
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function valueRange_23 () : java.lang.Object {
      return diseaseCodesForJurisdiction
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function valueRoot_14 () : java.lang.Object {
      return wc7SupplDiseaseExp1
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 86, column 45
    function valueRoot_28 () : java.lang.Object {
      return wc7SupplDiseaseExp1.DiseaseCode
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function value_12 () : entity.PolicyLocation {
      return wc7SupplDiseaseExp1.LocationWM
    }
    
    // 'value' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function value_20 () : entity.WC7DiseaseCode {
      return wc7SupplDiseaseExp1.DiseaseCode
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 86, column 45
    function value_27 () : java.lang.String {
      return wc7SupplDiseaseExp1.DiseaseCode.SupplDiseaseLoadingType
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 91, column 76
    function value_30 () : java.lang.Boolean {
      return wc7SupplDiseaseExp1.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on TextCell (id=AnnualRenum_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 104, column 85
    function value_36 () : java.lang.Integer {
      return wc7SupplDiseaseExp1.BasisAmount
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 111, column 31
    function value_41 () : java.util.Date {
      return wc7SupplDiseaseExp1.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 118, column 31
    function value_44 () : java.util.Date {
      return wc7SupplDiseaseExp1.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Period1_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 54, column 49
    function value_8 () : java.lang.String {
      return gw.api.web.util.DateRangeUtil.getPeriodNumbers(wc7SupplDiseaseExp1.EffectiveDateRange, splitDates)
    }
    
    // 'value' attribute on RowIterator (id=WC7SupplDiseaseExpList2) at WC7SupplementaryDiseaseInputSet.default.pcf: line 126, column 58
    function value_81 () : entity.WC7SupplDiseaseExposure[] {
      return wc7SupplDiseaseExp1.AdditionalVersions.whereTypeIs(WC7SupplDiseaseExposure)
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function verifyValueRangeIsAllowedType_16 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function verifyValueRangeIsAllowedType_16 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function verifyValueRangeIsAllowedType_24 ($$arg :  entity.WC7DiseaseCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function verifyValueRangeIsAllowedType_24 ($$arg :  gw.api.database.IQueryBeanResult<entity.WC7DiseaseCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function verifyValueRange_17 () : void {
      var __valueRangeArg = wcLine.Branch.getPolicyLocationWM(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(selectedJurisdiction.Jurisdiction))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function verifyValueRange_25 () : void {
      var __valueRangeArg = diseaseCodesForJurisdiction
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=Period1_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 54, column 49
    function visible_9 () : java.lang.Boolean {
      return splitDates.Count > 2
    }
    
    property get wc7SupplDiseaseExp1 () : entity.WC7SupplDiseaseExposure {
      return getIteratedValue(1) as entity.WC7SupplDiseaseExposure
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/wc7/policy/WC7SupplementaryDiseaseInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WC7SupplementaryDiseaseInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at WC7SupplementaryDiseaseInputSet.default.pcf: line 18, column 52
    function initialValue_0 () : java.util.List<java.util.Date> {
      return selectedJurisdiction.SplitDates
    }
    
    // 'initialValue' attribute on Variable at WC7SupplementaryDiseaseInputSet.default.pcf: line 22, column 59
    function initialValue_1 () : java.util.List<entity.WC7DiseaseCode> {
      return wcLine.getDiseaseCodesForJurisdiction(selectedJurisdiction.Jurisdiction, wcLine.Branch.PeriodStart)
    }
    
    // 'value' attribute on RangeCell (id=Loc_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 67, column 28
    function sortValue_3 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : java.lang.Object {
      return wc7SupplDiseaseExp1.LocationWM
    }
    
    // 'value' attribute on RangeCell (id=DiseaseCode_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 76, column 49
    function sortValue_4 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : java.lang.Object {
      return wc7SupplDiseaseExp1.DiseaseCode
    }
    
    // 'value' attribute on CheckBoxCell (id=IfAnyExposure_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 91, column 76
    function sortValue_5 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : java.lang.Object {
      return wc7SupplDiseaseExp1.IfAnyExposureAndClearBasisAmount
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 111, column 31
    function sortValue_6 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : java.lang.Object {
      return wc7SupplDiseaseExp1.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 118, column 31
    function sortValue_7 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : java.lang.Object {
      return wc7SupplDiseaseExp1.ExpirationDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=WC7SupplDiseaseExpList1) at WC7SupplementaryDiseaseInputSet.default.pcf: line 46, column 56
    function toCreateAndAdd_82 () : entity.WC7SupplDiseaseExposure {
      return wcLine.addSupplementaryDiseaseExposureWM()
    }
    
    // 'toRemove' attribute on RowIterator (id=WC7SupplDiseaseExpList1) at WC7SupplementaryDiseaseInputSet.default.pcf: line 46, column 56
    function toRemove_83 (wc7SupplDiseaseExp1 :  entity.WC7SupplDiseaseExposure) : void {
      wc7SupplDiseaseExp1.removeWM()
    }
    
    // 'value' attribute on RowIterator (id=WC7SupplDiseaseExpList1) at WC7SupplementaryDiseaseInputSet.default.pcf: line 46, column 56
    function value_84 () : entity.WC7SupplDiseaseExposure[] {
      return wcLine.getWC7SupplementaryDiseaseExposuresWM(selectedJurisdiction.Jurisdiction)
    }
    
    // 'visible' attribute on TextCell (id=Period1_Cell) at WC7SupplementaryDiseaseInputSet.default.pcf: line 54, column 49
    function visible_2 () : java.lang.Boolean {
      return splitDates.Count > 2
    }
    
    property get diseaseCodesForJurisdiction () : java.util.List<entity.WC7DiseaseCode> {
      return getVariableValue("diseaseCodesForJurisdiction", 0) as java.util.List<entity.WC7DiseaseCode>
    }
    
    property set diseaseCodesForJurisdiction ($arg :  java.util.List<entity.WC7DiseaseCode>) {
      setVariableValue("diseaseCodesForJurisdiction", 0, $arg)
    }
    
    property get selectedJurisdiction () : WC7Jurisdiction {
      return getRequireValue("selectedJurisdiction", 0) as WC7Jurisdiction
    }
    
    property set selectedJurisdiction ($arg :  WC7Jurisdiction) {
      setRequireValue("selectedJurisdiction", 0, $arg)
    }
    
    property get splitDates () : java.util.List<java.util.Date> {
      return getVariableValue("splitDates", 0) as java.util.List<java.util.Date>
    }
    
    property set splitDates ($arg :  java.util.List<java.util.Date>) {
      setVariableValue("splitDates", 0, $arg)
    }
    
    property get wcLine () : WC7WorkersCompLine {
      return getRequireValue("wcLine", 0) as WC7WorkersCompLine
    }
    
    property set wcLine ($arg :  WC7WorkersCompLine) {
      setRequireValue("wcLine", 0, $arg)
    }
    
    
  }
  
  
}