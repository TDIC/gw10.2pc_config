package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/GLRiskManagementDiscount_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GLRiskManagementDiscount_TDICDVExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLRiskManagementDiscount_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GLRiskManagementDiscount_TDICDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=updatefromEnroll) at GLRiskManagementDiscount_TDICDV.pcf: line 28, column 82
    function action_3 () : void {
      tdic.web.pcf.helper.JobWizardHelper.updateRiskManagementDiscount(glLine.Branch) //tdic.pc.common.batch.enrole.EnroleHelper.updateRiskManagementDiscount(glLine.Branch)
    }
    
    // 'initialValue' attribute on Variable at GLRiskManagementDiscount_TDICDV.pcf: line 13, column 22
    function initialValue_0 () : GLLine {
      return policyline as GLLine
    }
    
    // 'sortBy' attribute on IteratorSort at GLRiskManagementDiscount_TDICDV.pcf: line 42, column 30
    function sortBy_4 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.StartDate
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 78, column 44
    function sortValue_10 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.StartDate
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 89, column 44
    function sortValue_11 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.EndDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 48, column 54
    function sortValue_5 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.Name
    }
    
    // 'value' attribute on TextCell (id=Attended_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 53, column 58
    function sortValue_6 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.Attended
    }
    
    // 'value' attribute on DateCell (id=LPSDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 60, column 45
    function sortValue_7 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.LPSDate
    }
    
    // 'value' attribute on TextCell (id=LocationCode_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 65, column 62
    function sortValue_8 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.LocationCode
    }
    
    // 'value' attribute on TextCell (id=LocationName_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 70, column 62
    function sortValue_9 (riskManagementDiscount :  RiskManagementDisc_TDIC) : java.lang.Object {
      return riskManagementDiscount.LocationName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at GLRiskManagementDiscount_TDICDV.pcf: line 38, column 51
    function toCreateAndAdd_44 () : RiskManagementDisc_TDIC {
      var rm = glLine.createAndAddRiskManagement_TDIC(); tdic.web.pcf.helper.JobWizardHelper.toggleRiskManagement(glLine); return rm;
    }
    
    // 'toRemove' attribute on RowIterator at GLRiskManagementDiscount_TDICDV.pcf: line 38, column 51
    function toRemove_45 (riskManagementDiscount :  RiskManagementDisc_TDIC) : void {
      glLine.removeFromRiskManagementDisc_TDIC(riskManagementDiscount); tdic.web.pcf.helper.JobWizardHelper.toggleRiskManagement(glLine);
    }
    
    // 'value' attribute on RowIterator at GLRiskManagementDiscount_TDICDV.pcf: line 38, column 51
    function value_46 () : RiskManagementDisc_TDIC[] {
      return glLine.RiskManagementDisc_TDIC
    }
    
    // 'addVisible' attribute on IteratorButtons at GLRiskManagementDiscount_TDICDV.pcf: line 23, column 102
    function visible_1 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.Branch.Job typeis PolicyChange
    }
    
    // 'removeVisible' attribute on IteratorButtons at GLRiskManagementDiscount_TDICDV.pcf: line 23, column 102
    function visible_2 () : java.lang.Boolean {
      return glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal
    }
    
    property get glLine () : GLLine {
      return getVariableValue("glLine", 0) as GLLine
    }
    
    property set glLine ($arg :  GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get policyline () : PolicyLine {
      return getRequireValue("policyline", 0) as PolicyLine
    }
    
    property set policyline ($arg :  PolicyLine) {
      setRequireValue("policyline", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/GLRiskManagementDiscount_TDICDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GLRiskManagementDiscount_TDICDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 48, column 54
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Attended_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 53, column 58
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.Attended = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=LPSDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 60, column 45
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.LPSDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextCell (id=LocationCode_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 65, column 62
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.LocationCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=LocationName_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 70, column 62
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.LocationName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 78, column 44
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 89, column 44
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      riskManagementDiscount.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'onChange' attribute on PostOnChange at GLRiskManagementDiscount_TDICDV.pcf: line 80, column 96
    function onChange_32 () : void {
      tdic.web.pcf.helper.JobWizardHelper.toggleRiskManagement(glLine)
    }
    
    // 'onChange' attribute on PostOnChange at GLRiskManagementDiscount_TDICDV.pcf: line 91, column 96
    function onChange_38 () : void {
      tdic.web.pcf.helper.JobWizardHelper.toggleRiskManagement(glLine)
    }
    
    // 'required' attribute on DateCell (id=StartDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 78, column 44
    function required_33 () : java.lang.Boolean {
      return riskManagementDiscount.IsUserEntry
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 48, column 54
    function valueRoot_14 () : java.lang.Object {
      return riskManagementDiscount
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 48, column 54
    function value_12 () : java.lang.String {
      return riskManagementDiscount.Name
    }
    
    // 'value' attribute on TextCell (id=Attended_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 53, column 58
    function value_16 () : java.lang.String {
      return riskManagementDiscount.Attended
    }
    
    // 'value' attribute on DateCell (id=LPSDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 60, column 45
    function value_20 () : java.util.Date {
      return riskManagementDiscount.LPSDate
    }
    
    // 'value' attribute on TextCell (id=LocationCode_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 65, column 62
    function value_24 () : java.lang.String {
      return riskManagementDiscount.LocationCode
    }
    
    // 'value' attribute on TextCell (id=LocationName_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 70, column 62
    function value_28 () : java.lang.String {
      return riskManagementDiscount.LocationName
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 78, column 44
    function value_34 () : java.util.Date {
      return riskManagementDiscount.StartDate
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at GLRiskManagementDiscount_TDICDV.pcf: line 89, column 44
    function value_40 () : java.util.Date {
      return riskManagementDiscount.EndDate
    }
    
    property get riskManagementDiscount () : RiskManagementDisc_TDIC {
      return getIteratedValue(1) as RiskManagementDisc_TDIC
    }
    
    
  }
  
  
}