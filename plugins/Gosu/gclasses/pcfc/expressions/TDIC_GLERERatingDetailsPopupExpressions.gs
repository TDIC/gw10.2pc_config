package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLERERatingDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends SurchargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 141, column 51
    function currency_62 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 123, column 57
    function valueRoot_50 () : java.lang.Object {
      return surchargeWrapper
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 128, column 95
    function valueRoot_53 () : java.lang.Object {
      return surchargeWrapper.Cost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 123, column 57
    function value_49 () : java.lang.String {
      return surchargeWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 128, column 95
    function value_52 () : java.util.Date {
      return surchargeWrapper.Cost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 133, column 95
    function value_56 () : java.util.Date {
      return surchargeWrapper.Cost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 141, column 51
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return surchargeWrapper.Total
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 128, column 95
    function visible_54 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get surchargeWrapper () : gw.api.ui.GL_CostWrapper {
      return getIteratedValue(3) as gw.api.ui.GL_CostWrapper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends policyIteratorLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 99, column 51
    function currency_34 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 81, column 57
    function valueRoot_22 () : java.lang.Object {
      return stateCostWrapper
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 86, column 95
    function valueRoot_25 () : java.lang.Object {
      return stateCostWrapper.Cost
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 81, column 57
    function value_21 () : java.lang.String {
      return stateCostWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 86, column 95
    function value_24 () : java.util.Date {
      return stateCostWrapper.Cost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 91, column 95
    function value_28 () : java.util.Date {
      return stateCostWrapper.Cost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 99, column 51
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return stateCostWrapper.Total
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 86, column 95
    function visible_26 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get stateCostWrapper () : gw.api.ui.GL_CostWrapper {
      return getIteratedValue(3) as gw.api.ui.GL_CostWrapper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RatingCumulDetailsPanelSetExpressionsImpl extends TDIC_GLERERatingDetailsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 164, column 71
    function currency_66 () : typekey.Currency {
      return period.PreferredSettlementCurrency
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at TDIC_GLERERatingDetailsPopup.pcf: line 46, column 40
    function def_onEnter_4 (def :  pcf.RatingOverrideButtonDV) : void {
      def.onEnter(period, period.GLLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef at TDIC_GLERERatingDetailsPopup.pcf: line 49, column 62
    function def_onEnter_6 (def :  pcf.TDIC_GLTermsPanelSet) : void {
      def.onEnter(period, glCosts, true)
    }
    
    // 'def' attribute on PanelRef (id=RatingOverrideButtonDV) at TDIC_GLERERatingDetailsPopup.pcf: line 46, column 40
    function def_refreshVariables_5 (def :  pcf.RatingOverrideButtonDV) : void {
      def.refreshVariables(period, period.GLLine, jobWizardHelper, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on PanelRef at TDIC_GLERERatingDetailsPopup.pcf: line 49, column 62
    function def_refreshVariables_7 (def :  pcf.TDIC_GLTermsPanelSet) : void {
      def.refreshVariables(period, glCosts, true)
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 26, column 54
    function initialValue_0 () : java.util.Set<entity.GLCost> {
      return period.GLLine.Costs.cast(GLCost).toSet()
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 32, column 109
    function initialValue_1 () : java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>> {
      return glCosts.whereTypeIs(GLCovExposureCost).toSet().byFixedLocation()
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 36, column 98
    function initialValue_2 () : java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>> {
      return glCosts.whereTypeIs(GLCost)?.partition(\c -> c.State)
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 40, column 69
    function initialValue_3 () : java.util.Set<entity.GLHistoricalCost_TDIC> {
      return period.AllCosts.whereTypeIs(GLHistoricalCost_TDIC).toSet()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 164, column 71
    function value_65 () : gw.pl.currency.MonetaryAmount {
      return quoteScreenHelper.getTotalCost(period, true)
    }
    
    property get costsByState () : java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>> {
      return getVariableValue("costsByState", 1) as java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>>
    }
    
    property set costsByState ($arg :  java.util.Map<typekey.Jurisdiction, java.util.List<entity.GLCost>>) {
      setVariableValue("costsByState", 1, $arg)
    }
    
    property get glCosts () : java.util.Set<entity.GLCost> {
      return getVariableValue("glCosts", 1) as java.util.Set<entity.GLCost>
    }
    
    property set glCosts ($arg :  java.util.Set<entity.GLCost>) {
      setVariableValue("glCosts", 1, $arg)
    }
    
    property get glHistoricalCosts () : java.util.Set<entity.GLHistoricalCost_TDIC> {
      return getVariableValue("glHistoricalCosts", 1) as java.util.Set<entity.GLHistoricalCost_TDIC>
    }
    
    property set glHistoricalCosts ($arg :  java.util.Set<entity.GLHistoricalCost_TDIC>) {
      setVariableValue("glHistoricalCosts", 1, $arg)
    }
    
    property get locLevelCovCostMap () : java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>> {
      return getVariableValue("locLevelCovCostMap", 1) as java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>>
    }
    
    property set locLevelCovCostMap ($arg :  java.util.Map<entity.PolicyLocation, java.util.Set<entity.GLCovExposureCost>>) {
      setVariableValue("locLevelCovCostMap", 1, $arg)
    }
    
    property get quoteScreenHelper () : tdic.web.pcf.helper.GLQuoteScreenHelper {
      return getVariableValue("quoteScreenHelper", 1) as tdic.web.pcf.helper.GLQuoteScreenHelper
    }
    
    property set quoteScreenHelper ($arg :  tdic.web.pcf.helper.GLQuoteScreenHelper) {
      setVariableValue("quoteScreenHelper", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SurchargesLVExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 117, column 54
    function editable_48 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 112, column 50
    function initialValue_37 () : gw.api.ui.GL_CostWrapper[] {
      return quoteScreenHelper.getERETaxesAndSurchargeWrappers(glCosts)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 123, column 57
    function sortValue_38 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 128, column 95
    function sortValue_39 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper.Cost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 133, column 95
    function sortValue_41 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper.Cost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 141, column 51
    function sortValue_43 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper.Total
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 141, column 51
    function sumValueRoot_47 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 141, column 51
    function sumValue_46 (surchargeWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return surchargeWrapper.Total
    }
    
    // 'value' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 117, column 54
    function value_64 () : gw.api.ui.GL_CostWrapper[] {
      return surchargeWrappers
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 128, column 95
    function visible_40 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get surchargeWrappers () : gw.api.ui.GL_CostWrapper[] {
      return getVariableValue("surchargeWrappers", 2) as gw.api.ui.GL_CostWrapper[]
    }
    
    property set surchargeWrappers ($arg :  gw.api.ui.GL_CostWrapper[]) {
      setVariableValue("surchargeWrappers", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLERERatingDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (period :  PolicyPeriod, jobWizardHelper :  gw.api.web.job.JobWizardHelper, isEREJob :  boolean) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.TDIC_GLERERatingDetailsPopup {
      return super.CurrentLocation as pcf.TDIC_GLERERatingDetailsPopup
    }
    
    property get isEREJob () : boolean {
      return getVariableValue("isEREJob", 0) as java.lang.Boolean
    }
    
    property set isEREJob ($arg :  boolean) {
      setVariableValue("isEREJob", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getVariableValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setVariableValue("jobWizardHelper", 0, $arg)
    }
    
    property get period () : PolicyPeriod {
      return getVariableValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setVariableValue("period", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/TDIC_GLERERatingDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class policyIteratorLVExpressionsImpl extends RatingCumulDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 72, column 54
    function editable_20 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'initialValue' attribute on Variable at TDIC_GLERERatingDetailsPopup.pcf: line 66, column 50
    function initialValue_8 () : gw.api.ui.GL_CostWrapper[] {
      return quoteScreenHelper.getEREStandardPremiumWrappers(period)
    }
    
    // 'sortBy' attribute on IteratorSort at TDIC_GLERERatingDetailsPopup.pcf: line 75, column 32
    function sortBy_9 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Order
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 81, column 57
    function sortValue_10 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Description
    }
    
    // 'value' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 86, column 95
    function sortValue_11 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Cost.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=policyExpiratioDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 91, column 95
    function sortValue_13 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Cost.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProratedPremiumwithdiscount_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 99, column 51
    function sortValue_15 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Total
    }
    
    // '$$sumValue' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 99, column 51
    function sumValueRoot_19 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 99, column 51
    function sumValue_18 (stateCostWrapper :  gw.api.ui.GL_CostWrapper) : java.lang.Object {
      return stateCostWrapper.Total
    }
    
    // 'value' attribute on RowIterator at TDIC_GLERERatingDetailsPopup.pcf: line 72, column 54
    function value_36 () : gw.api.ui.GL_CostWrapper[] {
      return stateCostWrappers
    }
    
    // 'visible' attribute on DateCell (id=policyEffectiveDate_Cell) at TDIC_GLERERatingDetailsPopup.pcf: line 86, column 95
    function visible_12 () : java.lang.Boolean {
      return (period.Job typeis PolicyChange) and not period.isERERating_TDIC
    }
    
    property get stateCostWrappers () : gw.api.ui.GL_CostWrapper[] {
      return getVariableValue("stateCostWrappers", 2) as gw.api.ui.GL_CostWrapper[]
    }
    
    property set stateCostWrappers ($arg :  gw.api.ui.GL_CostWrapper[]) {
      setVariableValue("stateCostWrappers", 2, $arg)
    }
    
    
  }
  
  
}