package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyLineDV_GLLineExpressions {
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PolicyLineDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_144 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_146 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_148 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_150 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_152 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_154 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_156 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_158 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_160 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_162 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_164 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_166 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_168 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_170 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_172 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_174 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_176 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_178 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_180 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_182 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_184 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_186 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_188 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_190 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_192 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_194 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_196 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_198 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_200 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_202 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_204 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_206 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_208 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_210 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_212 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_214 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_216 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_218 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_220 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_222 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_224 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_226 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_228 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_230 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_232 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_234 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_236 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_238 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_240 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_242 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_244 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_246 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_onEnter_248 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_145 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_147 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_149 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_151 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_153 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_155 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_157 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_159 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_161 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_163 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_165 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_167 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_169 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_171 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_173 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_175 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_177 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_179 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_181 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_183 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_185 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_187 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_189 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_191 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_193 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_195 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_197 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_199 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_201 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_203 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_205 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_207 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_209 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_211 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_213 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_215 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_217 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_219 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_221 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_223 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_225 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_227 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_229 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_231 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_233 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_235 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_237 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_239 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_241 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_243 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_245 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_247 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function def_refreshVariables_249 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(conditionpattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 104, column 45
    function mode_250 () : java.lang.Object {
      return conditionpattern.PublicID
    }
    
    property get conditionpattern () : gw.api.productmodel.ConditionPattern {
      return getIteratedValue(1) as gw.api.productmodel.ConditionPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyLineDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_100 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_102 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_104 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_106 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_108 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_110 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_112 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_114 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_116 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_118 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_120 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_122 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_124 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_126 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_128 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_130 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_132 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_134 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_136 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_138 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_140 (def :  pcf.CoverageInputSet_default) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_36 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_38 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_40 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_42 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_44 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_46 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_48 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_50 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_52 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_54 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_56 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_58 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_60 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_62 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_64 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_66 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_68 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_70 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_72 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_74 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_76 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_78 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_80 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_82 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_84 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_86 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_88 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_90 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_92 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_94 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_96 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_onEnter_98 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.onEnter(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_101 (def :  pcf.CoverageInputSet_WC7AircraftPremiumEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_103 (def :  pcf.CoverageInputSet_WC7BenefitsDedCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_105 (def :  pcf.CoverageInputSet_WC7DesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_107 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsDesignatedLocationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_109 (def :  pcf.CoverageInputSet_WC7DesignatedOperationsExcl_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_111 (def :  pcf.CoverageInputSet_WC7DesignatedWorkplacesExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_113 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_115 (def :  pcf.CoverageInputSet_WC7EmployeeLeasingClientExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_117 (def :  pcf.CoverageInputSet_WC7FederalEmployersLiabilityActACov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_119 (def :  pcf.CoverageInputSet_WC7LaborContractorEndorsementACond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_121 (def :  pcf.CoverageInputSet_WC7LaborContractorExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_123 (def :  pcf.CoverageInputSet_WC7MaritimeACov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_125 (def :  pcf.CoverageInputSet_WC7MultipleCoordinatedPolicyEndorsementCond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_127 (def :  pcf.CoverageInputSet_WC7OtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_129 (def :  pcf.CoverageInputSet_WC7PartnersOfficersAndOthersExclEndorsementExcl) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_131 (def :  pcf.CoverageInputSet_WC7SoleProprietorsPartnersOfficersAndOthersCovCond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_133 (def :  pcf.CoverageInputSet_WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_135 (def :  pcf.CoverageInputSet_WC7WorkersCompEmpLiabInsurancePolicyACov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_137 (def :  pcf.CoverageInputSet_WCEmpLiabCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_139 (def :  pcf.CoverageInputSet_WCOtherStatesInsurance) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_141 (def :  pcf.CoverageInputSet_default) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_37 (def :  pcf.CoverageInputSet_BOPBuildingCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_39 (def :  pcf.CoverageInputSet_BOPManuscriptEndorsement_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_41 (def :  pcf.CoverageInputSet_BOPPersonalPropCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_43 (def :  pcf.CoverageInputSet_BOPSpoilageCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_45 (def :  pcf.CoverageInputSet_BOPToolsSchedCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_47 (def :  pcf.CoverageInputSet_CPBPPCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_49 (def :  pcf.CoverageInputSet_CPBldgCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_51 (def :  pcf.CoverageInputSet_CPBldgStockCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_53 (def :  pcf.CoverageInputSet_GLAdditionalInsuredCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_55 (def :  pcf.CoverageInputSet_GLBLAICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_57 (def :  pcf.CoverageInputSet_GLCOICov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_59 (def :  pcf.CoverageInputSet_GLCovExtCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_61 (def :  pcf.CoverageInputSet_GLCovExtDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_63 (def :  pcf.CoverageInputSet_GLExcludedServiceCond_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_65 (def :  pcf.CoverageInputSet_GLFullTimeFacultyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_67 (def :  pcf.CoverageInputSet_GLFullTimePostGradDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_69 (def :  pcf.CoverageInputSet_GLIRPMDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_71 (def :  pcf.CoverageInputSet_GLLocumTenensCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_73 (def :  pcf.CoverageInputSet_GLManuscriptEndor_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_75 (def :  pcf.CoverageInputSet_GLMobileDentalClinicCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_77 (def :  pcf.CoverageInputSet_GLMultiOwnerDPECov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_79 (def :  pcf.CoverageInputSet_GLNameOTDCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_81 (def :  pcf.CoverageInputSet_GLNewToCompanyDiscount_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_83 (def :  pcf.CoverageInputSet_GLSchoolServicesCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_85 (def :  pcf.CoverageInputSet_GLServiceMembersCRACov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_87 (def :  pcf.CoverageInputSet_GLSpecialEventCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_89 (def :  pcf.CoverageInputSet_GLStateExclCov_TDIC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_91 (def :  pcf.CoverageInputSet_HOPCovA) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_93 (def :  pcf.CoverageInputSet_HOPCovB) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_95 (def :  pcf.CoverageInputSet_HOPCovC) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_97 (def :  pcf.CoverageInputSet_HOPCovD) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function def_refreshVariables_99 (def :  pcf.CoverageInputSet_PALiabilityCov) : void {
      def.refreshVariables(coveragePattern, glLine, CurrentLocation.InEditMode)
    }
    
    // 'mode' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function mode_142 () : java.lang.Object {
      return coveragePattern.PublicID
    }
    
    // 'visible' attribute on InputSetRef at PolicyLineDV.GLLine.pcf: line 94, column 88
    function visible_35 () : java.lang.Boolean {
      return !(coveragePattern.CodeIdentifier == "GLMedPayCov_TDIC")
    }
    
    property get coveragePattern () : gw.api.productmodel.CoveragePattern {
      return getIteratedValue(1) as gw.api.productmodel.CoveragePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/line/gl/policy/PolicyLineDV.GLLine.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyLineDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=ClaimsMadeOrigEffDate_Input) at PolicyLineDV.GLLine.pcf: line 59, column 58
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.ClaimsMadeOrigEffDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Retrodate_Input) at PolicyLineDV.GLLine.pcf: line 70, column 59
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.RetroactiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=GLPolicySplitLimits_Input) at PolicyLineDV.GLLine.pcf: line 76, column 25
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.SplitLimits = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=GLPolicyClaimsMade_Input) at PolicyLineDV.GLLine.pcf: line 49, column 25
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      glLine.GLCoverageForm = (__VALUE_TO_SET as typekey.GLCoverageFormType)
    }
    
    // 'editable' attribute on DateInput (id=ClaimsMadeOrigEffDate_Input) at PolicyLineDV.GLLine.pcf: line 59, column 58
    function editable_13 () : java.lang.Boolean {
      return policyPeriod.EditEffectiveDate == policyPeriod.PeriodStart
    }
    
    // 'editable' attribute on DateInput (id=Retrodate_Input) at PolicyLineDV.GLLine.pcf: line 70, column 59
    function editable_21 () : java.lang.Boolean {
      return policyPeriod.Job.Subtype == typekey.Job.TC_SUBMISSION? true : false
    }
    
    // 'editable' attribute on TypeKeyInput (id=GLPolicyClaimsMade_Input) at PolicyLineDV.GLLine.pcf: line 49, column 25
    function editable_6 () : java.lang.Boolean {
      return policyPeriod.PolicyChange == null
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 14, column 35
    function initialValue_0 () : productmodel.GLLine {
      return policyLine as GLLine
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 18, column 52
    function initialValue_1 () : gw.api.productmodel.CoverageCategory {
      return glLine.Pattern.getCoverageCategoryByCodeIdentifier("PLRequiredCat_TDIC")
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 25, column 30
    function initialValue_2 () : java.util.Date {
      return glLine.ClaimsMadeOrigEffDate
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 29, column 35
    function initialValue_3 () : entity.PolicyPeriod {
      return policyLine.Branch
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 34, column 53
    function initialValue_4 () : gw.api.productmodel.CoveragePattern[] {
      return glGroupCategory.coveragePatternsForEntity(GeneralLiabilityLine).whereSelectedOrAvailable(glLine, CurrentLocation.InEditMode)
    }
    
    // 'initialValue' attribute on Variable at PolicyLineDV.GLLine.pcf: line 39, column 54
    function initialValue_5 () : gw.api.productmodel.ConditionPattern[] {
      return glGroupCategory.conditionPatternsForEntity(GeneralLiabilityLine).where(\ c -> glLine.isConditionSelectedOrAvailable(c))
    }
    
    // 'onChange' attribute on PostOnChange at PolicyLineDV.GLLine.pcf: line 62, column 56
    function onChange_12 () : void {
      changeRetroactiveDateIfTheSame()
    }
    
    // 'onChange' attribute on PostOnChange at PolicyLineDV.GLLine.pcf: line 79, column 103
    function onChange_29 () : void {
      gw.web.productmodel.ProductModelSyncIssuesHandler.syncCoverages({glLine}, null)
    }
    
    // 'sortBy' attribute on IteratorSort at PolicyLineDV.GLLine.pcf: line 90, column 26
    function sortBy_34 (coveragePattern :  gw.api.productmodel.CoveragePattern) : java.lang.Object {
      return coveragePattern.Priority
    }
    
    // 'value' attribute on TypeKeyInput (id=GLPolicyClaimsMade_Input) at PolicyLineDV.GLLine.pcf: line 49, column 25
    function valueRoot_9 () : java.lang.Object {
      return glLine
    }
    
    // 'value' attribute on InputIterator (id=GLGroupIterator) at PolicyLineDV.GLLine.pcf: line 87, column 59
    function value_143 () : gw.api.productmodel.CoveragePattern[] {
      return glGroupCategoryCoveragePatterns.where(\elt -> elt.CodeIdentifier != "GLAggLimit_A_TDIC" && elt.CodeIdentifier != "GLAggLimit_B_TDIC")
    }
    
    // 'value' attribute on DateInput (id=Retrodate_Input) at PolicyLineDV.GLLine.pcf: line 70, column 59
    function value_23 () : java.util.Date {
      return glLine.RetroactiveDate
    }
    
    // 'value' attribute on InputIterator (id=GLConditionGroupIterator) at PolicyLineDV.GLLine.pcf: line 101, column 60
    function value_251 () : gw.api.productmodel.ConditionPattern[] {
      return glGroupCategoryConditionPatterns
    }
    
    // 'value' attribute on BooleanRadioInput (id=GLPolicySplitLimits_Input) at PolicyLineDV.GLLine.pcf: line 76, column 25
    function value_30 () : java.lang.Boolean {
      return glLine.SplitLimits
    }
    
    // 'value' attribute on TypeKeyInput (id=GLPolicyClaimsMade_Input) at PolicyLineDV.GLLine.pcf: line 49, column 25
    function value_7 () : typekey.GLCoverageFormType {
      return glLine.GLCoverageForm
    }
    
    // 'visible' attribute on DateInput (id=ClaimsMadeOrigEffDate_Input) at PolicyLineDV.GLLine.pcf: line 59, column 58
    function visible_14 () : java.lang.Boolean {
      return glLine.GLCoverageForm == TC_CLAIMSMADE
    }
    
    property get glGroupCategory () : gw.api.productmodel.CoverageCategory {
      return getVariableValue("glGroupCategory", 0) as gw.api.productmodel.CoverageCategory
    }
    
    property set glGroupCategory ($arg :  gw.api.productmodel.CoverageCategory) {
      setVariableValue("glGroupCategory", 0, $arg)
    }
    
    property get glGroupCategoryConditionPatterns () : gw.api.productmodel.ConditionPattern[] {
      return getVariableValue("glGroupCategoryConditionPatterns", 0) as gw.api.productmodel.ConditionPattern[]
    }
    
    property set glGroupCategoryConditionPatterns ($arg :  gw.api.productmodel.ConditionPattern[]) {
      setVariableValue("glGroupCategoryConditionPatterns", 0, $arg)
    }
    
    property get glGroupCategoryCoveragePatterns () : gw.api.productmodel.CoveragePattern[] {
      return getVariableValue("glGroupCategoryCoveragePatterns", 0) as gw.api.productmodel.CoveragePattern[]
    }
    
    property set glGroupCategoryCoveragePatterns ($arg :  gw.api.productmodel.CoveragePattern[]) {
      setVariableValue("glGroupCategoryCoveragePatterns", 0, $arg)
    }
    
    property get glLine () : productmodel.GLLine {
      return getVariableValue("glLine", 0) as productmodel.GLLine
    }
    
    property set glLine ($arg :  productmodel.GLLine) {
      setVariableValue("glLine", 0, $arg)
    }
    
    property get jobWizardHelper () : gw.api.web.job.JobWizardHelper {
      return getRequireValue("jobWizardHelper", 0) as gw.api.web.job.JobWizardHelper
    }
    
    property set jobWizardHelper ($arg :  gw.api.web.job.JobWizardHelper) {
      setRequireValue("jobWizardHelper", 0, $arg)
    }
    
    property get policyLine () : PolicyLine {
      return getRequireValue("policyLine", 0) as PolicyLine
    }
    
    property set policyLine ($arg :  PolicyLine) {
      setRequireValue("policyLine", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get previousClaimsMadeDate () : java.util.Date {
      return getVariableValue("previousClaimsMadeDate", 0) as java.util.Date
    }
    
    property set previousClaimsMadeDate ($arg :  java.util.Date) {
      setVariableValue("previousClaimsMadeDate", 0, $arg)
    }
    
    function changeRetroactiveDateIfTheSame(){
      if(gw.api.web.util.PCDateUtil.equalsIgnoreTime(glLine.RetroactiveDate, previousClaimsMadeDate)){
        glLine.RetroactiveDate = glLine.ClaimsMadeOrigEffDate
      }
      previousClaimsMadeDate = glLine.ClaimsMadeOrigEffDate
    }
    
    
  }
  
  
}