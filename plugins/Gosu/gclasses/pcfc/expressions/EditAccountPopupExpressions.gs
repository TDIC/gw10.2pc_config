package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/EditAccountPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditAccountPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/EditAccountPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditAccountPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=EditAccountPopup) at EditAccountPopup.pcf: line 12, column 57
    function beforeCommit_99 (pickedValue :  java.lang.Object) : void {
      helper.updateAddressStatus(selectedAddress)
    }
    
    // 'canVisit' attribute on Popup (id=EditAccountPopup) at EditAccountPopup.pcf: line 12, column 57
    static function canVisit_100 (account :  Account) : java.lang.Boolean {
      return account.Editable and perm.Account.edit(account)
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 22, column 30
    function initialValue_0 () : entity.Contact {
      return account.AccountHolderContact
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 27, column 30
    function initialValue_1 () : entity.Address {
      return contact.PrimaryAddress
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 31, column 76
    function initialValue_2 () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return new tdic.pc.config.addressverification.AddressVerificationHelper()
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 54, column 50
    function initialValue_3 () : java.util.List<PolicyPeriod> {
      return contact.AssociationFinder.findLatestBoundPolicyPeriods()
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 58, column 25
    function initialValue_4 () : Account[] {
      return contact.AssociationFinder.findAccounts().where(\ elt -> elt != account and (perm.Account.view(elt) or revealAccountsIgnoringViewPermission_TDIC))
    }
    
    // 'parent' attribute on Popup (id=EditAccountPopup) at EditAccountPopup.pcf: line 12, column 57
    static function parent_101 (account :  Account) : pcf.api.Destination {
      return pcf.AccountFile_Summary.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.EditAccountPopup {
      return super.CurrentLocation as pcf.EditAccountPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get addressList () : java.util.ArrayList<Address> {
      return getVariableValue("addressList", 0) as java.util.ArrayList<Address>
    }
    
    property set addressList ($arg :  java.util.ArrayList<Address>) {
      setVariableValue("addressList", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get displayMembershipCheckError () : Boolean {
      return getVariableValue("displayMembershipCheckError", 0) as Boolean
    }
    
    property set displayMembershipCheckError ($arg :  Boolean) {
      setVariableValue("displayMembershipCheckError", 0, $arg)
    }
    
    property get helper () : tdic.pc.config.addressverification.AddressVerificationHelper {
      return getVariableValue("helper", 0) as tdic.pc.config.addressverification.AddressVerificationHelper
    }
    
    property set helper ($arg :  tdic.pc.config.addressverification.AddressVerificationHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get impactedAccounts_TDIC () : Account[] {
      return getVariableValue("impactedAccounts_TDIC", 0) as Account[]
    }
    
    property set impactedAccounts_TDIC ($arg :  Account[]) {
      setVariableValue("impactedAccounts_TDIC", 0, $arg)
    }
    
    property get impactedPolicies_TDIC () : java.util.List<PolicyPeriod> {
      return getVariableValue("impactedPolicies_TDIC", 0) as java.util.List<PolicyPeriod>
    }
    
    property set impactedPolicies_TDIC ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("impactedPolicies_TDIC", 0, $arg)
    }
    
    property get overrideVisible () : Boolean {
      return getVariableValue("overrideVisible", 0) as Boolean
    }
    
    property set overrideVisible ($arg :  Boolean) {
      setVariableValue("overrideVisible", 0, $arg)
    }
    
    property get revealAccountsIgnoringViewPermission_TDIC () : boolean {
      return getVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0) as java.lang.Boolean
    }
    
    property set revealAccountsIgnoringViewPermission_TDIC ($arg :  boolean) {
      setVariableValue("revealAccountsIgnoringViewPermission_TDIC", 0, $arg)
    }
    
    property get selectedAddress () : entity.Address {
      return getVariableValue("selectedAddress", 0) as entity.Address
    }
    
    property set selectedAddress ($arg :  entity.Address) {
      setVariableValue("selectedAddress", 0, $arg)
    }
    
    property get standardizeVisible () : Boolean {
      return getVariableValue("standardizeVisible", 0) as Boolean
    }
    
    property set standardizeVisible ($arg :  Boolean) {
      setVariableValue("standardizeVisible", 0, $arg)
    }
    
    property get updateVisible () : Boolean {
      return getVariableValue("updateVisible", 0) as Boolean
    }
    
    property set updateVisible ($arg :  Boolean) {
      setVariableValue("updateVisible", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/EditAccountPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditAccountScreenExpressionsImpl extends EditAccountPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at EditAccountPopup.pcf: line 84, column 71
    function action_11 () : void {
      if (impactedAccounts_TDIC.HasElements or impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(contact, impactedPolicies_TDIC)}
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_64 () : void {
      IndustryCodeSearchPopup.push(typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'action' attribute on ToolbarButton (id=StandardizeButton) at EditAccountPopup.pcf: line 75, column 57
    function action_8 () : void {
      addressList = helper.validateAddress(selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); updateVisible = helper.getUpdateVisible(); overrideVisible = helper.getOverrideVisible(); helper.displayExceptions();
    }
    
    // 'pickLocation' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function action_dest_65 () : pcf.api.Destination {
      return pcf.IndustryCodeSearchPopup.createDestination(typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'available' attribute on ToolbarButton (id=StandardizeButton) at EditAccountPopup.pcf: line 75, column 57
    function available_6 () : java.lang.Boolean {
      return standardizeVisible and openForEdit
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function def_onEnter_15 (def :  pcf.ContactNameInputSet_company) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function def_onEnter_17 (def :  pcf.ContactNameInputSet_person) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 124, column 111
    function def_onEnter_38 (def :  pcf.LinkedAddressInputSet) : void {
      def.onEnter(selectedAddress, contact, null, account, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 128, column 40
    function def_onEnter_42 (def :  pcf.AddressInputSet) : void {
      def.onEnter(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at EditAccountPopup.pcf: line 138, column 39
    function def_onEnter_44 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.onEnter(selectedAddress)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at EditAccountPopup.pcf: line 149, column 71
    function def_onEnter_54 (def :  pcf.AccountCurrencyInputSet) : void {
      def.onEnter(account, selectedAddress, false)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 158, column 40
    function def_onEnter_59 (def :  pcf.OfficialIDInputSet_company) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 158, column 40
    function def_onEnter_61 (def :  pcf.OfficialIDInputSet_person) : void {
      def.onEnter(contact, null)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function def_refreshVariables_16 (def :  pcf.ContactNameInputSet_company) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function def_refreshVariables_18 (def :  pcf.ContactNameInputSet_person) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 124, column 111
    function def_refreshVariables_39 (def :  pcf.LinkedAddressInputSet) : void {
      def.refreshVariables(selectedAddress, contact, null, account, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 128, column 40
    function def_refreshVariables_43 (def :  pcf.AddressInputSet) : void {
      def.refreshVariables(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))
    }
    
    // 'def' attribute on InputSetRef (id=AddressTypeRef_TDIC) at EditAccountPopup.pcf: line 138, column 39
    function def_refreshVariables_45 (def :  pcf.TDIC_AccountAddressTypeInputSet) : void {
      def.refreshVariables(selectedAddress)
    }
    
    // 'def' attribute on InputSetRef (id=AccountCurrency) at EditAccountPopup.pcf: line 149, column 71
    function def_refreshVariables_55 (def :  pcf.AccountCurrencyInputSet) : void {
      def.refreshVariables(account, selectedAddress, false)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 158, column 40
    function def_refreshVariables_60 (def :  pcf.OfficialIDInputSet_company) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'def' attribute on InputSetRef at EditAccountPopup.pcf: line 158, column 40
    function def_refreshVariables_62 (def :  pcf.OfficialIDInputSet_person) : void {
      def.refreshVariables(contact, null)
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at EditAccountPopup.pcf: line 102, column 39
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.Nickname = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Web_Input) at EditAccountPopup.pcf: line 108, column 29
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.WebAddress_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at EditAccountPopup.pcf: line 115, column 79
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.PrimaryLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TypeKeyInput (id=ServiceTier_Input) at EditAccountPopup.pcf: line 121, column 54
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.ServiceTier = (__VALUE_TO_SET as typekey.CustomerServiceTier)
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditAccountPopup.pcf: line 143, column 50
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedAddress.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.IndustryCode = (__VALUE_TO_SET as entity.IndustryCode)
    }
    
    // 'editable' attribute on InputSetRef at EditAccountPopup.pcf: line 128, column 40
    function editable_40 () : java.lang.Boolean {
      return selectedAddress.LinkedAddress == null
    }
    
    // 'editable' attribute on InputSetRef (id=AccountCurrency) at EditAccountPopup.pcf: line 149, column 71
    function editable_52 () : java.lang.Boolean {
      return account.Editable
    }
    
    // 'initialValue' attribute on Variable at EditAccountPopup.pcf: line 68, column 25
    function initialValue_5 () : Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'inputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function inputConversion_66 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.findCode(VALUE, typekey.IndustryCodeType.TC_SIC)
    }
    
    // 'updateLabel' attribute on EditButtons at EditAccountPopup.pcf: line 78, column 61
    function label_10 () : java.lang.Object {
      return updateVisible ? DisplayKey.get("Button.Update") : DisplayKey.get("Web.AccountFile.Locations.Override")
    }
    
    // 'label' attribute on Verbatim (id=ContactUsageImpact_TJT) at EditAccountPopup.pcf: line 91, column 25
    function label_13 () : java.lang.String {
      return DisplayKey.get("TDIC.ChangesToThisContactMayImpactPolicies", impactedPolicies_TDIC.Count) + " " + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(", ")
    }
    
    // 'mode' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function mode_19 () : java.lang.Object {
      return contact.Subtype.Code
    }
    
    // 'mode' attribute on InputSetRef at EditAccountPopup.pcf: line 158, column 40
    function mode_63 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'outputConversion' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function outputConversion_67 (VALUE :  entity.IndustryCode) : java.lang.String {
      return VALUE == null ? "" : VALUE.Code
    }
    
    // 'requestValidationExpression' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function requestValidationExpression_68 (VALUE :  entity.IndustryCode) : java.lang.Object {
      return gw.api.web.product.IndustryCodePickerUtil.validateValue(VALUE, null, null)
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditAccountPopup.pcf: line 211, column 56
    function sortValue_76 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at EditAccountPopup.pcf: line 215, column 56
    function sortValue_77 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at EditAccountPopup.pcf: line 219, column 48
    function sortValue_78 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at EditAccountPopup.pcf: line 224, column 44
    function sortValue_79 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at EditAccountPopup.pcf: line 228, column 54
    function sortValue_80 (suggestedAddress :  entity.Address) : java.lang.Object {
      return suggestedAddress.PostalCode
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at EditAccountPopup.pcf: line 102, column 39
    function valueRoot_22 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditAccountPopup.pcf: line 143, column 50
    function valueRoot_49 () : java.lang.Object {
      return selectedAddress
    }
    
    // 'value' attribute on TextInput (id=Nickname_Input) at EditAccountPopup.pcf: line 102, column 39
    function value_20 () : java.lang.String {
      return account.Nickname
    }
    
    // 'value' attribute on TextInput (id=Web_Input) at EditAccountPopup.pcf: line 108, column 29
    function value_24 () : java.lang.String {
      return account.WebAddress_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at EditAccountPopup.pcf: line 115, column 79
    function value_29 () : typekey.LanguageType {
      return account.PrimaryLanguage
    }
    
    // 'value' attribute on TypeKeyInput (id=ServiceTier_Input) at EditAccountPopup.pcf: line 121, column 54
    function value_34 () : typekey.CustomerServiceTier {
      return account.ServiceTier
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at EditAccountPopup.pcf: line 143, column 50
    function value_47 () : java.lang.String {
      return selectedAddress.Description
    }
    
    // 'value' attribute on IndustryCodeInput (id=IndustryCode_Input) at IndustryCodeWidget.xml: line 5, column 47
    function value_69 () : entity.IndustryCode {
      return account.IndustryCode
    }
    
    // 'value' attribute on RowIterator (id=melissaReturn) at EditAccountPopup.pcf: line 198, column 42
    function value_97 () : entity.Address[] {
      return addressList.toTypedArray()
    }
    
    // 'visible' attribute on Verbatim (id=ContactUsageImpact_TJT) at EditAccountPopup.pcf: line 91, column 25
    function visible_12 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and impactedPolicies_TDIC.Count > 0
    }
    
    // 'visible' attribute on InputSetRef at EditAccountPopup.pcf: line 97, column 40
    function visible_14 () : java.lang.Boolean {
      return contact != null
    }
    
    // 'visible' attribute on TypeKeyInput (id=PrimaryLanguage_Input) at EditAccountPopup.pcf: line 115, column 79
    function visible_28 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().size() > 1
    }
    
    // 'visible' attribute on InputSetRef (id=AccountCurrency) at EditAccountPopup.pcf: line 149, column 71
    function visible_53 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on InputSet at EditAccountPopup.pcf: line 160, column 67
    function visible_75 () : java.lang.Boolean {
      return account.AccountHolderContact typeis Company
    }
    
    // 'updateVisible' attribute on EditButtons at EditAccountPopup.pcf: line 78, column 61
    function visible_9 () : java.lang.Boolean {
      return updateVisible or overrideVisible
    }
    
    // 'visible' attribute on PanelRef at EditAccountPopup.pcf: line 187, column 44
    function visible_98 () : java.lang.Boolean {
      return addressList.Count > 0
    }
    
    property get openForEdit () : Boolean {
      return getVariableValue("openForEdit", 1) as Boolean
    }
    
    property set openForEdit ($arg :  Boolean) {
      setVariableValue("openForEdit", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/EditAccountPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditAccountScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SelectAddress) at EditAccountPopup.pcf: line 206, column 44
    function action_81 () : void {
      helper.replaceAddressWithSuggested(suggestedAddress, selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); addressList = null
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditAccountPopup.pcf: line 211, column 56
    function valueRoot_83 () : java.lang.Object {
      return suggestedAddress
    }
    
    // 'value' attribute on TextCell (id=AddressLine1_Cell) at EditAccountPopup.pcf: line 211, column 56
    function value_82 () : java.lang.String {
      return suggestedAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=AddressLine2_Cell) at EditAccountPopup.pcf: line 215, column 56
    function value_85 () : java.lang.String {
      return suggestedAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at EditAccountPopup.pcf: line 219, column 48
    function value_88 () : java.lang.String {
      return suggestedAddress.City
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at EditAccountPopup.pcf: line 224, column 44
    function value_91 () : typekey.State {
      return suggestedAddress.State
    }
    
    // 'value' attribute on TextCell (id=zip_Cell) at EditAccountPopup.pcf: line 228, column 54
    function value_94 () : java.lang.String {
      return suggestedAddress.PostalCode
    }
    
    property get suggestedAddress () : entity.Address {
      return getIteratedValue(2) as entity.Address
    }
    
    
  }
  
  
}