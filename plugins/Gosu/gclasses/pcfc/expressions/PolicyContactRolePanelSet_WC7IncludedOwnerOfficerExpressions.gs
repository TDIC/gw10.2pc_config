package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7IncludedOwnerOfficer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactRolePanelSet_WC7IncludedOwnerOfficerExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/PolicyContactRolePanelSet.WC7IncludedOwnerOfficer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactRolePanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRolePanelSet.WC7IncludedOwnerOfficer.pcf: line 23, column 61
    function def_onEnter_1 (def :  pcf.WC7OwnerOfficerInputSet) : void {
      def.onEnter(wc7OwnerOfficer)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactRolePanelSet.WC7IncludedOwnerOfficer.pcf: line 23, column 61
    function def_refreshVariables_2 (def :  pcf.WC7OwnerOfficerInputSet) : void {
      def.refreshVariables(wc7OwnerOfficer)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactRolePanelSet.WC7IncludedOwnerOfficer.pcf: line 14, column 44
    function initialValue_0 () : entity.WC7PolicyOwnerOfficer {
      return policyContactRole as WC7PolicyOwnerOfficer
    }
    
    property get policyContactRole () : PolicyContactRole {
      return getRequireValue("policyContactRole", 0) as PolicyContactRole
    }
    
    property set policyContactRole ($arg :  PolicyContactRole) {
      setRequireValue("policyContactRole", 0, $arg)
    }
    
    property get wc7OwnerOfficer () : entity.WC7PolicyOwnerOfficer {
      return getVariableValue("wc7OwnerOfficer", 0) as entity.WC7PolicyOwnerOfficer
    }
    
    property set wc7OwnerOfficer ($arg :  entity.WC7PolicyOwnerOfficer) {
      setVariableValue("wc7OwnerOfficer", 0, $arg)
    }
    
    
  }
  
  
}