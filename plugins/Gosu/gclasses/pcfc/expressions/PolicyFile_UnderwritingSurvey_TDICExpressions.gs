package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_UnderwritingSurvey_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyFile_UnderwritingSurvey_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_UnderwritingSurvey_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyFile_UnderwritingSurvey_TDICExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 64, column 38
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      uwDetail.Date = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyCell (id=SurveyStatus_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 70, column 54
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      uwDetail.SurveyStatus = (__VALUE_TO_SET as typekey.SurveyStatus_TDIC)
    }
    
    // 'value' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      uwDetail.TransactionNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'onChange' attribute on PostOnChange at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 54, column 54
    function onChange_4 () : void {
      setDescription(uwDetail)
    }
    
    // 'valueRange' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function valueRange_8 () : java.lang.Object {
      return getTransactionNumber()
    }
    
    // 'value' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function valueRoot_7 () : java.lang.Object {
      return uwDetail
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 59, column 45
    function value_12 () : java.lang.String {
      return uwDetail.Description
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 64, column 38
    function value_15 () : java.util.Date {
      return uwDetail.Date
    }
    
    // 'value' attribute on TypeKeyCell (id=SurveyStatus_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 70, column 54
    function value_19 () : typekey.SurveyStatus_TDIC {
      return uwDetail.SurveyStatus
    }
    
    // 'value' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function value_5 () : java.lang.String {
      return uwDetail.TransactionNumber
    }
    
    // 'valueRange' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function verifyValueRange_10 () : void {
      var __valueRangeArg = getTransactionNumber()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    property get uwDetail () : entity.UWSurveyDetails_TDIC {
      return getIteratedValue(1) as entity.UWSurveyDetails_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policyfile/PolicyFile_UnderwritingSurvey_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyFile_UnderwritingSurvey_TDICExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, asOfDate :  java.util.Date) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=PolicyFile_UnderwritingSurvey_TDIC) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 11, column 80
    function afterEnter_26 () : void {
      gw.pcf.policysummary.PolicySummaryHelper.setUWSurveyDetails_TDIC(policyPeriod)
    }
    
    // 'canEdit' attribute on Page (id=PolicyFile_UnderwritingSurvey_TDIC) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 11, column 80
    function canEdit_27 () : java.lang.Boolean {
      return perm.System.edituwsurvey_tdic
    }
    
    // 'canVisit' attribute on Page (id=PolicyFile_UnderwritingSurvey_TDIC) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 11, column 80
    static function canVisit_28 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return policyPeriod.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
    }
    
    // Page (id=PolicyFile_UnderwritingSurvey_TDIC) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 11, column 80
    static function parent_29 (asOfDate :  java.util.Date, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyFile.createDestination(policyPeriod,asOfDate)
    }
    
    // 'value' attribute on RangeCell (id=Transactions_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 52, column 44
    function sortValue_0 (uwDetail :  entity.UWSurveyDetails_TDIC) : java.lang.Object {
      return uwDetail.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 59, column 45
    function sortValue_1 (uwDetail :  entity.UWSurveyDetails_TDIC) : java.lang.Object {
      return uwDetail.Description
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 64, column 38
    function sortValue_2 (uwDetail :  entity.UWSurveyDetails_TDIC) : java.lang.Object {
      return uwDetail.Date
    }
    
    // 'value' attribute on TypeKeyCell (id=SurveyStatus_Cell) at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 70, column 54
    function sortValue_3 (uwDetail :  entity.UWSurveyDetails_TDIC) : java.lang.Object {
      return uwDetail.SurveyStatus
    }
    
    // 'toAdd' attribute on RowIterator at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 43, column 53
    function toAdd_23 (uwDetail :  entity.UWSurveyDetails_TDIC) : void {
      toAdd(uwDetail)
    }
    
    // 'toRemove' attribute on RowIterator at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 43, column 53
    function toRemove_24 (uwDetail :  entity.UWSurveyDetails_TDIC) : void {
      toRemove(uwDetail)
    }
    
    // 'value' attribute on RowIterator at PolicyFile_UnderwritingSurvey_TDIC.pcf: line 43, column 53
    function value_25 () : entity.UWSurveyDetails_TDIC[] {
      return policyPeriod.Policy.UWSurveyDetails_TDIC
    }
    
    override property get CurrentLocation () : pcf.PolicyFile_UnderwritingSurvey_TDIC {
      return super.CurrentLocation as pcf.PolicyFile_UnderwritingSurvey_TDIC
    }
    
    property get asOfDate () : java.util.Date {
      return getVariableValue("asOfDate", 0) as java.util.Date
    }
    
    property set asOfDate ($arg :  java.util.Date) {
      setVariableValue("asOfDate", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function getTransactionNumber(): List<String> {
      var jobs = policyPeriod.Policy.Jobs
      var jobNumber : List<String> = {}
      for (job in jobs) {
        if (job.Subtype == typekey.Job.TC_SUBMISSION or job.Subtype == typekey.Job.TC_RENEWAL or job.Subtype == typekey.Job.TC_REINSTATEMENT) {
          jobNumber.add(job.JobNumber)
        }
      }
      return jobNumber
    }
    
    function toRemove(uwDetail: UWSurveyDetails_TDIC){
      if(uwDetail.isRemovable){
        policyPeriod.Policy.removeFromUWSurveyDetails_TDIC(uwDetail)
      }
    }
    function toAdd(uwDetail: UWSurveyDetails_TDIC){
      policyPeriod.Policy.addToUWSurveyDetails_TDIC(uwDetail)
      uwDetail.isRemovable = true
    }
    
    function setDescription(uwDetail: UWSurveyDetails_TDIC){
      if(uwDetail.TransactionNumber.contains("SUB")){
        uwDetail.Description = typekey.Job.TC_SUBMISSION.DisplayName
      }
      else if(uwDetail.TransactionNumber.contains("REN")){
        uwDetail.Description = typekey.Job.TC_RENEWAL.DisplayName
      }
      else if(uwDetail.TransactionNumber.contains("REI")){
        uwDetail.Description = typekey.Job.TC_REINSTATEMENT.DisplayName
      }
    }
    
    
  }
  
  
}