package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/line/bop/policy/LineLocationDetailInputSet.BusinessOwners.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LineLocationDetailInputSet_BusinessOwnersExpressions {
  @javax.annotation.Generated("config/web/pcf/line/bop/policy/LineLocationDetailInputSet.BusinessOwners.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LineLocationDetailInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at LineLocationDetailInputSet.BusinessOwners.pcf: line 18, column 27
    function initialValue_0 () : BOPLocation {
      return polLocation.Branch.BOPLine.getBOPLocationForPolicyLocation(polLocation)
    }
    
    // 'value' attribute on TextAreaInput (id=ControlAddress_Input) at LineLocationDetailInputSet.BusinessOwners.pcf: line 50, column 55
    function valueRoot_2 () : java.lang.Object {
      return bopLocation.PolicyLocation
    }
    
    // 'value' attribute on TextAreaInput (id=ControlAddress_Input) at LineLocationDetailInputSet.BusinessOwners.pcf: line 50, column 55
    function value_1 () : java.lang.String {
      return bopLocation.PolicyLocation.Description
    }
    
    property get bopLocation () : BOPLocation {
      return getVariableValue("bopLocation", 0) as BOPLocation
    }
    
    property set bopLocation ($arg :  BOPLocation) {
      setVariableValue("bopLocation", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get polLocation () : PolicyLocation {
      return getRequireValue("polLocation", 0) as PolicyLocation
    }
    
    property set polLocation ($arg :  PolicyLocation) {
      setRequireValue("polLocation", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}