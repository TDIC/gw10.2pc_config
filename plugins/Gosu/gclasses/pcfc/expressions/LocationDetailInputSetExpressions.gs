package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
@javax.annotation.Generated("config/web/pcf/line/common/LocationDetailInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LocationDetailInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/line/common/LocationDetailInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationDetailInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at LocationDetailInputSet.pcf: line 68, column 41
    function def_onEnter_14 (def :  pcf.TargetedAddressInputSet) : void {
      def.onEnter(new gw.pcf.policylocation.PolicyLocationAddressOwner(polLocation))
    }
    
    // 'def' attribute on InputSetRef at LocationDetailInputSet.pcf: line 68, column 41
    function def_refreshVariables_15 (def :  pcf.TargetedAddressInputSet) : void {
      def.refreshVariables(new gw.pcf.policylocation.PolicyLocationAddressOwner(polLocation))
    }
    
    // 'value' attribute on TypeKeyInput (id=UnsyncedCountry_Input) at LocationDetailInputSet.pcf: line 82, column 37
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.Country = (__VALUE_TO_SET as typekey.Country)
    }
    
    // 'value' attribute on TextInput (id=ControlAddressAddressLine1_Input) at LocationDetailInputSet.pcf: line 122, column 55
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.ControlAddressLine1_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ControlAddressCity_Input) at LocationDetailInputSet.pcf: line 128, column 54
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.ControlAddressCity_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=effDate_Input) at LocationDetailInputSet.pcf: line 56, column 49
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.OriginalEffDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=UnsyncedCounty_Input) at LocationDetailInputSet.pcf: line 133, column 37
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.County = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.ControlAddressState_TDIC = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TextInput (id=ControlAddressPostalCode_Input) at LocationDetailInputSet.pcf: line 147, column 60
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.ControlAddressPostalCode_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at LocationDetailInputSet.pcf: line 63, column 63
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      polLocation.DBA_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TypeKeyInput (id=UnsyncedCountry_Input) at LocationDetailInputSet.pcf: line 82, column 37
    function editable_19 () : java.lang.Boolean {
      return polLocation.canChangeState()
    }
    
    // 'editable' attribute on TextInput (id=ControlAddressAddressLine1_Input) at LocationDetailInputSet.pcf: line 122, column 55
    function editable_31 () : java.lang.Boolean {
      return openForEdit and perm.System.controlAddress_TDIC
    }
    
    // 'initialValue' attribute on Variable at LocationDetailInputSet.pcf: line 19, column 46
    function initialValue_0 () : gw.api.spotlight.SpotlightURLs {
      return new gw.api.spotlight.SpotlightURLs()
    }
    
    // 'initialValue' attribute on Variable at LocationDetailInputSet.pcf: line 23, column 23
    function initialValue_1 () : Boolean {
      return polLocation.HasControlAddress_TDIC
    }
    
    // 'initialValue' attribute on Variable at LocationDetailInputSet.pcf: line 27, column 23
    function initialValue_2 () : Boolean {
      return setOriginalEffectiveDate_TDIC()
    }
    
    // 'mode' attribute on InputSetRef at LocationDetailInputSet.pcf: line 68, column 41
    function mode_16 () : java.lang.Object {
      return polLocation.CountryCode
    }
    
    // 'valueRange' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function valueRange_51 () : java.lang.Object {
      return gw.api.contact.AddressAutocompleteUtil.getStates(typekey.Country.TC_US)
    }
    
    // 'value' attribute on DateInput (id=effDate_Input) at LocationDetailInputSet.pcf: line 56, column 49
    function valueRoot_5 () : java.lang.Object {
      return polLocation
    }
    
    // 'value' attribute on TextInput (id=unsyncedAddressString_Input) at LocationDetailInputSet.pcf: line 75, column 74
    function value_17 () : java.lang.String {
      return polLocation.addressString("\n", false, false)
    }
    
    // 'value' attribute on TypeKeyInput (id=UnsyncedCountry_Input) at LocationDetailInputSet.pcf: line 82, column 37
    function value_20 () : typekey.Country {
      return polLocation.Country
    }
    
    // 'value' attribute on TextInput (id=unsyncedAddressDesc_TDIC_Input) at LocationDetailInputSet.pcf: line 89, column 57
    function value_26 () : java.lang.String {
      return polLocation.Description
    }
    
    // 'value' attribute on DateInput (id=effDate_Input) at LocationDetailInputSet.pcf: line 56, column 49
    function value_3 () : java.util.Date {
      return polLocation.OriginalEffDate_TDIC
    }
    
    // 'value' attribute on TextInput (id=ControlAddressAddressLine1_Input) at LocationDetailInputSet.pcf: line 122, column 55
    function value_32 () : java.lang.String {
      return polLocation.ControlAddressLine1_TDIC
    }
    
    // 'value' attribute on TextInput (id=ControlAddressCity_Input) at LocationDetailInputSet.pcf: line 128, column 54
    function value_38 () : java.lang.String {
      return polLocation.ControlAddressCity_TDIC
    }
    
    // 'value' attribute on TextInput (id=UnsyncedCounty_Input) at LocationDetailInputSet.pcf: line 133, column 37
    function value_43 () : java.lang.String {
      return polLocation.County
    }
    
    // 'value' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function value_48 () : typekey.State {
      return polLocation.ControlAddressState_TDIC
    }
    
    // 'value' attribute on TextInput (id=ControlAddressPostalCode_Input) at LocationDetailInputSet.pcf: line 147, column 60
    function value_57 () : java.lang.String {
      return polLocation.ControlAddressPostalCode_TDIC
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at LocationDetailInputSet.pcf: line 63, column 63
    function value_8 () : java.lang.String {
      return polLocation.DBA_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function verifyValueRangeIsAllowedType_52 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function verifyValueRangeIsAllowedType_52 ($$arg :  typekey.State[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ControlAddressState_Input) at LocationDetailInputSet.pcf: line 141, column 36
    function verifyValueRange_53 () : void {
      var __valueRangeArg = gw.api.contact.AddressAutocompleteUtil.getStates(typekey.Country.TC_US)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_52(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSetRef at LocationDetailInputSet.pcf: line 68, column 41
    function visible_13 () : java.lang.Boolean {
      return polLocation.SyncedToAccount
    }
    
    // 'visible' attribute on TextInput (id=unsyncedAddressDesc_TDIC_Input) at LocationDetailInputSet.pcf: line 89, column 57
    function visible_25 () : java.lang.Boolean {
      return not polLocation.Branch.BOPLineExists
    }
    
    // 'visible' attribute on InputSet (id=UnsyncedAddressInputSet) at LocationDetailInputSet.pcf: line 71, column 49
    function visible_30 () : java.lang.Boolean {
      return not polLocation.SyncedToAccount
    }
    
    // 'visible' attribute on InputSet (id=ControlAddressInputSet) at LocationDetailInputSet.pcf: line 116, column 92
    function visible_62 () : java.lang.Boolean {
      return (openForEdit)? hasControlAddress_TDIC : polLocation.HasControlAddress_TDIC
    }
    
    // 'visible' attribute on TextInput (id=DBA_Input) at LocationDetailInputSet.pcf: line 63, column 63
    function visible_7 () : java.lang.Boolean {
      return polLocation.Branch.Policy.Product.Commercial
    }
    
    property get hasControlAddress_TDIC () : Boolean {
      return getVariableValue("hasControlAddress_TDIC", 0) as Boolean
    }
    
    property set hasControlAddress_TDIC ($arg :  Boolean) {
      setVariableValue("hasControlAddress_TDIC", 0, $arg)
    }
    
    property get openForEdit () : boolean {
      return getRequireValue("openForEdit", 0) as java.lang.Boolean
    }
    
    property set openForEdit ($arg :  boolean) {
      setRequireValue("openForEdit", 0, $arg)
    }
    
    property get polLocation () : PolicyLocation {
      return getRequireValue("polLocation", 0) as PolicyLocation
    }
    
    property set polLocation ($arg :  PolicyLocation) {
      setRequireValue("polLocation", 0, $arg)
    }
    
    property get setOriginalEffDate () : Boolean {
      return getVariableValue("setOriginalEffDate", 0) as Boolean
    }
    
    property set setOriginalEffDate ($arg :  Boolean) {
      setVariableValue("setOriginalEffDate", 0, $arg)
    }
    
    property get spotlight () : gw.api.spotlight.SpotlightURLs {
      return getVariableValue("spotlight", 0) as gw.api.spotlight.SpotlightURLs
    }
    
    property set spotlight ($arg :  gw.api.spotlight.SpotlightURLs) {
      setVariableValue("spotlight", 0, $arg)
    }
    
    property get supportsNonSpecificLocation () : boolean {
      return getRequireValue("supportsNonSpecificLocation", 0) as java.lang.Boolean
    }
    
    property set supportsNonSpecificLocation ($arg :  boolean) {
      setRequireValue("supportsNonSpecificLocation", 0, $arg)
    }
    
    
    function getTaxLocation(code: String, policyLocation: PolicyLocation): TaxLocation {
      if (code == null) {
        return null
      } else {
        var state = gw.api.util.JurisdictionMappingUtil.getJurisdiction(policyLocation)
        var locs = new gw.lob.common.TaxLocationQueryBuilder()
            .withCodeStarting(code)
            .withState(state)
            .withEffectiveOnDate(policyLocation.Branch.PeriodStart)
            .build().select() as gw.api.database.IQueryBeanResult<TaxLocation>
        if (locs.Count == 1) {
          return locs.FirstResult
        } else {
          throw new DisplayableException(DisplayKey.get("TaxLocation.Search.Error.InvalidCode", code, state.Description))
        }
      }
    }
    
    function setOriginalEffectiveDate_TDIC():Boolean{
      if(polLocation.Branch.Job typeis Submission) polLocation.OriginalEffDate_TDIC = polLocation.Branch.PeriodStart
      if(!(polLocation.Branch.Job typeis Submission) && polLocation.New && polLocation.OriginalEffDate_TDIC == null) polLocation.OriginalEffDate_TDIC = java.util.Date.CurrentDate
      return true
    }
    
    
  }
  
  
}