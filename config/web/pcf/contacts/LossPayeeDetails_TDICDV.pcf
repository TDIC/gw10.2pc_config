<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    available="policyPeriod?.profileChange_TDIC"
    editable="openForEdit"
    id="LossPayeeDetails_TDICDV"
    visible="!(policyPeriod.Job typeis Submission) or perm.System.viewsubmission">
    <Require
      name="bopBuilding"
      type="entity.BOPBuilding"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="bopBuilding.PolicyPeriod"
      name="policyPeriod"
      recalculateOnRefresh="true"
      type="entity.PolicyPeriod"/>
    <Variable
      initialValue="gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)"
      name="contactConfigPlugin"
      type="gw.plugin.contact.IContactConfigPlugin"/>
    <Variable
      initialValue="null"
      name="existingLossPayee"
      recalculateOnRefresh="true"
      type="AccountContactView[]"/>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.Policy.LossPayee_TDIC&quot;, bopBuilding.TypeLabel)"/>
      <ListViewInput
        labelAbove="true">
        <Toolbar>
          <AddButton
            hideIfReadOnly="true"
            id="AddContactsButton"
            iterator="LossPayeeLV"
            label="DisplayKey.get(&quot;Web.Contact.Add&quot;)"
            visible="openForEdit">
            <AddMenuItemIterator
              elementName="contactType"
              value="contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC)"
              valueType="typekey.ContactType[]">
              <IteratorSort
                sortBy="contactType.DisplayName"
                sortOrder="1"/>
              <AddMenuItem
                id="ContactType"
                iterator="LossPayeeLV"
                label="DisplayKey.get(&quot;Web.Contact.AddNewOfType&quot;, contactType)"
                pickLocation="NewLossPayee_TDICPopup.push(bopBuilding, contactType)"/>
            </AddMenuItemIterator>
            <AddMenuItem
              conversionExpression="bopBuilding.addPolicyLossPayee_TDIC(PickedValue)"
              id="AddFromSearch"
              iterator="LossPayeeLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.FromAddressBook&quot;)"
              pickLocation="ContactSearchPopup.push(typekey.AccountContactRole.TC_LOSSPAYEE_TDIC)"
              visible="false"/>
            <AddMenuItem
              id="AddExistingContact"
              iterator="LossPayeeLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddExisting&quot;, DisplayKey.get(&quot;TDIC.entity.LossPayee&quot;))"
              visible="true">
              <AddMenuItemIterator
                elementName="losspayee"
                id="ContactOfSameType"
                value="getExistingLossPayees()"
                valueType="entity.AccountContactView[]">
                <AddMenuItem
                  id="ExistingMortgagees"
                  iterator="LossPayeeLV"
                  label="losspayee"
                  toCreateAndAdd="bopBuilding.addPolicyLossPayee_TDIC(losspayee.AccountContact.Contact)"/>
              </AddMenuItemIterator>
            </AddMenuItem>
            <AddMenuItem
              id="AddOtherContact"
              iterator="LossPayeeLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddOtherContacts&quot;)">
              <AddMenuItemIterator
                elementName="acctContact"
                id="ContactOfOtherType"
                value="bopBuilding.LossPayeeOtherCandidates_TDIC.asViews()"
                valueType="entity.AccountContactView[]">
                <IteratorSort
                  sortBy="acctContact.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="acctContact"
                  iterator="LossPayeeLV"
                  label="acctContact"
                  toCreateAndAdd="bopBuilding.addPolicyLossPayee_TDIC(acctContact.AccountContact.Contact)"/>
              </AddMenuItemIterator>
            </AddMenuItem>
          </AddButton>
          <IteratorButtons
            addVisible="false"
            id="IteratorButtons"
            iterator="LossPayeeLV"
            removeVisible="policyPeriod.Job typeis Submission or policyPeriod.Job typeis Renewal or bopBuilding.BOPBldgLossPayees.hasMatch(\lp -&gt; lp.BasedOn == null)"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="setExpirationDate(CheckedValues)"
            available="policyPeriod.Job typeis PolicyChange"
            id="expirebutton"
            iterator="LossPayeeLV"
            label="DisplayKey.get(&quot;TDIC.Policy.AdditionalInterests.ExpireButton&quot;)"
            visible="policyPeriod.Job typeis PolicyChange"/>
        </Toolbar>
        <ListViewPanel
          id="LossPayeeLV">
          <RowIterator
            checkBoxVisible="openForEdit"
            editable="true"
            elementName="lossPayeeDetail"
            hasCheckBoxes="true"
            pageSize="0"
            toRemove="bopBuilding.toRemoveFromBOPBldgLossPayees(lossPayeeDetail)"
            value="bopBuilding.BOPBldgLossPayees"
            valueType="entity.PolicyLossPayee_TDIC[]">
            <Row>
              <DateCell
                align="left"
                id="effDate"
                label="DisplayKey.get(&quot;TDIC.EffectiveDate&quot;)"
                required="true"
                value="getEffectiveDate(lossPayeeDetail)//lossPayeeDetail.EffectiveDate"/>
              <DateCell
                action="//bopBuilding.toggleLossPayee(policyPeriod)"
                align="left"
                id="expDate"
                label="DisplayKey.get(&quot;TDIC.ExpirationDate&quot;)"
                required="false"
                value="lossPayeeDetail.ExpirationDate_TDIC"/>
              <TextCell
                action="EditPolicyContactRolePopup.push(lossPayeeDetail, openForEdit)"
                id="Name"
                label="DisplayKey.get(&quot;Web.Policy.AdditionalInterest.Name&quot;)"
                required="false"
                value="lossPayeeDetail"
                valueType="entity.PolicyLossPayee_TDIC"/>
              <TextCell
                editable="true"
                id="LoanNumber"
                label="DisplayKey.get(&quot;Web.Policy.MortgageeLossPayee.LoanNumber_TDIC&quot;)"
                value="lossPayeeDetail.LoanNumberString"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputColumn>
    <Code><![CDATA[function setExpirationDate(policyLossPayees : PolicyLossPayee_TDIC[]) {
  policyLossPayees.each(\lossPayee -> {
    if(lossPayee.ExpirationDate_TDIC == null) {
      lossPayee.ExpirationDate_TDIC = lossPayee.Branch.EditEffectiveDate
    }
  })
}

function getExistingLossPayees() : AccountContactView[] {
  if (existingLossPayee == null) {
    var addedContacts = bopBuilding.BOPBldgLossPayees*.AccountContactRole*.AccountContact
    var all = bopBuilding.ExistingLossPayees_TDIC
    var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
    existingLossPayee = remaining
  }
  return existingLossPayee
}
function getEffectiveDate(lossPayee : PolicyLossPayee_TDIC) : Date {
  var effDates = bopBuilding.Branch.AllEffectiveDates.toSet()
  for(effDate in effDates.order()) {
    var version = lossPayee.VersionList.AsOf(effDate)
    if(version != null) {
      return version.EffectiveDate
    }
  }
  return lossPayee.EffectiveDate
}
]]></Code>
  </DetailViewPanel>
</PCF>