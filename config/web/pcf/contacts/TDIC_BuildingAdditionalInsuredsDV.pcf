<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    id="TDIC_BuildingAdditionalInsuredsDV">
    <Require
      name="bopBuilding"
      type="BOPBuilding"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Require
      name="displayLabel"
      type="boolean"/>
    <Require
      name="additionalInformationVisible"
      type="boolean"/>
    <Variable
      initialValue="null"
      name="existingAdditionalInsureds"
      recalculateOnRefresh="true"
      type="AccountContactView[]"/>
    <Variable
      initialValue="null"
      name="otherContacts"
      recalculateOnRefresh="true"
      type="AccountContactView[]"/>
    <Variable
      initialValue="gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)"
      name="contactConfigPlugin"
      type="gw.plugin.contact.IContactConfigPlugin"/>
    <Variable
      initialValue="new gw.pcf.contacts.AdditionalInsuredsDVUIHelper()"
      name="helper"
      type="gw.pcf.contacts.AdditionalInsuredsDVUIHelper"/>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured&quot;)"
        visible="displayLabel"/>
      <ListViewInput
        editable="(!(bopBuilding.Branch.Job typeis Submission) or perm.System.editsubmission) &amp;&amp; openForEdit"
        labelAbove="true">
        <Toolbar>
          <AddButton
            hideIfReadOnly="true"
            id="AddContactsButton"
            iterator="AdditionalInsuredLV"
            label="DisplayKey.get(&quot;Web.Contact.Add&quot;)"
            subMenuOnDemand="true">
            <AddMenuItemIterator
              elementName="contactType"
              value="contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYADDLINSURED)"
              valueType="typekey.ContactType[]">
              <IteratorSort
                sortBy="contactType.DisplayName"
                sortOrder="1"/>
              <AddMenuItem
                id="ContactType"
                iterator="AdditionalInsuredLV"
                label="DisplayKey.get(&quot;Web.Contact.AddNewOfType&quot;, contactType)"
                pickLocation="TDIC_NewBuildingAdditionalInsuredPopup.push(bopBuilding, contactType)"/>
            </AddMenuItemIterator>
            <AddMenuItem
              conversionExpression="bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(PickedValue)"
              id="AddFromSearch"
              iterator="AdditionalInsuredLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.FromAddressBook&quot;)"
              pickLocation="ContactSearchPopup.push(TC_ADDITIONALINSURED)"
              visible="false"/>
            <AddMenuItem
              id="AddExistingContact"
              iterator="AdditionalInsuredLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddExisting&quot;, PolicyAddlInsured.Type.TypeInfo.DisplayName)">
              <AddMenuItemIterator
                elementName="additionalInsured"
                id="ContactOfSameType"
                value="getExistingAdditionalInsureds()"
                valueType="entity.AccountContactView[]">
                <IteratorSort
                  sortBy="additionalInsured.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="ExistingAdditionalInsured"
                  iterator="AdditionalInsuredLV"
                  label="additionalInsured"
                  toCreateAndAdd="bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(additionalInsured.AccountContact.Contact)"/>
              </AddMenuItemIterator>
            </AddMenuItem>
            <AddMenuItem
              id="AddOtherContact"
              iterator="AdditionalInsuredLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddOtherContacts&quot;)">
              <AddMenuItemIterator
                elementName="otherContact"
                id="ContactOfOtherType"
                value="getOtherContacts()"
                valueType="entity.AccountContactView[]">
                <IteratorSort
                  sortBy="otherContact.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="OtherContact"
                  iterator="AdditionalInsuredLV"
                  label="otherContact"
                  toCreateAndAdd="bopBuilding.addNewAdditionalInsuredDetailForContact_TDIC(otherContact.AccountContact.Contact)"/>
              </AddMenuItemIterator>
            </AddMenuItem>
          </AddButton>
          <IteratorButtons
            addVisible="false"
            id="IteratorButtons"
            iterator="AdditionalInsuredLV"
            removeVisible="bopBuilding.Branch.Job typeis Submission or bopBuilding.Branch.Job typeis Renewal or bopBuilding.AdditionalInsureds*.PolicyAdditionalInsuredDetails.hasMatch(\elt -&gt; elt.New)"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="setExpirationDate(CheckedValues)"
            available="bopBuilding.Branch.Job typeis PolicyChange"
            id="expirebutton"
            iterator="AdditionalInsuredLV"
            label="DisplayKey.get(&quot;TDIC.Policy.AdditionalInterests.ExpireButton&quot;)"
            visible="bopBuilding.Branch.Job typeis PolicyChange"/>
        </Toolbar>
        <ListViewPanel
          id="AdditionalInsuredLV">
          <RowIterator
            checkBoxVisible="openForEdit"
            editable="true"
            elementName="additionalInsuredDetail"
            hasCheckBoxes="true"
            hideCheckBoxesIfReadOnly="true"
            toRemove="additionalInsuredDetail.PolicyAddlInsured.toRemoveFromPolicyAddIns(additionalInsuredDetail)"
            value="bopBuilding.AdditionalInsureds*.PolicyAdditionalInsuredDetails"
            valueType="entity.PolicyAddlInsuredDetail[]">
            <IteratorSort
              sortBy="additionalInsuredDetail.PolicyAddlInsured"
              sortOrder="1"/>
            <Row>
              <DateCell
                align="left"
                id="effDate"
                label="DisplayKey.get(&quot;TDIC.EffectiveDate&quot;)"
                required="true"
                value="getEffectiveDate(additionalInsuredDetail)//additionalInsuredDetail.EffectiveDate"/>
              <DateCell
                align="left"
                id="expDate"
                label="DisplayKey.get(&quot;TDIC.ExpirationDate&quot;)"
                required="false"
                value="additionalInsuredDetail.ExpirationDate_TDIC"/>
              <TextCell
                action="EditPolicyContactRolePopup.push(additionalInsuredDetail.PolicyAddlInsured, openForEdit)"
                id="Name"
                label="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured.Name&quot;)"
                value="additionalInsuredDetail.PolicyAddlInsured"
                valueType="entity.PolicyAddlInsured"/>
              <RangeCell
                editable="true"
                id="Type"
                label="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured.Type&quot;)"
                required="true"
                value="additionalInsuredDetail.AdditionalInsuredType"
                valueRange="helper.filterAdditionalInsuredTypes_TDIC(bopBuilding.Branch)"
                valueType="typekey.AdditionalInsuredType">
                <PostOnChange
                  deferUpdate="false"
                  onChange="helper.onAdditionalInsuredTypeChange(additionalInsuredDetail)"/>
              </RangeCell>
              <TextCell
                align="left"
                editable="true"
                id="Description"
                label="DisplayKey.get(&quot;TDIC.Web.LineWizard.AdditionalInsured.Desctription&quot;)"
                value="additionalInsuredDetail.Desciption"/>
              <TypeKeyCell
                id="AdditionalInformationType"
                label="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured.AdditionalInformationRequired&quot;)"
                value="additionalInsuredDetail.additionalInformationType(additionalInsuredDetail.AdditionalInsuredType)"
                valueType="AdditionalInformationType"
                visible="additionalInformationVisible"/>
              <TextCell
                align="left"
                editable="helper.additionalInformationRequired(additionalInsuredDetail)"
                id="AdditionalInformation"
                label="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured.AdditionalInformation&quot;)"
                value="additionalInsuredDetail.AdditionalInformation"
                visible="additionalInformationVisible"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputColumn>
    <Code><![CDATA[/*function getExistingAdditionalInsureds() : AccountContactView[] {
  if (existingAdditionalInsureds == null) {
    existingAdditionalInsureds = bopBuilding.ExistingAdditionalInsureds_TDIC.asViews()
  }
  return existingAdditionalInsureds
}*/

function getExistingAdditionalInsureds() : AccountContactView[] {
  if (existingAdditionalInsureds == null) {
    var addedContacts = bopBuilding.AdditionalInsureds*.AccountContactRole*.AccountContact
    var all = bopBuilding.ExistingAdditionalInsureds_TDIC
    var remaining = all?.subtract(addedContacts)?.toTypedArray()?.asViews()
    existingAdditionalInsureds = remaining
  }
  return existingAdditionalInsureds
}

function getOtherContacts() : AccountContactView[] {
  if (otherContacts == null) {
    otherContacts = bopBuilding.AdditionalInsuredOtherCandidates_TDIC.asViews()
  }
  return otherContacts
}

function setExpirationDate(policyInsuredDetail : entity.PolicyAddlInsuredDetail[]) {
  for (insured in policyInsuredDetail) {
    insured.ExpirationDate_TDIC = insured.Branch.EditEffectiveDate
  }
}

function getEffectiveDate(addnl : PolicyAddlInsuredDetail) : Date {
  var effDates = bopBuilding.Branch.AllEffectiveDates.toSet()
  for(effDate in effDates.order()) {
    var version = addnl.VersionList.AsOf(effDate)
    if(version != null) {
      return version.EffectiveDate
    }
  }
  return addnl.EffectiveDate
}
]]></Code>
  </DetailViewPanel>
</PCF>