<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="pcf.xsd">
  <Popup
    canEdit="false"
    id="TDIC_ReinsuranceSearchResultsPopup"
    title="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.Result.Title&quot;)">
    <LocationEntryPoint
      signature="TDIC_ReinsuranceSearchResultsPopup(reinsuranceSearchResult : tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult)"/>
    <Variable
      name="reinsuranceSearchResult"
      type="tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult"/>
    <Variable
      initialValue="gw.api.util.DateUtil.currentDate()"
      name="retrieveAsOfDate"
      type="Date"/>
    <Screen>
      <DetailViewPanel>
        <InputColumn>
          <TextInput
            id="Location"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.Location&quot;)"
            value="reinsuranceSearchResult.RelatedAddressLine1"/>
          <TextInput
            id="ControlAddress"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.ControlAddress&quot;)"
            value="(reinsuranceSearchResult.ControlAddress != null)? reinsuranceSearchResult.ControlAddress : &quot;&quot;"/>
        </InputColumn>
        <InputColumn>
          <TextInput
            id="Matches"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.Matches&quot;)"
            value="reinsuranceSearchResult.ReinsuranceGroup.Count"
            valueType="Integer"/>
          <TextInput
            id="TotalEmployees"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.TotalEmployees&quot;)"
            value="reinsuranceSearchResult.TotalEmployees_TDIC"
            valueType="Integer"
            visible="reinsuranceSearchResult.TotalEmployees_TDIC &gt; 0"/>
          <TextInput
            id="TotalBasis"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.TotalBasis&quot;)"
            value="reinsuranceSearchResult.TotalBasis_TDIC"
            valueType="Integer"
            visible="reinsuranceSearchResult.TotalBasis_TDIC &gt; 0"/>
          <TextInput
            formatType="currency"
            id="TotalValue"
            label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.TotalPropertyValue&quot;)"
            value="reinsuranceSearchResult.ReinsuranceGroup.hasMatch(\elt -&gt; elt.AssociatedPolicyPeriod?.BOPLineExists) ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getTotalBOPLinePropertyValue(reinsuranceSearchResult.ReinsuranceGroup) : null"
            valueType="java.math.BigDecimal"/>
        </InputColumn>
      </DetailViewPanel>
      <PanelRef>
        <Toolbar/>
        <ListViewPanel>
          <RowIterator
            editable="false"
            elementName="reinsuranceGroupEntry"
            id="ReinsuranceGroupLV"
            value="reinsuranceSearchResult.ReinsuranceGroup.orderBy( \ elt -&gt; elt.ReinsuranceSearchTag_TDIC)"
            valueType="gw.util.IOrderedList&lt;entity.PolicyLocation&gt;">
            <IteratorSort
              sortBy="reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber"
              sortOrder="1"/>
            <IteratorSort
              sortBy="tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBuildingYearBuilt(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum).intValue()"
              sortOrder="2"/>
            <IteratorSort
              sortBy="reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodStart"
              sortOrder="3"/>
            <IteratorSort
              sortBy="reinsuranceGroupEntry.AssociatedPolicyPeriod.PrimaryNamedInsured"
              sortOrder="4"/>
            <IteratorSort
              sortBy="tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBOPLinePropertyValue(reinsuranceGroupEntry).DisplayValue"
              sortOrder="5"/>
            <Row>
              <TextCell
                action="PolicyFileForward.go(reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber, retrieveAsOfDate)"
                id="PolicyNumber"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.PolicyNumber&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.PolicyNumber"/>
              <TextCell
                id="PrimaryNamedInsured"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.PrimaryNamedInsured&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.PrimaryNamedInsured"
                valueType="entity.PolicyPriNamedInsured"/>
              <DateCell
                id="EffectiveDate"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.EffectiveDate&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodStart"/>
              <DateCell
                id="ExpirationDate"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.ExpirationDate&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.PeriodEnd"/>
              <TextCell
                id="LocationNumber"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.LocationNumber&quot;)"
                value="reinsuranceGroupEntry.LocationNum"
                valueType="Integer"/>
              <TextCell
                id="AddressLines1and2"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.AddressLinesOneAndTwo&quot;)"
                value="reinsuranceGroupEntry.AddressLine1 +&quot;\n&quot;+ (reinsuranceGroupEntry.AddressLine2 == null ? &quot;&quot; : reinsuranceGroupEntry.AddressLine2)"/>
              <TextCell
                id="City"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.City&quot;)"
                value="reinsuranceGroupEntry.City"/>
              <TextCell
                id="State"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.State&quot;)"
                value="reinsuranceGroupEntry.State"
                valueType="typekey.State"/>
              <TextCell
                id="PostalCode"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.PostalCode&quot;)"
                value="reinsuranceGroupEntry.PostalCode"/>
              <TextCell
                id="YearBuilt"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.YearBuilt&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBuildingYearBuilt(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null"
                valueType="Integer"/>
              <TextCell
                id="ConstructionType"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.ConstructionType&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getConstructionType(reinsuranceGroupEntry.AssociatedPolicyPeriod.BOPLine, reinsuranceGroupEntry.LocationNum) : null"/>
              <TextCell
                formatType="currency"
                id="PropertyValue"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.PropertyValue&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod?.BOPLineExists ? tdic.pc.config.pcf.ReinsuranceSearchResultsHelper_TDIC.getBOPLinePropertyValue(reinsuranceGroupEntry) : null"
                valueType="java.math.BigDecimal"/>
              <TextCell
                id="NumberOfEmployees"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.NumEmployees&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalNumEmployeesForPolicyLocation_TDIC(reinsuranceGroupEntry) : null"
                valueType="Integer"
                visible="isBasisAndNumberOfEmpVisible()"/>
              <TextCell
                id="Basis"
                label="DisplayKey.get(&quot;TDIC.Web.ReinsuranceSearch.Basis&quot;)"
                value="reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7LineExists? reinsuranceGroupEntry.AssociatedPolicyPeriod.WC7Line?.getTotalBasisForPolicyLocation_TDIC(reinsuranceGroupEntry) : null"
                valueType="Integer"
                visible="isBasisAndNumberOfEmpVisible()"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </PanelRef>
    </Screen>
    <Code><![CDATA[function isBasisAndNumberOfEmpVisible() : boolean {
  return reinsuranceSearchResult.ReinsuranceGroup.hasMatch(\polLoc -> polLoc.AssociatedPolicyPeriod.WC7LineExists)
}]]></Code>
  </Popup>
</PCF>