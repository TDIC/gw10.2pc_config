<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <InputSet
    id="AuditProcessingInputSet"
    mode="FinalAudit">
    <Require
      name="audit"
      type="Audit"/>
    <Variable
      initialValue="CurrentLocation as pcf.api.Wizard"
      name="wizard"
      type="pcf.api.Wizard"/>
    <Variable
      initialValue="loadAvailableAuditors_TDIC()"
      name="AvailableAuditors_TDIC"
      type="java.util.List&lt;User&gt;"/>
    <Label
      label="DisplayKey.get(&quot;Web.AuditWizard.Processing&quot;)"/>
    <DateInput
      id="AccountFlaggedForPhysicalAudit"
      label="DisplayKey.get(&quot;TDIC.Web.AuditWizard.AccountFlaggedForPhysicalAudit&quot;)"
      value="audit.AuditInformation.PhysicalAuditFlagged_TDIC"
      visible="audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL"/>
    <DateInput
      id="InitDate"
      label="DisplayKey.get(&quot;Web.AuditWizard.StartDate&quot;)"
      value="audit.AuditInformation.InitDate"/>
    <DateInput
      editable="perm.Audit.reschedule"
      id="DueDate"
      label="DisplayKey.get(&quot;Web.AuditWizard.DueDate&quot;)"
      value="audit.AuditInformation.DueDate"
      visible="not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL)"/>
    <DateInput
      available="(audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED) and audit.AuditInformation.IsComplete and audit.PolicyPeriod.WC7LineExists"
      id="EstimatedDate"
      label="DisplayKey.get(&quot;TDIC.Web.AuditWizard.EstimatedDate&quot;)"
      value="audit.CloseDate"
      visible="(audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED) and audit.AuditInformation.IsComplete and audit.PolicyPeriod.WC7LineExists"/>
    <DateInput
      id="NoticeSentToPolicyHolder"
      label="DisplayKey.get(&quot;TDIC.Web.AuditWizard.NoticeSentToPolicyHolder&quot;)"
      value="audit.AuditInformation.NoticeSentHolder_TDIC"
      visible="audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL"/>
    <DateInput
      editable="true"
      id="NoticeSentToAuditor"
      label="DisplayKey.get(&quot;TDIC.Web.AuditWizard.NoticeSentToAuditor&quot;)"
      value="audit.AuditInformation.NoticeSentAuditor_TDIC"
      visible="audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL">
      <PostOnChange
        deferUpdate="false"/>
    </DateInput>
    <DateInput
      editable="true"
      id="AppointmentSet"
      label="DisplayKey.get(&quot;TDIC.Web.AuditWizard.AppointmentSet&quot;)"
      value="audit.AuditInformation.AppointmentDate_TDIC"
      visible="audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_PHYSICAL">
      <PostOnChange
        deferUpdate="false"/>
    </DateInput>
    <DateInput
      available="not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"
      editable="true"
      id="ReceivedDate"
      label="DisplayKey.get(&quot;Web.AuditWizard.ReceivedDate&quot;)"
      required="false"
      value="audit.AuditInformation.ReceivedDate"
      visible="not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)">
      <PostOnChange
        deferUpdate="false"
        onChange="audit.AuditInformation.onReceivedDateFieldChange(); wizard.saveDraft()"/>
    </DateInput>
    <DateInput
      available="not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"
      editable="true"
      id="ReadyForReviewDate"
      label="DisplayKey.get(&quot;Web.AuditWizard.ReadyForReviewDate&quot;)"
      required="false"
      value="audit.AuditInformation.ReadyForReviewDate"
      visible="not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)">
      <PostOnChange
        deferUpdate="false"
        onChange="audit.AuditInformation.onReadyToReviewDateFieldChange(); wizard.saveDraft()"/>
    </DateInput>
    <TextInput
      available="not audit.PolicyPeriod.WC7LineExists"
      id="Auditor"
      label="DisplayKey.get(&quot;Web.AuditWizard.Auditor&quot;)"
      value="audit.getUserRoleAssignmentByRole(TC_AUDITOR).AssignedUser.DisplayName"
      visible="not audit.PolicyPeriod.WC7LineExists"/>
    <RangeInput
      available="audit.PolicyPeriod.WC7LineExists and not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"
      editable="true"
      id="Auditor_TDIC"
      label="DisplayKey.get(&quot;Web.AuditWizard.Auditor&quot;)"
      multiSelect="false"
      required="true"
      value="audit.Auditor_TDIC"
      valueRange="AvailableAuditors_TDIC"
      valueType="entity.User"
      visible="audit.PolicyPeriod.WC7LineExists and not (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"/>
    <TextInput
      available="audit.PolicyPeriod.WC7LineExists and (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"
      id="TdicAuditorEstimate"
      label="DisplayKey.get(&quot;Web.AuditWizard.Auditor&quot;)"
      value="&quot;System&quot;"
      visible="audit.PolicyPeriod.WC7LineExists and (audit.AuditInformation.ActualAuditMethod == typekey.AuditMethod.TC_ESTIMATED)"/>
    <MonetaryAmountInput
      __disabled="true"
      currency="audit.Currency"
      desc="US463: Shane Sheridan: hide this field."
      editable="true"
      formatType="currency"
      id="AuditFee"
      label="DisplayKey.get(&quot;Web.AuditWizard.AuditFee&quot;)"
      value="audit.AuditInformation.AuditFee"/>
    <TextAreaInput
      __disabled="true"
      desc="US463: Shane Sheridan: hide this field."
      editable="true"
      id="Instructions"
      label="DisplayKey.get(&quot;Web.AuditWizard.Summary.Instructions&quot;)"
      numRows="5"
      value="audit.AuditInformation.Instructions"/>
    <Code><![CDATA[uses gw.api.database.Query
uses java.util.ArrayList

public function loadAvailableAuditors_TDIC() : java.util.List<User> {
  var auditors = new ArrayList<User>();
  var user : User;
  
  // Find all groups with the auditor group type.
  var groupType = audit.LatestPeriod.AuditProcess.AuditorGroupType_TDIC;
  var query = Query.make(Group).compare(Group#GroupType, Equals, groupType);
  var groups = query.select();
  
  // Build a unique list of active users from all the groups
  for (group in groups) {
    for (groupUser in group.Users) {
      user = groupUser.User;    
      if (user.Credential.Active) {
        if (auditors.contains(groupUser.User) == false) {
          auditors.add (groupUser.User);
        }
      }
    }
  }

  return auditors;
}]]></Code>
  </InputSet>
</PCF>