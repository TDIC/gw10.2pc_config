<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <Page
    afterCancel="PolicyFileForward.go(policyPeriod.PolicyNumber)"
    autosaveable="false"
    canEdit="true"
    canVisit="perm.PolicyPeriod.view(policyPeriod) and perm.Cancellation.create"
    countsAsWork="false"
    id="StartCancellation"
    parent="PolicyFile(policyPeriod, policyPeriod.EditEffectiveDate)"
    startInEditMode="true"
    title="DisplayKey.get(&quot;Web.Cancellation.StartCancellation&quot;, policyPeriod.PolicyNumberDisplayString)">
    <LocationEntryPoint
      signature="StartCancellation(policyPeriod : PolicyPeriod)"/>
    <Variable
      name="policyPeriod"
      type="PolicyPeriod"/>
    <Variable
      initialValue="policyPeriod.Policy"
      name="policy"
      type="Policy"/>
    <Variable
      initialValue="new Cancellation()"
      name="job"
      type="Cancellation"/>
    <Variable
      initialValue="null"
      name="effectiveDate"
      type="java.util.Date"/>
    <Variable
      initialValue="(effectiveDate != null) ? entity.Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(policy, effectiveDate) : null"
      name="inForcePeriod"
      recalculateOnRefresh="true"
      type="PolicyPeriod"/>
    <Variable
      initialValue="inForcePeriod != null and inForcePeriod.hasFinalAuditFinished()"
      name="hasFinalAuditFinished"
      recalculateOnRefresh="true"
      type="boolean"/>
    <Variable
      initialValue="effectiveDate != null and policy.isOOSChange(effectiveDate)"
      name="isOOSChange"
      recalculateOnRefresh="true"
      type="boolean"/>
    <Variable
      initialValue="job.findValidRefundMethods_TDIC(policyPeriod)"
      name="refundMethods"
      recalculateOnRefresh="true"
      type="typekey.CalculationMethod[]"/>
    <Variable
      initialValue="null"
      name="refundCalcMethod"
      type="CalculationMethod"/>
    <Screen
      id="StartCancellationScreen">
      <Toolbar>
        <ToolbarButton
          action="if (job.startJobAndCommit(policy, effectiveDate, refundCalcMethod, CurrentLocation)) {CancellationWizard.go(job, job.LatestPeriod)                         }"
          available="policy.canStartCancellation(effectiveDate) == null "
          confirmMessage="confirmationMessage()"
          id="NewCancellation"
          label="DisplayKey.get(&quot;Button.StartCancellation&quot;)"
          visible="not (policyPeriod.ShouldQuoteAsynchronously and gw.api.system.PCConfigParameters.AsynchronousQuotingEnabled.Value)"/>
        <ToolbarButton
          action="job.QuoteOnStart = false;if (job.startJobAndCommit(policy, effectiveDate, refundCalcMethod, CurrentLocation)) {CancellationWizard.go(job, job.LatestPeriod)                         }"
          available="policy.canStartCancellation(effectiveDate) == null"
          confirmMessage="confirmationMessage()"
          id="NewAsyncCancellation"
          label="DisplayKey.get(&quot;Button.StartCancellation&quot;)"
          visible="policyPeriod.ShouldQuoteAsynchronously and gw.api.system.PCConfigParameters.AsynchronousQuotingEnabled.Value"/>
        <ToolbarButton
          action="CurrentLocation.cancel()"
          id="Cancel"
          label="DisplayKey.get(&quot;Button.Cancel&quot;)"/>
      </Toolbar>
      <Verbatim
        id="ErrorMessage"
        label="DisplayKey.get(&quot;Web.Cancellation.Error.CannotStart&quot;, policy.canStartCancellation(effectiveDate))"
        visible="effectiveDate != null and policy.canStartCancellation(effectiveDate) != null"
        warning="true"/>
      <Verbatim
        id="WarningMessage"
        label="DisplayKey.get(&quot;Web.PolicyChange.StartPolicyChange.Warning&quot;, getWarningMessage(policyPeriod))"
        visible="getWarningMessage(policyPeriod) != null"
        warning="true"/>
      <DetailViewPanel
        id="CancelPolicyDV">
        <InputColumn>
          <!-- The cancellation source determines which reason codes are available -->
          <TypeKeyInput
            editable="true"
            filter="perm.System.cancelcarriersource or VALUE == typekey.CancellationSource.TC_INSURED"
            id="Source"
            label="DisplayKey.get(&quot;Web.CancellationWizard.Source&quot;)"
            required="true"
            value="job.Source"
            valueType="typekey.CancellationSource">
            <PostOnChange
              onChange="job.CancelReasonCode = null; refundCalcMethod = null; effectiveDate = null"/>
          </TypeKeyInput>
          <!-- Setting the reason code can also set a default refund calculation method and effective date -->
          <RangeInput
            editable="true"
            filter="job.TDIC_filterReasonCodesForCancellation(VALUE)"
            id="Reason"
            label="DisplayKey.get(&quot;Web.CancellationWizard.Reason&quot;)"
            required="true"
            value="job.CancelReasonCode"
            valueRange="tdic.web.pcf.helper.PolicyCancellationScreenHelper.getFilteredCancellationReasons(job, policyPeriod)"
            valueType="typekey.ReasonCode">
            <PostOnChange
              onChange="setRefundMethod(); setEffectiveDate();//refundCalcMethod = job.calculateRefundCalcMethod(policyPeriod); effectiveDate = job.CancelReasonCode != null ? job.getDefaultEffectiveDate(policyPeriod, refundCalcMethod) : effectiveDate"/>
          </RangeInput>
          <!-- The description field doesn't affect any other fields on this page -->
          <TextAreaInput
            editable="true"
            id="ReasonDescription"
            label="DisplayKey.get(&quot;Web.CancellationWizard.ReasonDescription&quot;)"
            numRows="3"
            validationExpression="job.hasDescriptionContainsSpecialChars()"
            value="job.Description"/>
          <!-- Refund calculation method is not editable for rewrites -->
          <RangeInput
            editable="perm.System.canceloverrefund and job.CancelReasonCode != TC_FLATREWRITE and job.CancelReasonCode != TC_MIDTERMREWRITE and refundMethods.Count != 1"
            id="CalcMethod"
            label="DisplayKey.get(&quot;Web.CancellationWizard.CalcMethod&quot;)"
            required="true"
            value="refundCalcMethod"
            valueRange="refundMethods"
            valueType="typekey.CalculationMethod">
            <PostOnChange
              onChange="effectiveDate = job.CancelReasonCode != null ? job.getDefaultEffectiveDate(policyPeriod, refundCalcMethod) : effectiveDate"/>
          </RangeInput>
          <!-- Effective date is only editable if the refund method is not "flat" and the period is issued.  If date's time component is not editable, need to apply effective time plugin. -->
          <DateInput
            editable="job.canEnterInitialEffectiveDate_TDIC(policyPeriod, effectiveDate, refundCalcMethod)"
            id="CancelDate"
            label="DisplayKey.get(&quot;Web.CancellationWizard.CancelDate&quot;)"
            required="true"
            validationExpression="job.validateEffectiveDate(policyPeriod, effectiveDate, refundCalcMethod)"
            value="effectiveDate">
            <PostOnChange
              onChange="if (effectiveDate != null) { effectiveDate = gw.api.job.EffectiveDateCalculator.instance().getCancellationEffectiveDate(effectiveDate, policyPeriod, job, refundCalcMethod); resetRefundMethod() }"/>
          </DateInput>
        </InputColumn>
      </DetailViewPanel>
    </Screen>
    <Code><![CDATA[function confirmationMessage() : String {
  if(hasFinalAuditFinished){
    return (effectiveDate == inForcePeriod.PeriodStart)
      ? DisplayKey.get("Web.Job.FinalAuditCompletedFlatCancellation")
      : DisplayKey.get("Web.Job.FinalAuditCompletedCancellation")
  }
  return ""
}

function getWarningMessage(pInForcePeriod : PolicyPeriod) : String {
  if ((pInForcePeriod != null) and (pInForcePeriod.Policy.RewrittenToNewAccountDestination != null)) {
    return DisplayKey.get("Web.Job.ChangeRewriteNewAccountPolicy", pInForcePeriod.Policy.RewrittenToNewAccountDestination.LatestBoundPeriod.PolicyNumber)
  }
  return null
}

function setRefundMethod() {
  refundCalcMethod = job.calculateRefundCalcMethod(policyPeriod)
}

function resetRefundMethod() {
  if(effectiveDate != null and effectiveDate.compareIgnoreTime(policyPeriod.PeriodStart) == 0) {
    if(job.findValidRefundMethods_TDIC(policyPeriod).contains(CalculationMethod.TC_FLAT)) {
      refundCalcMethod = CalculationMethod.TC_FLAT
    }
  }
}
function setEffectiveDate() {
  effectiveDate = job.CancelReasonCode != null ? job.getDefaultEffectiveDate(policyPeriod, refundCalcMethod) : effectiveDate
}
]]></Code>
  </Page>
</PCF>