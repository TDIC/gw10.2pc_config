<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <Screen
    id="PolicyFile_Coverage_GLScreen">
    <Require
      name="policyPeriod"
      type="PolicyPeriod"/>
    <Variable
      initialValue="policyPeriod.getGLUWQuestionSets_TDIC()"
      name="underwritingQuestionSets"
      recalculateOnRefresh="true"
      type="gw.api.productmodel.QuestionSet[]"/>
    <Variable
      initialValue="policyPeriod.GLLine"
      name="glLine"
      type="productmodel.GLLine"/>
    <Variable
      initialValue="setInitialQuoteType()"
      name="quoteType"
      recalculateOnRefresh="true"
      type="QuoteType"/>
    <PanelRef
      def="PreferredCoverageCurrencyPanelSet(policyPeriod.GLLine, false, null)"
      id="PreferredCoverageCurrencySelectorRef"/>
    <CardViewPanel>
      <Card
        id="GeneralLiability_IncludedCard"
        title="DisplayKey.get(&quot;Web.LineWizard.StandardCoverages&quot;)">
        <PanelRef
          available="policyPeriod.getLineExists(policyPeriod.GLLine.Pattern)"
          def="PolicyLineDV(policyPeriod.GLLine, null)"
          mode="policyPeriod.GLLine.Pattern.PublicID"/>
      </Card>
      <Card
        id="GLAdditionalCovCard"
        title="DisplayKey.get(&quot;Web.LineWizard.AdditionalCoverages&quot;)">
        <PanelRef
          def="GLAdditionalCoveragesDV(glLine) //AdditionalCoveragesDV(policyPeriod.GLLine, new String[]{&quot;GLGroup&quot;}, false)"/>
      </Card>
      <Card
        __disabled="true"
        id="ExclusionsAndConditionsCard"
        title="DisplayKey.get(&quot;Web.LineWizard.ExclusionsAndConditions&quot;)">
        <PanelRef
          def="AdditionalExclusionsAndConditionsPanelSet(policyPeriod.GLLine, policyPeriod.GLLine.getAdditionalCoverageCategories() , true)"/>
      </Card>
      <Card
        id="GeneralLiability_AdditionalInsuredCard"
        title="DisplayKey.get(&quot;Web.LineWizard.AdditionalInsured&quot;)"
        visible="policyPeriod.Offering.CodeIdentifier != &quot;PLCyberLiab_TDIC&quot;">
        <PanelRef
          def="TDIC_AdditionalInsuredsDV(policyPeriod.GLLine, false, true, false)"/>
      </Card>
      <Card
        id="GeneralLiability_CertificateHoldersCard"
        title="DisplayKey.get(&quot;TDIC.Web.Policy.Coverages.CertificateHolders&quot;)"
        visible="policyPeriod.Offering.CodeIdentifier != &quot;PLCyberLiab_TDIC&quot;">
        <PanelRef
          def="TDIC_CertificateHolderDetailsDV(policyPeriod.GLLine, false)"/>
      </Card>
      <Card
        id="GeneralLiability_PremiumModificationCardGene"
        title="DisplayKey.get(&quot;TDIC.Web.Policy.Coverages.PremiumModifications&quot;)"
        visible="!(policyPeriod.Offering.CodeIdentifier == &quot;PLCyberLiab_TDIC&quot;)">
        <PanelSet
          id="PLPMPanelSet">
          <PanelRef
            def="TDIC_GLDiscountsDV(policyPeriod.GLLine,null)"/>
        </PanelSet>
        <PanelRef
          def="GLRiskManagementDiscount_TDICDV(policyPeriod.GLLine)"
          visible="policyPeriod.GLLine.GLRiskMgmtDiscount_TDICExists"/>
        <PanelSet
          id="PLIRPMPanelSet">
          <PanelRef
            def="TDIC_IRPMDiscountDV(glLine,quoteType)"/>
        </PanelSet>
        <PanelRef
          def="TDIC_GLIRPMModifiersDV(policyPeriod.GLLine)"
          visible="policyPeriod.GLLine.GLIRPMDiscount_TDICExists">
          <TitleBar
            title="DisplayKey.get(&quot;Web.ModifiersScreen.RatingInputs&quot;)"
            visible="policyPeriod.GLLine.GLIRPMDiscount_TDICExists //true"/>
        </PanelRef>
      </Card>
      <Card
        id="GeneralLiability_UWInformationCard"
        title="DisplayKey.get(&quot;TDIC.Web.Policy.Coverages.UWInformation&quot;)">
        <PanelRef
          def="QuestionSets_TDICDV(underwritingQuestionSets,glLine.PolicyLine, null,false)"
          id="QuestionSet"/>
        <DetailViewPanel
          visible="(policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; and glLine.getAnswerForGLUWQuestion_TDIC(&quot;HoldDentalLicenseInOtherStates_TDIC&quot;)) or ((policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; or policyPeriod.Offering.CodeIdentifier == &quot;PLOccurence_TDIC&quot;) and glLine.getAnswerForGLUWQuestion_TDIC(&quot;HoldDentalLicensesInOtherStatesPLCMOC_TDIC&quot;)) and policyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION and !policyPeriod.isQuickQuote_TDIC">
          <InputColumn>
            <ListViewInput
              id="GLDentLicenseStatesLV"
              label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.DentalLicenseInOtherStates&quot;)"
              labelAbove="true">
              <Toolbar>
                <IteratorButtons
                  iterator="DentLicenseStatesLV"/>
              </Toolbar>
              <ListViewPanel
                id="DentLicenseStatesLV"
                validationExpression="glLine.GLDentLicenseStates_TDIC?.Count == 0 ? DisplayKey.get(&quot;TDIC.Web.Policy.GL.DentalLicenseInOtherStatesValidation&quot;) : null">
                <RowIterator
                  editable="true"
                  elementName="dentLicenseState"
                  toCreateAndAdd="glLine.createAndAddGLDentLicenseStates_TDIC()"
                  toRemove="glLine.removeFromGLDentLicenseStates_TDIC(dentLicenseState)"
                  value="glLine.GLDentLicenseStates_TDIC"
                  valueType="entity.GLDentLicenseStates_TDIC[]">
                  <Row>
                    <TypeKeyCell
                      editable="true"
                      filter="VALUES.where(\elt -&gt; elt.hasCategory(Country.TC_US)).toList()"
                      id="state"
                      label="DisplayKey.get(&quot;Web.Policy.GL.Quote.Jurisdiction&quot;)"
                      required="true"
                      value="dentLicenseState.State"
                      valueType="typekey.State"/>
                    <TypeKeyCell
                      editable="true"
                      id="practicedinthisstate"
                      label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.PracticedInThisState&quot;)"
                      required="true"
                      value="dentLicenseState.PracticedInThisState_TDIC"
                      valueType="typekey.YesNo_TDIC"/>
                    <TypeKeyCell
                      editable="true"
                      id="plantopracticeinthisstate"
                      label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.PlanToPracticeInThisState&quot;)"
                      required="true"
                      value="dentLicenseState.PlantoPracticeInThisState_TDIC"
                      valueType="typekey.YesNo_TDIC"/>
                    <TextCell
                      editable="true"
                      id="datesofpractice"
                      label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.DatesOfPractice&quot;)"
                      maxChars="80"
                      required="true"
                      value="dentLicenseState.DatesOfPractice_TDIC"/>
                  </Row>
                </RowIterator>
              </ListViewPanel>
            </ListViewInput>
          </InputColumn>
        </DetailViewPanel>
        <DetailViewPanel>
          <InputColumn
            labelWidth="700">
            <TypeKeyCheckBoxGroupInput
              __disabled="true"
              editable="true"
              id="DoYouPerformProvideBelowServices"
              label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.DoYouPerformProvideBelowServices&quot;)"
              required="true"
              value="glLine.GLDentistServicesOfLine_TDIC"
              valueType="typekey.DentistServices_TDIC[]"
              visible="(policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; and glLine.getAnswerForGLUWQuestion_TDIC(&quot;LicensedFirstTimeInLast12Months_TDIC&quot;)) or ((policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; or policyPeriod.Offering.CodeIdentifier == &quot;PLOccurence_TDIC&quot;) and !glLine.getAnswerForGLUWQuestion_TDIC(&quot;LicensedFirstTimeInLast12Months_TDIC&quot;)) and policyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION and !policyPeriod.isQuickQuote_TDIC">
              <PostOnChange/>
            </TypeKeyCheckBoxGroupInput>
            <TypeKeyCheckBoxGroupInput
              __disabled="true"
              editable="true"
              id="TreatPatientsUnderBelowAnestheticModalities"
              label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.TreatPatientsUnderBelowAnestheticModalities&quot;)"
              required="true"
              value="glLine.GLAnestheticModalitiesOfLine_TDIC"
              valueType="typekey.AnestheticModilities_TDIC[]"
              visible="(policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; and glLine.getAnswerForGLUWQuestion_TDIC(&quot;LicensedFirstTimeInLast12Months_TDIC&quot;)) or ((policyPeriod.Offering.CodeIdentifier == &quot;PLClaimsMade_TDIC&quot; or policyPeriod.Offering.CodeIdentifier == &quot;PLOccurence_TDIC&quot;) and !glLine.getAnswerForGLUWQuestion_TDIC(&quot;LicensedFirstTimeInLast12Months_TDIC&quot;)) and !policyPeriod.isQuickQuote_TDIC">
              <PostOnChange
                onChange="glLine.syncQuestions()"/>
            </TypeKeyCheckBoxGroupInput>
          </InputColumn>
        </DetailViewPanel>
      </Card>
      <Card
        __disabled="true"
        id="GeneralLiability_AdditionalCoveragesCard"
        title="DisplayKey.get(&quot;Web.LineWizard.AdditionalCoverages&quot;)">
        <PanelRef
          def="AdditionalCoveragesDV(policyPeriod.GLLine, new String[]{&quot;GLGroup&quot;}, false)"/>
      </Card>
    </CardViewPanel>
    <Code><![CDATA[function setInitialQuoteType() : QuoteType {
      if (policyPeriod.Job typeis Submission) {
        return (policyPeriod.Job as Submission).QuoteType
      }
      return null
    }]]></Code>
  </Screen>
</PCF>