<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <!-- The reason a specific input set is used for this coverage is that the
coverage requires a list view of the schedule items. -->
  <InputSet
    id="CoverageInputSet"
    mode="GLExcludedServiceCond_TDIC">
    <Require
      name="coveragePattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable.PolicyLine as GLLine"
      name="glLine"
      type="productmodel.GLLine"/>
    <Variable
      initialValue="coveragePattern"
      name="scheduledEquipmentPattern"
      type="gw.api.productmodel.ClausePattern"/>
    <InputGroup
      allowToggle="coveragePattern.allowToggle(coverable)"
      alwaysShowCheckbox="true"
      childrenVisible="coverable.getCoverageConditionOrExclusion(coveragePattern) != null"
      id="GLSchedEquipInputGroup"
      label="scheduledEquipmentPattern.DisplayName"
      onToggle="glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )">
      <ListViewInput
        boldLabel="true"
        editable="true"
        id="GLSchedEquipLV"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="EquipmentLV"
            removeVisible="glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLStateExclSched_TDIC.hasMatch(\sched -&gt; sched.BasedOn == null)"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="setExpirationDate(CheckedValues)"
            available="glLine.Branch.Job typeis PolicyChange"
            id="expirebutton"
            iterator="EquipmentLV"
            label="DisplayKey.get(&quot;TDIC.Policy.AdditionalInterests.ExpireButton&quot;)"
            visible="glLine.Branch.Job typeis PolicyChange"/>
        </Toolbar>
        <ListViewPanel
          id="EquipmentLV">
          <RowIterator
            editable="true"
            elementName="scheduledItem"
            hasCheckBoxes="true"
            numEntriesRequired="0"
            toCreateAndAdd="glLine.createAndAddExcludedServiceSched_TDIC()"
            toRemove="glLine.toRemoveFromExcludedServiceSched_TDIC(scheduledItem) "
            value="glLine.GLExcludedServiceSched_TDIC"
            valueType="entity.GLExcludedServiceSched_TDIC[]">
            <Row>
              <DateCell
                align="left"
                id="EndorsementEffDate"
                label="DisplayKey.get(&quot;Coverage.GL.Schedule.EndEffDate_TDIC&quot;)"
                value="scheduledItem.LTEffecticeDate"/>
              <DateCell
                align="center"
                id="EndorsementExpDate"
                label="DisplayKey.get(&quot;Coverage.GL.EndExpDate_TDIC&quot;)"
                value="scheduledItem.LTExpirationDate"/>
              <TextCell
                align="right"
                editable="true"
                id="ExcludedEntity"
                label="DisplayKey.get(&quot;Coverage.GL.Sched.ExcludedEntity_TDIC&quot;)"
                required="true"
                value="scheduledItem.ExcludedEntity"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputGroup>
    <InputDivider
      visible="isStateExclCovVisible()"/>
    <Code><![CDATA[function isStateExclCovVisible() : boolean {
  if (coveragePattern.CodeIdentifier == "GLExcludedServiceCond_TDIC") {
    if (((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and    coverable.PolicyLine.Branch.Status      == PolicyPeriodStatus.TC_QUOTED) or
          coverable.PolicyLine.Branch.Job.Subtype == TC_RENEWAL or        coverable.PolicyLine.Branch.Job.Subtype == TC_CANCELLATION or
          coverable.PolicyLine.Branch.Job.Subtype == TC_REINSTATEMENT or  coverable.PolicyLine.Branch.Job.Subtype == TC_REWRITE) ) {
      return coveragePattern != null and coverable.PolicyLine.Branch.GLLine.GLExcludedServiceCond_TDICExists

    } else if ((coverable.PolicyLine.Branch.Job.Subtype == TC_SUBMISSION and coverable.PolicyLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT) or
                coverable.PolicyLine.Branch.Job.Subtype == TC_POLICYCHANGE) {
      return true
    }
  }
  // as before for other coverages in .default PCF file
  return (openForEdit or coverable.getCoverageConditionOrExclusion(coveragePattern) != null)
}

function setExpirationDate(ExcludeServConds : GLExcludedServiceSched_TDIC[]) {
  ExcludeServConds.each(\ExcludeServCond -> {
    if(ExcludeServCond.LTExpirationDate == null) {
      ExcludeServCond.LTExpirationDate = ExcludeServCond.Branch.EditEffectiveDate
    }
  })
}

]]></Code>
  </InputSet>
</PCF>