<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <!-- The reason a specific input set is used for this coverage is that the
coverage requires a list view of the schedule items. -->
  <InputSet
    id="CoverageInputSet"
    mode="GLManuscriptEndor_TDIC">
    <Require
      name="coveragePattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable.PolicyLine as GLLine"
      name="glLine"
      type="productmodel.GLLine"/>
    <Variable
      initialValue="coveragePattern"
      name="scheduledEquipmentPattern"
      type="gw.api.productmodel.ClausePattern"/>
    <InputGroup
      allowToggle="coveragePattern.allowToggle(coverable) and perm.System.editmanuscript_tdic"
      alwaysShowCheckbox="true"
      childrenVisible="coverable.getCoverageConditionOrExclusion(coveragePattern) != null"
      id="GLManuscriptInputGroup"
      label="DisplayKey.get(&quot;TDIC.PolicyLine_BOPLine.CoveragePattern_BOPManuscriptEndorsement_TDIC&quot;)"
      onToggle="glLine.setCoverageConditionOrExclusionExists( scheduledEquipmentPattern, VALUE )">
      <ListViewInput
        boldLabel="true"
        editable="perm.System.editmanuscript_tdic"
        id="GLManuscriptLV"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="ManuscriptLV"
            removeVisible="glLine.Branch.Job typeis Submission or glLine.Branch.Job typeis Renewal or glLine.GLManuscript_TDIC.hasMatch(\elt1 -&gt; elt1.New)"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="setExpirationDate(CheckedValues)"
            available="glLine.Branch.Job typeis PolicyChange"
            id="expirebutton"
            iterator="ManuscriptLV"
            label="DisplayKey.get(&quot;TDIC.Policy.AdditionalInterests.ExpireButton&quot;)"
            visible="glLine.Branch.Job typeis PolicyChange"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="resetAiOrCh(CheckedValues)"
            available="glLine.Branch.Job typeis PolicyChange"
            id="resetButton"
            iterator="ManuscriptLV"
            label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Coverages.Reset&quot;)"
            visible="glLine.Branch.Job typeis PolicyChange"/>
        </Toolbar>
        <ListViewPanel
          id="ManuscriptLV">
          <RowIterator
            editable="true"
            elementName="scheduledItem"
            hasCheckBoxes="true"
            hideCheckBoxesIfReadOnly="true"
            toCreateAndAdd="glLine.createAndAddGLManuscript_TDIC()"
            toRemove="if(scheduledItem.New) {glLine.removeFromGLManuscript_TDIC(scheduledItem)}; "
            value="glLine.GLManuscript_TDIC"
            valueType="entity.GLManuscript_TDIC[]">
            <Row>
              <DateCell
                align="left"
                id="effDate"
                label="DisplayKey.get(&quot;Coverage.GL.Schedule.EndEffDate_TDIC&quot;)"
                required="true"
                value="scheduledItem.ManuscriptEffectiveDate"/>
              <DateCell
                align="left"
                id="expDate"
                label="DisplayKey.get(&quot;Coverage.GL.EndExpDate_TDIC&quot;)"
                requestValidationExpression="validateEndorsementExpDate(VALUE)"
                required="true"
                value="scheduledItem.ManuscriptExpirationDate"/>
              <RangeCell
                align="right"
                editable="true"
                id="manuscriptType"
                label="DisplayKey.get(&quot;TDIC.PolicyLine_BOPLine.CoveragePattern_BOPManuscriptEndorsement_TDIC.Type&quot;)"
                required="true"
                value="scheduledItem.ManuscriptType"
                valueRange="setTypeValue()"
                valueType="typekey.GLManuscriptType_TDIC">
                <PostOnChange/>
              </RangeCell>
              <TextCell
                available="(scheduledItem.ManuscriptType==GLManuscriptType_TDIC.TC_ADDITIONALINSUREDCONTINUED)"
                hideChildrenIfReadOnly="false"
                id="PolicyAddlInsuredDetail"
                label="DisplayKey.get(&quot;Coverage.GL.Sched.AddtlInsured_TDIC&quot;)"
                required="true"
                value="(scheduledItem.PolicyAddInsured_TDIC.DisplayName==null? &quot;&quot; : scheduledItem.PolicyAddInsured_TDIC.DisplayName) + &quot; &quot; + (scheduledItem.PolicyAddInsured_TDIC.Desciption==null? &quot;&quot; : scheduledItem.PolicyAddInsured_TDIC.Desciption)">
                <PostOnChange/>
                <MenuItemIterator
                  elementName="unassignedContact"
                  id="UnassignedContactIterator"
                  value="getAdditionalInsureds()"
                  valueType="entity.AccountContactView[]"
                  visible="glLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT">
                  <IteratorSort
                    sortBy="unassignedContact.DisplayName"
                    sortOrder="1"/>
                  <MenuItem
                    action="setAdditionalInsureds(scheduledItem, unassignedContact.AccountContact)"
                    available="openForEdit"
                    id="UnassignedContact"
                    label="unassignedContact"/>
                </MenuItemIterator>
              </TextCell>
              <TextCell
                available="(scheduledItem.ManuscriptType==GLManuscriptType_TDIC.TC_CERTIFICATEHOLDERNAMEAMENDEDTOREAD)"
                hideChildrenIfReadOnly="false"
                id="PolicyCertificateHolder"
                label="DisplayKey.get(&quot;Coverage.GL.Sched.CertificateHolder_TDIC&quot;)"
                required="true"
                value="(scheduledItem.CertificateHolder.DisplayName==null? &quot;&quot; : scheduledItem.CertificateHolder.DisplayName) + &quot; &quot; + (scheduledItem.CertificateHolder.Description==null? &quot;&quot; : scheduledItem.CertificateHolder.Description)">
                <PostOnChange/>
                <MenuItemIterator
                  elementName="unassignedContact"
                  id="UnassignedContactIterator"
                  value="getCertificateHolders()"
                  valueType="entity.AccountContactView[]"
                  visible="glLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT">
                  <IteratorSort
                    sortBy="unassignedContact.DisplayName"
                    sortOrder="1"/>
                  <MenuItem
                    action="setCertificateHolder(scheduledItem, unassignedContact.AccountContact)"
                    available="openForEdit"
                    id="UnassignedContact"
                    label="unassignedContact"/>
                </MenuItemIterator>
              </TextCell>
              <TextAreaCell
                align="right"
                editable="true"
                id="manuscriptDescription"
                label="DisplayKey.get(&quot;TDIC.PolicyLine_BOPLine.CoveragePattern_BOPManuscriptEndorsement_TDIC.Description&quot;)"
                required="true"
                value="scheduledItem.ManuscriptDescription"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputGroup>
    <InputDivider/>
    <Code><![CDATA[function validateEndorsementExpDate(date: Date): String {
  var transEffDate = coverable.PolicyLine.Branch.EditEffectiveDate
  var allowedDate = (date.afterOrEqual(transEffDate.addWeeks(2)) and date.beforeOrEqual(transEffDate.addDays(90)))
  if(!allowedDate){
    return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLManuscriptCovTDIC.ExpDate")
  }
  return null
}

function setTypeValue() : GLManuscriptType_TDIC[]{
  if(glLine.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || glLine.Branch.Offering.CodeIdentifier == "PLOccurence_TDIC"){
    return GLManuscriptType_TDIC.TF_PL.TypeKeys.toTypedArray()
  }else{
    return GLManuscriptType_TDIC.TF_CYB.TypeKeys.toTypedArray()
  }
}
function setExpirationDate(manuScripts : GLManuscript_TDIC[]) {
  manuScripts.each(\manuScript -> {
    if(manuScript.ManuscriptExpirationDate == null) {
      manuScript.ManuscriptExpirationDate = manuScript.Branch.EditEffectiveDate
    }
  })
}

function resetAiOrCh (manuScripts : GLManuscript_TDIC[]) {
  manuScripts.each(\manuScript -> {
    if(manuScript.PolicyAddInsured_TDIC != null) {
      manuScript.PolicyAddInsured_TDIC = null
    }
    if (manuScript.CertificateHolder != null) {
      manuScript.CertificateHolder = null
    }
    
  })
  
  
}

function getAdditionalInsureds() : AccountContactView[] {
  return glLine?.AdditionalInsureds*.AccountContactRole*.AccountContact?.asViews()
}

function getCertificateHolders() : AccountContactView[] {
  return glLine?.GLCertificateHolders_TDIC*.AccountContactRole*.AccountContact?.asViews()
}

function setAdditionalInsureds(scheduledItem : GLManuscript_TDIC, contact : AccountContact ) {
  scheduledItem.PolicyAddInsured_TDIC = glLine?.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\elt -> elt.PolicyAddlInsured?.AccountContactRole?.AccountContact == contact)
}

function setCertificateHolder(scheduledItem : GLManuscript_TDIC, contact : AccountContact ) {
  scheduledItem.CertificateHolder = glLine?.GLCertificateHolders_TDIC?.firstWhere(\elt -> elt.AccountContactRole?.AccountContact == contact)
}]]></Code>
  </InputSet>
</PCF>