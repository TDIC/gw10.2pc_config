<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <PanelSet
    id="TDIC_GLTermsPanelSet">
    <Require
      name="period"
      type="PolicyPeriod"/>
    <Require
      name="glCosts"
      type="Set&lt;entity.GLCost&gt;"/>
    <Require
      name="isEREJob"
      type="boolean"/>
    <Variable
      name="quoteScreenHelper"
      type="tdic.web.pcf.helper.GLQuoteScreenHelper"/>
    <DetailViewPanel
      visible="(period.Offering.CodeIdentifier != tdic.pc.config.rating.RatingConstants.plCyberOffering)">
      <InputColumn>
        <Label
          label="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.Terms&quot;)"/>
      </InputColumn>
    </DetailViewPanel>
    <PanelDivider
      visible="(period.Offering.CodeIdentifier != tdic.pc.config.rating.RatingConstants.plCyberOffering)"/>
    <ListViewPanel>
      <Variable
        initialValue="isEREJob ? quoteScreenHelper.getERETermPremiums(glCosts) : quoteScreenHelper.getNonERETermPremiums(glCosts)"
        name="termCosts"
        recalculateOnRefresh="true"
        type="Set&lt;GLCost&gt;"/>
      <Row
        renderAsSmartHeader="true">
        <TextCell
          id="termPremiumType"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.PremiumType&quot;)"/>
        <TextCell
          id="termLimitHeader"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.Limits&quot;)"/>
        <TextCell
          id="policyChangeEffDateTeHeader"
          value="DisplayKey.get(&quot;TDIC.Web.GL.PolicyChange.GL.ChangeEffectiveDate&quot;)"
          visible="(period.Job typeis PolicyChange) and not isEREJob"/>
        <TextCell
          id="policychangeExpDateHeader"
          value="DisplayKey.get(&quot;TDIC.Web.GL.PolicyChange.GL.ChangeExpirationDate&quot;)"
          visible="(period.Job typeis PolicyChange) and not isEREJob"/>
        <TextCell
          id="termEffDateTeHeader"
          value="DisplayKey.get(&quot;Web.CommercialPackage.LineReview.EffectiveDate&quot;)"/>
        <TextCell
          id="termExpDateHeader"
          value="DisplayKey.get(&quot;Web.CommercialPackage.LineReview.ExpirationDate&quot;)"/>
        <TextCell
          id="termClassCodeHeader"
          value="DisplayKey.get(&quot;Web.Policy.GL.ExposureUnits.ClassCode&quot;)"/>
        <TextCell
          id="territoryCodeHeader"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.TerritoryCode&quot;)"/>
        <TextCell
          id="specialityCodeHeader"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.ExposureUnits.SpecialityCode&quot;)"/>
        <TextCell
          id="TermDiscountHeader"
          value="DisplayKey.get(&quot;TDIC.Web.RatingParam.Discount&quot;)"/>
        <TextCell
          id="TermStepYear"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.stepYear&quot;)"/>
        <TextCell
          id="TermPremium"
          value="DisplayKey.get(&quot;Web.Quote.CumulDetail.Default.TermPremium&quot;)"/>
        <TextCell
          id="ProratedPremiumWithDiscount"
          value="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.ProRatedPremiumWithDiscount&quot;)"/>
      </Row>
      <RowIterator
        editable="CurrentLocation.InEditMode"
        elementName="termCost"
        id="TermIterator"
        pageSize="0"
        type="GLCost"
        value="termCosts.toTypedArray()"
        valueType="GLCost[]">
        <Variable
          initialValue="quoteScreenHelper.getTermExposure(termCost)"
          name="glExposure"
          type="GLExposure"/>
        <IteratorSort
          sortBy="(termCost typeis GLHistoricalCost_TDIC)?(termCost as GLHistoricalCost_TDIC).GLSplitParameters.EffDate : (isEREJob ? quoteScreenHelper.getERETermExpirationDate(termCost):(termCost as GLCovExposureCost).ExpirationDate)"
          sortDirection="descending"
          sortOrder="1"/>
        <Row
          visible="termCost typeis GLCovExposureCost">
          <TextCell
            id="premiumType"
            value="(termCost as GLCovExposureCost).GLCostType.DisplayName"/>
          <TextCell
            id="limit"
            value="quoteScreenHelper.getLiabilityLimit(termCost)"/>
          <DateCell
            id="policyChangeEffDate"
            value="(termCost as GLCovExposureCost).EffectiveDate"
            visible="(period.Job typeis PolicyChange) and not isEREJob"/>
          <DateCell
            id="policyChangeExpirationDate"
            value="(termCost as GLCovExposureCost).ExpirationDate"
            visible="(period.Job typeis PolicyChange) and not isEREJob"/>
          <DateCell
            id="effDate"
            value="isEREJob ? glExposure.SliceDate : (termCost as GLCovExposureCost).EffectiveDate"/>
          <DateCell
            id="expirDate"
            value="isEREJob ? quoteScreenHelper.getERETermExpirationDate(termCost):(termCost as GLCovExposureCost).ExpirationDate"/>
          <TextCell
            id="classCode"
            value="glExposure.ClassCode_TDIC.DisplayName"/>
          <TextCell
            id="territoryCode"
            value="glExposure.GLTerritory_TDIC.TerritoryCode"/>
          <TextCell
            id="specialityCode"
            value="glExposure.SpecialityCode_TDIC.DisplayName"/>
          <TextCell
            id="discount"
            value="(termCost as GLCovExposureCost).Discount_TDIC.DisplayName"/>
          <TextCell
            id="StepYearCov"
            value="tdic.pc.config.rating.RatingConstants.currentStepYear"/>
          <TextCell
            id="TermPremiumCov"
            value="(termCost as GLCovExposureCost).PreDiscountPremium_TDIC"
            valueType="java.math.BigDecimal"/>
          <MonetaryAmountCell
            align="right"
            currency="period.PreferredSettlementCurrency"
            formatType="currency"
            id="ProratedPremiumwithdiscountCov"
            value="(termCost as GLCovExposureCost).ActualAmountBilling"/>
        </Row>
        <Row
          visible="(termCost typeis GLHistoricalCost_TDIC) and (termCost.Branch.Offering.CodeIdentifier == tdic.pc.config.rating.RatingConstants.plCMOffering)">
          <TextCell
            footerLabel="DisplayKey.get(&quot;TDIC.Web.Policy.GL.Header.TotalTermPremium&quot;)"
            id="histPremType"
            value="(termCost as GLHistoricalCost_TDIC).CostType.DisplayName"/>
          <TextCell
            id="histLimitHeader"
            value="quoteScreenHelper.getLiabilityLimit(termCost)"/>
          <DateCell
            id="policChangeHistEffDate"
            value="(termCost as GLHistoricalCost_TDIC).EffectiveDate"
            visible="(period.Job typeis PolicyChange) and not isEREJob"/>
          <DateCell
            id="policyChangeExpDate"
            value="(termCost as GLHistoricalCost_TDIC).ExpirationDate"
            visible="(period.Job typeis PolicyChange) and not isEREJob"/>
          <DateCell
            id="histEffDate"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.EffDate"/>
          <DateCell
            id="expDate"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.ExpDate"/>
          <TextCell
            id="histClassCode"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.ClassCode"/>
          <TextCell
            id="histTerritoryCode"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.TerritoryCode"/>
          <TextCell
            id="histSpecialityCode"
            value="quoteScreenHelper.getSpecialtyCode(termCost)"/>
          <TextCell
            id="histDiscount"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.Discount.DisplayName"/>
          <TextCell
            id="StepYear"
            value="(termCost as GLHistoricalCost_TDIC).GLSplitParameters.splitYear "/>
          <TextCell
            footerSumValue="termCost typeis GLHistoricalCost_TDIC ? (termCost as GLHistoricalCost_TDIC).PreDiscountPremium_TDIC : termCost typeis GLCovExposureCost ? (termCost as GLCovExposureCost).PreDiscountPremium_TDIC : null "
            id="TermPremium"
            value="(termCost as GLHistoricalCost_TDIC).PreDiscountPremium_TDIC"
            valueType="java.math.BigDecimal"/>
          <MonetaryAmountCell
            align="right"
            currency="period.PreferredSettlementCurrency"
            footerSumValue="termCost typeis GLHistoricalCost_TDIC ? (termCost as GLHistoricalCost_TDIC).ActualAmountBilling : termCost typeis GLCovExposureCost ? (termCost as GLCovExposureCost).ActualAmountBilling : null "
            formatType="currency"
            id="ProratedPremiumwithdiscount"
            value="(termCost as GLHistoricalCost_TDIC).ActualAmount"/>
        </Row>
      </RowIterator>
    </ListViewPanel>
  </PanelSet>
</PCF>