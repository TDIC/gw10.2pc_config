<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <DetailViewPanel
    id="BOPBuilding_DetailsDV">
    <Require
      name="building"
      type="BOPBuilding"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Require
      name="bopLine"
      type="BOPLine"/>
    <Require
      name="quoteType"
      type="QuoteType"/>
    <Variable
      initialValue="building == null ? null : building.PolicyLine.Pattern.getCoverageCategoryByPublicId(&quot;BOPBuildingCat&quot;).coveragePatternsForEntity(BOPBuilding).whereSelectedOrAvailable(building, openForEdit)"
      name="bopBuildingCatCoveragePatterns"
      recalculateOnRefresh="true"
      type="gw.api.productmodel.CoveragePattern[]"/>
    <!-- Left column -->
    <InputColumn>
      <TextInput
        boldLabel="true"
        boldValue="true"
        id="LocationInfo"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Location&quot;)"
        value="building.Building.PolicyLocation.DisplayName"/>
      <Label
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Construction&quot;)"/>
      <TextInput
        align="left"
        editable="true"
        id="YearBuilt"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.YearBuilt&quot;)"
        required="!(quoteType == QuoteType.TC_QUICK || building.Branch.isDIMSPolicy_TDIC)"
        value="building.Building.YearBuilt"
        valueType="java.lang.Integer"/>
      <TypeKeyInput
        editable="bopLine?.Branch?.profileChange_TDIC"
        id="ConstructionType"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.Construction_TDIC&quot;)"
        required="true"
        value="building.ConstructionType"
        valueType="typekey.BOPConstructionType"/>
      <TextInput
        align="left"
        editable="true"
        id="NumStories"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.NumFloors_TDIC&quot;)"
        requestValidationExpression="checkValue_TDIC(VALUE) "
        required="!(quoteType == QuoteType.TC_QUICK || building.Branch.isDIMSPolicy_TDIC)"
        value="building.Building.NumStories"
        valueType="java.lang.Integer"/>
      <TextInput
        align="left"
        editable="bopLine?.Branch?.profileChange_TDIC"
        id="TotalArea"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.TotalBuilding_TDIC&quot;) "
        required="quoteType == QuoteType.TC_QUICK ? bopLine.Branch.Offering.CodeIdentifier==&quot;BOPLessorsRisk_TDIC&quot;?true:false : true"
        value="building.Building.TotalArea"
        valueType="java.lang.Integer"/>
      <TextInput
        align="left"
        editable="bopLine?.Branch?.profileChange_TDIC"
        id="TotalOfficeArea"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.TotalOffice_TDIC&quot;) "
        required="!(quoteType == QuoteType.TC_QUICK)"
        value="building.Building.TotalOfficeArea_TDIC"
        valueType="java.lang.Integer"
        visible="bopLine.Branch.Offering.CodeIdentifier == &quot;BOPBusinessOwnersPolicy_TDIC&quot;"/>
      <TextInput
        align="left"
        editable="true"
        id="NumOper"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.NumberOperatories_TDIC&quot;) "
        required="!(quoteType == QuoteType.TC_QUICK || building.Branch.isDIMSPolicy_TDIC)"
        value="building.Building.NumOperatories_TDIC"
        valueType="java.lang.Integer"
        visible="bopLine.Branch.Offering.CodeIdentifier == &quot;BOPBusinessOwnersPolicy_TDIC&quot;"/>
      <TextInput
        align="left"
        editable="true"
        id="NumUnits"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.NumUnits_TDIC&quot;) "
        required="!(quoteType == QuoteType.TC_QUICK || building.Branch.isDIMSPolicy_TDIC)"
        value="building.Building.NumUnits_TDIC"
        valueType="java.lang.Integer"/>
      <TextInput
        align="left"
        editable="bopLine?.Branch?.profileChange_TDIC"
        formatType="currency"
        id="RentalIncome"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.RentalIncome_TDIC&quot;) "
        required="!(quoteType == QuoteType.TC_QUICK || bopLine.Branch.isDIMSPolicy_TDIC)"
        value="building.Building.RentalIncome_TDIC"
        valueType="java.lang.Integer"
        visible="bopLine.Branch.Offering.CodeIdentifier == &quot;BOPLessorsRisk_TDIC&quot;"/>
      <BooleanRadioInput
        editable="bopLine?.Branch?.profileChange_TDIC"
        id="sprinkle"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.Sprinklered_TDIC&quot;)"
        required="!(quoteType == QuoteType.TC_QUICK)"
        value="building.Building.Sprinklered_TDIC"/>
      <BooleanRadioInput
        editable="true"
        id="alarm"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.BurglarAlarm_TDIC&quot;)"
        required="!(quoteType == QuoteType.TC_QUICK || (bopLine.Branch.isDIMSPolicy_TDIC &amp;&amp; bopLine.Branch.Offering.CodeIdentifier==&quot;BOPBusinessOwnersPolicy_TDIC&quot;))"
        value="building.Building.BurglarAlarm_TDIC"/>
      <InputDivider/>
      <Label
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.BuildingOccupancy_TDIC&quot;)"
        visible="!(quoteType == QuoteType.TC_QUICK)"/>
      <RangeInput
        editable="true"
        id="occStatus"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.OccupancyStatus_TDIC&quot;)"
        required="true"
        value="building.Building.BOPOccupancyStatus_TDIC"
        valueRange="setOccupancyValues_TDIC()"
        valueType="typekey.BOPOccupancyStatus_TDIC"/>
      <TextInput
        __disabled="true"
        id="Number"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.Number&quot;)"
        value="building.Building.BuildingNum"
        valueType="java.lang.Integer"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="Description"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.Description&quot;)"
        value="building.Building.Description"/>
      <RangeInput
        __disabled="true"
        editable="true"
        id="BOPBuildingClassCodeRange"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Liability.ClassCode&quot;)"
        optionLabel="VALUE.DisplayName"
        required="true"
        value="building.ClassCode"
        valueRange="building.ClassCodes"
        valueType="entity.BOPClassCode">
        <PickerMenuItem
          action="BOPClassCodeSearchPopup.push(building.PolicyLine as BOPLine, building)"
          id="BOPBuildingClassCodePicker"
          label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Liability.Search&quot;)"
          onPick="building.ClassCode = PickedValue"/>
      </RangeInput>
      <Label
        __disabled="true"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Liability&quot;)"/>
      <TextInput
        __disabled="true"
        id="BasisType"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Liability.PremiumBasisType&quot;)"
        value="building.ClassCode.Basis.Description"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="BasisAmount"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Liability.PremiumBasisAmount&quot;)"
        required="true"
        value="building.BasisAmount"
        valueType="java.lang.Integer"
        visible="building.ClassCode.Basis.Code == &quot;Payroll - per $100&quot; or                       building.ClassCode.Basis.Code == &quot;Payroll - per $1000&quot; or                       building.ClassCode.Basis.Code == &quot;Sales - per $1000&quot;"/>
      <InputDivider/>
      <InputIterator
        elementName="coveragePattern"
        forceRefreshDespiteChangedEntries="true"
        id="BOPBuildingCatIterator"
        value="bopBuildingCatCoveragePatterns"
        valueType="gw.api.productmodel.CoveragePattern[]">
        <IteratorSort
          sortBy="coveragePattern.Priority"
          sortOrder="1"/>
        <InputSetRef
          def="CoverageInputSet(coveragePattern, building, openForEdit)"
          mode="coveragePattern.PublicID"
          visible="false"/>
      </InputIterator>
    </InputColumn>
    <!-- Right column -->
    <InputColumn>
      <!-- postOnChange required because trigger field is numeric -->
      <TextInput
        __disabled="true"
        align="left"
        editable="true"
        id="NumBasements"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.NumBasements&quot;)"
        value="building.Building.NumBasements"
        valueType="java.lang.Integer">
        <PostOnChange
          deferUpdate="false"/>
      </TextInput>
      <InputSet
        id="BasementInputSet"
        visible="building.Building.NumBasements != null and building.Building.NumBasements &gt; 0">
        <TextInput
          align="left"
          editable="true"
          id="AreaFinished"
          label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.AreaFinished&quot;)"
          value="building.Building.AreaFinished"
          valueType="java.lang.Integer"/>
        <TextInput
          align="left"
          editable="true"
          id="AreaUnfinished"
          label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.AreaUnfinished&quot;)"
          value="building.Building.AreaUnfinished"
          valueType="java.lang.Integer"/>
      </InputSet>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="PercentSprinklered"
        label="DisplayKey.get(&quot;Web.Policy.BOP.PercentSprinklered&quot;)"
        value="building.Building.SprinklerCoverage"
        valueType="typekey.Sprinklered"/>
      <BooleanRadioInput
        editable="true"
        id="buildingImpCheck"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.BuildingImprovementCheck&quot;)"
        value="building.Building.BuildingImprovement_TDIC"
        visible="(!(quoteType == QuoteType.TC_QUICK)) and (building.PolicyPeriod.Offering.CodeIdentifier == &quot;BOPBusinessOwnersPolicy_TDIC&quot;)">
        <PostOnChange
          deferUpdate="false"/>
      </BooleanRadioInput>
      <Label
        id="buildingImprovementLabel"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.Text1_TDIC&quot;)"
        visible="(!(quoteType == QuoteType.TC_QUICK)) and buildingImpVisibility()"/>
      <TextInput
        __disabled="true"
        align="left"
        editable="true"
        id="LastUpdateHeating"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.LastUpdateHeating&quot;)"
        value="building.Building.Heating.YearAdded"
        valueType="java.lang.Integer"/>
      <TextInput
        align="left"
        editable="true"
        id="LastUpdatePlumbing"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.PlumbingYear_TDIC&quot;)"
        value="building.Building.Plumbing.YearAdded"
        valueType="java.lang.Integer"
        visible="!(quoteType == QuoteType.TC_QUICK) and buildingImpVisibility()"/>
      <TextInput
        align="left"
        editable="true"
        id="LastUpdateRoofing"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.RoofingYear_TDIC&quot;)"
        value="building.Building.Roofing.YearAdded"
        valueType="java.lang.Integer"
        visible="!(quoteType == QuoteType.TC_QUICK) and buildingImpVisibility()"/>
      <TextInput
        align="left"
        editable="true"
        id="LastUpdateWiring"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.ElectricalYear_TDIC&quot;)"
        value="building.Building.Wiring.YearAdded"
        valueType="java.lang.Integer"
        visible="!(quoteType == QuoteType.TC_QUICK) and buildingImpVisibility()"/>
      <InputDivider/>
      <Label
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.BuildingAttributes_TDIC&quot;)"/>
      <TypeKeyInput
        editable="bopLine?.Branch?.profileChange_TDIC"
        filter="VALUE.hasCategory(building.BOPLocation.CoverableState)"
        id="terrotory"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.Territory_TDIC&quot;)"
        required="true"
        value="building.Building.TerritoryDetails_TDIC"
        valueType="typekey.TerritoryDetails_TDIC"/>
      <TypeKeyInput
        editable="bopLine?.Branch?.profileChange_TDIC"
        id="protectionClass"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Building.ProtectionClass_TDIC&quot;)"
        required="true"
        value="building.Building.ProtectionClass_TDIC"
        valueSectionWidth="8em"
        valueType="typekey.ProtectionClass_TDIC"/>
      <Label
        __disabled="true"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm&quot;)"/>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="BuildingAlarmType"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm.BuildingAlarmType&quot;)"
        value="building.Building.BuildingAlarmType"
        valueType="typekey.BuildingAlarmType"/>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="AlarmGrade"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm.AlarmGrade&quot;)"
        value="building.Building.AlarmGrade"
        valueType="typekey.AlarmGrade"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="AlarmCertificate"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm.AlarmCertificate&quot;)"
        value="building.Building.AlarmCertificate"/>
      <DateInput
        __disabled="true"
        editable="true"
        id="AlarmExpiration"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm.AlarmExpiration&quot;)"
        value="building.Building.AlarmExpiration"/>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="AlarmCertification"
        label="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Alarm.AlarmCertification&quot;)"
        value="building.Building.AlarmCertification"
        valueType="typekey.AlarmCertification"/>
      <InputDivider
        __disabled="true"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="FrontExposure"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.FrontExposure&quot;)"
        value="building.Building.FrontSide.Description"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="RearExposure"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.RearExposure&quot;)"
        value="building.Building.RearSide.Description"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="LeftExposure"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.LeftExposure&quot;)"
        value="building.Building.LeftSide.Description"/>
      <TextInput
        __disabled="true"
        editable="true"
        id="RightExposure"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.RightExposure&quot;)"
        value="building.Building.RightSide.Description"/>
      <InputDivider
        __disabled="true"/>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="InterestType"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.InterestType&quot;)"
        value="building.Building.InterestType"
        valueType="typekey.InterestType"/>
      <TypeKeyInput
        __disabled="true"
        align="left"
        editable="true"
        id="PercentOccupied"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.PercentOccupied&quot;)"
        value="building.Building.PercentOccupied"
        valueType="typekey.PercentOccupied"/>
      <TypeKeyInput
        __disabled="true"
        editable="true"
        id="AnyAreaLeased"
        label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.AnyAreaLeased&quot;)"
        value="building.Building.AreaLeased"
        valueType="typekey.AreaLeased"/>
      <InputDivider
        __disabled="true"/>
    </InputColumn>
    <Code><![CDATA[/**
 * create by: ChitraK
 * @description: validation for number of floors
 * @create time: 5:58 PM 8/9/2019
 * @return: 
 */

function checkValue_TDIC(floor : Integer) : String{
  if(floor < 0 || floor > 999) 
    return "Number of Floors can accept value only between 0 and 999"
  return null
}
/**
 * create by: ChitraK
 * @description: Set Occupancy Status Values
 * @create time: 5:58 PM 8/9/2019
 * @return:BOPOccupancyStatus_TDIC 
 */

function setOccupancyValues_TDIC():List<typekey.BOPOccupancyStatus_TDIC>{
  var occList : List<BOPOccupancyStatus_TDIC> = {}
  if(bopLine.Branch.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"){
    var bopList = {BOPOccupancyStatus_TDIC.TC_TENANT,BOPOccupancyStatus_TDIC.TC_BUILDINGOWNER,BOPOccupancyStatus_TDIC.TC_CONDO,BOPOccupancyStatus_TDIC.TC_OTHER,BOPOccupancyStatus_TDIC.TC_TRIPLE}
    occList.addAll(bopList)
  }
  if(bopLine.Branch.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC"){
    var lrpList = {BOPOccupancyStatus_TDIC.TC_NOTOCCUPYING,BOPOccupancyStatus_TDIC.TC_BUILDINGOWNER,BOPOccupancyStatus_TDIC.TC_CONDO,BOPOccupancyStatus_TDIC.TC_OTHER,BOPOccupancyStatus_TDIC.TC_TRIPLE}
    occList.addAll(lrpList)
  }
  return occList
}

function buildingImpVisibility() : Boolean{
  if((building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC")){
    if(building.Building.BuildingImprovement_TDIC != null){
    return !building.Building.BuildingImprovement_TDIC}else{
      return false
    }
  }else{
    return true
  }
}]]></Code>
  </DetailViewPanel>
</PCF>