<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <Popup
    afterReturnFromPopup="jobWizardHelper.reloadPolicyPeriodIfAsyncQuoteComplete(bopLine.Branch)"
    beforeCommit="gw.lob.bop.BOPBuildingValidation.validateBuilding(building);gw.validation.ValidationUtil.checkCurrentResult();gw.lob.bop.BOPBuildingValidation.validateBuildingRules_TDIC(building,quoteType)"
    canEdit="openForEdit"
    countsAsWork="false"
    id="BOPBuildingPopup"
    returnType="BOPBuilding"
    startEditing="if (building == null and startInEdit == true and openForEdit) {building = bopLocation.addNewLineSpecificBuilding() as BOPBuilding}"
    startInEditMode="startInEdit"
    title="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.Details&quot;)">
    <LocationEntryPoint
      signature="BOPBuildingPopup(bopLine : BOPLine, bopLocation : BOPLocation, building : BOPBuilding, openForEdit : boolean, startInEdit : boolean, jobWizardHelper : gw.api.web.job.JobWizardHelper)"/>
    <Variable
      name="bopLine"
      type="BOPLine"/>
    <Variable
      name="bopLocation"
      type="BOPLocation"/>
    <Variable
      name="building"
      recalculateOnRefresh="true"
      type="BOPBuilding"/>
    <Variable
      name="openForEdit"
      type="boolean"/>
    <Variable
      name="startInEdit"
      type="boolean"/>
    <Variable
      name="jobWizardHelper"
      type="gw.api.web.job.JobWizardHelper"/>
    <Variable
      initialValue="bopLocation.Branch.Policy.Product.getQuestionSetsByType(QuestionSetType.TC_UNDERWRITING)"
      name="underwritingQuestionSets"
      recalculateOnRefresh="true"
      type="gw.api.productmodel.QuestionSet[]"/>
    <Variable
      initialValue="setInitialQuoteType()"
      name="quoteType"
      recalculateOnRefresh="true"
      type="QuoteType"/>
    <Screen
      id="BOPSingleBuildingDetailScreen">
      <Toolbar>
        <EditButtons
          pickValue="building"
          updateLabel="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Update&quot;)"/>
      </Toolbar>
      <PanelRef
        def="PreferredCoverageCurrencyPanelSet(building, openForEdit, jobWizardHelper)"
        id="PreferredCoverageCurrencySelectorRef"/>
      <CardViewPanel>
        <Card
          id="BOPBuilding_DetailsCard"
          onSelect="gw.web.productmodel.ProductModelSyncIssuesHandler.syncModifiers( {building}, jobWizardHelper)"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.Details&quot;)">
          <PanelRow>
            <PanelColumn>
              <PanelRef
                def="BOPBuilding_DetailsDV(building, CurrentLocation.InEditMode,bopLine,quoteType)"/>
              <PanelRef
                def="MortgageeDetails_TDICDV(building,openForEdit)"
                visible="(building.PolicyPeriod.Offering.CodeIdentifier == &quot;BOPBusinessOwnersPolicy_TDIC&quot; || building.PolicyPeriod.Offering.CodeIdentifier == &quot;BOPLessorsRisk_TDIC&quot;) &amp;&amp; !(quoteType == QuoteType.TC_QUICK)"/>
            </PanelColumn>
          </PanelRow>
          <PanelRef
            def="LossPayeeDetails_TDICDV(building,openForEdit)"
            visible="building.PolicyPeriod.Offering.CodeIdentifier == &quot;BOPBusinessOwnersPolicy_TDIC&quot; and !(quoteType == QuoteType.TC_QUICK)"/>
          <PanelRef
            __disabled="true"
            def="AdditionalInterestDetailsDV(building, openForEdit)"
            visible="false"/>
        </Card>
        <Card
          id="PropertyCov"
          onSelect="building.syncCoverages()"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Property&quot;)">
          <PanelRef
            def="BOPLinePropertyCoverageDV(bopLine,building,true)"/>
        </Card>
        <Card
          id="LiabCoverages"
          onSelect="building.syncCoverages()"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Liability&quot;)">
          <PanelRef
            def="BOPLineLiabilityCoverageDV(bopLine,building,true)"/>
          <PanelRef
            def="TDIC_BuildingAdditionalInsuredsDV(building, openForEdit, true, false)"
            editable="bopLine?.Branch?.profileChange_TDIC"
            visible="(building.BOPDentalGenLiabilityCov_TDICExists || building.BOPBuildingOwnersLiabCov_TDICExists) &amp;&amp; !(quoteType == QuoteType.TC_QUICK)"/>
        </Card>
        <Card
          id="BOPAddlCov"
          onSelect="building.setOrdinanceTermLimits_TDIC();building.syncCoverages();building.setEarthquakeLimits_TDIC();building.setEQSPExistence_TDIC();building.setILSubLimit_TDIC();building.setEnhancedCoverageLimits();building.remEnhancedCoverageLimits()"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.AdditionalCoverage&quot;)">
          <PanelRef
            def="BOPAdditionalCoveragesDV(bopLine,building,quoteType,true)"/>
        </Card>
        <Card
          id="Modifiers"
          onSelect="gw.web.productmodel.ProductModelSyncIssuesHandler.syncModifiers( {building}, jobWizardHelper);setNSRValues();setSRValues()"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.RateModifiers_TDIC&quot;)"
          visible="bopLine.Branch.CanViewModifiers">
          <PanelRef>
            <DetailViewPanel
              editable="bopLine?.Branch?.profileChange_TDIC">
              <InputColumn>
                <InputSetRef
                  def="TDIC_BOPModifiersInputSet(setNSRValues().toList(), bopLine.Branch)"
                  editable="bopLine.Branch.Status == PolicyPeriodStatus.TC_DRAFT"/>
              </InputColumn>
            </DetailViewPanel>
          </PanelRef>
          <PanelRef
            editable="not User.util.CurrentUser.ExternalUser &amp;&amp; bopLine?.Branch?.profileChange_TDIC"
            visible="not User.util.CurrentUser.ExternalUser and setSRValues().HasElements and not (quoteType == QuoteType.TC_QUICK)">
            <TitleBar
              title="DisplayKey.get(&quot;Web.ModifiersScreen.RatingInputs&quot;)"/>
            <DetailViewPanel>
              <Variable
                initialValue="setSRValues()"
                name="scheduleRates"
                recalculateOnRefresh="true"
                type="Modifier[]"/>
              <InputColumn>
                <Label
                  label="DisplayKey.get(&quot;Web.Policy.RatingInputs.None&quot;)"
                  visible="setSRValues().IsEmpty"/>
                <InputIterator
                  elementName="scheduleRate"
                  forceRefreshDespiteChangedEntries="true"
                  value="scheduleRates"
                  valueType="entity.Modifier[]">
                  <IteratorSort
                    sortBy="scheduleRate.Pattern.Priority"
                    sortOrder="1"/>
                  <TextInput
                    boldLabel="true"
                    boldValue="true"
                    id="ratingInputName"
                    label="DisplayKey.get(&quot;Web.ModifiersScreen.Modifier&quot;)"
                    value="scheduleRate.DisplayName"/>
                  <ListViewInput
                    available="scheduleRates != null"
                    def="ScheduleRateLV(scheduleRate)"
                    editable="(building.PolicyPeriod.Job typeis Submission or building.PolicyPeriod.Job typeis PolicyChange or building.PolicyPeriod.Job typeis Renewal or (building.PolicyPeriod.Job?.MigrationJobInd_TDIC or building.PolicyPeriod.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION))//true"
                    labelAbove="true"
                    visible="scheduleRates != null">
                    <Toolbar/>
                  </ListViewInput>
                  <InputDivider/>
                </InputIterator>
              </InputColumn>
            </DetailViewPanel>
          </PanelRef>
        </Card>
        <Card
          __disabled="true"
          id="BOPBuilding_AdditionalCoveragesCard"
          title="DisplayKey.get(&quot;Web.Policy.BOP.Buildings.AdditionalCoverage&quot;)">
          <CardViewPanel>
            <Card
              __disabled="true"
              id="BOPAddlCov"
              title="&quot;Additional Coverages&quot;">
              <PanelRef
                def="BOPAdditionalCoveragesDV(bopLine,building,quoteType,true)"/>
            </Card>
          </CardViewPanel>
          <DetailViewPanel>
            <InputColumn>
              <TextInput
                boldLabel="true"
                boldValue="true"
                id="LocationInfo"
                label="DisplayKey.get(&quot;Web.Policy.BOP.Location&quot;)"
                value="building.Building.PolicyLocation.DisplayName"/>
              <TextInput
                id="Number"
                label="DisplayKey.get(&quot;Web.Policy.LocationContainer.Location.Building.Number&quot;)"
                value="building.Building.BuildingNum"
                valueType="java.lang.Integer"/>
            </InputColumn>
          </DetailViewPanel>
          <PanelRef
            def="AdditionalCoveragesPanelSet(building, new String[]{&quot;BOPBuildingOtherCat&quot;,&quot;BOPIncomeExpenseCat&quot;,&quot;BOPBuildingUtilitiesCat&quot;,&quot;BOPBuildingSpecialPerilCat&quot;,&quot;BOPStateCat&quot;}, true)"/>
        </Card>
        <Card
          id="uwinformation"
          onSelect="gw.web.productmodel.ProductModelSyncIssuesHandler.syncQuestions( {bopLocation.PolicyLocation}, jobWizardHelper )"
          selectOnEnter="true"
          title="DisplayKey.get(&quot;TDIC.Web.Policy.BOP.Buildings.UWInformation&quot;)"
          visible="(quoteType != QuoteType.TC_QUICK) /*&amp;&amp; !(bopLine.Branch.LegacyPolicyNumber_TDIC!=null || bopLine.Branch.BasedOn.LegacyPolicyNumber_TDIC!=null)*/">
          <PanelRef
            def="QuestionSets_TDICDV(underwritingQuestionSets.where(\questionSet -&gt; questionSet.CodeIdentifier == &quot;BOPBuildingSupp&quot;),bopLocation.PolicyLocation,null,true)"
            editable="bopLine?.Branch?.profileChange_TDIC"
            id="QuestionSet"/>
        </Card>
      </CardViewPanel>
    </Screen>
    <Code><![CDATA[
function setNSRValues() : Modifier[]{
  if(building!=null) {
    var mod=building.BOPBuildingModifiers_TDIC
    return mod.where(\ modi -> not modi.ScheduleRate )
  }
  return new Modifier[]{}
}
function setSRValues():Modifier[]{
  var SRRates : Modifier[]
  if(building!=null) {
    var mod=building.BOPBuildingModifiers_TDIC
    SRRates =  mod.where(\modi -> modi.ScheduleRate) as Modifier[]
  } 
 return SRRates
}
function setInitialQuoteType():QuoteType{
  if(bopLine.Branch.Job typeis Submission){
    return (bopLine.Branch.Job as Submission).QuoteType
  }
  return null
}

function isIRPMRatingInputsEditable(): boolean {
  if (not User.util.CurrentUser.ExternalUser) {
    if (building.PolicyPeriod.Job typeis Submission or
        building.PolicyPeriod.Job typeis PolicyChange or
        (building.PolicyPeriod.Job typeis Renewal and building.PolicyPeriod.BasedOn?.Status != PolicyPeriodStatus.TC_LEGACYCONVERSION)) {
      return perm.System.vieweditirpm_tdic
    }
  }  
  return false
}

function isIRPMRatingInputsVisible(): boolean {
  if (not User.util.CurrentUser.ExternalUser) {
      return (perm.System.vieweditirpm_tdic or perm.System.viewirpmissuance_tdic)
  }
  return false
}

]]></Code>
  </Popup>
</PCF>