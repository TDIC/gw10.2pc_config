<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <!-- The reason a specific input set is used for this coverage is that the
coverage requires a list view of the schedule items. -->
  <InputSet
    id="CoverageInputSet"
    mode="WC7PartnersOfficersAndOthersExclEndorsementExcl">
    <Require
      name="exclusionPattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable.PolicyLine as WC7Line"
      name="theWC7Line"
      type="productmodel.WC7Line"/>
    <Variable
      initialValue="gw.plugin.Plugins.get(gw.plugin.contact.IContactConfigPlugin)"
      name="contactConfigPlugin"
      type="gw.plugin.contact.IContactConfigPlugin"/>
    <Variable
      initialValue="theWC7Line.WC7PolicyOwnerOfficers.where( \ owner -&gt; !owner.isExcluded_TDIC)"
      name="unassignedOwnerOfficers"
      recalculateOnRefresh="true"
      type="WC7PolicyOwnerOfficer[]"/>
    <Variable
      initialValue="theWC7Line.OwnerOfficerOtherCandidates"
      name="otherContacts"
      recalculateOnRefresh="true"
      type="entity.AccountContact[]"/>
    <HiddenInput
      id="ExclusionPatternDisplayName"
      value="exclusionPattern.DisplayName"
      valueType="java.lang.String"/>
    <InputGroup
      allowToggle="exclusionPattern.allowToggle(coverable)"
      childrenVisible="coverable.getCoverageConditionOrExclusion(exclusionPattern) != null"
      id="ExclusionInputGroup"
      label="exclusionPattern.DisplayName"
      onToggle="new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, exclusionPattern, VALUE)">
      <ListViewInput
        id="OwnerOfficersLV"
        label="DisplayKey.get(&quot;Web.Policy.Contact.OwnerOfficers&quot;)"
        labelAbove="true">
        <Toolbar>
          <AddButton
            hideIfReadOnly="true"
            id="AddContactsButton"
            iterator="ContactLV"
            label="DisplayKey.get(&quot;Web.Contact.Add&quot;)">
            <AddMenuItemIterator
              __disabled="true"
              elementName="contactType"
              value="contactConfigPlugin.getAllowedContactTypesForPolicyContactRoleType(typekey.PolicyContactRole.TC_POLICYOWNEROFFICER)"
              valueType="typekey.ContactType[]">
              <IteratorSort
                sortBy="contactType.DisplayName"
                sortOrder="1"/>
              <AddMenuItem
                conversionExpression="PickedValue as WC7ExcludedOwnerOfficer"
                id="ContactType"
                iterator="ContactLV"
                label="DisplayKey.get(&quot;Web.Contact.AddNewOfType&quot;, contactType)"
                pickLocation="WC7NewOwnerOfficerPopup.push(theWC7Line.Branch.WC7Line, contactType, exclusionPattern)"/>
            </AddMenuItemIterator>
            <AddMenuItem
              __disabled="true"
              conversionExpression="addOwnerOfficerForContactOnExclusion(PickedValue)"
              id="AddFromSearch"
              iterator="ContactLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.FromAddressBook&quot;)"
              pickLocation="ContactSearchPopup.push(TC_OWNEROFFICER)"/>
            <AddMenuItem
              id="AddExistingOwnerOfficerContact"
              iterator="ContactLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddExisting&quot;, PolicyOwnerOfficer.Type.TypeInfo.DisplayName)"
              visible="true">
              <AddMenuItemIterator
                elementName="ownerOfficer"
                id="ContactOfSameType"
                value="theWC7Line.WC7PolicyOwnerOfficersSubtractExcluded"
                valueType="entity.WC7PolicyOwnerOfficer[]">
                <IteratorSort
                  sortBy="ownerOfficer.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="UnassignedOwnerOfficer"
                  iterator="ContactLV"
                  label="ownerOfficer"
                  toCreateAndAdd="theWC7Line.addExcludedOwnerOfficer_TDIC(ownerOfficer)"/>
              </AddMenuItemIterator>
              <AddMenuItem
                id="AddAll"
                iterator="ContactLV"
                label="DisplayKey.get(&quot;Web.Account.Contact.AddAll&quot;)"
                toCreateAndAdd="theWC7Line.addAllExistingOwnerOfficersToExclusion()"
                visible="theWC7Line.WC7PolicyOwnerOfficersSubtractExcluded.length &gt; 0"/>
            </AddMenuItem>
            <AddMenuItem
              id="AddExistingEntityOwnerContact"
              iterator="ContactLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddExisting&quot;, PolicyEntityOwner_TDIC.Type.TypeInfo.DisplayName)"
              visible="true">
              <AddMenuItemIterator
                elementName="entityOwner"
                id="ContactOfSameType"
                value="theWC7Line.WC7PolicyEntityOwnersSubtractExcluded_TDIC"
                valueType="entity.PolicyEntityOwner_TDIC[]">
                <IteratorSort
                  sortBy="OwnerOfficer.Type.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="UnassignedEntityOwner"
                  iterator="ContactLV"
                  label="entityOwner"
                  toCreateAndAdd="theWC7Line.addExcludedOwnerOfficer_TDIC(entityOwner)"/>
              </AddMenuItemIterator>
              <AddMenuItem
                id="AddAll"
                iterator="ContactLV"
                label="DisplayKey.get(&quot;Web.Account.Contact.AddAll&quot;)"
                toCreateAndAdd="theWC7Line.addAllExistingPolicyEntityOwnersToExclusion_TDIC()"
                visible="theWC7Line.WC7PolicyEntityOwnersSubtractExcluded_TDIC.length &gt; 0"/>
            </AddMenuItem>
            <AddMenuItem
              __disabled="true"
              id="AddOtherContact"
              iterator="ContactLV"
              label="DisplayKey.get(&quot;Web.Policy.Contact.AddOtherContacts&quot;)"
              visible="otherContacts.Count &gt; 0">
              <AddMenuItemIterator
                elementName="otherContact"
                id="ContactOfOtherType"
                value="otherContacts"
                valueType="entity.AccountContact[]">
                <IteratorSort
                  sortBy="otherContact.DisplayName"
                  sortOrder="1"/>
                <AddMenuItem
                  id="OtherContact"
                  iterator="ContactLV"
                  label="otherContact"
                  toCreateAndAdd="addOwnerOfficerForContactOnExclusion(otherContact.Contact)"/>
              </AddMenuItemIterator>
            </AddMenuItem>
          </AddButton>
          <IteratorButtons
            addVisible="false"
            iterator="ContactLV"/>
        </Toolbar>
        <ListViewPanel
          editable="true"
          id="ContactLV">
          <RowIterator
            canPick="false"
            checkBoxVisible="true"
            editable="openForEdit"
            elementName="policyOwnerOfficer"
            hasCheckBoxes="true"
            hideCheckBoxesIfReadOnly="true"
            toRemove="theWC7Line.removeWC7ExcludedPolicyOwnerOfficer(policyOwnerOfficer)"
            value="theWC7Line.WC7PolicyOwnerOfficers.where( \ owner -&gt; owner.isExcluded_TDIC )"
            valueType="entity.WC7PolicyOwnerOfficer[]">
            <Row>
              <TextCell
                action="EditPolicyContactRolePopup.push(policyOwnerOfficer, openForEdit)"
                id="DispName"
                label="DisplayKey.get(&quot;Web.Policy.NamedInsured.NameOnly&quot;)"
                value="policyOwnerOfficer.DisplayName"
                valueType="java.lang.String"/>
              <RangeCell
                align="left"
                editable="true"
                id="State"
                label="DisplayKey.get(&quot;Web.Policy.WC.InclExcl.State&quot;)"
                required="true"
                value="policyOwnerOfficer.Jurisdiction"
                valueRange="theWC7Line.WC7Jurisdictions.map(\j -&gt; j.Jurisdiction)"
                valueType="typekey.Jurisdiction">
                <PostOnChange/>
              </RangeCell>
              <TypeKeyCell
                editable="true"
                filter="isOrgTypeCorp() ? Relationship.TF_WC7OWNEROFFICERRELATIONSHIP.TypeKeys.contains(VALUE) : Relationship.TF_TDICWC7PARTNERSHIP.TypeKeys.contains(VALUE)"
                flatten="true"
                id="Relationship"
                label="DisplayKey.get(&quot;Web.Policy.WC.InclExcl.Relationship&quot;)"
                required="true"
                value="policyOwnerOfficer.RelationshipTitle"
                valueType="typekey.Relationship"/>
              <TextCell
                align="right"
                editable="true"
                id="Ownership"
                label="DisplayKey.get(&quot;Web.Policy.WC.InclExcl.Ownership&quot;)"
                required="true"
                value="policyOwnerOfficer.WC7OwnershipPct"
                valueType="java.lang.Integer"/>
              <TextCell
                id="Role"
                label="DisplayKey.get(&quot;TDIC.Web.Policy.OwnerOfficer.Role&quot;)"
                value="policyOwnerOfficer.IntrinsicType.DisplayName"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputGroup>
    <InputDivider
      visible="openForEdit or coverable.getCoverageConditionOrExclusion(exclusionPattern) != null"/>
    <Code><![CDATA[function addOwnerOfficerForContactOnExclusion(ownerOfficer : WC7PolicyOwnerOfficer) : WC7PolicyOwnerOfficer {
  return theWC7Line.addExcludedOwnerOfficer_TDIC(ownerOfficer)
}

function addAllOwnerOfficersOnExclusion() : WC7PolicyOwnerOfficer[] {
  var resultOfficers = theWC7Line.addAllExistingOwnerOfficersToExclusion(theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
  return resultOfficers.toTypedArray()
}

function addOwnerOfficerForContactOnExclusion(aContact : entity.Contact) : WC7ExcludedOwnerOfficer {
  var newOfficer = theWC7Line.addExcludedOwnerOfficer(aContact, theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
  return newOfficer
}
/*  OOTB
function addAllOwnerOfficersOnExclusion() : WC7ExcludedOwnerOfficer[] {
  var resultOfficers = theWC7Line.addAllExistingOwnerOfficersToExclusion(theWC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl)
  return resultOfficers.toTypedArray()
}
*/
function isOrgTypeCorp() : Boolean {
  return theWC7Line.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_LLC 
      or theWC7Line.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_PRIVATECORP
}]]></Code>
  </InputSet>
</PCF>