<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <Popup
    countsAsWork="false"
    id="WC7ClassCodeSearchPopup"
    returnType="WC7ClassCode"
    title="DisplayKey.get(&quot;Web.ClassCodeSearch&quot;)">
    <LocationEntryPoint
      signature="WC7ClassCodeSearchPopup(locationWM : PolicyLocation, wcLine : WC7WorkersCompLine, previousWC7ClassCode : WC7ClassCode, excludedClassCodeTypes : java.util.List&lt;WC7ClassCodeType&gt;)"/>
    <LocationEntryPoint
      signature="WC7ClassCodeSearchPopup(locationWM : PolicyLocation, wcLine : WC7WorkersCompLine, previousWC7ClassCode : WC7ClassCode, classCodeType : WC7ClassCodeType, programType : WC7ClassCodeProgramType)"/>
    <LocationEntryPoint
      signature="WC7ClassCodeSearchPopup(locationWM : PolicyLocation, wcLine : WC7WorkersCompLine,  classCodeType : WC7ClassCodeType)"/>
    <LocationEntryPoint
      signature="WC7ClassCodeSearchPopup( polJurisdiction : Jurisdiction, wcLine : WC7WorkersCompLine)"/>
    <Variable
      name="locationWM"
      type="PolicyLocation"/>
    <Variable
      name="wcLine"
      type="WC7WorkersCompLine"/>
    <Variable
      name="previousWC7ClassCode"
      type="WC7ClassCode"/>
    <Variable
      name="classCodeType"
      type="WC7ClassCodeType"/>
    <Variable
      name="programType"
      type="WC7ClassCodeProgramType"/>
    <Variable
      name="excludedClassCodeTypes"
      type="java.util.List&lt;WC7ClassCodeType&gt;"/>
    <Variable
      name="polJurisdiction"
      type="Jurisdiction"/>
    <Screen
      id="WC7ClassCodeSearchScreen">
      <DetailViewPanel>
        <InputColumn>
          <TextInput
            boldValue="true"
            id="LocationLabel"
            value="locationWM != null ? DisplayKey.get(&quot;Web.Policy.WC.ClassCode.Location&quot;, locationWM.DisplayName ) : &quot;&quot;"
            valueType="java.lang.String"/>
        </InputColumn>
      </DetailViewPanel>
      <SearchPanel
        cachingEnabled="false"
        criteriaName="searchCriteria"
        resultsName="wcClassCodes"
        search="searchCriteria.performSearch()"
        searchCriteria="createCriteria()"
        searchCriteriaType="gw.lob.wc7.WC7ClassCodeSearchCriteria"
        searchOnEnter="true"
        searchResultsType="gw.api.database.IQueryBeanResult&lt;WC7ClassCode&gt;">
        <PanelRef>
          <DetailViewPanel
            id="WC7ClassCodeSearchDV">
            <InputColumn>
              <TextInput
                editable="true"
                id="Code"
                label="DisplayKey.get(&quot;Web.ClassCodeSearch.Code&quot;)"
                required="false"
                value="searchCriteria.Code"
                valueType="java.lang.String"/>
              <TextInput
                editable="true"
                id="Classification"
                label="DisplayKey.get(&quot;Web.ClassCodeSearch.Classification&quot;)"
                required="false"
                value="searchCriteria.Classification"
                valueType="java.lang.String"/>
              <BooleanRadioInput
                editable="true"
                id="ContractingClassCodes"
                label="DisplayKey.get(&quot;Web.ClassCodeSearch.ContractingClassCodes&quot;)"
                required="false"
                value="searchCriteria.ConstructionType"
                visible="var criteria = createCriteria(); criteria.ConstructionType = true; return criteria.performSearch().Count &gt; 0"/>
            </InputColumn>
            <InputFooterSection>
              <InputSetRef
                def="SearchAndResetInputSet()"/>
            </InputFooterSection>
          </DetailViewPanel>
        </PanelRef>
        <PanelRef>
          <TitleBar
            title="DisplayKey.get(&quot;Web.Search.Results&quot;)"/>
          <Toolbar/>
          <ListViewPanel
            id="ClassCodeSearchResultsLV">
            <RowIterator
              editable="false"
              elementName="wcClassCode"
              pickValue="wcClassCode"
              value="wcClassCodes"
              valueType="gw.api.database.IQueryBeanResult&lt;entity.WC7ClassCode&gt;">
              <Row>
                <TextCell
                  id="Code"
                  label="DisplayKey.get(&quot;Web.ClassCodeSearch.Code&quot;)"
                  value="wcClassCode.Code"
                  valueType="java.lang.String"/>
                <TextCell
                  align="left"
                  id="Classification"
                  label="DisplayKey.get(&quot;Web.ClassCodeSearch.Classification&quot;)"
                  value="wcClassCode.Classification"
                  valueType="java.lang.String"/>
                <TypeKeyCell
                  id="WC7ClassCodeType"
                  label="DisplayKey.get(&quot;Web.Policy.WC7.ClassCode.ClassCodeType&quot;)"
                  value="wcClassCode.ClassCodeType"
                  valueType="typekey.WC7ClassCodeType"/>
                <DateCell
                  id="EffectiveDate_TDIC"
                  label="DisplayKey.get(&quot;TDIC.EffectiveDate&quot;)"
                  value="wcClassCode.EffectiveDate"
                  visible="perm.System.ratebookview"/>
                <DateCell
                  id="ExpirationDate_TDIC"
                  label="DisplayKey.get(&quot;TDIC.ExpirationDate&quot;)"
                  value="wcClassCode.ExpirationDate"
                  visible="perm.System.ratebookview"/>
                <TextCell
                  id="RegulatoryRate_TDIC"
                  label="DisplayKey.get(&quot;TDIC.RegulatoryRate&quot;)"
                  value="wcClassCode.RegulatoryRate_TDIC"
                  valueType="java.math.BigDecimal"
                  visible="perm.System.ratebookview"/>
              </Row>
            </RowIterator>
          </ListViewPanel>
        </PanelRef>
      </SearchPanel>
    </Screen>
    <Code><![CDATA[uses gw.api.util.JurisdictionMappingUtil
uses gw.lob.wc7.WC7ClassCodeSearchCriteria

function createCriteria() : WC7ClassCodeSearchCriteria {
  var criteria = new WC7ClassCodeSearchCriteria()
  criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(locationWM))
  if (previousWC7ClassCode != null) {
    criteria.PreviousSelectedClassCode = previousWC7ClassCode
  }
  if (classCodeType != null) {
    criteria.ClassCodeType = classCodeType
  } 
  if (programType != null) {
    criteria.ProgramType = programType
  }
  if(locationWM != null){
    criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(locationWM))
    criteria.Jurisdiction = typekey.Jurisdiction.get(JurisdictionMappingUtil.getJurisdiction(locationWM) as String)
  }else{
    criteria.EffectiveAsOfDate = wcLine.getReferenceDateForCurrentJob(polJurisdiction)
    criteria.Jurisdiction = polJurisdiction
  } 
  criteria.ExcludedClassCodeTypes = excludedClassCodeTypes

  return criteria
}]]></Code>
  </Popup>
</PCF>