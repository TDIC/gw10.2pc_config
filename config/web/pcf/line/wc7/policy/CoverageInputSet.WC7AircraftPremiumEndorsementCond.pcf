<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <!-- The reason a specific input set is used for this coverage is that the
coverage requires a list view of the schedule items. -->
  <InputSet
    id="CoverageInputSet"
    mode="WC7AircraftPremiumEndorsementCond">
    <Require
      name="conditionPattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable.PolicyLine as WC7Line"
      name="theWC7Line"
      type="productmodel.WC7Line"/>
    <HiddenInput
      id="ConditionPatternDisplayName"
      value="conditionPattern.DisplayName"
      valueType="java.lang.String"/>
    <InputGroup
      allowToggle="conditionPattern.allowToggle(coverable)"
      childrenVisible="coverable.getCoverageConditionOrExclusion(conditionPattern) != null"
      id="ConditionInputGroup"
      label="conditionPattern.DisplayName"
      onToggle="new gw.pcf.WC7CoverageInputSetUIHelper().toggleClause(theWC7Line, conditionPattern, VALUE)">
      <ListViewInput
        editable="openForEdit"
        label="DisplayKey.get(&quot;Web.Policy.WC.AircraftSeatCharge&quot;)"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="WC7AircraftSeatSurchargeLV"/>
        </Toolbar>
        <ListViewPanel
          id="WC7AircraftSeatSurchargeLV">
          <RowIterator
            editable="true"
            elementName="wc7AircraftSeat"
            hasCheckBoxes="true"
            hideCheckBoxesIfReadOnly="true"
            toCreateAndAdd="theWC7Line.createAndAddWC7AircraftSeat(theWC7Line.WC7AircraftPremiumEndorsementCond)"
            toRemove="theWC7Line.removeFromWC7AircraftSeats( wc7AircraftSeat )"
            type="WC7AircraftSeat"
            value="theWC7Line.WC7AircraftSeats"
            valueType="entity.WC7AircraftSeat[]">
            <IteratorSort
              sortBy="wc7AircraftSeat.Jurisdiction"
              sortOrder="1"/>
            <IteratorSort
              sortBy="wc7AircraftSeat.AircraftNumber"
              sortOrder="2"/>
            <IteratorSort
              sortBy="wc7AircraftSeat.Description"
              sortOrder="3"/>
            <Row>
              <RangeCell
                editable="true"
                id="state"
                label="DisplayKey.get(&quot;Web.Policy.WC.AircraftSeatCharge.State&quot;)"
                required="true"
                value="wc7AircraftSeat.Jurisdiction"
                valueRange="theWC7Line.AircraftSeatsJurisdictions.sortBy(\ j -&gt; j.Code)"
                valueType="typekey.Jurisdiction"/>
              <TextCell
                editable="true"
                id="WC7AircraftSeatDesc"
                label="DisplayKey.get(&quot;Web.Policy.WC.AircraftSeatCharge.Description&quot;)"
                required="true"
                value="wc7AircraftSeat.Description"
                valueType="java.lang.String"/>
              <TextCell
                editable="true"
                id="WC7AircraftNumber"
                label="DisplayKey.get(&quot;Web.Policy.WC7.AircraftID&quot;)"
                value="wc7AircraftSeat.AircraftNumber"
                valueType="java.lang.String"/>
              <TextCell
                editable="canEditAircraftSeat(wc7AircraftSeat) "
                id="WC7AircraftSeatCount"
                label="DisplayKey.get(&quot;Web.Policy.WC.AircraftSeatCharge.NumberOfSeats&quot;)"
                required="true"
                value="wc7AircraftSeat.PassengerSeats"
                valueType="java.lang.Integer"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputGroup>
    <InputDivider
      visible="openForEdit or coverable.getCoverageConditionOrExclusion(conditionPattern) != null"/>
    <Code><![CDATA[function canEditAircraftSeat(wc7AircraftSeat : entity.WC7AircraftSeat) : boolean {
  return gw.api.util.DateUtil.compareIgnoreTime(wc7AircraftSeat.EffectiveDate, theWC7Line.Branch.EditEffectiveDate) == 0
}]]></Code>
  </InputSet>
</PCF>