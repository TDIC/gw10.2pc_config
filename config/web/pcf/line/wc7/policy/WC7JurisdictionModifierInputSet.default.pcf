<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="pcf.xsd">
  <InputSet
    editable="openForEdit"
    id="WC7JurisdictionModifierInputSet"
    mode="default">
    <Require
      name="modifiers"
      type="java.util.List&lt;WC7Modifier&gt;"/>
    <Require
      name="policyPeriod"
      type="PolicyPeriod"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Require
      name="stateConfig"
      type="gw.lob.wc7.stateconfigs.WC7StateConfig"/>
    <Variable
      initialValue="modifiers?.where( \ mod -&gt; mod.DataType != TC_RATE or mod.ScheduleRate)?.toTypedArray()"
      name="nonRateOrSchedRateModifiers"
      type="Modifier[]"/>
    <Variable
      initialValue="new gw.pcf.WC7ModifiersInputSetUIHelper()"
      name="uiHelper"
      type="gw.pcf.WC7ModifiersInputSetUIHelper"/>
    <Variable
      initialValue="(modifiers?.first()?.OwningModifiable as WC7Jurisdiction)?.getSlice(policyPeriod.EditEffectiveDate)"
      name="wc7JurisdictionSliced"
      recalculateOnRefresh="true"
      type="entity.WC7Jurisdiction"/>
    <InputIterator
      elementName="rateModifier"
      id="ModIteratorEligible"
      value="modifiers.where( \ mod -&gt; mod.DataType == TC_RATE and not mod.ScheduleRate).toTypedArray()"
      valueType="entity.WC7Modifier[]">
      <IteratorSort
        sortBy="rateModifier.Pattern.Priority"
        sortOrder="1"/>
      <IteratorSort
        sortBy="rateModifier.Pattern.Name"
        sortDirection="descending"
        sortOrder="2"/>
      <InputGroup
        allowToggle="uiHelper.rateModifierAvailable(rateModifier, policyPeriod)"
        childrenVisible="rateModifier.Eligible"
        id="EligibilityInputGroup"
        label="DisplayKey.get(&quot;Web.Policy.Modifiers.Eligibility&quot;, stateConfig.getModifierName(rateModifier.Pattern))"
        onToggle="toggleEligible(rateModifier)">
        <HiddenInput
          id="ModifierName"
          value="rateModifier.Pattern.Name"
          valueType="java.lang.String"/>
        <TextInput
          action="WC7ScheduleCreditPopup.push(rateModifier, policyPeriod.OpenForEdit)"
          actionAvailable="rateModifier.Pattern.CodeIdentifier == &quot;WC7ScheduleMod&quot;"
          editable="rateModifier.Pattern.CodeIdentifier != &quot;WC7ScheduleMod&quot;"
          id="ModifierInput"
          label="stateConfig.getModifierName(rateModifier.Pattern.Name)"
          requestValidationExpression="rateModifier.getModifierValidation(VALUE as String)"
          value="rateModifier.RateWithinLimits"
          valueType="java.math.BigDecimal"/>
        <BooleanRadioInput
          editable="true"
          id="ValueFinal"
          label="DisplayKey.get(&quot;Web.Policy.Modifiers.ExModFinal&quot;)"
          value="rateModifier.ValueFinal"
          visible="rateModifier.PatternCode == &quot;WC7ExpMod&quot;"/>
        <PostOnChange
          disablePostOnEnter="uiHelper.rateModifierDisablePostOnChange(rateModifier)"
          onChange="uiHelper.updateModifierDependencies(rateModifier); policyPeriod.WC7Line.WC7Jurisdictions.firstWhere( \ slicedJurisdiction -&gt; slicedJurisdiction.FixedId == rateModifier.WC7Jurisdiction.FixedId).syncModifiers();"/>
        <RangeInput
          available="rateModifier.PatternCode == &quot;WC7ExpMod&quot;"
          editable="uiHelper.filterTypeKeyExperienceModifierStatus(rateModifier).Count &gt; 1"
          id="ExpModStatus"
          label="DisplayKey.get(&quot;Web.Policy.WC7.Modifier.ExperienceModifierStatus&quot;)"
          value="rateModifier.ExperienceModifierStatus"
          valueRange="uiHelper.filterTypeKeyExperienceModifierStatus(rateModifier)"
          valueType="typekey.WC7ExpModStatus"
          visible="rateModifier.PatternCode == &quot;WC7ExpMod&quot;"/>
      </InputGroup>
    </InputIterator>
    <InputIterator
      elementName="modifier"
      id="ModIterator"
      value="modifiers.where( \ mod -&gt; mod.DataType != TC_RATE or mod.ScheduleRate).toTypedArray()"
      valueType="entity.WC7Modifier[]">
      <IteratorSort
        sortBy="modifier.Pattern.Priority"
        sortOrder="1"/>
      <IteratorSort
        sortBy="modifier.Pattern.Name"
        sortDirection="descending"
        sortOrder="2"/>
      <HiddenInput
        id="ModifierName"
        value="modifier.Pattern.Name"
        valueType="java.lang.String"/>
      <TextInput
        action="WC7ScheduleCreditPopup.push(modifier, policyPeriod.OpenForEdit)"
        actionAvailable="policyPeriod.CanViewModifiers and modifier.Pattern.CodeIdentifier == &quot;WC7ScheduleMod&quot;"
        editable="modifier.Pattern.CodeIdentifier != &quot;WC7ScheduleMod&quot;"
        id="ModifierInput_Rate"
        label="modifier.Pattern.Name"
        requestValidationExpression="modifier.getModifierValidation(VALUE as String)"
        value="modifier.RateWithinLimits"
        valueType="java.math.BigDecimal"
        visible="modifier.DataType == ModifierDataType.TC_RATE"/>
      <BooleanRadioInput
        desc="GW-1762 20161201 TJT - editable only by permission"
        editable="canEditBooleanModifier(modifier)"
        id="ModifierInput_Boolean"
        label="stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction)"
        value="modifier.BooleanModifier"
        visible="modifier.DataType == ModifierDataType.TC_BOOLEAN &amp;&amp; !stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction).equalsIgnoreCase(&quot;Multiline Discount&quot;)">
        <PostOnChange
          onChange="wc7JurisdictionSliced.syncModifiers(); onChange_TDIC(modifier);"/>
      </BooleanRadioInput>
      <BooleanRadioInput
        desc="GW-1762 20161201 TJT - editable only by permission"
        editable="canEditBooleanModifier(modifier)"
        falseLabel="&quot;None&quot;"
        id="ModifierInput_Boolean1"
        label="stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction)"
        trueLabel="&quot;All&quot;"
        value="modifier.BooleanModifier"
        visible="modifier.DataType == ModifierDataType.TC_BOOLEAN &amp;&amp; stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction).equalsIgnoreCase(&quot;Multiline Discount&quot;)">
        <PostOnChange
          onChange="wc7JurisdictionSliced.syncModifiers(); onChange_TDIC(modifier);"/>
      </BooleanRadioInput>
      <RangeInput
        available="uiHelper.typekeyModifierAvailable(modifier, policyPeriod)"
        editable="true"
        id="ModifierInput_TypeKey"
        label="modifier.Pattern.Name"
        required="stateConfig.isTypeKeyModifierRequired(modifier)"
        value="modifier.TypeKeyModifier"
        valueRange="uiHelper.filterTypekeyModifier(modifier)"
        valueType="java.lang.String"
        visible="modifier.DataType == ModifierDataType.TC_TYPEKEY">
        <PostOnChange
          disablePostOnEnter="uiHelper.typekeyModifierDisablePostOnChange(modifier)"/>
      </RangeInput>
      <DateInput
        editable="true"
        id="ModifierInput_Date"
        label="stateConfig.getJurisdictionModifierName(modifier,wc7JurisdictionSliced.Jurisdiction)"
        value="modifier.DateModifier"
        visible="modifier.DataType == ModifierDataType.TC_DATE"/>
    </InputIterator>
    <Code><![CDATA[function toggleEligible(mod : Modifier) {
  if (mod.Eligible) {
    mod.RateModifier = null
    mod.Eligible = false
  } else {
    mod.Eligible = true
  }
}

function onChange_TDIC (modifier : WC7Modifier) : void {
  if (policyPeriod.Job typeis Submission and policyPeriod.Job.QuickMode) {
    if (modifier.PatternCode == "WC7TDICMultiLineDiscount") {
      policyPeriod.MultiLineDiscountADANums_TDIC = "QuickQuote";
    }
  }
}

function canEditBooleanModifier(modifier : WC7Modifier) : boolean {
  if(modifier.PatternCode == "WC7CertifiedSafety"){
    return false//perm.System.editsafetygroup_TDIC //GPC-3531 - Safety Program Credit is not editable
  } else if(modifier.PatternCode == "WC7TDICMultiLineDiscount") {
    if(policyPeriod.Job.Subtype == TC_POLICYCHANGE and modifier.BasedOn != null and modifier.BasedOn.BooleanModifier) {
      return false
    }
  }
  return true
}]]></Code>
  </InputSet>
</PCF>