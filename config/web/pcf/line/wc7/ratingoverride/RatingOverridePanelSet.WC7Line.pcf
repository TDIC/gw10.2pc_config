<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <PanelSet
    id="RatingOverridePanelSet"
    mode="WC7Line">
    <Require
      name="period"
      type="PolicyPeriod"/>
    <Variable
      initialValue="period.WC7Line.Costs.cast( WC7Cost )"
      name="lineCosts"
      recalculateOnRefresh="true"
      type="java.util.Set&lt;entity.WC7Cost&gt;"/>
    <Variable
      initialValue="lineCosts.partition(\ c -&gt; c.JurisdictionState).toAutoMap(\ st -&gt; java.util.Collections.emptySet&lt;WC7Cost&gt;())"
      name="partitionCosts"
      recalculateOnRefresh="true"
      type="java.util.Map&lt;typekey.Jurisdiction,java.util.Set&lt;entity.WC7Cost&gt;&gt;"/>
    <Variable
      initialValue="period.WC7Line.RepresentativeJurisdictions.sortBy(\ juris -&gt; juris.Jurisdiction)"
      name="jurisdictions"
      recalculateOnRefresh="true"
      type="entity.WC7Jurisdiction[]"/>
    <PanelIterator
      elementName="jurisdiction"
      value="jurisdictions"
      valueType="entity.WC7Jurisdiction[]">
      <IteratorSort
        sortBy="jurisdictions"
        sortOrder="1"/>
      <Variable
        initialValue="partitionCosts.get(jurisdiction.Jurisdiction)"
        name="stateCosts"
        recalculateOnRefresh="true"
        type="java.util.Set&lt;entity.WC7Cost&gt;"/>
      <Variable
        initialValue="jurisdiction.RatingPeriods"
        name="ratingPeriods"
        recalculateOnRefresh="true"
        type="java.util.List&lt;gw.lob.wc7.rating.WC7RatingPeriod&gt;"/>
      <PanelRef
        visible="not stateCosts.Empty">
        <TitleBar
          title="jurisdiction.DisplayName"
          visible="jurisdictions.Count &gt; 1"/>
        <PanelSet
          id="JurisdictionPanelSet">
          <PanelIterator
            elementName="ratingPeriod"
            value="ratingPeriods.toTypedArray()"
            valueType="gw.lob.wc7.rating.WC7RatingPeriod[]">
            <Variable
              initialValue="stateCosts.byRatingPeriod( ratingPeriod )"
              name="periodCosts"
              recalculateOnRefresh="true"
              type="java.util.Set&lt;entity.WC7Cost&gt;"/>
            <PanelRef
              visible="!periodCosts.Empty">
              <DetailViewPanel
                id="PeriodDetailDV">
                <InputColumn>
                  <Label
                    label="standardPremLabel(ratingPeriods.Count &gt; 1, ratingPeriod)"/>
                  <ListViewInput
                    def="WC7RatingOverrideCostLV(period, periodCosts, jurisdiction, ratingPeriod.RatingStart, ratingPeriod.RatingEnd)">
                    <Toolbar/>
                  </ListViewInput>
                </InputColumn>
              </DetailViewPanel>
            </PanelRef>
          </PanelIterator>
          <PanelRef>
            <DetailViewPanel
              id="StateSummaryDetailDV">
              <InputColumn>
                <Label
                  label="DisplayKey.get(&quot;Web.SubmissionWizard.Quote.WC.OtherPremiumAndTax&quot;)"/>
                <ListViewInput
                  def="WC7RatingOverrideStateCostLV(stateCosts, jurisdiction)"
                  mode="period.Job.Subtype">
                  <Toolbar/>
                </ListViewInput>
              </InputColumn>
            </DetailViewPanel>
          </PanelRef>
        </PanelSet>
      </PanelRef>
    </PanelIterator>
    <Code><![CDATA[function standardPremLabel(splitPeriod : boolean, ratingPeriod : gw.lob.wc7.rating.WC7RatingPeriod ) : String {
  if (splitPeriod) {
    return DisplayKey.get("Web.Quote.WC.StandardPremium.SplitPeriod", gw.api.util.StringUtil.formatDate(ratingPeriod.RatingStart, "short"),
      gw.api.util.StringUtil.formatDate(ratingPeriod.RatingEnd, "short") )
  } else {
    return DisplayKey.get("Web.Quote.WC.StandardPremium.OnePeriod")
  }
}

function sortedDates(jurisdiction : WC7Jurisdiction) : java.util.Date[] {
  var rpsds = jurisdiction.getSortedRPSDs().toList().map(\ r -> r.StartDate)
  rpsds.add(period.PeriodStart)
  java.util.Collections.sort(rpsds)
  return rpsds.toTypedArray()
}]]></Code>
  </PanelSet>
</PCF>