<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Popup
    beforeCommit="period.PolicyAddress.copyFromAddress(tmpAddress);  tmpAddress.remove(); helper.updateAddressStatus(period.PolicyAddress.Address)"
    canEdit="true"
    id="EditPolicyAddressPopup"
    startInEditMode="true"
    title="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail&quot;)">
    <LocationEntryPoint
      signature="EditPolicyAddressPopup(period : PolicyPeriod, isNew : boolean)"/>
    <Variable
      name="period"
      type="PolicyPeriod"/>
    <Variable
      name="isNew"
      type="boolean"/>
    <Variable
      initialValue="initializePolicyAddressAndCreateTmpAddress()"
      name="tmpAddress"
      type="Address"/>
    <Variable
      initialValue="gw.util.concurrent.LocklessLazyVar.make(\ -&gt; period.OpenForEdit)"
      name="openForEditInit"
      recalculateOnRefresh="true"
      type="gw.util.concurrent.LocklessLazyVar&lt;java.lang.Boolean&gt;"/>
    <Variable
      initialValue="period != null ? openForEditInit.get() : CurrentLocation.InEditMode"
      name="openForEdit"
      recalculateOnRefresh="true"
      type="java.lang.Boolean"/>
    <Variable
      name="addressList"
      type="java.util.ArrayList&lt;Address&gt;"/>
    <Variable
      initialValue="false"
      name="overrideVisible"
      type="Boolean"/>
    <Variable
      initialValue="true"
      name="standardizeVisible"
      type="Boolean"/>
    <Variable
      initialValue="false"
      name="updateVisible"
      type="Boolean"/>
    <Variable
      initialValue="new tdic.pc.config.addressverification.AddressVerificationHelper()"
      name="helper"
      type="tdic.pc.config.addressverification.AddressVerificationHelper"/>
    <Variable
      initialValue="period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact == null ? null : period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.AssociationFinder.findLatestBoundPolicyPeriods().where( \ elt -&gt; elt.Policy != period.Policy)"
      name="impactedPolicies_TDIC"
      type="java.util.List&lt;PolicyPeriod&gt;"/>
    <Screen>
      <Toolbar>
        <ToolbarButton
          action="addressList = helper.validateAddress(tmpAddress, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); helper.displayExceptions()"
          available="standardizeVisible"
          id="Standardize"
          label="DisplayKey.get(&quot;Web.AccountFile.Locations.Standardize&quot;)"
          visible="standardizeVisible"/>
        <EditButtons
          updateLabel="overrideVisible ? DisplayKey.get(&quot;Web.AccountFile.Locations.Override&quot;) : DisplayKey.get(&quot;Web.AccountFile.Locations.Update&quot;)"
          updateVisible="updateVisible or overrideVisible"/>
        <ToolbarButton
          action="if (impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact, impactedPolicies_TDIC)}"
          desc="20150515 TJ Talluto: US1296 Discretionary Synchronizing"
          id="ToolbarButton"
          label="DisplayKey.get(&quot;TDIC.ViewContactUsages&quot;)"/>
      </Toolbar>
      <Verbatim
        desc="20150515 TJ Talluto: US1296 Discretionary Synchronizing"
        id="ContactUsageImpact_TDIC"
        label="DisplayKey.get(&quot;TDIC.ChangesToThisAddressMayImpactPolicies&quot;, impactedPolicies_TDIC.Count) + &quot; &quot; + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(&quot;, &quot;)"
        visible="CurrentLocation.InEditMode and impactedPolicies_TDIC.Count &gt; 0"
        warning="true"/>
      <DetailViewPanel>
        <InputColumn>
          <InputSet
            id="PolicyAddressInputSet">
            <InputSetRef
              def="AddressInputSet(new gw.pcf.contacts.AddressInputSetAddressOwner(tmpAddress, false, true))"/>
            <InputSetRef
              def="TDIC_AccountAddressTypeInputSet(tmpAddress)"
              id="AddressTypeRef_TDIC"/>
            <TypeKeyInput
              __disabled="true"
              editable="true"
              id="AddressType"
              label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.AddressType&quot;)"
              required="true"
              value="tmpAddress.AddressType"
              valueType="typekey.AddressType"/>
            <TextInput
              editable="true"
              id="AddressDescription"
              label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.Description&quot;)"
              value="tmpAddress.Description"/>
          </InputSet>
        </InputColumn>
      </DetailViewPanel>
      <PanelRef
        visible="addressList ?.Count &gt; 0 ? true : false">
        <TitleBar
          id="SelectTitle"
          title="DisplayKey.get(&quot;Web.AccountFile.Locations.Suggested&quot;)"
          visible="addressList ?.Count &gt; 0 ? true : false"/>
        <Toolbar/>
        <ListViewPanel>
          <RowIterator
            editable="false"
            elementName="suggestedAddress"
            id="melissaReturn"
            value="addressList"
            valueType="java.util.ArrayList&lt;Address&gt;"
            visible="addressList ?.Count &gt; 0 ? true : false">
            <Row>
              <LinkCell
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)">
                <Link
                  action="helper.replaceAddressWithSuggested(suggestedAddress,tmpAddress, true); addressList = helper.getAddressList(); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible() "
                  id="SelectAddress"
                  label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)"
                  styleClass="miniButton"/>
              </LinkCell>
              <TextCell
                id="AddressLine1"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine1&quot;)"
                value="suggestedAddress.AddressLine1"/>
              <TextCell
                id="AddressLine2"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine2&quot;)"
                value="suggestedAddress.AddressLine2"/>
              <TextCell
                id="city"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.City&quot;)"
                value="suggestedAddress.City"/>
              <TextCell
                id="State"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.State&quot;)"
                value="suggestedAddress.State"
                valueType="typekey.State"/>
              <TextCell
                id="zip"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.PostalCode&quot;)"
                value="suggestedAddress.PostalCode"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </PanelRef>
    </Screen>
    <Code><![CDATA[function initializePolicyAddressAndCreateTmpAddress() : Address {
  if (isNew) {
    // we do this here because none of the page before/after triggers occur at the right time
    var addr = new Address();
    period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.addAddress(addr);
    period.PolicyAddress.assignToSource(addr);
  }
  return period.PolicyAddress.copyToNewAddress()
}]]></Code>
  </Popup>
</PCF>