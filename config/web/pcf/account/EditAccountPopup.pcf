<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Popup
    beforeCommit="helper.updateAddressStatus(selectedAddress)"
    canEdit="true"
    canVisit="account.Editable and perm.Account.edit(account)"
    id="EditAccountPopup"
    parent="AccountFile_Summary(account)"
    startInEditMode="true"
    title="DisplayKey.get(&quot;Web.EditAccount&quot;)">
    <LocationEntryPoint
      signature="EditAccountPopup(account : Account)"/>
    <Variable
      name="account"
      type="Account"/>
    <Variable
      initialValue="account.AccountHolderContact"
      name="contact"
      recalculateOnRefresh="true"
      type="entity.Contact"/>
    <Variable
      initialValue="contact.PrimaryAddress"
      name="selectedAddress"
      recalculateOnRefresh="true"
      type="entity.Address"/>
    <Variable
      initialValue="new tdic.pc.config.addressverification.AddressVerificationHelper()"
      name="helper"
      type="tdic.pc.config.addressverification.AddressVerificationHelper"/>
    <Variable
      name="addressList"
      type="java.util.ArrayList&lt;Address&gt;"/>
    <Variable
      initialValue="true"
      name="standardizeVisible"
      type="Boolean"/>
    <Variable
      initialValue="false"
      name="updateVisible"
      type="Boolean"/>
    <Variable
      initialValue="false"
      name="overrideVisible"
      type="Boolean"/>
    <Variable
      initialValue="false"
      name="revealAccountsIgnoringViewPermission_TDIC"
      type="boolean"/>
    <Variable
      initialValue="contact.AssociationFinder.findLatestBoundPolicyPeriods()"
      name="impactedPolicies_TDIC"
      type="java.util.List&lt;PolicyPeriod&gt;"/>
    <Variable
      initialValue="contact.AssociationFinder.findAccounts().where(\ elt -&gt; elt != account and (perm.Account.view(elt) or revealAccountsIgnoringViewPermission_TDIC))"
      name="impactedAccounts_TDIC"
      type="Account[]"/>
    <Variable
      initialValue="true"
      name="displayMembershipCheckError"
      type="Boolean"/>
    <Screen
      id="EditAccountScreen">
      <Variable
        initialValue="CurrentLocation.InEditMode"
        name="openForEdit"
        type="Boolean"/>
      <Toolbar>
        <ToolbarButton
          action="addressList = helper.validateAddress(selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); updateVisible = helper.getUpdateVisible(); overrideVisible = helper.getOverrideVisible(); helper.displayExceptions();"
          available="standardizeVisible and openForEdit"
          id="StandardizeButton"
          label="DisplayKey.get(&quot;Web.AccountFile.Locations.Standardize&quot;)"
          visible="standardizeVisible and openForEdit"/>
        <EditButtons
          updateLabel="updateVisible ? DisplayKey.get(&quot;Button.Update&quot;) : DisplayKey.get(&quot;Web.AccountFile.Locations.Override&quot;)"
          updateVisible="updateVisible or overrideVisible"/>
        <ToolbarDivider/>
        <ToolbarButton
          action="if (impactedAccounts_TDIC.HasElements or impactedPolicies_TDIC.HasElements){ContactUsageImpact_TDICWorksheet.goInWorkspace(contact, impactedPolicies_TDIC)}"
          desc="20150515 TJ Talluto: US1296 Discretionary Synchronizing"
          id="ToolbarButton"
          label="DisplayKey.get(&quot;TDIC.ViewContactUsages&quot;)"/>
      </Toolbar>
      <Verbatim
        desc="20150515 TJ Talluto: US1296 Discretionary Synchronizing"
        id="ContactUsageImpact_TJT"
        label="DisplayKey.get(&quot;TDIC.ChangesToThisContactMayImpactPolicies&quot;, impactedPolicies_TDIC.Count) + &quot; &quot; + impactedPolicies_TDIC*.PolicyNumberDisplayString.join(&quot;, &quot;)"
        visible="CurrentLocation.InEditMode and impactedPolicies_TDIC.Count &gt; 0"
        warning="true"/>
      <DetailViewPanel>
        <InputColumn>
          <InputSetRef
            def="ContactNameInputSet(contact, true)"
            mode="contact.Subtype.Code"
            visible="contact != null"/>
          <TextInput
            editable="true"
            id="Nickname"
            label="DisplayKey.get(&quot;Web.EditAccount.Nickname&quot;)"
            value="account.Nickname"/>
          <TextInput
            editable="true"
            id="Web"
            label="DisplayKey.get(&quot;TDIC.Web.SubmissionWizard.Account.WebAddress&quot;)"
            value="account.WebAddress_TDIC"
            visible="true"/>
          <TypeKeyInput
            editable="true"
            id="PrimaryLanguage"
            label="DisplayKey.get(&quot;Web.EditAccount.PrimaryLanguage&quot;)"
            value="account.PrimaryLanguage"
            valueType="typekey.LanguageType"
            visible="gw.api.util.LocaleUtil.getAllLanguages().size() &gt; 1"/>
          <TypeKeyInput
            editable="true"
            id="ServiceTier"
            label="DisplayKey.get(&quot;Web.EditAccount.ServiceTier&quot;)"
            value="account.ServiceTier"
            valueType="typekey.CustomerServiceTier"/>
          <InputDivider/>
          <InputSetRef
            def="LinkedAddressInputSet(selectedAddress, contact, null, account, CurrentLocation.InEditMode)"/>
          <InputSetRef
            def="AddressInputSet(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))"
            editable="selectedAddress.LinkedAddress == null"
            visible="contact != null"/>
          <!--<TypeKeyInput
            editable="selectedAddress.LinkedAddress == null"
            id="AddressType"
            label="DisplayKey.get(&quot;Web.AddressDetail.AddressType&quot;)"
            required="true"
            value="selectedAddress.AddressType"
            valueType="typekey.AddressType"/>-->
          <InputSetRef
            def="TDIC_AccountAddressTypeInputSet(selectedAddress)"
            id="AddressTypeRef_TDIC"/>
          <TextInput
            editable="selectedAddress.LinkedAddress == null"
            id="AddressDescription"
            label="DisplayKey.get(&quot;Web.AddressDetail.Description&quot;)"
            value="selectedAddress.Description"/>
          <InputDivider/>
          <InputSetRef
            def="AccountCurrencyInputSet(account, selectedAddress, false)"
            editable="account.Editable"
            id="AccountCurrency"
            visible="gw.api.util.CurrencyUtil.isMultiCurrencyMode()"/>
          <InputDivider
            visible="gw.api.util.CurrencyUtil.isMultiCurrencyMode()"/>
          <Label
            label="DisplayKey.get(&quot;Web.AccountFile.Summary.OfficialIDs&quot;)"
            visible="contact != null"/>
          <InputSetRef
            def="OfficialIDInputSet(contact, null)"
            mode="contact.Subtype"
            visible="contact != null"/>
          <InputSet
            visible="account.AccountHolderContact typeis Company">
            <TypeKeyInput
              __disabled="true"
              editable="true"
              id="OrgType"
              label="DisplayKey.get(&quot;Web.EditAccount.OrgType&quot;)"
              value="account.AccountOrgType"
              valueType="typekey.AccountOrgType"/>
            <IndustryCodeInput
              clearEnabled="true"
              domain="typekey.IndustryCodeType.TC_SIC"
              editable="true"
              id="IndustryCode"
              label="DisplayKey.get(&quot;Web.EditAccount.IndustryCode&quot;)"
              pickLocation="IndustryCodeSearchPopup.push(typekey.IndustryCodeType.TC_SIC)"
              value="account.IndustryCode"/>
            <TextInput
              __disabled="true"
              desc="Jira GW-260: disabled"
              editable="true"
              id="DescriptionOfBusiness"
              label="DisplayKey.get(&quot;Web.EditAccount.DescriptionOfBusiness&quot;)"
              value="account.BusOpsDesc"/>
          </InputSet>
        </InputColumn>
      </DetailViewPanel>
      <PanelRef
        visible="addressList.Count &gt; 0">
        <TitleBar
          id="SelectTitle"
          title="DisplayKey.get(&quot;Web.AccountFile.Locations.Suggested&quot;)"/>
        <Toolbar/>
        <ListViewPanel>
          <RowIterator
            editable="false"
            elementName="suggestedAddress"
            id="melissaReturn"
            value="addressList.toTypedArray()"
            valueType="entity.Address[]">
            <Row>
              <LinkCell
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)">
                <Link
                  action="helper.replaceAddressWithSuggested(suggestedAddress, selectedAddress, true); standardizeVisible = helper.getStandardizeVisible(); overrideVisible = helper.getOverrideVisible(); updateVisible = helper.getUpdateVisible(); addressList = null"
                  id="SelectAddress"
                  label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)"
                  styleClass="miniButton"/>
              </LinkCell>
              <TextCell
                id="AddressLine1"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine1&quot;)"
                value="suggestedAddress.AddressLine1"/>
              <TextCell
                id="AddressLine2"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine2&quot;)"
                value="suggestedAddress.AddressLine2"/>
              <TextCell
                id="city"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.City&quot;)"
                value="suggestedAddress.City"/>
              <TextCell
                id="State"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.State&quot;)"
                value="suggestedAddress.State"
                valueType="typekey.State"/>
              <TextCell
                id="zip"
                label="DisplayKey.get(&quot;Web.AccountFile.Locations.PostalCode&quot;)"
                value="suggestedAddress.PostalCode"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </PanelRef>
    </Screen>
  </Popup>
</PCF>