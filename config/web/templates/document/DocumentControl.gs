package web.templates.document

<html>
<head>
  <title>Document Control</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="Cache-Control" content="no-cache">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="0">
</head>
<body onLoad="initDocumentOperations()">
<script>
function get_window_top (){
  if (window.top.selenium_myiframe ) {
    return window.top.selenium_myiframe;
  } else {
      return window.top;
  }
}

function initDocumentOperations() {
  var window_top = get_window_top();
  var localizedErrorMessage = "<%= localizedErrorMessage %>"
  if (window_top.top_frame && window_top.top_frame.DocumentUtil && window_top.top_frame.DocumentUtil.initDocumentOperations) {
<% if (allowActiveX) { %>
    window_top.top_frame.DocumentUtil.initDocumentOperations(localizedErrorMessage);
<% } %>
  }
  <% printContent(extraJavascript, false) %>
}

function resumeDocumentOperations() {
  var window_top = get_window_top();
  if (window_top.top_frame && window_top.top_frame.DocumentUtil && window_top.top_frame.DocumentUtil.resumeDocumentOperations) {
    window_top.top_frame.DocumentUtil.resumeDocumentOperations();
  }
  <% printContent(extraJavascript, false) %>
}
</script>
<%-- Omit the ActiveX control and upload form if the server is so configured --%>
<% if (allowActiveX) { %>
  <form name="fileUploadForm" action="<%=docUploadAction%>" method="post" enctype="multipart/form-data">
    <input name="docId" type="hidden" id="docId"/>
    <input name="fileContent" type="file" id="fileContent" size="60"/>
  </form>
<%--
  NOTE: The following is an ActiveX object. It is vital that the CLASSID be kept
        in sync with the CLSID specified in the sample TemplateRunner.HTM file.

  CLASSID="CLSID:CD429DD9-0DEE-47FA-B203-C862218AD6B5"
  CODEBASE="<%= activexCodebase %>/GuidewireDocumentAssistant.CAB#version=2,0,0,46"
--%>
<OBJECT ID="templateRunner"
  CLASSID="CLSID:01A307B7-5CB5-4D91-A830-68BC53F12FD6"
  CODEBASE="<%= activexCodebase %>/GuidewireDocumentAssistantConfigurable.CAB#version=2,1,0,1">
  STYLE="visibility:hidden"
  UNSELECTABLE="on"
  WIDTH="0"
  HEIGHT="0">
  <param name="_ExtentX" value="4207">
  <param name="_ExtentY" value="3334">
</OBJECT>
<% } %>
</body>
</html>
