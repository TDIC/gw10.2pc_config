<?xml version="1.0"?>
<typelist
  xmlns="http://guidewire.com/typelists"
  desc="TDIC_DocCreationEventType"
  name="TDIC_DocCreationEventType">
  <typecode
    code="SecondVoluntaryAudit"
    desc="Voluntary Audit on due date"
    name="Second Voluntary Audit"/>
  <typecode
    code="FinalAuditCompleted"
    desc="Night of final audit completion"
    name="Final Audit Completed"/>
  <typecode
    code="PhysicalAudit"
    desc="30 days after renewal"
    name="Physical Audit"/>
  <typecode
    code="AuditLetterEstimated"
    desc="75 Days after renewal unless payroll received"
    name="Audit Letter Estimated"/>
  <typecode
    code="FirstVoluntaryAudit"
    desc="15 Days after renewal"
    name="First Voluntary Audit"/>
  <typecode
    code="Withdraw"
    desc="Withdrawn Job"
    name="Withdraw"/>
  <typecode
    code="Rescission"
    desc="Rescission"
    name="Rescission"/>
  <typecode
    code="NonrenewalNonMembership"
    desc="Nonrenewal for reason of NonMembership"
    name="Nonrenewal NonMembership"/>
  <typecode
    code="Nonrenewal"
    desc="Nonrenewal"
    name="Nonrenewal"/>
  <typecode
    code="Rewrite"
    desc="Rewrite"
    name="Rewrite"/>
  <typecode
    code="Endorsement"
    desc="Endorsement"
    name="Endorsement"/>
  <typecode
    code="Cancellation"
    desc="Cancellation"
    name="Cancellation"/>
  <typecode
    code="Renewal"
    desc="Renewal"
    name="Renewal"/>
  <typecode
    code="Submission"
    desc="Submission"
    name="Submission"/>
  <typecode
    code="FinalizeExstreamDocument"
    desc="Finalizing a document"
    name="Finalization"/>
  <typecode
    code="Reinstatement"
    desc="Reinstatement"
    name="Reinstatement"/>
  <typecode
    code="ScheduleRating"
    desc="Schedule Rating Form"
    name="Schedule Rating"/>
  <typecode
    code="ClaimsKit"
    desc="Claims Kit"
    name="Claims Kit"/>
  <typecode
    code="AuditNotice"
    desc="Audit Notice"
    name="Audit Notice"/>
  <typecode
    code="AuditReminder"
    desc="Audit Reminder"
    name="Audit Reminder"/>
  <typecode
    code="PhysicalAuditLetter"
    desc="Physical Audit Letter"
    name="Physical Audit Letter"/>
  <typefilter
    desc="Document will print forms identified via OOTB forms infernce."
    name="UsesOOTBFormsInfernence">
    <include
      code="Cancellation"/>
    <include
      code="Endorsement"/>
    <include
      code="Reinstatement"/>
    <include
      code="Renewal"/>
    <include
      code="Rewrite"/>
    <include
      code="Submission"/>
  </typefilter>
  <typecode
    code="NoticeOfCancellation"
    desc="Document request for Scheduled Cancellations"
    name="Notice Of Cancellation"/>
  <typecode
    code="PLFacultyMemberDiscount"
    desc="PL Faculty Member Discount ConfirmationDiscount"
    name="PL Faculty Member Discount ConfirmationDiscount"/>
  <typecode
    code="PLGraduateStudentDiscount"
    desc="PL Graduate Student Discount Confirmation"
    name="PL Graduate Student Discount Confirmation"/>
  <typecode
    code="PLPT16Discount"
    desc="PL Part-Time 16 Status Confirmation"
    name="PL Part-Time 16 Status Confirmation"/>
  <typecode
    code="PLPT20Discount"
    desc="PL Part-Time 20 Status Confirmation"
    name="PL Part-Time 20 Status Confirmation"/>
  <typecode
    code="PLDisabilityDiscount"
    desc="PL Disability Discount Confirmation"
    name="PL Disability Discount Confirmation"/>
  <typecode
    code="PLVolunteerPolicy"
    desc="PL Volunteer Policy Status Confirmation"
    name="PL Volunteer Policy Status Confirmation"/>
  <typecode
    code="EndorsementHolderBOP"
    desc="BOP Dental General Liability Addtl Insd - Endmt holder copy"
    name="Endorsement Holder BOP"/>
  <typecode
    code="EndorsementHolderLRP"
    desc="LRP Building Owners Liability Addtl Insd - Endmt holder copy"
    name="Endorsement Holder LRP"/>
  <typecode
    code="CertificateHolderCOIOCC"
    desc="OCC Certificate of Insurance Certificate Holder Copy"
    name="Certificate Holder COI OCC"/>
  <typecode
    code="EndorsementHolderOCCBL"
    desc="OCC Additional Insured BL Endt Holder Copy Endt"
    name="Endorsement Holder OCC BL"/>
  <typecode
    code="EndorsementHolderOCCPL"
    desc="OCC AdditionaI Insured PL Endt holder copy Endt"
    name="Endorsement Holder OCC PL"/>
  <typecode
    code="CertificateHolderCOIPBL"
    desc="PBL Certificate of Insurance - Cert holder copy"
    name="Certificate Holder COI PBL"/>
  <typecode
    code="EndorsementHolderPBLBL"
    desc="PBL Additional Insured BL - Endt holder copy"
    name="Endorsement Holder PBL BL"/>
  <typecode
    code="EndorsementHolderPBLPL"
    desc="PBL Additional Insured - P/L Endt holder copy"
    name="Endorsement Holder PBL PL"/>
  <typecode
    code="CertificateHolderSchoolServicesPBL"
    desc="PBL Additional Insured-School Services Endt - Cert holder copy"
    name="Certificate Holder School Services PBL"
    retired="true"/>
  <typecode
    code="LendersLossPayableBOP"
    desc="BOP Lender&apos;s Loss Payable Endt"
    name="BOP Lender&apos;s Loss Payable Endt"/>
  <typecode
    code="MortgageeEndorsementBOP"
    desc="BOP Mortgagee Endorsement"
    name="BOP Mortgagee Endt"/>
  <typecode
    code="MortgageeEndorsementLRP"
    desc="LRP Mortgagee Endorsement"
    name="LRP Mortgagee Endorsement"/>
  <typecode
    code="NewDentistOffer"
    desc="New Dentist"
    name="New Dentist"/>
  <typecode
    code="EndorsementHolderNOC"
    desc="Endorsement Holder Notice Of Cancellation"
    name="Endorsement Holder NOC"/>
  <typecode
    code="CertificateHolderSSPBL"
    desc="PBL Additional Insured-School Services Endt - Cert holder copy"
    name="Certificate Holder School Services PBL"/>
  <typecode
    code="NWPaymentPlanLetter"
    desc="NW PaymentPlan Letter"
    name="NW PaymentPlan Letter"/>
  <typecode
    code="MortgageeEndorsementLRPWA"
    desc="LRP MortagageeEndorsement for WA"
    name="LRP MortagageeEndorsement for WA"/>
  <typecode
    code="MortgageeEndorsementBOPWA"
    desc="BOP Mortgagee endorsement for WA"
    name="BOP Mortgagee endorsement for WA"/>
</typelist>