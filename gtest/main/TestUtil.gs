package main
uses java.lang.StringBuffer
uses java.io.FileWriter
uses java.io.File

/** 
 * Utility class containing helper methods for the MainTest class. 
 */
class TestUtil {

  /**
   * Method to create the JUnit XML report file.
   */
  public static function createReport(file : String, data : String) {
    var report = new StringBuffer()
    var fw : FileWriter = null
    
    report.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
        
    try {
      fw = new FileWriter(new File(file))
      report.append(data)
      fw.write(report.toString())
      fw.flush()
    } catch (e) {
      print ("Failed to create report file - " + file + " for data - " + data + "\n Exception - " + e.Message)
    } finally {
      if (fw != null)
        fw.close()
    }
  }

}
