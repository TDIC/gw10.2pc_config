package tdic.pc.common.batch.generalledger

uses tdic.pc.common.batch.generalledger.TDIC_GLReportFactory
uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportTypeEnum
uses gw.api.util.DateUtil
uses java.lang.Exception
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_WrittenPremiumPolicies
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_EarnedPremiumLines
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_EarnedPremiumPolicies
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_WrittenPremiumLines

/**
 * US627
 * 12/26/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_GLReportFactory.
 */
@gw.testharness.ServerTest
class TDIC_GLReportFactoryTest extends gw.testharness.TestBase{

  /**
   * Test Premium Policies instance for Written premium report type request
   */
  function testPremiumPoliciesProcessorForWPRM() {
    try{
      var prmProcessor = TDIC_GLReportFactory.getPremiumPoliciesProcessor(TDIC_GLReportTypeEnum.WPRM, DateUtil.currentDate(), DateUtil.currentDate().addMonths(1))
      assertNotNull(prmProcessor)
      assertTrue(prmProcessor typeis TDIC_WrittenPremiumPolicies)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test Premium Policies instance for earned premium report type request
   */
  function testPremiumPoliciesProcessorForEPRM() {
    try{
      var prmProcessor = TDIC_GLReportFactory.getPremiumPoliciesProcessor(TDIC_GLReportTypeEnum.EPRM, DateUtil.currentDate(), DateUtil.currentDate().addMonths(1))
      assertNotNull(prmProcessor)
      assertTrue(prmProcessor typeis TDIC_EarnedPremiumPolicies)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test Premium lines instance for written premium report type request
   */
  function testPremiumLinesForWPRM() {
    try{
      var prmProcessor = TDIC_GLReportFactory.getPremiumPoliciesProcessor(TDIC_GLReportTypeEnum.WPRM, DateUtil.currentDate(), DateUtil.currentDate().addMonths(1))
      var prmLines = prmProcessor.getPremiumLines()
      assertNotNull(prmLines)
      assertTrue(prmLines typeis TDIC_WrittenPremiumLines)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test Premium lines instance for earned premium report type request
   */
  function testPremiumLinesForEPRM() {
    try{
      var prmProcessor = TDIC_GLReportFactory.getPremiumPoliciesProcessor(TDIC_GLReportTypeEnum.EPRM, DateUtil.currentDate(), DateUtil.currentDate().addMonths(1))
      var prmLines = prmProcessor.getPremiumLines()
      assertNotNull(prmLines)
      assertTrue(prmLines typeis TDIC_EarnedPremiumLines)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test Exception message for null report type request
   */
  function testPremiumPoliciesWithNull() {
    var reportType : TDIC_GLReportTypeEnum
    try{
      reportType = null
      var prmProcessor = TDIC_GLReportFactory.getPremiumPoliciesProcessor(reportType, DateUtil.currentDate(), DateUtil.currentDate().addMonths(1))
    }catch(e : Exception){
      assertEquals("Unable to process unknown GL report type ${reportType}.", e.Message)
    }
  }
}