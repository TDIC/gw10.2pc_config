package tdic.pc.common.batch.generalledger.prmpolicies

uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportUtil
uses gw.date.Month
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_EarnedPremiumLines
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_EarnedPremiumPolicies

/**
 * US627
 * 12/29/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_WrittenPremiumPolicies.
 */
@gw.testharness.ServerTest
class TDIC_EarnedPremiumPoliciesTest  extends gw.testharness.TestBase {

  /**
   * Test batch run period start date for dec 2014
   */
  function testGetBatchPeriodStartDate_1(){
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(12,2014)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    var prmProcessor = new TDIC_EarnedPremiumPolicies(startDate, endDate, new TDIC_EarnedPremiumLines(endDate))
    var periodStartDate = prmProcessor.getBatchPeriodStartDate()
    assertEquals(1, periodStartDate.Minute)
    assertEquals(1, periodStartDate.DayOfMonth)
    assertEquals(Month.DECEMBER, periodStartDate.MonthOfYearEnum)
    assertEquals(2014, periodStartDate.YearOfDate)
  }

  /**
   * Test batch run period start date for Jan 2015
   */
  function testGetBatchRunPeriodStartDate_2() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(1,2015)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    var prmProcessor = new TDIC_EarnedPremiumPolicies(startDate, endDate, new TDIC_EarnedPremiumLines(endDate))
    var periodStartDate = prmProcessor.getBatchPeriodStartDate()
    assertEquals(1, periodStartDate.Minute)
    assertEquals(1, periodStartDate.DayOfMonth)
    assertEquals(Month.JANUARY, periodStartDate.MonthOfYearEnum)
    assertEquals(2015, periodStartDate.YearOfDate)
  }

  /**
   * Test batch run period end date for dec 2014
   */
  function testGetBatchPeriodEndDate_1() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(12,2014)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    var prmProcessor = new TDIC_EarnedPremiumPolicies(startDate, endDate, new TDIC_EarnedPremiumLines(endDate))
    var periodStartDate = prmProcessor.getBatchPeriodStartDate()
    var periodEndDate = prmProcessor.getBatchPeriodEndDate(periodStartDate)
    assertEquals(0, periodEndDate.Minute)
    assertEquals(1, periodEndDate.DayOfMonth)
    assertEquals(Month.JANUARY, periodEndDate.MonthOfYearEnum)
    assertEquals(2015, periodEndDate.YearOfDate)
  }

  /**
   * Test batch run period end date for Jan 2015
   */
  function testGetBatchRunPeriodEndDate_2() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(1,2015)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    var prmProcessor = new TDIC_EarnedPremiumPolicies(startDate, endDate, new TDIC_EarnedPremiumLines(endDate))
    var periodStartDate = prmProcessor.getBatchPeriodStartDate()
    var periodEndDate = prmProcessor.getBatchPeriodEndDate(periodStartDate)
    assertEquals(0, periodEndDate.Minute)
    assertEquals(1, periodEndDate.DayOfMonth)
    assertEquals(Month.FEBRUARY, periodEndDate.MonthOfYearEnum)
    assertEquals(2015, periodEndDate.YearOfDate)
  }

  /**
   * Test get premium policy lines function
   */
  function testGetPremiumPolicyLines() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(1,2015)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    var prmProcessor = new TDIC_EarnedPremiumPolicies(startDate, endDate, new TDIC_EarnedPremiumLines(endDate))
    assertNotNull(prmProcessor.getPremiumLines())
    assertTrue(prmProcessor.getPremiumLines() typeis TDIC_EarnedPremiumLines)
  }
}