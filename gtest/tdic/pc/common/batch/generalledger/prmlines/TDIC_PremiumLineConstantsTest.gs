package tdic.pc.common.batch.generalledger.prmlines

/**
 * US627
 * 12/29/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_PremiumLineConstants.
 */
@gw.testharness.ServerTest
class TDIC_PremiumLineConstantsTest extends gw.testharness.TestBase {

  /**
   * Test contents of State Account Unit Map
   */
  function testStateAcctUnitMap() {
    assertNotNull(TDIC_PremiumLineConstants.stateAcctUnitMap)
    assertEquals("02", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_AK))
    assertEquals("04", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_AZ))
    assertEquals("06", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_CA))
    assertEquals("13", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_GA))
    assertEquals("15", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_HI))
    assertEquals("17", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_IL))
    assertEquals("28", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_MN))
    assertEquals("33", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_NV))
    assertEquals("35", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_NJ))
    assertEquals("36", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_NM))
    assertEquals("39", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_ND))
    assertEquals("45", TDIC_PremiumLineConstants.stateAcctUnitMap.get(typekey.Jurisdiction.TC_PA))
  }

  /**
   * Test contents of Lob Account Unit Map
   */
  function testLobAcctUnitMap() {
    assertNotNull(TDIC_PremiumLineConstants.lobAcctUnitMap)
    assertEquals("40", TDIC_PremiumLineConstants.lobAcctUnitMap.get(typekey.PolicyLine.TC_WC7WORKERSCOMPLINE))
  }

  /**
   * Test contents of Old Account number map
   */
  function testOldAccountNumber() {
    assertNotNull(TDIC_PremiumLineConstants.oldAcctNumber)
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.TERRORISM))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.EXP_CONSTANT))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.PREMIUM))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_STATE_ASSMNTS, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.STATE_ASSMNTS))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_PRM_RCV_ACCOUNT, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.PRM_RCV_ACCOUNT))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_UNEARNED_PREMIUM, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.UNEARNED_PREMIUM))
    assertEquals(TDIC_PremiumLinesTestUtil.ACC_NBR_CHG_UNEARNED_PREMIUM, TDIC_PremiumLineConstants.oldAcctNumber.get(TDIC_PremiumLineTypeEnum.CHG_UNEARNED_PREMIUM))
  }
}