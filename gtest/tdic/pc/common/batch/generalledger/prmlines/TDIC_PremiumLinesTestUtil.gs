package tdic.pc.common.batch.generalledger.prmlines

uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumObject
uses java.util.Map
uses java.text.SimpleDateFormat
uses gw.api.util.DateUtil
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLineTypeEnum

uses java.util.Date
uses java.util.List

/**
 * US627
 * 12/29/2014 Kesava Tavva
 *
 * Util class to build test data for validating different test cases to validate Premium lines classes
 */
class TDIC_PremiumLinesTestUtil {

  protected static final var ACC_NBR_TERRORISM : String = "0040502201"
  protected static final var ACC_NBR_EXP_CONSTANT : String = "0040502202"
  protected static final var ACC_NBR_PREMIUM : String = "0040502203"
  protected static final var ACC_NBR_STATE_ASSMNTS : String = "0026600000"
  protected static final var ACC_NBR_PRM_RCV_ACCOUNT : String = "0012652230"
  protected static final var ACC_NBR_UNEARNED_PREMIUM : String = "0020200000"
  protected static final var ACC_NBR_CHG_UNEARNED_PREMIUM : String = "0041700000"
  protected static final var SUB_LINE1 : String = "0,0,\"\",\"\",\"\""
  protected static final var SUB_LINE2_1 : String = "\"\",\"\",\"\",0,"
  protected static final var SUB_LINE2_2 : String = ",\"\",\"\",\"\",\"\",0"
  protected static final var SUB_LINE3 : String = "\"\",0,0,0,0,0,0"

  /**
   * This method builds premiums test data for generating written premium lines
   */
  static function buildWrittenPremiumTestData() : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    return  {
        typekey.Jurisdiction.TC_CA->
        {
          TDIC_PremiumLineTypeEnum.TERRORISM -> new TDIC_PremiumObject(0,540.00),
          TDIC_PremiumLineTypeEnum.EXP_CONSTANT -> new TDIC_PremiumObject(0,433.00),
          TDIC_PremiumLineTypeEnum.PREMIUM -> new TDIC_PremiumObject(5,14900.00),
          TDIC_PremiumLineTypeEnum.STATE_ASSMNTS -> new TDIC_PremiumObject(0,1490.00)
        }
    }
  }

  /**
   * This method builds premiums test data for generating earned premium lines
   */
  static function buildEarnedPremiumTestData() : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    return  {
        typekey.Jurisdiction.TC_CA->
        {
            TDIC_PremiumLineTypeEnum.UNEARNED_PREMIUM -> new TDIC_PremiumObject(5,3493.63)
        }
    }
  }

  /**
   * This method is to find total number of premium lines.
   */
  static function getPremiumsCount(premiums : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>) : int {
    var count : int = 0
    premiums?.eachKeyAndValue( \ k, val -> {
      count = count + val.Count
    })
    return count
  }

  /**
   * This method is to return current date in MM/dd/yyyy format.
   */
  static function getSystemDate() : String {
    return (new SimpleDateFormat("MM/dd/yyyy")).format(DateUtil.currentDate())
  }

  /**
   * This method is to return formatted date in requested format and date
   */
  static function getFormattedDate(formatter : SimpleDateFormat, reqDate : Date) : String {
    return (formatter.format(reqDate))
  }

  /**
   * This method is to return List of Policy line type codes.
   */
  static function getLOBs(): List<String> {
    return {typekey.PolicyLine.TC_WC7WORKERSCOMPLINE.Code}
  }

  /**
   * This method is to return List of Jurisdiction state codes.
   */
  static function getJurisdictions(): List<String> {
    return {typekey.Jurisdiction.TC_CA.Code}
  }
}