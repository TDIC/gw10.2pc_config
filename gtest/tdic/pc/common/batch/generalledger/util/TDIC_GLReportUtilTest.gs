package tdic.pc.common.batch.generalledger.util

uses gw.date.Month
uses java.util.Calendar

uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportUtil

uses java.text.SimpleDateFormat

/**
 * US627
 * 12/29/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_GLReportUtil.
 */
@gw.testharness.ServerTest
class TDIC_GLReportUtilTest extends gw.testharness.TestBase {

  /**
   * Test batch run period start date for dec 2014
   */
  function testGetBatchRunPeriodStartDate_Dec14(){
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(12, 2014)
    assertEquals(1, startDate.DayOfMonth)
    assertEquals(Month.DECEMBER, startDate.MonthOfYearEnum)
    assertEquals(2014, startDate.YearOfDate)
  }

  /**
   * Test batch run period start date for Jan 2015
   */
  function testGetBatchRunPeriodStartDate_Jan15() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(01, 2015)
    assertEquals(1, startDate.DayOfMonth)
    assertEquals(Month.JANUARY, startDate.MonthOfYearEnum)
    assertEquals(2015, startDate.YearOfDate)
  }

  /**
   * Test batch run period end date for dec 2014
   */
  function testGetBatchRunPeriodEndDate_Dec14() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(12, 2014)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    assertEquals(31, endDate.DayOfMonth)
    assertEquals(Month.DECEMBER, endDate.MonthOfYearEnum)
    assertEquals(2014, endDate.YearOfDate)
  }

  /**
   * Test batch run period end date for Jan 2015
   */
  function testGetBatchRunPeriodEndDate_Jan15() {
    var startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(01, 2015)
    var endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    assertEquals(31, endDate.DayOfMonth)
    assertEquals(Month.JANUARY, endDate.MonthOfYearEnum)
    assertEquals(2015, endDate.YearOfDate)
  }

  /**
   * Test file name suffix format
   */
  function testGetFileNameSuffix() {
    var fileNameSuffix = TDIC_GLReportUtil.getFileNameSuffix()
    var dateTime = Calendar.getInstance().getTime()
    var expected = (new SimpleDateFormat("MM-dd-yyyy-HHmmss")).format(dateTime)
    assertEquals(expected, fileNameSuffix)
  }

  /**
   * Test file path defined in integration database
   */
  function testGetFilePath() {
    var filePath = TDIC_GLReportUtil.getFilePath()
    assertNotNull(filePath)
    assertEquals("c:/tmp/gl/",filePath)
  }
}