package tdic.pc.common.batch.generalledger

uses tdic.pc.common.batch.generalledger.TDIC_GLReportBatchProcess

uses java.lang.Exception

/**
 * US627
 * 12/26/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_GLReportBatchProcess.
 */
@gw.testharness.ServerTest
class TDIC_GLReportBatchProcessTest extends gw.testharness.TestBase {

  var batchProcess : TDIC_GLReportBatchProcess
  var errorMsg : String

  /**
   * This method is used to initialize instances that are required for each test case execution
   */
  override function beforeMethod() {
    batchProcess = new TDIC_GLReportBatchProcess(null)
  }

  /**
   * Test batch process with default arguments
   */
  function testDoWork(){
    try{
      batchProcess.doWork()
      assertTrue(Boolean.TRUE)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test batch process with two valid arguments
   */
  function testDoWorkWithArgs(){
    try{
      batchProcess = new TDIC_GLReportBatchProcess({12, 2014})
      batchProcess.doWork()
      assertTrue(Boolean.TRUE)
    }catch(e : Exception){
      fail()
    }
  }

  /**
   * Test batch process with two invalid arguments
   */
  function testDoWorkWithInvalidArgs(){
    try{
      var args : String[] = {"arg1", "arg2"}
      errorMsg = "Invalid arguments (${args[0]}, ${args[1]}) received for batch run. Expected batch command arguments are <mm>(2 digit month. Valid values 01,02,..12) and <yyyy>(4 digit year)."
      batchProcess = new TDIC_GLReportBatchProcess(args)
      batchProcess.doWork()
    }catch(e : Exception){
      assertEquals(e.Message, errorMsg)
    }
  }

  /**
   * Test batch process with one argument that exception will be thrown
   */
  function testDoWorkWithOneArg(){
    try{
      var args : String[] = {"122014"}
      errorMsg = "Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year)."
      batchProcess = new TDIC_GLReportBatchProcess(args)
      batchProcess.doWork()
    }catch(e : Exception){
      assertEquals(e.Message, errorMsg)
    }
  }

  /**
   * Test batch process with three arguments that exception will be thrown
   */
  function testDoWorkWithThreeArg(){
    try{
      var args : String[] = {"12", "2014", "10"}
      errorMsg = "Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year)."
      batchProcess = new TDIC_GLReportBatchProcess(args)
      batchProcess.doWork()
    }catch(e : Exception){
      assertEquals(e.Message, errorMsg)
    }
  }

  /**
   * Test Email subject for success
   */
  function testGetEmailSubject_Success(){
    var args : String[] = {"12", "2014"}
    batchProcess = new TDIC_GLReportBatchProcess(args)
    var subject = "PC GLInterface Batch Job Completed for the period: ${batchProcess.PeriodStartDate} to ${batchProcess.PeriodEndDate}"
    var actualSubject = batchProcess.getEmailSubject(Boolean.TRUE)
    assertTrue(actualSubject.contains("SUCCESS"))
    assertTrue(actualSubject.contains(subject))
  }

  /**
   * Test Email subject for Failure
   */
  function testGetEmailSubject_Failure(){
    var args : String[] = {"12", "2014"}
    batchProcess = new TDIC_GLReportBatchProcess(args)
    var subject = "PC GLInterface Batch Job Failed for the period: ${batchProcess.PeriodStartDate} to ${batchProcess.PeriodEndDate}"
    var actualSubject = batchProcess.getEmailSubject(Boolean.FALSE)
    assertTrue(actualSubject.contains("FAILURE"))
    assertTrue(actualSubject.contains(subject))
  }
}