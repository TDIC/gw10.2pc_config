package tdic.pc.integ.plugins.hpexstream.util

uses gw.api.util.DateUtil
uses junit.framework.TestCase

uses java.lang.IllegalArgumentException
uses java.lang.Exception

/**
 * US555
 * 11/21/2014 shanem
 *
 * Tests the functionality of <code>TDIC_PCExstreamHelper</code>.
 */
class TDIC_PCExstreamHelperTest extends tdic.TDICTestBase {
  private static var _pcHelper: TDIC_PCExstreamHelper
  override function beforeMethod() {
    _pcHelper = new TDIC_PCExstreamHelper()
  }

  /**
   * TODO: Please enter your comments here!
   */
  function testDocNameCreation() {
    print("Testing testDocNameCreation")
    var aPolicyPeriod = createNewSubmission()
    var documentName = _pcHelper.getDocumentName(aPolicyPeriod.PolicyNumber, "WC000000C")
    assertEquals("WC000000C_${aPolicyPeriod.PolicyNumber}_${DateUtil.currentDate()}", documentName)
  }

  /**
   * TODO: Please enter your comments here!
   */
  function testDocNameCreation_NullPolicyNumber() {
    print("Testing testDocNameCreation_NullPolicyNumber")
    try {
      var aPolicyPeriod = createNewSubmission()
      var documentName = _pcHelper.getDocumentName(null, "WC000000C")
      fail("Expected Exception not thrown")
    } catch (var e: IllegalArgumentException) {
    }
  }

  /**
   * TODO: Please enter your comments here!
   */
  function testDocNameCreation_NullTemplateId() {
    print("Testing testDocNameCreation_NullTemplateId")

    try {
      var aPolicyPeriod = createNewSubmission()
      var documentName = _pcHelper.getDocumentName(aPolicyPeriod.PolicyNumber, null)
      fail("Expected Exception not thrown")
    } catch (var e: IllegalArgumentException) {
    }
  }

  /**
   * Check that the created document is associated with the policy period
   */
  function testCreateDocumentStub_PolicyCheck() {
    print("Testing testCreateDocumentStub_PolicyCheck")
    var aPolicyPeriod = createNewSubmission()
    _pcHelper.createDocumentStub("WC000000C", 1, aPolicyPeriod, "Submission")
    assertEquals(aPolicyPeriod.Documents.Count, 1)
  }

  /**
   * Check that the document created on the policy period has the appropriate TemplateId set
   */
  function testCreateDocumentStub_DocumentCheck() {
    print("Testing testCreateDocumentStub_DocumentCheck")
    var aPolicyPeriod = createNewSubmission()
    //      createDocumentOnPolicyPeriod(aPolicyPeriod)
    _pcHelper.createDocumentStub("WC000000C", 1, aPolicyPeriod, "Submission")
    assertEquals(aPolicyPeriod.Documents.first().TemplateId_TDIC, "WC000000C")
  }

}