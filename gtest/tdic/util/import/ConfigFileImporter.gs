package tdic.util.import

uses com.guidewire.commons.entity.type.ThreadLocalBundleProvider
uses gw.api.webservice.importTools.ImportToolsImpl
uses gw.util.StreamUtil

uses java.io.File
uses java.io.FileInputStream

uses org.apache.commons.lang.StringUtils
uses org.slf4j.Logger

/**
 * 11/07/2014 Rob Kelly
 *
 * An importer for config files.
 */
class ConfigFileImporter {

  private var _logger : Logger

  construct() {

  }

  construct(aLogger : Logger) {

    _logger = aLogger
  }

  /**
   * Imports the specified config file.
   */
  @Param("aConfigFile", "the config file to import")
  function importFile(aConfigFile : File) {

    var inputStream = new FileInputStream(aConfigFile)
    var byteContent = StreamUtil.getContent(inputStream)
    var importer = new ImportToolsImpl()

    var previousProvider = ThreadLocalBundleProvider.get()
    try {

      var importResults = importer.importXmlDataAsByteArray(byteContent)

      if (importResults == null) { // Importer returns null if no import data was found in the file
        throw "No data found in file: " + aConfigFile.Path
      }

      if (importResults.getErrorLog().length > 0) {

        _logger.warn("Import Warnings:\n  " + StringUtils.join(importResults.getErrorLog(), "\n  ") + "\n"
            + "  [Note:  Warnings on CalcRoutineParameter are normal.  They may indicate a bug in the OOTB code.  But they do not seem to be critical.]")
      }
    }
    finally {

      if (ThreadLocalBundleProvider.get() == null) {

        ThreadLocalBundleProvider.set(previousProvider)  // Work-around for problems caused by the OOTB importer
      }
    }
  }
}