package tdic.util.reinsurance.performance

uses gw.api.builder.AddressBuilder
uses gw.api.builder.AccountBuilder
uses gw.transaction.Transaction
uses java.util.ArrayList
uses gw.api.builder.CompanyBuilder
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Bundle
uses gw.sample.heatmap.SampleCatastropheSearch

/**
 * US1378
 * ShaneS 05/05/2015
 */
class TDIC_ReinsuranceSearchPerformanceTestSampleData {

  /**
   * US1378
   * ShaneS 05/05/2015
   *
   * Load reinsurance search sample policies (WC7) into system.
   */
  function create(numberOfSubmissions : int) {

    var addresses = SampleCatastropheSearch.Addresses
    var insureds = SampleCatastropheSearch.Insureds

    var addressIndex = 0
    var insuredIndex = 0
    var loopIndex = 0
    while(loopIndex < numberOfSubmissions){

      if(addressIndex == addresses.Count){
        addressIndex = 0
      }
      if(insuredIndex == insureds.Count){
        insuredIndex = 0
      }

      var address = addresses[addressIndex]
      var insured = insureds[insuredIndex]
      var policyPeriod : PolicyPeriod
      Transaction.runWithNewBundle(\ newBundle -> {
        // New bundle must be passed into create() function of entity builders.
        policyPeriod = createWC7Policy(address, insured, newBundle, loopIndex)
      })

      addressIndex++
      insuredIndex++
      loopIndex++
    }
  }

  /**
   * US1378
   * ShaneS 05/05/2015
   */
  private function createWC7Policy(address : ArrayList<String>, insured : ArrayList<String>, newBundle : Bundle, i : int) : PolicyPeriod {
    return createNewSubmissionWithLocation(address, insured, newBundle, i)
  }

  /**
   * US1378
   * ShaneS 05/05/2015
   */
  private function createNewSubmissionWithLocation(address : ArrayList<String>, insured : ArrayList<String>, newBundle : Bundle, i : int) : PolicyPeriod {
    var periodStart = DateUtil.currentDate().addDays(-50)
    var uwCompany = Query.make(UWCompany).compare(UWCompany#Name, Equals, "The Dentists Insurance Company").select().AtMostOneRow
    var tdicOrg = Query.make(Organization).compare(Organization#Name, Equals, "TDIC Insurance Solutions").select().AtMostOneRow
    var underwritingGroup = Query.make(Group).compare(Group#Name, Equals, "Underwriting").select().AtMostOneRow

    var producerCode = new gw.api.builder.ProducerCodeBuilder ()
        .withDescription("Producer")
        .withOrganization(tdicOrg)
        .withPreferredUnderwriter(underwritingGroup.Users.firstWhere(\ u -> u.User.UserType == typekey.UserType.TC_UNDERWRITER).User)
        .create(newBundle)

    var newSubmission = new gw.api.databuilder.wc7.WC7SubmissionBuilder (true, true, typekey.Jurisdiction.TC_CA)
        .withPeriodStart(periodStart)
        .withAccount(createCompanyAccountWithAddress(address, insured, newBundle, i))
        .withUWCompany(uwCompany)
        .withProducerCodeOfRecord(producerCode)
        .withPaymentPlanID("pymtplan:2")
        .issuePolicy()
        .create(newBundle)

    return newSubmission
  }

  /**
   * US1378
   * ShaneS 05/05/2015
   */
  private function createCompanyAccountWithAddress(address : ArrayList<String>, insured : ArrayList<String>, newBundle : Bundle, i : int) : Account {
    return new AccountBuilder(false)
        .withAccountHolderContact(createCompanyWithAddress(address, insured, newBundle, i))
        .withAccountOrgType(AccountOrgType.TC_SOLEPROPSHIP)
        .withBusinessDescription("Friendly, helpful, professional dental care")
        .create(newBundle)
  }

  /**
   * US1378
   * ShaneS 05/05/2015
   */
  private function createCompanyWithAddress(address : ArrayList<String>,insured : ArrayList<String>, newBundle : Bundle, i : int) : Company {
    return new CompanyBuilder()
        .withCompanyName(insured[0] + insured[1])
        .withPrimaryAddress(createAddress(address, newBundle, i))
        .withEmailAddress1("info@smilesrus.com")
        .withFax("916-442-1700")
        .withWorkPhone("916-442-1701")
        .withTaxID("12-3412670")
        .create(newBundle)
  }

  /**
   * US1378
   * ShaneS 05/05/2015
   */
  private function createAddress(address : ArrayList<String>, newBundle : Bundle, i : int) : Address {
    return new AddressBuilder()
        .withAddressLine1(address[0])
        .withAddressLine2("Floor "+ i)
        .withAddressLine3("")
        .withCity(address[1])
        .withState(typekey.State.get(address[2]))
        .withPostalCode(address[3])
        .create(newBundle)
  }
}