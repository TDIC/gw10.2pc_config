package tdic.util.flatfile.utility.parser

uses tdic.util.flatfile.utility.FlatFileUtility

uses java.io.File
uses java.lang.Exception
uses java.lang.IllegalArgumentException
uses java.io.FileNotFoundException

@gw.testharness.ServerTest
class FlatFileParserTest extends tdic.TDICTestBase {
  static final var VENDOR_DEFINITION = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\VendorDef.xml"
  static final var NONEXIST_VENDOR_DEFINITION = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\NoVendorDef.xml"
  static final var FILE_PATH = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\ParseFile.txt"
  static final var XML_OUTPUT_PATH = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\ParsedFileXMLOutputFile.txt"
  var flatFileUtil: FlatFileUtility
  /**
   * Initialise the flatFileUtility variable for each method call
   */
  override function beforeMethod() {
    flatFileUtil = new FlatFileUtility()
  }

  /**
   * Tests successful file parsing case
   */
  public function testParseSuccessful() {
    try {
      var parsedFile = flatFileUtil.parse(FILE_PATH, VENDOR_DEFINITION)
      assertNotNull("Null value returned from parse()", parsedFile)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown.")
    }
  }

  /**
   * Tests that an Illegal Argument Exception is thrown when null file path provided
   */
  public function testParseWithNullFilePath() {
    try {
      var parsedFile = flatFileUtil.parse(null, VENDOR_DEFINITION)
      fail("Failure - Expected Exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown.")
    }
  }

  /**
   * Tests that an Illegal Argument Exception is thrown when null vendor definition provided
   */
  public function testParseWithNullVendorDefinition() {
    try {
      var parsedFile = flatFileUtil.parse(FILE_PATH, null)
      fail("Failure - Expected Exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that an Illegal Argument Exception is thrown when a vendor definition does not exist at the path location
   */
  public function testParseWithNonExistentVendorDefinition() {
    try {
      var parsedFile = flatFileUtil.parse(FILE_PATH, NONEXIST_VENDOR_DEFINITION)
      fail("Failure - Expected Exception not thrown.")
    } catch (var e: FileNotFoundException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests the correct type of parser is returned
   */
  public function testCorrectParserReturnedTest() {
    try {
      var parsedFile = flatFileUtil.parse(FILE_PATH, VENDOR_DEFINITION)
      assertThat(parsedFile typeis FlatFileParser).isTrue()
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }

  /**
   * Tests that XML is output from the writeToFile method
   */
  public function testWriteToFile() {
    try {
      var file = new File(XML_OUTPUT_PATH)
      if (file.exists()){
        file.delete()
      }
      file.createNewFile()
      var parsedFile = flatFileUtil.parse(FILE_PATH, VENDOR_DEFINITION)
      parsedFile.writeToFile(XML_OUTPUT_PATH)
      var reparsedFile = flatFileUtil.parse(XML_OUTPUT_PATH, VENDOR_DEFINITION)
      assertNotNull("Null value returned from parse()", reparsedFile)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }


}