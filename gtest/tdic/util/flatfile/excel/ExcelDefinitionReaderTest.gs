package tdic.util.flatfile.excel

uses org.apache.poi.xssf.usermodel.XSSFWorkbook
uses tdic.util.flatfile.model.classes.FlatFile

uses java.lang.Exception
uses java.lang.IllegalArgumentException
uses java.io.FileNotFoundException
uses java.util.Properties

@gw.testharness.ServerTest
class ExcelDefinitionReaderTest extends tdic.TDICTestBase {
  static final var VENDOR_DEFINITION_XLSX = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\vendorDef.xlsx"
  static final var VENDOR_DEFINITION_XLSX_NONEXIST = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\vendorDefNonExist.xlsx"
  static final var VENDOR_DEFINITION_XLSX_SKIPPED_ROW = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\vendorDefSkippedRow.xlsx"
  static final var NON_EXCEL_FILE = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\EncodedFile.txt"
  var reader: ExcelDefinitionReader
  /**
   * Initialise the reader variable for each method call
   */
  override function beforeMethod() {
    reader = new ExcelDefinitionReader(".\\modules\\configuration\\gsrc\\tdic\\util\\flatfile\\excelFileHeaders")
  }

  /**
   * Tests that an Illegal argument exception is thrown if no file exists at the path provided
   */
  function testVendorDefNonExist() {
    try {
      var csv = reader.read(VENDOR_DEFINITION_XLSX_NONEXIST)
      fail("Failure - Exception not thrown")
    } catch (var e: FileNotFoundException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown: " + e.Message)
    }
  }

  /**
   * Tests that an Illegal argument exception is thrown if wrong file type provided
   */
  function testVendorDefNonExcel() {
    try {
      var csv = reader.read(NON_EXCEL_FILE)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }
  /**
   * Tests that
   */
  function testVendorDefSkippedRow() {
    try {
      var csv = reader.read(VENDOR_DEFINITION_XLSX_SKIPPED_ROW)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: CsvFormatException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that File Header record type includes 7 fields
   */
  function testGetRecordFieldsSuccessfulCase() {
    try {
      var workBook = new XSSFWorkbook(VENDOR_DEFINITION_XLSX)
      var fields = reader.getRecordFields("File Header", workBook)
      assert(fields.Count == 7)
    } catch (var e: Exception) {
      fail("Failure = Unexpected Exception")
    }
  }

  /**
   * Tests that if a non existant recordType is provided, the method returns null
   */
  function testGetRecordFieldsNonExistRecordType() {
    try {
      var workBook = new XSSFWorkbook(VENDOR_DEFINITION_XLSX)
      var fields = reader.getRecordFields("NonExist", workBook)
      assert(fields == null)
    } catch (var e: Exception) {
      fail("Failure = Unexpected Exception")
    }
  }

  /**
   * Tests that if null recordType is provided, the method returns null
   */
  function testGetRecordFieldsNullRecordType() {
    try {
      var workBook = new XSSFWorkbook(VENDOR_DEFINITION_XLSX)
      var fields = reader.getRecordFields(null, workBook)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that if null workbook is provided, the method returns null
   */
  function testGetRecordFieldsNullWorkBook() {
    try {
      var workBook = new XSSFWorkbook(VENDOR_DEFINITION_XLSX)
      var fields = reader.getRecordFields("File Header", null)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that if null file path is provided, the method throws an IllegalArgumentException
   */
  function testLoadHeadersPropertiesNullPath() {
    try {
      reader.loadHeadersProperties(null)
      fail("Failure - Expected Exception not thrown")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that loadHeadersProperties functions as expected
   */
  function testLoadHeadersProperties() {
    try {
      var properties = reader.loadHeadersProperties(".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\excelfileHeaders")
      assertThat(properties typeis Properties).isTrue()
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * Tests that loadHeadersProperties functions as expected
   */
  function testLoadHeadersPropertiesNonExistentFilePath() {
    try {
      reader.loadHeadersProperties(".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\excelfileHeadersNonExist")
      fail("Failure - Expected Exception not thrown")
    } catch (var e: FileNotFoundException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }
}