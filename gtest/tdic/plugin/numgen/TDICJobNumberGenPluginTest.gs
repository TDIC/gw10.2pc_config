package tdic.plugin.numgen

uses com.guidewire.pl.web.controller.UserDisplayableException
uses gw.plugin.Plugins
uses gw.plugin.jobnumbergen.IJobNumberGenPlugin
uses java.lang.Exception
uses gw.api.locale.DisplayKey
uses tdic.pc.integ.plugins.numgen.TDICJobNumberGenPlugin

/**
 * US679
 * 11/07/2014 Rob Kelly
 *
 * Tests the functionality of <code>TDICJobNumberGenPlugin</code>.
 */
class TDICJobNumberGenPluginTest extends tdic.TDICTestBase {

  private static final var JOB_NUM_SEQUENCE_NAME = "job_num"

  public function testGenNewJobNumberWithNullSequence() {

    try {

      var jobNumPlugin = new TDICJobNumberGenPlugin()
      jobNumPlugin.genNewJobNumber(null)
      fail("failure - exception should be thrown for null sequence")
    }
    catch (ude : UserDisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, DisplayKey.get("TDIC.NumberGenerator.Job.NoSequence"), ude.Message)
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for null sequence: ${e.getMessage()}")
    }
  }

  public function testGenNewJobNumberWithNullJob() {

    try {

      var jobNumPlugin = Plugins.get(IJobNumberGenPlugin)
      jobNumPlugin.genNewJobNumber(null)
      fail("failure - exception should be thrown for null job")
    }
    catch (ude : UserDisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, DisplayKey.get("TDIC.NumberGenerator.Job.NullJob"), ude.Message)
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for null job: ${e.getMessage()}")
    }
  }

  public function testGenNewJobNumberWithSubmission() {

    var expectedJobNumber = "SUB000006"
    setSequenceNumber(JOB_NUM_SEQUENCE_NAME, 5)
    var jobNum = createNewSubmission().Job.JobNumber
    assertEquals("failure - Job number incorrect", expectedJobNumber, jobNum)
  }

  public function testGenNewJobNumberWithPolicyChange() {

    var expectedJobNumber = "CHG000102"
    setSequenceNumber(JOB_NUM_SEQUENCE_NAME, 100)
    var jobNum = createPolicyChange("111").Job.JobNumber
    assertEquals("failure - Job number incorrect", expectedJobNumber, jobNum)
  }

  public function testGenNewJobNumberWithRenewal() {

    var expectedJobNumber = "REN000052"
    setSequenceNumber(JOB_NUM_SEQUENCE_NAME, 50)
    var jobNum = createPolicyRenewal("111").Job.JobNumber
    assertEquals("failure - Job number incorrect", expectedJobNumber, jobNum)
  }

  public function testGenNewJobNumberWithCancellation() {

    var expectedJobNumber = "CAN001002"
    setSequenceNumber(JOB_NUM_SEQUENCE_NAME, 1000)
    var jobNum = createCancellation("111").Job.JobNumber
    assertEquals("failure - Job number incorrect", expectedJobNumber, jobNum)
  }
}