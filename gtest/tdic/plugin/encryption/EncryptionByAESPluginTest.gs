package tdic.plugin.encryption

/**
 * GUnit test for EncryptionByAESPlugin.
 * This test has no dependencies, and does not require the server to be at any run level.
 */
class EncryptionByAESPluginTest extends gw.testharness.TestBase{

  private static var _encryptionByAESPlugin : EncryptionByAESPlugin

  override function beforeMethod() {
    _encryptionByAESPlugin = new EncryptionByAESPlugin()
  }

  /**
   * Test if EncryptionByAESPlugin encrypt and decrypt a short string.
   */
  function testEncryptShortString() {
    print("Testing testEncryptShortString")

    var shortString = "hello"

    var shortStringEncrypted = _encryptionByAESPlugin.encrypt(shortString)
    var shortStringDecrypted =  _encryptionByAESPlugin.decrypt(shortStringEncrypted)

    assertEquals(shortString , shortStringDecrypted)

  }

  /**
   * Test if EncryptionByAESPlugin encrypt and decrypt a long string.
   */
  function testEncryptLongString() {
    print("Testing testEncryptLongString")

    var shortString = "The California Dental Association (CDA) is the non-profit[1] organization representing organized dentistry in California. Founded in 1870 as the California State Dental Association,[2] CDA supports its members in service to their patients and the public. CDA also contributes to the oral health of Californians through various programs and advocacy. CDA's membership consists of nearly 25,000 dentists in 32 local dental societies throughout the state, making it the largest constituent of the American Dental Association."

    var shortStringEncrypted = _encryptionByAESPlugin.encrypt(shortString)
    var shortStringDecrypted =  _encryptionByAESPlugin.decrypt(shortStringEncrypted)

    assertEquals(shortString , shortStringDecrypted)

  }

  /**
   * Test if getEncryptedLength returns the correct output size if receives a 7 length size string.
   */
  function testGetEncryptedLengthWith7() {
    print("Testing testGetEncryptedLengthWith7")

    var string = "1234567"

    var shortStringEncrypted = _encryptionByAESPlugin.encrypt(string)

    assertEquals(_encryptionByAESPlugin.getEncryptedLength(string.length) , shortStringEncrypted.length)

  }

  /**
   * Test if getEncryptedLength returns the correct output size if receives a 7 length size string.
   */
  function testGetEncryptedLengthWith72() {
    print("Testing testGetEncryptedLengthWith7")

    var string = "123456789012345678901234567890123456789012345678901234567890123456789012"

    var shortStringEncrypted = _encryptionByAESPlugin.encrypt(string)

    assertEquals(_encryptionByAESPlugin.getEncryptedLength(string.length) , shortStringEncrypted.length)

  }

  /**
   * Test if getEncryptedLength returns the correct output size if receives a 152 length size string.
   */
  function testGetEncryptedLengthWith152() {
    print("Testing testGetEncryptedLengthWith7")

    var string = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891234567890123"

    var shortStringEncrypted = _encryptionByAESPlugin.encrypt(string)

    assertEquals(_encryptionByAESPlugin.getEncryptedLength(string.length) , shortStringEncrypted.length)

  }

  /**
   * Test if EncryptionIdentifier returns "gw.testplugin.aes"
   */
  function testEncryptionIdentifier() {
    print("Testing testEncryptionIdentifier")

    assertEquals(_encryptionByAESPlugin.EncryptionIdentifier, _encryptionByAESPlugin.IntrinsicType.Name)

  }

}
