package tdic

uses gw.api.system.PLLoggerCategory
uses gw.api.databuilder.wc7.WC7SubmissionBuilder
uses gw.api.util.DateUtil
uses gw.api.builder.AccountBuilder
uses gw.api.builder.CompanyBuilder
uses gw.api.builder.AddressBuilder
uses gw.api.database.Query
uses gw.api.builder.ProducerCodeBuilder
uses gw.api.builder.PolicyChangeBuilder
uses gw.api.builder.RenewalBuilder
uses gw.api.builder.RewriteBuilder
uses gw.api.builder.CancellationBuilder
uses gw.api.system.database.SequenceUtil
uses org.slf4j.Logger
uses java.util.Date
uses org.slf4j.LoggerFactory

/**
 * TestBase class that every TDIC GUnit test should extend. Initializes the environment and sets up the _logger
 */
@gw.testharness.ServerTest
abstract class TDICTestBase extends gw.testharness.TestBase {

  protected static var _logger: Logger

  protected static var _uwCompany : UWCompany as TheDentists

  protected static var _tdicOrg : Organization as TDICOrg

  protected static var _underwritingGroup : Group as Underwriting

  construct() {
    // nothing to do
  }

  override function beforeClass() {

    _logger = LoggerFactory.getLogger("Test")

    _uwCompany = Query.make(UWCompany).compare(UWCompany#Name, Equals, "The Dentists Insurance Company").select().AtMostOneRow

    var importer = new tdic.util.import.ConfigFileImporter(_logger)

    var rateBookFile1 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2011-1-v1.xml")
    var rateBookFile2 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2011-2-v1.xml")
    var rateBookFile3 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2012-1-v1.xml")
    var rateBookFile4 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2012-2-v1.xml")
    var rateBookFile5 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2013-1-v1.xml")
    var rateBookFile6 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2014-1-v1.xml")
    var rateBookFile7 = gw.api.util.ConfigAccess.getConfigFile("config/content/ratebooks/TDICWC7-2015-1-v1.xml")
    importer.importFile(rateBookFile1)
    importer.importFile(rateBookFile2)
    importer.importFile(rateBookFile3)
    importer.importFile(rateBookFile4)
    importer.importFile(rateBookFile5)
    importer.importFile(rateBookFile6)
    importer.importFile(rateBookFile7)

    var adminFile = gw.api.util.ConfigAccess.getConfigFile("config/datafiles/AdminData.xml")
    importer.importFile(adminFile)

    _tdicOrg = Query.make(Organization).compare(Organization#Name, Equals, "TDIC Insurance Solutions").select().AtMostOneRow
    _underwritingGroup = Query.make(Group).compare(Group#Name, Equals, "Underwriting").select().AtMostOneRow
  }

  protected function createAddress() : Address {

    return new AddressBuilder()
               .withAddressLine1("1816 L St")
               .withAddressLine2("Floor 3")
               .withAddressLine3("Apt. 4")
               .withCity("Sacramento")
               .withPostalCode("95811")
               .withState(typekey.State.TC_CA)
               .create()
  }

  protected function createCompany() : Company {

    return new CompanyBuilder()

               .withCompanyName("Smiles R US")
               .withPrimaryAddress(createAddress())
               .withEmailAddress1("info@smilesrus.com")
               .withFax("916-442-1700")
               .withWorkPhone("916-442-1701")
               .withTaxID("12-3412670")
               .create()
  }

  protected function createCompanyAccount() : Account {

    return createCompanyAccount(AccountOrgType.TC_SOLEPROPSHIP)
  }

  protected function createCompanyAccount(accountOrgType : AccountOrgType) : Account {

    return new AccountBuilder(false)
               .withAccountHolderContact(createCompany())
               .withAccountOrgType(accountOrgType)
               .withBusinessDescription("Friendly, helpful, professional dental care")
               .create()
  }

  // robk, 7th Nov: This method requires that the BillingCenter instance for the h2mem env is up and has the admin and plan data loaded.
  protected function createNewSubmission(periodStart : Date) : PolicyPeriod {

    var producerCode = new ProducerCodeBuilder()
                       .withDescription("Producer")
                       .withOrganization(TDICOrg)
                       .withPreferredUnderwriter(Underwriting.Users.firstWhere(\ u -> u.User.UserType == typekey.UserType.TC_UNDERWRITER).User)
                       .create()

    var newSubmission = new WC7SubmissionBuilder(true, true, typekey.Jurisdiction.TC_CA)
                        .withPeriodStart(periodStart)
                        .withAccount(createCompanyAccount())
                        .withUWCompany(TheDentists)
                        .withProducerCodeOfRecord(producerCode)
                        .withPaymentPlanID("pymtplan:2")
                        .issuePolicy()
                        .create()

    // robk, 12/16/2014: we need to explicitly finish issuance since the issuance workflow doesn't complete before we need the new submission
    // 20161129 TJT GW-2583 removing this function
    //   newSubmission.Job.finishIssuance()
    return newSubmission
  }

  protected function createNewSubmission() : PolicyPeriod {

    return createNewSubmission(DateUtil.currentDate().addDays(1))
  }

  protected function createPolicyChange(policyNumber : String) : PolicyPeriod {

    return new PolicyChangeBuilder()
           .withBasedOnPeriod(createNewSubmission())
           .withPolicyNumber(policyNumber)
           .isBound()
           .create()
  }

  protected function createPolicyChange(aPolicyPeriod : PolicyPeriod) : PolicyPeriod {

    var policyChange = new PolicyChangeBuilder()
                      .withBasedOnPeriod(aPolicyPeriod)
                      .withPolicyNumber(aPolicyPeriod.PolicyNumber)
                      .isBound()
                      .create()

    // robk, 12/16/2014: we need to explicitly finish issuance since the issuance workflow doesn't complete before we need the new submission
    policyChange.Job.finishIssuance()
    return policyChange
  }

  protected function createPolicyRenewal(policyNumber : String) : PolicyPeriod {

    return new RenewalBuilder()
           .withBasedOnPeriod(createNewSubmission())
           .withPolicyNumber(policyNumber)
           .isBound()
           .create()
  }

  protected function createPolicyRenewal(aPolicyPeriod : PolicyPeriod) : PolicyPeriod {

    return new RenewalBuilder()
        .withBasedOnPeriod(aPolicyPeriod)
        .withPolicyNumber(aPolicyPeriod.PolicyNumber)
        .isBound()
        .create()
  }

  protected function createCancellation(policyNumber : String) : PolicyPeriod {

    return  new CancellationBuilder()
            .withBasedOnPeriod(createNewSubmission())
            .withPolicyNumber(policyNumber)
            .canceledForRewrite()
            .isBound()
            .create()
  }

  protected function createCancellation(aPolicyPeriod : PolicyPeriod) : PolicyPeriod {

    return  new CancellationBuilder()
        .withBasedOnPeriod(aPolicyPeriod)
        .withPolicyNumber(aPolicyPeriod.PolicyNumber)
        .canceledForRewrite()
        .isBound()
        .create()
  }

  protected function createPolicyRewrite(policyNumber : String) : PolicyPeriod {

    var cancellation = createCancellation(policyNumber)

    return new RewriteBuilder()
               .withBasedOnPeriod(cancellation)
               .withPolicyNumber(policyNumber)
               .withPeriodStart(cancellation.PeriodStart.addWeeks(-1))
               .withPeriodEnd(cancellation.PeriodStart.addWeeks(-1).addYears(1))
               .isQuoted()
               .create()
  }

  protected function createRewriteNewAccount() : PolicyPeriod {

    return new gw.api.builder.RewriteNewAccountBuilder().create()
  }

  protected function setSequenceNumber(sequenceName : String, startNumber : long) {

    var sequence = Query.make(Sequence).compare("SequenceKey", Equals, sequenceName).select().AtMostOneRow
    if (sequence != null) {

      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {

        sequence = bundle.add(sequence)
        sequence.SequenceNumber = startNumber
      })
    }
    else {

      SequenceUtil.next(startNumber, sequenceName)
    }
  }

  function createDocumentOnPolicyPeriod(aPolicyPeriod: PolicyPeriod) {
    var doc = new Document()
    doc.Name = "Test_Doc"
    doc.Status = DocumentStatusType.TC_APPROVED
    doc.Policy = aPolicyPeriod.Policy
    doc.PolicyPeriod = aPolicyPeriod
    doc.Account = aPolicyPeriod.Policy.Account
    doc.Job = aPolicyPeriod.Job
    doc.Type = DocumentType.TC_LETTER_LIVE
    doc.Author = "System Generated"
    doc.Description = "Desc"
    doc.MimeType = "application/pdf"
    doc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING
    doc.TemplateId_TDIC = "WC040304"
    doc.DateCreated = new Date()
    doc.DateModified = new Date()
  }
}