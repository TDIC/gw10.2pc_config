package acc.onbase.util

uses acc.onbase.api.security.*

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */

/*
 * OnBaseDemoUnityUtils - Code for OnBaseDemoPop.pcf
 *
 */
class OnBaseDemoUnityUtils {

  public static function constructCustomQueryURL(policyPeriod: PolicyPeriod): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=PolicyCenter&Action=Custom+Query&Policy+Number=" + UrlEncode(policyPeriod.PolicyNumber)
  }

  public static function constructFolderURL(policyPeriod: PolicyPeriod): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=PolicyCenter&Action=Policy+Folders&Policy+Number=" + UrlEncode(policyPeriod.PolicyNumber)
  }

  public static function uploadDocURL(policyPeriod: PolicyPeriod): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=PolicyCenter&Action=Upload+Doc&Policy+Number=" + UrlEncode(policyPeriod.PolicyNumber)
  }

  public static function unityFormURL(policyPeriod: PolicyPeriod): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=PolicyCenter&Action=Unity+Form&Policy+Number=" + UrlEncode(policyPeriod.PolicyNumber)
  }

  public static function generateDocPacketURL(templateName: String, policyPeriod: PolicyPeriod): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=PolicyCenter&Action=Document+Packet+Generation&TemplateName=" + UrlEncode(templateName) + "&Account+Number=" + UrlEncode(policyPeriod.Policy.Account.AccountNumber)
  }

  /**
   * Url Encoder
   */
  private static function UrlEncode(urlParam: String): String {
    return java.net.URLEncoder.encode(urlParam, java.nio.charset.StandardCharsets.UTF_8.toString())
  }

}