package acc.onbase.configuration

uses java.io.File

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 *
 * Interface for OnBase configuration settings.
 */
interface IOnBaseConfiguration {

  /**
   * Gets the base URL for OnBase Pop Web Server
   * @return string URL of OnBase Pop Web Server
   */
  public property get PopURL(): String

  /**
   * Gets the key for generating checksums for the Get Doc service.
   * @return string key for checksum generation
   */
  public property get GetDocChecksumKey(): String

  /**
   * Gets the key for generating checksums for web client Pop.
   * @return string key for checksum generation
   */
  public property get PopChecksumKey(): String

  /**
   * Gets the OnBase client type for viewing documents
   * @return enum value of OnBase Client type
   */
  public property get ClientType(): OnBaseClientType

  /**
   * Gets the web client type (HTML or ActiveX) for the OnBase web client.
   * @return enum value of wb client type
   */
  public property get WebClientType(): OnBaseWebClientType
  /**
   * Switch to enable or disable checksum for DocPop URL
   * @return true for enabled; false for disabled
   */
  public property get EnableDocPopURLCheckSum(): Boolean

  /**
   * Switch to enable count of linked documents
   * @return true for enabled; false for disabled
   */
  public property get EnableLinkedDocumentCount(): Boolean

  /**
   * Gets reference to folder for asynchronous upload to OnBase
   * @return java.io.File file path to OnBase async upload folder
   */
  public property get AsyncDocumentFolder(): File

  /**
   * Gets the threshold above which document uploads are performed asynchronously
   * @return size in bytes of async threshold
   */
  public property get AsyncDocumentSize(): Long

  /**
   * Gets the interval between retries for the OnBaseMessageTransport plugin
   * @return retry interval in seconds
   */
  public property get RetryIntervalSeconds(): Integer

  /**
   * Gets the maximum number of retries for the OnBaseMessageTransport plugin
   * @return
   */
  public property get MaxRetries(): Integer

  /**
   * Gets the name of the current Guidewire center (e.g. "ClaimCenter")
   * @return string name of current center
   */
  public property get SourceCenterName(): String

  /**
   * Gets the namespace for OnBase typecode translation
   * @return string of OnBase typecode namespace
   */
  public property get TypecodeNamespace(): String
}

