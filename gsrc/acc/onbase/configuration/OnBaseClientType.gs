package acc.onbase.configuration

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 *
 * Enum to select the global OnBase client
 */
enum OnBaseClientType {
  Unity, Web
}