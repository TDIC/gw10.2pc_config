package acc.onbase.enhancement

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */
enhancement OnBasePolicyEnhancement : Policy {

  property get MostRecentPolicyNumber() : String {
    return this.MostRecentBoundPeriodOnMostRecentTerm.PolicyNumber
  }

}
