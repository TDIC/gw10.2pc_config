package acc.onbase.api.application

uses acc.onbase.api.security.SecurityManager
uses acc.onbase.api.service.ServicesManager
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.configuration.OnBaseWebClientType
uses acc.onbase.util.AePopUtils
uses acc.onbase.util.LoggerFactory

uses java.io.InputStream

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * 01/19/2015 - Daniel Q. Yu
 * * Initial implementation, refactored from OnBaseDocumentContentSource.gs
 * <p>
 * 4/1/2015 - mfowler
 * * added AEPOPutil refactoring
 * <p>
 * 02/17/2017 - Daniel Q. Yu
 * * Added optional check sum to docpop URL.
 */

/**
 * Document retrieval application.
 */
class DocumentRetrieval {
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)
  /**
   * Services Manager
   */
  private var _servicesManager = new ServicesManager()

  /**
   * Get document content from OnBase.
   *
   * @param document The OnBase document.
   */
  public function getDocumentContent(document : Document) : InputStream {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentRetrieval.getDocumentContent(" + document.DocUID + ")")
    }
    var service = ServicesManager.getDocumentContent()
    return service.getDocumentContent(document)
  }

  /**
   * Get document retrieval Unity URL.
   *
   * @param docId The document id.
   * @return The encoded Unity URL for this document.
   */
  public function getDocumentUnityURL(docId : String) : String {
    return AePopUtils.generateRetrievalUrl(docId)
  }

  /**
   * Get document retrieval web URL.
   *
   * @param docId         The document id.
   * @param webClientType The web client type.
   * @return The encoded web URL for this document.
   */
  public function getDocumentWebURL(docId : String, webClientType : OnBaseWebClientType) : String {
    var docpopURL = OnBaseConfigurationFactory.Instance.PopURL + "/docpop/docpop.aspx?clientType=" + webClientType + "&docid=" + docId
    if (OnBaseConfigurationFactory.Instance.EnableDocPopURLCheckSum) {
      var hash = SecurityManager.computeCheckSum(docId)
      docpopURL = docpopURL + "&chksum=" + hash
    }
    return docpopURL

  }
}
