package acc.onbase.api.security

uses org.apache.commons.codec.binary.Base64

uses javax.crypto.Cipher
uses javax.crypto.spec.SecretKeySpec
uses java.io.FileInputStream
uses java.io.FileNotFoundException

uses gw.api.util.ConfigAccess
uses acc.onbase.util.LoggerFactory

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * 08/09/2016 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Loading secure properties from a file.
 */
class SecurePropertiesManager {
  public static final var ENCRYPTION_KEY: String = "encryption_key"
  public static final var WSP_USERNAME: String = "wsp_username"
  public static final var WSP_PASSWORD: String = "wsp_password"
  public static final var DECRYPTED_WSP_PASSWORD: String = "decrypted_wsp_password"
  public static final var SECURE_PROPERTIES_FILE: String = "gsrc/acc/onbase/api/security/secure.properties"

  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * The secure properties
   */
  private static var _secureProperties = new Properties()

  private static function loadSecureProperties() {
    // Load secure properties from file.
    var fileInput: FileInputStream
    try {
      var securePropertiesFile = ConfigAccess.getConfigFile(SECURE_PROPERTIES_FILE)
      fileInput = new FileInputStream(securePropertiesFile)
      _secureProperties.load(fileInput)
    } catch (ex: Exception) {
    } finally {
      if (fileInput != null) {
        try {
          fileInput.close()
        } catch (ex2: Exception) {
          _logger.error("No Configuration file found at " + SECURE_PROPERTIES_FILE)
          throw new FileNotFoundException("No Configuration file found!")
        }
      }
    }

    // decrypt wsp password.
    var decryptedPassword = decrypt(_secureProperties.get(WSP_PASSWORD).toString(), _secureProperties.get(ENCRYPTION_KEY).toString().getBytes("UTF-8"))
    _secureProperties.setProperty(DECRYPTED_WSP_PASSWORD, decryptedPassword)
  }

  /**
   * Get the password for WSP requests.
   *
   * @return The decrypted password.
   */
  public static function getUsernameForWSP(): String {
    if (_secureProperties.size() == 0) {
      loadSecureProperties()
    }
    return _secureProperties.getProperty(WSP_USERNAME)
  }

  /**
   * Get the password for WSP requests.
   *
   * @return The decrypted password.
   */
  public static function getPasswordForWSP(): String {
    if (_secureProperties.size() == 0) {
      loadSecureProperties()
    }
    return _secureProperties.getProperty(DECRYPTED_WSP_PASSWORD)
  }

  /**
   * Encrypt a string using AES.
   *
   * @return The encrypted string.
   */
  public static function encrypt(str: String, key: byte[]): String {
    var cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
    var secretKey = new SecretKeySpec(key, "AES")
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    return new String(Base64.encodeBase64(cipher.doFinal(str.getBytes("UTF-8"))), "UTF-8")
  }

  /**
   * Decrypt an encrypted string using AES.
   *
   * @return The decrypted string.
   */
  private static function decrypt(str: String, key: byte[]): String {
    var cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
    var secretKey = new SecretKeySpec(key, "AES")
    cipher.init(Cipher.DECRYPT_MODE, secretKey);
    return new String(cipher.doFinal(Base64.decodeBase64(str.getBytes("UTF-8"))), "UTF-8")
  }
}