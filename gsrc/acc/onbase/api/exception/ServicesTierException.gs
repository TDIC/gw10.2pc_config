package acc.onbase.api.exception

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * 01/28/2015 - Clayton Sandham
 * * Initial implementation.
 */

/**
 * General-purpose service-tier exception.
 */
class ServicesTierException extends OnBaseApiException {
  /**
   * Constructor.
   *
   * @param msg The exception message.
   */
  construct(msg: String) {
    super(msg);
  }

  /**
   * Constructor.
   *
   * @param msg The exception message.
   * @param ex  The underlying cause of this exception.
   */
  construct(msg: String, ex: Throwable) {
    super(msg, ex);
  }
}
