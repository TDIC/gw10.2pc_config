package gw.api.system.logging

uses gw.api.system.PCLoggerCategory
uses org.slf4j.Logger

class LOBLoggerCategory extends PCLoggerCategory {
  static final var _transformation : Logger as TRANSFORMATION = createLogger("Transformation")
}