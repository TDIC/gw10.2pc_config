package gw.api.validation

uses java.util.Map
uses gw.api.locale.DisplayKey

class TaxIDValidator_TDIC extends FieldValidatorBase {
  construct() {
  }

  private static property get isNonMigrationMode() : boolean {
    return ScriptParameters.TDIC_DataSource == "policycenter";
  }

  override function validate(taxID : String, p1 : Object, parameters : Map<Object, Object>) : IFieldValidationResult {
    var result = new FieldValidationResult();
    if (isNonMigrationMode and taxID.matches("[0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]{2}-[0-9]{7}") == false) {
      result.addError(DisplayKey.get("Validator.TaxID", ""));
    }
    return result;
  }


}