package gw.api.productmodel

uses gw.lang.reflect.IType

class ScheduleStringPropertyInfoWithDefaultValue extends ScheduleStringPropertyInfo {
  var _defaultValue: String as DefaultValue

  construct(columnName: String, colLabel: String, isRequired: boolean, isIdentityColumn : boolean, priority : int, defaultValue: String) {
    super(columnName, colLabel, isRequired, isIdentityColumn, priority)
    _defaultValue = defaultValue
  }

  construct(scheduledItemType: IType, columnName: String, colLabel: String, isRequired: boolean, isIdentityColumn : boolean, priority : int, defaultValue: String) {
    super(scheduledItemType, columnName, colLabel, isRequired, isIdentityColumn, priority)
    _defaultValue = defaultValue
  }
}