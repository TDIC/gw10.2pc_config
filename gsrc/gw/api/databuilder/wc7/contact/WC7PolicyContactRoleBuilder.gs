package gw.api.databuilder.wc7.contact

uses gw.api.builder.PolicyContactRoleBuilder
uses gw.entity.IEntityType

@Export
class WC7PolicyContactRoleBuilder<T extends WC7PolicyContactRole, B extends WC7PolicyContactRoleBuilder<T,B>> extends PolicyContactRoleBuilder<T, B> {

  construct(type : IEntityType) {
    super(type)
  }
  
  function withDetail(detail : WC7ContactDetailBuilder) : B  {
    addArrayElement(WC7PolicyContactRole#WC7Details, detail)
    return this as B
  }

}
