package gw.api.databuilder.wc7.contact

uses gw.api.databuilder.DataBuilder
uses gw.api.databuilder.BuilderContext
uses gw.api.productmodel.ConditionPattern
uses gw.api.productmodel.ExclusionPattern
uses java.lang.IllegalStateException
uses java.lang.IllegalArgumentException
uses gw.api.productmodel.ClausePattern

@Export
abstract class WC7ContactDetailBuilder<T extends WC7ContactDetail, B extends WC7ContactDetailBuilder<T, B>> extends DataBuilder<T, B> {
  protected var _isIncluded : boolean = true
  protected var _parentCondition : ConditionPattern
  protected var _parentExclusion : ExclusionPattern

  construct() {
    super(T)
  }

  protected property get ClausePattern() : ClausePattern {
    return  _parentCondition ?: _parentExclusion
  }

  function withInclusion(inclusionVal : Inclusion) : B {
    _isIncluded = inclusionVal == TC_INCL
    return this as B
  }
  
  function asIncluded() : B {
    _isIncluded = true
    return this as B
  }

  function asExcluded() : B {
    _isIncluded = false
    return this as B
  }
  function forCondition(condition : ConditionPattern) : B {
    _parentCondition = condition
    return asIncluded()
  }

  function forExclusion(exclusion : ExclusionPattern) : B {
    _parentExclusion = exclusion
    return asExcluded()
  }

  protected override function createBean(context : BuilderContext) : T {
    var parentContact = context.ParentBean as WC7PolicyContactRole
    if (parentContact == null)
      throw new IllegalArgumentException("Expected a parent contact")
    var line = parentContact.Branch.WC7Line
    if (line == null)
      throw new IllegalArgumentException("Expected a policy with a WC7Line")
    if (ClausePattern != null)
      line.setCoverageConditionOrExclusionExists(ClausePattern, true)
    return newContactDetailFor(line, parentContact)
  }

  abstract protected function newContactDetailFor(line : WC7Line, contactRole : WC7PolicyContactRole) : T

  protected function invalidContactRoleType(contactRole : WC7PolicyContactRole) : IllegalStateException {
    return new IllegalStateException("Invalid contact role type: " + (typeof contactRole))
  }
}
