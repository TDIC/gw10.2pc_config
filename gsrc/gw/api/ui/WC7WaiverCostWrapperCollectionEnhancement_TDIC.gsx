package gw.api.ui

uses gw.api.locale.DisplayKey

enhancement WC7WaiverCostWrapperCollectionEnhancement_TDIC : Collection<WC7CostWrapper> {
  function addWC7SpecificWaiverCosts(costs : Collection<WC7Cost>) {
    if(costs != null and costs.Count > 0) {
      var waivers : Map<String, Set<WC7WaiverOfSubroCost>> = costs.toSet().cast(WC7WaiverOfSubroCost).partition(\c -> c.WC7WaiverOfSubro.JobID)
      var costWrappers : List<WC7CostWrapper> = {}
      for(jobID in waivers.Keys) {
        var locCosts : Collection<WC7WaiverOfSubroCost> = waivers.get(jobID)
        var firstCost = locCosts.first()
        var total = locCosts*.ActualAmount.sum()
        var costWrapper = new WC7CostWrapper(firstCost.CalcOrder, firstCost.ClassCode, DisplayKey.get("TDIC.Web.SubmissionWizard.Quote.WC7.SpecificWaiver", jobID), total, false)
        costWrappers.add(costWrapper)
      }
      this.addAll(costWrappers.toCollection() )
    }
  }
}
