package gw.api.phone.upgrade

uses gw.api.phone.PhoneFields
uses gw.api.phone.PhoneOwnerFieldId
uses gw.api.phone.StandardPhoneOwner

/**
 * File created by Guidewire Upgrade Tools.
 */
class UniversalPhoneOwner extends StandardPhoneOwner {
  var _editable: boolean
  var _available: boolean

  construct(fields: PhoneFields, label: String, required: boolean) {
    super(fields, label, required)
  }

  construct(fields: PhoneFields, label: String, required: boolean, editable: boolean, available: boolean) {
    this(fields, label, required)
    _editable = editable
    _available = available
  }

  override function isFieldVisibleEditMode(fieldId: PhoneOwnerFieldId): boolean {
    if (_editable) {
      return super.isFieldVisibleEditMode(fieldId)
    } else {
      return fieldId == PhoneOwnerFieldId.PHONE_DISPLAY
    }
  }

  override function isAvailable(fieldId: PhoneOwnerFieldId): boolean {
    return _available;
  }
}