package gw.webservice.pc.pc1000.ccintegration.lob

uses gw.webservice.pc.pc1000.ccintegration.CCBasePolicyLineMapper
uses gw.webservice.pc.pc1000.ccintegration.CCPolicyGenerator
uses gw.webservice.pc.pc1000.ccintegration.entities.types.complex.CCWC7CovEmpRU
uses gw.webservice.pc.pc1000.ccintegration.entities.types.complex.CCClassCode
uses gw.webservice.pc.pc1000.ccintegration.entities.anonymous.elements.CCPolicy_RiskUnits
uses gw.xml.XmlElement
uses java.lang.Integer
uses java.util.List

class CCWC7PolicyLineMapper_TDIC extends CCBasePolicyLineMapper {

  var _wcLine : WC7WorkersCompLine
  var _RUCount : Integer

  construct(line : PolicyLine, policyGen : CCPolicyGenerator) {
    super(line, policyGen);
    _wcLine = line as WC7WorkersCompLine;
  }

  override function getLineCoverages() : List<entity.Coverage> {

    for( cov in _wcLine.WC7LineCoverages) {
    }

    return _wcLine.WC7LineCoverages?.toList() as List<entity.Coverage>
  }


  override function setLineSpecificFields() {
    _ccPolicy.WCStates = _wcLine.WC7Jurisdictions*.Jurisdiction.sort().join(", ")
  }


  override function createRiskUnits() {

    _RUCount = _ccPolicy.RiskUnits.Count

    // Create all of the class codes
    var coveredEmployees = _wcLine.WC7CoveredEmployeeBases
    _ccPolicy.TotalProperties += coveredEmployees.Count
    for( e in coveredEmployees.sort(\ exp1, exp2 -> WC7ExpOrdering(exp1, exp2)) )
    {
      if (meetsLocationFilteringCriteria(e.Location)) {
        getOrCreateWCCovEmpRU( e )
      }
    }

  }

  // US531, robk: Uncommented this code to populate WC7 Risk Units in CC
  protected function getOrCreateWCCovEmpRU( coveredEmployee : WC7CoveredEmployeeBase ) : CCWC7CovEmpRU {
    
    if( coveredEmployee == null ) {
      return null
    }

    var wcCovEmpRU = _mappedObjects.get( coveredEmployee.ID ) as CCWC7CovEmpRU
    if( wcCovEmpRU != null ) {
      return wcCovEmpRU
    }
    
    wcCovEmpRU = new CCWC7CovEmpRU()
    _mappedObjects.put( coveredEmployee.ID, wcCovEmpRU )
    
    wcCovEmpRU.PolicyLocation = _policyGen.getOrCreateCCLocation( coveredEmployee.Location )
    _RUCount = _RUCount + 1
    
    wcCovEmpRU.RUNumber = _RUCount
    if (coveredEmployee.ClassCode != null) {
      wcCovEmpRU.ClassCode = getOrCreateCCClassCode( coveredEmployee.ClassCode )
    }
    wcCovEmpRU.PolicySystemID = coveredEmployee.TypeIDString
    wcCovEmpRU.WC7GoverningLaw = (coveredEmployee.GoverningLaw) as String
    _ccPolicy.RiskUnits.add(new CCPolicy_RiskUnits(wcCovEmpRU) )
    return wcCovEmpRU
  }

  private function WC7ExpOrdering(exp1 : WC7CoveredEmployeeBase, exp2 : WC7CoveredEmployeeBase) : boolean {

    if (exp1.Location.LocationNum == exp2.Location.LocationNum) {
      return (exp1.ClassCode.Code <= exp2.ClassCode.Code)
    } else {
      return (exp1.Location.LocationNum < exp2.Location.LocationNum)
    }

  }

  protected function getOrCreateCCClassCode( pcClassCode : WC7ClassCode ) : XmlElement {
    var el = _policyGen.getMappedPCObjects().get( pcClassCode.ID )
    if( el != null ) {
      return el
    }
    var ccClassCode = new CCClassCode()
    ccClassCode.Code = pcClassCode.Code
    ccClassCode.Description = pcClassCode.ShortDesc
    el = _policyGen.addClassCode(pcClassCode.ID, ccClassCode)
    _ccPolicy.ClassCodes.add(el)
    return el
  }
}
