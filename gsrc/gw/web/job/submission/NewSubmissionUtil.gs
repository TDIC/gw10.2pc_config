package gw.web.job.submission

uses gw.api.util.JurisdictionMappingUtil
uses gw.api.web.job.submission.SubmissionUtil
uses gw.api.locale.DisplayKey
uses pcf.JobForward

@Export
class NewSubmissionUtil {

  /**
   * Returns the a valid {@link ProducerSelection}, either from the session or from the account.
   * @return a {@link ProducerSelection} in the web session
   */
  static function getOrCreateProducerSelection (account : Account) : ProducerSelection {
    return SubmissionUtil.getOrCreateProducerSelection(account, \ a -> JurisdictionMappingUtil.getJurisdiction(a))
  }

  static function createOneSubmission(offer : ProductSelection, producerSelection : ProducerSelection, account : Account,
      quoteType : QuoteType) {
    if( producerSelection.DefaultPPEffDate == null ) {
      throw new gw.api.util.DisplayableException(DisplayKey.get('Web.SubmissionManagerLV.DefaultPPEffDateRequired'))
    }
    var availOffer = account.getAvailableProduct( producerSelection.SubmissionPolicyProductRoot, offer.Product )
    if( availOffer == null )
    {
      throw new gw.api.util.DisplayableException( DisplayKey.get('Web.SubmissionManagerLV.UnavailableProduct',  offer.Product ) )
    }
    if( producerSelection.ProducerCode == null )
    {
      throw new gw.api.util.DisplayableException( DisplayKey.get('Web.SubmissionManagerLV.ProducerCodeRequired') )
    }
    gw.api.web.job.submission.SubmissionUtil.setLastProducerSelection( producerSelection )
    offer.NumToCreate = 1
    var submission = gw.api.web.job.submission.SubmissionUtil.createSubmission( offer, account, producerSelection, quoteType )
    // For one new submission - go straight to Submission view
    var policyPeriod = submission.LatestPeriod
    gw.transaction.Transaction.runWithNewBundle( \ bun -> {
      policyPeriod = bun.add( policyPeriod )
      policyPeriod.SubmissionProcess.beginEditing()
    } )
    JobForward.go(submission, policyPeriod)
  }

  static function makeNumberRange( max : int ) : java.util.List<java.lang.Integer> {
    var result = new java.util.ArrayList<java.lang.Integer>()
    var count = 0
    while( count <= max ) {
      result.add( count )
      count = count + 1
    }
    return result
  }
  /*
   * create by: SureshB
   * @description: method to default the base state based on the Organization.
   * @create time: 7:01 PM 11/21/2019
    * @param entity.ProducerSelection
   * @return: typekey.Jurisdiction[]
   */

  static function setBaseStateAndSelection_TDIC(selectionOfProducer : entity.ProducerSelection, organizationChanged : boolean) : typekey.Jurisdiction[] {
    if (selectionOfProducer.Producer.DisplayName == "TDIC Insurance Solutions") {
      if(organizationChanged){
        selectionOfProducer.State = typekey.Jurisdiction.TC_CA
      }
      return Jurisdiction.TF_TDIC_IS_POLICY_STATES.TypeKeys.toTypedArray()
    }
    if (selectionOfProducer.Producer.DisplayName == "Conrad Houston Insurance") {
      if(organizationChanged){
        selectionOfProducer.State = typekey.Jurisdiction.TC_AK
      }
      return {typekey.Jurisdiction.TC_AK}
    }
    if (selectionOfProducer.Producer.DisplayName == "Jerry Hay, Inc.") {
      if(organizationChanged){
        selectionOfProducer.State = typekey.Jurisdiction.TC_HI
      }
      return {typekey.Jurisdiction.TC_HI}
    }
    if (selectionOfProducer.Producer.DisplayName == "Mid-Atlantic Insurance Resources") {
      if(organizationChanged){
        selectionOfProducer.State = typekey.Jurisdiction.TC_NJ
      }
      return {typekey.Jurisdiction.TC_NJ}
    }
    if (selectionOfProducer.Producer.DisplayName == "PDAIS - Pennsylvania Dental Association Insurance Services") {
      selectionOfProducer.State = typekey.Jurisdiction.TC_PA
      return {typekey.Jurisdiction.TC_PA}
    }
    if (selectionOfProducer.Producer.DisplayName == "IDIA - Idaho Dentists Insurance Agency") {
      selectionOfProducer.State = typekey.Jurisdiction.TC_ID
      return {Jurisdiction.TC_ID}
    }
    if (selectionOfProducer.Producer.DisplayName == "WDIA - Washington Dentists Insurance Agency") {
      selectionOfProducer.State = Jurisdiction.TC_WA
      return {Jurisdiction.TC_WA}
    }
    return Jurisdiction.AllTypeKeys.toTypedArray()
  }
  /*
   * create by: SureshB
   * @description: method to get the BaseSate Dropdown values
   * @create time: 7:44 PM 11/21/2019
    * @param PolicyPeriod
   * @return: typekey.Jurisdiction[]
   */

  static function setBaseStateAndSelection_TDIC(policyPeriod : PolicyPeriod, organizationChanged : boolean) : typekey.Jurisdiction[] {
    var organizationName = policyPeriod.ProducerOfRecord?.DisplayName
    if (organizationName == "TDIC Insurance Solutions") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_CA
      }
      return Jurisdiction.TF_TDIC_IS_POLICY_STATES.TypeKeys.toTypedArray()
    }
    if (organizationName == "Conrad Houston Insurance") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_AK
      }
      return {typekey.Jurisdiction.TC_AK}
    }
    if (organizationName == "Jerry Hay, Inc.") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_HI
      }
      return {typekey.Jurisdiction.TC_HI}
    }
    if (organizationName == "Mid-Atlantic Insurance Resources") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_NJ
      }
      return {typekey.Jurisdiction.TC_NJ}
    }
    if (organizationName == "PDAIS - Pennsylvania Dental Association Insurance Services") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_PA
      }
      return {typekey.Jurisdiction.TC_PA}
    }
    if (organizationName == "IDIA - Idaho Dentists Insurance Agency") {
      if(organizationChanged){
        policyPeriod.BaseState = typekey.Jurisdiction.TC_ID
      }
      return {typekey.Jurisdiction.TC_ID}
    }
    if (organizationName == "WDIA - Washington Dentists Insurance Agency") {
      if(organizationChanged){
        policyPeriod.BaseState = Jurisdiction.TC_WA
      }
      return {typekey.Jurisdiction.TC_WA}
    }
    return Jurisdiction.AllTypeKeys.toTypedArray()
  }


}

