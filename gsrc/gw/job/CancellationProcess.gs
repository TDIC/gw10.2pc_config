package gw.job

uses gw.api.diff.DiffItem
uses gw.api.job.EffectiveDateCalculator
uses gw.api.job.JobProcessLogger
uses gw.api.job.JobStateException
uses gw.api.locale.DisplayKey
uses gw.api.system.PCLoggerCategory
uses gw.api.util.DisplayableException
uses gw.api.util.MonetaryAmounts
uses gw.api.web.util.TransactionUtil
uses gw.forms.FormInferenceEngine
uses gw.job.cancellation.CancellationProcessValidator
uses gw.job.permissions.CancellationPermissions
uses gw.plugin.Plugins
uses gw.plugin.messaging.BillingMessageTransport
uses gw.plugin.reinsurance.IReinsurancePlugin
uses java.util.Date
uses gw.api.util.DateUtil
uses tdic.pc.integ.services.billing.BillingSystem_TDIC
uses wsi.remote.gw.webservice.cc.cc1000.claimapi.ClaimAPI
uses org.apache.commons.lang3.exception.ExceptionUtils

/**
 * Encapsulates the actions taken within a Cancellation job.
 *
 * @see JobProcess for general information and job process logic.
 * @see gw.plugin.policyperiod.impl.JobProcessCreationPlugin
 */
@Export
class CancellationProcess extends JobProcess implements ICancellationProcess {

  /* Activity Pattern codes used in this class */
  final var _DISCOUNT_POLICY_CANCELLED_PATTERN_CODE = "multiline_discount_policy_cancelled"

  construct(period : PolicyPeriod) {
    super(period, new CancellationPermissions(period.Job))
    JobProcessEvaluator = JobProcessUWIssueEvaluator.forCancellation()
    JobProcessValidator = new CancellationProcessValidator()
    AsyncJobProcessValidator = new CancellationProcessValidator()
  }

  override property get Job() : Cancellation {
    return super.Job as Cancellation
  }

  property get ActiveCancellationWorkflow() : CompleteCancellationWF {
    var workflow = _branch.ActiveWorkflow
    return (workflow == null or not(workflow typeis CompleteCancellationWF) or workflow.State == TC_COMPLETED)
        ? null
        : (workflow as CompleteCancellationWF)
  }

  override property get BillingSubjectToFinalAudit() : boolean {
    return _branch.hasScheduledFinalAudit() or _branch.hasOpenFinalAudit()
  }

  private property get InitialNotificationsHaveBeenSent() : boolean {
    return Job.InitialNotificationDate != null
  }

  property get CurrentNotificationsSent() : boolean {
    return (Job.CancelProcessDate != null and Job.LastNotifiedCancellationDate != null and Job.LastNotifiedCancellationDate == _branch.CancellationDate)
  }

  // ===== LIFECYCLE FUNCTIONS =====

  /**
   * Checks the conditions for which the policy period can be canceled.
   */
  function canStart() : JobConditions {
    return canStartJob(DisplayKey.get("Job.Process.Cancellation.StartCancellation"))
  }

  /**
   * Initiates the cancellation.
   */
  override function start() {
    canStart().assertOkay()
    JobProcessLogger.logDebug("Starting cancellation on branch: ${_branch}")
    startJobAsDraft()
    Job.assignRolesFromPolicy()
    var renewal = _branch.Policy.OpenRenewalJob
    if (renewal != null) {
      renewal.createRoleActivity(TC_UNDERWRITER,
          ActivityPattern.finder.getActivityPatternByCode("notification"),
          DisplayKey.get("Job.Cancellation.CancellationForRenewal"),
          DisplayKey.get("Job.Cancellation.OpenRenewal", _branch.PolicyNumber))
    }

    if (Job.QuoteOnStart) {
      //GW-2434
      Job.createCustomHistoryEvent(CustomHistoryType.TC_CAN_CREATED_TDIC, \-> DisplayKey.get("Cancellation.History.JobCreated"))
      requestQuote()
    }
  }

  /**
   * Adds Canceling and Rescinding to the list of statuses from which edit
   * mode can be entered.
   */
  override protected property get AllowedEditStatus() : PolicyPeriodStatus[] {
    return {TC_QUOTED, TC_RATED, TC_CANCELING, TC_RESCINDING}
  }

  /**
   * Checks the conditions for which the policy period can be switched
   * to edit mode ("Draft" status).
   */
  override function canEdit() : JobConditions {
    var cancellationCondition = super.canEdit()
    var canSendNotifications = not CurrentNotificationsSent or Permissions.RescheduleCancellation
    return cancellationCondition
        .checkCondition(canSendNotifications, DisplayKey.get("Web.Cancellation.Error.NotificationsSent"))
  }

  /**
   * Switches the policy period to edit mode.
   * Note that some of this code is duplicated in processSpecificPreemptionHandling below.
   */
  override function edit() {
    Job.CancelProcessDate = null
    super.edit()
    removeCancellationWorkflow()
  }

  private function removeCancellationWorkflow() {
    if (ActiveCancellationWorkflow != null) {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        bundle.add(ActiveCancellationWorkflow)
        ActiveCancellationWorkflow.remove()
      })
      _branch.ActiveWorkflow = null
    }
  }

  override protected function addJobSpecificStartQuoteProcessChecks(jobConditions : JobConditions) : JobConditions {
    return jobConditions.checkNotNull(Job.PolicyPeriod.RefundCalcMethod, "RefundCalcMethod is null")
        .checkFlatCancelOnPeriodStart()
        .checkCanStartQuoteProcessStatus()
        .checkNoUnhandledPreemptions()
  }

  /**
   * Two step quoting feature disabled by default for cancellation process
   */
  override property get TwoStepQuotingAvailable() : boolean {
    return false
  }

  /**
   * Checks the conditions for which the cancellation can be scheduled.
   */
  function canScheduleCancellation() : JobConditions {
    var jobConditions = canIssue(DisplayKey.get("Job.Process.Cancellation.ScheduleCancellation"))

    addScheduleCancellationChecks(jobConditions)
    jobConditions
        .checkCondition(!InitialNotificationsHaveBeenSent, DisplayKey.get("Web.Cancellation.Error.NotificationsSent"))

    return jobConditions
  }

  /**
   * checks the conditions for which the presumptive date of the cancellation
   * can be changed. Slightly different from the conditions under which the process
   * of actually scheduling the Cancellation can be started.
   */
  function canEditCancellationDate() : JobConditions {
    var jobConditions = startChecksFor(DisplayKey.get("Job.Process.Cancellation.EditCancellationDate"))

    addSetCancellationDateChecks(jobConditions)
    addNotificationChecks(jobConditions)

    jobConditions
        .checkPermission(perm.System.cancelovereffdate)
        .checkCondition(not CurrentNotificationsSent, DisplayKey.get("Web.Cancellation.Error.NotificationsResent"))
        .checkCondition(_branch.RefundCalcMethod != TC_FLAT, "Cannot edit cancellation date for a flat cancellation")

    return jobConditions
  }

  /**
   * Checks the conditions for which the cancellation can be escheduled.
   */
  function canRescheduleCancellation() : JobConditions {
    var jobConditions = startChecksFor(DisplayKey.get("Job.Process.Cancellation.RescheduleCancellation"))

    addScheduleCancellationChecks(jobConditions)

    jobConditions
        // comment out the following line to allow rescheduleCancellation to also do initial scheduling
        .checkCondition(InitialNotificationsHaveBeenSent, DisplayKey.get("Web.Cancellation.Error.NotScheduled"))
        .checkCondition(not CurrentNotificationsSent, DisplayKey.get("Web.Cancellation.Error.NotificationsResent"))
        .checkStatus(TC_QUOTED)

    return jobConditions
  }

  private function addSetCancellationDateChecks(jobConditions : JobConditions) : JobConditions {
    return jobConditions
        .checkAdvancePermission()
        .checkBranchNotLocked()
        .checkNoUnhandledPreemptions()
  }

  private function addScheduleCancellationChecks(jobConditions : JobConditions) : JobConditions {
    addSetCancellationDateChecks(jobConditions)

    return jobConditions
        .checkQuoteIsValid()
    // Uncomment this line to disable the ability to schedule more than one Cancellation at a time. Note that only
    // one Cancellation will be allowed to bind, the other will automatically be withdrawn at that time.
    // .checkCondition(!_branch.HasScheduledCancellation, "Another Cancellation is already scheduled for this policy")
  }

  private function addNotificationChecks(jobConditions : JobConditions) : JobConditions {
    // Need special permission to reschedule a cancellation after notices have been sent
    if (InitialNotificationsHaveBeenSent and !Permissions.RescheduleCancellation) {
      // Report both problems together
      jobConditions
          .checkCondition(!InitialNotificationsHaveBeenSent, DisplayKey.get("Web.Cancellation.Error.NotificationsSent"))
          .checkPermission(Permissions.RescheduleCancellation)
    }
    return jobConditions
  }

  /**
   * Adjusts the cancellationDate according to the EffectiveTimePlugin and makes this cancellation effective
   * as of the resulting date.
   * Will fail if the date lies outside of the current slice.
   */
  function setCancellationDate(cancellationDate : Date) {
    canEditCancellationDate().assertOkay()
    var effDateTime = EffectiveDateCalculator.instance().getCancellationEffectiveDate(cancellationDate, _branch, _branch.Cancellation, _branch.RefundCalcMethod)

    var errorMessage = Job.validateEffectiveDate(_branch, effDateTime, _branch.RefundCalcMethod)
    if (errorMessage != null) {
      throw new IllegalArgumentException(errorMessage)
    }
    JobProcessLogger.logInfo("Changing cancellation date to ${effDateTime} for branch \"${_branch}\"")
    var withinPeriod = startChecksFor(DisplayKey.get("Job.Process.Cancellation.CrossSliceBoundaries"))
        .checkDateWithinPeriodOrTerm(effDateTime).Okay // this check need to be done before setting EED below
    _branch.EditEffectiveDate = effDateTime
    _branch.cancel(effDateTime)
    if (withinPeriod) {
      _branch.updateEditEffectiveDateForReinsurance(_branch)
    } else {
      JobProcessLogger.logInfo("Could not apply reinsurance change to new effective date:${effDateTime} for branch \"${_branch}\"")
      var plugin = Plugins.get(IReinsurancePlugin)
      plugin.withdrawBranch(_branch)
    }
  }
  //BrittoS 03/09/2020 Cancellation eff. Date is Paid thru Date received from BC OR Cancellation request from BC + leadTime(days) (whichever is later)
  protected function calculateEffectiveDate_TDIC(processDate : Date) : Date {
    var cancellation = this.Job
    if (cancellation.CancelReasonCode == ReasonCode.TC_NONPAYMENT){
      //GWPS-2650 Include paid through date in cancellation effective date calculation
      //If payment related reason “AND” delinquency is on current term; then "YES" use paid through date.
      if(cancellation.PaidThroughDate_TDIC != null and
          _branch.TermNumber == _branch.Policy.finder.findPolicyPeriodByPolicyNumberAndAsOfDate(_branch.LatestPeriod.PolicyNumber, DateUtil.currentDate()).TermNumber) {
        return cancellation.PaidThroughDate_TDIC.after(processDate) ? cancellation.PaidThroughDate_TDIC : processDate
      }
    }
    return _branch.EditEffectiveDate
  }

  /**
   * Adjust cancellation process date for delinquency cancellations.
   */
  protected function calculateProcessDate(processDate : Date) : Date {
    var cancellation = this.Job

    JobProcessLogger.logInfo("ProcessDate before adjustments: " + processDate.formatDateTime(SHORT, SHORT))

    if (cancellation.IsDelinquencyCancellation_TDIC) {
      // 20150501 TJ Talluto - US149 US155 override the process date (ootb passes in an effective date)
      if (cancellation.CancelReasonCode == ReasonCode.TC_NOTTAKEN or cancellation.CancelReasonCode == ReasonCode.TC_NONPAYMENT) {
        var initialProcessingDate = cancellation.InitialNotificationDate != null ? cancellation.InitialNotificationDate : Date.CurrentDate
        //var initialProcessingDate = Date.CurrentDate
        var leadTimeCalculator = new CancellationLeadTimeCalculator(ReasonCode.TC_NONPAYMENT,
            _branch.AllPolicyLinePatternsAndJurisdictions,
            initialProcessingDate,
            initialProcessingDate <= cancellation.findUWPeriodEnd(_branch.BasedOn))
        var leadTime = leadTimeCalculator.calculateMaximumLeadTime()
        processDate = initialProcessingDate.trimToMidnight().addDays(leadTime)
      }
      JobProcessLogger.logInfo("ProcessDate after lead time adjustment: " + processDate.formatDateTime(SHORT, SHORT))
      // BrianS - Adjust process date to run after BillingCenter payment processing (10:45pm)
      processDate = processDate.addHours(23)
    }
    JobProcessLogger.logInfo("ProcessDate after BillingCenter payment processing adjustments: " + processDate.formatDateTime(SHORT, SHORT))

    return processDate
  }

  /**
   * Schedules the cancellation to take place on the given processDate.  This will run through UW approval,
   * and if approved it will send notices and then start the cancellation workflow.
   *
   * @param processDate the date on which cancellation takes place
   */
  function scheduleCancellation(processDate : Date) {
    canScheduleCancellation().assertOkay()
    processDate = calculateProcessDate(processDate)
    internalScheduleCancellation(processDate)
  }

  function scheduleCancellationForRescind_TDIC() {
    if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_MANUALMIGRATION.Code) {
      canScheduleCancellation().assertOkay()
      // BrianS - Push process date to 3 months from today, to give user time to rescind.
      internalScheduleCancellation(Date.Today.addMonths(3))
    }
  }

  /**
   * Reschedules the cancellation to take place on the given processDate.  This will run through UW approval,
   * and if approved it will send notices and then start the cancellation workflow.
   * <p>
   * Just like scheduleCancellation except with slightly different checks
   *
   * @param processDate the date on which cancellation takes place
   */
  function rescheduleCancellation(processDate : Date) {
    canRescheduleCancellation().assertOkay()
    processDate = calculateProcessDate(processDate)
    internalScheduleCancellation(processDate)
  }

  private function internalScheduleCancellation(processDate : Date) {
    startScheduledCancellation(processDate)
    sendNotices()
  }

  /**
   * Checks the conditions for which the cancellation can be scheduled and immediately bound.
   */
  function canImmediatelyScheduleCancellation() : JobConditions {
    var jobConditions = canIssue(DisplayKey.get("Job.Process.Cancellation.ScheduleImmediateCancellation"))

    addScheduleCancellationChecks(jobConditions)
    // If the cancellation is already scheduled we assume the notifications have already have been sent
    if (_branch.Status != TC_CANCELING) {
      addNotificationChecks(jobConditions)
    }

    return jobConditions
  }

  /**
   * Like scheduleCancellation, except that the cancellation will be completed
   * synchronously rather than using a workflow.
   */
  function cancelImmediately() {
    canImmediatelyScheduleCancellation().assertOkay()

    startScheduledCancellation(Date.CurrentDate)
    if (CurrentNotificationsSent) {
      removeCancellationWorkflow()
      issueCancellation()
    } else {
      sendNotices()
    }
  }

  private function startScheduledCancellation(processDate : Date) {
    //20160503 TJT: GW-1692 - the black box doesn't allow ShortRate to be backdated when Manually Migrating.  It doesn't allow the
    // user to cancel a policy that is already expired/cancelled.  I'm circumventing the earliest start date calc, so that the manual
    // migration user can backdate a ShortRate cancellation to any historical term.  Fix is in two places: here and CancellationEnhancement.gsx
    if (ScriptParameters.TDIC_DataSource != typekey.DataSource_TDIC.TC_POLICYCENTER.Code) {
      if (processDate < _branch.Policy.findEarliestPeriodStart()) {
        // if invalid, just throw the ootb error
        throw new JobStateException(DisplayKey.get("Web.Job.Warning.CancellationTooSoon", _branch.EditEffectiveDate.ShortFormat, _branch.Policy.findEarliestPeriodStart().ShortFormat))
      }
    } else {
      // ootb below
      // We want to throw here instead of disabling the buttons so that the end user gets visible feedback
      JobProcessValidator.validateCancellationDateNotTooSoon(_branch, Job)
    }
    if (InitialNotificationsHaveBeenSent) {
      Job.createCustomHistoryEvent(CustomHistoryType.TC_CANCEL_RESCHEDULE, \-> DisplayKey.get("Job.Cancellation.History.Reschedule", processDate))
    }
    //BrittoS 03/09/2020 - update Cancellation Effective date
    _branch.EditEffectiveDate = calculateEffectiveDate_TDIC(processDate)
    JobProcessLogger.logInfo("Scheduling cancellation on ${_branch.EditEffectiveDate} for branch \"${_branch}\"")
    _branch.cancel(_branch.EditEffectiveDate)

    _branch.Status = TC_CANCELING
    Job.CancelProcessDate = processDate
  }

  /**
   * Puts the policy period into "Canceling" status, sets the CancelProcessDate of the job
   * and sends cancellation notices.
   */
  private function sendNotices() {
    if (CurrentNotificationsSent) {
      JobProcessLogger.logInfo("No new notices needed for cancellation on ${Job.NotificationDate} already sent for branch \"${_branch}\"")
    } else if (InitialNotificationsHaveBeenSent) {
      sendReplacementNotices()
    } else {
      sendInitialNotices()
    }
    // For asynchronous integration, do not call finishSendNotices().  It will be called from a different path
    // when the external system finishes.  Also see sendIntiialNotices() and sendReplacementNotices().
    finishSendNotices()
  }

  /**
   * Notify an external system to send the initial cancellation notices.
   */
  protected function sendInitialNotices() {
    Job.LastNotifiedCancellationDate = _branch.CancellationDate
    var currentDate = Date.CurrentDate
    Job.InitialNotificationDate = currentDate
    Job.NotificationDate = currentDate
    JobProcessLogger.logInfo("Sending notice of cancellation on ${Job.NotificationDate} for branch \"${_branch}\"")
    if (this.IsCancellationDocumentProduced) {
      setDelinquencyInfo_TDIC()
      // GW-2987 - Produce document HERE
      //01/02/2020 Per GINTEG-1238 commenting out
      Job.createDocuments(TDIC_DocCreationEventType.TC_NOTICEOFCANCELLATION.Code, _branch)
      _branch.addEvent(TDIC_DocCreationEventType.TC_NOTICEOFCANCELLATION.Code)

      if((_branch.GLLineExists && _branch.GLLine.AdditionalInsureds.HasElements) ||
          (_branch.BOPLineExists && _branch.BOPLine.AdditionalInsureds.HasElements)){
        Job.createDocuments(TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERNOC.Code, _branch)
        _branch.addEvent(TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERNOC.Code)
      }
    }
    // For asynchronous integration, add the "SendCancellationNotices" event.  The event rules should create
    // a message for a MessageTransport to send to an external system.  When the external  system finishes
    // sending the notices, it should advance the CancellationProcess by calling the finishSendNotices() method.
    _branch.addEvent("SendCancellationNotices")
  }

  /**
   * Notify an external system to send replacement cancellation notices.  This should only be called if
   * cancellation notices were already sent, and the cancellation has been rescheduled to a different date.
   */
  protected function sendReplacementNotices() {
    Job.LastNotifiedCancellationDate = _branch.CancellationDate
    Job.NotificationDate = Date.CurrentDate
    JobProcessLogger.logInfo("Sending replacement notice of cancellation on ${Job.NotificationDate} for branch \"${_branch}\"")
    if (this.IsCancellationDocumentProduced) {
      setDelinquencyInfo_TDIC()
      // GW-2987 - Produce document HERE
      //01/02/2020 Per GINTEG-1238 commenting out
      /*Job.createDocuments(TDIC_DocCreationEventType.TC_NOTICEOFCANCELLATION.Code, _branch)
      _branch.addEvent(TDIC_DocCreationEventType.TC_NOTICEOFCANCELLATION.Code)*/
    }
    // For asynchronous integration, add the "SendReplacementCancellationNotices" event.  The event rules should create
    // a message for a MessageTransport to send to an external system.  When the external  system finishes
    // sending the notices, it should advance the CancellationProcess by calling the finishSendNotices() method.
    //_branch.addEvent("SendReplacementCancellationNotices")
  }

  /**
   * Is a Cancellation Document Produced?
   */
  protected property get IsCancellationDocumentProduced() : boolean {
    var retval = (Job.IsDelinquencyCancellation_TDIC and Job.IsRollForwardCancellation_TDIC == false)

    if (retval == false) {
      JobProcessLogger.logWarning("Cancellation documents will not be produced for Cancellation " + Job.JobNumber
          + ".  ReasonCode = " + Job.CancelReasonCode + ", RollForward = "
          + (Job.IsRollForwardCancellation_TDIC ? "true" : "false"))
    }

    return retval
  }

  /**
   * Set delinquency info.
   */
  protected function setDelinquencyInfo_TDIC() : void {
    var delinquencyInfo : DelinquencyInfo_TDIC

    // Clear out old data (if any).
    for (delinquency in Job.DelinquencyInfos_TDIC) {
      Job.removeFromDelinquencyInfos_TDIC(delinquency)
    }

    if (Job.IsDelinquencyCancellation_TDIC and Job.IsRollForwardCancellation_TDIC == false) {
      if (Job.DlnqProcessPublicID_TDIC != null) {
        var billingAPI = new BillingSystem_TDIC()
        for (delinquency in billingAPI.getDelinquencyInfo(Job.DlnqProcessPublicID_TDIC)) {
          delinquencyInfo = new DelinquencyInfo_TDIC()
          delinquencyInfo.PolicyTerm = PolicyPeriod.finder.findByPolicyNumberAndTerm(delinquency.PolicyNumber,
              delinquency.TermNumber)
              .FirstResult.PolicyTerm
          delinquencyInfo.InvoiceNumber = delinquency.InvoiceNumber
          delinquencyInfo.CurrentBalance = delinquency.CurrentBalance
          delinquencyInfo.MinimumPaymentDue = delinquency.MinimumPaymentDue
          delinquencyInfo.BarCode = delinquency.BarCode
          Job.addToDelinquencyInfos_TDIC(delinquencyInfo)
        }
      } else {
        JobProcessLogger.logError("DlnqProcessPublicID_TDIC was not set for cancellation " + Job.JobNumber
            + ".  Delinquency information will not print on the notice of cancelllation.")
      }
    }
  }

  /**
   * Checks the conditions to finish sending cancellation notices.
   */
  function canFinishSendNotices() : JobConditions {
    return startChecksFor(DisplayKey.get("Job.Process.Cancellation.FinishSendingNotices"))
        .checkBranchNotLocked()
        .checkQuoteIsValid()
        .checkCurrentNotificationsSent()
        .checkNotNull(Job.CancelProcessDate, "Cancel process date is not set")
  }

  /**
   * Finish sending cancellation notices and start the CompleteCancellation workflow if
   * the CancelProcessDate is before or equal to today.
   */
  function finishSendNotices() {
    canFinishSendNotices().assertOkay()

    JobProcessLogger.logInfo("Finished sending notices for cancellation for branch \"${_branch}\"")
    Job.NotificationAckDate = Date.CurrentDate

    if (Job.CancelProcessDate <= Date.CurrentDate) {
      issueCancellation()
    } else {
      _branch.startWorkflow(TC_COMPLETECANCELLATIONWF)
    }
  }

  function canRescind() : JobConditions {
    return this.canRescind(false)
  }


  /**
   * Checks the conditions for which the cancellation can be rescinded.
   */
  function canRescind(ignoreCancelProcessDate : boolean) : JobConditions {

    return startChecksFor(DisplayKey.get("Job.Process.Cancellation.Rescind"))
        .checkPermission(Permissions.RescindCancellation)
        .checkBranchNotLocked()
        .checkQuoteIsValid()
            //.checkCurrentNotificationsSent()
        .check(Job.LastNotifiedCancellationDate != null and Job.LastNotifiedCancellationDate == _branch.CancellationDate,
            DisplayKey.get("Web.Cancellation.Warning.CancellationNotificationDate"))
        .check(Job.RescindNotificationDate == null, "Rescind notification date is already set")
        .check(ignoreCancelProcessDate or (Job.CancelProcessDate > Date.CurrentDate), "Only available while pending")

  }


  /**
   * Rescinds the cancellation.  Invoke {@link #finishRescind} to finalize.
   */
  function rescind() {
    //canRescind(false).assertOkay()
    canRescind(false).assertOkay()
    /**
     * US555: Document Production
     * 05/18/2015 Shane Murphy
     *
     * Creates documents for Rescission and raises event.
     */

    /* SUPPRESS FOR COMPLETED MIGRATION TRANSACTION (AUTOMATED OR MANUAL) */
    if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_POLICYCENTER.Code and this.Job.PolicyPeriod.WC7LineExists) {
      if (gw.transaction.Transaction.Current == null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          _branch.Job.createSingleDocumentsForEvent(TDIC_DocCreationEventType.TC_RESCISSION.Code)
        })
      } else
        _branch.Job.createSingleDocumentsForEvent(TDIC_DocCreationEventType.TC_RESCISSION.Code)
    }
    JobProcessLogger.logInfo("Rescinding cancellation for branch \"${_branch}\"")
    Job.RescindNotificationDate = Date.CurrentDate
    _branch.Status = TC_RESCINDING
    finishRescind()
    //GW-2434
    Job.createCustomHistoryEvent(CustomHistoryType.TC_RESCIND_TDIC, \-> DisplayKey.get("Rescind.History.JobIssued"))
  }

  /**
   * Checks the conditions for which the cancellation rescind can be finished.
   */
  function canFinishRescind() : JobConditions {
    return startChecksFor(DisplayKey.get("Job.Process.Cancellation.FinishRescind"))
        .checkPermission(Permissions.RescindCancellation)
        .checkBranchNotLocked()
        .checkQuoteIsValid()
            //.checkCurrentNotificationsSent()
        .check(Job.LastNotifiedCancellationDate != null and Job.LastNotifiedCancellationDate == _branch.CancellationDate,
            DisplayKey.get("Web.Cancellation.Warning.CancellationNotificationDate"))
        .checkStatus(TC_RESCINDING)
  }

  /**
   * Finishes rescinding the cancellation.
   */
  function finishRescind() {
    canFinishRescind().assertOkay()
    JobProcessLogger.logInfo("Finished rescinding cancellation for branch \"${_branch}\"")
    _branch.Status = TC_RESCINDED
    _branch.lockBranch()
  }

  /**
   * Checks the conditions for which the cancellation can be issued.
   */
  function canIssueCancellation() : JobConditions {
    return startChecksFor(DisplayKey.get("Job.Process.Cancellation.IssueCancellation"))
        .checkBranchNotLocked()
        .checkQuoteIsValid()
        .checkCurrentNotificationsSent()
        .checkNoUnhandledPreemptions()
        .check(Job.CancelProcessDate <= Date.CurrentDate, "Only available after cancel process date")
        .checkNoFutureTermsArchived()
  }

  /**
   * Issues the cancellation.  Don't do AccountSyncable validation.
   */
  function issueCancellation() {
    canIssueCancellation().assertOkay()
    checkThatBasedOnPeriodNotArchivedInBillingSystem()

    JobProcessLogger.logInfo("Binding cancellation for branch \"${_branch}\"")
    try {
      TransactionUtil.runAtomically(\bundle -> {
        _branch.ensureProducerOfService()
        FormInferenceEngine.Instance.inferPreBindForms(_branch)
        finishCancellation()
        //GINTEG688 : Marking this policy Period to be considered for Pivotal Feed
        _branch.considerForPivotalInd_TDIC = true
        //GW-2434
        Job.createCustomHistoryEvent(CustomHistoryType.TC_CAN_ISSUED_TDIC, \-> DisplayKey.get("Cancellation.History.JobIssued"))
      }, _branch)
    /*
      US555: Document Production
      04/09/2015 Shane Murphy

      Creates document stubs and raises an event for the messaging queue

      If the Cancellation is a scheduled cancellation called from the
      workflow batch, the bundle will be null, so we need to create one
    */
      if (gw.transaction.Transaction.getCurrent() == null) {
        gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
          createDocumentsForCurrentJob()
        })
      } else {
        createDocumentsForCurrentJob()
      }
    } catch (e : Exception) {
      PCLoggerCategory.JOB_PROCESS.error("Unable to issueCancellation", e)
      throw e
    }

    var isPolicyMultiLineDiscount = _branch.MultilineDiscount
    if(isPolicyMultiLineDiscount and _branch.WC7LineExists){
      createPolicyWithDiscountCancelledActivity()
    }
    // push to future depends on commit of branch promotion...
    withdrawOrCancelRenewalInFuturePeriod()
  }

  function issueCancellationFromWorkflow() {
    try {

      // Do not issue cancellation if this cancellation has already been withdrawn or rescinded by a prior cancellation
      if (_branch.Status == TC_WITHDRAWN) {
        JobProcessLogger.logInfo("Not issuing withdrawn or rescinded cancellation for branch \"${_branch}\"")
        return
      }

      issueCancellation()

    } catch (e : JobStateException) {
      escalate(DisplayKey.get("Job.Cancellation.Escalation.CannotCancel", _branch.PolicyNumber), e.Message)
    }
  }

  /**
   * Checks the conditions for which the cancellation can be finished.
   */
  function canFinishCancellation() : JobConditions {
    return canFinishJob(DisplayKey.get("Job.Process.Cancellation.FinishCancellation"))
        .checkStatus(TC_CANCELING)
  }

  /**
   * Finishes cancellation.
   * As of 8.0.4, this no longer does it's own commit.  If calling from other than {@link RewriteNewAccountProcess#startBinding()},
   * you need to ensure the changes are committed.
   */
  function finishCancellation() {
    canFinishCancellation().assertOkay()
    JobProcessLogger.logInfo("Finish binding cancellation for branch \"${_branch}\"")
    prepareBranchForFinishingJob()
    _branch.Job.copyUsersFromJobToPolicy()
    processAudits()

    // For flat cancellation reporting policies, deposit released is set to true, and deposit will
    // be set to 0 when sent to the billing system.  For non flat cancellation in WC reporting
    // policies, deposit release is false, and deposit from the orignal basedon is sent to the
    // billing system.  See calculateDeposit() in PolicyInfoExt for more information.

    if (_branch.CancellationDate == _branch.PeriodStart and _branch.IsReportingPolicy) {
      _branch.DepositAmount = MonetaryAmounts.zeroOf(_branch.PreferredSettlementCurrency)
      _branch.PolicyTerm.DepositReleased = true
      _branch.PolicyTerm.DepositAmount = MonetaryAmounts.zeroOf(_branch.PreferredSettlementCurrency)
    }

    _branch.updateTrendAnalysisValues()
    withdrawOtherCancellations()
    createBillingEventMessages()
    if (Job.CancelReasonCode == TC_FLATREWRITE) {
      Job.createRoleActivity(TC_UNDERWRITER,
          ActivityPattern.finder.getActivityPatternByCode("notification"),
          DisplayKey.get("Job.Cancellation.RenewalToBeRewritten", _branch.PolicyNumber),
          DisplayKey.get("Job.Cancellation.RenewalToBeRewritten.Description"))
    }
    if (_branch.GLLineExists and (_branch.Offering.CodeIdentifier == "PLCyberLiab_TDIC" or
        _branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or
        (_branch.Offering.CodeIdentifier == "PLOccurence_TDIC" and _branch.GLLine.GLDentalEmpPracLiabCov_TDICExists))
        and Job.CancelReasonCode != typekey.ReasonCode.TC_NOTTAKEN) {
      var actPattern = ActivityPattern.finder.getActivityPatternByCode("send_extendedreportingperiodendosement_offer")
      var act = _branch.Job.createGroupActivity_TDIC(actPattern, DisplayKey.get("TDIC.ActivityPattern.Subject.Send_Extendedreportingperiodendosement_Offer", _branch.Offering), actPattern.Description)
      act.autoAssign()
    }
    //BR-0260 and BR-0261 of PL and CP Activities.
    if (_branch.GLLineExists and (_branch.Offering.CodeIdentifier == "PLCyberLiab_TDIC" or
        _branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC") and (_branch.RefundCalcMethod != CalculationMethod.TC_FLAT)
        and (Job.CancelReasonCode == typekey.ReasonCode.TC_NOTTAKEN || Job.CancelReasonCode == typekey.ReasonCode.TC_NONPAYMENT) &&
        (_branch.BasedOn.Renewal != null)) {
      if (_branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        var actPattern = ActivityPattern.finder.getActivityPatternByCode("ere_pl_nonpay_tdic")
        var act = _branch.Job.createGroupActivity_TDIC(actPattern, actPattern.Subject, actPattern.Description)
        act.autoAssign()
      }
      if (_branch.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        var actPattern = ActivityPattern.finder.getActivityPatternByCode("ere_cyb_nonpay_tdic")
        var act = _branch.Job.createGroupActivity_TDIC(actPattern, actPattern.Subject, actPattern.Description)
        act.autoAssign()
      }
    }
    if (_branch.BOPLineExists and _branch.Policy.Account.IssuedPolicies?.hasMatch(\issuedPolicy -> issuedPolicy?.Product?.CodeIdentifier == "GeneralLiability"
        and issuedPolicy?.Job?.LatestPeriod?.Offering?.CodeIdentifier == "PLCyberLiab_TDIC")) {
      var actPattern = ActivityPattern.finder.getActivityPatternByCode("cancel_cybersuite_liability")
      var act = _branch.Job.createGroupActivity_TDIC(actPattern, actPattern.Subject, actPattern.Description)
      if (this.AutomatedProcess) {
        act.autoAssign()
      } else {
        act.assignUserAndDefaultGroup(User.util.CurrentUser)
      }
    }
    // An activity should be created in Claim Center for Flat Cancelled Policy Claims
    try {
      if (_branch.CancellationDate == _branch.PeriodStart) {
        var claimAPI = new ClaimAPI()
        claimAPI.createFlatCancelPolicyActivity(_branch.PolicyNumber, _branch.PeriodStart, _branch.PeriodEnd)
      }
    } catch (e : Exception) {
      JobProcessLogger.logError("Error encountered during activity creation in Claim Center", e)
      throw new DisplayableException(DisplayKey.get("Web.Claims.ClaimSystem.Error.GeneralException", ExceptionUtils.getRootCauseMessage(e)))
    }
    // run this before promote branch because it more likely to fail
    bindReinsurableRisks() // flags Activity on error...

    try {
      _branch.promoteBranch(true)
    } catch (e : IllegalArgumentException) {
      // the following exception is hardcoded at platform level and as per PC-30436 we're making it translatable
      // preemption check is invoked for all jobs so, theoretically, can be thrown for any transaction
      if (e.Message == "Cannot promote preempted branch") {
        throw new DisplayableException(DisplayKey.get("Job.Error.Preempted"))
      }
      throw e
    }
  }

  protected override function processSpecificPreemptionHandling(newBranch : PolicyPeriod) {
    newBranch.Cancellation.CancelProcessDate = null
    removeCancellationWorkflow()
  }

  override function initializeFuturePeriodJob(futureJob : Job) {
    var futureCancellation = futureJob as Cancellation
    futureCancellation.Source = this.Job.Source
    futureCancellation.CancelReasonCode = this.Job.CancelReasonCode
    futureCancellation.PolicyPeriod.RefundCalcMethod = this.Job.PolicyPeriod.RefundCalcMethod
  }

  override function canWithdraw() : JobConditions {
    return super.canWithdraw()
        .checkCondition(!InitialNotificationsHaveBeenSent, DisplayKey.get("Web.Cancellation.Error.NotificationsSent"))
        .checkNull(Job.CancelProcessDate, "Cancellation has been processed")
  }

  override function withdrawWithoutCheckingConditions() {
    super.withdrawWithoutCheckingConditions()
    Job.NotificationDate = null
  }

  override function applyChangeToFutureRenewalAutomatic() : boolean {
    return true
  }

  override function canApplyChangesToFutureBoundRenewal() : boolean {
    return canApplyChangeToFutureRenewal(true)
  }

  override function canApplyChangesToFutureUnboundRenewal() : boolean {
    return canApplyChangeToFutureRenewal(false) and hasOpenRenewalToWithdraw()
  }

  override function applyChangesToFutureBoundRenewal() : ApplyChangesResult[] {
    // BrianS - Don't start a future cancel if there is already one in progress for Day 1 of the renewal term.
    var futureRenewal = _branch.FutureRenewals.first()
    var suppressFutureCancels = _branch.Policy.OpenJobs.whereTypeIs(Cancellation)
        .hasMatch(\c -> c.LatestPeriod.PolicyTerm == futureRenewal.PolicyTerm
            and c.LatestPeriod.EditEffectiveDate == futureRenewal.PeriodStart)
    var retval : ApplyChangesResult[]
    if (not suppressFutureCancels) {
      var futureBranch = Job.createFutureJob(_branch.NextRenewal).LatestPeriod
      futureBranch.Cancellation.IsRollForwardCancellation_TDIC = true
      _branch.clearResolveWithFuturePeriods()
      futureBranch.CancellationProcess.start()
      _branch.Bundle.commit()
      retval = {new ApplyChangesResult(futureBranch, new ArrayList<DiffItem>(), _branch.EditEffectiveDate)}
    } else {
      _branch.clearResolveWithFuturePeriods()
      _branch.Bundle.commit()
    }
    return retval
  }

  override function applyChangesToFutureUnboundRenewal() : ApplyChangesResult[] {
    var futureBranch = _branch.NextRenewal
    _branch.clearResolveWithFuturePeriods()
    _branch.Policy.OpenRenewalJob.withdrawOrSetNonRenewal()
    _branch.Bundle.commit()
    return {new ApplyChangesResult(futureBranch, new ArrayList<DiffItem>(), _branch.EditEffectiveDate)}
  }

  override function createBillingEventMessages() {
    _branch.addEvent(PolicyPeriod.CANCELPERIOD_EVENT)
  }

  override function setPaymentInfoWithNewQuote() {
    // No payment info needed for a Cancellation
    // superclass implementation will throw exceptions
  }

  function hasOpenRenewalToWithdraw() : boolean {
    var renewalJob = _branch.Policy.OpenRenewalJob
    var renewal = renewalJob.ActivePeriods[0].RenewalProcess
    return (renewal != null)
  }

  function hasBoundRenewalToCancel() : boolean {
    return hasFutureRenewals(true)
  }

  private function processAudits() {
    if (_branch.IsAuditable) {
      JobProcessLogger.logInfo("CancellationProcess::processAudits - PeriodStart = "
          + _branch.PeriodStart.formatDateTime(SHORT, SHORT) + ", CancellationDate = "
          + _branch.CancellationDate.formatDateTime(SHORT, SHORT))

      for (ai in _branch.AuditInformations) {
        JobProcessLogger.logInfo("Audit Before:  " + ai.Audit.JobNumber + " - "
            + ai.AuditPeriodStartDate.formatDate(SHORT) + "-"
            + ai.AuditPeriodEndDate.formatDate(SHORT) + " - " + ai.UIDisplayName + " - "
            + ai.AuditMethod.DisplayName + " - " + ai.DisplayStatus)
      }

      var auditInformation = _branch.AuditInformations.maxBy(\ai -> ai.ID)
      var initDate = auditInformation.InitDate
      var dueDate = auditInformation.DueDate
      var dontChangeDates = (DateUtil.currentDate() >= _branch.PeriodEnd and
          _branch.OpenFinalAudit.AuditInformation.HasBeenStarted)
      _branch.withdrawOpenRevisedFinalAudit()
      _branch.reverseFinalAudits()
      var hasScheduledFinalAudit = _branch.hasScheduledFinalAudit()
      _branch.removeScheduledFinalAudit()
      if (_branch.CancellationDate == _branch.PeriodStart) {
        _branch.cancelPremiumReports()
        _branch.withdrawOpenFinalAudit()
        _branch.addEvent(PolicyPeriod.WAIVEFINALAUDIT_EVENT)
      } else if (_branch.hasOpenFinalAudit()) {
        _branch.updateAuditPeriodEndDatesFollowingCancellation()
        _branch.rescheduleAuditSeries()
        _branch.createActivitiesTriggeredByCancellation()
      } else if (hasScheduledFinalAudit or not _branch.hasWaivedFinalAudit()) {
        _branch.rescheduleAuditSeries()
        //_branch.scheduleCancellationFinalAudit()
        // use TDIC version                                                                          3
        _branch.scheduleCancellationFinalAudit_TDIC()
      }
      if (dontChangeDates) {
        // Reset Init Date and Due Date
        auditInformation = _branch.AuditInformations.maxBy(\ai -> ai.ID)

        if (auditInformation.InitDate != initDate) {
          JobProcessLogger.logInfo("Resetting Audit Init Date on " + _branch
              + " from " + auditInformation.InitDate.formatDate(SHORT)
              + " to " + initDate.formatDate(SHORT))
          auditInformation.InitDate = initDate
        }
        if (auditInformation.DueDate != dueDate) {
          JobProcessLogger.logInfo("Resetting Audit Due Date on " + _branch
              + " from " + auditInformation.DueDate.formatDate(SHORT)
              + " to " + dueDate.formatDate(SHORT))
          auditInformation.DueDate = dueDate
        }
      }

      for (ai in _branch.AuditInformations) {
        JobProcessLogger.logInfo("Audit After:  " + ai.Audit.JobNumber + " - "
            + ai.AuditPeriodStartDate.formatDate(SHORT) + "-"
            + ai.AuditPeriodEndDate.formatDate(SHORT) + " - " + ai.UIDisplayName + " - "
            + ai.AuditMethod.DisplayName + " - " + ai.DisplayStatus)
      }
    }
  }

  private function withdrawOtherCancellations() {
    var otherCancellations = _branch.Policy.OpenJobs.whereTypeIs(Cancellation)
    // BrianS - Don't withdraw delinquency cancels in other terms or earlier than this cancellation in the current term.
    otherCancellations = otherCancellations.where(\c -> c.IsDelinquencyCancellation_TDIC == false or
        (c.LatestPeriod.PolicyTerm == _branch.PolicyTerm and
            c.LatestPeriod.EditEffectiveDate.afterOrEqual(_branch.EditEffectiveDate)))
    for (cancellation in otherCancellations) {
      var cancellationProcess = cancellation.PolicyPeriod.CancellationProcess
      JobProcessLogger.logInfo("Withdrawing pre-empted cancellation #${cancellation.JobNumber}")
      if (cancellationProcess.canWithdrawJob().Okay) {
        cancellationProcess.withdrawJob()
      } else if (cancellationProcess.canRescind(true).Okay) {
        // try to withdraw the completed cancellation
        cancellationProcess.withdrawWithoutCheckingConditions()

      }
    }
  }

  private function withdrawOrCancelRenewalInFuturePeriod() {
    if (_branch.ResolveWithFuturePeriods) {
      if (canApplyChangesToFutureBoundRenewal()) {
        var apply = applyChangesToFutureBoundRenewal()
        var renewal = apply*.Branch[0]
        Job.createRoleActivity(TC_UNDERWRITER, ActivityPattern.finder.getActivityPatternByCode("notification"),
            DisplayKey.get("Cancellation.FuturePeriod.Cancel.Subject", renewal.PolicyNumber),
            DisplayKey.get("Cancellation.FuturePeriod.Cancel.Description", renewal.Job.JobNumber, renewal.PolicyNumber))
      } else if (canApplyChangesToFutureUnboundRenewal()) {
        var apply = applyChangesToFutureUnboundRenewal()
        var renewal = apply*.Branch[0]
        // create and activity to notify about the new status of the withdrawal
        if (renewal.Status == PolicyPeriodStatus.TC_WITHDRAWN) {
          Job.createRoleActivity(TC_UNDERWRITER, ActivityPattern.finder.getActivityPatternByCode("notification"),
              DisplayKey.get("Cancellation.FuturePeriod.Withdraw.Subject", renewal.PolicyNumber),
              DisplayKey.get("Cancellation.FuturePeriod.Withdraw.Description", renewal.Job.JobNumber, renewal.PolicyNumber))
        } // else uw issue was created
        else if (renewal.Status == PolicyPeriodStatus.TC_NONRENEWING) {
          Job.createRoleActivity(TC_UNDERWRITER, ActivityPattern.finder.getActivityPatternByCode("notification"),
              DisplayKey.get("Cancellation.FuturePeriod.NonRenewing.Subject", renewal.PolicyNumber),
              DisplayKey.get("Cancellation.FuturePeriod.NonRenewing.Description", renewal.Job.JobNumber, renewal.PolicyNumber))
        } else {
          throw new IllegalStateException("Unexpected renewal status: ${renewal.Status}")
        }
      }
    }
  }

  override function issueJob(bindAndIssue : boolean) {
    throw new UnsupportedOperationException("Cannot issueJob for CancellationProcess")
  }

  // ---------------------------------------------------------- Job escalation

  /**
   * Checks the conditions for escalating the job.
   */
  function canEscalate() : JobConditions {
    return startChecksFor(DisplayKey.get("Job.Process.Cancellation.JobEscalation"))
        .checkBranchNotLocked()
  }

  /**
   * Escalates the job.
   */
  function escalate(subject : String, description : String) {
    canEscalate().assertOkay()
    JobProcessLogger.logInfo("Escalating cancellation branch: ${_branch}")
    Job.createRoleActivity(TC_UNDERWRITER,
        ActivityPattern.finder.getActivityPatternByCode("notification"),
        subject, description)
  }

  private class EscalationReasonChecker {
    private var _messages = new ArrayList<String>()
    private var _state : PolicyPeriodStatus

    construct(state : PolicyPeriodStatus) {
      _state = state
    }

    function checkForUnhandledPreemptions() : EscalationReasonChecker {
      if (_branch.hasAnyUnhandledPreemptions()) {
        _messages.add(DisplayKey.get("Job.Cancellation.Escalation.Reason.UnhandledPreemptions"))
      }
      return this
    }
  }

  /**
   * Create Documents for Cancellation and Audit documents
   */
  private function createDocumentsForCurrentJob() {
    Job.createSingleDocumentForCurrentJob()
    var auditMethod = _branch.AuditInformations.firstWhere(\elt -> elt.PolicyTerm == _branch.PolicyTerm)?.AuditMethod
    if (_branch.RefundCalcMethod != typekey.CalculationMethod.TC_FLAT) {
      /*JIRA-2284 - commented AuditNotice doc generation for mid term cancels.
       This doc generation is now part of Audit document creation batch process.*/
      /*if( auditMethod  == typekey.AuditMethod.TC_VOLUNTARY){
        _branch.Job.createSingleDocumentsForEvent(TDIC_DocCreationEventType.TC_AUDITNOTICE.Code)
      }else*/

      if (auditMethod == typekey.AuditMethod.TC_PHYSICAL) {
        // JIRA-828
        if ((this.Job.CancelReasonCode != ReasonCode.TC_NONPAYMENT)) {
          _branch.Job.createSingleDocumentsForEvent(TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER.Code)
        }
      }
    }
  }

  /**
   * US884
   * Shane Sheridan 03/12/2015
   */
  private function createPolicyWithDiscountCancelledActivity(){
    var pattern = ActivityPattern.finder.getActivityPatternByCode(_DISCOUNT_POLICY_CANCELLED_PATTERN_CODE)
    if(pattern != null){
      var activity = Job.createGroupActivity_TDIC(pattern, null, null)
      if(activity != null){
        // assign using Business Rules.
        activity.autoAssign()
      }
    }
    else{
      JobProcessLogger.logWarning("Activity Pattern not found for: "+ _DISCOUNT_POLICY_CANCELLED_PATTERN_CODE)
    }
  }
}
