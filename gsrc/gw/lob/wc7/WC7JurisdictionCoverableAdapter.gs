package gw.lob.wc7

uses gw.api.domain.CoverableAdapter
uses java.lang.UnsupportedOperationException
uses java.util.Date

uses gw.api.domain.ModifiableAdapter
uses gw.api.util.StateJurisdictionMappingUtil
uses gw.policy.PolicyLineConfiguration
uses java.util.List
uses gw.api.locale.DisplayKey

@Export
class WC7JurisdictionCoverableAdapter implements CoverableAdapter, ModifiableAdapter {
  var _owner : WC7Jurisdiction
  delegate _modifiable represents ModifiableAdapter

  construct(owner : WC7Jurisdiction) {
    _owner = owner
    _modifiable = new WC7JurisdictionModifiableAdapter(owner)
  }

  override property get PolicyLine() : PolicyLine {
    return _owner.WCLine
  }

  override property get PolicyLocations() : PolicyLocation[] {
    return _owner.Branch.PolicyLocations.where(\ p -> p.State == StateJurisdictionMappingUtil.getStateMappingForJurisdiction(_owner.Jurisdiction ))
  }

  override property get State() : Jurisdiction {
    return _owner.Jurisdiction
  }

  override property get CoveragesFromCoverable() : Coverage[] {
    return _owner.Coverages
  }

  override function addCoverage( p0: Coverage ) : void {
    _owner.addToCoverages(p0 as WC7JurisdictionCov)
  }

  override function removeCoverage( p0: Coverage ) : void {
    _owner.removeFromCoverages(p0 as WC7JurisdictionCov)    
  }

  override property get ExclusionsFromCoverable() : Exclusion[] {
    return new Exclusion[0]
  }

  override function addExclusion( p0: Exclusion ) : void {
    throw new UnsupportedOperationException(DisplayKey.get("CoverableAdapter.Error.ExclusionsNotImplemented"))
  }

  override function removeExclusion( p0: Exclusion ) : void {
    throw new UnsupportedOperationException(DisplayKey.get("CoverableAdapter.Error.ExclusionsNotImplemented"))
  }

  override property get ConditionsFromCoverable() : PolicyCondition[] {
    return _owner.Conditions
  }

  override function addCondition( p0: PolicyCondition ) : void {
    _owner.addToConditions(p0 as WC7JurisdictionCond)
  }

  override function removeCondition( p0: PolicyCondition ) : void {
    _owner.removeFromConditions(p0 as WC7JurisdictionCond)
  }

  override property get ReferenceDateInternal() : Date {
    return _owner.ReferenceDate
  }
  
  override property set ReferenceDateInternal( date : Date )  {
    _owner.ReferenceDate = date
  }
  
  override property get DefaultCurrency() : Currency {
    return _owner.WCLine.PreferredCoverageCurrency
  }

  override property get AllowedCurrencies() : List<Currency> {
    return PolicyLineConfiguration.getByLine(InstalledPolicyLine.TC_WC7).AllowedCurrencies
  }

  override property get AssociatedCoveragePartTypes() : CoveragePartType[] {
    return {}
  }
}
