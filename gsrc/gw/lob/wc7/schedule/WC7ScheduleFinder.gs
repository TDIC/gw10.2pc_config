package gw.lob.wc7.schedule

uses gw.api.productmodel.ClausePattern

interface WC7ScheduleFinder {
  function findBy(clausePattern : ClausePattern) : WC7Schedule
}