package gw.lob.wc7.schedule

uses gw.api.domain.Clause
uses gw.api.util.DisplayableException
uses gw.lang.reflect.features.BoundPropertyReference

class WC7AddNewContactDetail<T extends WC7PolicyContactRole, U extends WC7ContactDetail> {
  var _contactRoleType : typekey.PolicyContactRole
  var _existingContactRoles :  T[]
  var _line : WC7WorkersCompLine
  var _addContactRole(contactRole : T)
  var _addNewContactDetail(contactRole : T, scheduleClause : Clause) : U

  construct(
      contactRoleType : typekey.PolicyContactRole,
      existingContactRolesProperty :  BoundPropertyReference<WC7WorkersCompLine, T[]>,
      addContactRole(contactRole : T),
      addNewContactDetail(contactRole : T, scheduleClause : Clause) : U) {
    _contactRoleType = contactRoleType
    _existingContactRoles = existingContactRolesProperty.get()
    _line = existingContactRolesProperty.Ctx as WC7WorkersCompLine
    _addContactRole = addContactRole
    _addNewContactDetail = addNewContactDetail
  }

  function forContact(contact : Contact, scheduleClause : Clause) : U {
    if (scheduleClause == null)
      throw new DisplayableException("Parent condition cannot be null")
    var targetContactRole = _existingContactRoles.firstWhere(\ role ->
        role.AccountContactRole.AccountContact.Contact == contact)
    if (targetContactRole == null) {
      targetContactRole = _line.Branch.addNewPolicyContactRoleForContact(contact, _contactRoleType) as T
      _addContactRole(targetContactRole)
    }
    return _addNewContactDetail(targetContactRole, scheduleClause)
  }

  function forContactType(contactType : ContactType, scheduleClause : Clause) : U {
    var newAccountContact = _line.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return forContact(newAccountContact.Contact, scheduleClause)
  }
}