package gw.lob.wc7.schedule

uses gw.api.domain.Clause
uses java.lang.IllegalArgumentException
uses java.util.List
uses gw.api.locale.DisplayKey

class WC7ScheduleClientPresenter {
  enum Input {
    State,
    ContractDates
  }

  static function forBasicContact(contactType : ContactType, parentClause : Clause) : WC7ScheduleClientPresenter {
    return new WC7ScheduleClientPresenter(
        parentClause,
        contactDetailFactoryFor(parentClause, \ line ->
            line.addNewBasicContactDetailFor(contactType, parentClause)),
        DisplayKey.get("Web.Contact.NewContact", WC7PolicyContactRole.Type.DisplayName),
        {State, ContractDates})
  }

  static function forLaborClient(contactType : ContactType, parentClause : Clause) : WC7ScheduleClientPresenter {
    return new WC7ScheduleClientPresenter(
        parentClause,
        contactDetailFactoryFor(parentClause, \ line ->
            line.addNewLaborClientContactDetailFor(contactType, parentClause)))
  }

  private static function contactDetailFactoryFor(
      parentClause : Clause,
      createDetail(line : WC7WorkersCompLine) : WC7ContactDetail
  ) : block() : WC7ContactDetail {
    var line = parentClause.PolicyLine as WC7Line
    return \ -> createDetail(line)
  }

  var _parentClause : Clause
  var _createContactDetail() : WC7ContactDetail
  var _popupTitle : String as readonly PopupTitle
  var _hiddenInputs : List<Input>

  construct(
      parentClause : Clause,
      createContactDetail() : WC7ContactDetail,
      popupTitle : String = null,
      hiddenInputs : List<Input> = null) {
    _parentClause = parentClause
    _createContactDetail = createContactDetail
    _popupTitle = popupTitle ?: defaultPopupTitle()
    _hiddenInputs = hiddenInputs ?: {}
  }
  
  property get Line() : WC7Line {
    return _parentClause.PolicyLine as WC7Line
  }
  
  property get ScheduleLabel() : String {
    return labelFor(_parentClause)
  }
  
  property get ParentClause() : Clause {
    return _parentClause
  }
  
  property get ShowState() : boolean {
    return not _hiddenInputs.contains(Input.State)
  }

  property get ShowContractDates() : boolean {
    return not _hiddenInputs.contains(Input.ContractDates)
  }

  function addNewDetail() : WC7ContactDetail {
    return _createContactDetail()
  }
    
  private function labelFor(clause : Clause) : String {
    if (clause typeis PolicyCondition)
      return DisplayKey.get("Java.ProductModel.Name.Condition")
    else if (clause typeis Exclusion)
      return DisplayKey.get("Java.ProductModel.Name.Exclusion")
    else
      throw new IllegalArgumentException("Unsupported schedule clause type: " + typeof clause)
  }
  
  private function defaultPopupTitle() : String {
    return DisplayKey.get("Web.Contact.NewContact", entity.WC7PolicyLaborClient.Type.TypeInfo.DisplayName)
  }
}
