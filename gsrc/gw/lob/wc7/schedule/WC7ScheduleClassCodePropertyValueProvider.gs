package gw.lob.wc7.schedule

uses gw.api.productmodel.SchedulePropertyValueProvider
uses gw.entity.IEntityPropertyInfo

/**
 * Can't use the ScheduleForeignKeyPropertyValueProvider because it casts the objects of type T to be
 * EffDated which is not valid for WC7ClassCodes which are system table entities and are of type SimpleEffDated
 * which is not a child of EffDated.
*/
class WC7ScheduleClassCodePropertyValueProvider<T extends KeyableBean> extends SchedulePropertyValueProvider<T> {
  protected var _entity : ScheduledItem
  protected var _prop : IEntityPropertyInfo

  construct(entity : ScheduledItem, propertyInfo : IEntityPropertyInfo) {
    super(entity, propertyInfo);
    _entity = entity
    _prop = propertyInfo
  }

  override public property get Value() : T {
    return _prop.getAccessor().getValue(_entity) as T
  }

  override public property set Value(obj : T) {
    _prop.getAccessor().setValue(_entity, obj);
  }
}