package gw.lob.wc7

uses gw.api.productmodel.PolicyLinePatternLookup
uses gw.lob.wc7.rating.WC7RateRoutineConfig
uses gw.plugin.rateflow.IRateRoutineConfig
uses gw.policy.PolicyLineConfiguration
uses java.util.List

class WC7Configuration extends PolicyLineConfiguration {

  override property get RateRoutineConfig(): IRateRoutineConfig {
    return new WC7RateRoutineConfig()
  }

  override property get AllowedCurrencies(): List<Currency> {
    var pattern = PolicyLinePatternLookup.getByPublicID(InstalledPolicyLine.TC_WC7.UnlocalizedName)
    return pattern.AvailableCurrenciesByPriority
  }
}