package gw.lob.wc7.financials

uses gw.api.locale.DisplayKey

class WC7WaiverOfSubroSpecificBalanceCostMethodsImpl extends WC7GenericWC7CostMethodsImpl<WC7WaiverOfSubroSpecificBalanceCost> {

  construct(owner : WC7WaiverOfSubroSpecificBalanceCost) {
    super(owner)
  }
  
  override property get JurisdictionState() : Jurisdiction {
    return Cost.WC7Jurisdiction.Jurisdiction
  }

  override property get Description() : String {
    return DisplayKey.get("Web.Policy.WC7.SpecificWaiverBalanceCostDisplay", Cost.JobID)
  }
}