package gw.lob.wc7

uses gw.api.productmodel.ClausePattern
uses java.util.Collection
uses gw.api.upgrade.PCCoercions

/**
 * Employee leasing clauses in the WC basic template module.
 */
class WC7EmployeeLeasingClausesInLOB {
  
  protected var _clauses : Collection<ClausePattern>
  
  protected construct() {
    _clauses = {
      PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientEndorsementCond"),      // Employee Leasing Client Endorsement
      PCCoercions.makeProductModel<ClausePattern>("WC7EmployeeLeasingClientExclEndorsementExcl"),  // Employee Leasing Client Exclusion Endorsement
      PCCoercions.makeProductModel<ClausePattern>("WC7LaborContractorEndorsementACond"),           // Employee Leasing Endorsement (was, Labor Contractor endorsement)
      PCCoercions.makeProductModel<ClausePattern>("WC7LaborContractorExclEndorsementExcl"),        // Employee Leasing Exclusion Endorsement (was, Labor Contractor Exclusion Endorsement)
      PCCoercions.makeProductModel<ClausePattern>("WC7MultipleCoordinatedPolicyEndorsementCond")   // Multiple Coordinated Policy Endorsement (MCP)
    }
  }
  
  function get() : Collection<ClausePattern> {
    return _clauses
  }

}
