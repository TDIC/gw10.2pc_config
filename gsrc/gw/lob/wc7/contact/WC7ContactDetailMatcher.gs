package gw.lob.wc7.contact

uses gw.api.logicalmatch.AbstractEffDatedPropertiesMatcher
uses gw.entity.IEntityPropertyInfo
uses gw.entity.ILinkPropertyInfo
uses java.lang.Iterable

/**
 * {@link WC7ContactDetail}s do not have sufficiently unique columns to define matches in
 * the out of the box config, thus this matcher will return false when asked for matches.
 */
@Export
class WC7ContactDetailMatcher extends AbstractEffDatedPropertiesMatcher<WC7ContactDetail> {

  construct(contactDetail : WC7ContactDetail) {
    super(contactDetail)
  }

  override property get IdentityColumns() : Iterable<IEntityPropertyInfo> {
    return { WC7LaborContactDetail#ID.PropertyInfo as IEntityPropertyInfo }
  }

  override property get ParentColumns() : Iterable<ILinkPropertyInfo> {
    return { WC7LaborContactDetail#WC7Contact.PropertyInfo as ILinkPropertyInfo }
  }

  // OOTB, WC7ContactDetail entities are not matchable.  Customers should delete the overridden
  // isLogicalMatch() method and implement the IdentityColumns property if they would like to match
  // these entities (e.g. if a unique WC labor contact detail identifier was created and used).

  override function isLogicalMatch(other : WC7ContactDetail) : boolean {
    return false
  }

  override function isLogicalMatchUntyped(bean : KeyableBean) : boolean {
    return bean typeis WC7ContactDetail and isLogicalMatch(bean)
  }  
}
