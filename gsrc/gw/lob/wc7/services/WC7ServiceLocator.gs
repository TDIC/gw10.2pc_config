package gw.lob.wc7.services

uses gw.api.productmodel.ClausePattern
uses gw.lob.wc7.schedule.WC7ScheduleFinder

abstract class WC7ServiceLocator {
  static var _instance : WC7ServiceLocator

  static property get Instance() : WC7ServiceLocator {
    if (_instance == null)
      _instance = new WC7Configuration().newServiceLocator()
    return _instance
  }

  abstract property get CoverageInputSetModeFinder() : block(pattern : ClausePattern) : String

  abstract function scheduleFinderFor(line : WC7Line) : WC7ScheduleFinder
}