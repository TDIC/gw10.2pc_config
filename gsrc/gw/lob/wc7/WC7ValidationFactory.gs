package gw.lob.wc7

uses gw.validation.PCValidationContext
uses java.util.List

/**
 * Overridable factory class so we can use a slightly different PolicyLineValidation for standards based
 * vs. basic template
 */
class WC7ValidationFactory {

  static function getScreenPolicyLineValidation(line : WC7Line) : WC7ScreenLineValidation {
    return new WC7ScreenLineValidation(line)
  }

  static function getPolicyLineValidation(validationContext : PCValidationContext, line : WC7Line) : WC7LineValidation {
    return new WC7LineValidation(validationContext, line)
  }

  /**
   * Get post-quote validation messages for display on the quote page.
   */
  static function getPostQuoteValidation(line : WC7Line) : List<String> {
    return {}
  }

  static function getJurisdictionValidation(context : PCValidationContext, jurisdictionCoverable : WC7Jurisdiction)
      : WC7JurisdictionValidation {
    return new WC7JurisdictionValidation(context, jurisdictionCoverable)
  }
}
