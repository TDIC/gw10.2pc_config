package gw.lob.bop.rating

uses java.util.Date
uses gw.api.effdate.EffDatedUtil
uses entity.windowed.BOPBuildingVersionList
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.Key
uses java.util.List


class BOPBuildingCostData_TDIC extends BOPCostData<BOPBuildingCost_TDIC> {

  var _buildingID : Key
  var _costType : BOPCostType_TDIC


  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction, buildingID : Key) {
    super(effDate, expDate, stateArg)
    assertKeyType(buildingID, BOPBuilding)
    init(buildingID)

  }


  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, buildingID : Key) {
    super(effDate, expDate, c, rateCache, stateArg)
    assertKeyType(buildingID, BOPBuilding)
    init(buildingID)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, buildingID : Key, costType : BOPCostType_TDIC) {
    super(effDate, expDate, c, rateCache, stateArg)
    assertKeyType(buildingID, BOPBuilding)
    init(buildingID)
    _costType = costType
  }


  construct(cost : BOPBuildingCost_TDIC) {
    super(cost)
    init(cost.BOPBuilding_TDIC.FixedId)
    _costType = cost.BOPCostType_TDIC
  }


  construct(cost : BOPBuildingCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    init(cost.BOPBuilding_TDIC.FixedId)
    _costType = cost.BOPCostType_TDIC
  }


  private function init(buildingID : Key) {
    _buildingID = buildingID
  }


  override function setSpecificFieldsOnCost(line : BOPLine, cost : BOPBuildingCost_TDIC) {
    super.setSpecificFieldsOnCost(line, cost)
    cost.setFieldValue("BOPBuilding_TDIC", _buildingID)
    cost.BOPCostType_TDIC = _costType
  }


  override function getVersionedCosts(line : BOPLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    var covVL = EffDatedUtil.createVersionList(line.Branch, _buildingID) as BOPBuildingVersionList
    var costs = covVL.Costs.allVersions<BOPBuildingCost_TDIC>(true) // warm up the bundle and global cache
    return costs.Keys.where(\VL -> costs[VL].first().Building.FixedId == _buildingID and costs[VL].first().BOPCostType_TDIC == _costType)
  }


  protected override property get KeyValues() : List<Object> {
    return {_buildingID.toString() + _costType.toString()}
  }

}