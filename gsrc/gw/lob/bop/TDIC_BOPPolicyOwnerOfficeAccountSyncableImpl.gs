package gw.lob.bop
uses gw.contact.AbstractPolicyContactRoleAccountSyncableImpl
uses gw.account.AccountContactRoleToPolicyContactRoleSyncedField
uses com.google.common.collect.ImmutableSet
uses gw.api.domain.account.AccountSyncedField
uses gw.api.domain.account.AccountSyncable
uses java.util.Set

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/15/2020
 * Create Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_BOPPolicyOwnerOfficeAccountSyncableImpl extends AbstractPolicyContactRoleAccountSyncableImpl<BOPPolicyOwnerOfficer_TDIC> {

    static final var ACCOUNT_SYNCED_FIELDS = ImmutableSet.copyOf  (
        AbstractPolicyContactRoleAccountSyncableImpl.AccountSyncedFieldsInternal.union(
            {
                AccountContactRoleToPolicyContactRoleSyncedField.WC7RelationshipTitle
            }
        )
    )

    protected static property get AccountSyncedFieldsInternal() : Set<gw.api.domain.account.AccountSyncedField<gw.api.domain.account.AccountSyncable, java.lang.Object>> {
      // provided so subclasses can extend this list
      return ACCOUNT_SYNCED_FIELDS
    }

    construct(accountSyncable : BOPPolicyOwnerOfficer_TDIC) {
      super(accountSyncable)
    }

    override property get AccountSyncedFields() : Set<gw.api.domain.account.AccountSyncedField<gw.api.domain.account.AccountSyncable, java.lang.Object>> {
      // must override to ensure that we call the correct static AccountSyncedFieldsInternal property
      return AccountSyncedFieldsInternal
    }
  }


