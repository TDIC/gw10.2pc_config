package gw.lob.bop

uses gw.lob.common.AbstractPolicyInfoValidation
uses gw.validation.PCValidationContext
uses gw.validation.ValidationUtil
uses gw.validation.PCValidationBase

@Export
class BOPPolicyInfoValidation extends AbstractPolicyInfoValidation<BOPLine> {
    
  //var _line : BOPLine as Line

  property get bopLine() : BOPLine {
    return _line
  }

  construct(valContext : PCValidationContext, polLine : BOPLine) {
    super(valContext, polLine);
    _line = polLine
  }
  
  override protected function validateImpl() {
    Context.addToVisited(this, "validateImpl")
    checkYearBusinessStartedMakesSense()
  }

  function checkYearBusinessStartedMakesSense() {
    Context.addToVisited(this, "checkYearBusinessStartedMakesSense")
    new gw.policy.PolicyYearBusinessStartedValidator(Context, _line).validate()
  }
  
  static function validateFields(line : BOPLine) {
    var context = ValidationUtil.createContext(TC_DEFAULT)
    new BOPPolicyInfoValidation(context, line).validate()
    context.raiseExceptionIfProblemsFound()
  }
}
