package gw.lob.bop.financials

uses gw.api.locale.DisplayKey
uses gw.api.ui.BOPCostWrapper_TDIC
uses typekey.Currency
uses entity.BOPCost

enhancement BOPCostSetEnhancement<T extends BOPCost> : Set<T> {
  /**
   * Returns an AutoMap of boolean (indicating if a cost is a coverage premium)
   * to their costs.  The AutoMap returns an empty set of costs for any key
   * that is not in the map.
   */
  reified function byCoveragePremium() : Map<Boolean, Set<T>> {
    var ret = this.partition(\c -> c typeis BOPCoveragePremium).mapValues(\v -> v.toSet())
    var abc = ret.toAutoMap(\b -> Collections.emptySet<T>())
    return ret.toAutoMap(\b -> Collections.emptySet<T>())
  }

  /**
   * Returns an AutoMap of locations to their costs.  If the costs refer to
   * different versions of the same location, then the latest persisted one is
   * used as the key.  Any costs that are not associated to a location have a key
   * value of null.  Lastly, the AutoMap returns an empty set of costs for
   * any key that is not in the map.
   */
  reified function byFixedLocation() : Map<BOPLocation, Set<T>> {
    var locations = this.map(\c -> c.Location)
    var fixedIdToLocationMap = locations.partition(\l -> l.FixedId).mapValues(\list -> list.maxBy(\l -> l.ID))
    var ret = this.partition(\c -> fixedIdToLocationMap.get(c.Location.FixedId)).mapValues(\v -> v.toSet())
    return ret.toAutoMap(\b -> Collections.emptySet<T>())
  }

  /**
   * Returns an AutoMap of buildings to their costs.  If the costs refer to
   * different versions of the same building, then the latest persisted one is
   * used as the key.  Any costs that are not associated to a building have a key
   * value of null.  Lastly, the AutoMap returns an empty set of costs for
   * any key that is not in the map.
   */
  reified function byFixedBuilding() : Map<BOPBuilding, Set<T>> {
    var buildings = this.map(\c -> c.Building)
    var fixedIdToBuildingMap = buildings.partition(\l -> l.FixedId).mapValues(\list -> list.maxBy(\l -> l.ID))
    var ret = this.partition(\c -> fixedIdToBuildingMap.get(c.Building.FixedId)).mapValues(\v -> v.toSet())
    return ret.toAutoMap(\b -> Collections.emptySet<T>())
  }

  reified function byProperty_TDIC() : Set<T> {
    var buildingCostTypes = {BOPCostType_TDIC.TC_MULTILINEPROPERTYDISCOUNT, BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM, BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM}
    var modifierCosts = this.where(\elt -> (elt typeis BOPModifierCost_TDIC
        and elt.Modifier_TDIC.Pattern.ModifierSubtype == "BOPBuildingModifier_TDIC"
        and elt.Modifier_TDIC.Pattern.CodeIdentifier != "BOPIRPMLiab_TDIC")
        and not elt.IsTaxOrSurcharge)
    var buildingCosts = this.where(\elt -> (elt.Coverage.CoverageCategory.CodeIdentifier == "BOPBuildingPropCov_TDIC"
        or elt.Coverage.CoverageCategory.CodeIdentifier == "BOPBuildingOtherCat"
        or (elt typeis BOPBuildingCost_TDIC and buildingCostTypes.contains(elt.BOPCostType_TDIC)))
        and not elt.IsTaxOrSurcharge)

    return buildingCosts.union(modifierCosts)
  }

  reified function byLiability_TDIC() : Set<T> {
    var modifierCosts = this.where(\elt -> (elt typeis BOPModifierCost_TDIC
        and elt.Modifier_TDIC.Pattern.ModifierSubtype == "BOPBuildingModifier_TDIC"
        and elt.Modifier_TDIC.Pattern.CodeIdentifier == "BOPIRPMLiab_TDIC")
        and not elt.IsTaxOrSurcharge)
    var liabilityCosts = this.where(\elt -> (elt.Coverage.CoverageCategory.CodeIdentifier == "BOPBuildingLiabCov_TDIC"
        or (elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_MULTILINELIABILITYDISCOUNT))
        and not elt.IsTaxOrSurcharge)

    return liabilityCosts.union(modifierCosts)
  }

  /**
   *
   */
  reified function getLocationStandardPremiums_TDIC(currency : Currency) : BOPCostWrapper_TDIC[] {
    var minPremiumCosts = this.where(\elt -> (elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == TC_BOPMINPREMIUMADJ))
    var propertyCosts = this.byProperty_TDIC()
    var liabCosts = this.byLiability_TDIC()
    var ordered = new ArrayList<BOPCostWrapper_TDIC>()

    ordered.add(new BOPCostWrapper_TDIC(10, null, DisplayKey.get("TDIC.Web.Policy.BOP.SubTotal.Property.Premium"), propertyCosts.AmountSum(currency), true))
    ordered.add(new BOPCostWrapper_TDIC(20, null, DisplayKey.get("TDIC.Web.Policy.BOP.SubTotal.Liability.Premium"), liabCosts.AmountSum(currency), true))
    ordered.add(new BOPCostWrapper_TDIC(30, null, DisplayKey.get("Web.Policy.BOP.Balance.To.Minimum.Premium"), minPremiumCosts.AmountSum(currency), true))

    return ordered.toTypedArray()
  }

}
