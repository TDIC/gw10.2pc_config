package gw.lob.gl

uses gw.api.locale.DisplayKey
uses gw.api.tree.RowTreeRootNode
uses gw.api.web.util.PCDateUtil
uses gw.lob.common.AbstractPolicyInfoValidation
uses gw.lob.common.AsyncQuoteCoverableThresholdByLine
uses gw.pl.persistence.core.Key
uses gw.plugin.diff.impl.GLDiffHelper
uses gw.policy.PolicyLineValidation
uses gw.rating.worksheet.treenode.WorksheetTreeNodeContainer
uses gw.rating.worksheet.treenode.WorksheetTreeNodeUtil
uses gw.validation.PCValidationContext
uses java.lang.Math
uses java.util.Date
uses java.util.HashSet
uses java.util.Set
uses java.util.ArrayList
uses java.lang.Iterable
uses entity.windowed.GLCostVersionList
uses gw.api.util.JurisdictionMappingUtil
uses java.math.BigDecimal
uses gw.api.domain.Schedule
uses gw.lob.gl.schedule.GLScheduleHelper
uses gw.api.policy.AbstractPolicyLineMethodsImpl
uses gw.rating.AbstractRatingEngine
uses gw.lob.gl.rating.GLSysTableRatingEngine
uses org.apache.commons.lang.WordUtils
uses tdic.pc.config.rating.pl.GLRatingEngine
uses tdic.web.pcf.helper.GLQuoteScreenHelper
uses tdic.web.pcf.helper.JobWizardHelper
uses tdic.web.pcf.helper.PolicyRenewalScreenHelper

uses java.util.Map
uses java.util.List

@Export
class GLPolicyLineMethods extends AbstractPolicyLineMethodsImpl {

  var _line : entity.GeneralLiabilityLine
  
  construct(line : entity.GeneralLiabilityLine) {
    super(line)
    _line = line
  }

  /**
   * Jeff Lin 11/01/2019 3:35 PM PST
   * GPC-1929: Reset the additional Cov's effective dates with Renewal Effective Date,
   * if those coverages are carried over throught the Renewal Process from Prior Policy Period.
   */
  override function onRenewalInitialize() {
    super.onRenewalInitialize()
    JobWizardHelper.updateRiskManagementDiscount(_line.Branch)
    PolicyRenewalScreenHelper.resetCarryOverGLAdditionalCovEffDates_TDIC(_line)
    PolicyRenewalScreenHelper.removeExpiredGLLineItems(_line as GLLine)
    PolicyRenewalScreenHelper.incrementEndorsementLTEffectiveDates(_line as GLLine)
    PolicyRenewalScreenHelper.carryOverCovForRenewalAndMigration(_line.Branch)
  }

  override property get CoveredStates() : Jurisdiction[] {
    var covStates = new HashSet<Jurisdiction>()
    if (_line.BaseState != null) {
      covStates.add(_line.BaseState)
    }
    for (exp in _line.Exposures) {
      if (exp.Location != null) {
        covStates.add(JurisdictionMappingUtil.getJurisdiction(exp.Location))
      }
    }
    return covStates?.toTypedArray()
  }

  override property get AllCoverages() : Coverage[] {
    return _line.GLLineCoverages
  }

  override property get AllExclusions() : Exclusion[] {
    return _line.GLLineExclusions
  }

  override property get AllConditions() : PolicyCondition[] {
    return _line.GLLineConditions
  }

  override property get AllCoverables() : Coverable[] {
    var list : ArrayList<Coverable> = {_line}
    return list.toTypedArray()
  }
  
  override property get AllModifiables() : Modifiable[] {
    var list : ArrayList<Modifiable> = {_line}
    return list.toTypedArray()
  }

  override property get AllSchedules() : Schedule[] {
    return _line.CoveragesConditionsAndExclusionsFromCoverable.whereTypeIs(Schedule)
  }

  /**
   * BrittoS 04/06/2020
   * GL is not auditable for TDIC
   */
  override property get Auditable() : boolean {
    return false
  }

  /**
   * All GLCosts across the term, in window mode.
   */
  override property get CostVLs() : Iterable<GLCostVersionList> {
    return _line.VersionList.GLCosts
  }
  
  override property get Transactions() : Set<Transaction> {
    return _line.GLTransactions.toSet()
  }
  
  override function initialize() {
    _line.createCoveragesConditionsAndExclusions()
    _line.syncModifiers()
    _line.ClaimsMadeOrigEffDate = _line.Branch.PeriodStart
    _line.RetroactiveDate = _line.Branch.PeriodStart
  }

  override function createPolicyLineValidation(validationContext : PCValidationContext) : PolicyLineValidation<entity.GeneralLiabilityLine> {
    return new GLLineValidation_TDIC(validationContext, _line)
  }

  /**
   * Indicates whether ChangeEditEffectiveDate (CEED) can be performed on this policy line.  If this returns
   * null or an empty string, that means the line thinks it's OK.   Otherwise, this method should returnan
   * error message indicating why ChangeEditEffectiveDate cannot be performed.
   * @return an error message if Edit Effective Date can't be changed, null or the empty String if it can
   */
  override function canSafelyCEED() : boolean {
    for (e in _line.Exposures) {
      if (e.BasedOn != null and (e.EventDates != e.BasedOn.EventDates)) {
        return false
      }
    }
    return true
  }
  
  override function preLocationDelete(location : PolicyLocation) { 
    for(exposure in getExistingOrFutureExposuresRelatedTo(location)) {
      if (exposure.EffectiveDate < location.SliceDate) {
        exposure.ExpirationDate = location.SliceDate
      } else {
        exposure.ExpirationDate = exposure.EffectiveDate  
      }
    }
  }

  override property get LocationsInUseOnOrAfterSliceDate() : Set<Key> {
    var pLocsReferencedByExposures = _line.VersionList.Exposures.allVersionsFlat<GLExposure>()
        .where(\exp -> exp.ExpirationDate > _line.SliceDate)*.Location*.FixedId

    var glScheduleHelper = new GLScheduleHelper()
    var pLocsReferencedBySchedules =  glScheduleHelper.getCurrentAndFutureScheduledItemsForPolicyLine(_line)
        .where(\item -> item.ExpirationDate > _line.SliceDate)*.PolicyLocation*.FixedId

    return pLocsReferencedByExposures
        .union(pLocsReferencedBySchedules)
        .union({_line.Branch.PrimaryLocation.FixedId})
  }

  private function getExistingOrFutureExposuresRelatedTo(location : PolicyLocation) : List<GLExposure> {
    return _line.VersionList.Exposures.flatMap(\vl -> vl.AllVersions)
              .where(\g -> g.Location.FixedId == location.FixedId and g.ExpirationDate > location.SliceDate)
  }
  
  override function getExistingOrFutureSchedulesRelatedTo(location : PolicyLocation) : List<ScheduledItem> {
    var glScheduleHelper = new GLScheduleHelper()
    return glScheduleHelper.getCurrentAndFutureScheduledItemsForPolicyLine(_line)
           .where(\item -> item.PolicyLocation.FixedId == location.FixedId and item.ExpirationDate > location.SliceDate)
  }

  override function prorateBasesFromCancellation() {
    _line.Exposures.each(\ exposure -> {
        if (exposure.IsBasisScalable) {
          prorate(exposure)
        }
      })
  }
  
  override function prePeriodStartChanged(newDate : Date) {
    if (PCDateUtil.equalsIgnoreTime(_line.ClaimsMadeOrigEffDate, _line.Branch.PeriodStart)) {
      _line.ClaimsMadeOrigEffDate = newDate
    }
    if (PCDateUtil.equalsIgnoreTime(_line.RetroactiveDate, _line.Branch.PeriodStart)) {
      _line.RetroactiveDate = newDate
    }
  }

  override function createPolicyLineDiffHelper(reason : DiffReason, policyLine : PolicyLine) : GLDiffHelper {
    return new GLDiffHelper(reason, this._line, policyLine as GeneralLiabilityLine)
  }

  override function doGetTIVForCoverage(cov : Coverage) : BigDecimal {
    if (cov.FixedId.Type == GeneralLiabilityCov.Type) {  
      return getGeneralLiabilityCovLimit(cov as GeneralLiabilityCov)
    }
    
    return BigDecimal.ZERO
  }
  
  override property get AllCurrentAndFutureScheduledItems() : List<ScheduledItem> {
    return new GLScheduleHelper().getCurrentAndFutureScheduledItemsForPolicyLine(_line)
  }

  override function canDateSliceOnPropertyChange(bean : KeyableBean) : boolean {
    if (bean typeis GLExposure) {
      // Apply property changes to the above objects in window mode
      return false
    }
    return true
  }

  override property get PolicyLocationCanBeRemovedWithoutRemovingAssociatedRisks() : boolean {
    return true
  }

  override property get SupportsNonSpecificLocations() : boolean {
    return true
  }

  override function createRatingEngine(method: RateMethod, parameters: Map<RateEngineParameter, Object>): AbstractRatingEngine<GLLine> {
    if (RateMethod.TC_SYSTABLE == method) {
      return new GLSysTableRatingEngine(_line as GLLine)
    }
    return new GLRatingEngine(_line as GLLine,  parameters[RateEngineParameter.TC_RATEBOOKSTATUS] as RateBookStatus)
  }

  override property get BaseStateRequired(): boolean {
    return true
  }

  override function shouldQuoteAsynchronously() : boolean {
    return _line.Branch.GLLine.Exposures.Count > AsyncQuoteCoverableThresholdByLine.GLExposureThreshold
  }

  override function createPolicyInfoValidation_TDIC (context : PCValidationContext) : AbstractPolicyInfoValidation {
    return null
  }

  override function setMultiLineDiscount_TDIC (value : boolean) : void {
    //TODO - implement me
  }

  override function getMultiLineDiscount_TDIC() : Map<String,boolean> {
    //TODO - implement me
    return {}
  }

  override property get SupportsRatingOverrides() : boolean {
    return true
  }

  /**
   * BrittoS 03/28/2020
   */
  override function getWorksheetRootNode(showConditionals : boolean) : RowTreeRootNode {
    var costs = _line.Costs.cast(GLCost).toSet()
    costs = costs.where(\c -> c.GeneralLiabilityLine.FixedId == _line.FixedId).toSet()
    var treeNodes : List<WorksheetTreeNodeContainer> = {}
    var lineContainer = new WorksheetTreeNodeContainer(_line.Branch.Offering.Description)
    lineContainer.ExpandByDefault = true

    //Policy Terms
    var terms = costs.getTermPremiums_TDIC()
    if(terms.HasElements) {
      var termContainer = new WorksheetTreeNodeContainer(WordUtils.capitalize(DisplayKey.get("TDIC.Web.Policy.GL.Header.Terms").toLowerCase()))
      termContainer.ExpandByDefault = true
      for(term in terms.orderByDescending(\c -> ((c typeis GLCovExposureCost)? c.GeneralLiabilityLine.EffectiveDate : ((c typeis GLHistoricalCost_TDIC) ? c.GLSplitParameters.EffDate : c )))) {
        var displayName = term.DisplayName
        if(term typeis GLCovExposureCost) {
          displayName = displayName.concat(" (${term.GeneralLiabilityLine.EffectiveDate} ,  ${term.GeneralLiabilityLine.ExpirationDate})")
        } else if(term typeis GLHistoricalCost_TDIC) {
          displayName = displayName.concat(" (${term.GLSplitParameters.EffDate} ,  ${term.GLSplitParameters.ExpDate})")
        }
        var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(term, showConditionals)
        if(costTreeNodes.HasElements) {
          var costContainer = new WorksheetTreeNodeContainer(displayName)
          costContainer.addChildren(costTreeNodes)
          termContainer.addChild(costContainer)
        }

      }
      lineContainer.addChild(termContainer)
    }

    //Standard Premiums
    var stdPremiums = costs.getStandardPremiums_TDIC()
    if(stdPremiums.HasElements) {
      var stdPremiumsContainer = new WorksheetTreeNodeContainer(DisplayKey.get("TDIC.Web.Policy.GL.Header.StandaradPremium"))
      stdPremiumsContainer.ExpandByDefault = true
      for(premium in stdPremiums) {
        var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(premium, showConditionals)
        if(costTreeNodes.HasElements) {
          var displayName = GLQuoteScreenHelper.getStandardPremiumDisplayName(premium)
          var costContainer = new WorksheetTreeNodeContainer(displayName)
          costContainer.addChildren(costTreeNodes)
          stdPremiumsContainer.addChild(costContainer)
        }
      }
      lineContainer.addChild(stdPremiumsContainer)
    }

    //Taxes & Surcharges
    var taxSurcharges = costs.getTaxSurchargePremiums_TDIC()
    if(taxSurcharges.HasElements) {
      var taxSurchargesContainer = new WorksheetTreeNodeContainer(DisplayKey.get("TDIC.Web.Policy.GL.Header.TaxesAndSurcharges"))
      taxSurchargesContainer.ExpandByDefault = true
      for(taxSurcharge in taxSurcharges) {
        var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(taxSurcharge, showConditionals)
        if(costTreeNodes.HasElements) {
          var displayName = taxSurcharge.DisplayName
          var costContainer = new WorksheetTreeNodeContainer(displayName)
          costContainer.addChildren(costTreeNodes)
          taxSurchargesContainer.addChild(costContainer)
        }

      }
      lineContainer.addChild(taxSurchargesContainer)
    }

    treeNodes.add(lineContainer)
    return WorksheetTreeNodeUtil.buildRootNode(treeNodes)
  }
  

  //
  // PRIVATE SUPPORT FUNCTIONS
  //
  private function getGeneralLiabilityCovLimit(cov : GeneralLiabilityCov) : BigDecimal {
    switch (cov.PatternCode) {
      case "GLEmpBenefitsLiabilityCov":
        return cov.GLLine.GLEmpBenefitsLiabilityCov.GLEmpBenefitsAggLimitTerm.Value
      case "GLCGLCov":
        return cov.GLLine.GLCGLCov.GLCGLOccLimitTerm.Value
      case "GLUndergroundResourceCov":
        return cov.GLLine.GLUndergroundResourceCov.GLUndergroundResourceLimitTerm.Value
      default:
        return 0
    }
  }

  private function prorate(exposure: GLExposure) {
    var start = exposure.EffectiveDate
    var effDays = start.differenceInDays(exposure.ExpirationDate) as double
    var cancDays = start.differenceInDays(_line.Branch.CancellationDate) as double
    var ratio = cancDays / effDays
    exposure.BasisAmount = Math.round((exposure.BasisAmount.doubleValue() * ratio)) as int
  }

}
