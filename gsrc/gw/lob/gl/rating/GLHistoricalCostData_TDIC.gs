package gw.lob.gl.rating

uses entity.windowed.GLCovCostVersionList
uses entity.windowed.GLHistoricalCost_TDICVersionList
uses entity.windowed.GeneralLiabilityCovVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.Key
uses gw.pl.persistence.core.effdate.EffDatedVersionList

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 9/25/2019
 * Time: 7:50 PM
 * To change this template use File | Settings | File Templates.
 */
class GLHistoricalCostData_TDIC extends GLCostData<GLHistoricalCost_TDIC> {

  var _ratingParam : Key as RatingParam
  var _costType : GLCostType_TDIC as CostType
  var _covID : Key

  construct(effDate : Date, expDate : Date, __state : Jurisdiction, covID : Key, ratingParam : Key, costType : GLCostType_TDIC) {
    super(effDate, expDate, __state, null, null)
    _ratingParam = ratingParam
    _costType = costType
    _covID = covID
  }

  construct(cost : GLHistoricalCost_TDIC) {
    super(cost)
    _ratingParam = cost.GLSplitParameters.FixedId
    _costType = cost.CostType
    _covID = cost.GeneralLiabilityCov.FixedId
  }

  construct(cost : GLHistoricalCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    _ratingParam = cost.GLSplitParameters.FixedId
    _costType = cost.CostType
    _covID = cost.GeneralLiabilityCov.FixedId
  }

  override function setSpecificFieldsOnCost(line : GeneralLiabilityLine, costEntity : GLHistoricalCost_TDIC) : void {
    super.setSpecificFieldsOnCost(line, costEntity)
    costEntity.setFieldValue("GeneralLiabilityCov", _covID)
    costEntity.setFieldValue("GLSplitParameters", _ratingParam)
    costEntity.CostType = _costType
  }

  override function getVersionedCosts(line : GeneralLiabilityLine) : List<EffDatedVersionList> {
    var glCostVLs = EffDatedUtil.createVersionList(line.Branch, _covID) as GeneralLiabilityCovVersionList//line.VersionList.GLCosts
    return glCostVLs.Costs.where(\costVL -> isMyCost(costVL))
  }

  private function isMyCost(costVL : GLCovCostVersionList) : boolean {
    var cost = costVL.AllVersions.first()
    return cost typeis GLHistoricalCost_TDIC
        and _ratingParam == cost.GLSplitParameters.FixedId
        and cost.CostType == _costType
  }

  override property get GLKeyValues() : List<Object> {
    return {_ratingParam, _costType, _covID}
  }

  override function toString() : String {
    return "splitParam: ${_ratingParam}"
        + "StartDate: ${EffectiveDate} EndDate: ${ExpirationDate}"

  }
}