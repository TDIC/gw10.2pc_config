package gw.lob.gl.rating

uses gw.lob.bop.rating.BOPCostData
uses gw.lob.common.util.BigDecimalRange
uses gw.rating.CostDataWithOverrideSupport
uses gw.rating.flow.domain.CalcRoutineCostData
uses gw.rating.flow.domain.CalcRoutineCostDataWithTermOverride

uses java.math.BigDecimal
uses java.math.RoundingMode

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 9/25/2019
 * Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
class GLCalcRoutineCostData_TDIC extends CalcRoutineCostDataWithTermOverride {
  construct(costData : CostDataWithOverrideSupport, mode : OverrideMode, defaultRoundingLevel : Integer, defaultRoundingMode : RoundingMode) {
    super(costData, mode , defaultRoundingLevel , defaultRoundingMode)
  }
  property set PreDiscountPrem_TDIC(preDiscountPrem : BigDecimal) {
    (_costData as GLCostData).PreDiscountPremium = preDiscountPrem
  }

  property get PreDiscountPrem_TDIC() : BigDecimal {
    return (_costData as GLCostData).PreDiscountPremium
  }

}