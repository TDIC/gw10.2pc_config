package gw.lob.gl.rating

uses entity.windowed.GLCovCostVersionList
uses entity.windowed.GeneralLiabilityCovVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.Key
uses gw.pl.persistence.core.effdate.EffDatedVersionList
uses typekey.Currency
uses entity.GeneralLiabilityCov

@Export
class GLCovExposureCostData extends GLCostData<GLCovExposureCost> {
  var _covID : Key
  var _exposureID : Key as ExposureKey_TDIC
  var _idx : int
  var _ereSliceDate : Date as ERESliceDate
  var _basisScalable : boolean
  var _glCostType : GLCostType_TDIC as GLCostType_TDIC
  var _glDiscount : GLHistoricalDiscount_TDIC as Discount_TDIC


  construct(effDate : Date, expDate : Date, __state : Jurisdiction, covID : Key, exposureID : Key,
            basisScalable : boolean, glCostType : GLCostType_TDIC, __subline : GLCostSubline, __splitType : GLCostSplitType, discount : GLHistoricalDiscount_TDIC) {
    super(effDate, expDate, __state, __subline, __splitType)
    assertKeyType(covID, GeneralLiabilityCov)
    assertKeyType(exposureID, GLExposure)
    init(covID, exposureID, basisScalable)
    _glCostType = glCostType
    _glDiscount = discount

  }

  construct(effDate : Date, expDate : Date, __state : Jurisdiction, covID : Key, exposureID : Key,
            basisScalable : boolean, glCostType : GLCostType_TDIC, __subline : GLCostSubline, __splitType : GLCostSplitType, discount : GLHistoricalDiscount_TDIC, idx : int, ereSliceDate : Date) {
    super(effDate, expDate, __state, __subline, __splitType)
    assertKeyType(covID, GeneralLiabilityCov)
    assertKeyType(exposureID, GLExposure)
    init(covID, exposureID, basisScalable)
    _glCostType = glCostType
    _glDiscount = discount
    _idx = idx
    _ereSliceDate = ereSliceDate

  }

  construct(effDate : Date, expDate : Date, __state : Jurisdiction, covID : Key, exposureID : Key,
            basisScalable : boolean, __subline : GLCostSubline, __splitType : GLCostSplitType) {
    super(effDate, expDate, __state, __subline, __splitType)
    assertKeyType(covID, GeneralLiabilityCov)
    assertKeyType(exposureID, GLExposure)
    init(covID, exposureID, basisScalable)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, __state : Jurisdiction,
            covID : Key, exposureID : Key, basisScalable : boolean, __subline : GLCostSubline, __splitType : GLCostSplitType) {
    super(effDate, expDate, c, rateCache, __state, __subline, __splitType)
    assertKeyType(covID, GeneralLiabilityCov)
    assertKeyType(exposureID, GLExposure)
    init(covID, exposureID, basisScalable)
  }

  construct(cost : GLCovExposureCost) {
    super(cost)
    init(cost.GeneralLiabilityCov.FixedId, cost.GLExposure.FixedId, cost.GLExposure.ClassCode.Basis.Auditable)
    _glCostType = cost.GLCostType
    _glDiscount = cost.Discount_TDIC
    _idx = cost.EREIndex_TDIC
    _ereSliceDate = cost.ERESliceDate_TDIC
  }

  construct(cost : GLCovExposureCost, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    init(cost.GeneralLiabilityCov.FixedId, cost.GLExposure.FixedId, cost.GLExposure.ClassCode.Basis.Auditable)
    _glCostType = cost.GLCostType
    _glDiscount = cost.Discount_TDIC
    _idx = cost.EREIndex_TDIC
    _ereSliceDate = cost.ERESliceDate_TDIC
  }

  private function init(covID : Key, exposureID : Key, basisScalable : boolean) {
    _covID = covID
    _exposureID = exposureID
    _basisScalable = basisScalable
    _idx = 0
  }

  override function setSpecificFieldsOnCost(line : GeneralLiabilityLine, costEntity : GLCovExposureCost) : void {
    super.setSpecificFieldsOnCost(line, costEntity)
    costEntity.setFieldValue("GeneralLiabilityCov", _covID)
    costEntity.setFieldValue("GLExposure", _exposureID)
    costEntity.setFieldValue("GLCostType", _glCostType)
    costEntity.setFieldValue("Discount_TDIC", _glDiscount)
    costEntity.EREIndex_TDIC = _idx
    costEntity.ERESliceDate_TDIC = _ereSliceDate
  }

  override function getVersionedCosts(line : GeneralLiabilityLine) : List<EffDatedVersionList> {
    var covVL = EffDatedUtil.createVersionList(line.Branch, _covID) as GeneralLiabilityCovVersionList
    return covVL.Costs.where(\costVL -> isCostVersionListForExposureSublineSplitType(costVL))
  }

  private function isCostVersionListForExposureSublineSplitType(costVL : GLCovCostVersionList) : boolean {
    var v1 = costVL.AllVersions.first()
    return v1 typeis GLCovExposureCost
        and v1.GLExposure.FixedId == _exposureID
        and v1.GLCostType == _glCostType
        and v1.Discount_TDIC == _glDiscount
        and v1.EREIndex_TDIC == _idx
  }

  override function toString() : String {
    return "Cov: ${_covID} Exp: ${_exposureID} "
        + "GLCostType: ${_glCostType} "
        + "StartDate: ${EffectiveDate} EndDate: ${ExpirationDate}"

  }

  override property get GLKeyValues() : List<Object> {
    return {_covID, _exposureID, _glCostType, _glDiscount, _exposureID, _idx}
  }
}
