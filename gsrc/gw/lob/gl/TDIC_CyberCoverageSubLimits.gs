package gw.lob.gl

uses gw.api.database.Query

uses java.math.BigDecimal

/**
 * Description :Class to set Cyber Coverage Sub Limits
 * Create User: ChitraK
 * Create Date: 9/23/2019
 * Create Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_CyberCoverageSubLimits {

function getCyberSubLimits(line:entity.GeneralLiabilityLine){
  if(line.GLCyberLiabCov_TDICExists) {
    var q = Query.make(CyberCoverageLimits_TDIC).select()
    var cyb50Limits = new HashMap<String,BigDecimal>()
    var cyb100Limits = new HashMap<String,BigDecimal>()
    var cyb250Limits = new HashMap<String,BigDecimal>()
      if (line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 50000  ) {
        for (cov in q.iterator()) {
          cyb50Limits.put(cov.CovTerm_TDIC, cov.Cyb50Limit_TDIC)
        }
        setCybSubLimits(cyb50Limits,line)
        return;
      }
     if (line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 100000  ) {
        for (cov in q.iterator()) {
          cyb100Limits.put(cov.CovTerm_TDIC, cov.Cyb100Limit_TDIC)
        }
        setCybSubLimits(cyb100Limits,line)
        return;
     }
     if (line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 250000  ) {
        for (cov in q.iterator()) {
          cyb250Limits.put(cov.CovTerm_TDIC, cov.Cyb250Limit_TDIC)
        }
        setCybSubLimits(cyb250Limits,line)
        return;
     }
  }
}

  function setCybSubLimits(values:HashMap<String,BigDecimal>,line:GeneralLiabilityLine){
    //Coverage1 Limits
    line.GLCybCov1_TDIC.GLCyb1Limit_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1Limit_TDICTerm.Pattern.Description))
    line.GLCybCov1_TDIC.GLCyb1ForITSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1ForITSL_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1LegReviewSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1LegReviewSL_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1RegFinesSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1RegFinesSL_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1PCIFinesSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1PCIFinesSL_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1DCRExpDed_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1DCRExpDed_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1PubRelSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1PubRelSL_TDICTerm.DisplayName))
    line.GLCybCov1_TDIC.GLCyb1NamedMalwareSL_TDICTerm.setValue(values.get(line.GLCybCov1_TDIC.GLCyb1NamedMalwareSL_TDICTerm.DisplayName))
    //Coverage2&3 Limits
    line.GLCybCov2_TDIC.GLCyb2Limit_TDICTerm.setValue(values.get(line.GLCybCov2_TDIC.GLCyb2Limit_TDICTerm.Pattern.Description))
    line.GLCybCov2_TDIC.GLCyb2LossBussSL_TDICTerm.setValue(values.get(line.GLCybCov2_TDIC.GLCyb2LossBussSL_TDICTerm.DisplayName))
    line.GLCybCov2_TDIC.GLCyb2ExtSL_TDICTerm.setValue(values.get(line.GLCybCov2_TDIC.GLCyb2ExtSL_TDICTerm.DisplayName))
    line.GLCybCov2_TDIC.GLCyb2CompAttack_TDICTerm.setValue(values.get(line.GLCybCov2_TDIC.GLCyb2CompAttack_TDICTerm.DisplayName))
    line.GLCybCov2_TDIC.GLCyb2PRSL_TDICTerm.setValue((values.get(line.GLCybCov2_TDIC.GLCyb2PRSL_TDICTerm.DisplayName)))
    //Coverage4 Limits
    line.GLCybCov4_TDIC.GLCyb4Limit_TDICTerm.setValue(values.get(line.GLCybCov4_TDIC.GLCyb4Limit_TDICTerm.Pattern.Description))
    line.GLCybCov4_TDIC.GLCyb4DataComp_TDICTerm.setValue(values.get(line.GLCybCov4_TDIC.GLCyb4DataComp_TDICTerm.DisplayName))
    line.GLCybCov4_TDIC.GLCyb4NamedMalwareSL_TDICTerm.setValue(values.get(line.GLCybCov4_TDIC.GLCyb4NamedMalwareSL_TDICTerm.DisplayName))
    //Coverage5 Limits
    line.GLCybCov5_TDIC.GLCyb5Limit_TDICTerm.setValue(values.get(line.GLCybCov5_TDIC.GLCyb5Limit_TDICTerm.Pattern.Description))
    line.GLCybCov5_TDIC.GLCyb5NSLDed_TDICTerm.setValue(values.get(line.GLCybCov5_TDIC.GLCyb5NSLDed_TDICTerm.DisplayName))
    //Coverage 6 Limits
    line.GLCybCov6_TDIC.GLCyb6Limit_TDICTerm.setValue(values.get(line.GLCybCov6_TDIC.GLCyb6Limit_TDICTerm.Pattern.Description))
    line.GLCybCov6_TDIC.GLCyb6ElecMedia_TDICTerm.setValue(values.get(line.GLCybCov6_TDIC.GLCyb6ElecMedia_TDICTerm.DisplayName))
  }

}