package gw.lob.gl.financials

@Export
class GLHistoricalCostMethodsImpl_TDIC extends GenericGLCostMethodsImpl<GLHistoricalCost_TDIC>
{
  construct( owner : GLHistoricalCost_TDIC)
  {
    super( owner )
  }

  override property get Coverage() : Coverage
  {
    return Cost.GeneralLiabilityCov
  }

  override property get OwningCoverable() : Coverable {
    return Cost.GeneralLiabilityLine
  }

  override property get State() : Jurisdiction {
    return Cost.GeneralLiabilityLine.BaseState
  }
}
