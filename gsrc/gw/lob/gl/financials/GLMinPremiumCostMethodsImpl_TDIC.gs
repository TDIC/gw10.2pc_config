package gw.lob.gl.financials

class GLMinPremiumCostMethodsImpl_TDIC extends GenericGLCostMethodsImpl<GLMinPremCost_TDIC> {
  construct( owner : GLMinPremCost_TDIC )
  {
    super( owner )
  }

  override property get OwningCoverable() : Coverable
  {
    return Cost.GeneralLiabilityLine
  }

  override property get State() : Jurisdiction
  {
    return Cost.GeneralLiabilityLine.BaseState
  }

}