package gw.lob.gl.financials

uses gw.api.util.JurisdictionMappingUtil

class GLAdditionalInsdSchedCostImpl_TDIC extends GenericGLCostMethodsImpl<GLAddnlInsdSchedCost_TDIC> {
  construct( owner : GLAddnlInsdSchedCost_TDIC ) {
    super( owner )
  }

  override property get State() : Jurisdiction {
    return JurisdictionMappingUtil.
        getJurisdiction (Cost.GLAdditionInsdScheds?.AdditionalInsured?.PolicyAddlInsured?.ContactDenorm?.PrimaryAddress)
  }


  override property get DisplayOrder() : int {
    return Cost.GeneralLiabilityLine.GLCosts.whereTypeIs(GLAddnlInsdSchedCost_TDIC).Count
  }
 }