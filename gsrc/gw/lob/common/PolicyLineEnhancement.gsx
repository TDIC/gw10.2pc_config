package gw.lob.common

uses gw.api.productmodel.Question
uses typekey.Job

enhancement PolicyLineEnhancement : entity.PolicyLine {

  function addNewAdditionalInsuredDetailOfContactType(contactType : ContactType, additionalInsuredType : AdditionalInsuredType) : PolicyAddlInsuredDetail {
    var policyAddlInsuredDetail = this.addNewAdditionalInsuredDetailOfContactType(contactType)

    if (additionalInsuredType != null) {
      policyAddlInsuredDetail.AdditionalInsuredType = additionalInsuredType
    }

    return policyAddlInsuredDetail
  }
  /**
   * create by: ChitraK
   * @description: Cyber UW Questions Visibility
   * @create time: 5:46 PM 12/18/2019
   * @return:
   */

  function cyberUWQuestionsVisible_TDIC() : Boolean{
    if((this as GeneralLiabilityLine).GLCyberLiabCov_TDICExists) {
      if ((((this as GeneralLiabilityLine).GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 250000) or
          ((this as GeneralLiabilityLine).GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 100000 && (this as GeneralLiabilityLine).BaseState != Jurisdiction.TC_ND))) {
        return true
      }
    }
    return false
  }

  /**
   * create by: ChitraK
   * @description: Cyber UW Questions Visibility
   * @create time: 5:46 PM 12/18/2019
   * @return:
   */

  function cyberUWQuestionsVisibleOther_TDIC() : Boolean{
    if((this as GeneralLiabilityLine).GLCyberLiabCov_TDICExists) {
      if ((this as GeneralLiabilityLine).GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value == 250000) {
        return true
      }
    }
    return false
  }

  /**
   * create by: SanjanaS
   * @description: method to set Availability of GL UW Questions that are dependant on the question
   * 'Were you licensed for the first time in the last 12 months?'
   * @create time: 9:00 PM 03/25/2020
   * @return: Boolean
   */
  function glUWQuestionsAvailability_TDIC(): boolean {
    var NWStates = {Jurisdiction.TC_WA,Jurisdiction.TC_OR,Jurisdiction.TC_ID,Jurisdiction.TC_MT, Jurisdiction.TC_TN}
    var isNWState = NWStates.contains(this.AssociatedPolicyPeriod?.BaseState.getCode())

    if(this.AssociatedPolicyPeriod?.isDIMSPolicy_TDIC  or isNWState)
    {
      var oldAnswer = this.getAnswerValue(this.Branch.Policy.Product.getQuestionSetByCodeIdentifier("GLUnderwriting").getQuestionByCodeIdentifier("LicensedFirstTimeInLast12Months_TDIC"))

      if (oldAnswer == null) {
        this.getAnswer(this.Branch?.Policy?.Product?.getQuestionSetByCodeIdentifier("GLUnderwriting").getQuestionByCodeIdentifier("LicensedFirstTimeInLast12Months_TDIC"))?.setBooleanAnswer(false)
      }
    }


    var licensedFirsttimeAnswer = (this as GeneralLiabilityLine).getAnswerFromGLUWQuestionIfQuestionVisible_TDIC("LicensedFirstTimeInLast12Months_TDIC")
    if(this.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and licensedFirsttimeAnswer.BooleanAnswer != null){
      return !(licensedFirsttimeAnswer.BooleanAnswer)
    }
    else if(this.Branch.Offering.CodeIdentifier == "PLOccurence_TDIC"){
      return true
    }

    return false
  }

  /**
   * create by: SanjanaS
   * @description: method to set Availability of GL UW Questions when
   * 'Were you licensed for the first time in the last 12 months?' is answered Yes
   * @create time: 5:00 PM 03/30/2020
   * @return: Boolean
   */
  function getUWQuestionsAvailabilityNewDentist_TDIC(): boolean {
    var licensedFirsttimeAnswer = (this as GeneralLiabilityLine).getAnswerFromGLUWQuestionIfQuestionVisible_TDIC("LicensedFirstTimeInLast12Months_TDIC")
    if(this.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and licensedFirsttimeAnswer.BooleanAnswer != null){
      return licensedFirsttimeAnswer.BooleanAnswer
    }
    return false
  }

  /**
   * create by: SanjanaS
   * @description: method to sync GL UW Questions
   * @create time: 9:00 PM 03/25/2020
   * @return: Boolean
   */
  function syncGLUWQuestions_TDIC(question: Question){
    (this as GeneralLiabilityLine).syncQuestions()
  }

  /**
   * create by: IndramaniP
   *
   * @description: method to check availability of EPL UW Questions
   * @create time: 9:00 PM 04/28/2020
   * @return: Boolean
   */
  function eplUWQuestionsAvailability_TDIC() : Boolean {
    var line = this as GeneralLiabilityLine
    var offering = line.Branch.Offering.CodeIdentifier
    /*if ((offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC")
        and line.Branch.BasedOn?.Status != typekey.PolicyPeriodStatus.TC_LEGACYCONVERSION and !(this.Branch.Job typeis Renewal)) {
      return true
    }*/
    if ((offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC") //and (this.Branch.Job typeis Renewal)
        ) {
      if (not(line.BasedOn.GLDentalEmpPracLiabCov_TDICExists) and line.GLDentalEmpPracLiabCov_TDICExists) {
        return true
      }
      if (line.BasedOn.GLDentalEmpPracLiabCov_TDICExists and line.GLDentalEmpPracLiabCov_TDICExists and
          (line.BasedOn.GLDentalEmpPracLiabCov_TDIC.GLDEPLLimit_TDICTerm.Value != line.GLDentalEmpPracLiabCov_TDIC.GLDEPLLimit_TDICTerm.Value or
          line.BasedOn.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value != line.GLDentalEmpPracLiabCov_TDIC.GLDEPLEmpNumber_TDICTerm.Value)) {
        return true
      }
    }
    return false
  }
}
