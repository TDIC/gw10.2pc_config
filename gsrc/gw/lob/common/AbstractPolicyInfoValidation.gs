package gw.lob.common

uses gw.validation.PCValidationBase
uses gw.validation.PCValidationContext

class AbstractPolicyInfoValidation <T extends PolicyLine> extends PCValidationBase {
  protected var _line : T;

  construct(valContext : PCValidationContext, line : T) {
    super(valContext);
    _line = line;
  }

  override protected function validateImpl() {
    Context.addToVisited(this, "validateImpl");
  }
}