package gw.lob.common.schedules

uses gw.api.productmodel.ClausePattern

enhancement ScheduleEnhancement : gw.api.domain.Schedule {

  property get isComplexSchedule() : boolean {
    return not isSimpleSchedule
  }

  property get isSimpleSchedule() : boolean {
    return this.ScheduledItemMultiPatterns == null or this.ScheduledItemMultiPatterns.IsEmpty
  }

  property get ScheduledItemPattern() : ClausePattern {
    if(this.ScheduledItemMultiPatterns.Count > 1) {
      throw "please use ScheduledItemMultiPatterns"
    }
    return this.ScheduledItemMultiPatterns?.first()
  }
}
