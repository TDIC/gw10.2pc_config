package gw.lob.common.schedules.impl

uses gw.api.domain.Clause
uses gw.api.domain.Schedule
uses gw.api.productmodel.ScheduleForeignKeyWithOptionLabelsPropertyInfo
uses gw.api.productmodel.SchedulePropertyInfo
uses gw.lang.reflect.IType

class ProxyScheduleForeignKeyWithOptionLabelsPropertyInfo extends ProxyScheduleForeignKeyPropertyInfo {

  private var _optionGroupLabelMethodExpression: String as readonly OptionGroupLabelMethodExpression
  private var _optionLabelMethodExpression: String as readonly OptionLabelMethodExpression

  construct(colName: String, colLabel: String,
            valRangeGetterClassName: String, isRequired: boolean, isIdentityColumn: boolean, priority: int,
            optionGroupLabelMethodExpression: String, optionLabelMethodExpression: String) {
    super(colName, colLabel, valRangeGetterClassName, isRequired, isIdentityColumn, priority)
    init(optionGroupLabelMethodExpression, optionLabelMethodExpression)
  }

  construct(scheduledItemType: IType, colName: String, colLabel: String,
            valRangeGetterClassName: String, isRequired: boolean, isIdentityColumn: boolean, priority: int,
            optionGroupLabelMethodExpression: String, optionLabelMethodExpression: String) {
    super(scheduledItemType, colName, colLabel, valRangeGetterClassName, isRequired, isIdentityColumn, priority)
    init(optionGroupLabelMethodExpression, optionLabelMethodExpression)
  }

  private function init(optionGroupLabelMethodExpression: String, optionLabelMethodExpression: String) {
    _optionGroupLabelMethodExpression = optionGroupLabelMethodExpression
    _optionLabelMethodExpression = optionLabelMethodExpression
  }

  override reified function toSchedulePropertyInfo<T extends Schedule & Clause>(owner: T): SchedulePropertyInfo {
    var valRangeGetter = newValueRangeGetterInstance(owner)
    if (ItemType != null) {
      return new ScheduleForeignKeyWithOptionLabelsPropertyInfo (ItemType, ColumnName, ColumnLabel, valRangeGetter, Required, IdentityColumn, Priority,
          OptionGroupLabelMethodExpression, OptionLabelMethodExpression)
    }
    return new ScheduleForeignKeyWithOptionLabelsPropertyInfo (ColumnName, ColumnLabel, valRangeGetter, Required, IdentityColumn, Priority,
        OptionGroupLabelMethodExpression, OptionLabelMethodExpression)
  }
}
