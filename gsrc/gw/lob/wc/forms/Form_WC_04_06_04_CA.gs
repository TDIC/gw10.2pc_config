package gw.lob.wc.forms

uses java.util.Set
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses typekey.Job

class Form_WC_04_06_04_CA  extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, specialCaseStates: Set<Jurisdiction>) : boolean {
    if (context.Period.Job.Subtype == Job.TC_SUBMISSION || context.Period.Job.Subtype == Job.TC_RENEWAL ) {
      return true
    } return false
  }

}