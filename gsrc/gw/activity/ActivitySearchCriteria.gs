package gw.activity

uses gw.api.database.IQueryBuilder
uses gw.api.locale.DisplayKey
uses gw.api.system.PLDependenciesGateway
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses java.util.Date
uses java.util.GregorianCalendar
uses java.util.Calendar
uses gw.api.database.Relop
uses gw.api.database.InOperation
uses gw.search.EntitySearchCriteria

@Export
class ActivitySearchCriteria extends EntitySearchCriteria<Activity> {
  
  var _policyNumber : String as PolicyNumber
  var _accountNumber : String as AccountNumber
  var _overdueNow : Boolean as OverdueNow
  var _activityStatus : ActivityStatus as SearchedActivityStatus
  var _priority : Priority as SearchedPriority
  var _assignedUser : User as SearchedAssignedUser 
  var _detainedReason : DetainedReason_TDIC as DetainedReason
  var _subject : String as Subject_TDIC
  var _createdFromDate : Date as CreatedFromDate_TDIC
  var _createdToDate : Date as CreatedToDate_TDIC
  var _completedFromDate : Date as CompletedFromDate_TDIC
  var _completedToDate : Date as CompletedToDate_TDIC
  var _dueFromDate : Date as DueFromDate_TDIC
  var _dueToDate : Date as DueToDate_TDIC
  var _dateAvailable : Boolean as DateAvailable_TDIC = true

  
  function makeQuery() : Query<Activity> {
    var query = Query.make(Activity)
    if (SearchedAssignedUser != null) {
      query.compare(Activity#AssignedUser,Equals , SearchedAssignedUser)
    }
    if (SearchedActivityStatus != null) {
      query.compare(Activity#Status, Equals, SearchedActivityStatus)
    }
    if (SearchedPriority != null) {
      query.compare(Activity#Priority, Equals, SearchedPriority)
    }
    if(OverdueNow  != null) {
      var operator = OverdueNow ? Relop.LessThan : Relop.GreaterThanOrEquals 
      query.compare(Activity#Status, Equals, ActivityStatus.TC_OPEN)
      query.compare(Activity#TargetDate, operator, getCurrentNormalizedDate())
    }
    if (AccountNumber != null && PolicyNumber != null) {
      // if user specified both account and policy number
      // first join Activity with PolicyPeriod via JobID  
      // join chain: Activity -> Job <- PolicyPeriod)
      var policyPeriodTable = query.join(Activity#Job).join(PolicyPeriod#Job)
      policyPeriodTable.compare(PolicyPeriod#PolicyNumber, Equals, PolicyNumber)
      // then join with Policy then Account table
      // join chain: PolicyPeriod -> Policy -> Account)
      policyPeriodTable.join(PolicyPeriod#Policy).join(Policy#Account).compare(Account#AccountNumber, Equals, AccountNumber)
    } else if (PolicyNumber != null) {
      // if user only specified policy number but not account number
      // Need to get both job and policy activities that are related to policy with specified policy number
      var periodQuery = Query.make(PolicyPeriod)
      periodQuery.compare(PolicyPeriod#PolicyNumber, Equals, PolicyNumber)
      var policyQuery = Query.make(Policy)
      policyQuery.subselect(Policy#ID, InOperation.CompareIn, periodQuery, PolicyPeriod#Policy)
      query.subselect(Activity#Policy, InOperation.CompareIn, policyQuery, Policy#ID)
    } else if (AccountNumber != null) {
      // if user only specified account number but not policy number
      // join Activity with Account on AccountID
      query.join(Activity#Account).compare(Account#AccountNumber, Equals, AccountNumber)
    }
    if (DetainedReason != null) {
      addContainsDetainedReasonRestrict_TDIC(query)
    }
    if(Subject_TDIC != null){
      query.contains(Activity#Subject, Subject_TDIC, true)
    }
    if(CreatedToDate_TDIC != null and CreatedFromDate_TDIC != null){
      query.compare(Activity#CreateTime, Relop.GreaterThanOrEquals, CreatedFromDate_TDIC)
      query.compare(Activity#CreateTime, Relop.LessThanOrEquals, CreatedToDate_TDIC)
    }
    if(CompletedFromDate_TDIC != null and CompletedToDate_TDIC != null){
      query.compare(Activity#CloseDate, Relop.GreaterThanOrEquals, CompletedFromDate_TDIC)
      query.compare(Activity#CloseDate, Relop.LessThanOrEquals, CompletedToDate_TDIC)
    }
    if(DueFromDate_TDIC != null and DueToDate_TDIC != null){
      query.compare(Activity#TargetDate, Relop.GreaterThanOrEquals, DueFromDate_TDIC)
      query.compare(Activity#TargetDate, Relop.LessThanOrEquals, DueToDate_TDIC)
    }
    return query
  }
  /*
   * create by: SureshB
   * @description: mehtod to put the contains restriction for Detained Reasons
   * @create time: 4:02 PM 11/14/2019
    * @param Query<Activity>
   * @return:
   */

  private function addContainsDetainedReasonRestrict_TDIC(qry : Query<Activity>) {
    qry.contains("Description",DetainedReason.DisplayName, true)
  }

  function getCurrentNormalizedDate() : Date {
    var cal = GregorianCalendar.getInstance()
    cal.setTime(PLDependenciesGateway.getSystemClock().DateTime)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    return cal.Time
  }
  
  
  
  override protected function doSearch() : IQueryBeanResult<Activity> {
    return makeQuery().select()  
  }

  override protected property get InvalidSearchCriteriaMessage() : String {
    return null
  }

  override protected property get MinimumSearchCriteriaMessage() : String {
    return null
  }
}
