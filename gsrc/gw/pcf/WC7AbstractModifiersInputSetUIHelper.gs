package gw.pcf

uses gw.entity.TypeKey
uses java.util.List

abstract class WC7AbstractModifiersInputSetUIHelper {

  abstract function filterTypekeyModifier(modifier : Modifier) : List<TypeKey>
  abstract function filterTypeKeyExperienceModifierStatus(modifier : WC7Modifier) : List<TypeKey>
  abstract function updateModifierDependencies(modifier : Modifier)

  protected function updateExperienceModStatus(modifier : Modifier){
    if(isSelected(modifier)){
      (modifier as WC7Modifier).ExperienceModifierStatus = WC7ExpModStatus.TC_FINAL
    }
  }

  protected  function isSelected(modifier : Modifier) : boolean {
    return modifier.DataType == TC_RATE and modifier.Eligible or modifier.TypeKeyModifier.HasContent
  }

}