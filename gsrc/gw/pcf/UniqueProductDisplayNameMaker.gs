package gw.pcf

uses gw.api.locale.DisplayKey

uses java.lang.IllegalArgumentException
uses java.util.Collection
uses java.util.Map
uses gw.api.productmodel.Product

class UniqueProductDisplayNameMaker {

  var _products : List<Product> as readonly Products
  static var availableProducts = {"WC7WorkersComp", "GeneralLiability", "BusinessOwners"}

  construct() {
    this(gw.api.productmodel.ProductLookup.getAll().where(\product -> availableProducts.contains(product.CodeIdentifier)))
  }

  internal construct(products : List<Product>) {  // internal accessiblity for testing
    _products = products
  }

  function make(product: Product) : String {
    var displayName = product.DisplayName
    var nMatches = _products.countWhere(\p -> p.DisplayName == displayName) // evaluate DisplayName each time make() is called on the off chance the locale changed
    if (nMatches == 0) {
      throw new IllegalArgumentException("'" + displayName + "' is not a product used during the creation of this class [" + _products*.DisplayName.join(",") + "]")
    } else if (nMatches == 1) {
      return displayName
    } else {
      return DisplayKey.get("Web.FormPatternSearch.ProductName", displayName, product.PublicID)
    }
  }
}