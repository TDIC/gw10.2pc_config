package gw.pcf.policysummary

uses entity.Job
uses gw.api.locale.DisplayKey
uses pcf.DiffPolicyPeriodsPopup
uses pcf.JobForward

/**
 * Provides support for the PolicySummary page.
 */
@Export
class PolicySummaryHelper {

  private var _policyPeriod : PolicyPeriod
  private var _asOfDate : Date

  construct(policyPeriod : PolicyPeriod, asOfDate : Date) {
    _policyPeriod = policyPeriod
    _asOfDate = asOfDate
  }

  function gotoWorkOrdersDiff(checkedValues : Job[], title : String) {
    var firstRev = checkedValues.minBy(\j -> j.LatestPeriod.EditEffectiveDate.Time + j.LatestPeriod.CreateTime.Time)
    var secondRev = checkedValues.maxBy(\j -> j.LatestPeriod.EditEffectiveDate.Time + j.LatestPeriod.CreateTime.Time)
    if (firstRev.SelectedVersion.PolicyTerm.CheckArchived) {
      JobForward.go(firstRev)
    }
    if (secondRev.SelectedVersion.PolicyTerm.CheckArchived) {
      JobForward.go(secondRev)
    }
    DiffPolicyPeriodsPopup.push(firstRev.LatestPeriod, secondRev.LatestPeriod, _policyPeriod, _asOfDate, title)
  }

  function gotoPolicyTransactionsDiff(checkedValues : PolicyPeriod[], title : String) {
    var firstRev = checkedValues.minBy(\p -> p.EditEffectiveDate.Time + p.CreateTime.Time)
    var secondRev = checkedValues.maxBy(\p -> p.EditEffectiveDate.Time + p.CreateTime.Time)
    if (firstRev.PolicyTerm.CheckArchived) {
      JobForward.go(firstRev.Job)
    }
    if (secondRev.PolicyTerm.CheckArchived) {
      JobForward.go(secondRev.Job)
    }
    DiffPolicyPeriodsPopup.push(firstRev, secondRev, _policyPeriod, _asOfDate, title)
  }

  property get PendingCancellationExists() : boolean {
    return _policyPeriod.Policy.OpenJobs
        .hasMatch(\thejob -> thejob.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_CANCELING)
  }

  /**
   * Has a side effect of displaying a warning message on the top of the page if the policy is pending cancellation.
   *
   * @deprecated Please use PendingCancellationExists instead to control the visibility of the AlertBar
   */
  @Deprecated(:value = "Use PendingCancellationExists instead", :version = "10.0.0")
  function isCancelling(policyPeriod : PolicyPeriod) : boolean {
    var cancelling = false
    for (thejob in policyPeriod.Policy.OpenJobs) {
      if (thejob.LatestPeriod.Status == typekey.PolicyPeriodStatus.TC_CANCELING) {
        gw.api.util.LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Web.PolicyFile.Summary.PendingCancellation"))
        cancelling = true
      }
    }
    return cancelling
  }

  /*
   * create by: SanjanaS
   * @description: method to set underwriting survey details
   * @create time: 5:00 PM 01/28/2020
   * @param period PolicyPeriod
   * @return: null
   */
  static function setUWSurveyDetails_TDIC(period : PolicyPeriod) {
    var jobs = period.Policy.Jobs.where(\job -> job.Subtype == typekey.Job.TC_SUBMISSION or job.Subtype == typekey.Job.TC_RENEWAL
        or job.Subtype == typekey.Job.TC_REINSTATEMENT)
    for (job in jobs) {
      if (job != null and job.Subtype == typekey.Job.TC_SUBMISSION and job.LatestPeriod.Status == PolicyPeriodStatus.TC_BOUND) {
        var uwSurveyDetailsSub = new UWSurveyDetails_TDIC()
        uwSurveyDetailsSub.TransactionNumber = job.JobNumber
        uwSurveyDetailsSub.Description = job.Subtype.DisplayName
        uwSurveyDetailsSub.Date = job.CloseDate
        uwSurveyDetailsSub.SurveyStatus = SurveyStatus_TDIC.TC_SURVEYNOTSENT
        uwSurveyDetailsSub.isRemovable = false
        if (uwSurveyDetailsSub.TransactionNumber != null) {
          period.Policy.addToUWSurveyDetails_TDIC(uwSurveyDetailsSub)
        }
      }
      if (job != null and job.Subtype == typekey.Job.TC_RENEWAL and job.LatestPeriod.Status == PolicyPeriodStatus.TC_BOUND) {
        var uwSurveyDetailsRen = new UWSurveyDetails_TDIC()
        uwSurveyDetailsRen.TransactionNumber = job.JobNumber
        uwSurveyDetailsRen.Description = job.Subtype.DisplayName
        uwSurveyDetailsRen.Date = job.CloseDate
        uwSurveyDetailsRen.SurveyStatus = SurveyStatus_TDIC.TC_SURVEYNOTSENT
        uwSurveyDetailsRen.isRemovable = false
        if (uwSurveyDetailsRen.TransactionNumber != null) {
          period.Policy.addToUWSurveyDetails_TDIC(uwSurveyDetailsRen)
        }
      }
      if (job != null and job.Subtype == typekey.Job.TC_REINSTATEMENT and job.LatestPeriod.Status == PolicyPeriodStatus.TC_BOUND) {
        var uwSurveyDetailsRei = new UWSurveyDetails_TDIC()
        uwSurveyDetailsRei.TransactionNumber = job.JobNumber
        uwSurveyDetailsRei.Description = job.Subtype.DisplayName
        uwSurveyDetailsRei.Date = job.CloseDate
        uwSurveyDetailsRei.SurveyStatus = SurveyStatus_TDIC.TC_SURVEYNOTSENT
        uwSurveyDetailsRei.isRemovable = false
        if (uwSurveyDetailsRei.TransactionNumber != null) {
          period.Policy.addToUWSurveyDetails_TDIC(uwSurveyDetailsRei)
        }
      }
      var annualReviewDocs = job.Policy.Documents.where(\doc -> doc.TemplateId_TDIC == "PLPCYRW")
      for (annualReviewDoc in annualReviewDocs) {
        var transactionLevel = getTransactionLevel(annualReviewDoc)
        if (annualReviewDoc != null and transactionLevel == job.Subtype.DisplayName) {
          var uwSurveyDetailsWithDoc = new UWSurveyDetails_TDIC()
          uwSurveyDetailsWithDoc.TransactionNumber = job.JobNumber
          uwSurveyDetailsWithDoc.Description = job.Subtype.DisplayName
          uwSurveyDetailsWithDoc.Date = annualReviewDoc.DateCreated
          uwSurveyDetailsWithDoc.SurveyStatus = SurveyStatus_TDIC.TC_SURVEYSENTNOEPLI
          uwSurveyDetailsWithDoc.isRemovable = true
          if (uwSurveyDetailsWithDoc.TransactionNumber != null) {
            period.Policy.addToUWSurveyDetails_TDIC(uwSurveyDetailsWithDoc)
          }
        }
      }
      var annualReviewWithEPLIDocs = job.Policy.Documents.where(\doc -> doc.TemplateId_TDIC == "PLPCYRWEPLI")
      for (annualReviewDoc in annualReviewWithEPLIDocs) {
        var transactionLevel = getTransactionLevel(annualReviewDoc)
        if (annualReviewDoc != null and transactionLevel == job.Subtype.DisplayName) {
          var uwSurveyDetailsWithEPLIDoc = new UWSurveyDetails_TDIC()
          uwSurveyDetailsWithEPLIDoc.TransactionNumber = job.JobNumber
          uwSurveyDetailsWithEPLIDoc.Description = job.Subtype.DisplayName
          uwSurveyDetailsWithEPLIDoc.Date = annualReviewDoc.DateCreated
          uwSurveyDetailsWithEPLIDoc.SurveyStatus = SurveyStatus_TDIC.TC_SURVEYSENTEPLI
          uwSurveyDetailsWithEPLIDoc.isRemovable = true
          if (uwSurveyDetailsWithEPLIDoc.TransactionNumber != null) {
            period.Policy.addToUWSurveyDetails_TDIC(uwSurveyDetailsWithEPLIDoc)
          }
        }
      }
    }
  }

  /*
   * create by: SanjanaS
   * @description: method to set transaction level
   * @create time: 5:00 PM 01/28/2020
   * @param doc Document
   * @return: String
   */
  static function getTransactionLevel(doc : Document) : String {
    if (doc.Level.toString().contains("SUB")) {
      return typekey.Job.TC_SUBMISSION.DisplayName
    } else if (doc.Level.toString().contains("REN")) {
      return typekey.Job.TC_RENEWAL.DisplayName
    } else if (doc.Level.toString().contains("REI")) {
      return typekey.Job.TC_REINSTATEMENT.DisplayName
    }
    return null
  }

  /**
   * create by: ChitraK
   *
   * @description: set Endorsement Details
   * @create time: 1:56 PM 1/27/2020
   * @return:
   */

  static function setEndorsementDetails_TDIC(period : PolicyPeriod) {
    //Status = Offered
    var newEndorsementDetailsOffer = new EndorsementDetails_TDIC()
    var ereOfferJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" &&
        (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBEROFFER.DisplayName ||
            elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EREPLOFFER.DisplayName)).first()
    if (ereOfferJob != null) {
      newEndorsementDetailsOffer.TransactionNumber = ereOfferJob.JobNumber
      newEndorsementDetailsOffer.Description = ereOfferJob.PolicyTransactionReason_TDIC
      newEndorsementDetailsOffer.EREStatus = EndorsementStatus_TDIC.TC_OFFERED
      newEndorsementDetailsOffer.Date = ereOfferJob.CloseDate
    }
    if (newEndorsementDetailsOffer.TransactionNumber != null)
      period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsOffer)
    //GPC-4287 Status = Offer Not Accepted for PL
    var newEndorsementDetailsOfferNA = new EndorsementDetails_TDIC()
    var ereOfferNAJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" && (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EREPLOFFERNA.DisplayName
        || elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBERNA.DisplayName)).first()

    if (ereOfferNAJob != null) {
      newEndorsementDetailsOfferNA.TransactionNumber = ereOfferNAJob.JobNumber
      newEndorsementDetailsOfferNA.EREStatus = EndorsementStatus_TDIC.TC_OFFERNA
      newEndorsementDetailsOfferNA.Date = ereOfferNAJob.CloseDate
    }
    if (ereOfferNAJob.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EREPLOFFERNA.DisplayName) {
      newEndorsementDetailsOfferNA.Description = ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM.DisplayName
    } else if (ereOfferNAJob.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBERNA.DisplayName) {
      newEndorsementDetailsOfferNA.Description = ChangeReasonType_TDIC.TC_ERECYBER.DisplayName
    }
    if (newEndorsementDetailsOfferNA.TransactionNumber != null)
      period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsOfferNA)

    //Status = Billed
    //GWPS-1709 - Fix to display historical ERE changes on extended reporting
    var erePLBilledJobs = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" and
        ((elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM.DisplayName and
            elt.LatestPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists and elt.LatestPeriod.GLLine.GLExtendedReportPeriodCov_TDIC.GLERPRated_TDICTerm.Value == Boolean.TRUE)
            or (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBER.DisplayName and elt.LatestPeriod.GLLine.GLCyvSupplementalERECov_TDICExists and
            elt.LatestPeriod.GLLine.GLCyvSupplementalERECov_TDIC.GLCybERERated_TDICTerm.Value == Boolean.TRUE)))
    erePLBilledJobs.each(\ereBilledJob -> {
      var newEndorsementDetailsBilled = new EndorsementDetails_TDIC()
      newEndorsementDetailsBilled.TransactionNumber = ereBilledJob.JobNumber
      newEndorsementDetailsBilled.Description = ereBilledJob.PolicyTransactionReason_TDIC
      newEndorsementDetailsBilled.EREStatus = EndorsementStatus_TDIC.TC_BILLED
      newEndorsementDetailsBilled.Date = ereBilledJob.CloseDate
      if (newEndorsementDetailsBilled.TransactionNumber != null)
        period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsBilled)
    })
    //Status = Payment Waived and documents generated
    var erePaymentWaivedJobs = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE and
        elt.DisplayStatus == "Bound" and elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM.DisplayName
        and elt.LatestPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists and elt.LatestPeriod.GLLine.GLExtendedReportPeriodCov_TDIC.GLERPRated_TDICTerm.Value == Boolean.FALSE)
    erePaymentWaivedJobs.each(\erePaymentWaived -> {
      var newEndorsementDetailsWaived = new EndorsementDetails_TDIC()
      var newEndorsmentDetailsWaivedDocGen = new EndorsementDetails_TDIC()
      newEndorsementDetailsWaived.TransactionNumber = erePaymentWaived.JobNumber
      newEndorsementDetailsWaived.Description = erePaymentWaived.PolicyTransactionReason_TDIC
      newEndorsementDetailsWaived.EREStatus = EndorsementStatus_TDIC.TC_PAYMENTWAIVED
      newEndorsementDetailsWaived.Date = erePaymentWaived.CloseDate

      newEndorsmentDetailsWaivedDocGen.TransactionNumber = erePaymentWaived.JobNumber
      newEndorsmentDetailsWaivedDocGen.Description = erePaymentWaived.PolicyTransactionReason_TDIC
      newEndorsmentDetailsWaivedDocGen.EREStatus = EndorsementStatus_TDIC.TC_DOCGEN
      newEndorsmentDetailsWaivedDocGen.Date = erePaymentWaived.CloseDate
      if (newEndorsementDetailsWaived.TransactionNumber != null) {
        period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsWaived)
      }
      if (newEndorsmentDetailsWaivedDocGen.TransactionNumber != null) {
        period.Policy.addToEndorsementDetails_TDIC(newEndorsmentDetailsWaivedDocGen)
      }
    })


    //GPC-4287  Status=NonpaidRescinded for PL and Cyber
    var newEndorsementDetailsRescinded = new EndorsementDetails_TDIC()
    var erePaymentRescinded = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" && (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EREPLRESCINDED.DisplayName
        || elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED.DisplayName)).orderBy(\elt -> elt.CreateTime).first()

    if (erePaymentRescinded != null) {
      newEndorsementDetailsRescinded.TransactionNumber = erePaymentRescinded.JobNumber
      newEndorsementDetailsRescinded.EREStatus = EndorsementStatus_TDIC.TC_NOTPAIDRESCINDED
      newEndorsementDetailsRescinded.Date = erePaymentRescinded.CloseDate
    }

    if (erePaymentRescinded.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_EREPLRESCINDED.DisplayName) {
      newEndorsementDetailsRescinded.Description = ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM.DisplayName
    } else if (erePaymentRescinded.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED.DisplayName) {
      newEndorsementDetailsRescinded.Description = ChangeReasonType_TDIC.TC_ERECYBER.DisplayName
    }
    if (newEndorsementDetailsRescinded.TransactionNumber != null)
      period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsRescinded)
    // Status - Waived
    var newEndorsementDetailsEPLIWaived = new EndorsementDetails_TDIC()
    var ereEPLIPaymentWaivedPCJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" && (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_CANCELEPLI.DisplayName)).first()
    var ereEPLICancelJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_CANCELLATION && elt.DisplayStatus == "Bound" && (elt.LatestPeriod.GLLine.GLDentalEmpPracLiabCov_TDICExists)).first()
    var ereEPLIRenewalJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_RENEWAL && elt.LatestPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED && elt.LatestPeriod.GLLine.GLDentalEmpPracLiabCov_TDICExists).first()

    if (ereEPLIPaymentWaivedPCJob != null) {
      newEndorsementDetailsEPLIWaived.TransactionNumber = ereEPLIPaymentWaivedPCJob.JobNumber
      newEndorsementDetailsEPLIWaived.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIWaived.EREStatus = EndorsementStatus_TDIC.TC_WAIVED
      newEndorsementDetailsEPLIWaived.Date = ereEPLIPaymentWaivedPCJob.CloseDate
    }
    //GPC-4272 change display from cancellation date to system date
    else if (ereEPLICancelJob != null) {
      newEndorsementDetailsEPLIWaived.TransactionNumber = ereEPLICancelJob.Policy.MostRecentPolicyNumber
      newEndorsementDetailsEPLIWaived.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIWaived.EREStatus = EndorsementStatus_TDIC.TC_WAIVED
      newEndorsementDetailsEPLIWaived.Date = ereEPLICancelJob.CloseDate
    } else if (ereEPLIRenewalJob != null) {
      newEndorsementDetailsEPLIWaived.TransactionNumber = ereEPLIRenewalJob.Policy.MostRecentPolicyNumber
      newEndorsementDetailsEPLIWaived.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIWaived.EREStatus = EndorsementStatus_TDIC.TC_WAIVED
      newEndorsementDetailsEPLIWaived.Date = ereEPLIRenewalJob.LatestPeriod.BasedOn.PeriodEnd
    }
    if (newEndorsementDetailsEPLIWaived.TransactionNumber != null)
      period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsEPLIWaived)
    //Status - Documents Generated
    var newEndorsementDetailsEPLIDocGen = new EndorsementDetails_TDIC()
    var ereEPLIPaymentDocGenPCJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_POLICYCHANGE && elt.DisplayStatus == "Bound" && (elt.PolicyTransactionReason_TDIC == ChangeReasonType_TDIC.TC_CANCELEPLI.DisplayName)).first()
    var ereEPLIDocGenCancelJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_CANCELLATION && elt.DisplayStatus == "Bound" && (elt.LatestPeriod.GLLine.GLDentalEmpPracLiabCov_TDICExists)).first()
    var ereEPLIDocGenRenewalJob = period.Policy.Jobs.where(\elt -> elt.Subtype == typekey.Job.TC_RENEWAL && elt.LatestPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED && elt.LatestPeriod.GLLine.GLDentalEmpPracLiabCov_TDICExists).first()

    if (ereEPLIPaymentWaivedPCJob != null) {
      newEndorsementDetailsEPLIDocGen.TransactionNumber = ereEPLIPaymentWaivedPCJob.JobNumber
      newEndorsementDetailsEPLIDocGen.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIDocGen.EREStatus = EndorsementStatus_TDIC.TC_DOCGEN
      newEndorsementDetailsEPLIDocGen.Date = ereEPLIPaymentDocGenPCJob.CloseDate
    } else if (ereEPLICancelJob != null) {
      newEndorsementDetailsEPLIDocGen.TransactionNumber = ereEPLICancelJob.Policy.MostRecentPolicyNumber
      newEndorsementDetailsEPLIDocGen.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIDocGen.EREStatus = EndorsementStatus_TDIC.TC_DOCGEN
      newEndorsementDetailsEPLIDocGen.Date = (ereEPLIDocGenCancelJob typeis Cancellation) ? ereEPLIDocGenCancelJob.CancelProcessDate : null
    } else if (ereEPLIRenewalJob != null) {
      newEndorsementDetailsEPLIDocGen.TransactionNumber = ereEPLIRenewalJob.Policy.MostRecentPolicyNumber
      newEndorsementDetailsEPLIDocGen.Description = "ERE-EPLI"
      newEndorsementDetailsEPLIDocGen.EREStatus = EndorsementStatus_TDIC.TC_DOCGEN
      newEndorsementDetailsEPLIDocGen.Date = ereEPLIDocGenRenewalJob.LatestPeriod.BasedOn.PeriodEnd
    }
    if (newEndorsementDetailsEPLIDocGen.TransactionNumber != null)
      period.Policy.addToEndorsementDetails_TDIC(newEndorsementDetailsEPLIDocGen)
    print("Count:" + period.Policy.EndorsementDetails_TDIC.Count)

  }


}
