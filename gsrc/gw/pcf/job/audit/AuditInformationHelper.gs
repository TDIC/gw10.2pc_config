package gw.pcf.job.audit

uses pcf.JobForward
uses pcf.api.Location

/**
 * Helper code to job/audit/AuditInformationScreen.pcf
 */
@Export
class AuditInformationHelper {
  var currentLocation: Location;
  var policyPeriod: PolicyPeriod

  construct(location: Location, period: PolicyPeriod) {
    currentLocation = location
    policyPeriod = period
  }

  function startAudit(info: AuditInformation) {
    /**
     * US463
     * Shane Sheridan 11/7/2014
     */
    //info.InitDate = java.util.Date.CurrentDate

    info.startAuditJob()
    info.Bundle.commit()
    currentLocation.commit()
  }

  function waiveAndCommit(info: AuditInformation) {
    currentLocation.startEditing()

/*    // Guidewire Support Case 00229295
    // Under some scenarios, the wrong term is waived.
    // Fix provided by Rohan Joshi from Guidewire Support

    //first get correct term information
    var termNumber = info.PolicyTerm.findTermNumber()

    //get the current bundle
    var bundle = gw.transaction.Transaction.getCurrent()

    //get the correct policyperiod
    var pp = PolicyPeriod.finder.findByPolicyNumberAndTerm(policyPeriod.PolicyNumber, termNumber).FirstResult

    //add the pp to the current bundle
    pp = bundle.add(pp)

    //pass this to markWaived
    info.markWaived(pp)

    currentLocation.commit()*/

    currentLocation.startEditing()
    info.markWaived(policyPeriod)
    currentLocation.commit()
  }

  function reviseAndGoToWiz(audit: Audit) {
    var periods = audit.getPeriods()
    // 12/31/2019, Britto S, fix for GPC-2741, no paymment plan was set on revised audit, need guidewire support to see why
    if(audit.PolicyPeriod.SelectedPaymentPlan == null) {
      audit.PolicyPeriod.SelectedPaymentPlan = audit.PolicyPeriod.BasedOn.SelectedPaymentPlan
    }
    var period = audit.revise()
    JobForward.go(period.Job as Audit, period)
  }

  function transactionSumReversal(audits: AuditInformation[]): gw.pl.currency.MonetaryAmount {
    return audits.where(\a -> a.ReversalDate != null).sum(policyPeriod.PreferredSettlementCurrency, \a -> a.Audit.PolicyPeriod.TransactionCostRPT)
  }

  function totalCostForLastNonAuditPolicyPeriod(auditInfo: AuditInformation): gw.pl.currency.MonetaryAmount {
    var basedOn = auditInfo.Audit.PolicyPeriod.BasedOn
    while (basedOn.Job typeis Audit) {
      basedOn = basedOn.BasedOn
    }
    return basedOn.TotalCostRPT
  }

  function reversePremiumReport(info: AuditInformation) {
    currentLocation.startEditing()
    info.Audit.reverse()
    policyPeriod.updateTrendAnalysisValues()
    currentLocation.commit()
  }

  function canViewAuditJob(info: AuditInformation): boolean {
    return perm.Audit.view
        and info.HasBeenStarted
        and not info.IsReversal
  }

  function canEditAudit(info: AuditInformation): boolean {
    return perm.Audit.reschedule /* can only edit dates, so this is the correct permission */
        and (not info.HasBeenStarted)
        and (not info.IsWaived)
  }

  function canReviseAudit(info: AuditInformation): boolean {
    return perm.Audit.revise
        and info.IsFinalAudit
        and info.IsRevisable
  }

  function canReverseAudit(info: AuditInformation): boolean {
    return perm.Audit.reverse
        and info.IsPremiumReport
        and info.IsComplete
        and (not info.HasBeenReversed)
        and (policyPeriod.CompletedNotReversedFinalAudits.Count == 0)
  }

  function canWaiveAudit(info: AuditInformation): boolean {
    return perm.Audit.waive
        and not info.IsWaived
        and not info.HasBeenStarted
        and policyPeriod.canBeWaived(info)
  }

  function canStartAudit(info: AuditInformation): boolean {
    return policyPeriod.Policy.canStartAudit(info.AuditPeriodStartDate) == null
        and info.IsScheduled
        and not info.IsComplete
  }

  /**
   * US463
   * Shane Sheridan 11/7/2014
   */
  function onAuditMethodActualChanged_TDIC (info: AuditInformation){
    if(info.getFieldValue("ActualAuditMethod") == typekey.AuditMethod.TC_PHYSICAL){
      info.PhysicalAuditFlagged_TDIC = java.util.Date.CurrentDate
    } else if(info.getFieldValue("ActualAuditMethod") != typekey.AuditMethod.TC_PHYSICAL and info.PhysicalAuditFlagged_TDIC != null){
      info.PhysicalAuditFlagged_TDIC = null
    }

    var auditorRequired = AuditMethod.TF_REQUIRESAUDITOR_TDIC.TypeKeys.contains(info.ActualAuditMethod);
    if (auditorRequired) {
      // Assign auditor will not make a change if the auditor is already assigned.
      info.Audit.assignAuditor();
    } else {
      info.Audit.unassignAuditor_TDIC();
    }
  }

  /**
   * US477
   * Shane Sheridan
   */
  function initFinalAuditOption_TDIC () : boolean{
    if(policyPeriod.WC7LineExists and policyPeriod.FinalAuditOption != typekey.FinalAuditOption.TC_RULES){
      policyPeriod.FinalAuditOption = typekey.FinalAuditOption.TC_RULES
    }

    return false
  }
}