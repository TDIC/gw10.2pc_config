/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package gw.plugin.preupdate.impl

uses java.lang.IllegalArgumentException
uses gw.contact.LastUpdateTimeUtil
uses gw.api.domain.account.AccountSyncable
uses gw.api.system.PCDependenciesGateway
uses gw.api.database.Query
uses com.tdic.util.misc.EmailUtil
uses gw.api.upgrade.Coercions

uses org.slf4j.LoggerFactory

@Export
class DemoPreUpdateImpl {

  static final var _instance : DemoPreUpdateImpl as readonly Instance = new DemoPreUpdateImpl()

  /**
   * Demo implementation of pre-update logic. Any exception will cause the bounding database
   * transaction to roll back, effectively vetoing the update.
   */
  function executePreUpdate(bean : Object) {
    if (bean == null) {
      throw new IllegalArgumentException("The executePreUpdate method cannot be called with a null argument")
    }
    if (!(bean typeis KeyableBean)) {
      throw new IllegalArgumentException("The executePreUpdate method must be passed an entity as an argument. An object of type " + bean.getClass() + " was passed in instead")
    }
    if (bean typeis Coverable) {
      bean.getPreferredCoverageCurrency()
    } else if (bean typeis Account) {
      handleAccountPreUpdate(bean)
    } else if (bean typeis AccountContactRole) {
      handleAccountContactRolePreUpdate(bean)
    } else if (bean typeis Contact) {
      handleContactPreUpdate(bean)
    } else if (bean typeis Address) {
      handleAddressPreUpdate(bean)
    } else if (bean typeis AccountContact) {
      handleAccountContactPreUpdate(bean)
    } else if (bean typeis Job) {
      handleJobPreUpdate(bean)
    } else if (bean typeis Activity) {
      handleActivityPreUpdate_TDIC(bean)
    } else if (bean typeis Note) {
      handleNotePreUpdate_TDIC(bean);
    } else if (bean typeis User) {
      handleUserPreUpdate_TDIC(bean);
    }
    if (bean typeis AccountSyncable) {
      bean.handlePreUpdate()
    }
  }

  private function handleAccountPreUpdate(account : Account) {
    for (assignment in account.getChangedArrayElements("RoleAssignments")) {
      account.cascadeAssignment(assignment as UserRoleAssignment)
    }
  }

  private function handleAccountContactRolePreUpdate(accountContactRole : AccountContactRole) {
    changeAccountHolder(accountContactRole)
  }

  private function changeAccountHolder(accountContactRole : AccountContactRole) {
    if (accountContactRole typeis AccountHolder) {
      // update the cached account summaries
      var accountList = PCDependenciesGateway.getAccountList()
      if (accountList != null) {
        accountList.updateAccountNameAfterAccountHolderChange(accountContactRole)
      }
    }
  }

  private function handleContactPreUpdate(contact : Contact) {
    changeAccountHolderName(contact)
    var changedFields = contact.getChangedFields()
    if (changedFields.Count == 1 and (changedFields.single()) == "AutoSync") {
      //Do not set the LastUpdateTime if the update is only for the AutoSync flag.  It always runs
      //at the end of a submission, and it blows away any back-dated LastUpdateDates from back-dated submissions.
    } else {
      setLastUpdateTime(contact)
    }
  }

  private function handleAddressPreUpdate(address : Address) {
    setLastUpdateTime(address)
  }

  private function handleAccountContactPreUpdate(accountContact : AccountContact) {
    setLastUpdateTime(accountContact)
  }

  private function setLastUpdateTime(entity : KeyableBean) {
    var lastUpdateTime = LastUpdateTimeUtil.calculateLastUpdateTime(Coercions.makeDateFrom(entity.getFieldValue("LastUpdateTime")), Coercions.makeDateFrom(entity.getFieldValue("TemporaryLastUpdateTime")))
    entity.setFieldValue("LastUpdateTime", lastUpdateTime)
    entity.setFieldValue("TemporaryLastUpdateTime", null)
  }

  private function changeAccountHolderName(contact : Contact) {
    if (contact.isFieldChanged("Name") or
          (contact typeis Person and (contact.isFieldChanged("FirstName") or contact.isFieldChanged("LastName")))) {
      // find the account contact that owns this contact
      var query = Query.make(AccountContact)
        .compare(AccountContact#Contact.PropertyInfo.Name, Equals, contact).select()

      // update the cached account summaries
      for (accountContact in query) {
        if (accountContact.hasRole(TC_ACCOUNTHOLDER)) {
          var accountList = PCDependenciesGateway.PCWebSession.AccountList
          if (accountList != null) {
            accountList.updateAccountNameAfterContactNameChange(contact, accountContact.Account)
          }
        }
      }
    }
  }

  private function handleJobPreUpdate(job : Job) {
    for (assignment in job.getChangedArrayElements("RoleAssignments")) {
      job.cascadeAssignment(assignment as UserRoleAssignment)
    }
  }


  private function cascadeJobAssignments(job : Job) {
    if (job.isArrayElementChanged("RoleAssignments")) {
      for (var assignment in job.getChangedArrayElements("RoleAssignments")) {
        job.cascadeAssignment(assignment as UserRoleAssignment)
      }
    }
  }



  private function handleActivityPreUpdate_TDIC(passedInActivity: Activity) {

    //GW-1167, GW-1953, GW-1959, GW-1698 Suppress the activities for expired migrated policies
    // Can only suppress this during automatic migration and when the CHSI txn is complete
    var blnSuppressThisActivityDuringMigration : boolean = false

    // locate activities that are owned by migrated txns
    if (passedInActivity.Job != null){// some activities are not related to Jobs
      if (passedInActivity.Job.MigratedTxnIsCompletedInCHSI_TDIC == true){ // was the txn completed in CHSI?
        if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_AUTOMATEDMIGRATION as String){ // are we in auto or manual migration mode?
          blnSuppressThisActivityDuringMigration = true
        }
      }
    }

    if (blnSuppressThisActivityDuringMigration == true){
      passedInActivity.remove()
      print ("SUPPRESSED ACTIVITY: " + passedInActivity.ActivityPattern.Code + " \t\t" + passedInActivity.ActivityPattern.DisplayName)
    } else {
      //20150122 TJ Talluto - DE59 - enable email for activities
      if ((passedInActivity.New or passedInActivity.isFieldChanged("AssignedUser")) and passedInActivity.AssignedUser.UserSettings.EmailOnActAssign == true){
        var tmpSubject = passedInActivity.Subject == null ? "[no subject]" : passedInActivity.Subject
        var tmpBody = passedInActivity.Description == null ? "[no description]" : passedInActivity.Description
        if (passedInActivity.AssignedUser != null and passedInActivity.AssignedUser.Contact.EmailAddress1 != null){
          EmailUtil.sendEmail(passedInActivity.AssignedUser.Contact.EmailAddress1, tmpSubject, tmpBody)
        }
      }
    }
  }

  protected function handleNotePreUpdate_TDIC (note : Note) {
    // BrianS - GW-2716 - Do not associate job/activity notes with the policy period.
    if (note.Activity != null and note.Job != null and note.PolicyPeriod != null) {
      note.PolicyPeriod = null;
    }
  }

  /**
   * Pre-update rules for User entity.
  */
  protected function handleUserPreUpdate_TDIC (user : User) : void {
    if (user.UserSettings == null) {
      // BrianS - Create user settings for newly imported users.
      LoggerFactory.getLogger("User").info ("Creating default UserSettings for " + user);
      user.createDefaultUserSettings_TDIC();
    }
  }

}
