package gw.plugin.bizrules.impl

uses gw.bizrules.DefaultBizRulesPlugin
uses gw.bizrules.IRuleAction
uses gw.bizrules.IRuleContextDefinition
uses gw.bizrules.PCBizRulesNavigationSupport
uses gw.bizrules.pcf.BizRulesPageNavigationSupport
uses gw.bizrules.provisioning.AddUWIssueRuleAction
uses gw.bizrules.provisioning.contexts.BusinessAutoDriversIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.BusinessAutoUWContextDefinition
uses gw.bizrules.provisioning.contexts.BusinessAutoVehiclesIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.BusinessOwnersBuildingsIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.BusinessOwnersLocationsIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.BusinessOwnersUWContextDefinition
uses gw.bizrules.provisioning.contexts.CommercialPropertyBuildingsIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.CommercialPropertyLocationsIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.CommercialPropertyUWContextDefinition
uses gw.bizrules.provisioning.contexts.GeneralLiabilityExposuresIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.GeneralLiabilityUWContextDefinition
uses gw.bizrules.provisioning.contexts.GenericUWRuleContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersCoveragePartIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersDwellingAnimalIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersDwellingHazardIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersDwellingIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersSchedPersPropertyItemUWContextDefinition
uses gw.bizrules.provisioning.contexts.HomeownersUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineAccountsReceivableIterativeUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineBuildingIterativeUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineContractorsEquipmentIterativeUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineIMSignIterativeUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineLocationIterativeUWContextDefinition
uses gw.bizrules.provisioning.contexts.InlandMarineUWContextDefinition
uses gw.bizrules.provisioning.contexts.PersonalAutoDriversIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.PersonalAutoUWContextDefinition
uses gw.bizrules.provisioning.contexts.PersonalAutoVehiclesIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.PolicyContextDefinitionLibrary
uses gw.bizrules.provisioning.contexts.WorkersCompCoveredEmployeeIterableUWContextDefinition
uses gw.bizrules.provisioning.contexts.WorkersCompUWContextDefinition
uses gw.lang.reflect.features.IMethodReference
uses gw.lang.reflect.features.IPropertyReference
uses pcf.CreateLookup
uses pcf.api.Location

@Export
class BizRulesPlugin extends DefaultBizRulesPlugin {
  private final var _uwContexts: IRuleContextDefinition[]

  construct() {
    _uwContexts = {
        new GenericUWRuleContextDefinition(),
        new PersonalAutoUWContextDefinition(),
        new PersonalAutoDriversIterableUWContextDefinition(),
        new PersonalAutoVehiclesIterableUWContextDefinition(),

        new CommercialPropertyUWContextDefinition(),
        new CommercialPropertyLocationsIterableUWContextDefinition(),
        new CommercialPropertyBuildingsIterableUWContextDefinition(),

        new BusinessAutoUWContextDefinition(),
        new BusinessAutoDriversIterableUWContextDefinition(),
        new BusinessAutoVehiclesIterableUWContextDefinition(),

        new GeneralLiabilityUWContextDefinition(),
        new GeneralLiabilityExposuresIterableUWContextDefinition(),

        new WorkersCompUWContextDefinition(),
        new WorkersCompCoveredEmployeeIterableUWContextDefinition(),

        new InlandMarineUWContextDefinition(),
        new InlandMarineLocationIterativeUWContextDefinition(),
        new InlandMarineBuildingIterativeUWContextDefinition(),
        new InlandMarineAccountsReceivableIterativeUWContextDefinition(),
        new InlandMarineIMSignIterativeUWContextDefinition(),
        new InlandMarineContractorsEquipmentIterativeUWContextDefinition(),

        new BusinessOwnersUWContextDefinition(),
        new BusinessOwnersLocationsIterableUWContextDefinition(),
        new BusinessOwnersBuildingsIterableUWContextDefinition(),

        new HomeownersUWContextDefinition(),
        new HomeownersCoveragePartIterableUWContextDefinition(),
        new HomeownersDwellingIterableUWContextDefinition(),
        new HomeownersDwellingHazardIterableUWContextDefinition(),
        new HomeownersDwellingAnimalIterableUWContextDefinition(),
        new HomeownersSchedPersPropertyItemUWContextDefinition()

    }
  }

  override property get TriggeringPointMap(): Map<TriggeringPointKey, IRuleContextDefinition[]> {
    return TriggeringPointKey.TF_UWRULECHECKINGSETFILTER.TypeKeys.mapToKeyAndValue(\t -> t, \t -> _uwContexts)
  }

  override property get RuleActions(): Set<IRuleAction> {
    return {new AddUWIssueRuleAction()}
  }

  override property get BlackListedProperties(): Set<IPropertyReference> {
    return {}
  }

  override property get WhiteListedMethods(): Map<IMethodReference, String> {
    return {
        Object#toString() -> "converts instance to readable text",
        PolicyContextDefinitionLibrary#hasGoodDriverDiscount(PolicyDriver) -> "determines whether a driver qualifies for GoodDriverDiscount",
        PolicyContextDefinitionLibrary#getAnswerForBOPBuildingQuestion_TDIC(entity.PolicyPeriod, java.lang.String) -> "gets the answer for parsed question ",
        PolicyContextDefinitionLibrary#isBOPBuildingOver30YearsOld_TDIC(PolicyPeriod) -> "determines whether the building is over 30 years old? ",
        PolicyContextDefinitionLibrary#getBopBuildingYearOfBuiltIfOver30YearsOld_TDIC(entity.PolicyPeriod) -> "gets the year built if the building is over 30 years old? ",
        PolicyContextDefinitionLibrary#isBOPBuildingUWQuestionVisible_TDIC(PolicyPeriod, String) -> "gets the visibility of the parsed quesiotn? ",
        PolicyContextDefinitionLibrary#getAnswerForBOPBuildingIntegerQuestion_TDIC(PolicyPeriod, String) -> "gets the answers for type Integer questions ",
        PolicyContextDefinitionLibrary#getAnswerForBOPBuildingDropdownQuestion_TDIC(PolicyPeriod, String) -> "gets the answers for type Choice/Dropdown questions ",
        PolicyContextDefinitionLibrary#getBOPBuildingOccupancyStatusIfCondTripleNetNot_TDIC(PolicyPeriod) -> "gets the occupency status of building if (condo,triple or not)",
        PolicyContextDefinitionLibrary#canDetainedClaimsUWIssueRasied_TDIC(entity.PolicyPeriod) -> "determines whether to create Detained Claims UW Issue",
        PolicyContextDefinitionLibrary#isIntegrationMultinLineDiscountMatched_TDIC(PolicyPeriod) -> "Determine that the current MultiLineDiscount of UI is not different from Integration's",
        PolicyContextDefinitionLibrary#isRiskManagementDiscountModified_TDIC(PolicyPeriod) -> "Determine that Risk Management Discount has been modified or not.",
        PolicyContextDefinitionLibrary#hasKnownIncidents_TDIC(PolicyPeriod) -> "Determine there exists Has any claim or allegation of malpractice or  any complaint, demand, dispute, injury, adverse treatment",
        PolicyContextDefinitionLibrary#isMultiOwnerDentalPracticeEntity_TDIC(PolicyPeriod) -> "Determine it is Multi Owner Dental Practice Entity",
        PolicyContextDefinitionLibrary#isDentalAssociation_TDIC(PolicyPeriod) -> "Determine it is Dental Association by ADA and state dental association or society",
        PolicyContextDefinitionLibrary#canDetainedTwoOrMoreEPLClaimsIn5Years_TDIC(PolicyPeriod) -> "Check if it can deatin the 2 or more EPL Claims in the five years",
        PolicyContextDefinitionLibrary#isAnestheticModalitySelectionCorrect(PolicyPeriod) -> "Check to determine the Anesthetic Modality Selection with Specialty Code and Class Code combination are correct or not",
        PolicyContextDefinitionLibrary#hasSameBOPPolicyExpDateForCYB(PolicyPeriod) -> "Check if there exists a BOP policy with the same primary name insured and expiration date for this CYB submission",
        PolicyContextDefinitionLibrary#isBusinessCapacityIsOther_TDIC(PolicyPeriod) -> "Check if In what capacity do you provide services? Other or mire than one options",
        PolicyContextDefinitionLibrary#hasHealthProblems_TDIC(PolicyPeriod) -> "Check if Do you have any personal health problems that could reasonably be expected to affect the care you provide patients or your ability to manage your practice? Yes",
        PolicyContextDefinitionLibrary#isPartTimeHoursLessThan20_TDIC(PolicyPeriod) -> "Check if the part time hours is less than 20 hours or not",
        PolicyContextDefinitionLibrary#isFulltimeMemberDentalSchoolFaculty_TDIC(PolicyPeriod) -> "Check if it's the full time member of a dental school faculty",
        PolicyContextDefinitionLibrary#bldgGrtrThan7Flr(PolicyPeriod) -> "Check if any building is having more than 7 Floors",
        PolicyContextDefinitionLibrary#bldgMoreThan30kSQFT(PolicyPeriod) -> "Check if any building is having more than 30000 SQFT",
        PolicyContextDefinitionLibrary#hasCompletedASpecialtyProgram_TDIC(PolicyPeriod) -> "Check if it completed a specialty program? Yes",
        PolicyContextDefinitionLibrary#hasCrimeActivities_TDIC(PolicyPeriod) -> "Check if 'Have you ever been convicted of a crime other than minor traffic violations or are you currently charged with a crime?' Yes",
        PolicyContextDefinitionLibrary#hasHoldDentalLicenceOtherStates_TDIC(PolicyPeriod) -> "Check if it it holds the dental licence in other states? Yes",
        PolicyContextDefinitionLibrary#isFulltimeStudentInDentalPostgraduateProgram_TDIC(PolicyPeriod) -> "Check if 'Are you a full-time student enrolled in an accredited dental postgraduate program?' Yes",
        PolicyContextDefinitionLibrary#isPerformSleepApnea_TDIC(PolicyPeriod) -> "Check if 'Do you perform sleep apnea/snoring therapy?' Yes and 'Do you treat only after a physician's referral?' No",
        PolicyContextDefinitionLibrary#hasPracticedWithoutPLliability_TDIC(PolicyPeriod) -> "Check if You ever practiced without professional liability insurance? ' or 'Are you now or have you ever practiced without professional liability insurance?",
        PolicyContextDefinitionLibrary#canDetainedTwoOrMoreCYBClaimsIn5Years_TDIC(PolicyPeriod) -> "Check if it can deatin the 2 or more CYB Claims in the five years",
        PolicyContextDefinitionLibrary#validateDateAndMonth_TDIC(PolicyPeriod) -> "Check if effective date and month of below discounts match effective date and month of Policy Term.",
        PolicyContextDefinitionLibrary#validateDetainedNewGraduate_TDIC(PolicyPeriod) -> "Check if renewal AS400 policy has New Dentist Program in the previous term and brought",
        PolicyContextDefinitionLibrary#reviewPNIMailingAddress_TDIC(PolicyPeriod) -> "Check If Primary Name Insured state and policy base state is different",
        PolicyContextDefinitionLibrary#isFirstLicensedYearNotEqualToPolicyEffYear(PolicyPeriod) -> "Check if the first dentist license year within the lat 12 months and its year is not equal to the policy year return true to trigger the UW Issue.",
        PolicyContextDefinitionLibrary#isAllOccupanciesOtherThanMedicalDental(PolicyPeriod) -> "To determine whether the building CP/LRP building of question, both questions, Ocupencies By Others and All Occupancies Medical/Dental Offices? are answered Yes",
        PolicyContextDefinitionLibrary#hasListTypesOccupanciesNonMedicalDental(PolicyPeriod) -> "determine whether the building CP/BOP building of question, 'List types of occupancies if other than medical/dental offices' has value",
        PolicyContextDefinitionLibrary#hasGapInPriorPolicies_TDIC(PolicyPeriod) -> "Check if there exists the 'Gap in Prior Coverage' UW Issue to block the Issuance",
        PolicyContextDefinitionLibrary#hasOverlapInPriorPolicies_TDIC(PolicyPeriod) -> "Check if there exists the 'Overlap in prior coverage' UW Issue to block the Issuance",
        PolicyContextDefinitionLibrary#hasILMineSubsidenceCounty_TDIC(PolicyPeriod) -> "Check if the Base state is IL and its county is the following ones",
        PolicyContextDefinitionLibrary#isAnestheticModalitySelected_TDIC(PolicyPeriod) -> "Check if any of Anesthetic Modality is selected",
        PolicyContextDefinitionLibrary#isProblematicProceduresSelected_TDIC(PolicyPeriod) -> "Check if any of Problamatic Procedures is selected",
        PolicyContextDefinitionLibrary#findLocationNumHavingMoreThan7Bldg(PolicyPeriod) -> "Get Locations for Buildings having more than 7 floors",
        PolicyContextDefinitionLibrary#findLocationNumHavingMoreSQFT(PolicyPeriod) -> "Get Locations for Buildings having more than 30000 SQFT",
        PolicyContextDefinitionLibrary#isContactNameLengExceedsLmt_TDIC(PolicyPeriod) -> "Check if the Primary Insured NAme or Mortgagee or Additional Insureds contact name lengthexceeds Limit"



    }
  }

  /**
   * @return join between platform and application blacklist methods
   */
  override property get BlackListedMethods(): Set<IMethodReference> {
    return _blackListedMethods
  }

  override property get LookupEnabled() : boolean {
    return true
  }

  override function getAppBizRulesPageNavigation(rule: Rule): BizRulesPageNavigationSupport {
    return PCBizRulesNavigationSupport.Instance
  }

}
