package gw.plugin.processing

uses tdic.pc.common.batch.enrole.EnroleInboundBatch_TDIC
uses tdic.pc.common.batch.enrole.EnroleOutboundBatch_TDIC
uses tdic.pc.common.batch.finance.reports.TDIC_FinancePremiumsReportBatch
uses tdic.pc.common.batch.multilinediscount.TDIC_MultiLinePolicyBatch
uses tdic.pc.common.batch.finance.TDIC_FinancePremiumsProcessBatch
uses tdic.pc.conversion.batch.PolicyConversionBatchProcessor
uses tdic.pc.conversion.batch.PolicyPayloadBatchProcessor
uses gw.integration.document.production.bulk.BulkSubmission
uses gw.processes.BatchProcess
uses gw.processes.PolicyRenewalClearCheckDate
uses gw.processes.SolrDataImportBatchProcess
uses tdic.pc.common.batch.activities.CompleteActivitiesBatch
uses tdic.pc.common.batch.as400.AS400InboundBatch_TDIC
uses tdic.pc.common.batch.pivotal.TDIC_FeedPivotalBatchProcess
uses tdic.pc.common.batch.finalaudit.TDIC_ProcessFinalAuditEstimationBatch
uses tdic.pc.common.batch.generalledger.TDIC_GLReportBatchProcess
uses tdic.pc.config.rating.ratebook.AutomaticRateBookLoader_TDIC
uses tdic.pc.config.rating.ratebook.ExportRateBooks
uses tdic.pc.integ.plugins.hpexstream.batch.TDIC_DocumentCreationBatch
uses tdic.pc.common.batch.multilinediscount.TDIC_ReviewMutlilineDiscountBatch
uses tdic.pc.integ.plugins.wcpols.TDIC_WCPolicySpecificationReport
uses tdic.pc.integ.plugins.hpexstream.batch.TDIC_AuditDocumentCreationBatch
uses tdic.pc.common.batch.unistat.TDIC_WCUnitStatInitialReport
uses tdic.pc.common.batch.unistat.TDIC_WCUnitStatSubsequentReport
uses tdic.pc.common.batch.membership.TDIC_ADAMembershipSyncBatch
uses tdic.pc.common.batch.earnedpremium.TDIC_EarnedPremiumReportBatch
uses tdic.pc.common.batch.contacts.TDIC_CreateCMContacts
uses tdic.pc.common.batch.officialids.UnencryptOfficialIDsBatch
uses tdic.pc.common.batch.updatechange.UpdateChangeReasonsBatch
uses tdic.pc.common.batch.as400.TDIC_AS400BatchProcess
uses tdic.pc.common.batch.lossratio.TDIC_LossRatioRecalculateBatch

@Export
class ProcessesPlugin implements IProcessesPlugin {

  construct() {
  }

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {
      case BatchProcessType.TC_POLICYRENEWALCLEARCHECKDATE:
        return new PolicyRenewalClearCheckDate()
      case BatchProcessType.TC_SOLRDATAIMPORT:
        return new SolrDataImportBatchProcess()
      case BatchProcessType.TC_BULKSUBMISSION:
        return new BulkSubmission()
    /**
     * US561: Policy Feed to Pivotal
     * 12/04/2014 Vicente--12/05/2017 Praneethk
     *
     * Feed Pivotal batch.
     */
      case BatchProcessType.TC_FEEDPIVOTALBATCH_TDIC:
          return new TDIC_FeedPivotalBatchProcess ()
        // DE43 - related to US463
        // Shane Sheridan 28/11/2014
      case BatchProcessType.TC_PROCESSFINALAUDITESTIMATION_TDIC:
          return new TDIC_ProcessFinalAuditEstimationBatch()

        /*
         * US627: PC GL report
         * 11/10/2014 Kesava Tavva
         */
      case BatchProcessType.TC_GLINTERFACE_TDIC:
          return new TDIC_GLReportBatchProcess(arguments)
    /**
     * US555: Document Production
     * shanem 12/09/2014
     */
      case BatchProcessType.TC_DOCUMENTCREATIONBATCH:
          return new TDIC_DocumentCreationBatch()
        // US966, robk
      case BatchProcessType.TC_REVIEWMULTILINEDISCOUNT_TDIC:
          return new TDIC_ReviewMutlilineDiscountBatch()
      /*
       * US562: Statistical reporting
       * 02/11/15 Sunnihith Bojedla
       */
      case BatchProcessType.TC_WCPOLSREPORTBATCH:
          return new TDIC_WCPolicySpecificationReport()

      case BatchProcessType.TC_WCSTATINITIALREPORTBATCH:
          return new TDIC_WCUnitStatInitialReport()

      case BatchProcessType.TC_WCSTATSUBSEQUENTREPORTBATCH:
          return new TDIC_WCUnitStatSubsequentReport()

      case BatchProcessType.TC_AUDITDOCUMENTCREATIONBATCH:
          return new TDIC_AuditDocumentCreationBatch()
      case BatchProcessType.TC_ADAMEMBERSHIPSYNC_TDIC:
          return new TDIC_ADAMembershipSyncBatch()
      case BatchProcessType.TC_EARNEDPREMIUMREPORT_TDIC:
          return new TDIC_EarnedPremiumReportBatch(arguments)
      case BatchProcessType.TC_CREATE_CM_CONTACTS_TDIC:
        return new TDIC_CreateCMContacts()
      case BatchProcessType.TC_UNENCRYPTOFFICIALIDS_TDIC:
        return new UnencryptOfficialIDsBatch();
      case BatchProcessType.TC_COMPLETEACTIVITIES_TDIC:
        return new CompleteActivitiesBatch();
      case BatchProcessType.TC_UPDATECHANGEREASONS_TDIC:
          return new UpdateChangeReasonsBatch();
      case BatchProcessType.TC_AS400BATCH_TDIC:
          return new TDIC_AS400BatchProcess();
      case BatchProcessType.TC_LOSSRATIORECALC_TDIC:
          return new TDIC_LossRatioRecalculateBatch();
      case BatchProcessType.TC_AUTOMATICRATEBOOKLOADER_TDIC:
          return new AutomaticRateBookLoader_TDIC()
      case BatchProcessType.TC_AS400INBOUNDBATCH_TDIC:
        return new AS400InboundBatch_TDIC()
      case BatchProcessType.TC_EXPORTRATEBOOKS_TDIC:
        return new ExportRateBooks()
      case BatchProcessType.TC_POLICYPAYLOAD_EXT:
        return new PolicyPayloadBatchProcessor()
      case BatchProcessType.TC_POLICYCONVERSION_EXT:
        return new PolicyConversionBatchProcessor()
      case BatchProcessType.TC_ENROLEOUTBOUNDBATCH_TDIC:
        return new EnroleOutboundBatch_TDIC()
      case BatchProcessType.TC_ENROLEINBOUNDBATCH_TDIC:
        return new EnroleInboundBatch_TDIC()
      case BatchProcessType.TC_MULTILINEPOLICYBATCH_EXT:
        return new TDIC_MultiLinePolicyBatch()
      case BatchProcessType.TC_FINANCEPREMIUMSPROCESSBATCH_TDIC:
        return new TDIC_FinancePremiumsProcessBatch(arguments)
      case BatchProcessType.TC_FINANCEPREMIUMSREPORTBATCH_TDIC:
        return new TDIC_FinancePremiumsReportBatch(arguments)
      default:
        return null
    }
  }
}
