package com.tdic.pc.common.base.dao


uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.QueryRunner
uses org.apache.commons.dbutils.ResultSetHandler
uses org.apache.commons.dbutils.handlers.ScalarHandler
uses org.apache.commons.lang.ArrayUtils
uses org.slf4j.Logger

uses java.math.BigDecimal
uses java.sql.Connection
uses java.sql.PreparedStatement

/**
 * JDBC DAO Framework class. This class responsible to provide function to execute sql queries. If you have any
 * any integration which needs to execute SQL queries, that integration DAO must extend this class.
 */
abstract class AbstractPrepStmtJdbcDAO extends AbstractJdbcDAO {
  protected function runQuery(aQuery: String, aParams: Object[], aHandler: ResultSetHandler, aLogger: Logger): Object {
    var myMethodName = "runQuery"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "With params: %s", ArrayUtils.toString(aParams))
    }
    var myConnection: Connection = null
    try {
      myConnection = getConnection(aLogger)
      return getQueryRunner.query(myConnection, aQuery, aHandler, aParams)
    } finally {
      DbUtils.rollbackAndCloseQuietly(myConnection)
    }
  }

  protected function runQuery(myConnection: Connection, aQuery: String, aParams: Object[], aHandler: ResultSetHandler,
                              aLogger: Logger): Object {
    var myMethodName = "runQuery"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "With params: %s", ArrayUtils.toString(aParams))
    }
    return getQueryRunner.query(myConnection, aQuery, aHandler, aParams)
  }

  protected function runQuery(myConnection: Connection, aQuery: String, aHandler: ResultSetHandler,
                              aLogger: Logger): Object {
    var myMethodName = "runQuery"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
    }
    return getQueryRunner.query(myConnection, aQuery, aHandler)
  }

  protected function runUpdate(aConnection: Connection, aQuery: String, aParams: Object[], aLogger: Logger): int {
    var myMethodName = "runUpdate"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "With params: %s", ArrayUtils.toString(aParams))
    }
    return getQueryRunner.update(aConnection, aQuery, aParams)
  }

  protected function runUpdate(aConnection: Connection, aQuery: String, aLogger: Logger): int {
    var myMethodName = "runUpdate"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
    }
    return getQueryRunner.update(aConnection, aQuery)
  }

  protected function runInsert(aConnection: Connection, aQuery: String, aParams: Object[], aLogger: Logger): BigDecimal {
    var myMethodName = "runInsert"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "With params: %s", ArrayUtils.toString(aParams))
    }
    return getQueryRunner.insert(aConnection, aQuery,new ScalarHandler<BigDecimal>(), aParams)
  }

  protected function runBatch(aConnection: Connection, aQuery: String, aParams: Object[][], aLogger: Logger): int[] {
    var myMethodName = "runBatch"
    var myClassName = AbstractPrepStmtJdbcDAO.Type.Name
    if (aLogger.DebugEnabled) {
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "Executing query: %s", aQuery)
      aLogger.debug(myClassName + ": " + myMethodName + ": ", "With params: %s", ArrayUtils.toString(aParams))
    }
    return getQueryRunner.batch(aConnection, aQuery, aParams)
  }

  protected property get getQueryRunner(): QueryRunner {
    return new QueryRunner()
  }

  /**
   * function which executes sql and returns list of result objects. Make sure sql has alias name same as
   * result set handler object members.
   */
  protected function runQueryUsingJDBC(sql: String, aParams: Object[], aHandler: ResultSetHandler,
                                       aLogger: Logger): Object {
    var connection: Connection = null
    var preparedStatement: PreparedStatement = null
    var updatedRows: int

    try {
      // Get Connection Object
      connection = getConnection(aLogger)

      // Create Prepared Statement
      preparedStatement = connection.prepareStatement(sql)
      if (aParams != null){
        var elementIndex: int = 1
        for (aParam in aParams) {
          preparedStatement.setObject(elementIndex, aParam)
          elementIndex = elementIndex + 1
        }
      }
      var resultSet = preparedStatement.executeQuery()

      // Convert Result Set to DTO
      if (resultSet != null) {
        return aHandler.handle(resultSet)
      } else {
        return null
      }
    } finally {
      DbUtils.close(preparedStatement)
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }
}
