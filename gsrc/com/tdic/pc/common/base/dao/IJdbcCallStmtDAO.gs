package com.tdic.pc.common.base.dao

uses com.tdic.util.properties.exception.PropertyNotFoundException
uses org.slf4j.Logger

uses java.sql.CallableStatement
uses java.sql.ResultSet
uses java.sql.SQLException

/**
 * JDBC DAO Framework interface for executing procedures.
 */
interface IJdbcCallStmtDAO<T> extends IJdbcDAO<T> {
  @Throws(SQLException, "SQL Exception occurred")
  @Throws(PropertyNotFoundException, "SQL Exception occurred")
  /**
   * executes the stored procedure.
   * @param aQueryString query
   * @param aLogger logger
   * @return store procedure execution results
   */
  function executeStoredProc(aQueryString: String, aLogger: Logger): Object

  /**
   * Sets the stored proc parameters.
   *
   * @param aStatement the a statement
   * @param aPropertiesUtil the a properties util
   * @param aLogger the a logger
   * @throws SQLException the sQL exception
   * @throws PropertyNotFoundException the property not found exception
   */
  function setStoredProcParameters(aStatement: CallableStatement, aLogger: Logger): void

  /**
   * Retrieve result set values.
   *
   * @param aResultSet the a result set
   * @param aPropertiesUtil the a properties util
   * @param aLogger the a logger
   * @return the object
   * @throws SQLException the sQL exception
   */
  function retrieveResultSetValues(aResultSet: ResultSet, aLogger: Logger): Object
}
