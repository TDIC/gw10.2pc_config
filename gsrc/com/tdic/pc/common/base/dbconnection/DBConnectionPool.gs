package com.tdic.pc.common.base.dbconnection

uses com.microsoft.sqlserver.jdbc.SQLServerDriver
uses org.apache.commons.dbcp2.BasicDataSource
uses org.apache.commons.lang.StringUtils
uses org.slf4j.Logger

uses javax.sql.DataSource
uses java.sql.DriverManager

/**
 * Class responsible for creating connection (integration database) pool and maintaining them.
 */
class DBConnectionPool {
  private static final var CLASS_NAME = DBConnectionPool.Type.Name
  public static final var DEFAULT_MAX_ACTIVE: int = 50
  public static final var DEFAULT_MAX_IDLE: int = 5
  public static final var DEFAULT_MIN_IDLE: int = 0
  private static final var ENV_LOCAL = "local"
  private static final var ENV_DEV = "dev"
  private static final var ENV_TST = "tst"
  private var prefix: String
  private var connectionId: String
  private var password: String
  private var driver: String
  var driverClass : String as DriverClass
  private var server: String
  private var port: String
  private var name: String
  private var osAuth: boolean
  private var autoCommit: boolean
  private var maxActive = DEFAULT_MAX_ACTIVE
  private var maxIdle = DEFAULT_MAX_IDLE
  private var minIdle = DEFAULT_MIN_IDLE

  private var _dataSource: DataSource
  private var _aLogger: Logger

  construct(aPrefix: String, log: Logger) {
    assert StringUtils.isNotBlank(aPrefix);

    prefix = aPrefix;
    _aLogger = log
  }

  public property get getDataSource(): DataSource {
    var myMethodName = "getDataSource"
    _aLogger.debug(CLASS_NAME + ": " + myMethodName + ": " + "data source :" + _dataSource)
    if (_dataSource == null) {
      using (this as IMonitorLock) {
        if (_dataSource == null) {
          _aLogger.debug(CLASS_NAME + ": " + myMethodName + ": " + "creating datasource")
          createDataSource()
        }
      }
    }
    _aLogger.debug(CLASS_NAME + ": " + myMethodName + ": " + "returning get data source")
    return _dataSource
  }

  public property get getPrefix(): String {
    return prefix
  }

  public property get getConnectionId(): String {
    return connectionId
  }

  public property set setConnectionId(aConnectionId: String): void {
    connectionId = aConnectionId
  }

  public property  get getPassword(): String {
    return password
  }

  public property set setPassword(aPassword: String): void {
    password = aPassword
  }

  public property get getDriver(): String {
    return driver
  }

  public property set setDriver(aDriver: String): void {
    driver = aDriver
  }

  public property get getServer(): String {
    return server
  }

  public property set setServer(aServer: String): void {
    server = aServer
  }

  public property get getPort(): String {
    return port
  }

  public property set setPort(aPort: String): void {
    port = aPort
  }

  public property get getName(): String {
    return name
  }

  public property set setName(aName: String): void {
    name = aName
  }

  public property get isOSAuth(): boolean {
    return osAuth
  }

  public property set setOSAuth(aOSAuth: boolean): void {
    osAuth = aOSAuth
  }

  public property get isAutoCommit(): boolean {
    return autoCommit
  }

  public property set setAutoCommit(aAutoCommit: boolean): void {
    autoCommit = aAutoCommit
  }

  public property get getMaxActive(): int {
    return maxActive
  }

  public property set setMaxActive(aMaxActive: int): void {
    if (aMaxActive < 1) {
      aMaxActive = 1
    }
    maxActive = aMaxActive
  }

  public property get getMaxIdle(): int {
    return maxIdle
  }

  public property set setMaxIdle(aMaxIdle: int): void {
    maxIdle = aMaxIdle
  }

  public property get getMinIdle(): int {
    return minIdle
  }

  public property set setMinIdle(aMinIdle: int): void {
    if (aMinIdle < 0) {
      aMinIdle = 0
    }
    minIdle = aMinIdle
  }

  override public function toString(): String {
    return "DBConnection[prefix=" + prefix + ", connectionId=" + connectionId + ", driver=" + driver +
        ", server=" + server + ", port=" + port + ", name=" + name + ", osAuth=" + osAuth + ", autocommit=" +
        autoCommit + ", maxActive=" + maxActive + ", maxIdle=" + maxIdle + ", minIdle=" + minIdle + "]"
  }

  private function createDataSource(): void {
    var envName = gw.api.system.server.ServerUtil.getEnv()
    /*if(ENV_DEV.equalsIgnoreCase(envName) or ENV_TST.equalsIgnoreCase(envName)or ENV_LOCAL.equalsIgnoreCase(envName) or envName == null){*/
    _aLogger.debug("DBConnectionPool:createDataSource:local environment so looking up mem.properties")
    var myDataSource: BasicDataSource = new BasicDataSource()
    DriverManager.registerDriver(new SQLServerDriver())
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver())
    var myURL = ""
    var validationQuery = ""

    ///SQL SERVER CONNECTION STRING SETUP
    if(DriverClass == com.microsoft.sqlserver.jdbc.SQLServerDriver.DisplayName){
      myURL = driver + "://" + server + ":" + port + ";databaseName=" + name + ";user=" + connectionId + ";password=" + password
      validationQuery = "select 1"
    }else{
      //ORACLE CONNECTION STRING SETUP
      myURL = driver + ":" + "@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" + server + ")(PORT=" + port +
          "))(LOAD_BALANCE=YES)(CONNECT_DATA=(SERVICE_NAME=" + name + ")))"
      validationQuery = "select 1 from dual"
    }
    myDataSource.setDriverClassName(DriverClass)
    myDataSource.setUrl(myURL)
    myDataSource.setUsername(connectionId)
    myDataSource.setPassword(password)
    myDataSource.setValidationQuery(validationQuery)
    myDataSource.setDefaultAutoCommit(autoCommit)
    myDataSource.setMaxTotal(maxActive)
    myDataSource.setMaxIdle(maxIdle)
    myDataSource.setMinIdle(minIdle)
    myDataSource.setTestOnBorrow(true)
    myDataSource.setTestWhileIdle(true)
    _dataSource = myDataSource
    /*}else {
      var serverType = com.guidewire.pl.system.servlet.GuidewireStartupServlet.getInstance().ServletContext.ServerInfo
      _aLogger.debug("DBConnectionPool:createDataSource:environment is not local so looking up JNDI datasource " +
          "${serverType}")
      var context = new InitialContext()
      if(serverType.startsWith("Apache Tomcat")){
        _dataSource = context.lookup("java:/comp/env/jdbc/STuser") as DataSource
      }else{ //assuming it is jboss
        _dataSource = context.lookup("java:jboss/datasources/STuser") as DataSource
      }
    }*/
  }
}