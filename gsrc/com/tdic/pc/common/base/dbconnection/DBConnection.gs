package com.tdic.pc.common.base.dbconnection

uses com.tdic.util.properties.PropertyUtil
uses com.tdic.util.properties.exception.PropertyNotFoundException
uses org.apache.commons.lang.StringUtils
uses org.slf4j.Logger

uses javax.sql.DataSource
uses java.sql.Connection
uses java.sql.SQLException

/**
 * DB connection class. This class responsible to provide DB Connection to integration database.
 */
class DBConnection {
  private static final var CLASS_NAME = DBConnection.Type.Name


  private static var POOLS = new HashMap<String, DBConnectionPool>()
  private static var PROP_UTILS: PropertyUtil

  private construct() {
  }

  private construct(aLogger: Logger) {
    PROP_UTILS = PropertyUtil.getInstance(aLogger)
  }

  static function getConnection(aPrefix: String, aLogger: Logger): Connection {
    var myMethodName = "getConnection"
    PROP_UTILS = PropertyUtil.getInstance(aLogger)
    aLogger.debug(CLASS_NAME + " - " + myMethodName + ": " + "getting database connection")
    var myDataSource = getDataSource(aPrefix, aLogger)
    var connection =  myDataSource.getConnection()

    //setting autocommit to false, if it is true
    if(connection.AutoCommit == true){
      connection.AutoCommit = false
    }
    return connection
  }

  @Throws(SQLException, "SQL Exception Occured")
  private static function getDataSource(aPrefix: String, aLogger: Logger): DataSource {
    assert aLogger != null
    using (POOLS as IMonitorLock) {
      var myPool = POOLS.get(aPrefix)
      if (myPool == null) {
        myPool = createPool(aPrefix, aLogger)
      }
      return myPool.getDataSource
    }
  }

  private static function createPool(aPrefix: String, aLogger: Logger): DBConnectionPool {
    var myMethodName = "createPool"
    assert Thread.holdsLock(POOLS)
    assert aLogger != null
    var myPool = new DBConnectionPool(aPrefix, aLogger)
    setDBCredentials(myPool, aLogger)
    setDBProperties(myPool, aLogger)
    aLogger.debug(CLASS_NAME + " - " + myMethodName + ": " + "created pool: %s", myPool)
    POOLS.put(aPrefix, myPool)
    return myPool
  }

  private static function setDBCredentials(aPool: DBConnectionPool, aLogger: Logger): void {
    assert aPool != null
    assert aLogger != null
    aPool.setPassword = PROP_UTILS.getProperty(aPool.getPrefix + ".database.password")
    aPool.setConnectionId = PROP_UTILS.getProperty(aPool.getPrefix + ".database.username")
    aPool.DriverClass = PROP_UTILS.getProperty(aPool.getPrefix + ".database.class")
  }

  private static function setDBProperties(aPool: DBConnectionPool, aLogger: Logger): void {
    var myMethodName = "setDBProperties"
    assert aPool != null
    assert aLogger != null
    setDatabaseDriverProperty(aPool, aLogger, myMethodName)
    setDatabaseServerProperty(aPool, aLogger, myMethodName)
    setDatabaseNameProperty(aPool, aLogger, myMethodName)
  }

  private static function setDatabaseDriverProperty(aPool: DBConnectionPool, aLogger: Logger,
                                                    aMethodName: String): void {
    var myMethodName = "setDatabaseDriverProperty"
    assert aPool != null
    assert aLogger != null
    assert StringUtils.isNotBlank(aMethodName)
    var myPropertyName = aPool.getPrefix + ".database.driver"
    var myDriver: String = null

    try {
      myDriver = PROP_UTILS.getProperty(myPropertyName)
    } catch (myPropertyNotFoundException: PropertyNotFoundException) {
      aLogger.error(CLASS_NAME + " - " + myMethodName + ": " + "(myPropertyName=%s) property %s not found ",
          myPropertyNotFoundException)
    }
    aPool.setDriver = myDriver
  }

  private static function setDatabaseServerProperty(aPool: DBConnectionPool, aLogger: Logger,
                                                    aMethodName: String): void {
    var myMethodName = "setDatabaseServerProperty"
    assert aPool != null
    assert aLogger != null
    assert StringUtils.isNotBlank(aMethodName)
    var myServer: String = null
    var myPort: String = null
    try {
      myServer = PROP_UTILS.getProperty(aPool.getPrefix + ".database.host")
      myPort = PROP_UTILS.getProperty(aPool.getPrefix + ".database.port")
    } catch (myPropertyNotFoundException: PropertyNotFoundException) {
      aLogger.error(CLASS_NAME + " - " + myMethodName + ": " + "(myPropertyName=%s) property %s not found ",
          myPropertyNotFoundException)
    }
    aPool.setServer = myServer
    aPool.setPort = myPort
  }

  private static function setDatabaseNameProperty(aPool: DBConnectionPool, aLogger: Logger,
                                                  aMethodName: String): void {
    var myMethodName = "setDatabaseNameProperty"
    assert aPool != null
    assert aLogger != null
    assert StringUtils.isNotBlank(aMethodName)
    var myPropertyName = aPool.getPrefix + ".database.name"
    var myDatabaseName: String = null

    try {
      myDatabaseName = PROP_UTILS.getProperty(myPropertyName)
    } catch (myPropertyNotFoundException: PropertyNotFoundException) {
      aLogger.error(CLASS_NAME + " - " + myMethodName + ": " + "(myPropertyName=%s) property %s not found ",
          myPropertyNotFoundException)
    }
    aPool.setName = myDatabaseName
  }
}
