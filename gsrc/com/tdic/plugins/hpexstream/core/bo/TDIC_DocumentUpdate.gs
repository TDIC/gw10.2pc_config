package com.tdic.plugins.hpexstream.core.bo

uses gw.xml.ws.annotation.WsiExportable

/**
 * US645
 * 01/14/2015 shanem
 */
@WsiExportable
final class TDIC_DocumentUpdate {
  var docUrl: String as DocumentUrl
  var docPublicId: String as DocUID
}