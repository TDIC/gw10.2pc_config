package com.tdic.plugins.hpexstream.core.bo

class TDIC_TemplateIds {
  private var _templateIdsList: String[] as ListOfTemplates
  private var _templateMap: TDIC_TemplateidToDocPubIdMap[] as TemplateId
  construct(aTemplateIds: String[], aTemplateMap: TDIC_TemplateidToDocPubIdMap[]) {
    _templateIdsList = aTemplateIds
    _templateMap = aTemplateMap
  }
}