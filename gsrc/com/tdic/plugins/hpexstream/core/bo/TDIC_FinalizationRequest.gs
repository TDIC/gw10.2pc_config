package com.tdic.plugins.hpexstream.core.bo

class TDIC_FinalizationRequest {
  var _docId: String as DocId
  var _payload: String as Payload
  construct(docId: String, payload: String) {
    _docId = docId
    _payload = payload
  }
}