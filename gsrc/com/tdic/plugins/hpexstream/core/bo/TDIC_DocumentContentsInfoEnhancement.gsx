/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.bo

uses java.io.ByteArrayOutputStream

enhancement TDIC_DocumentContentsInfoEnhancement: gw.document.DocumentContentsInfo {
    property get ContentBytes() : byte[] {
      //return new java.io.File("C:\\GUIDEWIRE\\Accelarator\\Exstream\\Data Files\\Output.dlf").readBytes()
      if(this.InputStream == null) {
        return null
      } else {
        using(this.InputStream ) {
          var BUFFSIZE = 1024
          var out = new ByteArrayOutputStream(BUFFSIZE)
          var buff = new byte[BUFFSIZE]
          while (true) {
              var r = this.InputStream.read(buff)
              if (r == -1) {
                break
              }
              out.write(buff, 0, r)
          }
          return out.toByteArray()
        }
      }
    }
}
