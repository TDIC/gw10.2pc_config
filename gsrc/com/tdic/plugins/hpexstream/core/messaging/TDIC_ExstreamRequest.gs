/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.messaging

uses com.tdic.plugins.hpexstream.core.bo.TDIC_FinalizationRequest
uses gw.datatype.DataTypes
uses gw.plugin.messaging.MessageRequest
uses org.slf4j.LoggerFactory

class TDIC_ExstreamRequest implements MessageRequest {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  /**
   * US555
   * 10/13/2014 shanem
   */
  override function resume() {
    _logger.info("TDIC_ExstreamRequest#resume() - Entering")
    //Do nothing
    _logger.info("TDIC_ExstreamRequest#resume() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override property set DestinationID(aDestinationId: int) {
    _logger.debug("TDIC_ExstreamRequest#setDestinationID() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#setDestinationID() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function shutdown() {
    _logger.info("TDIC_ExstreamRequest#shutdown() - Entering")
    //Do nothing
    _logger.info("TDIC_ExstreamRequest#shutdown() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function suspend() {
    _logger.info("TDIC_ExstreamRequest#suspend() - Entering")
    //Do nothing
    _logger.info("TDIC_ExstreamRequest#suspend() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function afterSend(aMessage: Message) {
    _logger.debug("TDIC_ExstreamRequest#afterSend() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#afterSend() - Exiting")
  }

  /**
   * US555
   * 02/27/2015 shanem
   *
   * For Finalization request, we use the SenderRefId to store the Document PublicID.
   * We use a Finalization Request object to create the XML to store this information.
   */
  override function beforeSend(aMessage: Message): String {
    _logger.trace("TDIC_ExstreamRequest#beforeSend() - Entering")
    if (aMessage.SenderRefID == null && aMessage.EventName == "FinalizeExstreamDocument") {
      var refID = (aMessage.MessageRoot as Document).DocUID
      var refIDMaxLength =  DataTypes.get(Message.Type.TypeInfo.getProperty("SenderRefID")).asPersistentDataType().Length
      if(refID.HasContent && refID.length > refIDMaxLength){
        refID = refID.substring(0, refIDMaxLength)
      }
      aMessage.SenderRefID = refID
      var requestObject = new TDIC_FinalizationRequest((aMessage.MessageRoot as Document).DocUID, aMessage.Payload)
      var finalizationRequest = new com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest(requestObject)
      aMessage.Payload = finalizationRequest.asUTFString()
      _logger.debug("Finalization Payload: \n" + finalizationRequest.asUTFString())
      _logger.trace("TDIC_ExstreamRequest#beforeSend() - Exiting")
      return finalizationRequest.asUTFString()
    }
    _logger.trace("TDIC_ExstreamRequest#beforeSend() - Exiting")
    return aMessage.Payload
  }
}
