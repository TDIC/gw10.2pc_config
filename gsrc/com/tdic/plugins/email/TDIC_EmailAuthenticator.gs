package com.tdic.plugins.email

uses javax.mail.Authenticator
uses javax.mail.PasswordAuthentication


/**
 * US551
 * 09/18/2014 Rob Kelly
 *
 * TDIC Authenticator for Email Plugin.
 */
class TDIC_EmailAuthenticator extends Authenticator {

  private var _userName : String
  private var _password : String

  construct(userName : String, password : String) {
    _userName = userName
    _password = password
  }

  override protected property get PasswordAuthentication() : PasswordAuthentication {
    return new PasswordAuthentication(_userName, _password);
  }
}