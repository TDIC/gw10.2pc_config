package com.tdic.util.properties

uses com.tdic.util.properties.exception.LoadingPropertyFileException
uses com.tdic.util.properties.exception.PropertyNotFoundException
uses gw.api.util.ConfigAccess
uses gw.pl.logging.LoggerCategory
uses org.apache.commons.lang.ClassUtils
uses org.apache.commons.lang3.StringUtils
uses org.slf4j.Logger

uses java.io.FileInputStream
uses java.io.IOException
uses java.lang.Exception
uses java.lang.System
uses java.util.Properties
uses java.util.concurrent.locks.ReentrantLock
/**
 * Created by Souvik Kar 5/13/2019.
 */
class PropertyUtil extends Properties {
  public static var PROP_FILE_PATH_NAME: String = getEnvSpecificPropertiesFilePath()
  private static var uniqueInstance: PropertyUtil
  /**
   * Re-entrant Lock.
   */
  private static var LOCK = new ReentrantLock()
  private final var className = ClassUtils.getShortClassName(getClass())
  private var logger: Logger
  private var intProperties: Properties
  private var systemProperties: Properties

  /**
   * Construct a new PropertyUtil object
   */
  private construct() {
    var myMethodName = "PropertyUtil";
    logger = LoggerCategory.TDIC_INTEGRATION
    logger.info(className + ":" + myMethodName + ":Constructing a new PropertyUtil object")
    try {
      intProperties = new Properties()
      //print("property filepath: " + PROP_FILE_PATH_NAME)
      intProperties.load(new FileInputStream(PROP_FILE_PATH_NAME))
      systemProperties = new Properties();
      systemProperties = System.getProperties();
    } catch (e: IOException) {
      logger.error(className + ":" + myMethodName + ":" + e.getMessage())
      throw new LoadingPropertyFileException(PROP_FILE_PATH_NAME)
    }
  }

  /**
   * Instantiates a new property util.
   */
  @Param("Logger", "the Logger")
  @Throws(LoadingPropertyFileException, "The property file could not be loaded")
  private construct(alogger: Logger) {
    logger = alogger
    intProperties = new Properties()
    intProperties.load(new FileInputStream(PROP_FILE_PATH_NAME))
    systemProperties = new Properties();
    systemProperties = System.getProperties();
  }

  /**
   * Get an instance of the Property Util
   * @return
   */
  public static function getInstance(): PropertyUtil {
    if (uniqueInstance == null) {
      uniqueInstance = new PropertyUtil()
    }
    return uniqueInstance
  }

  /**
   * Gets the single instance of PropertyUtil.
   */
  @Param("Logger", "the Logger")
  @Returns("The PropertyUtil class instance")
  @Throws(LoadingPropertyFileException, "The class PropertyUtil could not be loaded")
  public static function getInstance(alogger: Logger): PropertyUtil {
    using (LOCK) {
      if (uniqueInstance == null) {
        uniqueInstance = new PropertyUtil(alogger)
      } else {
        uniqueInstance.logger = alogger
      }
    }
    return uniqueInstance;
  }

  /**
   * Get the String value of the property with the given key
   *
   * @param key
   * @return
   */
  public override function getProperty(key: String): String {
    var myMethodName = "getProperty"
    logger.debug(className + ":" + myMethodName + "Getting property value for key: " + key)
    var propertyValue: String = null
    var env = gw.api.system.server.ServerUtil.getEnv()
    try {
      if (StringUtils.isEmpty(env) or StringUtils.equalsIgnoreCase(env, "local")) {
        propertyValue = intProperties.getProperty(key)
      } else {
        propertyValue = intProperties.getProperty(env + "." + key)
        if (propertyValue == null) {
          propertyValue = intProperties.getProperty(key)
        }
      }
    }
    catch (e: Exception) {
      logger.error(className + ":" + myMethodName + ":KEY:" + key + ":" + e)
      throw new PropertyNotFoundException(key, e)
    }
    return propertyValue == null ? null : propertyValue.trim()
  }

  /**
   * Gets the property
   *
   * @param key
   * @param env
   * @return
   */
  public function getProperty(key: String, env: boolean): String {
    var myMethodName = "getProperty"
    logger.debug(className + " : " + myMethodName + " : Getting property value for key: " + key)
    var propertyValue: String = null
    if (env) {
      propertyValue = getProperty(key)
    } else {
      try {
        propertyValue = intProperties.getProperty(key)
      }
      catch (e: Exception) {
        logger.error(className + ":" + myMethodName + ":KEY:" + key + ":" + e)
        throw new PropertyNotFoundException(key, e)
      }
    }
    return propertyValue == null ? null : propertyValue.trim()
  }

  /**
   * Gets the System Property
   *
   * @param key
   * @return
   */
  public function getSystemProperty(key: String): String {
    var myMethodName = "getProperty"
    logger.debug(className + " : " + myMethodName + " : Getting property value for key: " + key)
    var propertyValue: String = null
    try {
      propertyValue = new String(systemProperties.getProperty(key, null))
    }
    catch (e: Exception) {
      logger.error(className + ":" + myMethodName + ":KEY:" + key + ":" + e)
      throw new PropertyNotFoundException(key, e)
    }
    return propertyValue == null ? null : propertyValue.trim()
  }

  /**
   * Gets the int properties.
   *
   * @return the int properties
   */
  public function getIntProperties(): Properties {
    return intProperties
  }

  /**
   * Sets the int properties.
   *
   * @param intProperties the new int properties
   */
  public function setIntProperties(intPropertiesIn: Properties): void {
    this.intProperties = intPropertiesIn
  }

  /**
   * Gets the system properties.
   *
   * @return the system properties
   */
  public function getSystemProperties(): Properties {
    return systemProperties
  }

  /**
   * Sets the system properties.
   *
   * @param systemProperties the new system properties
   */
  public function setSystemProperties(systemPropertiesIn: Properties): void {
    this.systemProperties = systemPropertiesIn
  }

  /**
   * Gets the environment specific properties file path to load properties
   *
   * @return
   */
  public static function getEnvSpecificPropertiesFilePath(): String {
    var propFilePath: String
    var fileLocation: String
    var _env = gw.api.system.server.ServerUtil.getEnv()
    var _app = gw.api.system.server.ServerUtil.getProduct().getProductCode()

    if (_env.toLowerCase() == "prod" or _env.toLowerCase() == "qa2") {
      return "D:/gwintprops/pc/${_env.toLowerCase()}-tdicintegrations.properties"
    }

    if (_env == null) {
      fileLocation = "gsrc/tdic/${_app.toLowerCase()}/integ/local-tdicintegrations.properties"
    } else {
      fileLocation = "gsrc/tdic/${_app.toLowerCase()}/integ/${_env.toLowerCase()}-tdicintegrations.properties"
    }
    propFilePath = ConfigAccess.getConfigFile(fileLocation).Path
    return propFilePath
  }

  public static function isSsoEnabled(): boolean {
    return PropertyUtil.getInstance().getProperty("authentication.sso.enabled")?.toBoolean()
  }


}