package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses java.util.Set
uses java.util.TreeSet
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

@ServerTest
@RunLevel(Runlevel.NONE)
class MethodProtectionTest extends TestBase {

  static var _testMethodsThatRan : Set<String> as TestMethodsThatRan = new TreeSet<String>()

  private function testPrivateTestMethodsDoNotRun() {
    TestMethodsThatRan.add("testPrivateTestMethodsDoNotRun")
  }

  protected function testProtectedTestMethodsDoNotRun() {
    TestMethodsThatRan.add("testProtectedTestMethodsDoNotRun")
  }

  function testMethodThatShouldRun() {
    TestMethodsThatRan.add("testMethodThatShouldRun")
  }

  private function thisMethodIsNeverCalled() {
    TestMethodsThatRan.add("thisMethodIsNeverCalled")
    if (false) {
      // This code suppresses the Guidewire Studio "This function is unused." warnings on the following methods:
      thisMethodIsNeverCalled()
      testPrivateTestMethodsDoNotRun()
    }
  }

}
