package tdic.util.gunit

uses gw.lang.reflect.IMethodInfo
uses gw.lang.reflect.IType
uses gw.testharness.TestBase
uses java.util.List

interface ITestActions {

  function begin()

  function end()

  function runTestsInClass(iType : IType)

  function runTestsInClass(iType : IType, testMethods : List<IMethodInfo>)

  function runTestMethod(iType : IType, method : IMethodInfo, isFirstMethodInClass : boolean, isLastMethodInClass : boolean)

  function invokeTestMethod(method : IMethodInfo, testInstance : TestBase)

}
