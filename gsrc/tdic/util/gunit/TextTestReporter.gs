package tdic.util.gunit

uses gw.lang.reflect.IMethodInfo
uses gw.lang.reflect.IType
uses java.lang.System
uses java.lang.Throwable
uses java.lang.Math
uses java.io.PrintWriter
uses java.io.StringWriter

class TextTestReporter implements ITestReporter {

  var _passingTests                    : int               as readonly PassingTests                    = 0
  var _failingTests                    : int               as readonly FailingTests                    = 0
  var _testClassesSkippedDueToRunlevel : int               as readonly TestClassesSkippedDueToRunlevel = 0
  var _runlevelAccessor                : IRunlevelAccessor
  var _startTime                                                                                       = System.currentTimeMillis()

  override property get TotalTests() : int {
    return PassingTests + FailingTests
  }

  construct(runlevelAccessor : IRunlevelAccessor) {
    _runlevelAccessor = runlevelAccessor
  }


  override function begin() {
    _startTime = System.currentTimeMillis()
  }

  override function end() {
    var endTime = System.currentTimeMillis()
    var duration = endTime - _startTime

    var seconds = duration / 1000.0
    if (seconds > 60)                              // If over a minute,
      seconds = Math.round(seconds)                //    Show only whole seconds.
    else if (seconds > 10)                         // If over 10 seconds (and under a minute),
      seconds = Math.round(seconds * 10) / 10.0    //    Show ONE digit to the right of the decimal -- like 10.3 or 59.2
    //                                             // Else...
    //                                             //    Milliseconds converted to seconds shows (up to) THREE digits to the right of the decimal.

    var minutes = seconds / 60.0                   // This division is prone to produce results with repeating decimal fractions, so...
    if (minutes > 5)                               // If it took over five minutes to run,
      minutes = Math.round(minutes)                //    then don't show any fractional amount.  (Show 5 minutes, not 5.2 minutes.)
    else if (minutes > 1)                          // One to Five minutes...
      minutes = Math.round(minutes * 10) / 10.0    //    shows one decimal place.  Ex: 1.2 or 4.6
    else                                           // Less than a minute...
      minutes = Math.round(minutes * 100) / 100.0  //    show two decimal places.  Ex:  0.00, 0.03, 0.13, 0.94

    print("Result: " + _passingTests + " passed, " + _failingTests + " failed, in " + seconds + " seconds = " + minutes + " minutes")
    if (_testClassesSkippedDueToRunlevel > 0) {
      print("  " + _testClassesSkippedDueToRunlevel + " test classes skipped because we are at run level <" + _runlevelAccessor.getCurrentServerRunlevel() + ">."
          + "  Connect Guidewire Studio to a running web server to run all tests.")
    }
    if (_failingTests > 0) {
      printFailLine()
    }
  }

  override function printFailLine() {
    print("====================================================================================================")
    print("  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  FAIL!  ")
    print("====================================================================================================")
  }

  override function skipClassDueToRunlevel(iType : IType) {
    _testClassesSkippedDueToRunlevel++
  }

  override function classHasNoTestMethods(iType : IType) {
    _failingTests++
    print(iType.DisplayName + " has no test methods.")
  }

  override function testMethodPassed(method : IMethodInfo) {
    _passingTests++
  }

  override function testMethodFailed(method : IMethodInfo, ex : Throwable) {
    _failingTests++

    var iType = method.OwnersType
    var methodName = method.DisplayName
    print("\n" + iType.DisplayName + "." + methodName + " failed:") 
    System.out.println("                 -----------  Stack Trace --------------")
    var sw = new StringWriter()
    var pw = new PrintWriter(sw)
    ex.printStackTrace(pw)
    print(sw)
    System.out.flush() 
    System.out.println("                --------------------------------------------------")
  }

}
