package tdic.util.gunit

uses gw.lang.reflect.IMethodInfo
uses gw.lang.reflect.IType
uses java.lang.Throwable

interface ITestReporter {

  property get PassingTests() : int
  property get FailingTests() : int
  property get TestClassesSkippedDueToRunlevel() : int
  property get TotalTests() : int

  function begin()

  function end()

  function printFailLine()

  function skipClassDueToRunlevel(iType : IType)

  function classHasNoTestMethods(iType : IType)

  function testMethodPassed(method : IMethodInfo)

  function testMethodFailed(method : IMethodInfo, ex : Throwable)

}
