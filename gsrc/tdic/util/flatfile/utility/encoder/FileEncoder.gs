package tdic.util.flatfile.utility.encoder

uses tdic.util.flatfile.model.gx.flatfilemodel.FlatFile

/**
 * US688
 * 05/09/2014 shanem
 *
 *  Encoder classes must extend this type to be returned from the FlatFileUtility class
 */
abstract class FileEncoder {

  /**
  *  US688
   * 05/09/2014 shanem
   *
   * Encode the file based on the first character of each line
   */
  abstract function encode(aFile: FlatFile, vendorDef: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile): String[]

  /**
  *  US688
   * 05/09/2014 shanem
   *
   * Writes resulting XML file to a file
   */
  abstract function writeToFile(aWritePath: String): void

}