package tdic.web.pcf.helper

uses gw.api.productmodel.Question

class GLQuestionSetUIHelper {

  static function checkOnChangeBlock(container: AnswerContainer, question: Question, originalOnChangeBlock: block()): block() {
    return \-> {
      if(originalOnChangeBlock != null) {
        originalOnChangeBlock()
      }
      validateQuestions(container, question)
    }
  }

  private static function validateQuestions(container: AnswerContainer, question : Question) {
    switch(question.CodeIdentifier) {
      case "NewGradDiscEffDate_TDIC" :
        container.setNewGradDiscountExpirationDate_TDIC()
        break
      case "DoYouPerformProvideBelowServices_TDIC" :
        container.setDoYouPerformProvideBelowServices_TDIC()
        break
      case "TreatPatientsUnderBelowAnestheticModalities_TDIC" :
        container.setTreatPatientsUnderBelowAnestheticModalities_TDIC()
        break
    }
  }

}