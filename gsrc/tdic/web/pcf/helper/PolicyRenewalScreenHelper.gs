package tdic.web.pcf.helper

uses gw.api.upgrade.PCCoercions
uses typekey.Job
uses gw.api.productmodel.ConditionPattern

class PolicyRenewalScreenHelper {

  public static function getNonWCRenewalDirections(policyPeriod: PolicyPeriod) : typekey.PreRenewalDirection[] {
    if (not policyPeriod.WC7LineExists) {
      // Jeff Lin, 12/16/2019 11:57 am, GPC-217/613: CP Renewal - Automatic and Manual Policy Renewal - Business Rules for Automatic Updates (Direction)
      if (policyPeriod.BOPLineExists) {
        return PreRenewalDirection.TF_UNDERWRITERONLY_TDIC.TypeKeys.toTypedArray()
      } else { // TODO: Based on the requirements of other Non-WC Lines
        return PreRenewalDirection.TF_UNDERWRITERONLY_TDIC.TypeKeys.toTypedArray()
      }
    }
    return PreRenewalDirection.getTypeKeys(false).toTypedArray()
  }

  public static function getSubjectSelections(policyPeriod: PolicyPeriod) : String[] {
    // Jeff Lin, 12/16/2019 11:57 am, GPC-217/613: CP Renewal - Automatic and Manual Policy Renewal - Business Rules for Automatic Updates (Subjects)
    var cp_BOP_Subjects     = {"Actual Cash Value Endorsement", "Claims History", "Other", "Underwriting Committee Review", "Underwriting Reasons", "Underwriting Survey Not Returned"}
    var cp_LRP_Subjects     = {"Claims History", "Other", "Underwriting Committee Review", "Underwriting Reasons", "Underwriting Survey Not Returned"}
    var cp_IL_Subjects      = "IL Membership Verification"
    var non_Membership_Subjet = "Non-Membership"

    var nonPLSubjects       = {"Claims History", "Schedule Rating", "Payroll / Employee Count", "Audit Information", "Interstate Operations", "Other Business", "Underwriting Reasons", "Other"}
    var pl_CYB_Subjects     = {"Claims History", "Underwriting Reasons", "Other"}
    var pl_CM_Subjects      = {"Coverage Extension", "Service Member Civil Relief Act"}
    var pl_IL_Subjects      = {"IL Membership Verification", "Non-Membership"}
    var pl_NJ_Subjects      = {"NJ Membership Verification", "Non-Membership"}
    var pl_CA_Subjects      = {"Non-Membership"}
    var pl_Common_Subjects  = {"Claims History", "Full-Time Faculty", "Full-Time Postgraduate", "Health Discount", "New Dentist", "Non-Licensed", "Other", "Part-Time Discount",
        "Risk Management Discount", "Underwriting Committee Review", "Underwriting Reasons", "Underwriting Survey Not Returned"}

    if (not policyPeriod.GLLineExists) {
      // Jeff Lin, 12/16/2019 11:57 am, GPC-217/613: CP Renewal - Automatic and Manual Policy Renewal - Business Rules for Automatic Updates (Subjects)
      if (policyPeriod.BOPLineExists) {
        if (policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
          if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
            cp_BOP_Subjects.add(non_Membership_Subjet)
          }
          if (policyPeriod.BaseState == Jurisdiction.TC_IL) {
            cp_BOP_Subjects.add(cp_IL_Subjects)
          }
          return cp_BOP_Subjects.toTypedArray()
        } else if (policyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
          if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
            cp_LRP_Subjects.add(non_Membership_Subjet)
          }
          if (policyPeriod.BaseState == Jurisdiction.TC_IL) {
            cp_LRP_Subjects.add(cp_IL_Subjects)
          }
          return cp_LRP_Subjects.toTypedArray()
        }
      }
      return nonPLSubjects.toTypedArray()
    }
    if (policyPeriod.GLLineExists) {
      if (policyPeriod.Offering != null and policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        return pl_CYB_Subjects.toTypedArray()
      }

      if (policyPeriod.Offering != null and policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        var results = pl_Common_Subjects.concat(pl_CM_Subjects) as java.util.ArrayList<String>
        switch(policyPeriod.BaseState){
          case TC_CA:
            results = results.concat(pl_CA_Subjects) as java.util.ArrayList<String>
            break
          case TC_IL:
            results = results.concat(pl_IL_Subjects) as java.util.ArrayList<String>
            break
          case TC_NJ:
            results = results.concat(pl_NJ_Subjects) as java.util.ArrayList<String>
            break
          default:
            break
        }
        return results.toTypedArray()
      }
      if(policyPeriod.Offering != null and policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC"){
        return pl_Common_Subjects.toTypedArray()
      }
    }
    return PreRenewalDirSubject_TDIC.AllTypeKeys*.DisplayName
  }

  // Jeff Lin, GPC-1931, 11/01/2019, Filtering Non-Renewal Reason Codes for PL Line
  public static function getFilteredNonRenewReasonCodes(policyPeriod: PolicyPeriod) :  typekey.NonRenewalCode[] {
    if (policyPeriod.GLLineExists) {
      if (policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC") {
        return NonRenewalCode.TF_NONRENEWCODEPL_CM_PL_OC.TypeKeys.toTypedArray()
      } else if (policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        return NonRenewalCode.TF_NONRENEWCODE_CYB.TypeKeys.toTypedArray()
      }
    } else if (policyPeriod.BOPLineExists) {
      return NonRenewalCode.TF_NONRENEW_BOP.TypeKeys.toTypedArray()
    }
    return typekey.NonRenewalCode.getTypeKeys(false).toTypedArray()
  }

  // Jeff, GPC-407, GPC-180
  public static function carryOverCovForRenewalAndMigration(period: PolicyPeriod) {
    if (period.GLLineExists and period.Job.Subtype != Job.TC_RENEWAL) {
      return
    }

    //BR-015 and BR-016
    if (period.BasedOn.GLLine.GLNewDentistDiscount_TDICExists) {
      period.GLLine.setConditionExists(period.GLLine.PolicyLine.Pattern.getConditionPatternByCodeIdentifier("GLNewGradDiscount_TDIC"), true)
      //period.GLLine.setConditionExists(PCCoercions.makeProductModel<ConditionPattern>("GLNewGradDiscount_TDIC"), true)
      period.TermType = TermType.TC_OTHER
      //period.setPeriodEnd(period.EditEffectiveDate.addMonths(12).FirstDayOfMonth)
      period.setPeriodEnd(period.PeriodStart.addMonths(12).FirstDayOfMonth)
    }
  }

  // Jeff, GPC-407, GPC-180
  /**
   * Jeff Lin 11/01/2019 3:35 PM PST
   * Jeff Lin 3/23/2020, 7:10 PM PST, for GPC-3785
   *
   * This is to rest the GL Line's Additioal coverages's effective dates to Renewal effective date
   * for those that have effective dates and expiration dates, during the carry over of the Renewal Process
   * after clone those addl coverages.
   */
  public static function resetCarryOverGLAdditionalCovEffDates_TDIC(_line: entity.GeneralLiabilityLine) {
    if (not (_line typeis GLLine and _line.Branch.Job.Subtype == typekey.Job.TC_RENEWAL)) {
      return
    }
    // Jeff Lin, 3/23/2020, 7:10 PM, GPC-3785: Manual & Automatic Policy Renewal Name on Door Endorsement did not expire as expected
    // ** Locum Tenens
    if (_line.GLLocumTenensCov_TDICExists and _line.GLLocumTenensSched_TDIC.HasElements) {
      if (_line.GLLocumTenensSched_TDIC.allMatch(\elt1 -> elt1.LTExpirationDate != null)) {
        var hasUnExpiredLocumTenens = _line.GLLocumTenensSched_TDIC.hasMatch(\elt1 -> elt1.LTExpirationDate.compareIgnoreTime(_line.EffectiveDate) > 0)
        if (not hasUnExpiredLocumTenens) {
          _line.setCoverageExists(_line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLLocumTenensCov_TDIC"), false)
        }
        _line.GLLocumTenensSched_TDIC.each(\locumSched -> {
          if(locumSched.LTExpirationDate.compareIgnoreTime(_line.EffectiveDate) <= 0){
            locumSched.remove()
          }
        })
      }
    }
    // ** Special Event
    if (_line.GLSpecialEventCov_TDICExists and _line.GLSpecialEventSched_TDIC.HasElements) {
      if (_line.GLSpecialEventSched_TDIC.allMatch(\elt1 -> elt1.EventEndDate != null)) {
        var hasUnExpiredSpecialEvent = _line.GLSpecialEventSched_TDIC.hasMatch(\elt1 -> elt1.EventEndDate.compareIgnoreTime(_line.EffectiveDate) > 0)
        if (not hasUnExpiredSpecialEvent) {
          _line.setCoverageExists(_line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLSpecialEventCov_TDIC"), false)
        }
        _line.GLSpecialEventSched_TDIC.each(\specialEventSched -> {
          if(specialEventSched.EventEndDate.compareIgnoreTime(_line.EffectiveDate) <= 0){
            specialEventSched.remove()
          }
        })
      }
    }
    // ** School Services:
    if (_line.GLSchoolServicesCov_TDICExists and _line.GLSchoolServSched_TDIC.HasElements) {
      if (_line.GLSchoolServSched_TDIC.allMatch(\elt1 -> elt1.EventEndDate != null)) {
        var hasUnExpiredSchoolServices = _line.GLSchoolServSched_TDIC.hasMatch(\elt1 -> elt1.EventEndDate.compareIgnoreTime(_line.EffectiveDate) > 0)
        if (not hasUnExpiredSchoolServices) {
          _line.setCoverageExists(_line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLSchoolServicesCov_TDIC"), false)
        }
        _line.GLSchoolServSched_TDIC.each(\schoolServiceSched -> {
          if(schoolServiceSched.EventEndDate.compareIgnoreTime(_line.EffectiveDate) <= 0){
            schoolServiceSched.remove()
          }
        })
      }
    }

    // ** Mobile Dental Clinic Endorsement
    // (Always Carry Over? Yes) ** Final Requirement: BR-011 "Mobile Dental Clinic Endorsement - Carry over if present"

    // Name on the Door Endorsement
    if (_line.GLNameOTDCov_TDICExists and _line.GLNameODSched_TDIC.HasElements) {
      if (_line.GLNameODSched_TDIC.allMatch(\elt1 -> elt1.LTExpirationDate != null)) {
        var hasUnExpiredNameOTD = _line.GLNameODSched_TDIC.hasMatch(\elt1 -> elt1.LTExpirationDate.compareIgnoreTime(_line.EffectiveDate) > 0)
        if(not hasUnExpiredNameOTD) {
          _line.setCoverageExists(_line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLNameOTDCov_TDIC"), false)
        }
        _line.GLNameODSched_TDIC.each(\nameODSched -> {
          if(nameODSched.LTExpirationDate.compareIgnoreTime(_line.EffectiveDate) <= 0){
            nameODSched.remove()
          }
        })
      }
    }
  }

  public static function setNonRenewedReasonAndRatedForExtendedCovEndorsement(polChgPeriod: PolicyPeriod, inForcePeriod: PolicyPeriod) {
    if (inForcePeriod.Renewal != null and inForcePeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) {
      if (inForcePeriod.Offering.Name == "Professional Liability - Claims Made") {
        var covEnabled = false
        if (not polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists) {
          covEnabled = true
          polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodCov_TDIC"), true)
        }
        if (polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists) {
          polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDIC.GLERPReason_TDICTerm.setValueFromString(inForcePeriod.Status.DisplayName)
          polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDIC.GLERPRated_TDICTerm.setValue(true)   // Default to true
        }
        //      if (covEnabled) {
        //        polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodCov_TDIC"), false)
        //      }
      }
      if (inForcePeriod.Offering.Name == "Cyber Suite Liability") {
        var covEnabled = false
        if (not polChgPeriod.GLLine.GLCyvSupplementalERECov_TDICExists) {
          covEnabled = true
          polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLCyvSupplementalERECov_TDIC"), true)
        }
        if (polChgPeriod.GLLine.GLCyvSupplementalERECov_TDICExists) {
          polChgPeriod.GLLine.GLCyvSupplementalERECov_TDIC.GLCybEREReason_TDICTerm.setValueFromString(inForcePeriod.Status.DisplayName)
          polChgPeriod.GLLine.GLCyvSupplementalERECov_TDIC.GLCybERERated_TDICTerm.setValue(true) // Always default to Yes
        }
        //      if (covEnabled) {
        //        polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLCyvSupplementalERECov_TDIC"), false)
        //      }
      }
    }
  }

  /**
   *
   */
  public static function removeExpiredGLLineItems(line : GLLine) {
    var renewalEffDate = line.Branch.EditEffectiveDate
    if(line.GLManuscriptEndor_TDICExists and line.GLManuscript_TDIC.HasElements){
      var glmanuScripts = line.GLManuscript_TDIC
      glmanuScripts.each(\manuScript -> {
        if (manuScript.ManuscriptExpirationDate != null and manuScript.ManuscriptExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          manuScript.remove()
        }
      })
      var hasUnexpiredManuscripts = line.GLManuscript_TDIC.hasMatch(\manuscript -> manuscript.ManuscriptExpirationDate == null or
          (manuscript.ManuscriptExpirationDate != null and manuscript.ManuscriptExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredManuscripts) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLManuscriptEndor_TDIC"), false)
      }
    }
    if(line.GLAdditionalInsuredCov_TDICExists and line.GLAdditionInsdSched_TDIC.HasElements){
      var glAddlInsureds = line.GLAdditionInsdSched_TDIC
      glAddlInsureds.each(\glAddlInsd -> {
        if(glAddlInsd.LTExpirationDate != null and glAddlInsd.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glAddlInsd.remove()
        }
      })
      var hasUnexpiredAddlInsds = line.GLAdditionInsdSched_TDIC.hasMatch(\addlInsd -> addlInsd.LTExpirationDate == null or
          (addlInsd.LTExpirationDate != null and addlInsd.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredAddlInsds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLAdditionalInsuredCov_TDIC"), false)
      }
    }
    if(line.GLMobileDentalClinicCov_TDICExists and line.GLMobileDCSched_TDIC.HasElements){
      var glMobileDCScheds = line.GLMobileDCSched_TDIC
      glMobileDCScheds.each(\glMobileDCSched -> {
        if(glMobileDCSched.LTExpirationDate != null and glMobileDCSched.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glMobileDCSched.remove()
        }
      })
      var hasUnexpiredMobileDCScheds = line.GLMobileDCSched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredMobileDCScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLMobileDentalClinicCov_TDIC"), false)
      }
    }
    if(line.GLNameOTDCov_TDICExists and line.GLNameODSched_TDIC.HasElements){
      var glNameODScheds = line.GLNameODSched_TDIC
      glNameODScheds.each(\glNameODSched -> {
        if(glNameODSched.LTExpirationDate != null and glNameODSched.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glNameODSched.remove()
        }
      })
      var hasUnexpiredNameODScheds = line.GLNameODSched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredNameODScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLNameOTDCov_TDIC"), false)
      }
    }
    if(line.GLStateExclCov_TDICExists and line.GLStateExclSched_TDIC.HasElements){
      var glStateExclScheds = line.GLStateExclSched_TDIC
      glStateExclScheds.each(\glStateExclSched -> {
        if(glStateExclSched.LTExpirationDate != null and glStateExclSched.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glStateExclSched.remove()
        }
      })
      var hasUnexpiredStateExclScheds = line.GLStateExclSched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredStateExclScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLStateExclCov_TDIC"), false)
      }
    }
    if(line.GLBLAICov_TDICExists and line.GLDentalBLAISched_TDIC.HasElements){
      var glDBLAddlInsds = line.GLDentalBLAISched_TDIC
      glDBLAddlInsds.each(\glDBLAddlInsd -> {
        if(glDBLAddlInsd.LTExpirationDate != null and glDBLAddlInsd.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glDBLAddlInsd.remove()
        }
      })
      var hasUnexpiredDentalBLAIScheds = line.GLDentalBLAISched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredDentalBLAIScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLBLAICov_TDIC"), false)
      }
    }
    if(line.GLCOICov_TDICExists and line.GLCertofInsSched_TDIC.HasElements){
      var glCertOfInsScheds = line.GLCertofInsSched_TDIC
      glCertOfInsScheds.each(\glCertOfInsSched -> {
        if(glCertOfInsSched.LTExpirationDate != null and glCertOfInsSched.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glCertOfInsSched.remove()
        }
      })
      var hasUnexpiredCOIScheds = line.GLCertofInsSched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredCOIScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLCOICov_TDIC"), false)
      }
    }
    //BR-022
    if(line.GLExcludedServiceCond_TDICExists and line.GLExcludedServiceSched_TDIC.HasElements){
      var glExcludedServiceSched = line.GLExcludedServiceSched_TDIC
      glExcludedServiceSched.each(\glExcServSched -> {
        if(glExcServSched.LTExpirationDate != null and glExcServSched.LTExpirationDate.compareIgnoreTime(renewalEffDate) <= 0){
          glExcServSched.remove()
        }
      })
      var hasUnexpiredESEScheds = line.GLExcludedServiceSched_TDIC.hasMatch(\sched -> sched.LTExpirationDate == null or
          (sched.LTExpirationDate != null and sched.LTExpirationDate.compareIgnoreTime(renewalEffDate) > 0))
      if (not hasUnexpiredESEScheds) {
        line.setCoverageExists(line.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLCOICov_TDIC"), false)
      }
    }
  }

  public static function incrementEndorsementLTEffectiveDates(line : GLLine) {
    var renewalEffDate = line.Branch.EditEffectiveDate
    if(line.GLManuscriptEndor_TDICExists and line.GLManuscript_TDIC.HasElements){
      line.GLManuscript_TDIC.each(\manuscript -> {
        manuscript.ManuscriptEffectiveDate = renewalEffDate
      })
    }
    if(line.GLAdditionalInsuredCov_TDICExists and line.GLAdditionInsdSched_TDIC.HasElements){
      line.GLAdditionInsdSched_TDIC.each(\glAddlInsd -> {
        glAddlInsd.LTEffectiveDate = renewalEffDate
      })
    }
    if(line.GLMobileDentalClinicCov_TDICExists and line.GLMobileDCSched_TDIC.HasElements){
      line.GLMobileDCSched_TDIC.each(\glMobileDCSched -> {
        glMobileDCSched.LTEffectiveDate = renewalEffDate
      })
    }
    if(line.GLNameOTDCov_TDICExists and line.GLNameODSched_TDIC.HasElements){
      line.GLNameODSched_TDIC.each(\glNameODSched -> {
        glNameODSched.LTEffectiveDate = renewalEffDate
      })
    }
    if(line.GLStateExclCov_TDICExists and line.GLStateExclSched_TDIC.HasElements){
      line.GLStateExclSched_TDIC.each(\glStateExclSched -> {
        glStateExclSched.LTEffectiveDate = renewalEffDate
      })
    }
    if(line.GLBLAICov_TDICExists and line.GLDentalBLAISched_TDIC.HasElements){
        line.GLDentalBLAISched_TDIC.each(\glDBLAddlInsd -> {
        glDBLAddlInsd.LTEffectiveDate = renewalEffDate
      })
    }
    if(line.GLCOICov_TDICExists and line.GLCertofInsSched_TDIC.HasElements){
      line.GLCertofInsSched_TDIC.each(\glCertOfInsSched -> {
        glCertOfInsSched.LTEffectiveDate = renewalEffDate
      })
    }
    if (line.GLLocumTenensCov_TDICExists and line.GLLocumTenensSched_TDIC.HasElements) {
      line.GLLocumTenensSched_TDIC.each(\locumSched -> {
        locumSched.LTEffectiveDate = renewalEffDate
      })
    }

    if (line.GLExcludedServiceCond_TDICExists and line.GLExcludedServiceSched_TDIC.HasElements) {
      line.GLExcludedServiceSched_TDIC.each(\exclSched -> {
        exclSched.LTEffecticeDate = renewalEffDate
      })
    }
  }
}