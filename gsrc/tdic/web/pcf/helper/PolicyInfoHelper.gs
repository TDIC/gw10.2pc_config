package tdic.web.pcf.helper

uses gw.plugin.billing.InstallmentPlanData
uses gw.billing.PolicyPeriodBillingInstructionsManager

class PolicyInfoHelper {

  public static function initializeMultiLineDiscount(period: PolicyPeriod) {
    if (period.MultiLineDiscount_TDIC == null) {
      if (period.BaseState == Jurisdiction.TC_AK) {
        return
      }
      period.MultiLineDiscount_TDIC = MultiLineDiscount_TDIC.TC_N   // No discount
    }
  }

  public static function isMultiLineDiscountVisible(period: PolicyPeriod): boolean {
    return period.BaseState == Jurisdiction.TC_AK? false : true
  }

  public static function isMultiLineDiscountRequired(period: PolicyPeriod): boolean {
    return period.BaseState == Jurisdiction.TC_AK? false : true
  }

  public static function getMultiLineDiscountSelections(policyPeriod: PolicyPeriod) : typekey.MultiLineDiscount_TDIC[] {

    if(policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
      if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
        return MultiLineDiscount_TDIC.getTypeKeys(false).toTypedArray()
      }else{
        return MultiLineDiscount_TDIC.TF_OTHER.TypeKeys.toTypedArray()
      }
    }
    if(policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC"){
      if (policyPeriod.BaseState == Jurisdiction.TC_PA) {
        return MultiLineDiscount_TDIC.TF_OTHER.TypeKeys.toTypedArray()
      }else{
        return null
      }
    }
    if(policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC"){
       return null
    }
    if(policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" || policyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC")
      return MultiLineDiscount_TDIC.TF_OTHER.TypeKeys.toTypedArray()

    if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
      return MultiLineDiscount_TDIC.getTypeKeys(false).toTypedArray()
    }
   return null
  }

}