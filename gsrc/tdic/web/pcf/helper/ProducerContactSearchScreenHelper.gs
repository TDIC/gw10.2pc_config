package tdic.web.pcf.helper

/**
 * Jeff Lin 4/3/2020, 2:35 PM PST,
 * For GPC-3723: Producer User Search - Able to pull up other agent clients
 * When Ia producer of an organization starts this query he/she should not be able to obtain information
 * on a client outside of the territory set for that user and/or region of the producer's organization .
 */

class ProducerContactSearchScreenHelper {

  public static function findPrducerOrganizationStateTypeKeys(): ArrayList<typekey.State> {
      return getStateTypeKeys(User.util.CurrentUser.Organization.RootGroup.Regions.toList())
  }

  private static function getStateTypeKeys(grpRegions: List<GroupRegion>): ArrayList<typekey.State> {
    var stateTypeKeyList = new ArrayList<typekey.State>()
    grpRegions.each(\elt -> stateTypeKeyList.add(mapStateTypeKey(elt.Region.Zones)))
    return stateTypeKeyList
  }

  private static function mapStateTypeKey(stateAbbrev: String) : typekey.State {
    return typekey.State.getTypeKey(stateAbbrev)
  }

}