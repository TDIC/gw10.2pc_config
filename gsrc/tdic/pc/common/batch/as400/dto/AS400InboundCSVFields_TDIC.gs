package tdic.pc.common.batch.as400.dto

/**
 * Sprint - 4 , GINTEG-237 : DTO for mapping data in the inbound AS400 csv file.
 * @Author : Sudheendra V
 * @Date: 08/12/2019
 */
class AS400InboundCSVFields_TDIC {
  var _ada : long as ADA
  var _as400polnumbr : long as AS400PolNumbr
  var _lastname : String as LastName
  var _firstname : String as FirstName
  var _title : String as Title
  var _longName : String as LongName
}