package tdic.pc.common.batch.multilinediscount

uses gw.api.database.Relop
uses org.apache.commons.lang.time.StopWatch
uses gw.processes.BatchProcessBase
uses gw.pl.logging.LoggerCategory
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses java.lang.Exception
uses gw.api.database.IQueryBeanResult
uses java.util.Date
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US966, DE197
 * 04/02/2015 Rob Kelly
 *
 * A batch to review the multiline discount on each In Force WC Policy outside a grace period.
 * A "Multiline Discount Out of Sync" activity is created on a Policy if the multiline discount indicator is set on the policy
 * and it is not eligible for the discount or if the multiline discount indicator is not set on the policy and it is eligible
 * for the discount.
 */
class TDIC_ReviewMutlilineDiscountBatch extends BatchProcessBase {

  /**
   * The logger for this Batch.
   */
  private static final var LOGGER = LoggerFactory.getLogger("REVIEW_MULITLINE_DISCOUNT")

  /**
   * The logger tag for this Batch.
   */
  private static final var LOG_TAG = "TDIC_ReviewMutlilineDiscountBatch#"

  /**
   * The code for the multiline discount out of sync Activity Pattern.
   */
  protected static final var MULTILINE_DISCOUNT_OUT_OF_SYNC_ACTIVITY_PATTERN_CODE : String = "multiline_discount_oos"

  private static final var BATCH_USER = "iu"

  private static final var POLICY_CHANGE_QUEUE = "Policy Change"

  construct() {
    super(BatchProcessType.TC_REVIEWMULTILINEDISCOUNT_TDIC)
  }

  /**
   * Returns true, since this batch process can be terminated.
   */
  override function requestTermination() : boolean {
    super.requestTermination()
    return true
  }

  /**
   * Reviews the multiline discount indicator on all In Force WC Policies outside the grace period and creates a "Multiline Discount Out of Sync"
   * activity on a Policy if the multiline discount indicator is set on the policy and it is not eligible for the discount or if the multiline
   * discount indicator is not set on the policy and it is eligible for the discount.
   */
  override function doWork() {
    var branch : PolicyPeriod
    var currentDate = DateUtil.currentDate();
    var issues : List<String>;

    var logTag =  LOG_TAG + "doWork() - "
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Entering")
    }

    var policiesToReview = getPoliciesToReview (currentDate);
    OperationsExpected = policiesToReview.Count
    LOGGER.debug(logTag + "Reviewing " + OperationsExpected + " Policies")

    var watch = new StopWatch()
    watch.start()
    for (aPolicyPeriod in policiesToReview) {
      if (TerminateRequested) {
        LOGGER.info(logTag + "TerminateRequested received...")
        break
      }

      branch = aPolicyPeriod.getSlice(currentDate);
      try {
        issues = branch.JobProcess.validateLineDiscount_TDIC();
        if (issues.HasElements) {
          createActivityIfRequired (branch, issues);
        }
      } catch (e : Exception) {
        LOGGER.error(logTag + "ERROR reviewing discount for Policy " + branch.PolicyNumber + ": " + e.getMessage())
        incrementOperationsFailed()
      } finally {
        incrementOperationsCompleted()
      }
    }

    watch.stop()
    LOGGER.info(logTag + "Reviewed " + OperationsCompleted + " Policies")
    LOGGER.info(logTag + "" + OperationsFailed + " failed reviews")
    LOGGER.info(logTag + "Time taken: " + watch.toString())

    if (LOGGER.DebugEnabled) {
      LOGGER.debug("TDIC_ReviewMutlilineDiscountBatch#doWork() - Exiting")
    }
  }

  /**
   * Returns all PolicyPeriods needing review.
   */
  protected function getPoliciesToReview (currentDate : Date) : IQueryBeanResult<PolicyPeriod> {
    var logTag =  LOG_TAG + "getPoliciesToReview(newSubmissionGracePeriod) - "
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Entering")
    }

    var query = Query.make(PolicyPeriod)
                     .compare (PolicyPeriod#Status, Equals, PolicyPeriodStatus.TC_BOUND)
                     .compare (PolicyPeriod#PeriodStart, LessThanOrEquals, currentDate)
                     .compare (PolicyPeriod#PeriodEnd, GreaterThan, currentDate)
                     .or (\orCriteria -> { orCriteria.compare (PolicyPeriod#CancellationDate, Equals, null);
                                           orCriteria.compare (PolicyPeriod#CancellationDate, GreaterThan, currentDate);
                                         })
                     .compare (PolicyPeriod#MostRecentModel, Equals, true);
    var results = query.select();

    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Exiting")
    }

    return results;
  }

  /**
   * Creates a "Multiline Discount Out of Sync" activity if the activity for this is not already open.
   */
  protected function createActivityIfRequired (aPolicyPeriod : PolicyPeriod, issues : List<String>) : void {
    var logTag =  LOG_TAG + "createActivityIfRequired(PolicyPeriod,List<String>) - "
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Entering")
    }

    var activityDescription = issues.join("\n");
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + activityDescription)
    }

    var activityRequired = aPolicyPeriod.Policy.AllOpenActivities.firstWhere( \ a -> a.ActivityPattern.Code == MULTILINE_DISCOUNT_OUT_OF_SYNC_ACTIVITY_PATTERN_CODE and a.Description == activityDescription and a.Status == typekey.ActivityStatus.TC_OPEN) == null
    if (activityRequired) {
      if (LOGGER.DebugEnabled) {
        LOGGER.debug(logTag + "Creating Multiline Discount Out of Sync activity for Policy " + aPolicyPeriod.PolicyNumber)
      }
      createActivity (aPolicyPeriod.Policy, activityDescription);
    } else {
      if (LOGGER.DebugEnabled) {
        LOGGER.debug(logTag + "No Multiline Discount Out of Sync Activity Required for Policy " + aPolicyPeriod.PolicyNumber)
      }
    }

    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Exiting")
    }
  }

  /**
   * Creates a "Multiline Discount Out of Sync" activity on the specified policy with the specified description and assigns it.
   */
  private function createActivity(aPolicy : Policy, description : String) {
    var logTag = LOG_TAG + "createActivity(Policy,String) - "
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Entering")
    }
    var pattern = ActivityPattern.finder.getActivityPatternByCode(MULTILINE_DISCOUNT_OUT_OF_SYNC_ACTIVITY_PATTERN_CODE)
    if (pattern != null) {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var policy = bundle.add(aPolicy)
        var activity = pattern.createPolicyActivity(bundle, aPolicy, null, description, null, null, null, null, null)
        // GINTEG-1539 Activity assigned to Policy Change Queue
        var group = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:206").select().AtMostOneRow
        if(group != null) {
          var queue = group.getQueue(POLICY_CHANGE_QUEUE)
          if(queue != null) {
            activity.assignActivityToQueue(queue, group)
          }
        }
      }, BATCH_USER)
    } else {
      LOGGER.error(logTag + "Cannot create activity: activity pattern with code " + MULTILINE_DISCOUNT_OUT_OF_SYNC_ACTIVITY_PATTERN_CODE + " not found")
    }
    if (LOGGER.DebugEnabled) {
      LOGGER.debug(logTag + "Exiting")
    }
  }
}