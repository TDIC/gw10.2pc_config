package tdic.pc.common.batch.multilinediscount.dto

class MultiLinePolicyBatchDTO {

  var _ada : String as ADA
  var _polnumbr : String as PolNumbr
  var _lastname : String as LastName
  var _firstname : String as FirstName
  var _lob : String as Lob
  var _baseState : String as BaseState

}