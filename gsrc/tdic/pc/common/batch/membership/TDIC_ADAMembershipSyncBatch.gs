package tdic.pc.common.batch.membership

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses org.w3c.dom.Element
uses org.w3c.dom.Node
uses org.xml.sax.InputSource
uses tdic.pc.common.batch.membership.dto.TDIC_ADAMembershipRecord
uses tdic.pc.integ.services.membership.aptifymembershipservice.AptifyMembershipService
uses javax.xml.parsers.DocumentBuilderFactory
uses java.io.StringReader
uses java.text.SimpleDateFormat

/**
 * US1316
 * 04/13/2015 Kesava Tavva
 *
 * Implementation of custom batch process to syncc membership status with Aptify. The process does following things
 *  Retrieve member status records from Aptify with update date equal or greater than batch run date
 *  Retrieve Person records from the application with ADANumbers retrieved from Aptify
 *  Compare status between Person record and Aptify record
 *  If there is a status change then update person record in the application
 *  Ignore if there is no status change.
 *
 */
class TDIC_ADAMembershipSyncBatch extends BatchProcessBase {
  private static final var _logger = LoggerFactory.getLogger("TDIC_BATCH_ADAMEMBERSHIPSYNC")
  private static var CLASS_NAME = "TDIC_ADAMembershipSyncBatch"
  private var _notMatchedRecords : List<TDIC_ADAMembershipRecord>
  private var _errorRecords : List<TDIC_ADAMembershipRecord>
  private static final var BATCH_USER = "iu"
  private var _failureEmailTo : String as readonly FailureEmailTo
  private static final var FAILURE_EMAIL_RECIPIENT = "PCInfraIssueNotificationEmail"
  private static final var EXTERNAL_APP_NAME = "Aptify"
  private static final var FAILURE_SUB = "Failure::${gw.api.system.server.ServerUtil.ServerId}-ADA Membership Sync Batch Job Failed"
  private static final var TERMINATE_SUB : String = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-ADA Membership Sync Batch Job Terminated"
  private static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private static final var GLOBAL_STATUS_TYPE = "GlobalStatus_TDIC"
  private static final var NAME_SPACE_APTIFY = "tdic:aptify"
  private static final var SELECT_LIMIT = 1500
  private static final var USER_NAME = "aptify.username"
  private static final var PASS_WORD = "aptify.password"
  private var APTIFY_USERNAME : String = null
  private var APTIFY_PASSWORD : String = null
  public static final var USERNAME_ERROR: String = "Cannot retrieve aptify username from properties file on server " + gw.api.system.server.ServerUtil.ServerId
  public static final var PASSWORD_ERROR: String = "Cannot retrieve aptify password from properties file on server " + gw.api.system.server.ServerUtil.ServerId

  construct(){
    super(BatchProcessType.TC_ADAMEMBERSHIPSYNC_TDIC)
  }

  /**
   * US1316
   * 04/13/2015 Kesava Tavva
   *
   * This function overrides dowork() method to process membership status with Aptify
   */
  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.trace("${logPrefix} - Entering")
    try {
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before retrieveMemberRecordsFromAptify().")
        EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "ADA Membership Sync Batch Job is terminated.")
        return
      }
      var aptifyMemberRecords = retrieveMemberRecordsFromAptify()
      _logger.debug("${logPrefix} - Member records retrieved from Aptify are: ${aptifyMemberRecords}")
      if(aptifyMemberRecords?.size() == 0){
        _logger.debug("No Member records found in Aptify.")
        _logger.debug("${logPrefix} - Exiting")
        return
      }
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before retrieveMemberRecrodsFromPC().")
        EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "ADA Membership Sync Batch Job is terminated.")
        return
      }
      //GW-2625: Limit querying the database to SELECT_LIMIT (1500) records.
      //How this works
      //Say there are 3501 ADA Numbers
      //The process below will break it down to selecting
      //0 to 1500 indexex, then 1501 to 3000 and then last 3001 to 3501
      var adaNumbersList = aptifyMemberRecords.Keys.toList()
      var pcMemberRecords : List<Person> = null
      if( adaNumbersList.Count > SELECT_LIMIT ) {
        pcMemberRecords = new ArrayList<Person>()
        var numLoops =  (adaNumbersList.Count / SELECT_LIMIT) as int
        var rem = adaNumbersList.Count % SELECT_LIMIT
        for(var i in 0..numLoops) {
          var start = i*SELECT_LIMIT
          var end = start+SELECT_LIMIT
          start = (start == 0) ? 0 : (start+1)
          if( end > adaNumbersList.Count) {
            end = adaNumbersList.Count - rem - 1
          }
          if( end > start ) {
            var adaNumbersSubList = adaNumbersList.subList(start, end)
            pcMemberRecords.addAll( retrieveMemberRecrodsFromPC(adaNumbersSubList) )
          }
        }
        if( rem > 0 ) {
          var adaNumSub = adaNumbersList.subList((adaNumbersList.Count-rem+1), adaNumbersList.Count)
          pcMemberRecords.addAll( retrieveMemberRecrodsFromPC(adaNumSub) )
        }
      }
      else {
        pcMemberRecords = retrieveMemberRecrodsFromPC(adaNumbersList)
      }
      _logger.debug("${logPrefix} - Person records found: ${pcMemberRecords}")
      if(!pcMemberRecords.HasElements){
        _logger.debug("No Person records found for Sync with Aptify.")
        _logger.debug("${logPrefix} - Exiting")
        return
      }

      OperationsExpected = pcMemberRecords.size()

      processADAMembershipForSync(pcMemberRecords, aptifyMemberRecords)
      _logger.trace("${logPrefix} - Exiting")

    }catch(e : Exception){
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "ADA membership sync batch process failed with errors. Please review below error details. \n ${e.StackTraceAsString}")
      _logger.error("${logPrefix} - Batch process failed with erorrs.", e)
    }
  }

  /**
   * US1316
   * 05/01/2015 Kesava Tavva
   *
   * This function retrieves membership status from Aptify
   */
  @Returns("Map<String,TDIC_ADAMembershipRecord>, ADAmembership records map with ADANumber and status")
  private function retrieveMemberRecordsFromAptify(): Map<String,TDIC_ADAMembershipRecord> {
    var logPrefix = "${CLASS_NAME}#retrieveMemberRecordsFromAptify(List<String>)"
    _logger.trace("${logPrefix}- Entering")

    var adaMembershipRecords : Map<String, TDIC_ADAMembershipRecord>
    try {
      var membershipService = new AptifyMembershipService()
      var dateUpdated = new java.util.Date()
      var dateFormat = new SimpleDateFormat("yyyyMMdd");
      var strDate = dateFormat.format(dateUpdated);
      _logger.info("Membership status sync started on date " + strDate)
      APTIFY_USERNAME = PropertyUtil.getInstance().getProperty(USER_NAME)
      if (APTIFY_USERNAME == null) {
        throw new GWConfigurationException(USERNAME_ERROR)
      }
      APTIFY_PASSWORD = PropertyUtil.getInstance().getProperty(PASS_WORD)
      if (APTIFY_PASSWORD == null) {
        throw new GWConfigurationException(PASSWORD_ERROR)
      }
      var gwByDateResponse = membershipService.GWByDate(APTIFY_USERNAME, APTIFY_PASSWORD, strDate)
      // Since the xml response is not well formed, adding node to counter parse issues
      gwByDateResponse =  "<GWByDateResult>" + gwByDateResponse.concat("</GWByDateResult>")
      var factory = DocumentBuilderFactory.newInstance();
      var builder = factory.newDocumentBuilder();
      var document = builder.parse(new InputSource(new StringReader(gwByDateResponse)));
      var custNodeList = document.getElementsByTagName("CustInfo");
      adaMembershipRecords = new HashMap<String,TDIC_ADAMembershipRecord>()
      for (elemIndex in 0..|custNodeList.getLength()) {
        var customerNode = custNodeList.item(elemIndex)
        if (customerNode.getNodeType() == Node.ELEMENT_NODE) {
          var eElement = customerNode as Element;
          var record = new TDIC_ADAMembershipRecord()
          record.ADANumber = eElement.getElementsByTagName("ADANumber").item(0).getTextContent()
          record.MemberType = eElement.getElementsByTagName("MemberType").item(0).getTextContent()
          var _dateUpdated = eElement.getElementsByTagName("DateUpdated").item(0).getTextContent()
          var formatter = new SimpleDateFormat("yyyyMMdd");
          var date = formatter.parse(_dateUpdated);
          record.DateUpdated = date
          adaMembershipRecords.put(record.ADANumber, record)
        }
      }
      _logger.trace("${logPrefix} - Exiting")
      return adaMembershipRecords
    } catch (e:Exception) {
      _logger.error("${logPrefix} - Exception= " + e)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "${logPrefix}- ADA membership sync batch failed due to :" + e.LocalizedMessage)
      throw(e)
    }
  }

  /**
   * US1316
   * 05/04/2015 Kesava Tavva
   *
   * This function retrieves Person records from the application with ADANumbers retreived from Aptify
   */
  @Param("List<String>","List of AdaNumbers retrieved from Aptify")
  @Returns("List<Person>, List of Person records retrieved from the application")
  protected function retrieveMemberRecrodsFromPC(adaNumbersList : List<String>) : List<Person> {
    var logPrefix = "${CLASS_NAME}#retrieveMemberRecordsFromPC(List<String>)"
    _logger.trace("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Person records retrieving for ADANumbersList: ${adaNumbersList}")
    var pQuery = Query.make(Person)
        .join(OfficialID,"Contact").compare("OfficialIDType",Equals,OfficialIDType.TC_ADANUMBER_TDIC)
        .compareIn("OfficialIDValue",adaNumbersList?.toTypedArray())
    _logger.trace("${logPrefix} - Exiting")
    return pQuery.select()?.toList()
  }

  /**
   * US1316
   * 04/13/2015 Kesava Tavva
   *
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.trace("${logPrefix} - Entering")
    _failureEmailTo = PropertyUtil.getInstance().getProperty(FAILURE_EMAIL_RECIPIENT)
    _logger.trace("${logPrefix} - ${FAILURE_EMAIL_RECIPIENT}-${_failureEmailTo}")
    var initFailureMsg:StringBuilder
    if(_failureEmailTo == null){
      initFailureMsg = new StringBuilder()
      initFailureMsg.append("${logPrefix} - Failed to retrieve notification email addresses with the key '${FAILURE_EMAIL_RECIPIENT}' from integration database.")
    }


    if (initFailureMsg?.toString().HasContent) {
      if(_failureEmailTo != null)
        EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "Unable to initiate batch process. Please review log file for more details.")
      throw new GWConfigurationException(initFailureMsg.toString())
    }
    _logger.trace("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  /**
   * US1316
   * 05/05/2015 Kesava Tavva
   *
   * This function processes updating membership status with updated status retrieved from Aptify.
   * It happens only when there is a change in status.
   */
  @Param("List<Person>", "List of Person records retrieved from the application")
  @Param("Map<String,TDIC_ADAMembershipRecord>", "ADAmembership records map with ADANumber and status")
  private function processADAMembershipForSync(pcMemberRecords : List<Person>, aptifyMemberRecords : Map<String, TDIC_ADAMembershipRecord>){
    var logPrefix = "${CLASS_NAME}#processADAMembershipForSync()"
    _logger.trace("${logPrefix} - Entering")
    _logger.debug("${logPrefix} - Contacts for processing: ${pcMemberRecords}")
    _logger.debug("${logPrefix} - Member records from Aptify: ${aptifyMemberRecords}")
    pcMemberRecords.each( \ person -> {
      try{
        if (TerminateRequested) {
          _logger.warn("${logPrefix} - Terminate requested during processADAMembershipForSync() method")
          EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "ADA Membership Sync Batch Job is terminated.")
          return
        }
        incrementOperationsCompleted()
        _logger.info("${logPrefix} - status update started for Person : ${person}")
        var aptifyMember = aptifyMemberRecords.get(person.ADANumberOfficialID_TDIC)
        if(aptifyMember.MemberType.HasContent){
          var status = MAPPER.getInternalCodeByAlias(GLOBAL_STATUS_TYPE, NAME_SPACE_APTIFY, aptifyMember.MemberType)
          _logger.info("${logPrefix} - AptifyStauts: ${aptifyMember.MemberType} | converted status: ${status} | Person status : ${person.CDAMembershipStatus_TDIC}")
          if(status.HasContent && status != person.CDAMembershipStatus_TDIC.Code){
            gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
              person = bundle.add(person)
              person.CDAMembershipStatus_TDIC = typekey.GlobalStatus_TDIC.get(status)
            },BATCH_USER)
          }
        }
        _logger.info("${logPrefix} - status update completed for Person : ${person}")
      }catch(e: Exception){
        incrementOperationsFailed()
        _logger.error("ADAMembershipForSync process failed for person : ${person}",e)
      }
    })
    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   * US1316
   * 07/06/2015 Kesava Tavva
   *
   * This is override function to handle termination request during batch process
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }

}