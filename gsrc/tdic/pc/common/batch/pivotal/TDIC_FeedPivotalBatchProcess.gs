package tdic.pc.common.batch.pivotal

uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.system.DatabaseDependenciesGateway
uses gw.pl.util.GWRunnable
uses tdic.util.csv.CSVFile
uses java.util.ArrayList
uses gw.api.database.IQueryBeanResult
uses gw.api.util.StateJurisdictionMappingUtil
uses java.util.Date
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses gw.api.util.DateUtil
uses java.lang.Exception
uses com.tdic.util.misc.EmailUtil
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US561: Policy Feed to Pivotal
 * 11/19/2014 Vicente--12/05/2017 Praneethk
 *
 * Feed Pivotal batch process implementation class.
 * This process is designed for read all the in force and canceled policies and write them to the following csv format texts files
 *  - Policy file
 *  - Policy Owner file
 *  - Policy Location file
 */
class TDIC_FeedPivotalBatchProcess extends BatchProcessBase {
  static protected var _logger : Logger = LoggerFactory.getLogger("TDIC_PIVOTAL");

  protected var _directoryName : String;  // Relative route to place the generated files.
  protected var _policyFileName : String;
  protected var _policyOwnerFileName : String;
  protected var _policyLocationFileName : String;

  protected var _policyFile : CSVFile;
  protected var _policyOwnerFile : CSVFile;
  protected var _policyLocationFile : CSVFile;

  protected var _failureEmailAddresses : String;    // Failure notification email addresses in case of any errors in batch process.
  protected var _failureEmailSubject : String;      // Failure notification email subject in case of any errors in batch process.

  protected var _pivotalFileForBOP : CSVFile
  protected var _pivotalFileForPL : CSVFile
  protected var _pivotalFileForCyber : CSVFile
  protected var _bopFileName : String
  protected var _plFileName : String
  protected var _cyberFileName : String
  static final var plcm = "PL-CM"
  static final var ploc = "PL-OC"

  private enum pivotalConstants_TDIC {
    Canceled, BOP, Y, N, CG, CQ, CC, CI, CM, CX, CL, CD, CW, CO, PLCyberLiab_TDIC, PLClaimsMade_TDIC, CYB  , LRP , BOPLessorsRisk_TDIC
  }
  construct() {
    super(BatchProcessType.TC_FEEDPIVOTALBATCH_TDIC)
  }

  /**
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  override function checkInitialConditions() : boolean {
    _logger.debug ("TDIC_FeedPivotalBatchProcess#checkInitialConditions() - Entering");

    _logger.trace ("TDIC_FeedPivotalBatchProcess#checkInitialConditions() - Loading data from cache");
    loadDataFromCache();

    _logger.trace ("TDIC_FeedPivotalBatchProcess#checkInitialConditions() - Validating data from cache");
    var retval = validateDataFromCache();

    if (retval == false) {
      incrementOperationsFailed()
      _logger.error ("TDIC_FeedPivotalBatchProcess#checkInitialConditions() - Properties with the files names of folder name not defined in GWINT");
    }

    _logger.debug ("TDIC_FeedPivotalBatchProcess#checkInitialConditions() - Exiting");

    return retval;
  }

  protected function loadDataFromCache() : void {
    _directoryName          = PropertyUtil.getInstance().getProperty("pivotal.filesdirectory.name");
    _policyFileName         = PropertyUtil.getInstance().getProperty("pivotal.policyfile.name");
    _policyOwnerFileName    = PropertyUtil.getInstance().getProperty("pivotal.policyownerfile.name");
    _policyLocationFileName = PropertyUtil.getInstance().getProperty ("pivotal.policylocationfile.name");

    _failureEmailAddresses = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail");
    _failureEmailSubject = "Feed Pivotal Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId
    //GINTEG-688 : pivotal Files for BOP, PL and Cyber
    _bopFileName = PropertyUtil.getInstance().getProperty("pivotal.fileForBOP.name")
    _plFileName = PropertyUtil.getInstance().getProperty("pivotal.fileForPL.name")
    _cyberFileName = PropertyUtil.getInstance().getProperty("pivotal.fileForCyber.name")
  }

  protected function validateDataFromCache() : boolean {
    return (_directoryName != null and _policyFileName != null and _policyOwnerFileName != null and
            _policyLocationFileName != null and _failureEmailAddresses != null);
  }

  override protected function doWork(): void {
    _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Entering");
    try {
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Opening files");
      openFiles();

      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Get policy periods for Pivotal feed");
      var currentDate = DateUtil.currentDate();
      var policyPeriods = getAllPoliciesToWrite(currentDate);

      OperationsExpected = policyPeriods.Count;
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Processing " + OperationsExpected + " policy periods");

      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Writing files");

      for (policyPeriod in policyPeriods) {
        if (TerminateRequested) {
          _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - TerminateRequested received...");
          _logger.debug("TDIC_FeedPivotalBatchProcess#requestTermination() - Deleting files");
          deleteFiles();
          break;
        }
        policyPeriod = slicePolicyPeriod(policyPeriod, currentDate)
        //GINTEG-688: This should only read WC7 Specific Data
        if (not policyPeriod.WC7LineExists) {
          continue
        }
        _logger.trace("TDIC_FeedPivotalBatchProcess#doWork() - Writing policy data started for Policy: " + policyPeriod);
        writePolicyData(policyPeriod);
        _logger.trace("TDIC_FeedPivotalBatchProcess#doWork() - Writing policy data completed for Policy: " + policyPeriod);
        incrementOperationsCompleted();
      }
      _policyFile.close()
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Closing files");
      closeFiles();
    } catch (e : Exception) {
      incrementOperationsFailed();
      _logger.error("TDIC_FeedPivotalBatchProcess#doWork() - Exception caught in batch process: " + e.LocalizedMessage, e);
      EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
      deleteFiles();
    }
    //GINTEG-688 : Pivotal Data Population for CP, PL and Cyb
    try {
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Opening files")
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Get policy periods for Pivotal feed");
      var currentDate = DateUtil.currentDate()
      var policyPeriods = getPoliciesForPivotal_TDIC(currentDate)
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Writing pivotal data for BOP")
      _pivotalFileForBOP = new CSVFile(_directoryName + _bopFileName)
      for (policyPeriod in policyPeriods) {
        if (TerminateRequested) {
          _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - TerminateRequested received...");
          _logger.debug("TDIC_FeedPivotalBatchProcess#requestTermination() - Deleting files");
          _pivotalFileForBOP?.delete();
          break;
        }
        policyPeriod = slicePolicyPeriod(policyPeriod, currentDate)
        if (policyPeriod.BOPLineExists) {
          _logger.trace("TDIC_FeedPivotalBatchProcess#doWork() - Writing policy data started for BOP Policy: " + policyPeriod)
          policyPeriod.BOPLine.BOPLocations.each(\bopLocation -> {
            var feedPivotalDTO = populateFeedPivotalDTOForBOP_TDIC(bopLocation, policyPeriod)
            _pivotalFileForBOP.writeRow(feedPivotalDTO)
            _logger.trace("TDIC_FeedPivotalBatchProcess#doWork() - Writing policy data completed for BOP Policy: " + policyPeriod)
            //GINTEG1241 : Reset the flag once the related data has been written to Pivotal File
            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
              try {
                var trans = new com.guidewire.pl.system.transaction.BootstrapTransaction(new GWRunnable() {
                  override function run() {
                    var sql = "update pc_policyperiod set considerForPivotalInd_TDIC = 0 where PolicyNumber = '"
                        + policyPeriod.PolicyNumber + "'"
                    DatabaseDependenciesGateway.getDatabaseAccessor().update(sql)
                  }
                })
                com.guidewire.pl.system.dependency.ServerDependencies.getTransactionManager().execute(trans)
              } catch (e : com.guidewire.pl.system.exception.TransactionException) {
                incrementOperationsFailed()
                _logger.error("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Exception caught in batch process for PolicyNumber "
                    + policyPeriod.PolicyNumber +"  "+ e.LocalizedMessage, e);
                EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
              }
            })
            incrementOperationsCompleted()
          })
        }
      }
      _pivotalFileForBOP?.close()
      _pivotalFileForCyber = new CSVFile(_directoryName + PropertyUtil.getInstance().getProperty("pivotal.fileForCyber.name"))
      _pivotalFileForPL = new CSVFile(_directoryName + PropertyUtil.getInstance().getProperty("pivotal.fileForPL.name"))
      for (policyPeriod in policyPeriods) {
        if (TerminateRequested) {
          _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - TerminateRequested received...");
          _logger.debug("TDIC_FeedPivotalBatchProcess#requestTermination() - Deleting files");
          _pivotalFileForPL?.delete()
          _pivotalFileForCyber?.delete()
          break;
        }
        policyPeriod = slicePolicyPeriod(policyPeriod, currentDate)
        if (policyPeriod.GLLineExists) {
          writePivotalDataForPL_TDIC(policyPeriod)
        }
      }
      _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Closing files")
      _pivotalFileForCyber?.close()
      _pivotalFileForPL?.close()
    } catch (e : Exception) {
      incrementOperationsFailed();
      _logger.error("TDIC_FeedPivotalBatchProcess#doWork() - Exception caught in batch process: " + e.LocalizedMessage, e);
      EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
      _pivotalFileForBOP?.delete();
      _pivotalFileForPL?.delete()
      _pivotalFileForCyber?.delete()
    }
    _logger.debug("TDIC_FeedPivotalBatchProcess#doWork() - Exiting");
  }

  protected function openFiles() : void {
    _policyFile         = new CSVFile (_directoryName + _policyFileName);
    _policyOwnerFile    = new CSVFile (_directoryName + _policyOwnerFileName);
    _policyLocationFile = new CSVFile (_directoryName + _policyLocationFileName);
  }

  protected function closeFiles() : void {
    _policyFile.close();
    _policyOwnerFile.close();
    _policyLocationFile.close();
  }

  protected function deleteFiles() : void {
    _policyFile?.delete();
    _policyOwnerFile?.delete();
    _policyLocationFile?.delete();
  }

  /**
   * Slice the policy period to the current date, unless the current date is not a valid date for the policy period.
   */
  protected function slicePolicyPeriod (policyPeriod : PolicyPeriod, currentDate : Date) : PolicyPeriod {
    var sliceDate : Date;

    if (currentDate < policyPeriod.PeriodStart) {
      sliceDate = policyPeriod.PeriodStart;
    } else if (currentDate >= policyPeriod.PeriodEnd) {
      // BrianS - PeriodEnd is not a valid slice date, subtract 1 second.
      sliceDate = policyPeriod.PeriodEnd.addSeconds(-1);
    } else {
      sliceDate = currentDate;
    }

    return policyPeriod.getSlice(sliceDate);
  }

  /**
   * Write data to all the files for the policy period.
   */
  protected function writePolicyData (policyPeriod : PolicyPeriod) : void {
    var record : List<String>;

    record = createPolicyRecord (policyPeriod);
    _policyFile.writeRow (record);

    var policyOwners = getPolicyOwners (policyPeriod);
    for (policyOwner in policyOwners) {
      record = createPolicyOwnerRecord (policyOwner);
      _policyOwnerFile.writeRow (record);
    }

    writePolicyLocationData (policyPeriod);
  }

  /**
   * Create a policy file record for the policy period.
   */
  protected function createPolicyRecord (policyPeriod : PolicyPeriod) : List<String> {
    _logger.trace ("TDIC_FeedPivotalBatchProcess#createPolicyRecord() - Entering")

    var xmod = policyPeriod.XMod_TDIC;

    var firstPaymentDate : Date;
    try {
      firstPaymentDate = policyPeriod.FirstPaymentDate_TDIC;
    } catch (e : Exception) {
      _logger.error ("Failed to retrieve first payment date for " + policyPeriod + " from BillingCenter: " + e);
    }

    var record = new ArrayList<String>();
    record.add (policyPeriod.PolicyNumber);
    record.add (policyPeriod.TermNumber as String);
    record.add (policyPeriod.Policy.OriginalEffectiveDate as String);             // Policy start date
    record.add (policyPeriod.PeriodDisplayStatus);                                // Policy status
    record.add (policyPeriod.PeriodStart as String);                              // Policy effective date
    record.add (policyPeriod.PeriodEnd as String);                                // Policy expiration date
    record.add (policyPeriod.PolicyOrgType_TDIC.DisplayName);                     // Organization type
    record.add (xmod as String);                                                  // X-Mod
    record.add (policyPeriod.TotalPremiumRPT.Amount as String);                   // Premium
    record.add (policyPeriod.CancellationDate as String);
    record.add (policyPeriod.CancellationReasonCode_TDIC.Description);            // Cancel reason
    record.add (policyPeriod.MultilineDiscount as String);                   // Multi-Line Discount
    record.add (firstPaymentDate as String);                                      // First Payment Date

    _logger.trace ("TDIC_FeedPivotalBatchProcess#createPolicyRecord() - Exiting");

    return record;
  }

  /**
   * Get a list of policy owners
   */
  protected function getPolicyOwners (policyPeriod : PolicyPeriod) : List<PolicyContactRole> {
    var retval = new ArrayList<PolicyContactRole>();
    var primaryNamedInsured = policyPeriod.PrimaryNamedInsured;
    var pniContact = primaryNamedInsured.AccountContactRole.AccountContact.Contact;

    retval.add (primaryNamedInsured);

    if (policyPeriod.WC7LineExists) {
      for (ownerOfficer in policyPeriod.WC7Line.WC7PolicyOwnerOfficers) {
        if (ownerOfficer.WC7OwnershipPct > 0 and ownerOfficer.AccountContactRole.AccountContact.Contact != pniContact) {
          retval.add (ownerOfficer);
        }
      }
    }

    return retval;
  }

  /**
   * Create a policy owner file record for the policy period.
   */
  protected function createPolicyOwnerRecord (policyContactRole : PolicyContactRole) : List<String> {
    _logger.trace ("TDIC_FeedPivotalBatchProcess#createPolicyOwnerRecord() - Entering")

    var record = new ArrayList<String>();
    var policyPeriod = policyContactRole.Branch;
    var contact = policyContactRole.AccountContactRole.AccountContact.Contact;

    record.add (policyPeriod.PolicyNumber);
    record.add (policyPeriod.TermNumber as String);
    record.add (contact typeis Person ? contact.ADANumberOfficialID_TDIC : null);   // ADA #
    record.add (policyContactRole.DisplayName);                                     // Name
    record.add (contact.getPolicyPeriodRoles_TDIC(policyPeriod));                   // Roles
    record.add (policyContactRole.TaxID);

    _logger.trace ("TDIC_FeedPivotalBatchProcess#createPolicyOwnerRecord() - Exiting");

    return record;
  }

  /**
   *  Method that writes the policy information required to Policy Location File. For each policy:
   *  - Policy#
   *  - Policy term
   *  - Location #
   *  - Location Address 1
   *  - Location Address 2
   *  - Location Address 3
   *  - Location City
   *  - Location State
   *  - Location Zip
   *  - Number of Employees
   *  - Payroll for the location
   *  - Location effective date
   *  - Location expiration date
   */
  @Param("policyPeriod", "Policy period where the information is taken")
  protected function writePolicyLocationData (policyPeriod: PolicyPeriod) {
    _logger.debug("TDIC_FeedPivotalBatchProcess#writePolicyLocationData() - Entering")

    if (policyPeriod.WC7LineExists) {
      var coveredEmployees = new ArrayList<WC7CoveredEmployee>();
      var effectiveDate : Date;
      var expirationDate : Date;
      var locationCoveredEmployees : List<WC7CoveredEmployee>;
      var numEmployees : int;
      var payroll : int;
      var policyLocations = new ArrayList<PolicyLocation>();
      var policyLocation : PolicyLocation;
      var record : ArrayList<String>;
      var state : State;
      var wc7Line = policyPeriod.WC7Line;

      for (jurisdiction in wc7Line.WC7Jurisdictions) {
        state = StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction);
        coveredEmployees.addAll (wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction).toList());
        policyLocations.addAll (policyPeriod.getPolicyLocationWM(state).toList());
      }

      var coveredEmployeeMap = coveredEmployees.partition(\ce -> ce.LocationWM.AccountLocation);
      var policyLocationMap = policyLocations.partitionUniquely(\pl -> pl.AccountLocation);

      for (accountLocation in coveredEmployeeMap.Keys) {
        locationCoveredEmployees = coveredEmployeeMap.get(accountLocation);
        policyLocation = policyLocationMap.get(accountLocation);

        effectiveDate = locationCoveredEmployees.min(\ce -> ce.EffectiveDate);
        expirationDate = locationCoveredEmployees.max(\ce -> ce.ExpirationDate);
        numEmployees = locationCoveredEmployees.sum(\ce -> ce.NumEmployees != null ? ce.NumEmployees : 0);
        payroll = locationCoveredEmployees.sum(\ce -> ce.BasisAmount);

        record = new ArrayList<String>()
        record.add (policyPeriod.PolicyNumber);
        record.add (policyPeriod.TermNumber as String);
        record.add (policyLocation.LocationNum as String);
        record.add (policyLocation.AddressLine1);
        record.add (policyLocation.AddressLine2);
        record.add (policyLocation.AddressLine3);
        record.add (policyLocation.City);
        record.add (policyLocation.State as String);
        record.add (policyLocation.PostalCode);
        record.add (numEmployees as String);
        record.add (payroll as String);
        record.add (effectiveDate as String);
        record.add (expirationDate as String);

        _policyLocationFile.writeRow (record);
      }
    }
    _logger.debug("TDIC_FeedPivotalBatchProcess#writePolicyLocationFile() - Exiting")
  }

  /**
   *  Method that returns all the policies in force or cancelled
   */
  @Returns("Returns all the policies in force")
  protected function getAllPoliciesToWrite (currentDate : Date): IQueryBeanResult<PolicyPeriod> {
    _logger.debug("TDIC_FeedPivotalBatchProcess#getAllPoliciesToWrite() - Entering")

    var query = Query.make(PolicyPeriod)
        .compare (PolicyPeriod#Status, Equals, PolicyPeriodStatus.TC_BOUND)
        .compare (PolicyPeriod#PeriodStart, LessThanOrEquals, currentDate)
        .compare (PolicyPeriod#PeriodEnd, GreaterThan, currentDate)
        .compare (PolicyPeriod#MostRecentModel, Equals, true)
        .withLogSQL(true);
    var results = query.select();

    _logger.debug("TDIC_FeedPivotalBatchProcess#getAllPoliciesToWrite() - Exiting")

    return results;
  }

  /**
   * This is override function to handle termination request during batch process.
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    _logger.debug("TDIC_FeedPivotalBatchProcess#requestTermination() - Entering")
    super.requestTermination()
    _logger.debug("TDIC_FeedPivotalBatchProcess#requestTermination() - Exiting")
    return true;
  }

  /**
   * GINTEG688
   * @param currentDate
   * @return: This method will return the list of Policy Periods which are active in the current window and has status = BOUND or has CANCELLATION Date
   */
  protected function getPoliciesForPivotal_TDIC(currentDate : Date): List<PolicyPeriod> {
    var query = Query.make(PolicyPeriod)
        .compare(PolicyPeriod#considerForPivotalInd_TDIC, Equals, true)
        .compare(PolicyPeriod#MostRecentModel, Equals, true)
        .or(\restriction -> {
          restriction.compare(PolicyPeriod#Status, Relop.Equals, PolicyPeriodStatus.TC_BOUND)
          restriction.compare(PolicyPeriod#CancellationDate, Relop.NotEquals, null)
        })
        .withLogSQL(true)
    return query.select().toList()
  }

  /**
   * GINTEG688
   * @param bopLocation
   * @param period
   * @return This method will return the List of Pivotal details
   */
  protected function populateFeedPivotalDTOForBOP_TDIC(bopLocation : BOPLocation, period : PolicyPeriod) : List<String> {
    _logger.debug("TDIC_FeedPivotalBatchProcess#populateFeedPivotalDTOForBOP_TDIC() - Start")
      var pivotalData = new ArrayList<String>()
      if((period.PrimaryNamedInsured.ContactDenorm typeis Person)){
        pivotalData.add((period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC == null ? "" :
            period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC)) //CDAMDA
      } else {
        pivotalData.add("")
      }
      if(period.BOPLine.BOPLocations.first().Buildings?.first()?.BOPBuildingCovExists){
        pivotalData.add(period.BOPLine.BOPLocations.first().Buildings?.first()?.BOPBuildingCov.BOPBldgLimTerm.ValueAsString) //BLDGCV // Building Coverage Limits/Value of the Building
      } else {
        pivotalData.add("") //BLDGCV // Building Coverage Limits/Value of the Building
      }
    //GINTEG-1271 : populating "Business Personal Property Coverage" value for CP-BOP policies.
      pivotalData.add(bopLocation.Buildings.first().BOPPersonalPropCovExists ? bopLocation.Buildings.first().BOPPersonalPropCov.BOPBPPBldgLimTerm.ValueAsString : "") //BSPRCV
      pivotalData.add(period.Policy.OriginalEffectiveDate.formatDate(gw.i18n.DateTimeFormat.SHORT)) //DTORIG
      var offeringCode = period.Offering.CodeIdentifier == pivotalConstants_TDIC.BOPLessorsRisk_TDIC.Code ? pivotalConstants_TDIC.LRP.Code : pivotalConstants_TDIC.BOP.Code
      pivotalData.add(period.BaseState.Code.concat(offeringCode)) // PLANCD
      pivotalData.add(period.PolicyNumber) //PLCYNR
      pivotalData.add(period.PeriodStart.formatDate(gw.i18n.DateTimeFormat.SHORT)) //PLCYYR
      pivotalData.add(period.EstimatedPremium_amt.toString()) //BLPREM
      pivotalData.add(bopLocation.Location.LocationNum.toString()) //PRPNBR
      pivotalData.add(period.PeriodDisplayStatus) //STATCD
      pivotalData.add(period.Policy.Product.ProductType.Code) //PTYPE
      pivotalData.add( "") //TDICST
      pivotalData.add( bopLocation.Location.AddressLine1) //ADRSS1
      pivotalData.add( bopLocation.Location.AddressLine2) //ADRSS2
      pivotalData.add( bopLocation.Location.AddressLine3) //ADRSS3
      pivotalData.add( bopLocation.Location.City) //PRCITY
      pivotalData.add( bopLocation.Location.State.Code) //STATE
      pivotalData.add( bopLocation.Location.PostalCode) //PRZIP9
      pivotalData.add( period.PrimaryInsuredName) //FULNAM
      pivotalData.add( period.Policy.OriginalEffectiveDate.formatDate(gw.i18n.DateTimeFormat.SHORT)) //DTAPPR
      pivotalData.add( "") //INVAMTPD
      pivotalData.add("") //PREAMTPD
      pivotalData.add( "") //PAIDFLAG
      try {
        var firstPaymentDate = period.FirstPaymentDate_TDIC
        pivotalData.add(firstPaymentDate != null ? firstPaymentDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "") //DTPAID
      } catch (e : Exception) {
        pivotalData.add("")
        _logger.error ("Failed to retrieve first payment date for " + period + " from BillingCenter: " + e);
      }
      pivotalData.add( "") //IVTAMTPD
      pivotalData.add(((period.PrimaryNamedInsured.ContactDenorm typeis Person) ? period.PrimaryNamedInsured.ContactDenorm.LicenseNumber_TDIC : "")) //LICNBR
      pivotalData.add( ((period.PeriodDisplayStatus == pivotalConstants_TDIC.Canceled.Code) ? period.CancellationDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "")) //DTCANCEL
      pivotalData.add( getPivotalCodeForCancReasonForBOP_TDIC(period.Cancellation.CancelReasonCode)) //RSNCODE
      pivotalData.add( "") //DCCOV
      pivotalData.add(bopLocation.Buildings.first().BOPDeductibleCov_TDICExists ? bopLocation.Buildings.first().BOPDeductibleCov_TDIC.BOPDeductibleOptionsTDICTerm?.ValueAsString: "") //DEDAMT
      pivotalData.add( (period.MultilineDiscount? pivotalConstants_TDIC.Y.Code : pivotalConstants_TDIC.N.Code)) //MULTILINE
      pivotalData.add(bopLocation.Buildings.first()?.BOPDentalGenLiabilityCov_TDICExists? pivotalConstants_TDIC.Y.Code : pivotalConstants_TDIC.N.Code) //GLCOV
      pivotalData.add(bopLocation.Buildings.first()?.BOPDentalGenLiabilityCov_TDICExists ? bopLocation.Buildings.first().BOPDentalGenLiabilityCov_TDIC.BOPGLLimits_TDICTerm?.PackageValue?.PackageCode : "") //GLLIMITS
    _logger.debug("TDIC_FeedPivotalBatchProcess#populateFeedPivotalDTOForBOP_TDIC() - End")
    return pivotalData
  }

  /**
   * GINTEG-688
   * @param cancellationReason
   * @return Cancellation reason - Pivotal Code mapper
   */
  protected  function getPivotalCodeForCancReasonForBOP_TDIC(cancellationReason : ReasonCode) : String {
    switch (cancellationReason){
      case TC_ESCROWFELLAPART_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_FOUNDOTHERINSURED_TDIC :
        return pivotalConstants_TDIC.CQ.Code
      case ReasonCode.TC_MOVEDPRACTICELOCATION_TDIC :
        return pivotalConstants_TDIC.CQ.Code
      case ReasonCode.TC_NONRENEWPOLICYHOLDERREQUEST_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_POLICYHOLDERREQUESTMISCELLANEOUS_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_PRACTICECLOSED_TDIC :
        return pivotalConstants_TDIC.CC.Code
      case ReasonCode.TC_REWRITTEN_TDIC :
        return pivotalConstants_TDIC.CI.Code
      case ReasonCode.TC_SOLD :
        return pivotalConstants_TDIC.CC.Code
      case ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC :
        return pivotalConstants_TDIC.CM.Code
      case ReasonCode.TC_OTHER_TDIC :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_NOTTAKEN :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_NONPAYMENT :
        return pivotalConstants_TDIC.CL.Code
      case ReasonCode.TC_FALSIFIEDDOCUMENTS_TDIC :
        return pivotalConstants_TDIC.CX.Code
      default:
        return ""
    }
  }

  /**
   * GINTEG688
   * @param period
   * Below method will write Pivotal data for Professional Liability Policies
   */
  protected function writePivotalDataForPL_TDIC(period : PolicyPeriod) {
    _logger.debug ("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Opening file")
    try {
      if(period.Offering.CodeIdentifier == pivotalConstants_TDIC.PLCyberLiab_TDIC.Code){
        //populate data for cyber liability
        _logger.trace("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Writing policy data started for Policy: " + period)
        period.PolicyLocations.each(\plLocation -> {
          var feedPivotalDTO = getFeedPivotalDataForCyber_TDIC(plLocation, period)
          _pivotalFileForCyber?.writeRow(feedPivotalDTO)
          _logger.trace("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Writing policy data completed for Policy: " + period)
          //GINTEG688 : Reset the flag once the related data has been written to Pivotal File
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            try {
              var trans = new com.guidewire.pl.system.transaction.BootstrapTransaction(new GWRunnable() {
                override function run() {
                  var sql = "update pc_policyperiod set considerForPivotalInd_TDIC = 0 where PolicyNumber = '"
                      + period.PolicyNumber + "'"
                  DatabaseDependenciesGateway.getDatabaseAccessor().update(sql)
                }
              })
              com.guidewire.pl.system.dependency.ServerDependencies.getTransactionManager().execute(trans)
            } catch (e : com.guidewire.pl.system.exception.TransactionException) {
              incrementOperationsFailed()
              _logger.error("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Exception caught in batch process for Policy "
                  + period.PolicyNumber +" "+ e.LocalizedMessage, e);
              EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
            }
          })
          incrementOperationsCompleted()
        })
      } else {
        _logger.trace("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Writing policy data started for Policy: " + period)
        period.PolicyLocations.each(\plLocation -> {
          var feedPivotalDTO = getFeedPivotalDataForPL_TDIC(plLocation, period)
          _pivotalFileForPL.writeRow(feedPivotalDTO)
          _logger.trace("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Writing policy data completed for Policy: " + period)
          //GINTEG-1241 : Reset the flag once the related data has been written to Pivotal File
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            try {
              var trans = new com.guidewire.pl.system.transaction.BootstrapTransaction(new GWRunnable() {
                override function run() {
                  var sql = "update pc_policyperiod set considerForPivotalInd_TDIC = 0 where PolicyNumber = '"
                      + period.PolicyNumber + "'"
                  DatabaseDependenciesGateway.getDatabaseAccessor().update(sql)
                }
              })
              com.guidewire.pl.system.dependency.ServerDependencies.getTransactionManager().execute(trans)
            } catch (e : com.guidewire.pl.system.exception.TransactionException) {
              incrementOperationsFailed()
              _logger.error("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Exception caught in batch process for Policy "
                  + period.PolicyNumber+": " + e.LocalizedMessage, e);
              EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
            }
          })
          incrementOperationsCompleted()
        })
      }
    } catch (e : Exception) {
      incrementOperationsFailed()
      _logger.error ("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Exception caught in batch process: " + e.LocalizedMessage, e);
      EmailUtil.sendEmail(_failureEmailAddresses, _failureEmailSubject, "Error writting files to Pivotal: " + e.Message);
    }
    _logger.debug ("TDIC_FeedPivotalBatchProcess#writePivotalDataForPL_TDIC() - Exiting")
  }

  /**
   * GINTEG688
   * @param plLocation
   * @param period
   * @return This method will return Pivotal Data as List for Professional Liability
   */
  protected function getFeedPivotalDataForPL_TDIC(plLocation : PolicyLocation, period : PolicyPeriod) : List<String> {
    _logger.debug("TDIC_FeedPivotalBatchProcess#getFeedPivotalDataForPL_TDIC() - Start")
    var pivotalData = new ArrayList<String>()
    pivotalData.add("") //CIGAMT

    if(period.PrimaryNamedInsured.ContactDenorm typeis Person) {
      pivotalData.add((period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC == null ? "" :
          period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC)) //CDANBR
    } else {
      pivotalData.add("") //CDANBR
    }

    if(period.GLLine.GLAggLimit_TDICExists) {
      //GINTEG-1268 : Corrected below condition to fix the missing data issue
      pivotalData.add(period.GLLine.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Value.toString()) //AGGLIM
    } else {
      pivotalData.add("") //AGGLIM
    }
    //GINTEG-1268 : Corrected below condition to fix the missing data issue
    if(period.GLLine.GLDentalEmpPracLiabCov_TDICExists) {
      pivotalData.add("1") //EPLIFLAG
      pivotalData.add(period.AllCosts.whereTypeIs(GLStateCost)?.where(\elt -> elt.StateCostType == GLStateCostType.TC_EPLI_TDIC)*.ActualAmount.sum().toNumber().DisplayValue) //EPLIPREM
      pivotalData.add(period.GLLine.GLDentalEmpPracLiabCov_TDIC.GLDEPLLimit_TDICTerm.Value.toString()) //EPLILIMITS
    } else {
      pivotalData.add("0") //EPLIFLAG
      pivotalData.add("") //EPLIPREM
      pivotalData.add("") //EPLILIMITS
    }
    pivotalData.add(period.getTotalPremiumRPT_amt()?.toString())//YRPREM
    pivotalData.add("") //DIVAMT
    //GWPS-1700: Dentists Proffessional Liability Per Claim Limit(Coverage A)
    if(period.GLLine.GLDentistProfLiabCov_TDICExists) {
      pivotalData.add(period.GLLine.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm?.Value?.toString())
    }else {
      pivotalData.add("")
    }
    pivotalData.add(period.Policy.OriginalEffectiveDate.formatDate(gw.i18n.DateTimeFormat.SHORT)) //DTORIG
    //GINTEG-1268 : Corrected below condition to fix the missing data issue
    var PLANCD = period.BaseState.Code + (period.Offering.CodeIdentifier == pivotalConstants_TDIC.PLClaimsMade_TDIC.Code ? plcm : ploc)
    pivotalData.add(PLANCD)//PLANCD
    pivotalData.add(period.PolicyNumber)//POLNBR
    pivotalData.add(period.PeriodStart.formatDate(gw.i18n.DateTimeFormat.SHORT))  //POLYR
    pivotalData.add(period.PeriodDisplayStatus)  //PSTAT
    pivotalData.add("")  //TDICST
    pivotalData.add(period.Policy.Product.CodeIdentifier)  //PTYPE
    pivotalData.add(period.PrimaryInsuredName)  //FULNAM
    //GWPS-1700 : WrittenDate will only be sent if its Submission Transaction otherwise empty string.
    if(period.Job typeis Submission) {
      pivotalData.add(period.WrittenDate.formatDate(gw.i18n.DateTimeFormat.SHORT))  //DTAPPR
    }else {
      pivotalData.add("")
    }
    pivotalData.add("")  //INVAMTPD
    pivotalData.add("")  //PREAMTPD
    pivotalData.add("")  //PAIDFLAG

    try {
      var firstPaymentDate = period.FirstPaymentDate_TDIC
      pivotalData.add(firstPaymentDate != null ? firstPaymentDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "")//DTPAID
    } catch (e : Exception) {
      pivotalData.add("") //DTPAID
      _logger.error ("Failed to retrieve first payment date for " + period + " from BillingCenter: " + e);
    }

    pivotalData.add("")  //IVTAMTPD
    pivotalData.add(((period.PrimaryNamedInsured.ContactDenorm typeis Person) ? period.PrimaryNamedInsured.ContactDenorm.LicenseNumber_TDIC : "")) //LICNBR
    pivotalData.add( ((period.PeriodDisplayStatus == pivotalConstants_TDIC.Canceled.Code) ? period.CancellationDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "")) //DTCANCEL
    pivotalData.add(getPivotalCodeCancReasonForPL_TDIC(period.Cancellation.CancelReasonCode)) //RSNCODE
    pivotalData.add("") //IDRCODE
    pivotalData.add((period.MultilineDiscount? "Y" : "N")) //MULTILINE
    _logger.debug("TDIC_FeedPivotalBatchProcess#getFeedPivotalDataForPL_TDIC() - End")
    return pivotalData
  }

  /**
   * GINTEG688
   * @param cancellationReason
   * @return Cancellation reason code - Pivotal Data mapping
   */
  protected  function getPivotalCodeCancReasonForPL_TDIC(cancellationReason : ReasonCode) : String {
    switch (cancellationReason){
      case ReasonCode.TC_NOLONGERINBUSINESSDEC_TDIC :
        return pivotalConstants_TDIC.CD.Code
      case ReasonCode.TC_EMPLOYEDBYGOVERNMENT_TDIC :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_EMPLOYERPROVIDINGINSURANCE_TDIC :
        return pivotalConstants_TDIC.CW.Code
      case ReasonCode.TC_FOUNDOTHERINSURER_TDIC :
        return pivotalConstants_TDIC.CQ.Code
      case ReasonCode.TC_INSUREDREQUESTDUETOMEMBERSHIPREQUIREMENT_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_MOVEDOUTOFCOUNTRY_TDIC :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_MOVEDOUTOFSTATE_TDIC :
        return pivotalConstants_TDIC.CO.Code
      case ReasonCode.TC_PERMANENTLYDISABLED_TDIC :
        return pivotalConstants_TDIC.CC.Code
      case ReasonCode.TC_POLICYHOLDERREQUESTMISCELLANEOUS_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_REWRITTEN_TDIC :
        return pivotalConstants_TDIC.CI.Code
      case ReasonCode.TC_TDICLEAVINGMARKET_TDIC :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_WENTBARE_TDIC :
        return pivotalConstants_TDIC.CG.Code
      case ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC :
        return pivotalConstants_TDIC.CM.Code
      case ReasonCode.TC_OTHER_TDIC :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_NONPAYMENT :
        return pivotalConstants_TDIC.CL.Code
      case ReasonCode.TC_NOTTAKEN :
        return pivotalConstants_TDIC.CX.Code
      case ReasonCode.TC_FALSIFIEDDOCUMENTS_TDIC :
        return pivotalConstants_TDIC.CX.Code
      default:
        return ""
    }
  }

  /**
   * GINTEG-688
   * @param cyberLocation
   * @param period
   * @return Below method will populate Pivotal Data for Cyber Liability Policies
   */
  protected function getFeedPivotalDataForCyber_TDIC(cyberLocation : PolicyLocation, period : PolicyPeriod) : List<String> {
    _logger.debug("TDIC_FeedPivotalBatchProcess#getFeedPivotalDataForCyber_TDIC() - Start")
    var pivotalData = new ArrayList<String>()
    if(period.PrimaryNamedInsured.ContactDenorm typeis Person){
      pivotalData.add((period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC == null ? "" :
          period.PrimaryNamedInsured.ContactDenorm.ADANumberOfficialID_TDIC)) //CDAMDA
    } else {
      pivotalData.add("")   //CDAMDA
    }
    /*if(cyberLocation.Buildings?.first()?.){
      pivotalData.add(period.BOPLine.BOPLocations.first().Buildings?.first()?.BOPBuildingCov.BOPBldgLimTerm.ValueAsString) //BLDGCV // Building Coverage Limits/Value of the Building
    } else {*/
      pivotalData.add("") //BLDGCV // Building Coverage Limits/Value of the Building
    //}
    pivotalData.add("") //BSPRCV
    pivotalData.add(period.Policy.OriginalEffectiveDate.formatDate(gw.i18n.DateTimeFormat.SHORT)) //DTORIG
    pivotalData.add(period.BaseState.Code + pivotalConstants_TDIC.CYB.Code) //PLANCD
    pivotalData.add(period.PolicyNumber) //PLCYNR
    pivotalData.add(period.PeriodStart.formatDate(gw.i18n.DateTimeFormat.SHORT)) //PLCYYR
    pivotalData.add(period.TotalPremiumRPT_amt.toString()) //BLPREM
    pivotalData.add(cyberLocation.LocationNum.toString()) //PRPNBR
    pivotalData.add(period.PeriodDisplayStatus) //STATCD
    pivotalData.add(period.Policy.Product.ProductType.Code) //PTYPE
    pivotalData.add( "") //TDICST
    pivotalData.add( cyberLocation.AddressLine1) //ADRSS1
    pivotalData.add( cyberLocation.AddressLine2) //ADRSS2
    pivotalData.add( cyberLocation.AddressLine3) //ADRSS3
    pivotalData.add( cyberLocation.City) //PRCITY
    pivotalData.add( cyberLocation.State.Code) //STATE
    pivotalData.add( cyberLocation.PostalCode) //PRZIP9
    pivotalData.add( period.PrimaryInsuredName) //FULNAM
    pivotalData.add( period.Policy.OriginalEffectiveDate.formatDate(gw.i18n.DateTimeFormat.SHORT)) //DTAPPR
    pivotalData.add( "") //INVAMTPD
    pivotalData.add("") //PREAMTPD
    pivotalData.add( "") //PAIDFLAG
    try {
      var firstPaymentDate = period.FirstPaymentDate_TDIC
      pivotalData.add(firstPaymentDate != null ? firstPaymentDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "") //DTPAID
    } catch (e : Exception) {
      pivotalData.add("")
      _logger.error ("Failed to retrieve first payment date for " + period + " from BillingCenter: " + e);
    }
    pivotalData.add( "") //IVTAMTPD
    pivotalData.add(((period.PrimaryNamedInsured.ContactDenorm typeis Person) ? period.PrimaryNamedInsured.ContactDenorm.LicenseNumber_TDIC : "")) //LICNBR
    pivotalData.add( ((period.PeriodDisplayStatus == pivotalConstants_TDIC.Canceled.Code) ? period.CancellationDate.formatDate(gw.i18n.DateTimeFormat.SHORT) : "")) //DTCANCEL
    pivotalData.add( getPivotalCodeForCancReasonForBOP_TDIC(period.Cancellation.CancelReasonCode)) //RSNCODE
    pivotalData.add( "") //DCCOV
    pivotalData.add(period.GLLine.GLCyberLiabDedCov_TDICExists ? period.GLLine.GLCyberLiabDedCov_TDIC.GLCybLiabDedLimit_TDICTerm?.Value?.asString() : "") //DEDAMT
    pivotalData.add( (period.MultilineDiscount ?  pivotalConstants_TDIC.Y.Code : pivotalConstants_TDIC.N.Code)) //MULTILINE
    pivotalData.add(period.GLLine.GLCyberLiabCov_TDICExists ? pivotalConstants_TDIC.Y.Code : pivotalConstants_TDIC.N.Code) //GLCOV
    pivotalData.add(period.GLLine.GLCyberLiabCov_TDICExists ? period.GLLine.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm?.Value?.asString() : "") //GLLIMITS
    _logger.debug("TDIC_FeedPivotalBatchProcess#getFeedPivotalDataForCyber_TDIC() - End")
    return pivotalData
  }
}