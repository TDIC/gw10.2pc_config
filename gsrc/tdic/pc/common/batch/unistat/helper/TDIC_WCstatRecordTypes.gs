package tdic.pc.common.batch.unistat.helper

uses java.util.ArrayList

uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatAddressFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatExposureFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatLossFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatNameFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatPolicyHeaderFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatUnitTotalFields
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/19/14
 * Time: 1:08 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatRecordTypes {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")

  var _policyHeaderRecord : TDIC_WCstatPolicyHeaderFields as PolicyHeaderRecord
  var _nameRecord : TDIC_WCstatNameFields as NameRecord
  var _addressRecord : TDIC_WCstatAddressFields as AddressRecord
  var _exposureRecord : ArrayList<TDIC_WCstatExposureFields> as ExposureRecord = new ArrayList<TDIC_WCstatExposureFields>()
  var _lossRecord : ArrayList<TDIC_WCstatLossFields> as LossRecord
  var _unitTotalsRecord : TDIC_WCstatUnitTotalFields as UnitTotalsRecord

  function getCount():int{

    var count = 0
    if(this._unitTotalsRecord != null ) count++
    if(this._policyHeaderRecord != null) count++
    if(this._nameRecord != null) count++
    if(this._addressRecord != null) count++
    count += this._exposureRecord.Count
    if(this._lossRecord != null) count+= this._lossRecord.Count
    return count
  }


  function convertToFlatFileRecord(records:Record[]) :String{
    var flatFileContents=""

    for(record in records){
      _logger.debug("************************"+record.RecordType+"*****************************")
      switch (record.RecordType) {
        case "Policy Header Record":
            flatFileContents += PolicyHeaderRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Name Record":
            flatFileContents += NameRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Address Record":
            flatFileContents += AddressRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Loss Record":
            LossRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Exposure Record":
            ExposureRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Unit Total Record":
            flatFileContents += UnitTotalsRecord.createFlatFileLine(record)+"\r\n"
            break
          default:
          break
      }

    }
    return flatFileContents

  }



}