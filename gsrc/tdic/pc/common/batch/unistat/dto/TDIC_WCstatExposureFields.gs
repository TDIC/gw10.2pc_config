package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/18/14
 * Time: 2:04 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatExposureFields extends TDIC_FlatFileLineGenerator{

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _exposureStateCode : int as ExposureStateCode
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _reportLevelCode : int as ReportLevelCode
  var _correctionSeqNum : String as CorrectionSeqNum
  var _recordTypeCode : int as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _classificationCode : String as ClassificationCode
  var _futureReserved2 : String as FutureReserved2
  var _experienceModFactor : String as ExperienceModFactor
  var _experienceModEffDate : String as ExperienceModEffDate
  var _rateEffdate : String as RateEffDate
  var _exposureAmount : long as ExposureAmount
  var _premiumAmount : String as PremiumAmount
  var _manualChargedRate : String as ManualChargedRate
  var _splitPeriodCode : int as SplitPeriodCode
  var _futureReserved3 : String as FutureReserved3
  var _ratingTieridCode : String as RatingTierIdCode
  var _futureReserved4 : String as FutureReserved4
  var _updateTypeCode : String as UpdateTypeCode
  var _futureReserved5 : String as FutureReserved5
  var _exposureActOrCovCode : int as ExposureActorCovCode
  var _futureReserved6 : String as FutureReserved6

}