package tdic.pc.common.batch.unistat.util

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses tdic.pc.common.batch.unistat.helper.TDIC_WCstatRecordTypes
uses java.util.ArrayList
uses gw.api.database.Query
uses tdic.pc.common.batch.unistat.helper.TDIC_WCStatCompleteRecord
uses java.lang.Exception
uses java.io.File

uses tdic.util.flatfilegenerator.TDIC_FlatFileUtilityHelper
uses java.text.SimpleDateFormat
uses gw.pl.exception.GWConfigurationException
uses com.tdic.util.misc.EmailUtil
uses java.sql.SQLException
uses java.io.IOException
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/25/15
 * Time: 4:33 PM
 * Batch for Workers Compensation unit statistical reporting subsequent reports, creates flat file and reports all the policies with claims.
 */
class TDIC_WCstatBatchUtil {
  private static var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")
  private static final var BATCH_USER = "iu"
//GINTEG-219: Reading properties from PropertyUtil rather from CacheManager
  public static final var WC_STAT_POL_NOTIFICATION_EMAIL : String = PropertyUtil.getInstance().getProperty("WCSTATPOLNotificationEmail")
  static final var _job = "Job"
  static final var _subtype = "Subtype"
  static final var _policy = "Policy"
  static final var _policyterm = "PolicyTerm"
  static final var _statreportcomp = "StatReportComp_TDIC"
  static final var _statreportcnt = "StatReportCnt_TDIC"
  static final var _issuedate = "IssueDate"
  static final var _periodstart = "PeriodStart"
  static final var _completed = "Completed"
  static final var _batchfailed = "WCSTAT Subsequent Report batch job failed on server:"

  public static function doWorkForMonthsReport(numMonths:int,reportNum:String){

    try
    {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {

        var wcstatRecords:ArrayList<TDIC_WCstatRecordTypes> = new ArrayList<TDIC_WCstatRecordTypes>()

        var dat = gw.api.util.DateUtil.currentDate()
        var pStart = dat.addMonths(-1*numMonths).FirstDayOfMonth.trimToMidnight()
        _logger.info("Policy effective start date :" + pStart)
        var pEnd = pStart.addMonths(1).trimToMidnight()
        _logger.info("Policy effective start date :" + pEnd)

        var policyQuery = Query.make(PolicyPeriod)
        var auditQuery = policyQuery.join(_job)
        auditQuery.compare(_subtype,Equals,typekey.Job.TC_AUDIT)
        var ppJoinQuery = policyQuery.join(_policy)
        var ptJoinQuery = policyQuery.join(_policyterm)
        ptJoinQuery.compare(_statreportcomp,Equals,false)
        ptJoinQuery.compare(_statreportcnt, GreaterThan,0)
        ptJoinQuery.compare(_statreportcnt,LessThan,11)
        ppJoinQuery.compare(_issuedate ,NotEquals,null)
        policyQuery.compare(_periodstart,GreaterThanOrEquals,pStart)
        policyQuery.compare(_periodstart,LessThan,pEnd)

        var res = ppJoinQuery.select().toSet()
          res.each( \ elt -> {
          elt = bundle.add(elt)
          if(elt == elt.CompletedNotReversedFinalAudits.firstWhere( \ faud -> faud.DisplayStatus.equalsIgnoreCase(_completed)).Audit.PolicyPeriod) {
          var record = TDIC_WCstatUtil.createPayload(elt)
          wcstatRecords.add(record)

          if(record.LossRecord.where( \ lr -> lr.ClaimOrStatusCode.equalsIgnoreCase("0")).Count>0){
            elt.PolicyTerm.StatReportComp_TDIC=false
            elt.PolicyTerm.StatReportCnt_TDIC +=1
          }
          else {
            elt.PolicyTerm.StatReportComp_TDIC=true
            elt.PolicyTerm.StatReportCnt_TDIC +=1
          }
          }
        })

        var completeRecord = new TDIC_WCStatCompleteRecord()
        completeRecord.ReportHeaderRecord = TDIC_WCstatUtil.createReportheaderFields()

        completeRecord.WCStatRecords = wcstatRecords
        var recordCount=1
        wcstatRecords.each( \ elt -> {
          recordCount +=elt.getCount()
        })

        completeRecord.ReportTrailerRecord = TDIC_WCstatUtil.createReportTrailerrecord(recordCount,wcstatRecords.Count)
        writeFlatFile(completeRecord,reportNum)
      },BATCH_USER)
    }
    catch (io:IOException) {
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - IOException= " + io)
      if(TDIC_WCstatUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else{
        EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, _batchfailed + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Subsequent Report batch job failed due to error:" + io.LocalizedMessage)
      }
      throw io
    }catch (sql:SQLException) {
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - SQLException= " + sql)
      if(TDIC_WCstatUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, _batchfailed + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Subsequent Report batch job failed due to error:" + sql.LocalizedMessage)
      }
      throw sql
    }catch(ex:Exception){
      _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport() - Exception= " + ex)
      if(TDIC_WCstatUtil.EMAIL_RECIPIENT == null){
        _logger.error("TDIC_WCstatBatchUtil#doWorkForMonthsReport - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, _batchfailed + gw.api.system.server.ServerUtil.ServerId, "WCSTAT Subsequent Report batch job failed due to error:" + ex.LocalizedMessage)
      }
      throw ex
    }
  }

  /**
   * Writes the generated encoded records to a specified file path
   */
  static function writeFlatFile(completeRecord:TDIC_WCStatCompleteRecord, reportNo:String){

    var sdf = new SimpleDateFormat("yyyyMMdd")
    var effDate = sdf.format(gw.api.util.DateUtil.currentDate()).toString().replaceAll("[^0-9]", "")

    //GINTEG-822 : Reading "excelHeaderPath" through ConfigAccess to avoid errors during file read
    var headerPath = ConfigAccess.getConfigFile(PropertyUtil.getInstance().getProperty("excelHeaderPath")).Path
    if(headerPath == null){
      throw new GWConfigurationException("TDIC_WCstatBatchUtil#writeFlatFile - Cannot retrieve header path with the key 'excelHeaderPath' from integration database.")
    }
    var excelPath = ConfigAccess.getConfigFile(PropertyUtil.getInstance().getProperty("srStatExcelPath")).Path
    if(excelPath == null){
      throw new GWConfigurationException("TDIC_WCstatBatchUtil#writeFlatFile - Cannot retrieve excel path with the key 'srStatExcelPath' from integration database.")
    }
    var destDir = PropertyUtil.getInstance().getProperty("statDestDirPath")
    if(destDir == null){
      throw new GWConfigurationException("TDIC_WCstatBatchUtil#writeFlatFile - Cannot retrieve destination directory path with the key 'statDestDirPath' from integration database.")
    }

    if(!new File(destDir).exists()){
      _logger.error("TDIC_WCstatBatchUtil#writeFlatFile - Cannot Access destDir. The Directory May Not Exist or There is an Error in network Connection")
      if(TDIC_WCstatUtil.EMAIL_RECIPIENT == null) {
        _logger.error("TDIC_WCstatBatchUtil#writeFlatFile - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      }
      else {
        EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "WCSTAT Subsequent Report batch job failed on server:" + gw.api.system.server.ServerUtil.ServerId, "Unable to access path:" + destDir + ": The Directory May Not Exist or There is an Error in network Connection.")
      }
    }

    var flatFileName = "WCSTAT"+reportNo+effDate+".txt"
    var destFile = destDir+flatFileName

    var flatFileUtility = new TDIC_FlatFileUtilityHelper()
    var vendorStructure = flatFileUtility.encodeandReturnEncodedString(excelPath,headerPath,destFile)
    var fileContents = completeRecord.convertToFlatFileRecord(vendorStructure?.toTypedArray())
    var fileGenerateStatus = flatFileUtility.writeEncodedFlatfile(fileContents,destFile)
    if(!fileGenerateStatus){         //Error in flat file generation
      _logger.debug("File not generated")
    }

  }


}