package tdic.pc.common.batch.earnedpremium.dto

uses java.math.BigDecimal
uses java.util.Date

/**
 *
 * 01/06/2015 Kesava Tavva
 *
 * Object to hold Earned Premium details such as PolicyNumber, WC7Transaction details, Calculated Earned premium etc.,
 */
class TDIC_EarnedPremiumObject {
  private var _policyNumber: String as PolicyNumber
  private var _termNumber: int as TermNumber
  private var _reportingMonth: String as ReportingMonth
  private var _reportingYear: int as ReportingYear
  private var _batchProcessDate: Date as BatchProcessDate
  private var _transactionID: int as TransactionID
  private var _transactionDate: Date as TransactionDate
  private var _transEffectiveDate: Date as TransactionEffectiveDate
  private var _lob: String as LineOfBusiness
  private var _state: String as State
  private var _rateCode: String as RateCode
  private var _amount: BigDecimal as Amount
}