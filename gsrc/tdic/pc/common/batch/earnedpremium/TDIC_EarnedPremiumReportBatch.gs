package tdic.pc.common.batch.earnedpremium

uses gw.api.database.Query
uses gw.api.database.IQueryBeanResult
uses tdic.pc.common.batch.earnedpremium.dto.TDIC_EarnedPremiumObject
uses com.tdic.util.misc.EmailUtil
uses java.lang.Exception
uses java.math.BigDecimal
uses java.text.DateFormatSymbols
uses java.util.ArrayList
uses java.util.Date
uses java.util.Calendar
uses java.util.GregorianCalendar
uses java.util.List
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths
uses gw.api.upgrade.Coercions

class TDIC_EarnedPremiumReportBatch extends TDIC_EarnedPremiumReportBatchBase {
  private static final var CLASS_NAME = "TDIC_EarnedPremiumReportBatch"
  private static final var BATCH_USER = "iu"
  private var monthName: String
  private static final var EP_CALC_DAYS = 365
  private var dayOfLeapYear: Date
  private static final var downloadDate = getPolicyDownloadDate()

  construct(args : Object[]){
    super(args)
  }

  /**
   *
   * 12/29/2015 Kesava Tavva
   *
   * This function overrides dowork() method to process Earned Premiums for qualified policies
   * for a given reporting month.
   */
  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.trace("${logPrefix} - Entering")
    try{
      _logger.info("${logPrefix} started for the Period: ${PeriodStartDate} to ${PeriodEndDate}")
      var startDate = getBatchPeriodStartDate()
      var endDate = getBatchPeriodEndDate(startDate)
      dayOfLeapYear = getLeapYearDay()
      monthName = new DateFormatSymbols().getShortMonths()[startDate.MonthOfYear-1]

      if(isEarnedPremiumDataAvailable()){
        var message = "Earned Premium data already present in report table for the period ${monthName}, ${PeriodStartDate.YearOfDate}."
        _logger.warn("${logPrefix} - ${message}")
        EmailUtil.sendEmail(EmailTo, "INFO::${gw.api.system.server.ServerUtil.ServerId}-PC ${message}",
            "${message} Delete Earned premium details for this period from the table if it needs to be reprocessed.")
        _logger.info("${logPrefix} completed for the Period: : ${PeriodStartDate} to ${PeriodEndDate}")
        return
      }
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before retrievePolicyPeriods(batchPSD, batchPED)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC EarnedPremiumReport batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      var policyPeriods = retrievePolicyPeriods(startDate, endDate)

      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before populateEarnedPremiums(policyPeriods)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC EarnedPremiumReport batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      populateEarnedPremiums(policyPeriods, startDate, endDate)
	    policyPeriods = null

      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before retrieveAuditPolicyPeriods(batchPSD, batchPED)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC EarnedPremiumReport batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }

      var auditPolicyPeriods = retrieveAuditPolicyPeriods(startDate, endDate)
      _logger.info("${logPrefix} auditPolicyPeriods found  for batch run period ${startDate} to ${endDate} - ${auditPolicyPeriods.Count}")
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested during doWork() method(before populateEarnedPremiums(policyPeriods)) for completed audits.")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC EarnedPremiumReport batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      populateEarnedPremiums(auditPolicyPeriods, startDate, endDate)
	    auditPolicyPeriods = null
	  
      EmailUtil.sendEmail(EmailTo, getEmailSubject(Boolean.TRUE), "PC EarnedPremiumReport batch process completed successfully for the Period: ${PeriodStartDate} to ${PeriodEndDate}.")
      _logger.info("${logPrefix} completed for the Period: : ${PeriodStartDate} to ${PeriodEndDate}")
    }catch(e: Exception){
      EmailUtil.sendEmail(EmailRecipient, getEmailSubject(Boolean.FALSE), "PC EarnedPremiumReport batch completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}.\n${e.StackTraceAsString}")
      throw new Exception("${logPrefix}- completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}",e)
    }
    _logger.trace("${logPrefix}- Exiting")
  }

  /**
   *
   * 01/04/2016 Kesava Tavva
   *
   * This function calculates earned premiums for each of the transactions
   * available for qualified police periods for a given reporting period
   */
  private function populateEarnedPremiums(policyPeriods: IQueryBeanResult<PolicyPeriod>, batchPSD: Date, batchPED: Date){
    var logPrefix = "${CLASS_NAME}#populateEarnedPremiumsForReport()"
    _logger.trace("${logPrefix} - Entering")
    var earnedPrmDays : int
    var numDaysInPeriod : int
    var batchProcessDate = new Date()
    var earnedPremiums = new ArrayList<TDIC_EarnedPremiumObject>()
    policyPeriods.orderBy( 
      QuerySelectColumns.path(Paths.make(entity.PolicyPeriod#PolicyNumber))
    )
    policyPeriods.thenBy( 
      QuerySelectColumns.path(Paths.make(entity.PolicyPeriod#TermNumber))
    )
    var cancellationPeriods = policyPeriods.where( \ pPeriod -> (pPeriod.Job.Subtype == typekey.Job.TC_CANCELLATION))
    var cPolicyNumbers = cancellationPeriods*.PolicyNumber
    var cPeriods:List<PolicyPeriod> = null
    var polNumber: String = null
    var termNumber = 0

    policyPeriods?.each( \ pp -> {
      _logger.debug("${logPrefix} - ${pp}; ${pp.ModelDate}; ${pp.EditEffectiveDate}; ${pp.PeriodEnd}; ${pp.CancellationDate}")
      var pl = pp.RepresentativePolicyLines?.first()
        if(pl typeis productmodel.WC7Line){
          //Find earned premium days
          numDaysInPeriod = getNumDaysInPeriod(pp, batchPSD, batchPED)
          if(pp.Job.Subtype == typekey.Job.TC_AUDIT )
            earnedPrmDays =  numDaysInPeriod
          else if(pp.Job.Subtype == typekey.Job.TC_CANCELLATION){
            if((pp.ModelDate >= batchPSD and pp.ModelDate <= batchPED)
                or (pp.CancellationDate >= batchPSD and pp.CancellationDate <= batchPED)){
                  earnedPrmDays = getNumDaysInPeriod(pp, batchPSD, batchPED)
            }else
              earnedPrmDays = 0
          }else{
            if(pp.PeriodEnd > batchPSD){
              if(not(polNumber == pp.PolicyNumber and termNumber == pp.TermNumber)){
                polNumber = pp.PolicyNumber
                termNumber = pp.TermNumber
                if(cPolicyNumbers?.contains(pp.PolicyNumber)){
                  cPeriods = cancellationPeriods?.where( \ pPeriod -> (pPeriod.PolicyNumber == pp.PolicyNumber and pPeriod.TermNumber == pp.TermNumber))
                }
                else
                  cPeriods = null
              }
              earnedPrmDays = getEarnedPremiumDays(cPeriods, pp, batchPSD, batchPED)
            }else
              earnedPrmDays = getEarnedPremiumDays(pp, batchPSD, batchPED)
          }

          if(earnedPrmDays != 0){
            pl.WC7Transactions?.each( \ wc7Trans -> {
              var amount = DECIMAL_FORMAT.format((wc7Trans.Amount.Amount*earnedPrmDays)/numDaysInPeriod)
              earnedPremiums.add(new TDIC_EarnedPremiumObject()
                  { :PolicyNumber =pp.PolicyNumber,:TermNumber=pp.TermNumber,:ReportingMonth=monthName,
                      :ReportingYear=batchPSD.YearOfDate,:BatchProcessDate=batchProcessDate,:TransactionID=Coercions.makePIntFrom("${wc7Trans.ID}"),
                      :TransactionDate=wc7Trans.CreateTime, :TransactionEffectiveDate=wc7Trans.EffDate,:LineOfBusiness=pl.Subtype.Description,
                      :State=wc7Trans.WC7Cost.JurisdictionState.Code,:RateCode=safeRateCode(wc7Trans.WC7Cost.Description),:Amount=new BigDecimal(amount)
                  })
            })
          }
        }
      })
    OperationsExpected = earnedPremiums?.size()
    _logger.info("${logPrefix}- Earned premiums calculation for report completed. Count:${OperationsExpected}")
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during populateEarnedPremiums() method(before inserting earned premium details to reprot table).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC EarnedPremiumReport batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }

    try{

      if(earnedPremiums.HasElements){
        _logger.info("${logPrefix}- Writing Earned premium details to reporting table started.")
        gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
          earnedPremiums.each( \ ep -> {
            bundle.add(new EarnedPremiumReport_TDIC()
                { :PolicyNumber =ep.PolicyNumber,:TermNumber=ep.TermNumber,:ReportingMonth=ep.ReportingMonth,
                    :ReportingYear=ep.ReportingYear,:BatchProcessDate=ep.BatchProcessDate,:TransactionID=ep.TransactionID,
                    :TransactionDate=ep.TransactionDate, :TransactionEffectiveDate=ep.TransactionEffectiveDate,:LineOfBusiness=ep.LineOfBusiness,
                    :State=ep.State,:RateCode=ep.RateCode,:Amount=ep.Amount
                })
          })
        },BATCH_USER)
        OperationsCompleted++
        _logger.info("${logPrefix}- Writing Earned premium details to reporting table is complete.")
      }
    }catch(e: Exception){
      OperationsFailed = OperationsExpected
      throw e
    }finally{
      earnedPremiums.clear()
    }

    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   *
   * 01/04/2016 Kesava Tavva
   *
   * This function builds query to retrieve all qualified policy periods for a given batch period
   * start and end dates and returns list of those policy periods.
   */
  @Param("batchPSD","Batch period start date")
  @Param("batchPED","Batch period end date")
  @Returns("Returns list of qualified policy periods")
  protected function retrievePolicyPeriods(batchPSD : Date, batchPED : Date) : IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrievePolicyPeriods(batchPSD, batchPED)"
    _logger.trace("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.compare("ModelDate",NotEquals,null)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE)
                                          .compare(PolicyLine#EffectiveDate,Equals,null)
    ppQuery.or(\ or1 -> {
      or1.and(\andE ->{
        andE.compare("EditEffectiveDate",LessThanOrEquals,batchPED)
        andE.compare("PeriodEnd",GreaterThan, batchPSD)
        andE.compare("ModelDate",LessThanOrEquals,batchPED)
      })
      or1.and(\ andT ->{
        andT.between("ModelDate",batchPSD,batchPED)
        andT.compare("PeriodEnd",LessThanOrEquals, batchPSD)
      })
    })

    _logger.trace("${logPrefix} - Exiting")
    return ppQuery.select()
  }

  /**
   *
   * 10/25/2016 Kesava Tavva
   *
   * This function builds query to retrieve all qualified completed audit policy periods for a given batch period
   * start and end dates and returns list of those policy periods.
   */
  @Param("batchPSD","Batch period start date")
  @Param("batchPED","Batch period end date")
  @Returns("Returns list of qualified policy periods")
  protected function retrieveAuditPolicyPeriods(batchPSD : Date, batchPED : Date) : IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrieveAuditPolicyPeriods(batchPSD, batchPED)"
    _logger.trace("${logPrefix}- Entering")
    _logger.info("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_AUDITCOMPLETE)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE)
        .compare(PolicyLine#EffectiveDate,Equals,null)
    if(batchPSD <= downloadDate and batchPED >= downloadDate)
      ppQuery.join(PolicyPeriod#Job).compare(Job#CloseDate,LessThanOrEquals,batchPED)
    else
      ppQuery.join(PolicyPeriod#Job).between(Job#CloseDate,batchPSD,batchPED)

    _logger.trace("${logPrefix} - Exiting")
    return ppQuery.select()
  }

  /**
   *
   * 01/05/2016 Kesava Tavva
   *
   * This function finds active days in a given batch period.
   */
  @Param("pp","Policy period")
  @Param("batchPSD", "WC7Transaction lines for each Policy period's policy line type")
  @Param("batchPED","Map object that represents aggregated amounts and units")
  @Returns("int value of active days in a given batch period")
  private function getEarnedPremiumDays(pp : PolicyPeriod, batchPSD : Date, batchPED : Date) : int {
    var epDays = 0
    var epStartDate: Date = null
    var epEndDate: Date = null
    if((pp.CreateTime >= batchPSD or pp.ModelDate >= batchPSD) and pp.PeriodEnd < batchPSD){
      epStartDate = pp.EditEffectiveDate
      epEndDate = pp.PeriodEnd
    }else if((pp.CreateTime >= batchPSD or pp.ModelDate >= batchPSD) and pp.EditEffectiveDate < batchPSD){
      if(pp.PeriodEnd > batchPED){
        epStartDate = pp.EditEffectiveDate
        epEndDate = batchPED
      }
      else{
        epStartDate = pp.EditEffectiveDate
        epEndDate = pp.PeriodEnd
      }
    }else{
      if(pp.EditEffectiveDate <= batchPSD){
        if(pp.PeriodEnd > batchPED){
          epStartDate = batchPSD
          epEndDate = batchPED
        }
        else{
          epStartDate = batchPSD
          epEndDate = pp.PeriodEnd
        }
      }else{
        if(pp.PeriodEnd > batchPED){
          epStartDate = pp.EditEffectiveDate
          epEndDate = batchPED
        }
        else{
          epStartDate = pp.EditEffectiveDate
          epEndDate = pp.PeriodEnd
        }
      }
    }

    epDays = epEndDate.daysBetween(epStartDate)
    if(pp.PeriodEnd.daysBetween(pp.EditEffectiveDate) > EP_CALC_DAYS){
      if(epStartDate <= dayOfLeapYear and epEndDate>=dayOfLeapYear)
        epDays = epDays-1
    }

    return (epDays > EP_CALC_DAYS) ? EP_CALC_DAYS : epDays
  }

  /**
   *
   * 01/05/2016 Kesava Tavva
   *
   * This function finds number of days in a policy period
   */
  @Param("pp","Policy period")
  @Param("batchPSD", "WC7Transaction lines for each Policy period's policy line type")
  @Param("batchPED","Map object that represents aggregated amounts and units")
  @Returns("int, number of days in a policy period")
  private function getNumDaysInPeriod(pp : PolicyPeriod, batchPSD : Date, batchPED : Date) : int {
     if(pp.PeriodEnd.daysBetween(pp.EditEffectiveDate) > EP_CALC_DAYS)
      return EP_CALC_DAYS
     else
      return pp.PeriodEnd.daysBetween(pp.EditEffectiveDate)
  }

  /**
   *
   * 01/07/2016 Kesava Tavva
   *
   * This function finds whether Earned Premium data already available for given period
   */
  @Returns("Boolean, Result of data find")
  private function isEarnedPremiumDataAvailable():Boolean{
    return Query.make(EarnedPremiumReport_TDIC).compareIgnoreCase(EarnedPremiumReport_TDIC#ReportingMonth,Equals,monthName)
    .compare(EarnedPremiumReport_TDIC#ReportingYear,Equals,PeriodStartDate.YearOfDate).select().HasElements
  }

  /**
   *
   * 11/16/2016 Kesava Tavva
   *
   * Get leap year day of current batch period year
   */
  private function getLeapYearDay(): Date{
    var  cal = new GregorianCalendar()
    if(cal.isLeapYear(getBatchPeriodStartDate().YearOfDate)){
      cal.set(Calendar.YEAR, getBatchPeriodStartDate().YearOfDate)
      cal.set(Calendar.MONTH, Calendar.FEBRUARY)
      cal.set(Calendar.DATE, 29)
    }
    return cal.getTime().trimToMidnight()
  }

  /**
   *
   * 11/16/2016 Kesava Tavva
   *
   * Function to return first day of legacy policies available in Production.
   * This date used to identify Audit periods reporting in earned premium report.
   */
  private static function getPolicyDownloadDate(): Date{
    var  cal = new GregorianCalendar()
    cal.set(Calendar.YEAR, 2016)
    cal.set(Calendar.MONTH, Calendar.AUGUST)
    cal.set(Calendar.DATE, 28)
    return cal.getTime().trimToMidnight()
  }

  /**
   *
   * 01/18/2017 Kesava Tavva
   *
   * This function finds active days in a given batch period.
   */
  @Param("policyPeriods","List of Policy periods to be reported for earned premium")
  @Param("pp","Policy period")
  @Param("batchPSD", "WC7Transaction lines for each Policy period's policy line type")
  @Param("batchPED","Map object that represents aggregated amounts and units")
  @Returns("int value of active days in a given batch period")
  private function getEarnedPremiumDays(cPeriods: List<PolicyPeriod>, pp : PolicyPeriod, batchPSD : Date, batchPED : Date) : int {
    var epDays = 0
    if(cPeriods.HasElements){//Cancellation periods present for this policy period
      var cPeriod = cPeriods.maxBy( \ elt -> (elt.ModelDate))
      if(pp.ModelDate > cPeriod.ModelDate){
        epDays = getEarnedPremiumDays(pp, batchPSD, batchPED)
      }else{
        if(pp.ModelDate >= batchPSD and pp.ModelDate <= batchPED){
          epDays =  getNumDaysInPeriod(pp, batchPSD, batchPED)
        }else{
          if((cPeriod.CancellationDate >= batchPSD and cPeriod.CancellationDate <= batchPED)
            or (cPeriod.ModelDate >= batchPSD and cPeriod.ModelDate <= batchPED)){
            if(cPeriods.Count > 1){
              if(cPeriods.where( \ cp -> cp.ModelDate.YearOfDate < cPeriod.ModelDate.YearOfDate or cp.ModelDate.MonthOfYear < cPeriod.ModelDate.MonthOfYear)?.HasElements
                  and (pp.Job.Subtype == typekey.Job.TC_SUBMISSION or pp.Job.Subtype == typekey.Job.TC_RENEWAL)){
                epDays = 0
              }else
                epDays = pp.PeriodEnd.daysBetween(batchPSD)
            }else
              epDays = pp.PeriodEnd.daysBetween(batchPSD)
          }else{
            epDays = 0
          }
        }
      }
    }else
      epDays = getEarnedPremiumDays(pp, batchPSD, batchPED)

    return (epDays > EP_CALC_DAYS) ? EP_CALC_DAYS : epDays
  }
}
