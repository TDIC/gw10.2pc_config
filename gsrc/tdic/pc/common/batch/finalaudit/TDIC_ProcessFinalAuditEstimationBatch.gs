package tdic.pc.common.batch.finalaudit

uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.transaction.Transaction
uses gw.api.database.DBFunction
uses java.lang.Exception
uses com.tdic.util.misc.EmailUtil
uses gw.api.database.IQueryBeanResult
uses org.slf4j.LoggerFactory

/**
 * DE43 - related to US463
 * Shane Sheridan 28/11/2014
 *
 * This batch process is required to run once a day. It will search for Final Audits in the system that fit the following criteria:
 *   a. Is not yet complete.
 *   b. Have a planned audit method of Voluntary.
 *   c. And have no value for Received Date, i.e. no information has yet been received from the insured.
 *   d. And the current date is "ScriptParameters.TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch days (or more) after the renewal date of the policy,
 *   i.e. the policy expired at least "ScriptParameters.TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch days ago.
 *
 * If this process does find such a Final Audit, then it should set the actual audit method to estimate and automatically process and complete the Final Audit job.
 */
class TDIC_ProcessFinalAuditEstimationBatch extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("PROCESS_FINAL_AUDIT_ESTIMATION_BATCH")

  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("PCInfraIssueNotificationEmail")

  construct() {
    super(BatchProcessType.TC_PROCESSFINALAUDITESTIMATION_TDIC)
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  /**
   * DE43 - related to US463
   * Shane Sheridan 28/11/2014
   *
   * This function specifies the work that is unique to this batch process.
   */
  override function doWork() {
    _logger.debug("TDIC_ProcessFinalAuditEstimationBatch#doWork() - Entering")

    // get candidate AuditInformations
    var candidateAuditInfos = CandidateAuditInfos

    for (auditInformation in candidateAuditInfos) {
      // Do not estimate audits for automated audits.
      if (auditInformation.Audit.AutomatedJob_TDIC != null) {
        _logger.trace ("Skipping estimated for " + auditInformation.Audit.AutomatedJob_TDIC.DisplayName
                          + " automated audit for " + auditInformation.Audit.LatestPeriod.PolicyNumber + "-"
                          + auditInformation.Audit.LatestPeriod.TermNumber);
        continue;
      }

      if (auditInformation.IsScheduled or auditInformation.IsOpen) {
        if (hasWorkflowStarted(auditInformation) == false) {
          try {
            Transaction.runWithNewBundle (\bundle -> {
              var workflow = new EstimateAuditWF_TDIC(bundle);
              workflow.AuditInformation_TDIC = auditInformation;
              workflow.startAsynchronously();
            })
          } catch (e : Exception) {
            _logger.error ("Exception occurred while starting estimate audit workflow for " + auditInformation
                              + ": " + e.toString());
            incrementOperationsFailed();
          }
          incrementOperationsCompleted()
        } else {
          _logger.trace ("Workflow has aleady started for " + auditInformation);
        }
      } else {
        _logger.trace ("Audit " + auditInformation + " is not scheduled or open.");
      }
    }

    if (OperationsFailed > 0) {
      if(EMAIL_RECIPIENT == null){
        _logger.error("CSVFile#construct - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
      } else {
        EmailUtil.sendEmail(EMAIL_RECIPIENT, " Process final audit estimation batch failed on server: " + gw.api.system.server.ServerUtil.ServerId, ",Please check logs on server for error details")
      }
    }
    _logger.debug("TDIC_ProcessFinalAuditEstimationBatch#doWork() - Exiting")
  }

  /**
   * DE43 - related to US463
   * Shane Sheridan 28/11/2014
   *
   * Get candidate AuditInformation objects.
  */
  @Returns("Array of AuditInformation objects that meet the criteria.")
  private property get CandidateAuditInfos() : IQueryBeanResult<AuditInformation> {
    // threshold is "ScriptParameters.TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch" ago
    _logger.debug("TDIC_ProcessFinalAuditEstimationBatch#getCandidateAuditInfos(auditInfo) - Entering")
    //final var DATE_THRESHOLD = java.util.Date.CurrentDate.addDays(-ScriptParameters.TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch)
    final var DATE_THRESHOLD = java.util.Date.CurrentDate.addDays(-ScriptParameters.TDIC_WC7DaysAfterAuditDueDateToStartFinalAuditBatch)

    var query = Query.make(AuditInformation).and(\ andCriteria -> {
                                                              andCriteria.compare(AuditInformation#AuditScheduleType, Relop.Equals, typekey.AuditScheduleType.TC_FINALAUDIT)
                                                              andCriteria.compare(DBFunction.DateFromTimestamp(andCriteria.getColumnRef("DueDate")), Relop.LessThanOrEquals, DATE_THRESHOLD)
                                                              andCriteria.compare(AuditInformation#AuditMethod, Relop.Equals, typekey.AuditMethod.TC_VOLUNTARY)
                                                              andCriteria.compare(AuditInformation#ReceivedDate, Relop.Equals, null)
                                            });

    var retval = query.select();
    _logger.debug("TDIC_ProcessFinalAuditEstimationBatch#getCandidateAuditInfos(auditInfo) - Exiting")
    return retval;
  }

  /**
  *  Check if there is already a workflow started for the AuditInformation.
   */
  protected function hasWorkflowStarted (auditInformation : AuditInformation) : boolean {
    return auditInformation.Workflows_TDIC.hasMatch(\wf -> wf typeis EstimateAuditWF_TDIC
                                                       and wf.State != WorkflowState.TC_COMPLETED);
  }
}