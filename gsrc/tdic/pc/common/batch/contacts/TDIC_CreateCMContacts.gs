package tdic.pc.common.batch.contacts

uses gw.contactmapper.ab1000.ContactIntegrationMapperFactory
uses gw.processes.BatchProcessBase
uses gw.api.database.Query
uses gw.plugin.Plugins
uses gw.plugin.contact.ContactSystemPlugin
uses java.lang.Exception
uses gw.pl.logging.LoggerCategory
uses java.io.File
uses java.io.PrintWriter
uses org.slf4j.LoggerFactory

/**
 * RadhakrishnaS
 *
 * This batch is a one time process that will take all contacts from PolicyCenter and
 * create them in ContactManager using the in-built OOTB integration with ContactManager.
 */
class TDIC_CreateCMContacts extends BatchProcessBase {

  private static final var _logger = LoggerFactory.getLogger("TDIC_BATCH_CREATECMCONTACTS")
  private static var CLASS_NAME = "TDIC_CreateCMContacts"
  private static var TXN_USER = "su"
  private static var EXCLUDED_CONTACT_PUBLIC_IDS = {"default_data:3"}
  private static var ABUID_OUTPUT_PATH = "/tmp/abuid_replaces.log"

  private var mapper = ContactIntegrationMapperFactory.ContactIntegrationMapper
  private var contactPlugin = Plugins.get(ContactSystemPlugin)
  private var transactionIDPrefix = "tdic_cm_create:"

  construct(){
    super(BatchProcessType.TC_CREATE_CM_CONTACTS_TDIC)
  }

  /**
   * This function overrides dowork() method to process membership status.
   */
  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.info("${logPrefix} - Entering")

    if ( hasAlreadyRun() ) {
      _logger.warn("${logPrefix} - This process has already run. This is a ONE TIME process. Batch stopping.")
      return
    }

    var outputFile = new File(ABUID_OUTPUT_PATH)
    if(not outputFile.exists()) {
      outputFile.createNewFile()
    }
    var abWriter = new PrintWriter(outputFile)

    var contactQuery = Query.make(Contact)
    contactQuery.or( \ criteria -> {
      criteria.compare("Subtype", Equals, typekey.Contact.TC_PERSON)
      criteria.compare("Subtype", Equals, typekey.Contact.TC_COMPANY)
    })
    if( EXCLUDED_CONTACT_PUBLIC_IDS != null ) {
      contactQuery.compareNotIn("PublicID", EXCLUDED_CONTACT_PUBLIC_IDS.toArray())
    }

    var allContacts = contactQuery.select()
    var currCtr = 1
    var totalCount = allContacts.Count
    OperationsExpected = totalCount
    _logger.info("${logPrefix} - Found ${totalCount} records to send to contact manager.")
    for(aContact in allContacts) {

      var oldABUID = ""
      try {
        gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
          aContact = bundle.add(aContact)
          //Take the existing AddressBookUID and make it the current transactionID.
          //var transactionID = aContact.AddressBookUID
          var transactionID = transactionIDPrefix + currCtr
          oldABUID = aContact.AddressBookUID
          //Empty the existing aContact.AddressBookUID so that PolicyCenter treats it
          //as a new contact and sends it to ContactManager.
          //The transactionID will become the new AddressBookUID in PC and CM
          //And since this AddressBookUID already exists in BC, the parity
          //between PC, BC and CM will be intact.
          aContact.AddressBookUID = null
          _logger.info("Adding contact '${aContact}' with publicID ${aContact.PublicID} and transactionID ${transactionID}" )

          checkEssentialDetails(aContact)

          if( isContactPrimaryNamedInsured(aContact) ) {
            _logger.info("Contact is Primary Named Insured. Setting VendorNumber to 99")
            aContact.VendorNumber = "99"
          }

          var transformedPayload = mapper.populateXMLFromContact(aContact)
          print(transformedPayload.asUTFString())
          contactPlugin.addContact(aContact, transformedPayload.asUTFString(), transactionID)

          incrementOperationsCompleted()

          var percComplete = (currCtr*100)/totalCount
          _logger.info("Done. " + currCtr + "/" + totalCount + ", " + percComplete + "% complete.")
        }, TXN_USER)
      }
      catch(excep : Exception) {
        _logger.error("Exception adding contact '" + aContact + "' with publicID " + aContact.PublicID + " :" + excep.Message)
        excep.printStackTrace()
        this.incrementOperationsFailed()
      }

      currCtr++;
      var newABUID = aContact.AddressBookUID
      _logger.info("Converted:${oldABUID}|${newABUID}" )
      abWriter.println(oldABUID + "|" + newABUID)
    }

    _logger.info("Done. 100% complete.")
    _logger.info("${logPrefix} - Done processing all. Exiting")
    abWriter.close()
  }

  /**
   * This is override function to handle termination request during batch process
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }

  /**
   * This is a ONE TIME process.
   * This function exists to determine if this has been already run.
  */
  private function hasAlreadyRun() : boolean {
    var processHistoryQuery = Query.make(entity.ProcessHistory)
    processHistoryQuery.compare("ProcessType", Equals, BatchProcessType.TC_CREATE_CM_CONTACTS_TDIC)
    var count = processHistoryQuery.select().Count
    return count > 1
  }

  private function checkEssentialDetails(aContact : Contact) {
    if( aContact.FaxPhoneCountry == typekey.PhoneCountryCode.TC_UNPARSEABLE ) {
      _logger.warn("FaxPhoneCountry was UNPARSEABLE, SETTING TO US")
      aContact.FaxPhoneCountry = typekey.PhoneCountryCode.TC_US
    }
    //If auto-sync is not set, then set it. because we are creating it from here.
    if( aContact.AutoSync == null || aContact.AutoSync != typekey.AutoSync.TC_ALLOW ) {
      _logger.warn("AutoSync was turned off. Turning on.")
      aContact.AutoSync = typekey.AutoSync.TC_ALLOW
    }
    if( aContact typeis Person ) {
      if( aContact.FirstName == null and aContact.LastName == null ) {
        _logger.warn("Person name was missing. Setting default values.")
        aContact.FirstName = "MISSING"
        aContact.LastName  = "PERSON NAME"
      }
    }
    else if( aContact typeis Company ) {
      if( aContact.Name == null ) {
        _logger.warn("Company name was missing. Setting default values.")
        aContact.Name = "MISSING COMPANY NAME"
      }
    }
  }

  /**
   * Identifies if a contct is Primary Named Insured.
  */
  private function isContactPrimaryNamedInsured(aContact : Contact) : boolean {
    var period = aContact.PolicyPeriods.orderBy( \ per -> per.PeriodEnd).last()
    var currPer = period?.getSlice(period.PeriodEnd.addSeconds(-1))
    return currPer?.PolicyContactRoles?.whereTypeIs(PolicyPriNamedInsured)?.first()?.ContactDenorm?.equals(aContact)
  }
}