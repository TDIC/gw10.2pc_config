package tdic.pc.common.batch.enrole.dao

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.enrole.exception.EnroleException
uses tdic.pc.common.batch.enrole.vo.EnroleCourseVO
uses tdic.pc.common.batch.enrole.vo.EnrolePolicyVO

uses java.sql.CallableStatement
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.sql.Statement

/**
 * EnroleDAOImpl is responsible to
 * Insert enrole policies details to table: EnrolePolicyDetails of GWINT DB.
 * Insert enrole course details to table: EnroleCourseDetails.
 * Update processed enrole policy records to true in table: EnrolePolicyDetails.
 * Fetch unprocessed records from table: EnrolePolicyDetails.
 * Fetch enrole course details from table: EnroleCourseDetails based on Policy Number and ADA number.
 * Created by RambabuN on 10/15/2019.
 */
class EnroleDAOImpl implements IEnroleDAO {

  static final var _logger = LoggerFactory.getLogger("TDIC_Enrole")

  var _conn : Connection

  var _inEnroleCourseDetailsCalStmt : CallableStatement

  var _enrolePolicyRowIDs : String

  /**
   * Function to initialize the Callable statements & the DB Connection Object.
   */
  @Override
  function initialize() {
    _logger.debug("Initalizing DB connections - EnroleDAOImpl.initialize()")

    _conn = DatabaseManager.getConnection(PropertyUtil.getInstance().getProperty("IntDBURL"))
    _conn.setAutoCommit(false)

    _inEnroleCourseDetailsCalStmt = _conn.prepareCall(insert_enrole_course_details_feed_sp)

    _logger.debug("Exit EnroleDAOImpl.initialize()")
  }


  /**
   * Cleans up the database connection  and open statements
   */
  @Override
  function cleanConnections() {
    DatabaseManager.closeConnection(_conn)
    DatabaseManager.closeCallableStatement(_inEnroleCourseDetailsCalStmt)

  }

  /**
   * Function to insert EnroleCourse details into GWINT DB table: EnroleCourseDetails.
   *
   * @param courseVO EnroleCourseVO
   */
  @Override
  function insertEnroleCourseDetails(courseVOs : List<EnroleCourseVO>) {
    _logger.debug("Enter EnroleDAOImpl.insertEnroleCourseDetails()")

    try {
      for(courseVO in courseVOs) {
        _inEnroleCourseDetailsCalStmt.setString(1, courseVO.PersonID)
        _inEnroleCourseDetailsCalStmt.setString(2, courseVO.PolicyNumber)
        _inEnroleCourseDetailsCalStmt.setString(3, courseVO.ADA?.replace(CHAR_HYPHEN,""))
        _inEnroleCourseDetailsCalStmt.setString(4, courseVO.SessionNumber)
        _inEnroleCourseDetailsCalStmt.setString(5, courseVO.SessionName)
        _inEnroleCourseDetailsCalStmt.setInt(6, courseVO.CourseNumber)
        _inEnroleCourseDetailsCalStmt.setString(7, courseVO.CourseName)
        _inEnroleCourseDetailsCalStmt.setDate(8, courseVO.CourseAttendedDate?.toSQLDate())
        _inEnroleCourseDetailsCalStmt.addBatch()
      }
      _inEnroleCourseDetailsCalStmt.executeBatch()
      _conn.commit()

      _logger.debug("Exit EnroleDAOImpl.insertEnroleCourseDetails()")
    } catch (ex : Exception) {
      _logger.error("Failed to insert EnroleCourseDetails.")
      throw ex
    }finally {
      //Release connections.
      _inEnroleCourseDetailsCalStmt.clearBatch()
    }
  }


  /**
   * Function to insert EnroleCourse details into GWINT DB table: EnroleCourseDetails.
   *
   * @param policyVO : EnrolePolicyVO
   */
  @Override
  function insertEnrolePolicyDetails(policyVO : EnrolePolicyVO) {
    _logger.debug("Enter EnroleDAOImpl.insertEnrolePolicyDetails()")
    var _inEnrolePolicyDetailsCalStmt : CallableStatement
    try {
      _inEnrolePolicyDetailsCalStmt = _conn.prepareCall(insert_enrole_policy_details_feed_sp)

      _inEnrolePolicyDetailsCalStmt.setString(1, policyVO.PolicyNBR)
      _inEnrolePolicyDetailsCalStmt.setString(2, policyVO.FirstName)
      _inEnrolePolicyDetailsCalStmt.setString(3, policyVO.MiddleInitial)
      _inEnrolePolicyDetailsCalStmt.setString(4, policyVO.LastName)
      _inEnrolePolicyDetailsCalStmt.setString(5, policyVO.Salutation)
      _inEnrolePolicyDetailsCalStmt.setString(6, policyVO.Gender)
      _inEnrolePolicyDetailsCalStmt.setString(7, policyVO.Address1)
      _inEnrolePolicyDetailsCalStmt.setString(8, policyVO.Address2)
      _inEnrolePolicyDetailsCalStmt.setString(9, policyVO.City)
      _inEnrolePolicyDetailsCalStmt.setString(10, policyVO.State)
      _inEnrolePolicyDetailsCalStmt.setString(11, policyVO.Zip)
      _inEnrolePolicyDetailsCalStmt.setString(12, policyVO.Country)
      _inEnrolePolicyDetailsCalStmt.setDate(13, policyVO.LastUpdated?.toSQLDate())
      _inEnrolePolicyDetailsCalStmt.setString(14, policyVO.Email)
      _inEnrolePolicyDetailsCalStmt.setString(15, policyVO.Phone)
      _inEnrolePolicyDetailsCalStmt.setString(16, policyVO.Ext)
      _inEnrolePolicyDetailsCalStmt.setString(17, policyVO.Fax)
      _inEnrolePolicyDetailsCalStmt.setString(18, policyVO.Title)
      _inEnrolePolicyDetailsCalStmt.setString(19, policyVO.TitleCode)
      _inEnrolePolicyDetailsCalStmt.setString(20, policyVO.FunctCode)
      _inEnrolePolicyDetailsCalStmt.setInt(21, policyVO.FirmSize)
      _inEnrolePolicyDetailsCalStmt.setString(22, policyVO.IndustryCode)
      _inEnrolePolicyDetailsCalStmt.setString(23, policyVO.ADA)
      _inEnrolePolicyDetailsCalStmt.setString(24, policyVO.PersonType)
      _inEnrolePolicyDetailsCalStmt.setString(25, policyVO.PolicyIndicator)
      _inEnrolePolicyDetailsCalStmt.setString(26, policyVO.Status)
      _inEnrolePolicyDetailsCalStmt.setString(27, policyVO.SpecType)
      _inEnrolePolicyDetailsCalStmt.setString(28, policyVO.ComponentState)
      _inEnrolePolicyDetailsCalStmt.setString(29, policyVO.ComponentID)
      _inEnrolePolicyDetailsCalStmt.setString(30, policyVO.CompDesc)
      _inEnrolePolicyDetailsCalStmt.setDate(31, policyVO.RenewDT?.toSQLDate())
      _inEnrolePolicyDetailsCalStmt.setDate(32, policyVO.DscExpDT?.toSQLDate())
      _inEnrolePolicyDetailsCalStmt.setString(33, policyVO.ADA)
      _inEnrolePolicyDetailsCalStmt.setString(34, policyVO.Source)
      _inEnrolePolicyDetailsCalStmt.setString(35, policyVO.SemtekStatus)
      _inEnrolePolicyDetailsCalStmt.setString(36, policyVO.D3C9C3C5D5E2C5)

      _inEnrolePolicyDetailsCalStmt.executeUpdate()
      _conn.commit()
      _logger.debug("Successfully inserted enrole policy details for Policy and ADA nymbers are [" +
          policyVO.PolicyNBR + "," + policyVO.ADA + "]")
    } catch (ex : SQLException) {
      _logger.error("Failed insert EnroleCourseDetails for Policy and ADA nymbers are [" +
          policyVO.PolicyNBR + "," + policyVO.ADA + "]")
      throw new EnroleException(ex.Message, ex)

    } finally {
      //Release connections.
      DatabaseManager.closeCallableStatement(_inEnrolePolicyDetailsCalStmt)
    }
    _logger.debug("Exit EnroleDAOImpl.insertEnrolePolicyDetails()")
  }

  /**
   * Function to fetch un processed policies from table EnrolePolicyDetails of GWINT DB.
   *
   * @return _policyrecords : List<String>
   */
  @Override
  function getEnrolePolicyDetails() : List<String> {
    _logger.debug("Enter EnroleDAOImpl.getEnrolePolicyDetails()")
    var _policyrecords = new ArrayList<String>()
    var _selEnrolePolicyDetailsCalStmt : CallableStatement
    try {
      var _rowIds = new ArrayList<Integer>()
      var _policies = new ArrayList<String>()
      _selEnrolePolicyDetailsCalStmt = _conn.prepareCall(select_enrole_policy_details_sp)

      var _resultSet = _selEnrolePolicyDetailsCalStmt.executeQuery()
      while (_resultSet.next()) {
        _rowIds.add(_resultSet.getInt(1))
        _policies.add(_resultSet.getString(2))
        _policyrecords.add(_resultSet.getString(3))
      }

      //Remove unused first header row data
      _rowIds.remove(0)
      _policies.remove(0)

      //Join all the processed policies ID's as string.
      _enrolePolicyRowIDs = _rowIds.join(CHAR_COMMA)

      _logger.debug("${_rowIds.Count} enrole policy records are fetched successfylly and policy numbers : ${_policies}")
    } catch (ex : SQLException) {
      _logger.error("Failed fetch enrole policy records. ")
      throw new EnroleException(ex.Message, ex)
    } finally {
      //Release Connections
      DatabaseManager.closeCallableStatement(_selEnrolePolicyDetailsCalStmt)
    }
    _logger.debug("Exit EnroleDAOImpl.getEnrolePolicyDetails()")
    return _policyrecords
  }

  /**
   * Function to update processed enrole policy records status to true after writing policies to enrole policy file.
   */
  @Override
  function updateEnrolePolicyDetails() {
    _logger.debug("Enter EnroleDAOImpl.updateEnrolePolicyDetails()")
    var _sqlStatement : Statement

    try {
      var sqlString = new StringBuilder().append(update_enrole_policy_details).append(_enrolePolicyRowIDs)
          .append(CLOSE_PERENTHESIS)
      _sqlStatement = _conn.createStatement()
      _sqlStatement.executeUpdate(sqlString.toString())
      _conn.commit()
    } catch (ex : SQLException) {
      _logger.error("Failed record for EnroleCourseDetails:: ")
      throw new EnroleException(ex.Message, ex)
    } finally {
      //Release Connections
      if (_sqlStatement != null) _sqlStatement.close()
    }
    _logger.debug("Exit EnroleDAOImpl.updateEnrolePolicyDetails()")
  }


  /**
   * Function to fetch EnrolePolicy Details based on policy and ADA numbers.
   *
   * @param policyNbr
   * @param legacyPolicyNbr
   * @param ada
   * @return List<EnroleCourseVO>
   */
  @Override
  function fetchEnrolePolicyDetails(policyNbr : String, legacyPolicyNbr : String, ada : String) : List<EnroleCourseVO> {
    _logger.debug("Enter EnroleDAOImpl.insertEnroleCourseDetails")
    var _resultSet : ResultSet
    var _selEnroleCourseDetailsCalStmt : CallableStatement
    var courseVOs : List<EnroleCourseVO>
    try {
      _selEnroleCourseDetailsCalStmt = _conn.prepareCall(select_enrole_course_details_sp)
      _selEnroleCourseDetailsCalStmt.setString(1, policyNbr)
      _selEnroleCourseDetailsCalStmt.setString(2, legacyPolicyNbr)
      _selEnroleCourseDetailsCalStmt.setString(3, ada)
      _resultSet = _selEnroleCourseDetailsCalStmt.executeQuery()
      courseVOs = new ArrayList<EnroleCourseVO>()
      while (_resultSet.next()) {
        var courseVO = new EnroleCourseVO()

        courseVO.PersonID = _resultSet.getString(1)
        courseVO.PolicyNumber = _resultSet.getString(2)
        courseVO.ADA = _resultSet.getString(3)
        courseVO.SessionNumber = _resultSet.getString(4)
        courseVO.SessionName = _resultSet.getString(5)
        courseVO.CourseNumber = _resultSet.getInt(6)
        courseVO.CourseName = _resultSet.getString(7)
        courseVO.CourseAttendedDate = _resultSet.getDate(8)
        courseVOs.add(courseVO)
      }
    } catch (ex : SQLException) {
      _logger.error("Failed to fetch enrole course details for EnroleCourseDetails.")
      throw new EnroleException(ex.Message, ex)
    } finally {
      //Release Connections
      DatabaseManager.closeCallableStatement(_selEnroleCourseDetailsCalStmt)
    }
    return courseVOs
  }

}