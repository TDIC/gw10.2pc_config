package tdic.pc.common.batch.enrole.dao

uses tdic.pc.common.batch.enrole.vo.EnroleCourseVO
uses tdic.pc.common.batch.enrole.vo.EnrolePolicyVO

/**
 * IEnroleDAO interface has SQL Query constants and  method difinations for following
 * Contains all the query constants.
 * Insert enrole policies details to table: EnrolePolicyDetails of GWINT DB.
 * Insert enrole course details to table: EnroleCourseDetails.
 * Update processed enrole policy records to true in table: EnrolePolicyDetails.
 * Fetch unprocessed records from table: EnrolePolicyDetails.
 * Fetch enrole course details from table: EnroleCourseDetails based on Policy Number and ADA number.
 * Created by RambabuN on 10/15/2019.
 */
interface IEnroleDAO {

  //Stored Procedure to insert enrole policy details.
  var insert_enrole_policy_details_feed_sp : String =
      "dbo.SP_Insert_EnrolePolicyDetails ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"

  //Stored Procedure to insert enrole course details.
  var insert_enrole_course_details_feed_sp : String = "dbo.SP_Insert_EnroleCourseDetails ?,?,?,?,?,?,?,?"

  //Stored Procedure to select enrole policy details.
  var select_enrole_policy_details_sp : String = "{ call dbo.SP_Select_EnrolePolicyDetails() }"

  //Stored Procedure to select enrole course details.
  var select_enrole_course_details_sp : String = "{ call dbo.SP_Select_EnroleCourseDetails(?,?,?) }"

  //SQL query to update processed enrole policy details to true.
  var update_enrole_policy_details : String = "UPDATE ENROLEPOLICYDETAILS SET PROCESSED = 1 WHERE ID IN ("

  var CHAR_HYPHEN : String = "-"

  var CHAR_COMMA : String = ","

  var CLOSE_PERENTHESIS : String = ")"

  /**
   * Function to insert EnroleCourse details into GWINT DB table: EnroleCourseDetails.
   *
   * @param courseVO EnroleCourseVO
   */
  function insertEnroleCourseDetails(courseVO : List<EnroleCourseVO>)

  /**
   * Function to insert EnroleCourse details into GWINT DB table: EnroleCourseDetails.
   *
   * @param policyVO : EnrolePolicyVO
   */
  function insertEnrolePolicyDetails(policyVO : EnrolePolicyVO)

  /**
   * Function to fetch un processed policies from table EnrolePolicyDetails of GWINT DB.
   *
   * @return _policyrecords : List<String>
   */
  function getEnrolePolicyDetails() : List<String>

  /**
   * Function to update processed enrole policy records status to true after writing policies to enrole policy file.
   */
  function updateEnrolePolicyDetails()

  /**
   * Function to fetch EnrolePolicy Details based on policy and ADA numbers.
   *
   * @param policyNbr
   * @param legacyPolicyNbr
   * @param ada
   * @return List<EnroleCourseVO>
   */
  function fetchEnrolePolicyDetails(policyNbr : String, legacyPolicyNbr : String, ada : String) : List<EnroleCourseVO>

  /**
   * Function to initialize the Callable statements & the DB Connection Object.
   */
  function initialize()

  /**
   * Cleans up the database connection  and open statements
   */
  function cleanConnections()

}