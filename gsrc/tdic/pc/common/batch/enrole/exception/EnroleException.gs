package tdic.pc.common.batch.enrole.exception

uses com.tdic.util.exception.TDICException

class EnroleException extends TDICException {

  public construct() {
    super()
  }

  public construct(errorMessage : String) {
    super(errorMessage)
  }

  public construct(errorMessage : String, exception : Exception) {
    super(errorMessage, exception)
  }

}