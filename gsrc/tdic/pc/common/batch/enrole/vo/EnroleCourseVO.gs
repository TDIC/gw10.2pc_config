package tdic.pc.common.batch.enrole.vo

/**
 * EnroleCourseVO contains Dentiest course attended fields for enrole inbound.
 * Created by RambabuN on 10/15/2019.
 */

class EnroleCourseVO {

  var _personID : String as PersonID
  var _PolNumbr : String as PolicyNumber
  var _ada : String as ADA
  var _sessionNumber : String as SessionNumber
  var _sessionName : String as SessionName
  var _courseNumber : int as CourseNumber
  var _courseName : String as CourseName
  var _courseAttendedDate : Date as CourseAttendedDate

  /**
   * Function returns concatinated Enrole courese detaisl as string.
   * @return enrole course details
   */
  override function toString() : String {
    return new StringBuffer()
        //Claim
        .append("  \n PersonID : " + PersonID).append(", \n PolicyNumber : " + PolicyNumber)
        .append(", \n ADA : " + ADA).append(", \n SessionNumber : " + SessionNumber)
        .append(", \n SessionName : " + SessionName).append(", \n CourseNumber : " + CourseNumber)
        .append(", \n CourseName : " + CourseName).append(", \n CourseAttendedDate : " + CourseAttendedDate).toString()
  }
}