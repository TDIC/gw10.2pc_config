package tdic.pc.common.batch.generalledger.prmlines

uses java.util.Map
uses java.math.BigDecimal

uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumObject
uses java.util.List

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * Interface to define common functions required for generating premium lines in reporting
 * Written and Earned premium files.
 *
 */
interface TDIC_PremiumLinesInterface {
  function generatePremiumLines(map: Map<typekey.Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>) : List<String>
  function getRunGroup() : String
  function getSeqNumber() : int
  function getOldCompany(lob : typekey.PolicyLine, state : typekey.Jurisdiction, lineType: TDIC_PremiumLineTypeEnum, offSet : Boolean): String
  function getOldAcctNumber(oldAcctNumType : TDIC_PremiumLineTypeEnum) : String
  function getSystemDate() : String
  function getReference(state : typekey.Jurisdiction, seqNum : int) : String
  function getDescription(lob : typekey.PolicyLine, state : typekey.Jurisdiction, prmLineType : TDIC_PremiumLineTypeEnum, offSet : Boolean) : String
  function getPostingDate() : String
  function getTransAmount(amount : BigDecimal, offset : Boolean) : BigDecimal
  function getNegativeAdj(amount: BigDecimal, offset : Boolean) : String
}