package tdic.pc.common.batch.generalledger

uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLinesInterface
uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportTypeEnum
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_WrittenPremiumLines
uses java.lang.Exception
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumPoliciesInterface
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_WrittenPremiumPolicies
uses java.util.Date
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_EarnedPremiumPolicies
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_EarnedPremiumLines

/**
 * US627
 * 11/26/2014 Kesava Tavva
 *
 * This is a factory class to get premium policies processor and premium lines processor
 * based on General ledger report type i.e Written Premium / Earned Premium
 *
 */
class TDIC_GLReportFactory {

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns premium policies processor based on General Ledger Report type.
   *
   */
  @Param("reportType","represents Written or Earned premium type")
  @Returns("Returns TDIC_PremiumPoliciesInterface")
  protected static function getPremiumPoliciesProcessor(reportType : TDIC_GLReportTypeEnum, psd: Date, ped: Date) : TDIC_PremiumPoliciesInterface {
    var prmPoliciesProcessor : TDIC_PremiumLinesInterface
    switch(reportType){
      case TDIC_GLReportTypeEnum.WPRM :
          return new TDIC_WrittenPremiumPolicies(psd, ped, getPremiumLinesProcessor(reportType, ped))
      case TDIC_GLReportTypeEnum.EPRM :
          return new TDIC_EarnedPremiumPolicies(psd, ped, getPremiumLinesProcessor(reportType, ped))
        default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }

  /**
   * US627
   * 11/26/2014 Kesava Tavva
   *
   * This function returns premium lines processor based on General Ledger Report type.
   *
   */
  @Param("reportType","represents Written or Earned premium type")
  @Returns("Returns TDIC_PremiumLinesInterface")
  private static function getPremiumLinesProcessor(reportType : TDIC_GLReportTypeEnum, ped : Date) : TDIC_PremiumLinesInterface {
    switch(reportType){
      case TDIC_GLReportTypeEnum.WPRM :
        return new TDIC_WrittenPremiumLines(ped)
      case TDIC_GLReportTypeEnum.EPRM :
        return new TDIC_EarnedPremiumLines(ped)
      default :
        throw new Exception("Unable to process unknown GL report type ${reportType}.")
    }
  }
}