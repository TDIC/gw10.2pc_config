package tdic.pc.common.batch.generalledger.prmpolicies

uses java.math.BigDecimal

uses org.slf4j.Logger
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLineTypeEnum
uses org.slf4j.LoggerFactory

/**
 * US627
 * 12/01/2014 Kesava Tavva
 *
 * This class represents aggregate premium values for each premium line type reported in premium batch file
 * and number of units (policy units) represents this aggregated value.
 */
class TDIC_PremiumObject {
  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  private static final var CLASS_NAME : String = "TDIC_PremiumObject"

  private var _prmLineType: TDIC_PremiumLineTypeEnum as readonly PremiumLineType
  private var _units : int as readonly Units
  private var _amount : BigDecimal as readonly Amount
  private var _jobPublicID : String

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * Default constructor that initializes units and amount with zero values.
   */
  construct(prmLineType : TDIC_PremiumLineTypeEnum){
    _prmLineType = prmLineType
    _units = 0
    _amount = 0.0
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * Constructor to initialize units and amounts with input values
   */
  @Param("units","int value that represents number of units.")
  @Param("amount","BigDecimal value that represents aggregate premium values of all corresponding units.")
  construct(units : int, amount : BigDecimal) {
    _units = units
    _amount = amount
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function updates units and aggregate amounts based on transaction type.
   */
  @Param("job","Job that represents policy transaction type")
  @Param("amount","BigDecimal value that represents cost value of a single unit.")

  function addToAmount(job : Job, amount : BigDecimal) {
    var logPrefix = "${CLASS_NAME}#addToAmount(job, amount)"
    _logger.debug("${logPrefix} - Entering")
    if(job.PublicID != _jobPublicID && (PremiumLineType == TDIC_PremiumLineTypeEnum.PREMIUM || PremiumLineType == TDIC_PremiumLineTypeEnum.UNEARNED_PREMIUM)) {
      _jobPublicID = job.PublicID
      switch(job.Subtype) {
       case typekey.Job.TC_AUDIT :
       case typekey.Job.TC_POLICYCHANGE :
            break
       case typekey.Job.TC_CANCELLATION :
             _units = _units-1
             break
       default :
            _units = _units+1
            break
      }
    }
    _amount = _amount + amount
    _logger.debug("${logPrefix} - Job: ${job}; LineType: ${PremiumLineType}; Amount: ${amount}")
    _logger.debug("${logPrefix} - Exiting")
  }
}