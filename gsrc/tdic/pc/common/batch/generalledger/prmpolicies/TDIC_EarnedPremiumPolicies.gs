package tdic.pc.common.batch.generalledger.prmpolicies

uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLineTypeEnum
uses java.util.Map
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLinesInterface
uses org.slf4j.Logger
uses java.util.Date
uses gw.api.database.Query
uses java.util.HashMap
uses gw.api.database.IQueryBeanResult
uses org.slf4j.LoggerFactory

/**
 * US627
 * 12/19/2014 Kesava Tavva
 *
 * This class is to calculate aggregated cost values for earned premium in reporting period
 * from the qualified policy periods for a given batch run period.
 */
class TDIC_EarnedPremiumPolicies extends TDIC_PremiumPoliciesBase {
  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_GLREPORT")
  private static final var CLASS_NAME : String = "TDIC_EarnedPremiumPolicies"
  private static final var EP_CALC_DAYS = 365

  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_PremiumLinesInterface){
    super(periodStartDate, periodEndDate, prmLines)
  }

  /**
   * US627
   * 12/19/2014 Kesava Tavva
   *
   * This function returns aggregated cost amounts corresponding to each Jurisdiction from the qualified
   * policy periods for a given batch run period.
   */
  @Returns("Map object of aggregate amounts for each Jurisdiction")
  override function getPremiumAmounts(): Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    return buildPremiums()
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function retrieves all qualified policy periods for the batch period and aggregate
   * costs based on cost type.
   */
  @Returns("Map object of aggregate amounts for each Jurisdiction")
  private function buildPremiums() : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>> {
    var logPrefix = "${CLASS_NAME}#buildPremiums()"
    _logger.debug("${logPrefix} - Entering")
    var premiums = new HashMap<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>()
    //BatchPeriods
    var batchPSD = getBatchPeriodStartDate()
    var batchPED = getBatchPeriodEndDate(batchPSD)
    //Retrieve PolicyPeriods
    var policyPeriods = retrievePolicyPeriods(batchPSD, batchPED)
    //Retrieve PolicyPeriods
    var auditPolicyPeriods = retrieveAuditPolicyPeriods(batchPSD, batchPED)
    var periods = policyPeriods.concat(auditPolicyPeriods?.toCollection())
    //Calculate Premiums from Policy Periods
    var prmLineTypeMap : Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>
    var earnedPrmDays : int
    var numDaysInPeriod : int
    _logger.info("${logPrefix} - PolicyPeriod; ModelDate; EditEffectiveDate; PeriodEnd; CancellationDate")
    for(pp in periods){
      _logger.info("${logPrefix} - ${pp}; ${pp.ModelDate}; ${pp.EditEffectiveDate}; ${pp.PeriodEnd}; ${pp.CancellationDate}")
      for(pl in pp.RepresentativePolicyLines){
        if(pl typeis productmodel.WC7Line){
          for(jur in pl.RepresentativeJurisdictions){
            var wc7Transactions = pl.WC7Transactions?.where( \ wc7Trans -> (wc7Trans.WC7Cost.JurisdictionState == jur.Jurisdiction
                and wc7Trans.WC7Cost.CalcOrder <= CALC_ORDER_TERRORISM
            )==true)?.orderBy(\ wc7Trans -> wc7Trans.WC7Cost.CalcOrder)
            //By state
            prmLineTypeMap = premiums.get(jur.Jurisdiction)
            if(prmLineTypeMap == null){
              prmLineTypeMap = new HashMap<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>()
              premiums.put(jur.Jurisdiction, prmLineTypeMap)
            }
            //Find earned premium days
            earnedPrmDays = getEarnedPremiumDays(pp, batchPSD, batchPED)
            numDaysInPeriod = getNumDaysInPeriod(pp, batchPSD, batchPED)
            for(wc7Trans in wc7Transactions){
              addAmountToPremiumLineType(pp.Job, wc7Trans, prmLineTypeMap, earnedPrmDays, numDaysInPeriod)
            }
          }
        }
      }
    }
    _logger.debug("${logPrefix} - Exiting")
    return premiums
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function builds query to retrieve all qualified policy periods for a given batch period
   * start and end dates and returns list of those policy periods.
   */
  @Param("batchPSD","Batch period start date")
  @Param("batchPED","Batch period end date")
  @Returns("Returns list of qualified policy periods")
  private function retrievePolicyPeriods(batchPSD : Date, batchPED : Date) : IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrievePolicyPeriods(batchPSD, batchPED)"
    _logger.debug("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.compare("ModelDate",NotEquals,null)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE).compare(PolicyLine#EffectiveDate,Equals,null)
    ppQuery.or(\ or1 -> {
      or1.and(\andE ->{
        andE.compare("EditEffectiveDate",LessThanOrEquals,batchPED)
        andE.compare("PeriodEnd",GreaterThan, batchPSD)
        andE.compare("ModelDate",LessThanOrEquals,batchPED)
      })
      or1.and(\ andT ->{
        andT.between("ModelDate",batchPSD,batchPED)
        andT.compare("PeriodEnd",LessThanOrEquals, batchPSD)
      })
    })
    _logger.debug("${logPrefix}- Retrieving Policy Periods complete for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    _logger.debug("${logPrefix} - Exiting")
    return ppQuery.select()
  }

  private function retrieveAuditPolicyPeriods(batchPSD : Date, batchPED : Date): IQueryBeanResult<PolicyPeriod> {
    var logPrefix = "${CLASS_NAME}#retrieveAuditPolicyPeriods(batchPSD, batchPED)"
    _logger.debug("${logPrefix}- Entering")
    _logger.debug("${logPrefix}- Retrieving Policy Periods for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compareIgnoreCase("PolicyNumber",NotEquals,"Unassigned")
    ppQuery.compare("Status",Equals,PolicyPeriodStatus.TC_AUDITCOMPLETE)
    ppQuery.join(PolicyLine,"BranchValue").compare("Subtype",Equals,typekey.PolicyLine.TC_WC7WORKERSCOMPLINE)
        .compare(PolicyLine#EffectiveDate,Equals,null)
    ppQuery.join(PolicyPeriod#Job).between(Job#CloseDate,batchPSD,batchPED)
    _logger.debug("${logPrefix}- Retrieving Policy Periods complete for the dates : ${batchPSD.XmlDateTime}---${batchPED.XmlDateTime}")
    _logger.debug("${logPrefix} - Exiting")
    return ppQuery.select()
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function adds cost amounts to corresponding premium line type aggregated amounts.
   */
  @Param("job","Job that represents transaction type")
  @Param("wcTrans", "WC7Transaction lines for each Policy period's policy line type")
  @Param("prmLineTypeMap","Map object that represents aggregated amounts and units")
  private function addAmountToPremiumLineType(job : Job, wc7Trans : WC7Transaction, prmLineTypeMap : Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>, eDays : int, periodDays : int) {
    var logPrefix = "${CLASS_NAME}#addAmountToPremiumLineType(job, wc7Trans, prmLineTypeMap)"
    _logger.debug("${logPrefix} - Entering")
    if(wc7Trans == null || prmLineTypeMap == null)
      return
    var prmLineType =  TDIC_PremiumLineTypeEnum.UNEARNED_PREMIUM
    var prmObject = prmLineTypeMap.get(prmLineType)
    if(prmObject == null) {
      prmObject = new TDIC_PremiumObject(prmLineType)
      prmLineTypeMap.put(prmLineType, prmObject)
    }
    _logger.debug("${logPrefix} - ${job.SelectedVersion}---${wc7Trans.Amount.Amount}--${eDays}---${periodDays}")
    prmObject.addToAmount(job,(wc7Trans.Amount.Amount*eDays)/periodDays)
    _logger.debug("${logPrefix} - Exiting")
  }

  /**
   * US627
   * 12/24/2014 Kesava Tavva
   *
   * This function finds active days in a given batch period.
   */
  @Param("pp","Policy period")
  @Param("batchPSD", "WC7Transaction lines for each Policy period's policy line type")
  @Param("batchPED","Map object that represents aggregated amounts and units")
  @Returns("int value of active days in a given batch period")
  private function getEarnedPremiumDays(pp : PolicyPeriod, batchPSD : Date, batchPED : Date) : int {
    //if((pp.CreateTime >= batchPSD and pp.PeriodEnd < batchPSD) or pp.Job.Subtype == typekey.Job.TC_AUDIT){
    if(pp.CreateTime >= batchPSD and pp.PeriodEnd < batchPSD){
      return pp.NumDaysInPeriod
    }else if(pp.CreateTime >= batchPSD and pp.EditEffectiveDate < batchPSD){
      return batchPED.daysBetween(pp.EditEffectiveDate)
    }else{
      if(pp.EditEffectiveDate <= batchPSD){
        if(pp.PeriodEnd > batchPED)
          return batchPED.daysBetween(batchPSD)
        else
          return pp.PeriodEnd.daysBetween(batchPSD)
      }else{
        return batchPED.daysBetween(pp.EditEffectiveDate)
      }
    }
  }

  /**
   * US627
   * 12/24/2014 Kesava Tavva
   *
   * This function finds number of days in a policy period
   */
  @Param("pp","Policy period")
  @Param("batchPSD", "WC7Transaction lines for each Policy period's policy line type")
  @Param("batchPED","Map object that represents aggregated amounts and units")
  @Returns("int, number of days in a policy period")
  private function getNumDaysInPeriod(pp : PolicyPeriod, batchPSD : Date, batchPED : Date) : int {
    if(pp.PeriodEnd.daysBetween(pp.EditEffectiveDate) > EP_CALC_DAYS)
      return EP_CALC_DAYS
    else
      return pp.PeriodEnd.daysBetween(pp.EditEffectiveDate)
  }
}