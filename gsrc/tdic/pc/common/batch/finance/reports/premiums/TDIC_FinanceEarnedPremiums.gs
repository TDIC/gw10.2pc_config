
package tdic.pc.common.batch.finance.reports.premiums

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLineTypeEnum
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLinesInterface
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants
uses typekey.Job
uses typekey.PolicyLine

uses java.math.BigDecimal

class TDIC_FinanceEarnedPremiums extends TDIC_FinancePremiumsBase {

  private static final var CLASS_NAME = TDIC_FinanceEarnedPremiums.Type.RelativeName
  protected static final var _logger: Logger = LoggerFactory.getLogger("TDIC_FINANCE_PREMIUMS")

  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_FinancePremiumLinesInterface){
    super(periodStartDate, periodEndDate, prmLines)
  }

  override function getPremiumAmounts(productCodes : List<String>) : List<TDIC_FinanceReportPremiumObject> {
    var reportPremiums = new ArrayList<TDIC_FinanceReportPremiumObject>()
    for(productCode in productCodes) {
      switch (productCode) {
        case TDIC_FinanceReportConstants.WC7WORKERSCOMP: {
          reportPremiums.addAll(buildWC7PremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        case TDIC_FinanceReportConstants.COMMERCIAL_PROPERTY: {
          reportPremiums.addAll(buildCPPremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        case TDIC_FinanceReportConstants.PROFESSIONAL_LIABILITY: {
          reportPremiums.addAll(buildPLPremiumAmounts(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate))
          break
        }
        default:
          return null
      }
    }
    return reportPremiums
  }

  private function retrieveEarnedPremiums(productCode: String, reportingMonth: int, reportingYear: int): IQueryBeanResult<FinanceEarnedPremiums_TDIC> {
    var prmsQuery = Query.make(FinanceEarnedPremiums_TDIC)
        .compare(FinanceEarnedPremiums_TDIC#ReportingYear, Equals, reportingYear)
        .compare(FinanceEarnedPremiums_TDIC#ReportingMonth,Equals,reportingMonth)
        .join(entity.FinanceEarnedPremiums_TDIC#FinancePremium_TDIC)
        .compare(FinancePremiums_TDIC#ProductCode,Equals,productCode)
        .select()
    return prmsQuery
  }

  private function buildWC7PremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()

    var jurisdictions : Set<String> = null
    var linePremiumAmount : BigDecimal = null
    var premiums = retrieveEarnedPremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)
    jurisdictions = premiums*.FinancePremium_TDIC*.Jurisdiction?.toSet()
    for (j in jurisdictions) {
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j)?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.WC_EP_PREMIUM, linePremiumAmount))
    }
    return premiumAmounts
  }

  private function buildCPPremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()

    var jurisdictions : Set<String> = null
    var linePremiumAmount : BigDecimal = null
    var premiums = retrieveEarnedPremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)
    jurisdictions = premiums*.FinancePremium_TDIC*.Jurisdiction?.toSet()
    for (j in jurisdictions) {
      //Property
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j and elt.FinancePremium_TDIC.CMP_Type == "BOPBuildingPropCov_TDIC" and
          !(elt.FinancePremium_TDIC.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM.DisplayName
              or elt.FinancePremium_TDIC.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM.DisplayName))
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_EP_PROPERTY, linePremiumAmount))

      //EQUIPMENT BREAKDOWN
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j and elt.FinancePremium_TDIC.CMP_Type == "BOPBuildingPropCov_TDIC" and
          (elt.FinancePremium_TDIC.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM.DisplayName
              or elt.FinancePremium_TDIC.StateCostType == BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM.DisplayName))
          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_EP_EQUIP_BREAKDOWN, linePremiumAmount))

      //General Liability
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j and elt.FinancePremium_TDIC.CMP_Type == "BOPBuildingLiabCov_TDIC")?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.CP_EP_GENERAL_LIAB, linePremiumAmount))
    }
    return premiumAmounts
  }

  private function buildPLPremiumAmounts(productCode: String, reportingMonth: int, reportingYear: int): List<TDIC_FinanceReportPremiumObject> {
    var premiumAmounts = new ArrayList<TDIC_FinanceReportPremiumObject>()

    var jurisdictions : Set<String> = null
    var linePremiumAmount : BigDecimal = null
    var premiums = retrieveEarnedPremiums(productCode, BatchPeriodStartDate.MonthOfYear, BatchPeriodStartDate.YearOfDate)
    jurisdictions = premiums*.FinancePremium_TDIC*.Jurisdiction?.toSet()

    for (j in jurisdictions) {
      //REP. ENDORSEMENTS
      //GWPS-2068 - excluding cyber liab offering
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.Offering != "PLCyberLiab_TDIC"
                            and elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.PL_EP_REP_ENDORSEMENTS, linePremiumAmount))

      //Cyber Liability
      //GWPS-2068 - including ERE premium charge pattern
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.Offering == "PLCyberLiab_TDIC"
                            and (elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_PREMIUM.Code
                              or elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_EREPREMIUM_TDIC.Code)
                            and elt.FinancePremium_TDIC.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_EP_CYBER_LIAB, linePremiumAmount))

      //IDENTITY THEFT
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.FinancePremium_TDIC.StateCostType == GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_EP_IDENITY_THEFT, linePremiumAmount))

      //PROFESSIONAL LIABILITY OCCURRENCE
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.Offering == "PLOccurence_TDIC"
                            and elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.FinancePremium_TDIC.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB_OCCUR, linePremiumAmount))

      //EMPLOYMENT PRACTICES
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.Offering == "PLClaimsMade_TDIC"
                            and elt.FinancePremium_TDIC.StateCostType == GLStateCostType.TC_EPLI_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j,TDIC_FinancePremiumLineTypeEnum.PL_EP_EMPL_PRACTICES, linePremiumAmount))

      //PROF. LIAB
      linePremiumAmount = premiums.where(\elt -> elt.FinancePremium_TDIC.Jurisdiction == j
                            and elt.FinancePremium_TDIC.Offering == "PLClaimsMade_TDIC"
                            and elt.FinancePremium_TDIC.ChargePattern == ChargePattern.TC_PREMIUM.Code
                            and elt.FinancePremium_TDIC.StateCostType != GLStateCostType.TC_IDENTITYTHEFTREC_TDIC.Code
                            and elt.FinancePremium_TDIC.StateCostType != GLStateCostType.TC_EPLI_TDIC.Code)
                          ?.sum(\elt -> elt.Amount)
      premiumAmounts.add(new TDIC_FinanceReportPremiumObject(productCode, j, TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB, linePremiumAmount))
    }
    return premiumAmounts
  }
}