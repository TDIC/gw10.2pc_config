package tdic.pc.common.batch.finance.reports

uses com.tdic.util.misc.EmailUtil
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportTypeEnum
uses typekey.PolicyLine

class TDIC_FinancePremiumsReportBatch extends TDIC_FinancePremiumsReportBatchBase {

  private static final var CLASS_NAME : String = TDIC_FinancePremiumsReportBatch.Type.RelativeName

  construct(args : Object[]){
    super(args)
  }


  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Entering")
    try{

      TDIC_FinanceReportConstants.LOGGER.info("${logPrefix} started for the Period: ${PeriodStartDate} to ${PeriodEndDate}")
      var batchStartTime = System.currentTimeMillis()
      if (TerminateRequested) {
        TDIC_FinanceReportConstants.LOGGER.warn("${logPrefix} - Terminate requested during doWork() method(before generateGeneralLedgerReport(WPRM)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      generateGeneralLedgerReport(TDIC_FinanceReportTypeEnum.WPRM)
      if (TerminateRequested) {
        TDIC_FinanceReportConstants.LOGGER.warn("${logPrefix} - Terminate requested during doWork() method(before generateGeneralLedgerReport(EPRM)).")
        EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
        return
      }
      generateGeneralLedgerReport(TDIC_FinanceReportTypeEnum.EPRM)
      EmailUtil.sendEmail(EmailTo, getEmailSubject(Boolean.TRUE), "PC GLInterface batch completed successfully for the Period: ${PeriodStartDate} to ${PeriodEndDate}.")
      TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- Total time taken for completing this batch process: ${System.currentTimeMillis()-batchStartTime}(ms)")
      TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- completed for the Period: : ${PeriodStartDate} to ${PeriodEndDate}")
    }catch(e: Exception){
      EmailUtil.sendEmail(EmailRecipient, getEmailSubject(Boolean.FALSE), "PC GLInterface batch completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}.\n${e.StackTraceAsString}")
      throw new Exception("${logPrefix}- completed with errors for the Period: ${PeriodStartDate} to ${PeriodEndDate}",e)
    }
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Exiting")
  }

  @Param("reportType", "Type of report i.e Written or Earned premium report")
  private function generateGeneralLedgerReport(reportType : TDIC_FinanceReportTypeEnum) {
    var logPrefix = "${CLASS_NAME}#generateGeneralLedgerReport(reportType)"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Entering")
    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- ${reportType} started for the Period: ${PeriodStartDate} to ${PeriodEndDate}")

    var batchStartTime = System.currentTimeMillis()

    var prmPolicyProcessor = getPremiumPoliciesProcessor(reportType)
    if (TerminateRequested) {
      TDIC_FinanceReportConstants.LOGGER.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before getPremiumAmounts(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }

    var premiums = prmPolicyProcessor.getPremiumAmounts({TDIC_FinanceReportConstants.WC7WORKERSCOMP, TDIC_FinanceReportConstants.COMMERCIAL_PROPERTY
                                ,TDIC_FinanceReportConstants.PROFESSIONAL_LIABILITY})

    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- ${reportType} premiums extraction completed. Count:${premiums?.size()}")
    if (TerminateRequested) {
      TDIC_FinanceReportConstants.LOGGER.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before generatePremiumLines(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }
    //prepare list of lines to be written to external file
    var prmLines = prmPolicyProcessor.getPremiumLinesProcessor().generatePremiumLines(premiums)
    OperationsExpected = OperationsExpected+prmLines?.size()
    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- ${reportType} premiums lines construction completed. Count:${prmLines?.size()}")
    if (TerminateRequested) {
      TDIC_FinanceReportConstants.LOGGER.warn("${logPrefix} - Terminate requested during generateGeneralLedgerReport() method(before writeToFile(${reportType})).")
      EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC GLInterface batch job for the Period: ${PeriodStartDate} to ${PeriodEndDate} is terminated.")
      return
    }

    writeToFile(reportType, prmLines)
    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- ${reportType} writing premium lines to a file is completed.")

    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- Total time taken for completing ${reportType}: ${System.currentTimeMillis()-batchStartTime}(ms)")
    TDIC_FinanceReportConstants.LOGGER.info("${logPrefix}- ${reportType} completed for the Period: ${PeriodStartDate} to ${PeriodEndDate}")
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix}- Exiting")
  }

}