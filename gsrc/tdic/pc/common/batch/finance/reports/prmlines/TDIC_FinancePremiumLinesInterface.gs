package tdic.pc.common.batch.finance.reports.prmlines

uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinanceReportPremiumObject

uses java.math.BigDecimal


interface TDIC_FinancePremiumLinesInterface {
  function generatePremiumLines(reportPremiums: List<TDIC_FinanceReportPremiumObject>) : List<String>
  function getRunGroup() : String
  function getSeqNumber() : int
  function getOldCompany(reportPremium: TDIC_FinanceReportPremiumObject, prmLineAccount: TDIC_FinancePremiumLineAccountObject): String
  function getSystemDate() : String
  function getReference(state : String, seqNum : int) : String
  function getPostingDate() : String
}