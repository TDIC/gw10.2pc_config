package tdic.pc.integ.plugins.retrorating

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.database.DatabaseUtil
uses com.tdic.util.properties.PropertyUtil
uses entity.GLHistoricalParameters_TDIC
uses gw.api.database.Query
uses gw.api.database.Relop
uses org.slf4j.LoggerFactory
uses java.sql.Connection
uses java.sql.SQLException
uses java.text.ParseException
uses java.text.SimpleDateFormat
uses java.time.Period

/**
 * GINTEG904 - Retro Rating Integration
 * 12/06/2019 Vipul Kumar
 *
 * Fetches legacy policy data from datamart table and populates on historical paramater tab  .
 */
class RetroRating_TDIC {

  /**
   * Standard logger for retro rating.
   */
  private static final var _logger = LoggerFactory.getLogger("TDIC_RetroRating")
  private static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private static final var SPECIALITY_CODE_TYPE = "SpecialityCode_TDIC"
  private static final var NAME_SPACE_LEGACYDATA = "tdic:historicaldata"
  /**
   * Flag to determine the discount coming from the Legacy System(AS400) database.
   */
  public static final var DISCOUNT_FLAG : String = "Y"

  /**
   * SQL query to pull out data from Legacy System(AS400) database.
   */
  public static final var RETRORATING_SQL_QRY : String = "select * from AS400.DLFPHCMS where POLNBR=?"

  /**
   * Key for looking up the DATAMART db url from properties file.
   */
  public static final var DATAMART_URL : String = "DataMartDBURL"
  /**
   * Trasaction code coming from legacy table.
   */
  public static final var TXN_CODE : String = "C0001"

  /**
   * This function will refresh the legacy policy data coming from datamart table
   * on historical tab screen, by picking latest five year data only.
   */
  function populateHistoricalData(polPeriod : PolicyPeriod) {
    _logger.debug("RetroRating_TDIC#populateHistoricalData() - Entering")
    if (!polPeriod.GLHistoricalParameters_TDIC.IsEmpty) {
      for (historicalParam in polPeriod.GLHistoricalParameters_TDIC) {
        // delete all the lines items coming from Legacy systems(AS400) while retaining rows added in policy center
        if (historicalParam.isIsLegacy() or (polPeriod.BasedOn != null and polPeriod.isLegacyConversion)) {
          polPeriod.removeFromGLHistoricalParameters_TDIC(historicalParam)
        }
      }
    }
    var _con : Connection = null
    try {
      //  retrieve the Datamart DB URL from tdicintegrations.properties file
      var _datamartDBURL =  PropertyUtil.getInstance().getProperty(DATAMART_URL)
      // Get a connection from DatabaseManager using the Datamart DB URL
      _con = DatabaseManager.getConnection(_datamartDBURL)
      var _preStatement = _con.prepareStatement(RETRORATING_SQL_QRY)
      _preStatement.setString(1, getHistoricalPolicyNumber(polPeriod))
      var _result = _preStatement.executeQuery()
      var retroActiveDate = polPeriod.GLLine?.GLPriorActsCov_TDIC?.GLPriorActsRetroDate_TDICTerm?.Value
      while (_result.next()) {
        // Load data with prior 5 Years of Historical Information
        var legacyEffectiveDate = toDate(_result.getString("DTEFF"))
        var timePeriod = Period.between(legacyEffectiveDate.toSQLDate().toLocalDate(), polPeriod.PeriodStart.toSQLDate().toLocalDate());
        var years = timePeriod.getYears();
        //GWPS-493 : ignore all those historical parameter records where "DLTCDE" is D.
        if (years <= 5 and retroActiveDate <= legacyEffectiveDate and _result.getString("DLTCDE") != "D") {
          if(_result.getString("TXNCDE")?.equals(TXN_CODE)) {
            var historicalParam = new GLHistoricalParameters_TDIC()
            historicalParam.EffectiveDate = toDate(_result.getString("DTEFF"))
            historicalParam.ExpirationDate = toDate(_result.getString("DTEXP"))
            historicalParam.Component = _result.getString("COMPNT")
            historicalParam.TerritoryCode = getTerritoryCode(polPeriod.BaseState.Code,_result.getString("COMPNT"))
            var spcltyCode = MAPPER.getInternalCodeByAlias(SPECIALITY_CODE_TYPE, NAME_SPACE_LEGACYDATA, _result.getString("SPCLTY"))
            if(spcltyCode != null)
            historicalParam.SpecialityCode = spcltyCode
            historicalParam.ClassCode = _result.getString("CLASS")
            historicalParam.IsLegacy = true
            switch (DISCOUNT_FLAG) {
              case _result.getString("NEWDEN"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_NEWGRADUATE
                break
              case _result.getString("FACMBR"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_FULLTIMEFACULTYMEMBER
                break
              case _result.getString("GRADST"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_FULLTIMEPOSTGRADUATE
                break
              case _result.getString("DISABL"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_TEMPORARYDISABILITY
                break
              case _result.getString("PRTDIS"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_PARTTIME_16
                break
              case _result.getString("PT20"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_PARTTIME_20
                break
              case _result.getString("LOADSC"):
                historicalParam.Discount = GLHistoricalDiscount_TDIC.TC_COVERAGEEXTENSION
                break
              default:
                historicalParam.Discount = null
            }
            polPeriod.addToGLHistoricalParameters_TDIC(historicalParam)
          }
        }
      }
    } catch (sql: SQLException) {
      _logger.error("RetroRating_TDIC#populateHistoricalData() - SQLException= " + sql)
      throw(sql)
    } catch (ex : Exception) {
      _logger.error("RetroRating_TDIC#populateHistoricalData() - Exception during updating historical data " + ex.StackTraceAsString)
      throw(ex)
    } finally {
      if(_con != null){
        try {
          _con.close()
        } catch (sql:SQLException) {
          _logger.error("RetroRating_TDIC#populateHistoricalData() - SQLException = " + sql)
          throw (sql)
        }
      }
    }
    _logger.debug("RetroRating_TDIC#populateHistoricalData() - Exiting")
  }

  /**
   * This function will convert the string date coming from datamart
   * table on historical tab screen, to Java date.
   */
  private function toDate(dbDate :  String): Date {
    var formatter = new SimpleDateFormat("yyyyMMdd");
    var date : Date
    try {
      date = formatter.parse(dbDate);
    } catch ( e: ParseException) {
      _logger.error("RetroRating_TDIC#populateHistoricalData() - ParseException = " + e.StackTraceAsString)
      throw e
    }
    return date
  }

  /**
   * This function will convert the legacyPolicyNumber_TDIC in policycenter
   * to match the corresponding legacypolicynumber format in datamart legacy table.
   */
  private function getHistoricalPolicyNumber(policyPeriod :  PolicyPeriod): String {
    // legacyPolcyNum = "CA 061939-5-C3"  datamartPolicyNum = "0619395"
    var datamartPolicyNum : String = ""
    datamartPolicyNum = (policyPeriod.LegacyPolicySource_TDIC == "DIMS") ? policyPeriod.LegacyPolicyNumber_TDIC?.replace("-", "")
                            : policyPeriod.LegacyPolicyNumber_TDIC?.substring(3,11)?.replace("-","")
    return datamartPolicyNum
  }

  /**
   * This function return the  territory code from system table,
   * based on component and state code passed
   */
  private function getTerritoryCode(stateCd :  String, componentCd : String): String {
    var territoryCode = Query.make(GLTerritory_TDIC)
            .compare("StateCode", Relop.Equals, stateCd)
            .compare("Component", Relop.Equals, componentCd).select().FirstResult?.TerritoryCode
    return territoryCode
  }

}