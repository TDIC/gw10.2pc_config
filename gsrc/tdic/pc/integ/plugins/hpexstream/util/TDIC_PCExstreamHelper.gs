/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package tdic.pc.integ.plugins.hpexstream.util

uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIdToPrintOrderMap
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap
uses com.tdic.plugins.hpexstream.core.util.ExstreamMapParam
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamPropertyUtil
uses com.tdic.plugins.hpexstream.core.util.TDIC_Helper
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.system.database.SequenceUtil
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentTemplateDescriptor
uses gw.plugin.document.IDocumentTemplateSource
uses gw.util.concurrent.LockingLazyVar
uses org.slf4j.LoggerFactory
uses entity.Job

class TDIC_PCExstreamHelper extends TDIC_ExstreamHelper implements TDIC_Helper {
  private static var _theInstance = LockingLazyVar.make(\-> new TDIC_PCExstreamHelper ())
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private static var _excludeDocsFromLiveDate = {"WC040004"}
  private static var _includeDocsFromLiveDate = {"WC040004F"}
  private static var _portalStatusYes =  "Yes"
  private static var _portalStatusNo =  "No"
  private static var _authorSystemGenerated = "System Generated"
  private static var _endorsmentAndCertifiateHolderForms = {
      "BOP2517(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERBOP.Code,
      "LRP2518(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERLRP.Code,
      "OCC2010(B)AS" -> TDIC_DocCreationEventType.TC_CERTIFICATEHOLDERCOIOCC.Code,
      "OCC2020(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDEROCCBL.Code,
      "OCC2025(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDEROCCPL.Code,
      "PBL2010(B)AS" -> TDIC_DocCreationEventType.TC_CERTIFICATEHOLDERCOIPBL.Code,
      "PBL2020(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERPBLBL.Code,
      "PBL2025(B)AS" -> TDIC_DocCreationEventType.TC_ENDORSEMENTHOLDERPBLPL.Code,
      "PBL2027(B)AS" -> TDIC_DocCreationEventType.TC_CERTIFICATEHOLDERSSPBL.Code,
      "BOP2030(B)AS" -> TDIC_DocCreationEventType.TC_LENDERSLOSSPAYABLEBOP.Code,
      "BOP2515(B)AS" -> TDIC_DocCreationEventType.TC_MORTGAGEEENDORSEMENTBOP.Code,
      "LRP2515(B)AS" -> TDIC_DocCreationEventType.TC_MORTGAGEEENDORSEMENTLRP.Code,
      "LRP2220(B)WA" -> TDIC_DocCreationEventType.TC_MORTGAGEEENDORSEMENTLRPWA.Code,
      "BOP2220(B)WA" -> TDIC_DocCreationEventType.TC_MORTGAGEEENDORSEMENTBOPWA.Code,
          "NWPaymentPlanLetter" -> TDIC_DocCreationEventType.TC_NWPAYMENTPLANLETTER.Code}

  private static var _discountConforForms = {
      "PLFCLTY" -> TDIC_DocCreationEventType.TC_PLFACULTYMEMBERDISCOUNT.Code,
      "PLGRAD" -> TDIC_DocCreationEventType.TC_PLGRADUATESTUDENTDISCOUNT.Code,
      "PLPT16" -> TDIC_DocCreationEventType.TC_PLPT16DISCOUNT.Code,
      "PLPT20" -> TDIC_DocCreationEventType.TC_PLPT20DISCOUNT.Code,
      "PLTEMPDI" -> TDIC_DocCreationEventType.TC_PLDISABILITYDISCOUNT.Code,
      "PLVLNTR" -> TDIC_DocCreationEventType.TC_PLVOLUNTEERPOLICY.Code,
      "NEWDENTIST" -> TDIC_DocCreationEventType.TC_NEWDENTISTOFFER.Code,
      "NONRENEWCA" -> TDIC_DocCreationEventType.TC_NONRENEWALNONMEMBERSHIP.Code
      }

  static property get Instance(): TDIC_PCExstreamHelper {
    return _theInstance.get()
  }

  /**
   * This function
   * 1) Sets the appropriate document status and doc delivery status
   * 2) Instantiates Document Approval Workflow
   * when the user tries to Finalize a live document
   *
   * @param pLiveDocument  Live document being finalized
   * @return void
   */
  function finalizeLiveDocument(pLiveDocument: Document): void {
    _logger.trace("TDIC_PCExstreamHelper#finalizeLiveDocument(Document) - Entering.")

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {

      // Update document status and doc delivery status
      var writeableDoc = bundle.add(pLiveDocument)
      writeableDoc.Status = DocumentStatusType.TC_APPROVING
      writeableDoc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING

      // Save updated document metadata
      try {
        var documentMetadataSource = Plugins.get(gw.plugin.document.IDocumentMetadataSource)
        documentMetadataSource.saveDocument(writeableDoc)
      }catch (e : Exception) {
        // ignore
        _logger.warn("BaseLocalDocumentMetadataSource.storeDocument: " + e.Message, e)
      }

      // Instantiate workflow
      var workflow = new entity.DocumentApprovalWF_TDIC()
      workflow.Document = writeableDoc
      workflow.startAsynchronously()
    })
    _logger.trace("TDIC_PCExstreamHelper#finalizeLiveDocument(Document) - Exiting.")
  }

  /**
   * US555
   * 11/18/2014 Shane Murphy
   *
   * Returns a document name for a given PolicyPeriod
   */
  @Param("aPolicyNumber", "PolicyNumber of the document's associated policy")
  @Param("aTemplateId", "Template ID used to create document")
  @Returns("Template Desccriptor")
  function getDocumentName(aPolicyNumber: String, aTemplateId: String): String {
    if (aTemplateId == null){
      throw new IllegalArgumentException("Template Id Is NULL")
    }
    return aTemplateId + DateUtil.currentDate()
  }

  /**
   * GW 51, 142
   * 08/20/2015 Kesava Tavva
   *
   * Returns a document name for a given PolicyPeriod
   */
  @Param("aPolicyNumber", "PolicyNumber of the document's associated policy")
  @Param("aTemplateId", "Template ID used to create document")
  @Returns("Template Desccriptor")
  function getDocumentName(templateDescriptor: IDocumentTemplateDescriptor): String {
    if (templateDescriptor.TemplateId == null){
      throw new IllegalArgumentException("Template Id Is NULL")
    }
    if(templateDescriptor.TemplateId == typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE.Code
        || templateDescriptor.TemplateId == typekey.TDIC_DocCreationEventType.TC_AUDITREMINDER.Code)
      return templateDescriptor.Name + DateUtil.currentDate()
    else
      return templateDescriptor.TemplateId + DateUtil.currentDate()
  }

  override function createStub(aTemplateId: String, aPrintOrder: int, anEntity: Object, eventName: String, user: String) {
    createDocumentStub(aTemplateId, aPrintOrder, (anEntity as PolicyPeriod), eventName, user)
  }

  /**
   * US555
   * 02/18/2015 Shane Murphy
   *
   * Creates a System Generated document for a given PolicyPeriod
   */
  @Param("aTemplateId", "Template to create stub for")
  @Param("aPolicyPeriod", "PolicyPeriod to associate document stub with")
  function createDocumentStub(aTemplateId: String, aPrintOrder: int, aPolicyPeriod: PolicyPeriod, eventName: String): Document {
    return createDocumentStub(aTemplateId, aPrintOrder, aPolicyPeriod, eventName, "System Generated")
  }

  /**
   * US555
   * 11/18/2014 Shane Murphy
   *
   * Creates a document for a given PolicyPeriod by a given user
   */
  @Param("aTemplateId", "Template to create stub for")
  @Param("aPolicyPeriod", "PolicyPeriod to associate document stub with")
  function createDocumentStub(aTemplateId: String, aPrintOrder: int, aPolicyPeriod: PolicyPeriod, eventName: String, user: String): Document {
    var docInfoAsString = getDocInfoAsString(aPolicyPeriod, eventName)
    _logger.info("TDIC_PCExstreamHelper#createDocumentStub() - Doc stub creation started for ${docInfoAsString}.")
    if(eventName != "OnDemand"){
      if((aTemplateId == null || !getEventMapParams(eventName, aPolicyPeriod.Job.getJobDate())?.HasElements)) {
        _logger.info("TDIC_PCExstreamHelper#createDocumentStub() - TemplateID is null for ${docInfoAsString} . Can not proceed. Returning.")
        throw new Exception("Document template mappings not found. Please contact System Administrator. Policy#:"+ aPolicyPeriod.PolicyNumber)
      }
    }
    var doc: Document
    var dts = gw.plugin.Plugins.get(IDocumentTemplateSource)
    var templateDescriptor = dts.getDocumentTemplate(aTemplateId, null)
    if (templateDescriptor != null ) {
      if(gw.transaction.Transaction.getCurrent() != null){
        doc = new Document()
        gw.transaction.Transaction.getCurrent().add(doc)
      }else{
        //Using aPolicyPeriod.Bundle for scenarios where the document stub production request is initiated from a workflow.
        doc = new Document(aPolicyPeriod.Bundle);
      }
      doc.PendingDocUID = (SequenceUtil.next(1, "ExstreamDocUID")) as String
      doc.Name = getDocumentName(templateDescriptor)
      doc.Status = DocumentStatusType.TC_APPROVED
      doc.Policy = aPolicyPeriod.Policy

      // BrianS - Do not associate documents to policy periods.  It causes preemptions to fail (GW-3079)
      // doc.PolicyPeriod = aPolicyPeriod

      doc.Account = aPolicyPeriod.Policy.Account
      doc.Job = aPolicyPeriod.Job
      doc.Type = typekey.DocumentType.get(templateDescriptor.TemplateType)
      doc.Subtype = typekey.OnBaseDocumentSubtype_Ext.get(templateDescriptor.getMetadataPropertyValue("subtype") as String)
      doc.Author = user
      doc.Description = templateDescriptor.Description
      doc.MimeType = templateDescriptor.MimeType
      doc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING
      doc.TemplateId_TDIC = aTemplateId
      doc.Event_TDIC = eventName.substring(eventName.indexOf(".") + 1)
      doc.DateCreated = DateUtil.currentDate()
      doc.DateModified = DateUtil.currentDate()
      doc.PrintOrder_TDIC = aPrintOrder
      doc.PortalStatus_TDIC = user.equalsIgnoreCase(_authorSystemGenerated) ? _portalStatusYes : _portalStatusNo
    } else {
      _logger.warn("TDIC_PCExstreamHelper#createDocumentStub() - No Template Descriptor found for Template Id " + aTemplateId)
    }
    _logger.info("TDIC_PCExstreamHelper#createDocumentStub() - Doc stub creation completed for ${getDocInfoAsString(aPolicyPeriod, eventName)} - DocUID: ${doc.DocUID}.")
    return doc
  }
  /**
   * US555
   * 11/20/2014 Shane Murphy
   *
   * Creates a document stub for all template Id's
   */
  @Param("aTemplateIdList", "Templates to create stubs for")
  @Param("aPolicyPeriod", "PolicyPeriod for documents to be associated with")
  function createDocumentStubs(eventName: String, isJob: boolean, aPolicyPeriod: PolicyPeriod): void {
    _logger.trace("TDIC_PCExstreamHelper#createDocumentStubs() - Entering.")
    var templatesForEvent = aPolicyPeriod.Job.getTemplates(eventName, isJob)
    gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
      templatesForEvent.eachKeyAndValue(\k, val -> createDocumentStub(k, val, aPolicyPeriod, eventName))
    })
    _logger.debug("TDIC_PCExstreamHelper#createDocumentStubs() - Created " + templatesForEvent.Count + " document stubs.")
    _logger.trace("TDIC_PCExstreamHelper#createDocumentStubs() - Exiting.")
  }
  
  /**
   * US644
   * 11/21/2014 Shane Murphy
   *
   * Gets all documents for the current job and separates them based on their
   * Template Descriptors immediate-creation meta data property.
   */
  @Param("job", "Job to return documents for")
  @Returns("2 Lists of 'immediate' and 'overnight' documents as a HashMap")
  function getDocumentsForCurrentJob(job: Job, anEvent: String): HashMap {
    _logger.trace("TDIC_PCExstreamHelper()#getDocumentsForCurrentJob(${job}) - Entering.")
    var documents = getDocs(job, anEvent)
    if (documents != null) {
      var batchDocuments = new ArrayList<Document>()
      var immediateDocuments = new ArrayList<Document>()
      var dts = Plugins.get(IDocumentTemplateSource)
      var bundle = job.Bundle
      for (doc in documents) {
        var templateDescriptor = dts.getDocumentTemplate(doc.TemplateId_TDIC, null)
        if (templateDescriptor != null){
          doc = bundle.add(doc)
          doc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENT
          var immediateCreation = Coercions.makeBooleanFrom(templateDescriptor.getMetadataPropertyValue("immediatecreation"))
          if (immediateCreation == true) {
            immediateDocuments.add(doc)
          } else {
            batchDocuments.add(doc)
          }
        }
      }
      _logger.info("TDIC_PCExstreamHelper#getDocumentsForCurrentJob() - Returning ${immediateDocuments.Count} Immediate documents and ${batchDocuments.Count} Batch documents")
      _logger.trace("TDIC_PCExstreamHelper()#getDocumentsForCurrentJob() - Exiting.")
      return {"immediate" -> immediateDocuments, "overnight" -> batchDocuments}
    } else {
      _logger.info("TDIC_PCExstreamHelper#getDocumentsForCurrentJob() - No documents for current job.")
      _logger.trace("TDIC_PCExstreamHelper()#getDocumentsForCurrentJob() - Exiting.")
      return null
    }
  }

  /**
   * US644
   * 11/21/2014 Shane Murphy
   *
   * Queries for all documents related to a job.
   */
  @Param("job", "Job which the required documents were creeated on.")
  @Returns("Job's associated documents")
  function getDocs(job: Job, anEvent: String): List<Document> {
    _logger.trace("TDIC_PCExstreamHelper#getDocs(${job}) - Entering")
    //804 upgrade - Getting documents from the bundle available from the Job process
    var documents = job.Bundle.InsertedBeans.whereTypeIs(entity.Document)?.where( \ doc -> doc.Event_TDIC == anEvent and doc.Job == job)
    _logger.info("TDIC_PCExstreamHelper#getDocs(${job}) - Number of documents retrieved from the bundle: " + documents.Count)
    if(!documents.HasElements){
      documents = Query.make(entity.Document)
        .compare(Document#Job, Equals, job)
        .compare(Document#DeliveryStatus_TDIC, Equals, DocDlvryStatusType_TDIC.TC_SENDING)
        .compare(Document#Event_TDIC, Equals, anEvent)
        .select()
        .toList()
      _logger.info("TDIC_PCExstreamHelper#getDocs(${job}) - Number of documents retrieved from the query: " + documents.Count)
    }
    if (documents.Count > 0){
      _logger.info("TDIC_PCExstreamHelper#getDocs(${job}) - Docs retrieved:")
      documents.each(\doc -> _logger.info("DocUID: ${doc.DocUID}; JobID: ${doc.Job.ID}; PPID: ${doc.PolicyPeriod.ID}; PolNum: ${doc.PolicyPeriod.PolicyNumber}; Evnet: ${doc.Event_TDIC}; Template: ${doc.TemplateId_TDIC}"))
    }
    _logger.trace("TDIC_PCExstreamHelper#getDocs(${job}) - Exiting")
    return documents
  }

  /**
   * US644
   * 11/21/2014 Shane Murphy
   *
   * Creates a message for documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("aBatchIdMap", "HashMap of templateIds and their corresponding document stub public Id")
  @Param("aDoc", "Document to create message for.")
  function createBatchMessage(aMessageContext: MessageContext, aBatchIdMap: TDIC_TemplateIds, aBatchRoot: Document): void {
    _logger.trace("TDIC_PCExstreamHelper#createBatchMessage() - Entering.")
    try {
      if (aBatchIdMap != null){
        var aPolicyPeriod = aBatchRoot.PolicyPeriod
        if (aPolicyPeriod == null) {
          aPolicyPeriod = aBatchRoot.Job.LatestPeriod;
        }

        var slice: PolicyPeriod

        /**
         * US669
         * 03/31/2015 Shane Murphy
         *
         * If there is no Policy Period, create a new blank one so that a Policy Period
         * with null fields is still sent to Exstream
         */
        if (aPolicyPeriod == null) {
          slice = new PolicyPeriod(){: EditEffectiveDate = new Date()}
        } else {
          slice = aPolicyPeriod.Slice ? aPolicyPeriod : aPolicyPeriod.getSlice(aPolicyPeriod.EditEffectiveDate)
        }

        var parameters = new java.util.HashMap(){"policyPeriod" -> slice}
        var payload = com.tdic.plugins.hpexstream.core.xml
            .TDIC_ExstreamXMLCreatorFactory
            .getExstreamXMLCreator(parameters)
            .generateTransactionXML(aBatchIdMap, aMessageContext.EventName, true, null, null)
        var message = aMessageContext.createMessage(payload)
        message.MessageRoot = aBatchRoot
      }
    }catch(c){
      throw "Exception occurred in createBatchMessage method, for Policy#:" + aBatchRoot.PolicyPeriod.PolicyNumber + ":Exception:" + c
    }

    _logger.info("TDIC_PCExstreamHelper#createBatchMessage() - Payload generation completed for ${getDocInfoAsString(aBatchRoot.PolicyPeriod, aBatchRoot.Event_TDIC)}")
    _logger.trace("TDIC_PCExstreamHelper#createBatchMessage() - Exiting.")
  }

  /**
   * US555
   * 11/24/2014 Shane Murphy
   *
   * Gets the template Ids for the specified event Name and date
   */
  @Param("anEventName", "Event name to get associated Template Ids for")
  @Param("asOfDate", "Date the template should be in effect for")
  @Returns("List of associated template Ids")
  function getEventMapParams(anEventName: String, asOfDate: Date): ArrayList<ExstreamMapParam> {
    _logger.trace("TDIC_PCExstreamHelper#getEventMapParams() - Entering")
    var templateIds = new ArrayList<ExstreamMapParam>()
    var mappingForEvent = TDIC_ExstreamPropertyUtil.getInstance().getEventTemplateMappings(anEventName)
    for (mapping in mappingForEvent) {
      setLiveDateForTemplate(mapping)
      // US644, 11/24/2014, shanem: Filter out mappings that don't fall between the effective and expiration dates
      if ((mapping.EffectiveDate == null || mapping.EffectiveDate <= asOfDate)
          && (mapping.ExpirationDate == null || mapping.ExpirationDate >= asOfDate)) {
        templateIds.add(new ExstreamMapParam(){
            : EventName = mapping.EventName,
            : TemplateId = mapping.TemplateId,
            : EffectiveDate = mapping.EffectiveDate,
            : ExpirationDate = mapping.ExpirationDate,
            : PrintOrder = mapping.PrintOrder,
            : DocType = mapping.DocType
        })
      }
    }
    _logger.debug("TDIC_PCExstreamHelper#getEventMapParams() - Event: ${anEventName}; AsOfDate: ${asOfDate} - ${templateIds.Count} Templates ")
    _logger.trace("TDIC_PCExstreamHelper#getEventTemplates() - Exiting")
    return templateIds
  }

  /**
   * US669
   * 05/12/2015 Shane Murphy
   *
   * Skip document creation and just create an event.
   * Used where HP Exstream will store the document locally for the user, and not in the DMS
   */
  @Param("aTemplateId", "The TemplateId")
  @Param("anEntity", "A Policy Period needs to be passed as this variable")
  @Param("aBundle", "A Bundle")
  override function skipDocCreation(aTemplateId: String, anEntity: Object, aBundle: Bundle) {
    if (anEntity typeis PolicyPeriod) {
      var policyPeriod = aBundle.add(anEntity)
      policyPeriod.addEvent(aTemplateId)
    }
  }

  /**
   * US669
   * 05/15/2015 Shane Murphy
   *
   * Creates a message for Account documents to be sent asynchronously
   *
   * Event name is passed as the messageContext.EventName may be different.
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("anEventName", "The Event.")
  @Param("aMessageContext", "The data type is: MessageContext")
  function createMessage(aMessageContext: MessageContext, anEventName: String, aDocument: Document): void {
    var aPolicyPeriod = aMessageContext.Root as PolicyPeriod
    var templatesForEvent = aPolicyPeriod.Job.getTemplates(anEventName, true)

    var templateList = new ArrayList<TDIC_TemplateIdToPrintOrderMap>()
    templatesForEvent.eachKeyAndValue(\templateId, printOrder -> {
      templateList.add(new TDIC_TemplateIdToPrintOrderMap(templateId, printOrder))
    })

    //US555, 11/13/14, shanem: the message root just needs to be any batch document for this policy
    if (Document != null){
      var batchIds = new ArrayList<TDIC_TemplateidToDocPubIdMap>()
      var templates = new ArrayList<String>()
      var orderedTemplates = templateList.orderBy(\elt -> elt.PrintOrder)
      orderedTemplates.each(\temp -> {
        templates.add(temp.TemplateId)
        batchIds.add(new TDIC_TemplateidToDocPubIdMap(temp.TemplateId, aDocument))
      })

      var templateMap = new TDIC_TemplateIds(templates?.toTypedArray(), batchIds?.toTypedArray())

      createBatchMessage(aMessageContext, templateMap, aDocument)
    }
  }

  function printQuoteDocs(aPolicyPeriod: PolicyPeriod) {
    aPolicyPeriod.printDocument("ODWCQuoteProposal", "Submission Quote", gw.api.web.document.DocumentsHelper.PrintQuoteParameters)
  }

  /**
   * JIRA 184
   * 10/13/2015 Kesava Tavva
   *
   * Set Live Date for excluded and included documents to trigger document generation
   * based on live date defined using script parameter
   */
  @Param("aTemplateId", "The TemplateId")
  @Param("anEntity", "A Policy Period needs to be passed as this variable")
  @Param("aBundle", "A Bundle")
  private function setLiveDateForTemplate(mapping: ExstreamMapParam) {
    var _WC7ReleaseAGoLiveDate = PropertyUtil.getInstance().getProperty("TDIC_WC7ReleaseAGoLiveDate")
    if(_excludeDocsFromLiveDate.contains(mapping.TemplateId))
      mapping.ExpirationDate = (Coercions.makeDateFrom(_WC7ReleaseAGoLiveDate))

    if(_includeDocsFromLiveDate.contains(mapping.TemplateId))
      mapping.EffectiveDate = (Coercions.makeDateFrom(_WC7ReleaseAGoLiveDate))
  }

  function getDocInfoAsString(pp: PolicyPeriod, event: String):String{
    return "AccountNumber: ${pp.Policy.Account.AccountNumber}; PolicyNumber: ${pp.PolicyNumber}; Term: ${pp.TermNumber}; Event: ${event}"
  }

  /**
   * Function to get lob code based on LOB.
   * @param policyPeriod
   * @return
   */
  public static function getLOBCode(policyPeriod: PolicyPeriod): String{
    switch(true){
      case policyPeriod.WC7LineExists:
        return "wc"
      case policyPeriod.GLLineExists:
        return "pl"
      case policyPeriod.BOPLineExists:
        return "cp"
      default:
        return null
    }
  }

  public static function getEndorsmentAndCertifiateHolderForms(): Map {
    return _endorsmentAndCertifiateHolderForms
  }

  public static function getDiscountConforForms(): Map {
    return _discountConforForms
  }

}
