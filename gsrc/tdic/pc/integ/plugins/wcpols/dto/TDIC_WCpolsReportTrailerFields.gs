package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/11/14
 * Time: 5:10 PM
 * This class includes variables for all fields in report trailer record for policy specification report.
 */
class TDIC_WCpolsReportTrailerFields  extends  TDIC_FlatFileLineGenerator {
  var _blank : String as Blank
  var _recordTypeCode : String as RecordTypeCode
  var _recordTotals : long as RecordTotals
  var _headerRecordTotals : long as HeaderRecordTotals
  var _transactionFromDate : String as TransactionFromDate
  var _transactionToDate : String as TransactionToDate
  var _endBlank : String as EndBlank
}