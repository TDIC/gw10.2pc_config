package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/10/16
 * Time: 11:00 AM
 * This class includes variables for all fields in Xmod factor change record for policy specification report.
 */
class TDIC_WCpolsExpRatingModChangeEndRecFields extends  TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _expModEffectiveDate : String as ExpModEffectiveDate
  var _expModFactor : String as ExpModFactor
  var _expModStatusCode : int as ExpModStatusCode
  var _futureReserved2 : String as FutureReserved2
  var _nameOfInsured : String as NameOfInsured
  var _endorsementEffDate : String as EndorsementEffDate
  var _futureReserved3 : String as FutureReserved3

}