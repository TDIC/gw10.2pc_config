package tdic.pc.integ.plugins.wcpols.dto

uses tdic.pc.integ.plugins.wcpols.TDIC_WCpolsFlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 9/15/16
 * Time: 11:48 AM
 * This class includes variables for all fields in data elements change record for policy specification report.
 */
class TDIC_WCpolsDataElementsChgEndRecFields extends TDIC_WCpolsFlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _futureReserved1 : String as FutureReserved1
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved2 : String as FutureReserved2
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _carrierCode1 : String as CarrierCode1
  var _policyNumberIdentifier : String as PolicyNumberIdentifier
  var _policyEffectiveDate1 : String as PolicyEffectiveDate1
  var _policyExpirationDate : String as PolicyExpirationDate
  var _legalNatureOfInsuredCode : String as LegalNatureOfInsuredCode
  var _textForOtherlegNatOfInsrd : String as TextForOtherLegNatOfInsrd
  var _item3AOr3CCode : String as Item3AOr3CCode
  var _item3CInclusionOrExclusionCode : String as Item3CInclusionOrExclusionCode
  var _stateCodesForItem3AOr3C : String as StateCodesForItem3AOr3C
  var _stateCodesForItem3AOr3C1 : String as StateCodesForItem3AOr3C1
  var _stateCodesForItem3AOr3C2 : String as StateCodesForItem3AOr3C2
  var _stateCodesForItem3AOr3C3 : String as StateCodesForItem3AOr3C3
  var _stateCodesForItem3AOr3C4 : String as StateCodesForItem3AOr3C4
  var _stateCodesForItem3AOr3C5 : String as StateCodesForItem3AOr3C5
  var _stateCodesForItem3AOr3C6 : String as StateCodesForItem3AOr3C6
  var _stateCodesForItem3AOr3C7 : String as StateCodesForItem3AOr3C7
  var _stateCodesForItem3AOr3C8 : String as StateCodesForItem3AOr3C8
  var _stateCodesForItem3AOr3C9 : String as StateCodesForItem3AOr3C9
  var _stateCodesForItem3AOr3C10 : String as StateCodesForItem3AOr3C10
  var _stateCodesForItem3AOr3C11: String as StateCodesForItem3AOr3C11
  var _stateCodesForItem3AOr3C12: String as StateCodesForItem3AOr3C12
  var _stateCodesForItem3AOr3C13: String as StateCodesForItem3AOr3C13
  var _stateCodesForItem3AOr3C14: String as StateCodesForItem3AOr3C14
  var _emprLiabLimAmntBodInjByAcciEachAcciAmnt : long as EmprLiabLimAmntBodInjByAcciEachAccAmnt
  var _emprLiabLimAmntBodInjByDisePolLimAmnt : long as EmprLiabLimAmntBodInjByDisePolAmnt
  var _emprLiabLimAmntBodInjByDiseEachEmpeAmnt : long as EmprLiabLimAmntBodInjByDiseEachEmpeAmnt
  var _premiumAdjustmentPeriodCode : String as PremiumAdjustmentPeriodCode
  var _endorsementNumber1 : String as EndorsementNumber1
  var _bureauVersionIdentifier1 : String as BureauVersionIdentifier1
  var _carrierVersionIdentifier1 : String as CarrierVersionIdentifier1
  var _nameOfProducer : String as NameOfProducer
  var _interstateRiskIdNumber : String as InterstateRiskIdNumber
  var _futureReserved3 : String as FutureReserved3
  var _endorsementNumberRevisionCode : String as EndorsementNumberRevisionCode
  var _futureReserved4 : String as FutureReserved4
  var _endorsementSequenceNumber : String as EndorsementSequenceNumber
  var _nameOfInsured : String as NameOfInsured
  var _endorsementEffectiveDate : String as EndorsementEffectiveDate
  var _futureReserved5 : String as FutureReserved5


}