package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 9/15/16
 * Time: 1:42 PM
 * This class includes variables for all fields in address change record for policy specification report.
 */
class TDIC_WCpolsAddressChangeEndRecFields extends  TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _futureReserved1 : String as FutureReserved1
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved2 : String as FutureReserved2
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _addressTypeCode : int as AddressTypeCode
  var _addressStructureCode : int as AddressStructureCode
  var _street : String as Street
  var _city : String as City
  var _state : String as State
  var _zipCode : String as ZipCode
  var _nameLinkIdentifier : int as NameLinkIdentifier
  var _stateCodeLink : int as StateCodeLink
  var _exposureRecLinkForLocCode : int as ExposureRecLinkForLocCode
  var _futureReserved3 : String as FutureReserved3
  var _emailAddress : String as EmailAddress
  var _foreignAddressInd : String as ForeignAddressInd
  var _geographicArea : String as GeographicArea
  var _countryCode : String as CountryCode
  var _emailAddressContinued : String as EmailAddressContinued
  var _addressRevisionCode : String as AddressRevisionCode
  var _nameOfInsured1 : String as NameOfInsured1
  var _endorsementEffectiveDate : String as EndorsementEffectiveDate
  var _nameLinkCounterIdntfr : String as NameLinkCounterIdntfr
  var _futureReserved4 : String as FutureReserved4

}