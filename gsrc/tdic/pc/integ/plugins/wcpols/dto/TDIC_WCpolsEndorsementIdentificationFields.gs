package tdic.pc.integ.plugins.wcpols.dto

uses java.util.ArrayList
uses tdic.util.flatfile.model.classes.Record
uses tdic.util.flatfilegenerator.TDIC_FlatFileUtilityHelper
uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 1:37 PM
 * This class includes variables for all fields in endorsement identification record for policy specification report.
 */
class TDIC_WCpolsEndorsementIdentificationFields  extends  TDIC_FlatFileLineGenerator {
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _endorsementNumbers : ArrayList<TDIC_EndorsementNumberFields> as EndorsementNumbers
  var _futureReserved2 : String as FutureReserved2
  var _polChngEffDate : String as PolChngEffDate
  var _polChngExpDate : String as PolChngExpDate



  override function createFlatFileLine(record:Record):String{

    var flatFileLine=""
    var endorsementCount=0
    var bureauVersionIdentifierCount=0
    var carrierVersionIdentifierCount = 0
    for(field in record.Fields){

      var value =""
      if(field.Name=="EndorsementNumber"){
        if(this.EndorsementNumbers.Count > endorsementCount ) {
          value = this.EndorsementNumbers.get(endorsementCount).EndorsementNumber.replaceAll("[^0-9A-Z]", "")
          endorsementCount++
        }
      }
      else if(field.Name=="BureauVersionIdentifier"){
        if(this.EndorsementNumbers.Count > bureauVersionIdentifierCount ) {
          value = this.EndorsementNumbers.get(bureauVersionIdentifierCount).BureauVersionIdentifier
          bureauVersionIdentifierCount++
        }
      }
      else if(field.Name=="CarrierVersionIdentifier"){
          if(this.EndorsementNumbers.Count > carrierVersionIdentifierCount ) {
            value = this.EndorsementNumbers.get(carrierVersionIdentifierCount).CarrierVersionIdentifier
            carrierVersionIdentifierCount++
          }
      }
      else{

        value = objHashMap().get(field.Name)
      }
        flatFileLine += TDIC_FlatFileUtilityHelper.formatString(value, field.Length as int, field.Truncate, field.Justify, field.Fill, field.Format, field.Strip) //Format string and add it to flat file line
    }
    return flatFileLine
  }
}