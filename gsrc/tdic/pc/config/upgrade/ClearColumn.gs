package tdic.pc.config.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.system.PLLoggerCategory
uses org.slf4j.LoggerFactory

// BrianS - Class that can be used to remove all data from a column.
class ClearColumn  extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${RenameColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber
  private var _tableName : String as readonly TableName
  private var _columnName : String as readonly ColumnName

  construct(versionNumber : int, tableName : String, columnName : String) {
    super(versionNumber)
    _versionNumber = versionNumber
    _tableName = tableName
    _columnName = columnName
  }

  override public property get Description() : String {
    return "Remove data from " + _tableName + "." + _columnName
  }

  // BrianS - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false
  }

  override function execute() : void {
    var table = getTable(_tableName)
    var column = table.getColumn(_columnName)

    // Step 1:  Set column to null.
    _logger.info (_LOG_TAG + "Clearing data from " + _tableName + "." + _columnName)
    var builder = table.update()
    builder.set (column, null)
    builder.execute()

    _logger.info (_LOG_TAG + "Completed " + Description)
  }
}