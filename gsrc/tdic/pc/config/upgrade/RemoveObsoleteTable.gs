package tdic.pc.config.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses org.slf4j.LoggerFactory
/**
 * BrittoS - Class that can be used to remove tables.
 */
class RemoveObsoleteTable extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${RemoveObsoleteTable.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber
  private var _tableName : String as readonly TableName


  construct(versionNumber : int, tableName : String) {
    super(versionNumber)
    _versionNumber = versionNumber
    _tableName = tableName
  }

  override property get Description(): String {
    return "Remove Obsolete Table " + _tableName
  }

  override function hasVersionCheck(): boolean {
    return false
  }

  override function execute() {

    var table = getTable(_tableName)
    // Step 1:  Delete Table.
    _logger.info (_LOG_TAG + "Removing Table " + _tableName)
    table.drop()
    _logger.info (_LOG_TAG + "Completed " + Description)
  }

}