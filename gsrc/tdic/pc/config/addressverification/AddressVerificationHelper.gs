package tdic.pc.config.addressverification

uses java.util.ArrayList
uses gw.api.util.DisplayableException
uses java.util.HashMap

uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

/**
 * ID:US553
 * Name: Address Verification
 * User: Praneethk
 * Description:  Address Verification Helper is a Helper Class for the Melissa Data Handler.
 */
class AddressVerificationHelper {

  private var _logger = LoggerFactory.getLogger("Integration")
  private var overrideVisible = false
  private var updateVisible   = false
  private var standardizeVisible = true
  private var addressList = new ArrayList<Address>()
  private var exceptions = ""

  /* private variables used for address standardization. */
  var _dataHandler : MelissaDataHandler
  var _standardizedAddress : Address
  var _errorCodes : String
  var _errorCodeArray : String[]

  /* Cache for Integ DB */
  public static var errorCodeCache: HashMap<String,ErrorMessageProp> = new HashMap<String,ErrorMessageProp>()

  /**
   * createHashMap() is called using Constructor
   */
  construct(){
    createHashMap()
  }

  /**US553 - Praneethk
   *This function will create the Hashmap with corresponding keys and respective values.
   *Where Keys are Error Codes that Melissa Data Returns and Values are Display Messages Associated with Respective Error Codes.
   */
  function createHashMap(){

    var _AE01 = new ErrorMessageProp("AE01", "The ZIP or Postal Code does not exist and could not be determined by the city/municipality and state/province",
        null,true,false,false)
    errorCodeCache.put(_AE01.code,_AE01)

    var _AE02 = new ErrorMessageProp("AE02","The street name was not found.",null,true,false,false)
    errorCodeCache.put(_AE02.code,_AE02)

    var _AE03 = new ErrorMessageProp("AE03","Either the directionals (N, E, SW, etc) or the suffix (AVE, ST, BLVD) are missing or invalid.",
        null, true, false, false)
    errorCodeCache.put(_AE03.code,_AE03)

    var _AE04 = new ErrorMessageProp("AE04","The physical location exists but there are no addresses on this side of the street.",
        null, true, false, false)
    errorCodeCache.put(_AE04.code,_AE04)

    var _AE05 = new ErrorMessageProp("AE05","Input matched to multiple addresses and there is not enough information to break the tie.",
        null, true, false, false)
    errorCodeCache.put(_AE05.code,_AE05)

    var _AE06 = new ErrorMessageProp("AE06","This address cannot be verified now but will be at a future date.",null,true,false,false)
    errorCodeCache.put(_AE06.code,_AE06)

    var _AE07 = new ErrorMessageProp("AE07", "The required combination of address/city/state or address/zip is missing.",
        null, true, false, false)
    errorCodeCache.put(_AE07.code,_AE07)

    var _AE08 = new ErrorMessageProp("AE08", "The suite or apartment number is not valid. Select a Suggested Address",
        null, true, false, true)
    errorCodeCache.put(_AE08.code,_AE08)

    var _AE09 = new ErrorMessageProp("AE09", "Address is not complete. Please select the correct address below or override", null,
        true, false, true)
    errorCodeCache.put(_AE09.code, _AE09)

    var _AE10 = new ErrorMessageProp("AE10","The address number in the input address is not valid.", null, true, false, false)
    errorCodeCache.put(_AE10.code,_AE10)

    var _AE11 = new ErrorMessageProp("AE11", "The address number in the input address is missing.", null, true, false, false)
    errorCodeCache.put(_AE11.code,_AE11)

    var _AE12 = new ErrorMessageProp("AE12", "The input address box number is invalid.", null, true, false, false)
    errorCodeCache.put(_AE12.code,_AE12)

    var _AE13 = new ErrorMessageProp("AE13", "The input address box number is missing.", null, true, false, false)
    errorCodeCache.put(_AE13.code,_AE13)

    var _AE14 = new ErrorMessageProp("AE14","The address is a Commercial Mail Receiving Agency (CMRA) and the Private Mail Box (PMB or #) number is missing.",
        null, true, false, false)
    errorCodeCache.put(_AE14.code,_AE14)

    var _AE15 = new ErrorMessageProp("AE15", "Limited to Demo Mode operation.", null, true, false, false)
    errorCodeCache.put(_AE15.code,_AE15)

    var _AE16 = new ErrorMessageProp("AE16", "The Database has expired.", null, true, false, false)
    errorCodeCache.put(_AE16.code,_AE16)

    var _AE17 = new ErrorMessageProp("AE17", "Address does not have Suites or Apartments.", null, true, false, false)
    errorCodeCache.put(_AE17.code,_AE17)

    var _AE18 = new ErrorMessageProp("AE18","Extra information found in address.",null, true, false, false)
    errorCodeCache.put(_AE18.code,_AE18)

    var _AE19 = new ErrorMessageProp("AE19","FindSuggestion function has exceeded time limit.", null, true, false, false)
    errorCodeCache.put(_AE19.code,_AE19)

    var _AE20 = new ErrorMessageProp("AE20", "FindSuggestion function is disabled, see manual for details.", null, true, false, false)
    errorCodeCache.put(_AE20.code,_AE20)

    var _AC04 = new ErrorMessageProp("AC04", "The USPS changed the address.", null, true, false, false)
    errorCodeCache.put(_AC04.code,_AC04)

    var _AS02 = new ErrorMessageProp("AS02", "The default building address was verified but the suite or apartment number is missing or invalid.", null, true, false, false)
    errorCodeCache.put(_AS02.code,_AS02)

    var _AC05 = new ErrorMessageProp("AC05", "The USPS changed the street name.", null, true, false, false)
    errorCodeCache.put(_AC05.code,_AC05)

    var _AS01 = new ErrorMessageProp("AS01", "Address is not complete. Please select the correct address below or override.",
        "Address is already Standardized.", false, true, false)
    errorCodeCache.put(_AS01.code, _AS01)
	
	  var _AS03 = new ErrorMessageProp("AS03", "NON-USPS Address	This address is served by FedEx, UPS and NOT the USPS. Override the Address to Proceed", null, true, false, false)
    errorCodeCache.put(_AS03.code,_AS03)

  }

  /**
   * Getter Methods for private variables
   * will be called by pcf action properties
   */
  function getOverrideVisible():boolean{
      return overrideVisible
  }

  function getUpdateVisible():boolean{
    return updateVisible
  }

  function getStandardizeVisible():boolean{
    return standardizeVisible
  }

  function getAddressList():ArrayList<Address>{
    return addressList
  }

  /**
   * US734
   * Shane Sheridan 04/13/2015
   *
   * Convert PolicyLocation to an Address before invoking address verification.
   */
  function validatePolicyLocation(theLocation : PolicyLocation) : ArrayList<Address>{
    var address = new Address()
    address.AddressLine1 = theLocation.AddressLine1
    address.AddressLine2 = theLocation.AddressLine2
    address.City = theLocation.City
    address.State = theLocation.State
    address.PostalCode = theLocation.PostalCode

    var result = validateAddress(address, true)
    return result
  }

  /**
   * US734
   * Shane Sheridan 03/20/2015
   * US553
   * Praneeth
  */
  function validateAddress(enteredAddress : Address, isPopupPcf : boolean) : ArrayList<Address> {
    exceptions = ""
    _logger.debug("AddressVerificationHelper#validateAddress() - Entering Address Verification logic.")

    // Check if required fields are empty
    if( (enteredAddress.AddressLine1 == null) or
        (enteredAddress.City == null) or
        (enteredAddress.State == null) or
        (enteredAddress.PostalCode == null) ){

      exceptions += DisplayKey.get("Web.AccountFile.Locations.MissingField") +"\n"
      return addressList
    }

    var isAlreadyStandardized = (enteredAddress.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_STANDARDIZED)
    if(isAlreadyStandardized){
      if(isPopupPcf){
        standardizeVisible = false
        overrideVisible = false
        updateVisible = true
      }
      exceptions += DisplayKey.get("TDIC.MelissaData.PreviouslyStandardized")
      return addressList
    }

    var result = standardizeAddressAndProcessErrors(enteredAddress, isPopupPcf)

    _logger.debug("AddressVerificationHelper#validateAddress() - exiting Address Verification logic.")
    return result
  }

  /**
   * US734
   * Shane Sheridan 03/24/2015
   *
   * Standardize the User entered address by sending it to the Melissa Data Handler.
  */
  private function standardizeAddressAndProcessErrors(enteredAddress : Address, isPopupPcf : boolean) : ArrayList<Address> {
    if(not isPopupPcf){
      updateVisible = true
      updateStatus(enteredAddress)
    }
    else{
      overrideVisible = true
      updateVisible = false
    }

    addressList = new ArrayList<Address>()
    _logger.debug("AddressVerificationHelper#standardizeAddressAndProcessErrors() - starting the address verification service.")

    var isResponseValid = standardizeAddress(enteredAddress)
    if(isResponseValid){
      processErrorCodesReturnedFromService(enteredAddress, isPopupPcf)
    }
    else{
      handleInvalidResponseFromMelissa(isPopupPcf)
    }
    _logger.debug("AddressVerificationHelper#standardizeAddressAndProcessErrors() - finished with the address verification service.")
    return addressList
  }

  /**
   * US734
   * Shane Sheridan 04/15/2015
   *
   * For PCF popups, the UI ensures that the user standardizes or overrides a new address;
   * but for Contact addresses, the UI cannot ensure this so the invalid status is used.
   */
  private function updateStatus(enteredAddress : Address){
    var isAddressNew = (enteredAddress.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_NEW)
    if(isAddressNew){
      enteredAddress.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_INVALID
    }
  }

  /**
   * US734
   * Shane Sheridan 04/10/2015
   */
  private function standardizeAddress(address : Address) : boolean{
    _dataHandler = new MelissaDataHandler()
    _standardizedAddress = _dataHandler.standardizeAddress(address)
    _errorCodes = _dataHandler.getErrorCodes()

    var isInvalidResponseFromMelissa = (_errorCodes == "")
    var isConnectionTimedOut = (_errorCodes == "NIC")
    if(isInvalidResponseFromMelissa or isConnectionTimedOut){
      return false
    }

    _errorCodeArray = _errorCodes.split(",")
    return true
  }

  /**
   * US734
   * Shane Sheridan 04/10/2015
   *
   * Process return codes from address verification service.
   */
  private function processErrorCodesReturnedFromService(enteredAddress : Address, isPopupPcf : boolean) :ArrayList<Address>{
    for (error in _errorCodeArray) {
      if(errorCodeCache.containsKey(error)){
        if(errorCodeCache.get(error).isStandardized){
          enteredAddress.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_STANDARDIZED
          processCodesForStandardizedAddress(enteredAddress, isPopupPcf, error)
          standardizeVisible = false
        }
        else if(errorCodeCache.get(error).doStreetSearch){
          _logger.debug("AddressVerificationHelper#processErrorsInAddress() - Address is not standardized but can do street search - get display message.")
          enteredAddress.AddressLine1 = _standardizedAddress.AddressLine1
          enteredAddress.City = _standardizedAddress.City
          enteredAddress.State = _standardizedAddress.State
          enteredAddress.PostalCode = _standardizedAddress.PostalCode
          exceptions = errorCodeCache.get(error).displayMessage
          addressList = _dataHandler.doStreetSearch(enteredAddress)
          return addressList
        }
        else{
          _logger.debug("AddressVerificationHelper#processErrorsInAddress() - Address is not standardized and cannot do street search - get display message.")
          exceptions += errorCodeCache.get(error).displayMessage
        }
      }
    }

    return addressList
  }

  /**
   * US734
   * Shane Sheridan 04/15/2015
   */
  private function processCodesForStandardizedAddress(enteredAddress:Address, isPopupPcf:boolean, error:String){
    if(_errorCodeArray.length == 1){
      processCodeCompletelyStandardized(enteredAddress, isPopupPcf, error)
    }
    else{
      processCodeAlmostStandardized(enteredAddress, isPopupPcf, error)
    }
  }

  /**
   * US734
   * Shane Sheridan 04/15/2015
   * US553 - Praneethk
   * DE241 - Praneethk - Address selections not displaying at bottom of the screen
   */
  private function processCodeCompletelyStandardized(enteredAddress:Address, isPopupPcf:boolean, error:String){
    _logger.debug("AddressVerificationHelper#processCodeCompletelyStandardized() - Address is completely standardized - get alternate message.")

    standardizeVisible = false
    if(isPopupPcf){
      overrideVisible = false
      updateVisible = true
    }

    if(!(enteredAddress.AddressLine1.equals(_standardizedAddress.AddressLine1)&&
        enteredAddress.City.equals(_standardizedAddress.City)&&
        enteredAddress.State.equals(_standardizedAddress.State)&&
        enteredAddress.PostalCode.equals(_standardizedAddress.PostalCode))){

        addressList.add(_standardizedAddress)
        exceptions = DisplayKey.get("TDIC.MelissaData.StandardSuggestion")
    }
    else if((enteredAddress.AddressLine2!=null && !enteredAddress.AddressLine2.equals(_standardizedAddress.AddressLine2)))
    {
      addressList.add(_standardizedAddress)
      exceptions = DisplayKey.get("TDIC.MelissaData.StandardSuggestion")
    }
    else{
       exceptions = errorCodeCache.get(error).alternateMessage
    }
  }

  /**
   * US734
   * Shane Sheridan 04/15/2015
   */
  private function processCodeAlmostStandardized(enteredAddress:Address, isPopupPcf:boolean, error:String){
    _logger.debug("AddressVerificationHelper#processCodeAlmostStandardized() -  Address is almost standardized - get display message.")

    if(isPopupPcf){
      standardizeVisible = false
    }
    addressList.add(_standardizedAddress)
    exceptions = errorCodeCache.get(error).displayMessage
  }

  /**
   * US734
   * Shane Sheridan 04/15/2015
   */
  private function handleInvalidResponseFromMelissa(isPopupPcf : boolean){
    _logger.debug("AddressVerificationHelper#handleInvalidResponseFromMelissa() -  Handling invalid response from address verification service.")
    exceptions = DisplayKey.get("TDIC.Contact.AddressStandardization.InvalidResponseFromAddressVerificationSystem")

    if(isPopupPcf){
      overrideVisible = true
      updateVisible = false
    }
    else{
      updateVisible = true
    }
    standardizeVisible = false
  }

  /**
   * Display the error messages to the User (if any); these are technically validation errors, not exceptions.
  */
  public function displayExceptions(){
    throw new DisplayableException(exceptions)
  }

  /**
   * US734
   * Shane Sheridan 03/23/2015
  */
  public function replaceAddressWithSuggested(returnedAddress : Address, savedAddress : Address, isPopupPcf : boolean){
    if(isPopupPcf){
      updateButtonsOnReplaceAddress()
    }
    savedAddress.AddressLine1 = returnedAddress.AddressLine1
    savedAddress.AddressLine2 = returnedAddress.AddressLine2
    savedAddress.AddressLine3 = returnedAddress.AddressLine3
    savedAddress.City = returnedAddress.City
    savedAddress.State = returnedAddress.State
    savedAddress.PostalCode = returnedAddress.PostalCode

    addressList = null
  }

  /**
   * US734
   * Shane Sheridan 03/23/2015
   */
  public function replaceLocationWithSuggestedAddress(returnedAddress : Address, theLocation : PolicyLocation){
    updateButtonsOnReplaceAddress()

    theLocation.AddressLine1 = returnedAddress.AddressLine1
    theLocation.AddressLine2 = returnedAddress.AddressLine2
    theLocation.AddressLine3 = returnedAddress.AddressLine3
    theLocation.City = returnedAddress.City
    theLocation.State = returnedAddress.State
    theLocation.PostalCode = returnedAddress.PostalCode

    addressList = null
  }

  /**
   * US734
   * Shane Sheridan 04/16/2015
  */
  private function updateButtonsOnReplaceAddress(){
    updateVisible = true
    overrideVisible = false
  }

  /**
   * US734
   * Shane Sheridan 04/09/2015
   *
   * This function is invoked at commit time.
  */
  function updateAddressStatus(address : Address){
    var isAddressStandardized = (address.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_STANDARDIZED)
    if(not isAddressStandardized){
      address.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_OVERRIDDEN
    }
  }

  /**
   * US734
   * Shane Sheridan 04/10/2015
   *
   * This function is invoked at commit time.
   */
  function validateAndUpdateStatusOfAddresses(contact : Contact){
    for(anAddress in contact.AllAddresses){
      checkAddressStatus(anAddress)
      updateAddressStatus(anAddress)
    }
  }
  //GWPS-2624 validate and update address should not be called if the PolicyChangeReason is ProfileChange
  function validateAndUpdateStatusOfAddressIfNotProfileChange(policyContactRole : PolicyContactRole){
    if(!(policyContactRole.Branch.Job.ChangeReasons.hasMatch(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE))){
      validateAndUpdateStatusOfAddresses(policyContactRole.AccountContactRole.AccountContact.Contact)
    }
  }

  /**
   * US734
   * Shane Sheridan 04/10/2015
  */
  private function checkAddressStatus(address : Address){
    var isAddressNew = (address.StandardizationStatus_TDIC == typekey.AddressStatusType_TDIC.TC_NEW)
    if(isAddressNew){
      // notify the user that new address cannot be committed.
      throw new DisplayableException(DisplayKey.get("TDIC.Contact.AddressStandardization.NewAddressExists"))
    }
  }

  function isAdressStandsbuttonVisible(currentAdrress :Address) : boolean {
    //If the address is not empty and the newly address Address is same as StandardizedAddress then don't show the "standardized" button
    if((currentAdrress.AddressLine1 != null and currentAdrress.City != null and currentAdrress.State != null and currentAdrress.PostalCode != null)
        and !(currentAdrress.AddressLine1 == _standardizedAddress.AddressLine1 and currentAdrress.City == _standardizedAddress.City and
        currentAdrress.State == _standardizedAddress.State and currentAdrress.PostalCode == _standardizedAddress.PostalCode)){
      return Boolean.TRUE
    }else {
      if( _errorCodeArray != null and _errorCodeArray.hasMatch(\elt -> errorCodeCache.containsKey(elt) and errorCodeCache.get(elt).isStandardized)){
        updateVisible = true
        return Boolean.FALSE
      }
      return Boolean.TRUE
    }
  }
}