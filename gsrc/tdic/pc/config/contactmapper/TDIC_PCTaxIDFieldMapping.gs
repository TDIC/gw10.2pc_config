
package tdic.pc.config.contactmapper

uses gw.webservice.contactapi.mapping.FieldMappingImpl
uses gw.webservice.contactapi.abcontactapihelpers.core.XmlPopulator
uses gw.webservice.contactapi.abcontactapihelpers.core.BeanPopulator
uses gw.webservice.contactapi.beanmodel.XmlBackedInstance
uses gw.webservice.contactapi.beanmodel.anonymous.elements.XmlBackedInstance_Field
uses gw.lang.reflect.IType
uses gw.internal.xml.xsd.typeprovider.XmlSchemaTypeToGosuTypeMappings

  /**
   * Class for handling mapping of Contacts TaxID and Social Security Number while setting and getting information
   * from the XML payload
   *
   * If the Contact is a Company the TaxID field is used
   * If the Contact is a Person the Social Security Number is used
   */
public class TDIC_PCTaxIDFieldMapping extends FieldMappingImpl<Contact> {

  protected static final var LINK_ID            : String = "LinkID"
  protected static final var EXTERNAL_PUBLIC_ID : String = "External_PublicID"

  construct() {
    super(Contact#TaxID, BOTH, Contact#TaxID.PropertyInfo.Name)
  }

  override function populateXml(xp : XmlPopulator<Contact>) {
    // Amended TaxID -- SSN/FEIN filed mapping and amended PersonFEIN_TDIC usage as part of GW-1623 & GW-753
    var theContact = xp.Bean
    var abContactXML = xp.XmlBackedInstance
    var colType = OfficialID.Type.TypeInfo.getProperty("OfficialIDValue").FeatureType
    var taxIDOriginalValue = theContact.getOriginalValue("TaxID")
    var taxIDCurrentValue = theContact.getFieldValue("TaxID")
    populateFieldXMLWithValue(abContactXML, "TaxID", taxIDCurrentValue, taxIDOriginalValue, colType, theContact.New)
  }



  override function populateBean(bp : BeanPopulator<Contact>) {
    var theContact = bp.Bean
    var abContactXML = bp.XmlBackedInstance
    // Amended TaxID -- SSN/FEIN filed mapping and amended PersonFEIN_TDIC usage as part of GW-1958
    theContact.TaxID = abContactXML.fieldValue("TaxID")
  }


  private function populateFieldXMLWithValue(instanceXML : XmlBackedInstance,
  ab_fieldName    : String,
  value           : Object,
  originalValue   : Object,
  columnType      : IType,
  beanIsNew       : boolean) {
    var fieldXML = new XmlBackedInstance_Field()
    instanceXML.Field.add(fieldXML)
    fieldXML.Name = ab_fieldName
    var pair = XmlSchemaTypeToGosuTypeMappings.gosuToSchema(columnType)
    fieldXML.Type = pair.First
    fieldXML.setAttributeSimpleValue(XmlBackedInstance_Field.$ATTRIBUTE_QNAME_Value,
    pair.Second.gosuValueToStorageValue(value))

    if (beanIsNew or ab_fieldName == LINK_ID or ab_fieldName == EXTERNAL_PUBLIC_ID) return

    // Krishna - GW-632 - Changed mapping
    fieldXML.setAttributeSimpleValue(XmlBackedInstance_Field.$ATTRIBUTE_QNAME_OrigValue,
    pair.Second.gosuValueToStorageValue(originalValue))
  }

}
