package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffRemove
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OCC2080AS_TDIC extends AbstractMultipleCopiesForm<GLLocumTenensSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLLocumTenensSched_TDIC> {
    var period = context.Period
    var glLocumTenensSchedItems = period.GLLine?.GLLocumTenensSched_TDIC
    var locumTenensExclItems : ArrayList<GLLocumTenensSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLOccurence_TDIC" and period.GLLine?.GLLocumTenensCov_TDICExists and
        glLocumTenensSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var locumTenensSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLLocumTenensSched_TDIC)*.Bean
        var locumTenensSchedLineItemsRemoved = changeList?.where(\changedItem -> (changedItem typeis DiffRemove) and
            changedItem.Bean typeis GLLocumTenensSched_TDIC)*.Bean
        if(glLocumTenensSchedItems.hasMatch(\item -> item.BasedOn == null)){
          locumTenensExclItems.add(glLocumTenensSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glLocumTenensSchedItems.hasMatch(\item -> locumTenensSchedLineItemsModified.contains(item))) {
          locumTenensExclItems.add(glLocumTenensSchedItems.firstWhere(\item -> locumTenensSchedLineItemsModified.contains(item)))
        }
        else if(locumTenensSchedLineItemsRemoved.HasElements){
          locumTenensExclItems.add(glLocumTenensSchedItems.first())
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          locumTenensExclItems.add(glLocumTenensSchedItems.first())
        }
      }
      else {
        locumTenensExclItems.add(glLocumTenensSchedItems.first())
      }
      return locumTenensExclItems.HasElements? locumTenensExclItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLLocumTenensSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    contentNode.addChild(createTextNode("DentistName", _entity.DentistName))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}