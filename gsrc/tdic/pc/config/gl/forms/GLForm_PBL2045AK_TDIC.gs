package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_PBL2045AK_TDIC  extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if(context.Period.GLLine?.GLIDTheftREcoveryCov_TDICExists && (context.Period.GLLine?.GLIDTheftREcoveryCov_TDIC?.GLTRTypeTDICTerm?.Value
          == IdentityTheftRecType_TDIC.TC_FAMILY or context.Period.GLLine?.GLIDTheftREcoveryCov_TDIC?.GLTRTypeTDICTerm?.Value == IdentityTheftRecType_TDIC.TC_INDIVIDUAL))
        return true
      }
      return false
  }


}