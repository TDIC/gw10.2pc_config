package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OCC1111AS_TDIC extends AbstractSimpleAvailabilityForm {
  var period:PolicyPeriod

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    period = context.Period
    if (context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      return (period.Job.Subtype == typekey.Job.TC_CANCELLATION || period.Job.Subtype == typekey.Job.TC_POLICYCHANGE )? period.RefundCalcMethod != CalculationMethod.TC_FLAT: true
    }
    return false
  }
  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (period?.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
     // contentNode.addChild(createTextNode("RateAsOfDate", period.RateAsOfDate.trimToMidnight().toString()))
          contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }

    if(period?.Job?.isValidPolicyChangeReasons_TDIC(period)){
      contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }
  }
}