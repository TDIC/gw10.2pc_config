package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2110AS_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        return (context.Period.GLLine?.GLExtendedReportPeriodCov_TDICExists and
            (context?.Period?.Job?.ChangeReasons?.hasMatch(\changeReason -> changeReason?.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN)))
      or (context.Period.GLLine?.GLExtendedReportPeriodCov_TDICExists and (!context.Period.GLLine?.GLExtendedReportPeriodCov_TDIC.GLERPRated_TDICTerm.Value)
            and  (context?.Period?.Job?.ChangeReasons?.hasMatch(\CR -> CR.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM)))
    }
    return false
  }
}