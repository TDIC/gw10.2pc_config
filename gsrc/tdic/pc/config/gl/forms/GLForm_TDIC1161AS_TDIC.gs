package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 04/09/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_TDIC1161AS_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var period = context.Period
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC"){
      var priorTermExists = (context.Period.TermNumber - 1) > 0
      if(context.Period.Job typeis Cancellation){
        return (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC")?
            (period.Cancellation.CancelReasonCode == ReasonCode.TC_NOTTAKEN and !priorTermExists):
            period.Cancellation.CancelReasonCode == ReasonCode.TC_NOTTAKEN
      }
    }
    return false
  }
}