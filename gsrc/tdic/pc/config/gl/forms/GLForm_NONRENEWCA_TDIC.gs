package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm


/**
 * Description :
 * Create User: ChitraK
 * Create Date: 4/8/2020
 * Create Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */

class GLForm_NONRENEWCA_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or context.Period.Offering.CodeIdentifier == "PLCyberLiab_TDIC"
        or context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" or context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC"){
      if(context.Period.PrimaryNamedInsured?.ContactDenorm?.Person.CDAMembershipStatus_TDIC == GlobalStatus_TDIC.TC_INACTIVE && context.Period.isRenewalFormInferred_TDIC == false){
        context.Period.isRenewalFormInferred_TDIC = true
        return true
      }
    }
    return false
  }
}