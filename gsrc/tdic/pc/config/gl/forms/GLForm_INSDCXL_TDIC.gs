package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: HarishP
 * Date: 10/09/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_INSDCXL_TDIC extends AbstractSimpleAvailabilityForm {

  //Infer form if Cancellation Reason other than 'NotTaken' and 'NonPayment'
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.BOPLineExists or context.Period.GLLineExists) {
      var job = context.Period.Job
      if (job typeis Cancellation ) {
        return (job.CancelReasonCode != ReasonCode.TC_NONPAYMENT and job.CancelReasonCode != ReasonCode.TC_NOTTAKEN) ? true : false
      }
    }
    return false
  }

}