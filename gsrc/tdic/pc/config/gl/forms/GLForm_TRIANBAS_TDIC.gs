package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses typekey.Job

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 01/25/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_TRIANBAS_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var period = context.Period
    //GWNW-1346/1347 Infer form for submission
    if ((period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" or period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC")) {
      if(period.Job.Subtype==Job.TC_SUBMISSION){
        return true
      }
      else if((period.Job.Subtype== Job.TC_RENEWAL)  && (period.isDIMSPolicy_TDIC)){
        return true
      }
    }
    else if((period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or period.Offering.CodeIdentifier =="PLOccurence_TDIC")) {
      if(period.Job.Subtype==Job.TC_SUBMISSION){
        return true
      }
      else if((period.Job.Subtype== Job.TC_RENEWAL) && (period.isDIMSPolicy_TDIC)){
        return true
      }
    }
    return false
  }
}