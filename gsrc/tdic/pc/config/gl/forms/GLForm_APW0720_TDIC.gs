package tdic.pc.config.gl.forms
/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_APW0720_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {

    if ((context.Period.Job typeis Renewal || context.Period.Job typeis Submission )  && !(context.Period?.isDIMSPolicy_TDIC) &&
        !(context.Period?.SelectedPaymentPlan?.Name?.equalsIgnoreCase("Monthly APW (12 Pay)"))) {
      return true
    }
    return false
  }
}