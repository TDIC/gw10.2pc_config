package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 01/25/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PLVLNTR_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      if (context.Period.GLLine?.GLExposuresWM?.hasMatch(\exposure -> exposure?.ClassCode_TDIC == ClassCode_TDIC.TC_01)) {
        return true
      }
    }
    return false
  }
}