package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2521IL_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.Job typeis Submission) {
        return true
      }
      else {
        return context.Period.GLLine?.GLAggLimit_TDICExists and
            context.Period.GLLine?.GLAggLimit_TDIC?.GLAggLimitAB_TDICTerm?.Value !=
                context.Period.GLLine?.BasedOn?.GLAggLimit_TDIC?.GLAggLimitAB_TDICTerm?.Value
      }
    }
    return false
  }
}