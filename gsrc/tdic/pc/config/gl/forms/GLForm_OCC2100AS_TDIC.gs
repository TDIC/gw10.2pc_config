package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffRemove
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OCC2100AS_TDIC extends AbstractMultipleCopiesForm<GLNameODSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLNameODSched_TDIC> {
    var period = context.Period
    var glNameODSchedItems = period.GLLine?.GLNameODSched_TDIC
    var nameODExclItems : ArrayList<GLNameODSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLOccurence_TDIC" and period.GLLine?.GLNameOTDCov_TDICExists and
        glNameODSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var nameODSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLNameODSched_TDIC)*.Bean
        var nameODSchedLineItemRemoved = changeList?.where(\changedItem -> (changedItem typeis DiffRemove) and
            changedItem.Bean typeis GLNameODSched_TDIC)*.Bean
        if(glNameODSchedItems.hasMatch(\item -> item.BasedOn == null)){
          nameODExclItems.add(glNameODSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glNameODSchedItems.hasMatch(\item -> nameODSchedLineItemsModified.contains(item))) {
          nameODExclItems.add(glNameODSchedItems.firstWhere(\item -> nameODSchedLineItemsModified.contains(item)))
        }
        else if(nameODSchedLineItemRemoved.HasElements){
          nameODExclItems.add(glNameODSchedItems.first())
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          nameODExclItems.add(glNameODSchedItems.first())
        }
      }
      else {
        nameODExclItems.add(glNameODSchedItems.first())
      }
      return nameODExclItems.HasElements? nameODExclItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLNameODSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    contentNode.addChild(createTextNode("DoctorName", _entity.DoctorName as String))

  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}