package tdic.pc.config.gl.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffRemove
uses gw.forms.generic.AbstractMultipleCopiesForm
uses entity.FormAssociation
uses gw.forms.FormInferenceContext
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */

class GLForm_PBL2027BAS_TDIC extends AbstractMultipleCopiesForm<GLSchoolServSched_TDIC> {
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLSchoolServSched_TDIC> {
    var period = context.Period
    var AddInsd : ArrayList<GLSchoolServSched_TDIC> = {}
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var GLAddInsureds = context.Period.GLLine?.GLSchoolServSched_TDIC
      if(period.GLLine?.GLSchoolServicesCov_TDICExists and GLAddInsureds.HasElements){
        var GLAddInsuredAddedOnPolicyChange = GLAddInsureds.where(\addInsured -> addInsured.BasedOn == null)
        if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
          var changeList = period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
          var addlInsuredModifiedOnPolicyChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis GLSchoolServSched_TDIC)*.Bean
          var addlInsuredItemRemoved = changeList?.where(\changedItem -> (changedItem typeis DiffRemove) and
              changedItem.Bean typeis GLSchoolServSched_TDIC)*.Bean
          if(GLAddInsuredAddedOnPolicyChange.HasElements){
            AddInsd.add(GLAddInsuredAddedOnPolicyChange.first())
          }
          else if(GLAddInsureds.hasMatch(\ai -> addlInsuredModifiedOnPolicyChange.contains(ai))){
            AddInsd.add(GLAddInsureds?.firstWhere(\ai -> addlInsuredModifiedOnPolicyChange.contains(ai)))
          }
          else if(addlInsuredItemRemoved.HasElements){
            AddInsd.add(GLAddInsureds.first())
          }
        }
        else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
          if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
            AddInsd.add(GLAddInsureds.first())
          }
        }
        else {
          AddInsd.add(GLAddInsureds.first())
        }
      }
      return AddInsd.HasElements? AddInsd.toList() : {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLSchoolServSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("EventName", _entity.EventName))
    contentNode.addChild(createTextNode("EventEndDate", _entity.EventEndDate as String))
    contentNode.addChild(createTextNode("CanrecComp", _entity.CanrecComp as String))
    contentNode.addChild(createTextNode("Address", _entity.Address as String))
    if (_entity.AddlInsured.PolicyAddlInsured.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.AddlInsured.PolicyAddlInsured.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.AddlInsured.PolicyAddlInsured.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.AddlInsured.PolicyAddlInsured.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.AddlInsured.PolicyAddlInsured.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.AddlInsured.PolicyAddlInsured.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.AddlInsured.PolicyAddlInsured.Suffix as String))
    }
    if (_entity.AddlInsured.PolicyAddlInsured.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.AddlInsured.PolicyAddlInsured.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.AddlInsured.PolicyAddlInsured.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}