package tdic.pc.config.gl.historicaldata

uses entity.GLHistoricalParameters_TDIC
uses org.joda.time.DateTime
uses org.joda.time.Years
uses tdic.pc.integ.plugins.retrorating.RetroRating_TDIC


/**
 * Britto Sebastian - 11/20/2019
 * Populate GL Historical information
 */
class GLHistoricalParamsBuilder {
  private var _period : PolicyPeriod
  private var _historyCache : List<GLHistoricalParameters_TDIC>as HistoryCache = {}
  private var retroActiveThreshold = GLHistoryHelper.RetroActiveThresholdYears
  private var GL_DISCOUNTS_CATEGORY_TDIC = "GLDiscountsCat_TDIC"
  private var discountMap = {
      "GLNewDentistDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_NEWGRADUATE},
      "GLNewGradDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_NEWGRADUATE},
      "GLFullTimeFacultyDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_FULLTIMEFACULTYMEMBER},
      "GLFullTimePostGradDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_FULLTIMEPOSTGRADUATE},
      "GLTempDisDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_TEMPORARYDISABILITY},
      "GLPartTimeDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_PARTTIME_16, GLHistoricalDiscount_TDIC.TC_PARTTIME_20},
      "GLCovExtDiscount_TDIC" -> {GLHistoricalDiscount_TDIC.TC_COVERAGEEXTENSION}
  }

  private construct() {/*always initialize with a policy period*/}

  construct(period : PolicyPeriod) {
    _period = period
  }

  /**
   * Main method for creating past and current historical parameters
   *
   * @return Newly created parameters
   */
  public function buildHistory() : GLHistoricalParameters_TDIC[] {
    if (isPriorActsCoverageExists(_period) and isExposureValid(getExposure(_period))) { //check if PriorActs exists and exposure is valid

      if (_period.GLHistoricalParameters_TDIC.HasElements and _period.Job typeis PolicyChange) {
        return GLHistoryHelper.getHistoryFor(_period)
      }

      clearHistory()
      if (not((_period.Job typeis Submission) or (_period.isLegacyConversion))) {
        updateCache(copyHistoryFromPreviousPeriod())      //clone hostories from BasedOn branch
      }

      if (_period.Job typeis Submission) {
        updateCache(buildSubmissionHistory(_period))      //creating new submission histories
      }else if (_period.Job typeis Renewal) {
        //Legacy history
        if (_period.isLegacyConversion) {

          if(_period.isDIMSPolicy_TDIC){
            updateCache(buildSubmissionHistory(_period))
          }
          else {
            updateLegacyHistory()
          }
          // Legacy histories

        } else {
          //commneting out, Retro Active date cannot change after New Business
          //updateCache(adjustBackDatedHistory())
        }
      }
      if (not _period.isLegacyConversion ) {
        if (HistoryCache.HasElements) {
          HistoryCache = cleanseAndMerge(HistoryCache)
        }
        updateCache(buildCurrentTermHistory())              //creating history elements for current period
        finalizeAndAddToPeriod()
     }
      if(_period.isDIMSPolicy_TDIC){
        if (HistoryCache.HasElements) {
          HistoryCache = cleanseAndMerge(HistoryCache)
        }
        updateCache(buildCurrentTermHistory())              //creating history elements for current period
        finalizeAndAddToPeriod()
      }
      return GLHistoryHelper.getHistoryFor(_period)
    }
    return {}
  }

  /**
   *
   */
  private function buildSubmissionHistory(period : PolicyPeriod) : List<GLHistoricalParameters_TDIC> {
    return createHistoricalPrameters(period)
  }

  /**
   * Legacy Data : populate only on Legacy renewal
   */
  public function updateLegacyHistory() : GLHistoricalParameters_TDIC[] {
    new RetroRating_TDIC().populateHistoricalData(_period)
    return GLHistoryHelper.getHistoryFor(_period)
  }

  /**
   *
   */
  private function buildCurrentTermHistory() : List<GLHistoricalParameters_TDIC> {
    var historicalParams : List<GLHistoricalParameters_TDIC> = {}
    var effDate = _period.PeriodStart.trimToMidnight()
    var expDate = _period.PeriodEnd.trimToMidnight()
    var exposure = getExposure(_period)
    var discountEffDate = getDiscountEffectiveDate(_period)

    if (_period.Job typeis Submission or _period.Job typeis Renewal) { //New Submission
      if (discountEffDate != null and discountEffDate.after(effDate) and discountEffDate.before(expDate)) {
        historicalParams.add(createCurrentTermHistoricalParam(discountEffDate, expDate, exposure))
        expDate = discountEffDate
      }
      historicalParams.add(createCurrentTermHistoricalParam(effDate, expDate, exposure))
    } else if (_period.Job typeis PolicyChange) {  //Policy Change
      var editEffDate = _period.EditEffectiveDate.trimToMidnight()
      var basedOnExposure = _period.BasedOn.GLLine.Exposures.first()
      if (discountEffDate != null and discountEffDate != editEffDate and not discountEffDate.before(effDate)) { //dicount effective date must be within the current period's date range
        if (discountEffDate.after(editEffDate)) {
          historicalParams.add(createCurrentTermHistoricalParam(discountEffDate, expDate, exposure))
          historicalParams.add(createCurrentTermHistoricalParam(editEffDate, discountEffDate, exposure))
          historicalParams.add(createCurrentTermHistoricalParam(effDate, editEffDate, basedOnExposure))
          historicalParams = merge(historicalParams)
        } else if (discountEffDate.after(effDate) and discountEffDate.before(editEffDate)) {
          historicalParams.add(createCurrentTermHistoricalParam(editEffDate, expDate, exposure))
          historicalParams.add(createCurrentTermHistoricalParam(discountEffDate, editEffDate, basedOnExposure))
          historicalParams.add(createCurrentTermHistoricalParam(effDate, discountEffDate, basedOnExposure))
          historicalParams = merge(historicalParams)
        }
      } else {  //normal policy change segments
        historicalParams.add(createCurrentTermHistoricalParam(editEffDate, expDate, exposure))
        historicalParams.add(createCurrentTermHistoricalParam(effDate, editEffDate, basedOnExposure))
        historicalParams = merge(historicalParams)
      }
    }

    if (historicalParams != null and historicalParams.Count > 0) {
      historicalParams = merge(historicalParams)
    }
    return historicalParams
  }

  /**
   *
   */
  private function copyHistoryFromPreviousPeriod() : List<GLHistoricalParameters_TDIC> {
    var historicalParams : List<GLHistoricalParameters_TDIC> = {}
    var lowestRetroActiveDate = _period.PeriodStart.addYears(-retroActiveThreshold)
    if (_period.BasedOn.GLHistoricalParameters_TDIC != null and _period.BasedOn.GLHistoricalParameters_TDIC.HasElements) {
      var hps = _period.BasedOn.GLHistoricalParameters_TDIC.where(\elt -> elt.EffectiveDate.afterOrEqual(lowestRetroActiveDate))
      for (hp in _period.BasedOn.GLHistoricalParameters_TDIC) {
        historicalParams.add(hp.shallowCopy() as GLHistoricalParameters_TDIC)
      }
    }
    return historicalParams
  }

  /**
   *
   */
/*  private function adjustBackDatedHistory() : List<GLHistoricalParameters_TDIC> {
    var historyParams : List<GLHistoricalParameters_TDIC> = {}
    var currRetroDate = getRetroActiveDate(_period)
    var lowestRetroActiveDate = _period.PeriodStart.addYears(-retroActiveThreshold)
    var retroDateFinal = (currRetroDate.before(lowestRetroActiveDate)) ? lowestRetroActiveDate : currRetroDate

    var periods = _period.Policy.BoundPeriods.toList()
    if (periods != null and periods.Count > 0) {
      periods = periods.where(\elt -> elt.PeriodEnd.after(retroDateFinal))
      periods.add(_period)
    }
    for (period in periods.orderBy(\p -> p.EditEffectiveDate)) {
      var hps = buildBackDatedHistory(period)
      if (hps != null and hps.Count > 0) {
        historyParams.addAll(hps)
      }
    }
    //filter and remove parameters if it's in Legacy range
    if(historyParams.HasElements){
      historyParams = historyParams.where(\hp -> not HistoryCache.hasMatch(\cache -> cache.IsLegacy
          and (hp.EffectiveDate.beforeOrEqual(cache.EffectiveDate) or hp.ExpirationDate.beforeOrEqual(cache.ExpirationDate))))
    }
    historyParams = cleanseAndMerge(historyParams)

    return historyParams
  }*/

/*  private function buildBackDatedHistory(period : PolicyPeriod) : List<GLHistoricalParameters_TDIC> {
    return createHistoricalPrameters(period)
  }*/

  /**
   * @param period
   * @return
   */
  private function createHistoricalPrameters(period : PolicyPeriod) : List<GLHistoricalParameters_TDIC> {
    var historicalParams : List<GLHistoricalParameters_TDIC> = {}
    var retroActiveDate = getRetroActiveDate(period)
    var totalYearsOfHistory = getTotalYearsOfHistory(period, retroActiveDate)
    var exposure = getExposure(period)
    var midTermFirstYearCompleted = isAnnualTerm(period)
    var effDate = period.PeriodStart.trimToMidnight()
    var expDate = period.PeriodEnd.trimToMidnight()

    for (i in 1..totalYearsOfHistory) {
      if (retroActiveDate.after(effDate)) {
        break
      }
      expDate = expDate.addYears((-1))
      if (midTermFirstYearCompleted and effDate != expDate) {
        expDate = effDate
      }
      effDate = effDate.addYears((-1))

      if (not isAnnualTerm(period) and not midTermFirstYearCompleted) {  //apply polcy midterm vs full term effdates
        effDate = expDate
        expDate = period.PeriodStart.trimToMidnight()
        midTermFirstYearCompleted = true
      }
      //reset effdate for the final item
      if (i == totalYearsOfHistory) {
        effDate = retroActiveDate.after(effDate) ? retroActiveDate : effDate
      }

      //apply discounts, if applicable
      var discountEffDate = getDiscountEffectiveDate(period)
      if (discountEffDate != null and discountEffDate.after(effDate) and discountEffDate.before(expDate)) {
        historicalParams.add(createHistoricalParam(discountEffDate, expDate, exposure))
        expDate = discountEffDate
      }
      if (effDate.before(expDate)) {
        historicalParams.add(createHistoricalParam(effDate, expDate, exposure))
      }
      //adding remaining period from retro active date
      if(not isAnnualTerm(period) and midTermFirstYearCompleted and (i == totalYearsOfHistory)) {
        if(retroActiveDate.before(effDate)) {
          historicalParams.add(createHistoricalParam(retroActiveDate, effDate, exposure))
        }
      }
    }
    return historicalParams
  }

  /**
   * @param effDate
   * @param expDate
   * @param exposure
   * @param year
   */
  private function createHistoricalParam(effDate : Date, expDate : Date, exposure : GLExposure) : GLHistoricalParameters_TDIC {
    var hp = new GLHistoricalParameters_TDIC(_period)
    hp.EffectiveDate = effDate
    hp.ExpirationDate = expDate
    hp.Component = exposure.GLTerritory_TDIC.Component
    hp.TerritoryCode = exposure.GLTerritory_TDIC?.TerritoryCode
    hp.ClassCode = exposure.ClassCode_TDIC.Code
    hp.SpecialityCode = exposure.SpecialityCode_TDIC.Code
    var discountEffDate = getDiscountEffectiveDate(exposure.Branch)
    if (discountEffDate == null or (discountEffDate != null and discountEffDate.beforeOrEqual(effDate))) {
      hp.Discount = getDiscount(exposure.Branch)
    }
    hp.JobNumber = _period.Job.JobNumber
    hp.IsLegacy = false
    return hp
  }

  private function createCurrentTermHistoricalParam(effDate : Date, expDate : Date, exposure : GLExposure) : GLHistoricalParameters_TDIC {
    var hp = createHistoricalParam(effDate, expDate, exposure)
    var discountEffDate = getDiscountEffectiveDate(exposure.Branch)
    if (discountEffDate == null or (discountEffDate != null and discountEffDate.beforeOrEqual(effDate))) {
      hp.Discount = getDiscount(exposure.Branch, true)
    }
    return hp
  }

  /**
   * delete everything, except legacy information
   */
  private function clearHistory() {
    for (param in _period.GLHistoricalParameters_TDIC) {
      _period.removeFromGLHistoricalParameters_TDIC(param)
    }
  }

  /**
   * @return
   */
  private function getExposure(branch : PolicyPeriod) : GLExposure {
    return branch.GLLine.Exposures.first()
  }

  /**
   * @return
   */
  private function getRetroActiveDate(branch : PolicyPeriod) : Date {
    return branch.GLLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm.Value.trimToMidnight()
  }

  /**
   * @param retroActiveDate
   * @return
   */
  private function getTotalYearsOfHistory(branch : PolicyPeriod, retroActiveDate : Date) : int {
    if (retroActiveDate == null or branch == null) {
      return 0
    }
    var periodStart = branch.PeriodStart.trimToMidnight()
    var noOfYearsHistory = Years.yearsBetween(new DateTime(retroActiveDate), new DateTime(periodStart)).getYears()
    if (retroActiveDate.before(periodStart.addYears(-noOfYearsHistory))) {
      noOfYearsHistory++
    }
    noOfYearsHistory = (noOfYearsHistory > retroActiveThreshold) ? retroActiveThreshold : noOfYearsHistory

    return (noOfYearsHistory > 0) ? noOfYearsHistory : 0
  }

  /**
   * @return
   */
  private function getDiscount(branch : PolicyPeriod, currentTerm : boolean = false) : GLHistoricalDiscount_TDIC {
    var selectedDiscount = getPremiumModification(branch)
    //For a New Business Policy , If Full time Post Graduate Discount is applied to the current Policy Year, it will not be applied to the Prior Segments.
    if (branch.Job typeis Submission
        and selectedDiscount typeis GLFullTimePostGradDiscount_TDIC
        and not currentTerm) {
      return null
    }
    var discount = discountMap.get(selectedDiscount.Pattern.CodeIdentifier)
    if (discount != null) {
      if (discount.Count == 1) {
        return discount.first()
      } else if (selectedDiscount typeis GLPartTimeDiscount_TDIC) {
        if ((selectedDiscount as GLPartTimeDiscount_TDIC).HasGLPTPercent_TDICTerm) {
          if ((selectedDiscount as GLPartTimeDiscount_TDIC).GLPTPercent_TDICTerm.Value == 1) {
            return GLHistoricalDiscount_TDIC.TC_PARTTIME_16
          } else {
            return GLHistoricalDiscount_TDIC.TC_PARTTIME_20
          }
        }
      }
    }
    return null
  }

  /**
   * @return
   */
  private function isPriorActsCoverageExists(branch : PolicyPeriod) : Boolean {
    return branch.GLLine.GLPriorActsCov_TDICExists
  }

  private function isAnnualTerm(branch : PolicyPeriod) : Boolean {
    var isAnnual = branch.TermType == TermType.TC_ANNUAL
    var isPeriodAnnual = branch.PeriodStart.addYears(1) == branch.PeriodEnd
    return isAnnual or isPeriodAnnual
  }

  /**
   *
   */
  private function getPremiumModification(branch : PolicyPeriod) : PolicyCondition {
    var glLine = branch.GLLine
    var discountCategory = glLine.Pattern.getCoverageCategoryByCodeIdentifier(GL_DISCOUNTS_CATEGORY_TDIC)
    var discounts = discountCategory.conditionPatternsForEntity(GeneralLiabilityLine)
    discounts = discounts.where(\elt -> discountMap.Keys.contains(elt.CodeIdentifier))
    var selectedPattern = discounts.firstWhere(\elt -> glLine.hasCondition(elt))
    return glLine.getCondition(selectedPattern)
  }

  /**
   * @return
   */
  private function getDiscountEffectiveDate(branch : PolicyPeriod) : Date {
    var selectedDiscount = getPremiumModification(branch)
    var effDate : Date = null
    if (selectedDiscount typeis GLPartTimeDiscount_TDIC) {
      effDate = (selectedDiscount as GLPartTimeDiscount_TDIC).GLPTEffDate_TDICTerm.Value
    } else if (selectedDiscount typeis GLFullTimeFacultyDiscount_TDIC) {
      effDate = (selectedDiscount as GLFullTimeFacultyDiscount_TDIC).GLFTFEffDate_TDICTerm.Value
    } else if (selectedDiscount typeis GLNewGradDiscount_TDIC) {
      var questionSet = branch.getGLUWQuestionSets_TDIC().first()
      var question = questionSet.getQuestionByCodeIdentifier("NewGradDiscEffDate_TDIC")
      if (question != null) {
        effDate = branch.GLLine.getAnswer(question).DateAnswer
      }
    } else if (selectedDiscount typeis GLNewDentistDiscount_TDIC) {
      effDate = selectedDiscount.EffectiveDate
    } else if(selectedDiscount typeis GLCovExtDiscount_TDIC) {
      effDate = selectedDiscount.EffectiveDate
    }
    return effDate != null ? effDate.trimToMidnight() : effDate
  }

  /**
   * @param historicalParams
   * @return
   */
  private function optimizeEffDates(historicalParams : List<GLHistoricalParameters_TDIC>) : List<GLHistoricalParameters_TDIC> {
    var hpMap = HistoryCache.partition(\elt -> elt.ExpirationDate)
    hpMap.eachKeyAndValue(\k, val -> {
      if (val.Count > 1) {
        var itemsProcessed : List<GLHistoricalParameters_TDIC> = {}
        for (hp in val.orderByDescending(\elt -> elt.EffectiveDate)) {
          var tmpHps = val.where(\elt -> elt != hp and not itemsProcessed.contains(elt))
          if (tmpHps.HasElements) {
            var tmpHp = tmpHps.orderByDescending(\elt -> elt.EffectiveDate).first()
            tmpHp.ExpirationDate = hp.EffectiveDate
            itemsProcessed.add(hp)
          }
        }
      }
    })
    if (historicalParams.HasElements) { //eliminate the ones with same effdate and exp dates
      historicalParams = historicalParams.where(\elt -> elt.EffectiveDate != elt.ExpirationDate)
    }
    return historicalParams
  }

  private function resetDiscounts() {
    //code here
  }

  /**
   * @param hps
   */
  private function addAllToPeriod(hps : GLHistoricalParameters_TDIC[]) {
    if (hps.HasElements) {
      for (hp in hps) {
        addToPeriod(hp)
      }
    }
  }

  /**
   * @param hp
   */
  private function addToPeriod(hp : GLHistoricalParameters_TDIC) {
    if (hp != null) {
      _period.addToGLHistoricalParameters_TDIC(hp)
    }
  }

  /**
   * finalize parameters and add to policyperiod
   */
  private function finalizeAndAddToPeriod() {
    var histories = cleanseAndMerge(HistoryCache)
    histories = optimizeEffDates(histories)
    if (histories.HasElements) {
      addAllToPeriod(histories.toTypedArray())
    }
  }

  /**
   * @param historicalParams
   * @return
   */
  private function cleanseAndMerge(historicalParams : List<GLHistoricalParameters_TDIC>) : List<GLHistoricalParameters_TDIC> {
    historicalParams = removeDuplicates(historicalParams)
    historicalParams = merge(historicalParams)

    return historicalParams
  }

  /**
   * remove duplicates
   */
  private function removeDuplicates(historicalParams : List<GLHistoricalParameters_TDIC>) : List<GLHistoricalParameters_TDIC> {
    var finalParams : List<GLHistoricalParameters_TDIC> = {}
    if (historicalParams.HasElements) {
      var tempParams = historicalParams.copy()
      for (hp in historicalParams) {
        var matches = tempParams.where(\elt -> match(elt, hp))
        if (matches.HasElements) {
          tempParams.removeAll(matches)
          finalParams.add(hp)
        }
      }
    }
    return finalParams
  }

  /**
   * merge parameters
   */
  private function merge(historicalParams : List<GLHistoricalParameters_TDIC>) : List<GLHistoricalParameters_TDIC> {
    var it = historicalParams.iterator()
    while (it.hasNext()) {
      var param = it.next()
      var existingParam = HistoryCache.firstWhere(\elt -> canMerge(elt, param))
      if (existingParam != null) {
        existingParam.ExpirationDate = param.ExpirationDate
        it.remove()
      }
    }
    return historicalParams
  }

  /**
   * @param param1
   * @param param2
   * @return
   */
  private function canMerge(param1 : GLHistoricalParameters_TDIC, param2 : GLHistoricalParameters_TDIC) : Boolean {
    if (param1.EffectiveDate == param2.EffectiveDate
        and param1.ExpirationDate != param2.ExpirationDate
        and param1.Component == param2.Component
        and param1.TerritoryCode == param2.TerritoryCode
        and param1.ClassCode == param2.ClassCode
        and param1.SpecialityCode == param2.SpecialityCode
        and param1.Discount == param2.Discount) {
      return true
    }
    return false
  }

  private function match(param1 : GLHistoricalParameters_TDIC, param2 : GLHistoricalParameters_TDIC) : Boolean {
    if (param1.EffectiveDate == param2.EffectiveDate
        and param1.ExpirationDate == param2.ExpirationDate
        and param1.Component == param2.Component
        and param1.TerritoryCode == param2.TerritoryCode
        and param1.ClassCode == param2.ClassCode
        and param1.SpecialityCode == param2.SpecialityCode
        and param1.Discount == param2.Discount) {
      return true
    }
    return false
  }

  private function updateCache(params : List<GLHistoricalParameters_TDIC>) {
    if (params.HasElements) {
      HistoryCache.addAll(params)
    }
  }

  private function isExposureValid(exposure : GLExposure) : boolean {
    if (exposure.ClassCode_TDIC == null or exposure.SpecialityCode_TDIC == null or exposure.GLTerritory_TDIC == null) {
      return false
    }
    return true
  }
}