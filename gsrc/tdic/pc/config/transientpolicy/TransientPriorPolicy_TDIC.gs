package tdic.pc.config.transientpolicy

uses java.util.Date

//20180219 TJT GW-2981 - include TDIC policies when evaluating gaps in prior coverage
class TransientPriorPolicy_TDIC {

  private var _policyNumber : String as PolicyNumber
  private var _carrier : String as Carrier
  private var _dtPolicyStart : Date as PolicyStart
  private var _dtPolicyEnd : Date as PolicyEnd

  construct(passedInPriorCarrierPolicy : entity.PriorPolicy){
    _policyNumber = passedInPriorCarrierPolicy.PolicyNumber
    _carrier = passedInPriorCarrierPolicy.Carrier
    _dtPolicyStart = passedInPriorCarrierPolicy.EffectiveDate
    _dtPolicyEnd = passedInPriorCarrierPolicy.ExpirationDate
  }

  //20180308 TJT: GW-3141
  // CHECK TO SEE IF THE POLICY HAS A BOUND PERIOD BEFORE CALLING THIS CONSTRUCTOR
  // This will fail if there is another submission in draft status on the Account
  construct(passedInPriorTDICPolicy : entity.Policy){
    _policyNumber = passedInPriorTDICPolicy.LatestBoundPeriod.PolicyNumber
    _carrier = "TDIC"
    _dtPolicyStart = passedInPriorTDICPolicy.BoundPeriods.orderBy(\elt -> elt.PeriodStart).first().PeriodStart
    _dtPolicyEnd = passedInPriorTDICPolicy.LatestBoundPeriod.PeriodEnd
  }

}