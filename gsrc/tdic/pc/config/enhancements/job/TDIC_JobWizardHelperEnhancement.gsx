package tdic.pc.config.enhancements.job

uses gw.api.productmodel.ClausePattern
uses gw.api.upgrade.PCCoercions
uses gw.api.util.DisplayableException
uses gw.api.web.job.JobWizardHelper
uses gw.job.SubmissionProcess
uses entity.Job
uses tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper

/**
 * Shane Sheridan 10/14/2014
 * This is an extension to the OOTB JobWizardHelperEnhancement for TDIC.
 */
enhancement TDIC_JobWizardHelperEnhancement : JobWizardHelper {

  /**
  * US462
  * 10/14/2014 Shane Sheridan
  */
  function getInitialWizardStepId_TDIC(branch: PolicyPeriod) : String {
    if ( branch.ValidQuote ) {
      return this.getQuoteStep(branch)
    }
    else if ( branch.WC7LineExists and (branch.Job as Submission).QuoteType == typekey.QuoteType.TC_QUICK ){
      return "WC7WorkersCompStateCoverages"
    }
    else{
      return null
    }
  }

  /**
  * US462
  * 10/17/2014 Shane Sheridan
  */
  function convertQuickQuoteToFullApp_TDIC(submissionProcess : SubmissionProcess, policyPeriod : PolicyPeriod,
                                           jobWizardHelper : JobWizardHelper) {
    // first call OOTB functionality
    this.convertQuickQuoteToFullApp(submissionProcess, policyPeriod)

    // create the exclusion during the transfer process to ensure that the Exclusion is Suggested.
    if(policyPeriod.WC7LineExists){
      //GW-102: Receiving validation error in error regarding owner/officer exclusions for Sole Proprietor/Individual submissions
      if( policyPeriod.WC7Line.partnersOfficersAndOthersExclusionEndorsementAvailability() ) {
         policyPeriod.WC7Line.setCoverageConditionOrExclusionExists(PCCoercions.makeProductModel<ClausePattern>("WC7PartnersOfficersAndOthersExclEndorsementExcl"), true)
      }
    }
  }

/**
 * US505
 * 11/17/2014 Shane Sheridan
 */
 function auditReportingInitiation(thisPolicyPeriod : PolicyPeriod) {
   var lastPolicyPeriod = thisPolicyPeriod.BasedOn
   var threshold = ScriptParameters.TDIC_WC7AuditReportingInitializationThresholdValue
   var lastTotalPremium = lastPolicyPeriod.TotalPremiumRPT.Amount
   var thisTotalPremium = thisPolicyPeriod.TotalPremiumRPT.Amount
   var thisAuditInfo = thisPolicyPeriod.Policy.AllAuditInformations.firstWhere( \ elt -> elt.PolicyTerm == thisPolicyPeriod.PolicyTerm)

   // BrianS - Audit Information was expected to exist, but was suppressed during migration by GW-1492.
   if (thisAuditInfo != null) {
     if(lastTotalPremium < threshold and thisTotalPremium > threshold){
       thisAuditInfo.AuditMethod = typekey.AuditMethod.TC_PHYSICAL
       thisAuditInfo.PhysicalAuditFlagged_TDIC = java.util.Date.CurrentDate
     }
     else if(lastTotalPremium > threshold and thisTotalPremium < threshold){
       thisAuditInfo.AuditMethod = typekey.AuditMethod.TC_VOLUNTARY
     }
     // ELSE no such changes to premium - no re-initialization required.
   }
 }

  /**
   * US469
   * Shane Sheridan 1/16/2015
   *
   * Ensures State Coverage screen does not show deleted WC7CoveredEmployee rows for Renewals that pull
   * data from previous Final Audit.
  */
  function stateCoveragesScreenOnFirstEnter(job : Job) : boolean{
    if(job.Subtype == typekey.Job.TC_RENEWAL){
      this.refreshBundle()
    }

    // Returns a dummy value so function can be invoked by initializing a PCF variable
    return false
  }

  //GW-834
  function checkForDocumentTemplates(eventName: typekey.TDIC_DocCreationEventType, aPolicyPeriod: PolicyPeriod){
  // KW 04-12-16  removed for POC
    if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_POLICYCENTER.Code){
      var lob = TDIC_PCExstreamHelper.getLOBCode(aPolicyPeriod)
      if(!(new TDIC_PCExstreamHelper()).getEventMapParams("${lob}.${eventName.Code}", aPolicyPeriod.Job.getJobDate())?.HasElements){
        throw new DisplayableException("Document template mappings not found. Please contact System Administrator.")
      }
    }
  }

}
