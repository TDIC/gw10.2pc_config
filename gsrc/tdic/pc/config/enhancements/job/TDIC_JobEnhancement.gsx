package tdic.pc.config.enhancements.job

uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory
uses tdic.pc.common.util.StringUtil
uses tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper

/**
 * Shane Sheridan 08/24/2014
 *
 * Created to extend the OOTB JobEnhancement for TDIC.
 * 20150225 TJ Talluto - enhanced to provide reasons and descriptions homogeneously for all txns
 */
enhancement TDIC_JobEnhancement: entity.Job {
  property get PolicyTransactionReason_TDIC(): String {
    var tmpReason: String
    switch (typeof this) {
      case Audit:
          break;
      case Cancellation:
          tmpReason = this.CancelReasonCode.Description
          break
      case Issuance:
          break
      case PolicyChange:
          tmpReason = this.ChangeReasons*.ChangeReason*.Description.join(", ")
          break
      case Reinstatement:
          tmpReason = this.ReinstateCode.Description
          break
      case Renewal:
          if (this.SelectedVersion.Status == PolicyPeriodStatus.TC_NONRENEWING) {
            tmpReason = this.RenewalReasons*.NonRenewalCode*.Description.join(", ")
          }
          break
      case Rewrite:
          tmpReason = this.RewriteType.Description
          break
      case RewriteNewAccount:
          break
      case Submission:
          tmpReason = this.RejectReason.Description
          break
        default: throw new IllegalArgumentException("Unhandled job subtype " + this.DisplayType)
    }
    return tmpReason
  }

  property get PolicyTransactionDescription_TDIC(): String {
    var tmpDesc: String
    switch (typeof this) {
      case Audit:
      case Cancellation:
      case Issuance:
      case PolicyChange:
      case Reinstatement:
      case Renewal:
      case Rewrite:
      case RewriteNewAccount:
          tmpDesc = this.Description
          break
      case Submission:
          tmpDesc = this.RejectReasonText
          break
        default: throw new IllegalArgumentException("Unhandled job subtype " + this.DisplayType)
    }
    //GWPC-151 - ProdSupport Code merge
    if(tmpDesc.HasContent){
      tmpDesc =  StringUtil.removeSpecialCharacters(tmpDesc, "•#&")
    }
    return tmpDesc
  }


  /**
   * US669
   * 04/22/2015 Shane Murphy
   *
   * This method is used by jobs to call the 'createSingleDocumentsForEvent' method with a null argument
   */
  function createSingleDocumentForCurrentJob() {
  //KW 04/12/16 - commented out to prevent re-issuing documents during manual entry
    if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_POLICYCENTER as String){
      createSingleDocumentsForEvent(null)
    }
  }

  /**
   * US555
   * 11/19/2014 shanem
   *
   * Creates a document stub for all documents associated with the SendIssuance event and
   * then raises this event for messaging queue to call HP Exstream to create content.
   * If it is a Job, the workflwo passes null and the Job's subtype is used.
   */
  function createSingleDocumentsForEvent(event: String): void {
    //Britto - disabling , revisit after the v10 upgrade.

    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Entering.")
    //804 upgrade - Generating documents with in an existing transaction
    var pp = this.SelectedVersion
    var eventName = event == null ? getEventNameForCurrentJob() : event
    if((eventName == TDIC_DocCreationEventType.TC_CANCELLATION as String ||
        eventName == TDIC_DocCreationEventType.TC_ENDORSEMENT as String) and not(pp.WC7LineExists)) {
      if(pp.NewlyAddedForms != null and  pp.NewlyAddedForms.HasElements){
        _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Searching TemplateToEventMap for relevant templates for ${eventName} event.")
        createDocuments(eventName, pp)
        _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Notifying Exstream Message Queue for event: ${eventName}.")
        pp.addEvent(eventName)
        _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Event ${eventName} added to PolicyPeriod: ${pp}.")
        //JIRA GW-2687 - Additional logging to trace this issue
        if(eventName == TDIC_DocCreationEventType.TC_CANCELLATION as String) {
          if(gw.transaction.Transaction.getCurrent() != null){
            var documents = gw.transaction.Transaction.getCurrent().InsertedBeans.whereTypeIs(entity.Document)?.where( \ doc -> doc.Event_TDIC == eventName)
            _exstreamLogger.info("TDIC_PCExstreamHelper#getDocs(${Job}) - Documents retrieved from the bundle are: ${documents}")
          }else{
            _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - ${eventName} - CurrentBundle is null")
          }
        }
        _exstreamLogger.trace("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Exiting.")
      }
    }
    else{
      _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Searching TemplateToEventMap for relevant templates for ${eventName} event.")
      createDocuments(eventName, pp)
      _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Notifying Exstream Message Queue for event: ${eventName}.")
      pp.addEvent(eventName)
      _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Event ${eventName} added to PolicyPeriod: ${pp}.")
      //JIRA GW-2687 - Additional logging to trace this issue
      if(eventName == TDIC_DocCreationEventType.TC_CANCELLATION as String) {
        if(gw.transaction.Transaction.getCurrent() != null){
          var documents = gw.transaction.Transaction.getCurrent().InsertedBeans.whereTypeIs(entity.Document)?.where( \ doc -> doc.Event_TDIC == eventName)
          _exstreamLogger.info("TDIC_PCExstreamHelper#getDocs(${Job}) - Documents retrieved from the bundle are: ${documents}")
        }else{
          _exstreamLogger.info("TDIC_JobEnhancement#createSingleDocumentsForEvent() - ${eventName} - CurrentBundle is null")
        }
      }
      _exstreamLogger.trace("TDIC_JobEnhancement#createSingleDocumentsForEvent() - Exiting.")
    }
  }

  /**
   * US669
   * 04/21/2015 Shane Murphy
   *
   * Creates a single document to hold the event's documents
   * If the event is a Submission/PolicyChange/Renewal, creates a schedule rating document
   * If the event is a Submission, creates a ClaimsKit document
   *
   * N.B. The order in which the documents are created is important for the event fired rules
   */
  @Param("anEventName", "The event name")
  @Param("p", "The policyPeriod to creat the documents for")
  function createDocuments(anEventName: String, pp: PolicyPeriod): void {
    var pcHelper = new TDIC_PCExstreamHelper()
    var lob = TDIC_PCExstreamHelper.getLOBCode(pp)
    pcHelper.createDocumentStub(anEventName, 99, pp, "${lob}.${anEventName}")

    if(pp.WC7LineExists){
      if (anEventName == TDIC_DocCreationEventType.TC_SUBMISSION.Code
          || anEventName == TDIC_DocCreationEventType.TC_RENEWAL.Code
          || (anEventName == TDIC_DocCreationEventType.TC_ENDORSEMENT.Code && pp.Job.ChangeReasons*.ChangeReason.contains(ChangeReasonType_TDIC.TC_SCHEDULERATING))) {
        if (pp.ManualPremium_TDIC.Amount >= 7500) {
          pcHelper.createDocumentStub(TDIC_DocCreationEventType.TC_SCHEDULERATING.Code, 99, pp, "${lob}.${TDIC_DocCreationEventType.TC_SCHEDULERATING.Code}")
          pp.addEvent(TDIC_DocCreationEventType.TC_SCHEDULERATING.Code)
        }
        // 20180330 TJT: GW-3174
        if (pp.ManualPremium_TDIC.Amount < 7500) {
          for (eachWC7Jurisdiction in pp.WC7Line.WC7Jurisdictions) {
            var tmpWC7modifiers = eachWC7Jurisdiction.WC7Modifiers
            for (eachWC7Modifier in tmpWC7modifiers) {
              if (eachWC7Modifier.PatternCode == "WC7ScheduleMod" and eachWC7Modifier.Eligible and eachWC7Modifier.RateWithinLimits != 0) {
                pcHelper.createDocumentStub(TDIC_DocCreationEventType.TC_SCHEDULERATING.Code, 99, pp, "${lob}.${TDIC_DocCreationEventType.TC_SCHEDULERATING.Code}")
                pp.addEvent(TDIC_DocCreationEventType.TC_SCHEDULERATING.Code)
              }
            }
          }
        }
        if (anEventName == TDIC_DocCreationEventType.TC_SUBMISSION.Code){
          pcHelper.createDocumentStub(TDIC_DocCreationEventType.TC_CLAIMSKIT.Code, 99, pp, "${lob}.${TDIC_DocCreationEventType.TC_CLAIMSKIT.Code}")
          pp.addEvent(TDIC_DocCreationEventType.TC_CLAIMSKIT.Code)
        }
      }
    }else{
      if(anEventName == TDIC_DocCreationEventType.TC_SUBMISSION.Code || anEventName == TDIC_DocCreationEventType.TC_RENEWAL.Code
          || anEventName == TDIC_DocCreationEventType.TC_ENDORSEMENT.Code || anEventName == TDIC_DocCreationEventType.TC_CANCELLATION.Code
          || anEventName == TDIC_DocCreationEventType.TC_REINSTATEMENT.Code)
        //Crate documents for events endorsment,Certificate Holder and Mortgage Forms.
        createDocumentsForCustomEvents(lob, pp)
    }
  }

  @Param("anEventName", "The event name")
  @Param("p", "The policyPeriod to creat the documents for")
  function createDocumentsForCustomEvents(lob: String,pp: PolicyPeriod): void {
    var endorsAndCertHolderForms = TDIC_PCExstreamHelper.getEndorsmentAndCertifiateHolderForms()
    var pcHelper = new TDIC_PCExstreamHelper()
    for(form in pp.NewlyAddedForms){
      var eventName : String
      if(endorsAndCertHolderForms.containsKey(form.FormPatternCode)){
        eventName = endorsAndCertHolderForms.get(form.FormPatternCode) as String
      }
      if(eventName!=null){
        pcHelper.createDocumentStub(form.FormPatternCode, 99, pp, "${lob}.${eventName}")
        pp.addEvent(eventName)
      }

    }

  }


  /**
   * US669
   * 02/27/2015 Shane Murphy
   *
   * Gets all possible forms and documents for this policy from the exstream mapping cache. Removes inapplicable
   * forms by comparing against the actual forms inferred on the policy (this method is called after the forms
   * are inferred). returns a map of templateId's to send to exstream and their print order for this event.
   */
  @Param("eventName", "The name of the event")
  @Param("isJob", "Whether the event is triggered by a job (i.e. has forms)")
  function getTemplates(eventName: String, isJob: boolean): HashMap<String, Integer> {
    var usesOOTBFormsInference = TDIC_DocCreationEventType.TF_USESOOTBFORMSINFERNENCE.TypeKeys.hasMatch(\tc -> tc.Code == eventName);

    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_JobEnhancement#getTemplates() - Entering.")
    var templateMapping = new HashMap<String, Integer>()
    var policyForms = this.LatestPeriod.Forms
    var helper = new TDIC_ExstreamHelper()
    var pcHelper = new TDIC_PCExstreamHelper()
    var formCodes = new ArrayList<String>()
    var lob = TDIC_PCExstreamHelper.getLOBCode(this.LatestPeriod)
    eventName = "${lob}.${eventName}"

    /*
     * If there are forms on the policy and it is a job (i.e. not a process e.g. Withdraw),
      * use these and any additional mappings in the cache manager
     */
    if (usesOOTBFormsInference == false and policyForms != null && isJob) {
      policyForms.each(\form -> formCodes.add(form.Pattern.Code))
      var eventTemplates = pcHelper.getEventMapParams(eventName, this.getJobDate())
      for (formTemplate in eventTemplates.where(\elt -> elt.DocType == "Form")) {
        if (!formCodes.contains(formTemplate.TemplateId)){
          eventTemplates.remove(formTemplate)
        }
      }
      eventTemplates.each(\elt -> templateMapping.put(elt.TemplateId, elt.PrintOrder))
    } else {
      //If no forms, use eventmappings properites file
      templateMapping = helper.getEventTemplates(eventName, this.getJobDate())
    }

    if (usesOOTBFormsInference) {
      var endorsAndCertHolderForms = TDIC_PCExstreamHelper.getEndorsmentAndCertifiateHolderForms().keySet()
      var discountForms = TDIC_PCExstreamHelper.getDiscountConforForms().keySet()
      for (form in this.LatestPeriod.NewlyAddedForms) {
        if(!endorsAndCertHolderForms.contains(form.FormPatternCode)
            && !discountForms.contains(form.FormPatternCode))
          templateMapping.put (form.FormPatternCode, form.Priority_TDIC);
      }
    }

    //Remove the template ID's which are ending with Packet.
    templateMapping.entrySet().removeIf(\ entry -> (entry.getKey().endsWithIgnoreCase("Packet")))

    _exstreamLogger.info("TDIC_JobEnhancement#getTemplates() - ${templateMapping.Count} templates found for event: ${eventName}.")
    _exstreamLogger.trace("TDIC_JobEnhancement#getTemplates() - Templates found for event: ${eventName} - ${templateMapping}")
    _exstreamLogger.trace("TDIC_JobEnhancement#getTemplates() - Exiting.")
    return templateMapping
  }

  /**
   * Gets an event name to raise for the job
   */
  @Param("job", "The Job for which we require the event name")
  @Returns("Appropritate Event Name for the job")
  @Throws(IllegalArgumentException, "Thrown if no event name defined for job type")
  function getEventNameForCurrentJob(): String {
    switch (this.Subtype) {
      case typekey.Job.TC_SUBMISSION:
          return typekey.Job.TC_SUBMISSION.toString()
      case typekey.Job.TC_RENEWAL:
          return typekey.Job.TC_RENEWAL.toString()
      case typekey.Job.TC_CANCELLATION:
          return typekey.Job.TC_CANCELLATION.toString()
      case typekey.Job.TC_REWRITE:
          return typekey.Job.TC_REWRITE.toString()
      case typekey.Job.TC_REINSTATEMENT:
          return typekey.Job.TC_REINSTATEMENT.toString()
      case typekey.Job.TC_POLICYCHANGE:
          return "Endorsement"
        default:
        throw new IllegalArgumentException("The job type does not have an event name associated with it.")
    }
  }

  function isIssuanceInProgress(): boolean {
    var issuanceInProgress = this.Documents.toList().hasMatch(\d -> d.Status == TC_APPROVED)
    var logger = LoggerFactory.getLogger("Plugin")
    logger.info("Is Issuance In Progress: " + issuanceInProgress)
    return issuanceInProgress
  }

  function finishIssuance(): void {
    var logger = LoggerFactory.getLogger("Plugin")
    logger.info("Inside finish Issuance")
    switch (typeof this) {
      case Cancellation:
          this.SelectedVersion.CancellationProcess.finishCancellation()
          break
      case Issuance:
          this.SelectedVersion.IssuanceProcess.finishIssuing()
          break
      case PolicyChange:
          this.SelectedVersion.PolicyChangeProcess.finishBinding()
          break
      case Reinstatement:
          this.SelectedVersion.ReinstatementProcess.finishReinstatement()
          break
      case Renewal:
          //20161129 TJT GW-2583 removing this function (it was called from TDICTestBase.gs only)
          //  this.SelectedVersion.RenewalProcess.finishRenewing()   //finishRenewing()
		  break
      case Rewrite:
          this.SelectedVersion.RewriteProcess.finishRewrite()
          break
      case Submission:
          this.SelectedVersion.SubmissionProcess.finishIssuing()
          break
        default: throw new IllegalArgumentException("Unhandled job subtype " + this.DisplayType)
    }
  }

  //GW-446 Migration job indicator
  property get MigrationJobInd_TDIC() : boolean{
    // All Migration job will have MigrationJobInfo_Ext
    return this.MigrationJobInfo_Ext != null
  }

  //20160304 TJT : GW-299
  // This function was originally created to support ProductModel Availability for the WorkersComp Supplemental Question Set
  // If any Period on this Policy was migrated, then we know this is a migrated policy.
  // There are other approaches, but this one seems agnostic to hardcoding (prefix on the Txn, digits in policy number, etc.)
  property get BasedOnMigratedRootTxn_TDIC() : boolean {
    var blnFoundAMigratedRootTxn : boolean = false

    // BrianS - Check script parameter, since MigrationJobInd_TDIC does not return true when this function
    //          is called for submissions.
    if (ScriptParameters.TDIC_DataSource != DataSource_TDIC.TC_POLICYCENTER as String) {
      blnFoundAMigratedRootTxn = true;
    } else {
      for (each in this.Policy.Periods.orderBy(\ elt -> elt.EditEffectiveDate)){ // This will locate unbound Submissions
        if (each.BasedOn != null){
          if (each.BasedOn.Job.MigrationJobInd_TDIC){
            blnFoundAMigratedRootTxn = true
            break; //exit the loop
          }
        }
      }
    }
    return blnFoundAMigratedRootTxn
  }

  //20160412 TJT - This needs to survive after migration
  property get TxnOrTermIsMigrated_TDIC() : boolean {
    var blnFoundAMigratedTxnInThisTerm : boolean = false
    var tmpPeriod = this.SelectedVersion // the context of the Period being rated on this Job
    var tmpTerm = tmpPeriod.TermNumber
    while (tmpPeriod != null){
      if (tmpPeriod.TermNumber != tmpTerm){
        break; //exit the loop
      }
      if (tmpPeriod.Job.MigrationJobInd_TDIC){
        blnFoundAMigratedTxnInThisTerm = true
        break; //exit the loop
      }
      tmpPeriod = tmpPeriod.BasedOn
    }
    return blnFoundAMigratedTxnInThisTerm
  }

  //20160610 TJT GW-1953 - Use to suppress functionality for migrated txns that are expected to reach completion
  //returns true if this txn is completed in CHSI, returns false if the txn is incomplete in CHSI.  Examples:
  // Generic txn is bound/issued without any subsequent txn events = TRUE
  // Cancellation txn is bound/issued + rescinded = TRUE
  // Renewal txn is bound/issued and subsequently not-taken/not-renewed = TRUE
  property get MigratedTxnIsCompletedInCHSI_TDIC() : boolean {
    var blnMigratedTxnIsCompletedInCHSI_TDIC : boolean = false
    if (this.MigrationJobInd_TDIC){
      if (this.MigrationJobInfo_Ext.PayloadType != null){
        if (typekey.MigrationPayloadType_Ext.TF_COMPLETETRANSACTIONS.TypeKeys.contains(this.MigrationJobInfo_Ext.PayloadType)){
          blnMigratedTxnIsCompletedInCHSI_TDIC = true
        }
      }
    }
    return blnMigratedTxnIsCompletedInCHSI_TDIC
  }


  // GPS 568 Infer Document based on policy hcnage reason

  public function isValidPolicyChangeReasons_TDIC(pp : PolicyPeriod) : boolean {
    var reason = pp?.Job?.ChangeReasons
    if (reason.hasMatch(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_CLASSCODESPECIALTYCHANGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_CERTINSURANCEPDL ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_COMPONENTCHANGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_DENTALEMPLOYMENTPRACTICESLIABILITYEPLI ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_DENTALBUSINESSLIABAI ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_FULLTIMEFACULTYMEMBER ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_FULLTIMEPOSTGRADUATESTUDENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_DISABILITYDISCOUNT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_IDENTITYTHEFTRECOVERYIDR ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_LIMITSINCREASEDECREASE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_LOCUMTENENS ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MAILINGADDRESS ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MOBILECLINIC ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MULTILINEDISCOUNT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MULTIOWNERDENTALPRACTICEENTITY ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NAMEONTHEDOOR ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NAMEDINSURED ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NEWTOCOMPANY ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NEWGRADUATE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NEWJERSEYWAIVEROFRIGHTTOSETTLE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NEWJERSEYPLDEDUCTIBLE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NONMEMBERSURCHARGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_PARTTIMEDISCOUNT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_PROFLIABAI ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_RISKMANAGEMENTDISCOUNT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_SCHOOLSERVICESENDORSEMENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_SERVICEMEMBERSCIVILRELIEFACT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_SPECIALEVENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_STATEEXCLUSION ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_ADDITIONALINSURED ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXCLUDEDSERVICEENDORSEMENT
    )) {
      return true
    }
    return false
  }

  /**
   * GWPS-181 UI Validation for removal of special character.
   *
   */
  public function hasDescriptionContainsSpecialChars() : String{
    if(this.Description.HasContent and StringUtil.hasSpecialCharacters(this.Description,".*[&#•].*")){
      return DisplayKey.get("TDIC.Job.Policy.Error.SpecialChar.Description")
    }
    return null
  }
}
