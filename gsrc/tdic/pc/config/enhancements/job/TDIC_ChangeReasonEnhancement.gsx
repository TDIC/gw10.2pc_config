package tdic.pc.config.enhancements.job

/**
 * US1274
 * 04/02/2015 Rob Kelly
 *
 * Enhancement for adding and ChangedEntity_TDIC instances.
 */
enhancement TDIC_ChangeReasonEnhancement : entity.ChangeReason_TDIC {

  function addChangedEntity(isChangedToReadText : String) {
    if (this.ChangedEntities.firstWhere( \ e -> e.IsChangedToRead == isChangedToReadText) == null) {
      addNewChangedEntity(isChangedToReadText)
    }
  }

  private function addNewChangedEntity(isChangedToReadText : String) {
    var newChangedEntity = new ChangedEntity_TDIC()
    newChangedEntity.IsChangedToRead = isChangedToReadText
    this.addToChangedEntities(newChangedEntity)
  }
}
