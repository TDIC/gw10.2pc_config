package tdic.pc.config.enhancements.job.audit

uses java.text.SimpleDateFormat

enhancement TDIC_DisplayableAuditInfoEnhancement : gw.job.audit.DisplayableAuditInfo {
  /**
    Added this property for HP-Exstream Audit documents
  */
  property get DueDate_TDIC() : String {
    var dueDateTDIC:String = null
    if(this.DueDate != null){
      var dateFormat = new SimpleDateFormat("MM/dd/yyyy")
      dueDateTDIC = dateFormat.format(this.DueDate)
    }
    return dueDateTDIC
  }
}