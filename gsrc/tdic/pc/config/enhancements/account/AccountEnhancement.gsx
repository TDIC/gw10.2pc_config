package tdic.pc.config.enhancements.account
uses gw.account.SharedAccountSearchCriteria_TDIC
uses gw.api.database.IQueryResult
uses gw.api.database.IQueryBeanResult
uses typekey.AccountContactRole

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 9/9/14
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
enhancement AccountEnhancement : entity.Account {
  /**
  *  Return IndustryCode object from database and set the foreign key for Account.IndustryCode
  * */
  function InitializeIndustryCode(industryCode : String) : entity.IndustryCode {
   // Query to get the Industry Code from Database
   var queryObj = gw.api.database.Query.make(IndustryCode)
                  .compare(IndustryCode#Code, Equals, industryCode).select()

   // Set the foreign key value for the Account.IndustryCode
   this.IndustryCode = queryObj.AtMostOneRow
   return this.IndustryCode
  }

  /**
   * BrittoS 03/19/2020
   * @return related accounts with a common account holder or named insured
   */
  property get RelatedAccountsBySharedNamedInsuredContacts_TDIC() : Account[] {
    var searchCriteria = new SharedAccountSearchCriteria_TDIC() {
      :Secure = true,
      :AccountNumber = this.AccountNumber,
      :SearchableSharedContactRoles={typekey.AccountContactRole.TC_NAMEDINSURED, AccountContactRole.TC_OWNEROFFICER, AccountContactRole.TC_ENTITYOWNER_TDIC} }
    var result = searchCriteria.performSearch()
    return result.map(\accountSummary -> accountSummary.Account).toTypedArray()
  }
}
