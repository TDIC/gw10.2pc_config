package tdic.pc.config.enhancements.lob.gl

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty

enhancement TDIC_GLLineEnhancement : GLLine {

  property get Discounts_TDIC(): String[] {

    var output = new ArrayList<String>()
    if (this.Branch.MultiLineDiscount_TDIC != null && this.Branch.MultiLineDiscount_TDIC != MultiLineDiscount_TDIC.TC_N){
      if (this.Branch.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_A)
        output.add("Multi-line Optimum")
      else if (this.Branch.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_P)
        output.add("Multi-line Property")
      else if (this.Branch.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_W)
        output.add("Multi-line WC")
    }

    var discounts = this.Pattern.getCoverageCategoryByCodeIdentifier("GLDiscountsCat_TDIC").conditionPatternsForEntity(GeneralLiabilityLine).whereSelectedOrAvailable(this,false).sortBy(\ cond -> cond.Priority)
    foreach (discount in discounts){
      output.add(discount.DisplayName)
    }

    return output.toTypedArray()
  }

  property get RMDiscountEndDate_TDIC():Date{
    return this.RiskManagementDisc_TDIC?.maxBy(\elt -> elt.EndDate)?.EndDate
  }

  /**
   *
   * @return
   */
  property get AdditionalInsuredSchedule_TDIC() : Set<GLAdditionInsdSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLAdditionInsdSched_TDIC)*.Bean
      var schedules = this.GLAdditionInsdSched_TDIC.where(\elt -> newOrChangedSched?.hasMatch(\elt1 ->(elt1 typeis GLAdditionInsdSched_TDIC) and  (elt1 as GLAdditionInsdSched_TDIC).AdditionalInsured==elt.AdditionalInsured))?.toSet()

      var newOrChangedAddlIns = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsured)*.Bean
      var addlInsrdsSched = this.GLAdditionInsdSched_TDIC.where(\elt -> newOrChangedAddlIns?.hasMatch(\elt1 ->(elt1 typeis PolicyAddlInsured) and  (elt1 as PolicyAddlInsured)==elt.AdditionalInsured.PolicyAddlInsured))?.toSet()

      var newOrChangedAddlInsDetail = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsuredDetail)*.Bean
      var addlInsrdsSchedDetail = this.GLAdditionInsdSched_TDIC.where(\elt -> newOrChangedAddlInsDetail?.hasMatch(\elt1 ->(elt1 typeis PolicyAddlInsuredDetail) and
          elt.AdditionalInsured.PolicyAddlInsured.PolicyAdditionalInsuredDetails?.hasMatch(\elt2 -> elt2 typeis PolicyAddlInsuredDetail and elt2 == elt1 as PolicyAddlInsuredDetail)))?.toSet()

      /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLManuscript_TDIC)*.Bean
      var aiList=new ArrayList<GLAdditionInsdSched_TDIC>()
      aiList.addAll(schedules?.toList())
      aiList.addAll(addlInsrdsSched?.toList())
      aiList.addAll(addlInsrdsSchedDetail?.toList())
      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= this.Branch.GLLine.GLAdditionInsdSched_TDIC?.where(\elt-> (ai as GLManuscript_TDIC).PolicyAddInsured_TDIC==elt.AdditionalInsured)
        aiList.addAll(manuscript?.toList())
      }
      return aiList.toSet()
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLAdditionInsdSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLAdditionInsdSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLAdditionInsdSched_TDIC.toSet()
    }
  }

  property get CertificateOfInsuredSchedule_TDIC() : Set<GLCertofInsSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLCertofInsSched_TDIC)*.Bean
      var schedules = this.GLCertofInsSched_TDIC.where(\elt -> newOrChangedSched?.hasMatch(\elt1 ->(elt1 typeis GLCertofInsSched_TDIC) and  (elt1 as GLCertofInsSched_TDIC).CertificateHolder==elt.CertificateHolder))?.toSet()

      var newOrChangedCertHolders = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyCertificateHolder_TDIC)*.Bean
      var certHldrSched = this.GLCertofInsSched_TDIC.where(\elt -> newOrChangedCertHolders?.hasMatch(\elt1 ->(elt1 typeis PolicyCertificateHolder_TDIC) and  (elt1 as PolicyCertificateHolder_TDIC)==elt.CertificateHolder))?.toSet()

      /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLManuscript_TDIC)*.Bean
      var aiList=new ArrayList<GLCertofInsSched_TDIC>()
      aiList.addAll(schedules?.toList())
      aiList.addAll(certHldrSched?.toList())
      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= this.Branch.GLLine.GLCertofInsSched_TDIC?.where(\elt-> (ai as GLManuscript_TDIC).CertificateHolder==elt.CertificateHolder)
        aiList.addAll(manuscript?.toList())
      }
      return aiList.toSet()
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLCertofInsSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLCertofInsSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLCertofInsSched_TDIC.toSet()
    }
  }

  property get DentalBLAISchedule_TDIC() : Set<GLDentalBLAISched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLDentalBLAISched_TDIC)*.Bean
      var schedules = this.GLDentalBLAISched_TDIC.where(\elt -> newOrChangedSched?.hasMatch(\elt1 ->(elt1 typeis GLDentalBLAISched_TDIC) and  (elt as GLDentalBLAISched_TDIC).AdditionalInsured == elt.AdditionalInsured))?.toSet()

      var newOrChangedAddlIns = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsured)*.Bean
      var addlInsrdsSched = this.GLDentalBLAISched_TDIC.where(\elt -> newOrChangedAddlIns?.hasMatch(\elt1 ->(elt1 typeis PolicyAddlInsured) and  (elt1 as PolicyAddlInsured)==elt.AdditionalInsured.PolicyAddlInsured))?.toSet()

      var newOrChangedAddlInsDetail = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsuredDetail)*.Bean
      var addlInsrdsSchedDetail = this.GLDentalBLAISched_TDIC.where(\elt -> newOrChangedAddlInsDetail?.hasMatch(\elt1 ->(elt1 typeis PolicyAddlInsuredDetail) and
          elt.AdditionalInsured.PolicyAddlInsured.PolicyAdditionalInsuredDetails?.hasMatch(\elt2 -> elt2 typeis PolicyAddlInsuredDetail and elt2 == elt1 as PolicyAddlInsuredDetail)))?.toSet()

      /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLManuscript_TDIC)*.Bean
      var aiList=new ArrayList<GLDentalBLAISched_TDIC>()
      aiList.addAll(schedules?.toList())
      aiList.addAll(addlInsrdsSched?.toList())
      aiList.addAll(addlInsrdsSchedDetail?.toList())
      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= this.Branch.GLLine.GLDentalBLAISched_TDIC?.where(\elt-> (ai as GLManuscript_TDIC).PolicyAddInsured_TDIC==elt.AdditionalInsured)
        aiList.addAll(manuscript?.toList())
      }
      return aiList.toSet()
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLDentalBLAISched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLDentalBLAISched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLDentalBLAISched_TDIC.toSet()
    }
  }


  property get SchoolServSchedule_TDIC() : Set<GLSchoolServSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLSchoolServSched_TDIC)*.Bean
      var schedules = this.GLSchoolServSched_TDIC.where(\elt -> newOrChangedSched?.contains(elt))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLSchoolServSched_TDIC.where(\elt -> elt.EventEndDate == null || elt.EventEndDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLSchoolServSched_TDIC.where(\elt -> elt.EventEndDate == null || elt.EventEndDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLSchoolServSched_TDIC.toSet()
    }

  }

  property get LocumTenensSchedule_TDIC() : Set<GLLocumTenensSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLLocumTenensSched_TDIC)*.Bean
      var schedules = this.GLLocumTenensSched_TDIC.where(\elt -> newOrChangedSched?.contains(elt))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLLocumTenensSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLLocumTenensSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLLocumTenensSched_TDIC.toSet()
    }
  }

  property get NameOnDoorEndorsmntSchedule_TDIC() : Set<GLNameODSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLNameODSched_TDIC)*.Bean
      var schedules = this.GLNameODSched_TDIC.where(\elt -> newOrChangedSched?.contains(elt))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLNameODSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLNameODSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLNameODSched_TDIC.toSet()
    }
  }

  property get SpecialEventEndorsmntSchedule_TDIC() : Set<GLSpecialEventSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLSpecialEventSched_TDIC)*.Bean
      var schedules = this.GLSpecialEventSched_TDIC.where(\elt -> newOrChangedSched?.contains(elt))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLSpecialEventSched_TDIC.where(\elt -> elt.EventEndDate == null || elt.EventEndDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLSpecialEventSched_TDIC.where(\elt -> elt.EventEndDate == null || elt.EventEndDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLSpecialEventSched_TDIC.toSet()
    }
  }

  property get MobileClinicEndorsmntSchedule_TDIC() : Set<GLMobileDCSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var schedules = this.GLMobileDCSched_TDIC?.where(\elt -> elt.LTEffectiveDate?.daysBetween(this.Branch.EditEffectiveDate) == 0 ||
          (elt.LTExpirationDate != null && (elt.LTExpirationDate?.daysBetween(this.Branch.EditEffectiveDate) == 0)))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLMobileDCSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLMobileDCSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLMobileDCSched_TDIC.toSet()
    }
  }

  property get StateExclEndorsmntSchedule_TDIC() : Set<GLStateExclSched_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLStateExclSched_TDIC)*.Bean
      var schedules = this.GLStateExclSched_TDIC.where(\elt -> newOrChangedSched?.contains(elt))?.toSet()
      return schedules
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLStateExclSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLStateExclSched_TDIC.where(\elt -> elt.LTExpirationDate == null || elt.LTExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLStateExclSched_TDIC.toSet()
    }
  }

  property get ManuscriptEndorsmntSchedule_TDIC() : Set<GLManuscript_TDIC> {
    if (this.Branch.Job typeis PolicyChange) {
      var manuscriptItems : ArrayList<GLManuscript_TDIC> = {}
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var newOrChangedSched = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis GLManuscript_TDIC)*.Bean
      var schedules = this.GLManuscript_TDIC.where(\elt -> newOrChangedSched?.hasMatch(\elt1 -> (elt as GLManuscript_TDIC) == elt1))?.toList()
      manuscriptItems.addAll(schedules)

      AdditionalInsuredSchedule_TDIC?.each(\elt -> {
        manuscriptItems.addAll(elt.AdditionalInsured.GLManuscript?.toList())
      })

      CertificateOfInsuredSchedule_TDIC?.each(\elt -> {
        manuscriptItems.addAll(elt.CertificateHolder.GLManuscript?.toList())
      })

      DentalBLAISchedule_TDIC?.each(\elt -> {
        manuscriptItems.addAll(elt.AdditionalInsured.GLManuscript?.toList())
      })

      return manuscriptItems?.toSet()
    } else if(this.Branch.Job typeis  Cancellation){
      return this.GLManuscript_TDIC.where(\elt -> elt.ManuscriptExpirationDate == null || elt.ManuscriptExpirationDate > this.Branch.CancellationDate).toSet()
    } else if(this.Branch.Job typeis Reinstatement){
      return this.GLManuscript_TDIC.where(\elt -> elt.ManuscriptExpirationDate == null || elt.ManuscriptExpirationDate > this.Branch.EditEffectiveDate).toSet()
    } else{
      return this.GLManuscript_TDIC.toSet()
    }
  }

  property get CovD_TDIC() : GLDentalEmpPracLiabCov_TDIC {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.GLDentalEmpPracLiabCov_TDIC
    } else {
      if(this.GLDentalEmpPracLiabCov_TDICExists){
        return this.GLDentalEmpPracLiabCov_TDIC
      }else if(this.BasedOn?.GLDentalEmpPracLiabCov_TDICExists){
        return this.BasedOn?.GLDentalEmpPracLiabCov_TDIC
      }else{
        return null
      }
    }
  }
  property get isBlankManuscriptEndorsement() : Boolean {
    if (this.GLManuscript_TDIC.where(\elt -> elt.ManuscriptType == GLManuscriptType_TDIC.TC_BLANKMANUSCRIPT).Count > 0){
      return true
    }
    return false
  }

  property get covExtEndt_TDIC() : GLCovExtDiscount_TDIC {
    if (not(this.Branch.Job typeis PolicyChange)) {
      if(this.Branch.Job typeis Renewal and !this.GLCovExtDiscount_TDICExists and this.BasedOn?.GLCovExtDiscount_TDICExists){
        return null
      }
      return this.GLCovExtDiscount_TDIC
    } else {
      if(this.GLCovExtDiscount_TDICExists){
        return this.GLCovExtDiscount_TDIC
      }else{
        return null
      }
    }
  }
  property get covExtResumptionEfdt_TDIC() : Date {
    if(this.BasedOn?.GLCovExtDiscount_TDICExists){
      return this.Branch.EditEffectiveDate
    }
    return null
  }
  property get covEPLRetroExpDate_TDIC():Date{

    if(this.GLDentalEmpPracLiabCov_TDICExists){
      return this.Branch.PeriodEnd
    }else if(this.BasedOn?.GLDentalEmpPracLiabCov_TDICExists){
      return this.Branch.EditEffectiveDate
    }
    else{
    return null
    }
  }

  property get covGLExcludedServiceSched_TDIC():ArrayList<GLExcludedServiceSched_TDIC>{
    var excludeServiceItems : ArrayList<GLExcludedServiceSched_TDIC> = {}
    var glExcServSchedItems = this.GLExcludedServiceSched_TDIC
    if (this.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and this.Branch.GLLine?.GLExcludedServiceCond_TDICExists and
        glExcServSchedItems.HasElements) {
      if (this.Branch.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var GLExclSerItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLExcludedServiceSched_TDIC)*.Bean

        if (glExcServSchedItems.hasMatch(\item -> GLExclSerItemsModified.contains(item))) {
          var exclSerSchItems=glExcServSchedItems.where(\item -> GLExclSerItemsModified.contains(item))
          exclSerSchItems.each(\elt -> {
            excludeServiceItems.add(elt)
          })
        }
      }

      else if(this.Branch.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(this.Branch.RefundCalcMethod != CalculationMethod.TC_FLAT){
          glExcServSchedItems.each(\elt -> {
            excludeServiceItems.add(elt)
          })
        }
      } else {
        glExcServSchedItems.each(\elt -> {
          excludeServiceItems.add(elt)
        })
      }
    }
      return excludeServiceItems
  }

}