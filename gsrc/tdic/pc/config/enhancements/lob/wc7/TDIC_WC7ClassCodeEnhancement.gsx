package tdic.pc.config.enhancements.lob.wc7

uses tdic.pc.integ.plugins.wcpols.helper.TDIC_WCpolsUtil

uses java.math.BigDecimal

enhancement TDIC_WC7ClassCodeEnhancement: entity.WC7ClassCode {

  property get Rate(): BigDecimal {
    return TDIC_WCpolsUtil.getClassCodeRate(this.Code)
  }
}
