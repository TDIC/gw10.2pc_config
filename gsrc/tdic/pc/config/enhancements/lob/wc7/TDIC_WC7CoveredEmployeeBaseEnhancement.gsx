package tdic.pc.config.enhancements.lob.wc7
/**
 * US462
 * Shane Sheridan 10/16/2014
 *
 * This is an extension to the OOTB WC7CoveredEmployeeBaseEnhancement for TDIC.
 */
enhancement TDIC_WC7CoveredEmployeeBaseEnhancement : entity.WC7CoveredEmployee {

  /**
   * US462
   * Shane Sheridan 10/16/2014
   *
   * This is a copy of the OOTB set LocationWM property,
   * except the assertWindowMode(loc) line is removed, this is to allow the Location
   * field to be pre-populated in the UI of the State Coverages screen.
   */
  property set LocationWM_TDIC(loc: PolicyLocation)  {
    this.Location = loc
    if(loc != null){
      var exposureDateRange = loc.EffectiveDateRangeWM
          .intersect(loc.Branch.EditEffectiveDateRange)
      this.EffectiveDateRange = exposureDateRange
    }
  }
}
