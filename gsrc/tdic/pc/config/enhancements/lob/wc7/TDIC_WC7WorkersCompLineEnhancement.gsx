package tdic.pc.config.enhancements.lob.wc7

uses gw.api.util.StateJurisdictionMappingUtil
uses tdic.pc.integ.plugins.membership.TDIC_MembershipCheck
uses gw.lob.wc7.WC7ClassCodeSearchCriteria
uses gw.api.util.JurisdictionMappingUtil
uses gw.api.database.IQueryBeanResult

/**
 * US462
 * Shane Sheridan 10/14/2014
 *
 * This is an extension to the OOTB WC7WorkersCompLineEnhancement for TDIC.
 */
enhancement TDIC_WC7WorkersCompLineEnhancement : entity.WC7WorkersCompLine {

  /**
   * US462 - Shane Sheridan.
  */
  function addCoveredEmployeeWM_TDIC(state : State) : WC7CoveredEmployee {
    var eu = createAndAddWC7CoveredEmployee_TDIC(state)
    var monopolisticStates = gw.api.domain.StateSet.get(gw.api.domain.StateSet.WC_MONOPOLISTIC)
    if (monopolisticStates.contains(state)) {
      eu.GoverningLaw = WC7GoverningLaw.TC_DEFENSEBASEACT
    }
    return eu.VersionList.AllVersions.single()
  }

  /**
   * US462 - Shane Sheridan.
  */
  //FIXME TJT - need date in search, this was done incorrectly and doesn't match a manual search using the UI
  function createAndAddWC7CoveredEmployee_TDIC(state : State) : WC7CoveredEmployee {
    var eu = new WC7CoveredEmployee(this.Branch)
    eu.GoverningLaw = WC7GoverningLaw.TC_STATE

    //20160212 TJT
    var locationWM = this.PolicyLocations.firstWhere( \ elt -> elt.State == state)
    var criteria = new WC7ClassCodeSearchCriteria()
    criteria.Code = "8839"
    criteria.EffectiveAsOfDate = this.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(locationWM))
    criteria.Jurisdiction = typekey.Jurisdiction.get(JurisdictionMappingUtil.getJurisdiction(locationWM) as String)
    var q : IQueryBeanResult<WC7ClassCode> = criteria.performSearch()
    eu.ClassCode = q.AtMostOneRow

    // Initialize Location field.
    if(this.WC7CoveredEmployees.IsEmpty or this.PolicyLocations.length < 2){
      var location = this.Branch.getPolicyLocationWM(state).first()
      eu.LocationWM_TDIC = location
    }

    this.addToWC7CoveredEmployees(eu)
    return eu
  }

  /**
   * US696, robk
   */
  public function setCDAMembershipStatusOnAllOwnerOfficers_TDIC() {
    var membershipCheck = new TDIC_MembershipCheck()
    for (anOwnerOfficer in this.WC7PolicyOwnerOfficers) {
      var ooContact = anOwnerOfficer.AccountContactRole.AccountContact.Contact
      if (ooContact typeis Person and ooContact.ADANumberOfficialID_TDIC!=null) {
        ooContact.CDAMembershipStatus_TDIC = membershipCheck.checkMembership(ooContact.ADANumberOfficialID_TDIC, StateJurisdictionMappingUtil.getStateMappingForJurisdiction(this.getBaseState()))
      }
    }
  }

  /**
   * US1377
   * ShaneS 04/27/2015
  */
  function getTotalBasisForPolicyLocation_TDIC(location : PolicyLocation) : int{
    var totalBasis = this.WC7CoveredEmployeeBases.where(\ e -> e.Location == location).sum( \ e -> e.BasisAmount)
    return totalBasis
  }

  /**
   * US1377
   * ShaneS 04/27/2015
   */
  function getTotalNumEmployeesForPolicyLocation_TDIC(location : PolicyLocation) : int {
    return location.NumEmployees
  }
}