package tdic.pc.config.enhancements.lob.wc7
/**
 * Created with IntelliJ IDEA.
 * User: jlane
 * Date: 10/3/14
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */
enhancement TDIC_WorkersCompLine : entity.WC7WorkersCompLine {

  function anniversaryDateChanged() : Boolean{
    var jurisdictions = this.WC7Jurisdictions

    foreach(jurisdiction in jurisdictions ){
         if(jurisdiction.AnniversaryDate != this.Branch.getPeriodStart()){
            return true
         }
    }
      return false
  }

  /**
  * US462
  * 10/20/2014 Shane Sheridan
  * This is the availability script for Partners, Officers And Others Exclusion Endorsement.
  */
  @Returns("Boolean result of availability check.")
  function partnersOfficersAndOthersExclusionEndorsementAvailability() : boolean{
    // IF Policy has the appropriate OrgType.
    if( this.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_LLC
        or this.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_PARTNERSHIP
        or this.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_PRIVATECORP ){

      // THEN - IF the associated Job is a Submission.
      if( this.Branch.Job typeis Submission ){
        // THEN true if Quote Type is Full App.
        return this.Branch.Job.QuoteType == typekey.QuoteType.TC_FULL
      }
      else{
        // true for any other Job subtypes.
        return true
      }
    }
    else{
      // false if Policy does NOT have the appropriate OrgType.
      return false
    }
  }

}
