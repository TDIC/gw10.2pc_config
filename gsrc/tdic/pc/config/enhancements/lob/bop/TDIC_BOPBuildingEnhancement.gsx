package tdic.pc.config.enhancements.lob.bop

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty

enhancement TDIC_BOPBuildingEnhancement : BOPBuilding {

  property get AdditionalInsuredSchedule_TDIC() : Set<PolicyAddlInsured> {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.AdditionalInsureds.toSet()
    } else{
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var addedOrModifiedItems = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsured)*.Bean
      var removedItems = this.AdditionalInsureds?.where(\elt -> elt.PolicyAdditionalInsuredDetails.first()?.ExpirationDate_TDIC != null
          and elt.PolicyAdditionalInsuredDetails.first()?.BasedOn.ExpirationDate_TDIC == null)
      var addlSched = this.AdditionalInsureds.where(\elt -> addedOrModifiedItems?.contains(elt) || removedItems.contains(elt))?.toSet()

      var newOrChangedAddlInsDetail = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsuredDetail)*.Bean
      var addlInsrdsSchedDetail = this.AdditionalInsureds?.where(\elt -> !newOrChangedAddlInsDetail?.intersect(elt.PolicyAdditionalInsuredDetails).Empty)?.toSet()

     /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis BOPManuscript_TDIC)*.Bean
      var aiList=new ArrayList<PolicyAddlInsured>()
      aiList.addAll(addlSched?.toList())
      aiList.addAll(addlInsrdsSchedDetail?.toList())

      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= (ai as BOPManuscript_TDIC).policyAddInsured_TDIC.PolicyAddlInsured
        /* Logic to add policyAddlInsured only for the building which has manuscript endorsement. */
        var isPolAddInsExists=this.BOPManuscript*.policyAddInsured_TDIC*.PolicyAddlInsured.hasMatch(\polAddlIns ->polAddlIns== manuscript)
        if(this.BOPManuscriptEndorsement_TDICExists and  isPolAddInsExists ) {
          aiList.add(manuscript)
        }
      }
      return aiList.toSet()
    }
  }

  property get LossPayeesSchedule_TDIC() : Set<PolicyLossPayee_TDIC> {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.BOPBldgLossPayees.toSet()
    } else {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var addedOrModifiedItems = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyLossPayee_TDIC)*.Bean
      var addlSched = this.BOPBldgLossPayees.where(\elt -> addedOrModifiedItems?.contains(elt))?.toSet()
      /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis BOPManuscript_TDIC)*.Bean
      var aiList=new ArrayList<PolicyLossPayee_TDIC>()
      aiList.addAll(addlSched?.toList())
      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= (ai as BOPManuscript_TDIC).PolicyLossPayee
        /* Logic to add PolicyLossPayee only for the building which has manuscript endorsement. */
        var isPolicyLossPayeeExists = this.BOPManuscript*.PolicyLossPayee.hasMatch(\policyLossPayee -> policyLossPayee == manuscript)
        if (this.BOPManuscriptEndorsement_TDICExists and isPolicyLossPayeeExists) {
          aiList.add(manuscript)
        }
      }
      return aiList.toSet()
    }
  }

  property get MortgageesSchedule_TDIC() : Set<PolicyMortgagee_TDIC> {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.BOPBldgMortgagees.toSet()
    } else {
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var addedOrModifiedItems = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyMortgagee_TDIC)*.Bean
      var addlSched = this.BOPBldgMortgagees.where(\elt -> addedOrModifiedItems?.contains(elt))?.toSet()
      /*Added Manuscript logic*/
      var addedOrModifiedItemsManuscript = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis BOPManuscript_TDIC)*.Bean
      var aiList=new ArrayList<PolicyMortgagee_TDIC>()
      aiList.addAll(addlSched?.toList())
      for (ai in addedOrModifiedItemsManuscript){
        var manuscript= (ai as BOPManuscript_TDIC).PolicyMortgagee
        /* Logic to add PolicyMortgagee only for the building which has manuscript endorsement. */
        var isPolMortageeExists = this.BOPManuscript*.PolicyMortgagee.hasMatch(\polMortagee -> polMortagee == manuscript)
        if (this.BOPManuscriptEndorsement_TDICExists and isPolMortageeExists) {
          aiList.add(manuscript)
        }
      }
      return aiList.toSet()
    }
  }

  property get ACVEndorsement_TDIC() : BOPACVEndCov_TDIC {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.BOPACVEndCov_TDIC
    } else {
      if(this.BOPACVEndCov_TDICExists){
        return this.BOPACVEndCov_TDIC
      }else if(this.BasedOn?.BOPACVEndCov_TDICExists){
        return this.BasedOn?.BOPACVEndCov_TDIC
      }else{
        return null
      }
    }
  }

  property get ACVExpirationDate_TDIC() : Date {
    if(this.Branch.Job typeis Cancellation and
        (!this.BasedOn.Branch.BOPLine.BOPLocations*.Buildings?.hasMatch(\elt -> elt.FixedId == this.FixedId and elt.BOPACVEndCov_TDICExists))) {
      return null
    }
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.Branch?.PeriodEnd
    } else {
      if(this.BOPACVEndCov_TDICExists){
        return this.Branch?.PeriodEnd
      }else if(this.BasedOn?.BOPACVEndCov_TDICExists){
        return this.Branch?.EditEffectiveDate
      }else{
        return null
      }
    }
  }

  property get BOPManuscriptSched_TDIC() : Set<BOPManuscript_TDIC> {
    if (not(this.Branch.Job typeis PolicyChange)) {
      return this.BOPManuscript.toSet()
    }
    else {
      var manuscriptItems : ArrayList<BOPManuscript_TDIC> = {}
      var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
      var addedOrModifiedItems = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis BOPManuscript_TDIC)*.Bean
      var addlSched = this.BOPManuscript.where(\elt -> addedOrModifiedItems?.contains(elt))?.toList()
      manuscriptItems.addAll(addlSched)

      AdditionalInsuredSchedule_TDIC?.each(\elt -> {
        var additionalInsuredManuscript = elt.PolicyAdditionalInsuredDetails*.BOPManuScript
        if (additionalInsuredManuscript.Count > 0) {

          additionalInsuredManuscript?.each(\elt2 -> {
            if (this.BOPManuscript?.hasMatch(\manuscript -> manuscript == elt2)) {
              manuscriptItems.add(elt2)
            }
          })
        }
      })

      LossPayeesSchedule_TDIC?.each(\elt -> {
        var lossPayeeManuscript = elt.BOPManuscript
        if (lossPayeeManuscript.Count > 0) {

          lossPayeeManuscript?.each(\elt2 -> {
            if (this.BOPManuscript?.hasMatch(\manuscript -> manuscript == elt2)) {
              manuscriptItems.add(elt2)
            }
          })
        }
      })

      MortgageesSchedule_TDIC?.each(\elt -> {
        var mortgageeManuscript = elt.BOPManuscript
        if (mortgageeManuscript.Count > 0) {

          mortgageeManuscript?.each(\elt2 -> {
            if (this.BOPManuscript?.hasMatch(\manuscript -> manuscript == elt2)) {
              manuscriptItems.add(elt2)
            }
          })
        }
      })

      return manuscriptItems?.toSet()
    }
  }


  property get EnhancedCovEndorsement_TDIC() : BOPEncCovEndtCond_TDIC {
    if (not(this.Branch.Job typeis PolicyChange)) {
      if(this.BOPEncCovEndtCond_TDICExists){
        return this.BOPEncCovEndtCond_TDIC
      }
      else if(this.BasedOn?.BOPEncCovEndtCond_TDICExists){
        return this.BasedOn?.BOPEncCovEndtCond_TDIC
      }
      return null
    } else {
      if(this.BOPEncCovEndtCond_TDICExists){
        return this.BOPEncCovEndtCond_TDIC
      }else if(this.BasedOn?.BOPEncCovEndtCond_TDICExists){
        return this.BasedOn?.BOPEncCovEndtCond_TDIC
      }else{
        return null
      }
    }
  }

  property get EnhancedCovExpirationDate_TDIC() : Date {
    if (not(this.Branch.Job typeis PolicyChange)) {
      if(this.BOPEncCovEndtCond_TDICExists and not(this.Branch.Job typeis Cancellation)){
        return this.Branch?.PeriodEnd
      }
      else if(this.BasedOn?.BOPEncCovEndtCond_TDICExists){
        return this.Branch.EditEffectiveDate
      }
      return null
    } else {
      if(this.BOPEncCovEndtCond_TDICExists){
        return this.Branch?.PeriodEnd
      }else if(this.BasedOn?.BOPEncCovEndtCond_TDICExists){
        return this.Branch?.EditEffectiveDate
      }else{
        return null
      }
    }
  }

}