package tdic.pc.config.enhancements.lob.bop

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.pl.currency.MonetaryAmount
uses typekey.Job

enhancement TDIC_BOPLocationEnhancement : BOPLocation {

  @Returns("Total Premium")
  property get LocationTotalPremium_TDIC() : MonetaryAmount {
    var coverageOrOtherPremiumMap = this.BOPLine.Costs.cast(BOPCost).toSet().byCoveragePremium()
    var locationCoveragePremiumMap = coverageOrOtherPremiumMap.get(true).byFixedLocation()
    var locationOtherPremiumMap = coverageOrOtherPremiumMap.get(false).byFixedLocation()
    var standardCost = locationCoveragePremiumMap.get(this).union(locationOtherPremiumMap.get(this)).getLocationStandardPremiums_TDIC(this.Branch.PreferredSettlementCurrency)
    var standardCostTotal = standardCost.where(\elt -> elt.Total != null)*.Total.sum()
    var taxAndSurcharges = locationCoveragePremiumMap.get(this).union(locationOtherPremiumMap.get(this)).TaxSurcharges.where(\elt -> elt.ActualAmountBilling != null)*.ActualAmountBilling.sum()
    return standardCostTotal + taxAndSurcharges
  }
  /* // Added bellow method to check condition for "closed end credit" change reason in building
  public function isCreditEndClosedistrue(): boolean {
    var rootNode = gw.diff.tree.DiffTree.recalculateRootNodeForPolicyReview(this.Branch)
    if (rootNode.NumChildren!=0) {
      var changedFlag1 = rootNode?.Children?.where(\elt -> (elt.Data as String)?.remove('"')?.equalsIgnoreCase("Modifiers"))
      var changedFlag2 = changedFlag1?.first()?.Children?.hasMatch(\elt1 -> (elt1.Data as String)?.remove('"')?.containsIgnoreCase("Closed End Credit"))
      return changedFlag2
    }
    return false
  }
  */
  property get LocationChanged_TDIC() : String {
    var changeBoolean : String = null
    if (this.Branch.Job.Subtype == Job.TC_POLICYCHANGE) {
      var rootNode = gw.diff.tree.DiffTree.recalculateRootNodeForPolicyReview(this.Branch)
      var numRootNode = rootNode.getNumChildren()  // to check the whether rootnode has value
      var polyInfoDiff = rootNode?.Children?.where(\elt -> (elt.Data as String)?.remove('"')?.equalsIgnoreCase("Policy Info"))  //Added
      var changedFlag : boolean = rootNode?.Children?.where(\elt -> (elt.Data as String)?.remove('"')?.equalsIgnoreCase("Locations and Buildings"))?.first()?.Children?.hasMatch(\elt1 -> (elt1.Data as String) != null and (elt1.Data as String)?.remove('"')?.split(":")[0] == this.Location.LocationNum?.toString())
      if (changedFlag) {
        changeBoolean = "true"
    //  } else if(isCreditEndClosedistrue()){     //added to check closed end credit
    //    changeBoolean = "true"                  //added
      }else if(polyInfoDiff?.first()?.Children?.hasMatch(\elt1 -> (elt1.Data as String)!=null and (elt1.Data as String)?.containsIgnoreCase("Additional Insured"))) {
        var changeList = this.Branch.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var addedOrModifiedItems = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsured)*.Bean
        var removedItems = this.Buildings*.AdditionalInsureds?.where(\elt -> elt.PolicyAdditionalInsuredDetails.first()?.ExpirationDate_TDIC != null and elt.PolicyAdditionalInsuredDetails.first()?.BasedOn.ExpirationDate_TDIC == null)
        var addlSched = this.Buildings*.AdditionalInsureds?.where(\elt -> addedOrModifiedItems?.contains(elt) || removedItems.contains(elt))?.toSet()
        var newOrChangedAddlInsDetail = changeList.where(\elt -> (elt typeis DiffAdd or elt typeis DiffProperty) and elt.Bean typeis PolicyAddlInsuredDetail)*.Bean
        var addlInsrdsSchedDetail = this.Buildings*.AdditionalInsureds?.where(\elt -> !newOrChangedAddlInsDetail?.intersect(elt.PolicyAdditionalInsuredDetails).Empty)?.toSet()

        if(addlSched?.size() > 0 or addlInsrdsSchedDetail?.size() >0 ){
          changeBoolean = "true"
        }else {
          changeBoolean = "false"
        }
      } else if(polyInfoDiff?.first()?.Children?.hasMatch(\elt1 ->  (elt1.Data as String)!=null and (elt1.Data as String)?.containsIgnoreCase("Policy Address"))) {       //added
        changeBoolean = "true"
      } else if(polyInfoDiff?.first()?.Children?.hasMatch(\elt1 ->  (elt1.Data as String)!=null and (elt1.Data as String)?.containsIgnoreCase("Multi Line Discount:"))) {   //added
        changeBoolean = "true"
      } else if(polyInfoDiff?.first()?.Children?.hasMatch(\elt1 ->  (elt1.Data as String)!=null and (elt1.Data as String)?.containsIgnoreCase("Primary Named Insured:"))) {   //added
        changeBoolean = "true"
      } else if(this.Branch.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
        changeBoolean = "true"
      } else if(this.Branch.PrimaryLocation.LocationNum!=this.Branch.BasedOn.PrimaryLocation.LocationNum and this.Location.PrimaryLoc) {
        changeBoolean = "true"
      } else {
        changeBoolean = "false"
      }
    }
    return changeBoolean
  }
}
