package tdic.pc.config.enhancements.lob.bop

enhancement TDIC_BOPPolicyAddlInsuredDetailEnhancement : PolicyAddlInsuredDetail {
  property get EffectiveDate_TDIC() : Date {
    var effDates = this.Branch?.AllEffectiveDates.toSet()
    for(effDate in effDates.order()) {
      var version = this.VersionList.AsOf(effDate)
      if(version != null) {
        return version.EffectiveDate
      }
    }
    return this.EffectiveDate
  }
}
