package tdic.pc.config.enhancements.workflow

enhancement AuditInformationWF_TDICEnhancement : entity.AuditInformationWF_TDIC {
  /**
   * Return the audit associated to the Audit Information.
  */
  public property get Audit_TDIC() : Audit {
    return this.AuditInformation_TDIC.Audit;
  }

  /**
   * Return the policy period associated to the Audit.
  */
  public property get PolicyPeriod_TDIC() : PolicyPeriod {
    return this.Audit_TDIC.PolicyPeriod;
  }
}
