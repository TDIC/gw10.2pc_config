package tdic.pc.config.enhancements.billing

uses gw.plugin.billing.BillingPeriodInfo
uses gw.pl.currency.MonetaryAmount
uses java.util.Date
uses gw.api.util.CurrencyUtil

/**
 * Created with IntelliJ IDEA.
 * User: RadhakrishnaS
 */
enhancement BillingPeriodInfoEnhancement : BillingPeriodInfo {

  /**
   * returns total paid amount for a given invoice date. Returns value only for the first record in the list of invoices.
  */
  public function totalPaidAmountByInvoiceDate(invStream : String, invDate : Date) : MonetaryAmount {

    var totalAmount = new MonetaryAmount(0, CurrencyUtil.getDefaultCurrency())

    var firstItemForDate = this?.Invoices?.where( \ inv -> inv.InvoiceDate == invDate).orderBy(\inv -> inv.InvoiceStream).first()
    if( invStream == firstItemForDate.InvoiceStream ) {
      var summedAmount = this?.Invoices?.where( \ inv -> inv.InvoiceDate == invDate)?.sum(\inv -> inv.Paid)
      if( summedAmount.toNumber() > 0 )
        totalAmount = summedAmount
    }

    return totalAmount
  }

}