package tdic.pc.config.enhancements.search.reinsurance

uses tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchResult

/**
 * US1377
 * Shane Sheridan 04/22/2015
 */
enhancement TDIC_ReinsuranceSearchResultEnhancement : TDIC_ReinsuranceSearchResult {

  /**
   * US1377
   * Shane Sheridan 04/22/2015
   *
   * Returns the total NumberOfEmployees (per matching location) for all policies associated to the ReinsuranceGroup.
   *
   * todo - find source of null pointer exception
   *
   */
  property get TotalEmployees_TDIC(): int {
    var totalEmployees: int
    for (aPolicyLocation in this.ReinsuranceGroup) {
      var a = aPolicyLocation.AssociatedPolicyPeriod.BOPLine.BOPLocations*.Buildings*.ConstructionType
      if (aPolicyLocation != null){
        totalEmployees += aPolicyLocation.AssociatedPolicyPeriod.WC7Line?.getTotalNumEmployeesForPolicyLocation_TDIC(aPolicyLocation)
      }
    }
    return totalEmployees
  }

  /**
   * US1377
   * Shane Sheridan 04/22/2015
   *
   * Returns the total Basis (per matching location) for all policies associated to the ReinsuranceGroup.
   */
  property get TotalBasis_TDIC(): int {
    var totalBasis: int
    for (aPolicyLocation in this.ReinsuranceGroup) {
      if (aPolicyLocation != null){
        totalBasis += aPolicyLocation.AssociatedPolicyPeriod.WC7Line?.getTotalBasisForPolicyLocation_TDIC(aPolicyLocation)
      }
    }
    return totalBasis
  }

  /**
   * US1378
   * ShaneS 05/12/2015
   */
  property get HasRelatedAddress() : boolean{
    if(this.RelatedAddressLine1 == null or this.RelatedCity == null or this.RelatedState == null or this.RelatedPostalCode == null){
      return false
    }

    return true
  }
}
