package tdic.pc.config.enhancements.user

uses gw.api.database.Query
uses gw.api.database.Relop
/**
 * US957
 * Shane Sheridan 02/20/2015
 *
 * This is an extension to the OOTB UserEnhancement for TDIC.
 */
enhancement TDIC_UserEnhancement : entity.User {

  /**
   * US957
   * Shane Sheridan 02/20/2015
   */
  property get AvailableVacationStatusOptions() : typekey.VacationStatusType[] {
    var availableOptions = typekey.VacationStatusType.TF_ONLYONEONVACATIONOPTION.TypeKeys
    return availableOptions.toTypedArray()
  }

  /**
   * This method returns true if user has TDIC Upload Documents role
   * @return boolean
   */
  function hasUploadDocumentsRole() : boolean {
    var role = Query.make(Role).compare(Role#Name, Relop.Equals,"TDIC Upload Documents").select()?.AtMostOneRow
    return this.hasRole(role)
  }
}
