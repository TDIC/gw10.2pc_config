package tdic.pc.config.enhancements.policy

/**
 * US469
 * Shane Sheridan 02/05/2015
 *
 * This enhancement contains functionality used when checking a PolicyPeriod for Escalation Reasons.
 */
enhancement TDIC_PolicyPeriodEscalationReasonCheckEnhancement : entity.PolicyPeriod {

  /**
   * US469
   * Shane Sheridan 01/30/2015
   *
   * This function returns TRUE if:
   *  a. Org type is Sole proprietorship, then if Primary Named Insured's CDA Membership status is Inactive.
   *  b. Org type is NOT Sole proprietorship, then if ALL Owner Officer [Person] Contacts, with greater than 0% ownership, on expiring policy CDA Membership statuses are Inactive.
   */
  @Returns("Boolean result indicating if Renewal should be escalated to underwriter because of inactive CDA Membersip.")
  property get HasInactiveCDAMembership_TDIC() : boolean{
    var inactiveCDAMembership = false

    if(this.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP){
      inactiveCDAMembership = isPrimaryNamedInsuredCdaStatusInactive_TDIC()
    }
    else if (this.PolicyOrgType_TDIC != typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP){
      inactiveCDAMembership = isAllPersonOwnerOfficerCdaStatusInactive_TDIC()
    }

    return inactiveCDAMembership
  }

  /**
   * US469
   * Shane Sheridan 01/30/2015
   *
   * This checks if the Primary Named Insured's CDA Membership status is Inactive, if it is Inactive then return true,
   * otherwise return false.
   */
  private function isPrimaryNamedInsuredCdaStatusInactive_TDIC() : boolean{
    var priNamedInsuredAsContact = this.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact

    if(priNamedInsuredAsContact.Subtype == typekey.Contact.TC_PERSON){
      var isInactive = (priNamedInsuredAsContact as Person).CDAMembershipStatus_TDIC != typekey.GlobalStatus_TDIC.TC_ACTIVE
      if(isInactive){
        return true
      }
    }

    return false
  }

  /**
   * US469
   * Shane Sheridan 01/30/2015
   *
   * GW-1963 - KrishnaD - Update rule as per updated requirements
   * This checks if ALL Owner Officer Contacts, with greater or equal to 0% ownership, on expiring policy are Inactive, returning the boolean result.
   * Only Person contacts can have CDA Membership status, so only Person contacts are considered.
   * and all Entity owners have inactive membership status. Entity owners can have 0% ownership.
   */
  private function isAllPersonOwnerOfficerCdaStatusInactive_TDIC() : boolean {
    var ownerOfficerContacts = this.WC7Line.WC7PolicyOwnerOfficers

    if(ownerOfficerContacts.Count > 0){
      var inactiveCount = 0
      var personContactWithOwnershipCount = 0
      var inactiveEntityOwnerCount = 0
      var personEntityOwnerCount = 0

      for(ooc in ownerOfficerContacts){
         if(ooc.AccountContactRole.AccountContact.Contact.Subtype == typekey.Contact.TC_PERSON){
            // To include entity owners with 0% ownership also
            if(ooc.AccountContactRole.Subtype == typekey.AccountContactRole.TC_ENTITYOWNER_TDIC){
              // inactive entity owner
              personEntityOwnerCount++
              if((ooc.AccountContactRole.AccountContact.Contact as Person).CDAMembershipStatus_TDIC != typekey.GlobalStatus_TDIC.TC_ACTIVE){
                inactiveEntityOwnerCount++
              }

            } else {
              // non-entity owner with ownership
              if(ooc.WC7OwnershipPct > 0){
                personContactWithOwnershipCount++
                // non-entity owner without membership
                if((ooc.AccountContactRole.AccountContact.Contact as Person).CDAMembershipStatus_TDIC != typekey.GlobalStatus_TDIC.TC_ACTIVE){
                  inactiveCount++
                }

              }
            }
         }
      }
      var isPersonContacts = (personContactWithOwnershipCount != 0)
      var isPersonWithoutMembership = (inactiveCount == personContactWithOwnershipCount)
      var isEntityOwnerWithoutMembership = (inactiveEntityOwnerCount == personEntityOwnerCount)
      //GW-2072 : Corrected the Active membership flag check process
      var isAllPersonContactInactiveMembership = (isPersonContacts and isPersonWithoutMembership) and isEntityOwnerWithoutMembership

      return isAllPersonContactInactiveMembership
    }

    return false
  }
  /*
  Inactive CDA Membership (All Primary Named Insured, owner/officer,  contacts with:
      > 0% ownership on expiring policy have inactive membership status

  and all Entity owners have inactive membership status. Entity owners can have 0% ownership.)
*/
}
