package tdic.pc.config.enhancements.policy

uses java.lang.Integer

enhancement FormEnhancement : entity.Form {
  /**
   * Form priority.
   */
  public property get Priority_TDIC() : Integer {
    var formPattern = FormPattern.getByCode(this.FormPatternCode);
    return formPattern.Priority;
  }

  /**
   * Was form Added?
   */
  public property get FormAdded_TDIC() : boolean {
    var editEffectiveDate = this.Branch.EditEffectiveDate;
    var basedOnPeriod = this.Branch.BasedOn;
    return this.BasedOn == null
       and (basedOnPeriod == null or basedOnPeriod.isFormEffective_TDIC(this.FormPatternCode, editEffectiveDate) == false);
  }

  /**
   * Was form Updated?
   */
  public property get FormUpdated_TDIC() : boolean {
    var editEffectiveDate = this.Branch.EditEffectiveDate;
    var basedOnPeriod = this.Branch.BasedOn;
    return this.BasedOn == null
       and basedOnPeriod != null
       and basedOnPeriod.isFormEffective_TDIC(this.FormPatternCode, editEffectiveDate) == true;
  }

  /**
   * Was form Removed?
   */
  public property get FormRemoved_TDIC() : boolean {
    var editEffectiveDate = this.Branch.EditEffectiveDate;
    var basedOnPeriod = this.Branch.BasedOn;
    return this.FormRemovalDate == editEffectiveDate
       and this.Branch.isFormEffective_TDIC(this.FormPatternCode, editEffectiveDate) == false
       and basedOnPeriod.isFormEffective_TDIC(this.FormPatternCode, editEffectiveDate) == true;
  }
}
