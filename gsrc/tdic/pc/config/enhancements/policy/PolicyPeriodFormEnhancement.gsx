package tdic.pc.config.enhancements.policy

uses java.util.Date

enhancement PolicyPeriodFormEnhancement : entity.PolicyPeriod {
  /**
   * Is the form effective on the specified date.
  */
  public function isFormEffective_TDIC (formPatternCode : String, date : Date) : boolean {
    // Subtract 1 second to handle renewals.
    if (date == this.PeriodEnd) {
      date = date.addSeconds(-1);
    }

    return this.Forms.hasMatch(\f -> f.FormPatternCode == formPatternCode
                                 and f.FormEffDate <= date
                                 and f.FormExpDate >  date);
  }
}
