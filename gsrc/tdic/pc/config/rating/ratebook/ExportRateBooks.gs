package tdic.pc.config.rating.ratebook

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.rating.rtm.domain.migration.RateBookXMLExportVisitor
uses gw.rating.rtm.domain.migration.RatingXmlWriter
uses gw.rating.rtm.domain.table.RateBookModel
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.io.FileOutputStream
uses java.io.IOException

/**
 * Class to run Export Ratebook Batch
 * This batch process automatically exports rstebook specified for different LOB's
 * Input the details in Script parameters to automatically export required ratebooks
 * on condition - Ratebook should be in Draft status
 *
 * @author : Ankita Gupta
 * @date : 08/05/2019
 */
class ExportRateBooks extends BatchProcessBase {
  private static var _expLogger = LoggerFactory.getLogger("DocumentExportRatebooksLog")
  construct() {
    super(BatchProcessType.TC_EXPORTRATEBOOKS_TDIC)
  }

  override function doWork() {
    var LOB = ScriptParameters.TDIC_RateBooksToBeExported?.split(",").toSet()
    var rateBookStatus = ScriptParameters.TDIC_ExportRateBooksWithStatus?.split(",").toSet()
    _expLogger.info("Start of doWork for Export RateBook")
    var isExportRateBookEnv = ScriptParameters.TDIC_RateBookEnvs.split(",").toSet().contains("gw.pc.env.rb")
    if (isExportRateBookEnv) {
      var rateBooksExp = new ArrayList<String>()
      var RBpath : String
      RBpath = ScriptParameters.TDIC_RatebookExportImportArchivePath

      var fxpath = new File(RBpath)
      CopyAndClearDirectory(fxpath)
      //Specify the folder you want your XML's to export to here.
      var rateBookVal = new gw.rating.rtm.RateBookUIValidator()
      var failedRateBooks = new StringBuffer()
      gw.transaction.Transaction.runWithNewBundle(\bd -> {
        var ratebooks = Query.make(RateBook).or( \orCriteria -> {
          orCriteria.compare("Status",Equals,typekey.RateBookStatus.TC_DRAFT)
          orCriteria.compare("Status",Equals,typekey.RateBookStatus.TC_ACTIVE)
          orCriteria.compare("Status",Equals,typekey.RateBookStatus.TC_STAGE)
        }).select().order()
        var draftRateBooks = Query.make(RateBook).compare("Status",Equals,typekey.RateBookStatus.TC_DRAFT).select().order()
        for (rb in ratebooks) {
          if (LOB.contains(rb.PolicyLine)){
            //If ratebook is in Draft status then first change status to Stage
            try{
              if (rb.Status == RateBookStatus.TC_DRAFT and rateBookStatus.contains(RateBookStatus.TC_DRAFT.Code)){
                _expLogger.info("Converting "+rb+" in Draft Status to Stage")
                var ratebookHelper = new gw.pcf.rating.ratebook.RateBookDetailsScreenUIHelper(rb)
                ratebookHelper.processInTx(\rbook -> rbook.changeStatusTo(RateBookStatus.TC_STAGE, true))
                _expLogger.info("RateBook: "+rb + " Staged successfully")
              }
            }catch (e : Exception){
              _expLogger.info("Error Changing the " + rb.BookName + " Rate Book Status from Draft to Staged " + e.Message)
              failedRateBooks.append(System.lineSeparator()).append(fileName(rb))
            }

           if((rateBookStatus.contains(RateBookStatus.TC_ACTIVE.Code) and rb.Status==RateBookStatus.TC_ACTIVE)
                or (rateBookStatus.contains(RateBookStatus.TC_STAGE.Code) and rb.Status==RateBookStatus.TC_STAGE)
                or (rateBookStatus.contains(RateBookStatus.TC_DRAFT.Code) and rb.Status==RateBookStatus.TC_DRAFT
               and draftRateBooks.hasMatch( \ ratebook -> ratebook == rb))){
              try {
                var fos = new FileOutputStream(fxpath.AbsolutePath + File.separator + fileName(rb))
                //Specify the array of Ratebooks you want to export here.
                rateBooksExp.add(fileName(rb))
                _expLogger.info("Starting Export of Ratebook " + fileName(rb))
                // Export the ratebook
                var exportVisitor = new RateBookXMLExportVisitor(new RatingXmlWriter())
                bd.add(rb)
                new RateBookModel(rb).accept(exportVisitor)
                fos.write(exportVisitor.exportBytes())
                _expLogger.info("Completed Export of RB " + fileName(rb))
                fos.close()
              } catch (e : Exception) {
                _expLogger.info("Error exporting " + rb.BookName +" " + e.Message)
                failedRateBooks.append(System.lineSeparator()).append(fileName(rb))
              }
            }
          }
       }
      })
      //Sending mail if RateBooks are exported successfully
      var expRateBooks = new StringBuffer()
      if (rateBooksExp.Count > 0){
        for (rateBook in rateBooksExp) {
          expRateBooks.append(System.lineSeparator())
          expRateBooks.append(rateBook)
        }
      }
      var successMessage = "The following ratebooks are successfully Exported" + expRateBooks.toString()
      var failedMessage = "Ratebook Export failed for:" + failedRateBooks.toString()
      var messageDetails = ((expRateBooks.length() == 0)? "" : successMessage + "\n") + (failedRateBooks.length() == 0 ? "" : "\n" + failedMessage)
      _expLogger.info(messageDetails)
    } else {
      _expLogger.info("Export FAILED:Export cannot be done in current environment")
    }
    _expLogger.info("End of doWork for Export RateBook")
  }

  /**
   * Function return ratebook name which has to be exported
   */
  function fileName(book: RateBook): String {
    if(book.PolicyLine=="GLLine"){
      return book.BookName+" - "+book.BookEdition+".xml"
    }
    var ratebookName = new StringBuilder()
    var rateBookNameWordsList = book.BookName?.split(" ").toList()
    for (word in rateBookNameWordsList) {
      ratebookName.append(word).append("+")

    }
    return ratebookName.toString() + "-+" + book.BookEdition + ".xml"
  }

  /**
   * Function to archive existing exported ratebooks.
   * @param src : File - list of previously exported ratebooks
   */
  function CopyAndClearDirectory(src:File){

    var destPath :  String
    destPath = ScriptParameters.TDIC_RatebookExportImportArchivePath
    var dest = new File(destPath)
    try{
      FileUtils.cleanDirectory(dest)
      FileUtils.copyDirectoryToDirectory(src ,dest)
      FileUtils.cleanDirectory(src)
    }catch(e:IOException){
      _expLogger.info("Unable to Copy folder from " + src + " to " + dest + "\n" + e.Message)
    }
  }
}