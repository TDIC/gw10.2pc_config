package tdic.pc.config.rating.ratebook

uses gw.api.database.Query
uses gw.api.system.PCDependenciesGateway
uses gw.api.system.PCLoggerCategory
uses gw.api.webservice.importTools.ImportToolsImpl

uses java.io.File
uses java.util.Map
uses java.lang.Integer

uses gw.api.upgrade.Coercions

/**
 * Class is used to greatly simplify ratebook maintenance during development.
 * Based upon the script paramter/flag, it will clean and automatically reload the
 * ratebooks in the content directory, as indicated in the Map result of getRatebookList.
 *
 * @author - Ankita Gupta
 */
class RateBookLoader {


  static var _dir = "/config/content/ratebooks"
  static var _log = PCLoggerCategory.RATING
  construct() {
  }


  public static function Start() {

    if (ScriptParameters.TDIC_AutomatedRateBookLoader and (not PCDependenciesGateway.ServerMode.Production)) {

      clearRateBooks()    //clear Rate Books before import
      loadRateBooks()
    }
  }


  private static function getLoBs() : Map<String, List<String>> {
    return {
        "WorkersComp" -> {"default"},
        "ProfessionalLiability" -> {"CM", "Occ", "Cyber"},
        "CommercialProperty"  -> {"BOP", "LRP"}
    }
  }

  /**
   * Method to get list of ratebooks to be loaded
   *
   * @return Map of name and last change date of rateBook
   */
  private static function getRatebookList() : Map<Integer, Map<String, String>> {
    return {
        1 -> {"Name" -> "TDIC+Work+Comp+2010+-+CW+-+1.xml", "StatusDate" -> "2010-01-01 0:0:0.0"},
        2 -> {"Name" -> "TDIC+Work+Comp+2011+-+CA+-+1.xml", "StatusDate" -> "2011-01-01 0:0:0.0"},
        3 -> {"Name" -> "TDIC+Work+Comp+2011+-+CA+-+2.xml", "StatusDate" -> "2011-04-18 0:0:0.0"},
        4 -> {"Name" -> "TDIC+Work+Comp+2012+-+CA+-+1.xml", "StatusDate" -> "2012-01-01 0:0:0.0"},
        5 -> {"Name" -> "TDIC+Work+Comp+2013+-+CA+-+1.xml", "StatusDate" -> "2013-01-01 0:0:0.0"},
        6 -> {"Name" -> "TDIC+Work+Comp+2014+-+CA+-+1.xml", "StatusDate" -> "2014-01-01 0:0:0.0"},
        7 -> {"Name" -> "TDIC+Work+Comp+2015+-+CA+-+1.xml", "StatusDate" -> "2016-01-26 17:44:58.0"},
        8 -> {"Name" -> "TDIC+Work+Comp+2016+-+CA+-+1.xml", "StatusDate" -> "2016-01-26 17:49:38.0"},
        9 -> {"Name" -> "TDIC+Work+Comp+2017+-+CA+-+1.xml", "StatusDate" -> "2016-10-31 22:21:17.0"},
        10 -> {"Name" -> "TDIC+Work+Comp+2018+-+CA+-+1.xml", "StatusDate" -> "2017-12-04 16:35:28.0"},
        11 -> {"Name" -> "TDIC+Work+Comp+2018+-+CA+-+2.xml", "StatusDate" -> "2018-01-15 13:39:22.0"},
        12 -> {"Name" -> "TDIC+Work+Comp+2019+-+CA+-+1.xml", "StatusDate" -> "2018-10-18 18:55:58.0"},
        13 -> {"Name" -> "TDIC+Work+Comp+2019+-+CA+-+2.xml", "StatusDate" -> "2019-02-14 18:06:51.0"},
        14 -> {"Name" -> "TDIC+Work+Comp+2020+-+CA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},


        15 -> {"Name" -> "TDIC+BOP+2020+-+CW+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        16 -> {"Name" -> "TDIC+BOP+2020+-+CA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        17 -> {"Name" -> "TDIC+BOP+2020+-+AZ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        18 -> {"Name" -> "TDIC+BOP+2020+-+HI+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        19 -> {"Name" -> "TDIC+BOP+2020+-+IL+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        20 -> {"Name" -> "TDIC+BOP+2020+-+MN+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        21 -> {"Name" -> "TDIC+BOP+2020+-+ND+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        22 -> {"Name" -> "TDIC+BOP+2020+-+NJ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        23 -> {"Name" -> "TDIC+BOP+2020+-+NV+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        24 -> {"Name" -> "TDIC+BOP+2020+-+PA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},

        25 -> {"Name" -> "TDIC+LRP+2020+-+CW+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        26 -> {"Name" -> "TDIC+LRP+2020+-+AZ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        27 -> {"Name" -> "TDIC+LRP+2020+-+CA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        28 -> {"Name" -> "TDIC+LRP+2020+-+HI+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        29 -> {"Name" -> "TDIC+LRP+2020+-+IL+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        30 -> {"Name" -> "TDIC+LRP+2020+-+MN+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        31 -> {"Name" -> "TDIC+LRP+2020+-+ND+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        32 -> {"Name" -> "TDIC+LRP+2020+-+NJ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        33 -> {"Name" -> "TDIC+LRP+2020+-+NV+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        34 -> {"Name" -> "TDIC+LRP+2020+-+PA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},

        35 -> {"Name" -> "TDIC+PL+CM+2020+-+CW+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        36 -> {"Name" -> "TDIC+PL+CM+2020+-+AZ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        37 -> {"Name" -> "TDIC+PL+CM+2020+-+AK+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        38 -> {"Name" -> "TDIC+PL+CM+2020+-+CA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        39 -> {"Name" -> "TDIC+PL+CM+2020+-+HI+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        40 -> {"Name" -> "TDIC+PL+CM+2020+-+IL+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        41 -> {"Name" -> "TDIC+PL+CM+2020+-+MN+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        42 -> {"Name" -> "TDIC+PL+CM+2020+-+ND+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        43 -> {"Name" -> "TDIC+PL+CM+2020+-+NJ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        44 -> {"Name" -> "TDIC+PL+CM+2020+-+NV+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        45 -> {"Name" -> "TDIC+PL+CM+2020+-+PA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},

        46 -> {"Name" -> "TDIC+PL+Cyber+2020+-+CW+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        47 -> {"Name" -> "TDIC+PL+Cyber+2020+-+AZ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        48 -> {"Name" -> "TDIC+PL+Cyber+2020+-+CA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        49 -> {"Name" -> "TDIC+PL+Cyber+2020+-+HI+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        50 -> {"Name" -> "TDIC+PL+Cyber+2020+-+IL+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        51 -> {"Name" -> "TDIC+PL+Cyber+2020+-+MN+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        52 -> {"Name" -> "TDIC+PL+Cyber+2020+-+ND+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        53 -> {"Name" -> "TDIC+PL+Cyber+2020+-+NJ+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        54 -> {"Name" -> "TDIC+PL+Cyber+2020+-+NV+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        55 -> {"Name" -> "TDIC+PL+Cyber+2020+-+PA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},

        56 -> {"Name" -> "TDIC+PL+Occurrence+2020+-+CW+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},
        57 -> {"Name" -> "TDIC+PL+Occurrence+2020+-+PA+-+1.xml", "StatusDate" -> "2020-01-01 0:0:0.0"},

        //GWPS-1824 PC Update PBL, BOP & LRP Rates for AZ
        58 -> {"Name" -> "TDIC+PL+CM+2020+-+AZ+-+2.xml", "StatusDate" -> "2020-07-21 7:42:20.0"},
        59 -> {"Name" -> "TDIC+LRP+2020+-+AZ+-+2.xml", "StatusDate" -> "2020-07-24 8:55:05.0"},
        60 -> {"Name" -> "TDIC+BOP+2020+-+AZ+-+2.xml", "StatusDate" -> "2020-07-24 8:50:32.0"},

        //GWPS-2307
        61 -> {"Name" -> "TDIC+PL+Cyber+2020+-+AZ+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        62 -> {"Name" -> "TDIC+PL+Cyber+2020+-+CA+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        63 -> {"Name" -> "TDIC+PL+Cyber+2020+-+HI+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        64 -> {"Name" -> "TDIC+PL+Cyber+2020+-+IL+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        65 -> {"Name" -> "TDIC+PL+Cyber+2020+-+MN+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        66 -> {"Name" -> "TDIC+PL+Cyber+2020+-+ND+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        67 -> {"Name" -> "TDIC+PL+Cyber+2020+-+NJ+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        68 -> {"Name" -> "TDIC+PL+Cyber+2020+-+NV+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},
        69 -> {"Name" -> "TDIC+PL+Cyber+2020+-+PA+-+2.xml", "StatusDate" -> "2020-07-01 0:0:0.0"},

        //GWNW-1126 Update NW states (ID, MN, OR, TN and WA) rate table with R2 state rate table
        70 -> {"Name" -> "TDIC+BOP+2020+-+WA+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        71 -> {"Name" -> "TDIC+BOP+2020+-+TN+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        72 -> {"Name" -> "TDIC+BOP+2020+-+OR+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        73 -> {"Name" -> "TDIC+BOP+2020+-+ID+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        74 -> {"Name" -> "TDIC+LRP+2020+-+WA+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        75 -> {"Name" -> "TDIC+LRP+2020+-+TN+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        76 -> {"Name" -> "TDIC+LRP+2020+-+OR+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        77 -> {"Name" -> "TDIC+LRP+2020+-+ID+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        78 -> {"Name" -> "TDIC+PLCM+2020+-+ID+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        79 -> {"Name" -> "TDIC+PLCM+2020+-+WA+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        80 -> {"Name" -> "TDIC+PLCM+2020+-+TN+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        81 -> {"Name" -> "TDIC+PLCM+2020+-+OR+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        82 -> {"Name" -> "TDIC+PLCM+2020+-+MT+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        83 -> {"Name" -> "TDIC+PLCyber+2020+-+WA+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        84 -> {"Name" -> "TDIC+PLCyber+2020+-+TN+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        85 -> {"Name" -> "TDIC+PLCyber+2020+-+OR+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        86 -> {"Name" -> "TDIC+PLCyber+2020+-+ID+-+1.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        87 -> {"Name" -> "TDIC+PL+CM+2020+-+CW+-+2.xml", "StatusDate" -> "2020-07-27 0:0:0.0"},
        88 -> {"Name" -> "TDIC+BOP+2020+-+CW+-+2.xml", "StatusDate" -> "2020-07-27 0:0:0.0"}



    }
  }

  private static function loadRateBooks() {
    var configuration = gw.api.util.ConfigAccess.getModuleRoot("configuration")
    if (configuration.isDirectory()) {
      var lobs = getLoBs()
      //var lob = lobs.get(lobOrder)
      for(key in lobs.Keys) {
        var offerings = lobs.get(key)
        var keyDir = "/" + key
        for(offering in offerings) {
          var offeringDir = (offering != "default") ? (keyDir + "/" + offering) : keyDir
          var ratebooks = new File(configuration.getAbsolutePath() + _dir + offeringDir)
          var ratebooklist = getRatebookList()
          if (ratebooks.isDirectory()) {
            _log.info("Ratebooks importing from : ${ratebooks.AbsolutePath}")
            var rbFiles = ratebooks.listFiles().partition(\f -> f.Name)
            for (order in ratebooklist.Keys.min()..ratebooklist.Keys.max()) {
              var fileName = ratebooklist.get(order).get("Name")
              var file = rbFiles.get(fileName)?.first()
              if (file != null and file.isFile()) {

                try {
                  _log.info("Importing ratebook : ${fileName} - Start")
                  gw.transaction.Transaction.runWithNewBundle(\bundle -> {
                    new ImportToolsImpl().importXmlData(file.read())
                  }, "su")
                  _log.info("Importing ratebook : ${fileName} - Completed")
                } catch (t : Exception) {
                  _log.info("Error Importing ratebook ${fileName}" + " " + t.StackTrace)
                }

              }
            }
          }
        }
      }
      updateLastStatusChangedDate()
    } else {
      throw "Missing directory ${_dir}"
    }
  }

  //Clear ratebooks
  private static function clearRateBooks() {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var b = gw.transaction.Transaction.getCurrent()
      Query
          .make(RateBook)
          .select()
          .each(\item -> {
            item = b.add(item)
            item.remove()
          })

      Query
          .make(RateTableDefinition)
          .select()
          .each(\def -> {
            def = b.add(def)
            def.remove()
          })

      Query
          .make(CalcRoutineParameterSet)
          .select()
          .each(\def -> {
            def = b.add(def)
            def.remove()
          })

      Query
          .make(DefaultRateFactorRow)
          .select()
          .each(\def -> {
            def = b.add(def)
            def.remove()
          })

      Query
          .make(CalcRoutineDefinition)
          .select()
          .each(\def -> {
            def = b.add(def)
            def.remove()
          })
    }, "su")
  }

  /**
   * Update last Change Date for Ratebook
   */
  private static function updateLastStatusChangedDate() {
    var rateBookMap = getRatebookList().Values
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var rateBooks = Query.make(RateBook).select()
      for (rBook in rateBooks) {
        bundle.add(rBook)
        var fileName = "${rBook.BookName} - ${rBook.BookEdition}.xml".replaceAll(" ", "+")
        var rb = rateBookMap.firstWhere(\m -> m.get("Name") == fileName)
        if (rb != null and rb.get("StatusDate") != null) {
          rBook.LastStatusChangeDate = Coercions.makeDateFrom(rb.get("StatusDate"))
        }
      }
    }, "su")
  }

}
