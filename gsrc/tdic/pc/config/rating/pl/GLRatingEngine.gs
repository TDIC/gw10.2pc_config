package tdic.pc.config.rating.pl

uses gw.api.domain.financials.PCFinancialsLogger
uses gw.financials.Prorater
uses gw.job.RenewalProcess
uses gw.lob.gl.rating.GLAddnlInsdSchedCostData_TDIC
uses gw.lob.gl.rating.GLCostData
uses gw.lob.gl.rating.GLCovCostData_TDIC
uses gw.lob.gl.rating.GLCovExposureCostData
uses gw.lob.gl.rating.GLHistoricalCostData_TDIC
uses gw.lob.gl.rating.GLMinPremCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses gw.rating.AbstractRatingEngine
uses gw.rating.CostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.pl.params.PLRatingParams
uses tdic.pc.config.rating.pl.rater.GLClaimsMadeRater
uses tdic.pc.config.rating.pl.rater.GLCyberLiabilityRater
uses tdic.pc.config.rating.pl.rater.GLEREClaimsMadeRater
uses tdic.pc.config.rating.pl.rater.GLOccurrenceRater
uses tdic.pc.config.rating.pl.rater.GLRater
uses typekey.Job

uses java.math.BigDecimal
uses java.math.RoundingMode

/**
 * Rating engine, this is the starting point for Professional Liability rating.
 * package (tdic.rating.pl)
 *
 * @author- Ankita Gupta
 * 09/12/2019
 * .
 */
class GLRatingEngine extends AbstractRatingEngine<GLLine> {

  var _baseRatingDate : Date
  var _rateBook : RateBook as RateBook
  var _ratingLevel : RateBookStatus as RatingLevel
  var _costCache : Map<String, GLCostData>as readonly CostCache = {}
  var _isERERating : Boolean as readonly IsERERating
  var _ratingInfo : PLRatingParams as RatingInfo

  construct(GLLineArg : GLLine) {
    super(GLLineArg)
    // set the base Rating using the first policyperiod in the term.
    // this will be used for U/W lookup and other basic items
    // rating date by object will be set separately
    _baseRatingDate = GLLineArg.Branch.FirstPeriodInTerm.getReferenceDateForCurrentJob(GLLineArg.BaseState)

  }

  construct(line : GLLine, minimumRatingLevel : RateBookStatus) {
    super(line)
    _ratingLevel = minimumRatingLevel
    _rateBook = getRateBook(line.Branch.PeriodStart)
    _isERERating = isERERating()
    _ratingInfo = new PLRatingParams(this)
  }

  override protected function rateSlice(lineVersion : GLLine) {
    if (lineVersion.Branch.isCanceledSlice()) {
      return
    }
    assertSliceMode(lineVersion)
    var logMsg = "Rating ${lineVersion} ${lineVersion.SliceDate} version..."
    PCFinancialsLogger.logInfo(logMsg)

    getRater(lineVersion).rate()

    PCFinancialsLogger.logInfo(logMsg + "done")
  }

  /**
   * create by: AnkitaG
   *
   * @param GLLine
   * @description: Rate window to rate all taxes and discounts
   * @create time:  9/25/2019
   * @return: null
   */
  override protected function rateWindow(lineVersion : GLLine) {
    if (isERERating()) {
      var lineVersions = getVersionsOnDates(AllEffectiveDates).where(\l -> getSliceDate(l) >= Branch.EditEffectiveDate)
      new GLEREClaimsMadeRater(lineVersions.first(), this).rate()
    }
  }

  public static function checkIfDateInRange(inputDate : Date, startDate : Date, expirationDate : Date) : boolean {
    var dateInRange = false
    if (inputDate >= startDate and inputDate < expirationDate) {
      dateInRange = true
    } else {
      dateInRange = false
    }
    return dateInRange
  }

  public static function checkIfDateInRangeIncluded(inputDate : Date, startDate : Date, expirationDate : Date) : boolean {
    var dateInRange = false
    if (inputDate >= startDate and inputDate <= expirationDate) {
      dateInRange = true
    } else {
      dateInRange = false
    }
    return dateInRange
  }

  public static function checkIfDateInRangeNotIncluded(inputDate : Date, startDate : Date, expirationDate : Date) : boolean {
    var dateInRange = false
    if (inputDate > startDate and inputDate < expirationDate) {
      dateInRange = true
    } else {
      dateInRange = false
    }
    return dateInRange
  }

  function getNextSliceDate(start : Date) : Date {
    var nextSliceDate = getNextSliceDateAfter(start)
    if(IsERERating and nextSliceDate.afterOrEqual(Branch.EditEffectiveDate) and Branch?.BasedOn?.PeriodDisplayStatus != PolicyPeriodStatus.TC_EXPIRED.Code) {
      return Branch.CancellationDate
    }
    return nextSliceDate
  }

  /**
   * General Liability is rated on a 365-day term
   *
   * @return
   */
  override protected property get NumDaysInCoverageRatedTerm() : int {
    return Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(PolicyLine.EffectiveDate, PolicyLine.EffectiveDate.addYears(1))
  }

  override protected function existingSliceModeCosts() : Iterable<Cost> {
    return PolicyLine.Costs.where(\c -> c typeis GLCovExposureCost
        or c typeis GLCovCost
        or c typeis GLCovCost_TDIC
        or c typeis GLAddnlInsdSchedCost_TDIC
        or c typeis GLHistoricalCost_TDIC
        or c typeis GLStateCost
        or c typeis GLMinPremCost_TDIC)
  }

  /**
   * create by: AnkitaG
   *
   * @param c: CostData
   * @description: Used by the extractCostDatasFromExistingCosts method.
   * Must be implemented if that method is going to be called
   * @create time:  9/19/2019
   * @return: null
   */
  override protected function createCostDataForCost(c : Cost) : CostData {
    switch (typeof c) {
      case GLCovExposureCost:
        return new GLCovExposureCostData(c, RateCache)
      case GLCovCost_TDIC:
        return new GLCovCostData_TDIC(c, RateCache)
      case GLHistoricalCost_TDIC:
        return new GLHistoricalCostData_TDIC(c, RateCache)
      case GLStateCost:
        return new GLStateCostData(c, RateCache)
      case GLAddnlInsdSchedCost_TDIC:
        return new GLAddnlInsdSchedCostData_TDIC(c, RateCache)
      case GLMinPremCost_TDIC:
        return new GLMinPremCostData_TDIC(c, RateCache)
      default:
        throw "Unepxected cost type ${c.DisplayName}"
    }
  }

  /**
   * Method to execute rate routine to get premium for a given coverage
   *
   * @param routineName             : String - param to hold routine name
   * @param rateRoutineParameterMap : Map - map to hold params to be used routine
   * @param cost                    : CostData for holding premiums
   */
  public function execute(routineName : String, rateRoutineParameterMap : Map<CalcRoutineParamName, Object>, cost : GLCostData) {
    cost.NumDaysInRatedTerm = this.NumDaysInCoverageRatedTerm
    _rateBook.executeCalcRoutine(routineName, :costData = cost,
        :worksheetContainer = cost, :paramSet = rateRoutineParameterMap)
    if (cost.OverrideAmount != null) {
      cost.ActualBaseRate = 0
      cost.Basis = 0
      cost.ActualAdjRate = 0
      cost.ActualAmount = cost.OverrideAmountBilling
      cost.ActualTermAmount = cost.OverrideAmountBilling
    } else {
      cost.copyStandardColumnsToActualColumns()
    }
    addCost(cost)
  }

  /**
   * GL Rater initialization
   */
  private function getRater(lineVersion : GLLine) : GLRater {
    switch (lineVersion.Branch.Offering.CodeIdentifier) {
      case RatingConstants.plCyberOffering:
        return new GLCyberLiabilityRater(lineVersion, this)   //Cyber
      case RatingConstants.plCMOffering:
        return new GLClaimsMadeRater(lineVersion, this)       //Claims Made
      case RatingConstants.plOccurenceOffering:
        return new GLOccurrenceRater(lineVersion, this)       //Occurrence
      default:
        return null
    }
  }

  private function getRateBook(refDate : Date) : RateBook {
    return RateBook.selectRateBook(refDate, PolicyLine.Branch.RateAsOfDate, PolicyLine.PolicyLine.PatternCode,
        PolicyLine.BaseState, _ratingLevel, PolicyLine.Branch.JobProcess typeis RenewalProcess, PolicyLine.Branch.Offering.PublicID)
  }

  private property get RoundingLevel() : int {
    return Branch.Policy.Product.QuoteRoundingLevel
  }

  private property get RoundingMode() : RoundingMode {
    return Branch.Policy.Product.QuoteRoundingMode
  }

  public function getNumberDaysInCoverageRatedTerm() : int {
    return NumDaysInCoverageRatedTerm
  }


  function isERERating() : Boolean {
    return Branch.isERERating_TDIC
  }

  function getCancellationDate() : Date {
    if(Branch.isCanceled()) {
      return Branch.CancellationDate
    }
    return null
  }

  function addGLCost(cost : CostData) {
    addCost(cost)
  }

  function removeGLCost(cost : CostData) {
    removeCost(cost)
  }

  function getBranchEditEffectiveDate() : Date {
    if(IsERERating and Branch.PeriodStart.compareIgnoreTime(Branch.EditEffectiveDate) == 0) {
      return Branch.CancellationDate
    }
    return Branch.EditEffectiveDate
  }
}