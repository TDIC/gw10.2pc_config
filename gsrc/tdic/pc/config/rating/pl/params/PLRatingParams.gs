package tdic.pc.config.rating.pl.params

uses entity.GLSplitParameters_TDIC
uses gw.api.util.DateUtil
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.pl.GLRatingEngine
uses tdic.pc.config.rating.pl.GLRatingUtil

uses java.math.BigDecimal


/**
 * Rating Params, class to populate PL Rating params
 * package (tdic.rating.gl.params)
 *
 * @author- Ankita Gupta
 * 09/12/2019
 * .
 */
class PLRatingParams {

  private var _line : GLLine
  private var _branch : PolicyPeriod
  private var _exposure : GLExposure
  private var _ratingEngine : GLRatingEngine

  private static var currentStepYear = RatingConstants.currentStepYear

  var _effectiveDate : Date as EffectiveDate
  var _expirationDate : Date as ExpirationDate
  var _classCode : String as ClassCode
  var _territoryCode : String as TerritoryCode
  var _specialityCode : String as SpecialityCode
  var _discount : GLHistoricalDiscount_TDIC as Discount
  var _isNewDentist : boolean as IsNewDentist
  var _limit : String as Limit
  var _stepYear : String as StepYear
  var _prorationFactor : BigDecimal as ProrationFactor = 1
  var _prorationFactor1 : BigDecimal as ProrationFactor1 = 1
  var _prorationFactor2 : BigDecimal as ProrationFactor2 = 1
  var _policyProrationFactor : BigDecimal as PolicyProrationFactor = 1
  var _termProrationFactor : BigDecimal as TermProrationFactor = 1
  var _newGradProrationFactor : BigDecimal as NewGradProrationFactor = 1
  var _plcmAddnlInsuredBasis : BigDecimal as PLCMAddnlInsuredBasis = 0
  var _plOccAddnlInsuredBasis : BigDecimal as PLOccAddnlInsuredBasis = 0
  var _plcmDisabilityDiscountBasis : BigDecimal as PLCMDisabilityDiscountBasis = 0
  var _plOccDisabilityDiscountBasis : BigDecimal as PLOccDisabilityDiscountBasis = 0
  var _plcmPartTimeDiscountBasis : BigDecimal as PLCMPartTimeDiscountBasis = 0
  var _plOccPartTimeDiscountBasis : BigDecimal as PLOccPartTimeDiscountBasis = 0
  var _plcmMultiLineDiscountBasis : BigDecimal as PLCMMultiLineDiscountBasis = 0
  var _plOccMultiLineDiscountBasis : BigDecimal as PLOccMultiLineDiscountBasis = 0
  var _plcmRiskMgmtBasis : BigDecimal as PLCMRiskMgmtBasis = 0
  var _plOccRiskMgmtBasis : BigDecimal as PLOccRiskMgmtBasis = 0
  var _plNewToCompanyBasis : BigDecimal as PLNewToCompanyBasis = 0
  var _plNewDetinstBasis : BigDecimal as PLNewDentistBasis = 0
  var _dentistProfLiabLimit : BigDecimal as DentistProfLiabLimit
  var _aggLimit : BigDecimal as AggLimit
  var _isNewGradSplit : boolean as IsNewGradSplit = false
  var _newGradYear : int as NewGradYear = 0
  var _newGradSplitYear1 : int as NewGradSplitYear1 = 0
  var _newGradSplitYear2 : int as NewGradSplitYear2 = 0
  var _newGradSplitProrateFactor1 : BigDecimal as NewGradSplitProrateFactor1 = 1.00bd
  var _newGradSplitProrateFactor2 : BigDecimal as NewGradSplitProrateFactor2 = 1.00bd
  var _fixedExpense : BigDecimal as FixedExpense = 0
  var _deductibleBasis : BigDecimal as DeductibleBasis = 0
  var _deductiblePerClaimLimit : BigDecimal as DeductiblePerClaimLimit = 0
  var _schoolServiceEventCount : int as SchoolServiceEventCount = 0
  var _specialEventCount : int as SpecialEventCount = 0
  var _idrType : IdentityTheftRecType_TDIC as IDRType
  var _epliMinNumOfEmp : BigDecimal as EPLIMinNumOfEmployees
  var _epliClaimsInLast2Years : boolean as EPLIClaimsInLast2Years
  var _epliRMRequirements : boolean as EPLIRMRequirements
  var _epliLimits : BigDecimal as EPLILimits
  var _waiverOfConsesntBasis : BigDecimal as WaiverOfConsentBasis = 0
  var _nonMemberDiscountBasis : BigDecimal as NonMemberSurchage = 0
  var _igaSurchargeBasis : BigDecimal as IGASurchage = 0
  var _PLClaimsMadeEREBasis : BigDecimal as PLClaimsMadeEREBasis = 0
  var _isEREApplicable : boolean as ISEREApplicable = false
  var _cancelProrataFactor : BigDecimal as CancelProrataFactor = 1.0
  var _irpmDiscountBasis : BigDecimal as IRPMDiscountBasis = 0
  var _serviceMemberDiscountBasis : BigDecimal as ServiceMemberDiscountBasis = 0
  var _irpmFactor : BigDecimal as IRPMFactor = 0
  var _multiLineDiscount : MultiLineDiscount_TDIC as MultiLineDiscount
  var _cyberLimit : BigDecimal as CyberLimit
  var _minimumPremiumAdjustmentBasis : BigDecimal as MinimumPremiumAdjustmentBasis = 0

  construct() {
  }

  construct(ratingEngine : GLRatingEngine) {
    _ratingEngine = ratingEngine
    init()
  }

  public function init() {
    _line = _ratingEngine.PolicyLine
    _branch = _ratingEngine.Branch

    // 1. ClaimsMade/Occurence parameters
    if (_branch.Offering.CodeIdentifier == RatingConstants.plCMOffering or _branch.Offering.CodeIdentifier == RatingConstants.plOccurenceOffering) {
      _exposure = _line.Exposures.first()

      _classCode = _exposure.ClassCode_TDIC.DisplayName
      _specialityCode = _exposure.SpecialityCode_TDIC.Code
      _territoryCode = _exposure.GLTerritory_TDIC.TerritoryCode
      _stepYear = currentStepYear

      _newGradProrationFactor = ((_line.EffectiveDate.daysBetween(_line.ExpirationDate)) / 365)
      _discount = getCurrentPeriodDiscount()
      _isNewDentist = _line.GLNewDentistDiscount_TDICExists
      _multiLineDiscount = _branch.MultiLineDiscount_TDIC

      if (_line.GLDentistProfLiabCov_TDICExists) {
        if (_branch.Offering.CodeIdentifier == RatingConstants.plCMOffering) {
          _dentistProfLiabLimit = _line.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value
        } else {
          _dentistProfLiabLimit = _line.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value
        }
      }

      if (_line.GLAggLimit_TDICExists) {
        _aggLimit = _line.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Value
      }

      if (_line.GLNJDeductibleCov_TDICExists) {
        _deductiblePerClaimLimit = _line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value
      }


      // 2. Cyber parameters
    } else if (_branch.Offering.CodeIdentifier == RatingConstants.plCyberOffering) {
      _cyberLimit = (_line.GLCyberLiabCov_TDICExists) ? _line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value : null
    }

  }

  /**
   * @return
   */
  public function getCurrentPeriodDiscount() : GLHistoricalDiscount_TDIC {
    if (_line.GLFullTimeFacultyDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_FULLTIMEFACULTYMEMBER
    else if (_line.GLFullTimePostGradDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_FULLTIMEPOSTGRADUATE
    else if (_line.GLPartTimeDiscount_TDICExists and _line.GLPartTimeDiscount_TDIC.GLPTPercent_TDICTerm.Value == 1)
      return GLHistoricalDiscount_TDIC.TC_PARTTIME_16
    else if (_line.GLPartTimeDiscount_TDICExists and _line.GLPartTimeDiscount_TDIC.GLPTPercent_TDICTerm.Value == 2)
      return GLHistoricalDiscount_TDIC.TC_PARTTIME_20
    else if (_line.GLNewGradDiscount_TDICExists or _line.GLNewDentistDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_NEWGRADUATE
    else if (_line.GLTempDisDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_TEMPORARYDISABILITY
    else if (_line.GLCovExtDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_COVERAGEEXTENSION
    else
      return null
  }

  /**
   *
   */
  public function populateLineParameters() {
    var basedOnPeriod = _branch?.BasedOn
    if (basedOnPeriod != null and basedOnPeriod.Job.Subtype == TC_CANCELLATION) {
      var canceledDays = basedOnPeriod.GLLine.ExpirationDate.daysBetween(basedOnPeriod.CancellationDate)
      CancelProrataFactor = (canceledDays / _line.ExpirationDate.daysBetween(_line.EffectiveDate) as BigDecimal)
    }
  }

  /**
   * @param ratingParam
   */
  public function populateRoutineParams(ratingParam : GLSplitParameters_TDIC) {
    StepYear = ratingParam.splitYear
    ClassCode = ratingParam.ClassCode
    SpecialityCode = ratingParam.SpecialityCode
    TerritoryCode = ratingParam.TerritoryCode
    Discount = ratingParam.Discount
    EffectiveDate = ratingParam.EffDate
    ExpirationDate = ratingParam.ExpDate
  }

  /**
   *
   */
  public function populateEREParam() {
    if (_ratingEngine.IsERERating) {
      ISEREApplicable = true
      if (_ratingEngine.getCancellationDate() != null) {
        var numOfDaysCovered : BigDecimal = DateUtil.daysBetween(_line.EffectiveDate, _ratingEngine.getCancellationDate())
        CancelProrataFactor = numOfDaysCovered / (_ratingEngine.getNumberDaysInCoverageRatedTerm() as BigDecimal)
      }
    }
  }

  /**
   * @param effDate
   * @param expDate
   */
  public function populateNewDoctorInfoForCurrentTerm(effDate : Date, expDate : Date) {
    NewGradProrationFactor = (effDate.trimToMidnight().daysBetween(expDate.trimToMidnight()) as BigDecimal) / _ratingEngine.getNumberDaysInCoverageRatedTerm()
    populateNewDoctorInfo(effDate, expDate)
  }

  /**
   * @param effDate
   * @param expDate
   */
  public function populateNewDoctorInfoForHistoricalTerms(effDate : Date, expDate : Date) {
    NewGradProrationFactor = 1
    TermProrationFactor = (effDate.daysBetween(expDate) as BigDecimal) / (expDate.daysBetween(expDate.addYears(-1)))
    populateNewDoctorInfo(effDate, expDate)
  }

  /**
   * @param effDate
   * @param expDate
   */
  public function populateNewDoctorInfo(effDate : Date, expDate : Date) {
    if (_line.GLNewGradDiscount_TDICExists or _line.GLNewDentistDiscount_TDICExists or Discount.Code == GLHistoricalDiscount_TDIC.TC_NEWGRADUATE.Code) {
      IsNewGradSplit = false
      NewGradSplitYear1 = 0
      NewGradSplitYear2 = 0
      var queston = _branch.Policy.Product.getQuestionSetByCodeIdentifier(RatingConstants.glUnderwriting)?.getQuestionByCodeIdentifier(RatingConstants.newGradDiscEffDate)
      var newGradEffDate = _line.getAnswer(queston).DateAnswer
      if (newGradEffDate != null) {
        var newGradDateMap = GLRatingUtil.createNewGradSplitYearMap(newGradEffDate)
        for (newGrad in newGradDateMap.entrySet()) {
          NewGradYear = 0
          var newGradStartDate = newGrad.Key.Start.trimToMidnight()
          var newGradEndDate = newGrad.Key.End.trimToMidnight()
          if (effDate == newGradStartDate and expDate == newGradEndDate) {
            IsNewGradSplit = false
            NewGradYear = newGrad.Value
            break
          } else if (effDate >= newGradStartDate and expDate <= newGradEndDate) {
            IsNewGradSplit = true
            NewGradSplitYear1 = (newGrad.Value)
            NewGradSplitProrateFactor1 = (((expDate.daysBetween(effDate)) as BigDecimal) / _ratingEngine.getNumberDaysInCoverageRatedTerm())
            NewGradSplitYear2 = 0
            NewGradSplitProrateFactor2 = 0
            TermProrationFactor = 1
            break
          } else {
            if (newGradStartDate < effDate and _ratingEngine.checkIfDateInRangeIncluded(newGradEndDate, effDate, expDate)) {
              IsNewGradSplit = true
              NewGradSplitYear1 = (newGrad.Value)
              NewGradSplitProrateFactor1 = (((newGradEndDate.daysBetween(effDate)) as BigDecimal) / _ratingEngine.getNumberDaysInCoverageRatedTerm())
              NewGradSplitProrateFactor2 = (newGradEndDate.daysBetween(expDate)) as BigDecimal / _ratingEngine.getNumberDaysInCoverageRatedTerm()
              TermProrationFactor = 1
            } else if (_ratingEngine.checkIfDateInRange(newGradStartDate, effDate, expDate) and newGradEndDate >= expDate) {
              IsNewGradSplit = true
              NewGradSplitYear2 = (newGrad.Value)
              NewGradSplitProrateFactor1 = (((newGradStartDate.daysBetween(effDate)) as BigDecimal) / _ratingEngine.getNumberDaysInCoverageRatedTerm())
              NewGradSplitProrateFactor2 = (((newGradStartDate.daysBetween(expDate)) as BigDecimal) / _ratingEngine.getNumberDaysInCoverageRatedTerm())
              TermProrationFactor = 1
              break
            }
          }
        }
      }
    }
  }

  public function populateLiabilityParamsForERE(exp : GLExposure, cov : GLDentistProfLiabCov_TDIC) {
    ClassCode = exp.ClassCode_TDIC.DisplayName
    SpecialityCode = exp.SpecialityCode_TDIC.Code
    TerritoryCode = exp.GLTerritory_TDIC.TerritoryCode
    DentistProfLiabLimit = cov.GLDPLPerClaimLimit_TDICTerm.Value
  }

}