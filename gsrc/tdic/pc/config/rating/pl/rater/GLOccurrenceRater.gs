package tdic.pc.config.rating.pl.rater

uses gw.lob.gl.rating.GLAddnlInsdSchedCostData_TDIC
uses gw.lob.gl.rating.GLCovExposureCostData
uses gw.lob.gl.rating.GLMinPremCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.RatingUtil
uses tdic.pc.config.rating.pl.GLRatingEngine


class GLOccurrenceRater implements GLRater {

  private var _ratingEngine : GLRatingEngine as RatingEngine
  private var _line : GLLine as Line
  private var _branch : PolicyPeriod as Branch

  private construct() { /*do nothing*/ }

  construct(line : GLLine, ratingEngine : GLRatingEngine) {
    _ratingEngine = ratingEngine
    _line = line
    _branch = _ratingEngine.Branch
    initRatingInfo()
  }

  override function rate() {
    rateOccurence()
  }

  private function initRatingInfo() {
    RatingEngine.RatingInfo.populateNewDoctorInfoForCurrentTerm(Line.EffectiveDate.trimToMidnight(), Line.ExpirationDate.trimToMidnight())
    RatingEngine.RatingInfo.populateEREParam()
  }

  private function rateOccurence(){
    rateProfessionalLiability()
    ratePolicyLevelCoverages()
  }

  /*
  * create by: AnkitaG, BidishS, IndramaniP
  * @description: Function to rate all Policy level coverages
  * @create time:  11/22/2019
  * @param glLine
  * @return: null
  */
  private function ratePolicyLevelCoverages() {
    RatingEngine.RatingInfo.populateLineParameters()
    rateAddnlInsuredEndorsement()
    rateRiskManagement()
    rateMultiLineDiscount()
    rateIRPMDiscount()
    rateMinimumPremiumAdjustment()
    rateNewDentist()
    rateLineCoverages()
    rateServiceMemberDiscount()
    rateIGASurcharge()
  }

  /**
   * Line Coverages
   * @param ratingInfo
   */
  private function rateLineCoverages() {
    for (cov in Line.GLLineCoverages) {
      rateLineCoverage(cov)
    }
  }

  /**
   *
   */
  private function rateProfessionalLiability() {
    var exp = Line.Exposures.first()
    var cov = Line.GLDentistProfLiabCov_TDIC
    RatingEngine.RatingInfo.populateNewDoctorInfoForCurrentTerm(Line.EffectiveDate.trimToMidnight(), Line.ExpirationDate.trimToMidnight())
    var discount = RatingEngine.RatingInfo.getCurrentPeriodDiscount()
    var cost = new GLCovExposureCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate),
        Branch.getBaseState(), cov.FixedId, exp.FixedId, false, GLCostType_TDIC.TC_PROFESSIONALLIABILITY, null, null, discount)
    cost.init(Line)
    var map : Map<CalcRoutineParamName, Object> = {
        TC_POLICYLINE -> Line,
        TC_RATINGINFO -> RatingEngine.RatingInfo
    }
    RatingEngine.execute(RatingConstants.plOccurenceRoutine, map, cost)
    RatingEngine.CostCache.put(GLCostType_TDIC.TC_PROFESSIONALLIABILITY.Code, cost)
  }

  /*
  * create by: AnkitaG
  * @description: Please add description
  * @create time:  11/02/2019
  * @param null
  * @return:
  */
  private function rateIGASurcharge() {
    if (Line.Branch.BaseState == TC_NJ) {
      RatingEngine.RatingInfo.IGASurchage = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NJIGASURCHARGE_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccIGASurchargeRoutine, map, cost)
     /*
     * 02/26/2020 Britto S
     * Per BillingCenter requirement and downstream system impacts, changing to state specific code,
     * may need to revist and aggregate to one in upcoming relases, as it gets included in other states
     */
      if (Line.Branch.BaseState == Jurisdiction.TC_NJ) {
        cost.ChargePattern = ChargePattern.TC_NJIGASURCHARGE_TDIC
      } else {
        cost.ChargePattern = ChargePattern.TC_IGASURCHARGE_TDIC
      }
    }
  }

  private function rateAddnlInsuredEndorsement() {
    if (Line.GLAdditionalInsuredCov_TDICExists and Line.GLAdditionInsdSched_TDIC.Count > 0) {
      RatingEngine.RatingInfo.PLCMAddnlInsuredBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var additionlaInsuredSchedItems = Line.GLAdditionInsdSched_TDIC
      for (addInsured in additionlaInsuredSchedItems) {
        if (addInsured.LTExpirationDate == null or addInsured.LTExpirationDate.compareIgnoreTime(Branch.EditEffectiveDate) > 0) {
          var addnlInsuredExpDate = addInsured.LTExpirationDate != null ? addInsured.LTExpirationDate : RatingEngine.getNextSliceDate(Line.SliceDate)
          var cost = new GLAddnlInsdSchedCostData_TDIC(Line.SliceDate, addnlInsuredExpDate ,
              Line.Branch.BaseState, addInsured, GLAddnlInsuredCostType_TDIC.TC_ADDITIONALINSURED)
          cost.init(Line)
          var map : Map<CalcRoutineParamName, Object> = {
              TC_POLICYLINE -> Line,
              TC_RATINGINFO -> RatingEngine.RatingInfo
          }
          RatingEngine.execute(RatingConstants.plOccAddnlInsuredEndorsementRoutine, map, cost)
          RatingEngine.CostCache.put(addInsured.FixedId.toString(), cost)
        }
      }
    }
  }

  private function rateRiskManagement() {
    if (Line.GLRiskMgmtDiscount_TDICExists) {
      RatingEngine.RatingInfo.PLCMRiskMgmtBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_RISKMANAGEMENT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccRiskManagementRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_RISKMANAGEMENT_TDIC.Code, cost)
    }
  }

  private function rateMultiLineDiscount() {
    if (Line.Branch.MultiLineDiscount_TDIC != null and
        Line.Branch.MultiLineDiscount_TDIC != MultiLineDiscount_TDIC.TC_N) {
      RatingEngine.RatingInfo.PLCMMultiLineDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_MULTILINEDISCOUNT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccMultiLineDiscountRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_MULTILINEDISCOUNT_TDIC.Code, cost)
    }
  }

  private function rateIRPMDiscount() {
    if (Line.GLIRPMDiscount_TDICExists
        and (Line.Branch.BaseState == TC_NJ or Line.Branch.BaseState == TC_PA)) {
      RatingEngine.RatingInfo.IRPMDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      RatingEngine.RatingInfo.IRPMFactor = Line.GLModifiers?.firstWhere(\mod -> mod.Pattern.CodeIdentifier == "GLIRPMModifier_TDIC")?.RateFactorsSum
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_IRPMDISCOUNT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccIRPMRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_IRPMDISCOUNT_TDIC.Code, cost)
    }
  }

  /*
  * create by: AnkitaG
  * @description: Method to rate New To Company
  * @create time:  11/21/2019
  * @param null
  * @return:
  */
/*  private function rateNewToCompany(ratingInfo : PLRatingParams) {
    if (Line.GLNewToCompanyDiscount_TDICExists) {
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NEWTOCOMPANY_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> ratingInfo
      }
      cost.init(Line)
      ratingInfo.PLNewToCompanyBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      RatingEngine.execute(RatingConstants.plCMNewToCompanyRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NEWTOCOMPANY_TDIC.Code, cost)
    }
  }*/

  /*
  * create by: AnkitaG
  * @description: Method  to rate new Dentist Dsicount
  * @create time:  11/21/2019
  * @param null
  * @return:
  */
  private function rateNewDentist() {
    if (Line.GLNewDentistDiscount_TDICExists) {
      RatingEngine.RatingInfo.PLNewDentistBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NEWDENTIST_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccNewDentistRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NEWDENTIST_TDIC.Code, cost)
    }
  }

  /*
  * create by: AnkitaG
  * @description: Please add description
  * @create time:  11/22/2019
  * @param null
  * @return:
  */
  private function rateServiceMemberDiscount() {
    if (Line.GLServiceMembersCRADiscount_TDICExists) {
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_SERVICEMEMBERDISC_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.RatingInfo.ServiceMemberDiscountBasis = RatingUtil.calculateProratedAmount(Line, RatingEngine.CostCache.Values*.ActualTermAmount.sum())
      RatingEngine.execute(RatingConstants.plOccServiceMemberRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_SERVICEMEMBERDISC_TDIC.Code, cost)
    }
  }

  /*
  * create by: AnkitaG
  * @description: Function to rate coverages
  * @create time:  12/02/2019
  * @param line
  * @return: null
  */
  private function rateLineCoverage(cov : Coverage) {
    var routine : String
    var costType : GLStateCostType
    switch (typeof cov) {
      case GLSchoolServicesCov_TDIC:
        routine = RatingConstants.plOccSchoolServiceRoutine
        RatingEngine.RatingInfo.SchoolServiceEventCount = Line.GLSchoolServSched_TDIC?.where(\elt -> elt.CanrecComp and RatingEngine.checkIfDateInRangeIncluded(elt.EventEndDate, Line.EffectiveDate, Line.ExpirationDate)).Count
        costType = GLStateCostType.TC_SCHOOLSERVICES_TDIC
        break
      case GLSpecialEventCov_TDIC:
        routine = RatingConstants.plOccSpecialEvenRoutine
        RatingEngine.RatingInfo.SpecialEventCount = Line.GLSpecialEventSched_TDIC?.where(\elt -> !(elt.isOnsite()) and RatingEngine.checkIfDateInRangeIncluded(elt.EffectiveDate, Line.EffectiveDate, Line.ExpirationDate)).Count
        costType = GLStateCostType.TC_SPECIALEVENT_TDIC
        break
      case GLIDTheftREcoveryCov_TDIC:
        routine = RatingConstants.plOccIDRRoutine
        RatingEngine.RatingInfo.IDRType = cov.GLTRTypeTDICTerm.Value
        costType = GLStateCostType.TC_IDENTITYTHEFTREC_TDIC
        break
      case GLDentalEmpPracLiabCov_TDIC:
        routine = RatingConstants.plOccEPLIRoutine
        RatingEngine.RatingInfo.EPLIMinNumOfEmployees = (cov.GLDEPLEmpNumber_TDICTerm.Value) as int
        RatingEngine.RatingInfo.EPLIClaimsInLast2Years = cov.GLDEPLClaimslasttwoyears_TDICTerm.Value
        RatingEngine.RatingInfo.EPLIRMRequirements = cov.GLDEPLRishMgtReq_TDICTerm.Value
        RatingEngine.RatingInfo.EPLILimits = cov.GLDEPLLimit_TDICTerm.Value
        costType = GLStateCostType.TC_EPLI_TDIC
        break
    }
    if (routine != null) {
      var cost = new GLStateCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, costType, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      if (cost.StateCostType == GLStateCostType.TC_SPECIALEVENT_TDIC or cost.StateCostType == GLStateCostType.TC_SCHOOLSERVICES_TDIC) {
        cost.ProrationMethod = TC_FLAT
      }
      RatingEngine.execute(routine, map, cost)
      RatingEngine.CostCache.put(cost.StateCostType.Code, cost)
    }
  }

  /**
   * This methos checks if the total calculated premium is less than 100, if so then it sets the Minimum Premium as the
   * difference between the 100 and calculated premium
   */
  private function rateMinimumPremiumAdjustment() {
    if (Line.GLExposuresWM.hasMatch(\glExposure -> (glExposure.ClassCode_TDIC == ClassCode_TDIC.TC_01 or glExposure.ClassCode_TDIC == ClassCode_TDIC.TC_02))) {
      RatingEngine.RatingInfo.MinimumPremiumAdjustmentBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLMinPremCostData_TDIC(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLCostType_TDIC.TC_GLMINPREMIUMADJ)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plOccMinimumPremiumAdjustmentRoutine, map, cost)
      RatingEngine.CostCache.put(GLCostType_TDIC.TC_GLMINPREMIUMADJ.Code, cost)
    }
  }

}