package tdic.pc.config.rating.pl.rater

uses tdic.pc.config.rating.pl.GLRatingEngine

structure GLRater {
  property get Line() : GLLine
  property get RatingEngine() : GLRatingEngine

  function rate()
}