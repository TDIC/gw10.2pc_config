package tdic.pc.config.rating.pl.rater

uses gw.api.util.DateUtil
uses gw.lob.gl.rating.GLAddnlInsdSchedCostData_TDIC
uses gw.lob.gl.rating.GLCostData
uses gw.lob.gl.rating.GLCovExposureCostData
uses gw.lob.gl.rating.GLHistoricalCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.pl.GLRatingEngine

uses java.math.BigDecimal

class GLEREClaimsMadeRater implements GLRater {

  private var _ratingEngine : GLRatingEngine as RatingEngine
  private var _line : GLLine as Line
  private var _branch : PolicyPeriod as Branch
  private var _effectiveDate : Date as EREEffectiveDate
  private var _expirationDate : Date as EREExpirationDate
  private var _isShortTermPolicy : boolean as IsShortTermPolicy

  private construct() { /*do nothing*/ }

  construct(line : GLLine, ratingEngine : GLRatingEngine) {
    _ratingEngine = ratingEngine
    _line = line
    _branch = _ratingEngine.Branch
    _ratingEngine.CostCache.clear()
    setEffDates()
    initRatingInfo()
    _isShortTermPolicy = (Branch.PeriodStart.addYears(1).compareIgnoreTime(Branch.PeriodEnd)!=0)
    if(_isShortTermPolicy){
      RatingEngine.RatingInfo.PolicyProrationFactor = DateUtil.daysBetween(Branch.PeriodStart,Branch.PeriodEnd) / (_ratingEngine.getNumberDaysInCoverageRatedTerm() as BigDecimal)
    }
  }

  override function rate() {
    rateClaimsMadeForERE()
  }

  private function rateClaimsMadeForERE() {
    rateLiability()
    rateHistoricalParameters()
    ratePolicyLevelCoverages()
  }

  /**
   *
   */
  private function ratePolicyLevelCoverages() {
    rateAddnlInsuredEndorsement()
    rateRiskManagement()
    rateIRPMDiscount()
    rateNJPolicyCoverages()
    rateIGASurcharge()
    rateExtendedReportingEndorsement()
  }

  /**
   *
   */
  private function rateNJPolicyCoverages() {
    if (Line.Branch.BaseState == TC_NJ) {
      rateDeductible()
      rateWaiverConsent()
    }
  }

  /**
   * @param ratingInfo
   */
  private function rateLiability() {

    var boundPeriods : Set<PolicyPeriod> = {}
    var periodMap = Branch.Policy.BoundPeriods.where(\elt -> elt.PeriodStart.compareIgnoreTime(Branch.PeriodStart) >= 0).partition(\elt -> elt.EditEffectiveDate)
    periodMap.eachKeyAndValue(\k, val -> {
      if(val.Count > 1) {
        var latest = val.orderByDescending(\elt -> elt.UpdateTime).first()
        boundPeriods.add(latest)
      } else {
        boundPeriods.add(val.first())
      }
    })
    var lines = boundPeriods*.GLLine?.where(\elt -> elt.SliceDate < RatingEngine.getBranchEditEffectiveDate()).sortByDescending(\param -> param.SliceDate)
    var exposuresRated : Set<GLExposure> = {}
    for (line in lines index idx) {
      var exp = line.Exposures.first()
      var cov = line.GLDentistProfLiabCov_TDIC
      exposuresRated.add(exp)
      if (lines.Count > 1) {
        var numDaysCovered : BigDecimal = DateUtil.daysBetween(line.SliceDate, RatingEngine.getNextSliceDate(line.SliceDate))
        RatingEngine.RatingInfo.CancelProrataFactor = 1
        RatingEngine.RatingInfo.TermProrationFactor = numDaysCovered / (_ratingEngine.getNumberDaysInCoverageRatedTerm() as BigDecimal)
      } else {
        RatingEngine.RatingInfo.TermProrationFactor = 1
      }
      RatingEngine.RatingInfo.populateLiabilityParamsForERE(exp, cov)

      var discount = getDiscount(line)
      var cost = new GLCovExposureCostData(EREEffectiveDate, EREExpirationDate, Branch.BaseState, cov.FixedId, exp.FixedId, false, GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY, null, null, discount, idx, line.SliceDate)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      executeERE(RatingConstants.plClaimsMadeRoutine, map, cost)
      if(lines.Count == 1 or (lines.Count > 1 and not isDuplicateTermCost(cost, exposuresRated, idx))) {
        RatingEngine.CostCache.put(GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.Code + idx, cost)
      } else {
        RatingEngine.removeGLCost(cost)
      }
    }
  }


  /**
   *
   */
  private function rateHistoricalParameters() {
    var splitParameters = Line.GLSplitParameters_TDIC
    var cov = Line.GLDentistProfLiabCov_TDIC
    for (param in splitParameters) {
      if(param.splitYear == RatingConstants.currentStepYear and _isShortTermPolicy){
        RatingEngine.RatingInfo.CancelProrataFactor = 1
      }else{
        RatingEngine.RatingInfo.populateEREParam()
      }
      RatingEngine.RatingInfo.populateRoutineParams(param)
      RatingEngine.RatingInfo.populateNewDoctorInfoForHistoricalTerms(param.EffDate.trimToMidnight(), param.ExpDate.trimToMidnight())
      var cost = new GLHistoricalCostData_TDIC(EREEffectiveDate, EREExpirationDate, Branch.BaseState, cov.FixedId, param.FixedId, GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY)
      cost.init(Line)
      var paramMap : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      executeERE(RatingConstants.plClaimsMadeRoutine, paramMap, cost)
      RatingEngine.CostCache.put(param.DisplayName, cost)
    }
  }

  /**
   *
   */
  private function rateDeductible() {
    if (Line.GLNJDeductibleCov_TDICExists) {
      var cost = new GLStateCostData(EREEffectiveDate, EREExpirationDate, Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_ERE_NJDEDUCTIBLE_TDIC, null, null)
      RatingEngine.RatingInfo.DeductibleBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      executeERE(RatingConstants.plCMNJDeductibleRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_ERE_NJDEDUCTIBLE_TDIC.Code, cost)
    }
  }

  /**
   *
   */
  private function rateWaiverConsent() {
    if (Line.GLNJWaiverCov_TDICExists and Line.GLNJWaiverCov_TDIC.GLNJWaiverSelection_TDICTerm.OptionValue.Value == 1) {
      RatingEngine.RatingInfo.WaiverOfConsentBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(EREEffectiveDate, EREExpirationDate, Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_ERE_NJWAIVEROFCONSENT_TDIC, null, null)
      cost.init(Line)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      executeERE(RatingConstants.plCMWaiverOfConsentRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_ERE_NJWAIVEROFCONSENT_TDIC.DisplayName, cost)
    }
  }

  /**
   *
   */
  private function rateIGASurcharge() {
    if (Line.Branch.BaseState == TC_NJ) { //Only for NJ for now
      RatingEngine.RatingInfo.IGASurchage = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(EREEffectiveDate, EREExpirationDate, Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_ERE_NJIGASURCHARGE_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
     /*
     * Per BillingCenter requirement and downstream system impacts, changing to state specific code,
     * may need to revist and aggregate to one in upcoming relases, as it gets included in other states
     */
      if (Line.Branch.BaseState == Jurisdiction.TC_NJ) {
        cost.ChargePattern = ChargePattern.TC_NJIGASURCHARGE_TDIC
      } else {
        cost.ChargePattern = ChargePattern.TC_IGASURCHARGE_TDIC
      }
      cost.init(Line)
      executeERE(RatingConstants.plCMIGASurchargeRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NJIGASURCHARGE_TDIC.DisplayName, cost)
    }
  }

  /**
   *
   */
  private function rateAddnlInsuredEndorsement() {
    if (Line.GLAdditionalInsuredCov_TDICExists and Line.GLAdditionInsdSched_TDIC.Count > 0) {
      RatingEngine.RatingInfo.PLCMAddnlInsuredBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var additionlaInsuredSchedItems = Line.getSlice(EREEffectiveDate).GLAdditionInsdSched_TDIC
      for (addInsured in additionlaInsuredSchedItems) {
        if (addInsured.LTExpirationDate == null or addInsured.LTExpirationDate.compareIgnoreTime(Branch.EditEffectiveDate) > 0) {
          var cost = new GLAddnlInsdSchedCostData_TDIC(EREEffectiveDate, EREExpirationDate, Line.Branch.BaseState, addInsured, GLAddnlInsuredCostType_TDIC.TC_ERE_ADDITIONALINSURED)
          cost.init(Line)
          var map : Map<CalcRoutineParamName, Object> = {
              TC_POLICYLINE -> Line,
              TC_RATINGINFO -> RatingEngine.RatingInfo
          }
          executeERE(RatingConstants.plCMAddnlInsuredEndorsementRoutine, map, cost)
          RatingEngine.CostCache.put(addInsured.FixedId.toString(), cost)
        }
      }
    }
  }

  /**
   *
   */
  private function rateRiskManagement() {
    if (Line.GLRiskMgmtDiscount_TDICExists) {
      RatingEngine.RatingInfo.PLCMRiskMgmtBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(EREEffectiveDate, EREExpirationDate, Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_ERE_RISKMANAGEMENT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      executeERE(RatingConstants.plCMRiskManagementRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_ERE_RISKMANAGEMENT_TDIC.Code, cost)
    }
  }

  /**
   * @param ratingInfo
   */
  private function rateIRPMDiscount() {
    if (Line.GLIRPMDiscount_TDICExists
        and (Line.Branch.BaseState == TC_NJ or Line.Branch.BaseState == TC_PA)) {
      RatingEngine.RatingInfo.IRPMDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      RatingEngine.RatingInfo.IRPMFactor = Line.GLModifiers?.firstWhere(\mod -> mod.Pattern.CodeIdentifier == "GLIRPMModifier_TDIC")?.RateFactorsSum
      var cost = new GLStateCostData(EREEffectiveDate, EREExpirationDate, Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_ERE_IRPMDISCOUNT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      executeERE(RatingConstants.plCMIRPMRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_ERE_IRPMDISCOUNT_TDIC.Code, cost)
    }
  }

  /*
  * create by: AnkitaG
  * @description: Method to rate PLCM ERE coverage
  * @create time:  1/07/2019
  * @param line
  * @return:
  */

  private function rateExtendedReportingEndorsement() {
    if (Line.GLExtendedReportPeriodCov_TDICExists and not Line.GLExtendedReportPeriodCov_TDIC?.GLERPRated_TDICTerm?.Value) {
      var cov = Line.GLExtendedReportPeriodCov_TDIC
      if (cov != null) {
        var cost = new GLStateCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache,
            Line.Branch.BaseState, GLStateCostType.TC_ERE_PLCLAIMSMADE_TDIC, null, null)
        cost.init(Line)

        var map : Map<CalcRoutineParamName, Object> = {
            TC_POLICYLINE -> Line,
            TC_RATINGINFO -> RatingEngine.RatingInfo
        }
        var totalERECost = RatingEngine.CostCache.Values.ActualTermAmountSum
        RatingEngine.RatingInfo.PLClaimsMadeEREBasis = totalERECost
        executeERE(RatingConstants.plMCERERoutine, map, cost)
        cost.ChargePattern = ChargePattern.TC_EREPREMIUM_TDIC
      }
    }
  }

  /**
   * @return
   */
  private function initRatingInfo() {
    RatingEngine.RatingInfo.init()
    RatingEngine.RatingInfo.populateNewDoctorInfoForCurrentTerm(Line.EffectiveDate.trimToMidnight(), Line.ExpirationDate.trimToMidnight())
    RatingEngine.RatingInfo.populateEREParam()
  }

  /*
  * Function to execute ERE routines
   */
  public function executeERE(routineName : String, rateRoutineParameterMap : Map<CalcRoutineParamName, Object>, cost : GLCostData) {
    cost.NumDaysInRatedTerm = RatingEngine.getNumberDaysInCoverageRatedTerm()
    RatingEngine.RateBook.executeCalcRoutine(routineName, :costData = cost,
        :worksheetContainer = cost, :paramSet = rateRoutineParameterMap)
    cost.copyStandardColumnsToActualColumns()
    cost.StandardAmount = cost.StandardTermAmount
    cost.ActualAmount = cost.ActualTermAmount
    cost.ChargePattern = ChargePattern.TC_EREPREMIUM_TDIC
    RatingEngine.addGLCost(cost)
  }

  private function setEffDates() {
    _effectiveDate = Branch.EditEffectiveDate
    var cancellationDate = _ratingEngine.getCancellationDate()
    _expirationDate = cancellationDate == null ? Branch.PeriodEnd : cancellationDate
  }


  public function getDiscount(line : GLLine) : GLHistoricalDiscount_TDIC {
    if (line.GLFullTimeFacultyDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_FULLTIMEFACULTYMEMBER
    else if (line.GLFullTimePostGradDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_FULLTIMEPOSTGRADUATE
    else if (line.GLPartTimeDiscount_TDICExists and line.GLPartTimeDiscount_TDIC.GLPTPercent_TDICTerm.Value == 1)
      return GLHistoricalDiscount_TDIC.TC_PARTTIME_16
    else if (line.GLPartTimeDiscount_TDICExists and line.GLPartTimeDiscount_TDIC.GLPTPercent_TDICTerm.Value == 2)
      return GLHistoricalDiscount_TDIC.TC_PARTTIME_20
    else if (line.GLNewGradDiscount_TDICExists or line.GLNewDentistDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_NEWGRADUATE
    else if (line.GLTempDisDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_TEMPORARYDISABILITY
    else if (line.GLCovExtDiscount_TDICExists)
      return GLHistoricalDiscount_TDIC.TC_COVERAGEEXTENSION
    else
      return null
  }

  private function isDuplicateTermCost(cost : GLCostData, exposures : Set<GLExposure>, idx : Integer = 0) : Boolean {
    if(idx == 0) {
      return false
    }

    if(cost typeis GLCovExposureCostData) {
      for(i in 0..idx) {
        var existingCost = RatingEngine.CostCache.get(GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.Code + i) as GLCovExposureCostData
        var costExp = exposures.firstWhere(\elt -> elt.FixedId == cost.ExposureKey_TDIC)
        var existingCostExp = exposures.firstWhere(\elt -> elt.FixedId == existingCost.ExposureKey_TDIC)
        var match = cost.GLCostType_TDIC == existingCost.GLCostType_TDIC
            and cost.StandardTermAmount == existingCost.ActualTermAmount
            and cost.Discount_TDIC == existingCost.Discount_TDIC
            and costExp.SpecialityCode_TDIC.Code == existingCostExp.SpecialityCode_TDIC.Code
            and costExp.ClassCode_TDIC.Code == existingCostExp.ClassCode_TDIC.Code
            and costExp.GLTerritory_TDIC.TerritoryCode == existingCostExp.GLTerritory_TDIC.TerritoryCode
        if(match) {
          return true
        }
      }
    }
    return false
  }
}