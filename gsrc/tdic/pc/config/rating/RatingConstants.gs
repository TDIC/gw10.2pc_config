package tdic.pc.config.rating

/**
 * Rating Constants, class to store constants
 * package (tdic.rating)
 *
 * @author- Ankita Gupta
 * 11/08/2019
 * .
 */
class RatingConstants {

  // LOB - Professional Liability

  //Constants for routines
  public static var plClaimsMadeRoutine : String = "PLCMLiability_TDIC"
  public static var plCMFixedExpenseRoutine : String =  "PLCMFixedExpense_TDIC"
  public static var plOccurenceRoutine : String =  "PLOccLiability_TDIC"
  public static var plCyberLiabilityRoutine : String = "PLCyberLiability_TDIC"
  public static var plCyberERELiabilityRoutine : String = "PLCyberERELiability_TDIC"
  public static var plCMAddnlInsuredEndorsementRoutine : String = "PLCMAddnlInsured_TDIC"
  public static var plOccAddnlInsuredEndorsementRoutine : String = "PLOccAddnlInsured_TDIC"
  public static var plCMRiskManagementRoutine : String = "PLCMRiskManagement_TDIC"
  public static var plOccRiskManagementRoutine : String = "PLOccRiskManagement_TDIC"
  public static var plCMMultiLineDiscountRoutine : String = "PLCMMultiLineDiscount_TDIC"
  public static var plOccMultiLineDiscountRoutine : String = "PLOccMultiLineDiscount_TDIC"
  public static var plCMNJDeductibleRoutine : String = "PLCMNJDeductible_TDIC"
  public static var plCMNewToCompanyRoutine : String = "PLCMNewToCompany_TDIC"
  public static var currentStepYear : String = "1st Year"
  public static var plCMSchoolServiceRoutine : String =  "PLCMSchoolServices_TDIC"
  public static var plCMSpecialEvenRoutine : String =  "PLCMSpecialEvent_TDIC"
  public static var plOccSchoolServiceRoutine : String =  "PLOccSchoolServices_TDIC"
  public static var plOccSpecialEvenRoutine : String =  "PLOccSpecialEvent_TDIC"
  public static var plCMIDRRoutine : String =  "PLCMIDR_TDIC"
  public static var plCMEPLIRoutine : String =  "PLCMEPLI_TDIC"
  public static var plOccIDRRoutine : String =  "PLOccIDR_TDIC"
  public static var plOccEPLIRoutine : String =  "PLOccEPLI_TDIC"
  public static var plCMWaiverOfConsentRoutine : String = "PLCMNJWaiverOfConsent_TDIC"
  public static var plCMNonMemberSurchargeRoutine : String = "PLCMNJNonMemberSurcharge_TDIC"
  public static var plCMIGASurchargeRoutine : String = "PLCMIGASurcharge_TDIC"
  public static var plOccIGASurchargeRoutine : String = "PLOccIGASurcharge_TDIC"
  public static var plCMIRPMRoutine : String = "PLCMIRPMDiscount_TDIC"
  public static var plOccIRPMRoutine : String = "PLOccIRPMDiscount_TDIC"
  public static var plCMServiceMemberRoutine : String = "PLCMServiceMemberDiscount_TDIC"
  public static var plOccServiceMemberRoutine : String = "PLOccServiceMemberDiscount_TDIC"
  public static var plCyberIGARoutine : String = "PLCyberNJIGASurcharge_TDIC"
  public static var plCMNJCurrentPeriodRoutine : String = "PLCMNJLiability_TDIC"
  public static var plCMNewDentistRoutine : String =  "PLCMNewDentistDiscount_TDIC"
  public static var plOccNewDentistRoutine : String =  "PLOccNewDentistDiscount_TDIC"
  public static var plMCERERoutine : String = "PLCMERE_TDIC"
  public static var plCMMinimumPremiumAdjustmentRoutine : String = "PLCMMinimumPremiumAdjustment_TDIC"
  public static var plOccMinimumPremiumAdjustmentRoutine : String = "PLOccMinimumPremiumAdjustment_TDIC"

// Offering Code
  public static var plCyberOffering : String = "PLCyberLiab_TDIC"
  public static var plCMOffering : String =  "PLClaimsMade_TDIC"
  public static var plOccurenceOffering : String =  "PLOccurence_TDIC"

// coverage code
  public static var glUnderwriting :String = "GLUnderwriting"
  public static var newGradDiscEffDate :String ="NewGradDiscEffDate_TDIC"
//------------------------------------------------------------------------
  //LOB: Business Owners line

  public static var businessOwnersPolicy : String = "BOPBusinessOwnersPolicy_TDIC"


  //routine
  public static var bopFireFighterReliefSurchargeRoutine : String = "BOPFireFighterReliefSurcharge_TDIC"
  public static var lrpFireFighterReliefSurchargeRoutine : String = "LRPFireFighterReliefSurcharge_TDIC"
  public static var bopIGASurchargeRoutine : String = "BOPIGASurcharge_TDIC"
  public static var lrpIGASurchargeRoutine : String = "LRPIGASurcharge_TDIC"
  public static var bopFireSafetySurchargeRoutine : String = "BOPFireSafetySurcharge_TDIC"
  public static var lrpFireSafetySurchargeRoutine : String = "LRPFireSafetySurcharge_TDIC"
  public static var bopClosedEndWaterCreditRoutine : String = "BOPClosedEndWaterCredit_TDIC"
  public static var bopIRPMLiabilityRoutine : String = "BOPIRPMLiability_TDIC"
  public static var bopIRPMPropertyRoutine :String = "BOPIRPMProperty_TDIC"
  public static var lrpIRPMLiabilityRoutine : String = "LRPIRPMLiability_TDIC"
  public static var lrpIRPMPropertyRoutine : String = "LRPIRPMProperty_TDIC"
  public static var bopMinimumPremiumAdjustmentRoutine : String = "BOPMinimumPremiumAdjustment_TDIC"
  public static var lrpMinimumPremiumadjustmentRoutine : String = "LRPMinimumPremiumAdjustment_TDIC"
  public static var bopMultiLineDiscountLiabilityRoutine : String = "BOPMultiLineDiscountLiability_TDIC"
  public static var lrpMultiLineDiscountLiabilityRoutine : String = "LRPMultiLineDiscountLiability_TDIC"
  public static var bopMultiLineDiscountPropertyRoutine : String = "BOPMultiLineDiscountProperty_TDIC"
  public static var lrpMultiLineDiscountPropertyRoutine : String = "LRPMultiLineDiscountProperty_TDIC"
  public static var bopEquipBreakBuildingLimitRoutine : String = "BOPEquipmentBreakdownBuildingLimit_TDIC"
  public static var lrpEquipBreakBuildingLimitRoutine : String = "LRPEquipmentBreakdownBuildingLimit_TDIC"
  public static var bopEquipBreakBPPLimitRoutine : String = "BOPEquipmentBreakdownBPPLimit_TDIC"


  // coverage pattern
  public static var bopPersonalPropertyCoverage : String = "BOPPersonalPropCov"
  public static var bopBuildingCoverage : String = "BOPBuildingCov"
  public static var bopDentalGeneralLiability : String = "BOPDentalGenLiabilityCov_TDIC"
  public static var bopBuildingOwnersLiability : String = "BOPBuildingOwnersLiabCov_TDIC"
  public static var bopClosedEndCredit : String = "BOPClosedEndCredit_TDIC"
  public static var bopIRPMProperty : String = "BOPIRPMProperty_TDIC"
  public static var bopIRPMLiability : String =  "BOPIRPMLiab_TDIC"
  public static var bopMoneyAndSecurityCoverage : String = "BOPMoneySecCov_TDIC"
  public static var bopLossOfIncomeCoverage :String = "BOPLossofIncomeTDIC"
  public static var bopGoldAndPreciousMetalsCoverage : String = "BOPGoldPreMetals_TDIC"
  public static var bopValuablePapersCoverage : String = "BOPValuablePapersCov_TDIC"
  public static var bopAccRecievablesCoverage : String = "BOPAccReceivablesCov_TDIC"
  public static var bopFineArtsCoverage : String = "BOPFineArtsCov_TDIC"
  public static var bopExtraExpensecoverage : String = "BOPExtraExpenseCov_TDIC"
  public static var bopFungiCoverage : String = "BOPFungiCov_TDIC"
  public static var bopILMineSubsidenceCoverage : String = "BOPILMineSubCov_TDIC"
  public static var bopEmpDishonestyCoverage : String = "BOPEmpDisCov_TDIC"
  public static var bopSignsCoverage : String = "BOPSigns_TDIC"
  public static var bopEquipBreakDownCoverage : String = "BOPEquipBreakCov_TDIC"
  public static var bopMultiLineLiablityCoverage : String = "MultiLineLiabilityDiscount"
  public static var bopMultiLinePropertyCoverage : String = "MultiLinePropertyDiscount"
  public static var bopEnhancedCoverage : String = "BOP_Enhanced_Coverage_TDIC"
  public static var bcpWAStopGapEmployersLiabilityCoverage : String = "BOPWAStopGapEmployersLiabilityCoverage_TDIC"

  // GLHistorical CostType
  public static var glHistoricalCostType : String = "PL"


  //city
  public static var validFireFighterSurchargeCity : List<String> = {
      "Duluth",
      "St. Paul",
      "Saint Paul",
      "St Paul",
      "Minneapolis",
      "Rochester"
  }

  //Rate table Code
  public static var bopMultineDiscountTable : String = "BOPMultiLineDiscount_TDIC"

}

