package tdic.pc.config.rating.wc7.factory


uses com.google.common.base.Optional
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext
uses tdic.pc.config.rating.wc7.WC7ROCEntry

uses java.util.Map
uses java.util.HashMap

uses gw.api.domain.Clause
uses tdic.pc.config.rating.wc7.WC7StepData
uses tdic.pc.config.rating.wc7.properties.WC7CoveredEmployeeProperties
uses tdic.pc.config.rating.wc7.properties.WC7JurisdictionProperties
uses tdic.pc.config.rating.wc7.properties.WC7LineProperties

/**
 * Factory class for computine rating params for WC rating
 *
 * @author - Ankita Gupta
 */
class WC7ParamsFactory {

  var _createEligibilityParam = false
  var _rateFlowContext : WC7RateFlowContext
  var _lineVersion : WC7Line
  var _lineProperties : WC7LineProperties as LineProperties

  construct(rateFlowContext : WC7RateFlowContext) {
    _rateFlowContext = rateFlowContext
    _lineVersion = rateFlowContext.PolicyLine
    _lineProperties = new WC7LineProperties(rateFlowContext)
  }

  construct(book : RateBook, rateFlowContext : WC7RateFlowContext, createEligibilityParam : boolean) {
    _createEligibilityParam = createEligibilityParam
    _rateFlowContext = rateFlowContext
    _lineVersion = rateFlowContext.PolicyLine
    _lineProperties = new WC7LineProperties(rateFlowContext)
  }

  public function getParams(stepData : WC7StepData, entry : WC7ROCEntry) : Map<CalcRoutineParamName, Object> {
    return createParams(stepData.RateBook, stepData.OptionalCoverage, stepData.FocusBean, entry)
  }

  public function createParams(book : RateBook, clause : Optional<Clause>, focusObject : Object, rateOrderEntry : WC7ROCEntry) : Map<CalcRoutineParamName, Object> {

    var params = new HashMap<CalcRoutineParamName, Object>()
    if (focusObject typeis entity.WC7CoveredEmployee) {
      params.put(CalcRoutineParamName.TC_WC7COVEMPPROPERTIES_TDIC, new WC7CoveredEmployeeProperties(focusObject))
      params.put(CalcRoutineParamName.TC_WC7JURISDICTIONPROPERTIES_TDIC, null)
    } else if (focusObject typeis WC7Jurisdiction) {
      params.put(CalcRoutineParamName.TC_WC7JURISDICTIONPROPERTIES_TDIC, new WC7JurisdictionProperties(focusObject))
      params.put(CalcRoutineParamName.TC_WC7COVEMPPROPERTIES_TDIC, null)
    } else if (focusObject typeis WC7Modifier) {
      params.put(CalcRoutineParamName.TC_WC7JURISDICTIONPROPERTIES_TDIC, new WC7JurisdictionProperties(focusObject.WC7Jurisdiction))
      params.put(CalcRoutineParamName.TC_WC7COVEMPPROPERTIES_TDIC, null)
    } else if (focusObject typeis WC7WaiverOfSubro) {
      params.put(CalcRoutineParamName.TC_WC7JURISDICTIONPROPERTIES_TDIC, new WC7JurisdictionProperties(focusObject) )
      params.put(CalcRoutineParamName.TC_WC7COVEMPPROPERTIES_TDIC, null)
    }
    params.put(CalcRoutineParamName.TC_COVERAGE, clause.orNull())
    params.put(CalcRoutineParamName.TC_POLICYLINE, _lineVersion)
    params.put(CalcRoutineParamName.TC_WC7LINEPROPERTIES_TDIC, _lineProperties)

    return params
  }
}