package tdic.pc.config.rating.wc7

uses java.util.Date

uses typekey.Jurisdiction

uses java.lang.Object
uses java.lang.String

uses typekey.WC7PremiumLevelType

uses java.lang.Comparable
uses java.lang.IllegalArgumentException

class WC7PremiumKey implements Comparable<WC7PremiumKey> {

  var _type : typekey.WC7PremiumLevelType as readonly PremiumLevelType
  var _date : Date as readonly RatingPeriodStart
  var _jurisdiction : typekey.Jurisdiction as readonly Jurisdiction


  construct(theJurisdiction : Jurisdiction, theType : WC7PremiumLevelType, theDate : Date) {
    _type = theType
    _date = theDate
    _jurisdiction = theJurisdiction
    if (theType == null or theJurisdiction == null) {
      throw new IllegalArgumentException("Cannot create key without a state and premium level")
    }
  }

  override function hashCode() : int {
    var hash = 7
    hash = 31 * hash + (PremiumLevelType == null ? 0 : PremiumLevelType.hashCode())
    hash = 31 * hash + (RatingPeriodStart == null ? 0 : RatingPeriodStart.hashCode())
    hash = 31 * hash + (Jurisdiction == null ? 0 : Jurisdiction.hashCode())
    return hash
  }

  override function equals(obj : Object) : boolean {
    if (this === obj) {
      return true
    }
    if ((obj == null) || (typeof obj != WC7PremiumKey)) {
      return false
    }
    var typedObj = obj as WC7PremiumKey
    return typedObj.PremiumLevelType == this.PremiumLevelType
        and typedObj.RatingPeriodStart == RatingPeriodStart
        and typedObj.Jurisdiction == this.Jurisdiction
  }

  override function compareTo(key : WC7PremiumKey) : int {
    var sort = 0
    sort = this.PremiumLevelType.compareTo(key.PremiumLevelType)

    if (sort == 0) {
      sort = this.Jurisdiction.compareTo(key.Jurisdiction)
    }
    if (sort == 0) {
      if (this.RatingPeriodStart != null and key.RatingPeriodStart != null) {
        sort = this.RatingPeriodStart.compareTo(key.RatingPeriodStart)
      } else if (this.RatingPeriodStart == null and key.RatingPeriodStart == null) {
        sort = 0
      } else {
        sort = this.RatingPeriodStart == null ? 1 : -1
      }
    }
    return sort
  }

  override function toString() : String {
    var str = "${PremiumLevelType.Code} ${Jurisdiction.Code}"
    var dateStr = RatingPeriodStart == null ? "" : " " + RatingPeriodStart
    return str + dateStr
  }

  function withPremiumLevel(premiumLevel : WC7PremiumLevelType) : WC7PremiumKey {
    return new WC7PremiumKey(Jurisdiction, premiumLevel, RatingPeriodStart)
  }
}
