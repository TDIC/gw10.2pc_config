package tdic.pc.config.rating.wc7

uses gw.lob.wc7.rating.WC7CostData

uses java.util.ArrayList
uses java.math.BigDecimal

class WC7PremiumTotal {

  var _costs : List<WC7CostData>
  var _key : WC7PremiumKey
  var _priorTotal : WC7PremiumTotal

  construct(pKey : WC7PremiumKey, priorTotal : WC7PremiumTotal) {
    this(pKey.Jurisdiction, pKey.PremiumLevelType, pKey.RatingPeriodStart, priorTotal)
  }

  construct(juris : typekey.Jurisdiction, level : WC7PremiumLevelType, ratingPeriodStart : Date, priorTotal : WC7PremiumTotal) {
    _key = new WC7PremiumKey(juris, level, ratingPeriodStart)
    _costs = new ArrayList<WC7CostData>()
    _priorTotal = priorTotal
  }

  property get Key() : WC7PremiumKey {
    return _key
  }

  function priorTotal() : BigDecimal {
    var total : BigDecimal = 0bd
    if (_priorTotal != null) {
      total = _priorTotal.totalCost()
    }
    return total
  }

  function totalCost() : BigDecimal {
    var total = 0.00bd
    total = total + _costs.sum(\aCost -> aCost.ActualAmount)
    total += priorTotal()
    return total
  }

  function addCost(aCost : WC7CostData) {
    _costs.add(aCost)
  }

  property get Costs() : List<WC7CostData> {
    return new ArrayList<WC7CostData>(_costs)
  }

  property get PriorPremiumTotal() : WC7PremiumTotal {
    return _priorTotal
  }
}
