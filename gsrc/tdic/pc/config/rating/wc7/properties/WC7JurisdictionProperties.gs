package tdic.pc.config.rating.wc7.properties


/**
 * Properties class to hold params for
 * Jurisdiction for Workers compensation
 *
 * @author- Ankita Gupta
 * 06/7/2019
 */

class WC7JurisdictionProperties extends WC7LineProperties {

  var _jurisdiction : Jurisdiction as readonly Jurisdiction
  var  _classCode : String as ClassCode


  construct(juri : WC7Jurisdiction) {
    super(juri.WCLine)
    _jurisdiction = juri.Jurisdiction
  }
  construct(waiver : WC7WaiverOfSubro) {
    super(waiver.WCLine)
   _classCode = waiver.ClassCode.Code
  }


}
