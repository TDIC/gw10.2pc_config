package tdic.pc.config.rating.wc7.properties

/**
 * Properties class to hold params for
 * Covered Employees for Workers compensation
 *
 * @author- Ankita Gupta
 * 06/7/2019
 */

class WC7CoveredEmployeeProperties extends WC7JurisdictionProperties {
  var _covEmp : WC7CoveredEmployee
  var _basisForRating : Integer as readonly BasisForRating
  var _classCode : String as readonly ClassCode

  construct(coveredEmpBase : WC7CoveredEmployee) {
    super(coveredEmpBase.Jurisdiction)
    _covEmp=coveredEmpBase
    _basisForRating = _covEmp.BasisForRating
    _classCode = _covEmp.ClassCode.Code
  }
}