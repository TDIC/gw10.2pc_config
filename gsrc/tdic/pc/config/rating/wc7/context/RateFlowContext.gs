package tdic.pc.config.rating.wc7.context

uses java.util.Map
uses gw.rating.CostData
uses tdic.pc.config.rating.wc7.flow.RateFlowItem
uses java.util.Collections
uses java.util.List


class RateFlowContext <LINE extends PolicyLine>{
  var _costDataMap : Map<PolicyLine , List<CostData>> = {}
  var _rateFlowItems : List<RateFlowItem> = {}

  construct(costDataMap : Map<PolicyLine, List<CostData>>) {
    _costDataMap = costDataMap
  }

  public function getCostDatas(lineVersion: LINE) : List<CostData> {
    return _costDataMap.get(lineVersion) ?: {}
  }

  public function getAllCostDatas() : List<CostData> {

    return _costDataMap
        .values()
        .flatten()
        .toList()

  }

  public function getRateFlowItems() : List<RateFlowItem> {
    return Collections.unmodifiableList(_rateFlowItems)
  }

  public function addRateFlowItem(item : RateFlowItem) {
    _rateFlowItems.add(item)
  }
}