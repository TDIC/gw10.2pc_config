package tdic.pc.config.rating.wc7.context

uses gw.rating.CostData
uses tdic.pc.config.rating.wc7.context.RateRoutineContext
uses gw.rating.worksheet.domain.WorksheetEntryContainer
uses com.google.common.base.Optional
uses java.util.Map
uses gw.lob.wc7.rating.WC7RatingPeriod
uses tdic.pc.config.rating.wc7.WC7StepData
uses tdic.pc.config.rating.wc7.WC7ROCEntry


class WC7RateRoutineContext extends RateRoutineContext<WC7Line> {
  var _rateFlowContext : WC7RateFlowContext as readonly RateFlowContext
  var _rateBook : RateBook as readonly RateBook
  var _linversion : WC7WorkersCompLine as WC7WorkersCompLine
  var _ratingPeriod : WC7RatingPeriod as RatingPeriod
  var _stepData : WC7StepData as RatingStepData
  var _rocEntry : WC7ROCEntry as RateOrderEntry

  construct(
      rBook : RateBook,
      lineVersion : WC7Line,
      parmSet : Map<typekey.CalcRoutineParamName, Object>,
      cData : Optional<CostData>,
      wsEntryContainer : WorksheetEntryContainer,
      rtFlowContext : WC7RateFlowContext, 
      rPeriod : WC7RatingPeriod,
      stepData : WC7StepData,
      rocEntry : WC7ROCEntry){


    super(
        rtFlowContext.getAllCostDatas(),
        parmSet,
        lineVersion,
        cData.orNull(),
        wsEntryContainer)

    _rateFlowContext = rtFlowContext
    _rateBook = rBook
    _linversion = lineVersion
    _ratingPeriod = rPeriod
    _stepData = stepData
    _rocEntry = rocEntry
  }

}