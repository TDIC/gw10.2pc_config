package tdic.pc.config.rating.wc7.flow

uses gw.lob.wc7.rating.WC7CovEmpCostData
uses tdic.pc.config.rating.wc7.WC7ROCEntry
uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses tdic.pc.config.rating.wc7.WC7StepData
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext
uses tdic.pc.config.rating.wc7.context.WC7RateRoutineContext
uses tdic.pc.config.rating.wc7.factory.WC7CostDataFactory
uses tdic.pc.config.rating.wc7.factory.WC7ParamsFactory
uses gw.plugin.Plugins
uses gw.plugin.rateflow.IRateRoutinePlugin
uses gw.rating.CostData
uses gw.rating.NoCostWorksheetContainer
uses gw.rating.worksheet.domain.Worksheet
uses gw.rating.worksheet.domain.WorksheetEntryContainer
uses com.google.common.base.Optional
uses gw.lob.wc7.rating.WC7CostData
uses tdic.pc.config.rating.wc7.WC7PremiumKey
uses gw.lob.wc7.rating.WC7RatingPeriod
uses tdic.pc.config.rating.wc7.properties.WC7LineProperties

uses java.math.BigDecimal
uses java.math.RoundingMode

class WC7CovCostRateFlowItem extends WC7RateFlowItem
{
  protected var _rateOrderEntry: WC7ROCEntry
  protected var _stepData : WC7StepData
  protected var _order : int as readonly RatingOrder
  protected var _paramsFactory : WC7ParamsFactory
  protected var _costDataFactory : WC7CostDataFactory
  
  /**  Flag to bypass cache for specific instances */
  protected var _doNotCache: Boolean as DoNotCache = false

  construct(paramsFactory : WC7ParamsFactory, costDataFactory : WC7CostDataFactory, data : WC7StepData, rateOrderEntry : WC7ROCEntry, ord : int) {
    super(ord)
    _stepData = data
    _rateOrderEntry = rateOrderEntry
    _paramsFactory = paramsFactory
    _costDataFactory = costDataFactory
  }
  
  override function execute(context: WC7RateFlowContext): List<CostData<Cost, PolicyLine>> {
    var wsc : gw.rating.worksheet.domain.WorksheetEntryContainer

    var costData = Optional.of( _costDataFactory.createCostData(_stepData.RatingPeriod, _stepData.PolicyLineVersion, _rateOrderEntry.CostEntityName, _stepData.OptionalCoverage.orNull() as Coverage, _stepData.FocusBean, _rateOrderEntry.CostType, _rateOrderEntry.Order) )
    wsc = createOrGetWorksheetEntryContainer(costData)
    
    var params = _paramsFactory.getParams(_stepData, _rateOrderEntry)
    var routineContext = new WC7RateRoutineContext( _stepData.RateBook, _stepData.PolicyLineVersion, params, costData, wsc, context, _stepData.RatingPeriod, _stepData, _rateOrderEntry)
    params.put(CalcRoutineParamName.TC_ROUTINECONTEXT, routineContext)
    var ratingExtEligibility =evaluateRatingEligibility(context, costData.orNull(), wsc, params)
    var shouldRateCoverage = (ratingExtEligibility == RatingEligibility_TDIC.TC_ELIGIBLE)? true : false
    if ( !shouldRateCoverage ) {
      return {}
    }
    var cost = costData.get().getExistingCost(_stepData.PolicyLineVersion)
    _stepData.RateBook.executeCalcRoutine(_rateOrderEntry.RoutineCode, costData.orNull(), wsc, params)
    var overriddenCost = WC7RatingUtil.isCostOveridden(cost as WC7Cost)
    if (overriddenCost) {
        if(cost.OverrideAmount != null) {
          computeValuesFromCostOverrides(costData.get().getExistingCost(_stepData.PolicyLineVersion), costData.get())
        }
    } else {
      costData.get().copyStandardColumnsToActualColumns()
    }
    if (costData.Present) {
      costData.get().RateBook = _stepData.RateBook
      costData.get().SubjectToReporting = _rateOrderEntry.IsPremiumReporting
      if(!DoNotCache)
        context.updateCache(getKey(_stepData.RatingPeriod, _rateOrderEntry), (costData.get() as WC7CostData))
    }
    
    var result: List<CostData<Cost, PolicyLine>> = costData.Present ? {costData.get()} : {}
    return result
  }

  protected function createOrGetWorksheetEntryContainer(costData : Optional<CostData>) : WorksheetEntryContainer {
    if (costData.Present) {
      return costData.get()
    }
    return null
  }

  protected function addNonCostWorksheet(bean : EffDated, container : NoCostWorksheetContainer, desc : String, tag : String) {
    if (Plugins.get(IRateRoutinePlugin).worksheetsEnabledForLine("WC7Line")) {
      var worksheet = new Worksheet() { :Description = desc, :WorksheetEntries = container.WorksheetEntries }
      _stepData.PolicyLineVersion.Branch.addWorksheetFor(bean, worksheet, tag)
    }
  }

  protected function getKey(ratingPeriod : WC7RatingPeriod, rocEntry : WC7ROCEntry) : WC7PremiumKey {
    return new WC7PremiumKey(ratingPeriod.Jurisdiction.Jurisdiction, rocEntry.PremiumLevel, ratingPeriod.RatingStart)
  }

  protected function evaluateRatingEligibility(context : WC7RateFlowContext, costData : CostData<Cost, PolicyLine>,
                                               wsc : WorksheetEntryContainer, params : Map<CalcRoutineParamName,Object>) : typekey.RatingEligibility_TDIC {
    if(_rateOrderEntry.EligibilityRoutineCode != null) {
      _stepData.RateBook.executeCalcRoutine(_rateOrderEntry.EligibilityRoutineCode, costData, wsc, params)
      var lineProperties = params.get(CalcRoutineParamName.TC_WC7LINEPROPERTIES_TDIC) as WC7LineProperties
      if(lineProperties.RatingEligiblity != null) {
        return lineProperties.RatingEligiblity
      }
    }
    return typekey.RatingEligibility_TDIC.TC_ELIGIBLE
  }
  /**
   * create by: AnkitaG
   * @description: Function to override Term Amount
   * @create time: 2:35 PM 9/24/2019
    * @param cos
   * @param: costData
   * @return: null
   */

  private function computeValuesFromCostOverrides(cost : Cost, costData : CostData) {
      costData.Basis = 0
      costData.ActualBaseRate = 0
      costData.ActualAdjRate = 0
      costData.ActualTermAmount = cost.OverrideAmount
      costData.ActualAmount = cost.OverrideAmount
  }

  private function getRoundingLevel() : int {
    return _stepData.PolicyLineVersion.Branch.Policy.Product.QuoteRoundingLevel
  }

  private function getRoundingMode() : RoundingMode {
    return _stepData.PolicyLineVersion.Branch.Policy.Product.QuoteRoundingMode
  }
}