package tdic.pc.config.rating.wc7.flow

uses gw.rating.CostData
uses tdic.pc.config.rating.wc7.context.RateFlowContext
uses java.util.List


abstract class RateFlowItem <CTX extends RateFlowContext> {
  var _order : int as readonly Order
  construct(ord : int) {
    _order = ord
  }


  abstract function execute(context : CTX) : List<CostData>
}