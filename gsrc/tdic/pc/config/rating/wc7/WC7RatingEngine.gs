package tdic.pc.config.rating.wc7

uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext
uses tdic.pc.config.rating.wc7.factory.WC7RateFlowFactory
uses gw.rating.AbstractRatingEngine
uses gw.rating.CostData
uses tdic.pc.config.rating.wc7.flow.RateFlowItem
uses java.util.Map
uses java.lang.Exception
uses gw.api.util.Logger
uses gw.financials.Prorater
uses java.lang.System

uses gw.lob.wc7.rating.WC7CovEmpCostData
uses typekey.Job

/**
 * Rating engine, this is the starting point for LOB rating.
 * The majority of new code and modification will be done in this class
 * package (tdic.pc.config.rating.wc7) is closely tied to the platform and
 * the Basic Templates.
 *
 * @author- Ankita Gupta
 * 05/21/2019
 * .
 */


class WC7RatingEngine extends AbstractRatingEngine<productmodel.WC7Line> {


  static var log = Logger.forCategory("Application.Rating")

  var _ratingLevel : RateBookStatus as readonly RatingLevel
  var _rateFlowContext : WC7RateFlowContext
  var _ignoreErrors : boolean
  var _rateBook : RateBook as readonly RateBook


  /**
   * Constructs a new rating engine instance based around the particular line.
   *
   * @param line
   */
  construct(line : WC7Line, minimumRatingLevel : RateBookStatus, ignoreErrors : boolean = false) {
    super(line)
    _ratingLevel = minimumRatingLevel
    _ignoreErrors = ignoreErrors
    _rateFlowContext = new WC7RateFlowContext(this)

  }

  /**
   * Function call to prorate should consider leap years
   */
  property get NumDaysInCoverageRatedTerm() : int {
    return Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(PolicyLine.EffectiveDate, PolicyLine.EffectiveDate.addYears(1))
  }

  /**
   * Main entry point for each rating iteration.
   */
  override protected function rateOnly() : Map<PolicyLine, List<CostData>> {
    var clock = System.currentTimeMillis()
    try {
     var rateFlowFactory = new WC7RateFlowFactory(_rateFlowContext)
      var ratingItems = rateFlowFactory.createRatingItems()
      ratingItems.each(\item -> {
        safeExecute(item)
            .where(\eachCost -> filter(eachCost))
            .each(\cost -> {
              cost.StandardAmount = cost.StandardTermAmount
              cost.ActualAmount = cost.ActualTermAmount
              addCost(cost)
            })
      })
      return CostDataMap
    } finally {log.info("Rating performance (job number " + this.PolicyLine.Branch.Job.JobNumber +
          ") = " + (System.currentTimeMillis() - clock) + " ms.")
    }
  }

  /**
   * Exception wrapper to gracefully handle the execution step
   */

  private function safeExecute(item : RateFlowItem) : List<CostData> {
    try {
      return item.execute(_rateFlowContext)
    } catch (ex : Exception) {
      log.error("Error rating " + item.toString() + ": " + ex.Message)
      ex.printStackTrace()
      _rateFlowContext.addErrorMessage("Error rating " + item.toString() + ": " + ex.Message)
      return {}
    }
  }

  /**
   * Exclude certain empty or 0$ costs from display.
   */

  private function filter(cData : CostData) : Boolean {
    if (cData typeis WC7CovEmpCostData
        and ((this.PolicyLine.AssociatedPolicyPeriod.RefundCalcMethod == CalculationMethod.TC_FLAT)
        or this.PolicyLine.AssociatedPolicyPeriod.Job.Subtype.Code == Job.TC_AUDIT.Code)
        and cData.ActualTermAmount == 0bd) {
      return false
    }

    if (cData.ActualTermAmount == null)
      return false

    return true
  }

  property get WC7Line() : WC7Line {
    return PolicyLine
  }

}