package tdic.pc.config.rating

uses gw.financials.Prorater

uses java.math.BigDecimal
uses java.math.RoundingMode


/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 01/31/2020
 * Time: 7:59 PM
 * To change this template use File | Settings | File Templates.
 */
class RatingUtil {

 /*
* create by: AnkitaG
* @description: Method to populate PLCM ERE param
* @create time:  1/29/2019
* @param line, cost
* @return: actualAmount
*/
  public static function calculateProratedAmount(line : GLLine,termAmount : BigDecimal) : BigDecimal {
    var proratedAmount = 0.00bd
    var numDaysInCoverageRatedTerm= Prorater.forRounding(0, HALF_UP, ProrationMethod.TC_PRORATABYDAYS).financialDaysBetween(line.EffectiveDate, line.EffectiveDate.addYears(1))
    if(termAmount!=null) {
      var periodStartDate = line.Branch.StartOfRatedTerm
      var p = Prorater.forRounding(line.Branch.Policy.Product.QuoteRoundingLevel, line.Branch.Policy.Product.QuoteRoundingMode, TC_PRORATABYDAYS)
      var endDate = p.findEndOfRatedTerm(periodStartDate, numDaysInCoverageRatedTerm)
      proratedAmount = p.prorate(periodStartDate, endDate, line.EffectiveDate, line.ExpirationDate, termAmount)
    }
    return proratedAmount
  }

}