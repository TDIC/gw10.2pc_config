package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set
uses java.math.BigDecimal
uses java.lang.Exception
uses gw.api.system.PCLoggerCategory

class WC7Form_PN_04_99_03 extends WC7FormData {
  var _wc7Line : WC7WorkersCompLine;
  var logger = PCLoggerCategory.TDIC_INTEGRATION
  
  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Line = context.Period.WC7Line;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    if (_wc7Line.BasedOn == null) {
      throw new Exception ("WC7Form_PN_04_99_03.InferredByCurrentData called without a valid BasedOn version of the WC7WorkersCompLine line of business.  Verify that the most recent Form Pattern data has been loaded.");
    }
    var currentRate  = getGoverningClassRate(_wc7Line);
    var previousRate = getGoverningClassRate(_wc7Line.BasedOn);

    /**
    *GWPS-238: Checking the PreviousRate and CurrentRatte are not equal to zero
    */
    if(currentRate!=0 and previousRate!=0){
      return (currentRate >= 1.25 * previousRate);
    }
    return false;
  }

  /**
   * No form data.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
  }

  /**
  * Return the rate used for the governing class code.
   */
  protected function getGoverningClassRate (wc7Line : WC7WorkersCompLine) : BigDecimal {
    var rate : BigDecimal = 0;
    var governingClass = wc7Line.WC7GoverningClass;

    /**
    *GWPS-238: Return WCCosts when met the WC7CoveredEmployee'sClasscode is same as GoverningClassCode
    */
    var cost = wc7Line.WC7Costs.firstWhere(\c -> c typeis WC7CovEmpCost
                                             and c.WC7CoveredEmployee.ClassCode.Code == governingClass.Code);
    if (cost != null) {
      rate = cost.StandardBaseRate;
    }

    return rate;
  }
}