package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_03_05 extends WC7FormData {
  var _condition : WC7VoluntaryCompensationAndEmployersLiabilityCovCond;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _condition = context.Period.WC7Line.WC7VoluntaryCompensationAndEmployersLiabilityCovCond;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return (_condition != null);
  }

  /**
   * Form contains a schedule of Name and Title/Relationship.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var scheduledItemNode : XMLNode;
    var scheduledItemsNode = new XMLNode("ScheduledItems");
    contentNode.addChild(scheduledItemsNode);

    for (scheduledItem in _condition.WC7LineScheduleCondItems.orderBy(\c -> c.FixedId)) {
      scheduledItemNode = new XMLNode("ScheduledItem");
      scheduledItemNode.addChild(createTextNode("Name", scheduledItem.getFieldValue("StringCol1") as String));
      scheduledItemNode.addChild(createTextNode("TitleRelationship", scheduledItem.getFieldValue("TypeKeyCol2") as String));
      scheduledItemsNode.addChild(scheduledItemNode);
    }
  }
}