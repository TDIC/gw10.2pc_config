package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses java.util.Set
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.ArrayList
uses java.util.List

/**
 * US751
 * Shane Sheridan 11/27/2014
 *
 * This is the forms inference class for the WC_04_00_02 form pattern.
 * Form name: Extension of Information Page - Schedule of Named Insured
 */
class TDIC_WC7Form_WC_04_00_02 extends WC7FormData {

  var _period : PolicyPeriod
  var _additionalNamedInsureds : List<PolicyAddlNamedInsured>
  var _dbas : List<String>;
  var _partners : List<WC7PolicyOwnerOfficer>;
  var _ownerOfficers : List<WC7PolicyOwnerOfficer>;

  /**
   * US751
   * Shane Sheridan 11/26/2014
   *
   * See FormData.gs for abstract description of the use for this function.
   */
  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    // get the policy period
    _period = context.Period
    // get additional Named insureds - if any
    _additionalNamedInsureds = _period.PolicyContactRoles.whereTypeIs(PolicyAddlNamedInsured).toList()

    // Get list of DBA's
    _dbas = new ArrayList<String>();
    for (location in _period.PolicyLocations) {
      if (location.DBA_TDIC != null and _dbas.contains(location.DBA_TDIC) == false) {
        _dbas.add(location.DBA_TDIC);
      }
    }

    // Get list of Partners
    _partners = new ArrayList<WC7PolicyOwnerOfficer>();
    if (_period.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_PARTNERSHIP) {
      for (pcr in _period.PolicyContactRoles) {
        if (pcr typeis WC7PolicyOwnerOfficer) {
          if (pcr.WC7OwnershipPct > 0) {
            _partners.add(pcr);
          }
        }
      }
    }

    // Get list of Owner/Officers
    _ownerOfficers = new ArrayList<WC7PolicyOwnerOfficer>();
    if (_period.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_JOINTVENTURE) {
      for (pcr in _period.PolicyContactRoles) {
        if (pcr typeis WC7PolicyOwnerOfficer) {
          _ownerOfficers.add(pcr);
        }
      }
    }
  }

  /**
   * US751
   * Shane Sheridan 11/26/2014
   *
   * See FormData.gs for abstract description of the use for this function.
   */
  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return _additionalNamedInsureds.HasElements or _dbas.HasElements or
           _partners.HasElements or _ownerOfficers.HasElements;
  }

  /**
  * Schedule of Additional Named Insured Name / FEIN
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var namedInsuredNode : XMLNode;
    var namedInsuredsNode = new XMLNode("NamedInsureds");
    contentNode.addChild (namedInsuredsNode);

    for (namedInsured in _additionalNamedInsureds.orderBy(\ani -> ani.FixedId)) {
      namedInsuredNode = new XMLNode("NamedInsured");
      namedInsuredNode.addChild(createTextNode("NamedInsured", namedInsured.DisplayName));
      namedInsuredNode.addChild(createTextNode("FEIN", namedInsured.TaxID));
      namedInsuredsNode.addChild(namedInsuredNode);
    }

    for (dba in _dbas.sort()) {
      namedInsuredNode = new XMLNode("NamedInsured");
      namedInsuredNode.addChild(createTextNode("NamedInsured", "(dba) " + dba));
      namedInsuredsNode.addChild(namedInsuredNode);
    }

    for (partner in _partners.sortBy(\p -> p.FixedId)) {
      namedInsuredNode = new XMLNode("NamedInsured");
      namedInsuredNode.addChild(createTextNode("NamedInsured", partner + " (partner)"));
      namedInsuredNode.addChild(createTextNode("FEIN", partner.TaxID));
      namedInsuredsNode.addChild(namedInsuredNode);
    }

    for (ownerOfficer in _ownerOfficers.orderBy(\oo -> oo.FixedId)) {
      namedInsuredNode = new XMLNode("NamedInsured");
      namedInsuredNode.addChild(createTextNode("NamedInsured", ownerOfficer.DisplayName));
      namedInsuredNode.addChild(createTextNode("FEIN", ownerOfficer.TaxID));
      namedInsuredsNode.addChild(namedInsuredNode);
    }
  }
}