package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_03_99 extends WC7FormData {
  var _wc7Line : WC7WorkersCompLine;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Line = context.Period.WC7Line;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return _wc7Line.WC7CustomizedExcl_TDICExists
  }

  /**
   * Form contains a schedule of text paragraphs.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var paragraphNode : XMLNode;
    var paragraphsNode = new XMLNode("Paragraphs");
    contentNode.addChild(paragraphsNode);

    for (paragraph in _wc7Line.WC7CustomizedExcl_TDIC.WC7LineScheduleExclItems.orderBy(\sei -> sei.FixedId)) {
      paragraphNode = new XMLNode("Paragraph");
      paragraphNode.addChild(createTextNode("Text", paragraph.getFieldValue("StringCol5_TDIC") as String));
      paragraphsNode.addChild(paragraphNode);
    }
  }
}