package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_03_06 extends WC7FormData {
  var _waiver : WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _waiver = context.Period.WC7Line.WC7WaiverOfOurRightToRecoverFromOthersEndorsemCond;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return (_waiver != null);
  }

  /**
   * Form contains a schedule of Person or Organization and Job Description.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var specificWaiverNode : XMLNode;
    var specificWaiversNode = new XMLNode("SpecificWaivers");
    contentNode.addChild(specificWaiversNode);

    for (specificWaiver in _waiver.WCLine.WC7SpecificWaiversWM.orderBy(\w -> w.FixedId)) {
      specificWaiverNode = new XMLNode("SpecificWaiver");
      specificWaiverNode.addChild(createTextNode("PersonOrOrganization", specificWaiver.Description));
      specificWaiverNode.addChild(createTextNode("JobDescription", specificWaiver.Location as String));
      specificWaiversNode.addChild(specificWaiverNode);
    }
  }
}