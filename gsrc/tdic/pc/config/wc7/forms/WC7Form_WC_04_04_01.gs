package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_04_01 extends WC7FormData {
  var _wc7Line : WC7WorkersCompLine;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Line = context.Period.WC7Line;
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return _wc7Line.WC7TDICAnniversaryRatingDateEndorsementExists;
  }

  /**
   * Form contains a schedule of Anniversary Rating Dates.  Should only be one, but data model supports multiple.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var anniversaryRatingDateNode : XMLNode;
    var anniversaryRatingDatesNode = new XMLNode("AnniversaryRatingDates");
    contentNode.addChild(anniversaryRatingDatesNode);

    for (jurisdiction in _wc7Line.WC7Jurisdictions.orderBy(\j -> j.FixedId)) {
      anniversaryRatingDateNode = new XMLNode("AnniversaryRatingDate");
      anniversaryRatingDateNode.addChild(createTextNode("Jursidiction", jurisdiction.Jurisdiction.Code));
      anniversaryRatingDateNode.addChild(createTextNode("AnniversaryRatingDate",
                                                        jurisdiction.AnniversaryDate.formatDate(SHORT)));
      anniversaryRatingDatesNode.addChild(anniversaryRatingDateNode);
    }
  }
}