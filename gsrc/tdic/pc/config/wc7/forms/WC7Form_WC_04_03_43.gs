package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_03_43 extends WC7Form_AlternateCoverage {
  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Exclusion = context.Period.WC7Line.WC7DesignatedOperationsDesignatedLocationsExcl_TDIC;
  }

  /**
   * Form contains a schedule of Operations and Alternate Coverages.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    super.addDataForComparisonOrExport(contentNode);

    var designatedOperationNode : XMLNode;
    var designatedOperationsNode = new XMLNode("DesignatedOperations");
    contentNode.addChild(designatedOperationsNode);

    for (operation in _wc7Exclusion.WCLine.WC7DesignatedOperationDesignatedLocationExclOperations_TDIC.orderBy(\o -> o.FixedId)) {
      designatedOperationNode = new XMLNode("Operation");
      designatedOperationNode.addChild(createTextNode("Operation", operation.Operation));
      designatedOperationNode.addChild(createTextNode("StandardClassificationNumber", operation.StandardClassificationCode.Code));
      designatedOperationNode.addChild(createTextNode("AbbreviatedPhraseology", operation.StandardClassificationCode.ShortDesc));
      designatedOperationNode.addChild(createTextNode("LocationAddress", operation.LocationAddress));
      designatedOperationsNode.addChild(designatedOperationNode);
    }
  }
}