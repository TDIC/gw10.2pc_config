package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode
uses java.util.Set
uses gw.pl.currency.MonetaryAmount

class WC7Form_WC_04_00_05 extends WC7FormData {
  var _totalCost : MonetaryAmount;

  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    // TotalCost_RPT is not used because this is called before it is set (or updated).
    _totalCost = context.Period.AllCosts.sum(\c -> c.ActualAmount);
  }

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return true;
  }

  /**
   * Form contains a schedule of Anniversary Rating Dates.  Should only be one, but data model supports multiple.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    contentNode.addChild(createTextNode("Cost", _totalCost.DisplayValue));
  }
}