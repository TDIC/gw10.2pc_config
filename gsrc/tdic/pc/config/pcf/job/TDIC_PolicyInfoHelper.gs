package tdic.pc.config.pcf.job

uses gw.api.productmodel.ExclusionPattern
uses gw.api.upgrade.PCCoercions

/**
 * DE138
 * Shane Sheridan 02/26/2015
 *
 * Functionality for use by the Policy Info screen.
 */
class TDIC_PolicyInfoHelper {

  /**
   * DE138
   * Shane Sheridan 02/26/2015
  */
  static function isEffectiveDateValid(effectiveDate : java.util.Date, period : PolicyPeriod) : boolean {
    var currentDate = gw.api.util.DateUtil.currentDate()
    if (period.Job.Subtype == typekey.Job.TC_RENEWAL or currentDate.after(effectiveDate)){
      return true
    }
    return currentDate.addDays(60).compareIgnoreTime(effectiveDate) >= 0
  }

  /**
   * DE167, robk
   * The org. type for a policy is tied to the contact type of the Primary Named Insured on that Policy, so if the primary named insured changes reset the org. type for the policy
   */
  static function changePrimaryNamedInsuredTo_TDIC(aPolicyPeriod : PolicyPeriod, aContact : Contact) {
    aPolicyPeriod.changePrimaryNamedInsuredTo(aContact)
    if (aPolicyPeriod.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact typeis Person) {
      if(aPolicyPeriod.WC7LineExists) {
        aPolicyPeriod.PolicyOrgType_TDIC = typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP
      }
      onPolicyOrgTypeChange_TDIC(aPolicyPeriod)
    }
    else {
      aPolicyPeriod.PolicyOrgType_TDIC = null
    }
  }

  /**
   * DE167, robk
   * If the org. type changes for a policy then update the Owner/Officer exclusion if required.
   */
  static function onPolicyOrgTypeChange_TDIC(aPolicyPeriod : PolicyPeriod) {
    if (aPolicyPeriod.WC7LineExists) {
      var ooExclusionAvailable = aPolicyPeriod.WC7Line.partnersOfficersAndOthersExclusionEndorsementAvailability()
      if (ooExclusionAvailable and not aPolicyPeriod.WC7Line.WC7PartnersOfficersAndOthersExclEndorsementExclExists) {
        aPolicyPeriod.WC7Line.getOrCreateExclusion(PCCoercions.makeProductModel<ExclusionPattern>("WC7PartnersOfficersAndOthersExclEndorsementExcl"))
      }
      else if (not ooExclusionAvailable and aPolicyPeriod.WC7Line.WC7PartnersOfficersAndOthersExclEndorsementExclExists){
        for (anOO in aPolicyPeriod.WC7Line.WC7PolicyOwnerOfficers) {
          anOO.isExcluded_TDIC = false
        }
      }
    }
  }
  //GWPS-2165 Default OrgType to Individual for PL-CM/PL-OCC
  static function getDefaultOrganizationType(period : PolicyPeriod){
      if(period.Offering.CodeIdentifier == "PLOccurence_TDIC" or period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
        if(period.PolicyOrgType_TDIC != PolicyOrgType_TDIC.TC_INDIVIDUAL){
          period.PolicyOrgType_TDIC = PolicyOrgType_TDIC.TC_INDIVIDUAL
        }
      } else {
        period.PolicyOrgType_TDIC = null
      }
  }
}