package tdic.pc.config.pcf.activity

uses com.guidewire.pl.web.config.PCFTypeUtil
uses com.guidewire.pl.web.config.find.PCFElementFindUtil
uses com.guidewire.pl.web.iterator.IteratorOfTypeFindCriteria
uses com.guidewire.pl.web.iterator.IteratorWidget
uses entity.Activity
uses gw.api.assignment.AssignmentPopup
uses gw.api.assignment.UserAssignee
uses gw.api.database.PCBeanFinder
uses gw.api.system.PCLoggerCategory
uses pcf.api.Location

/**
 * BrittoS 04/03/2020
 * Utility for PC activities and assignments
 */

class ActivityAssignmentHelper {


  /**
   * Assign the frist activity from the Queue to the current user
   *
   * @param location
   */
  public static function assignMeFirstActivityFromQueue(location : Location) {
    var iteratorWidget = PCFElementFindUtil.findDescendantByCriteria(PCFTypeUtil.getLocationWidget(location), new IteratorOfTypeFindCriteria((Activity)))
    if (iteratorWidget typeis IteratorWidget) {
      var widgetEntries = iteratorWidget.Entries
      if (widgetEntries.HasElements) {
        var firstActivity = widgetEntries.first().Value// as Activity
        if(firstActivity typeis Activity) {
          assignActivityTo(firstActivity, User.util.CurrentUser)
        }
      }
    }
  }

  /**
   * @param activities
   */
  public static function assignMeActivities(activities : Activity[]) {
    if (activities.HasElements) {
      gw.api.web.assignment.AssignableQueueUtil.assignActivitiesFromQueue(activities)
      for (activity in activities) {
        assignUnderwriter(activity, User.util.CurrentUser)
      }
    }
  }

  public static function assignActivityFromPopUp(activityPopUp : AssignmentPopup, activities : Activity[]) : Boolean {
    if (activityPopUp == null) {
      return false
    }

    var hasAssigned = activityPopUp.performAssignment()
    var assignee = activityPopUp.SelectedFromList
    if(activityPopUp.Picker.Selection != null) {
      assignee = activityPopUp.Picker.Selection
    }

    if (activities.HasElements and hasAssigned and assignee typeis UserAssignee) {
      activities.each(\elt -> {
        var activity = PCBeanFinder.loadBeanByPublicID<Activity>(elt.PublicID, Activity)
        if (activity.ActivityPattern.Code == "approve_submission" and activity.AssignedUser != null) {
          assignUnderwriter(activity, activity.AssignedUser)
        }
      })
    }
    return hasAssigned
  }

  /**
   * Assign an activity to a user
   */
  public static function assignActivityTo(activity : Activity, user : User) {
    if (activity != null) {
      gw.api.web.assignment.AssignableQueueUtil.assignActivitiesFromQueue({activity})
      if (user.UserType == TC_UNDERWRITER) {
        assignUnderwriter(activity, user)
      }
    }
  }

  /**
   * reassign Underwriter if one of the activities are for aapprove submission request
   */
  public static function assignUnderwriter(activities : Activity[], underwriter : User) {
    activities.each(\elt -> assignUnderwriter(elt, underwriter))
  }

  /**
   * Reassign the associated Job's Underwriter to the current user, if the user is an Underwriter
   */
  public static function assignUnderwriter(activity : Activity, underwriter : User) : Boolean {
    var reviewPatterns = {"approve_submission"}
    activity = PCBeanFinder.loadBeanByPublicID<Activity>(activity.PublicID, Activity)

    if (underwriter.UserType == TC_UNDERWRITER and activity != null
        and reviewPatterns.contains(activity.ActivityPattern.Code)) {
      var previousUW = activity.Job.RoleAssignments.firstWhere(\elt -> elt.Role == TC_UNDERWRITER)
      var previousUWUser : User
      if (previousUW != null and previousUW.AssignedUser != underwriter) {
        previousUWUser = previousUW.AssignedUser
        try {
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            bundle.add(activity)
            //remove previous Underwriter
            bundle.add(activity.Job)
            activity.Job.removeFromRoleAssignments(previousUW)
            //assign underwriter to the Job
            var newAssignement = activity.Job.createAndAddRoleAssignment()
            newAssignement.assign(activity.AssignedGroup, underwriter)
            newAssignement.Role = TC_UNDERWRITER
            bundle.add(newAssignement)
          }, User.util.UnrestrictedUser)
        } catch (e) {
          PCLoggerCategory.ASSIGNMENT.error(e.StackTraceAsString)
          return false
        }


        //reassign all open activities
        if (previousUWUser != null and activity.Job.AllOpenActivities.HasElements) {
          try {
            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
              activity.Job.AllOpenActivities.where(\elt -> elt.AssignedUser == previousUWUser
                              and not reviewPatterns.contains(activity.ActivityPattern.Code)).each(\elt -> {
                bundle.add(elt)
                elt.assign(elt.AssignedGroup, underwriter)
              })
            }, User.util.UnrestrictedUser)
          } catch (e) {
            PCLoggerCategory.ASSIGNMENT.error(e.Message)
          }
        }
      }
    }
    return true
  }


}