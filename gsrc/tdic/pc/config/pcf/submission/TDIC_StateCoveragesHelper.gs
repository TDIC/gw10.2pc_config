package tdic.pc.config.pcf.submission

uses gw.api.web.job.JobWizardHelper

/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 14/10/14
 * Time: 11:15
 *
 * US462
 * This class is used for TDIC configurations to the WC7 submission wizard, specifically the State Coverages step.
 */
class TDIC_StateCoveragesHelper {

  var _branch : PolicyPeriod
  var _openForEdit : Boolean
  var _jobWizardHelper : JobWizardHelper

  construct(policyPeriod:PolicyPeriod, openForEdit:Boolean, jobWizardHelper:JobWizardHelper){
    _branch = policyPeriod
    _openForEdit = openForEdit
    _jobWizardHelper = jobWizardHelper
    runStateCoveragesOnEnter()
  }

  /**
   * US463
   * 10/14/2014 Shane Sheridan
   * Execute the OOTB taken from the OnEnter property if the WC7WorkersCompStateCoverages WizardStep.
   */
  private function runStateCoveragesOnEnter(){
    if (_openForEdit) {
      _branch.syncComputedValues()
      gw.web.productmodel.ProductModelSyncIssuesHandler.sync(_branch.WC7Line.AllCoverables, _branch.WC7Line.AllModifiables, null, null, _jobWizardHelper)
    }
  }

  /**
   * US463
   * 10/17/2014 Shane Sheridan
   * When the user enters the State Coverages screen, this checks if the WC7CoveredEmployees array is empty,
   * IF yes then it creates a CoveredEmployee Object.
   * This means that the user does not have to specifically create the first WC7CoveredEmployee object.
   *
   * Note: This function returns null to allow a PCF variable to be initialized by this function, but this is an unused variable.
   */
  @Param("line", "The WC7WorkersCompLine associated with this PolicyPeriod.")
  @Param("jurisdiction", "The WC7Jurisdiction associated with this PolicyPeriod.")
  @Returns("Returns null to allow a PCF variable to be initialized by this function.")
  static function createCoveredEmployeeObject(line : WC7WorkersCompLine, jurisdiction : WC7Jurisdiction) : Boolean{
    if(line.WC7CoveredEmployees.IsEmpty){
      line.addCoveredEmployeeWM_TDIC(gw.api.util.StateJurisdictionMappingUtil.getStateMappingForJurisdiction(jurisdiction.Jurisdiction))
    }
    return null
  }
}