package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class CPForm_TDIC1165AS_TDIC extends AbstractSimpleAvailabilityForm{
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
      if (context.Period.Job typeis Cancellation) {
        return (context.Period.Job as Cancellation).CancelReasonCode == ReasonCode.TC_NONPAYMENT or
            (context.Period.Job as Cancellation).CancelReasonCode == ReasonCode.TC_NOTTAKEN
      }
    }
    return false
  }
}