package tdic.pc.config.cp.forms


uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffRemove
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode
uses typekey.Job

class CPForm_BOP2525AS_TDIC extends AbstractSimpleAvailabilityForm {

  var period : PolicyPeriod

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    period = context.Period

    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
      var isBOPEncCovExists = context.Period.BOPLine.BOPLocations*.Buildings.firstWhere(\elt -> elt.BOPEncCovEndtCond_TDICExists) != null
      var isEncCoveRemoved = (context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)?.where(\changedItem -> (changedItem typeis DiffRemove)
          and changedItem.Bean typeis BOPEncCovEndtCond_TDIC)*.Bean).Count >= 1
      var isEncCovAdded = (context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)?.where(\changedItem -> (changedItem typeis DiffAdd)
          and changedItem.Bean typeis BOPEncCovEndtCond_TDIC)*.Bean).Count >= 1
      if (isBOPEncCovExists && (context.Period.Job.Subtype == Job.TC_SUBMISSION || context.Period.Job.Subtype == Job.TC_REINSTATEMENT)) {
        return true
      }
      //GWNW-1344 Infer when Enhanced Coverage Endorsment is present in Policy Change and renewal
      if (context.Period.Job.Subtype == Job.TC_POLICYCHANGE) {
        return isEncCoveRemoved || isEncCovAdded
      }
      if (context.Period.Job.Subtype == Job.TC_RENEWAL) {
        return isBOPEncCovExists || isEncCoveRemoved
      }
      if (period.Job.Subtype == typekey.Job.TC_CANCELLATION && isBOPEncCovExists) {
        if (period.RefundCalcMethod != CalculationMethod.TC_FLAT) {
          return true
        }
      }
    }
    return false
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
  }
}