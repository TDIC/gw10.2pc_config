package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 15/01/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */

class CPForm_OTHRCANC_TDIC extends AbstractMultipleCopiesForm<EffDated> {
  var period : PolicyPeriod
  var formForDisplay : Form
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<EffDated> {
    period = context.Period
    if (context.Period.Job typeis Cancellation and context.Period.BOPLineExists) {
      return ((context.Period.Job.CancelReasonCode == ReasonCode.TC_NONPAYMENT ||
          context.Period.Job.CancelReasonCode == ReasonCode.TC_NOTTAKEN) and hasNamedEntities(context).HasElements) ? hasNamedEntities(context) : {}
    }
    if (context.Period.Job typeis Cancellation and context.Period.GLLineExists and (context.Period.Job.CancelReasonCode == ReasonCode.TC_NONPAYMENT || context.Period.Job.CancelReasonCode == ReasonCode.TC_NOTTAKEN)
        and (context.Period.GLLine?.AdditionalInsureds?.Count > 0 or context.Period.GLLine?.GLCertificateHolders_TDIC.Count > 0)) {
      return context.Period.GLLine?.GLCertificateHolders_TDIC*.GLCertificateHolder_TDIC*.AdditionalInsureds.toList() as ArrayList<PolicyAddlInsured>
    }

    return {}
  }

  function hasNamedEntities(context: FormInferenceContext): ArrayList<EffDated>{
    var listofAllEndHolder = {}
    if(context.Period.BOPLineExists){
      var mortgagees = context.Period.BOPLine?.BOPLocations*.Buildings*.BOPBldgMortgagees
      var lossPayees = context.Period.BOPLine?.BOPLocations*.Buildings*.BOPBldgLossPayees
      var addlInsureds = context.Period.BOPLine?.BOPLocations*.Buildings*.AdditionalInsureds
      if(mortgagees.HasElements) { mortgagees.each(\elt -> listofAllEndHolder.add(elt as EffDated)) }
      if(lossPayees.HasElements) { lossPayees.each(\elt -> listofAllEndHolder.add(elt as EffDated)) }
      if(addlInsureds.HasElements) { addlInsureds.each(\elt -> listofAllEndHolder.add(elt as EffDated))}
      return listofAllEndHolder  as ArrayList<EffDated>
      }
    return {}
    }

  override property get FormAssociationPropertyName() : String {
    if(_entity typeis PolicyMortgagee_TDIC)
    return "PolicyMortgagee_TDIC"
    else if(_entity typeis PolicyLossPayee_TDIC){
      return "PolicyLossPayee_TDIC"
    }else{
      return "PolicyAddlInsured"
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {

    contentNode.addChild(createTextNode("Description", period.Job.JobNumber))
    if (_entity typeis PolicyMortgagee_TDIC) {
      contentNode.addChild(createTextNode("LoanNumber", _entity.LoanNumber as String))
      contentNode.addChild(createTextNode("ExpirationDate_TDIC", _entity.ExpirationDate_TDIC?.toString()))
      if (_entity.ContactDenorm typeis Person) {
        contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
        contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
        contentNode.addChild(createTextNode("LastName", _entity.LastName))
        contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
        contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
        contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
      }
      if (_entity.ContactDenorm typeis Company) {
        contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
        contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
      }
    } else if (_entity typeis PolicyLossPayee_TDIC) {
      contentNode.addChild(createTextNode("LoanNumber", _entity.LoanNumber as String))
      contentNode.addChild(createTextNode("ExpirationDate_TDIC", _entity.ExpirationDate_TDIC?.toString()))
      if (_entity.ContactDenorm typeis Person) {
        contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
        contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
        contentNode.addChild(createTextNode("LastName", _entity.LastName))
        contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
        contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
        contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
      }
      if (_entity.ContactDenorm typeis Company) {
        contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
        contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
      }
    } else if (_entity typeis PolicyAddlInsured) {
      if (_entity.ContactDenorm typeis Person) {
        contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
        contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
        contentNode.addChild(createTextNode("LastName", _entity.LastName))
        contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
        contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
        contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
      }
      if (_entity.ContactDenorm typeis Company) {
        contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
        contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
      }
    }
  }



}