package tdic.pc.config.cp.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses entity.FormAssociation
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 02/01/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP2030BAS_TDIC extends AbstractMultipleCopiesForm<PolicyLossPayee_TDIC> {
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<PolicyLossPayee_TDIC> {
    var listOfLossPayees : ArrayList<PolicyLossPayee_TDIC> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
      var lossPayees =  context.Period.BOPLine?.BOPLocations*.Buildings*.BOPBldgLossPayees
      var lossPayeesAddedOnPolicyChange = lossPayees.where(\lossPayee -> lossPayee.BasedOn == null)
      if(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
        var changeList = context.Period.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var lossPayeesModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyLossPayee_TDIC)*.Bean
        var manuScriptsModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis BOPManuscript_TDIC)*.Bean
        if(lossPayeesAddedOnPolicyChange.HasElements) {
          listOfLossPayees.add(lossPayeesAddedOnPolicyChange.first())
        }
        else if(lossPayees.hasMatch(\lp -> lossPayeesModifiedOnPolicyChange.contains(lp))) {
          listOfLossPayees.add(lossPayees.firstWhere(\lp -> lossPayeesModifiedOnPolicyChange.contains(lp)))
        }

        else if(manuScriptsModifiedOnPolicyChange.HasElements){
          var lossPayeeManuscript= (manuScriptsModifiedOnPolicyChange.firstWhere(\elt ->  (elt as BOPManuscript_TDIC).PolicyLossPayee!=null)) as BOPManuscript_TDIC
            if(lossPayeeManuscript!=null){
              listOfLossPayees.add(lossPayeeManuscript.PolicyLossPayee)
            }
          }
      }
      else if(context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(context.Period.RefundCalcMethod != CalculationMethod.TC_FLAT and lossPayees.HasElements){
          listOfLossPayees.add(lossPayees.first())
        }
      }
      else if(!(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE or context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION) and lossPayees.HasElements){
        listOfLossPayees.add(lossPayees.first())
      }
    }
    return listOfLossPayees.HasElements? listOfLossPayees.toList(): {}
  }

  override property get FormAssociationPropertyName() : String {
    return "PolicyLossPayee_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LoanNumber", _entity.LoanNumber as String))
    contentNode.addChild(createTextNode("ExpirationDate_TDIC", _entity.ExpirationDate_TDIC?.toString()))
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    if (_entity.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
    }
    if (_entity.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}