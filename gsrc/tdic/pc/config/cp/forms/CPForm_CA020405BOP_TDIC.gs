package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 02/01/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_CA020405BOP_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    var bopLocations = context.Period.BOPLine?.BOPLocations
    var locations : ArrayList<BOPLocation> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and bopLocations.HasElements) {
      var bopLocationsOnPolicyChange = bopLocations.where(\loc -> loc.BasedOn == null)
      if (context.Period.Job typeis Submission and bopLocations*.Buildings.HasElements and !(context.Period.isDIMSPolicy_TDIC)) {
        if(bopLocations.hasMatch(\loc -> loc.Buildings.hasMatch(\building -> !building.BOPBuildingCovExists))){
          locations.add(bopLocations.first())
        }
      } else if ((context.Period.Job typeis PolicyChange and !(context.Period.BasedOn.Job typeis Renewal) and !(context.Period.isDIMSPolicy_TDIC)) and
          context.Period.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_N and bopLocationsOnPolicyChange.HasElements) {
        if(bopLocationsOnPolicyChange.hasMatch(\loc -> loc.BasedOn == null and loc.Buildings.hasMatch(\building -> building.BOPDentalGenLiabilityCov_TDICExists
            and !building.BOPBuildingCovExists))){
          locations.add(bopLocationsOnPolicyChange.first())
        }
      }
    }
    return locations.HasElements? locations.toList(): {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("OriginalEffectiveDate", _entity.Location.OriginalEffDate_TDIC.toString()))
    contentNode.addChild(createTextNode("AddressLine1", _entity.Location.AddressLine1.toString()))
    contentNode.addChild(createTextNode("City", _entity.Location.City.toString()))
    contentNode.addChild(createTextNode("PostalCode", _entity.Location.PostalCode.toString()))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}