package tdic.pc.config.cp.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation
uses typekey.Job

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/5/2020
 * Create Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP2115AS_TDIC extends AbstractMultipleCopiesForm<BOPManuscript_TDIC> {


  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPManuscript_TDIC> {
    var period = context.Period
    var bopManuscriptSchedItems = period.BOPLine.BOPLocations*.Buildings*.BOPManuscript
    var bopManuscriptEndorExists = period.BOPLine.BOPLocations?.hasMatch(\loc -> loc.Buildings?.
        hasMatch(\building -> building.BOPManuscriptEndorsement_TDICExists))
    var manuscriptItems : ArrayList<BOPManuscript_TDIC> = {}
    if (period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and bopManuscriptEndorExists and bopManuscriptSchedItems.HasElements) {
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var manuscriptSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis BOPManuscript_TDIC)*.Bean
        var additionalInsuredItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsured)*.Bean
        var lossPayeeItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyLossPayee_TDIC)*.Bean
        var mortgageeItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyMortgagee_TDIC)*.Bean
        var policyAddInsuredDetailModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsuredDetail)*.Bean

        if (bopManuscriptSchedItems.hasMatch(\item -> item.BasedOn == null)) {
          manuscriptItems.add(bopManuscriptSchedItems.where(\item -> item.BasedOn == null).first())

        } else if (bopManuscriptSchedItems.hasMatch(\item -> manuscriptSchedLineItemsModified.contains(item))) {
          manuscriptItems.add(bopManuscriptSchedItems.where(\item -> manuscriptSchedLineItemsModified.contains(item)).first())
        }
         if (additionalInsuredItemsModified.HasElements) {
          var additionalInsuredManuscript= (additionalInsuredItemsModified.firstWhere(\elt ->  (elt as PolicyAddlInsured).PolicyAdditionalInsuredDetails*.BOPManuScript.Count>0)) as PolicyAddlInsured
         if (additionalInsuredManuscript != null && manuscriptItems.Count==0) {
            manuscriptItems.add(additionalInsuredManuscript*.PolicyAdditionalInsuredDetails*.BOPManuScript.first())
          }
        }
        if(policyAddInsuredDetailModified.HasElements){
          var policyAddInsuredDetail=(policyAddInsuredDetailModified.firstWhere(\elt -> (elt as PolicyAddlInsuredDetail).BOPManuScript.Count>0)) as PolicyAddlInsuredDetail
          if(policyAddInsuredDetail!=null && manuscriptItems.Count==0){
            manuscriptItems.add(policyAddInsuredDetail*.BOPManuScript.first())
          }
        }

         if(lossPayeeItemsModified.HasElements){
            var lossPayeeManuscript=(lossPayeeItemsModified.firstWhere(\elt -> (elt as PolicyLossPayee_TDIC).BOPManuscript.Count>0)) as PolicyLossPayee_TDIC
            if(lossPayeeManuscript!=null && manuscriptItems.Count==0){
              manuscriptItems.add(lossPayeeManuscript.BOPManuscript.first())
            }
        }
         if(mortgageeItemsModified.HasElements){
          var mortgageeManuscript=(mortgageeItemsModified.firstWhere(\elt -> (elt as PolicyMortgagee_TDIC).BOPManuscript.Count>0)) as PolicyMortgagee_TDIC
          if(mortgageeManuscript!=null && manuscriptItems.Count==0){
            manuscriptItems.add(mortgageeManuscript.BOPManuscript.first())
          }
        }

      }
        else if (period.Job.Subtype == typekey.Job.TC_CANCELLATION) {
          if (period.RefundCalcMethod != CalculationMethod.TC_FLAT) {
            manuscriptItems.add(bopManuscriptSchedItems.first())
          }
        } else {
          manuscriptItems.add(bopManuscriptSchedItems.first())
        }

        return manuscriptItems.HasElements ? manuscriptItems.toList() : {}
      }
      return {}
    }

  override property get FormAssociationPropertyName() : String {
    return "BOPManuscript_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    //contentNode.addChild(createTextNode("EffectiveDate", _entity.ManuscriptEffectiveDate.toString()))
    contentNode.addChild(createTextNode("ManuscriptEffectiveDate", _entity.ManuscriptEffectiveDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptExpirationDate", _entity.ManuscriptExpirationDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptType", _entity.ManuscriptType.toString()))
    contentNode.addChild(createTextNode("ManuscriptDescription", _entity.ManuscriptDescription))
    contentNode.addChild(createTextNode("policyAddInsured_TDIC", _entity.policyAddInsured_TDIC?.toString()))
    contentNode.addChild(createTextNode("policyLossPayee_TDIC", _entity.PolicyLossPayee?.toString()))
    contentNode.addChild(createTextNode("PolicyMortgagee", _entity.PolicyMortgagee?.toString()))
    contentNode.addChild(createTextNode("Desciption", _entity.policyAddInsured_TDIC?.Desciption?.toString()))
    contentNode.addChild(createTextNode("LoanNumberString", _entity.PolicyMortgagee?.LoanNumberString?.toString()))
    contentNode.addChild(createTextNode("LoanNumberString", _entity.PolicyLossPayee?.LoanNumberString?.toString()))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }

  override function getMatchKeyForForm(form : Form) : String {
    if(form.FormAssociations.HasElements){
      if ((form.FormAssociations[0] as CPFormAssociation_TDIC).BOPLocation_TDIC !== null) {
        return form.FormAssociations[0].fixedIdValue("BOPLocation_TDIC") as java.lang.String
      }
      return form.FormAssociations[0].fixedIdValue(FormAssociationPropertyName) as java.lang.String
    }
    return null
  }
}

