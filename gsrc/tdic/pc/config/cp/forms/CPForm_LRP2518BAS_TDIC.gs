package tdic.pc.config.cp.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses entity.FormAssociation
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 02/01/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_LRP2518BAS_TDIC extends AbstractMultipleCopiesForm<PolicyAddlInsured>{
  var policyperiod : PolicyPeriod
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<PolicyAddlInsured> {
    var listOfAddlInsureds : ArrayList<PolicyAddlInsured> = {}
    var finalListOfAddlInsureds : ArrayList<PolicyAddlInsured> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
      var buildings = context.Period.BOPLine?.BOPLocations*.Buildings
      for(building in buildings){
        var additionalInsureds = building.AdditionalInsureds
        var additionalInsuredsAddedOnPolicyChange = building.AdditionalInsureds.where(\addlInsured -> addlInsured.BasedOn == null)
        if (context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE and building.BOPBuildingOwnersLiabCov_TDICExists) {
          var changeList = context.Period.getDiffItems(DiffReason.TC_COMPAREJOBS)
          var addlInsuredsModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsured)*.Bean
          var addlInsuredDetailsModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsuredDetail)*.Bean
          var manusScriptModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis BOPManuscript_TDIC)*.Bean
          if(additionalInsuredsAddedOnPolicyChange.HasElements){
            listOfAddlInsureds.add(additionalInsuredsAddedOnPolicyChange.first())
          }
          else if(additionalInsureds.hasMatch(\addlInsured -> addlInsuredsModifiedOnPolicyChange.contains(addlInsured))){
            listOfAddlInsureds.add(additionalInsureds.firstWhere(\ai -> addlInsuredsModifiedOnPolicyChange.contains(ai)))
          }
          else if(additionalInsureds*.PolicyAdditionalInsuredDetails.hasMatch(\addlDetail -> addlInsuredDetailsModifiedOnPolicyChange.contains(addlDetail))){
            listOfAddlInsureds.add(additionalInsureds.firstWhere(\ai -> ai.PolicyAdditionalInsuredDetails.hasMatch(\elt1 -> addlInsuredDetailsModifiedOnPolicyChange.contains(elt1))))
          }
          else if(manusScriptModifiedOnPolicyChange.HasElements){
            var additionalInsuredManuscript= (manusScriptModifiedOnPolicyChange.firstWhere(\elt -> (elt as BOPManuscript_TDIC).policyAddInsured_TDIC.PolicyAddlInsured!=null)) as BOPManuscript_TDIC
            if(additionalInsuredManuscript!=null){
              listOfAddlInsureds.add(additionalInsuredManuscript.policyAddInsured_TDIC.PolicyAddlInsured)
            }
          }
        }
        else if(context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION) {
          if (context.Period.RefundCalcMethod != CalculationMethod.TC_FLAT and building.BOPBuildingOwnersLiabCov_TDICExists
              and additionalInsureds.HasElements) {
            listOfAddlInsureds.add(additionalInsureds.first())
          }
        }
        else if (!(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE or context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION)
            and building.BOPBuildingOwnersLiabCov_TDICExists and additionalInsureds.HasElements) {
          listOfAddlInsureds.add(additionalInsureds.first())
        }
      }
    }
    if(listOfAddlInsureds.HasElements){
      finalListOfAddlInsureds.add(listOfAddlInsureds.first())
    }
    return finalListOfAddlInsureds.HasElements? finalListOfAddlInsureds.toList(): {}
  }

  override property get FormAssociationPropertyName() : String {
    return "PolicyAddlInsured_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("Type", _entity.PolicyAdditionalInsuredDetails*.AdditionalInsuredType as String))
    contentNode.addChild(createTextNode("Description", _entity.PolicyAdditionalInsuredDetails*.Desciption as String))
    contentNode.addChild(createTextNode("Description",  policyperiod.Job.JobNumber))


    if (_entity.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
    }
    if (_entity.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}