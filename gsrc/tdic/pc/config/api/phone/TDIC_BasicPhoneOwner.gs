package tdic.pc.config.api.phone

uses gw.lang.reflect.features.PropertyReference
uses gw.lang.reflect.features.BoundPropertyReference
uses gw.api.phone.ContactPhoneDelegate

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 10/16/14
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_BasicPhoneOwner extends ContactPhoneDelegate {
  var _type: PhoneType

  construct(value: Object, aType: PhoneType) {
    super(value,aType)
    var propRef = toBuiltInColumnReference(aType)
    initialize(value, propRef.PropertyInfo)
    _type = aType
  }

  construct(value: Object, propRef: PropertyReference ) {
    super(value,propRef)
    initialize(value, propRef.PropertyInfo)
    _type = fromPropInfoToPhoneType(propRef.PropertyInfo)
  }

  construct(value: Object, boundPropRef: BoundPropertyReference){
    super(value,boundPropRef)
    initialize(value, boundPropRef.PropertyInfo)
    _type = fromPropInfoToPhoneType(boundPropRef.PropertyInfo)
  }

  /**
    *  Do not set the Primary Phone when the user enters a number.  Have the user manually select the Primary Phone.
    */
  override function setPrimaryPhoneType(value: String) {
    // Do nothing
  }
}