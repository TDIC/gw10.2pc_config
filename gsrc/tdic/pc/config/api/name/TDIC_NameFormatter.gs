package tdic.pc.config.api.name

uses gw.api.name.ContactNameFields
uses gw.api.name.NameOwnerFieldId
uses java.lang.StringBuffer
uses gw.api.name.NameLocaleSettings
uses org.apache.commons.lang.WordUtils
uses gw.api.name.NameFormatter

/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 22/08/14
 * Time: 11:42
 * This class replaces the OOTB NameFormatter for use in formatting a Person Contact's display name - TDIC.
 */
@Export
class TDIC_NameFormatter extends NameFormatter {

  /* Delimiters */
  final var _SPACE_DELIMITER = " "
  final var _PERIOD_DELIMITER = "."
  final var _COMMA_DELIMITER = ", "

  construct() {
  }

  /**
  *  Method copied from NameFormatter and modified for displaying TDIC custom format for Person contact DisplayName.
   * Format a name as text, including all fields.
   */
   @Param("name","The name to format.")
  public function format(name : ContactNameFields) : String {
    return internalFormat(name)
  }

  /**
   *  Non-Overrideable [i.e. private] method copied from NameFormatter and modified for displaying TDIC custom format for Person contact DisplayName.
    * If the field is non-empty and is a default display field, append the specified delimiter if needed and then the value
    */
  private function append (sb : StringBuffer, fieldId : NameOwnerFieldId, delimiter : String, value : String) {
    if (value.HasContent and TDIC_NameLocaleSettings.TDIC_DEFAULT_DISPLAY_NAME_FIELDS.contains(fieldId)) {
      if (sb.length() > 0) {
        sb.append(delimiter)
      }
      sb.append(value)
      //if(fieldId == NameOwnerFieldId.MIDDLENAME){
      //  sb.append(_PERIOD_DELIMITER)
      //}
    }
  }

  /**
  * Non-Overrideable [i.e. private] method copied from NameFormatter and modified for displaying TDIC custom format for Person contact DisplayName.
   * Use value1 if it is non-empty, otherwise value2.
   */
  private function firstNonEmpty(value1 : String, value2 : String) : String {
    return value1.HasContent ? value1 : value2
  }

  /**
   * Non-Overrideable [i.e. private] method copied from NameFormatter and modified for displaying TDIC custom format for Person contact DisplayName.
   * New fields created for TDIC are accessed from TDIC_NameOwnerFieldId.
  */
  private function internalFormat(name : ContactNameFields) : String {
    if (name == null) {
      return null
    }

    var fieldId = NameOwnerFieldId
    var sb = new StringBuffer()
    var mode = NameLocaleSettings.TextFormatMode

    if (not (name typeis TDIC_PersonNameFieldsImpl)) {
      return name.NameKanji.HasContent ? name.NameKanji : name.Name
    }
    var pName = name as TDIC_PersonNameFieldsImpl

    var lastName = firstNonEmpty(pName.LastNameKanji, pName.LastName)
    /*if( lastName != null and isAllUpperCase(lastName) ) {
      lastName = WordUtils.capitalize(lastName.toLowerCase())
    }*/
    var firstName = firstNonEmpty(pName.FirstNameKanji, pName.FirstName)
    /*if( firstName != null and isAllUpperCase(firstName) ) {
      firstName = WordUtils.capitalize(firstName.toLowerCase())
    }*/
    if (mode == "default") {
      append(sb, fieldId.FIRSTNAME, _SPACE_DELIMITER, firstName)
      append(sb, fieldId.MIDDLENAME, _SPACE_DELIMITER, pName.MiddleName)
      append(sb, fieldId.LASTNAME, _SPACE_DELIMITER, pName.Particle)
      append(sb, fieldId.LASTNAME, _SPACE_DELIMITER, lastName)
      append(sb, fieldId.LASTNAME, _COMMA_DELIMITER, pName.Suffix.DisplayName)
      append(sb, TDIC_NameOwnerFieldId.CREDENTIALS, _COMMA_DELIMITER, pName.Credential_TDIC.DisplayName)
    } else if (mode == "Japan") {
      append(sb, fieldId.LASTNAME, _SPACE_DELIMITER, lastName)
      append(sb, fieldId.FIRSTNAME, _SPACE_DELIMITER, firstName)
    }
    return sb.toString()
  }
}