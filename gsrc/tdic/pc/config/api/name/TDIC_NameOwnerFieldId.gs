package tdic.pc.config.api.name

    uses gw.api.name.NameOwnerFieldId
/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 21/08/14
 * Time: 14:31
 * Extension of NameOwnerFieldId for the addition of new entity fields for TDIC.
 */
    class TDIC_NameOwnerFieldId extends NameOwnerFieldId{

    /**
     * Constants for available fields for person name which were created for TDIC.
     * OOTB available fields are inherited.
     */
    public static final var CREDENTIALS : NameOwnerFieldId = new TDIC_NameOwnerFieldId("Credential_TDIC") as NameOwnerFieldId

    protected construct(aName : String) {
    super(aName)
    }
    }