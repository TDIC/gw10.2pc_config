package tdic.pc.config.job

uses java.util.Date

uses gw.api.validation.EntityValidationException
uses gw.job.JobProcess

uses java.lang.Exception
uses gw.job.uw.UWAuthorityBlocksProgressException
uses org.slf4j.LoggerFactory

abstract class AutomatedJobTemplate<J extends Job> extends AutomatedJob {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${AutomatedJobTemplate.Type.RelativeName} - "

  private var _job : J as readonly Job;

  public construct (jobType : AutomatedJob_TDIC, policy : Policy, effectiveDate : Date) {
    super(jobType, policy, effectiveDate);
  }

  override public property get Description() : String {
    return "\"" + _jobType.DisplayName + "\" automated " + J.Type.DisplayName;
  }

  /**
   * Process the automated job.
   */
  override final public function processAutomatedJob() : Job {
    var jobProcess : JobProcess;
    var policyPeriod : PolicyPeriod;

    resetErrorsAndWarnings();
    _job = null;

    if (IsJobNeeded == false) {
      _logger.info (_LOG_TAG + "Automated " + J.Type.DisplayName + " is not needed.");
      return null;
    }

    var error = canStartJob();
    if (error != null) {
      logError (_LOG_TAG + "Unable to start " + Description + ": " + error);
    } else {
      _logger.info (_LOG_TAG + "Creating " + J.Type.DisplayName + " for " + Description);
      _job = createJob();
      if (_job == null) {
        logError (_LOG_TAG + "Unable to create " + J.Type.DisplayName + " for " + Description);
      }
    }

    if (HasErrors == false) {
      _job.AutomatedJob_TDIC = _jobType;
      _logger.info (_LOG_TAG + "Starting " + J.Type.DisplayName + " for " + Description);
      startJob();

      policyPeriod = _job.LatestPeriod;
      jobProcess = policyPeriod.JobProcess;

      _logger.info (_LOG_TAG + "Updating " + _job.Subtype.DisplayName + " " + _job.JobNumber + " for " + Description);
      if (updatePolicy(policyPeriod) == false) {
        logError (_LOG_TAG + "Failed to update " + _job.Subtype.DisplayName + " " + _job.JobNumber
                      + " for " + Description);
        preExitCleanup (policyPeriod);
      }
    }

    if (ShouldQuote == false) {
      preExitCleanup (policyPeriod);
      return _job;
    }

    if (HasErrors == false) {
      try {
        _logger.info (_LOG_TAG + "Quoting " + _job.Subtype.DisplayName + " " + _job.JobNumber + " for " + Description);
        quote (jobProcess);
        if (isQuoteValid(policyPeriod) == false) {
          logError (_LOG_TAG + "Received invalid quote for " + _job.Subtype.DisplayName + " " + _job.JobNumber
                      + " for " + Description);
          preExitCleanup (policyPeriod);
        }
      } catch (e : Exception) {
        logError (_LOG_TAG + "Failed to quote " + _job.Subtype.DisplayName + " " + _job.JobNumber
                      + " for " + Description + ": " + e);
        preExitCleanup (policyPeriod);
      }
    }

    if (HasErrors == false) {
      try {
        try {
          completeJobIgnoreWarnings (jobProcess);
        } catch (e : UWAuthorityBlocksProgressException) {
          // Approve issues generated from complete and rerun
          handleUWIssues (policyPeriod, {UWIssueBlockingPoint.TC_BLOCKSBIND, UWIssueBlockingPoint.TC_BLOCKSISSUANCE});
          completeJobIgnoreWarnings (jobProcess);
        }
      } catch (e : Exception) {
        logError (_LOG_TAG + "Failed to complete " + _job.Subtype.DisplayName + " " + _job.JobNumber
                      + " for " + Description + ": " + e);
        preExitCleanup (policyPeriod);
      }
    }

    if (HasErrors == false) {
      if (jobProcess.canApplyChangesToFutureUnboundRenewal()) {
        applyChangesToFutureUnboundRenewal(policyPeriod);
      } else if (jobProcess.canApplyChangesToFutureBoundRenewal()) {
        // Clear flag for future bound renewals.
        // Assumption is that an automated job will be processed separately on the bound renewal.
        policyPeriod.clearResolveWithFuturePeriods()
      }

      // Handle Preemptions
      var preemptingBranches : PolicyPeriod[];
      for (preemptedPeriod in policyPeriod.PreemptedPeriodsIfBoundNow) {
        preemptingBranches = preemptedPeriod.PreemptionsOfThisPeriod;
        if (preemptingBranches.length == 1) {
          handlePreemption (preemptedPeriod);
        } else {
          logWarning (_LOG_TAG + preemptedPeriod.Job.Subtype.DisplayName + " " + preemptedPeriod.Job.JobNumber
                        + " (Branch = " + preemptedPeriod.BranchName
                        + ") was already preempted before the automated " + _job.Subtype.DisplayName + ".  "
                        + "Preemption will NOT be automatically handled.")
        }
      }
    }

    preExitCleanup (policyPeriod);
    return _job;
  }

  private function completeJobIgnoreWarnings (jobProcess : JobProcess) : void {
    try {
      _logger.info (_LOG_TAG + "Completing " + _job.Subtype.DisplayName + " " + _job.JobNumber + " for " + Description);
      complete(jobProcess);
    } catch (e : EntityValidationException) {
      _logger.info (_LOG_TAG + "Caught entity validation exception: " + e.toString());
      _logger.info (_LOG_TAG + "Completing (again to ignore warnings) "
                      + _job.Subtype.DisplayName + " " + _job.JobNumber + " for " + Description);
      complete(jobProcess);
    }
  }

  abstract protected function canStartJob() : String;

  abstract protected function createJob() : J;

  abstract protected function startJob() : void;

  /**
   *  Update the policy data.
   */
  abstract protected function updatePolicy (policyPeriod : PolicyPeriod) : boolean;

  protected property get ShouldQuote() : boolean {
    return true;
  }

  /**
   * Quote the job.
   */
  protected function quote (jobProcess : JobProcess) : void {
    jobProcess.requestQuote(null, ValidationLevel.TC_QUOTABLE, RatingStyle.TC_DEFAULT, false);
  }

  /**
   * Validate the quote.
   */
  protected function isQuoteValid (policyPeriod : PolicyPeriod) : boolean {
    return policyPeriod.ValidQuote;
  }

  /**
   * Handle UW Issues for the specified blocking points.
  */
  protected function handleUWIssues (policyPeriod : PolicyPeriod, blockingPoints : UWIssueBlockingPoint[]) : void {
    var uwIssues = policyPeriod.UWIssuesActiveOnly.where (\ui -> blockingPoints.contains(ui.CurrentBlockingPoint));
    for (uwIssue in uwIssues) {
      if (isIssueAutoApprovable (uwIssue)) {
        approveIssue (uwIssue);
      }
    }
  }

  /**
   *  Is issue auto approvable?
  */
  protected function isIssueAutoApprovable (uwIssue : UWIssue) : boolean {
    return false;
  }

  /**
   * Approve issue
   */
  protected function approveIssue (uwIssue : UWIssue) : void {
    _logger.info ("Approving UW Issue " + uwIssue);
    var approval = uwIssue.createAutoApproval();
    approval.ReferenceValue = uwIssue.IssueType.calculateDefaultValue(approval.IssueValue);
    approval.EditBeforeBind = false;
    approval.Duration = UWApprovalDurationType.TC_NEXTCHANGE;
    uwIssue.createApprovalHistoryFromCurrentValues();
  }

  /**
   * Complete the job.
   */
  protected function complete (jobProcess : JobProcess) : void {
    jobProcess.bind();
  }

  /**
   * Post-Error Cleanup
   */
  protected function preExitCleanup(policyPeriod : PolicyPeriod) : void {
    if (_job.Complete == false) {
      _logger.info (_LOG_TAG + "Withdrawing " + _job.Subtype.DisplayName + " " + _job.JobNumber
                        + " for " + Description);
      _job.LatestPeriod.JobProcess.withdraw();
    }
  }

  /**
   * Apply changes to unbound renewals
   */
  private function applyChangesToFutureUnboundRenewal (policyPeriod : PolicyPeriod) : void {
    var renewal = policyPeriod.FutureRenewals.first().Renewal;
    var status = renewal.LatestPeriod.Status;

    _logger.info (_LOG_TAG + "Applying changes to unbound renewal " + renewal.JobNumber);

    var results = policyPeriod.JobProcess.applyChangesToFutureUnboundRenewal()
    if (results.hasMatch(\r -> r.hasConflicts())) {
      logError (_LOG_TAG + "Conflict occurred while applying changes to a future unbound renewal " + renewal.JobNumber);
    } else {
      if (PolicyPeriodStatus.TF_CLOSING.TypeKeys.contains(status)) {
        var renewalPeriod = renewal.LatestPeriod;
        var renewalProcess = renewalPeriod.RenewalProcess;

        try {
          // Quote Renewal
          renewalProcess.requestQuote(null, ValidationLevel.TC_QUOTABLE, RatingStyle.TC_DEFAULT, false);
          if (renewalPeriod.ValidQuote) {
            try {
              // Reschedule Renewal
              if (status == PolicyPeriodStatus.TC_RENEWING) {
                _logger.info (_LOG_TAG + "Scheduling renewal " + renewal.JobNumber);
                renewalProcess.pendingRenew();
              } else if (status == PolicyPeriodStatus.TC_NONRENEWING) {
                _logger.info (_LOG_TAG + "Scheduling non-renewal " + renewal.JobNumber);
                renewalProcess.pendingNonRenew();
              } else if (status == PolicyPeriodStatus.TC_NOTTAKING) {
                _logger.info (_LOG_TAG + "Scheduling not-taken renewal " + renewal.JobNumber);
                renewalProcess.pendingNotTaken();
              }
            } catch (e : Exception) {
              logError (_LOG_TAG + "Failed to schedule renewal " + renewal.JobNumber + ": " + e);
            }
          } else {
            logError (_LOG_TAG + "Received invalid quote for renewal " + renewal.JobNumber);
          }
        } catch (e : Exception) {
          logError (_LOG_TAG + "Failed to quote renewal " + renewal.JobNumber + ": " + e);
        }
      }
    }
  }

  /**
   * Handle the preemption on the preempted policy period.
   */
  private function handlePreemption (preemptedPolicyPeriod : PolicyPeriod) : void {
    var job = preemptedPolicyPeriod.Job;
    var status = preemptedPolicyPeriod.Status;

    _logger.info (_LOG_TAG + "Handling preemption on " + job.Subtype.DisplayName + " " + job.JobNumber
                    + " (Branch = " + preemptedPolicyPeriod.BranchName + ")");
    preemptedPolicyPeriod.AllAccountSyncables.each(\ a -> a.prepareForDiff());
    var result = preemptedPolicyPeriod.JobProcess.handlePreemptions();

    if (result.hasConflicts()) {
      logError (_LOG_TAG + "Conflict occurred while handling preemption on "
                  + job.Subtype.DisplayName + " " + job.JobNumber
                  + " (Branch = " + preemptedPolicyPeriod.BranchName + ")");
    } else {
      if (status == PolicyPeriodStatus.TC_CANCELING) {
        var cancelPeriod = job.LatestPeriod;
        var cancelProcess = cancelPeriod.CancellationProcess;
        try {
          // Quote Cancellation
          cancelProcess.requestQuote(null, ValidationLevel.TC_QUOTABLE, RatingStyle.TC_DEFAULT, false);
          if (cancelPeriod.ValidQuote) {
            try {
              // Reschedule Cancellation
              _logger.info (_LOG_TAG + "Rescheduling cancellation " + job.JobNumber);
              cancelProcess.rescheduleCancellation(cancelPeriod.EditEffectiveDate);
            } catch (e : Exception) {
              logError (_LOG_TAG + "Failed to reschedule cancellation " + job.JobNumber + ": " + e);
            }
          } else {
            logError (_LOG_TAG + "Received invalid quote for cancellation " + job.JobNumber);
          }
        } catch (e : Exception) {
          logError (_LOG_TAG + "Failed to quote cancellation " + job.JobNumber + ": " + e);
        }
      }
    }
  }
}