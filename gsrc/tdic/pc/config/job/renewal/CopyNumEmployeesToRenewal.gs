package tdic.pc.config.job.renewal

uses java.util.ArrayList
uses java.util.Map
uses java.lang.Integer
uses java.util.List
uses org.slf4j.LoggerFactory

class CopyNumEmployeesToRenewal {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${CopyNumEmployeesToRenewal.Type.RelativeName} - "

  protected var _policyPeriod : PolicyPeriod;

  public construct (policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod;
  }

  /**
   *  Determine if any covered employees are missing number of employees and copy data
   *  from an earlier transaction (if available).
   */
  public function copyNumberOfEmployeesIfNeeded() {
    var covEmps = CoveredEmployeesWithMissingNumEmployees;
    var policyTerm = _policyPeriod.BasedOn.PolicyTerm;

    if (covEmps.Empty == false) {
      _logger.info (_LOG_TAG + "Updating missing number of employees on " + _policyPeriod);
    }

    while (policyTerm != null and covEmps.Empty == false) {
      processPolicyTerm (policyTerm, covEmps);

      if (covEmps.Empty == false) {
        policyTerm = getPreviousPolicyTerm(policyTerm);
      }
    }

    if (covEmps.Empty == false) {
      _logger.warn (_LOG_TAG + "Failed to find data for missing number of employees on " + covEmps.Count
                                  + " covered employee records.");
    }
  }

  protected function processPolicyTerm (policyTerm : PolicyTerm, covEmps : List<WC7CoveredEmployee>) : void {
    var covEmpMap : Map<Jurisdiction,List<WC7CoveredEmployee>>;
    var numEmployees : Integer;
    var srcCovEmp : WC7CoveredEmployee;
    var srcCovEmps : List<WC7CoveredEmployee>;
    var wc7Line : WC7WorkersCompLine;

    var termPeriods = policyTerm.Periods.where(\pp -> pp.Status == PolicyPeriodStatus.TC_BOUND or pp.Status == PolicyPeriodStatus.TC_AUDITCOMPLETE)
                                        .orderByDescending(\pp -> pp.Job.CloseDate);
    for (period in termPeriods) {
      _logger.trace ("Examining " + period.Job.Subtype + " " + period.Job.JobNumber + " for employee numbers.");
      covEmpMap = covEmps.partition(\ce -> ce.Jurisdiction.Jurisdiction);
      wc7Line = period.WC7Line;
      for (jurisdiction in covEmpMap.Keys) {
        srcCovEmps = wc7Line.getWC7CoveredEmployeesWM(jurisdiction).toList();
        for (covEmp in covEmpMap.get(jurisdiction)) {
          srcCovEmp = srcCovEmps.firstWhere(\ce -> ce.FixedId == covEmp.FixedId);
          if (srcCovEmp != null) {
            numEmployees = (period.Job typeis Audit) ? srcCovEmp.AuditedNumEmployees_TDIC : srcCovEmp.NumEmployees;
            if (numEmployees != null and numEmployees > 0) {
              _logger.info (_LOG_TAG + "Updating Covered Employee (ClassCode = " + covEmp.ClassCode
                                          + ", Location = " + covEmp.LocationWM + ", FixedId = " + covEmp.FixedId
                                          + ") to " + numEmployees + " based upon " + period.Job.Subtype
                                          + " " + period.Job.JobNumber
                                          + " (effective " + period.EditEffectiveDate.formatDate(SHORT) + ").");
              covEmp.NumEmployees = numEmployees;
              covEmps.remove(covEmp);
            }
          }
        }
      }
      if (covEmps.Empty) {
        break;
      }
    }
  }

  protected property get CoveredEmployeesWithMissingNumEmployees() : List<WC7CoveredEmployee> {
    var retval = new ArrayList<WC7CoveredEmployee>();

    var wc7Line = _policyPeriod.WC7Line;
    for (jurisdiction in wc7Line.WC7Jurisdictions) {
      for (covEmp in wc7Line.getWC7CoveredEmployeesWM(jurisdiction.Jurisdiction)) {
        if (covEmp.NumEmployees == null) {
          retval.add(covEmp);
        }
      }
    }

    return retval;
  }

  protected function getPreviousPolicyTerm(policyTerm : PolicyTerm) : PolicyTerm {
    var newTermPeriod = policyTerm.Periods.firstWhere(\pp -> pp.Job.NewTerm
                                                         and pp.Status == PolicyPeriodStatus.TC_BOUND);
    return newTermPeriod.BasedOn.PolicyTerm;
  }
}