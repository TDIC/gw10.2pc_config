package tdic.pc.config.job.policychange

uses entity.BOPBuildingCov
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffItem
uses gw.api.diff.DiffProperty
uses gw.api.diff.DiffRemove
uses gw.api.locale.DisplayKey
uses gw.api.web.job.JobWizardHelper
uses org.slf4j.LoggerFactory

uses java.math.BigDecimal
uses java.math.RoundingMode
uses java.text.DecimalFormat

uses entity.GeneralLiabilityCov
uses entity.Building
uses entity.GeneralLiabilityCond
uses entity.BOPBuildingModifier_TDIC
uses entity.WC7Modifier
uses typekey.Job

class ChangeReasons {
  protected var _jobWizardHelper : JobWizardHelper;

  construct() {
  }

  construct(jobWizardHelper : JobWizardHelper) {
    _jobWizardHelper = jobWizardHelper;
  }

  /**
   * US1274, DE216 robk
   * Examines the DiffItems for the specified PolicyPeriod and adds/removes missing/unnecessary Change Reasons to/from the associated Job.
   */
  public function validateChangeReasons(policyPeriod : PolicyPeriod, addedReasons : ChangeReason_TDIC[]) {
    var requiredChangeReasons = determineRequiredChangeReasons(policyPeriod)
    var matchingChangeReason : ChangeReason_TDIC

    // GWPS-546
    var expPolicy = policyPeriod?.Job?.LatestPeriod?.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.DisplayName
    var nonRenewedPolicy = policyPeriod.Policy?.Jobs?.where(\elt -> elt.Subtype == Job.TC_RENEWAL && elt.DisplayStatus == "Non-renewed")?.Count > 0
    var cyberChangedPolicy = policyPeriod.Policy?.Jobs?.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE and elt.ID != policyPeriod.Job.ID)*.ChangeReasons?.
        where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER or elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER)
    var plChanngedPolicy = policyPeriod.Policy?.Jobs.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE and elt.ID != policyPeriod.Job.ID)*.ChangeReasons?.
        where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)
    for(reason in addedReasons.where(\r -> (r.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER) or (r.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER))){
      //var changedPolicy = reason.ChangeReason == TC_ERECYBEROFFER ? cyberChangedPolicy : plChanngedPolicy
      if(policyPeriod?.Job?.ChangeReasons?.hasMatch(\changeReason -> (changeReason?.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER) or (changeReason?.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)) and
          policyPeriod.CancellationDate == null && !nonRenewedPolicy && !expPolicy && ((plChanngedPolicy?.Count>0) or (cyberChangedPolicy?.Count>0))){
        policyPeriod.Job.removeFromChangeReasons(reason)
      }
    }



    for (aReason in requiredChangeReasons) {
      matchingChangeReason = addedReasons.firstWhere( \ r -> r.ChangeReason == aReason.ChangeReason)
      if (matchingChangeReason == null and !(policyPeriod?.Job?.ChangeReasons?.hasMatch(\changeReason -> changeReason?.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN))) {
        policyPeriod.Job.addToChangeReasons(aReason)
        if (_jobWizardHelper != null) {
          _jobWizardHelper.addInfoWebMessage(DisplayKey.get("TDIC.Validation.ChangeReasons.RequiredChangeReasonAdded", aReason.ChangeReason.DisplayName))
        }
      } else if(!(policyPeriod?.Job?.ChangeReasons?.hasMatch(\changeReason -> changeReason?.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN))){
        for (aChangedEntity in matchingChangeReason.ChangedEntities) {
          matchingChangeReason.removeFromChangedEntities(aChangedEntity)
        }

        for (aChangedEntity in aReason.ChangedEntities) {
          matchingChangeReason.addToChangedEntities(aChangedEntity)
        }

      }
    }

    for (aReason in addedReasons) {
      matchingChangeReason = requiredChangeReasons.firstWhere( \ r -> r.ChangeReason == aReason.ChangeReason)
      if (matchingChangeReason == null && !(aReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED || aReason.ChangeReason
          == ChangeReasonType_TDIC.TC_EREPLOFFERNR
          || aReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNA || aReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER ||
          aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED || aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNR
          || aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNA || aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER ||
          aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER || aReason.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM ||
          aReason.ChangeReason == ChangeReasonType_TDIC.TC_REGENERATEDECLARATION || aReason.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI || aReason.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE
          || aReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN || aReason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBDOCGEN)) {
        policyPeriod.Job.removeFromChangeReasons(aReason)
        if (_jobWizardHelper != null) {
          _jobWizardHelper.addInfoWebMessage(DisplayKey.get("TDIC.Validation.ChangeReasons.UnrequiredChangeReasonRemoved", aReason.ChangeReason.DisplayName))
        }
      }
    }
  }

  /**
   * US1274, robk
   * Examines the DiffItems for the specified PolicyPeriod and returns the Change Reasons that should be added to the associated Job.
   */
  private function determineRequiredChangeReasons(policyPeriod : PolicyPeriod) : List<ChangeReason_TDIC> {
    var diffItems = policyPeriod.getDiffItems(DiffReason.TC_POLICYREVIEW)
    var requiredChangeReasons = new java.util.ArrayList<ChangeReason_TDIC>()
    for (diffItem in diffItems) {
      addRequiredChangeReasonsForDiffItem(diffItem, requiredChangeReasons)
    }

    determineFormChangeReasons(policyPeriod, diffItems, requiredChangeReasons);

    return requiredChangeReasons
  }

  // Mapping of form pattern code to change reason type, null indicates that a change reason should not be generated.
  static var _formPatternMap : HashMap<String,ChangeReasonType_TDIC> =
      { "WC040002"  -> ChangeReasonType_TDIC.TC_ADDITIONALNAMEDINSURED,
        "WC040302A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040302B" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040302C" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040302D" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040303"  -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040303A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040303B" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040303C" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040304"  -> ChangeReasonType_TDIC.TC_CONDITIONALENDORSEMENT,
        "WC040305"  -> ChangeReasonType_TDIC.TC_CONDITIONALENDORSEMENT,
        "WC040306"  -> ChangeReasonType_TDIC.TC_CONDITIONALENDORSEMENT,
        "WC040331"  -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040331A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040337"  -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040337A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040341A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040341B" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040342A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040342B" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040343A" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040343B" -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040399"  -> ChangeReasonType_TDIC.TC_EXCLUSIONENDORSEMENT,
        "WC040401"  -> ChangeReasonType_TDIC.TC_ANNIVERSARYRATINGDATE,
        "WC040401A" -> ChangeReasonType_TDIC.TC_ANNIVERSARYRATINGDATE,
        "WC890600B" -> null,
        "WC900000A" -> ChangeReasonType_TDIC.TC_SAFETYGROUP,
        "WC900000B" -> ChangeReasonType_TDIC.TC_SAFETYGROUP
  };

  protected function determineFormChangeReasons (policyPeriod : PolicyPeriod, diffItems : List<DiffItem>,
                                                 requiredChangeReasons : List<ChangeReason_TDIC>) : void {
    var changeReason : ChangeReason_TDIC;
    var changeReasonType : ChangeReasonType_TDIC;
    var changeText : String;
    var formDataPopulated = policyPeriod.PolicyTerm.FormDataPopulated_TDIC;
    var formPatternCode : String;

    for (form in policyPeriod.Forms) {
      formPatternCode = form.FormPatternCode;
      changeText = null;
      if (form.FormAdded_TDIC) {
        changeText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FormAdded", form.FormNumber, form.FormDescription);
      } else if (form.FormUpdated_TDIC) {
        changeText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FormUpdated", form.FormNumber, form.FormDescription);
      } else if (form.FormRemoved_TDIC) {
        changeText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FormRemoved", form.FormNumber, form.FormDescription);
      }

      if (changeText != null) {
        if (_formPatternMap.containsKey(formPatternCode)) {
          changeReasonType = _formPatternMap.get(form.FormPatternCode);
          if (changeReasonType != null) {
            // Get or create change reason
            changeReason = getOrAddRequiredChangeReasonWithType (changeReasonType, requiredChangeReasons);
            changeReason.addChangedEntity(changeText);
          }
        } else {
          LoggerFactory.getLogger("Application.JobProcess").error("ChangeReasons - Unknown change reason type for form pattern code "
                                                + form.FormPatternCode);
        }
      }
    }
  }


  /**
   * US1274, robk
   * Returns the Change Reason that describes the specified DiffItem.
   */
  private function addRequiredChangeReasonsForDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    switch (diffItem.Bean.IntrinsicType) {
      case WC7CoveredEmployee:
          addRequiredChangeReasonsForWC7CoveredEmployeeDiffItem(diffItem, requiredChangeReasons)
          break
      case PolicyLocation:
          addRequiredChangeReasonsForPolicyLocationDiffItem(diffItem, requiredChangeReasons)
          break
      case EffectiveDatedFields:
          addRequiredChangeReasonsForEffectiveDatedFieldsDiffItem(diffItem, requiredChangeReasons)
          break
      case WC7Modifier:
          addRequiredChangeReasonsForWC7ModifierDiffItem(diffItem, requiredChangeReasons)
          break
      case PolicyPriNamedInsured:
          addRequiredChangeReasonsForPolicyPriNamedInsuredDiffItem(diffItem, requiredChangeReasons)
          break
      case PolicyAddress:
          addRequiredChangeReasonsForPolicyAddressDiffItem(diffItem, requiredChangeReasons)
          break
      case WC7RateFactor:
          addRequiredChangeReasonsForWC7RateFactorDiffItem(diffItem, requiredChangeReasons)
          break
      case PolicyLossPayee_TDIC:
        addRequiredChangeReasonsForLossPayeeDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case BOPBuilding:
        addRequiredChangeReasonsForBOPBuildingDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case Building:
        addRequiredChangeReasonsForBuildingDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case PolicyMortgagee_TDIC:
        addRequiredChangeReasonsForMortgageeDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case BOPBuildingCov:
        addRequiredChangeReasonsForBOPBuildingCovDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case BOPBuildingModifier_TDIC:
        addRequiredChangeReasonsForBOPBuildingModifierDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case PolicyPeriod:
        addRequiredChangeReasonsForPolicyPeriodDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GLExposure:
        addRequiredChangeReasonsForGLExposureDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GeneralLiabilityCond:
        addRequiredChangeReasonsForGeneralLiabilityCondDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GeneralLiabilityCov:
        addRequiredChangeReasonsForGeneralLiabilityCovDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case NamedInsured:
        addRequiredChangeReasonsForNamedInsuredDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case PolicyAddlInsuredDetail:
        addRequiredChangeReasonsForAdditionalInsuredDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case PolicyCertificateHolder_TDIC:
        addRequiredChangeReasonsForGLCOIDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GLDentalBLAISched_TDIC:
        addRequiredChangeReasonsForGLDentalAIDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GLAdditionInsdSched_TDIC:
        addRequiredChangeReasonsForPLAddnlInsDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case GLNameODSched_TDIC:
        addRequiredChangeReasonsForGLNameOTDCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
      case GLExcludedServiceSched_TDIC:
          addRequiredChangeReasonsForGLExcludedServiceCond_TDICDiffItem_TDIC(diffItem, requiredChangeReasons);
      default:
        // Nothing to do
    }
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified WC7CoveredEmployee DiffItem.
   */
  private function addRequiredChangeReasonsForWC7CoveredEmployeeDiffItem(diffItem: DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == WC7CoveredEmployee.Type.TypeInfo.getProperty("ClassCode")) {
        addRequiredChangeReasonsForAddedClassCode((diffItem.Bean as WC7CoveredEmployee).ClassCode.Code,(diffItem.Bean as WC7CoveredEmployee).Location, requiredChangeReasons)
        addRequiredChangeReasonsForRemovedClassCode((diffItem.Bean as WC7CoveredEmployee).BasedOn.ClassCode.Code,(diffItem.Bean as WC7CoveredEmployee).Location, requiredChangeReasons)
      }
      else if (changedProperty == WC7CoveredEmployee.Type.TypeInfo.getProperty("BasisAmount")) {
        addRequiredChangeReasonsForChangedBasisAmount(requiredChangeReasons)
      }
    }
    else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonsForAddedClassCode((diffItem.Bean as WC7CoveredEmployee).ClassCode.Code,(diffItem.Bean as WC7CoveredEmployee).Location, requiredChangeReasons)
      addRequiredChangeReasonsForChangedBasisAmount(requiredChangeReasons)
    }
    else {
      addRequiredChangeReasonsForRemovedClassCode((diffItem.Bean as WC7CoveredEmployee).ClassCode.Code,(diffItem.Bean as WC7CoveredEmployee).Location, requiredChangeReasons)
      addRequiredChangeReasonsForChangedBasisAmount(requiredChangeReasons)
    }
  }

  /**
   * DE215, robk
   * Adds the change reasons that describe an added class code with the specified code.
   */
  private function addRequiredChangeReasonsForAddedClassCode(addedClassCodeCode : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_CLASSCODE, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClassCodeAdded", addedClassCodeCode)
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForAddedClassCode(addedClassCodeCode : String, policyLocation: PolicyLocation, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_CLASSCODE, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClassCodeAddedToLocation", addedClassCodeCode, policyLocation.LocationNum)
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * DE215, robk
   * Adds the change reasons that describe an added class code with the specified code.
   */
  private function addRequiredChangeReasonsForRemovedClassCode(removedClassCodeCode : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_CLASSCODE, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClassCodeRemoved", removedClassCodeCode)
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForRemovedClassCode(removedClassCodeCode : String, policyLocation: PolicyLocation, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_CLASSCODE, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClassCodeRemovedFromLocation", removedClassCodeCode,policyLocation.LocationNum)
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * DE215, robk
   * Adds the change reasons that describe a changed basis amount.
   */
  private function addRequiredChangeReasonsForChangedBasisAmount(requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_PAYROLL, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.Payroll")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified PolicyLocation DiffItem.
   */
  private function addRequiredChangeReasonsForPolicyLocationDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var policyLocation = (diffItem.Bean as PolicyLocation)
    var primaryNamedInsuredName = policyLocation.Branch.PrimaryNamedInsured.DisplayName
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == PolicyLocation.Type.TypeInfo.getProperty("DBA_TDIC")) {
        if (policyLocation.BasedOn.DBA_TDIC == null) {
          addRequiredChangeReasonForDBAAdd(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
        } else if (policyLocation.DBA_TDIC == null) {
          addRequiredChangeReasonForDBARemove(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
        }
        else {
          addRequiredChangeReasonForDBAChange(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
        }
      }
          // DE268: Changes to TDIC RI properties should not trigger change reasons
      else if (not isChangeOnTDICRIProperty(changedProperty)) {
        addRequiredChangeReasonForPolicyLocationChange(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
      }
    }
    else if (diffItem typeis DiffAdd) {
      if (policyLocation.DBA_TDIC != null) {
        addRequiredChangeReasonForDBAAdd(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
      }
      addRequiredChangeReasonForPolicyLocationAdd(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
    }
    else {
      if (policyLocation.DBA_TDIC != null) {
        addRequiredChangeReasonForDBARemove(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
      }
      addRequiredChangeReasonForPolicyLocationRemove(policyLocation, primaryNamedInsuredName, requiredChangeReasons)
    }
  }
  /**
   * US1274, robk
   * Adds the change reason that describes a DBA change.
   */
  private function addRequiredChangeReasonForDBAChange(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_DBA, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBAUpdated", policyLocation.LocationNum, policyLocation.DBA_TDIC))
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a DBA add.
   */
  private function addRequiredChangeReasonForDBAAdd(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_DBA, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBAAdded", policyLocation.LocationNum, policyLocation.DBA_TDIC))
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a DBA remove.
   */
  private function addRequiredChangeReasonForDBARemove(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_DBA, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBARemoved", policyLocation.LocationNum, policyLocation.DBA_TDIC))
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified EffectiveDatedFields DiffItem.
   */
  private function addRequiredChangeReasonsForEffectiveDatedFieldsDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var effectiveDatedFields = diffItem.Bean as EffectiveDatedFields
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == EffectiveDatedFields.Type.TypeInfo.getProperty("PolicyOrgType_TDIC")) {
        addRequiredChangeReasonForPolicyOrgTypeChange(effectiveDatedFields.PolicyOrgType_TDIC, requiredChangeReasons)
      }
      else if (changedProperty == EffectiveDatedFields.Type.TypeInfo.getProperty("PrimaryLocation")) {
        addRequiredChangeReasonForPolicyLocationChange(effectiveDatedFields.Branch.PolicyLocations, effectiveDatedFields.Branch.PrimaryNamedInsured.DisplayName, requiredChangeReasons)
      }
    }
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a PolicyOrgType change.
   */
  private function addRequiredChangeReasonForPolicyOrgTypeChange(policyOrgType : typekey.PolicyOrgType_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_ENTITYORGANIZATIONTYPE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.OrganizatonType", policyOrgType))
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a PolicyLocation Change.
   */
  private function addRequiredChangeReasonForPolicyLocationChange(policyLocations : PolicyLocation[], primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_LOCATION, requiredChangeReasons)
    var allLocationNames = constructAllLocationNamesString(policyLocations, primaryNamedInsuredName)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.Location", allLocationNames))
  }

  private function addRequiredChangeReasonForPolicyLocationChange(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_LOCATION, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocationChanged", policyLocation.LocationNum, policyLocation.addressString(",", false, false)))
  }

  private function addRequiredChangeReasonForPolicyLocationAdd(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_LOCATION, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocationAdded", policyLocation.LocationNum, policyLocation.addressString(",", false, false)))
  }

  private function addRequiredChangeReasonForPolicyLocationRemove(policyLocation : PolicyLocation, primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_LOCATION, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocationRemoved", policyLocation.LocationNum, policyLocation.addressString(",", false, false)))
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified WC7Modifier DiffItem.
   */
  private function addRequiredChangeReasonsForWC7ModifierDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var mod = diffItem.Bean as WC7Modifier
    var requiredChangeReason : ChangeReason_TDIC
    var isChangedToReadText : String
    if (mod.PatternCode == "WC7ExpMod") {
      addRequiredChangeReasonsForWC7ExpModDiffItem(diffItem, requiredChangeReasons)
    } else if (mod.PatternCode == "WC7TDICMultiLineDiscount") {
      addRequiredChangeReasonsForWC7TDICMultiLineDiscountDiffItem(diffItem, requiredChangeReasons)
    }
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified WC7ExpMod DiffItem.
   */
  private function addRequiredChangeReasonsForWC7ExpModDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var mod = diffItem.Bean as WC7Modifier
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_EXPERIENCEMODIFICATION, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ExperienceModifierChanged", formatValue("0.00", RoundingMode.DOWN, mod.RateModifier))
    }
    else if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ExperienceModifierAdded", formatValue("0.00", RoundingMode.DOWN, mod.RateModifier))
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ExperienceModifierRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  /**
   * US1274, robk
   * Adds the change reasons that describe the specified WC7TDICMultiLineDiscount DiffItem.
   */
  private function addRequiredChangeReasonsForWC7TDICMultiLineDiscountDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var mod = diffItem.Bean as WC7Modifier
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_MULTILINEDISCOUNT, requiredChangeReasons)
    var isChangedToReadText : String
    if (mod.BooleanModifier) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultilineDiscountAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultilineDiscountRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified PolicyPriNamedInsured DiffItem.
   */
  // Amended TaxID -- SSN/FEIN filed mapping and amended PersonFEIN_TDIC usage as part of GW-1623 & GW-753
  private function addRequiredChangeReasonsForPolicyPriNamedInsuredDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var primaryNamedInsured = (diffItem.Bean as PolicyPriNamedInsured)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == PolicyPriNamedInsured.Type.TypeInfo.getProperty("TaxIDInternal_TDIC")) {
        addRequiredChangeReasonForFEINChange(primaryNamedInsured.TaxID, requiredChangeReasons)
      }
      else {
        addRequiredChangeReasonForPrimaryNamedInsuredChange(primaryNamedInsured.DisplayName, requiredChangeReasons)
      }
    }
    else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForFEINChange(primaryNamedInsured.TaxID, requiredChangeReasons)
      addRequiredChangeReasonForPrimaryNamedInsuredChange(primaryNamedInsured.DisplayName, requiredChangeReasons)
    }
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a FEIN change.
   */
  private function addRequiredChangeReasonForFEINChange(FEIN : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_FEIN, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FEIN", FEIN))
  }

  /**
   * US1274, robk
   * Adds the change reason that describes a PrimaryNamedInsured change.
   */
  private function addRequiredChangeReasonForPrimaryNamedInsuredChange(primaryNamedInsuredName : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_NAMEDINSURED, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.PrimaryNamedInsured", primaryNamedInsuredName))
  }




  /**
   * US1274, robk
   * Adds the change reasons that describe the specified PolicyAddress DiffItem.
   */
  private function addRequiredChangeReasonsForPolicyAddressDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var policyAddress = (diffItem.Bean as PolicyAddress)
    if (diffItem typeis DiffProperty) {
      var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_MAILINGADDRESS, requiredChangeReasons)
      var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MailingAddress", policyAddress.addressString(",", false, false))
      requiredChangeReason.addChangedEntity(isChangedToReadText)
    }
  }

  /**
   * US1274, robk
   * Adds the change reasons that describe the specified WC7RateFactor DiffItem.
   */
  private function addRequiredChangeReasonsForWC7RateFactorDiffItem(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var rateFactor = (diffItem.Bean as WC7RateFactor)
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(typekey.ChangeReasonType_TDIC.TC_SCHEDULERATING, requiredChangeReasons)
    var isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ScheduleRating", rateFactor.WC7Modifier.RateModifier)
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * US1274, robk
   * Returns the change reason with the specified type from the specified list of change reasons.
   * If no matching change reason is found a new change reason with the specified type is created, added to the list and returned.
   */
  private function getOrAddRequiredChangeReasonWithType(aChangeReasonType : typekey.ChangeReasonType_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) : ChangeReason_TDIC {
    var requiredChangeReason : ChangeReason_TDIC
    if (requiredChangeReasons != null and !(requiredChangeReasons.hasMatch(\cr -> cr.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN))) {
      requiredChangeReason = requiredChangeReasons.firstWhere( \ r -> r.ChangeReason == aChangeReasonType)
      if (requiredChangeReason == null) {
        requiredChangeReason = createNewChangeReason(aChangeReasonType)
        requiredChangeReasons.add(requiredChangeReason)
      }
    }
    return requiredChangeReason
  }

  /**
   * DE268, robk
   * Returns true if the specified changed property is a TDIC Reinsurance property; false otherwise.
   */
  private function isChangeOnTDICRIProperty(changedProperty : gw.lang.reflect.IPropertyInfo) : boolean {
    return changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ReinsuranceSearchTag_TDIC")
        or changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ControlAddressLine1_TDIC")
        or changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ControlAddressCity_TDIC")
        or changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ControlAddressPostalCode_TDIC")
        or changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ControlAddressState_TDIC")
        or changedProperty == PolicyLocation.Type.TypeInfo.getProperty("ControlAddressRISearchTag_TDIC")
  }

  /**
   * US1274, robk
   * Returns a string representation from the Names of the specified PolicyLocations.
   * If a PolicyLocation does not have a Name, the specified primaryNamedInsuredName is used as the Name.
   */
  private function constructAllLocationNamesString(policyLocations : PolicyLocation[], primaryNamedInsuredName : String) : String {
    var allLocationNamesBuilder = new java.lang.StringBuilder()
    policyLocations?.eachWithIndex( \ aLocation, i -> {
      if (aLocation.DisplayName != null)
        allLocationNamesBuilder.append(aLocation.DisplayName)
      else
        allLocationNamesBuilder.append(primaryNamedInsuredName)
      if(i != policyLocations.length-1)
        allLocationNamesBuilder.append(" | ")

    })
    return allLocationNamesBuilder.toString()
  }

  /**
   * US1274, robk
   * Returns a new ChangeReason_TDIC for the specified change type.
   */
  private function createNewChangeReason(aReasonType: typekey.ChangeReasonType_TDIC) : ChangeReason_TDIC {
    var newChangeReason = new ChangeReason_TDIC()
    newChangeReason.ChangeReason = aReasonType
    return newChangeReason
  }

  private function formatValue(format: String, mode: RoundingMode, value: BigDecimal) : String {
    var df = new DecimalFormat(format)
    df.setRoundingMode(mode)
    return df.format(value)
  }
  /*-----------------------------------------------------------
      Methods Added Newly for BOP Policy Change
    ---------------------------------------------------------
   */

  private function addRequiredChangeReasonsForPolicyPeriodDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var policyPeriod = (diffItem.Bean as PolicyPeriod)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == PolicyPeriod.Type.TypeInfo.getProperty("MultiLineDiscount_TDIC")) {
        addRequiredChangeReasonForPolicyPeriodChange_TDIC(policyPeriod.MultiLineDiscount_TDIC, requiredChangeReasons)
      }
    }
    else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForPolicyPeriodChange_TDIC(policyPeriod.MultiLineDiscount_TDIC, requiredChangeReasons)
    }
  }
 /**
  * create by: ChitraK
  * @description: LOSSPAYEE
  * @create time: 5:11 PM 11/22/2019
  * @return:
  */

  private function addRequiredChangeReasonsForLossPayeeDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var policyLossPayee_tdic = (diffItem.Bean as PolicyLossPayee_TDIC)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      addRequiredChangeReasonForLossPayeeChange_TDIC(policyLossPayee_tdic.DisplayName, requiredChangeReasons)
    } else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForLossPayeeChange_TDIC(policyLossPayee_tdic.DisplayName, requiredChangeReasons)
    }
  }

  private function addRequiredChangeReasonForLossPayeeChange_TDIC(policyLossPayee : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_LOSSPAYEE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LossPayee", policyLossPayee))
  }

  private function addRequiredChangeReasonForPolicyPeriodChange_TDIC(multiDiscount : MultiLineDiscount_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MULTILINEDISCOUNT, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultilineDiscountAdded", multiDiscount))
  }
  /*
   * create by: SureshB
   * @description: method to add the change reason for ClassCode_TDIC
   * @create time: 7:05 PM 11/26/2019
    * @param ClassCode_TDIC, List<ChagneReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonForClassCodeChange_TDIC(classCode : ClassCode_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CLASSCODESPECIALTYCHANGE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClassCodeChanged", classCode))
  }
    /*
   * create by: SureshB
   * @description: method to add the change reason for Specialitycode_TDIC
   * @create time: 7:05 PM 11/26/2019
    * @param SpecialityCode_TDIC, List<ChagneReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonForSpecialityCodeChange_TDIC(specialityCode : SpecialityCode_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CLASSCODESPECIALTYCHANGE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.SpecialityCodeCodeChanged", specialityCode))
  }
      /*
   * create by: SureshB
   * @description: method to add the change reason for GLTerritory_TDIC
   * @create time: 7:05 PM 11/26/2019
    * @param GLTerritory_TDIC, List<ChagneReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonForComponentChange_TDIC(component : GLTerritory_TDIC, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_COMPONENTCHANGE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ComponentCodeCodeChanged", component))
  }
  /*
   * create by: SureshB
   * @description: method to add required reason for GLCovExtDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLCovExtDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_COVERAGEEXTENSIONPROVISION, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CoverageExtensionDiscountAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CoverageExtensionDiscountRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
    /*
   * create by: SureshB
   * @description: method to add required reason for GLFullTimeFacultyDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLFullTimeFacultyDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_FULLTIMEFACULTYMEMBER, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimeFacultyMemmberDiscountAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimeFacultyMemmberDiscountRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimeFacultyMemmberDiscountModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
      /*
   * create by: SureshB
   * @description: method to add required reason for GLFullTimePostGradDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLFullTimePostGradDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_FULLTIMEPOSTGRADUATESTUDENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimePostGraduateDiscountAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimePostGraduateDiscountRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FullTimePostGraduateDiscountModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
        /*
   * create by: SureshB
   * @description: method to add required reason for GLNewGradDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLNewGradDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NEWGRADUATE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NewGraduateDiscountAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NewGraduateDiscountRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NewGraduateDiscountModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
          /*
   * create by: SureshB
   * @description: method to add required reason for GLNJNonMemberDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLNJNonMemberDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NONMEMBERSURCHARGE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJNonMemberDiscount_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJNonMemberDiscount_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJNonMemberDiscount_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
            /*
   * create by: SureshB
   * @description: method to add required reason for GLPartTimeDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLPartTimeDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_PARTTIMEDISCOUNT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLPartTimeDiscount_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLPartTimeDiscount_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLPartTimeDiscount_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
              /*
   * create by: SureshB
   * @description: method to add required reason for GLRiskMgmtDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLRiskMgmtDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_RISKMANAGEMENTDISCOUNT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLRiskMgmtDiscount_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLRiskMgmtDiscount_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLRiskMgmtDiscount_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                /*
   * create by: SureshB
   * @description: method to add required reason for GLServiceMembersCRADiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLServiceMembersCRADiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SERVICEMEMBERSCIVILRELIEFACT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLServiceMembersCRADiscount_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLServiceMembersCRADiscount_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLServiceMembersCRADiscount_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                  /*
   * create by: SureshB
   * @description: method to add required reason for GLTempDisDiscount_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLTempDisDiscount_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DISABILITYDISCOUNT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLTempDisDiscount_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLTempDisDiscount_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLTempDisDiscount_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
    /*
   * create by: SureshB
   * @description: method to add required reason for GLDentalEmpPracLiabCov_TDIC
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLDentalEmpPracLiabCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DENTALEMPLOYMENTPRACTICESLIABILITYEPLI, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovDAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovDRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovDModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
      /*
   * create by: SureshB
   * @description: method to add required reason for "GLExtendedReportPeriodCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLExtendedReportPeriodCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ERPEAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ERPERemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ERPEModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
        /*
   * create by: SureshB
   * @description: method to add required reason for "GLExtendedReportPeriodDPLCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLExtendedReportPeriodDPLCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EREDPL, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ERPEDEPLAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ERPEDEPLRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
          /*
   * create by: SureshB
   * @description: method to add required reason for "GLIDTheftREcoveryCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLIDTheftREcoveryCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_IDENTITYTHEFTRECOVERYIDR, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.IdentityTheftRecoveryCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.IdentityTheftRecoveryCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.IdentityTheftRecoveryCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  /*
   * create by: ChitraK
   * @description: method to add required reason for "CyberLiability"
   * @create time: 9:00 PM 02/12/2020
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLCyberLiabCovDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_LIMITSINCREASEDECREASE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CybLiabilityAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CybLiabilityRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CybLiabilityModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /*
   * create by: SureshB
   * @description: method to add required reason for "GLDentistProfLiabCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLDentistProfLiabCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_LIMITSINCREASEDECREASE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovAAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovARemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.CovAModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
              /*
   * create by: SureshB
   * @description: method to add required reason for "GLLocumTenensCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLLocumTenensCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_LOCUMTENENS, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocumTenesCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocumTenesCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LocumTenesCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                /*
   * create by: SureshB
   * @description: method to add required reason for "GLManuscriptEndor_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLManuscriptEndor_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ManuscriptEndorsementCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ManuscriptEndorsementCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ManuscriptEndorsementCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                  /*
   * create by: SureshB
   * @description: method to add required reason for "GLMobileDentalClinicCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLMobileDentalClinicCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MOBILECLINIC, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MobileDentalClinicEndorsementCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MobileDentalClinicEndorsementCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MobileDentalClinicEndorsementCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                    /*
   * create by: SureshB
   * @description: method to add required reason for "GLMultiOwnerDPECov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLMultiOwnerDPECov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MULTIOWNERDENTALPRACTICEENTITY, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultiOwnerDentalPracticeEntityEndorsementCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultiOwnerDentalPracticeEntityEndorsementCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MultiOwnerDentalPracticeEntityEndorsementCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                      /*
   * create by: SureshB
   * @description: method to add required reason for "GLNameOTDCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLNameOTDCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NAMEONTHEDOOR, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NameOnTheDoorEndorsementCovAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NameOnTheDoorEndorsementCovRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NameOnTheDoorEndorsementCovModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                        /*
   * create by: SureshB
   * @description: method to add required reason for "GLNJWaiverCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLNJWaiverCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NEWJERSEYWAIVEROFRIGHTTOSETTLE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJWaiverCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJWaiverCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJWaiverCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                          /*
   * create by: SureshB
   * @description: method to add required reason for "GLNJDeductibleCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLNJDeductibleCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NEWJERSEYPLDEDUCTIBLE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJDeductibleCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJDeductibleCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNJDeductibleCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                            /*
   * create by: SureshB
   * @description: method to add required reason for "GLSchoolServicesCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLSchoolServicesCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SCHOOLSERVICESENDORSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSchoolServicesCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSchoolServicesCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSchoolServicesCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
                              /*
   * create by: SureshB
   * @description: method to add required reason for "GLSpecialEventCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLSpecialEventCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SPECIALEVENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSpecialEventCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSpecialEventCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLSpecialEventCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  /*
   * create by: SureshB
   * @description: method to add required reason for "GLStateExclCov_TDIC"
   * @create time: 9:00 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLStateExclCov_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_STATEEXCLUSION, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLStateExclCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLStateExclCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLStateExclCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForGLExcludedServiceCond_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EXCLUDEDSERVICEENDORSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLExcludedServiceCond_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLExcludedServiceCond_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLExcludedServiceCond_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  private function addRequiredChangeReasonsForGLNewtoCompany_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NEWTOCOMPANY, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNewtoComp_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNewtoComp_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLNewtoComp_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForGLCOICovDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CERTINSURANCEPDL, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLCOICov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLCOICov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLCOICov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForGLDBLCovDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DENTALBUSINESSLIABAI, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLDBLCov_TDICAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLDBLCov_TDICRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GLDBLCov_TDICModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  /**
   * create by: ChitraK
   * @description: MORTGAGEE
   * @create time: 5:13 PM 11/22/2019
   * @return:
   */

  private function addRequiredChangeReasonsForMortgageeDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var policyMortgagee_tdic = (diffItem.Bean as PolicyMortgagee_TDIC)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      addRequiredChangeReasonForMortgageeChange_TDIC(policyMortgagee_tdic.DisplayName, requiredChangeReasons)
    }else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForMortgageeChange_TDIC(policyMortgagee_tdic.DisplayName, requiredChangeReasons)
    }
  }

  private function addRequiredChangeReasonForMortgageeChange_TDIC(policyMortgagee : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MORTGAGEE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.Mortgagee", policyMortgagee))
  }
 /**
  * create by: ChitraK
  * @description: Building Construction Type
  * @create time: 5:14 PM 11/22/2019
  * @return:
  */

  private function addRequiredChangeReasonsForBOPBuildingDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var building = (diffItem.Bean as BOPBuilding)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("ConstructionType").DisplayName) {
        addRequiredChangeReasonForContTypeChange_TDIC(building.ConstructionType.DisplayName, requiredChangeReasons)
      }
    }
    else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForContTypeChange_TDIC(building.ConstructionType.DisplayName, requiredChangeReasons)
    }
  }

  private function addRequiredChangeReasonsForBuildingDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var building = (diffItem.Bean as Building)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("TerritoryDetails_TDIC").DisplayName) {
        addRequiredChangeReasonForProtectionChange_TDIC(building.TerritoryDetails_TDIC.DisplayName, requiredChangeReasons)
      }
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("ProtectionClass_TDIC").DisplayName) {
        addRequiredChangeReasonForProtectionChange_TDIC(building.ProtectionClass_TDIC.DisplayName, requiredChangeReasons)
      }
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("Sprinklered_TDIC").DisplayName) {
        addRequiredChangeReasonForSprinklerChange_TDIC(building.Sprinklered_TDIC.toString(), requiredChangeReasons)
      }
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("TotalArea").DisplayName) {
        addRequiredChangeReasonForTotalAreaChange_TDIC(building.TotalArea.toString(), requiredChangeReasons)
      }
      if (changedProperty.DisplayName == Building.Type.TypeInfo.getProperty("TotalOfficeArea_TDIC").DisplayName) {
        addRequiredChangeReasonForTotalOfficeAreaChange_TDIC(building.TotalOfficeArea_TDIC.toString(), requiredChangeReasons)
      }
    } else if (diffItem typeis DiffAdd) {
      addRequiredChangeReasonForProtectionChange_TDIC(building.ProtectionClass_TDIC.DisplayName, requiredChangeReasons)
    }
  }


  private function addRequiredChangeReasonForContTypeChange_TDIC(consType : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CONSTRUCTIONTYPE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ConstructionType", consType))
  }
  private function addRequiredChangeReasonForProtectionChange_TDIC(consType : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_PROTECTIONCLASSTERRITORYCHANGE, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ProtectionTerritoryType"))
  }
  private function addRequiredChangeReasonForSprinklerChange_TDIC(consType : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SPRINKLER, requiredChangeReasons)
    requiredChangeReason.addChangedEntity(DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.SprinklerChange"))
  }
  private function addRequiredChangeReasonForTotalAreaChange_TDIC(consType : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SQFT, requiredChangeReasons)
    var isChangeToReadText = "Sq Feet Changed"
    requiredChangeReason.addChangedEntity(isChangeToReadText)
  }
  private function addRequiredChangeReasonForTotalOfficeAreaChange_TDIC(consType : String, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SQFT, requiredChangeReasons)
    var isChangeToReadText = "Sq Feet Changed"
    requiredChangeReason.addChangedEntity(isChangeToReadText)
  }
  /**
   * create by: ChitraK
   * @description: Building Coverages
   * @create time: 5:15 PM 11/22/2019
   * @return:4
   */

  private function addRequiredChangeReasonsForBOPBuildingCovDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var bopBuildingCov = diffItem.Bean as BOPBuildingCov
    var requiredChangeReason : ChangeReason_TDIC
    var isChangedToReadText : String
    switch(bopBuildingCov.Pattern.CodeIdentifier){
    //Actual Cash Value Endorsement
    case "BOPACVEndCov_TDIC" :
      addRequiredChangeReasonsForBOPACVDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    //Deductible Limit Change
    case "BOPDeductibleCov_TDIC" :
      addRequiredChangeReasonsForBOPDedDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    //Employee Dishonesty Change
    case "BOPEmpDisCov_TDIC" :
      addRequiredChangeReasonsForBOPEmpDishonestyDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    //Extra Expense
    case "BOPExtraExpenseCov_TDIC" :
      addRequiredChangeReasonsForBOPExtraExpenseDiffItem_TDIC(diffItem, requiredChangeReasons)
    break
    //Loss Of Income
    case "BOPLossofIncomeTDIC" :
      addRequiredChangeReasonsForBOPLossofIncomeDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPManuscriptEndorsement_TDIC" :
      addRequiredChangeReasonsForBOPManuscriptDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPGoldPreMetals_TDIC" :
      addRequiredChangeReasonsForBOPGoldDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPValuablePapersCov_TDIC" :
      addRequiredChangeReasonsForBOPValuablePapers_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPEqBldgCov" :
      addRequiredChangeReasonsForBOPEQEQSLCov_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPEqSpBldgCov" :
      addRequiredChangeReasonsForBOPEQEQSLCov_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPMoneySecCov_TDIC" :
      addRequiredChangeReasonsForBOPMoneyDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPFineArtsCov_TDIC" :
      addRequiredChangeReasonsForBOPFineArtsDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPFungiCov_TDIC" :
      addRequiredChangeReasonsForBOPFungiDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPILMineSubCov_TDIC" :
      addRequiredChangeReasonsForBOPILMineSubDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPAccReceivablesCov_TDIC" :
      addRequiredChangeReasonsForBOPAccRecDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPPersonalPropCov" :
      addRequiredChangeReasonsForBOPBPPCoverageDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPBuildingCov" :
      addRequiredChangeReasonsForBOPBuildingCoverageDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
    case "BOPSigns_TDIC" :
      addRequiredChangeReasonsForBOPSignsCoverageDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
      case "BOPDentalGenLiabilityCov_TDIC" :
        addRequiredChangeReasonsForDGLAIDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "BOPBuildingOwnersLiabCov_TDIC" :
        addRequiredChangeReasonsForBOLAIDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "BOPEncCovEndtCond_TDIC" :
        addRequiredChangeReasonsForBOPEncCovEndtCond_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "BOPWASTOPGAP_TDIC" :
        addRequiredChangeReasonsForBOPEncCovEndtCond_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break




    }

  }

  private function addRequiredChangeReasonsForBOPACVDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_ACTUALCASHVALUEENDROSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ACVAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ACVRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPDedDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DEDUCTIBLECHANGE, requiredChangeReasons)
    var isChangedToReadText : String
     isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DeductibleAdded")
     requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPEmpDishonestyDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EMPLOYEEDISHONESTYCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.EmpDisAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPExtraExpenseDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EXTRAEXPENSE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ExtraExpAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPLossofIncomeDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_LOSSOFINCOMECHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.LossofIncomeAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPManuscriptDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ManuscriptAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ManuscriptRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }


  private function addRequiredChangeReasonsForBOPEncCovEndtCond_TDICDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_ENHANCEDCOVERAGEENDORSEMENT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.EnhancedCoverageEndorsementAdded")
    }
    else if (diffItem typeis DiffRemove){
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.EnhancedCoverageEndorsementRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPGoldDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_GOLDANDOTHERPRECIOUSMETALS, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GoldAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPValuablePapers_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_VALUABLEPAPERSCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.GoldAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPEQEQSLCov_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_EQEQSL, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.EQEQSLAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.EQEQSLRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPMoneyDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_MONEYANDSECURITIESCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.MoneyAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPFineArtsDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_FINEARTSCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FineArtsAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPFungiDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_FUNGIMOLDCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.FungiAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPILMineSubDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_ILMINESUBSIDENCE, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ILAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ILChanged")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPAccRecDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_ACCOUNTSRECEIVABLECHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.AccRecAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPBuildingCoverageDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_BLDGLIMITSINCREASEDECREASE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.BuildingCoverageAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPSignsCoverageDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_SIGNSCOVERAGELIMITCHANGE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.SignsCoverageAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForDGLAIDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DENTALGENERALLIAB, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DGLAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DGLRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOLAIDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_BUILDINGOWNERSLIAB, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DGLAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DGLRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForBOPBPPCoverageDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_BPPLIMITSINCREASEDECREASE, requiredChangeReasons)
    var isChangedToReadText : String
    isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.BPPCoverageAdded")
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }


  private function addRequiredChangeReasonsForBOPBuildingModifierDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var bopBuildingmod = diffItem.Bean as BOPBuildingModifier_TDIC
    if (bopBuildingmod.Pattern.CodeIdentifier == "BOPClosedEndCredit_TDIC") {
      addRequiredChangeReasonsForBOPCLosedEndCreditDiffItem_TDIC(diffItem, requiredChangeReasons)
    }
  }

  private function addRequiredChangeReasonsForBOPCLosedEndCreditDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var mod = diffItem.Bean as BOPBuildingModifier_TDIC
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CLOSEDENDWATERCREDIT, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClosedEndAdded")
    }
    else {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.ClosedEndRemoved")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  /*
   * create by: SureshB
   * @description: method to add the required change reasons for GLExposure entity
   * @create time: 7:12 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForGLExposureDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var glExposure = (diffItem.Bean as GLExposure)
    if (diffItem typeis DiffProperty) {
      var changedProperty = diffItem.PropertyInfo
      if (changedProperty == GLExposure.Type.TypeInfo.getProperty("ClassCode_TDIC")) {
        addRequiredChangeReasonForClassCodeChange_TDIC(glExposure.ClassCode_TDIC, requiredChangeReasons)
      }
      if (changedProperty == GLExposure.Type.TypeInfo.getProperty("SpecialityCode_TDIC")) {
        addRequiredChangeReasonForSpecialityCodeChange_TDIC(glExposure.SpecialityCode_TDIC, requiredChangeReasons)
      }
      if (changedProperty == GLExposure.Type.TypeInfo.getProperty("GLTerritory_TDIC")) {
        addRequiredChangeReasonForComponentChange_TDIC(glExposure.GLTerritory_TDIC, requiredChangeReasons)
      }
    }else if(diffItem typeis DiffAdd) {
      var exposure = glExposure.Branch.GLLine.Exposures.first()
      var basedOnExposure = exposure.BasedOn
      if (exposure.ClassCode_TDIC != basedOnExposure.ClassCode_TDIC) {
        addRequiredChangeReasonForClassCodeChange_TDIC(glExposure.ClassCode_TDIC, requiredChangeReasons)
      }
      if (exposure.SpecialityCode_TDIC != basedOnExposure.SpecialityCode_TDIC) {
        addRequiredChangeReasonForSpecialityCodeChange_TDIC(glExposure.SpecialityCode_TDIC, requiredChangeReasons)
      }
      if (exposure.GLTerritory_TDIC != basedOnExposure.GLTerritory_TDIC) {
        addRequiredChangeReasonForComponentChange_TDIC(glExposure.GLTerritory_TDIC, requiredChangeReasons)
      }
    }
  }
    /*
   * create by: SureshB
   * @description: method to add the required change reasons for NamedInsured entity
   * @create time: 7:12 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForNamedInsuredDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_NAMEDINSURED, requiredChangeReasons)
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NamedInsuredAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NamedInsuredRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.NamedInsuredModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
      /*
   * create by: SureshB
   * @description: method to add the required change reasons for AdditionalInsured entity
   * @create time: 7:12 PM 11/26/2019
    * @param DiffItem, List<ChangeReason_TDIC>
   * @return:
   */

  private function addRequiredChangeReasonsForAdditionalInsuredDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason : ChangeReason_TDIC
    var policyAI = diffItem.Bean as PolicyAddlInsuredDetail
    if(policyAI.Branch.BOPLineExists) {
      if (policyAI.Branch.BOPLine.BOPLocations.where(\elt -> elt.Buildings.first().BOPBuildingOwnersLiabCov_TDICExists).Count > 0) {
        requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_BUILDINGOWNERSLIABAI, requiredChangeReasons)
      } else if (policyAI.Branch.BOPLine.BOPLocations.where(\elt -> elt.Buildings.first().BOPDentalGenLiabilityCov_TDICExists).Count > 0) {
        requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DENTALGENERALLIABAI, requiredChangeReasons)
      }
    }
    else {
      requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_ADDITIONALINSURED, requiredChangeReasons)
    }
    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.AdditionalInsuredAdded")
    }
    else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.AdditionalInsuredRemoved")
    }
    else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.AdditionalInsuredModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForGLCOIDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason : ChangeReason_TDIC
    var bopBuilding = diffItem.Bean as PolicyCertificateHolder_TDIC
    requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_CERTINSURANCEPDL, requiredChangeReasons)

    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.COIAdded")
    } else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.COIRemoved")
    } else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.COIModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForGLDentalAIDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason : ChangeReason_TDIC
    var bopBuilding = diffItem.Bean as GLDentalBLAISched_TDIC
    requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_DENTALBUSINESSLIABAI, requiredChangeReasons)

    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBAIAdded")
    } else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBAIRemoved")
    } else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.DBAIModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }

  private function addRequiredChangeReasonsForPLAddnlInsDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var requiredChangeReason : ChangeReason_TDIC
    //var bopBuilding = diffItem.Bean as GLAdditionInsdSched_TDIC
    requiredChangeReason = getOrAddRequiredChangeReasonWithType(ChangeReasonType_TDIC.TC_PROFLIABAI, requiredChangeReasons)

    var isChangedToReadText : String
    if (diffItem typeis DiffAdd) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.PLAIAdded")
    } else if (diffItem typeis DiffRemove) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.PLAIRemoved")
    } else if (diffItem typeis DiffProperty) {
      isChangedToReadText = DisplayKey.get("TDIC.Validation.ChangeReasons.IsChangedToRead.PLAIModified")
    }
    requiredChangeReason.addChangedEntity(isChangedToReadText)
  }
  /*
 * create by: SureshB
 * @description: method to add the required change reasons for GeneralLiabilityCond
 * @create time: 7:12 PM 11/26/2019
  * @param DiffItem, List<ChangeReason_TDIC>
 * @return:
 */
  private function addRequiredChangeReasonsForGeneralLiabilityCondDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var generalLiabilityCond = (diffItem.Bean as GeneralLiabilityCond)
    switch (generalLiabilityCond.Pattern.CodeIdentifier) {
      case "GLCovExtDiscount_TDIC":
        addRequiredChangeReasonsForGLCovExtDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLFullTimeFacultyDiscount_TDIC":
        addRequiredChangeReasonsForGLFullTimeFacultyDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
      break
      case "GLFullTimePostGradDiscount_TDIC":
        addRequiredChangeReasonsForGLFullTimePostGradDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNewGradDiscount_TDIC":
        addRequiredChangeReasonsForGLNewGradDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNJNonMemberDiscount_TDIC":
        addRequiredChangeReasonsForGLNJNonMemberDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case"GLPartTimeDiscount_TDIC":
        addRequiredChangeReasonsForGLPartTimeDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLRiskMgmtDiscount_TDIC":
        addRequiredChangeReasonsForGLRiskMgmtDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLServiceMembersCRADiscount_TDIC":
        addRequiredChangeReasonsForGLServiceMembersCRADiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLTempDisDiscount_TDIC":
        addRequiredChangeReasonsForGLTempDisDiscount_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLIRPMDiscount_TDIC":
        addRequiredChangeReasonsForGLStateExclCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNewToCompanyDiscount_TDIC":
        addRequiredChangeReasonsForGLNewtoCompany_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      default:
        //Nothing to do
    }
  }
  /*
* create by: SureshB
* @description: method to add the required change reasons for GeneralLiabilityCov
* @create time: 7:12 PM 11/26/2019
* @param DiffItem, List<ChangeReason_TDIC>
* @return:
*/
  private function addRequiredChangeReasonsForGeneralLiabilityCovDiffItem_TDIC(diffItem : DiffItem, requiredChangeReasons : List<ChangeReason_TDIC>) {
    var generalLiabilityCov = (diffItem.Bean as GeneralLiabilityCov)
    switch (generalLiabilityCov.Pattern.CodeIdentifier) {
      case "GLDentalEmpPracLiabCov_TDIC":
        addRequiredChangeReasonsForGLDentalEmpPracLiabCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLExtendedReportPeriodCov_TDIC":
        addRequiredChangeReasonsForGLExtendedReportPeriodCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLExtendedReportPeriodDPLCov_TDIC":
        addRequiredChangeReasonsForGLExtendedReportPeriodDPLCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLIDTheftREcoveryCov_TDIC":
        addRequiredChangeReasonsForGLIDTheftREcoveryCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLDentistProfLiabCov_TDIC":
        addRequiredChangeReasonsForGLDentistProfLiabCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLCyberLiabCov_TDIC":
        addRequiredChangeReasonsForGLCyberLiabCovDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLLocumTenensCov_TDIC":
        addRequiredChangeReasonsForGLLocumTenensCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLManuscriptEndor_TDIC":
        addRequiredChangeReasonsForGLManuscriptEndor_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLMobileDentalClinicCov_TDIC":
        addRequiredChangeReasonsForGLMobileDentalClinicCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLMultiOwnerDPECov_TDIC":
        addRequiredChangeReasonsForGLMultiOwnerDPECov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNameOTDCov_TDIC":
        addRequiredChangeReasonsForGLNameOTDCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNJWaiverCov_TDIC":
        addRequiredChangeReasonsForGLNJWaiverCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLNJDeductibleCov_TDIC":
        addRequiredChangeReasonsForGLNJDeductibleCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLSchoolServicesCov_TDIC":
        addRequiredChangeReasonsForGLSchoolServicesCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLSpecialEventCov_TDIC":
        addRequiredChangeReasonsForGLSpecialEventCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLStateExclCov_TDIC":
        addRequiredChangeReasonsForGLStateExclCov_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLCOICov_TDIC":
        addRequiredChangeReasonsForGLCOICovDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLBLAICov_TDIC":
        addRequiredChangeReasonsForGLDBLCovDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLAdditionalInsuredCov_TDIC":
        addRequiredChangeReasonsForPLAddnlInsDiffItem_TDIC(diffItem, requiredChangeReasons)
        break
      case "GLExcludedServiceCond_TDIC":
        addRequiredChangeReasonsForGLExcludedServiceCond_TDICDiffItem_TDIC(diffItem, requiredChangeReasons)
        break

      default:
        // Nothing to do
    }
  }
}