package tdic.pc.config.job.policychange.automated

uses java.util.Date
uses java.util.ArrayList

uses org.slf4j.LoggerFactory

class AB2883RemoveExclusions extends AutomatedPolicyChange {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${AB2883RemoveExclusions.Type.RelativeName} - "

  public construct (policy : Policy, effectiveDate : Date) {
    super(AutomatedJob_TDIC.TC_AB2883REMOVEEXCLUSION, policy, effectiveDate);
  }

  protected override function isPolicyChangeNeededImpl (policyPeriod : PolicyPeriod) : boolean {
    return true;
  }

  protected override function updatePolicy (policyPeriod : PolicyPeriod) : boolean {
    var retval = true;
    var exclusion = policyPeriod.WC7Line.WC7PartnersOfficersAndOthersExclEndorsementExcl;
    var wc7Line : WC7Line;

    if (exclusion != null) {
      var sliceDates = new ArrayList<Date>();
      sliceDates.add (policyPeriod.EditEffectiveDate);
      sliceDates.addAll (policyPeriod.FutureSliceDatesInPeriod.toList());

      for (sliceDate in sliceDates) {
        wc7Line = policyPeriod.getSlice(sliceDate).WC7Line;
        if (wc7Line.WC7PartnersOfficersAndOthersExclEndorsementExclExists) {
          for (ownerOfficer in wc7Line.WC7ExcludedPolicyOwnerOfficers) {
            _logger.info (_LOG_TAG + "Changing " + ownerOfficer + " to an included owner officer effective "
                + sliceDate.formatDate(SHORT));
            ownerOfficer.isExcluded_TDIC = false;
          }
        } else {
          break;
        }
      }

      _logger.info (_LOG_TAG + "Removing " + exclusion + " from policy");
      policyPeriod.WC7Line.removeExclusionFromCoverable(exclusion);
    } else {
      logError (_LOG_TAG + "Exclusion is NOT on the policy effective " + policyPeriod.EditEffectiveDate.formatDate(SHORT));
      retval = false;
    }

    return retval;
  }
}