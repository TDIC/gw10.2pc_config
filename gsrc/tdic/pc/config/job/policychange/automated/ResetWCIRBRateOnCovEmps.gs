package tdic.pc.config.job.policychange.automated

uses java.util.Date

uses gw.lob.wc7.WC7ClassCodeSearchCriteria
uses gw.api.database.IQueryBeanResult
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: TimT
 * Date: 12/20/17
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
class ResetWCIRBRateOnCovEmps extends AutomatedPolicyChange {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG : String = "${ResetWCIRBRateOnCovEmps.Type.RelativeName} - "

  public construct (policy : Policy, effectiveDate : Date) {
    super(AutomatedJob_TDIC.TC_RESETWCIRBRATEONCOVEMPS, policy, effectiveDate);
  }

  protected override function isPolicyChangeNeededImpl (policyPeriod : PolicyPeriod) : boolean {
    var tmpPP = policyPeriod.getSlice(_effectiveDate)
    _logger.info(_LOG_TAG + "isPolicyChangeNeededImpl: getting jurisdictions");
    for (eachWC7Jurisdiction in tmpPP.WC7Line.WC7Jurisdictions){
      _logger.info(_LOG_TAG + "isPolicyChangeNeededImpl: getting cov emps for " + eachWC7Jurisdiction.Jurisdiction);
      for (eachWC7CoveredEmployeeBase in tmpPP.WC7Line.getWC7CoveredEmployeesWM(eachWC7Jurisdiction.Jurisdiction)){
        _logger.info(_LOG_TAG + "isPolicyChangeNeededImpl: getting class code for " + eachWC7CoveredEmployeeBase.DisplayName);
        var tmpWC7ClassCode_existing : entity.WC7ClassCode = eachWC7CoveredEmployeeBase.ClassCode;

        _logger.info(_LOG_TAG + "isPolicyChangeNeededImpl: starting search to match: " + tmpWC7ClassCode_existing.Code + " (" + tmpWC7ClassCode_existing.PublicID + ")");
        var searchCriteria = new WC7ClassCodeSearchCriteria();
        searchCriteria.Code = tmpWC7ClassCode_existing.Code;
        searchCriteria.EffectiveAsOfDate = tmpPP.WC7Line.getReferenceDateForCurrentJob(eachWC7Jurisdiction.Jurisdiction); //JurisdictionMappingUtil.getJurisdiction(eachWC7CoveredEmployeeBase.LocationWM));
        //searchCriteria.PreviousSelectedClassCode = tmpWC7ClassCode_existing;
        searchCriteria.Jurisdiction = eachWC7Jurisdiction.Jurisdiction; //JurisdictionMappingUtil.getJurisdiction(eachWC7CoveredEmployeeBase.LocationWM) as String;
        var searchResults : IQueryBeanResult<WC7ClassCode> = searchCriteria.performExactSearch();
        for (each in searchResults){ _logger.info ("    " + each.DisplayName + " " + each.EffectiveDate + " " + each.PublicID);}
        var tmpWC7ClassCode_required : entity.WC7ClassCode = searchResults.AtMostOneRow;

        if (tmpWC7ClassCode_required == null){
          _logger.error(_LOG_TAG + "isPolicyChangeNeededImpl: couldn't find a WC7ClassCode for policy: " + tmpPP.PolicyNumber + " " + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") ClassCode: " + tmpWC7ClassCode_existing.Code);
        } else if (tmpWC7ClassCode_existing == tmpWC7ClassCode_required){
          // do nothing, its accurate
        } else if (tmpWC7ClassCode_existing != tmpWC7ClassCode_required){
          // stage for an endorsement
          _logger.info(_LOG_TAG + "isPolicyChangeNeededImpl: staging endorsement for policy: " + tmpPP.PolicyNumber + "-" + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") PeriodStart:" + tmpPP.PeriodStart + " ClassCode: " + tmpWC7ClassCode_existing.Code);
          return true;
        } else {
          _logger.error(_LOG_TAG + "isPolicyChangeNeededImpl: how did we get here? " + tmpPP.PolicyNumber + " " + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") ClassCode: " + tmpWC7ClassCode_existing.Code);
        }
      }
    }
    return false;
  }

  protected override function updatePolicy (policyPeriod : PolicyPeriod) : boolean {
    var retval = false;

    var tmpPP = policyPeriod.getSlice(_effectiveDate)
    _logger.info(_LOG_TAG + "updatePolicy: getting jurisdictions");
    for (eachWC7Jurisdiction in tmpPP.WC7Line.WC7Jurisdictions){
      _logger.info(_LOG_TAG + "updatePolicy: getting cov emps for " + eachWC7Jurisdiction.Jurisdiction);
      for (eachWC7CoveredEmployeeBase in tmpPP.WC7Line.getWC7CoveredEmployeesWM(eachWC7Jurisdiction.Jurisdiction)){
        _logger.info(_LOG_TAG + "updatePolicy: getting class code for " + eachWC7CoveredEmployeeBase.DisplayName);
        var tmpWC7ClassCode_existing : entity.WC7ClassCode = eachWC7CoveredEmployeeBase.ClassCode;

        _logger.info(_LOG_TAG + "updatePolicy: starting search to match: " + tmpWC7ClassCode_existing.Code + " (" + tmpWC7ClassCode_existing.PublicID + ")");
        var searchCriteria = new WC7ClassCodeSearchCriteria();
        searchCriteria.Code = tmpWC7ClassCode_existing.Code;
        searchCriteria.EffectiveAsOfDate = tmpPP.WC7Line.getReferenceDateForCurrentJob(eachWC7Jurisdiction.Jurisdiction); //JurisdictionMappingUtil.getJurisdiction(eachWC7CoveredEmployeeBase.LocationWM));
        //searchCriteria.PreviousSelectedClassCode = tmpWC7ClassCode_existing;
        searchCriteria.Jurisdiction = eachWC7Jurisdiction.Jurisdiction; //JurisdictionMappingUtil.getJurisdiction(eachWC7CoveredEmployeeBase.LocationWM) as String;
        var searchResults : IQueryBeanResult<WC7ClassCode> = searchCriteria.performExactSearch();
        for (each in searchResults){ _logger.info ("    " + each.DisplayName + " " + each.EffectiveDate + " " + each.PublicID);}
        var tmpWC7ClassCode_required : entity.WC7ClassCode = searchResults.AtMostOneRow;

        if (tmpWC7ClassCode_required == null){
          _logger.error(_LOG_TAG + "updatePolicy: couldn't find a WC7ClassCode for policy: " + tmpPP.PolicyNumber + " " + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") ClassCode: " + tmpWC7ClassCode_existing.Code);
        } else if (tmpWC7ClassCode_existing == tmpWC7ClassCode_required){
          // do nothing, its accurate
        } else if (tmpWC7ClassCode_existing != tmpWC7ClassCode_required){
          // stage for an endorsement
          _logger.info(_LOG_TAG + "updatePolicy: performing endorsement for policy: " + tmpPP.PolicyNumber + "-" + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") PeriodStart:" + tmpPP.PeriodStart + " ClassCode: " + tmpWC7ClassCode_existing.Code);
//          tmpWC7ClassCode_existing = tmpWC7ClassCode_required;  //  <--- * * * * * * * W O R K    I T E M   H E R E  * * * * * * * //
          retval = true;
        } else {
          _logger.error(_LOG_TAG + "updatePolicy: how did we get here? " + tmpPP.PolicyNumber + " " + tmpPP.TermNumber + " (" + tmpPP.PublicID + ") ClassCode: " + tmpWC7ClassCode_existing.Code);
        }
      }
    }
    return retval;
  }
}