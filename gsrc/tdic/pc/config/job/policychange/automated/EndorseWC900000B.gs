package tdic.pc.config.job.policychange.automated

uses java.util.Date

uses org.slf4j.LoggerFactory

class EndorseWC900000B extends AutomatedPolicyChange {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${EndorseWC900000B.Type.RelativeName} - "

  public construct (policy : Policy, effectiveDate : Date) {
    super(AutomatedJob_TDIC.TC_ENDORSEWC900000B, policy, effectiveDate);
  }

  protected override function isPolicyChangeNeededImpl (policyPeriod : PolicyPeriod) : boolean {
    return true;
  }

  protected override function updatePolicy (policyPeriod : PolicyPeriod) : boolean {
    return true;
  }

  static protected var approvableIssueCodes : String[] = {
      "TDICBackdatedBind", "TDICClassCode", "TDICConditionEndorsement", "TDIC_MismatchedMultiLineDiscount",
      "TDIC_MissingADANumbers", "TDIC_OwnershipPercentage", "TDIC_WC7AccountUnhealthy", "TDIC_WC7AccountUnpaidBalance",
      "TDIC_WC7EmploymentPriorToEffectiveDate", "TDIC_WC7ExModGreaterThanOne", "TDIC_WC7ExModLessThanOrEqualOne",
      "TDIC_WC7ExceedClaimsMax", "TDIC_WC7GapOrOverLapInCov", "TDIC_WC7MissingLossHistory", "TDIC_WC7NoLossHistory",
      "TDIC_WC7OpenClaims", "TDIC_WC7RiskAnalysisPriorPolicies", "TDIC_WC7SchedRatingManPremExceeded",
      "TDIC_WC7ScheduleRatingFactorCredit", "TDIC_WC7ScheduleRatingFactorDebit", "TDIC_WC7TotalIncurredMax",
      "TDIC_WaiverOfSubroExists"
  };

  /**
   *  Is issue auto approvable?
   */
  protected override function isIssueAutoApprovable (uwIssue : UWIssue) : boolean {
    var retval = super.isIssueAutoApprovable(uwIssue);

    if (retval == false) {
      var issueCode = uwIssue.IssueType.Code;
      retval = approvableIssueCodes.contains(issueCode);
    }

    return retval;
  }

  /**
   * Return the effective date of the first Form A on the term.
  */
  public static function nextPolicyChangeDate (policyPeriod : PolicyPeriod) : Date {
    var effectiveDate : Date;

    var forms = policyPeriod.Forms.where(\f -> f.FormPatternCode == "WC900000A" and f.RemovedOrSuperseded != true);
    if (forms.HasElements) {
      effectiveDate = forms.min(\f -> f.EffectiveDate);
    }

    return effectiveDate;
  }
}