package tdic.pc.config.job.audit

uses tdic.pc.config.job.AutomatedJobTemplate

uses java.util.Date
uses java.lang.Exception
uses gw.job.JobProcess
uses gw.job.AuditProcess
uses org.slf4j.LoggerFactory

abstract class AutomatedAudit extends AutomatedJobTemplate<Audit> {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");
  static private final var _LOG_TAG = "${AutomatedAudit.Type.RelativeName} - "

  public construct (changeType : AutomatedJob_TDIC, policy : Policy, effectiveDate : Date) {
    super(changeType, policy, effectiveDate);
  }

  final override protected function canStartJob() : String {
    return _policy.canStartAudit(_effectiveDate);
  }

  final override protected function createJob() : Audit {
    var audit : Audit;
    var policyPeriod = Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(_policy, _effectiveDate);

    try {
      scheduleNewAudit (policyPeriod);
    } catch (e : Exception) {
      logError (_LOG_TAG + "Unable to schedule new audit for " + policyPeriod.PolicyNumber + "-" + policyPeriod.TermNumber
                  + " - " + e.Message);
    }

    var auditInformation = policyPeriod.AuditInformations.firstWhere(\ai -> ai.IsScheduled);
    if (auditInformation != null) {
      audit = startAudit (auditInformation);
    }

    return audit;
  }

  protected function scheduleNewAudit (policyPeriod : PolicyPeriod) : void {
    var auditScheduleType = AuditScheduleType.TC_FINALAUDIT;
    var suggestedDateRange = policyPeriod.suggestedAuditDateRange(auditScheduleType);

    var auditStartDate = suggestedDateRange.first();
    var auditEndDate = suggestedDateRange.last();

    var processStartDate = auditEndDate;
    var dueDate = auditEndDate.addMonths(1);

    var auditMethod = AuditMethod.TC_VOLUNTARY;

    gw.job.audit.AuditScheduler.scheduleNewAudit (policyPeriod, auditStartDate, auditEndDate,
        processStartDate, dueDate, auditScheduleType, auditMethod,
        auditScheduleType == AuditScheduleType.TC_PREMIUMREPORT);
  }

  protected function startAudit (auditInformation : AuditInformation) : Audit {
    auditInformation.startAuditJob();
    return auditInformation.Audit;
  }

  final override protected function startJob() : void {
    // No action needed, since Audit was started by createJob()
  }

  /**
   * Complete the job.
   */
  override protected function complete (jobProcess : JobProcess) : void {
    (jobProcess as AuditProcess).complete();
  }
}