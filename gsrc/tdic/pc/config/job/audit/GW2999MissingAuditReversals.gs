package tdic.pc.config.job.audit

uses gw.api.database.Query
uses java.util.Date
uses tdic.pc.integ.services.billing.BillingSystem_TDIC
uses gw.job.JobProcess
uses org.slf4j.LoggerFactory

class GW2999MissingAuditReversals extends AutomatedAudit {
  static private var _logger = LoggerFactory.getLogger("Application.JobProcess");

  protected var _sourceAudit : Audit as readonly SourceAudit;
  protected var _sourceAuditStatus : PolicyPeriodStatus as readonly SourceAuditStatus;
  protected var _payrollSource : Job as readonly PayrollSource;
  protected var _activity : Activity as readonly Activity;
  protected var _invoiceDate : Date as readonly InvoiceDate;

  protected var _inprogress : boolean;
  protected var _dontQuote : boolean;


  public construct (sourceAudit : Audit) {
    super(AutomatedJob_TDIC.TC_GW2999MISSINGAUDITREVERSALS, sourceAudit.Policy,
          sourceAudit.LatestPeriod.EditEffectiveDate);
    _sourceAudit = sourceAudit;
    _sourceAuditStatus = sourceAudit.LatestPeriod.Status;
    _inprogress = _sourceAuditStatus != PolicyPeriodStatus.TC_AUDITCOMPLETE;
    _dontQuote = _inprogress;

    // Determine the next invoice date.
    var billingSystem = new BillingSystem_TDIC();
    _invoiceDate = billingSystem.getNextInvoiceDateAfterDate (SourceAudit.LatestPeriod.PolicyNumber, Date.Yesterday);
  }

  override protected function scheduleNewAudit (policyPeriod : PolicyPeriod) : void {
    // Schedule a new audit based upon the source audit's audit information.
    var sourceAuditInformation = _sourceAudit.AuditInformation;
    gw.job.audit.AuditScheduler.scheduleNewAudit (policyPeriod, sourceAuditInformation.AuditPeriodStartDate,
                                                  // Need to calculate end date in case of different term lengths.
                                                  policyPeriod.CancellationDate == null ? sourceAuditInformation.AuditPeriodEndDate : policyPeriod.CancellationDate,
                                                  sourceAuditInformation.InitDate, sourceAuditInformation.DueDate,
                                                  sourceAuditInformation.AuditScheduleType,
                                                  sourceAuditInformation.AuditMethod,
                                                  sourceAuditInformation.AuditScheduleType == AuditScheduleType.TC_PREMIUMREPORT);

    // Copy other fields from the source audit's audit information.
    var auditInformation = policyPeriod.AuditInformations.firstWhere(\ai -> ai.IsScheduled);
    auditInformation.ActualAuditMethod         = sourceAuditInformation.ActualAuditMethod;
    auditInformation.PhysicalAuditFlagged_TDIC = sourceAuditInformation.PhysicalAuditFlagged_TDIC;
    auditInformation.NoticeSentHolder_TDIC     = sourceAuditInformation.NoticeSentHolder_TDIC;
    auditInformation.NoticeSentAuditor_TDIC    = sourceAuditInformation.NoticeSentAuditor_TDIC;
    auditInformation.AppointmentDate_TDIC      = sourceAuditInformation.AppointmentDate_TDIC;
    auditInformation.ReceivedDate              = sourceAuditInformation.ReceivedDate;
  }

  override protected function startAudit (auditInformation : AuditInformation) : Audit {
    var audit = super.startAudit(auditInformation);

    // Do not set description on in progress audits.
    if (_inprogress == false) {
      audit.Description = "Replacement Audit";
    }

    if (_inprogress == true) {
      // Use the same auditor for in progress audits.
      audit.Auditor_TDIC = _sourceAudit.Auditor_TDIC;
    } else {
      // Set the auditor on completed audits to Cathy.
      audit.Auditor_TDIC = Cathy;
    }

    return audit;
  }

  protected override function updatePolicy (policyPeriod : PolicyPeriod) : boolean {
    var job : Job;
    var sourcePolicyPeriod = _sourceAudit.LatestPeriod;
    var subject : String;
    var description : String;
    var retval : boolean;

    if (_inprogress == false) {
      // Check if there was an payroll or classcode endorsement after the audit.
      var auditBasedOn = getAuditBasedOn(_sourceAudit);
      var tempPolicyPeriod = Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(_policy, _effectiveDate);

      while (tempPolicyPeriod != auditBasedOn) {
        job = tempPolicyPeriod.Job;
        if (job typeis PolicyChange) {
          if (job.ChangeReasons.hasMatch(\cr -> cr.ChangeReason == ChangeReasonType_TDIC.TC_PAYROLL
                                             or cr.ChangeReason == ChangeReasonType_TDIC.TC_CLASSCODE)) {
            sourcePolicyPeriod = tempPolicyPeriod;
            break;
          }
        }
        tempPolicyPeriod = tempPolicyPeriod.BasedOn;
      }
    }

    _payrollSource = sourcePolicyPeriod.Job;

    if (sourcePolicyPeriod.EndOfCoverageDate != policyPeriod.EndOfCoverageDate) {
      _dontQuote = true;
      retval = false;

      subject = "Review replacement audit with different end of coverage date";
      description = Job.Subtype.DisplayName + " " + Job.JobNumber + " has an end of coverage date of "
                      + policyPeriod.EndOfCoverageDate.formatDate(SHORT) + ".  Payroll from "
                      + _payrollSource.Subtype.DisplayName + " " + _payrollSource.JobNumber
                      + " has an end of coverage date of " + sourcePolicyPeriod.EndOfCoverageDate.formatDate(SHORT)
                      + ".";
      logError (description);
      createActivity (subject, description);
    } else {
      _logger.info ("Copying payroll from " + _payrollSource.Subtype.DisplayName + " " + _payrollSource.JobNumber
                      + " to " + policyPeriod.Job.Subtype.DisplayName + " " + policyPeriod.Job.JobNumber);
      retval = copyPayrollData (policyPeriod, sourcePolicyPeriod) and copyWaiverData (policyPeriod, sourcePolicyPeriod);

      if (_payrollSource != _sourceAudit) {
        _dontQuote = true;
        subject = "Review replacement audit with payroll endorsement";
        description = Job.Subtype.DisplayName + " " + Job.JobNumber + " was created with the payroll from "
                        + _payrollSource.Subtype.DisplayName + " " + _payrollSource.JobNumber
                        + ".  Original audit was " + _sourceAudit.Subtype.DisplayName + " " + _sourceAudit.JobNumber
                        + ".";
        logWarning (description);
        createActivity (subject, description);
      } else if (retval == false) {
        _dontQuote = true;
        subject = "Review replacement audit payroll";
        description = "Unable to successfully copy payroll from " + _payrollSource.Subtype.DisplayName + " "
                        + _payrollSource.JobNumber + " to " + Job.Subtype.DisplayName + " " + Job.JobNumber
                        + ".  Please verify payroll information.";
        createActivity (subject, description);
      }
    }

    return retval;
  }

  protected function copyPayrollData (destPolicyPeriod : PolicyPeriod, sourcePolicyPeriod : PolicyPeriod) : boolean {
    var retval = true;

    var sourceJurisdictions = sourcePolicyPeriod.WC7Line.WC7Jurisdictions;
    var destJurisdictions   = destPolicyPeriod.WC7Line.WC7Jurisdictions.toList();
    var destJurisdiction : WC7Jurisdiction;

    // Copy Covered Employee Data
    for (sourceJurisdiction in sourceJurisdictions) {
      destJurisdiction = destJurisdictions.firstWhere(\j -> j.Jurisdiction == sourceJurisdiction.Jurisdiction);

      if (destJurisdiction != null) {
        if (copyPayrollData (destJurisdiction, sourceJurisdiction) == false) {
          retval = false;
        }
        destJurisdictions.remove(destJurisdiction);
      } else {
        logError ("Failed for find WC7Jurisdiction " + sourceJurisdiction.Jurisdiction.DisplayName + " on "
                      + destPolicyPeriod.Job.Subtype.DisplayName + " " + destPolicyPeriod.Job.JobNumber + ".");
        retval = false;
      }
    }

    if (destJurisdictions.HasElements) {
      for (jurisdiction in destJurisdictions) {
        logError ("Failed to find WC7Jurisdiction " + jurisdiction.Jurisdiction.DisplayName + " on "
                    + sourcePolicyPeriod.Job.Subtype.DisplayName + " " + sourcePolicyPeriod.Job.JobNumber + ".");
      }
      retval = false;
    }

    return retval;
  }

  protected function copyPayrollData (destJurisdiction : WC7Jurisdiction,
                                      sourceJurisdiction : WC7Jurisdiction) : boolean {
    var retval = true;
    var isSourceAudit = sourceJurisdiction.Branch.Job typeis Audit;

    var sourceCovEmps = sourceJurisdiction.WCLine.getWC7CoveredEmployeesWM(sourceJurisdiction.Jurisdiction);
    var destCovEmps   = destJurisdiction.WCLine.getWC7CoveredEmployeesWM(destJurisdiction.Jurisdiction).toList();
    var destCovEmp : WC7CoveredEmployee;

    for (sourceCovEmp in sourceCovEmps) {
      destCovEmp = destCovEmps.firstWhere(\ce -> ce.FixedId == sourceCovEmp.FixedId);
      if (destCovEmp != null) {
        // Copy Payroll Data
        if (isSourceAudit) {
          destCovEmp.AuditedNumEmployees_TDIC = sourceCovEmp.AuditedNumEmployees_TDIC;
          destCovEmp.AuditedAmount            = sourceCovEmp.AuditedAmount;
        } else {
          destCovEmp.AuditedNumEmployees_TDIC = sourceCovEmp.NumEmployees;
          destCovEmp.AuditedAmount            = sourceCovEmp.BasisAmount;
        }
        destCovEmps.remove(destCovEmp);
      } else {
        logError ("Failed to find WC7CoveredEmployee with FixedId = "
                    + sourceCovEmp.FixedId + " on "+ destJurisdiction.Branch.Job.Subtype.DisplayName + " "
                    + destJurisdiction.Branch.Job.JobNumber + ".");
        retval = false;
      }
    }

    if (destCovEmps.HasElements) {
      for (covEmp in destCovEmps) {
        logError ("Failed to find WC7CoveredEmployee with FixedId = "
                    + covEmp.FixedId + " on " + sourceJurisdiction.Branch.Job.Subtype.DisplayName + " "
                    + sourceJurisdiction.Branch.Job.JobNumber + ".");
      }
      retval = false;
    }

    return retval;
  }

  protected function copyWaiverData (destPolicyPeriod : PolicyPeriod, sourcePolicyPeriod : PolicyPeriod) : boolean {
    var retval = true;
    var isSourceAudit = sourcePolicyPeriod.Job typeis Audit;

    var sourceWaivers = sourcePolicyPeriod.WC7Line.WC7SpecificWaiversWM;
    var destWaivers   = destPolicyPeriod.WC7Line.WC7SpecificWaiversWM.toList();
    var destWaiver : WC7WaiverOfSubro;

    for (sourceWaiver in sourceWaivers) {
      destWaiver = destWaivers.firstWhere(\w -> w.FixedId == sourceWaiver.FixedId);
      if (destWaiver != null) {
        // Copy Waiver Data
        if (isSourceAudit) {
          destWaiver.AuditedAmount = sourceWaiver.AuditedAmount;
        } else {
          destWaiver.AuditedAmount = sourceWaiver.BasisAmount;
        }
        destWaivers.remove(destWaiver);
      } else {
        logError ("Failed to find WC7WaiverOfSubro with FixedId = "
                    + sourceWaiver.FixedId + " on "+ destPolicyPeriod.Job.Subtype.DisplayName + " "
                    + destPolicyPeriod.Job.JobNumber + ".");
        retval = false;
      }
    }

    if (destWaivers.HasElements) {
      for (waiver in destWaivers) {
        logError ("Failed to find WC7WaiverOfSubro with FixedId = "
                    + waiver.FixedId + " on " + sourcePolicyPeriod.Job.Subtype.DisplayName + " "
                    + sourcePolicyPeriod.Job.JobNumber + ".");
      }
      retval = false;
    }

    return retval;
  }

  override protected property get ShouldQuote() : boolean {
    return _dontQuote == false;
  }

  /**
   * Validate the quote.
   */
  override protected function isQuoteValid (policyPeriod : PolicyPeriod) : boolean {
    var retval = super.isQuoteValid(policyPeriod);

    if (retval == true) {
      if (policyPeriod.TotalCostRPT != _sourceAudit.LatestPeriod.TotalCostRPT) {
        var subject = "Review replacement audit premium";
        var description = policyPeriod.Job.Subtype.DisplayName + " " + policyPeriod.Job.JobNumber
                            + " has a total cost of " + policyPeriod.TotalCostRPT
                            + ", which does not match the total cost of " + _sourceAudit.LatestPeriod.TotalCostRPT
                            + " from " + _sourceAudit.Subtype.DisplayName + " " + _sourceAudit.JobNumber + ".";
        logError (description);
        createActivity (subject, description);

        retval = false;
      }
    }

    return retval;
  }

  /**
   * Complete the job.
   */
  override protected function complete (jobProcess : JobProcess) : void {
    // Suppress documents on completed audits
    Job.SuppressDocuments_TDIC = true;
    super.complete(jobProcess);
  }

  protected function createActivity(subject : String, description : String) : void {
    var groupQuery = Query.make(Group).compare(Group#Name, Equals, "Final Audits");
    var group = groupQuery.select().FirstResult;

    _activity = new Activity();
    _activity.Account       = Job.Policy.Account;
    _activity.setFieldValue ("Policy", Job.Policy);
    _activity.setFieldValue ("Job",    Job);
    _activity.ActivityClass = ActivityClass.TC_TASK;
    _activity.Subject       = subject;
    _activity.Description   = description;
    _activity.Priority      = Priority.TC_URGENT;

    _activity.TargetDate    = Date.Today.addBusinessDays(10);
    if (_invoiceDate.after(_activity.TargetDate)) {
      _activity.TargetDate = _invoiceDate;
    }

    _activity.assign(group, Cathy);
  }

  /**
   * Post-Error Cleanup
   */
  override protected function preExitCleanup(policyPeriod : PolicyPeriod) : void {
    if (Job != null) {
      if (Job.Complete) {
        // Remove auditor on completed jobs.
        Job.Auditor_TDIC = null;
      } else {
        if (_inprogress == true) {
          // Remove automated job so that documents cannot be suppressed.
          Job.AutomatedJob_TDIC = null;
        }
      }
    }
  }

  /**
   * Get the non-audit based on revision.
  */
  static public function getAuditBasedOn (audit : Audit) : PolicyPeriod {
    var policyPeriod = audit.LatestPeriod.BasedOn;
    while (policyPeriod.Job typeis Audit) {
      policyPeriod = policyPeriod.BasedOn;
    }
    return policyPeriod;
  }

  /**
   * Get the user entity for Cathy.
  */
  protected property get Cathy() : User {
    var userQuery = Query.make(User).compare(User#PublicID, Equals, "pc-tdic:83");  // CathyL
    var user = userQuery.select().FirstResult;
    return user;
  }
}