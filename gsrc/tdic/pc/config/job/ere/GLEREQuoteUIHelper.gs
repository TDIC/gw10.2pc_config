package tdic.pc.config.job.ere

uses gw.api.archive.PCArchivingUtil
uses gw.api.domain.financials.PCFinancialsLogger
uses gw.api.locale.DisplayKey
uses gw.api.system.PCDependenciesGateway
uses gw.api.system.database.SequenceUtil
uses gw.api.ui.GL_CostWrapper
uses gw.api.util.DisplayableException
uses gw.api.util.StringUtil
uses gw.lob.gl.rating.GLAddnlInsdSchedCostData_TDIC
uses gw.lob.gl.rating.GLCostData
uses gw.lob.gl.rating.GLCovExposureCostData
uses gw.lob.gl.rating.GLHistoricalCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses tdic.pc.config.rating.pl.GLEREQuickQuote

/**
 * BrittoS 04/28/2020, Quick Quote for ERE offer
 */
class GLEREQuoteUIHelper {

  private static var standardPremiumOrderMap = {
      "1st Year" -> 1.0,   //Professional Liability "1st Year"
      "2nd Year" -> 2.0,   //Professional Liability "2nd Year"
      "3rd Year" -> 3.0,   //Professional Liability "3rd Year"
      "4th Year" -> 4.0,   //Professional Liability "4th Year"
      "Mature" -> 5.0,   //Professional Liability "Mature"
      "ERE_AdditionalInsured" -> 6.0,   //Additional Insured
      "ERE_RiskManagement_TDIC" -> 7.0,   //Risk Management
      "ERE_IRPMDiscount_TDIC" -> 8.0,   //IRPM Discount
      "ERE_NJDeductible_TDIC" -> 9.0,   //NJ Deductible
      "ERE_NJWaiverOfConsent_TDIC" -> 10.0,  //NJ Waiver Of Consent
      "ERE_NJIGASurcharge_TDIC" -> 11.0   //NJ IGA Surcharge
  }

  /**
   * @param policyPeriod
   * @param effectiveDate
   * @return
   */
  public static function requestEREQuote(policyPeriod : PolicyPeriod, effectiveDate : Date) : GLEREQuickQuote_TDIC {
    var qQuote : GLEREQuickQuote_TDIC

    gw.transaction.Transaction.runWithNewBundle(\currentBundle -> {
    var ratingPeriod = getInForcePeriodWithBasedOn(policyPeriod, effectiveDate)
    ratingPeriod = currentBundle?.add(ratingPeriod)
    qQuote = currentBundle?.add(qQuote)
    var revisedEffectiveDate = effectiveDate
    if(ratingPeriod.PeriodStart.compareIgnoreTime(effectiveDate) == 0) {
      revisedEffectiveDate = effectiveDate.addDays(-1)
      ratingPeriod = getInForcePeriodWithBasedOn(policyPeriod, revisedEffectiveDate)
    }

    try {
    var line = ratingPeriod.GLLine.getSlice(revisedEffectiveDate)

      var ereQuote = new GLEREQuickQuote(line as GLLine, effectiveDate, getMinimumRatingLevel())
      var costSet = ereQuote.requestQuote()
      if (costSet.HasElements) {
        qQuote = createQuickQuote(ratingPeriod, effectiveDate, costSet)
        costSet.removeAll(costSet)
      }
    } catch (e) {
      PCFinancialsLogger.logError(e.StackTraceAsString)
      throw new DisplayableException(DisplayKey.get("TDIC.Web.EREQuickQuote.StartEREQuickQuote.InvalidQute"))
    }
  },User?.util?.CurrentUser)
    return qQuote
  }

  /**
   *
   */
  public static function getInForcePeriodWithBasedOn(policyPeriod : PolicyPeriod, effectiveDate : Date) : PolicyPeriod {
    return (effectiveDate != null) ? Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(policyPeriod.Policy, effectiveDate) : null
  }

  /**
   *
   */
  public static function canVisitQuoteERE(policyPeriod : PolicyPeriod) : Boolean {
    return policyPeriod.Policy.Issued
        and policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
        and perm.System.erequote_tdic
  }

  public static function canVisitEREQuoteHistory(policyPeriod : PolicyPeriod) : Boolean {
    return policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
        and perm.PolicyPeriod.view(policyPeriod)
        and perm.System.viewpolicyfile
        and perm.System.viewerequotehistory_tdic
  }

  public static function getStandardCostWrappers(quote : GLEREQuickQuote_TDIC) : GL_CostWrapper[] {
    var costWrappers : List<GL_CostWrapper> = {}
    costWrappers.add(new GL_CostWrapper(0, null, DisplayKey.get("TDIC.Web.Policy.GL.Header.TotalforAllTerms"), quote.TotalTermPremium, true))

    var addnlInsdCosts = quote.Costs.where(\elt -> elt.PremiumType == GLAddnlInsuredCostType_TDIC.TC_ERE_ADDITIONALINSURED.DisplayName).toSet()
    if(addnlInsdCosts.HasElements) {
      var firstCost = addnlInsdCosts.first()
      var totalAddnlPremium = addnlInsdCosts.sum(\elt -> elt.ProratedPremium)
      costWrappers.add(new GL_CostWrapper(firstCost.PremiumOrder.doubleValue(), null, firstCost.PremiumType, totalAddnlPremium ,true))
    }
    var costs = quote.Costs.where(\elt -> elt.PremiumType != GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY.DisplayName
        and not addnlInsdCosts.contains(elt)
        and elt.RateAmountType != RateAmountType.TC_TAXSURCHARGE)
    if (costs.HasElements) {
      costs.each(\cost -> {
        costWrappers.add(new GL_CostWrapper(cost.PremiumOrder.doubleValue(), null, cost.PremiumType, cost.ProratedPremium, true))
      })
    }
    return costWrappers.toTypedArray()
  }

  public static function canStartEREQuote(policy: Policy, effectiveDate: Date ) : String {
    if (effectiveDate == null) {
      return DisplayKey.get("Job.Error.EnterValidEffectiveDate")
    }
    var period = entity.Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(policy, effectiveDate)
    if (period == null) {
      return DisplayKey.get("Job.Error.NoPeriodForEffectiveDate")
    } else if (period.PolicyTerm.CheckArchived) {
      return DisplayKey.get("Web.Job.Warning.ArchivedTerm", period.EditEffectiveDate.ShortFormat)
    } else if (PCArchivingUtil.hasFutureArchivedTerms(period)) {
      return DisplayKey.get("Web.Job.Warning.ArchivedFutureTerm", period.EditEffectiveDate.ShortFormat)
    }
    return null
  }

  /****************************************************Private functions********************************************************/
  private static function getMinimumRatingLevel() : RateBookStatus {
    return PCDependenciesGateway.ServerMode.Production ? RateBookStatus.TC_ACTIVE : RateBookStatus.TC_STAGE
  }

  /**
   * ERE Quick Quote number
   */
  private static function generateQuoteNumber() : String {
    return "ERE" + StringUtil.formatNumber(SequenceUtil.next(1, "ere_qq"), "000000")
  }

  private static function createQuickQuote(period : PolicyPeriod, ereEffDate : Date, costDataSet : Set<GLCostData>) : GLEREQuickQuote_TDIC {
    var qQuote : GLEREQuickQuote_TDIC
    if (costDataSet.HasElements) {
      qQuote = period.Policy.createAndAddEREQuickQuote_TDIC()
      qQuote.QuoteNumber = generateQuoteNumber()
      qQuote.PeriodStart = period.PeriodStart
      qQuote.EREEffectiveDate = ereEffDate
      updateCosts(period, qQuote, costDataSet)
    }
    return qQuote
  }

  private static function updateCosts(ratingPeriod : PolicyPeriod, qQuote : GLEREQuickQuote_TDIC, costDataSet : Set<GLCostData>) : GLEREQuickQuote_TDIC {
    var defaultAmount = 0bd.ofDefaultCurrency()
    var line = ratingPeriod.GLLine

    for (costData in costDataSet) {
      var cost = qQuote.createAndAddQuickQuoteCost()
      cost.PremiumOrder = getPremiumOrder(costData)
      cost.PremiumType = getPremiumType(costData)

      var exposure = getExposure(line, costData)

      if(costData typeis GLHistoricalCostData_TDIC) {
        var splitParameter = getSplitParameter(line, costData)
        cost.EffectiveDate = splitParameter.EffDate
        cost.ExpirationDate = splitParameter.ExpDate
      } else {
        cost.EffectiveDate =  (costData typeis GLCovExposureCostData) ? exposure.SliceDate : exposure.EffectiveDate
        cost.ExpirationDate = getTermExpirationDate(line, costData)
      }

      if (costData typeis GLCovExposureCostData or costData typeis GLHistoricalCostData_TDIC) {
        cost.Limit = line.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.DisplayValue
        cost.ClassCode = getClassCode(line, exposure, costData)
        cost.TerritoryCode = getTerritoryCode(line, exposure, costData)
        cost.SpecialtyCode = getSpecialtyCode(line, exposure, costData)
        cost.Discount = getDiscount(line, costData)
        cost.StepYear = getStepYear(line, costData)
        cost.PremiumOrder = standardPremiumOrderMap.get(cost.StepYear)
      }

      cost.TermPremium = (costData.PreDiscountPremium != null) ? costData.PreDiscountPremium.ofDefaultCurrency() : defaultAmount
      cost.ProratedPremium = (costData.ActualTermAmount != null) ? costData.ActualTermAmount.ofDefaultCurrency() : defaultAmount

      if (costData typeis GLStateCostData and costData.StateCostType == GLStateCostType.TC_ERE_NJIGASURCHARGE_TDIC) {
        cost.RateAmountType = RateAmountType.TC_TAXSURCHARGE
      } else {
        cost.RateAmountType = RateAmountType.TC_STDPREMIUM
      }
    }
    return qQuote
  }

  /**
   * @param costData
   * @return
   */
  private static function getPremiumOrder(costData : GLCostData) : double {
    if (costData typeis GLStateCostData) {
      return standardPremiumOrderMap.get(costData.StateCostType.Code)
    } else if (costData typeis GLAddnlInsdSchedCostData_TDIC) {
      return standardPremiumOrderMap.get(costData.CostType.Code)
    }
    return 20.0
  }

  /**
   * @param costData
   * @return
   */
  private static function getPremiumType(costData : GLCostData) : String {
    switch (typeof(costData)) {
      case GLCovExposureCostData:
        return costData.GLCostType_TDIC.DisplayName
      case GLHistoricalCostData_TDIC:
        return costData.CostType.DisplayName
      case GLStateCostData:
        return costData.StateCostType.DisplayName
      case GLAddnlInsdSchedCostData_TDIC:
        return GLAddnlInsuredCostType_TDIC.TC_ERE_ADDITIONALINSURED.DisplayName

    }
    return null
  }

  /**
   * @param line
   * @param costData
   * @return
   */
  private static function getDiscount(line : GLLine, costData : GLCostData) : String {
    if (costData typeis GLCovExposureCostData) {
      return costData.Discount_TDIC.DisplayName
    } else if (costData typeis GLHistoricalCostData_TDIC) {
      var param = line.GLSplitParameters_TDIC.firstWhere(\elt -> elt.FixedId == costData.RatingParam)
      if (param != null) {
        return param.Discount.DisplayName
      }
    }
    return null
  }

  /**
   * @param line
   * @param costData
   * @return
   */
  private static function getStepYear(line : GLLine, costData : GLCostData) : String {
    if (costData typeis GLCovExposureCostData) {
      return tdic.pc.config.rating.RatingConstants.currentStepYear
    } else if (costData typeis GLHistoricalCostData_TDIC) {
      var param = line.GLSplitParameters_TDIC.firstWhere(\elt -> elt.FixedId == costData.RatingParam)
      if (param != null) {
        return param.splitYear
      }
    }
    return null
  }

  private static function getSplitParameter(line : GLLine, costData : GLCostData) : GLSplitParameters_TDIC {
    if (costData typeis GLHistoricalCostData_TDIC) {
      return line.GLSplitParameters_TDIC.firstWhere(\elt -> elt.FixedId == costData.RatingParam)
    }
    return null
  }

  private static function getExposure(line: GLLine, costData : GLCostData) : GLExposure {
    if(costData typeis GLCovExposureCostData) {
      var exposures = line.Branch.Policy.BoundPeriods*.GLLine*.Exposures.orderBy(\elt -> elt.SliceDate).toList()
      return exposures.firstWhere(\exp -> exp.SliceDate != null and costData.ERESliceDate == exp.SliceDate)
    }
    return line.GLExposuresWM.first()
  }

  private static function getClassCode(line: GLLine, exp : GLExposure, costData : GLCostData) : String {
    if(costData typeis GLCovExposureCostData) {
      return exp.ClassCode_TDIC.DisplayName
    }
    var param = getSplitParameter(line, costData)
    if(param != null) {
      return param.ClassCode
    }
    return null
  }

  private static function getTerritoryCode(line: GLLine, exp : GLExposure, costData : GLCostData) : String {
    if(costData typeis GLCovExposureCostData) {
      return exp.GLTerritory_TDIC.TerritoryCode
    }
    var param = getSplitParameter(line, costData)
    if(param != null) {
      return param.TerritoryCode
    }
    return null
  }

  private static function getSpecialtyCode(line: GLLine, exp : GLExposure, costData : GLCostData) : String {
    if(costData typeis GLCovExposureCostData) {
      return exp.SpecialityCode_TDIC.DisplayName
    }
    var param = getSplitParameter(line, costData)
    if(param != null) {
      return SpecialityCode_TDIC.get(param.SpecialityCode).DisplayName
    }
    return null
  }

  public static function getTermExpirationDate(line: GLLine, costData : GLCostData) : Date {
    if(costData typeis GLCovExposureCostData) {
      var expDate = line.Branch.AllEffectiveDates.firstWhere(\d -> d > costData.ERESliceDate)
      if (expDate != null) {
        if(line.Branch.EditEffectiveDate == expDate) {
          return line.Branch.isCanceled() ? line.Branch.CancellationDate : line.Branch.PeriodEnd
        }
        return expDate
      }
    }
    return costData.ExpirationDate
  }
}