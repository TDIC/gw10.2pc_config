package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.gllocumtenenssched_tdicmodel.types.complex.GLLocumTenensSched_TDIC

enhancement GLLocumTenensSched_TDICModelEnhancement : GLLocumTenensSched_TDIC {

  function populateGLLocumTenensSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddLocumTenesSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
  }
}
