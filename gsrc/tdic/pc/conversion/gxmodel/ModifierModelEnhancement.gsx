package tdic.pc.conversion.gxmodel

uses gw.api.productmodel.ModifierPatternLookup
uses tdic.pc.conversion.exception.ConversionException


enhancement ModifierModelEnhancement : tdic.pc.conversion.gxmodel.modifiermodel.types.complex.Modifier {

  function populateModifier(bldg : BOPBuilding) {
    var pattern = ModifierPatternLookup.getByCodeIdentifier(this.Pattern.CodeIdentifier)
    var mod = bldg.getModifier(pattern)
    if (mod == null) {
      // Other way to handle is to have ETL not create records for unavailable states
      return
    }
    mod.Eligible = true
    switch (this.DataType) {
      case ModifierDataType.TC_BOOLEAN:
        mod.setBooleanModifier(this.BooleanModifier)
        break
      case ModifierDataType.TC_RATE:
        this.RateFactors.Entry.each(\factor -> {
          var rate = mod.getRateFactor(factor.RateFactorType)
          rate.Assessment = factor.Assessment
          rate.Justification = factor.Justification
        })
        break
      default:
        throw new ConversionException("Undefined Modifier Type :" + this.Pattern.CodeIdentifier + ";" + this.DataType)
    }
  }

  function populateModifier(line : GLLine) {
    var pattern = ModifierPatternLookup.getByCodeIdentifier(this.Pattern.CodeIdentifier)
    var mod = line.getModifier(pattern)
    if (mod == null) {
      // Other way to handle is to have ETL not create records for unavailable states
      return
    }
    mod.Eligible = true
    switch (this.DataType) {
      case ModifierDataType.TC_BOOLEAN:
        mod.setBooleanModifier(this.BooleanModifier)
        break
      case ModifierDataType.TC_RATE:
        this.RateFactors.Entry.each(\factor -> {
          var rate = mod.getRateFactor(factor.RateFactorType)
          rate.Assessment = factor.Assessment
          rate.Justification = factor.Justification
        })
        break
      default:
        throw new ConversionException("Undefined Modifier Type :" + this.Pattern.CodeIdentifier + ";" + this.DataType)
    }
  }

}
