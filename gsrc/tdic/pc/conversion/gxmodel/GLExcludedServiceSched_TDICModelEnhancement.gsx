package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.glexcludedservicesched_tdicmodel.types.complex.GLExcludedServiceSched_TDIC

enhancement GLExcludedServiceSched_TDICModelEnhancement : GLExcludedServiceSched_TDIC {

  function populateGLExcludedServiceSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddExcludedServiceSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
  }
}
