package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator

enhancement PolicyLocationModelEnhancement : tdic.pc.conversion.gxmodel.policylocationmodel.types.complex.PolicyLocation {

  function populatePolicyLocation(location : PolicyLocation) {
    SimpleValuePopulator.populate(this, location)
    location.LocationNum = this.LocationNum
    location.AccountLocation.LocationNum = this.LocationNum
    location.State = this.AccountLocation.State
    location.City = this.AccountLocation.City
    location.PostalCode = this.AccountLocation.PostalCode
    if (this.OriginalEffDate_TDIC != null) {
      location.OriginalEffDate_TDIC = this.OriginalEffDate_TDIC
      location.Branch.Policy.OriginalEffectiveDate = this.OriginalEffDate_TDIC
    }
    location.copyPolicyContractDataUnchecked()
    this.AccountLocation.$TypeInstance.populateAccountLocation(location.AccountLocation)
    location.TerritoryCodes.each(\t -> t.fillWithFirst())
    location.AddressLine1 = this.AccountLocation.AddressLine1
    location.setReinsuranceSearchTag()
  }

}
