package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator

enhancement AccountLocationModelEnhancement : tdic.pc.conversion.gxmodel.accountlocationmodel.types.complex.AccountLocation {

  function findMatchedLocation(account : Account) : AccountLocation {
    var result = account.AccountLocations.firstWhere(\accountLocation -> {
      var doMatch =
          accountLocation.AddressLine1 == this.AddressLine1
              and accountLocation.AddressLine2 == this.AddressLine2
              //and accountLocation.AddressLine3 == this.AddressLine3
              and accountLocation.City == this.City
              and accountLocation.State == this.State
              //and accountLocation.Country == Country.get(this.Country as String)
              and accountLocation.PostalCode == this.PostalCode
      //and accountLocation.LocationNum==this.LocationNum
        /*and accountLocation.CEDEX == this.CEDEX
        and accountLocation.CEDEXBureau == this.CEDEXBureau*/

      return doMatch
    }

    )

    return result
  }

  function populateAccountLocation(accountLocation : AccountLocation) {
    SimpleValuePopulator.populate(this, accountLocation)
  }

}
