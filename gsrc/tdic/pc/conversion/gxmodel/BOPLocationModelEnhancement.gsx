package tdic.pc.conversion.gxmodel

uses tdic.pc.common.pcfexpressions.CovTermDirectInputExprHelper
uses tdic.pc.conversion.util.ConversionUtility

enhancement BOPLocationModelEnhancement : tdic.pc.conversion.gxmodel.boplocationmodel.types.complex.BOPLocation {

  function populateBOPLocation(line : BOPLine, bopLine : tdic.pc.conversion.gxmodel.boplinemodel.types.complex.BOPLine) {
    maybeCreateLocation(line, bopLine)
  }

  private function maybeCreateLocation(line : BOPLine, bopLine : tdic.pc.conversion.gxmodel.boplinemodel.types.complex.BOPLine) {
    var policyLocation =  line.Branch.PolicyLocations.firstWhere(\elt -> elt.AccountLocation.LocationNum == this.PolicyLocation.AccountLocation.LocationNum)
    var bopLocation = line.addToLineSpecificLocations(policyLocation.AccountLocation) as BOPLocation
    bopLocation.PolicyLocation.LocationNum = policyLocation.AccountLocation.LocationNum
    bopLocation.PolicyLocation.OriginalEffDate_TDIC = this.PolicyLocation.OriginalEffDate_TDIC
    line.Branch.Policy.OriginalEffectiveDate = this.PolicyLocation.OriginalEffDate_TDIC
    line.addToBOPLocations(bopLocation)
    for (bldg in this.Buildings.Entry) {
      var bopBuilding = bldg.Building.$TypeInstance.populateBuilding(line, bopLocation)
      bopBuilding.ConstructionType = bldg.ConstructionType
      bopBuilding.createOrSyncCoverages()
      for (cov in bldg.Coverages.Entry) {
        cov.$TypeInstance.populateCoverages(bopBuilding, bopLine)
      }
      CovTermDirectInputExprHelper.Instance.setCovTermLimits(bopBuilding)
      bopBuilding.syncModifiers()
      for (mod in bldg.Modifiers.Entry) {
        mod.$TypeInstance.populateModifier(bopBuilding)
      }
      for (mortgagee in bldg.BOPBldgMortgagees.Entry) {
        if (!bopBuilding.BOPBldgMortgagees?.hasMatch(\mrtg -> mrtg.AccountContactRole.AccountContact.Contact.Name == mortgagee.AccountContactRole.AccountContact.Contact.Name_elem.$Value)) {
          var contactType = ConversionUtility.getContactType(mortgagee.AccountContactRole.AccountContact.Contact.Subtype)
          var policyMortgagee = bopBuilding.addNewPolicyMortgageeOfContactType_TDIC(contactType)
          mortgagee.$TypeInstance.populateBOPBldgMortgagee(bopBuilding, policyMortgagee)
          bopBuilding.addToBOPBldgMortgagees(policyMortgagee)
        }
      }
      for (lossPayee in bldg.BOPBldgLossPayees.Entry) {
        if (!bopBuilding.BOPBldgLossPayees?.hasMatch(\lossP -> lossP.AccountContactRole.AccountContact.Contact.Name == lossPayee.AccountContactRole.AccountContact.Contact.Name_elem.$Value)) {
          var contactType = ConversionUtility.getContactType(lossPayee.AccountContactRole.AccountContact.Contact.Subtype)
          var policyLossPayee = bopBuilding.addNewPolicyLossOfPayeeOfContactType_TDIC(contactType)
          lossPayee.$TypeInstance.populateBOPLossPayee(bopBuilding, policyLossPayee)
          bopBuilding.addToBOPBldgLossPayees(policyLossPayee)
        }
      }
      for (additionalInsured in bldg.AdditionalInsureds.Entry) {
        if (!bopBuilding.AdditionalInsureds?.hasMatch(\aadlIns -> aadlIns.AccountContactRole.AccountContact.Contact.Name == additionalInsured.AccountContactRole.AccountContact.Contact.Name_elem.$Value)) {
          var policyAddlInsDetail =
              bopBuilding.addNewAdditionalInsuredDetailOfContactType_TDIC(ConversionUtility.
                  getContactType(additionalInsured.AccountContactRole.AccountContact.Contact.Subtype))
          additionalInsured.AccountContactRole.AccountContact.
              Contact.$TypeInstance.populateContact(policyAddlInsDetail.PolicyAddlInsured.AccountContactRole.AccountContact.Contact)
          bopBuilding.addToAdditionalInsureds(policyAddlInsDetail.PolicyAddlInsured)
        }
      }
    }
  }


}
