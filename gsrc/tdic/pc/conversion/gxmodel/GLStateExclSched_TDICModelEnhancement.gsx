package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.glstateexclsched_tdicmodel.types.complex.GLStateExclSched_TDIC

enhancement GLStateExclSched_TDICModelEnhancement : GLStateExclSched_TDIC {

  function populateGLStateExclSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddStateExclSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
  }
}
