package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.glmobiledcsched_tdicmodel.types.complex.GLMobileDCSched_TDIC

enhancement GLMobileDCSched_TDICModelEnhancement : GLMobileDCSched_TDIC {

  function populateGLMobileDCSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddMobileDCSched_TDIC()
    SimpleValuePopulator.populate(this, sched)
  }
}
