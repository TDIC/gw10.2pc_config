package tdic.pc.conversion.gxmodel

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator

enhancement NoteModelEnhancement : tdic.pc.conversion.gxmodel.notemodel.types.complex.Note {

  function populateNote(pp : PolicyPeriod) {
    var note = new Note(pp)
    SimpleValuePopulator.populate(this, note)
    var user = Query.make(User).join(User#Credential).compare(Credential#UserName, Relop.Equals, this.Author.Credential.UserName).select().FirstResult
    note.Author =  user == null ? Query.make(User).join(User#Credential).compare(Credential#UserName, Relop.Equals, "cu").select().FirstResult : user
    note.Language = LanguageType.TC_EN_US
    note.Level = pp.Policy
    pp.addToNotes(note)
  }

}
