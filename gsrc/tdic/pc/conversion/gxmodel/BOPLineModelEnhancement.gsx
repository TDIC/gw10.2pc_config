package tdic.pc.conversion.gxmodel

enhancement BOPLineModelEnhancement : tdic.pc.conversion.gxmodel.boplinemodel.types.complex.BOPLine {

  function populateBOPLineModel(period : PolicyPeriod) {
    period.BOPLine.BOPLocations?.each(\elt -> {
      elt.remove()
    })
    this.BOPLocations.Entry.each(\elt -> elt.$TypeInstance.populateBOPLocation(period.BOPLine, this))
  }


}
