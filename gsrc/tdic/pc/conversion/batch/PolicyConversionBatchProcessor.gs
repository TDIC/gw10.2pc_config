package tdic.pc.conversion.batch

uses com.tdic.pc.base.TDICProfilerTag
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.DateUtil
uses gw.processes.BatchProcessBase
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.PayloadDAO
uses tdic.pc.conversion.dao.PayloadStatusDAO
uses tdic.pc.conversion.dao.PolicyConversionDAO
uses tdic.pc.conversion.dao.PolicyPeriodDAO
uses tdic.pc.conversion.dto.PayloadDTO
uses tdic.pc.conversion.dto.PolicyConversionDTO
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.helper.PolicyConversionHelperFactory

class PolicyConversionBatchProcessor extends BatchProcessBase {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CLASS_NAME = PolicyConversionBatchProcessor.Type.Name

  construct() {
    super(BatchProcessType.TC_POLICYCONVERSION_EXT)
  }

  override function requestTermination() : boolean {
    var methodName = "requestTermination"
    super.requestTermination()
    _logger.info(CLASS_NAME + " : " + methodName + ": Terminated")

    return true
  }

  override function doWork() : void {

    var methodName = "doWork"
    var batchID = DateUtil.currentDate().Time
    var accountNum : String
    var policyNum : String
    var offering : String
    _logger.info("${CLASS_NAME} :${methodName} :starts with batchID: {}..", batchID)
    OperationsCompleted = 0
    using(new TDICProfilerTag("ConversionBatch")) {
      try {
        var payloadTimeDTO = new PayloadDTO()
        payloadTimeDTO.BatchID = batchID
        payloadTimeDTO.BatchType = "PC"

        var convertedPolicies = new ArrayList<PolicyPeriodDTO>()
        if (terminate())
          return
        var policies : ArrayList<PolicyPeriodDTO>
        using(new TDICProfilerTag("RetrieveAllUnprocessed")) {
          policies = new PolicyPeriodDAO("PC").retrieveAllUnprocessed()
        }
        _logger.info("${CLASS_NAME} :${methodName} :Processing total policies : {}", policies.Count)
        using(new TDICProfilerTag("LoopToPullXMLPayload")) {
          for (policyDTO in policies) {
            try {
                defaultValues(policyDTO)
                //Retrieve payload xml
                var payload = new PayloadDAO("PC", new Object[]{policyDTO.PolicyNumber,
                    policyDTO.OFFERING_LOB, policyDTO.Counter == null ? 0 : policyDTO.Counter}).retrieveAllUnprocessed().first()
                if(checkIfExists(payload.GWPolicyNumber, policyDTO, batchID)){
                  continue
                }
                if (TerminateRequested) {
                  _logger.info(CLASS_NAME + " : " + methodName + ": Terminating Batch Process ")
                  return
                }
                if (payload != null) {
                  _logger.info(CLASS_NAME + " : " + methodName + ": Starting conversion for Policy: " + policyDTO.PolicyNumber)
                  accountNum = payload.AccountKey
                  policyNum = payload.PolicyNumber
                  offering = payload.OfferingLOB
                  var jobNumber = PolicyConversionHelperFactory.getConversionHelper(policyDTO).startConversion(payload)
                  _logger.info(CLASS_NAME + " : " + methodName + ": Policy created with JobNumber: " + jobNumber)

                  payloadTimeDTO.AccountKey = policyDTO.AccountId
                  payloadTimeDTO.PolicyNumber = policyDTO.PolicyNumber
                  new PayloadStatusDAO().update(payloadTimeDTO)
                }

                if (terminate())
                  return

                var policyConversionDetails = new PolicyConversionDTO(policyDTO.AccountId, policyDTO.PolicyNumber, policyDTO.OFFERING_LOB,
                    policyDTO.Counter == null ? 0 : policyDTO.Counter, policyDTO.BaseState, policyDTO.TermNumber as String, policyDTO.IndustryCode, batchID)
                new PolicyConversionDAO().persist(policyConversionDetails)

                convertedPolicies.add(policyDTO)

                incrementOperationsCompleted()
            } catch (e : Exception) {
              _logger.error("${CLASS_NAME} :${methodName} :Error details " + e.StackTraceAsString)
              incrementOperationsFailed()
              CommonConversionHelper.saveConversionError(policyNum, accountNum, offering, "PC", batchID, e)
              //new PayloadDAO().update(payloadTimeDTO)
              //CommonConversionHelper.saveConversionError(policyPeriodDTO.PolicyNumber, policyPeriodDTO.AccountKey, BatchType.POLICY_CONVERSION, batchID, e)
            }
            if (terminate())
              return
          }
        }
        //Save Converted Policies
        if (convertedPolicies.Count != 0)
          new PolicyPeriodDAO().markAsProcessed(convertedPolicies)
        new PayloadStatusDAO().update(payloadTimeDTO)
        if (terminate())
          return
      } catch (e : Exception) {
        _logger.error("${CLASS_NAME} :${methodName} :Error details " + e.StackTraceAsString)
        incrementOperationsCompleted()
        incrementOperationsFailed()
        CommonConversionHelper.saveConversionError(policyNum, accountNum, offering, "PC", batchID, e)
      }
    }
  }

  private function terminate() : boolean {
    var methodName = "terminate"
    if (TerminateRequested) {
      _logger.warn("${CLASS_NAME} :${methodName} :Termination of the process requested.")
      return true
    }
    return false
  }

  private function defaultValues(policyPeriodDTO : PolicyPeriodDTO) {
    policyPeriodDTO.UWCompany = ConversionConstants.DEFAULT_UWCOMPANY
    policyPeriodDTO.IndustryCode = ConversionConstants.DEFAULT_INDUSTRYCODE
    var producerCode = ConversionConstants.PRODUCERCODE_BY_STATE.get(policyPeriodDTO.BaseState)
    policyPeriodDTO.ProducerCode =  producerCode == null ? ConversionConstants.DEFAULT_PRODUCERCODE : producerCode
    policyPeriodDTO.TermType = ConversionConstants.DEFAULT_TERMTYPE
  }

  private function checkIfExists(policyNumber : String, policyDTO : PolicyPeriodDTO, batchID : Long) : boolean {
    var pps = Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Relop.Equals, policyNumber)
    .compareNotIn(PolicyPeriod#Status, {PolicyPeriodStatus.TC_WITHDRAWN, PolicyPeriodStatus.TC_NONRENEWED}).select()
    if(!pps.Empty){
      var policyConversionDetails = new PolicyConversionDTO(policyDTO.AccountId, policyDTO.PolicyNumber, policyDTO.OFFERING_LOB,
          policyDTO.Counter == null ? 0 : policyDTO.Counter, policyDTO.BaseState, policyDTO.TermNumber as String, policyDTO.IndustryCode, batchID)
      new PolicyConversionDAO().persist(policyConversionDetails)
      return true
    }
    return false
  }

}