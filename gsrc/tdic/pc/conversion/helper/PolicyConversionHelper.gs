package tdic.pc.conversion.helper

uses gw.api.database.Query
uses tdic.pc.common.utility.PolicyNumberUtility
uses tdic.pc.conversion.account.AccountConversionHelper
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.AddressDAO
uses tdic.pc.conversion.dao.ContactDAO
uses tdic.pc.conversion.dao.NoteDAO
uses tdic.pc.conversion.dao.PolicyContactDAO
uses tdic.pc.conversion.dao.PolicyLocationDAO
uses tdic.pc.conversion.dto.ContactDTO
uses tdic.pc.conversion.dto.PayloadDTO
uses tdic.pc.conversion.dto.PolicyLocationDTO
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.exception.ConversionException
uses tdic.pc.conversion.gxmodel.accountcontactrolemodel.anonymous.elements.AccountContactRole_AccountContact_Contact
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs_Entry
uses tdic.pc.conversion.gxmodel.policycontactrolemodel.anonymous.elements.PolicyContactRole_AccountContactRole
uses tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod
uses tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_PolicyContactRoles_Entry
uses tdic.pc.conversion.services.TDICPolicyRenewalAPI
uses tdic.pc.conversion.util.ConversionUtility

abstract class PolicyConversionHelper {

  private static final var _logger = LoggerUtil.CONVERSION
  //private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"
  private static final var CLASS_NAME = PolicyConversionHelper.Type.Name

  private static final var CURRENCY = Currency.TC_USD

  var _policyPeriodDTO : PolicyPeriodDTO as PolicyDTO
  var _policyPeriod : PolicyPeriod as PolicyPeriod


  construct(policyPeriodDTO : PolicyPeriodDTO) {
    _policyPeriodDTO = policyPeriodDTO
  }

  private construct() {
  }

  abstract function generatePayloadXML() : String

  function preparePayload() {

    _policyPeriod = new tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod()

    _policyPeriod.LegacyPolicyNumber_TDIC = _policyPeriodDTO.LegacyPolicyNumber_TDIC
    _policyPeriod.LegacyPolicySuffix_TDIC = _policyPeriodDTO.LegacyPolicySuffix_TDIC
    _policyPeriod.LegacyPolicySource_TDIC = _policyPeriodDTO.SourceSystemCd
    _policyPeriod.Offering.Product.CodeIdentifier = ConversionConstants.OFFERRINGTYPE_MAPPER.get(_policyPeriodDTO.OFFERING_LOB)
    _policyPeriod.MultiLineDiscount_TDIC = _policyPeriodDTO.OFFERING_LOB == ConversionConstants.OfferingCodeIdentifier.CYB.Code ?
        null : (_policyPeriodDTO.MultiLineDiscount_TDIC =="" or _policyPeriodDTO.MultiLineDiscount_TDIC == null) ?
          MultiLineDiscount_TDIC.TC_N : MultiLineDiscount_TDIC.get(_policyPeriodDTO.MultiLineDiscount_TDIC)
    _policyPeriod.Policy.Account = new AccountConversionHelper(_policyPeriodDTO.AccountId, _policyPeriodDTO.BaseState).generateAccountModel()
    var orgType = PolicyOrgType_TDIC.get(_policyPeriodDTO.PolicyOrgType_TDIC)
    _policyPeriod.EffectiveDatedFields.PolicyOrgType_TDIC = orgType
    if(orgType == PolicyOrgType_TDIC.TC_OTHER){
      _policyPeriod.Policy.Account.OtherOrgTypeDescription = ConversionConstants.DEFAULT_ORGTYPE_DESC
    }
    initializeContactRoles()
    preparePNI()

    _policyPeriod.TermNumber = _policyPeriodDTO.TermNumber as Integer
    _policyPeriod.BaseState = Jurisdiction.get(_policyPeriodDTO.BaseState)
    _policyPeriod.WrittenDate = CommonConversionHelper.parseDate(_policyPeriodDTO.WrittenDate)
    var endDate = CommonConversionHelper.parseDate(_policyPeriodDTO.PeriodEnd)
    _policyPeriod.PeriodEnd = endDate
    _policyPeriod.PeriodStart = endDate.addYears(-1)
    _policyPeriod.Policy.OriginalEffectiveDate = CommonConversionHelper.parseDate(_policyPeriodDTO.OriginalEffectiveDate)
    _policyPeriod.BillingMethod = BillingMethod.get(ConversionConstants.DEFAULT_PAYMENT_PLAN)

    _policyPeriod.ProducerCodeOfRecord.Code = _policyPeriodDTO.ProducerCode
    _policyPeriod.UWCompany.Code = getUWCompanyCode(_policyPeriodDTO.UWCompany)

    _policyPeriod.PreferredCoverageCurrency = CURRENCY
    _policyPeriod.PreferredSettlementCurrency = CURRENCY
    _policyPeriod.EstimatedPremium.Amount = 0
    _policyPeriod.EstimatedPremium.Currency = CURRENCY

    /*var selectedPaymentPlan = ConversionConstants.PAYMENTPLAN_MAPPER.get(_policyPeriodDTO.PaymentPlanName)
    _policyPeriod.SelectedPaymentPlan.PaymentPlanType.Code = selectedPaymentPlan[0]
    _policyPeriod.SelectedPaymentPlan.Name = selectedPaymentPlan[1]*/

    _policyPeriod.TermType.Code = typekey.TermType.get(_policyPeriodDTO.TermType).Code
    // _policyPeriod.Policy.Account.BusOpsDesc = _policyPeriodDTO.BusinessDescription

    preparePolicyContacts()
    _policyPeriod.PolicyNumber = preparePolicyNumber()
    _policyPeriodDTO.GWPolicyNumber = _policyPeriod.PolicyNumber
    preparePolicyNotes()
    preparePaymentPlan()

  }

  private function preparePaymentPlan(){
    _policyPeriod.SelectedPaymentPlan.Name = ConversionConstants.PAYMENTPLAN_MAPPER.get(_policyPeriodDTO.PaymentPlan)
    _policyPeriod.SelectedPaymentPlan.BillingId = ConversionConstants.PAYMENTPLAN_BILLING_MAPPER.get(_policyPeriodDTO.PaymentPlan)
    _policyPeriod.SelectedPaymentPlan.InvoiceFrequency = BillingPeriodicity.get(ConversionConstants.PAYMENT_FREQUENCY_MAPPER.get(_policyPeriodDTO.PaymentPlan))//Using this as placeholder at renewal to match the payment plan. This invoice frequency will not get set on the policy.
    _policyPeriod.BankAccountNumber_TDIC = _policyPeriodDTO.BankAccountNumber == null ? null : _policyPeriodDTO.BankAccountNumber.replaceAll(" ","")
    _policyPeriod.BankABARoutingNumber_TDIC = _policyPeriodDTO.ABARoutingNumber
  }

  private function preparePolicyNotes(){
    var noteDAO = new NoteDAO(new Object[]{_policyPeriodDTO.PublicID})
    var notes = noteDAO.retrieveAllUnprocessed()
    for(note in notes index i){
      _policyPeriod.Notes.Entry[i].Topic = NoteTopicType.get(note.Topic)
      _policyPeriod.Notes.Entry[i].AuthoringDate = CommonConversionHelper.parseDate(note.AuthoringDate)
      _policyPeriod.Notes.Entry[i].SecurityType = NoteSecurityType.get(note.SecurityType)
      _policyPeriod.Notes.Entry[i].Subject = note.Subject
      _policyPeriod.Notes.Entry[i].Author.Credential.UserName = note.Author_ID
      var body = new StringBuilder()
      body.append(note.Body)
      body.append(System.getProperty("line.separator"))
      body.append("Created By : "+note.Author_ID)
      _policyPeriod.Notes.Entry[i].Body = body.toString()
    }
  }

  private function preparePolicyNumber() : String {
    var polNo = new StringBuilder()
    polNo.append(PolicyNumberUtility.getLegacyIdentifier(_policyPeriodDTO.SourceSystemCd))
    polNo.append(_policyPeriodDTO.LegacyPolicyIdentifier_TDIC)
    polNo.append(_policyPeriodDTO.Counter == null or _policyPeriodDTO.Counter == 0 ? 0 : _policyPeriodDTO.Counter)
    polNo.append(ConversionConstants.OFFERRING_IDENTIFIER_MAP.get(_policyPeriodDTO.OFFERING_LOB))
    return polNo.toString()
  }

  private function preparePolicyContacts() {
    var policyContactDAO = new PolicyContactDAO(new Object[]{_policyPeriodDTO.PublicID})
    var policyContacts = policyContactDAO.retrieveAllUnprocessed()
    for (contactRole in policyContacts index i) {
      var contactDAO = new ContactDAO({contactRole.ContactID})
      var contactDTO = contactDAO.retrieveAllUnprocessed().first()
      var contactRoleEntry = new PolicyPeriod_PolicyContactRoles_Entry()
      contactRoleEntry.AccountContactRole = preparePolicyContactRole(contactDTO)
      contactRoleEntry.Subtype = typekey.PolicyContactRole.get(contactRole.Subtype)
      _policyPeriod.PolicyContactRoles.Entry.add(contactRoleEntry)
    }
  }

  private static function getUWCompanyCode(companyName : String) : UWCompanyCode {
    return Query.make(UWCompany).compare(UWCompany#Name, Equals, companyName).select().FirstResult.Code
  }

  function prepareLocation(policyPeriod : PolicyPeriod, location : PolicyLocationDTO, i : int) {
    var locNum = location.LocationNum
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.LocationNum = locNum
    policyPeriod.PolicyLocations.Entry[i].LocationNum = locNum
    policyPeriod.PolicyLocations.Entry[i].OriginalEffDate_TDIC = CommonConversionHelper.parseDate(location.OriginalEffectiveDate)
    var addressDAO = new AddressDAO({location.AddressID})
    var addressDTO = addressDAO.retrieveAllUnprocessed().first()
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.AddressType = typekey.AddressType.get(addressDTO.AddressType)
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.AddressLine1 = addressDTO.AddressLine1?.replaceAll("\\n", "")
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.AddressLine2 = addressDTO.AddressLine2
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.AddressLine3 = addressDTO.AddressLine3
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.City = addressDTO.City
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.Country = Country.TC_US//get(_accountDTO.Country)
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.PostalCode = addressDTO.PostalCode
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.State = typekey.State.get(addressDTO.State)
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.County = addressDTO.County
    policyPeriod.PolicyLocations.Entry[i].AccountLocation.Description = location.AddrCntl == null ? null :location.AddrCntl.replaceAll(" ","")
  }

  function startConversion(payload : PayloadDTO) : String {
    var methodName = "startConversion"
    var jobNumber : String
    try {
      var policyAPI = new TDICPolicyRenewalAPI()
      /*var payloadStream:InputStream = payload.PayloadXML.getAsciiStream()
      var sw:StringWriter = new StringWriter()
      IOUtils.copy(payloadStream, sw)
      var payloadAsString:String = sw.toString()*/
      jobNumber = policyAPI.startConversionRenewal(payload.AccountKey, _policyPeriodDTO.ProducerCode,
          payload.GWPolicyNumber, payload.PayloadXML, null, null, _policyPeriodDTO.PeriodStart,
          _policyPeriodDTO.PeriodEnd)
    } catch (e : Exception) {
      _logger.error("${CLASS_NAME} :${methodName} : Error details" + e.StackTraceAsString)
      throw new ConversionException(e)
    }
    return jobNumber
  }

  /*
  private function prepareLineDetails(){
    switch(_policyPeriod.Policy.ProductCode){
      case ConversionConstants.ProductCode.GENERALLIABILITY.Type : prepareGLLine()
        break
      case ConversionConstants.ProductCode.BUSINESSOWNERS.Type : prepareBOPLine()
        break

      default:
        throw new ConversionException("Invalid LOB : "+ _policyPeriod.Policy.ProductCode+ " for payload preparation")
    }
  }*/

  function retrieveLocations() : List<PolicyLocationDTO> {
    var locationDAO = new PolicyLocationDAO(new Object[]{_policyPeriodDTO.PublicID, _policyPeriodDTO.OFFERING_LOB})
    var locations = locationDAO.retrieveAllUnprocessed()
    return locations
  }

  private function initializeContactRoles() {
    if (_policyPeriod.PolicyContactRoles.Entry == null) {
      _policyPeriod.PolicyContactRoles.Entry = new ArrayList<PolicyPeriod_PolicyContactRoles_Entry>()
    }
  }

  private function preparePNI() {
    var contactDAO = new ContactDAO({_policyPeriodDTO.PNIContactDenorm})
    var contactDTO = contactDAO.retrieveAllUnprocessed().first()
    overridePNI(contactDTO)
    var contactRoleEntry = new PolicyPeriod_PolicyContactRoles_Entry()
    contactRoleEntry.AccountContactRole = preparePolicyContactRole(contactDTO)
    contactRoleEntry.Subtype = typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED
    _policyPeriod.PolicyContactRoles.Entry.add(contactRoleEntry)

    var insured = _policyPeriod.PolicyContactRoles?.Entry?.firstWhere(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED)
    if (insured == null) {
      throw new ConversionException("No primary named inusred found in the policy contacts.")
    }
  }

  private function overridePNI(contactDTO : ContactDTO) {
    if (_policyPeriodDTO.PNIName == null or _policyPeriodDTO.PNIName.Empty) {
      return
    }
    contactDTO.FirstName = null
    contactDTO.LastName = _policyPeriodDTO.PNIName
    contactDTO.Name = _policyPeriodDTO.PNIName
    contactDTO.Credential_TDIC = null
    contactDTO.SubType = typekey.Contact.TC_COMPANY.Code
  }

  function preparePolicyContactRole(contactDTO : ContactDTO) : PolicyContactRole_AccountContactRole {
    var accountContactRole = new PolicyContactRole_AccountContactRole()
    accountContactRole.AccountContact.Contact.PrimaryAddress.AddressLine1 = " "
    if (ConversionConstants.CONTACT_PERSON.equalsIgnoreCase(contactDTO.SubType)) {
      accountContactRole.AccountContact.Contact.entity_Person.FirstName = contactDTO.FirstName?.length() > 30 ? contactDTO.FirstName?.substring(0, 29) : contactDTO.FirstName?.trim()
      accountContactRole.AccountContact.Contact.entity_Person.LastName = contactDTO.LastName?.length() > 30 ? contactDTO.LastName?.substring(0, 29) : contactDTO.LastName?.trim()
      accountContactRole.AccountContact.Contact.entity_Person.DateOfBirth = CommonConversionHelper.parseDate(contactDTO.DateOfBirth)
      accountContactRole.AccountContact.Contact.entity_Person.CellPhone = contactDTO.CellPhone == null ? null : contactDTO.CellPhone.contains("000-") ? null : contactDTO.CellPhone
      accountContactRole.AccountContact.Contact.entity_Person.LicenseNumber_TDIC = contactDTO.LicenseNumber
      accountContactRole.AccountContact.Contact.entity_Person.LicenseState = contactDTO.LicenseState == null ? null : Jurisdiction.get(contactDTO.LicenseState)
      accountContactRole.AccountContact.Contact.entity_Person.Credential_TDIC = contactDTO.Credential_TDIC == null ? null : Credential_TDIC.get(contactDTO.Credential_TDIC)
      accountContactRole.AccountContact.Contact.entity_Person.Component_TDIC = contactDTO.Component_TDIC == null ? null : Component_TDIC.get(contactDTO.Component_TDIC)
      accountContactRole.AccountContact.Contact.Subtype = typekey.Contact.TC_PERSON
      //_policyPeriod.EffectiveDatedFields.PolicyOrgType_TDIC = PolicyOrgType_TDIC.TC_SOLEPROPSHIP
    } else {
      accountContactRole.AccountContact.Contact.Name = contactDTO.Name == null ? null : contactDTO.Name.trim()
      accountContactRole.AccountContact.Contact.Subtype = typekey.Contact.TC_COMPANY
      //_policyPeriod.EffectiveDatedFields.PolicyOrgType_TDIC = PolicyOrgType_TDIC.TC_OTHER
    }
    var contact = accountContactRole.AccountContact.Contact.$TypeInstance
    populatePolicyContactAddress(contact.PrimaryAddress.$TypeInstance, contactDTO.PrimaryAddressID)
    contact.EmailAddress1 = contactDTO.EmailAddress1
    var officialIDCount : Integer = 0
    contact.OfficialIDs = contact.OfficialIDs == null ? new Contact_OfficialIDs() : contact.OfficialIDs
    contact.OfficialIDs.Entry = contact.OfficialIDs.Entry == null ? new ArrayList<Contact_OfficialIDs_Entry>() : contact.OfficialIDs.Entry
    officialIDCount = prepareOfficialID(accountContactRole.AccountContact.Contact, contactDTO.SSNbr, true, officialIDCount)
    if(contactDTO.SSNbr == null
        or (contactDTO.TaxID != null and contactDTO.TaxID.replaceAll("-","") != contactDTO.SSNbr.replaceAll("-","") )) {
      officialIDCount = prepareOfficialID(accountContactRole.AccountContact.Contact, contactDTO.TaxID, false, officialIDCount)
    }
    prepareADANumber(accountContactRole.AccountContact.Contact, contactDTO.getADANumberOfficialID_TDIC(contact.PrimaryAddress.State), officialIDCount)
    if (contactDTO.PrimaryPhone?.equalsIgnoreCase(PrimaryPhoneType.TC_HOME.Code)) {
      contact.PrimaryPhone = PrimaryPhoneType.TC_HOME
    } else {
      contact.PrimaryPhone = PrimaryPhoneType.TC_WORK
    }
    contact.WorkPhone = contactDTO.WorkPhone == null ? null : contactDTO.WorkPhone.contains("000-") ? null : contactDTO.WorkPhone
    contact.HomePhone = contactDTO.HomePhone == null ? null : contactDTO.HomePhone.contains("000-") ? null : contactDTO.HomePhone
    contact.FaxPhone = contactDTO.FaxPhone == null ? null : contactDTO.FaxPhone.contains("000-") ? null : contactDTO.FaxPhone
    return accountContactRole
  }

  function populatePolicyContactAddress(address : gw.webservice.pc.pc1000.gxmodel.addressmodel.types.complex.Address, primaryAddressID : String) {
    var addressDAO = new AddressDAO({primaryAddressID})
    var addressDTO = addressDAO.retrieveAllUnprocessed().first()
    address.AddressType = typekey.AddressType.get(addressDTO.AddressType)
    address.AddressLine1 = addressDTO.AddressLine1?.replaceAll("\\n", "")
    address.AddressLine2 = addressDTO.AddressLine2
    address.AddressLine3 = addressDTO.AddressLine3
    address.City = addressDTO.City
    address.Country = Country.TC_US//get(_accountDTO.Country)
    address.PostalCode = addressDTO.PostalCode
    address.State = typekey.State.get(addressDTO.State)
    address.County = addressDTO.County

  }

  function prepareOfficialID(contact : AccountContactRole_AccountContact_Contact,
                             taxID : String, ssnFlag : boolean, officialIDCount : Integer) : Integer {
    if (taxID == null) {
      return officialIDCount
    }
    var taxEntry = new Contact_OfficialIDs_Entry()
    if (ssnFlag or taxID.matches(ConversionConstants.TAXID_FORMAT)) {
      taxEntry.OfficialIDType = OfficialIDType.TC_SSN
      taxEntry.OfficialIDValue = ConversionUtility.formatSSN(taxID)
    } else {
      taxEntry.OfficialIDType = OfficialIDType.TC_FEIN
      taxEntry.OfficialIDValue = ConversionUtility.formatFEIN(taxID)
    }
    contact.OfficialIDs.Entry[officialIDCount] = taxEntry
    officialIDCount = officialIDCount + 1
    return officialIDCount
  }

  function prepareADANumber(contact : AccountContactRole_AccountContact_Contact, adaNumber : String, officialIDCount : Integer) {
    if (adaNumber == null) {
      return
    }
    var adaEntry = new Contact_OfficialIDs_Entry()
    adaEntry.OfficialIDType = OfficialIDType.TC_ADANUMBER_TDIC
    adaEntry.OfficialIDValue = adaNumber
    contact.OfficialIDs.Entry[officialIDCount] = adaEntry
  }

}