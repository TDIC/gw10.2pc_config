package tdic.pc.conversion.helper

uses entity.AccountContact
uses entity.Contact
uses gw.address.AddressQueryBuilder
uses gw.api.database.PCBeanFinder
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.DateUtil
uses gw.contact.ContactQueryBuilder
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.ConversionErrorDAO
uses tdic.pc.conversion.dto.ConversionErrorDTO

uses java.text.SimpleDateFormat

class CommonConversionHelper {

  private static final var TIMEZONE_FORMAT = "America/Los_Angeles"
  private static final var _logger = LoggerUtil.CONVERSION
  private static var DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  public static function saveConversionError(policyNumber : String, accountKey : String, offering : String, batchType : String,
                                             batchID : long, exception : Exception) {
    var errorDTO = new ConversionErrorDTO(policyNumber, accountKey, offering, batchType, batchID)
    errorDTO.ErrorMessageBusiness = safeSubString(exception.Message, 490)
    errorDTO.ErrorMessageTechnical = safeSubString(exception.StackTraceAsString, 1190)
    new ConversionErrorDAO().persist(errorDTO)
  }

  public static function existingContact(account : Account, contactXml : tdic.pc.conversion.gxmodel.contactmodel.types.complex.Contact) : AccountContact {
    for (contact in account.AccountContacts) {
      var duplicate : AccountContact
      var con = contact.Contact
      //Check for Duplicate Name
      if (con typeis Person and ( null != contactXml.entity_Person?.FirstName or null != contactXml.entity_Person?.LastName)) {
        if (con.FirstName?.equalsIgnoreCase(contactXml.entity_Person?.FirstName) && con.LastName?.equalsIgnoreCase(contactXml.entity_Person?.LastName)) {
          duplicate = contact
        }
      } else if (con typeis Company && contactXml.Name != null) {
        if (con.Name?.equalsIgnoreCase(contactXml.Name)) {
          duplicate = contact
        }
      }

      if (duplicate != null) {
        //Check for duplicate Fein & DOLID
        var fein = contactXml.OfficialIDs?.
            Entry?.firstWhere(\elt -> elt.OfficialIDType == OfficialIDType.TC_FEIN)?.OfficialIDValue
        var dolid = contactXml.OfficialIDs?.
            Entry?.firstWhere(\elt -> elt.OfficialIDType == OfficialIDType.TC_DOLID)?.OfficialIDValue
        var ncciid = contactXml.OfficialIDs?.
            Entry?.firstWhere(\elt -> elt.OfficialIDType == OfficialIDType.TC_NCCIINTRASTATE)?.OfficialIDValue
        if (fein != null && fein != duplicate.Contact.FEINOfficialID)
          duplicate.Contact.FEINOfficialID = fein
        //continue
        if (dolid != null && dolid != duplicate.Contact.DOLIDOfficialID)
          duplicate.Contact.DOLIDOfficialID = dolid
        //continue
        if (ncciid != null && ncciid != duplicate.Contact.NCCIintrastateOfficialID)
          duplicate.Contact.NCCIintrastateOfficialID = ncciid
        //continue
        //Check for duplicate email
        if (duplicate.Contact.EmailAddress1 != null && contactXml.EmailAddress1 != null
            && duplicate.Contact.EmailAddress1 != contactXml.EmailAddress1) {
          continue
        }
        //Check for duplicate phone
        if (duplicate.Contact.PrimaryPhoneValue != null && contactXml.WorkPhone != null
            && !contactXml.WorkPhone.replaceAll("-", "").contains(duplicate.Contact.PrimaryPhoneValue.replaceAll("-", ""))) {
          continue
        }
        return duplicate
      }

    }
    return null
  }

  public static function getIndustryCode(code : String, desc : String) : IndustryCode {
    var industryCode : IndustryCode
    if (code != null) {
      var industryCodes = Query.make(IndustryCode).compare(IndustryCode#Code, Relop.Equals, code).select()

      for (ic in industryCodes) {
        if (ic.Classification.toLowerCase() == (desc.toLowerCase().trim())) {
          industryCode = ic
          break
        }
      }
      if (industryCode == null)
        for (ic in industryCodes) {
          if (ic.Classification.toLowerCase().contains(desc.toLowerCase().trim())) {
            industryCode = ic
            break
          }
        }
      if (industryCode == null && industryCodes.Count != 0) {
        industryCode = industryCodes.FirstResult
      }
    }
    if (industryCode == null) {
      //throw new DataConversionException(ConversionConstants.NO_INDUSTRY_CODE_FOUND+code+":"+desc)
    }
    return industryCode
  }


  public static function getCurrentDateTime() : String {
    var currentDate = DateUtil.currentDate()
    return DATE_FORMAT.format(currentDate)
  }

  public static function getConvertedDate(date : Date) : String {
    return DATE_FORMAT.format(date)
  }

  public static function parseDate(date : String) : Date {
    var parsedDate : Date
    DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(TIMEZONE_FORMAT))
    if (date != null) {
      parsedDate = (date.length == 10) ? (new SimpleDateFormat("yyyy-MM-dd")).parse(date) : DATE_FORMAT.parse(date)
    }
    return parsedDate
  }

  public static function safeSubString(string : String, endIndex : int) : String {
    if (string == null) {
      return ""
    }
    if (string.length() < endIndex) {
      return string.substring(0, string.length())
    }
    return string.substring(0, endIndex)
  }

  public static function getUserID(firstName : String, lastName : String) : String {
    if (firstName == null && lastName == null) {
      firstName = "System"
      lastName = "User"
    }
    var result = Query.make(User).join("Contact").compareIgnoreCase("FirstName", Equals, firstName).compareIgnoreCase("LastName", Equals, lastName).select()

    if (result.FirstResult == null) {
      firstName = "System"
      lastName = "User"
      result = Query.make(User).join("Contact").compareIgnoreCase("FirstName", Equals, firstName).compareIgnoreCase("LastName", Equals, lastName).select()
    }

    _logger.info("Returning the user: " + firstName + " " + lastName)

    return result.FirstResult.PublicID
  }

  public static function getUserID(id : String) : User {
    var result = Query.make(User).compare(User#PublicID, Relop.Equals, id).select().FirstResult
    if (result == null) {
      var firstName = "System"
      var lastName = "User"
      result = Query.make(User).join("Contact").compareIgnoreCase("FirstName", Equals, firstName).compareIgnoreCase("LastName", Equals, lastName).select().FirstResult
    }
    return result
  }

  public static function getConversionUser() : User {
    var firstName = ConversionConstants.CONVERSION_USER_FIRSTNAME
    var lastName = ConversionConstants.CONVERSION_USER_LASTNAME

    var convuser = Query.make(User).join("Contact").compareIgnoreCase("FirstName", Equals, firstName).compareIgnoreCase("LastName", Equals, lastName)?.select()

    return convuser?.FirstResult
  }

  public static function getUWCompany(uwCompanyCode : UWCompanyCode) : UWCompany {
    return Query.make(entity.UWCompany).compare("Code", Equals, uwCompanyCode)?.select()?.first() //select().where( \ elt -> elt.Code == uwCompanyCode ).first()
  }

  public static function loadOrgByPublicID(publicID : String) : entity.Organization {
    return publicID == null ? null : PCBeanFinder.loadBeanByPublicID<entity.Organization>(publicID, entity.Organization)
  }

  public static function loadProducerCodeByPublicID(publicID : String) : entity.ProducerCode {
    return publicID == null ? null : PCBeanFinder.loadBeanByPublicID<entity.ProducerCode>(publicID, entity.ProducerCode)
  }

  public static function loadProducerCodeByCode(code : String) : entity.ProducerCode {
    return code == null ? null : Query.make(ProducerCode).compare(ProducerCode#Code, Equals, code).select().AtMostOneRow
  }

  /*public static function findBeanByPublicIDOrThrow<T extends KeyableBean>(orgPublicID : String) : T {
    var bean = Query.make(T).compare(KeyableBean.PUBLICID_DYNPROP, Relop.Equals, orgPublicID).select().AtMostOneRow
    if (bean == null) {
      throw new BadIdentifierException(DisplayKey.get("OrganizationModel.populateOrganization.Error.CannotFindForeignKeyBeanWithPublicID", T, orgPublicID))
    }
    return bean
  }*/
  public function existingContact(contactXml : tdic.pc.conversion.gxmodel.contactmodel.types.complex.Contact) : Contact {
    var contactQuery = new ContactQueryBuilder()
    if( null != contactXml.entity_Person.FirstName and contactXml.entity_Person.FirstName !=''){
      contactQuery.withFirstName(contactXml.entity_Person.FirstName)
    }
    if( null != contactXml.entity_Person.LastName and contactXml.entity_Person.LastName !=''){
      contactQuery.withLastName(contactXml.entity_Person.LastName)
    }
    if( null != contactXml.Name and contactXml.Name !=''){
      contactQuery.withCompanyName(contactXml.Name)
    }
    if( null != contactXml.OfficialIDs.Entry and  !contactXml.OfficialIDs.Entry.Empty and
        contactXml.OfficialIDs.Entry.firstWhere(\elt ->
            elt.OfficialIDType == OfficialIDType.TC_SSN or elt.OfficialIDType == OfficialIDType.TC_FEIN) != null){
      var ssn = contactXml.OfficialIDs.Entry.firstWhere(\elt -> elt.OfficialIDType == OfficialIDType.TC_SSN)
      if(ssn != null){
        contactQuery.withOfficialId(ssn.OfficialIDValue)
      }else{
        var fein = contactXml.OfficialIDs.Entry.firstWhere(\elt -> elt.OfficialIDType == OfficialIDType.TC_FEIN).OfficialIDValue
        contactQuery.withOfficialId(fein)
      }
    }
    var addressQueryBuilder = new AddressQueryBuilder()
        .withAddressLine1(contactXml.PrimaryAddress.AddressLine1)
        .withCity(contactXml.PrimaryAddress.City)
        .withState(contactXml.PrimaryAddress.State)
    contactQuery.withPrimaryAddress(addressQueryBuilder)
    var result = contactQuery.build().select()
    if(result != null and !result.Empty){
      return result.first() as Contact
    }
    return null
  }


}