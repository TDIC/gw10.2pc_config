package tdic.pc.conversion.lob.gl

uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.AddlInsuredDAO
uses tdic.pc.conversion.dao.ContactDAO
uses tdic.pc.conversion.dao.GLAddlInsuredDAO
uses tdic.pc.conversion.dao.GLAnestheticModalityDAO
uses tdic.pc.conversion.dao.GLCertofInsDAO
uses tdic.pc.conversion.dao.GLConditionsDAO
uses tdic.pc.conversion.dao.GLCoverageDAO
uses tdic.pc.conversion.dao.GLDentalBLAIDAO
uses tdic.pc.conversion.dao.GLExecludedServicesDAO
uses tdic.pc.conversion.dao.GLExposureDAO
uses tdic.pc.conversion.dao.GLLocumTenensDAO
uses tdic.pc.conversion.dao.GLManuscriptDAO
uses tdic.pc.conversion.dao.GLNameOTDDAO
uses tdic.pc.conversion.dao.GLRateFactorDAO
uses tdic.pc.conversion.dao.GLRiskManagementDAO
uses tdic.pc.conversion.dao.GLStateExclDAO
uses tdic.pc.conversion.dao.PracticeLocationDAO
uses tdic.pc.conversion.dao.UWQuestionsDAO
uses tdic.pc.conversion.dto.GLConditionDTO
uses tdic.pc.conversion.dto.GLCoverageDTO
uses tdic.pc.conversion.dto.PolicyLocationDTO
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.gxmodel.generalliabilitycovmodel.anonymous.elements.GeneralLiabilityCov_CovTerms
uses tdic.pc.conversion.gxmodel.generalliabilitycovmodel.anonymous.elements.GeneralLiabilityCov_CovTerms_Entry
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_Answers
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_Answers_Entry
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_GLGeneralAnesthesiaInOffice_TDIC_Entry
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_GLIVIMSedationInHospital_TDIC_Entry
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_GLLineConditions_Entry
uses tdic.pc.conversion.gxmodel.gllinemodel.anonymous.elements.GLLine_GLLineCoverages_Entry
uses tdic.pc.conversion.gxmodel.policycontactrolemodel.anonymous.elements.PolicyContactRole_AccountContactRole
uses tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.helper.PolicyConversionHelper
uses tdic.pc.conversion.util.ConversionUtility

class GLPolicyConversionHelper extends PolicyConversionHelper {

  private var _productCode : ConversionConstants.ProductCode
  private var _policyPeriod : PolicyPeriod
  private var _policyPeriodDTO : PolicyPeriodDTO

  construct(policyPeriodDTO : PolicyPeriodDTO, productCode : ConversionConstants.ProductCode) {
    super(policyPeriodDTO)
    _productCode = productCode
    _policyPeriodDTO = policyPeriodDTO
  }

  override function generatePayloadXML() : String {
    super.preparePayload()
    _policyPeriod = super.PolicyPeriod
    _policyPeriod.Policy.ProductCode = _productCode.Type
    prepareGLLine()
    return _policyPeriod.asUTFString()
  }

  private function prepareGLLine() {
    _policyPeriod.GLLine.EffectiveDate = _policyPeriod.PeriodStart
    var locations = retrieveLocations()
    prepareGLClassifications()
    if (locations != null && locations.size() != 0) {
      prepareGLLocations(locations)
      overrideNonMatchingLocations()
    }
    prepareGLManuscript_TDIC()
    prepareGLCoverages()
    prepareLongManuscriptPNI()
    prepareGLConditions()
    prepareGLModifiers()
    prepareRiskManagement()
    prepareGLMobileDCSched_TDIC()
    prepareGLLocumTenensSched_TDIC()
    prepareGLAddlInsSched_TDIC()
    prepareGLNameODSched_TDIC()
    prepareGLStateExclSched_TDIC()
    prepareGLDentalBLAISched_TDIC()
    prepareGLCertofInsSched_TDIC()
    prepareGLExcludedServiceSched_TDIC()
    prepareGLUWInformation()
    prepareGLAnestheticModalities()
    prepareNewDentistToNewGrad()
    updateNewGradDiscount()
  }

  private function updateNewGradDiscount(){
    var gradExp : Date
    if(null != _policyPeriod.GLLine.Answers.Entry and !_policyPeriod.GLLine.Answers.Entry.Empty){
      var q = _policyPeriod.GLLine.Answers.Entry.firstWhere(\elt -> elt.QuestionCode == 'NewGradDiscExpDate_TDIC')
      gradExp = q.DateAnswer
    }
    if(gradExp != null and _policyPeriod.PeriodStart.addYears(1) >= gradExp){
      if(null != _policyPeriod.GLLine.GLLineConditions.Entry and !_policyPeriod.GLLine.GLLineConditions.Entry.Empty){
        _policyPeriod.GLLine.GLLineConditions.Entry.removeWhere(\elt -> elt.Pattern.CodeIdentifier == "GLNewGradDiscount_TDIC")
      }
    }
  }

  private function prepareNewDentistToNewGrad(){
    if(_policyPeriodDTO.AS400_Ptype != ConversionConstants.NEWDENTIST_AS400_PTYPE){
      return
    }
    var condsEntry = _policyPeriod.GLLine.GLLineConditions.Entry
    if(condsEntry == null or condsEntry.Empty){
      condsEntry = new ArrayList<GLLine_GLLineConditions_Entry>()
    }
    var newDentist = new GLLine_GLLineConditions_Entry()
    newDentist.Pattern.CodeIdentifier = ConversionConstants.NEWDENTIST_CONDITION_PATTERNCODE
    condsEntry.add(newDentist)
  }

  private function prepareGLUWInformation(){
    var glUWAnsDAO = new UWQuestionsDAO({_policyPeriodDTO.PublicID})
    var qs = glUWAnsDAO.retrieveAllUnprocessed()
    if(qs.Empty){
      return
    }
    initializeAnswers()
    Collections.sort(qs)
    for(q in qs index i) {
      var ans = new GLLine_Answers_Entry()
      ans.QuestionCode = q.QuestionCd
      switch(q.Answer){
        case "Y":
        case "N":
          ans.BooleanAnswer = q.Answer == "Y"? true : false
          break
        default:
          if(q.Answer.matches("\\d{4}-\\d{2}-\\d{2}")){
            ans.DateAnswer = ConversionUtility.parseDateFromString(q.Answer, q.QuestionCd)
          }else{
            ans.TextAnswer = q.Answer
          }
          break
      }
      _policyPeriod.GLLine.Answers.Entry.add(ans)
    }

  }

  private function prepareRiskManagement() {
    var riskMgmtDAO = new GLRiskManagementDAO({_policyPeriodDTO.PublicID})
    for (risk in riskMgmtDAO.retrieveAllUnprocessed()index i) {
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].Name = risk.Name
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].Attended = risk.Attended == null ? null : risk.Attended
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].LPSDate = CommonConversionHelper.parseDate(risk.LPSDate)
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].StartDate = CommonConversionHelper.parseDate(risk.StartDate)
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].EndDate = CommonConversionHelper.parseDate(risk.EndDate)
      _policyPeriod.GLLine.RiskManagementDisc_TDIC.Entry[i].LocationCode = risk.LocationCode
    }
  }

  private function prepareGLCertofInsSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var addlDAO = new GLCertofInsDAO({_policyPeriodDTO.PublicID})
      var addlIns = addlDAO.retrieveAllUnprocessed()
      for (addl in addlIns index i) {
        var addlInsuredDAO = new AddlInsuredDAO(new Object[]{addl.AdditionalInsured})
        var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
        for (contactRole in addlContacts) {
          var contactDAO = new ContactDAO({contactRole.ContactID})
          var contactDTO = contactDAO.retrieveAllUnprocessed().first()
          _policyPeriod.GLLine.GLCertofInsSched_TDIC.Entry[i].
              CertificateHolder.AccountContactRole = preparePolicyContactRole(contactDTO)
          _policyPeriod.GLLine.GLCertofInsSched_TDIC.Entry[i].CertificateHolder.entity_PolicyCertificateHolder_TDIC.Description=contactRole.Description
          _policyPeriod.GLLine.GLCertofInsSched_TDIC.Entry[i].LTEffectiveDate = CommonConversionHelper.parseDate(addl.LTEffectiveDate)
          _policyPeriod.GLLine.GLCertofInsSched_TDIC.Entry[i].LTExpirationDate = CommonConversionHelper.parseDate(addl.LTExpirationDate)
        }
      }
    }
  }

  private function prepareGLDentalBLAISched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var addlDAO = new GLDentalBLAIDAO({_policyPeriodDTO.PublicID})
      var addlIns = addlDAO.retrieveAllUnprocessed()
      for (addl in addlIns index i) {
        var addlInsuredDAO = new AddlInsuredDAO(new Object[]{addl.AdditionalInsured})
        var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
        for (contactRole in addlContacts) {
          var contactDAO = new ContactDAO({contactRole.ContactID})
          var contactDTO = contactDAO.retrieveAllUnprocessed().first()
          _policyPeriod.GLLine.GLDentalBLAISched_TDIC.Entry[i].
              AdditionalInsured.PolicyAddlInsured.AccountContactRole = preparePolicyContactRole(contactDTO)
          _policyPeriod.GLLine.GLDentalBLAISched_TDIC.Entry[i].AdditionalInsured.Desciption = contactRole.Description
          _policyPeriod.GLLine.GLDentalBLAISched_TDIC.Entry[i].AdditionalInsured.AdditionalInsuredType = typekey.AdditionalInsuredType.TC_OTHER
        }
      }
    }
  }

  private function prepareGLStateExclSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var stateExclDAO = new GLStateExclDAO({_policyPeriodDTO.PublicID})
      var stateExcls = stateExclDAO.retrieveAllUnprocessed()
      for (stateExcl in stateExcls index i) {
        _policyPeriod.GLLine.GLStateExclSched_TDIC.Entry[i].State = stateExcl.State
      }
    }
  }

  private function prepareGLNameODSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var addlDAO = new GLNameOTDDAO({_policyPeriodDTO.PublicID})
      var addlIns = addlDAO.retrieveAllUnprocessed()
      for (addl in addlIns index i) {
        _policyPeriod.GLLine.GLNameODSched_TDIC.Entry[i].LTEffectiveDate = CommonConversionHelper.parseDate(addl.LTEffectiveDate)
        _policyPeriod.GLLine.GLNameODSched_TDIC.Entry[i].LTExpirationDate = CommonConversionHelper.parseDate(addl.LTExpirationDate)
        var addlInsuredDAO = new AddlInsuredDAO(new Object[]{addl.AdditionalInsured})
        var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
        for (contactRole in addlContacts) {
          var contactDAO = new ContactDAO({contactRole.ContactID})
          var contactDTO = contactDAO.retrieveAllUnprocessed().first()
          var pcr = preparePolicyContactRole(contactDTO)
          _policyPeriod.GLLine.GLNameODSched_TDIC.Entry[i].
              AdditionalInsured.PolicyAddlInsured.AccountContactRole = pcr
          _policyPeriod.GLLine.GLNameODSched_TDIC.Entry[i].AdditionalInsured.AdditionalInsuredType = typekey.AdditionalInsuredType.TC_OTHER
          _policyPeriod.GLLine.GLNameODSched_TDIC.Entry[i].DoctorName = prepareContactNameFromPCR(pcr)
        }
      }
    }
  }

  private function prepareContactNameFromPCR(pcr : PolicyContactRole_AccountContactRole): String{
    var contact = pcr.AccountContact.Contact
    if(contact.Name != null){
      return contact.Name
    }
    var str = new StringBuilder("")
    var person = contact.entity_Person
    if(person.FirstName!=null){str.append(person.FirstName)}
    if(person.MiddleName!=null){str.append(person.MiddleName)}
    if(person.LastName != null){str.append(" ")str.append(person.LastName)}
    return str.toString()
  }

  private function prepareGLAddlInsSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var addlDAO = new GLAddlInsuredDAO({_policyPeriodDTO.PublicID})
      var addlIns = addlDAO.retrieveAllUnprocessed()
      for (addl in addlIns index i) {
        _policyPeriod.GLLine.GLAdditionInsdSched_TDIC.Entry[i].LTEffectiveDate = CommonConversionHelper.parseDate(addl.LTEffectiveDate)
        _policyPeriod.GLLine.GLAdditionInsdSched_TDIC.Entry[i].LTExpirationDate = CommonConversionHelper.parseDate(addl.LTExpirationDate)
        var addlInsuredDAO = new AddlInsuredDAO(new Object[]{addl.AdditionalInsured})
        var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
        for (contactRole in addlContacts) {
          var contactDAO = new ContactDAO({contactRole.ContactID})
          var contactDTO = contactDAO.retrieveAllUnprocessed().first()
          _policyPeriod.GLLine.GLAdditionInsdSched_TDIC.Entry[i].
              AdditionalInsured.PolicyAddlInsured.AccountContactRole = preparePolicyContactRole(contactDTO)
          _policyPeriod.GLLine.GLAdditionInsdSched_TDIC.Entry[i].AdditionalInsured.AdditionalInsuredType = typekey.AdditionalInsuredType.TC_OTHER
        }
      }
    }
  }

  private function prepareGLLocumTenensSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var locumTenensDAO = new GLLocumTenensDAO({_policyPeriodDTO.PublicID})
      var locumTenens = locumTenensDAO.retrieveAllUnprocessed()
      for (locum in locumTenens index i) {
        _policyPeriod.GLLine.GLLocumTenensSched_TDIC.Entry[i].LTEffectiveDate = CommonConversionHelper.parseDate(locum.LTEffectiveDate)
        _policyPeriod.GLLine.GLLocumTenensSched_TDIC.Entry[i].LTExpirationDate = CommonConversionHelper.parseDate(locum.LTExpirationDate)
        _policyPeriod.GLLine.GLLocumTenensSched_TDIC.Entry[i].DentistName = locum.DentistName
      }
    }
  }

  private function prepareGLExcludedServiceSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      var exludedservDAO = new GLExecludedServicesDAO({_policyPeriodDTO.PublicID})
      var  exludedserv = exludedservDAO.retrieveAllUnprocessed()
      for (servs in exludedserv index i) {
        //_policyPeriod.GLLine.GLExcludedServiceSched_TDIC.Entry[i].LT = CommonConversionHelper.parseDate(servs.LTEffectiveDate)
        //_policyPeriod.GLLine.GLExcludedServiceSched_TDIC.Entry[i].LTExpirationDate = CommonConversionHelper.parseDate(servs.LTExpirationDate)
        _policyPeriod.GLLine.GLExcludedServiceSched_TDIC.Entry[i].ExcludedEntity = servs.Excludedentity
      }
    }
  }




  private function prepareGLMobileDCSched_TDIC() {
    if (_policyPeriod.GLLine.GLLineCoverages.Entry == null or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty) {
      return
    }
    if (_policyPeriod.GLLine.GLLineCoverages.Entry.firstWhere(\elt ->
        ConversionConstants.AUTO_POPULATE_ENDORS_LIST.contains(elt.PatternCode)) != null) {
      _policyPeriod.GLLine.GLMobileDCSched_TDIC.Entry[0].LTEffectiveDate = _policyPeriod.PeriodStart
      _policyPeriod.GLLine.GLMobileDCSched_TDIC.Entry[0].LTExpirationDate = _policyPeriod.PeriodEnd
    }
  }

  private function prepareGLLocations(locations : List<PolicyLocationDTO>) {
    for (location in locations index i) {
      prepareLocation(_policyPeriod, location, i)
    }
  }

  private function overrideNonMatchingLocations() {
    var prtLocDAO = new PracticeLocationDAO({_policyPeriodDTO.LegacyPolicyIdentifier_TDIC})
    var newLocs = prtLocDAO.retrieveAllUnprocessed()
    for (newLoc in newLocs index i) {
      var loc = _policyPeriod.PolicyLocations.Entry.firstWhere(\elt -> newLoc.MailingLocation.contains(elt.AccountLocation.AddressLine1))
      if(loc != null){
        loc.AccountLocation.AddressLine1 = newLoc.AddressLine1
        loc.AccountLocation.City = newLoc.City
        loc.AccountLocation.PostalCode = newLoc.PostalCode
        loc.AccountLocation.State = typekey.State.get(newLoc.State)
        loc.AccountLocation.County = newLoc.County
        _policyPeriod.GLLine.Exposures.Entry.each(\elt -> {
          elt.GLTerritory_TDIC.Component = newLoc.Territory_Code
        })
      }
    }
  }

  private function prepareGLCoverages() {
    var glCovDAO = new GLCoverageDAO({_policyPeriodDTO.PublicID})
    var glCoverages = glCovDAO.retrieveAllUnprocessed()
    if (glCoverages.Empty) {
      return
    }
    var glCoveragesMap = new HashMap<String, List<GLCoverageDTO>>()
    glCoverages.each(\elt -> {
      if (!glCoveragesMap.containsKey(elt.CovPatternCode)) {
        var list = new ArrayList<GLCoverageDTO>()
        list.add(elt)
        glCoveragesMap.put(elt.CovPatternCode, list)
      } else {
        glCoveragesMap.get(elt.CovPatternCode).add(elt)
      }
    })
    for (cov in glCoveragesMap.entrySet()index i) {
      _policyPeriod.GLLine.GLLineCoverages.Entry[i].PatternCode = cov.Key
      for (covTerm in cov.Value index j) {
        if (covTerm.CovTermPatternCode != null) {
          _policyPeriod.GLLine.GLLineCoverages.Entry[i].CovTerms.Entry[j].PatternCode = covTerm.CovTermPatternCode
          _policyPeriod.GLLine.GLLineCoverages.Entry[i].CovTerms.Entry[j].DisplayValue =
              covTerm.DateTerm == null ?
                  covTerm.ChoiceTerm == null ?
                      covTerm.DirectTerm == null ?
                          null : covTerm.DirectTerm :
                      "Y".equalsIgnoreCase(covTerm.ChoiceTerm) ? "true" : "N".equalsIgnoreCase(covTerm.ChoiceTerm) ? "false" : covTerm.ChoiceTerm
                  : covTerm.DateTerm.toString()
        }
      }
    }
  }

  private function prepareGLConditions() {
    var glConditionDAO = new GLConditionsDAO({_policyPeriodDTO.PublicID})
    var glConditions = glConditionDAO.retrieveAllUnprocessed()
    if (glConditions.Empty) {
      return
    }
    var glConditionsMap = new HashMap<String, List<GLConditionDTO>>()
    glConditions.each(\elt -> {
      if (!glConditionsMap.containsKey(elt.CondPatternCode)) {
        var list = new ArrayList<GLConditionDTO>()
        list.add(elt)
        glConditionsMap.put(elt.CondPatternCode, list)
      } else {
        glConditionsMap.get(elt.CondPatternCode).add(elt)
      }
    })
    for (condition in glConditionsMap.entrySet()index i) {
      _policyPeriod.GLLine.GLLineConditions.Entry[i].Pattern.CodeIdentifier = condition.Key
      for (conditionTerm in condition.Value) {
        if (conditionTerm.CondTermPatternCode != null) {
          var j = _policyPeriod.GLLine.GLLineConditions.Entry[i].CovTerms.Entry.Count
          _policyPeriod.GLLine.GLLineConditions.Entry[i].CovTerms.Entry[j].PatternCode = conditionTerm.CondTermPatternCode
          _policyPeriod.GLLine.GLLineConditions.Entry[i].CovTerms.Entry[j].DisplayValue =
              conditionTerm.DateTerm == null ?
                  conditionTerm.ChoiceTerm == null ?
                      conditionTerm.DirectTerm == null ?
                          null : conditionTerm.DirectTerm :
                      "Y".equalsIgnoreCase(conditionTerm.ChoiceTerm) ? "true" : "N".equalsIgnoreCase(conditionTerm.ChoiceTerm) ? "false" : conditionTerm.ChoiceTerm
                  : conditionTerm.DateTerm.toString()
        }
      }
    }
  }

  private function prepareGLModifiers() {
    var factorsDAO = new GLRateFactorDAO({_policyPeriodDTO.PublicID})
    var factorsRes = factorsDAO.retrieveAllUnprocessed()
    if (!factorsRes.Empty) {
      var count = _policyPeriod.GLLine.GLLineConditions.Entry.Count
      _policyPeriod.GLLine.GLLineConditions.Entry[count].Pattern.CodeIdentifier = "GLIRPMDiscount_TDIC"
      _policyPeriod.GLLine.GLModifiers.Entry[0].DataType = ModifierDataType.TC_RATE
      _policyPeriod.GLLine.GLModifiers.Entry[0].Pattern.CodeIdentifier = "GLIRPMModifier_TDIC"
      for (factor in factorsRes index i) {
        _policyPeriod.GLLine.GLModifiers.Entry[0].RateFactors.Entry[i].Assessment = factor.Assessment
        //bopBuilding.Modifiers.Entry[count].RateFactors.Entry[i].Pattern.CodeIdentifier = ConversionConstants.RATEFACTOR_MAPPER.get(factor.PatternCode)
        _policyPeriod.GLLine.GLModifiers.Entry[0].RateFactors.Entry[i].RateFactorType = RateFactorType.get(factor.PatternCode)
        _policyPeriod.GLLine.GLModifiers.Entry[0].RateFactors.Entry[i].Justification = ConversionConstants.DEFAULT_JUSTIFICATION
      }
    }
  }

  private function prepareGLClassifications() {
    var exposureDAO = new GLExposureDAO({_policyPeriodDTO.PublicID})
    var exposures = exposureDAO.retrieveAllUnprocessed()
    for (exposure in exposures index i) {
      _policyPeriod.GLLine.Exposures.Entry[i].ClassCode_TDIC = ClassCode_TDIC.get(exposure.ClassCode_TDIC)
      _policyPeriod.GLLine.Exposures.Entry[i].GLTerritory_TDIC.Component = exposure.GLTerritory_TDIC
      _policyPeriod.GLLine.Exposures.Entry[i].SpecialityCode_TDIC = SpecialityCode_TDIC.get(exposure.SpecialityCode_TDIC)
    }
  }

  private function prepareGLAnestheticModalities() {
    var anestheticModalityDAO = new GLAnestheticModalityDAO({_policyPeriodDTO.PublicID})
    var anesthetics = anestheticModalityDAO.retrieveAllUnprocessed()
    _policyPeriod.GLLine.GLIVIMSedationInHospital_TDIC.Entry = new ArrayList<GLLine_GLIVIMSedationInHospital_TDIC_Entry>()
    _policyPeriod.GLLine.GLGeneralAnesthesiaInOffice_TDIC.Entry = new ArrayList<GLLine_GLGeneralAnesthesiaInOffice_TDIC_Entry>()
    for (anesthetic in anesthetics index i){
      var modalityType = typekey.AnestheticModilities_TDIC.get(anesthetic.AnestheticModality)
      switch(modalityType){
        case typekey.AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINOFFICE :
          var ivim = new GLLine_GLIVIMSedationInHospital_TDIC_Entry()
          ivim.Name_TDIC = anesthetic.Name
          ivim.PermitLicenseNumber_TDIC = anesthetic.PermitNumber
          _policyPeriod.GLLine.GLIVIMSedationInHospital_TDIC.Entry.add(ivim)
          break
        case typekey.AnestheticModilities_TDIC.TC_GENERALANESTHESIAINOFFICE :
          var general = new GLLine_GLGeneralAnesthesiaInOffice_TDIC_Entry()
          general.Name_TDIC = anesthetic.Name
          general.PermitLicenseNumber_TDIC = anesthetic.PermitNumber
          _policyPeriod.GLLine.GLGeneralAnesthesiaInOffice_TDIC.Entry.add(general)
          break
        default:
      }
      _policyPeriod.GLLine.GLAnestheticModalities_TDIC.Entry[i].AnestheticModality = modalityType
      prepareAnestheticModalityAnswers(modalityType)
    }
    //_policyPeriod.GLLine.GLAnestheticModalities_TDIC.Entry[]
  }

  private function prepareAnestheticModalityAnswers(modalityType : typekey.AnestheticModilities_TDIC) {
    initializeAnswers()
    var ans = new GLLine_Answers_Entry()
    ans.QuestionCode = ConversionConstants.GLUNDERWRITING_ANESTHETIC_QS.get(modalityType)
    ans.BooleanAnswer = true
    _policyPeriod.GLLine.Answers.Entry.add(ans)
  }

  private function initializeAnswers(){
    if(null == _policyPeriod.GLLine.Answers  or null == _policyPeriod.GLLine.Answers.Entry ){
      _policyPeriod.GLLine.Answers = new GLLine_Answers()
      _policyPeriod.GLLine.Answers.Entry = new ArrayList<GLLine_Answers_Entry>()
    }
  }

  private function prepareLongManuscriptPNI(){
    var pni = _policyPeriod.PolicyContactRoles?.Entry?.firstWhere(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED)
    if(pni.AccountContactRole.AccountContact.Contact.Name.length > 64){
      var longName = pni.AccountContactRole.AccountContact.Contact.Name
      pni.AccountContactRole.AccountContact.Contact.Name = ConversionUtility.formatNamesGreaterThan64Chars(longName,false)
      if(null == _policyPeriod.GLLine.GLLineCoverages.Entry or _policyPeriod.GLLine.GLLineCoverages.Entry.Empty){
        return
      }
      var manuscriptCov = new GLLine_GLLineCoverages_Entry()
      manuscriptCov.PatternCode = ConversionConstants.GL_MANUSCRIPT_PATTERN_CODE
      var covTerms = new GeneralLiabilityCov_CovTerms()
      covTerms.Entry = new ArrayList<GeneralLiabilityCov_CovTerms_Entry>()
      var term = new GeneralLiabilityCov_CovTerms_Entry()
      term.PatternCode = ConversionConstants.GL_MANUSCRIPT_TYPE_PATTERN_CODE
      term.DisplayValue = ConversionConstants.GL_MANUSCRIPT_TYPE_NAMEDINSURED_CONTINUED
      covTerms.Entry.add(term)
      var term2 = new GeneralLiabilityCov_CovTerms_Entry()
      term2.PatternCode = ConversionConstants.GL_MANUSCRIPT_DESC_PATTERN_CODE
      term2.DisplayValue = ConversionUtility.formatNamesGreaterThan64Chars(longName,true)
      covTerms.Entry.add(term2)
      manuscriptCov.CovTerms = covTerms
      _policyPeriod.GLLine.GLLineCoverages.Entry.add(manuscriptCov)
    }
  }

  private function prepareGLManuscript_TDIC() {
    var manuScriptTypes = ConversionConstants.GL_COVERAGETERM_OPTIONS_MAPPER.get("GLManuscriptType_TDIC")
    var manuscriptDAO = new GLManuscriptDAO({_policyPeriodDTO.PublicID})
    var manuscripts = manuscriptDAO.retrieveAllUnprocessed()
    for (manuscript in manuscripts index i) {
      var addlInsuredDAO = new AddlInsuredDAO(new Object[]{manuscript.AdditionalInsured})
      var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
      for (contactRole in addlContacts) {
        var contactDAO = new ContactDAO({contactRole.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].
            PolicyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole = preparePolicyContactRole(contactDTO)
        _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].PolicyAddInsured_TDIC.Desciption = contactRole.Description
        _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].PolicyAddInsured_TDIC.AdditionalInsuredType = typekey.AdditionalInsuredType.TC_OTHER
      }
      var certificateHolderDAO = new AddlInsuredDAO(new Object[]{manuscript.CertificateHolder})
      var certificateHolders = certificateHolderDAO.retrieveAllUnprocessed()
      for (certificateHolder in certificateHolders) {
        var contactDAO = new ContactDAO({certificateHolder.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].
            CertificateHolder.AccountContactRole = preparePolicyContactRole(contactDTO)
        _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].CertificateHolder.entity_PolicyCertificateHolder_TDIC.Description=certificateHolder.Description
      }
      _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].ManuscriptDescription = manuscript.Manuscript_Des
      _policyPeriod.GLLine.GLManuscript_TDIC.Entry[i].ManuscriptType = typekey.GLManuscriptType_TDIC.get(manuScriptTypes.get(manuscript.Manuscript_Type))
    }
  }

}