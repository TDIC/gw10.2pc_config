package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.NoteDTO
uses tdic.pc.conversion.query.PolicyConversionBatchQueries

uses java.sql.Connection

class NoteDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<NoteDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<NoteDTO> {
    _logger.info("Inside NoteDAO.retrieveAllUnprocessed() ")
    var query = PolicyConversionBatchQueries.SELECT_NOTE_QUERY
    var handler = new BeanListHandler(NoteDTO)
    var rows = runQuery(query, _params, handler, _logger) as ArrayList<NoteDTO>

    return rows
  }

  override function update(aTransientObject : NoteDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : NoteDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<NoteDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : NoteDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<NoteDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<NoteDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<NoteDTO>) {
  }

}