package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.ConversionErrorDTO
uses tdic.pc.conversion.query.AccountConversionBatchQueries

uses java.math.BigDecimal
uses java.sql.Connection

class ConversionErrorDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<ConversionErrorDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"
  private static var CREATED_BY : String

  var _params : Object[]


  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : List<ConversionErrorDTO> {
    return null
  }

  override function update(aTransientObject : ConversionErrorDTO) : int {
    return 0
  }

  override function persist(errorDTO : ConversionErrorDTO) : int {
    var connection : Connection
    var insertedRow : BigDecimal = 0
    try {
      connection = getConnection(_logger)
      var params = new ArrayList<Object>()
      params.add(errorDTO.PolicyNumber)
      params.add(errorDTO.AccountKey)
      params.add(errorDTO.Offering_LOB)
      params.add(errorDTO.BatchID)
      params.add(errorDTO.BatchType)
      params.add(errorDTO.ErrorMessageBusiness)
      params.add(errorDTO.ErrorMessageTechnical)

      insertedRow = runUpdate(connection, AccountConversionBatchQueries.INSERT_CONVERSION_ERROR_QUERY, params.toArray(), _logger)
      if (insertedRow > 0) {
        connection.commit()
      }
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
    return insertedRow as int
  }

  override function persistAll(payloads : List<ConversionErrorDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : ConversionErrorDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<ConversionErrorDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<ConversionErrorDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<ConversionErrorDTO>) {
  }

}