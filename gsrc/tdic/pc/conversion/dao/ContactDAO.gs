package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.ContactDTO
uses tdic.pc.conversion.query.AccountConversionBatchQueries

uses java.sql.Connection

class ContactDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<ContactDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<ContactDTO> {
    _logger.info("Inside AccountDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(ContactDTO)
    var rows = runQuery(AccountConversionBatchQueries.CONTACT_DETAILS_SELECT_QUERY, _params, handler, _logger) as ArrayList<ContactDTO>

    return rows
  }

  override function update(aTransientObject : ContactDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : ContactDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<ContactDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : ContactDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<ContactDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<ContactDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<ContactDTO>) {
  }

}