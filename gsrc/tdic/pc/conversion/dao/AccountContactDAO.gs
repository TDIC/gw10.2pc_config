package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.AccountContactDTO
uses tdic.pc.conversion.query.AccountConversionBatchQueries

uses java.sql.Connection

class AccountContactDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<AccountContactDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<AccountContactDTO> {
    _logger.info("Inside AccountContactDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(AccountContactDTO)
    var rows = runQuery(AccountConversionBatchQueries.ACCOUNT_CONTACT_SELECT_QUERY, _params, handler, _logger) as ArrayList<AccountContactDTO>

    return rows
  }

  override function update(aTransientObject : AccountContactDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : AccountContactDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<AccountContactDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : AccountContactDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<AccountContactDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<AccountContactDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<AccountContactDTO>) {
  }

}