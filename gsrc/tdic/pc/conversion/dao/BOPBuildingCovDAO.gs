package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.BOPBuildingCovDTO
uses tdic.pc.conversion.query.PolicyConversionBatchQueries

uses java.sql.Connection

class BOPBuildingCovDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<BOPBuildingCovDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]

  construct(params : Object[]) {
    _params = params
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<BOPBuildingCovDTO> {
    _logger.info("Inside BOPBuildingCovDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(BOPBuildingCovDTO)
    var rows = runQuery(PolicyConversionBatchQueries.BOPBUILDING_COVERAGES_SELECT_QUERY, _params, handler, _logger) as ArrayList<BOPBuildingCovDTO>

    return rows
  }

  override function update(aTransientObject : BOPBuildingCovDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : BOPBuildingCovDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<BOPBuildingCovDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : BOPBuildingCovDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<BOPBuildingCovDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<BOPBuildingCovDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<BOPBuildingCovDTO>) {
  }

}