package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.PayloadDTO
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.query.PolicyConversionBatchQueries
uses tdic.pc.conversion.query.PolicyPayloadBatchQueries

uses java.sql.Connection

class PayloadDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<PayloadDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"
  private static var CREATED_BY : String

  var _batchType : String

  var _params : Object[]

  construct(batchType : String, params : Object[]) {
    _batchType = batchType
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : List<PayloadDTO> {
    _logger.info("Inside PayloadDAO.retrieveAllUnprocessed() ")

    var query : String
    if (_batchType == "AC")
      query = null//AccountConversionBatchQueries.SELECT_ACCOUNT_PAYLOAD_QUERY
    else if (_batchType == "PC")
      query = PolicyConversionBatchQueries.SELECT_POLICY_PAYLOAD_QUERY

    var handler = new BeanListHandler(PayloadDTO)
    var rows = runQuery(query, _params, handler, _logger) as List<PayloadDTO>

    return rows
  }

  override function update(aTransientObject : PayloadDTO) : int {
    return 0
  }

  override function persist(payload : PayloadDTO) : int {
    var connection : Connection
    var insertedRow =0
    try {
      connection = getConnection(_logger)
      var params = new ArrayList<Object>()
      params.add(payload.PolicyNumber)
      params.add(payload.GWPolicyNumber)
      params.add(payload.AccountKey)
      /*var accPayloadSQLXml = connection.createSQLXML()
      accPayloadSQLXml.setString(payload.PayloadXML)*/
      params.add(payload.PayloadXMLAsString)
      params.add(payload.BatchID)
      params.add(payload.BatchType)
      params.add(payload.OfferingLOB)
      params.add(payload.Counter == null ? 0 : payload.Counter)
      params.add(CommonConversionHelper.getCurrentDateTime())
      params.add("su")

      insertedRow = runUpdate(connection, PolicyPayloadBatchQueries.INSERT_PAYLOAD_QUERY, params.toTypedArray(), _logger)
      connection.commit()
    } catch (e : Exception) {
      _logger.error("Error details" + e.StackTraceAsString)
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
    return insertedRow
  }

  override function persistAll(payloads : List<PayloadDTO>) : int[] {
    return null
  }

  property get CreatedBy() : String {
    if (CREATED_BY == null) {
      var connection = getConnection(_logger)
      var rs = connection.createStatement().executeQuery("select suser_sname()")
      if (rs.next()) {
        CREATED_BY = rs.getString(1)
      }
    }
    return CREATED_BY
  }

  override function delete(aTransientObject : PayloadDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<PayloadDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<PayloadDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<PayloadDTO>) {
  }

}