package tdic.pc.conversion.common.logging

uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

class LoggerUtil {

  public static final var CONVERSION : Logger = LoggerFactory.getLogger("Conversion")
  public static final var CONVERSION_ERROR : Logger = LoggerFactory.getLogger("ConversionError")
  public static final var CONVERSION_ERROR_EMAIL : Logger = LoggerFactory.getLogger("ConversionErrorEmail")
  public static final var CONVERSION_ERROR_ADDRESS : Logger = LoggerFactory.getLogger("ConversionErrorAddress")

}