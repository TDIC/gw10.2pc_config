package tdic.pc.conversion.dto

class BuildingDTO {

  var yearBuilt : Integer as YearBuilt
  var constructionType : String as ConstructionType
  var numStories : Integer as NumStories
  var totalArea : Integer as TotalArea
  var totalOfficeArea_TDIC : Integer as TotalOfficeArea_TDIC
  var numOperatories_TDIC : Integer as NumOperatories_TDIC
  var numUnits_TDIC : Integer as NumUnits_TDIC
  var rentalIncome_TDIC : Integer as RentalIncome_TDIC
  var sprinklered_TDIC : String as Sprinklered_TDIC
  var burglarAlarm_TDIC : String as BurglarAlarm_TDIC
  var bopOccupancyStatus_TDIC : String as BOPOccupancyStatus_TDIC
  var territoryDetails_TDIC : String as TerritoryDetails_TDIC
  var protectionClass_TDIC : String as ProtectionCLass_TDIC
  var policyID : String as PolicyID
  var locationID : String as LocationID
  var offeringLOB : String as Offering_LOB
  var publicid : String as PublicID

}