package tdic.pc.conversion.dto

class GLLocumTenensDTO {

  var ltEffDate : String as LTEffectiveDate
  var ltExpDate : String as LTExpirationDate
  var dentistName : String as DentistName

}