package tdic.pc.conversion.dto

class ConversionErrorDTO {

  private var _policyNumber : String as PolicyNumber
  private var _offering : String as Offering_LOB
  private var _acctKey : String as AccountKey
  private var _batchID : long as BatchID
  private var _batchType : String as BatchType
  private var _errorMessageBusiness : String as ErrorMessageBusiness
  private var _errorMessageTechnical : String as ErrorMessageTechnical

  construct() {
  }

  construct(policyNumber : String, acctKey : String, offering : String, batchType : String, batchID : long) {
    _policyNumber = policyNumber
    _offering = offering
    _acctKey = acctKey
    _batchType = batchType
    _batchID = batchID
  }

}