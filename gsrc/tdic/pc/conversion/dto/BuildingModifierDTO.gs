package tdic.pc.conversion.dto

class BuildingModifierDTO {

  var value : String as ValueFinal
  var locationID : String as LocationID
  var offeringLOB : String as Offering_LOB
  var publicid : String as PublicID

}