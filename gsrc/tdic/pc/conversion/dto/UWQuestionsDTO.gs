package tdic.pc.conversion.dto

uses tdic.pc.conversion.constants.ConversionConstants

class UWQuestionsDTO implements Comparable<UWQuestionsDTO> {

  var questionCd : String as QuestionCd
  var answer : String as Answer
  var policyID : String as PolicyID
  var publicID : String as PublicID

  override function compareTo(q : UWQuestionsDTO) : int {
    if(ConversionConstants.LICENSEDFIRSTTIME_QUESTIONCODE ==  q.QuestionCd){
      return 1
    }
    return -1
  }
}