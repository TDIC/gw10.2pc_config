package tdic.pc.conversion.dto

uses tdic.pc.conversion.util.ConversionUtility

class PracticeLocationDTO {

  var _territoryCode : String as Territory_Code
  var _addrLine1 : String as AddressLine1
  var _city : String as City
  var _county : String as County
  var _state : String as State
  var _postalCode : String as PostalCode
  var _mailingLocation : String as MailingLocation
  var _legacyPolicyIDentifies_TDIC : String as LegacyPolicyIDentifies_TDIC

  property get PostalCode() : String {
    return ConversionUtility.formatPostalCode(this._postalCode)
  }

}